.class public LX/2NK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2N8;

.field private final b:LX/2NL;

.field private final c:LX/2Nl;


# direct methods
.method public constructor <init>(LX/2N8;LX/2NL;LX/2Nl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 399175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399176
    iput-object p1, p0, LX/2NK;->a:LX/2N8;

    .line 399177
    iput-object p2, p0, LX/2NK;->b:LX/2NL;

    .line 399178
    iput-object p3, p0, LX/2NK;->c:LX/2Nl;

    .line 399179
    return-void
.end method

.method public static b(LX/0QB;)LX/2NK;
    .locals 5

    .prologue
    .line 399180
    new-instance v3, LX/2NK;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v0

    check-cast v0, LX/2N8;

    invoke-static {p0}, LX/2NL;->a(LX/0QB;)LX/2NL;

    move-result-object v1

    check-cast v1, LX/2NL;

    .line 399181
    new-instance v2, LX/2Nl;

    const/16 v4, 0x1527

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v2, v4}, LX/2Nl;-><init>(LX/0Or;)V

    .line 399182
    move-object v2, v2

    .line 399183
    check-cast v2, LX/2Nl;

    invoke-direct {v3, v0, v1, v2}, LX/2NK;-><init>(LX/2N8;LX/2NL;LX/2Nl;)V

    .line 399184
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/share/Share;)LX/0m9;
    .locals 9
    .param p1    # Lcom/facebook/messaging/model/share/Share;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 399185
    if-nez p1, :cond_0

    .line 399186
    const/4 v0, 0x0

    .line 399187
    :goto_0
    return-object v0

    .line 399188
    :cond_0
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 399189
    const-string v1, "name"

    iget-object v2, p1, Lcom/facebook/messaging/model/share/Share;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399190
    const-string v1, "caption"

    iget-object v2, p1, Lcom/facebook/messaging/model/share/Share;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399191
    const-string v1, "description"

    iget-object v2, p1, Lcom/facebook/messaging/model/share/Share;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399192
    const-string v1, "href"

    iget-object v2, p1, Lcom/facebook/messaging/model/share/Share;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399193
    const-string v1, "media"

    iget-object v2, p1, Lcom/facebook/messaging/model/share/Share;->g:LX/0Px;

    .line 399194
    new-instance v4, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v3}, LX/162;-><init>(LX/0mC;)V

    .line 399195
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/share/ShareMedia;

    .line 399196
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 399197
    const-string v7, "type"

    iget-object v8, v3, Lcom/facebook/messaging/model/share/ShareMedia;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    invoke-virtual {v8}, Lcom/facebook/messaging/model/share/ShareMedia$Type;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399198
    const-string v7, "src"

    iget-object v8, v3, Lcom/facebook/messaging/model/share/ShareMedia;->c:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399199
    const-string v7, "href"

    iget-object v8, v3, Lcom/facebook/messaging/model/share/ShareMedia;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399200
    sget-object v7, Lcom/facebook/messaging/model/share/ShareMedia$Type;->VIDEO:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    iget-object v8, v3, Lcom/facebook/messaging/model/share/ShareMedia;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    invoke-virtual {v7, v8}, Lcom/facebook/messaging/model/share/ShareMedia$Type;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, v3, Lcom/facebook/messaging/model/share/ShareMedia;->d:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 399201
    const-string v7, "playable_src"

    iget-object v3, v3, Lcom/facebook/messaging/model/share/ShareMedia;->d:Ljava/lang/String;

    invoke-virtual {v6, v7, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399202
    :cond_1
    invoke-virtual {v4, v6}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_1

    .line 399203
    :cond_2
    move-object v2, v4

    .line 399204
    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399205
    const-string v1, "properties"

    iget-object v2, p1, Lcom/facebook/messaging/model/share/Share;->h:LX/0Px;

    .line 399206
    new-instance v4, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v3}, LX/162;-><init>(LX/0mC;)V

    .line 399207
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/share/ShareProperty;

    .line 399208
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 399209
    const-string v7, "name"

    iget-object v2, v3, Lcom/facebook/messaging/model/share/ShareProperty;->a:Ljava/lang/String;

    invoke-virtual {v6, v7, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399210
    const-string v7, "text"

    iget-object v2, v3, Lcom/facebook/messaging/model/share/ShareProperty;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399211
    const-string v7, "href"

    iget-object v2, v3, Lcom/facebook/messaging/model/share/ShareProperty;->c:Ljava/lang/String;

    invoke-virtual {v6, v7, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399212
    move-object v3, v6

    .line 399213
    invoke-virtual {v4, v3}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_2

    .line 399214
    :cond_3
    move-object v2, v4

    .line 399215
    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399216
    const-string v1, "robotext"

    iget-object v2, p1, Lcom/facebook/messaging/model/share/Share;->i:Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    .line 399217
    if-nez v2, :cond_6

    .line 399218
    const/4 v3, 0x0

    .line 399219
    :goto_3
    move-object v2, v3

    .line 399220
    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399221
    const-string v1, "attribution"

    iget-object v2, p1, Lcom/facebook/messaging/model/share/Share;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399222
    const-string v1, "deep_link_url"

    iget-object v2, p1, Lcom/facebook/messaging/model/share/Share;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399223
    const-string v1, "commerce_data"

    iget-object v2, p1, Lcom/facebook/messaging/model/share/Share;->l:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    invoke-static {v2}, LX/2NL;->a(Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;)LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399224
    const-string v1, "moments_invite_data"

    iget-object v2, p0, LX/2NK;->c:LX/2Nl;

    iget-object v3, p1, Lcom/facebook/messaging/model/share/Share;->m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    .line 399225
    iget-object v4, v2, LX/2Nl;->a:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_4

    if-nez v3, :cond_8

    .line 399226
    :cond_4
    const/4 v4, 0x0

    .line 399227
    :cond_5
    :goto_4
    move-object v2, v4

    .line 399228
    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto/16 :goto_0

    .line 399229
    :cond_6
    new-instance v4, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 399230
    const-string v3, "robotext"

    .line 399231
    iget-object v5, v2, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->a:Ljava/lang/String;

    move-object v5, v5

    .line 399232
    invoke-virtual {v4, v3, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399233
    new-instance v5, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v3}, LX/162;-><init>(LX/0mC;)V

    .line 399234
    iget-object v3, v2, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;->b:Ljava/util/List;

    move-object v3, v3

    .line 399235
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext$Span;

    .line 399236
    new-instance v7, LX/0m9;

    sget-object v8, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v8}, LX/0m9;-><init>(LX/0mC;)V

    .line 399237
    const-string v8, "start"

    .line 399238
    iget v2, v3, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext$Span;->mOffset:I

    move v2, v2

    .line 399239
    invoke-virtual {v7, v8, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 399240
    const-string v8, "end"

    invoke-virtual {v3}, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext$Span;->b()I

    move-result v2

    invoke-virtual {v7, v8, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 399241
    move-object v3, v7

    .line 399242
    invoke-virtual {v5, v3}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_5

    .line 399243
    :cond_7
    const-string v3, "spans"

    invoke-virtual {v4, v3, v5}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    move-object v3, v4

    .line 399244
    goto/16 :goto_3

    .line 399245
    :cond_8
    new-instance v6, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v4}, LX/162;-><init>(LX/0mC;)V

    .line 399246
    iget-object v4, v3, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->a:LX/0Px;

    move-object v7, v4

    .line 399247
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v5, v4

    :goto_6
    if-ge v5, v8, :cond_9

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 399248
    invoke-virtual {v6, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 399249
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_6

    .line 399250
    :cond_9
    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 399251
    const-string v5, "image_srcs"

    invoke-virtual {v4, v5, v6}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 399252
    const-string v5, "photo_count"

    .line 399253
    iget v6, v3, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->b:I

    move v6, v6

    .line 399254
    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 399255
    iget-object v5, v3, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->c:Ljava/lang/String;

    move-object v5, v5

    .line 399256
    if-eqz v5, :cond_a

    .line 399257
    const-string v5, "share_xma_token"

    .line 399258
    iget-object v6, v3, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->c:Ljava/lang/String;

    move-object v6, v6

    .line 399259
    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399260
    :cond_a
    iget-object v5, v3, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->d:Ljava/lang/String;

    move-object v5, v5

    .line 399261
    if-eqz v5, :cond_5

    .line 399262
    const-string v5, "share_id"

    .line 399263
    iget-object v6, v3, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;->d:Ljava/lang/String;

    move-object v6, v6

    .line 399264
    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto/16 :goto_4
.end method

.method public final a(LX/0lF;)Lcom/facebook/messaging/model/share/Share;
    .locals 9

    .prologue
    .line 399265
    new-instance v0, LX/6fT;

    invoke-direct {v0}, LX/6fT;-><init>()V

    .line 399266
    const-string v1, "fbid"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399267
    iput-object v1, v0, LX/6fT;->a:Ljava/lang/String;

    .line 399268
    const-string v1, "name"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399269
    iput-object v1, v0, LX/6fT;->c:Ljava/lang/String;

    .line 399270
    const-string v1, "caption"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399271
    iput-object v1, v0, LX/6fT;->d:Ljava/lang/String;

    .line 399272
    const-string v1, "description"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399273
    iput-object v1, v0, LX/6fT;->e:Ljava/lang/String;

    .line 399274
    const-string v1, "media"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 399275
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 399276
    invoke-virtual {v1}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lF;

    .line 399277
    new-instance v5, LX/6fX;

    invoke-direct {v5}, LX/6fX;-><init>()V

    .line 399278
    const-string v6, "href"

    invoke-virtual {v2, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 399279
    iput-object v6, v5, LX/6fX;->b:Ljava/lang/String;

    .line 399280
    const-string v6, "type"

    invoke-virtual {v2, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/messaging/model/share/ShareMedia$Type;->fromString(Ljava/lang/String;)Lcom/facebook/messaging/model/share/ShareMedia$Type;

    move-result-object v6

    .line 399281
    iput-object v6, v5, LX/6fX;->a:Lcom/facebook/messaging/model/share/ShareMedia$Type;

    .line 399282
    const-string v6, "src"

    invoke-virtual {v2, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 399283
    iput-object v6, v5, LX/6fX;->c:Ljava/lang/String;

    .line 399284
    const-string v6, "playable_src"

    invoke-virtual {v2, v6}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 399285
    const-string v6, "playable_src"

    invoke-virtual {v2, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 399286
    iput-object v6, v5, LX/6fX;->d:Ljava/lang/String;

    .line 399287
    :cond_0
    :goto_1
    invoke-virtual {v5}, LX/6fX;->e()Lcom/facebook/messaging/model/share/ShareMedia;

    move-result-object v5

    move-object v2, v5

    .line 399288
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 399289
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 399290
    iput-object v1, v0, LX/6fT;->g:Ljava/util/List;

    .line 399291
    const-string v1, "href"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399292
    iput-object v1, v0, LX/6fT;->f:Ljava/lang/String;

    .line 399293
    const-string v1, "properties"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 399294
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 399295
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v1}, LX/0lF;->e()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 399296
    invoke-virtual {v1, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v4

    .line 399297
    const-string v5, "name"

    invoke-virtual {v4, v5}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "text"

    invoke-virtual {v4, v5}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 399298
    new-instance v5, LX/6fZ;

    invoke-direct {v5}, LX/6fZ;-><init>()V

    .line 399299
    const-string v6, "name"

    invoke-virtual {v4, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 399300
    iput-object v6, v5, LX/6fZ;->a:Ljava/lang/String;

    .line 399301
    const-string v6, "text"

    invoke-virtual {v4, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 399302
    iput-object v6, v5, LX/6fZ;->b:Ljava/lang/String;

    .line 399303
    const-string v6, "href"

    invoke-virtual {v4, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399304
    iput-object v4, v5, LX/6fZ;->c:Ljava/lang/String;

    .line 399305
    new-instance v4, Lcom/facebook/messaging/model/share/ShareProperty;

    invoke-direct {v4, v5}, Lcom/facebook/messaging/model/share/ShareProperty;-><init>(LX/6fZ;)V

    move-object v4, v4

    .line 399306
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399307
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 399308
    :cond_3
    move-object v1, v3

    .line 399309
    iput-object v1, v0, LX/6fT;->h:Ljava/util/List;

    .line 399310
    const-string v1, "robotext"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 399311
    if-eqz v1, :cond_4

    instance-of v2, v1, LX/2FN;

    if-eqz v2, :cond_a

    .line 399312
    :cond_4
    const/4 v2, 0x0

    .line 399313
    :goto_3
    move-object v1, v2

    .line 399314
    iput-object v1, v0, LX/6fT;->i:Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    .line 399315
    const-string v1, "attribution"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399316
    iput-object v1, v0, LX/6fT;->j:Ljava/lang/String;

    .line 399317
    const-string v1, "deep_link_url"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399318
    iput-object v1, v0, LX/6fT;->k:Ljava/lang/String;

    .line 399319
    const-string v2, "commerce_data"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const/4 v1, 0x0

    .line 399320
    const/4 v4, 0x0

    .line 399321
    if-eqz v2, :cond_5

    invoke-virtual {v2}, LX/0lF;->i()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, LX/0lF;->e()I

    move-result v3

    if-nez v3, :cond_16

    :cond_5
    move-object v3, v4

    .line 399322
    :cond_6
    :goto_4
    move-object v3, v3

    .line 399323
    if-nez v3, :cond_c

    .line 399324
    :cond_7
    :goto_5
    move-object v1, v1

    .line 399325
    iput-object v1, v0, LX/6fT;->l:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    .line 399326
    iget-object v1, p0, LX/2NK;->c:LX/2Nl;

    const-string v2, "moments_invite_data"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 399327
    iget-object v3, v1, LX/2Nl;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_8

    if-eqz v2, :cond_8

    invoke-virtual {v2}, LX/0lF;->i()Z

    move-result v3

    if-nez v3, :cond_1d

    .line 399328
    :cond_8
    const/4 v3, 0x0

    .line 399329
    :goto_6
    move-object v1, v3

    .line 399330
    iput-object v1, v0, LX/6fT;->m:Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    .line 399331
    invoke-virtual {v0}, LX/6fT;->n()Lcom/facebook/messaging/model/share/Share;

    move-result-object v0

    return-object v0

    .line 399332
    :cond_9
    const-string v6, "video"

    invoke-virtual {v2, v6}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 399333
    const-string v6, "video"

    invoke-virtual {v2, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    const-string v1, "source_url"

    invoke-virtual {v6, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 399334
    iput-object v6, v5, LX/6fX;->d:Ljava/lang/String;

    .line 399335
    goto/16 :goto_1

    .line 399336
    :cond_a
    const-string v2, "robotext"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 399337
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 399338
    const-string v2, "spans"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 399339
    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v5}, LX/0lF;->e()I

    move-result v6

    if-ge v2, v6, :cond_b

    .line 399340
    invoke-virtual {v5, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v6

    .line 399341
    const-string v7, "start"

    invoke-virtual {v6, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->d(LX/0lF;)I

    move-result v7

    .line 399342
    const-string v8, "end"

    invoke-virtual {v6, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    invoke-static {v8}, LX/16N;->d(LX/0lF;)I

    move-result v8

    .line 399343
    sub-int/2addr v8, v7

    .line 399344
    new-instance v1, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext$Span;

    invoke-direct {v1, v7, v8}, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext$Span;-><init>(II)V

    move-object v6, v1

    .line 399345
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399346
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 399347
    :cond_b
    new-instance v2, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    invoke-direct {v2, v3, v4}, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto/16 :goto_3

    .line 399348
    :cond_c
    const-string v4, "messenger_commerce_bubble_type"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 399349
    if-nez v4, :cond_1b

    .line 399350
    sget-object v4, LX/5TJ;->UNKNOWN:LX/5TJ;

    .line 399351
    :goto_8
    move-object v4, v4

    .line 399352
    sget-object v5, LX/5TJ;->RECEIPT:LX/5TJ;

    if-ne v4, v5, :cond_d

    .line 399353
    new-instance v1, LX/5TM;

    invoke-direct {v1}, LX/5TM;-><init>()V

    .line 399354
    const-string v4, "receipt_id"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399355
    iput-object v4, v1, LX/5TM;->a:Ljava/lang/String;

    .line 399356
    const-string v4, "order_id"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399357
    iput-object v4, v1, LX/5TM;->b:Ljava/lang/String;

    .line 399358
    const-string v4, "shipping_method"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399359
    iput-object v4, v1, LX/5TM;->c:Ljava/lang/String;

    .line 399360
    const-string v4, "payment_method"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399361
    iput-object v4, v1, LX/5TM;->d:Ljava/lang/String;

    .line 399362
    const-string v4, "order_url"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/5TM;->e(Ljava/lang/String;)LX/5TM;

    .line 399363
    const-string v4, "cancellation_url"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399364
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1c

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    :goto_9
    iput-object v5, v1, LX/5TM;->f:Landroid/net/Uri;

    .line 399365
    const-string v4, "structured_address"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/2NL;->g(LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    move-result-object v4

    .line 399366
    iput-object v4, v1, LX/5TM;->g:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

    .line 399367
    const-string v4, "status"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399368
    iput-object v4, v1, LX/5TM;->h:Ljava/lang/String;

    .line 399369
    const-string v4, "total_cost"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399370
    iput-object v4, v1, LX/5TM;->i:Ljava/lang/String;

    .line 399371
    const-string v4, "total_tax"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399372
    iput-object v4, v1, LX/5TM;->j:Ljava/lang/String;

    .line 399373
    const-string v4, "shipping_cost"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399374
    iput-object v4, v1, LX/5TM;->k:Ljava/lang/String;

    .line 399375
    const-string v4, "subtotal"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399376
    iput-object v4, v1, LX/5TM;->m:Ljava/lang/String;

    .line 399377
    const-string v4, "order_time"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399378
    iput-object v4, v1, LX/5TM;->n:Ljava/lang/String;

    .line 399379
    const-string v4, "partner_logo"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/2NL;->i(LX/0lF;)Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-result-object v4

    .line 399380
    iput-object v4, v1, LX/5TM;->p:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 399381
    const-string v4, "items"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/2NL;->h(LX/0lF;)Ljava/util/List;

    move-result-object v4

    .line 399382
    iput-object v4, v1, LX/5TM;->q:Ljava/util/List;

    .line 399383
    const-string v4, "recipient_name"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399384
    iput-object v4, v1, LX/5TM;->r:Ljava/lang/String;

    .line 399385
    const-string v4, "account_holder_name"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399386
    iput-object v4, v1, LX/5TM;->s:Ljava/lang/String;

    .line 399387
    new-instance v4, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    invoke-virtual {v1}, LX/5TM;->v()Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;-><init>(Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;)V

    move-object v1, v4

    .line 399388
    goto/16 :goto_5

    .line 399389
    :cond_d
    sget-object v5, LX/5TJ;->CANCELLATION:LX/5TJ;

    if-ne v4, v5, :cond_e

    .line 399390
    new-instance v1, LX/5TO;

    invoke-direct {v1}, LX/5TO;-><init>()V

    .line 399391
    const-string v4, "cancellation_id"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399392
    iput-object v4, v1, LX/5TO;->a:Ljava/lang/String;

    .line 399393
    const-string v4, "items"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/2NL;->h(LX/0lF;)Ljava/util/List;

    move-result-object v4

    .line 399394
    iput-object v4, v1, LX/5TO;->d:Ljava/util/List;

    .line 399395
    new-instance v4, LX/5TM;

    invoke-direct {v4}, LX/5TM;-><init>()V

    const-string v5, "receipt_id"

    invoke-virtual {v3, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 399396
    iput-object v5, v4, LX/5TM;->a:Ljava/lang/String;

    .line 399397
    move-object v4, v4

    .line 399398
    const-string v5, "order_id"

    invoke-virtual {v3, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 399399
    iput-object v5, v4, LX/5TM;->b:Ljava/lang/String;

    .line 399400
    move-object v4, v4

    .line 399401
    const-string v5, "partner_logo"

    invoke-virtual {v3, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/2NL;->i(LX/0lF;)Lcom/facebook/messaging/business/attachments/model/LogoImage;

    move-result-object v5

    .line 399402
    iput-object v5, v4, LX/5TM;->p:Lcom/facebook/messaging/business/attachments/model/LogoImage;

    .line 399403
    move-object v4, v4

    .line 399404
    invoke-virtual {v4}, LX/5TM;->v()Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    move-result-object v4

    .line 399405
    iput-object v4, v1, LX/5TO;->b:Lcom/facebook/messaging/business/commerce/model/retail/Receipt;

    .line 399406
    new-instance v4, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    invoke-virtual {v1}, LX/5TO;->e()Lcom/facebook/messaging/business/commerce/model/retail/ReceiptCancellation;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;-><init>(Lcom/facebook/messaging/business/commerce/model/retail/CommerceBubbleModel;)V

    move-object v1, v4

    .line 399407
    goto/16 :goto_5

    .line 399408
    :cond_e
    sget-object v5, LX/5TJ;->SHIPMENT:LX/5TJ;

    if-ne v4, v5, :cond_f

    .line 399409
    invoke-static {v3}, LX/2NL;->f(LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    move-result-object v1

    goto/16 :goto_5

    .line 399410
    :cond_f
    sget-object v5, LX/5TJ;->SHIPMENT_TRACKING_ETA:LX/5TJ;

    if-ne v4, v5, :cond_10

    .line 399411
    invoke-static {v4, v3}, LX/2NL;->a(LX/5TJ;LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    move-result-object v1

    goto/16 :goto_5

    .line 399412
    :cond_10
    sget-object v5, LX/5TJ;->SHIPMENT_TRACKING_IN_TRANSIT:LX/5TJ;

    if-ne v4, v5, :cond_11

    .line 399413
    invoke-static {v4, v3}, LX/2NL;->a(LX/5TJ;LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    move-result-object v1

    goto/16 :goto_5

    .line 399414
    :cond_11
    sget-object v5, LX/5TJ;->SHIPMENT_TRACKING_OUT_FOR_DELIVERY:LX/5TJ;

    if-ne v4, v5, :cond_12

    .line 399415
    invoke-static {v4, v3}, LX/2NL;->a(LX/5TJ;LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    move-result-object v1

    goto/16 :goto_5

    .line 399416
    :cond_12
    sget-object v5, LX/5TJ;->SHIPMENT_TRACKING_DELAYED:LX/5TJ;

    if-ne v4, v5, :cond_13

    .line 399417
    invoke-static {v4, v3}, LX/2NL;->a(LX/5TJ;LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    move-result-object v1

    goto/16 :goto_5

    .line 399418
    :cond_13
    sget-object v5, LX/5TJ;->SHIPMENT_TRACKING_DELIVERED:LX/5TJ;

    if-ne v4, v5, :cond_14

    .line 399419
    invoke-static {v4, v3}, LX/2NL;->a(LX/5TJ;LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    move-result-object v1

    goto/16 :goto_5

    .line 399420
    :cond_14
    sget-object v5, LX/5TJ;->SHIPMENT_FOR_UNSUPPORTED_CARRIER:LX/5TJ;

    if-ne v4, v5, :cond_15

    .line 399421
    invoke-static {v3}, LX/2NL;->f(LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    move-result-object v1

    goto/16 :goto_5

    .line 399422
    :cond_15
    sget-object v5, LX/5TJ;->SHIPMENT_ETA:LX/5TJ;

    if-ne v4, v5, :cond_7

    .line 399423
    invoke-static {v4, v3}, LX/2NL;->a(LX/5TJ;LX/0lF;)Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    move-result-object v1

    goto/16 :goto_5

    .line 399424
    :cond_16
    const-string v3, "fb_object_contents"

    invoke-virtual {v2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 399425
    invoke-virtual {v2}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lF;

    move-object v2, v3

    .line 399426
    :cond_17
    if-eqz v2, :cond_19

    invoke-virtual {v2}, LX/0lF;->i()Z

    move-result v3

    if-eqz v3, :cond_19

    const/4 v3, 0x1

    .line 399427
    :goto_a
    if-eqz v3, :cond_1a

    const-string v3, "fb_object_contents"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 399428
    :goto_b
    if-eqz v3, :cond_18

    invoke-virtual {v3}, LX/0lF;->e()I

    move-result v5

    if-nez v5, :cond_6

    :cond_18
    move-object v3, v4

    .line 399429
    goto/16 :goto_4

    .line 399430
    :cond_19
    const/4 v3, 0x0

    goto :goto_a

    :cond_1a
    move-object v3, v4

    .line 399431
    goto :goto_b

    :cond_1b
    invoke-static {v4}, LX/16N;->d(LX/0lF;)I

    move-result v4

    invoke-static {v4}, LX/5TJ;->getModelType(I)LX/5TJ;

    move-result-object v4

    goto/16 :goto_8

    .line 399432
    :cond_1c
    const/4 v5, 0x0

    goto/16 :goto_9

    .line 399433
    :cond_1d
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 399434
    const-string v3, "image_srcs"

    invoke-virtual {v2, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 399435
    const-string v3, "image_srcs"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 399436
    invoke-virtual {v3}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1e
    :goto_c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lF;

    .line 399437
    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 399438
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1e

    .line 399439
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 399440
    :cond_1f
    invoke-static {}, LX/6gB;->newBuilder()LX/6gB;

    move-result-object v3

    .line 399441
    invoke-virtual {v3, v4}, LX/6gB;->a(Ljava/util/List;)LX/6gB;

    .line 399442
    const-string v4, "photo_count"

    invoke-virtual {v2, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 399443
    const-string v4, "photo_count"

    invoke-virtual {v2, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->d(LX/0lF;)I

    move-result v4

    .line 399444
    iput v4, v3, LX/6gB;->b:I

    .line 399445
    :cond_20
    const-string v4, "share_xma_token"

    invoke-virtual {v2, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 399446
    const-string v4, "share_xma_token"

    invoke-virtual {v2, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399447
    iput-object v4, v3, LX/6gB;->c:Ljava/lang/String;

    .line 399448
    :cond_21
    const-string v4, "share_id"

    invoke-virtual {v2, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 399449
    const-string v4, "share_id"

    invoke-virtual {v2, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 399450
    iput-object v4, v3, LX/6gB;->d:Ljava/lang/String;

    .line 399451
    :cond_22
    invoke-virtual {v3}, LX/6gB;->e()Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    move-result-object v3

    goto/16 :goto_6
.end method
