.class public LX/2V1;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/2V2;

.field private final c:LX/0s6;

.field private final d:LX/0WV;

.field private final e:Landroid/content/Context;

.field private final f:LX/03V;

.field private final g:LX/2V3;

.field private final h:LX/00I;

.field private final i:LX/11H;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 416981
    const-class v0, LX/2V1;

    sput-object v0, LX/2V1;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2V2;LX/0s6;LX/0WV;Landroid/content/Context;LX/03V;LX/2V3;LX/00I;LX/11H;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416983
    iput-object p1, p0, LX/2V1;->b:LX/2V2;

    .line 416984
    iput-object p2, p0, LX/2V1;->c:LX/0s6;

    .line 416985
    iput-object p3, p0, LX/2V1;->d:LX/0WV;

    .line 416986
    iput-object p4, p0, LX/2V1;->e:Landroid/content/Context;

    .line 416987
    iput-object p5, p0, LX/2V1;->f:LX/03V;

    .line 416988
    iput-object p6, p0, LX/2V1;->g:LX/2V3;

    .line 416989
    iput-object p7, p0, LX/2V1;->h:LX/00I;

    .line 416990
    iput-object p8, p0, LX/2V1;->i:LX/11H;

    .line 416991
    return-void
.end method

.method public static b(LX/0QB;)LX/2V1;
    .locals 9

    .prologue
    .line 416992
    new-instance v0, LX/2V1;

    .line 416993
    new-instance v1, LX/2V2;

    invoke-direct {v1}, LX/2V2;-><init>()V

    .line 416994
    move-object v1, v1

    .line 416995
    move-object v1, v1

    .line 416996
    check-cast v1, LX/2V2;

    invoke-static {p0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v2

    check-cast v2, LX/0s6;

    invoke-static {p0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v3

    check-cast v3, LX/0WV;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/2V3;->b(LX/0QB;)LX/2V3;

    move-result-object v6

    check-cast v6, LX/2V3;

    const-class v7, LX/00I;

    invoke-interface {p0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/00I;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v8

    check-cast v8, LX/11H;

    invoke-direct/range {v0 .. v8}, LX/2V1;-><init>(LX/2V2;LX/0s6;LX/0WV;Landroid/content/Context;LX/03V;LX/2V3;LX/00I;LX/11H;)V

    .line 416997
    return-object v0
.end method


# virtual methods
.method public final a(Z)LX/Fjz;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 416998
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 416999
    :goto_0
    iget-object v2, p0, LX/2V1;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 417000
    iget-object v3, p0, LX/2V1;->h:LX/00I;

    invoke-interface {v3}, LX/00I;->c()Ljava/lang/String;

    move-result-object v3

    .line 417001
    new-instance v4, Lcom/facebook/selfupdate/protocol/AppServerParams;

    invoke-direct {v4, v3, v0, v2}, Lcom/facebook/selfupdate/protocol/AppServerParams;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 417002
    :try_start_0
    iget-object v0, p0, LX/2V1;->i:LX/11H;

    iget-object v2, p0, LX/2V1;->b:LX/2V2;

    invoke-virtual {v0, v2, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fjz;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 417003
    :goto_1
    return-object v0

    .line 417004
    :cond_0
    iget-object v0, p0, LX/2V1;->d:LX/0WV;

    invoke-virtual {v0}, LX/0WV;->b()I

    move-result v0

    goto :goto_0

    .line 417005
    :catch_0
    move-object v0, v1

    goto :goto_1

    .line 417006
    :catch_1
    move-exception v0

    .line 417007
    iget-object v2, p0, LX/2V1;->f:LX/03V;

    sget-object v3, LX/2V1;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failed to fetch update information from server"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 417008
    goto :goto_1
.end method

.method public final b(Z)LX/Fk1;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 417009
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 417010
    :goto_0
    iget-object v2, p0, LX/2V1;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 417011
    :try_start_0
    iget-object v3, p0, LX/2V1;->c:LX/0s6;

    const/16 v4, 0x40

    invoke-virtual {v3, v2, v4}, LX/01H;->d(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 417012
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v4, :cond_1

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v4

    const/4 p1, 0x1

    if-ne v4, p1, :cond_1

    .line 417013
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 p1, 0x0

    aget-object v4, v4, p1

    invoke-virtual {v4}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v4

    invoke-static {v4}, LX/03l;->a([B)Ljava/lang/String;

    move-result-object v4

    .line 417014
    :goto_1
    move-object v3, v4

    .line 417015
    new-instance v4, LX/Fk0;

    invoke-direct {v4, v2, v3, v0}, LX/Fk0;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 417016
    iget-object v0, p0, LX/2V1;->i:LX/11H;

    iget-object v2, p0, LX/2V1;->g:LX/2V3;

    invoke-virtual {v0, v2, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fk1;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 417017
    :goto_2
    return-object v0

    .line 417018
    :cond_0
    iget-object v0, p0, LX/2V1;->d:LX/0WV;

    invoke-virtual {v0}, LX/0WV;->b()I

    move-result v0

    goto :goto_0

    .line 417019
    :catch_0
    move-object v0, v1

    goto :goto_2

    .line 417020
    :catch_1
    move-object v0, v1

    goto :goto_2

    .line 417021
    :catch_2
    move-exception v0

    .line 417022
    iget-object v2, p0, LX/2V1;->f:LX/03V;

    sget-object v3, LX/2V1;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failed to fetch update information from server"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 417023
    goto :goto_2

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method
