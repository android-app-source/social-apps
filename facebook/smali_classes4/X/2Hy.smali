.class public LX/2Hy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2AX;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Hy;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2Hy;
    .locals 3

    .prologue
    .line 391352
    sget-object v0, LX/2Hy;->a:LX/2Hy;

    if-nez v0, :cond_1

    .line 391353
    const-class v1, LX/2Hy;

    monitor-enter v1

    .line 391354
    :try_start_0
    sget-object v0, LX/2Hy;->a:LX/2Hy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391355
    if-eqz v2, :cond_0

    .line 391356
    :try_start_1
    new-instance v0, LX/2Hy;

    invoke-direct {v0}, LX/2Hy;-><init>()V

    .line 391357
    move-object v0, v0

    .line 391358
    sput-object v0, LX/2Hy;->a:LX/2Hy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391359
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391360
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391361
    :cond_1
    sget-object v0, LX/2Hy;->a:LX/2Hy;

    return-object v0

    .line 391362
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391363
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391364
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 391365
    new-instance v1, LX/1se;

    const-string v2, "/get_media_resp"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v2, LX/2C3;->ALWAYS:LX/2C3;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 391366
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
