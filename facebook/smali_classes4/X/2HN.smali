.class public LX/2HN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Gb;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2HN;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cf6;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/registration/FacebookPushServerRegistrar;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2Gc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 390354
    const-class v0, LX/2HN;

    sput-object v0, LX/2HN;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Or;LX/2Gc;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Cf6;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/registration/FacebookPushServerRegistrar;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2Gc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390349
    iput-object p1, p0, LX/2HN;->b:LX/0Ot;

    .line 390350
    iput-object p2, p0, LX/2HN;->c:LX/0Ot;

    .line 390351
    iput-object p3, p0, LX/2HN;->d:LX/0Or;

    .line 390352
    iput-object p4, p0, LX/2HN;->e:LX/2Gc;

    .line 390353
    return-void
.end method

.method public static a(LX/0QB;)LX/2HN;
    .locals 7

    .prologue
    .line 390335
    sget-object v0, LX/2HN;->f:LX/2HN;

    if-nez v0, :cond_1

    .line 390336
    const-class v1, LX/2HN;

    monitor-enter v1

    .line 390337
    :try_start_0
    sget-object v0, LX/2HN;->f:LX/2HN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390338
    if-eqz v2, :cond_0

    .line 390339
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390340
    new-instance v4, LX/2HN;

    const/16 v3, 0x3025

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x1027

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x15e7

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2Gc;->a(LX/0QB;)LX/2Gc;

    move-result-object v3

    check-cast v3, LX/2Gc;

    invoke-direct {v4, v5, v6, p0, v3}, LX/2HN;-><init>(LX/0Ot;LX/0Ot;LX/0Or;LX/2Gc;)V

    .line 390341
    move-object v0, v4

    .line 390342
    sput-object v0, LX/2HN;->f:LX/2HN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390343
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390344
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390345
    :cond_1
    sget-object v0, LX/2HN;->f:LX/2HN;

    return-object v0

    .line 390346
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390347
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 390334
    iget-object v0, p0, LX/2HN;->e:LX/2Gc;

    sget-object v1, LX/2Ge;->NNA:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gc;->a(LX/2Ge;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/2Ge;
    .locals 1

    .prologue
    .line 390318
    sget-object v0, LX/2Ge;->NNA:LX/2Ge;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 390331
    invoke-direct {p0}, LX/2HN;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390332
    :goto_0
    return-void

    .line 390333
    :cond_0
    iget-object v0, p0, LX/2HN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->NNA:LX/2Ge;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 390328
    invoke-direct {p0}, LX/2HN;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390329
    iget-object v0, p0, LX/2HN;->e:LX/2Gc;

    const-string v1, "com.nokia.pushnotifications.service"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Lcom/facebook/push/nna/NNABroadcastReceiver;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Lcom/facebook/push/nna/NNAService;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, LX/2Gc;->a(Ljava/lang/String;[Ljava/lang/Class;)V

    .line 390330
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 390325
    invoke-direct {p0}, LX/2HN;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390326
    :goto_0
    return-void

    .line 390327
    :cond_0
    iget-object v0, p0, LX/2HN;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cf6;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Cf6;->a(Z)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 390321
    invoke-direct {p0}, LX/2HN;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 390322
    :cond_0
    :goto_0
    return-void

    .line 390323
    :cond_1
    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/CharSequence;

    iget-object v0, p0, LX/2HN;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    aput-object v0, v1, v2

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 390324
    iget-object v0, p0, LX/2HN;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cf6;

    invoke-virtual {v0, v2}, LX/Cf6;->a(Z)V

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 390319
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/2HN;->a(Ljava/lang/String;)V

    .line 390320
    return-void
.end method
