.class public final LX/2qF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2qG;


# instance fields
.field public final synthetic a:LX/2q6;


# direct methods
.method public constructor <init>(LX/2q6;)V
    .locals 0

    .prologue
    .line 470781
    iput-object p1, p0, LX/2qF;->a:LX/2q6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 470782
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 470783
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v1, p0, LX/2qF;->a:LX/2q6;

    iget-object v1, v1, LX/2q6;->m:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, v0, LX/2q6;->K:J

    .line 470784
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-boolean v0, v0, LX/2q6;->L:Z

    if-eqz v0, :cond_1

    .line 470785
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v0, v0, LX/2q6;->g:LX/1C2;

    iget-object v1, p0, LX/2qF;->a:LX/2q6;

    iget-object v1, v1, LX/2q6;->y:LX/04G;

    iget-object v2, p0, LX/2qF;->a:LX/2q6;

    iget-object v2, v2, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v3, p0, LX/2qF;->a:LX/2q6;

    iget-object v3, v3, LX/2q6;->w:LX/04D;

    iget-object v4, p0, LX/2qF;->a:LX/2q6;

    iget-object v4, v4, LX/2q6;->z:LX/04g;

    iget-object v4, v4, LX/04g;->value:Ljava/lang/String;

    iget-object v5, p0, LX/2qF;->a:LX/2q6;

    invoke-virtual {v5}, LX/2q6;->r()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/1C2;->a(LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;)LX/1C2;

    .line 470786
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iput-boolean v6, v0, LX/2q6;->L:Z

    .line 470787
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    sget-object v1, LX/2qE;->STATE_UPDATED:LX/2qE;

    iput-object v1, v0, LX/2q6;->V:LX/2qE;

    .line 470788
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v0, v0, LX/2q6;->J:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_0

    .line 470789
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v1, p0, LX/2qF;->a:LX/2q6;

    invoke-virtual {v1}, LX/2q6;->b()I

    move-result v1

    iput v1, v0, LX/2q6;->B:I

    .line 470790
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    const-string v1, "ResetLastStartPos: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/2qF;->a:LX/2q6;

    iget v3, v3, LX/2q6;->B:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, LX/2q6;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470791
    :cond_0
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    invoke-virtual {v0}, LX/2q6;->x()V

    .line 470792
    :cond_1
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v0, v0, LX/2q6;->T:LX/2qG;

    if-eqz v0, :cond_2

    .line 470793
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v0, v0, LX/2q6;->T:LX/2qG;

    invoke-interface {v0}, LX/2qG;->a()V

    .line 470794
    :cond_2
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 470777
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    invoke-virtual {v0}, LX/2q6;->y()V

    .line 470778
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v0, v0, LX/2q6;->T:LX/2qG;

    if-eqz v0, :cond_0

    .line 470779
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v0, v0, LX/2q6;->T:LX/2qG;

    invoke-interface {v0, p1, p2}, LX/2qG;->a(II)V

    .line 470780
    :cond_0
    return-void
.end method

.method public final a(LX/7KG;)V
    .locals 3

    .prologue
    .line 470770
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    const-string v1, "VideoSurfaceTarget::onSurfaceUnavailable"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/2q6;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470771
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    invoke-virtual {v0}, LX/2q6;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470772
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, LX/2q6;->c(LX/04g;)V

    .line 470773
    :cond_0
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    invoke-virtual {v0, p1}, LX/2q6;->a(LX/7KG;)V

    .line 470774
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v0, v0, LX/2q6;->T:LX/2qG;

    if-eqz v0, :cond_1

    .line 470775
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v0, v0, LX/2q6;->T:LX/2qG;

    invoke-interface {v0, p1}, LX/2qG;->a(LX/7KG;)V

    .line 470776
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 3

    .prologue
    .line 470764
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    const-string v1, "VideoSurfaceTarget::onSurfaceAvailable"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/2q6;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470765
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    invoke-virtual {v0, p1}, LX/2q6;->b(Landroid/view/Surface;)V

    .line 470766
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    sget-object v1, LX/2qE;->STATE_CREATED:LX/2qE;

    iput-object v1, v0, LX/2q6;->V:LX/2qE;

    .line 470767
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v0, v0, LX/2q6;->T:LX/2qG;

    if-eqz v0, :cond_0

    .line 470768
    iget-object v0, p0, LX/2qF;->a:LX/2q6;

    iget-object v0, v0, LX/2q6;->T:LX/2qG;

    invoke-interface {v0, p1}, LX/2qG;->a(Landroid/view/Surface;)V

    .line 470769
    :cond_0
    return-void
.end method
