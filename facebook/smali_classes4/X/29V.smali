.class public abstract LX/29V;
.super LX/29W;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/29W",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public limit:I

.field public offset:I

.field public final omitEmptyStrings:Z

.field public final toSplit:Ljava/lang/CharSequence;

.field public final trimmer:LX/1IA;


# direct methods
.method public constructor <init>(LX/2Cb;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 376296
    invoke-direct {p0}, LX/29W;-><init>()V

    .line 376297
    const/4 v0, 0x0

    iput v0, p0, LX/29V;->offset:I

    .line 376298
    iget-object v0, p1, LX/2Cb;->trimmer:LX/1IA;

    iput-object v0, p0, LX/29V;->trimmer:LX/1IA;

    .line 376299
    iget-boolean v0, p1, LX/2Cb;->omitEmptyStrings:Z

    iput-boolean v0, p0, LX/29V;->omitEmptyStrings:Z

    .line 376300
    iget v0, p1, LX/2Cb;->limit:I

    iput v0, p0, LX/29V;->limit:I

    .line 376301
    iput-object p2, p0, LX/29V;->toSplit:Ljava/lang/CharSequence;

    .line 376302
    return-void
.end method


# virtual methods
.method public bridge synthetic computeNext()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 376303
    const/4 v5, -0x1

    .line 376304
    iget v0, p0, LX/29V;->offset:I

    .line 376305
    :cond_0
    :goto_0
    iget v1, p0, LX/29V;->offset:I

    if-eq v1, v5, :cond_6

    .line 376306
    iget v1, p0, LX/29V;->offset:I

    invoke-virtual {p0, v1}, LX/29V;->separatorStart(I)I

    move-result v1

    .line 376307
    if-ne v1, v5, :cond_1

    .line 376308
    iget-object v1, p0, LX/29V;->toSplit:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 376309
    iput v5, p0, LX/29V;->offset:I

    .line 376310
    :goto_1
    iget v2, p0, LX/29V;->offset:I

    if-ne v2, v0, :cond_8

    .line 376311
    iget v1, p0, LX/29V;->offset:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/29V;->offset:I

    .line 376312
    iget v1, p0, LX/29V;->offset:I

    iget-object v2, p0, LX/29V;->toSplit:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 376313
    iput v5, p0, LX/29V;->offset:I

    goto :goto_0

    .line 376314
    :cond_1
    invoke-virtual {p0, v1}, LX/29V;->separatorEnd(I)I

    move-result v2

    iput v2, p0, LX/29V;->offset:I

    goto :goto_1

    .line 376315
    :goto_2
    if-ge v2, v1, :cond_7

    iget-object v0, p0, LX/29V;->trimmer:LX/1IA;

    iget-object v3, p0, LX/29V;->toSplit:Ljava/lang/CharSequence;

    invoke-interface {v3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, LX/1IA;->matches(C)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 376316
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 376317
    :goto_3
    if-le v0, v2, :cond_2

    iget-object v1, p0, LX/29V;->trimmer:LX/1IA;

    iget-object v3, p0, LX/29V;->toSplit:Ljava/lang/CharSequence;

    add-int/lit8 v4, v0, -0x1

    invoke-interface {v3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, LX/1IA;->matches(C)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 376318
    add-int/lit8 v1, v0, -0x1

    move v0, v1

    goto :goto_3

    .line 376319
    :cond_2
    iget-boolean v1, p0, LX/29V;->omitEmptyStrings:Z

    if-eqz v1, :cond_3

    if-ne v2, v0, :cond_3

    .line 376320
    iget v0, p0, LX/29V;->offset:I

    goto :goto_0

    .line 376321
    :cond_3
    iget v1, p0, LX/29V;->limit:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_4

    .line 376322
    iget-object v0, p0, LX/29V;->toSplit:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 376323
    iput v5, p0, LX/29V;->offset:I

    .line 376324
    :goto_4
    if-le v0, v2, :cond_5

    iget-object v1, p0, LX/29V;->trimmer:LX/1IA;

    iget-object v3, p0, LX/29V;->toSplit:Ljava/lang/CharSequence;

    add-int/lit8 v4, v0, -0x1

    invoke-interface {v3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, LX/1IA;->matches(C)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 376325
    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    .line 376326
    :cond_4
    iget v1, p0, LX/29V;->limit:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/29V;->limit:I

    .line 376327
    :cond_5
    iget-object v1, p0, LX/29V;->toSplit:Ljava/lang/CharSequence;

    invoke-interface {v1, v2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 376328
    :goto_5
    move-object v0, v0

    .line 376329
    return-object v0

    :cond_6
    invoke-virtual {p0}, LX/29W;->endOfData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_3

    :cond_8
    move v2, v0

    goto :goto_2
.end method

.method public abstract separatorEnd(I)I
.end method

.method public abstract separatorStart(I)I
.end method
