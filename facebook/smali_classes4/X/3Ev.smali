.class public final LX/3Ev;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Dw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/3Ev;


# instance fields
.field private final b:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 538178
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "input_indicator"

    sget v2, LX/3Dx;->J:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "output_indicator"

    sget v2, LX/3Dx;->K:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "_v"

    sget v2, LX/3Dx;->I:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/3Ev;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 538158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538159
    iput-object p1, p0, LX/3Ev;->b:LX/0ad;

    .line 538160
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ev;
    .locals 4

    .prologue
    .line 538165
    sget-object v0, LX/3Ev;->c:LX/3Ev;

    if-nez v0, :cond_1

    .line 538166
    const-class v1, LX/3Ev;

    monitor-enter v1

    .line 538167
    :try_start_0
    sget-object v0, LX/3Ev;->c:LX/3Ev;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 538168
    if-eqz v2, :cond_0

    .line 538169
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 538170
    new-instance p0, LX/3Ev;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/3Ev;-><init>(LX/0ad;)V

    .line 538171
    move-object v0, p0

    .line 538172
    sput-object v0, LX/3Ev;->c:LX/3Ev;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 538173
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 538174
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 538175
    :cond_1
    sget-object v0, LX/3Ev;->c:LX/3Ev;

    return-object v0

    .line 538176
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 538177
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 538179
    sget-object v0, LX/3Ev;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 538180
    if-nez v0, :cond_0

    .line 538181
    const/4 v0, 0x0

    .line 538182
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/3Ev;->b:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->On:LX/0c1;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v2, v3, v0, p2}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 538164
    const-string v0, "rtc_audio_level_indicator_android"

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 538163
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 538161
    iget-object v0, p0, LX/3Ev;->b:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget v2, LX/3Dx;->aH:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(LX/0c0;I)V

    .line 538162
    return-void
.end method
