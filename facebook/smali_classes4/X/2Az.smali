.class public LX/2Az;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0m2;

.field public final b:LX/0lS;

.field public final c:LX/0nr;

.field public final d:LX/0lU;

.field public e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(LX/0m2;LX/0lS;)V
    .locals 1

    .prologue
    .line 378652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378653
    iput-object p1, p0, LX/2Az;->a:LX/0m2;

    .line 378654
    iput-object p2, p0, LX/2Az;->b:LX/0lS;

    .line 378655
    invoke-virtual {p1}, LX/0m2;->b()LX/0nr;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0lS;->a(LX/0nr;)LX/0nr;

    move-result-object v0

    iput-object v0, p0, LX/2Az;->c:LX/0nr;

    .line 378656
    iget-object v0, p0, LX/2Az;->a:LX/0m2;

    invoke-virtual {v0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    iput-object v0, p0, LX/2Az;->d:LX/0lU;

    .line 378657
    return-void
.end method

.method private a(LX/0lO;ZLX/0lJ;)LX/0lJ;
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 378635
    iget-object v0, p0, LX/2Az;->d:LX/0lU;

    invoke-virtual {v0, p1}, LX/0lU;->i(LX/0lO;)Ljava/lang/Class;

    move-result-object v0

    .line 378636
    if-eqz v0, :cond_6

    .line 378637
    iget-object v2, p3, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v2

    .line 378638
    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 378639
    invoke-virtual {p3, v0}, LX/0lJ;->c(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    :goto_0
    move p2, v1

    .line 378640
    :goto_1
    iget-object v2, p0, LX/2Az;->a:LX/0m2;

    invoke-static {v2, p1, v0}, LX/0nR;->b(LX/0m2;LX/0lO;LX/0lJ;)LX/0lJ;

    move-result-object v2

    .line 378641
    if-eq v2, v0, :cond_5

    move-object v0, v2

    move v2, v1

    .line 378642
    :goto_2
    if-nez v2, :cond_4

    .line 378643
    iget-object v3, p0, LX/2Az;->d:LX/0lU;

    invoke-virtual {v3, p1}, LX/0lU;->l(LX/0lO;)LX/1Xv;

    move-result-object v3

    .line 378644
    if-eqz v3, :cond_4

    .line 378645
    sget-object v2, LX/1Xv;->STATIC:LX/1Xv;

    if-ne v3, v2, :cond_2

    .line 378646
    :goto_3
    if-eqz v1, :cond_3

    :goto_4
    return-object v0

    .line 378647
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 378648
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Illegal concrete-type annotation for method \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\': class "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " not a super-type of (declared) class "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 378649
    :cond_1
    iget-object v2, p0, LX/2Az;->a:LX/0m2;

    invoke-virtual {v2, p3, v0}, LX/0m4;->a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    goto :goto_0

    .line 378650
    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    .line 378651
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    :cond_4
    move v1, v2

    goto :goto_3

    :cond_5
    move v2, p2

    goto :goto_2

    :cond_6
    move-object v0, p3

    goto :goto_1
.end method

.method private static a(Ljava/lang/Exception;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 378624
    move-object v0, p0

    .line 378625
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 378626
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    .line 378627
    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 378628
    :cond_1
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 378629
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to get property \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' of default "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " instance"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Ljava/lang/String;LX/2An;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 378630
    invoke-direct {p0}, LX/2Az;->b()Ljava/lang/Object;

    move-result-object v0

    .line 378631
    :try_start_0
    invoke-virtual {p2, v0}, LX/2An;->b(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 378632
    :goto_0
    return-object v0

    .line 378633
    :catch_0
    move-exception v1

    .line 378634
    invoke-static {v1, p1, v0}, LX/2Az;->a(Ljava/lang/Exception;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 378588
    iget-object v0, p0, LX/2Az;->e:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 378589
    iget-object v0, p0, LX/2Az;->b:LX/0lS;

    iget-object v1, p0, LX/2Az;->a:LX/0m2;

    invoke-virtual {v1}, LX/0m4;->h()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/0lS;->a(Z)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/2Az;->e:Ljava/lang/Object;

    .line 378590
    iget-object v0, p0, LX/2Az;->e:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 378591
    iget-object v0, p0, LX/2Az;->b:LX/0lS;

    invoke-virtual {v0}, LX/0lS;->c()LX/0lN;

    move-result-object v0

    .line 378592
    iget-object v1, v0, LX/0lN;->a:Ljava/lang/Class;

    move-object v0, v1

    .line 378593
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " has no default constructor; can not instantiate default bean value to support \'properties=JsonSerialize.Inclusion.NON_DEFAULT\' annotation"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 378594
    :cond_0
    iget-object v0, p0, LX/2Az;->e:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public final a()LX/0lQ;
    .locals 1

    .prologue
    .line 378595
    iget-object v0, p0, LX/2Az;->b:LX/0lS;

    invoke-virtual {v0}, LX/0lS;->g()LX/0lQ;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2Aq;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;LX/4qz;LX/2An;Z)LX/2Ax;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Aq;",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;",
            "LX/4qz;",
            "LX/4qz;",
            "LX/2An;",
            "Z)",
            "LX/2Ax;"
        }
    .end annotation

    .prologue
    .line 378596
    move-object/from16 v0, p6

    move/from16 v1, p7

    invoke-direct {p0, v0, v1, p2}, LX/2Az;->a(LX/0lO;ZLX/0lJ;)LX/0lJ;

    move-result-object v2

    .line 378597
    if-eqz p5, :cond_5

    .line 378598
    if-nez v2, :cond_0

    move-object v2, p2

    .line 378599
    :cond_0
    invoke-virtual {v2}, LX/0lJ;->r()LX/0lJ;

    move-result-object v3

    .line 378600
    if-nez v3, :cond_1

    .line 378601
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Problem trying to create BeanPropertyWriter for property \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' (of type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LX/2Az;->b:LX/0lS;

    invoke-virtual {v5}, LX/0lS;->a()LX/0lJ;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "); serialization type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " has no content"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 378602
    :cond_1
    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, LX/0lJ;->b(Ljava/lang/Object;)LX/0lJ;

    move-result-object v9

    .line 378603
    :goto_0
    const/4 v11, 0x0

    .line 378604
    const/4 v2, 0x0

    .line 378605
    iget-object v3, p0, LX/2Az;->d:LX/0lU;

    iget-object v4, p0, LX/2Az;->c:LX/0nr;

    move-object/from16 v0, p6

    invoke-virtual {v3, v0, v4}, LX/0lU;->a(LX/0lO;LX/0nr;)LX/0nr;

    move-result-object v3

    .line 378606
    if-eqz v3, :cond_2

    .line 378607
    sget-object v4, LX/2B5;->a:[I

    invoke-virtual {v3}, LX/0nr;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    :cond_2
    move v10, v2

    .line 378608
    :goto_1
    new-instance v2, LX/2Ax;

    iget-object v3, p0, LX/2Az;->b:LX/0lS;

    invoke-virtual {v3}, LX/0lS;->g()LX/0lQ;

    move-result-object v5

    move-object v3, p1

    move-object/from16 v4, p6

    move-object v6, p2

    move-object v7, p3

    move-object/from16 v8, p4

    invoke-direct/range {v2 .. v11}, LX/2Ax;-><init>(LX/2Aq;LX/2An;LX/0lQ;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;LX/0lJ;ZLjava/lang/Object;)V

    .line 378609
    iget-object v3, p0, LX/2Az;->d:LX/0lU;

    move-object/from16 v0, p6

    invoke-virtual {v3, v0}, LX/0lU;->b(LX/2An;)LX/4ro;

    move-result-object v3

    .line 378610
    if-eqz v3, :cond_3

    .line 378611
    invoke-virtual {v2, v3}, LX/2Ax;->b(LX/4ro;)LX/2Ax;

    move-result-object v2

    .line 378612
    :cond_3
    return-object v2

    .line 378613
    :pswitch_0
    invoke-virtual {p1}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-direct {p0, v3, v0}, LX/2Az;->a(Ljava/lang/String;LX/2An;)Ljava/lang/Object;

    move-result-object v11

    .line 378614
    if-nez v11, :cond_4

    .line 378615
    const/4 v2, 0x1

    move v10, v2

    goto :goto_1

    .line 378616
    :cond_4
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->isArray()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 378617
    invoke-static {v11}, LX/0nj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    move v10, v2

    goto :goto_1

    .line 378618
    :pswitch_1
    const/4 v2, 0x1

    .line 378619
    sget-object v11, LX/2Ax;->a:Ljava/lang/Object;

    move v10, v2

    .line 378620
    goto :goto_1

    .line 378621
    :pswitch_2
    const/4 v2, 0x1

    .line 378622
    :pswitch_3
    invoke-virtual {p2}, LX/0lJ;->l()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/2Az;->a:LX/0m2;

    sget-object v4, LX/0mt;->WRITE_EMPTY_JSON_ARRAYS:LX/0mt;

    invoke-virtual {v3, v4}, LX/0m2;->c(LX/0mt;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 378623
    sget-object v11, LX/2Ax;->a:Ljava/lang/Object;

    move v10, v2

    goto :goto_1

    :cond_5
    move-object v9, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
