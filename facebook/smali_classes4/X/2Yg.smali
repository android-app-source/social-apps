.class public LX/2Yg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2Yg;


# instance fields
.field public final b:LX/0Zb;

.field public final c:LX/0SG;

.field private final d:LX/0ie;

.field public final e:LX/0ii;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 421201
    const-class v0, LX/2Yg;

    sput-object v0, LX/2Yg;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0SG;LX/0ie;LX/0ii;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 421202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421203
    iput-object p1, p0, LX/2Yg;->b:LX/0Zb;

    .line 421204
    iput-object p2, p0, LX/2Yg;->c:LX/0SG;

    .line 421205
    iput-object p3, p0, LX/2Yg;->d:LX/0ie;

    .line 421206
    iput-object p4, p0, LX/2Yg;->e:LX/0ii;

    .line 421207
    return-void
.end method

.method public static a(LX/0QB;)LX/2Yg;
    .locals 7

    .prologue
    .line 421208
    sget-object v0, LX/2Yg;->f:LX/2Yg;

    if-nez v0, :cond_1

    .line 421209
    const-class v1, LX/2Yg;

    monitor-enter v1

    .line 421210
    :try_start_0
    sget-object v0, LX/2Yg;->f:LX/2Yg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 421211
    if-eqz v2, :cond_0

    .line 421212
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 421213
    new-instance p0, LX/2Yg;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0ie;->a(LX/0QB;)LX/0ie;

    move-result-object v5

    check-cast v5, LX/0ie;

    invoke-static {v0}, LX/0ii;->a(LX/0QB;)LX/0ii;

    move-result-object v6

    check-cast v6, LX/0ii;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2Yg;-><init>(LX/0Zb;LX/0SG;LX/0ie;LX/0ii;)V

    .line 421214
    move-object v0, p0

    .line 421215
    sput-object v0, LX/2Yg;->f:LX/2Yg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 421216
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 421217
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 421218
    :cond_1
    sget-object v0, LX/2Yg;->f:LX/2Yg;

    return-object v0

    .line 421219
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 421220
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 421221
    if-nez p2, :cond_0

    .line 421222
    :goto_0
    return-void

    .line 421223
    :cond_0
    iget-object v0, p0, LX/2Yg;->e:LX/0ii;

    invoke-virtual {v0, p2}, LX/0ii;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 421224
    iget-object v0, p0, LX/2Yg;->d:LX/0ie;

    sget-object v1, LX/21C;->JEWEL:LX/21C;

    invoke-virtual {v0, v1, p2}, LX/0ie;->a(LX/21C;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 421225
    iget-object v0, p0, LX/2Yg;->b:LX/0Zb;

    new-instance v1, Lcom/facebook/notifications/logging/NotificationsLogger$JewelNotificationEvent;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/notifications/logging/NotificationsLogger$JewelNotificationEvent;-><init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    const-string v2, "reaction_unit_interaction"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "reaction_destination_entity_id"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 2
    .param p1    # Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 421226
    sget-object v0, LX/3B2;->graph_notification_click:LX/3B2;

    invoke-virtual {p0, v0, p1, v1, v1}, LX/2Yg;->a(LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 421227
    return-void
.end method

.method public final a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V
    .locals 2
    .param p1    # Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 421228
    if-nez p1, :cond_0

    .line 421229
    :goto_0
    return-void

    .line 421230
    :cond_0
    sget-object v0, LX/3B2;->CLICK_FROM_TRAY:LX/3B2;

    if-ne p2, v0, :cond_1

    .line 421231
    iget-object v0, p0, LX/2Yg;->e:LX/0ii;

    invoke-virtual {v0, p1}, LX/0ii;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 421232
    iget-object v0, p0, LX/2Yg;->d:LX/0ie;

    sget-object v1, LX/21C;->PUSH:LX/21C;

    invoke-virtual {v0, v1, p1}, LX/0ie;->a(LX/21C;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 421233
    :cond_1
    iget-object v0, p0, LX/2Yg;->b:LX/0Zb;

    new-instance v1, Lcom/facebook/notifications/logging/NotificationsLogger$PushNotificationEvent;

    invoke-direct {v1, p0, p2, p1}, Lcom/facebook/notifications/logging/NotificationsLogger$PushNotificationEvent;-><init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;)V
    .locals 10
    .param p1    # Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 421234
    if-nez p1, :cond_1

    .line 421235
    :cond_0
    :goto_0
    return-void

    .line 421236
    :cond_1
    iget-object v0, p0, LX/2Yg;->e:LX/0ii;

    invoke-virtual {v0, p1}, LX/0ii;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 421237
    iget-object v2, p0, LX/2Yg;->b:LX/0Zb;

    const/4 v3, 0x0

    invoke-interface {v2, p2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 421238
    if-eqz p1, :cond_2

    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-nez v3, :cond_3

    .line 421239
    :cond_2
    :goto_1
    move-object v0, v2

    .line 421240
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 421241
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0

    .line 421242
    :cond_3
    const-string v3, "ct"

    invoke-virtual {p1}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 421243
    const-string v3, "ci"

    .line 421244
    iget-object v4, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->c:Ljava/lang/String;

    move-object v4, v4

    .line 421245
    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 421246
    iget-object v3, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->s:Ljava/lang/String;

    move-object v3, v3

    .line 421247
    if-eqz v3, :cond_2

    .line 421248
    const-string v3, "push_source"

    .line 421249
    iget-object v4, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->r:Ljava/lang/String;

    move-object v4, v4

    .line 421250
    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 421251
    const-string v3, "push_id"

    .line 421252
    iget-object v4, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->s:Ljava/lang/String;

    move-object v4, v4

    .line 421253
    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 421254
    iget-object v3, p0, LX/2Yg;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 421255
    iget-wide v8, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->t:J

    move-wide v6, v8

    .line 421256
    sub-long/2addr v4, v6

    .line 421257
    const-string v3, "time_to_tray_ms"

    invoke-virtual {v2, v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    goto :goto_1
.end method
