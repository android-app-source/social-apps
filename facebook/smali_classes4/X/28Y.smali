.class public final LX/28Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0cM;


# instance fields
.field public final synthetic a:LX/0Uo;


# direct methods
.method public constructor <init>(LX/0Uo;)V
    .locals 0

    .prologue
    .line 374236
    iput-object p1, p0, LX/28Y;->a:LX/0Uo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0cK;Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 374237
    iget-object v2, p0, LX/28Y;->a:LX/0Uo;

    monitor-enter v2

    .line 374238
    :try_start_0
    iget-object v0, p0, LX/28Y;->a:LX/0Uo;

    iget-object v0, v0, LX/0Uo;->X:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

    .line 374239
    if-nez v0, :cond_0

    .line 374240
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "The peer "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, LX/0cK;->c:LX/00G;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " wasn\'t registered"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374241
    monitor-exit v2

    .line 374242
    :goto_0
    return-void

    .line 374243
    :cond_0
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 374244
    const-class v3, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 374245
    const-string v3, "app_state_info"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

    .line 374246
    iget-object v3, p0, LX/28Y;->a:LX/0Uo;

    invoke-static {v3, v1, v0}, LX/0Uo;->a$redex0(LX/0Uo;Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;)V

    .line 374247
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
