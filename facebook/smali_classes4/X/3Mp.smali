.class public LX/3Mp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field public a:LX/3LP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2RQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 556328
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3Mp;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 556325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 556326
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3Mp;->c:Ljava/util/Map;

    .line 556327
    return-void
.end method

.method public static a(LX/0QB;)LX/3Mp;
    .locals 8

    .prologue
    .line 556329
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 556330
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 556331
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 556332
    if-nez v1, :cond_0

    .line 556333
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 556334
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 556335
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 556336
    sget-object v1, LX/3Mp;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 556337
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 556338
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 556339
    :cond_1
    if-nez v1, :cond_4

    .line 556340
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 556341
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 556342
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 556343
    new-instance p0, LX/3Mp;

    invoke-direct {p0}, LX/3Mp;-><init>()V

    .line 556344
    invoke-static {v0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v1

    check-cast v1, LX/3LP;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v7

    check-cast v7, LX/2RQ;

    .line 556345
    iput-object v1, p0, LX/3Mp;->a:LX/3LP;

    iput-object v7, p0, LX/3Mp;->b:LX/2RQ;

    .line 556346
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 556347
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 556348
    if-nez v1, :cond_2

    .line 556349
    sget-object v0, LX/3Mp;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Mp;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 556350
    :goto_1
    if-eqz v0, :cond_3

    .line 556351
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 556352
    :goto_3
    check-cast v0, LX/3Mp;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 556353
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 556354
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 556355
    :catchall_1
    move-exception v0

    .line 556356
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 556357
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 556358
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 556359
    :cond_2
    :try_start_8
    sget-object v0, LX/3Mp;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Mp;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static c(LX/3Mp;Lcom/facebook/user/model/User;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/user/model/User;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 556294
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 556295
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->p()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserPhoneNumber;

    .line 556296
    iget-object v1, p0, LX/3Mp;->b:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    .line 556297
    iget-object v6, v0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    move-object v0, v6

    .line 556298
    iput-object v0, v1, LX/2RR;->e:Ljava/lang/String;

    .line 556299
    move-object v0, v1

    .line 556300
    invoke-virtual {v0}, LX/2RR;->i()LX/2RR;

    move-result-object v0

    .line 556301
    iget-object v1, p0, LX/3Mp;->a:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v6

    const/4 v1, 0x0

    .line 556302
    :goto_1
    :try_start_0
    invoke-interface {v6}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556303
    invoke-interface {v6}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 556304
    iget-object v7, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v7

    .line 556305
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_1

    .line 556306
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 556307
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_2
    if-eqz v6, :cond_0

    if-eqz v1, :cond_3

    :try_start_2
    invoke-interface {v6}, LX/3On;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_3
    throw v0

    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, LX/3On;->close()V

    .line 556308
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 556309
    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_3
    invoke-interface {v6}, LX/3On;->close()V

    goto :goto_3

    .line 556310
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 556311
    :catchall_1
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/User;)Z
    .locals 3

    .prologue
    .line 556314
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 556315
    iget-object v0, p0, LX/3Mp;->c:Ljava/util/Map;

    .line 556316
    iget-object v1, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 556317
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 556318
    if-nez v0, :cond_0

    .line 556319
    invoke-static {p0, p1}, LX/3Mp;->c(LX/3Mp;Lcom/facebook/user/model/User;)LX/0Px;

    move-result-object v0

    .line 556320
    iget-object v1, p0, LX/3Mp;->c:Ljava/util/Map;

    .line 556321
    iget-object v2, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 556322
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556323
    :cond_0
    move-object v0, v0

    .line 556324
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 556312
    iget-object v0, p0, LX/3Mp;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 556313
    return-void
.end method
