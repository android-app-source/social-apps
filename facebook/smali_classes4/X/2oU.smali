.class public LX/2oU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2oU;


# instance fields
.field private final a:LX/0W3;

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 466404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 466405
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2oU;->b:Z

    .line 466406
    iput-object p1, p0, LX/2oU;->a:LX/0W3;

    .line 466407
    return-void
.end method

.method public static a(LX/0QB;)LX/2oU;
    .locals 4

    .prologue
    .line 466408
    sget-object v0, LX/2oU;->d:LX/2oU;

    if-nez v0, :cond_1

    .line 466409
    const-class v1, LX/2oU;

    monitor-enter v1

    .line 466410
    :try_start_0
    sget-object v0, LX/2oU;->d:LX/2oU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 466411
    if-eqz v2, :cond_0

    .line 466412
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 466413
    new-instance p0, LX/2oU;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/2oU;-><init>(LX/0W3;)V

    .line 466414
    move-object v0, p0

    .line 466415
    sput-object v0, LX/2oU;->d:LX/2oU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 466416
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 466417
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 466418
    :cond_1
    sget-object v0, LX/2oU;->d:LX/2oU;

    return-object v0

    .line 466419
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 466420
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 4

    .prologue
    .line 466421
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2oU;->b:Z

    if-nez v0, :cond_0

    .line 466422
    iget-object v0, p0, LX/2oU;->a:LX/0W3;

    sget-wide v2, LX/0X5;->cf:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    .line 466423
    if-eqz v0, :cond_1

    .line 466424
    iget-object v0, p0, LX/2oU;->a:LX/0W3;

    sget-wide v2, LX/0X5;->cg:J

    const/16 v1, 0x32

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/2oU;->c:I

    .line 466425
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2oU;->b:Z

    .line 466426
    :cond_0
    iget v0, p0, LX/2oU;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 466427
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, LX/2oU;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 466428
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 466429
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/2oU;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466430
    monitor-exit p0

    return-void

    .line 466431
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
