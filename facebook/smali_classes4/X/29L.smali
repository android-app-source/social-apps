.class public LX/29L;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/29L;


# instance fields
.field public final a:LX/69l;

.field private final b:LX/0Zb;

.field public final c:LX/18A;

.field private final d:LX/1Mz;

.field private final e:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation
.end field

.field private final f:LX/0SI;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/4VZ",
            "<",
            "Lcom/facebook/privacy/protocol/FeedPrivacyInvalidationQueryModels$FBFeedPrivacyInvalidationQueryModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/69l;LX/0Zb;LX/18A;LX/1Mz;LX/0TD;LX/0SI;)V
    .locals 1
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 376203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376204
    iput-object p1, p0, LX/29L;->a:LX/69l;

    .line 376205
    iput-object p2, p0, LX/29L;->b:LX/0Zb;

    .line 376206
    iput-object p3, p0, LX/29L;->c:LX/18A;

    .line 376207
    iput-object p4, p0, LX/29L;->d:LX/1Mz;

    .line 376208
    iput-object p5, p0, LX/29L;->e:LX/0TD;

    .line 376209
    iput-object p6, p0, LX/29L;->f:LX/0SI;

    .line 376210
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/29L;->g:Ljava/util/Map;

    .line 376211
    return-void
.end method

.method public static a(LX/0QB;)LX/29L;
    .locals 10

    .prologue
    .line 376176
    sget-object v0, LX/29L;->h:LX/29L;

    if-nez v0, :cond_1

    .line 376177
    const-class v1, LX/29L;

    monitor-enter v1

    .line 376178
    :try_start_0
    sget-object v0, LX/29L;->h:LX/29L;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 376179
    if-eqz v2, :cond_0

    .line 376180
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 376181
    new-instance v3, LX/29L;

    invoke-static {v0}, LX/69l;->a(LX/0QB;)LX/69l;

    move-result-object v4

    check-cast v4, LX/69l;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/18A;->b(LX/0QB;)LX/18A;

    move-result-object v6

    check-cast v6, LX/18A;

    invoke-static {v0}, LX/1Mz;->a(LX/0QB;)LX/1Mz;

    move-result-object v7

    check-cast v7, LX/1Mz;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v9

    check-cast v9, LX/0SI;

    invoke-direct/range {v3 .. v9}, LX/29L;-><init>(LX/69l;LX/0Zb;LX/18A;LX/1Mz;LX/0TD;LX/0SI;)V

    .line 376182
    move-object v0, v3

    .line 376183
    sput-object v0, LX/29L;->h:LX/29L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376184
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 376185
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 376186
    :cond_1
    sget-object v0, LX/29L;->h:LX/29L;

    return-object v0

    .line 376187
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 376188
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized a(LX/29L;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 376189
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/29L;->g:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 376190
    :goto_0
    monitor-exit p0

    return-void

    .line 376191
    :cond_0
    :try_start_1
    new-instance v0, LX/5n2;

    invoke-direct {v0}, LX/5n2;-><init>()V

    move-object v0, v0

    .line 376192
    const-string v1, "node_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 376193
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 376194
    iget-object v1, p0, LX/29L;->f:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 376195
    iput-object v1, v0, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 376196
    iget-object v1, p0, LX/29L;->d:LX/1Mz;

    const-string v2, "fb4a_feed_privacy"

    iget-object v3, p0, LX/29L;->e:LX/0TD;

    new-instance v4, LX/69m;

    invoke-direct {v4, p0, p1, p2}, LX/69m;-><init>(LX/29L;Ljava/lang/String;Ljava/lang/String;)V

    .line 376197
    new-instance v5, LX/4VZ;

    iget-object v10, v1, LX/1Mz;->l:LX/1N7;

    move-object v6, v0

    move-object v7, v2

    move-object v8, v1

    move-object v9, v3

    move-object v11, v4

    invoke-direct/range {v5 .. v11}, LX/4VZ;-><init>(LX/0zO;Ljava/lang/String;LX/1Mz;Ljava/util/concurrent/Executor;LX/1N7;LX/0TF;)V

    move-object v0, v5

    .line 376198
    invoke-virtual {v0}, LX/4VZ;->a()V

    .line 376199
    iget-object v1, p0, LX/29L;->g:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 376200
    :catch_0
    move-exception v0

    .line 376201
    :try_start_2
    const-string v1, "RealtimePrivacyHandler"

    const-string v2, "FBFeedPrivacyInvalidationQuery query failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 376202
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(LX/29L;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 376212
    iget-object v0, p0, LX/29L;->b:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 376213
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 376214
    const-string v1, "node_id"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 376215
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 376158
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/29L;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4VZ;

    .line 376159
    if-eqz v0, :cond_0

    .line 376160
    invoke-virtual {v0}, LX/4VZ;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376161
    :goto_0
    monitor-exit p0

    return-void

    .line 376162
    :cond_0
    :try_start_1
    const-string v0, "RealtimePrivacyHandler"

    const-string v1, "Tried unsubscribing from non-existing dedup key: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 376163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 376164
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/29L;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 376165
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 376166
    invoke-static {v3, v2}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    invoke-virtual {v0}, LX/0Ro;->a()LX/0Rf;

    move-result-object v4

    .line 376167
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 376168
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 376169
    invoke-static {p0, v1, v0}, LX/29L;->a(LX/29L;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 376170
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 376171
    :cond_0
    :try_start_1
    invoke-static {v2, v3}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    invoke-virtual {v0}, LX/0Ro;->a()LX/0Rf;

    move-result-object v1

    .line 376172
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 376173
    invoke-virtual {p0, v0}, LX/29L;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 376174
    :cond_1
    invoke-interface {v4}, Ljava/util/Set;->size()I

    invoke-interface {v1}, Ljava/util/Set;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376175
    monitor-exit p0

    return-void
.end method
