.class public final LX/2Cd;
.super LX/0rP;
.source ""


# instance fields
.field public final a:F


# direct methods
.method private constructor <init>(F)V
    .locals 0

    .prologue
    .line 383129
    invoke-direct {p0}, LX/0rP;-><init>()V

    iput p1, p0, LX/2Cd;->a:F

    return-void
.end method

.method public static a(F)LX/2Cd;
    .locals 1

    .prologue
    .line 383128
    new-instance v0, LX/2Cd;

    invoke-direct {v0, p0}, LX/2Cd;-><init>(F)V

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 383127
    invoke-virtual {p0}, LX/0lF;->z()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 383126
    iget v0, p0, LX/2Cd;->a:F

    float-to-double v0, v0

    invoke-static {v0, v1}, LX/13I;->a(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 383109
    sget-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    return-object v0
.end method

.method public final b()LX/16L;
    .locals 1

    .prologue
    .line 383125
    sget-object v0, LX/16L;->FLOAT:LX/16L;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 383118
    if-ne p1, p0, :cond_1

    .line 383119
    :cond_0
    :goto_0
    return v0

    .line 383120
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 383121
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 383122
    goto :goto_0

    .line 383123
    :cond_3
    check-cast p1, LX/2Cd;

    iget v2, p1, LX/2Cd;->a:F

    .line 383124
    iget v3, p0, LX/2Cd;->a:F

    invoke-static {v3, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 383117
    iget v0, p0, LX/2Cd;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    return v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 383115
    iget v0, p0, LX/2Cd;->a:F

    invoke-virtual {p1, v0}, LX/0nX;->a(F)V

    .line 383116
    return-void
.end method

.method public final v()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 383114
    iget v0, p0, LX/2Cd;->a:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 383113
    iget v0, p0, LX/2Cd;->a:F

    float-to-int v0, v0

    return v0
.end method

.method public final x()J
    .locals 2

    .prologue
    .line 383112
    iget v0, p0, LX/2Cd;->a:F

    float-to-long v0, v0

    return-wide v0
.end method

.method public final y()D
    .locals 2

    .prologue
    .line 383111
    iget v0, p0, LX/2Cd;->a:F

    float-to-double v0, v0

    return-wide v0
.end method

.method public final z()Ljava/math/BigDecimal;
    .locals 2

    .prologue
    .line 383110
    iget v0, p0, LX/2Cd;->a:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method
