.class public LX/36b;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C43;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/36b",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C43;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499502
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 499503
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/36b;->b:LX/0Zi;

    .line 499504
    iput-object p1, p0, LX/36b;->a:LX/0Ot;

    .line 499505
    return-void
.end method

.method public static a(LX/0QB;)LX/36b;
    .locals 4

    .prologue
    .line 499506
    const-class v1, LX/36b;

    monitor-enter v1

    .line 499507
    :try_start_0
    sget-object v0, LX/36b;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499508
    sput-object v2, LX/36b;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499509
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499510
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499511
    new-instance v3, LX/36b;

    const/16 p0, 0x1eee

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/36b;-><init>(LX/0Ot;)V

    .line 499512
    move-object v0, v3

    .line 499513
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499514
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/36b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499515
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499516
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 499517
    check-cast p2, LX/C41;

    .line 499518
    iget-object v0, p0, LX/36b;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C43;

    iget-object v1, p2, LX/C41;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C41;->b:LX/1Pf;

    .line 499519
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v3

    iget-object p0, v0, LX/C43;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p0}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object p0

    .line 499520
    iget-object v3, v0, LX/C43;->e:LX/1Vm;

    invoke-virtual {v3, p1}, LX/1Vm;->c(LX/1De;)LX/C2N;

    move-result-object v3

    iget-object p2, v0, LX/C43;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, p2}, LX/C2N;->a(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object p2

    .line 499521
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 499522
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {p2, v3}, LX/C2N;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/C2N;->a(LX/1Pp;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/C2N;->a(LX/1dc;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 499523
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 499524
    invoke-static {}, LX/1dS;->b()V

    .line 499525
    const/4 v0, 0x0

    return-object v0
.end method
