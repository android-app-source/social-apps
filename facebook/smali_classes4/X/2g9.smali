.class public LX/2g9;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/2gA;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ApP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 447966
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/2g9;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ApP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 447963
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 447964
    iput-object p1, p0, LX/2g9;->b:LX/0Ot;

    .line 447965
    return-void
.end method

.method public static a(LX/0QB;)LX/2g9;
    .locals 4

    .prologue
    .line 447928
    const-class v1, LX/2g9;

    monitor-enter v1

    .line 447929
    :try_start_0
    sget-object v0, LX/2g9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 447930
    sput-object v2, LX/2g9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 447931
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447932
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 447933
    new-instance v3, LX/2g9;

    const/16 p0, 0x21fd

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2g9;-><init>(LX/0Ot;)V

    .line 447934
    move-object v0, v3

    .line 447935
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 447936
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2g9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447937
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 447938
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 447949
    check-cast p2, LX/ApN;

    .line 447950
    iget-object v0, p0, LX/2g9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ApP;

    iget v2, p2, LX/ApN;->a:I

    iget-object v3, p2, LX/ApN;->b:Ljava/lang/CharSequence;

    iget-object v4, p2, LX/ApN;->c:Ljava/lang/CharSequence;

    iget v5, p2, LX/ApN;->d:I

    iget-object v6, p2, LX/ApN;->e:Landroid/util/SparseArray;

    move-object v1, p1

    const/4 p2, 0x5

    const/4 v9, 0x2

    const/4 p1, 0x0

    const/4 p0, 0x1

    .line 447951
    new-instance v7, LX/ApO;

    invoke-direct {v7, v1, v2}, LX/ApO;-><init>(LX/1De;I)V

    .line 447952
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, v9}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    iget v9, v7, LX/ApO;->f:I

    invoke-interface {v8, v9}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v8

    iget v9, v7, LX/ApO;->g:I

    invoke-interface {v8, v9}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v8

    const/4 v9, 0x4

    iget v10, v7, LX/ApO;->a:I

    invoke-interface {v8, v9, v10}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v8

    iget v9, v7, LX/ApO;->b:I

    invoke-interface {v8, p2, v9}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v8

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    move-object v4, v3

    :cond_0
    invoke-interface {v8, v4}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, v6}, LX/1Dh;->b(Landroid/util/SparseArray;)LX/1Dh;

    move-result-object v8

    .line 447953
    if-lez v5, :cond_1

    .line 447954
    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 447955
    invoke-static {v9}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 447956
    iget-object v10, v7, LX/ApO;->h:Landroid/content/res/ColorStateList;

    invoke-static {v9, v10}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 447957
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v10

    invoke-virtual {v10, v9}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v9

    invoke-virtual {v9}, LX/1n6;->b()LX/1dc;

    move-result-object v9

    .line 447958
    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v10

    invoke-virtual {v10, v9}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    iget v10, v7, LX/ApO;->e:I

    invoke-interface {v9, v10}, LX/1Di;->g(I)LX/1Di;

    move-result-object v9

    iget v10, v7, LX/ApO;->e:I

    invoke-interface {v9, v10}, LX/1Di;->o(I)LX/1Di;

    move-result-object v9

    invoke-interface {v9, p0}, LX/1Di;->a(Z)LX/1Di;

    move-result-object v9

    iget v10, v7, LX/ApO;->d:I

    invoke-interface {v9, p2, v10}, LX/1Di;->a(II)LX/1Di;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 447959
    :cond_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 447960
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v9

    iget-object v10, v7, LX/ApO;->h:Landroid/content/res/ColorStateList;

    invoke-virtual {v9, v10}, LX/1ne;->a(Landroid/content/res/ColorStateList;)LX/1ne;

    move-result-object v9

    iget v7, v7, LX/ApO;->c:I

    invoke-virtual {v9, v7}, LX/1ne;->p(I)LX/1ne;

    move-result-object v7

    iget-object v9, v0, LX/ApP;->a:LX/23P;

    const/4 v10, 0x0

    invoke-virtual {v9, v3, v10}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v7, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    sget-object v9, LX/ApP;->b:Landroid/graphics/Typeface;

    invoke-virtual {v7, v9}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v7

    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v7, v9}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, p1}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Di;->a(Z)LX/1Di;

    move-result-object v7

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-interface {v7, v9}, LX/1Di;->c(F)LX/1Di;

    move-result-object v7

    invoke-interface {v8, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 447961
    :cond_2
    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 447962
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 447947
    invoke-static {}, LX/1dS;->b()V

    .line 447948
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/2gA;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 447939
    new-instance v1, LX/ApN;

    invoke-direct {v1, p0}, LX/ApN;-><init>(LX/2g9;)V

    .line 447940
    sget-object v2, LX/2g9;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2gA;

    .line 447941
    if-nez v2, :cond_0

    .line 447942
    new-instance v2, LX/2gA;

    invoke-direct {v2}, LX/2gA;-><init>()V

    .line 447943
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/2gA;->a$redex0(LX/2gA;LX/1De;IILX/ApN;)V

    .line 447944
    move-object v1, v2

    .line 447945
    move-object v0, v1

    .line 447946
    return-object v0
.end method
