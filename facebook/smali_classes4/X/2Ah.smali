.class public LX/2Ah;
.super LX/0mx;
.source ""


# static fields
.field private static final f:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Class;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer;",
            ">;"
        }
    .end annotation
.end field

.field private static g:Z

.field private static h:Lcom/fasterxml/jackson/databind/JsonSerializer;

.field private static i:Lcom/fasterxml/jackson/databind/JsonSerializer;

.field private static j:Lcom/fasterxml/jackson/databind/JsonSerializer;


# instance fields
.field private mCollectionSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

.field private final mJsonLogger:LX/0ly;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 377515
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    .line 377516
    const/4 v0, 0x0

    sput-boolean v0, LX/2Ah;->g:Z

    .line 377517
    new-instance v0, Lcom/facebook/common/json/FbSerializerProvider$2;

    invoke-direct {v0}, Lcom/facebook/common/json/FbSerializerProvider$2;-><init>()V

    sput-object v0, LX/2Ah;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 377518
    new-instance v0, Lcom/facebook/common/json/FbSerializerProvider$3;

    invoke-direct {v0}, Lcom/facebook/common/json/FbSerializerProvider$3;-><init>()V

    sput-object v0, LX/2Ah;->i:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 377519
    new-instance v0, Lcom/facebook/common/json/FbSerializerProvider$4;

    invoke-direct {v0}, Lcom/facebook/common/json/FbSerializerProvider$4;-><init>()V

    sput-object v0, LX/2Ah;->j:Lcom/fasterxml/jackson/databind/JsonSerializer;

    return-void
.end method

.method public constructor <init>(LX/0my;LX/0m2;LX/0nS;LX/0ly;)V
    .locals 1

    .prologue
    .line 377525
    invoke-direct {p0, p1, p2, p3}, LX/0mx;-><init>(LX/0my;LX/0m2;LX/0nS;)V

    .line 377526
    new-instance v0, Lcom/facebook/common/json/FbSerializerProvider$1;

    invoke-direct {v0, p0}, Lcom/facebook/common/json/FbSerializerProvider$1;-><init>(LX/2Ah;)V

    iput-object v0, p0, LX/2Ah;->mCollectionSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 377527
    iput-object p4, p0, LX/2Ah;->mJsonLogger:LX/0ly;

    .line 377528
    sget-boolean v0, LX/2Ah;->g:Z

    if-nez v0, :cond_0

    .line 377529
    invoke-static {}, LX/2Ah;->n()V

    .line 377530
    const/4 v0, 0x1

    sput-boolean v0, LX/2Ah;->g:Z

    .line 377531
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377520
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 377521
    if-nez v0, :cond_0

    .line 377522
    invoke-static {p0}, LX/2Ah;->b(Ljava/lang/Class;)V

    .line 377523
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 377524
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 377513
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0, p1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377514
    return-void
.end method

.method private b(LX/0m2;LX/0nS;)LX/2Ah;
    .locals 2

    .prologue
    .line 377532
    new-instance v0, LX/2Ah;

    iget-object v1, p0, LX/2Ah;->mJsonLogger:LX/0ly;

    invoke-direct {v0, p0, p1, p2, v1}, LX/2Ah;-><init>(LX/0my;LX/0m2;LX/0nS;LX/0ly;)V

    return-object v0
.end method

.method private static b(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 377509
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x24

    const/16 v3, 0x5f

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Serializer"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 377510
    :goto_0
    return-void

    .line 377511
    :catch_0
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "$Serializer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 377512
    :catch_1
    goto :goto_0
.end method

.method private static n()V
    .locals 4

    .prologue
    .line 377479
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/String;

    new-instance v2, Lcom/fasterxml/jackson/databind/ser/std/StringSerializer;

    invoke-direct {v2}, Lcom/fasterxml/jackson/databind/ser/std/StringSerializer;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377480
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/Integer;

    new-instance v2, Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$IntegerSerializer;

    invoke-direct {v2}, Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$IntegerSerializer;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377481
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/Long;

    new-instance v2, Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$LongSerializer;

    invoke-direct {v2}, Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$LongSerializer;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377482
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/Boolean;

    new-instance v2, Lcom/fasterxml/jackson/databind/ser/std/BooleanSerializer;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/fasterxml/jackson/databind/ser/std/BooleanSerializer;-><init>(Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377483
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/Float;

    new-instance v2, Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$FloatSerializer;

    invoke-direct {v2}, Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$FloatSerializer;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377484
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    const-class v1, Ljava/lang/Double;

    new-instance v2, Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$DoubleSerializer;

    invoke-direct {v2}, Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$DoubleSerializer;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377485
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/0m2;LX/0nS;)LX/0mx;
    .locals 1

    .prologue
    .line 377508
    invoke-direct {p0, p1, p2}, LX/2Ah;->b(LX/0m2;LX/0nS;)LX/2Ah;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;ZLX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .param p3    # LX/2Ay;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;Z",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377486
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 377487
    if-eqz v0, :cond_1

    .line 377488
    :cond_0
    :goto_0
    return-object v0

    .line 377489
    :cond_1
    invoke-static {p1}, LX/2Ah;->b(Ljava/lang/Class;)V

    .line 377490
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 377491
    if-nez v0, :cond_0

    .line 377492
    const-class v0, LX/0gT;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377493
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    sget-object v1, LX/2Ah;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377494
    sget-object v0, LX/2Ah;->h:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0

    .line 377495
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 377496
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    sget-object v1, LX/2Ah;->i:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377497
    sget-object v0, LX/2Ah;->i:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0

    .line 377498
    :cond_3
    const-class v0, Ljava/util/Collection;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 377499
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    iget-object v1, p0, LX/2Ah;->mCollectionSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377500
    iget-object v0, p0, LX/2Ah;->mCollectionSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0

    .line 377501
    :cond_4
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 377502
    sget-object v0, LX/2Ah;->f:Ljava/util/concurrent/ConcurrentMap;

    sget-object v1, LX/2Ah;->j:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377503
    sget-object v0, LX/2Ah;->j:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0

    .line 377504
    :cond_5
    invoke-super {p0, p1, p2, p3}, LX/0mx;->a(Ljava/lang/Class;ZLX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 377505
    iget-object v1, p0, LX/2Ah;->mJsonLogger:LX/0ly;

    if-eqz v1, :cond_0

    .line 377506
    invoke-virtual {p1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    .line 377507
    goto :goto_0
.end method
