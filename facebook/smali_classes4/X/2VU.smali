.class public LX/2VU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Xl;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/00G;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Xl;LX/0Or;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "LX/00G;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 417738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417739
    iput-object p1, p0, LX/2VU;->a:LX/0Xl;

    .line 417740
    iput-object p2, p0, LX/2VU;->b:LX/0Or;

    .line 417741
    return-void
.end method

.method public static a(LX/0QB;)LX/2VU;
    .locals 1

    .prologue
    .line 417742
    invoke-static {p0}, LX/2VU;->b(LX/0QB;)LX/2VU;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2VU;
    .locals 3

    .prologue
    .line 417743
    new-instance v1, LX/2VU;

    invoke-static {p0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v0

    check-cast v0, LX/0Xl;

    const/16 v2, 0x2d4

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/2VU;-><init>(LX/0Xl;LX/0Or;)V

    .line 417744
    return-object v1
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 417745
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.facebook.abtest.action.UPDATE_CACHE"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 417746
    if-nez p1, :cond_0

    .line 417747
    const-string v2, "process_name"

    iget-object v0, p0, LX/2VU;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00G;

    .line 417748
    iget-object p1, v0, LX/00G;->b:Ljava/lang/String;

    move-object v0, p1

    .line 417749
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 417750
    :cond_0
    iget-object v0, p0, LX/2VU;->a:LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 417751
    return-void
.end method
