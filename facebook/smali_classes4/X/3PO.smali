.class public LX/3PO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3PP;


# direct methods
.method public constructor <init>(LX/3PP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562018
    iput-object p1, p0, LX/3PO;->a:LX/3PP;

    .line 562019
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/4VT;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562020
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;

    .line 562021
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 562022
    :cond_0
    const/4 v0, 0x0

    .line 562023
    :goto_0
    return-object v0

    .line 562024
    :cond_1
    new-instance v0, LX/6A6;

    invoke-direct {v0, p1}, LX/6A6;-><init>(Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;)V

    .line 562025
    move-object v0, v0

    .line 562026
    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 562027
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562028
    const/4 v0, 0x0

    return-object v0
.end method
