.class public LX/22k;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/22l;

.field public final d:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/187;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pm;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/22m;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 361145
    const-class v0, LX/22k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/22k;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/22l;Landroid/os/Handler;LX/0Ot;LX/0Ot;LX/22m;LX/0Ot;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/FeedFetchExecutorService;
        .end annotation
    .end param
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/22l;",
            "Landroid/os/Handler;",
            "LX/0Ot",
            "<",
            "LX/187;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0pm;",
            ">;",
            "LX/22m;",
            "LX/0Ot",
            "<",
            "LX/0pl;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 361146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361147
    iput-object p1, p0, LX/22k;->b:Ljava/util/concurrent/ExecutorService;

    .line 361148
    iput-object p2, p0, LX/22k;->c:LX/22l;

    .line 361149
    iput-object p3, p0, LX/22k;->d:Landroid/os/Handler;

    .line 361150
    iput-object p4, p0, LX/22k;->e:LX/0Ot;

    .line 361151
    iput-object p5, p0, LX/22k;->f:LX/0Ot;

    .line 361152
    iput-object p6, p0, LX/22k;->g:LX/22m;

    .line 361153
    iput-object p7, p0, LX/22k;->h:LX/0Ot;

    .line 361154
    return-void
.end method

.method public static a(LX/0QB;)LX/22k;
    .locals 9

    .prologue
    .line 361155
    new-instance v1, LX/22k;

    invoke-static {p0}, LX/0ph;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/22l;->a(LX/0QB;)LX/22l;

    move-result-object v3

    check-cast v3, LX/22l;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    const/16 v5, 0x670

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x5f2

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v7, LX/22m;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/22m;

    const/16 v8, 0x5f0

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, LX/22k;-><init>(Ljava/util/concurrent/ExecutorService;LX/22l;Landroid/os/Handler;LX/0Ot;LX/0Ot;LX/22m;LX/0Ot;)V

    .line 361156
    move-object v0, v1

    .line 361157
    return-object v0
.end method


# virtual methods
.method public final a(LX/230;)V
    .locals 3

    .prologue
    .line 361158
    iget-object v0, p0, LX/22k;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feed/data/FeedFetcher$FeedFetcherRunnable;-><init>(LX/22k;LX/230;)V

    const v2, -0x31a72a73    # -9.0946848E8f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 361159
    return-void
.end method
