.class public LX/35j;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/1qb;

.field public final b:LX/2sO;

.field public final c:LX/2y3;

.field public final d:LX/C0D;

.field public final e:LX/Ap5;

.field public final f:LX/0Uh;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1qb;LX/2sO;LX/2y3;LX/C0D;LX/Ap5;LX/0Uh;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1qb;",
            "LX/2sO;",
            "LX/2y3;",
            "LX/C0D;",
            "LX/Ap5;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497543
    iput-object p1, p0, LX/35j;->a:LX/1qb;

    .line 497544
    iput-object p2, p0, LX/35j;->b:LX/2sO;

    .line 497545
    iput-object p3, p0, LX/35j;->c:LX/2y3;

    .line 497546
    iput-object p4, p0, LX/35j;->d:LX/C0D;

    .line 497547
    iput-object p5, p0, LX/35j;->e:LX/Ap5;

    .line 497548
    iput-object p6, p0, LX/35j;->f:LX/0Uh;

    .line 497549
    iput-object p7, p0, LX/35j;->g:LX/0Ot;

    .line 497550
    return-void
.end method

.method public static a(LX/0QB;)LX/35j;
    .locals 11

    .prologue
    .line 497551
    const-class v1, LX/35j;

    monitor-enter v1

    .line 497552
    :try_start_0
    sget-object v0, LX/35j;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 497553
    sput-object v2, LX/35j;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 497554
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497555
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 497556
    new-instance v3, LX/35j;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v4

    check-cast v4, LX/1qb;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v5

    check-cast v5, LX/2sO;

    invoke-static {v0}, LX/2y3;->a(LX/0QB;)LX/2y3;

    move-result-object v6

    check-cast v6, LX/2y3;

    invoke-static {v0}, LX/C0D;->b(LX/0QB;)LX/C0D;

    move-result-object v7

    check-cast v7, LX/C0D;

    invoke-static {v0}, LX/Ap5;->a(LX/0QB;)LX/Ap5;

    move-result-object v8

    check-cast v8, LX/Ap5;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    const/16 v10, 0x2ca

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/35j;-><init>(LX/1qb;LX/2sO;LX/2y3;LX/C0D;LX/Ap5;LX/0Uh;LX/0Ot;)V

    .line 497557
    move-object v0, v3

    .line 497558
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 497559
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/35j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497560
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 497561
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2y3;LX/1V4;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2y3;",
            "LX/1V4;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 497562
    iget-object v0, p2, LX/1V4;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 497563
    iget-object v0, p2, LX/1V4;->a:LX/0ad;

    sget v1, LX/1Dd;->a:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 497564
    invoke-static {v0}, LX/1V4;->a(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/1V4;->c:Ljava/lang/Boolean;

    .line 497565
    :cond_0
    iget-object v0, p2, LX/1V4;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 497566
    if-eqz v0, :cond_1

    invoke-static {p0, p1}, Lcom/facebook/feedplugins/attachments/linkshare/CoverPhotoShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2y3;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
