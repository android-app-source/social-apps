.class public LX/34B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/Gd2;

.field public b:LX/0sK;

.field public c:LX/Gd5;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Gd2;LX/0sK;LX/Gd5;)V
    .locals 3

    .prologue
    .line 494518
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "main.jsbundle"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, LX/34B;-><init>(LX/Gd2;LX/0sK;LX/Gd5;Ljava/util/List;)V

    .line 494519
    return-void
.end method

.method private constructor <init>(LX/Gd2;LX/0sK;LX/Gd5;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Gd2;",
            "LX/0sK;",
            "LX/Gd5;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 494520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494521
    iput-object p1, p0, LX/34B;->a:LX/Gd2;

    .line 494522
    iput-object p2, p0, LX/34B;->b:LX/0sK;

    .line 494523
    iput-object p3, p0, LX/34B;->c:LX/Gd5;

    .line 494524
    iput-object p4, p0, LX/34B;->d:Ljava/util/List;

    .line 494525
    return-void
.end method

.method public static b(LX/34B;)LX/2sp;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 494526
    iget-object v0, p0, LX/34B;->b:LX/0sK;

    invoke-virtual {v0}, LX/0sK;->c()I

    move-result v1

    .line 494527
    iget-object v0, p0, LX/34B;->b:LX/0sK;

    invoke-virtual {v0}, LX/0sK;->b()I

    move-result v2

    .line 494528
    iget-object v0, p0, LX/34B;->d:Ljava/util/List;

    const/4 v3, 0x0

    .line 494529
    const/4 v4, -0x1

    if-ne v2, v4, :cond_5

    .line 494530
    :cond_0
    :goto_0
    move-object v3, v3

    .line 494531
    if-nez v3, :cond_3

    const/4 v0, 0x0

    .line 494532
    :goto_1
    if-eq v0, v1, :cond_4

    .line 494533
    iget-object v4, p0, LX/34B;->b:LX/0sK;

    .line 494534
    invoke-static {v4}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v10

    const-string v11, "download_end"

    const-wide/16 v12, 0x0

    invoke-virtual {v10, v11, v12, v13}, LX/0WS;->a(Ljava/lang/String;J)J

    move-result-wide v10

    move-wide v6, v10

    .line 494535
    const/4 v4, 0x0

    .line 494536
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    .line 494537
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 494538
    cmp-long v5, v8, v6

    if-lez v5, :cond_1

    .line 494539
    sub-long v4, v8, v6

    long-to-int v4, v4

    div-int/lit16 v4, v4, 0x3e8

    .line 494540
    :cond_1
    iget-object v5, p0, LX/34B;->b:LX/0sK;

    .line 494541
    invoke-static {v5}, LX/0sK;->i(LX/0sK;)LX/0WS;

    move-result-object v6

    invoke-virtual {v6}, LX/0WS;->b()LX/1gW;

    move-result-object v6

    const-string v7, "activated"

    invoke-interface {v6, v7, v0}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    move-result-object v6

    const-string v7, "next"

    invoke-interface {v6, v7}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v6

    const-string v7, "download_end"

    invoke-interface {v6, v7}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v6

    invoke-interface {v6}, LX/1gW;->c()V

    .line 494542
    invoke-virtual {p0, v0, v4}, LX/34B;->a(II)V

    .line 494543
    :cond_2
    :goto_2
    return-object v3

    .line 494544
    :cond_3
    iget v0, v3, LX/2sp;->a:I

    move v0, v0

    .line 494545
    goto :goto_1

    .line 494546
    :cond_4
    if-eq v0, v2, :cond_2

    .line 494547
    iget-object v0, p0, LX/34B;->b:LX/0sK;

    invoke-virtual {v0}, LX/0sK;->h()V

    goto :goto_2

    .line 494548
    :cond_5
    if-nez v2, :cond_6

    if-eqz v1, :cond_0

    .line 494549
    :cond_6
    new-instance v4, LX/2sp;

    iget-object v5, p0, LX/34B;->c:LX/Gd5;

    invoke-direct {v4, v5, v2}, LX/2sp;-><init>(LX/Gd5;I)V

    .line 494550
    invoke-virtual {v4, v0}, LX/2sp;->a(Ljava/util/List;)Z

    move-result v5

    if-eqz v5, :cond_7

    move-object v3, v4

    .line 494551
    goto :goto_0

    .line 494552
    :cond_7
    new-instance v4, LX/2sp;

    iget-object v5, p0, LX/34B;->c:LX/Gd5;

    invoke-direct {v4, v5, v1}, LX/2sp;-><init>(LX/Gd5;I)V

    .line 494553
    invoke-virtual {v4, v0}, LX/2sp;->a(Ljava/util/List;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v3, v4

    .line 494554
    goto :goto_0
.end method


# virtual methods
.method public a(II)V
    .locals 4

    .prologue
    .line 494555
    iget-object v0, p0, LX/34B;->a:LX/Gd2;

    int-to-long v2, p2

    invoke-virtual {v0, p1, v2, v3}, LX/Gd2;->a(IJ)V

    .line 494556
    return-void
.end method
