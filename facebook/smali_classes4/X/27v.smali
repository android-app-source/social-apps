.class public LX/27v;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/27v;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0dC;

.field private final d:Ljava/lang/Object;

.field private final e:Ljava/lang/Object;

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private g:LX/282;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mShouldShareLock"
    .end annotation
.end field

.field private i:LX/4hA;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mShouldShareSfdidLock"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0dC;LX/0Or;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/device_id/ShareDeviceId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 373311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373312
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/27v;->d:Ljava/lang/Object;

    .line 373313
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/27v;->e:Ljava/lang/Object;

    .line 373314
    iput-object v1, p0, LX/27v;->h:Ljava/lang/Boolean;

    .line 373315
    iput-object v1, p0, LX/27v;->j:Ljava/lang/Boolean;

    .line 373316
    iput-object p1, p0, LX/27v;->c:LX/0dC;

    .line 373317
    iput-object p2, p0, LX/27v;->a:LX/0Or;

    .line 373318
    iput-object p3, p0, LX/27v;->b:LX/0Ot;

    .line 373319
    iput-object p4, p0, LX/27v;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 373320
    return-void
.end method

.method public static a(LX/0QB;)LX/27v;
    .locals 7

    .prologue
    .line 373298
    sget-object v0, LX/27v;->k:LX/27v;

    if-nez v0, :cond_1

    .line 373299
    const-class v1, LX/27v;

    monitor-enter v1

    .line 373300
    :try_start_0
    sget-object v0, LX/27v;->k:LX/27v;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 373301
    if-eqz v2, :cond_0

    .line 373302
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 373303
    new-instance v5, LX/27v;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v3

    check-cast v3, LX/0dC;

    const/16 v4, 0x1482

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v4, 0xdf4

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v5, v3, v6, p0, v4}, LX/27v;-><init>(LX/0dC;LX/0Or;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 373304
    move-object v0, v5

    .line 373305
    sput-object v0, LX/27v;->k:LX/27v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373306
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 373307
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 373308
    :cond_1
    sget-object v0, LX/27v;->k:LX/27v;

    return-object v0

    .line 373309
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 373310
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/282;)V
    .locals 6

    .prologue
    .line 373294
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/27v;->g:LX/282;

    .line 373295
    iget-object v0, p0, LX/27v;->c:LX/0dC;

    new-instance v1, LX/0dI;

    iget-object v2, p0, LX/27v;->g:LX/282;

    iget-object v2, v2, LX/282;->a:Ljava/lang/String;

    iget-object v3, p0, LX/27v;->g:LX/282;

    iget-wide v4, v3, LX/282;->b:J

    invoke-direct {v1, v2, v4, v5}, LX/0dI;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, LX/0dC;->a(LX/0dI;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373296
    monitor-exit p0

    return-void

    .line 373297
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/4hA;)V
    .locals 1

    .prologue
    .line 373290
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/27v;->i:LX/4hA;

    .line 373291
    invoke-virtual {p0, p1}, LX/27v;->b(LX/4hA;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373292
    monitor-exit p0

    return-void

    .line 373293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 373281
    iget-object v2, p0, LX/27v;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 373282
    :try_start_0
    iget-object v0, p0, LX/27v;->h:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 373283
    iget-object v0, p0, LX/27v;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v4, LX/0X5;->bK:J

    const/4 v3, 0x1

    invoke-interface {v0, v4, v5, v3}, LX/0W4;->a(JZ)Z

    move-result v3

    .line 373284
    iget-object v0, p0, LX/27v;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/27v;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 373285
    :goto_0
    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/27v;->h:Ljava/lang/Boolean;

    .line 373286
    :cond_0
    iget-object v0, p0, LX/27v;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    monitor-exit v2

    return v0

    :cond_1
    move v0, v1

    .line 373287
    goto :goto_0

    .line 373288
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 373289
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized b()LX/282;
    .locals 8

    .prologue
    .line 373241
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/27v;->c:LX/0dC;

    invoke-virtual {v0}, LX/0dC;->b()LX/0dI;

    move-result-object v0

    .line 373242
    iget-object v1, p0, LX/27v;->g:LX/282;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/27v;->g:LX/282;

    iget-wide v2, v1, LX/282;->b:J

    .line 373243
    iget-wide v6, v0, LX/0dI;->b:J

    move-wide v4, v6

    .line 373244
    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, LX/27v;->g:LX/282;

    iget-object v1, v1, LX/282;->a:Ljava/lang/String;

    .line 373245
    iget-object v2, v0, LX/0dI;->a:Ljava/lang/String;

    move-object v2, v2

    .line 373246
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373247
    iget-object v0, p0, LX/27v;->g:LX/282;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373248
    :goto_0
    monitor-exit p0

    return-object v0

    .line 373249
    :cond_0
    :try_start_1
    new-instance v1, LX/282;

    .line 373250
    iget-object v2, v0, LX/0dI;->a:Ljava/lang/String;

    move-object v2, v2

    .line 373251
    iget-wide v6, v0, LX/0dI;->b:J

    move-wide v4, v6

    .line 373252
    invoke-direct {v1, v2, v4, v5}, LX/282;-><init>(Ljava/lang/String;J)V

    iput-object v1, p0, LX/27v;->g:LX/282;

    .line 373253
    iget-object v0, p0, LX/27v;->g:LX/282;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 373254
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(LX/4hA;)V
    .locals 4

    .prologue
    .line 373279
    iget-object v0, p0, LX/27v;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dH;->e:LX/0Tn;

    iget-wide v2, p1, LX/4hA;->b:J

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dH;->d:LX/0Tn;

    iget-object v2, p1, LX/4hA;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dH;->g:LX/0Tn;

    iget-object v2, p1, LX/4hA;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dH;->h:LX/0Tn;

    iget-object v2, p1, LX/4hA;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 373280
    return-void
.end method

.method public final c()Z
    .locals 5

    .prologue
    .line 373274
    iget-object v1, p0, LX/27v;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 373275
    :try_start_0
    iget-object v0, p0, LX/27v;->j:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 373276
    iget-object v0, p0, LX/27v;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->bN:J

    const/4 v4, 0x1

    invoke-interface {v0, v2, v3, v4}, LX/0W4;->a(JZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/27v;->j:Ljava/lang/Boolean;

    .line 373277
    :cond_0
    iget-object v0, p0, LX/27v;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 373278
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized d()LX/4hA;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 373268
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/27v;->i:LX/4hA;

    if-eqz v0, :cond_0

    .line 373269
    iget-object v0, p0, LX/27v;->i:LX/4hA;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373270
    :goto_0
    monitor-exit p0

    return-object v0

    .line 373271
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LX/27v;->f()LX/4hA;

    move-result-object v0

    iput-object v0, p0, LX/27v;->i:LX/4hA;

    .line 373272
    iget-object v0, p0, LX/27v;->i:LX/4hA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 373273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 373265
    iget-object v0, p0, LX/27v;->c:LX/0dC;

    .line 373266
    iget-object p0, v0, LX/0dC;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result p0

    move v0, p0

    .line 373267
    return v0
.end method

.method public final f()LX/4hA;
    .locals 10

    .prologue
    const-wide v8, 0x7fffffffffffffffL

    const/4 v0, 0x0

    .line 373257
    invoke-virtual {p0}, LX/27v;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 373258
    :cond_0
    :goto_0
    return-object v0

    .line 373259
    :cond_1
    iget-object v1, p0, LX/27v;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dH;->d:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 373260
    iget-object v2, p0, LX/27v;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0dH;->e:LX/0Tn;

    invoke-interface {v2, v3, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 373261
    iget-object v3, p0, LX/27v;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0dH;->g:LX/0Tn;

    invoke-interface {v3, v4, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 373262
    iget-object v3, p0, LX/27v;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0dH;->h:LX/0Tn;

    invoke-interface {v3, v5, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 373263
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v6, v8

    if-eqz v3, :cond_0

    .line 373264
    new-instance v0, LX/4hA;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct/range {v0 .. v5}, LX/4hA;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 373255
    :try_start_0
    iget-object v0, p0, LX/27v;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 373256
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
