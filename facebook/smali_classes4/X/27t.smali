.class public LX/27t;
.super LX/16B;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0dC;

.field public final c:LX/0SG;

.field private final d:Landroid/content/Context;

.field private final e:LX/12x;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2X5;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/49e;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/lang/Boolean;

.field private final i:LX/0W3;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/280;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/phoneid/PhoneIdStore;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/01U;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 373178
    const-class v0, LX/27t;

    sput-object v0, LX/27t;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0dC;LX/0SG;Landroid/content/Context;LX/12x;LX/0Or;Ljava/util/Set;Ljava/lang/Boolean;LX/0W3;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/01U;)V
    .locals 0
    .param p7    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/device_id/ShareDeviceId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0SG;",
            "Landroid/content/Context;",
            "Lcom/facebook/common/alarm/FbAlarmManager;",
            "LX/0Or",
            "<",
            "LX/2X5;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/49e;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/280;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/phoneid/PhoneIdStore;",
            ">;",
            "LX/01U;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 373179
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 373180
    iput-object p1, p0, LX/27t;->b:LX/0dC;

    .line 373181
    iput-object p2, p0, LX/27t;->c:LX/0SG;

    .line 373182
    iput-object p3, p0, LX/27t;->d:Landroid/content/Context;

    .line 373183
    iput-object p4, p0, LX/27t;->e:LX/12x;

    .line 373184
    iput-object p5, p0, LX/27t;->f:LX/0Or;

    .line 373185
    iput-object p6, p0, LX/27t;->g:Ljava/util/Set;

    .line 373186
    iput-object p7, p0, LX/27t;->h:Ljava/lang/Boolean;

    .line 373187
    iput-object p8, p0, LX/27t;->i:LX/0W3;

    .line 373188
    iput-object p9, p0, LX/27t;->j:LX/0Ot;

    .line 373189
    iput-object p10, p0, LX/27t;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 373190
    iput-object p11, p0, LX/27t;->l:LX/0Ot;

    .line 373191
    iput-object p12, p0, LX/27t;->m:LX/0Ot;

    .line 373192
    iput-object p13, p0, LX/27t;->n:LX/01U;

    .line 373193
    return-void
.end method

.method public static b(LX/0QB;)LX/27t;
    .locals 14

    .prologue
    .line 373194
    new-instance v0, LX/27t;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v1

    check-cast v1, LX/0dC;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v4

    check-cast v4, LX/12x;

    const/16 v5, 0x4c8

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/27u;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v6

    invoke-static {p0}, LX/38I;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    const/16 v9, 0x2ba

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v11, 0xf18

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x4c6

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {p0}, LX/0XV;->b(LX/0QB;)LX/01U;

    move-result-object v13

    check-cast v13, LX/01U;

    invoke-direct/range {v0 .. v13}, LX/27t;-><init>(LX/0dC;LX/0SG;Landroid/content/Context;LX/12x;LX/0Or;Ljava/util/Set;Ljava/lang/Boolean;LX/0W3;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/01U;)V

    .line 373195
    return-object v0
.end method

.method public static l(LX/27t;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 373196
    iget-object v0, p0, LX/27t;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 373197
    :goto_0
    return-void

    .line 373198
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 373199
    const-string v0, "com.facebook.GET_UNIQUE_ID"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 373200
    iget-object v0, p0, LX/27t;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2X5;

    .line 373201
    new-instance v0, LX/2X6;

    invoke-direct {v0, p0}, LX/2X6;-><init>(LX/27t;)V

    .line 373202
    iput-object v0, v3, LX/2X5;->c:LX/2X6;

    .line 373203
    iget-object v0, p0, LX/27t;->d:Landroid/content/Context;

    iget-object v2, p0, LX/27t;->n:LX/01U;

    invoke-virtual {v2}, LX/01U;->getReceiverPermission()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 373204
    iget-object v0, p0, LX/27t;->i:LX/0W3;

    sget-wide v2, LX/0X5;->bL:J

    const v1, 0x93a80

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v1

    .line 373205
    iget-object v0, p0, LX/27t;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dH;->c:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 373206
    iget-object v0, p0, LX/27t;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/27t;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 373207
    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/27t;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    int-to-long v0, v1

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    .line 373208
    iget-object v0, p0, LX/27t;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/280;

    invoke-virtual {v0}, LX/280;->a()V

    .line 373209
    iget-object v0, p0, LX/27t;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 373210
    iget-object v0, p0, LX/27t;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dH;->c:LX/0Tn;

    iget-object v2, p0, LX/27t;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 373211
    :cond_1
    iget-object v0, p0, LX/27t;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 373212
    iget-object v0, p0, LX/27t;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dH;->f:LX/0Tn;

    iget-object v2, p0, LX/27t;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 373213
    :cond_2
    return-void

    .line 373214
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 12
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 373215
    invoke-static {p0}, LX/27t;->l(LX/27t;)V

    .line 373216
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/27t;->d:Landroid/content/Context;

    const-class v2, Lcom/facebook/device_id/UniqueDeviceIdBroadcastSender$LocalBroadcastReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 373217
    const-string v1, "com.facebook.GET_UNIQUE_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 373218
    iget-object v1, p0, LX/27t;->d:Landroid/content/Context;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 373219
    iget-object v1, p0, LX/27t;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    .line 373220
    new-instance v6, Ljava/util/Random;

    iget-object v7, p0, LX/27t;->c:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/util/Random;-><init>(J)V

    .line 373221
    const/16 v7, 0xc

    invoke-virtual {v6, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v7

    add-int/lit8 v7, v7, -0x6

    int-to-long v8, v7

    const-wide/32 v10, 0x36ee80

    mul-long/2addr v8, v10

    const/16 v7, 0x3c

    invoke-virtual {v6, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    add-int/lit8 v6, v6, -0x1e

    int-to-long v6, v6

    const-wide/32 v10, 0xea60

    mul-long/2addr v6, v10

    add-long/2addr v6, v8

    move-wide v4, v6

    .line 373222
    add-long/2addr v2, v4

    .line 373223
    iget-object v1, p0, LX/27t;->e:LX/12x;

    const/4 v4, 0x1

    .line 373224
    invoke-virtual {v1, v4, v2, v3, v0}, LX/12x;->c(IJLandroid/app/PendingIntent;)V

    .line 373225
    return-void
.end method
