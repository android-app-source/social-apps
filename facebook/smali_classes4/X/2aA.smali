.class public LX/2aA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2aA;


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 423499
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2aA;-><init>(Ljava/lang/String;)V

    .line 423500
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 2
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 423501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 423502
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/2aA;->a:Ljava/lang/String;

    .line 423503
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".permission.CROSS_PROCESS_BROADCAST_MANAGER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2aA;->b:Ljava/lang/String;

    .line 423504
    return-void
.end method

.method public static a(LX/0QB;)LX/2aA;
    .locals 4

    .prologue
    .line 423505
    sget-object v0, LX/2aA;->c:LX/2aA;

    if-nez v0, :cond_1

    .line 423506
    const-class v1, LX/2aA;

    monitor-enter v1

    .line 423507
    :try_start_0
    sget-object v0, LX/2aA;->c:LX/2aA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 423508
    if-eqz v2, :cond_0

    .line 423509
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 423510
    new-instance p0, LX/2aA;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/2aA;-><init>(Landroid/content/Context;)V

    .line 423511
    move-object v0, p0

    .line 423512
    sput-object v0, LX/2aA;->c:LX/2aA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423513
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 423514
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 423515
    :cond_1
    sget-object v0, LX/2aA;->c:LX/2aA;

    return-object v0

    .line 423516
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 423517
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 423518
    iget-object v0, p0, LX/2aA;->a:Ljava/lang/String;

    move-object v0, v0

    .line 423519
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 423520
    invoke-virtual {p2, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 423521
    return-void
.end method
