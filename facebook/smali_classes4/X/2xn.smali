.class public abstract LX/2xn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ai;


# instance fields
.field private a:LX/0So;

.field private final b:LX/1BP;

.field public final c:LX/2xo;

.field private d:Z


# direct methods
.method public constructor <init>(LX/1BP;J)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 479260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 479261
    sget-object v1, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v1, v1

    .line 479262
    iput-object v1, p0, LX/2xn;->a:LX/0So;

    .line 479263
    iput-boolean v0, p0, LX/2xn;->d:Z

    .line 479264
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 479265
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 479266
    iput-object p1, p0, LX/2xn;->b:LX/1BP;

    .line 479267
    new-instance v0, LX/2xo;

    invoke-direct {v0, p2, p3}, LX/2xo;-><init>(J)V

    iput-object v0, p0, LX/2xn;->c:LX/2xo;

    .line 479268
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 479300
    iget-boolean v0, p0, LX/2xn;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2xn;->c:LX/2xo;

    invoke-virtual {v0}, LX/2xo;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479301
    iget-object v0, p0, LX/2xn;->b:LX/1BP;

    iget-object v1, p0, LX/2xn;->c:LX/2xo;

    invoke-interface {v0, v1}, LX/1BP;->a(LX/2xo;)V

    .line 479302
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2xn;->d:Z

    .line 479303
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 479299
    iget-object v0, p0, LX/2xn;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)V
    .locals 6

    .prologue
    .line 479294
    iget-object v0, p0, LX/2xn;->c:LX/2xo;

    .line 479295
    iget-wide v1, v0, LX/2xo;->b:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 479296
    iput-wide p1, v0, LX/2xo;->b:J

    .line 479297
    :cond_0
    invoke-direct {p0}, LX/2xn;->b()V

    .line 479298
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 479293
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 479304
    iget-object v0, p0, LX/2xn;->c:LX/2xo;

    invoke-virtual {p0}, LX/2xn;->a()J

    move-result-wide v2

    .line 479305
    iget-wide v4, v0, LX/2xo;->d:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 479306
    iput-wide v2, v0, LX/2xo;->d:J

    .line 479307
    :cond_0
    iget-object v0, p0, LX/2xn;->c:LX/2xo;

    .line 479308
    iput-object p2, v0, LX/2xo;->h:Ljava/lang/Object;

    .line 479309
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 9
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 479288
    iget-object v0, p0, LX/2xn;->c:LX/2xo;

    invoke-virtual {p0}, LX/2xn;->a()J

    move-result-wide v2

    .line 479289
    iget-wide v4, v0, LX/2xo;->f:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 479290
    iput-wide v2, v0, LX/2xo;->f:J

    .line 479291
    :cond_0
    invoke-direct {p0}, LX/2xn;->b()V

    .line 479292
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 479287
    return-void
.end method

.method public final b(J)V
    .locals 6

    .prologue
    .line 479282
    iget-object v0, p0, LX/2xn;->c:LX/2xo;

    .line 479283
    iget-wide v1, v0, LX/2xo;->c:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 479284
    iput-wide p1, v0, LX/2xo;->c:J

    .line 479285
    :cond_0
    invoke-direct {p0}, LX/2xn;->b()V

    .line 479286
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 479277
    iget-object v0, p0, LX/2xn;->c:LX/2xo;

    invoke-virtual {p0}, LX/2xn;->a()J

    move-result-wide v2

    .line 479278
    iget-wide v4, v0, LX/2xo;->e:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 479279
    iput-wide v2, v0, LX/2xo;->e:J

    .line 479280
    :cond_0
    invoke-direct {p0}, LX/2xn;->b()V

    .line 479281
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 9

    .prologue
    .line 479269
    iget-object v0, p0, LX/2xn;->c:LX/2xo;

    invoke-virtual {p0}, LX/2xn;->a()J

    move-result-wide v2

    .line 479270
    iget-wide v4, v0, LX/2xo;->g:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 479271
    iput-wide v2, v0, LX/2xo;->g:J

    .line 479272
    :cond_0
    if-eqz p2, :cond_1

    .line 479273
    iget-object v0, p0, LX/2xn;->c:LX/2xo;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 479274
    iput-object v1, v0, LX/2xo;->i:Ljava/lang/String;

    .line 479275
    :cond_1
    invoke-direct {p0}, LX/2xn;->b()V

    .line 479276
    return-void
.end method
