.class public final LX/35A;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/359;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:LX/0jW;

.field public d:LX/0wd;

.field public e:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Integer;

.field public i:I

.field public final synthetic j:LX/359;


# direct methods
.method public constructor <init>(LX/359;)V
    .locals 1

    .prologue
    .line 496764
    iput-object p1, p0, LX/35A;->j:LX/359;

    .line 496765
    move-object v0, p1

    .line 496766
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 496767
    const/4 v0, 0x0

    iput v0, p0, LX/35A;->f:I

    .line 496768
    const/16 v0, 0x11

    iput v0, p0, LX/35A;->i:I

    .line 496769
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 496770
    const-string v0, "TouchSpringIconComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/359;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 496771
    check-cast p1, LX/35A;

    .line 496772
    iget-object v0, p1, LX/35A;->d:LX/0wd;

    iput-object v0, p0, LX/35A;->d:LX/0wd;

    .line 496773
    iget-object v0, p1, LX/35A;->g:Ljava/lang/Integer;

    iput-object v0, p0, LX/35A;->g:Ljava/lang/Integer;

    .line 496774
    iget-object v0, p1, LX/35A;->h:Ljava/lang/Integer;

    iput-object v0, p0, LX/35A;->h:Ljava/lang/Integer;

    .line 496775
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 496776
    if-ne p0, p1, :cond_1

    .line 496777
    :cond_0
    :goto_0
    return v0

    .line 496778
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 496779
    goto :goto_0

    .line 496780
    :cond_3
    check-cast p1, LX/35A;

    .line 496781
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 496782
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 496783
    if-eq v2, v3, :cond_0

    .line 496784
    iget-object v2, p0, LX/35A;->a:LX/1Pr;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/35A;->a:LX/1Pr;

    iget-object v3, p1, LX/35A;->a:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 496785
    goto :goto_0

    .line 496786
    :cond_5
    iget-object v2, p1, LX/35A;->a:LX/1Pr;

    if-nez v2, :cond_4

    .line 496787
    :cond_6
    iget-object v2, p0, LX/35A;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/35A;->b:Ljava/lang/String;

    iget-object v3, p1, LX/35A;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 496788
    goto :goto_0

    .line 496789
    :cond_8
    iget-object v2, p1, LX/35A;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 496790
    :cond_9
    iget-object v2, p0, LX/35A;->c:LX/0jW;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/35A;->c:LX/0jW;

    iget-object v3, p1, LX/35A;->c:LX/0jW;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 496791
    goto :goto_0

    .line 496792
    :cond_b
    iget-object v2, p1, LX/35A;->c:LX/0jW;

    if-nez v2, :cond_a

    .line 496793
    :cond_c
    iget-object v2, p0, LX/35A;->e:LX/1dc;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/35A;->e:LX/1dc;

    iget-object v3, p1, LX/35A;->e:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 496794
    goto :goto_0

    .line 496795
    :cond_e
    iget-object v2, p1, LX/35A;->e:LX/1dc;

    if-nez v2, :cond_d

    .line 496796
    :cond_f
    iget v2, p0, LX/35A;->f:I

    iget v3, p1, LX/35A;->f:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 496797
    goto :goto_0

    .line 496798
    :cond_10
    iget v2, p0, LX/35A;->i:I

    iget v3, p1, LX/35A;->i:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 496799
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 496800
    const/4 v1, 0x0

    .line 496801
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/35A;

    .line 496802
    iput-object v1, v0, LX/35A;->d:LX/0wd;

    .line 496803
    iput-object v1, v0, LX/35A;->g:Ljava/lang/Integer;

    .line 496804
    iput-object v1, v0, LX/35A;->h:Ljava/lang/Integer;

    .line 496805
    return-object v0
.end method
