.class public final LX/38X;
.super LX/38Y;
.source ""

# interfaces
.implements LX/1qf;
.implements LX/1qg;


# instance fields
.field public final synthetic a:LX/37g;

.field private b:J


# direct methods
.method public constructor <init>(LX/37g;)V
    .locals 0

    .prologue
    .line 521109
    iput-object p1, p0, LX/38X;->a:LX/37g;

    invoke-direct {p0}, LX/38Y;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 521070
    iget-object v0, p0, LX/38X;->a:LX/37g;

    sget-object v1, LX/38a;->SUSPENDED:LX/38a;

    invoke-static {v0, v1}, LX/37g;->a$redex0(LX/37g;LX/38a;)V

    .line 521071
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v3, 0x0

    const/16 v4, 0xd

    .line 521089
    if-eqz p1, :cond_0

    const-string v0, "com.google.android.gms.cast.EXTRA_APP_NO_LONGER_RUNNING"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521090
    iget-object v0, p0, LX/38X;->a:LX/37g;

    const-string v1, "CastDeviceConnector.onConnected: Application not running"

    invoke-static {v0, v4, v1}, LX/37g;->a$redex0(LX/37g;ILjava/lang/String;)V

    .line 521091
    :cond_0
    :try_start_0
    sget-object v0, LX/7Yq;->c:LX/7Yl;

    iget-object v1, p0, LX/38X;->a:LX/37g;

    iget-object v1, v1, LX/37g;->m:LX/2wX;

    invoke-interface {v0, v1}, LX/7Yl;->a(LX/2wX;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 521092
    iget-object v0, p0, LX/38X;->a:LX/37g;

    iget-object v0, v0, LX/37g;->k:LX/37h;

    .line 521093
    iget-object v1, v0, LX/37h;->h:LX/384;

    move-object v0, v1

    .line 521094
    if-nez v0, :cond_2

    .line 521095
    iget-object v0, p0, LX/38X;->a:LX/37g;

    iget-object v0, v0, LX/37g;->d:LX/37f;

    const-string v1, "CastDeviceConnector.onConnected: Selected device missing"

    invoke-virtual {v0, v4, v1}, LX/37f;->a(ILjava/lang/String;)V

    .line 521096
    iget-object v0, p0, LX/38X;->a:LX/37g;

    invoke-virtual {v0}, LX/37g;->b()V

    .line 521097
    :cond_1
    :goto_0
    return-void

    .line 521098
    :catch_0
    move-exception v0

    .line 521099
    :goto_1
    iget-object v1, p0, LX/38X;->a:LX/37g;

    iget-object v1, v1, LX/37g;->d:LX/37f;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CastDeviceConnector.onConnected: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, LX/37f;->a(ILjava/lang/String;)V

    .line 521100
    iget-object v0, p0, LX/38X;->a:LX/37g;

    invoke-virtual {v0}, LX/37g;->b()V

    goto :goto_0

    .line 521101
    :cond_2
    iget-object v1, p0, LX/38X;->a:LX/37g;

    iget-object v1, v1, LX/37g;->f:LX/37a;

    sget-object v2, LX/37g;->b:LX/0Tn;

    .line 521102
    iget-object v4, v0, LX/384;->c:Ljava/lang/String;

    move-object v0, v4

    .line 521103
    invoke-virtual {v1, v2, v0}, LX/37a;->a(LX/0Tn;Ljava/lang/String;)V

    .line 521104
    iget-object v0, p0, LX/38X;->a:LX/37g;

    sget-object v1, LX/38a;->CONNECTED:LX/38a;

    invoke-static {v0, v1}, LX/37g;->a$redex0(LX/37g;LX/38a;)V

    .line 521105
    iget-wide v0, p0, LX/38X;->b:J

    cmp-long v0, v0, v8

    if-lez v0, :cond_1

    .line 521106
    iget-object v0, p0, LX/38X;->a:LX/37g;

    iget-object v1, v0, LX/37g;->c:LX/0AU;

    const-string v2, "self_reconnect_on_network_failure.succeed"

    iget-object v0, p0, LX/38X;->a:LX/37g;

    iget-object v0, v0, LX/37g;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    iget-wide v6, p0, LX/38X;->b:J

    sub-long/2addr v4, v6

    move-object v6, v3

    invoke-virtual/range {v1 .. v6}, LX/0AU;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 521107
    iput-wide v8, p0, LX/38X;->b:J

    goto :goto_0

    .line 521108
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 521072
    iget-object v2, p0, LX/38X;->a:LX/37g;

    invoke-virtual {v2}, LX/37g;->b()V

    .line 521073
    iget-wide v4, p0, LX/38X;->b:J

    cmp-long v2, v4, v8

    if-lez v2, :cond_0

    move v2, v0

    .line 521074
    :goto_0
    if-nez v2, :cond_1

    iget v4, p1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v4, v4

    .line 521075
    const/4 v5, 0x7

    if-ne v4, v5, :cond_1

    .line 521076
    :goto_1
    if-eqz v0, :cond_2

    .line 521077
    iget-object v0, p0, LX/38X;->a:LX/37g;

    iget-object v0, v0, LX/37g;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/38X;->b:J

    .line 521078
    iget-object v0, p0, LX/38X;->a:LX/37g;

    iget-object v0, v0, LX/37g;->c:LX/0AU;

    const-string v1, "self_reconnect_on_network_failure.start"

    invoke-virtual {v0, v1, v3, v3}, LX/0AU;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 521079
    iget-object v0, p0, LX/38X;->a:LX/37g;

    invoke-static {v0}, LX/37g;->i(LX/37g;)V

    .line 521080
    :goto_2
    return-void

    :cond_0
    move v2, v1

    .line 521081
    goto :goto_0

    :cond_1
    move v0, v1

    .line 521082
    goto :goto_1

    .line 521083
    :cond_2
    if-eqz v2, :cond_3

    .line 521084
    iget-object v0, p0, LX/38X;->a:LX/37g;

    iget-object v1, v0, LX/37g;->c:LX/0AU;

    const-string v2, "self_reconnect_on_network_failure.fail"

    iget-object v0, p0, LX/38X;->a:LX/37g;

    iget-object v0, v0, LX/37g;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    iget-wide v6, p0, LX/38X;->b:J

    sub-long/2addr v4, v6

    move-object v6, v3

    invoke-virtual/range {v1 .. v6}, LX/0AU;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 521085
    iput-wide v8, p0, LX/38X;->b:J

    .line 521086
    :cond_3
    iget-object v0, p0, LX/38X;->a:LX/37g;

    iget-object v0, v0, LX/37g;->d:LX/37f;

    iget v1, p1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v1, v1

    .line 521087
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CastDeviceConnector.onConnectionFailed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/gms/common/ConnectionResult;->e:Ljava/lang/String;

    move-object v3, v3

    .line 521088
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/37f;->a(ILjava/lang/String;)V

    goto :goto_2
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 521068
    iget-object v0, p0, LX/38X;->a:LX/37g;

    const-string v1, "CastDeviceConnector.onApplicationDisconnected"

    invoke-static {v0, p1, v1}, LX/37g;->a$redex0(LX/37g;ILjava/lang/String;)V

    .line 521069
    return-void
.end method
