.class public LX/2KP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile j:LX/2KP;


# instance fields
.field public b:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Fn;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Yb;

.field public h:Z

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 393812
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "inv_notifier/count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2KP;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 393843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393844
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 393845
    iput-object v0, p0, LX/2KP;->b:LX/0Ot;

    .line 393846
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 393847
    iput-object v0, p0, LX/2KP;->c:LX/0Ot;

    .line 393848
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 393849
    iput-object v0, p0, LX/2KP;->d:LX/0Ot;

    .line 393850
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 393851
    iput-object v0, p0, LX/2KP;->e:LX/0Ot;

    .line 393852
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 393853
    iput-object v0, p0, LX/2KP;->f:LX/0Ot;

    .line 393854
    iput-boolean v1, p0, LX/2KP;->h:Z

    .line 393855
    iput-boolean v1, p0, LX/2KP;->i:Z

    .line 393856
    return-void
.end method

.method public static a(LX/0QB;)LX/2KP;
    .locals 8

    .prologue
    .line 393857
    sget-object v0, LX/2KP;->j:LX/2KP;

    if-nez v0, :cond_1

    .line 393858
    const-class v1, LX/2KP;

    monitor-enter v1

    .line 393859
    :try_start_0
    sget-object v0, LX/2KP;->j:LX/2KP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 393860
    if-eqz v2, :cond_0

    .line 393861
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 393862
    new-instance v3, LX/2KP;

    invoke-direct {v3}, LX/2KP;-><init>()V

    .line 393863
    const/16 v4, 0x1ce

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2ca

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xf9a

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xbd2

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p0, 0x69d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 393864
    iput-object v4, v3, LX/2KP;->b:LX/0Ot;

    iput-object v5, v3, LX/2KP;->c:LX/0Ot;

    iput-object v6, v3, LX/2KP;->d:LX/0Ot;

    iput-object v7, v3, LX/2KP;->e:LX/0Ot;

    iput-object p0, v3, LX/2KP;->f:LX/0Ot;

    .line 393865
    move-object v0, v3

    .line 393866
    sput-object v0, LX/2KP;->j:LX/2KP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 393867
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 393868
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 393869
    :cond_1
    sget-object v0, LX/2KP;->j:LX/2KP;

    return-object v0

    .line 393870
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 393871
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2KP;)V
    .locals 10

    .prologue
    .line 393816
    iget-boolean v0, p0, LX/2KP;->h:Z

    .line 393817
    iget-boolean v1, p0, LX/2KP;->i:Z

    .line 393818
    iget-object v2, p0, LX/2KP;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-virtual {v2}, LX/0kb;->v()Z

    move-result v2

    iput-boolean v2, p0, LX/2KP;->i:Z

    .line 393819
    iget-object v2, p0, LX/2KP;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    iput-boolean v2, p0, LX/2KP;->h:Z

    .line 393820
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-boolean v0, p0, LX/2KP;->h:Z

    if-nez v0, :cond_0

    .line 393821
    iget-object v2, p0, LX/2KP;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Fn;

    .line 393822
    iget-object v6, v2, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0W3;

    sget-wide v8, LX/0X5;->dY:J

    invoke-interface {v6, v8, v9}, LX/0W4;->a(J)Z

    move-result v6

    move v2, v6

    .line 393823
    if-nez v2, :cond_1

    .line 393824
    :cond_0
    :goto_0
    return-void

    .line 393825
    :cond_1
    iget-object v2, p0, LX/2KP;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2KP;->a:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v4

    .line 393826
    iget-object v2, p0, LX/2KP;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Fn;

    .line 393827
    iget-object v6, v2, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0W3;

    sget-wide v8, LX/0X5;->dZ:J

    const/4 v7, 0x1

    invoke-interface {v6, v8, v9, v7}, LX/0W4;->a(JI)I

    move-result v6

    move v2, v6

    .line 393828
    if-ge v4, v2, :cond_0

    .line 393829
    iget-object v2, p0, LX/2KP;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0iA;

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v5, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->OFFLINE_FEED_AIRPORT_TRIGGER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, v5}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v5, LX/Erz;

    invoke-virtual {v2, v3, v5}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/Erz;

    .line 393830
    if-eqz v2, :cond_0

    .line 393831
    iget-object v3, p0, LX/2KP;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-virtual {v3}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v3

    const/4 v6, 0x0

    .line 393832
    iget-object v5, v2, LX/Erz;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1Fn;

    invoke-virtual {v5}, LX/1Fn;->a()Z

    move-result v5

    if-nez v5, :cond_2

    move v5, v6

    .line 393833
    :goto_1
    move v2, v5

    .line 393834
    if-eqz v2, :cond_0

    .line 393835
    iget-object v2, p0, LX/2KP;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/2KP;->a:LX/0Tn;

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_0

    .line 393836
    :cond_2
    iget-object v5, v2, LX/Erz;->d:LX/13n;

    .line 393837
    iget-object v7, v5, LX/13n;->a:Landroid/app/Activity;

    move-object v5, v7

    .line 393838
    instance-of v7, v5, Landroid/support/v4/app/FragmentActivity;

    if-nez v7, :cond_3

    move v5, v6

    .line 393839
    goto :goto_1

    .line 393840
    :cond_3
    new-instance v6, LX/6WI;

    invoke-direct {v6, v5}, LX/6WI;-><init>(Landroid/content/Context;)V

    const v7, 0x7f021164

    invoke-virtual {v6, v7}, LX/6WI;->c(I)LX/6WI;

    move-result-object v6

    const v7, 0x7f082eee

    invoke-virtual {v6, v7}, LX/6WI;->b(I)LX/6WI;

    move-result-object v6

    const v7, 0x7f082ef0

    new-instance v8, LX/Ery;

    invoke-direct {v8, v2, v3}, LX/Ery;-><init>(LX/Erz;Lcom/facebook/interstitial/manager/InterstitialLogger;)V

    invoke-virtual {v6, v7, v8}, LX/6WI;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v6

    const v7, 0x7f082eef

    new-instance v8, LX/Erx;

    invoke-direct {v8, v2, v3, v5}, LX/Erx;-><init>(LX/Erz;Lcom/facebook/interstitial/manager/InterstitialLogger;Landroid/content/Context;)V

    invoke-virtual {v6, v7, v8}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v6

    invoke-virtual {v6}, LX/6WI;->a()LX/6WJ;

    move-result-object v6

    .line 393841
    check-cast v5, Landroid/app/Activity;

    new-instance v7, Lcom/facebook/feed/offlinefeed/nux/OfflineFeedNuxInterstitialController$3;

    invoke-direct {v7, v2, v3, v6}, Lcom/facebook/feed/offlinefeed/nux/OfflineFeedNuxInterstitialController$3;-><init>(LX/Erz;Lcom/facebook/interstitial/manager/InterstitialLogger;LX/6WJ;)V

    invoke-virtual {v5, v7}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 393842
    const/4 v5, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 393813
    iget-object v0, p0, LX/2KP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v2, LX/2KQ;

    invoke-direct {v2, p0}, LX/2KQ;-><init>(LX/2KP;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2KP;->g:LX/0Yb;

    .line 393814
    iget-object v0, p0, LX/2KP;->g:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 393815
    return-void
.end method
