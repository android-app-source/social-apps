.class public LX/2ZP;
.super LX/2ZH;
.source ""

# interfaces
.implements LX/2ZK;


# instance fields
.field public final a:LX/2ZQ;

.field public final b:LX/2ZX;

.field public final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/2ZQ;LX/2ZX;Landroid/content/Context;)V
    .locals 0
    .param p2    # LX/2ZX;
        .annotation runtime Lcom/facebook/katana/urimap/fetchable/FacewebUriTemplateMapParser;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 422488
    invoke-direct {p0}, LX/2ZH;-><init>()V

    .line 422489
    iput-object p1, p0, LX/2ZP;->a:LX/2ZQ;

    .line 422490
    iput-object p2, p0, LX/2ZP;->b:LX/2ZX;

    .line 422491
    iput-object p3, p0, LX/2ZP;->c:Landroid/content/Context;

    .line 422492
    return-void
.end method

.method public static b(LX/0QB;)LX/2ZP;
    .locals 4

    .prologue
    .line 422493
    new-instance v3, LX/2ZP;

    invoke-static {p0}, LX/2ZQ;->b(LX/0QB;)LX/2ZQ;

    move-result-object v0

    check-cast v0, LX/2ZQ;

    invoke-static {p0}, LX/2ZU;->b(LX/0QB;)LX/2ZX;

    move-result-object v1

    check-cast v1, LX/2ZX;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {v3, v0, v1, v2}, LX/2ZP;-><init>(LX/2ZQ;LX/2ZX;Landroid/content/Context;)V

    .line 422494
    return-object v3
.end method


# virtual methods
.method public final b()LX/2ZE;
    .locals 2

    .prologue
    .line 422495
    new-instance v0, LX/2YZ;

    invoke-direct {v0, p0}, LX/2YZ;-><init>(LX/2ZP;)V

    return-object v0
.end method

.method public final c()LX/2ZE;
    .locals 2

    .prologue
    .line 422496
    new-instance v0, LX/2YZ;

    invoke-direct {v0, p0}, LX/2YZ;-><init>(LX/2ZP;)V

    return-object v0
.end method

.method public final dq_()Z
    .locals 2

    .prologue
    .line 422497
    iget-object v0, p0, LX/2ZP;->c:Landroid/content/Context;

    invoke-static {v0}, LX/2ZY;->b(Landroid/content/Context;)LX/2Zg;

    move-result-object v0

    const/4 v1, 0x0

    .line 422498
    iget-object p0, v0, LX/2Zg;->g:LX/2Zm;

    invoke-virtual {p0, v1}, LX/2Zm;->c(Ljava/lang/Object;)Z

    move-result p0

    move v0, p0

    .line 422499
    return v0
.end method

.method public final dr_()J
    .locals 2

    .prologue
    .line 422500
    const-wide/32 v0, 0x240c8400

    return-wide v0
.end method
