.class public LX/2KO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2KO;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Uh;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Uh;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 393770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393771
    iput-object p1, p0, LX/2KO;->a:Landroid/content/Context;

    .line 393772
    iput-object p2, p0, LX/2KO;->b:LX/0Uh;

    .line 393773
    const v0, 0x7f0d0201

    iput v0, p0, LX/2KO;->c:I

    .line 393774
    return-void
.end method

.method public static a(LX/0QB;)LX/2KO;
    .locals 3

    .prologue
    .line 393775
    sget-object v0, LX/2KO;->d:LX/2KO;

    if-nez v0, :cond_1

    .line 393776
    const-class v1, LX/2KO;

    monitor-enter v1

    .line 393777
    :try_start_0
    sget-object v0, LX/2KO;->d:LX/2KO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 393778
    if-eqz v2, :cond_0

    .line 393779
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2KO;->b(LX/0QB;)LX/2KO;

    move-result-object v0

    sput-object v0, LX/2KO;->d:LX/2KO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 393780
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 393781
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 393782
    :cond_1
    sget-object v0, LX/2KO;->d:LX/2KO;

    return-object v0

    .line 393783
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 393784
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 393785
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 393786
    const-class v1, LX/2KO;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    .line 393787
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 393788
    return-object v0
.end method

.method private a()Z
    .locals 3

    .prologue
    .line 393789
    iget-object v0, p0, LX/2KO;->b:LX/0Uh;

    const/16 v1, 0x439

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private static b(LX/0QB;)LX/2KO;
    .locals 3

    .prologue
    .line 393790
    new-instance v2, LX/2KO;

    const-class v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-direct {v2, v0, v1}, LX/2KO;-><init>(Landroid/content/Context;LX/0Uh;)V

    .line 393791
    return-object v2
.end method

.method private b()V
    .locals 7

    .prologue
    .line 393792
    iget-object v0, p0, LX/2KO;->a:Landroid/content/Context;

    invoke-static {v0}, LX/45m;->a(Landroid/content/Context;)LX/45m;

    move-result-object v0

    .line 393793
    if-nez v0, :cond_0

    .line 393794
    invoke-direct {p0}, LX/2KO;->c()V

    .line 393795
    :goto_0
    return-void

    .line 393796
    :cond_0
    new-instance v1, LX/45t;

    iget v2, p0, LX/2KO;->c:I

    invoke-direct {v1, v2}, LX/45t;-><init>(I)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 393797
    iput-wide v2, v1, LX/45t;->h:J

    .line 393798
    move-object v1, v1

    .line 393799
    const/4 v2, 0x1

    .line 393800
    iput v2, v1, LX/45t;->b:I

    .line 393801
    move-object v1, v1

    .line 393802
    invoke-virtual {v1}, LX/45t;->a()LX/45u;

    move-result-object v1

    .line 393803
    invoke-virtual {v0, v1}, LX/45m;->a(LX/45u;)V

    goto :goto_0
.end method

.method private c()V
    .locals 7

    .prologue
    .line 393804
    iget-object v0, p0, LX/2KO;->a:Landroid/content/Context;

    invoke-static {v0}, LX/2KO;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 393805
    iget-object v1, p0, LX/2KO;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v2, v0, v3}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 393806
    iget-object v0, p0, LX/2KO;->a:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 393807
    const/4 v1, 0x3

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x14

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 393808
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 1

    .prologue
    .line 393809
    invoke-direct {p0}, LX/2KO;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 393810
    :goto_0
    return-void

    .line 393811
    :cond_0
    invoke-direct {p0}, LX/2KO;->b()V

    goto :goto_0
.end method
