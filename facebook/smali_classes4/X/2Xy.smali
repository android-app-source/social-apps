.class public LX/2Xy;
.super Landroid/os/Handler;
.source ""


# static fields
.field private static final a:LX/0Zh;


# instance fields
.field private final b:LX/2Xu;

.field public final c:Landroid/content/Context;

.field private final d:LX/2WX;

.field public final e:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private g:LX/2YF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:LX/2Y6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field private final j:LX/2Xz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 420425
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    sput-object v0, LX/2Xy;->a:LX/0Zh;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/2WX;LX/2Xu;)V
    .locals 2

    .prologue
    .line 420418
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 420419
    new-instance v0, LX/01J;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/01J;-><init>(I)V

    iput-object v0, p0, LX/2Xy;->e:LX/01J;

    .line 420420
    new-instance v0, LX/2Xz;

    invoke-direct {v0, p0}, LX/2Xz;-><init>(LX/2Xy;)V

    iput-object v0, p0, LX/2Xy;->j:LX/2Xz;

    .line 420421
    iput-object p1, p0, LX/2Xy;->c:Landroid/content/Context;

    .line 420422
    iput-object p3, p0, LX/2Xy;->d:LX/2WX;

    .line 420423
    iput-object p4, p0, LX/2Xy;->b:LX/2Xu;

    .line 420424
    return-void
.end method

.method public static a(LX/2Xy;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 420537
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/2Xy;->a(LX/2Xy;Z)V

    .line 420538
    invoke-direct {p0}, LX/2Xy;->h()V

    .line 420539
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to create instance of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static a(LX/2Xy;Z)V
    .locals 1

    .prologue
    .line 420533
    if-eqz p1, :cond_0

    .line 420534
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2Xy;->f:Z

    .line 420535
    :cond_0
    iget-object v0, p0, LX/2Xy;->b:LX/2Xu;

    invoke-interface {v0, p1}, LX/2Xu;->a(Z)V

    .line 420536
    return-void
.end method

.method private b(Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 420526
    const-string v0, "doUploadFailure"

    const v1, -0x43ccbeb6

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 420527
    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, LX/01m;->b(I)Z

    .line 420528
    iget-object v0, p0, LX/2Xy;->d:LX/2WX;

    iget-object v0, v0, LX/2WX;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v0}, LX/2Xy;->a(LX/2Xy;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420529
    const v0, -0xb4601d

    invoke-static {v0}, LX/03q;->a(I)V

    .line 420530
    return-void

    .line 420531
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 420532
    :catchall_0
    move-exception v0

    const v1, 0x5c69d26e

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method private d()V
    .locals 8

    .prologue
    .line 420490
    const-string v0, "doInit"

    const v1, 0x42effa1e

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 420491
    :try_start_0
    iget-object v0, p0, LX/2Xy;->d:LX/2WX;

    iget-object v2, v0, LX/2WX;->b:LX/2DI;

    .line 420492
    iget-object v0, p0, LX/2Xy;->d:LX/2WX;

    iget-object v0, v0, LX/2WX;->b:LX/2DI;

    .line 420493
    iget-object v1, v0, LX/2DI;->b:Ljava/lang/String;

    move-object v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420494
    :try_start_1
    iget-object v1, p0, LX/2Xy;->c:Landroid/content/Context;

    invoke-static {v1}, LX/0pF;->a(Landroid/content/Context;)LX/0pF;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0pF;->c(Ljava/lang/String;)Lcom/facebook/analytics2/logger/Uploader;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    move-result-object v1

    .line 420495
    :goto_0
    move-object v1, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 420496
    if-nez v1, :cond_0

    .line 420497
    const v0, -0x207063f

    invoke-static {v0}, LX/03q;->a(I)V

    .line 420498
    :goto_1
    return-void

    .line 420499
    :cond_0
    :try_start_3
    iget-object v0, v2, LX/2DI;->c:Ljava/lang/String;

    move-object v0, v0

    .line 420500
    iget-object v3, p0, LX/2Xy;->c:Landroid/content/Context;

    .line 420501
    if-eqz v0, :cond_1

    .line 420502
    invoke-static {v3}, LX/0pF;->a(Landroid/content/Context;)LX/0pF;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0pF;->a(Ljava/lang/String;)Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    move-result-object v4

    .line 420503
    :goto_2
    move-object v5, v4

    .line 420504
    new-instance v0, LX/2Y2;

    iget-object v3, p0, LX/2Xy;->c:Landroid/content/Context;

    sget-object v4, LX/2Xy;->a:LX/0Zh;

    .line 420505
    iget-object v6, v2, LX/2DI;->f:Ljava/lang/String;

    move-object v6, v6

    .line 420506
    invoke-direct {v0, v3, v4, v6, v5}, LX/2Y2;-><init>(Landroid/content/Context;LX/0Zh;Ljava/lang/String;Lcom/facebook/flexiblesampling/SamplingPolicyConfig;)V

    .line 420507
    iget-object v3, v2, LX/2DI;->a:Ljava/io/File;

    move-object v3, v3

    .line 420508
    iget-object v4, p0, LX/2Xy;->j:LX/2Xz;

    .line 420509
    new-instance v6, LX/2Y6;

    const/16 v7, 0x4e20

    invoke-direct {v6, v3, v0, v4, v7}, LX/2Y6;-><init>(Ljava/io/File;LX/2Y2;LX/2Xz;I)V

    move-object v0, v6

    .line 420510
    iput-object v0, p0, LX/2Xy;->h:LX/2Y6;

    .line 420511
    new-instance v0, LX/2YF;

    .line 420512
    iget-object v3, v2, LX/2DI;->e:LX/0pD;

    move-object v2, v3

    .line 420513
    iget-object v3, p0, LX/2Xy;->h:LX/2Y6;

    new-instance v4, LX/2YG;

    invoke-direct {v4, p0}, LX/2YG;-><init>(LX/2Xy;)V

    invoke-direct/range {v0 .. v5}, LX/2YF;-><init>(Lcom/facebook/analytics2/logger/Uploader;LX/0pD;Ljava/util/Iterator;LX/2WY;Lcom/facebook/flexiblesampling/SamplingPolicyConfig;)V

    iput-object v0, p0, LX/2Xy;->g:LX/2YF;

    .line 420514
    iget-object v0, p0, LX/2Xy;->h:LX/2Y6;

    invoke-virtual {v0}, LX/2Y6;->hasNext()Z

    .line 420515
    invoke-virtual {p0}, LX/2Xy;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 420516
    const v0, -0x61ecb1f2

    invoke-static {v0}, LX/03q;->a(I)V

    goto :goto_1

    :catchall_0
    move-exception v0

    const v1, -0x4523df2c

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0

    .line 420517
    :catch_0
    :try_start_4
    move-exception v1

    .line 420518
    invoke-static {p0, v0, v1}, LX/2Xy;->a(LX/2Xy;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 420519
    :goto_3
    const/4 v1, 0x0

    goto :goto_0

    .line 420520
    :catch_1
    move-exception v1

    .line 420521
    invoke-static {p0, v0, v1}, LX/2Xy;->a(LX/2Xy;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 420522
    :catch_2
    move-exception v1

    .line 420523
    invoke-static {p0, v0, v1}, LX/2Xy;->a(LX/2Xy;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 420524
    :catch_3
    move-exception v1

    .line 420525
    invoke-static {p0, v0, v1}, LX/2Xy;->a(LX/2Xy;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_2
.end method

.method private e()V
    .locals 2

    .prologue
    .line 420540
    const-string v0, "doMaybeUploadNext"

    const v1, -0x29cc7516

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 420541
    :try_start_0
    iget-object v0, p0, LX/2Xy;->g:LX/2YF;

    .line 420542
    move-object v0, v0

    .line 420543
    check-cast v0, LX/2YF;

    .line 420544
    invoke-virtual {v0}, LX/2YF;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 420545
    invoke-virtual {v0}, LX/2YF;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420546
    :goto_0
    const v0, -0x36a6d813

    invoke-static {v0}, LX/03q;->a(I)V

    .line 420547
    return-void

    .line 420548
    :cond_0
    :try_start_1
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/2Xy;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2Xy;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420549
    goto :goto_0

    .line 420550
    :catchall_0
    move-exception v0

    const v1, 0x25e623c6

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 420485
    const-string v0, "doNoMoreInput"

    const v1, 0x2557b639

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 420486
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, v0}, LX/2Xy;->a(LX/2Xy;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420487
    const v0, -0x3c831558

    invoke-static {v0}, LX/03q;->a(I)V

    .line 420488
    return-void

    .line 420489
    :catchall_0
    move-exception v0

    const v1, 0x5fa24202

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method private h()V
    .locals 12

    .prologue
    .line 420440
    const-string v0, "exitStateMachine"

    const v1, 0x3f240e06

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 420441
    :try_start_0
    iget-object v0, p0, LX/2Xy;->d:LX/2WX;

    iget-object v0, v0, LX/2WX;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 420442
    iget-object v0, p0, LX/2Xy;->d:LX/2WX;

    iget-object v0, v0, LX/2WX;->b:LX/2DI;

    .line 420443
    iget-object v1, v0, LX/2DI;->a:Ljava/io/File;

    move-object v0, v1

    .line 420444
    iget-object v1, p0, LX/2Xy;->d:LX/2WX;

    iget-object v1, v1, LX/2WX;->d:Ljava/lang/String;

    const/4 v3, 0x0

    .line 420445
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 420446
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 420447
    if-eqz v6, :cond_2

    .line 420448
    array-length v7, v6

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_2

    aget-object v2, v6, v4

    .line 420449
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 420450
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .line 420451
    if-eqz v8, :cond_1

    .line 420452
    array-length v9, v8

    move v2, v3

    :goto_1
    if-ge v2, v9, :cond_1

    aget-object v10, v8, v2

    .line 420453
    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 420454
    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420455
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 420456
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 420457
    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 420458
    invoke-static {v2}, LX/44r;->a(Ljava/io/File;)Z

    goto :goto_2

    .line 420459
    :cond_3
    iget-object v0, p0, LX/2Xy;->c:Landroid/content/Context;

    iget-object v1, p0, LX/2Xy;->d:LX/2WX;

    iget v1, v1, LX/2WX;->a:I

    iget-object v2, p0, LX/2Xy;->d:LX/2WX;

    iget-object v2, v2, LX/2WX;->c:Ljava/lang/String;

    iget-boolean v3, p0, LX/2Xy;->f:Z

    iget-object v4, p0, LX/2Xy;->e:LX/01J;

    .line 420460
    new-instance v5, LX/2Wi;

    invoke-direct {v5, v1, v2, v3, v4}, LX/2Wi;-><init>(ILjava/lang/String;ZLX/01J;)V

    .line 420461
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.facebook.analytics2.action.UPLOAD_JOB_RAN"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 420462
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 420463
    const-string v7, "job_id"

    iget v8, v5, LX/2Wi;->a:I

    invoke-virtual {v9, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 420464
    const-string v7, "hack_action"

    iget-object v8, v5, LX/2Wi;->b:Ljava/lang/String;

    invoke-virtual {v9, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 420465
    const-string v7, "will_retry"

    iget-boolean v8, v5, LX/2Wi;->c:Z

    invoke-virtual {v9, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 420466
    iget-object v7, v5, LX/2Wi;->d:LX/01J;

    invoke-virtual {v7}, LX/01J;->size()I

    move-result v1

    .line 420467
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 420468
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 420469
    const/4 v7, 0x0

    move v8, v7

    :goto_3
    if-ge v8, v1, :cond_5

    .line 420470
    iget-object v7, v5, LX/2Wi;->d:LX/01J;

    invoke-virtual {v7, v8}, LX/01J;->c(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/io/File;

    .line 420471
    iget-object v4, v5, LX/2Wi;->d:LX/01J;

    invoke-virtual {v4, v8}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420472
    if-eqz v7, :cond_4

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    :goto_4
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420473
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_3

    .line 420474
    :cond_4
    const/4 v7, 0x0

    goto :goto_4

    .line 420475
    :cond_5
    const-string v7, "successful_processes"

    invoke-virtual {v9, v7, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 420476
    const-string v7, "newest_files_uploaded"

    invoke-virtual {v9, v7, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 420477
    move-object v5, v9

    .line 420478
    invoke-virtual {v6, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v5

    .line 420479
    invoke-virtual {v0, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 420480
    iget-object v0, p0, LX/2Xy;->b:LX/2Xu;

    invoke-interface {v0}, LX/2Xu;->a()V

    .line 420481
    invoke-virtual {p0}, LX/2Xy;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420482
    const v0, 0x499d5a94    # 1289042.5f

    invoke-static {v0}, LX/03q;->a(I)V

    .line 420483
    return-void

    .line 420484
    :catchall_0
    move-exception v0

    const v1, 0x4a5426d0    # 3475892.0f

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 420438
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/2Xy;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2Xy;->sendMessage(Landroid/os/Message;)Z

    .line 420439
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 420426
    iget-boolean v0, p0, LX/2Xy;->i:Z

    if-eqz v0, :cond_0

    .line 420427
    :goto_0
    return-void

    .line 420428
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 420429
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420430
    :pswitch_0
    invoke-direct {p0}, LX/2Xy;->d()V

    goto :goto_0

    .line 420431
    :pswitch_1
    invoke-direct {p0}, LX/2Xy;->e()V

    goto :goto_0

    .line 420432
    :pswitch_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2Xy;->i:Z

    .line 420433
    invoke-direct {p0}, LX/2Xy;->h()V

    goto :goto_0

    .line 420434
    :pswitch_3
    invoke-direct {p0}, LX/2Xy;->g()V

    .line 420435
    invoke-direct {p0}, LX/2Xy;->h()V

    goto :goto_0

    .line 420436
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/io/IOException;

    invoke-direct {p0, v0}, LX/2Xy;->b(Ljava/io/IOException;)V

    .line 420437
    invoke-direct {p0}, LX/2Xy;->h()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
