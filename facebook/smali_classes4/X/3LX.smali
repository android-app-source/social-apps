.class public LX/3LX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0tX;

.field public final c:LX/03V;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550748
    iput-object p1, p0, LX/3LX;->a:LX/1Ck;

    .line 550749
    iput-object p2, p0, LX/3LX;->b:LX/0tX;

    .line 550750
    iput-object p3, p0, LX/3LX;->c:LX/03V;

    .line 550751
    return-void
.end method

.method public static b(LX/0QB;)LX/3LX;
    .locals 4

    .prologue
    .line 550752
    new-instance v3, LX/3LX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-direct {v3, v0, v1, v2}, LX/3LX;-><init>(LX/1Ck;LX/0tX;LX/03V;)V

    .line 550753
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/DI1;)V
    .locals 5

    .prologue
    .line 550754
    new-instance v0, LX/DI2;

    invoke-direct {v0}, LX/DI2;-><init>()V

    move-object v0, v0

    .line 550755
    new-instance v1, LX/4Gp;

    invoke-direct {v1}, LX/4Gp;-><init>()V

    .line 550756
    const-string v2, "user_id"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 550757
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 550758
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 550759
    iget-object v1, p0, LX/3LX;->a:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "send_wave"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/3LX;->b:LX/0tX;

    sget-object v4, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v3, v0, v4}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v3, LX/DHz;

    invoke-direct {v3, p0, p2}, LX/DHz;-><init>(LX/3LX;LX/DI1;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 550760
    return-void
.end method

.method public final b(Ljava/lang/String;LX/DI1;)V
    .locals 5

    .prologue
    .line 550761
    new-instance v0, LX/DI3;

    invoke-direct {v0}, LX/DI3;-><init>()V

    move-object v0, v0

    .line 550762
    new-instance v1, LX/4Gq;

    invoke-direct {v1}, LX/4Gq;-><init>()V

    .line 550763
    const-string v2, "user_id"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 550764
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 550765
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 550766
    iget-object v1, p0, LX/3LX;->a:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unsend_wave"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/3LX;->b:LX/0tX;

    sget-object v4, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v3, v0, v4}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v3, LX/DI0;

    invoke-direct {v3, p0, p2}, LX/DI0;-><init>(LX/3LX;LX/DI1;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 550767
    return-void
.end method
