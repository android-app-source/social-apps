.class public final LX/3HV;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2pY;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V
    .locals 0

    .prologue
    .line 543559
    iput-object p1, p0, LX/3HV;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2pY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 543560
    const-class v0, LX/2pY;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 543561
    iget-object v0, p0, LX/3HV;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/3HV;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->d(Ljava/lang/String;)J

    move-result-wide v0

    .line 543562
    iget-object v2, p0, LX/3HV;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v2, v2, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->i()I

    move-result v2

    int-to-long v2, v2

    .line 543563
    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 543564
    iget-object v2, p0, LX/3HV;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v2, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->c:LX/2ml;

    iget-wide v2, v2, LX/2ml;->f:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, LX/3HV;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_0

    .line 543565
    iget-object v0, p0, LX/3HV;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    sget-object v1, LX/BSU;->STREAM_DRY_OUT:LX/BSU;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/BSU;)V

    .line 543566
    :cond_0
    return-void
.end method
