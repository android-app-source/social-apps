.class public final LX/3BX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 528696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528697
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;)Landroid/graphics/RectF;
    .locals 6
    .param p0    # Lcom/facebook/graphql/model/GraphQLGeoRectangle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 528698
    if-nez p0, :cond_0

    .line 528699
    const/4 v0, 0x0

    .line 528700
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v4

    double-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLocation;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;
    .locals 6
    .param p0    # Lcom/facebook/graphql/model/GraphQLGeoRectangle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 528701
    invoke-static {p0}, LX/3BX;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;)Landroid/graphics/RectF;

    move-result-object v0

    .line 528702
    new-instance v1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-direct {v1, p1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    .line 528703
    if-eqz v0, :cond_0

    .line 528704
    invoke-virtual {v1, v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Landroid/graphics/RectF;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    .line 528705
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    goto :goto_0
.end method
