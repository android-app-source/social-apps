.class public LX/37N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 501001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501002
    return-void
.end method

.method public static a(LX/0QB;)LX/37N;
    .locals 3

    .prologue
    .line 501003
    const-class v1, LX/37N;

    monitor-enter v1

    .line 501004
    :try_start_0
    sget-object v0, LX/37N;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 501005
    sput-object v2, LX/37N;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 501006
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501007
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 501008
    new-instance v0, LX/37N;

    invoke-direct {v0}, LX/37N;-><init>()V

    .line 501009
    move-object v0, v0

    .line 501010
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 501011
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/37N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501012
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 501013
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
