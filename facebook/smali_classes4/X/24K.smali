.class public final LX/24K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24L;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/24L",
        "<",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;

.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:LX/1RN;

.field private final d:LX/1Qa;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;LX/1Qa;)V
    .locals 0

    .prologue
    .line 367092
    iput-object p1, p0, LX/24K;->a:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367093
    iput-object p2, p0, LX/24K;->d:LX/1Qa;

    .line 367094
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1RN;)V
    .locals 0

    .prologue
    .line 367095
    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 367096
    iput-object p1, p0, LX/24K;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 367097
    iput-object p2, p0, LX/24K;->c:LX/1RN;

    .line 367098
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0xe3d8577

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 367086
    iget-object v1, p0, LX/24K;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 367087
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 367088
    const/4 v1, 0x1

    move v1, v1

    .line 367089
    if-eqz v1, :cond_0

    .line 367090
    iget-object v1, p0, LX/24K;->a:Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;

    iget-object v1, v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerPromptIconPartDefinition;->e:LX/24B;

    iget-object v2, p0, LX/24K;->c:LX/1RN;

    iget-object v3, p0, LX/24K;->d:LX/1Qa;

    sget-object v4, LX/5S9;->ICON:LX/5S9;

    invoke-virtual {v1, v2, v3, v4}, LX/24B;->a(LX/1RN;LX/1Qa;LX/5S9;)V

    .line 367091
    :cond_0
    const v1, 0x5de539a7

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
