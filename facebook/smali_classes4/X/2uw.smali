.class public final LX/2uw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/32Z;


# instance fields
.field public final synthetic a:LX/1Hv;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/cache/disk/DiskStorage$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Hv;)V
    .locals 1

    .prologue
    .line 477121
    iput-object p1, p0, LX/2uw;->a:LX/1Hv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 477122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2uw;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 477120
    return-void
.end method

.method public final b(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 477115
    iget-object v0, p0, LX/2uw;->a:LX/1Hv;

    invoke-static {v0, p1}, LX/1Hv;->b(LX/1Hv;Ljava/io/File;)LX/1gF;

    move-result-object v0

    .line 477116
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/1gF;->a:LX/1gG;

    sget-object v2, LX/1gG;->CONTENT:LX/1gG;

    if-ne v1, v2, :cond_0

    .line 477117
    iget-object v1, p0, LX/2uw;->b:Ljava/util/List;

    new-instance v2, LX/35g;

    iget-object v0, v0, LX/1gF;->b:Ljava/lang/String;

    invoke-direct {v2, v0, p1}, LX/35g;-><init>(Ljava/lang/String;Ljava/io/File;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 477118
    :cond_0
    return-void
.end method

.method public final c(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 477119
    return-void
.end method
