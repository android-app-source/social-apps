.class public LX/27d;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/00H;

.field public final d:Landroid/content/Context;

.field public e:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Landroid/os/IBinder;

.field private g:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Landroid/content/ServiceConnection;

.field public i:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/ExecutorService;LX/00H;Landroid/content/Context;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 372976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372977
    iput-object v0, p0, LX/27d;->e:Lcom/google/common/util/concurrent/SettableFuture;

    .line 372978
    iput-object v0, p0, LX/27d;->f:Landroid/os/IBinder;

    .line 372979
    new-instance v0, LX/41Y;

    invoke-direct {v0, p0}, LX/41Y;-><init>(LX/27d;)V

    iput-object v0, p0, LX/27d;->g:LX/0TF;

    .line 372980
    new-instance v0, LX/41Z;

    invoke-direct {v0, p0}, LX/41Z;-><init>(LX/27d;)V

    iput-object v0, p0, LX/27d;->h:Landroid/content/ServiceConnection;

    .line 372981
    new-instance v0, Lcom/facebook/auth/login/KindleSsoUtil$3;

    invoke-direct {v0, p0}, Lcom/facebook/auth/login/KindleSsoUtil$3;-><init>(LX/27d;)V

    iput-object v0, p0, LX/27d;->i:Ljava/lang/Runnable;

    .line 372982
    iput-object p1, p0, LX/27d;->a:Ljava/util/concurrent/Executor;

    .line 372983
    iput-object p2, p0, LX/27d;->b:Ljava/util/concurrent/Executor;

    .line 372984
    iput-object p3, p0, LX/27d;->c:LX/00H;

    .line 372985
    iput-object p4, p0, LX/27d;->d:Landroid/content/Context;

    .line 372986
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372987
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/27d;->e:Lcom/google/common/util/concurrent/SettableFuture;

    if-nez v0, :cond_1

    .line 372988
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 372989
    iput-object v0, p0, LX/27d;->e:Lcom/google/common/util/concurrent/SettableFuture;

    .line 372990
    iget-object v1, p0, LX/27d;->e:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v2, p0, LX/27d;->g:LX/0TF;

    iget-object v3, p0, LX/27d;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 372991
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 372992
    const-string v2, "com.amazon.identity.snds"

    const-string v3, "com.amazon.identity.snds.fb.FacebookSSOService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372993
    iget-object v2, p0, LX/27d;->d:Landroid/content/Context;

    iget-object v3, p0, LX/27d;->h:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    const v5, 0x59e01eae

    invoke-static {v2, v1, v3, v4, v5}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 372994
    iget-object v1, p0, LX/27d;->e:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Could not connect to FacebookSSOService"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372995
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LX/27d;->e:Lcom/google/common/util/concurrent/SettableFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 372996
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
