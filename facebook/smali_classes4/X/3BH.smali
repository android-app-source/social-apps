.class public LX/3BH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Uh;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 527739
    iput-object p1, p0, LX/3BH;->a:LX/0Or;

    .line 527740
    iput-object p2, p0, LX/3BH;->b:LX/0Uh;

    .line 527741
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPlace;)Z
    .locals 1

    .prologue
    .line 527758
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->B()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->V()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 527759
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;LX/0Px;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 527751
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v0, v1

    .line 527752
    :goto_0
    return v0

    .line 527753
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 527754
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 527755
    const/4 v0, 0x1

    goto :goto_0

    .line 527756
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 527757
    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3BH;
    .locals 3

    .prologue
    .line 527760
    new-instance v1, LX/3BH;

    const/16 v0, 0x15e7

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v2, v0}, LX/3BH;-><init>(LX/0Or;LX/0Uh;)V

    .line 527761
    return-object v1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLPlace;)Z
    .locals 1

    .prologue
    .line 527750
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLPlace;)Z
    .locals 1

    .prologue
    .line 527749
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->P()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 527742
    iget-object v0, p0, LX/3BH;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v3

    invoke-static {v0, v3}, LX/3BH;->a(Ljava/lang/String;LX/0Px;)Z

    move-result v3

    .line 527743
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    .line 527744
    :goto_0
    iget-object v4, p0, LX/3BH;->b:LX/0Uh;

    const/16 v5, 0x31d

    invoke-virtual {v4, v5, v1}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 527745
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->P()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->x()Z

    move-result v4

    if-nez v4, :cond_1

    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v1, v2

    .line 527746
    :cond_1
    :goto_1
    return v1

    .line 527747
    :cond_2
    iget-object v0, p0, LX/3BH;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-static {v0, v4}, LX/3BH;->a(Ljava/lang/String;LX/0Px;)Z

    move-result v0

    goto :goto_0

    .line 527748
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->P()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->x()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    goto :goto_1
.end method
