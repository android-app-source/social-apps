.class public LX/2TJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2TJ;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0lC;

.field public final c:LX/0SG;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414011
    iput-object p1, p0, LX/2TJ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 414012
    iput-object p2, p0, LX/2TJ;->b:LX/0lC;

    .line 414013
    iput-object p3, p0, LX/2TJ;->c:LX/0SG;

    .line 414014
    return-void
.end method

.method public static a(LX/0QB;)LX/2TJ;
    .locals 6

    .prologue
    .line 414015
    sget-object v0, LX/2TJ;->d:LX/2TJ;

    if-nez v0, :cond_1

    .line 414016
    const-class v1, LX/2TJ;

    monitor-enter v1

    .line 414017
    :try_start_0
    sget-object v0, LX/2TJ;->d:LX/2TJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 414018
    if-eqz v2, :cond_0

    .line 414019
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 414020
    new-instance p0, LX/2TJ;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v3, v4, v5}, LX/2TJ;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/0SG;)V

    .line 414021
    move-object v0, p0

    .line 414022
    sput-object v0, LX/2TJ;->d:LX/2TJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414023
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 414024
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 414025
    :cond_1
    sget-object v0, LX/2TJ;->d:LX/2TJ;

    return-object v0

    .line 414026
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 414027
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
