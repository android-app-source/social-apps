.class public final LX/2ph;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/2pb;

.field public b:J


# direct methods
.method public constructor <init>(LX/2pb;)V
    .locals 0

    .prologue
    .line 468908
    iput-object p1, p0, LX/2ph;->a:LX/2pb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 468909
    iget-wide v0, p0, LX/2ph;->b:J

    cmp-long v0, v0, v8

    if-lez v0, :cond_0

    .line 468910
    iget-object v0, p0, LX/2ph;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/2ph;->b:J

    sub-long/2addr v0, v2

    .line 468911
    new-instance v2, LX/2ok;

    sget-object v3, LX/3np;->NEW_START_TIME:LX/3np;

    iget-object v3, v3, LX/3np;->value:Ljava/lang/String;

    const-string v4, "%.2f s"

    long-to-double v0, v0

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 468912
    iget-object v0, p0, LX/2ph;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    invoke-virtual {v0, v2}, LX/2oj;->a(LX/2ol;)V

    .line 468913
    :cond_0
    iput-wide v8, p0, LX/2ph;->b:J

    .line 468914
    return-void
.end method
