.class public final enum LX/2fy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2fy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2fy;

.field public static final enum DISMISSAL:LX/2fy;

.field public static final enum DISMISS_ACTION:LX/2fy;

.field public static final enum IMPRESSION:LX/2fy;

.field public static final enum PRIMARY_ACTION:LX/2fy;

.field public static final enum SECONDARY_ACTION:LX/2fy;


# instance fields
.field private final mReadableName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 447703
    new-instance v0, LX/2fy;

    const-string v1, "IMPRESSION"

    const-string v2, "Impression"

    invoke-direct {v0, v1, v3, v2}, LX/2fy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2fy;->IMPRESSION:LX/2fy;

    .line 447704
    new-instance v0, LX/2fy;

    const-string v1, "PRIMARY_ACTION"

    const-string v2, "Primary Action Clicks"

    invoke-direct {v0, v1, v4, v2}, LX/2fy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2fy;->PRIMARY_ACTION:LX/2fy;

    .line 447705
    new-instance v0, LX/2fy;

    const-string v1, "SECONDARY_ACTION"

    const-string v2, "Secondary Action Clicks"

    invoke-direct {v0, v1, v5, v2}, LX/2fy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2fy;->SECONDARY_ACTION:LX/2fy;

    .line 447706
    new-instance v0, LX/2fy;

    const-string v1, "DISMISS_ACTION"

    const-string v2, "Dismiss Action Clicks"

    invoke-direct {v0, v1, v6, v2}, LX/2fy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2fy;->DISMISS_ACTION:LX/2fy;

    .line 447707
    new-instance v0, LX/2fy;

    const-string v1, "DISMISSAL"

    const-string v2, "Dismissal"

    invoke-direct {v0, v1, v7, v2}, LX/2fy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2fy;->DISMISSAL:LX/2fy;

    .line 447708
    const/4 v0, 0x5

    new-array v0, v0, [LX/2fy;

    sget-object v1, LX/2fy;->IMPRESSION:LX/2fy;

    aput-object v1, v0, v3

    sget-object v1, LX/2fy;->PRIMARY_ACTION:LX/2fy;

    aput-object v1, v0, v4

    sget-object v1, LX/2fy;->SECONDARY_ACTION:LX/2fy;

    aput-object v1, v0, v5

    sget-object v1, LX/2fy;->DISMISS_ACTION:LX/2fy;

    aput-object v1, v0, v6

    sget-object v1, LX/2fy;->DISMISSAL:LX/2fy;

    aput-object v1, v0, v7

    sput-object v0, LX/2fy;->$VALUES:[LX/2fy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 447709
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 447710
    iput-object p3, p0, LX/2fy;->mReadableName:Ljava/lang/String;

    .line 447711
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2fy;
    .locals 1

    .prologue
    .line 447712
    const-class v0, LX/2fy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2fy;

    return-object v0
.end method

.method public static values()[LX/2fy;
    .locals 1

    .prologue
    .line 447713
    sget-object v0, LX/2fy;->$VALUES:[LX/2fy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2fy;

    return-object v0
.end method


# virtual methods
.method public final getReadableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 447714
    iget-object v0, p0, LX/2fy;->mReadableName:Ljava/lang/String;

    return-object v0
.end method
