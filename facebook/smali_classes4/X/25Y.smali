.class public abstract LX/25Y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field private b:Landroid/support/v7/widget/RecyclerView;

.field public c:LX/1OR;

.field private d:Z

.field public e:Z

.field private f:Landroid/view/View;

.field private final g:LX/25Z;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 369699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369700
    const/4 v0, -0x1

    iput v0, p0, LX/25Y;->a:I

    .line 369701
    new-instance v0, LX/25Z;

    invoke-direct {v0, v1, v1}, LX/25Z;-><init>(II)V

    iput-object v0, p0, LX/25Y;->g:LX/25Z;

    .line 369702
    return-void
.end method

.method private a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 369731
    iget-object v0, p0, LX/25Y;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0, p1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(LX/25Y;II)V
    .locals 0

    .prologue
    .line 369730
    invoke-static {p0, p1, p2}, LX/25Y;->a$redex0(LX/25Y;II)V

    return-void
.end method

.method public static a(Landroid/graphics/PointF;)V
    .locals 4

    .prologue
    .line 369726
    iget v0, p0, Landroid/graphics/PointF;->x:F

    iget v1, p0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Landroid/graphics/PointF;->y:F

    iget v2, p0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 369727
    iget v2, p0, Landroid/graphics/PointF;->x:F

    float-to-double v2, v2

    div-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Landroid/graphics/PointF;->x:F

    .line 369728
    iget v2, p0, Landroid/graphics/PointF;->y:F

    float-to-double v2, v2

    div-double v0, v2, v0

    double-to-float v0, v0

    iput v0, p0, Landroid/graphics/PointF;->y:F

    .line 369729
    return-void
.end method

.method private static a$redex0(LX/25Y;II)V
    .locals 3

    .prologue
    .line 369703
    iget-object v0, p0, LX/25Y;->b:Landroid/support/v7/widget/RecyclerView;

    .line 369704
    iget-boolean v1, p0, LX/25Y;->e:Z

    if-eqz v1, :cond_0

    iget v1, p0, LX/25Y;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-nez v0, :cond_1

    .line 369705
    :cond_0
    invoke-virtual {p0}, LX/25Y;->e()V

    .line 369706
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/25Y;->d:Z

    .line 369707
    iget-object v1, p0, LX/25Y;->f:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 369708
    iget-object v1, p0, LX/25Y;->f:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/view/View;)I

    move-result v1

    iget v2, p0, LX/25Y;->a:I

    if-ne v1, v2, :cond_4

    .line 369709
    iget-object v1, p0, LX/25Y;->f:Landroid/view/View;

    iget-object v2, p0, LX/25Y;->g:LX/25Z;

    invoke-virtual {p0, v1, v2}, LX/25Y;->a(Landroid/view/View;LX/25Z;)V

    .line 369710
    iget-object v1, p0, LX/25Y;->g:LX/25Z;

    .line 369711
    invoke-static {v1, v0}, LX/25Z;->a$redex0(LX/25Z;Landroid/support/v7/widget/RecyclerView;)V

    .line 369712
    invoke-virtual {p0}, LX/25Y;->e()V

    .line 369713
    :cond_2
    :goto_0
    iget-boolean v1, p0, LX/25Y;->e:Z

    if-eqz v1, :cond_3

    .line 369714
    iget-object v1, p0, LX/25Y;->g:LX/25Z;

    invoke-virtual {p0, p1, p2, v1}, LX/25Y;->a(IILX/25Z;)V

    .line 369715
    iget-object v1, p0, LX/25Y;->g:LX/25Z;

    invoke-virtual {v1}, LX/25Z;->a()Z

    move-result v1

    .line 369716
    iget-object v2, p0, LX/25Y;->g:LX/25Z;

    .line 369717
    invoke-static {v2, v0}, LX/25Z;->a$redex0(LX/25Z;Landroid/support/v7/widget/RecyclerView;)V

    .line 369718
    if-eqz v1, :cond_3

    .line 369719
    iget-boolean v1, p0, LX/25Y;->e:Z

    if-eqz v1, :cond_5

    .line 369720
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/25Y;->d:Z

    .line 369721
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->aa:Landroid/support/v7/widget/RecyclerView$ViewFlinger;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a()V

    .line 369722
    :cond_3
    :goto_1
    return-void

    .line 369723
    :cond_4
    const-string v1, "RecyclerView"

    const-string v2, "Passed over target position while smooth scrolling."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 369724
    const/4 v1, 0x0

    iput-object v1, p0, LX/25Y;->f:Landroid/view/View;

    goto :goto_0

    .line 369725
    :cond_5
    invoke-virtual {p0}, LX/25Y;->e()V

    goto :goto_1
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(IILX/25Z;)V
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;LX/1OR;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 369732
    iput-object p1, p0, LX/25Y;->b:Landroid/support/v7/widget/RecyclerView;

    .line 369733
    iput-object p2, p0, LX/25Y;->c:LX/1OR;

    .line 369734
    iget v0, p0, LX/25Y;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 369735
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid target position"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369736
    :cond_0
    iget-object v0, p0, LX/25Y;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    iget v1, p0, LX/25Y;->a:I

    .line 369737
    iput v1, v0, LX/1Ok;->f:I

    .line 369738
    iput-boolean v2, p0, LX/25Y;->e:Z

    .line 369739
    iput-boolean v2, p0, LX/25Y;->d:Z

    .line 369740
    iget v0, p0, LX/25Y;->a:I

    move v0, v0

    .line 369741
    invoke-direct {p0, v0}, LX/25Y;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/25Y;->f:Landroid/view/View;

    .line 369742
    iget-object v0, p0, LX/25Y;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->aa:Landroid/support/v7/widget/RecyclerView$ViewFlinger;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$ViewFlinger;->a()V

    .line 369743
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 369694
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/view/View;)I

    move-result v0

    .line 369695
    iget v1, p0, LX/25Y;->a:I

    move v1, v1

    .line 369696
    if-ne v0, v1, :cond_0

    .line 369697
    iput-object p1, p0, LX/25Y;->f:Landroid/view/View;

    .line 369698
    :cond_0
    return-void
.end method

.method public abstract a(Landroid/view/View;LX/25Z;)V
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 369692
    iput p1, p0, LX/25Y;->a:I

    .line 369693
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 369676
    iget-boolean v0, p0, LX/25Y;->e:Z

    if-nez v0, :cond_0

    .line 369677
    :goto_0
    return-void

    .line 369678
    :cond_0
    invoke-virtual {p0}, LX/25Y;->a()V

    .line 369679
    iget-object v0, p0, LX/25Y;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 369680
    iput v2, v0, LX/1Ok;->f:I

    .line 369681
    iput-object v1, p0, LX/25Y;->f:Landroid/view/View;

    .line 369682
    iput v2, p0, LX/25Y;->a:I

    .line 369683
    iput-boolean v3, p0, LX/25Y;->d:Z

    .line 369684
    iput-boolean v3, p0, LX/25Y;->e:Z

    .line 369685
    iget-object v0, p0, LX/25Y;->c:LX/1OR;

    invoke-static {v0, p0}, LX/1OR;->b(LX/1OR;LX/25Y;)V

    .line 369686
    iput-object v1, p0, LX/25Y;->c:LX/1OR;

    .line 369687
    iput-object v1, p0, LX/25Y;->b:Landroid/support/v7/widget/RecyclerView;

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 369691
    iget-boolean v0, p0, LX/25Y;->d:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 369690
    iget-boolean v0, p0, LX/25Y;->e:Z

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 369689
    iget v0, p0, LX/25Y;->a:I

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 369688
    iget-object v0, p0, LX/25Y;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->v()I

    move-result v0

    return v0
.end method
