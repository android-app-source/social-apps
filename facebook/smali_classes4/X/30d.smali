.class public LX/30d;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0if;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 484946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484947
    iput-object p1, p0, LX/30d;->a:LX/0Zb;

    .line 484948
    iput-object p2, p0, LX/30d;->b:LX/0if;

    .line 484949
    return-void
.end method

.method public static a(LX/0QB;)LX/30d;
    .locals 1

    .prologue
    .line 484931
    invoke-static {p0}, LX/30d;->b(LX/0QB;)LX/30d;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/30d;
    .locals 3

    .prologue
    .line 484950
    new-instance v2, LX/30d;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v1

    check-cast v1, LX/0if;

    invoke-direct {v2, v0, v1}, LX/30d;-><init>(LX/0Zb;LX/0if;)V

    .line 484951
    return-object v2
.end method

.method public static d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 484961
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "ccu_module"

    .line 484962
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 484963
    move-object v0, v0

    .line 484964
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 484952
    sget-object v0, LX/0ig;->I:LX/0ih;

    const/4 v1, 0x0

    .line 484953
    iput-boolean v1, v0, LX/0ih;->d:Z

    .line 484954
    move-object v0, v0

    .line 484955
    const/16 v1, 0xe10

    .line 484956
    iput v1, v0, LX/0ih;->c:I

    .line 484957
    iget-object v0, p0, LX/30d;->b:LX/0if;

    sget-object v1, LX/0ig;->I:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 484958
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 484959
    iget-object v0, p0, LX/30d;->a:LX/0Zb;

    sget-object v1, LX/Ejs;->CCU_CONTACTS_UPLOAD_FAILED:LX/Ejs;

    invoke-virtual {v1}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/30d;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "failure_reason"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 484960
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZJJILjava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 484942
    iget-object v0, p0, LX/30d;->a:LX/0Zb;

    sget-object v1, LX/Ejs;->CCU_CONTACTS_UPLOAD_FAILED:LX/Ejs;

    invoke-virtual {v1}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/30d;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "failure_reason"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "failure_message"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "full_upload"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "last_upload_success_time"

    invoke-virtual {v1, v2, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time_spent"

    invoke-virtual {v1, v2, p6, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "num_of_retries"

    invoke-virtual {v1, v2, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ccu_session_id"

    invoke-virtual {v1, v2, p9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 484943
    return-void
.end method

.method public final a(ZIIIIIIIJILjava/lang/String;)V
    .locals 5

    .prologue
    .line 484944
    iget-object v1, p0, LX/30d;->a:LX/0Zb;

    sget-object v2, LX/Ejs;->CCU_CONTACTS_UPLOAD_INFORMATION:LX/Ejs;

    invoke-virtual {v2}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/30d;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "upload_step"

    const-string v4, "close_session"

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "full_upload"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "total_batch_count"

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "contacts_upload_count"

    invoke-virtual {v2, v3, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "add_count"

    invoke-virtual {v2, v3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "remove_count"

    invoke-virtual {v2, v3, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "update_count"

    invoke-virtual {v2, v3, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "phonebook_size"

    invoke-virtual {v2, v3, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "max_contacts_to_upload"

    invoke-virtual {v2, v3, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "time_spent"

    invoke-virtual {v2, v3, p9, p10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "num_of_retries"

    move/from16 v0, p11

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "ccu_session_id"

    move-object/from16 v0, p12

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 484945
    return-void
.end method

.method public final a(ZIIIIIIJILjava/lang/String;)V
    .locals 4

    .prologue
    .line 484940
    iget-object v0, p0, LX/30d;->a:LX/0Zb;

    sget-object v1, LX/Ejs;->CCU_CONTACTS_UPLOAD_INFORMATION:LX/Ejs;

    invoke-virtual {v1}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/30d;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "upload_step"

    const-string v3, "batch_upload"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "full_upload"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "batch_index"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "batch_size"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "contacts_upload_count"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "add_count"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "remove_count"

    invoke-virtual {v1, v2, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "update_count"

    invoke-virtual {v1, v2, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time_spent"

    invoke-virtual {v1, v2, p8, p9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "num_of_retries"

    invoke-virtual {v1, v2, p10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ccu_session_id"

    invoke-virtual {v1, v2, p11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 484941
    return-void
.end method

.method public final a(ZILjava/lang/String;IIJ)V
    .locals 4

    .prologue
    .line 484938
    iget-object v0, p0, LX/30d;->a:LX/0Zb;

    sget-object v1, LX/Ejs;->CCU_CONTACTS_UPLOAD_INFORMATION:LX/Ejs;

    invoke-virtual {v1}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/30d;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "upload_step"

    const-string v3, "create_session"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "full_upload"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "num_of_retries"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "source"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "batch_size"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "contacts_upload_count"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "time_spent"

    invoke-virtual {v1, v2, p6, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 484939
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 484936
    iget-object v0, p0, LX/30d;->b:LX/0if;

    sget-object v1, LX/0ig;->I:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 484937
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 484934
    iget-object v0, p0, LX/30d;->b:LX/0if;

    sget-object v1, LX/0ig;->I:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 484935
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 484932
    iget-object v0, p0, LX/30d;->b:LX/0if;

    sget-object v1, LX/0ig;->I:LX/0ih;

    invoke-virtual {v0, v1, p1, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 484933
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 484929
    iget-object v0, p0, LX/30d;->b:LX/0if;

    sget-object v1, LX/0ig;->I:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 484930
    return-void
.end method
