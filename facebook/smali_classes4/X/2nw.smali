.class public final LX/2nw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0hn;


# direct methods
.method public constructor <init>(LX/0hn;)V
    .locals 0

    .prologue
    .line 465450
    iput-object p1, p0, LX/2nw;->a:LX/0hn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 465397
    sget-object v0, LX/0hn;->d:Ljava/lang/String;

    const-string v1, "Video Home badge subscription query failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 465398
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 465399
    check-cast p1, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;

    .line 465400
    if-nez p1, :cond_0

    .line 465401
    sget-object v0, LX/0hn;->d:Ljava/lang/String;

    const-string v1, "Video Home badge subscription succeeded but result was null."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 465402
    :goto_0
    return-void

    .line 465403
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->l()I

    move-result v1

    .line 465404
    invoke-virtual {p1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->a()I

    move-result v2

    .line 465405
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    invoke-virtual {p1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->k()I

    move-result v3

    .line 465406
    iput v3, v0, LX/0hn;->v:I

    .line 465407
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    sget-object v3, LX/095;->MQTT:LX/095;

    .line 465408
    iput-object v3, v0, LX/0hn;->w:LX/095;

    .line 465409
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    iget-object v0, v0, LX/0hn;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AW;

    sget-object v3, LX/095;->MQTT:LX/095;

    invoke-virtual {v0, v3, v2}, LX/3AW;->b(LX/095;I)V

    .line 465410
    new-instance v0, LX/EUb;

    invoke-direct {v0}, LX/EUb;-><init>()V

    .line 465411
    iput v2, v0, LX/EUb;->a:I

    .line 465412
    move-object v0, v0

    .line 465413
    invoke-virtual {p1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->k()I

    move-result v3

    .line 465414
    iput v3, v0, LX/EUb;->b:I

    .line 465415
    move-object v0, v0

    .line 465416
    iput v1, v0, LX/EUb;->c:I

    .line 465417
    move-object v0, v0

    .line 465418
    invoke-virtual {p1}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$VideoHomeBadgeSubModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    move-result-object v3

    .line 465419
    iput-object v3, v0, LX/EUb;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 465420
    move-object v0, v0

    .line 465421
    invoke-virtual {v0}, LX/EUb;->a()LX/EUc;

    move-result-object v3

    .line 465422
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    iget-object v0, v0, LX/0hn;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    sget-object v4, LX/1vy;->PREFETCH_CONTROLLER:LX/1vy;

    invoke-virtual {v0, v4}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 465423
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    invoke-static {v0}, LX/0hn;->m(LX/0hn;)V

    .line 465424
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    iget-object v0, v0, LX/0hn;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AX;

    .line 465425
    iput-object v3, v0, LX/3AX;->k:LX/EUc;

    .line 465426
    sget-object v1, LX/0JU;->MQTT:LX/0JU;

    invoke-static {v0, v1}, LX/3AX;->a(LX/3AX;LX/0JU;)Z

    .line 465427
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    iget-object v0, v0, LX/0hn;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AX;

    invoke-virtual {v0}, LX/3AX;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    invoke-virtual {v0}, LX/0hn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    invoke-static {v0}, LX/0hn;->p(LX/0hn;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 465428
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    iget-object v0, v0, LX/0hn;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hp;

    .line 465429
    iget v1, v3, LX/EUc;->a:I

    move v1, v1

    .line 465430
    invoke-virtual {v0, v1}, LX/0hp;->a(I)V

    .line 465431
    :cond_1
    :goto_1
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    invoke-virtual {v0, v3}, LX/0hn;->a(LX/EUc;)V

    goto/16 :goto_0

    .line 465432
    :cond_2
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    invoke-virtual {v0, v1, v2}, LX/0hn;->a(II)Z

    move-result v0

    .line 465433
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 465434
    iget v4, v3, LX/EUc;->c:I

    move v4, v4

    .line 465435
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v2, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v2, 0x2

    .line 465436
    iget-object v4, v3, LX/EUc;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    move-object v4, v4

    .line 465437
    aput-object v4, v1, v2

    const/4 v2, 0x3

    .line 465438
    iget v4, v3, LX/EUc;->a:I

    move v4, v4

    .line 465439
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v2, 0x4

    .line 465440
    iget v4, v3, LX/EUc;->b:I

    move v4, v4

    .line 465441
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    .line 465442
    iget-object v1, p0, LX/2nw;->a:LX/0hn;

    invoke-virtual {v1}, LX/0hn;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 465443
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    invoke-static {v0}, LX/0hn;->m(LX/0hn;)V

    goto :goto_1

    .line 465444
    :cond_3
    if-eqz v0, :cond_4

    .line 465445
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    sget-object v1, LX/0JU;->MQTT:LX/0JU;

    invoke-static {v0, v3, v1}, LX/0hn;->a$redex0(LX/0hn;LX/EUc;LX/0JU;)V

    goto :goto_1

    .line 465446
    :cond_4
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    invoke-virtual {v0, v3}, LX/0hn;->b(LX/EUc;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    invoke-static {v0}, LX/0hn;->p(LX/0hn;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 465447
    iget-object v0, p0, LX/2nw;->a:LX/0hn;

    iget-object v0, v0, LX/0hn;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hp;

    .line 465448
    iget v1, v3, LX/EUc;->a:I

    move v1, v1

    .line 465449
    invoke-virtual {v0, v1}, LX/0hp;->a(I)V

    goto :goto_1
.end method
