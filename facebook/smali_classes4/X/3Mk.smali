.class public LX/3Mk;
.super LX/3Ml;
.source ""


# instance fields
.field public final c:LX/3Lw;

.field public final d:LX/2Oq;

.field private final e:LX/3LP;

.field private final f:LX/3Lv;

.field private final g:LX/3Mn;

.field private final h:LX/3Mp;

.field private final i:LX/0lB;

.field public final j:LX/0Uh;

.field public final k:LX/0W9;

.field private final l:LX/3Mq;

.field private final m:LX/2RQ;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3A0;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field public p:Z

.field public q:Z

.field private r:Z

.field public s:Z

.field public t:Z

.field public u:Z


# direct methods
.method public constructor <init>(LX/0Zr;LX/3Lw;LX/2Oq;LX/3LP;LX/3Lv;LX/3Mn;LX/3Mp;LX/0lB;LX/0Uh;LX/0W9;LX/3Mq;LX/2RQ;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zr;",
            "LX/3Lw;",
            "LX/2Oq;",
            "LX/3LP;",
            "LX/3Lv;",
            "LX/3Mn;",
            "LX/3Mp;",
            "LX/0lB;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0W9;",
            "LX/3Mq;",
            "LX/2RQ;",
            "LX/0Ot",
            "<",
            "LX/3A0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 555193
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 555194
    iput-boolean v0, p0, LX/3Mk;->r:Z

    .line 555195
    iput-boolean v0, p0, LX/3Mk;->s:Z

    .line 555196
    iput-boolean v0, p0, LX/3Mk;->t:Z

    .line 555197
    iput-boolean v0, p0, LX/3Mk;->u:Z

    .line 555198
    iput-object p2, p0, LX/3Mk;->c:LX/3Lw;

    .line 555199
    iput-object p3, p0, LX/3Mk;->d:LX/2Oq;

    .line 555200
    iput-object p4, p0, LX/3Mk;->e:LX/3LP;

    .line 555201
    iput-object p5, p0, LX/3Mk;->f:LX/3Lv;

    .line 555202
    iput-object p7, p0, LX/3Mk;->h:LX/3Mp;

    .line 555203
    iput-object p8, p0, LX/3Mk;->i:LX/0lB;

    .line 555204
    iput-object p9, p0, LX/3Mk;->j:LX/0Uh;

    .line 555205
    iput-object p10, p0, LX/3Mk;->k:LX/0W9;

    .line 555206
    iput-object p11, p0, LX/3Mk;->l:LX/3Mq;

    .line 555207
    iput-object p12, p0, LX/3Mk;->m:LX/2RQ;

    .line 555208
    iput-object p13, p0, LX/3Mk;->n:LX/0Ot;

    .line 555209
    iput-object p6, p0, LX/3Mk;->g:LX/3Mn;

    .line 555210
    return-void
.end method

.method public static a(LX/0QB;)LX/3Mk;
    .locals 14

    .prologue
    .line 555211
    new-instance v0, LX/3Mk;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v1

    check-cast v1, LX/0Zr;

    invoke-static {p0}, LX/3Lw;->b(LX/0QB;)LX/3Lw;

    move-result-object v2

    check-cast v2, LX/3Lw;

    invoke-static {p0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v3

    check-cast v3, LX/2Oq;

    invoke-static {p0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v4

    check-cast v4, LX/3LP;

    invoke-static {p0}, LX/3Lv;->b(LX/0QB;)LX/3Lv;

    move-result-object v5

    check-cast v5, LX/3Lv;

    invoke-static {p0}, LX/3Mn;->b(LX/0QB;)LX/3Mn;

    move-result-object v6

    check-cast v6, LX/3Mn;

    invoke-static {p0}, LX/3Mp;->a(LX/0QB;)LX/3Mp;

    move-result-object v7

    check-cast v7, LX/3Mp;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lB;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v10

    check-cast v10, LX/0W9;

    invoke-static {p0}, LX/3Mq;->a(LX/0QB;)LX/3Mq;

    move-result-object v11

    check-cast v11, LX/3Mq;

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v12

    check-cast v12, LX/2RQ;

    const/16 v13, 0x110e

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, LX/3Mk;-><init>(LX/0Zr;LX/3Lw;LX/2Oq;LX/3LP;LX/3Lv;LX/3Mn;LX/3Mp;LX/0lB;LX/0Uh;LX/0W9;LX/3Mq;LX/2RQ;LX/0Ot;)V

    .line 555212
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 555213
    iget-object v0, p0, LX/3Mk;->m:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    .line 555214
    iput-object p1, v0, LX/2RR;->e:Ljava/lang/String;

    .line 555215
    move-object v0, v0

    .line 555216
    sget-object v2, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 555217
    iput-object v2, v0, LX/2RR;->b:Ljava/util/Collection;

    .line 555218
    move-object v2, v0

    .line 555219
    iget-boolean v0, p0, LX/3Mk;->o:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 555220
    :goto_0
    iput-boolean v0, v2, LX/2RR;->f:Z

    .line 555221
    move-object v0, v2

    .line 555222
    iget-boolean v2, p0, LX/3Mk;->p:Z

    .line 555223
    iput-boolean v2, v0, LX/2RR;->g:Z

    .line 555224
    move-object v0, v0

    .line 555225
    invoke-virtual {v0}, LX/2RR;->i()LX/2RR;

    move-result-object v0

    .line 555226
    iput-boolean v1, v0, LX/2RR;->l:Z

    .line 555227
    move-object v0, v0

    .line 555228
    const/16 v1, 0x1e

    .line 555229
    iput v1, v0, LX/2RR;->p:I

    .line 555230
    move-object v0, v0

    .line 555231
    iget-object v1, p0, LX/3Mk;->e:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 555232
    :goto_1
    :try_start_0
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 555233
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 555234
    iget-object v2, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 555235
    invoke-interface {p2, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 555236
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    .line 555237
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 555238
    :cond_1
    invoke-interface {v1}, LX/3On;->close()V

    .line 555239
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 555240
    iget-object v0, p0, LX/3Mk;->l:LX/3Mq;

    const-string v1, "timestamp_ms"

    const/4 v2, -0x1

    sget-object v3, LX/6ek;->INBOX:LX/6ek;

    .line 555241
    sget-object v4, LX/5e9;->ONE_TO_ONE:LX/5e9;

    invoke-static {v4, v3}, LX/3Mq;->a(LX/5e9;LX/6ek;)LX/0uw;

    move-result-object v4

    .line 555242
    invoke-static {v0, v4, v1, v2}, LX/3Mq;->a(LX/3Mq;LX/0ux;Ljava/lang/String;I)LX/6dO;

    move-result-object v4

    move-object v1, v4

    .line 555243
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 555244
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v1}, LX/6dO;->a()LX/6g6;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 555245
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 555246
    iget-object v3, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 555247
    iget-object v4, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    iget-object v5, p0, LX/3Mk;->i:LX/0lB;

    invoke-virtual {v4, v3, v5}, Lcom/facebook/messaging/model/threads/NicknamesMap;->a(Ljava/lang/String;LX/0lC;)Ljava/lang/String;

    move-result-object v4

    .line 555248
    if-eqz v4, :cond_0

    .line 555249
    iget-object v5, p0, LX/3Mk;->k:LX/0W9;

    invoke-virtual {v5}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v5

    .line 555250
    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    move v5, v5

    .line 555251
    if-eqz v5, :cond_0

    .line 555252
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v6, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 555253
    invoke-static {v3}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-interface {p3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 555254
    :catch_0
    move-exception v0

    .line 555255
    :try_start_1
    const-string v3, "orca:ContactPickerFriendFilter"

    const-string v4, "exception with filtering threads with nickname"

    invoke-static {v3, v4, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 555256
    invoke-virtual {v1}, LX/6dO;->close()V

    .line 555257
    :goto_1
    iget-object v0, p0, LX/3Mk;->m:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    invoke-static {v2}, Lcom/facebook/user/model/UserKey;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    .line 555258
    iput-object v1, v0, LX/2RR;->d:Ljava/util/Collection;

    .line 555259
    move-object v0, v0

    .line 555260
    sget-object v1, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 555261
    iput-object v1, v0, LX/2RR;->b:Ljava/util/Collection;

    .line 555262
    move-object v1, v0

    .line 555263
    iget-boolean v0, p0, LX/3Mk;->o:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 555264
    :goto_2
    iput-boolean v0, v1, LX/2RR;->f:Z

    .line 555265
    move-object v0, v1

    .line 555266
    iget-boolean v1, p0, LX/3Mk;->p:Z

    .line 555267
    iput-boolean v1, v0, LX/2RR;->g:Z

    .line 555268
    move-object v0, v0

    .line 555269
    invoke-virtual {v0}, LX/2RR;->i()LX/2RR;

    move-result-object v0

    const/16 v1, 0x1e

    .line 555270
    iput v1, v0, LX/2RR;->p:I

    .line 555271
    move-object v0, v0

    .line 555272
    iget-object v1, p0, LX/3Mk;->e:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 555273
    :goto_3
    :try_start_2
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 555274
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 555275
    iget-object v2, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 555276
    invoke-interface {p2, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 555277
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    .line 555278
    :cond_1
    invoke-virtual {v1}, LX/6dO;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    invoke-virtual {v1}, LX/6dO;->close()V

    throw v0

    .line 555279
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 555280
    :cond_3
    invoke-interface {v1}, LX/3On;->close()V

    .line 555281
    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/3OQ;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 555282
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 555283
    iget-object v2, v0, Lcom/facebook/user/model/User;->ai:Lcom/facebook/user/model/UserIdentifier;

    move-object v2, v2

    .line 555284
    if-nez v2, :cond_2

    .line 555285
    :cond_0
    :goto_1
    goto :goto_0

    .line 555286
    :cond_1
    return-void

    .line 555287
    :cond_2
    invoke-virtual {p0, v2}, LX/3Ml;->a(Lcom/facebook/user/model/UserIdentifier;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 555288
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 555289
    instance-of v3, v2, Lcom/facebook/user/model/UserSmsIdentifier;

    if-eqz v3, :cond_a

    .line 555290
    check-cast v2, Lcom/facebook/user/model/UserSmsIdentifier;

    .line 555291
    iget-object v3, v2, Lcom/facebook/user/model/UserSmsIdentifier;->b:Ljava/lang/String;

    move-object v3, v3

    .line 555292
    invoke-static {v3}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    move v3, v4

    .line 555293
    :goto_2
    move v2, v3

    .line 555294
    if-nez v2, :cond_0

    .line 555295
    iget-object v2, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v2, v0}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v3

    .line 555296
    if-eqz v3, :cond_0

    .line 555297
    instance-of v2, v3, LX/3OO;

    if-eqz v2, :cond_3

    .line 555298
    iget-object v2, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 555299
    invoke-interface {p3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v2, v3

    .line 555300
    check-cast v2, LX/3OO;

    .line 555301
    iget-object v4, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 555302
    invoke-interface {p3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 555303
    iput-object v4, v2, LX/3OO;->L:Ljava/lang/String;

    .line 555304
    :cond_3
    instance-of v2, v3, LX/3OO;

    if-eqz v2, :cond_4

    iget-boolean v2, p0, LX/3Mk;->u:Z

    if-eqz v2, :cond_4

    move-object v2, v3

    .line 555305
    check-cast v2, LX/3OO;

    .line 555306
    iget-object v4, p0, LX/3Mk;->n:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3A0;

    .line 555307
    iget-object v5, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v5

    .line 555308
    new-instance v6, LX/EFb;

    invoke-direct {v6, v4, v5}, LX/EFb;-><init>(LX/3A0;Ljava/lang/String;)V

    move-object v4, v6

    .line 555309
    iput-object v4, v2, LX/3OO;->N:LX/EFb;

    .line 555310
    :cond_4
    iget-boolean v2, p0, LX/3Mk;->s:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, LX/3Mk;->t:Z

    if-eqz v2, :cond_7

    :cond_5
    instance-of v2, v3, LX/3OO;

    if-eqz v2, :cond_7

    .line 555311
    check-cast v3, LX/3OO;

    .line 555312
    iget-boolean v2, p0, LX/3Mk;->s:Z

    .line 555313
    iput-boolean v2, v3, LX/3OO;->C:Z

    .line 555314
    const-string v2, "top_level_call_button"

    .line 555315
    iput-object v2, v3, LX/3OO;->F:Ljava/lang/String;

    .line 555316
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->ax()Z

    move-result v2

    if-nez v2, :cond_6

    .line 555317
    iget-boolean v2, p0, LX/3Mk;->t:Z

    .line 555318
    iput-boolean v2, v3, LX/3OO;->D:Z

    .line 555319
    const-string v2, "top_level_call_button_video"

    .line 555320
    iput-object v2, v3, LX/3OO;->G:Ljava/lang/String;

    .line 555321
    :cond_6
    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 555322
    :cond_7
    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 555323
    :cond_8
    iget-object v3, p0, LX/3Ml;->a:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/UserIdentifier;

    .line 555324
    instance-of v3, v3, Lcom/facebook/user/model/UserFbidIdentifier;

    if-eqz v3, :cond_9

    move v3, v5

    .line 555325
    goto/16 :goto_2

    .line 555326
    :cond_a
    instance-of v3, v2, Lcom/facebook/user/model/UserFbidIdentifier;

    if-eqz v3, :cond_d

    .line 555327
    iget-object v3, p0, LX/3Ml;->a:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_b
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/UserIdentifier;

    .line 555328
    instance-of p1, v3, Lcom/facebook/user/model/UserSmsIdentifier;

    if-eqz p1, :cond_b

    .line 555329
    check-cast v3, Lcom/facebook/user/model/UserSmsIdentifier;

    .line 555330
    iget-object v6, v3, Lcom/facebook/user/model/UserSmsIdentifier;->b:Ljava/lang/String;

    move-object v3, v6

    .line 555331
    invoke-static {v3}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    move v3, v4

    .line 555332
    goto/16 :goto_2

    :cond_c
    move v3, v5

    .line 555333
    goto/16 :goto_2

    :cond_d
    move v3, v4

    .line 555334
    goto/16 :goto_2
.end method

.method public static b(LX/0QB;)LX/3Mk;
    .locals 1

    .prologue
    .line 555335
    invoke-static {p0}, LX/3Mk;->a(LX/0QB;)LX/3Mk;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 12
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x0

    .line 555336
    const-string v0, "ContactPickerFriendFilter.Filtering"

    const v1, 0x7e8dea8d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 555337
    new-instance v7, LX/39y;

    invoke-direct {v7}, LX/39y;-><init>()V

    .line 555338
    if-eqz p1, :cond_3

    :try_start_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 555339
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_7

    .line 555340
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 555341
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v10

    .line 555342
    invoke-direct {p0, v1, v0}, LX/3Mk;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 555343
    const/4 v2, 0x0

    .line 555344
    iget-object v3, p0, LX/3Mk;->j:LX/0Uh;

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/3Mk;->j:LX/0Uh;

    const/16 v4, 0x1c7

    invoke-virtual {v3, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v2, v2

    .line 555345
    if-eqz v2, :cond_1

    .line 555346
    invoke-direct {p0, v1, v0, v10}, LX/3Mk;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    .line 555347
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v11

    .line 555348
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 555349
    iget-boolean v0, p0, LX/3Mk;->q:Z

    if-nez v0, :cond_9

    .line 555350
    invoke-static {v1}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v4

    .line 555351
    iget-object v0, p0, LX/3Mk;->f:LX/3Lv;

    const/16 v2, 0x1e

    invoke-virtual {p0}, LX/3Mk;->c()Z

    move-result v3

    invoke-static {v1}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v5

    sget-object v6, LX/3Oo;->SEARCH:LX/3Oo;

    invoke-virtual/range {v0 .. v6}, LX/3Lv;->a(Ljava/lang/String;IZZZLX/3Oo;)LX/0Px;

    move-result-object v2

    .line 555352
    iget-object v0, p0, LX/3Mk;->c:LX/3Lw;

    invoke-virtual {v0}, LX/3Lw;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/3Mk;->c:LX/3Lw;

    .line 555353
    iget-object v3, v0, LX/3Lw;->a:LX/0Uh;

    const/16 v5, 0x1cf

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v0, v3

    .line 555354
    if-eqz v0, :cond_a

    iget-object v0, p0, LX/3Mk;->d:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->a()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 555355
    if-eqz v0, :cond_4

    .line 555356
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_2
    if-ge v8, v3, :cond_5

    invoke-virtual {v2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 555357
    iget-object v5, p0, LX/3Mk;->h:LX/3Mp;

    invoke-virtual {v5, v0}, LX/3Mp;->a(Lcom/facebook/user/model/User;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 555358
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 555359
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 555360
    :cond_3
    const-string v1, ""

    goto/16 :goto_0

    .line 555361
    :cond_4
    invoke-interface {v11, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 555362
    :cond_5
    iget-boolean v0, p0, LX/3Mk;->r:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/3Mk;->d:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 555363
    iget-object v0, p0, LX/3Mk;->g:LX/3Mn;

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, LX/3Mn;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 555364
    :goto_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 555365
    invoke-direct {p0, v11, v1, v10}, LX/3Mk;->a(Ljava/util/List;Ljava/util/List;Ljava/util/Map;)V

    .line 555366
    iget-boolean v2, p0, LX/3Mk;->r:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/3Mk;->d:LX/2Oq;

    invoke-virtual {v2}, LX/2Oq;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 555367
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 555368
    iget-object v5, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v5, v2}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 555369
    :cond_6
    new-instance v0, LX/DAn;

    invoke-direct {v0, v11, v4}, LX/DAn;-><init>(Ljava/util/Collection;Z)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 555370
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 555371
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 555372
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    .line 555373
    iput-object v0, v7, LX/39y;->a:Ljava/lang/Object;

    .line 555374
    iget v1, v0, LX/3Og;->d:I

    move v0, v1

    .line 555375
    iput v0, v7, LX/39y;->b:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 555376
    :goto_5
    const v0, -0x55f8d389

    invoke-static {v0}, LX/02m;->a(I)V

    .line 555377
    const-string v0, "orca:ContactPickerFriendFilter"

    invoke-static {v0}, LX/0PR;->c(Ljava/lang/String;)V

    move-object v0, v7

    .line 555378
    :goto_6
    return-object v0

    .line 555379
    :cond_7
    :try_start_1
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v7, LX/39y;->a:Ljava/lang/Object;

    .line 555380
    const/4 v0, -0x1

    iput v0, v7, LX/39y;->b:I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 555381
    :catch_0
    move-exception v0

    .line 555382
    :try_start_2
    const-string v1, "orca:ContactPickerFriendFilter"

    const-string v2, "Exception while filtering"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 555383
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v7, LX/39y;->a:Ljava/lang/Object;

    .line 555384
    const/4 v0, -0x1

    iput v0, v7, LX/39y;->b:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 555385
    const v0, -0x6dfba02c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 555386
    const-string v0, "orca:ContactPickerFriendFilter"

    invoke-static {v0}, LX/0PR;->c(Ljava/lang/String;)V

    move-object v0, v7

    .line 555387
    goto :goto_6

    .line 555388
    :catchall_0
    move-exception v0

    const v1, 0x15b1b84d

    invoke-static {v1}, LX/02m;->a(I)V

    .line 555389
    const-string v1, "orca:ContactPickerFriendFilter"

    invoke-static {v1}, LX/0PR;->c(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, v9

    goto/16 :goto_3

    :cond_9
    move v4, v8

    move-object v0, v9

    goto/16 :goto_3

    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 555390
    iput-boolean p1, p0, LX/3Mk;->o:Z

    .line 555391
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 555392
    iget-boolean v0, p0, LX/3Mk;->s:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 555393
    iput-boolean p1, p0, LX/3Mk;->q:Z

    .line 555394
    return-void
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 555395
    iput-boolean p1, p0, LX/3Mk;->r:Z

    .line 555396
    return-void
.end method
