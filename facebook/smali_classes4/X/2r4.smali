.class public final LX/2r4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/2aH;


# direct methods
.method public constructor <init>(LX/2aH;)V
    .locals 0

    .prologue
    .line 471661
    iput-object p1, p0, LX/2r4;->a:LX/2aH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 471662
    iget-object v0, p0, LX/2r4;->a:LX/2aH;

    iget-boolean v0, v0, LX/2aH;->i:Z

    if-eqz v0, :cond_0

    .line 471663
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 471664
    :goto_0
    return-object v0

    .line 471665
    :cond_0
    iget-object v0, p0, LX/2r4;->a:LX/2aH;

    iget-object v0, v0, LX/2aH;->a:LX/0yP;

    sget-object v1, LX/0yi;->NORMAL:LX/0yi;

    sget-object v2, LX/32P;->LOGIN:LX/32P;

    invoke-virtual {v0, v1, v2}, LX/0yP;->b(LX/0yi;LX/32P;)Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;

    move-result-object v0

    .line 471666
    iget-object v1, p0, LX/2r4;->a:LX/2aH;

    iget-object v1, v1, LX/2aH;->d:LX/2Zp;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetchZeroToken"

    .line 471667
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 471668
    move-object v0, v0

    .line 471669
    invoke-virtual {v0, v8}, LX/2Vk;->a(Z)LX/2Vk;

    move-result-object v0

    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 471670
    iget-object v1, p0, LX/2r4;->a:LX/2aH;

    iget-object v1, v1, LX/2aH;->a:LX/0yP;

    sget-object v2, LX/0yi;->DIALTONE:LX/0yi;

    sget-object v3, LX/32P;->LOGIN:LX/32P;

    invoke-virtual {v1, v2, v3}, LX/0yP;->b(LX/0yi;LX/32P;)Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;

    move-result-object v1

    .line 471671
    iget-object v2, p0, LX/2r4;->a:LX/2aH;

    iget-object v2, v2, LX/2aH;->d:LX/2Zp;

    invoke-static {v2, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    const-string v2, "fetchZeroTokenForDialtone"

    .line 471672
    iput-object v2, v1, LX/2Vk;->c:Ljava/lang/String;

    .line 471673
    move-object v1, v1

    .line 471674
    invoke-virtual {v1, v8}, LX/2Vk;->a(Z)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    .line 471675
    new-instance v2, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;

    iget-object v3, p0, LX/2r4;->a:LX/2aH;

    iget-object v3, v3, LX/2aH;->c:LX/1pA;

    invoke-virtual {v3}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v3

    iget-object v4, p0, LX/2r4;->a:LX/2aH;

    iget-object v4, v4, LX/2aH;->c:LX/1pA;

    invoke-virtual {v4}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/2r4;->a:LX/2aH;

    iget-object v5, v5, LX/2aH;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/26p;->f:LX/0Tn;

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 471676
    iget-object v3, p0, LX/2r4;->a:LX/2aH;

    iget-object v3, v3, LX/2aH;->e:LX/2cH;

    invoke-static {v3, v2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v2

    const-string v3, "fetchZeroHeaderRequest"

    .line 471677
    iput-object v3, v2, LX/2Vk;->c:Ljava/lang/String;

    .line 471678
    move-object v2, v2

    .line 471679
    invoke-virtual {v2, v8}, LX/2Vk;->a(Z)LX/2Vk;

    move-result-object v2

    invoke-virtual {v2}, LX/2Vk;->a()LX/2Vj;

    move-result-object v2

    .line 471680
    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 471681
    const-string v0, "fetchZeroToken"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/token/ZeroToken;

    .line 471682
    if-eqz v0, :cond_0

    .line 471683
    iget-object v1, p0, LX/2r4;->a:LX/2aH;

    iget-object v1, v1, LX/2aH;->a:LX/0yP;

    sget-object v2, LX/0yi;->NORMAL:LX/0yi;

    .line 471684
    invoke-virtual {v1, v0, v2}, LX/0yP;->b(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V

    .line 471685
    :cond_0
    const-string v0, "fetchZeroTokenForDialtone"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/token/ZeroToken;

    .line 471686
    if-eqz v0, :cond_1

    .line 471687
    iget-object v1, p0, LX/2r4;->a:LX/2aH;

    iget-object v1, v1, LX/2aH;->a:LX/0yP;

    sget-object v2, LX/0yi;->DIALTONE:LX/0yi;

    .line 471688
    invoke-virtual {v1, v0, v2}, LX/0yP;->b(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V

    .line 471689
    :cond_1
    iget-object v0, p0, LX/2r4;->a:LX/2aH;

    iget-object v0, v0, LX/2aH;->g:LX/2cI;

    .line 471690
    iget-object v1, v0, LX/2cI;->a:LX/0ad;

    sget-short v2, LX/2Ez;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 471691
    if-eqz v0, :cond_2

    .line 471692
    iget-object v0, p0, LX/2r4;->a:LX/2aH;

    iget-object v0, v0, LX/2aH;->h:LX/2r3;

    const-string v1, "app_install"

    const-string v2, "fb4a_login"

    .line 471693
    new-instance v3, LX/7Xi;

    invoke-direct {v3, v0}, LX/7Xi;-><init>(LX/2r3;)V

    move-object v3, v3

    .line 471694
    new-instance v4, LX/7X6;

    invoke-direct {v4}, LX/7X6;-><init>()V

    move-object v4, v4

    .line 471695
    const-string v5, "incentive_type"

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "source"

    invoke-virtual {v5, v6, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 471696
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 471697
    iget-object v5, v0, LX/2r3;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 471698
    if-nez v4, :cond_4

    .line 471699
    :cond_2
    :goto_0
    const-string v0, "fetchZeroHeaderRequest"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;

    .line 471700
    if-eqz v0, :cond_3

    .line 471701
    iget-object v1, p0, LX/2r4;->a:LX/2aH;

    iget-object v1, v1, LX/2aH;->b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    .line 471702
    iget-object v2, v1, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0df;->t:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;->f()I

    move-result v4

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 471703
    const-string v2, "enabled"

    invoke-virtual {v0}, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 471704
    invoke-static {v1, v0}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->b(Lcom/facebook/zero/service/ZeroHeaderRequestManager;Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 471705
    :cond_3
    return-void

    .line 471706
    :cond_4
    new-instance v5, LX/7Xh;

    invoke-direct {v5, v0}, LX/7Xh;-><init>(LX/2r3;)V

    iget-object v6, v0, LX/2r3;->b:LX/0TD;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 471707
    iget-object v5, v0, LX/2r3;->b:LX/0TD;

    invoke-static {v4, v3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
