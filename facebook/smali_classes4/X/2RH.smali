.class public LX/2RH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/platform/server/protocol/UploadStagingResourcePhotoMethod$Params;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 409722
    const-class v0, LX/2RH;

    sput-object v0, LX/2RH;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409724
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 409725
    check-cast p1, Lcom/facebook/platform/server/protocol/UploadStagingResourcePhotoMethod$Params;

    .line 409726
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409727
    iget-object v0, p1, Lcom/facebook/platform/server/protocol/UploadStagingResourcePhotoMethod$Params;->b:Landroid/graphics/Bitmap;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409728
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 409729
    iget-object v1, p1, Lcom/facebook/platform/server/protocol/UploadStagingResourcePhotoMethod$Params;->b:Landroid/graphics/Bitmap;

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 409730
    new-instance v1, LX/4cq;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const-string v2, "image/jpeg"

    iget-object v3, p1, Lcom/facebook/platform/server/protocol/UploadStagingResourcePhotoMethod$Params;->a:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3}, LX/4cq;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    .line 409731
    new-instance v0, LX/4cQ;

    iget-object v2, p1, Lcom/facebook/platform/server/protocol/UploadStagingResourcePhotoMethod$Params;->a:Ljava/lang/String;

    invoke-direct {v0, v2, v1}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 409732
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 409733
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "uploadStagingResourcePhoto"

    .line 409734
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 409735
    move-object v2, v2

    .line 409736
    const-string v3, "POST"

    .line 409737
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 409738
    move-object v2, v2

    .line 409739
    const-string v3, "me/staging_resources"

    .line 409740
    iput-object v3, v2, LX/14O;->d:Ljava/lang/String;

    .line 409741
    move-object v2, v2

    .line 409742
    sget-object v3, LX/14S;->JSON:LX/14S;

    .line 409743
    iput-object v3, v2, LX/14O;->k:LX/14S;

    .line 409744
    move-object v2, v2

    .line 409745
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 409746
    iput-object v0, v2, LX/14O;->l:Ljava/util/List;

    .line 409747
    move-object v0, v2

    .line 409748
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 409749
    move-object v0, v0

    .line 409750
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 409751
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
