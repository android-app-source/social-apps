.class public LX/325;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/325;


# instance fields
.field public final a:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 489395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 489396
    iput-object p1, p0, LX/325;->a:LX/03V;

    .line 489397
    return-void
.end method

.method public static a(LX/0QB;)LX/325;
    .locals 4

    .prologue
    .line 489410
    sget-object v0, LX/325;->b:LX/325;

    if-nez v0, :cond_1

    .line 489411
    const-class v1, LX/325;

    monitor-enter v1

    .line 489412
    :try_start_0
    sget-object v0, LX/325;->b:LX/325;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 489413
    if-eqz v2, :cond_0

    .line 489414
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 489415
    new-instance p0, LX/325;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/325;-><init>(LX/03V;)V

    .line 489416
    move-object v0, p0

    .line 489417
    sput-object v0, LX/325;->b:LX/325;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 489418
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 489419
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 489420
    :cond_1
    sget-object v0, LX/325;->b:LX/325;

    return-object v0

    .line 489421
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 489422
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static e(Lcom/facebook/api/feed/FetchFeedResult;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 489406
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 489407
    iget-wide v4, p0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v4

    .line 489408
    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 489409
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->format3339(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/api/feed/FetchFeedResult;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 3

    .prologue
    .line 489404
    iget-object v0, p0, LX/325;->a:LX/03V;

    const-string v1, "LastNewerLoadSuccess"

    invoke-static {p1}, LX/325;->e(Lcom/facebook/api/feed/FetchFeedResult;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 489405
    return-void
.end method

.method public final b(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 3

    .prologue
    .line 489402
    iget-object v0, p0, LX/325;->a:LX/03V;

    const-string v1, "LastNewerLoadEmpty"

    invoke-static {p1}, LX/325;->e(Lcom/facebook/api/feed/FetchFeedResult;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 489403
    return-void
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 3

    .prologue
    .line 489400
    iget-object v0, p0, LX/325;->a:LX/03V;

    const-string v1, "LastOlderLoadSuccess"

    invoke-static {p1}, LX/325;->e(Lcom/facebook/api/feed/FetchFeedResult;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 489401
    return-void
.end method

.method public final d(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 3

    .prologue
    .line 489398
    iget-object v0, p0, LX/325;->a:LX/03V;

    const-string v1, "LastOlderLoadEmpty"

    invoke-static {p1}, LX/325;->e(Lcom/facebook/api/feed/FetchFeedResult;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 489399
    return-void
.end method
