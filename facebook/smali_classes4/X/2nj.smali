.class public LX/2nj;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/2nj;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:LX/2nk;

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 465063
    new-instance v0, LX/2nj;

    const/4 v1, 0x0

    sget-object v2, LX/2nk;->INITIAL:LX/2nk;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, LX/2nj;-><init>(Ljava/lang/String;LX/2nk;Z)V

    sput-object v0, LX/2nj;->a:LX/2nj;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/2nk;Z)V
    .locals 0

    .prologue
    .line 465058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465059
    iput-object p1, p0, LX/2nj;->b:Ljava/lang/String;

    .line 465060
    iput-object p2, p0, LX/2nj;->c:LX/2nk;

    .line 465061
    iput-boolean p3, p0, LX/2nj;->d:Z

    .line 465062
    return-void
.end method


# virtual methods
.method public final b()LX/2nk;
    .locals 1

    .prologue
    .line 465064
    iget-object v0, p0, LX/2nj;->c:LX/2nk;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 465053
    if-ne p0, p1, :cond_1

    .line 465054
    :cond_0
    :goto_0
    return v0

    .line 465055
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 465056
    :cond_3
    check-cast p1, LX/2nj;

    .line 465057
    iget-boolean v2, p0, LX/2nj;->d:Z

    iget-boolean v3, p1, LX/2nj;->d:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, LX/2nj;->b:Ljava/lang/String;

    iget-object v3, p1, LX/2nj;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/2nj;->c:LX/2nk;

    iget-object v3, p1, LX/2nj;->c:LX/2nk;

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 465052
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/2nj;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/2nj;->c:LX/2nk;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, LX/2nj;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 465051
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mCursor"

    iget-object v2, p0, LX/2nj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mLocationType"

    iget-object v2, p0, LX/2nj;->c:LX/2nk;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mHasAnotherPage"

    iget-boolean v2, p0, LX/2nj;->d:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
