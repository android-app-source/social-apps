.class public LX/3Ai;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem;


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field public j:Z

.field public k:Z

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Landroid/view/Menu;

.field public n:Landroid/view/SubMenu;

.field private o:Landroid/view/MenuItem$OnMenuItemClickListener;


# direct methods
.method public constructor <init>(Landroid/view/Menu;III)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 525968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525969
    iput-boolean v0, p0, LX/3Ai;->f:Z

    .line 525970
    iput-boolean v0, p0, LX/3Ai;->g:Z

    .line 525971
    iput-boolean v1, p0, LX/3Ai;->h:Z

    .line 525972
    iput-boolean v1, p0, LX/3Ai;->i:Z

    .line 525973
    iput-boolean v0, p0, LX/3Ai;->j:Z

    .line 525974
    iput-boolean v0, p0, LX/3Ai;->k:Z

    .line 525975
    iput-object p1, p0, LX/3Ai;->m:Landroid/view/Menu;

    .line 525976
    iput p2, p0, LX/3Ai;->a:I

    .line 525977
    iput p3, p0, LX/3Ai;->b:I

    .line 525978
    invoke-virtual {p0, p4}, LX/3Ai;->setTitle(I)Landroid/view/MenuItem;

    .line 525979
    return-void
.end method

.method public constructor <init>(Landroid/view/Menu;IILjava/lang/CharSequence;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 525980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525981
    iput-boolean v0, p0, LX/3Ai;->f:Z

    .line 525982
    iput-boolean v0, p0, LX/3Ai;->g:Z

    .line 525983
    iput-boolean v1, p0, LX/3Ai;->h:Z

    .line 525984
    iput-boolean v1, p0, LX/3Ai;->i:Z

    .line 525985
    iput-boolean v0, p0, LX/3Ai;->j:Z

    .line 525986
    iput-boolean v0, p0, LX/3Ai;->k:Z

    .line 525987
    iput-object p1, p0, LX/3Ai;->m:Landroid/view/Menu;

    .line 525988
    iput p2, p0, LX/3Ai;->a:I

    .line 525989
    iput p3, p0, LX/3Ai;->b:I

    .line 525990
    invoke-virtual {p0, p4}, LX/3Ai;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 525991
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 525992
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    instance-of v0, v0, LX/5OG;

    if-eqz v0, :cond_0

    .line 525993
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/5OG;

    .line 525994
    iget-object v1, v0, LX/5OG;->c:Landroid/content/Context;

    move-object v0, v1

    .line 525995
    :goto_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0

    .line 525996
    :cond_0
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/34c;

    .line 525997
    iget-object v1, v0, LX/34c;->c:Landroid/content/Context;

    move-object v0, v1

    .line 525998
    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525999
    iput-object p1, p0, LX/3Ai;->d:Ljava/lang/CharSequence;

    .line 526000
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    instance-of v0, v0, LX/34d;

    if-eqz v0, :cond_0

    .line 526001
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/34d;

    invoke-interface {v0}, LX/34d;->f()V

    .line 526002
    :cond_0
    return-object p0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 526003
    iget-object v0, p0, LX/3Ai;->o:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v0, :cond_0

    .line 526004
    iget-object v0, p0, LX/3Ai;->o:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v0, p0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v0

    .line 526005
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 526006
    iput-object p1, p0, LX/3Ai;->e:Ljava/lang/CharSequence;

    .line 526007
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    instance-of v0, v0, LX/34d;

    if-eqz v0, :cond_0

    .line 526008
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/34d;

    invoke-interface {v0}, LX/34d;->f()V

    .line 526009
    :cond_0
    return-object p0
.end method

.method public final collapseActionView()Z
    .locals 1

    .prologue
    .line 526010
    const/4 v0, 0x0

    return v0
.end method

.method public final expandActionView()Z
    .locals 1

    .prologue
    .line 526011
    const/4 v0, 0x0

    return v0
.end method

.method public final getActionProvider()Landroid/view/ActionProvider;
    .locals 1

    .prologue
    .line 526012
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 526013
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 526014
    const/4 v0, 0x0

    return v0
.end method

.method public final getGroupId()I
    .locals 1

    .prologue
    .line 526025
    const/4 v0, 0x0

    return v0
.end method

.method public final getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 526015
    iget-object v0, p0, LX/3Ai;->l:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 526016
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId()I
    .locals 1

    .prologue
    .line 526017
    iget v0, p0, LX/3Ai;->a:I

    return v0
.end method

.method public final getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 526018
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getNumericShortcut()C
    .locals 1

    .prologue
    .line 526019
    const/4 v0, 0x0

    return v0
.end method

.method public final getOrder()I
    .locals 1

    .prologue
    .line 526020
    iget v0, p0, LX/3Ai;->b:I

    return v0
.end method

.method public final getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 526021
    iget-object v0, p0, LX/3Ai;->n:Landroid/view/SubMenu;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 526022
    iget-object v0, p0, LX/3Ai;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 526023
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasSubMenu()Z
    .locals 1

    .prologue
    .line 526024
    iget-object v0, p0, LX/3Ai;->n:Landroid/view/SubMenu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isActionViewExpanded()Z
    .locals 1

    .prologue
    .line 525966
    const/4 v0, 0x0

    return v0
.end method

.method public final isCheckable()Z
    .locals 1

    .prologue
    .line 525967
    iget-boolean v0, p0, LX/3Ai;->f:Z

    return v0
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 525902
    iget-boolean v0, p0, LX/3Ai;->g:Z

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 525903
    iget-boolean v0, p0, LX/3Ai;->i:Z

    return v0
.end method

.method public final isVisible()Z
    .locals 1

    .prologue
    .line 525904
    invoke-virtual {p0}, LX/3Ai;->hasSubMenu()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 525905
    iget-boolean v0, p0, LX/3Ai;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Ai;->n:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 525906
    :goto_0
    return v0

    .line 525907
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 525908
    :cond_1
    iget-boolean v0, p0, LX/3Ai;->h:Z

    goto :goto_0
.end method

.method public final setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525909
    const/4 v0, 0x0

    return-object v0
.end method

.method public final setActionView(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525910
    const/4 v0, 0x0

    return-object v0
.end method

.method public final setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525911
    const/4 v0, 0x0

    return-object v0
.end method

.method public final setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 525912
    return-object p0
.end method

.method public final setCheckable(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525913
    iget-boolean v0, p0, LX/3Ai;->f:Z

    if-eq v0, p1, :cond_0

    .line 525914
    iput-boolean p1, p0, LX/3Ai;->f:Z

    .line 525915
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    instance-of v0, v0, LX/34d;

    if-eqz v0, :cond_0

    .line 525916
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/34d;

    invoke-interface {v0}, LX/34d;->f()V

    .line 525917
    :cond_0
    return-object p0
.end method

.method public final setChecked(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525918
    iget-boolean v0, p0, LX/3Ai;->g:Z

    if-eq v0, p1, :cond_0

    .line 525919
    iput-boolean p1, p0, LX/3Ai;->g:Z

    .line 525920
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    instance-of v0, v0, LX/34d;

    if-eqz v0, :cond_0

    .line 525921
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/34d;

    invoke-interface {v0}, LX/34d;->f()V

    .line 525922
    :cond_0
    return-object p0
.end method

.method public final setEnabled(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525923
    iget-boolean v0, p0, LX/3Ai;->i:Z

    if-eq v0, p1, :cond_0

    .line 525924
    iput-boolean p1, p0, LX/3Ai;->i:Z

    .line 525925
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    instance-of v0, v0, LX/34d;

    if-eqz v0, :cond_0

    .line 525926
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/34d;

    invoke-interface {v0}, LX/34d;->f()V

    .line 525927
    :cond_0
    return-object p0
.end method

.method public final setIcon(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 525928
    if-lez p1, :cond_0

    .line 525929
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    instance-of v0, v0, LX/5OG;

    if-eqz v0, :cond_1

    .line 525930
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/5OG;

    .line 525931
    iget-object v1, v0, LX/5OG;->c:Landroid/content/Context;

    move-object v0, v1

    .line 525932
    :goto_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 525933
    :cond_0
    return-object p0

    .line 525934
    :cond_1
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/34c;

    .line 525935
    iget-object v1, v0, LX/34c;->c:Landroid/content/Context;

    move-object v0, v1

    .line 525936
    goto :goto_0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525937
    iput-object p1, p0, LX/3Ai;->l:Landroid/graphics/drawable/Drawable;

    .line 525938
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    instance-of v0, v0, LX/34d;

    if-eqz v0, :cond_0

    .line 525939
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/34d;

    invoke-interface {v0}, LX/34d;->f()V

    .line 525940
    :cond_0
    return-object p0
.end method

.method public final setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 525941
    return-object p0
.end method

.method public final setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 525942
    return-object p0
.end method

.method public final setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525943
    const/4 v0, 0x0

    return-object v0
.end method

.method public final setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 525944
    iput-object p1, p0, LX/3Ai;->o:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 525945
    return-object p0
.end method

.method public final setShortcut(CC)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 525946
    return-object p0
.end method

.method public final setShowAsAction(I)V
    .locals 0

    .prologue
    .line 525947
    return-void
.end method

.method public final setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525948
    const/4 v0, 0x0

    return-object v0
.end method

.method public final setTitle(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 525949
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    instance-of v0, v0, LX/5OG;

    if-eqz v0, :cond_0

    .line 525950
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/5OG;

    .line 525951
    iget-object v1, v0, LX/5OG;->c:Landroid/content/Context;

    move-object v0, v1

    .line 525952
    :goto_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3Ai;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0

    .line 525953
    :cond_0
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/34c;

    .line 525954
    iget-object v1, v0, LX/34c;->c:Landroid/content/Context;

    move-object v0, v1

    .line 525955
    goto :goto_0
.end method

.method public final setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525956
    iput-object p1, p0, LX/3Ai;->c:Ljava/lang/CharSequence;

    .line 525957
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    instance-of v0, v0, LX/34d;

    if-eqz v0, :cond_0

    .line 525958
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/34d;

    invoke-interface {v0}, LX/34d;->f()V

    .line 525959
    :cond_0
    return-object p0
.end method

.method public final setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 525960
    return-object p0
.end method

.method public final setVisible(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 525961
    iget-boolean v0, p0, LX/3Ai;->h:Z

    if-eq v0, p1, :cond_0

    .line 525962
    iput-boolean p1, p0, LX/3Ai;->h:Z

    .line 525963
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    instance-of v0, v0, LX/34d;

    if-eqz v0, :cond_0

    .line 525964
    iget-object v0, p0, LX/3Ai;->m:Landroid/view/Menu;

    check-cast v0, LX/34d;

    invoke-interface {v0}, LX/34d;->f()V

    .line 525965
    :cond_0
    return-object p0
.end method
