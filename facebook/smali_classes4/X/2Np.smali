.class public LX/2Np;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2N8;


# direct methods
.method public constructor <init>(LX/2N8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 400038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400039
    iput-object p1, p0, LX/2Np;->a:LX/2N8;

    .line 400040
    return-void
.end method

.method public static a(LX/0QB;)LX/2Np;
    .locals 2

    .prologue
    .line 400041
    new-instance v1, LX/2Np;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v0

    check-cast v0, LX/2N8;

    invoke-direct {v1, v0}, LX/2Np;-><init>(LX/2N8;)V

    .line 400042
    move-object v0, v1

    .line 400043
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/payment/PaymentRequestData;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 400044
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 400045
    :goto_0
    return-object v0

    .line 400046
    :cond_0
    iget-object v1, p0, LX/2Np;->a:LX/2N8;

    invoke-virtual {v1, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 400047
    const-string v1, "id"

    invoke-virtual {v7, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    if-nez v1, :cond_1

    move-object v1, v0

    .line 400048
    :goto_1
    const-string v2, "from"

    invoke-virtual {v7, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    if-nez v2, :cond_2

    move-wide v2, v4

    .line 400049
    :goto_2
    const-string v6, "to"

    invoke-virtual {v7, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    if-nez v6, :cond_3

    .line 400050
    :goto_3
    const-string v6, "amount"

    invoke-virtual {v7, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    if-nez v6, :cond_4

    const/4 v6, 0x0

    .line 400051
    :goto_4
    const-string v8, "currency"

    invoke-virtual {v7, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    if-nez v8, :cond_5

    move-object v7, v0

    .line 400052
    :goto_5
    new-instance v0, Lcom/facebook/messaging/model/payment/PaymentRequestData;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/payment/PaymentRequestData;-><init>(Ljava/lang/String;JJILjava/lang/String;)V

    goto :goto_0

    .line 400053
    :cond_1
    const-string v1, "id"

    invoke-virtual {v7, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 400054
    :cond_2
    const-string v2, "from"

    invoke-virtual {v7, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->x()J

    move-result-wide v2

    goto :goto_2

    .line 400055
    :cond_3
    const-string v4, "to"

    invoke-virtual {v7, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->x()J

    move-result-wide v4

    goto :goto_3

    .line 400056
    :cond_4
    const-string v6, "amount"

    invoke-virtual {v7, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-virtual {v6}, LX/0lF;->w()I

    move-result v6

    goto :goto_4

    .line 400057
    :cond_5
    const-string v0, "currency"

    invoke-virtual {v7, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v7

    goto :goto_5
.end method
