.class public LX/2CB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/2CB;


# instance fields
.field public a:Z

.field public final b:LX/2CC;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0WV;

.field public final e:LX/0SG;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/appirater/AppiraterDialogMaker;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final h:LX/0Sh;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/29r;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/29q;

.field private k:J


# direct methods
.method public constructor <init>(LX/2CC;LX/0Or;LX/0WV;LX/0SG;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0Ot;)V
    .locals 2
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/appirater/IsAppiraterEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/appirater/AppiraterParams;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0WV;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/appirater/AppiraterDialogMaker;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Ot",
            "<",
            "LX/29r;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 382128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382129
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2CB;->a:Z

    .line 382130
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/2CB;->k:J

    .line 382131
    iput-object p1, p0, LX/2CB;->b:LX/2CC;

    .line 382132
    iput-object p2, p0, LX/2CB;->c:LX/0Or;

    .line 382133
    iput-object p3, p0, LX/2CB;->d:LX/0WV;

    .line 382134
    iput-object p4, p0, LX/2CB;->e:LX/0SG;

    .line 382135
    iput-object p5, p0, LX/2CB;->f:LX/0Ot;

    .line 382136
    iput-object p6, p0, LX/2CB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 382137
    iput-object p7, p0, LX/2CB;->h:LX/0Sh;

    .line 382138
    iput-object p8, p0, LX/2CB;->i:LX/0Ot;

    .line 382139
    new-instance v0, LX/29q;

    invoke-direct {v0, p0}, LX/29q;-><init>(LX/2CB;)V

    iput-object v0, p0, LX/2CB;->j:LX/29q;

    .line 382140
    return-void
.end method

.method public static a(LX/0QB;)LX/2CB;
    .locals 12

    .prologue
    .line 382165
    sget-object v0, LX/2CB;->l:LX/2CB;

    if-nez v0, :cond_1

    .line 382166
    const-class v1, LX/2CB;

    monitor-enter v1

    .line 382167
    :try_start_0
    sget-object v0, LX/2CB;->l:LX/2CB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 382168
    if-eqz v2, :cond_0

    .line 382169
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 382170
    new-instance v3, LX/2CB;

    .line 382171
    new-instance v4, LX/2CC;

    invoke-direct {v4}, LX/2CC;-><init>()V

    .line 382172
    move-object v4, v4

    .line 382173
    move-object v4, v4

    .line 382174
    check-cast v4, LX/2CC;

    const/16 v5, 0x2f9

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v6

    check-cast v6, LX/0WV;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    const/16 v8, 0x177b

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v10

    check-cast v10, LX/0Sh;

    const/16 v11, 0x12f

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, LX/2CB;-><init>(LX/2CC;LX/0Or;LX/0WV;LX/0SG;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;LX/0Ot;)V

    .line 382175
    move-object v0, v3

    .line 382176
    sput-object v0, LX/2CB;->l:LX/2CB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382177
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 382178
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 382179
    :cond_1
    sget-object v0, LX/2CB;->l:LX/2CB;

    return-object v0

    .line 382180
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 382181
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(LX/2CB;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 382160
    iget-object v0, p0, LX/2CB;->d:LX/0WV;

    invoke-virtual {v0}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v0

    .line 382161
    iget-object v1, p0, LX/2CB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2tq;->f:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 382162
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 382163
    iget-object v1, p0, LX/2CB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2tq;->b:LX/0Tn;

    iget-object v3, p0, LX/2CB;->e:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    sget-object v2, LX/2tq;->d:LX/0Tn;

    invoke-interface {v1, v2, v6}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    sget-object v2, LX/2tq;->e:LX/0Tn;

    invoke-interface {v1, v2, v6}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    sget-object v2, LX/2tq;->f:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 382164
    :cond_0
    return-void
.end method

.method public static e(LX/2CB;)Z
    .locals 20

    .prologue
    .line 382141
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/2CB;->a:Z

    if-eqz v2, :cond_0

    .line 382142
    const/4 v2, 0x0

    .line 382143
    :goto_0
    return v2

    .line 382144
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2CB;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, LX/03R;->YES:LX/03R;

    if-eq v2, v3, :cond_1

    .line 382145
    const/4 v2, 0x0

    goto :goto_0

    .line 382146
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2CB;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 382147
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2CB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/2tq;->b:LX/0Tn;

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/2CB;->k:J

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 382148
    move-object/from16 v0, p0

    iget-object v6, v0, LX/2CB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/2tq;->c:LX/0Tn;

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/2CB;->k:J

    invoke-interface {v6, v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 382149
    sub-long v8, v2, v4

    .line 382150
    const-wide/32 v10, 0x5265c00

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2CB;->b:LX/2CC;

    invoke-virtual {v12}, LX/2CC;->c()I

    move-result v12

    int-to-long v12, v12

    mul-long/2addr v10, v12

    .line 382151
    sub-long/2addr v2, v6

    .line 382152
    const-wide/32 v12, 0x5265c00

    move-object/from16 v0, p0

    iget-object v14, v0, LX/2CB;->b:LX/2CC;

    invoke-virtual {v14}, LX/2CC;->f()I

    move-result v14

    int-to-long v14, v14

    mul-long/2addr v12, v14

    .line 382153
    move-object/from16 v0, p0

    iget-object v14, v0, LX/2CB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v15, LX/2tq;->d:LX/0Tn;

    const/16 v16, 0x0

    invoke-interface/range {v14 .. v16}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v14

    .line 382154
    move-object/from16 v0, p0

    iget-object v15, v0, LX/2CB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v16, LX/2tq;->e:LX/0Tn;

    const/16 v17, 0x0

    invoke-interface/range {v15 .. v17}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v15

    .line 382155
    move-object/from16 v0, p0

    iget-object v0, v0, LX/2CB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v16, v0

    sget-object v17, LX/2tq;->h:LX/0Tn;

    const-string v18, ""

    invoke-interface/range {v16 .. v18}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 382156
    move-object/from16 v0, p0

    iget-object v0, v0, LX/2CB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v17, v0

    sget-object v18, LX/2tq;->g:LX/0Tn;

    const-string v19, ""

    invoke-interface/range {v17 .. v19}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 382157
    move-object/from16 v0, p0

    iget-wide v0, v0, LX/2CB;->k:J

    move-wide/from16 v18, v0

    cmp-long v4, v4, v18

    if-eqz v4, :cond_2

    cmp-long v4, v8, v10

    if-ltz v4, :cond_4

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2CB;->b:LX/2CC;

    invoke-virtual {v4}, LX/2CC;->d()I

    move-result v4

    if-lt v14, v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2CB;->b:LX/2CC;

    invoke-virtual {v4}, LX/2CC;->e()I

    move-result v4

    if-lt v15, v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2CB;->d:LX/0WV;

    invoke-virtual {v4}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2CB;->d:LX/0WV;

    invoke-virtual {v4}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/2CB;->k:J

    cmp-long v4, v6, v4

    if-eqz v4, :cond_3

    cmp-long v2, v2, v12

    if-ltz v2, :cond_4

    .line 382158
    :cond_3
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 382159
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0
.end method
