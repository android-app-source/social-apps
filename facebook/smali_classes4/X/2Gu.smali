.class public LX/2Gu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Gu;


# instance fields
.field public final a:LX/04P;

.field public final b:LX/2zj;


# direct methods
.method public constructor <init>(LX/04P;LX/2zj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389581
    iput-object p1, p0, LX/2Gu;->a:LX/04P;

    .line 389582
    iput-object p2, p0, LX/2Gu;->b:LX/2zj;

    .line 389583
    return-void
.end method

.method public static a(LX/0QB;)LX/2Gu;
    .locals 5

    .prologue
    .line 389584
    sget-object v0, LX/2Gu;->c:LX/2Gu;

    if-nez v0, :cond_1

    .line 389585
    const-class v1, LX/2Gu;

    monitor-enter v1

    .line 389586
    :try_start_0
    sget-object v0, LX/2Gu;->c:LX/2Gu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389587
    if-eqz v2, :cond_0

    .line 389588
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 389589
    new-instance p0, LX/2Gu;

    invoke-static {v0}, LX/1Mp;->b(LX/0QB;)LX/04P;

    move-result-object v3

    check-cast v3, LX/04P;

    invoke-static {v0}, LX/2Gv;->a(LX/0QB;)LX/2Gv;

    move-result-object v4

    check-cast v4, LX/2zj;

    invoke-direct {p0, v3, v4}, LX/2Gu;-><init>(LX/04P;LX/2zj;)V

    .line 389590
    move-object v0, p0

    .line 389591
    sput-object v0, LX/2Gu;->c:LX/2Gu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389592
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389593
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389594
    :cond_1
    sget-object v0, LX/2Gu;->c:LX/2Gu;

    return-object v0

    .line 389595
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389596
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()J
    .locals 2

    .prologue
    .line 389597
    iget-object v0, p0, LX/2Gu;->b:LX/2zj;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/2Gu;->b:LX/2zj;

    invoke-interface {v0}, LX/2zj;->h()J

    move-result-wide v0

    goto :goto_0
.end method
