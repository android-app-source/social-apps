.class public LX/2R7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;",
        "Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409456
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 409457
    check-cast p1, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;

    .line 409458
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 409459
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409460
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "proxied_app_id"

    .line 409461
    iget-object v2, p1, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->a:Ljava/lang/String;

    move-object v2, v2

    .line 409462
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409463
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "android_key_hash"

    .line 409464
    iget-object v2, p1, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->b:Ljava/lang/String;

    move-object v2, v2

    .line 409465
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409466
    iget-object v0, p1, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->c:LX/0am;

    move-object v0, v0

    .line 409467
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409468
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "permissions"

    .line 409469
    iget-object v0, p1, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->c:LX/0am;

    move-object v0, v0

    .line 409470
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409471
    :cond_0
    iget-object v0, p1, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->d:LX/0am;

    move-object v0, v0

    .line 409472
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 409473
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 409474
    const-string v2, "value"

    .line 409475
    iget-object v0, p1, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->d:LX/0am;

    move-object v0, v0

    .line 409476
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 409477
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "write_privacy"

    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409478
    :cond_1
    iget-object v0, p1, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->e:LX/0am;

    move-object v0, v0

    .line 409479
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 409480
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_refresh_only"

    .line 409481
    iget-object v0, p1, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Params;->e:LX/0am;

    move-object v0, v0

    .line 409482
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409483
    :cond_2
    new-instance v0, LX/14N;

    const-string v1, "authorize_app_method"

    const-string v2, "POST"

    const-string v3, "method/auth.androidauthorizeapp"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 409484
    iget-object v0, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 409485
    instance-of v0, v0, LX/0lF;

    if-eqz v0, :cond_0

    .line 409486
    iget-object v0, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 409487
    check-cast v0, LX/0lF;

    .line 409488
    if-eqz v0, :cond_0

    .line 409489
    invoke-virtual {v0}, LX/0lF;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 409490
    const-string v1, "error_code"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 409491
    const-string v1, "error_code"

    invoke-virtual {v0, v1}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    .line 409492
    const/16 v1, 0x198

    if-ne v0, v1, :cond_0

    .line 409493
    const/4 v0, 0x0

    .line 409494
    :goto_0
    return-object v0

    .line 409495
    :cond_0
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 409496
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 409497
    const-string v1, "access_token"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    .line 409498
    const-string v2, "expires"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->D()J

    move-result-wide v2

    .line 409499
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 409500
    const-string v5, "permissions"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 409501
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 409502
    :cond_1
    new-instance v0, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;

    invoke-direct/range {v0 .. v4}, Lcom/facebook/platform/auth/server/AuthorizeAppMethod$Result;-><init>(Ljava/lang/String;JLjava/util/List;)V

    goto :goto_0
.end method
