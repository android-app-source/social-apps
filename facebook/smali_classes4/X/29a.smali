.class public LX/29a;
.super LX/16B;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile j:LX/29a;


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:Lcom/facebook/xanalytics/XAnalyticsNative;

.field private final d:LX/00I;

.field public final e:LX/0dC;

.field public final f:LX/0WV;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/util/concurrent/ScheduledExecutorService;

.field public i:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 376425
    const-class v0, LX/29a;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/29a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;LX/00I;Landroid/content/Context;LX/0Or;LX/0WV;LX/0dC;LX/0Xl;)V
    .locals 4
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/00I;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0WV;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0Xl;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 376383
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 376384
    iput-object v3, p0, LX/29a;->i:Ljava/util/concurrent/ScheduledFuture;

    .line 376385
    iput-object p2, p0, LX/29a;->d:LX/00I;

    .line 376386
    new-instance v0, Lcom/facebook/xanalytics/XAnalyticsNative;

    invoke-direct {v0}, Lcom/facebook/xanalytics/XAnalyticsNative;-><init>()V

    iput-object v0, p0, LX/29a;->c:Lcom/facebook/xanalytics/XAnalyticsNative;

    .line 376387
    iput-object p3, p0, LX/29a;->b:Landroid/content/Context;

    .line 376388
    iput-object p6, p0, LX/29a;->e:LX/0dC;

    .line 376389
    iput-object p5, p0, LX/29a;->f:LX/0WV;

    .line 376390
    iget-object v0, p0, LX/29a;->b:Landroid/content/Context;

    const-string v1, "fbacore"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/29a;->g:Ljava/lang/String;

    .line 376391
    new-instance v0, LX/29b;

    invoke-direct {v0}, LX/29b;-><init>()V

    iget-object v1, p0, LX/29a;->d:LX/00I;

    invoke-interface {v1}, LX/00I;->c()Ljava/lang/String;

    move-result-object v1

    .line 376392
    iput-object v1, v0, LX/29b;->a:Ljava/lang/String;

    .line 376393
    move-object v0, v0

    .line 376394
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/29a;->d:LX/00I;

    invoke-interface {v2}, LX/00I;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/29a;->d:LX/00I;

    invoke-interface {v2}, LX/00I;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 376395
    iput-object v1, v0, LX/29b;->b:Ljava/lang/String;

    .line 376396
    move-object v0, v0

    .line 376397
    iget-object v1, p0, LX/29a;->g:Ljava/lang/String;

    .line 376398
    iput-object v1, v0, LX/29b;->c:Ljava/lang/String;

    .line 376399
    move-object v0, v0

    .line 376400
    const-string v1, "graph.facebook.com"

    .line 376401
    iput-object v1, v0, LX/29b;->d:Ljava/lang/String;

    .line 376402
    move-object v0, v0

    .line 376403
    const/16 v1, 0x61

    .line 376404
    iput v1, v0, LX/29b;->e:I

    .line 376405
    move-object v0, v0

    .line 376406
    const/16 v1, 0xb

    .line 376407
    iput v1, v0, LX/29b;->f:I

    .line 376408
    move-object v0, v0

    .line 376409
    iput-object v3, v0, LX/29b;->g:Lcom/facebook/tigon/iface/TigonServiceHolder;

    .line 376410
    move-object v0, v0

    .line 376411
    const v1, 0xc800

    .line 376412
    iput v1, v0, LX/29b;->h:I

    .line 376413
    move-object v0, v0

    .line 376414
    iput-object p1, v0, LX/29b;->i:Ljava/util/concurrent/Executor;

    .line 376415
    move-object v0, v0

    .line 376416
    invoke-virtual {v0}, LX/29b;->a()LX/29c;

    move-result-object v0

    .line 376417
    iget-object v1, p0, LX/29a;->c:Lcom/facebook/xanalytics/XAnalyticsNative;

    new-instance v2, LX/29d;

    invoke-direct {v2, p0, p4}, LX/29d;-><init>(LX/29a;LX/0Or;)V

    invoke-virtual {v1, v0, v2}, Lcom/facebook/xanalytics/XAnalyticsNative;->a(LX/29c;Lcom/facebook/xanalytics/XAnalyticsNative$XAnalyticsPropertiesProvider;)J

    move-result-wide v0

    .line 376418
    sget-boolean v2, LX/007;->i:Z

    move v2, v2

    .line 376419
    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 376420
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/29a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": FBAnalyticsCore Failed to Initialize."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 376421
    :cond_0
    iput-object p1, p0, LX/29a;->h:Ljava/util/concurrent/ScheduledExecutorService;

    .line 376422
    invoke-interface {p7}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v2, LX/29e;

    invoke-direct {v2, p0}, LX/29e;-><init>(LX/29a;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 376423
    invoke-interface {p7}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v2, LX/29f;

    invoke-direct {v2, p0}, LX/29f;-><init>(LX/29a;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 376424
    return-void
.end method

.method public static a(LX/0QB;)LX/29a;
    .locals 11

    .prologue
    .line 376430
    sget-object v0, LX/29a;->j:LX/29a;

    if-nez v0, :cond_1

    .line 376431
    const-class v1, LX/29a;

    monitor-enter v1

    .line 376432
    :try_start_0
    sget-object v0, LX/29a;->j:LX/29a;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 376433
    if-eqz v2, :cond_0

    .line 376434
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 376435
    new-instance v3, LX/29a;

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    const-class v5, LX/00I;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/00I;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    const/16 v7, 0x15e7

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v8

    check-cast v8, LX/0WV;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v9

    check-cast v9, LX/0dC;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-direct/range {v3 .. v10}, LX/29a;-><init>(Ljava/util/concurrent/ScheduledExecutorService;LX/00I;Landroid/content/Context;LX/0Or;LX/0WV;LX/0dC;LX/0Xl;)V

    .line 376436
    move-object v0, v3

    .line 376437
    sput-object v0, LX/29a;->j:LX/29a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376438
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 376439
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 376440
    :cond_1
    sget-object v0, LX/29a;->j:LX/29a;

    return-object v0

    .line 376441
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 376442
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/xanalytics/XAnalyticsNative;
    .locals 1

    .prologue
    .line 376443
    iget-object v0, p0, LX/29a;->c:Lcom/facebook/xanalytics/XAnalyticsNative;

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 376428
    iget-object v0, p0, LX/29a;->c:Lcom/facebook/xanalytics/XAnalyticsNative;

    invoke-virtual {v0}, Lcom/facebook/xanalytics/XAnalyticsNative;->onSwitchUserId()V

    .line 376429
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 376426
    iget-object v0, p0, LX/29a;->c:Lcom/facebook/xanalytics/XAnalyticsNative;

    invoke-virtual {v0}, Lcom/facebook/xanalytics/XAnalyticsNative;->onSwitchUserId()V

    .line 376427
    return-void
.end method
