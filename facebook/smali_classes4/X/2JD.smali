.class public LX/2JD;
.super LX/0Tw;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2JD;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392685
    const-string v0, "contacts_upload"

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, LX/0Tw;-><init>(Ljava/lang/String;I)V

    .line 392686
    return-void
.end method

.method public static a(LX/0QB;)LX/2JD;
    .locals 3

    .prologue
    .line 392673
    sget-object v0, LX/2JD;->a:LX/2JD;

    if-nez v0, :cond_1

    .line 392674
    const-class v1, LX/2JD;

    monitor-enter v1

    .line 392675
    :try_start_0
    sget-object v0, LX/2JD;->a:LX/2JD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 392676
    if-eqz v2, :cond_0

    .line 392677
    :try_start_1
    new-instance v0, LX/2JD;

    invoke-direct {v0}, LX/2JD;-><init>()V

    .line 392678
    move-object v0, v0

    .line 392679
    sput-object v0, LX/2JD;->a:LX/2JD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392680
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 392681
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392682
    :cond_1
    sget-object v0, LX/2JD;->a:LX/2JD;

    return-object v0

    .line 392683
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 392684
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 392666
    const-string v0, "CREATE TABLE phone_address_book_snapshot (local_contact_id INTEGER PRIMARY KEY, contact_hash TEXT)"

    const v1, 0x3f7e61aa

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x67846a0c

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392667
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 392670
    const-string v0, "DROP TABLE IF EXISTS phone_address_book_snapshot"

    const v1, -0x201f990a

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x4edc6d9a

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392671
    invoke-virtual {p0, p1}, LX/2JD;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 392672
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 392668
    const-string v0, "phone_address_book_snapshot"

    invoke-virtual {p1, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 392669
    return-void
.end method
