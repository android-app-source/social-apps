.class public LX/28E;
.super LX/28F;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public _path:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/4pp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 373765
    invoke-direct {p0, p1}, LX/28F;-><init>(Ljava/lang/String;)V

    .line 373766
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/28G;)V
    .locals 0

    .prologue
    .line 373767
    invoke-direct {p0, p1, p2}, LX/28F;-><init>(Ljava/lang/String;LX/28G;)V

    .line 373768
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 373769
    invoke-direct {p0, p1, p2, p3}, LX/28F;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    .line 373770
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 373774
    invoke-direct {p0, p1, p2}, LX/28F;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 373775
    return-void
.end method

.method public static a(LX/15w;Ljava/lang/String;)LX/28E;
    .locals 2

    .prologue
    .line 373771
    new-instance v1, LX/28E;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, p1, v0}, LX/28E;-><init>(Ljava/lang/String;LX/28G;)V

    return-object v1

    :cond_0
    invoke-virtual {p0}, LX/15w;->k()LX/28G;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/15w;Ljava/lang/String;Ljava/lang/Throwable;)LX/28E;
    .locals 2

    .prologue
    .line 373772
    new-instance v1, LX/28E;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, p1, v0, p2}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    return-object v1

    :cond_0
    invoke-virtual {p0}, LX/15w;->k()LX/28G;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/io/IOException;)LX/28E;
    .locals 3

    .prologue
    .line 373773
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected IOException (of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Throwable;LX/4pp;)LX/28E;
    .locals 3

    .prologue
    .line 373756
    instance-of v0, p0, LX/28E;

    if-eqz v0, :cond_0

    .line 373757
    check-cast p0, LX/28E;

    .line 373758
    :goto_0
    invoke-virtual {p0, p1}, LX/28E;->a(LX/4pp;)V

    .line 373759
    return-object p0

    .line 373760
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 373761
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 373762
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(was "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 373763
    :cond_2
    new-instance v1, LX/28E;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2, p0}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    move-object p0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Throwable;Ljava/lang/Object;I)LX/28E;
    .locals 1

    .prologue
    .line 373764
    new-instance v0, LX/4pp;

    invoke-direct {v0, p1, p2}, LX/4pp;-><init>(Ljava/lang/Object;I)V

    invoke-static {p0, v0}, LX/28E;->a(Ljava/lang/Throwable;LX/4pp;)LX/28E;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Throwable;Ljava/lang/Object;Ljava/lang/String;)LX/28E;
    .locals 1

    .prologue
    .line 373755
    new-instance v0, LX/4pp;

    invoke-direct {v0, p1, p2}, LX/4pp;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0}, LX/28E;->a(Ljava/lang/Throwable;LX/4pp;)LX/28E;

    move-result-object v0

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 373745
    invoke-super {p0}, LX/28F;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 373746
    iget-object v0, p0, LX/28E;->_path:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 373747
    :goto_0
    return-object v0

    .line 373748
    :cond_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 373749
    :goto_1
    const-string v1, " (through reference chain: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373750
    invoke-static {p0, v0}, LX/28E;->b(LX/28E;Ljava/lang/StringBuilder;)V

    .line 373751
    move-object v0, v0

    .line 373752
    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 373753
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 373754
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static b(LX/28E;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 373738
    iget-object v0, p0, LX/28E;->_path:Ljava/util/LinkedList;

    if-nez v0, :cond_1

    .line 373739
    :cond_0
    return-void

    .line 373740
    :cond_1
    iget-object v0, p0, LX/28E;->_path:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 373741
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373742
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4pp;

    invoke-virtual {v0}, LX/4pp;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373743
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 373744
    const-string v0, "->"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/4pp;)V
    .locals 2

    .prologue
    .line 373733
    iget-object v0, p0, LX/28E;->_path:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    .line 373734
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/28E;->_path:Ljava/util/LinkedList;

    .line 373735
    :cond_0
    iget-object v0, p0, LX/28E;->_path:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x3e8

    if-ge v0, v1, :cond_1

    .line 373736
    iget-object v0, p0, LX/28E;->_path:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 373737
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 373730
    new-instance v0, LX/4pp;

    invoke-direct {v0, p1, p2}, LX/4pp;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373731
    invoke-virtual {p0, v0}, LX/28E;->a(LX/4pp;)V

    .line 373732
    return-void
.end method

.method public final getLocalizedMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 373729
    invoke-direct {p0}, LX/28E;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 373728
    invoke-direct {p0}, LX/28E;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 373727
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/28E;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
