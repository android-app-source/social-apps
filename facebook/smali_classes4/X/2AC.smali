.class public LX/2AC;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 376991
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 376992
    return-void
.end method

.method public static a(LX/0Sh;LX/0rb;LX/03V;)LX/2AD;
    .locals 6
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/notifications/annotations/RegularNotificationCache;
    .end annotation

    .prologue
    .line 376993
    new-instance v0, LX/2AD;

    const-string v2, "Notification.NotificationStoryCache"

    const-string v3, "notification_story_cache_entries"

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/2AD;-><init>(LX/0Sh;Ljava/lang/String;Ljava/lang/String;LX/0rb;LX/03V;)V

    return-object v0
.end method

.method public static b(LX/0Sh;LX/0rb;LX/03V;)LX/2AD;
    .locals 6
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/notifications/annotations/OverflowedNotificationCache;
    .end annotation

    .prologue
    .line 376994
    new-instance v0, LX/2AD;

    const-string v2, "Notification.OverflowedNotificationStoryCache"

    const-string v3, "overflowed_notification_story_cache_entries"

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/2AD;-><init>(LX/0Sh;Ljava/lang/String;Ljava/lang/String;LX/0rb;LX/03V;)V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 376995
    return-void
.end method
