.class public LX/24r;
.super LX/24s;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/24s",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/1ca;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 367809
    invoke-direct {p0, p2}, LX/24s;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 367810
    iput-object p1, p0, LX/24r;->b:LX/1ca;

    .line 367811
    iput-object p2, p0, LX/24r;->a:Lcom/google/common/util/concurrent/SettableFuture;

    .line 367812
    new-instance v0, LX/24t;

    invoke-direct {v0, p0}, LX/24t;-><init>(LX/24r;)V

    .line 367813
    iget-object v1, p0, LX/24r;->b:LX/1ca;

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 367814
    return-void
.end method

.method public static a(LX/1ca;)LX/24r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1ca",
            "<TT;>;)",
            "LX/24r",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 367815
    new-instance v0, LX/24r;

    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/24r;-><init>(LX/1ca;Lcom/google/common/util/concurrent/SettableFuture;)V

    return-object v0
.end method


# virtual methods
.method public final cancel(Z)Z
    .locals 1

    .prologue
    .line 367816
    iget-object v0, p0, LX/24r;->b:LX/1ca;

    invoke-interface {v0}, LX/1ca;->g()Z

    .line 367817
    invoke-super {p0, p1}, LX/24s;->cancel(Z)Z

    move-result v0

    return v0
.end method
