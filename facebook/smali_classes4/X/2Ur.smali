.class public LX/2Ur;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile m:LX/2Ur;


# instance fields
.field public b:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3MQ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/SmsThreadManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FOd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oq;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/30m;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Uq;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/03R;

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 416765
    const-class v0, LX/2Ur;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Ur;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416750
    const-string v0, "SmsUserMatchingBackgroundTask"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 416751
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416752
    iput-object v0, p0, LX/2Ur;->e:LX/0Ot;

    .line 416753
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416754
    iput-object v0, p0, LX/2Ur;->f:LX/0Ot;

    .line 416755
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416756
    iput-object v0, p0, LX/2Ur;->g:LX/0Ot;

    .line 416757
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416758
    iput-object v0, p0, LX/2Ur;->h:LX/0Ot;

    .line 416759
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416760
    iput-object v0, p0, LX/2Ur;->i:LX/0Ot;

    .line 416761
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416762
    iput-object v0, p0, LX/2Ur;->j:LX/0Ot;

    .line 416763
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2Ur;->k:LX/03R;

    .line 416764
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ur;
    .locals 12

    .prologue
    .line 416735
    sget-object v0, LX/2Ur;->m:LX/2Ur;

    if-nez v0, :cond_1

    .line 416736
    const-class v1, LX/2Ur;

    monitor-enter v1

    .line 416737
    :try_start_0
    sget-object v0, LX/2Ur;->m:LX/2Ur;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416738
    if-eqz v2, :cond_0

    .line 416739
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416740
    new-instance v3, LX/2Ur;

    invoke-direct {v3}, LX/2Ur;-><init>()V

    .line 416741
    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    const/16 v7, 0xde6

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xd9c

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2a3b

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xd9e

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xda0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 p0, 0xd9f

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 416742
    iput-object v4, v3, LX/2Ur;->b:LX/0SG;

    iput-object v5, v3, LX/2Ur;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v6, v3, LX/2Ur;->d:LX/0TD;

    iput-object v7, v3, LX/2Ur;->e:LX/0Ot;

    iput-object v8, v3, LX/2Ur;->f:LX/0Ot;

    iput-object v9, v3, LX/2Ur;->g:LX/0Ot;

    iput-object v10, v3, LX/2Ur;->h:LX/0Ot;

    iput-object v11, v3, LX/2Ur;->i:LX/0Ot;

    iput-object p0, v3, LX/2Ur;->j:LX/0Ot;

    .line 416743
    move-object v0, v3

    .line 416744
    sput-object v0, LX/2Ur;->m:LX/2Ur;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416745
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416746
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416747
    :cond_1
    sget-object v0, LX/2Ur;->m:LX/2Ur;

    return-object v0

    .line 416748
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416749
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static l(LX/2Ur;)V
    .locals 14

    .prologue
    const/4 v10, 0x2

    const/4 v2, 0x0

    .line 416696
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 416697
    iget-object v0, p0, LX/2Ur;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/SmsThreadManager;

    const/16 v3, 0x14

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v3, v4, v5, v1}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(IJLjava/util/Map;)Ljava/util/List;

    move-result-object v0

    .line 416698
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 416699
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 416700
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 416701
    iget-object v6, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v1, v2

    :goto_0
    if-ge v1, v7, :cond_0

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 416702
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->g()Ljava/lang/String;

    move-result-object v8

    .line 416703
    if-eqz v8, :cond_1

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 416704
    iget-object v0, p0, LX/2Ur;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOd;

    sget-object v9, LX/FOZ;->FOF:LX/FOZ;

    .line 416705
    iget-object v11, v0, LX/FOd;->d:LX/FOa;

    new-instance v12, LX/FOc;

    invoke-direct {v12}, LX/FOc;-><init>()V

    .line 416706
    iput-object v8, v12, LX/FOc;->a:Ljava/lang/String;

    .line 416707
    move-object v12, v12

    .line 416708
    iget-object v13, v0, LX/FOd;->b:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    invoke-virtual {v13}, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->a()Z

    move-result v13

    .line 416709
    iput-boolean v13, v12, LX/FOc;->b:Z

    .line 416710
    move-object v12, v12

    .line 416711
    iput-object v9, v12, LX/FOc;->c:LX/FOZ;

    .line 416712
    move-object v12, v12

    .line 416713
    sget-object v13, LX/FOZ;->NONE:LX/FOZ;

    .line 416714
    iput-object v13, v12, LX/FOc;->d:LX/FOZ;

    .line 416715
    move-object v12, v12

    .line 416716
    const-string v13, "UNKNOWN"

    .line 416717
    iput-object v13, v12, LX/FOc;->f:Ljava/lang/String;

    .line 416718
    move-object v12, v12

    .line 416719
    const-string v13, "SMS_MATCHING"

    .line 416720
    iput-object v13, v12, LX/FOc;->e:Ljava/lang/String;

    .line 416721
    move-object v12, v12

    .line 416722
    new-instance v13, LX/FOb;

    invoke-direct {v13, v12}, LX/FOb;-><init>(LX/FOc;)V

    move-object v12, v13

    .line 416723
    invoke-static {v11, v12}, LX/FOa;->b(LX/FOa;LX/FOb;)LX/FOe;

    move-result-object v13

    .line 416724
    if-nez v13, :cond_4

    const/4 v13, 0x0

    :goto_1
    move-object v11, v13

    .line 416725
    move-object v0, v11

    .line 416726
    if-nez v0, :cond_2

    .line 416727
    invoke-interface {v4, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 416728
    iget-object v0, p0, LX/2Ur;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30m;

    invoke-virtual {v0, v2}, LX/30m;->b(Z)V

    .line 416729
    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 416730
    :cond_2
    invoke-interface {v3, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416731
    iget-object v0, p0, LX/2Ur;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30m;

    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/30m;->b(Z)V

    goto :goto_2

    .line 416732
    :cond_3
    iget-object v0, p0, LX/2Ur;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MQ;

    invoke-virtual {v0, v3, v10}, LX/3MQ;->a(Ljava/util/Map;I)V

    .line 416733
    iget-object v0, p0, LX/2Ur;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MQ;

    invoke-virtual {v0, v4, v10}, LX/3MQ;->a(Ljava/util/Collection;I)V

    .line 416734
    return-void

    :cond_4
    iget-object v13, v13, LX/FOe;->a:Lcom/facebook/user/model/User;

    goto :goto_1
.end method

.method private m()Z
    .locals 3

    .prologue
    .line 416766
    iget-boolean v0, p0, LX/2Ur;->l:Z

    if-nez v0, :cond_1

    .line 416767
    iget-object v0, p0, LX/2Ur;->k:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 416768
    iget-object v0, p0, LX/2Ur;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Kr;->Y:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/2Ur;->k:LX/03R;

    .line 416769
    :cond_0
    iget-object v0, p0, LX/2Ur;->k:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    move v0, v0

    .line 416770
    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, LX/2Ur;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/2Ur;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Uq;

    invoke-virtual {v0}, LX/2Uq;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/2Ur;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOd;

    .line 416771
    iget-object v1, v0, LX/FOd;->a:LX/30I;

    invoke-virtual {v1}, LX/30I;->a()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, LX/FOd;->b:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    invoke-virtual {v1}, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 416772
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 416695
    const-class v0, Lcom/facebook/messaging/background/annotations/MessagesLocalTaskTag;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 416688
    invoke-direct {p0}, LX/2Ur;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416689
    iget-object v0, p0, LX/2Ur;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 416690
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416694
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 416693
    invoke-direct {p0}, LX/2Ur;->m()Z

    move-result v0

    return v0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416691
    new-instance v0, LX/FNF;

    invoke-direct {v0, p0}, LX/FNF;-><init>(LX/2Ur;)V

    .line 416692
    iget-object v1, p0, LX/2Ur;->d:LX/0TD;

    invoke-interface {v1, v0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
