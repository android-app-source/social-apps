.class public LX/2JJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field private final b:LX/0Uh;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Gxb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Uh;)V
    .locals 0
    .param p2    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/Gxb;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392773
    iput-object p1, p0, LX/2JJ;->c:LX/0Or;

    .line 392774
    iput-object p2, p0, LX/2JJ;->b:LX/0Uh;

    .line 392775
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 392767
    iget-object v0, p0, LX/2JJ;->b:LX/0Uh;

    const/16 v1, 0x2b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 392771
    sget-object v0, LX/2Jm;->STATE_CHANGE:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<",
            "LX/Gxb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392770
    iget-object v0, p0, LX/2JJ;->c:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 2

    .prologue
    .line 392769
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Jh;->BACKGROUND:LX/2Jh;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jh;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Ji;->NOT_LOW:LX/2Ji;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Ji;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jq;->LOGGED_IN:LX/2Jq;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jq;)LX/2Jk;

    move-result-object v0

    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 392768
    const-wide/32 v0, 0x36ee80

    return-wide v0
.end method
