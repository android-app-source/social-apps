.class public final LX/2ZM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/2ZJ;


# direct methods
.method public constructor <init>(LX/2ZJ;)V
    .locals 0

    .prologue
    .line 422360
    iput-object p1, p0, LX/2ZM;->a:LX/2ZJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 422361
    new-instance v1, LX/2K3;

    .line 422362
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 422363
    sget-object v2, LX/2K4;->IS_NOT_SESSIONLESS:LX/2K4;

    invoke-direct {v1, v0, v2}, LX/2K3;-><init>(LX/0Rf;LX/2K4;)V

    .line 422364
    iget-object v0, p0, LX/2ZM;->a:LX/2ZJ;

    iget-object v0, v0, LX/2ZJ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "gk"

    .line 422365
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 422366
    move-object v0, v0

    .line 422367
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 422368
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 422369
    const-string v0, "gk"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 422370
    if-nez v0, :cond_1

    .line 422371
    :cond_0
    :goto_0
    return-void

    .line 422372
    :cond_1
    iget-object v1, p0, LX/2ZM;->a:LX/2ZJ;

    invoke-static {v1}, LX/2ZJ;->f(LX/2ZJ;)Ljava/util/List;

    move-result-object v3

    .line 422373
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 422374
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 422375
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    .line 422376
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2K2;

    invoke-interface {v1, v0}, LX/2K2;->a(Landroid/os/Bundle;)V

    .line 422377
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 422378
    :cond_2
    iget-object v1, p0, LX/2ZM;->a:LX/2ZJ;

    .line 422379
    iget-object v5, v1, LX/2ZJ;->e:LX/0Uo;

    .line 422380
    iget-wide v9, v5, LX/0Uo;->W:J

    const-wide/high16 v11, -0x8000000000000000L

    cmp-long v9, v9, v11

    if-nez v9, :cond_3

    .line 422381
    invoke-static {v5}, LX/0Uo;->A(LX/0Uo;)V

    .line 422382
    :cond_3
    iget-wide v9, v5, LX/0Uo;->W:J

    move-wide v5, v9

    .line 422383
    iget-object v7, v1, LX/2ZJ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    sget-object v8, LX/2ai;->e:LX/0Tn;

    invoke-interface {v7, v8, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 422384
    goto :goto_0
.end method
