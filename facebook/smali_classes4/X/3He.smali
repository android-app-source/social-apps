.class public final LX/3He;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final synthetic a:LX/3Hd;


# direct methods
.method public constructor <init>(LX/3Hd;)V
    .locals 0

    .prologue
    .line 544266
    iput-object p1, p0, LX/3He;->a:LX/3Hd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 544267
    check-cast p2, LX/Acn;

    .line 544268
    iget-object v0, p0, LX/3He;->a:LX/3Hd;

    .line 544269
    iget-object v1, p2, LX/Acn;->a:Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;

    move-object v1, v1

    .line 544270
    iput-object v1, v0, LX/3Hd;->h:Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;

    .line 544271
    iget-object v0, p0, LX/3He;->a:LX/3Hd;

    iget-object v0, v0, LX/3Hd;->h:Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;

    .line 544272
    iget-object v1, v0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;->a:LX/Acp;

    move-object v0, v1

    .line 544273
    if-eqz v0, :cond_0

    .line 544274
    iget-object v0, p0, LX/3He;->a:LX/3Hd;

    iget-object v0, v0, LX/3Hd;->h:Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;

    .line 544275
    iget-object v1, v0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;->a:LX/Acp;

    move-object v0, v1

    .line 544276
    iget-object v1, p0, LX/3He;->a:LX/3Hd;

    .line 544277
    iput-object v1, v0, LX/Acp;->f:LX/3Hd;

    .line 544278
    iget-object v0, p0, LX/3He;->a:LX/3Hd;

    iget-object v0, v0, LX/3Hd;->h:Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;

    .line 544279
    iget-object v1, v0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;->a:LX/Acp;

    move-object v0, v1

    .line 544280
    iget-object v1, p0, LX/3He;->a:LX/3Hd;

    iget-object v1, v1, LX/3Hd;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Hd;->b:LX/0Tn;

    invoke-interface {v1, v2, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    iget-object v2, p0, LX/3He;->a:LX/3Hd;

    iget-object v2, v2, LX/3Hd;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/3Hd;->c:LX/0Tn;

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    const/4 p2, 0x0

    .line 544281
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 544282
    iget-object v5, v0, LX/Acp;->d:Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 544283
    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 544284
    iget v6, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v0}, LX/Acp;->getWidth()I

    move-result p1

    sub-int/2addr v6, p1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 544285
    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 544286
    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v0}, LX/Acp;->getHeight()I

    move-result p1

    sub-int/2addr v3, p1

    invoke-static {v6, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 544287
    iget-object v6, v0, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    iput v5, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 544288
    iget-object v5, v0, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    iput v3, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 544289
    iget-object v3, v0, LX/Acp;->d:Landroid/view/WindowManager;

    iget-object v5, v0, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v3, v0, v5}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 544290
    :cond_0
    iget-object v0, p0, LX/3He;->a:LX/3Hd;

    .line 544291
    iput-boolean v4, v0, LX/3Hd;->i:Z

    .line 544292
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 544293
    iget-object v0, p0, LX/3He;->a:LX/3Hd;

    const/4 v1, 0x0

    .line 544294
    iput-object v1, v0, LX/3Hd;->h:Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;

    .line 544295
    iget-object v0, p0, LX/3He;->a:LX/3Hd;

    const/4 v1, 0x0

    .line 544296
    iput-boolean v1, v0, LX/3Hd;->i:Z

    .line 544297
    return-void
.end method
