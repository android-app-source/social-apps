.class public LX/2a9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2a9;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/2aA;

.field private final c:LX/0Xl;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Xl;LX/2aA;)V
    .locals 0
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 423491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 423492
    iput-object p1, p0, LX/2a9;->a:Landroid/content/Context;

    .line 423493
    iput-object p2, p0, LX/2a9;->c:LX/0Xl;

    .line 423494
    iput-object p3, p0, LX/2a9;->b:LX/2aA;

    .line 423495
    return-void
.end method

.method public static a(LX/0QB;)LX/2a9;
    .locals 6

    .prologue
    .line 423478
    sget-object v0, LX/2a9;->d:LX/2a9;

    if-nez v0, :cond_1

    .line 423479
    const-class v1, LX/2a9;

    monitor-enter v1

    .line 423480
    :try_start_0
    sget-object v0, LX/2a9;->d:LX/2a9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 423481
    if-eqz v2, :cond_0

    .line 423482
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 423483
    new-instance p0, LX/2a9;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-static {v0}, LX/2aA;->a(LX/0QB;)LX/2aA;

    move-result-object v5

    check-cast v5, LX/2aA;

    invoke-direct {p0, v3, v4, v5}, LX/2a9;-><init>(Landroid/content/Context;LX/0Xl;LX/2aA;)V

    .line 423484
    move-object v0, p0

    .line 423485
    sput-object v0, LX/2a9;->d:LX/2a9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423486
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 423487
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 423488
    :cond_1
    sget-object v0, LX/2a9;->d:LX/2a9;

    return-object v0

    .line 423489
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 423490
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 423496
    iget-object v0, p0, LX/2a9;->c:LX/0Xl;

    invoke-interface {v0, p1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 423497
    iget-object v0, p0, LX/2a9;->b:LX/2aA;

    iget-object v1, p0, LX/2a9;->a:Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 423498
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 2

    .prologue
    .line 423474
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.presence.ACTION_PUSH_RECEIVED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 423475
    const-string v1, "extra_message"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 423476
    invoke-direct {p0, v0}, LX/2a9;->a(Landroid/content/Intent;)V

    .line 423477
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/presence/PresenceList;Z)V
    .locals 2

    .prologue
    .line 423468
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.presence.ACTION_PRESENCE_RECEIVED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 423469
    const-string v1, "extra_topic_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 423470
    const-string v1, "extra_presence_map"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 423471
    const-string v1, "extra_full_list"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 423472
    invoke-direct {p0, v0}, LX/2a9;->a(Landroid/content/Intent;)V

    .line 423473
    return-void
.end method
