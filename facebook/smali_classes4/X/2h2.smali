.class public abstract LX/2h2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 449705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 449683
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->a:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-object v0, v0

    .line 449684
    iget-object v1, p0, Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 449685
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 449686
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "carrier_mcc"

    .line 449687
    iget-object v5, v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object v5, v5

    .line 449688
    iget-object p0, v5, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    move-object v5, p0

    .line 449689
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 449690
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "carrier_mnc"

    .line 449691
    iget-object v5, v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object v5, v5

    .line 449692
    iget-object p0, v5, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    move-object v5, p0

    .line 449693
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 449694
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "sim_mcc"

    .line 449695
    iget-object v5, v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object v5, v5

    .line 449696
    iget-object p0, v5, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    move-object v5, p0

    .line 449697
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 449698
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "sim_mnc"

    .line 449699
    iget-object v5, v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object v0, v5

    .line 449700
    iget-object v5, v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    move-object v0, v5

    .line 449701
    invoke-direct {v3, v4, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 449702
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v0, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 449703
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "interface"

    invoke-direct {v0, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 449704
    return-object v2
.end method
