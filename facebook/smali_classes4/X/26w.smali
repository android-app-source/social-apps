.class public LX/26w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/26w;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:[I

.field private final c:[I

.field public d:Ljava/nio/ByteBuffer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 372618
    const-class v0, LX/26w;

    sput-object v0, LX/26w;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 372602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372603
    new-array v0, p1, [I

    iput-object v0, p0, LX/26w;->b:[I

    .line 372604
    new-array v0, p1, [I

    iput-object v0, p0, LX/26w;->c:[I

    .line 372605
    iget-object v0, p0, LX/26w;->b:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 372606
    iget-object v0, p0, LX/26w;->c:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 372607
    mul-int/lit8 v0, p1, 0xc

    .line 372608
    add-int/lit8 v0, v0, 0x14

    .line 372609
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    .line 372610
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372611
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    const v2, -0x5312ff3

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 372612
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372613
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    const v2, 0x20151014

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 372614
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372615
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 372616
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372617
    return-void
.end method

.method private a(ILX/0oc;I)V
    .locals 5

    .prologue
    .line 372588
    invoke-direct {p0, p2, p3}, LX/26w;->b(LX/0oc;I)V

    .line 372589
    iget-object v0, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    rem-int/lit8 v0, v0, 0x4

    .line 372590
    :goto_0
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int v2, v0, p1

    add-int/2addr v1, v2

    iget-object v2, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 372591
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 372592
    iget-object v2, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 372593
    iget-object v3, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372594
    iget-object v3, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 372595
    iput-object v2, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    .line 372596
    iget-object v2, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372597
    goto :goto_0

    .line 372598
    :cond_0
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    iget-object v2, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372599
    invoke-static {p0, p2}, LX/26w;->a(LX/26w;LX/0oc;)[I

    move-result-object v0

    .line 372600
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    aput v1, v0, p3

    .line 372601
    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 372581
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 372582
    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "yes"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 372583
    :cond_0
    const/4 v0, 0x1

    .line 372584
    :goto_0
    return v0

    .line 372585
    :cond_1
    const-string v1, "false"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "no"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 372586
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 372587
    :cond_3
    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0}, Ljava/lang/NumberFormatException;-><init>()V

    throw v0
.end method

.method public static a(LX/26w;LX/0oc;)[I
    .locals 3

    .prologue
    .line 372577
    sget-object v0, LX/2XY;->a:[I

    invoke-virtual {p1}, LX/0oc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 372578
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal authority: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 372579
    :pswitch_0
    iget-object v0, p0, LX/26w;->b:[I

    .line 372580
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/26w;->c:[I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(LX/0oc;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 372571
    invoke-static {p0, p1}, LX/26w;->a(LX/26w;LX/0oc;)[I

    move-result-object v0

    .line 372572
    aget v0, v0, p2

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 372573
    sget-object v0, LX/0oc;->EFFECTIVE:LX/0oc;

    if-eq p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0Tp;->a(Z)V

    .line 372574
    return-void

    :cond_0
    move v0, v2

    .line 372575
    goto :goto_0

    :cond_1
    move v1, v2

    .line 372576
    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/nio/ByteBuffer;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 372548
    iget-object v0, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    .line 372549
    iget-object v0, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    const/16 v3, 0x14

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372550
    iget-object v0, p0, LX/26w;->b:[I

    array-length v3, v0

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    .line 372551
    iget-object v5, p0, LX/26w;->c:[I

    aget v5, v5, v0

    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    .line 372552
    iget-object v5, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    iget-object v6, p0, LX/26w;->b:[I

    aget v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 372553
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 372554
    :cond_0
    iget-object v5, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    iget-object v6, p0, LX/26w;->c:[I

    aget v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 372555
    :cond_1
    iget-object v0, p0, LX/26w;->b:[I

    array-length v5, v0

    move v3, v1

    move v0, v2

    :goto_2
    if-ge v3, v5, :cond_3

    .line 372556
    iget-object v6, p0, LX/26w;->b:[I

    aget v6, v6, v3

    .line 372557
    if-ltz v6, :cond_2

    move v0, v1

    .line 372558
    :cond_2
    iget-object v7, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 372559
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 372560
    :cond_3
    iget-object v3, p0, LX/26w;->c:[I

    array-length v5, v3

    move v3, v1

    :goto_3
    if-ge v3, v5, :cond_4

    .line 372561
    iget-object v6, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    iget-object v7, p0, LX/26w;->c:[I

    aget v7, v7, v3

    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 372562
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 372563
    :cond_4
    iget-object v3, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372564
    iget-object v3, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    .line 372565
    iget-object v5, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372566
    iget-object v5, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 372567
    iget-object v3, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    const/16 v5, 0x10

    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372568
    iget-object v3, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_5

    move v1, v2

    :cond_5
    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 372569
    iget-object v0, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372570
    iget-object v0, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 372544
    sget-object v0, LX/0oc;->OVERRIDE:LX/0oc;

    invoke-direct {p0, v0, p1}, LX/26w;->b(LX/0oc;I)V

    .line 372545
    sget-object v0, LX/0oc;->OVERRIDE:LX/0oc;

    invoke-static {p0, v0}, LX/26w;->a(LX/26w;LX/0oc;)[I

    move-result-object v0

    .line 372546
    const/4 v1, -0x2

    aput v1, v0, p1

    .line 372547
    return-void
.end method

.method public final a(LX/0oc;IF)V
    .locals 1

    .prologue
    .line 372514
    const/4 v0, 0x4

    invoke-direct {p0, v0, p1, p2}, LX/26w;->a(ILX/0oc;I)V

    .line 372515
    iget-object v0, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 372516
    return-void
.end method

.method public final a(LX/0oc;II)V
    .locals 1

    .prologue
    .line 372541
    const/4 v0, 0x4

    invoke-direct {p0, v0, p1, p2}, LX/26w;->a(ILX/0oc;I)V

    .line 372542
    iget-object v0, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 372543
    return-void
.end method

.method public final a(LX/0oc;IJ)V
    .locals 1

    .prologue
    .line 372538
    const/16 v0, 0x8

    invoke-direct {p0, v0, p1, p2}, LX/26w;->a(ILX/0oc;I)V

    .line 372539
    iget-object v0, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p3, p4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 372540
    return-void
.end method

.method public final a(LX/0oc;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 372533
    sget-object v0, LX/0oe;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p3, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 372534
    array-length v1, v0

    add-int/lit8 v1, v1, 0x4

    invoke-direct {p0, v1, p1, p2}, LX/26w;->a(ILX/0oc;I)V

    .line 372535
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 372536
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 372537
    return-void
.end method

.method public final a(LX/0oc;IZ)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 372529
    invoke-direct {p0, v0, p1, p2}, LX/26w;->a(ILX/0oc;I)V

    .line 372530
    iget-object v1, p0, LX/26w;->d:Ljava/nio/ByteBuffer;

    if-eqz p3, :cond_0

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 372531
    return-void

    .line 372532
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0oc;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 6

    .prologue
    .line 372517
    packed-switch p5, :pswitch_data_0

    .line 372518
    :try_start_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 372519
    :catch_0
    move-exception v0

    .line 372520
    const-string v1, "Could not coerce %s to type %d for %s.%s"

    .line 372521
    sget-object v2, LX/26w;->a:Ljava/lang/Class;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p6, v3, v4

    const/4 v4, 0x1

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p2, v3, v4

    const/4 v4, 0x3

    aput-object p3, v3, v4

    invoke-static {v2, v0, v1, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 372522
    :goto_0
    return-void

    .line 372523
    :pswitch_0
    :try_start_1
    invoke-static {p6}, LX/26w;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, p1, p4, v0}, LX/26w;->a(LX/0oc;IZ)V

    goto :goto_0

    .line 372524
    :pswitch_1
    invoke-virtual {p0, p1, p4, p6}, LX/26w;->a(LX/0oc;ILjava/lang/String;)V

    goto :goto_0

    .line 372525
    :pswitch_2
    invoke-static {p6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p0, p1, p4, v0}, LX/26w;->a(LX/0oc;IF)V

    goto :goto_0

    .line 372526
    :pswitch_3
    invoke-static {p6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, p1, p4, v0}, LX/26w;->a(LX/0oc;II)V

    goto :goto_0

    .line 372527
    :pswitch_4
    invoke-static {p6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0, p1, p4, v0, v1}, LX/26w;->a(LX/0oc;IJ)V

    goto :goto_0

    .line 372528
    :pswitch_5
    invoke-virtual {p0, p1, p4, p6}, LX/26w;->a(LX/0oc;ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
