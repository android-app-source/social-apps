.class public final LX/3Jp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:F

.field public e:F

.field public f:F

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3Jt;",
            ">;"
        }
    .end annotation
.end field

.field public h:[[[F

.field public i:I

.field public j:Landroid/graphics/Paint$Cap;

.field public k:LX/3K1;

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3Jf;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/3kN;

.field public n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 548055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 548056
    const/4 v0, 0x0

    iput v0, p0, LX/3Jp;->e:F

    .line 548057
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, LX/3Jp;->f:F

    .line 548058
    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    iput-object v0, p0, LX/3Jp;->j:Landroid/graphics/Paint$Cap;

    return-void
.end method


# virtual methods
.method public final a()LX/3K1;
    .locals 15

    .prologue
    .line 548059
    new-instance v0, LX/3K1;

    iget-object v1, p0, LX/3Jp;->a:Ljava/lang/String;

    iget v2, p0, LX/3Jp;->b:I

    iget v3, p0, LX/3Jp;->c:I

    iget v4, p0, LX/3Jp;->d:F

    iget v5, p0, LX/3Jp;->e:F

    iget v6, p0, LX/3Jp;->f:F

    iget-object v7, p0, LX/3Jp;->g:Ljava/util/List;

    iget-object v8, p0, LX/3Jp;->h:[[[F

    iget v9, p0, LX/3Jp;->i:I

    iget-object v10, p0, LX/3Jp;->j:Landroid/graphics/Paint$Cap;

    iget-object v11, p0, LX/3Jp;->k:LX/3K1;

    iget-object v12, p0, LX/3Jp;->l:Ljava/util/List;

    iget-object v13, p0, LX/3Jp;->m:LX/3kN;

    iget-object v14, p0, LX/3Jp;->n:Ljava/lang/String;

    invoke-direct/range {v0 .. v14}, LX/3K1;-><init>(Ljava/lang/String;IIFFFLjava/util/List;[[[FILandroid/graphics/Paint$Cap;LX/3K1;Ljava/util/List;LX/3kN;Ljava/lang/String;)V

    return-object v0
.end method
