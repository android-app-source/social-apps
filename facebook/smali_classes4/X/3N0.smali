.class public LX/3N0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/3N0;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2P7;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 557776
    const-class v0, LX/3N0;

    sput-object v0, LX/3N0;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/2P7;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/2P7;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 557777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 557778
    iput-object p1, p0, LX/3N0;->b:LX/0Or;

    .line 557779
    iput-object p2, p0, LX/3N0;->c:LX/2P7;

    .line 557780
    return-void
.end method

.method public static a(LX/0QB;)LX/3N0;
    .locals 5

    .prologue
    .line 557781
    sget-object v0, LX/3N0;->d:LX/3N0;

    if-nez v0, :cond_1

    .line 557782
    const-class v1, LX/3N0;

    monitor-enter v1

    .line 557783
    :try_start_0
    sget-object v0, LX/3N0;->d:LX/3N0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 557784
    if-eqz v2, :cond_0

    .line 557785
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 557786
    new-instance v4, LX/3N0;

    const/16 v3, 0x274b

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2P7;->b(LX/0QB;)LX/2P7;

    move-result-object v3

    check-cast v3, LX/2P7;

    invoke-direct {v4, p0, v3}, LX/3N0;-><init>(LX/0Or;LX/2P7;)V

    .line 557787
    move-object v0, v4

    .line 557788
    sput-object v0, LX/3N0;->d:LX/3N0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 557789
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 557790
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 557791
    :cond_1
    sget-object v0, LX/3N0;->d:LX/3N0;

    return-object v0

    .line 557792
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 557793
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JJ)V
    .locals 7

    .prologue
    .line 557794
    iget-object v0, p0, LX/3N0;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 557795
    const v0, 0xff35ad5

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 557796
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 557797
    const-string v2, "montage_thread_fbid"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 557798
    const-string v2, "thread_users"

    const-string v3, "montage_thread_fbid=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 557799
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 557800
    const v0, -0x6cff2558

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 557801
    return-void

    .line 557802
    :catch_0
    move-exception v0

    .line 557803
    :try_start_1
    sget-object v2, LX/3N0;->a:Ljava/lang/Class;

    const-string v3, "SQLException"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 557804
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 557805
    :catchall_0
    move-exception v0

    const v2, -0x72bb0447

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 557806
    iget-object v0, p0, LX/3N0;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 557807
    const v0, 0x150bdea3

    invoke-static {v5, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 557808
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 557809
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 557810
    const-string v1, "user_key"

    .line 557811
    iget-object v8, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v8, v8

    .line 557812
    invoke-virtual {v8}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557813
    iget-object v1, v0, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v1, v1

    .line 557814
    if-eqz v1, :cond_0

    .line 557815
    const-string v8, "first_name"

    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557816
    const-string v8, "last_name"

    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557817
    const-string v8, "name"

    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557818
    :cond_0
    const-string v1, "username"

    .line 557819
    iget-object v8, v0, Lcom/facebook/user/model/User;->g:Ljava/lang/String;

    move-object v8, v8

    .line 557820
    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557821
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 557822
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v8

    .line 557823
    new-instance v10, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v10, v1}, LX/162;-><init>(LX/0mC;)V

    .line 557824
    invoke-virtual {v8}, Lcom/facebook/user/model/PicSquare;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    const/4 v1, 0x0

    move v9, v1

    :goto_1
    if-ge v9, v12, :cond_1

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/PicSquareUrlWithSize;

    .line 557825
    new-instance v13, LX/0m9;

    sget-object p1, LX/0mC;->a:LX/0mC;

    invoke-direct {v13, p1}, LX/0m9;-><init>(LX/0mC;)V

    .line 557826
    const-string p1, "url"

    iget-object v8, v1, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    invoke-virtual {v13, p1, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 557827
    const-string p1, "size"

    iget v8, v1, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    invoke-virtual {v13, p1, v8}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 557828
    move-object v1, v13

    .line 557829
    invoke-virtual {v10, v1}, LX/162;->a(LX/0lF;)LX/162;

    .line 557830
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto :goto_1

    .line 557831
    :cond_1
    move-object v1, v10

    .line 557832
    const-string v8, "profile_pic_square"

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557833
    :cond_2
    const-string v8, "is_messenger_user"

    .line 557834
    iget-boolean v1, v0, Lcom/facebook/user/model/User;->s:Z

    move v1, v1

    .line 557835
    if-eqz v1, :cond_9

    move v1, v3

    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 557836
    const-string v8, "is_commerce"

    .line 557837
    iget-boolean v1, v0, Lcom/facebook/user/model/User;->t:Z

    move v1, v1

    .line 557838
    if-eqz v1, :cond_a

    move v1, v3

    :goto_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 557839
    const-string v8, "is_partial"

    .line 557840
    iget-boolean v1, v0, Lcom/facebook/user/model/User;->F:Z

    move v1, v1

    .line 557841
    if-eqz v1, :cond_b

    move v1, v3

    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 557842
    const-string v1, "user_rank"

    .line 557843
    iget v8, v0, Lcom/facebook/user/model/User;->m:F

    move v8, v8

    .line 557844
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 557845
    const-string v1, "profile_type"

    .line 557846
    iget-object v8, v0, Lcom/facebook/user/model/User;->r:Ljava/lang/String;

    move-object v8, v8

    .line 557847
    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557848
    const-string v1, "is_blocked_by_viewer"

    .line 557849
    iget-boolean v8, v0, Lcom/facebook/user/model/User;->I:Z

    move v8, v8

    .line 557850
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 557851
    const-string v1, "is_message_blocked_by_viewer"

    .line 557852
    iget-boolean v8, v0, Lcom/facebook/user/model/User;->J:Z

    move v8, v8

    .line 557853
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 557854
    const-string v8, "commerce_page_type"

    .line 557855
    iget-object v1, v0, Lcom/facebook/user/model/User;->u:LX/4nY;

    move-object v1, v1

    .line 557856
    if-nez v1, :cond_c

    move-object v1, v2

    :goto_5
    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557857
    const-string v1, "can_viewer_message"

    .line 557858
    iget-boolean v8, v0, Lcom/facebook/user/model/User;->L:Z

    move v8, v8

    .line 557859
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 557860
    const-string v8, "commerce_page_settings"

    .line 557861
    iget-object v1, v0, Lcom/facebook/user/model/User;->v:LX/0Px;

    move-object v1, v1

    .line 557862
    if-nez v1, :cond_d

    move-object v1, v2

    :goto_6
    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557863
    const-string v1, "is_friend"

    .line 557864
    iget-boolean v8, v0, Lcom/facebook/user/model/User;->y:Z

    move v8, v8

    .line 557865
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 557866
    const-string v1, "last_fetch_time"

    .line 557867
    iget-wide v10, v0, Lcom/facebook/user/model/User;->M:J

    move-wide v8, v10

    .line 557868
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 557869
    const-string v1, "montage_thread_fbid"

    .line 557870
    iget-wide v10, v0, Lcom/facebook/user/model/User;->N:J

    move-wide v8, v10

    .line 557871
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557872
    const-string v8, "can_see_viewer_montage_thread"

    .line 557873
    iget-boolean v1, v0, Lcom/facebook/user/model/User;->O:Z

    move v1, v1

    .line 557874
    if-eqz v1, :cond_e

    move v1, v3

    :goto_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 557875
    const-string v1, "is_messenger_bot"

    .line 557876
    iget-boolean v8, v0, Lcom/facebook/user/model/User;->A:Z

    move v8, v8

    .line 557877
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 557878
    const-string v1, "is_vc_endpoint"

    .line 557879
    iget-boolean v8, v0, Lcom/facebook/user/model/User;->B:Z

    move v8, v8

    .line 557880
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 557881
    const-string v1, "is_messenger_promotion_blocked_by_viewer"

    .line 557882
    iget-boolean v8, v0, Lcom/facebook/user/model/User;->K:Z

    move v8, v8

    .line 557883
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 557884
    const-string v1, "is_receiving_subscription_messages"

    .line 557885
    iget-boolean v8, v0, Lcom/facebook/user/model/User;->S:Z

    move v8, v8

    .line 557886
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 557887
    const-string v1, "is_messenger_platform_bot"

    .line 557888
    iget-boolean v8, v0, Lcom/facebook/user/model/User;->T:Z

    move v8, v8

    .line 557889
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 557890
    iget-object v1, v0, Lcom/facebook/user/model/User;->d:LX/0Px;

    move-object v1, v1

    .line 557891
    if-eqz v1, :cond_4

    .line 557892
    iget-object v1, v0, Lcom/facebook/user/model/User;->d:LX/0Px;

    move-object v1, v1

    .line 557893
    new-instance v10, LX/162;

    sget-object v8, LX/0mC;->a:LX/0mC;

    invoke-direct {v10, v8}, LX/162;-><init>(LX/0mC;)V

    .line 557894
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v11

    const/4 v8, 0x0

    move v9, v8

    :goto_8
    if-ge v9, v11, :cond_3

    invoke-virtual {v1, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/user/model/UserCustomTag;

    .line 557895
    new-instance v12, LX/0m9;

    sget-object v13, LX/0mC;->a:LX/0mC;

    invoke-direct {v12, v13}, LX/0m9;-><init>(LX/0mC;)V

    .line 557896
    const-string v13, "id"

    iget-object p1, v8, Lcom/facebook/user/model/UserCustomTag;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 557897
    const-string v13, "name"

    iget-object p1, v8, Lcom/facebook/user/model/UserCustomTag;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 557898
    const-string v13, "color"

    iget p1, v8, Lcom/facebook/user/model/UserCustomTag;->c:I

    invoke-virtual {v12, v13, p1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 557899
    const-string v13, "fillColor"

    iget p1, v8, Lcom/facebook/user/model/UserCustomTag;->d:I

    invoke-virtual {v12, v13, p1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 557900
    const-string v13, "borderColor"

    iget p1, v8, Lcom/facebook/user/model/UserCustomTag;->e:I

    invoke-virtual {v12, v13, p1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 557901
    move-object v8, v12

    .line 557902
    invoke-virtual {v10, v8}, LX/162;->a(LX/0lF;)LX/162;

    .line 557903
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_8

    .line 557904
    :cond_3
    move-object v1, v10

    .line 557905
    const-string v8, "user_custom_tags"

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557906
    :cond_4
    iget-object v1, v0, Lcom/facebook/user/model/User;->R:LX/0Px;

    move-object v1, v1

    .line 557907
    if-eqz v1, :cond_5

    .line 557908
    iget-object v1, v0, Lcom/facebook/user/model/User;->R:LX/0Px;

    move-object v1, v1

    .line 557909
    invoke-static {v1}, LX/4gg;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 557910
    const-string v8, "user_call_to_actions"

    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557911
    :cond_5
    iget-object v1, v0, Lcom/facebook/user/model/User;->U:LX/0Px;

    move-object v1, v1

    .line 557912
    if-eqz v1, :cond_6

    .line 557913
    iget-object v1, v0, Lcom/facebook/user/model/User;->U:LX/0Px;

    move-object v1, v1

    .line 557914
    invoke-static {v1}, LX/4gg;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 557915
    const-string v8, "structured_menu_call_to_actions"

    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557916
    :cond_6
    iget-object v1, v0, Lcom/facebook/user/model/User;->ad:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    move-object v1, v1

    .line 557917
    if-eqz v1, :cond_7

    .line 557918
    iget-object v1, v0, Lcom/facebook/user/model/User;->ad:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    move-object v1, v1

    .line 557919
    if-nez v1, :cond_11

    .line 557920
    const/4 v8, 0x0

    .line 557921
    :goto_9
    move-object v1, v8

    .line 557922
    const-string v8, "extension_properties"

    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557923
    :cond_7
    const-string v8, "extension_resume_url"

    .line 557924
    iget-object v1, v0, Lcom/facebook/user/model/User;->W:Landroid/net/Uri;

    move-object v1, v1

    .line 557925
    if-nez v1, :cond_f

    move-object v1, v2

    :goto_a
    invoke-virtual {v7, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557926
    const-string v1, "extension_resume_text"

    .line 557927
    iget-object v8, v0, Lcom/facebook/user/model/User;->X:Ljava/lang/String;

    move-object v8, v8

    .line 557928
    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557929
    const-string v1, "extension_payment_policy"

    .line 557930
    iget-object v8, v0, Lcom/facebook/user/model/User;->Y:Ljava/lang/String;

    move-object v8, v8

    .line 557931
    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557932
    const-string v1, "structured_menu_badge_count"

    .line 557933
    iget v8, v0, Lcom/facebook/user/model/User;->Z:I

    move v8, v8

    .line 557934
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 557935
    const-string v1, "does_accept_user_feedback"

    .line 557936
    iget-boolean v8, v0, Lcom/facebook/user/model/User;->ab:Z

    move v8, v8

    .line 557937
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 557938
    iget-object v1, v0, Lcom/facebook/user/model/User;->af:LX/0XK;

    move-object v1, v1

    .line 557939
    if-eqz v1, :cond_8

    .line 557940
    const-string v1, "viewer_connection_status"

    .line 557941
    iget-object v8, v0, Lcom/facebook/user/model/User;->af:LX/0XK;

    move-object v0, v8

    .line 557942
    iget-object v0, v0, LX/0XK;->dbValue:Ljava/lang/String;

    invoke-virtual {v7, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557943
    :cond_8
    const-string v0, "thread_users"

    const-string v1, ""

    const v8, 0x5ef60b89

    invoke-static {v8}, LX/03h;->a(I)V

    invoke-virtual {v5, v0, v1, v7}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x3e5851a1

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 557944
    :catch_0
    move-exception v0

    .line 557945
    :try_start_1
    sget-object v1, LX/3N0;->a:Ljava/lang/Class;

    const-string v2, "SQLException"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 557946
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 557947
    :catchall_0
    move-exception v0

    const v1, -0x6f86996b

    invoke-static {v5, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    :cond_9
    move v1, v4

    .line 557948
    goto/16 :goto_2

    :cond_a
    move v1, v4

    .line 557949
    goto/16 :goto_3

    :cond_b
    move v1, v4

    .line 557950
    goto/16 :goto_4

    .line 557951
    :cond_c
    :try_start_2
    iget-object v1, v0, Lcom/facebook/user/model/User;->u:LX/4nY;

    move-object v1, v1

    .line 557952
    invoke-virtual {v1}, LX/4nY;->name()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    .line 557953
    :cond_d
    iget-object v1, v0, Lcom/facebook/user/model/User;->v:LX/0Px;

    move-object v1, v1

    .line 557954
    new-instance v9, LX/0m9;

    sget-object v10, LX/0mC;->a:LX/0mC;

    invoke-direct {v9, v10}, LX/0m9;-><init>(LX/0mC;)V

    .line 557955
    const-string v10, "commerce_faq_enabled"

    sget-object v11, LX/4nX;->COMMERCE_FAQ_ENABLED:LX/4nX;

    invoke-virtual {v1, v11}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v11

    invoke-virtual {v9, v10, v11}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 557956
    const-string v10, "in_messenger_shopping_enabled"

    sget-object v11, LX/4nX;->IN_MESSENGER_SHOPPING_ENABLED:LX/4nX;

    invoke-virtual {v1, v11}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v11

    invoke-virtual {v9, v10, v11}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 557957
    const-string v10, "commerce_nux_enabled"

    sget-object v11, LX/4nX;->COMMERCE_NUX_ENABLED:LX/4nX;

    invoke-virtual {v1, v11}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v11

    invoke-virtual {v9, v10, v11}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 557958
    const-string v10, "structured_menu_enabled"

    sget-object v11, LX/4nX;->STRUCTURED_MENU_ENABLED:LX/4nX;

    invoke-virtual {v1, v11}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v11

    invoke-virtual {v9, v10, v11}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 557959
    const-string v10, "user_control_topic_manage_enabled"

    sget-object v11, LX/4nX;->USER_CONTROL_TOPIC_MANAGE_ENABLED:LX/4nX;

    invoke-virtual {v1, v11}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v11

    invoke-virtual {v9, v10, v11}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 557960
    const-string v10, "null_state_cta_button_always_enabled"

    sget-object v11, LX/4nX;->NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED:LX/4nX;

    invoke-virtual {v1, v11}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v11

    invoke-virtual {v9, v10, v11}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 557961
    move-object v1, v9

    .line 557962
    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_6

    :cond_e
    move v1, v4

    .line 557963
    goto/16 :goto_7

    .line 557964
    :cond_f
    iget-object v1, v0, Lcom/facebook/user/model/User;->W:Landroid/net/Uri;

    move-object v1, v1

    .line 557965
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_a

    .line 557966
    :cond_10
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 557967
    const v0, 0x6ed7ff28

    invoke-static {v5, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 557968
    return-void

    .line 557969
    :cond_11
    new-instance v8, LX/0m9;

    sget-object v9, LX/0mC;->a:LX/0mC;

    invoke-direct {v8, v9}, LX/0m9;-><init>(LX/0mC;)V

    .line 557970
    const-string v9, "is_structured_menu_type_message_hidden"

    iget-boolean v10, v1, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->a:Z

    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 557971
    const-string v9, "cart"

    iget-object v10, v1, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->c:Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;

    .line 557972
    if-nez v10, :cond_12

    .line 557973
    const/4 v11, 0x0

    .line 557974
    :goto_b
    move-object v10, v11

    .line 557975
    invoke-virtual {v8, v9, v10}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 557976
    const-string v9, "top_level_ctas"

    iget-object v10, v1, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->b:LX/0Px;

    invoke-static {v10}, LX/4gg;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 557977
    invoke-virtual {v8}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_9

    .line 557978
    :cond_12
    new-instance v11, LX/0m9;

    sget-object v12, LX/0mC;->a:LX/0mC;

    invoke-direct {v11, v12}, LX/0m9;-><init>(LX/0mC;)V

    .line 557979
    const-string v12, "cart_item_count"

    iget v13, v10, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->a:I

    invoke-virtual {v11, v12, v13}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 557980
    const-string v12, "cart_url"

    iget-object v13, v10, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->b:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 557981
    const-string v12, "cart_native_url"

    iget-object v13, v10, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->c:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_b
.end method
