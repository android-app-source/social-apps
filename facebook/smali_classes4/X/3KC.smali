.class public final LX/3KC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qf;
.implements LX/1qg;


# instance fields
.field public final a:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/2wX;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/2wX;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 548363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 548364
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/3KC;->a:Lcom/google/common/util/concurrent/SettableFuture;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 548365
    const/4 v0, 0x0

    iput-object v0, p0, LX/3KC;->b:LX/2wX;

    .line 548366
    iget-object v0, p0, LX/3KC;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 548367
    iget-object v0, p0, LX/3KC;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onConnectionSuspended: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 548368
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 548369
    iget-object v0, p0, LX/3KC;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3KC;->b:LX/2wX;

    if-eqz v0, :cond_0

    .line 548370
    iget-object v0, p0, LX/3KC;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, LX/3KC;->b:LX/2wX;

    const v2, 0x766523f8

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 548371
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/3KC;->b:LX/2wX;

    .line 548372
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 4

    .prologue
    .line 548373
    const/4 v0, 0x0

    iput-object v0, p0, LX/3KC;->b:LX/2wX;

    .line 548374
    iget-object v0, p0, LX/3KC;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 548375
    iget-object v0, p0, LX/3KC;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onConnectionFailed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 548376
    :cond_0
    return-void
.end method
