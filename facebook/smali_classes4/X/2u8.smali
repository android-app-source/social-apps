.class public LX/2u8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6bX;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 475525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475526
    iput-object p1, p0, LX/2u8;->a:Landroid/content/Context;

    .line 475527
    iput-object p2, p0, LX/2u8;->b:Lcom/facebook/content/SecureContextHelper;

    .line 475528
    return-void
.end method

.method public static b(LX/0QB;)LX/2u8;
    .locals 3

    .prologue
    .line 475529
    new-instance v2, LX/2u8;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/2u8;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 475530
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 475531
    sget-object v1, LX/007;->b:Ljava/lang/String;

    move-object v1, v1

    .line 475532
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 475533
    :cond_0
    :goto_0
    return v0

    .line 475534
    :cond_1
    const-string v1, "compose"

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 475535
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "autocompose"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 475536
    new-instance v1, Landroid/content/Intent;

    sget-object v2, LX/3GK;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 475537
    iget-object v0, p0, LX/2u8;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/2u8;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 475538
    const/4 v0, 0x1

    goto :goto_0
.end method
