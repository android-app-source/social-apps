.class public abstract LX/22z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/api/feed/FetchFeedResult;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 361478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361479
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/22z;->a:Z

    return-void
.end method


# virtual methods
.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 361480
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/22z;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 361481
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/22z;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361482
    monitor-exit p0

    return-void

    .line 361483
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
