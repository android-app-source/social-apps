.class public LX/2pJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 467964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 467965
    return-void
.end method

.method public static a(FFFF)Landroid/view/animation/Interpolator;
    .locals 2

    .prologue
    .line 467966
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 467967
    new-instance v0, Landroid/view/animation/PathInterpolator;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/view/animation/PathInterpolator;-><init>(FFFF)V

    move-object v0, v0

    .line 467968
    :goto_0
    return-object v0

    .line 467969
    :cond_0
    new-instance v0, LX/2pK;

    invoke-direct {v0, p0, p1, p2, p3}, LX/2pK;-><init>(FFFF)V

    move-object v0, v0

    .line 467970
    goto :goto_0
.end method
