.class public LX/26I;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/17Q;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1CK;

.field private final e:LX/26J;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17Q;LX/0Or;LX/1CK;LX/26J;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/1CK;",
            "LX/26J;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 371870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 371871
    iput-object p1, p0, LX/26I;->a:LX/0Zb;

    .line 371872
    iput-object p2, p0, LX/26I;->b:LX/17Q;

    .line 371873
    iput-object p3, p0, LX/26I;->c:LX/0Or;

    .line 371874
    iput-object p4, p0, LX/26I;->d:LX/1CK;

    .line 371875
    iput-object p5, p0, LX/26I;->e:LX/26J;

    .line 371876
    return-void
.end method

.method public static a(LX/0QB;)LX/26I;
    .locals 9

    .prologue
    .line 371877
    const-class v1, LX/26I;

    monitor-enter v1

    .line 371878
    :try_start_0
    sget-object v0, LX/26I;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 371879
    sput-object v2, LX/26I;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 371880
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371881
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 371882
    new-instance v3, LX/26I;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/1CK;->a(LX/0QB;)LX/1CK;

    move-result-object v7

    check-cast v7, LX/1CK;

    invoke-static {v0}, LX/26J;->a(LX/0QB;)LX/26J;

    move-result-object v8

    check-cast v8, LX/26J;

    invoke-direct/range {v3 .. v8}, LX/26I;-><init>(LX/0Zb;LX/17Q;LX/0Or;LX/1CK;LX/26J;)V

    .line 371883
    move-object v0, v3

    .line 371884
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 371885
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/26I;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 371886
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 371887
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 371888
    iget-object v0, p0, LX/26I;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 371889
    invoke-static {v0}, LX/3Bc;->a(Landroid/content/Context;)V

    .line 371890
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;ILandroid/view/View;LX/1PT;Ljava/lang/String;ZLX/9hN;LX/1Pq;)V
    .locals 7
    .param p7    # LX/9hN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I",
            "Landroid/view/View;",
            "LX/1PT;",
            "Ljava/lang/String;",
            "Z",
            "LX/9hN;",
            "LX/1Pq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 371891
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 371892
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 371893
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 371894
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 371895
    sget-object v3, LX/BqA;->a:[I

    invoke-interface {p4}, LX/1PT;->a()LX/1Qt;

    move-result-object v4

    invoke-virtual {v4}, LX/1Qt;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 371896
    sget-object v3, LX/1EO;->NEWSFEED:LX/1EO;

    :goto_0
    move-object v3, v3

    .line 371897
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    if-nez v4, :cond_b

    .line 371898
    :cond_0
    :goto_1
    iget-object v1, p0, LX/26I;->d:LX/1CK;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, p8, v3}, LX/1CK;->a(Lcom/facebook/graphql/model/FeedUnit;LX/1Pq;Ljava/lang/Boolean;)V

    .line 371899
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->t()Ljava/lang/String;

    move-result-object v2

    .line 371900
    if-nez v2, :cond_9

    .line 371901
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 371902
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 371903
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 371904
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    .line 371905
    if-eqz v1, :cond_1

    .line 371906
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 371907
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 371908
    :cond_2
    const-class v1, LX/23W;

    new-instance v2, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v1, v2}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v1

    move-object v1, v1

    .line 371909
    if-eqz p6, :cond_6

    move-object v0, v1

    .line 371910
    :goto_3
    new-instance v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-direct {v2}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;-><init>()V

    .line 371911
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 371912
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 371913
    const-string v5, "story_id"

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 371914
    const-string v5, "story_cache_id"

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 371915
    const-string v5, "starting_index"

    invoke-virtual {v3, v5, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 371916
    const-string v5, "starting_media_id"

    invoke-virtual {v3, v5, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 371917
    const-string v5, "media_fetcher_rule"

    invoke-virtual {v3, v5, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 371918
    const-string v5, "gallery_fetcher_rule"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 371919
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 371920
    const-string v5, "target_id"

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 371921
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 371922
    const-string v5, "target_type"

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 371923
    :cond_4
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 371924
    move-object v0, v2

    .line 371925
    if-nez p4, :cond_e

    .line 371926
    sget-object v1, LX/74S;->UNKNOWN:LX/74S;

    .line 371927
    :goto_4
    move-object v1, v1

    .line 371928
    new-instance v2, LX/9eM;

    invoke-direct {v2, v1}, LX/9eM;-><init>(LX/74S;)V

    sget-object v1, LX/31M;->RIGHT:LX/31M;

    invoke-virtual {v2, v1}, LX/9eM;->a(LX/31M;)LX/9eM;

    move-result-object v1

    sget-object v2, LX/31M;->RIGHT:LX/31M;

    invoke-virtual {v2}, LX/31M;->flag()I

    move-result v2

    .line 371929
    iput v2, v1, LX/9eM;->f:I

    .line 371930
    move-object v1, v1

    .line 371931
    iget-object v2, v1, LX/9eM;->b:Ljava/lang/String;

    if-nez v2, :cond_f

    const/4 v2, 0x1

    :goto_5
    const-string v3, "Multiple start id\'s set"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 371932
    iput-object p5, v1, LX/9eM;->b:Ljava/lang/String;

    .line 371933
    move-object v1, v1

    .line 371934
    invoke-direct {p0}, LX/26I;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a05c2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 371935
    iput v2, v1, LX/9eM;->g:I

    .line 371936
    move-object v1, v1

    .line 371937
    invoke-direct {p0}, LX/26I;->a()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1}, LX/9eM;->a()Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v2, v0, v1, p7, v3}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->a(Landroid/content/Context;Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;LX/9hN;Landroid/content/DialogInterface$OnDismissListener;)Z

    move-result v1

    .line 371938
    if-nez v1, :cond_5

    .line 371939
    invoke-virtual {v0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->c()V

    .line 371940
    :cond_5
    return-void

    .line 371941
    :cond_6
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 371942
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_6
    if-ge v3, v6, :cond_8

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 371943
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p3

    if-eqz p3, :cond_7

    .line 371944
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object p3

    .line 371945
    if-eqz p3, :cond_7

    invoke-static {v2}, LX/1VO;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 371946
    invoke-virtual {v4, p3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 371947
    :cond_7
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 371948
    :cond_8
    const-class v2, LX/23W;

    new-instance v3, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v2, v3}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v2

    move-object v0, v2

    .line 371949
    goto/16 :goto_3

    .line 371950
    :cond_9
    new-instance v0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;

    const-string v1, "ALL"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v0

    .line 371951
    const-class v1, LX/23Z;

    invoke-static {v1, v0}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v0

    move-object v1, v0

    .line 371952
    if-eqz p6, :cond_a

    move-object v0, v1

    goto/16 :goto_3

    .line 371953
    :cond_a
    invoke-static {v2}, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;->b(Ljava/lang/String;)Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;

    move-result-object v0

    .line 371954
    const-class v3, LX/23Z;

    invoke-static {v3, v0}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v0

    move-object v0, v0

    .line 371955
    goto/16 :goto_3

    .line 371956
    :pswitch_0
    sget-object v3, LX/1EO;->TIMELINE:LX/1EO;

    goto/16 :goto_0

    .line 371957
    :pswitch_1
    sget-object v3, LX/1EO;->PERMALINK:LX/1EO;

    goto/16 :goto_0

    .line 371958
    :pswitch_2
    sget-object v3, LX/1EO;->GROUP:LX/1EO;

    goto/16 :goto_0

    .line 371959
    :pswitch_3
    sget-object v3, LX/1EO;->EVENT:LX/1EO;

    goto/16 :goto_0

    .line 371960
    :pswitch_4
    sget-object v3, LX/1EO;->PAGE:LX/1EO;

    goto/16 :goto_0

    .line 371961
    :cond_b
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 371962
    iget-object v5, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v6, v5

    .line 371963
    if-eqz v4, :cond_c

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v4, 0x1

    move v5, v4

    .line 371964
    :goto_7
    if-eqz v6, :cond_d

    invoke-static {v6}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    .line 371965
    :goto_8
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v5, v4, v3}, LX/17Q;->a(Ljava/lang/String;ZLX/0lF;LX/1EO;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 371966
    invoke-static {v4, p3}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 371967
    iget-object v5, p0, LX/26I;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_1

    .line 371968
    :cond_c
    const/4 v4, 0x0

    move v5, v4

    goto :goto_7

    .line 371969
    :cond_d
    const/4 v4, 0x0

    goto :goto_8

    .line 371970
    :cond_e
    sget-object v1, LX/8wo;->b:[I

    invoke-interface {p4}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    invoke-virtual {v2}, LX/1Qt;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 371971
    sget-object v1, LX/74S;->OTHER:LX/74S;

    goto/16 :goto_4

    .line 371972
    :pswitch_5
    sget-object v1, LX/74S;->NEWSFEED:LX/74S;

    goto/16 :goto_4

    .line 371973
    :pswitch_6
    sget-object v1, LX/74S;->TIMELINE_WALL:LX/74S;

    goto/16 :goto_4

    .line 371974
    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method
