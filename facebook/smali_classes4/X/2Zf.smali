.class public LX/2Zf;
.super LX/0Tr;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Zf;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/2aL;)V
    .locals 2
    .param p2    # LX/0Tt;
        .annotation runtime Lcom/facebook/database/threadchecker/AllowAnyThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 422733
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const-string v1, "mds_cache_db"

    invoke-direct {p0, p1, p2, v0, v1}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 422734
    return-void
.end method

.method public static a(LX/0QB;)LX/2Zf;
    .locals 6

    .prologue
    .line 422735
    sget-object v0, LX/2Zf;->a:LX/2Zf;

    if-nez v0, :cond_1

    .line 422736
    const-class v1, LX/2Zf;

    monitor-enter v1

    .line 422737
    :try_start_0
    sget-object v0, LX/2Zf;->a:LX/2Zf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 422738
    if-eqz v2, :cond_0

    .line 422739
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 422740
    new-instance p0, LX/2Zf;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0sm;->a(LX/0QB;)LX/0Tt;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/2aL;->a(LX/0QB;)LX/2aL;

    move-result-object v5

    check-cast v5, LX/2aL;

    invoke-direct {p0, v3, v4, v5}, LX/2Zf;-><init>(Landroid/content/Context;LX/0Tt;LX/2aL;)V

    .line 422741
    move-object v0, p0

    .line 422742
    sput-object v0, LX/2Zf;->a:LX/2Zf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 422743
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 422744
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 422745
    :cond_1
    sget-object v0, LX/2Zf;->a:LX/2Zf;

    return-object v0

    .line 422746
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 422747
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
