.class public LX/2fj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0aG;

.field private final b:LX/1dw;


# direct methods
.method public constructor <init>(LX/0aG;LX/1dw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 447298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447299
    iput-object p1, p0, LX/2fj;->a:LX/0aG;

    .line 447300
    iput-object p2, p0, LX/2fj;->b:LX/1dw;

    .line 447301
    return-void
.end method

.method public static b(LX/0QB;)LX/2fj;
    .locals 3

    .prologue
    .line 447302
    new-instance v2, LX/2fj;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-static {p0}, LX/1dw;->a(LX/0QB;)LX/1dw;

    move-result-object v1

    check-cast v1, LX/1dw;

    invoke-direct {v2, v0, v1}, LX/2fj;-><init>(LX/0aG;LX/1dw;)V

    .line 447303
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/story/UpdateTimelineAppCollectionParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447304
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 447305
    const-string v1, "timelineAppCollectionParamsKey"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 447306
    iget-object v1, p0, LX/2fj;->a:LX/0aG;

    const-string v2, "update_timeline_app_collection_in_newsfeed"

    const v3, -0x95e656a

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 447307
    iget-object v1, p0, LX/2fj;->b:LX/1dw;

    invoke-virtual {v1, v0}, LX/1dw;->a(LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
