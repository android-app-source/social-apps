.class public LX/2Aj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:[LX/2VZ;

.field private final b:I


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/2Ak;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 377555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377556
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, LX/2Aj;->a(I)I

    move-result v0

    .line 377557
    iput v0, p0, LX/2Aj;->b:I

    .line 377558
    add-int/lit8 v2, v0, -0x1

    .line 377559
    new-array v3, v0, [LX/2VZ;

    .line 377560
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 377561
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Ak;

    .line 377562
    invoke-virtual {v1}, LX/2Ak;->hashCode()I

    move-result v5

    and-int/2addr v5, v2

    .line 377563
    new-instance v6, LX/2VZ;

    aget-object v7, v3, v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-direct {v6, v7, v1, v0}, LX/2VZ;-><init>(LX/2VZ;LX/2Ak;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    aput-object v6, v3, v5

    goto :goto_0

    .line 377564
    :cond_0
    iput-object v3, p0, LX/2Aj;->a:[LX/2VZ;

    .line 377565
    return-void
.end method

.method private static final a(I)I
    .locals 2

    .prologue
    .line 377566
    const/16 v0, 0x40

    if-gt p0, v0, :cond_0

    add-int v0, p0, p0

    .line 377567
    :goto_0
    const/16 v1, 0x8

    .line 377568
    :goto_1
    if-ge v1, v0, :cond_1

    .line 377569
    add-int/2addr v1, v1

    goto :goto_1

    .line 377570
    :cond_0
    shr-int/lit8 v0, p0, 0x2

    add-int/2addr v0, p0

    goto :goto_0

    .line 377571
    :cond_1
    return v1
.end method


# virtual methods
.method public final a(LX/2Ak;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ak;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 377572
    invoke-virtual {p1}, LX/2Ak;->hashCode()I

    move-result v0

    iget-object v2, p0, LX/2Aj;->a:[LX/2VZ;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    and-int/2addr v0, v2

    .line 377573
    iget-object v2, p0, LX/2Aj;->a:[LX/2VZ;

    aget-object v0, v2, v0

    .line 377574
    if-nez v0, :cond_0

    move-object v0, v1

    .line 377575
    :goto_0
    return-object v0

    .line 377576
    :cond_0
    iget-object v2, v0, LX/2VZ;->a:LX/2Ak;

    invoke-virtual {p1, v2}, LX/2Ak;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 377577
    iget-object v0, v0, LX/2VZ;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0

    .line 377578
    :cond_1
    iget-object v0, v0, LX/2VZ;->c:LX/2VZ;

    if-eqz v0, :cond_2

    .line 377579
    iget-object v2, v0, LX/2VZ;->a:LX/2Ak;

    invoke-virtual {p1, v2}, LX/2Ak;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 377580
    iget-object v0, v0, LX/2VZ;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 377581
    goto :goto_0
.end method
