.class public LX/2Rm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;",
        "Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410121
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 410138
    check-cast p1, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;

    .line 410139
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 410140
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "third_party_app_id"

    .line 410141
    iget-object v2, p1, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;->a:Ljava/lang/String;

    move-object v2, v2

    .line 410142
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410143
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "app_context"

    const-string v2, "platform_share"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410144
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410145
    new-instance v0, LX/14N;

    const-string v1, "get_app_permissions_method"

    const-string v2, "GET"

    const-string v3, "me/permissions"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 410122
    const/4 v2, 0x0

    .line 410123
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 410124
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 410125
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 410126
    if-eqz v0, :cond_0

    .line 410127
    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 410128
    :cond_0
    if-eqz v0, :cond_1

    .line 410129
    invoke-virtual {v0, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    .line 410130
    :cond_1
    if-eqz v0, :cond_3

    .line 410131
    invoke-virtual {v0}, LX/0lF;->j()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    .line 410132
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 410133
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 410134
    const-string v5, "installed"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 410135
    const/4 v0, 0x1

    move v1, v0

    goto :goto_0

    .line 410136
    :cond_2
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move v1, v2

    .line 410137
    :cond_4
    new-instance v0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;

    invoke-direct {v0, v1, v3}, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;-><init>(ZLjava/util/List;)V

    return-object v0
.end method
