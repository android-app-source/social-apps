.class public final LX/2cX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public mCollectionParams:Ljava/lang/String;

.field public mIdl:Ljava/lang/String;

.field public mInitialGlobalVersionId:J

.field public mRequiresSnapshot:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 441026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 441027
    const-string v0, ""

    iput-object v0, p0, LX/2cX;->mCollectionParams:Ljava/lang/String;

    .line 441028
    const-string v0, ""

    iput-object v0, p0, LX/2cX;->mIdl:Ljava/lang/String;

    .line 441029
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2cX;->mInitialGlobalVersionId:J

    .line 441030
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2cX;->mRequiresSnapshot:Z

    return-void
.end method


# virtual methods
.method public build()LX/2cY;
    .locals 2

    .prologue
    .line 441031
    new-instance v0, LX/2cY;

    invoke-direct {v0, p0}, LX/2cY;-><init>(LX/2cX;)V

    return-object v0
.end method
