.class public LX/2NI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2N8;


# direct methods
.method public constructor <init>(LX/2N8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 399165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399166
    iput-object p1, p0, LX/2NI;->a:LX/2N8;

    .line 399167
    return-void
.end method

.method public static a(LX/0QB;)LX/2NI;
    .locals 1

    .prologue
    .line 399164
    invoke-static {p0}, LX/2NI;->b(LX/0QB;)LX/2NI;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0lF;)Lcom/facebook/messaging/model/attachment/AttachmentImageMap;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 399137
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0lF;->o()Z

    move-result v1

    if-nez v1, :cond_1

    .line 399138
    :cond_0
    :goto_0
    return-object v0

    .line 399139
    :cond_1
    invoke-virtual {p1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    .line 399140
    invoke-static {}, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->newBuilder()LX/5dP;

    move-result-object v2

    .line 399141
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 399142
    iget-object v0, p0, LX/2NI;->a:LX/2N8;

    invoke-virtual {v0, v1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 399143
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v3

    .line 399144
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 399145
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 399146
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, LX/5dQ;->fromSerializedName(Ljava/lang/String;)LX/5dQ;

    move-result-object v1

    .line 399147
    if-eqz v1, :cond_2

    .line 399148
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 399149
    new-instance v4, LX/5dM;

    invoke-direct {v4}, LX/5dM;-><init>()V

    .line 399150
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 399151
    iget-object v5, p0, LX/2NI;->a:LX/2N8;

    invoke-virtual {v5, v0}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 399152
    const-string p1, "width"

    invoke-virtual {v5, p1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 399153
    const-string p1, "width"

    invoke-virtual {v5, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p1

    invoke-static {p1}, LX/16N;->d(LX/0lF;)I

    move-result p1

    .line 399154
    iput p1, v4, LX/5dM;->a:I

    .line 399155
    :cond_3
    const-string p1, "height"

    invoke-virtual {v5, p1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 399156
    const-string p1, "height"

    invoke-virtual {v5, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p1

    invoke-static {p1}, LX/16N;->d(LX/0lF;)I

    move-result p1

    .line 399157
    iput p1, v4, LX/5dM;->b:I

    .line 399158
    :cond_4
    const-string p1, "src"

    invoke-virtual {v5, p1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 399159
    const-string p1, "src"

    invoke-virtual {v5, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 399160
    iput-object v5, v4, LX/5dM;->c:Ljava/lang/String;

    .line 399161
    :cond_5
    invoke-virtual {v4}, LX/5dM;->d()Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v4

    move-object v0, v4

    .line 399162
    invoke-virtual {v2, v1, v0}, LX/5dP;->a(LX/5dQ;Lcom/facebook/messaging/model/attachment/ImageUrl;)LX/5dP;

    goto :goto_1

    .line 399163
    :cond_6
    invoke-virtual {v2}, LX/5dP;->b()Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Lcom/facebook/messaging/model/attachment/AttachmentImageMap;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 399080
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 399081
    if-eqz p0, :cond_1

    .line 399082
    iget-object v0, p0, Lcom/facebook/messaging/model/attachment/AttachmentImageMap;->a:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 399083
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 399084
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5dQ;

    iget-object v1, v1, LX/5dQ;->serializedName:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/ImageUrl;

    .line 399085
    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 399086
    if-eqz v0, :cond_0

    .line 399087
    const-string v5, "width"

    .line 399088
    iget p0, v0, Lcom/facebook/messaging/model/attachment/ImageUrl;->a:I

    move p0, p0

    .line 399089
    invoke-virtual {v4, v5, p0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 399090
    const-string v5, "height"

    .line 399091
    iget p0, v0, Lcom/facebook/messaging/model/attachment/ImageUrl;->b:I

    move p0, p0

    .line 399092
    invoke-virtual {v4, v5, p0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 399093
    const-string v5, "src"

    .line 399094
    iget-object p0, v0, Lcom/facebook/messaging/model/attachment/ImageUrl;->c:Ljava/lang/String;

    move-object p0, p0

    .line 399095
    invoke-virtual {v4, v5, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 399096
    :cond_0
    invoke-virtual {v4}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 399097
    invoke-virtual {v2, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 399098
    :cond_1
    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2NI;
    .locals 2

    .prologue
    .line 399135
    new-instance v1, LX/2NI;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v0

    check-cast v0, LX/2N8;

    invoke-direct {v1, v0}, LX/2NI;-><init>(LX/2N8;)V

    .line 399136
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/attachment/Attachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 399099
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399100
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 399101
    :goto_0
    return-object v0

    .line 399102
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 399103
    iget-object v0, p0, LX/2NI;->a:LX/2N8;

    invoke-virtual {v0, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 399104
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/0lF;

    .line 399105
    new-instance v0, LX/5dN;

    const-string v1, "id"

    invoke-virtual {v8, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, LX/5dN;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "fbid"

    invoke-virtual {v8, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399106
    iput-object v1, v0, LX/5dN;->c:Ljava/lang/String;

    .line 399107
    move-object v0, v0

    .line 399108
    const-string v1, "mime_type"

    invoke-virtual {v8, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399109
    iput-object v1, v0, LX/5dN;->d:Ljava/lang/String;

    .line 399110
    move-object v0, v0

    .line 399111
    const-string v1, "filename"

    invoke-virtual {v8, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 399112
    iput-object v1, v0, LX/5dN;->e:Ljava/lang/String;

    .line 399113
    move-object v0, v0

    .line 399114
    const-string v1, "file_size"

    invoke-virtual {v8, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->d(LX/0lF;)I

    move-result v1

    .line 399115
    iput v1, v0, LX/5dN;->f:I

    .line 399116
    move-object v11, v0

    .line 399117
    const-string v0, "image_data_width"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "image_data_height"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 399118
    const-string v0, "image_data_source"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    invoke-static {v0}, LX/5dT;->fromIntVal(I)LX/5dT;

    move-result-object v5

    .line 399119
    new-instance v0, Lcom/facebook/messaging/model/attachment/ImageData;

    const-string v1, "image_data_width"

    invoke-virtual {v8, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->d(LX/0lF;)I

    move-result v1

    const-string v2, "image_data_height"

    invoke-virtual {v8, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->d(LX/0lF;)I

    move-result v2

    const-string v3, "urls"

    invoke-virtual {v8, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-direct {p0, v3}, LX/2NI;->a(LX/0lF;)Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    move-result-object v3

    const-string v4, "image_animated_urls"

    invoke-virtual {v8, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-direct {p0, v4}, LX/2NI;->a(LX/0lF;)Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    move-result-object v4

    const-string v6, "render_as_sticker"

    invoke-virtual {v8, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->g(LX/0lF;)Z

    move-result v6

    const-string v7, "mini_preview"

    invoke-virtual {v8, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/attachment/ImageData;-><init>(IILcom/facebook/messaging/model/attachment/AttachmentImageMap;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;LX/5dT;ZLjava/lang/String;)V

    .line 399120
    iput-object v0, v11, LX/5dN;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    .line 399121
    :cond_1
    const-string v0, "video_data_width"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "video_data_height"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 399122
    const-string v0, "video_data_source"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    invoke-static {v0}, LX/5dX;->fromIntVal(I)LX/5dX;

    move-result-object v5

    .line 399123
    const/4 v7, 0x0

    .line 399124
    const-string v0, "video_data_thumbnail_url"

    invoke-virtual {v8, v0}, LX/0lF;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 399125
    const-string v0, "video_data_thumbnail_url"

    invoke-virtual {v8, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 399126
    :cond_2
    new-instance v0, Lcom/facebook/messaging/model/attachment/VideoData;

    const-string v1, "video_data_width"

    invoke-virtual {v8, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->d(LX/0lF;)I

    move-result v1

    const-string v2, "video_data_height"

    invoke-virtual {v8, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->d(LX/0lF;)I

    move-result v2

    const-string v3, "video_data_rotation"

    invoke-virtual {v8, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->d(LX/0lF;)I

    move-result v3

    const-string v4, "video_data_length"

    invoke-virtual {v8, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->d(LX/0lF;)I

    move-result v4

    const-string v6, "video_data_url"

    invoke-virtual {v8, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/attachment/VideoData;-><init>(IIIILX/5dX;Landroid/net/Uri;Landroid/net/Uri;)V

    .line 399127
    iput-object v0, v11, LX/5dN;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 399128
    :cond_3
    const-string v0, "is_voicemail"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 399129
    const-string v0, "call_id"

    invoke-virtual {v8, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 399130
    new-instance v0, Lcom/facebook/messaging/model/attachment/AudioData;

    const-string v1, "is_voicemail"

    invoke-virtual {v8, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    const-string v2, "call_id"

    invoke-virtual {v8, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/model/attachment/AudioData;-><init>(ZLjava/lang/String;)V

    .line 399131
    :goto_2
    iput-object v0, v11, LX/5dN;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    .line 399132
    :cond_4
    invoke-virtual {v11}, LX/5dN;->m()Lcom/facebook/messaging/model/attachment/Attachment;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 399133
    :cond_5
    new-instance v0, Lcom/facebook/messaging/model/attachment/AudioData;

    const-string v1, "is_voicemail"

    invoke-virtual {v8, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/model/attachment/AudioData;-><init>(ZLjava/lang/String;)V

    goto :goto_2

    .line 399134
    :cond_6
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0
.end method
