.class public final LX/3AI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/CSJ;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/CSJ;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 525070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 525071
    iput-object p1, p0, LX/3AI;->a:LX/0QB;

    .line 525072
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 525073
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/3AI;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 525074
    packed-switch p2, :pswitch_data_0

    .line 525075
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 525076
    :pswitch_0
    new-instance p2, LX/GEa;

    invoke-static {p1}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v0

    check-cast v0, LX/13Q;

    invoke-static {p1}, LX/0sh;->a(LX/0QB;)LX/0sh;

    move-result-object v1

    check-cast v1, LX/0sh;

    invoke-static {p1}, LX/2U4;->a(LX/0QB;)LX/2U4;

    move-result-object v2

    check-cast v2, LX/2U4;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    invoke-direct {p2, v0, v1, v2, p0}, LX/GEa;-><init>(LX/13Q;LX/0sh;LX/2U4;LX/0ad;)V

    .line 525077
    move-object v0, p2

    .line 525078
    :goto_0
    return-object v0

    .line 525079
    :pswitch_1
    new-instance v0, LX/Guw;

    invoke-direct {v0}, LX/Guw;-><init>()V

    .line 525080
    move-object v0, v0

    .line 525081
    move-object v0, v0

    .line 525082
    goto :goto_0

    .line 525083
    :pswitch_2
    new-instance v2, LX/HT1;

    const-class v3, LX/AEo;

    invoke-interface {p1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/AEo;

    const/16 v4, 0x360

    invoke-static {p1, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p1}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v5

    check-cast v5, LX/2U1;

    invoke-static {p1}, LX/0sh;->a(LX/0QB;)LX/0sh;

    move-result-object v6

    check-cast v6, LX/0sh;

    invoke-static {p1}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v7

    check-cast v7, LX/3iH;

    invoke-static {p1}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v8

    check-cast v8, LX/0SI;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {p1}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v10

    check-cast v10, LX/13Q;

    invoke-direct/range {v2 .. v10}, LX/HT1;-><init>(LX/AEo;LX/0Or;LX/2U1;LX/0sh;LX/3iH;LX/0SI;LX/0ad;LX/13Q;)V

    .line 525084
    move-object v0, v2

    .line 525085
    goto :goto_0

    .line 525086
    :pswitch_3
    new-instance v3, LX/D38;

    const-class v0, LX/AEo;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/AEo;

    const/16 v1, 0x360

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p1}, LX/2nC;->a(LX/0QB;)LX/2nC;

    move-result-object v2

    check-cast v2, LX/2nC;

    invoke-direct {v3, v0, v4, v1, v2}, LX/D38;-><init>(LX/AEo;LX/0Or;LX/0Uh;LX/2nC;)V

    .line 525087
    move-object v0, v3

    .line 525088
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 525089
    const/4 v0, 0x4

    return v0
.end method
