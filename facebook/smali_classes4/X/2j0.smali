.class public LX/2j0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2j1;


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 452532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452533
    iput-object p1, p0, LX/2j0;->a:LX/0SG;

    .line 452534
    return-void
.end method

.method private static a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1
    .param p2    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 452523
    invoke-virtual {p0}, LX/2jY;->E()V

    .line 452524
    const/4 v0, 0x0

    .line 452525
    iput-boolean v0, p0, LX/2jY;->o:Z

    .line 452526
    iput-object p1, p0, LX/2jY;->e:Ljava/lang/String;

    .line 452527
    iput-object p2, p0, LX/2jY;->f:Ljava/lang/Long;

    .line 452528
    invoke-virtual {p0}, LX/2jY;->t()LX/CfG;

    move-result-object v0

    .line 452529
    if-eqz v0, :cond_0

    .line 452530
    invoke-interface {v0}, LX/CfG;->kU_()V

    .line 452531
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2jY;)V
    .locals 2

    .prologue
    .line 452521
    const-string v0, "EMPTY_REQUEST"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, LX/2j0;->a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452522
    return-void
.end method

.method public final a(LX/2jY;LX/2jQ;)V
    .locals 2

    .prologue
    .line 452519
    const-string v0, "NETWORK_FAILURE"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, LX/2j0;->a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452520
    return-void
.end method

.method public final a(LX/2jY;LX/2jS;)V
    .locals 4

    .prologue
    .line 452487
    iget-object v0, p2, LX/2jS;->a:Ljava/lang/String;

    move-object v0, v0

    .line 452488
    iget-object v1, p0, LX/2j0;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2j0;->a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452489
    return-void
.end method

.method public final a(LX/2jY;LX/2jT;)V
    .locals 2

    .prologue
    .line 452518
    new-instance v0, Lorg/apache/http/MethodNotSupportedException;

    const-string v1, "There shouldn\'t be any cache result for PaginationReactionRequestTracker."

    invoke-direct {v0, v1}, Lorg/apache/http/MethodNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/2jY;LX/2jV;)V
    .locals 6

    .prologue
    .line 452498
    iget-object v0, p2, LX/2jV;->c:LX/9qT;

    move-object v0, v0

    .line 452499
    if-nez v0, :cond_1

    const/4 v1, 0x0

    .line 452500
    :goto_0
    if-nez v1, :cond_2

    .line 452501
    const-string v0, "NO_UNITS_RETURNED"

    iget-object v1, p0, LX/2j0;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2j0;->a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    .line 452502
    :cond_0
    :goto_1
    return-void

    .line 452503
    :cond_1
    iget-object v0, p2, LX/2jV;->c:LX/9qT;

    move-object v1, v0

    .line 452504
    goto :goto_0

    .line 452505
    :cond_2
    invoke-virtual {p1, v1}, LX/2jY;->a(LX/9qT;)V

    .line 452506
    instance-of v0, v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 452507
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/2jY;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;)V

    .line 452508
    :cond_3
    invoke-virtual {p1}, LX/2jY;->E()V

    .line 452509
    iget-wide v4, p2, LX/2jV;->a:J

    move-wide v2, v4

    .line 452510
    iput-wide v2, p1, LX/2jY;->g:J

    .line 452511
    iget-wide v4, p2, LX/2jV;->b:J

    move-wide v2, v4

    .line 452512
    iput-wide v2, p1, LX/2jY;->h:J

    .line 452513
    iget-object v0, p0, LX/2j0;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 452514
    iput-wide v2, p1, LX/2jY;->i:J

    .line 452515
    invoke-virtual {p1}, LX/2jY;->t()LX/CfG;

    move-result-object v0

    .line 452516
    if-eqz v0, :cond_0

    .line 452517
    invoke-interface {v0, v1}, LX/CfG;->a(LX/9qT;)V

    goto :goto_1
.end method

.method public final a(LX/2jY;LX/2jW;)V
    .locals 2

    .prologue
    .line 452497
    new-instance v0, Lorg/apache/http/MethodNotSupportedException;

    const-string v1, "There shouldn\'t be any cache result for PaginationReactionRequestTracker."

    invoke-direct {v0, v1}, Lorg/apache/http/MethodNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/2jY;LX/2jX;)V
    .locals 1

    .prologue
    .line 452490
    const/4 v0, 0x0

    .line 452491
    iput-object v0, p1, LX/2jY;->e:Ljava/lang/String;

    .line 452492
    const/4 v0, 0x1

    .line 452493
    iput-boolean v0, p1, LX/2jY;->n:Z

    .line 452494
    iget-object v0, p2, LX/2jX;->a:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v0, v0

    .line 452495
    iput-object v0, p1, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    .line 452496
    return-void
.end method
