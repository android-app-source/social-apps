.class public LX/3JH;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/3FP;
.implements LX/3FQ;
.implements LX/1a7;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public a:Lcom/facebook/video/player/InlineVideoPlayer2;

.field public b:LX/7gP;

.field public c:F

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 547620
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/3JH;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 547621
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 547614
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 547615
    const/4 v0, 0x0

    iput v0, p0, LX/3JH;->c:F

    .line 547616
    const v0, 0x7f030924

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 547617
    const v0, 0x7f0d176a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/InlineVideoPlayer2;

    iput-object v0, p0, LX/3JH;->a:Lcom/facebook/video/player/InlineVideoPlayer2;

    .line 547618
    new-instance v0, LX/7gP;

    iget-object v1, p0, LX/3JH;->a:Lcom/facebook/video/player/InlineVideoPlayer2;

    invoke-direct {v0, v1}, LX/7gP;-><init>(LX/7Kf;)V

    iput-object v0, p0, LX/3JH;->b:LX/7gP;

    .line 547619
    return-void
.end method


# virtual methods
.method public final cr_()Z
    .locals 1

    .prologue
    .line 547597
    const/4 v0, 0x1

    return v0
.end method

.method public getAndClearShowLiveCommentDialogFragment()Z
    .locals 2

    .prologue
    .line 547611
    iget-boolean v0, p0, LX/3JH;->d:Z

    .line 547612
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/3JH;->d:Z

    .line 547613
    return v0
.end method

.method public getAudioChannelLayout()LX/03z;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 547610
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLastStartPosition()I
    .locals 1

    .prologue
    .line 547607
    iget-object v0, p0, LX/3JH;->a:Lcom/facebook/video/player/InlineVideoPlayer2;

    move-object v0, v0

    .line 547608
    iget-object p0, v0, Lcom/facebook/video/player/InlineVideoPlayer2;->a:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result p0

    move v0, p0

    .line 547609
    return v0
.end method

.method public getProjectionType()LX/19o;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 547606
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSeekPosition()I
    .locals 1

    .prologue
    .line 547604
    iget-object v0, p0, LX/3JH;->a:Lcom/facebook/video/player/InlineVideoPlayer2;

    move-object v0, v0

    .line 547605
    invoke-virtual {v0}, Lcom/facebook/video/player/InlineVideoPlayer2;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getTransitionNode()LX/3FT;
    .locals 1

    .prologue
    .line 547602
    iget-object v0, p0, LX/3JH;->a:Lcom/facebook/video/player/InlineVideoPlayer2;

    move-object v0, v0

    .line 547603
    return-object v0
.end method

.method public getVideoAspectRatio()F
    .locals 1

    .prologue
    .line 547601
    iget v0, p0, LX/3JH;->c:F

    return v0
.end method

.method public getVideoStoryPersistentState()LX/2oM;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 547600
    const/4 v0, 0x0

    return-object v0
.end method

.method public setShowLiveCommentDialogFragment(Z)V
    .locals 0

    .prologue
    .line 547598
    iput-boolean p1, p0, LX/3JH;->d:Z

    .line 547599
    return-void
.end method
