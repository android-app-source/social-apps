.class public LX/2q3;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/1Lt;

.field private final c:LX/0kb;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0A1;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2q4;

.field private f:LX/16V;

.field public g:LX/37C;

.field public h:I

.field public i:I

.field private j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 469274
    const-class v0, LX/2q3;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2q3;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Lt;LX/0kb;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/server/VideoKeyCreator;",
            "LX/0kb;",
            "LX/0Ot",
            "<",
            "LX/0A1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 469275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 469276
    iput-object p1, p0, LX/2q3;->b:LX/1Lt;

    .line 469277
    iput-object p2, p0, LX/2q3;->c:LX/0kb;

    .line 469278
    iput-object p3, p0, LX/2q3;->d:LX/0Ot;

    .line 469279
    new-instance v0, LX/2q4;

    invoke-direct {v0, p0}, LX/2q4;-><init>(LX/2q3;)V

    iput-object v0, p0, LX/2q3;->e:LX/2q4;

    .line 469280
    const/4 v0, -0x1

    iput v0, p0, LX/2q3;->i:I

    .line 469281
    const/4 v0, 0x0

    iput-object v0, p0, LX/2q3;->j:Ljava/lang/String;

    .line 469282
    return-void
.end method

.method public static a(LX/0QB;)LX/2q3;
    .locals 1

    .prologue
    .line 469283
    invoke-static {p0}, LX/2q3;->b(LX/0QB;)LX/2q3;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/2q3;JJ)V
    .locals 9

    .prologue
    .line 469284
    iget-object v0, p0, LX/2q3;->c:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->v()Z

    move-result v7

    .line 469285
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 469286
    iget-object v0, p0, LX/2q3;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0A1;

    iget-object v1, p0, LX/2q3;->g:LX/37C;

    iget v6, p0, LX/2q3;->h:I

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v7}, LX/0A1;->a(LX/2WG;JJIZ)V

    .line 469287
    return-void
.end method

.method public static b(LX/0QB;)LX/2q3;
    .locals 4

    .prologue
    .line 469288
    new-instance v2, LX/2q3;

    invoke-static {p0}, LX/1Ls;->a(LX/0QB;)LX/1Lt;

    move-result-object v0

    check-cast v0, LX/1Lt;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v1

    check-cast v1, LX/0kb;

    const/16 v3, 0x12f4

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/2q3;-><init>(LX/1Lt;LX/0kb;LX/0Ot;)V

    .line 469289
    return-object v2
.end method


# virtual methods
.method public final a(II)V
    .locals 4

    .prologue
    .line 469290
    iget v0, p0, LX/2q3;->h:I

    if-eq v0, p1, :cond_0

    .line 469291
    iget v0, p0, LX/2q3;->i:I

    int-to-long v0, v0

    int-to-long v2, p2

    invoke-static {p0, v0, v1, v2, v3}, LX/2q3;->a$redex0(LX/2q3;JJ)V

    .line 469292
    iput p1, p0, LX/2q3;->h:I

    .line 469293
    iput p2, p0, LX/2q3;->i:I

    .line 469294
    :cond_0
    return-void
.end method

.method public final a(LX/16V;)V
    .locals 3

    .prologue
    .line 469295
    iget-object v0, p0, LX/2q3;->f:LX/16V;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot register twice"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 469296
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16V;

    iput-object v0, p0, LX/2q3;->f:LX/16V;

    .line 469297
    iget-object v0, p0, LX/2q3;->f:LX/16V;

    const-class v1, LX/2qL;

    iget-object v2, p0, LX/2q3;->e:LX/2q4;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469298
    iget-object v0, p0, LX/2q3;->f:LX/16V;

    const-class v1, LX/2qT;

    iget-object v2, p0, LX/2q3;->e:LX/2q4;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469299
    return-void

    .line 469300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 469301
    invoke-static {p1}, LX/1m0;->f(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469302
    invoke-static {p1}, LX/1m0;->g(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2q3;->j:Ljava/lang/String;

    .line 469303
    invoke-static {p1}, LX/1m0;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p1

    .line 469304
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/2q3;->g:LX/37C;

    .line 469305
    return-void

    .line 469306
    :cond_1
    iget-object v0, p0, LX/2q3;->b:LX/1Lt;

    invoke-virtual {v0, p1}, LX/1Lt;->a(Landroid/net/Uri;)LX/37C;

    move-result-object v0

    goto :goto_0
.end method
