.class public LX/33N;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 492767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 492768
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-nez v1, :cond_1

    .line 492769
    :cond_0
    :goto_0
    return-object v0

    .line 492770
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x25d6af

    if-ne v1, v2, :cond_2

    .line 492771
    invoke-static {p0}, LX/6Wy;->a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/6X4;->b(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto :goto_0

    .line 492772
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x285feb

    if-ne v1, v2, :cond_0

    .line 492773
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x285feb

    if-eq v3, v4, :cond_4

    .line 492774
    :cond_3
    const/4 v3, 0x0

    .line 492775
    :goto_1
    move-object v0, v3

    .line 492776
    invoke-static {v0}, LX/2em;->a(Lcom/facebook/graphql/model/GraphQLUser;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto :goto_0

    .line 492777
    :cond_4
    new-instance v3, LX/33O;

    invoke-direct {v3}, LX/33O;-><init>()V

    .line 492778
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aR()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v4

    .line 492779
    iput-object v4, v3, LX/33O;->c:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 492780
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->l()Ljava/lang/String;

    move-result-object v4

    .line 492781
    iput-object v4, v3, LX/33O;->f:Ljava/lang/String;

    .line 492782
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aL()Ljava/lang/String;

    move-result-object v4

    .line 492783
    iput-object v4, v3, LX/33O;->g:Ljava/lang/String;

    .line 492784
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aT()I

    move-result v4

    .line 492785
    iput v4, v3, LX/33O;->h:I

    .line 492786
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 492787
    iput-object v4, v3, LX/33O;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 492788
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aN()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v4

    .line 492789
    iput-object v4, v3, LX/33O;->k:Lcom/facebook/graphql/model/GraphQLDate;

    .line 492790
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->o()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v4

    .line 492791
    iput-object v4, v3, LX/33O;->l:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 492792
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->p()Z

    move-result v4

    .line 492793
    iput-boolean v4, v3, LX/33O;->o:Z

    .line 492794
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->q()Z

    move-result v4

    .line 492795
    iput-boolean v4, v3, LX/33O;->p:Z

    .line 492796
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->r()Z

    move-result v4

    .line 492797
    iput-boolean v4, v3, LX/33O;->q:Z

    .line 492798
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->s()Z

    move-result v4

    .line 492799
    iput-boolean v4, v3, LX/33O;->r:Z

    .line 492800
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->t()Z

    move-result v4

    .line 492801
    iput-boolean v4, v3, LX/33O;->s:Z

    .line 492802
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->u()Z

    move-result v4

    .line 492803
    iput-boolean v4, v3, LX/33O;->t:Z

    .line 492804
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->v()Z

    move-result v4

    .line 492805
    iput-boolean v4, v3, LX/33O;->u:Z

    .line 492806
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->x()D

    move-result-wide v5

    .line 492807
    iput-wide v5, v3, LX/33O;->w:D

    .line 492808
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->y()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    .line 492809
    iput-object v4, v3, LX/33O;->y:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 492810
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    .line 492811
    iput-object v4, v3, LX/33O;->C:Lcom/facebook/graphql/model/GraphQLPage;

    .line 492812
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->bb()Z

    move-result v4

    .line 492813
    iput-boolean v4, v3, LX/33O;->E:Z

    .line 492814
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->A()LX/0Px;

    move-result-object v4

    .line 492815
    iput-object v4, v3, LX/33O;->F:LX/0Px;

    .line 492816
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->B()Ljava/lang/String;

    move-result-object v4

    .line 492817
    iput-object v4, v3, LX/33O;->H:Ljava/lang/String;

    .line 492818
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aP()Ljava/lang/String;

    move-result-object v4

    .line 492819
    iput-object v4, v3, LX/33O;->I:Ljava/lang/String;

    .line 492820
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->C()Lcom/facebook/graphql/model/GraphQLEventsConnection;

    move-result-object v4

    .line 492821
    iput-object v4, v3, LX/33O;->J:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    .line 492822
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->D()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v4

    .line 492823
    iput-object v4, v3, LX/33O;->O:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 492824
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    .line 492825
    iput-object v4, v3, LX/33O;->P:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 492826
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->F()Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v4

    .line 492827
    iput-object v4, v3, LX/33O;->R:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 492828
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->G()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    .line 492829
    iput-object v4, v3, LX/33O;->T:Lcom/facebook/graphql/model/GraphQLPage;

    .line 492830
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    .line 492831
    iput-object v4, v3, LX/33O;->U:Ljava/lang/String;

    .line 492832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aY()Ljava/lang/String;

    move-result-object v4

    .line 492833
    iput-object v4, v3, LX/33O;->W:Ljava/lang/String;

    .line 492834
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aK()Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    move-result-object v4

    .line 492835
    iput-object v4, v3, LX/33O;->X:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 492836
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->I()Z

    move-result v4

    .line 492837
    iput-boolean v4, v3, LX/33O;->Y:Z

    .line 492838
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->J()Z

    move-result v4

    .line 492839
    iput-boolean v4, v3, LX/33O;->ab:Z

    .line 492840
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->K()Z

    move-result v4

    .line 492841
    iput-boolean v4, v3, LX/33O;->ac:Z

    .line 492842
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->L()Z

    move-result v4

    .line 492843
    iput-boolean v4, v3, LX/33O;->ad:Z

    .line 492844
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->M()Z

    move-result v4

    .line 492845
    iput-boolean v4, v3, LX/33O;->ae:Z

    .line 492846
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aO()Z

    move-result v4

    .line 492847
    iput-boolean v4, v3, LX/33O;->ag:Z

    .line 492848
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->N()Z

    move-result v4

    .line 492849
    iput-boolean v4, v3, LX/33O;->ah:Z

    .line 492850
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->O()Z

    move-result v4

    .line 492851
    iput-boolean v4, v3, LX/33O;->ai:Z

    .line 492852
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->P()Z

    move-result v4

    .line 492853
    iput-boolean v4, v3, LX/33O;->aj:Z

    .line 492854
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->Q()Z

    move-result v4

    .line 492855
    iput-boolean v4, v3, LX/33O;->ak:Z

    .line 492856
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v4

    .line 492857
    iput-boolean v4, v3, LX/33O;->ao:Z

    .line 492858
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->S()Z

    move-result v4

    .line 492859
    iput-boolean v4, v3, LX/33O;->ap:Z

    .line 492860
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->T()Z

    move-result v4

    .line 492861
    iput-boolean v4, v3, LX/33O;->aq:Z

    .line 492862
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->U()Z

    move-result v4

    .line 492863
    iput-boolean v4, v3, LX/33O;->as:Z

    .line 492864
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->V()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v4

    .line 492865
    iput-object v4, v3, LX/33O;->at:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 492866
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->W()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v4

    .line 492867
    iput-object v4, v3, LX/33O;->au:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 492868
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 492869
    iput-object v4, v3, LX/33O;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    .line 492870
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 492871
    iput-object v4, v3, LX/33O;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    .line 492872
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->X()J

    move-result-wide v5

    .line 492873
    iput-wide v5, v3, LX/33O;->aB:J

    .line 492874
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->Y()D

    move-result-wide v5

    .line 492875
    iput-wide v5, v3, LX/33O;->aC:D

    .line 492876
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aQ()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v4

    .line 492877
    iput-object v4, v3, LX/33O;->aD:Lcom/facebook/graphql/model/GraphQLUser;

    .line 492878
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->Z()Ljava/lang/String;

    move-result-object v4

    .line 492879
    iput-object v4, v3, LX/33O;->aF:Ljava/lang/String;

    .line 492880
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v4

    .line 492881
    iput-object v4, v3, LX/33O;->aH:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 492882
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v4

    .line 492883
    iput-object v4, v3, LX/33O;->aI:Ljava/lang/String;

    .line 492884
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ac()LX/0Px;

    move-result-object v4

    .line 492885
    iput-object v4, v3, LX/33O;->aJ:LX/0Px;

    .line 492886
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ad()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v4

    .line 492887
    iput-object v4, v3, LX/33O;->aK:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 492888
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->af()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v4

    .line 492889
    iput-object v4, v3, LX/33O;->aL:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 492890
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 492891
    iput-object v4, v3, LX/33O;->aS:Lcom/facebook/graphql/model/GraphQLImage;

    .line 492892
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ah()Lcom/facebook/graphql/model/GraphQLProfileBadge;

    move-result-object v4

    .line 492893
    iput-object v4, v3, LX/33O;->aV:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    .line 492894
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ai()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    .line 492895
    iput-object v4, v3, LX/33O;->aW:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 492896
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 492897
    iput-object v4, v3, LX/33O;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 492898
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ak()J

    move-result-wide v5

    .line 492899
    iput-wide v5, v3, LX/33O;->bb:J

    .line 492900
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->al()Z

    move-result v4

    .line 492901
    iput-boolean v4, v3, LX/33O;->bc:Z

    .line 492902
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->am()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v4

    .line 492903
    iput-object v4, v3, LX/33O;->bd:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 492904
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->an()Ljava/lang/String;

    move-result-object v4

    .line 492905
    iput-object v4, v3, LX/33O;->bj:Ljava/lang/String;

    .line 492906
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ao()Ljava/lang/String;

    move-result-object v4

    .line 492907
    iput-object v4, v3, LX/33O;->bk:Ljava/lang/String;

    .line 492908
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v4

    .line 492909
    iput-object v4, v3, LX/33O;->bl:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 492910
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aq()Ljava/lang/String;

    move-result-object v4

    .line 492911
    iput-object v4, v3, LX/33O;->bn:Ljava/lang/String;

    .line 492912
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ar()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v4

    .line 492913
    iput-object v4, v3, LX/33O;->bo:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 492914
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 492915
    iput-object v4, v3, LX/33O;->bp:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 492916
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 492917
    iput-object v4, v3, LX/33O;->bq:Lcom/facebook/graphql/model/GraphQLImage;

    .line 492918
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 492919
    iput-object v4, v3, LX/33O;->br:Lcom/facebook/graphql/model/GraphQLImage;

    .line 492920
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->au()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 492921
    iput-object v4, v3, LX/33O;->bs:Lcom/facebook/graphql/model/GraphQLImage;

    .line 492922
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->av()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v4

    .line 492923
    iput-object v4, v3, LX/33O;->bu:Lcom/facebook/graphql/model/GraphQLName;

    .line 492924
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aw()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v4

    .line 492925
    iput-object v4, v3, LX/33O;->bw:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 492926
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ax()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 492927
    iput-object v4, v3, LX/33O;->by:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 492928
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ay()I

    move-result v4

    .line 492929
    iput v4, v3, LX/33O;->bC:I

    .line 492930
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ba()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v4

    .line 492931
    iput-object v4, v3, LX/33O;->bD:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 492932
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v4

    .line 492933
    iput-object v4, v3, LX/33O;->bE:Ljava/lang/String;

    .line 492934
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aA()Ljava/lang/String;

    move-result-object v4

    .line 492935
    iput-object v4, v3, LX/33O;->bF:Ljava/lang/String;

    .line 492936
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result v4

    .line 492937
    iput-boolean v4, v3, LX/33O;->bG:Z

    .line 492938
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aC()Z

    move-result v4

    .line 492939
    iput-boolean v4, v3, LX/33O;->bH:Z

    .line 492940
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aD()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    .line 492941
    iput-object v4, v3, LX/33O;->bJ:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 492942
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v4

    .line 492943
    iput-boolean v4, v3, LX/33O;->bL:Z

    .line 492944
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v4

    .line 492945
    iput-boolean v4, v3, LX/33O;->bM:Z

    .line 492946
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 492947
    iput-object v4, v3, LX/33O;->bQ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 492948
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 492949
    iput-object v4, v3, LX/33O;->bR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 492950
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->bd()Z

    move-result v4

    .line 492951
    iput-boolean v4, v3, LX/33O;->bT:Z

    .line 492952
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aJ()D

    move-result-wide v5

    .line 492953
    iput-wide v5, v3, LX/33O;->bW:D

    .line 492954
    invoke-virtual {v3}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    goto/16 :goto_1
.end method
