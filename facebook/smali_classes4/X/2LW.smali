.class public LX/2LW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2LP;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/03V;

.field public final c:LX/2LS;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private f:LX/03R;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/03V;LX/2LS;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/launcherbadges/AppLaunchClass;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395142
    iput-object p1, p0, LX/2LW;->a:Landroid/content/Context;

    .line 395143
    iput-object p2, p0, LX/2LW;->b:LX/03V;

    .line 395144
    iput-object p3, p0, LX/2LW;->c:LX/2LS;

    .line 395145
    iput-object p4, p0, LX/2LW;->e:Ljava/lang/String;

    .line 395146
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2LW;->d:Ljava/lang/String;

    .line 395147
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2LW;->f:LX/03R;

    .line 395148
    return-void
.end method


# virtual methods
.method public final a(I)LX/03R;
    .locals 7

    .prologue
    .line 395149
    iget-object v0, p0, LX/2LW;->f:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_1

    .line 395150
    iget-object v0, p0, LX/2LW;->c:LX/2LS;

    .line 395151
    invoke-static {v0}, LX/2LS;->h(LX/2LS;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.app.twlauncher"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 395152
    if-nez v0, :cond_0

    iget-object v0, p0, LX/2LW;->c:LX/2LS;

    .line 395153
    invoke-static {v0}, LX/2LS;->h(LX/2LS;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.app.launcher"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 395154
    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 395155
    if-eqz v0, :cond_2

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_1
    iput-object v0, p0, LX/2LW;->f:LX/03R;

    .line 395156
    :cond_1
    iget-object v0, p0, LX/2LW;->f:LX/03R;

    sget-object v1, LX/03R;->NO:LX/03R;

    if-ne v0, v1, :cond_3

    .line 395157
    sget-object v0, LX/03R;->NO:LX/03R;

    .line 395158
    :goto_2
    return-object v0

    .line 395159
    :cond_2
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_1

    .line 395160
    :cond_3
    :try_start_0
    iget-object v0, p0, LX/2LW;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 395161
    const-string v1, "content://com.sec.badge/apps"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 395162
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 395163
    const-string v3, "package"

    iget-object v4, p0, LX/2LW;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 395164
    const-string v3, "class"

    iget-object v4, p0, LX/2LW;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 395165
    const-string v3, "badgecount"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 395166
    const-string v3, "%s=? AND %s=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "package"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "class"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, LX/2LW;->d:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, LX/2LW;->e:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 395167
    if-eqz v3, :cond_4

    .line 395168
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_2

    .line 395169
    :cond_4
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 395170
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_2

    .line 395171
    :catch_0
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, LX/2LW;->f:LX/03R;

    .line 395172
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_2

    .line 395173
    :catch_1
    move-exception v0

    .line 395174
    iget-object v1, p0, LX/2LW;->b:LX/03V;

    const-class v2, LX/2LW;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "unexpected exception"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 395175
    sget-object v0, LX/03R;->NO:LX/03R;

    iput-object v0, p0, LX/2LW;->f:LX/03R;

    .line 395176
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
