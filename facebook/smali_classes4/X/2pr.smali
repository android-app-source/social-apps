.class public final enum LX/2pr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2pr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2pr;

.field public static final enum EVENT_PUBLISHED:LX/2pr;

.field public static final enum TIMER_STARTED:LX/2pr;

.field public static final enum UNKNOWN_OR_UNSET:LX/2pr;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 469008
    new-instance v0, LX/2pr;

    const-string v1, "TIMER_STARTED"

    invoke-direct {v0, v1, v2}, LX/2pr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2pr;->TIMER_STARTED:LX/2pr;

    new-instance v0, LX/2pr;

    const-string v1, "EVENT_PUBLISHED"

    invoke-direct {v0, v1, v3}, LX/2pr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2pr;->EVENT_PUBLISHED:LX/2pr;

    new-instance v0, LX/2pr;

    const-string v1, "UNKNOWN_OR_UNSET"

    invoke-direct {v0, v1, v4}, LX/2pr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2pr;->UNKNOWN_OR_UNSET:LX/2pr;

    .line 469009
    const/4 v0, 0x3

    new-array v0, v0, [LX/2pr;

    sget-object v1, LX/2pr;->TIMER_STARTED:LX/2pr;

    aput-object v1, v0, v2

    sget-object v1, LX/2pr;->EVENT_PUBLISHED:LX/2pr;

    aput-object v1, v0, v3

    sget-object v1, LX/2pr;->UNKNOWN_OR_UNSET:LX/2pr;

    aput-object v1, v0, v4

    sput-object v0, LX/2pr;->$VALUES:[LX/2pr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 469010
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2pr;
    .locals 1

    .prologue
    .line 469011
    const-class v0, LX/2pr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2pr;

    return-object v0
.end method

.method public static values()[LX/2pr;
    .locals 1

    .prologue
    .line 469012
    sget-object v0, LX/2pr;->$VALUES:[LX/2pr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2pr;

    return-object v0
.end method
