.class public LX/36g;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 499828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Intent;)LX/0m9;
    .locals 8
    .param p0    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 499829
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 499830
    if-nez p0, :cond_0

    .line 499831
    :goto_0
    return-object v0

    .line 499832
    :cond_0
    const-string v1, "category"

    invoke-virtual {p0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v2

    .line 499833
    new-instance v5, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/162;-><init>(LX/0mC;)V

    .line 499834
    if-nez v2, :cond_3

    move-object v4, v5

    .line 499835
    :goto_1
    move-object v2, v4

    .line 499836
    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 499837
    const-string v1, "action"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 499838
    const-string v1, "flags"

    invoke-virtual {p0}, Landroid/content/Intent;->getFlags()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 499839
    const-string v1, "data"

    invoke-virtual {p0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 499840
    const-string v1, "package"

    invoke-virtual {p0}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 499841
    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 499842
    if-eqz v1, :cond_1

    .line 499843
    const-string v2, "component"

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 499844
    :goto_2
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 499845
    if-eqz v1, :cond_2

    .line 499846
    const-string v2, "extras"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 499847
    new-instance v5, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/162;-><init>(LX/0mC;)V

    .line 499848
    if-nez v1, :cond_5

    move-object v4, v5

    .line 499849
    :goto_3
    move-object v1, v4

    .line 499850
    invoke-virtual {v0, v2, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0

    .line 499851
    :cond_1
    const-string v1, "component"

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;[B)LX/0m9;

    goto :goto_2

    .line 499852
    :cond_2
    const-string v1, "extras"

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;[B)LX/0m9;

    goto :goto_0

    .line 499853
    :cond_3
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 499854
    invoke-virtual {v5, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_4

    :cond_4
    move-object v4, v5

    .line 499855
    goto :goto_1

    .line 499856
    :cond_5
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 499857
    invoke-virtual {v1, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 499858
    instance-of p0, v7, Landroid/content/Intent;

    if-nez p0, :cond_6

    .line 499859
    if-eqz v7, :cond_7

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_7

    .line 499860
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p0, ":"

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_5

    .line 499861
    :cond_7
    if-eqz v7, :cond_8

    .line 499862
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p0, ":"

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_5

    .line 499863
    :cond_8
    invoke-virtual {v5, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_5

    :cond_9
    move-object v4, v5

    .line 499864
    goto/16 :goto_3
.end method
