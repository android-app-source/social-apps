.class public final LX/2lX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/2lX;


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/zero/annotations/IsFreeFacebookSettingsBookmarkEnabledGateKeeper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Fn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/27w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2lM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2lJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 458031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2lX;
    .locals 10

    .prologue
    .line 458032
    sget-object v0, LX/2lX;->h:LX/2lX;

    if-nez v0, :cond_1

    .line 458033
    const-class v1, LX/2lX;

    monitor-enter v1

    .line 458034
    :try_start_0
    sget-object v0, LX/2lX;->h:LX/2lX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 458035
    if-eqz v2, :cond_0

    .line 458036
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 458037
    new-instance v3, LX/2lX;

    invoke-direct {v3}, LX/2lX;-><init>()V

    .line 458038
    invoke-static {v0}, LX/2lJ;->a(LX/0QB;)LX/2lJ;

    move-result-object v4

    check-cast v4, LX/2lJ;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    const/16 v7, 0x15ac

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/1Fn;->b(LX/0QB;)LX/1Fn;

    move-result-object v8

    check-cast v8, LX/1Fn;

    invoke-static {v0}, LX/27w;->b(LX/0QB;)LX/27w;

    move-result-object v9

    check-cast v9, LX/27w;

    invoke-static {v0}, LX/2lM;->b(LX/0QB;)LX/2lM;

    move-result-object p0

    check-cast p0, LX/2lM;

    .line 458039
    iput-object v4, v3, LX/2lX;->f:LX/2lJ;

    iput-object v5, v3, LX/2lX;->g:LX/0Uh;

    iput-object v6, v3, LX/2lX;->a:Ljava/lang/Boolean;

    iput-object v7, v3, LX/2lX;->b:LX/0Or;

    iput-object v8, v3, LX/2lX;->c:LX/1Fn;

    iput-object v9, v3, LX/2lX;->d:LX/27w;

    iput-object p0, v3, LX/2lX;->e:LX/2lM;

    .line 458040
    move-object v0, v3

    .line 458041
    sput-object v0, LX/2lX;->h:LX/2lX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 458042
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 458043
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 458044
    :cond_1
    sget-object v0, LX/2lX;->h:LX/2lX;

    return-object v0

    .line 458045
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 458046
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;ZZZ)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "ZZZ)",
            "Ljava/util/List",
            "<",
            "LX/FBn;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 458047
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 458048
    iget-object v0, p0, LX/2lX;->g:LX/0Uh;

    const/16 v2, 0x570

    invoke-virtual {v0, v2, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458049
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->o:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f0200a0

    const v4, 0x7f080100

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458050
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->p:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f0200a0

    const v4, 0x7f080101

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458051
    :cond_0
    if-eqz p2, :cond_11

    .line 458052
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->q:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f0200a0

    const v4, 0x7f0831f5

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458053
    :goto_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->n:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f020ddf

    const v4, 0x7f0831f1

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458054
    iget-object v0, p0, LX/2lX;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 458055
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->b:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f0200a0

    const v4, 0x7f0831f3

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458056
    :cond_1
    if-nez p2, :cond_2

    iget-object v0, p0, LX/2lX;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 458057
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->c:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f020adf

    const v4, 0x7f0831ec

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458058
    :cond_2
    iget-object v0, p0, LX/2lX;->c:LX/1Fn;

    .line 458059
    iget-object v7, v0, LX/1Fn;->a:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0W3;

    sget-wide v9, LX/0X5;->ec:J

    invoke-interface {v7, v9, v10}, LX/0W4;->a(J)Z

    move-result v7

    move v0, v7

    .line 458060
    if-eqz v0, :cond_3

    .line 458061
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->f:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f021165

    const v4, 0x7f082eeb

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458062
    :cond_3
    if-nez p2, :cond_4

    if-eqz p3, :cond_4

    .line 458063
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->d:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f02040f

    const v4, 0x7f0809fd

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458064
    :cond_4
    if-nez p2, :cond_5

    .line 458065
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->g:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f020041

    const v4, 0x7f0831f2

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458066
    :cond_5
    if-nez p2, :cond_6

    iget-object v0, p0, LX/2lX;->g:LX/0Uh;

    const/16 v2, 0x72

    invoke-virtual {v0, v2, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 458067
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->h:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f0200a0

    const v4, 0x7f081e0a

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458068
    :cond_6
    if-nez p2, :cond_8

    const/4 v0, 0x1

    .line 458069
    iget-object v2, p0, LX/2lX;->d:LX/27w;

    invoke-virtual {v2}, LX/27w;->a()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 458070
    :cond_7
    :goto_1
    move v0, v0

    .line 458071
    if-eqz v0, :cond_8

    .line 458072
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->j:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f02026e

    const v4, 0x7f0831dc

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458073
    :cond_8
    if-nez p2, :cond_9

    .line 458074
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->i:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f020cbe

    const v4, 0x7f0831fd

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458075
    :cond_9
    iget-object v0, p0, LX/2lX;->f:LX/2lJ;

    invoke-virtual {v0}, LX/2lJ;->a()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    .line 458076
    if-nez p2, :cond_a

    if-eqz v0, :cond_a

    .line 458077
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f020056

    const v4, 0x7f0831f6

    invoke-static {p1, v2, v0, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458078
    :cond_a
    iget-object v0, p0, LX/2lX;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_b

    .line 458079
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->k:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f0214fa

    const v4, 0x7f083202

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458080
    :cond_b
    if-eqz p2, :cond_c

    .line 458081
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->r:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f020cbe

    const v4, 0x7f0831fe

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458082
    :cond_c
    if-nez p2, :cond_d

    .line 458083
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v0, p0, LX/2lX;->f:LX/2lJ;

    iget-object v3, v0, LX/2lJ;->l:Lcom/facebook/bookmark/model/Bookmark;

    const v4, 0x7f021910

    iget-object v0, p0, LX/2lX;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_12

    const v0, 0x7f083204

    :goto_2
    invoke-static {p1, v2, v3, v4, v0}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458084
    :cond_d
    if-nez p2, :cond_e

    if-eqz p4, :cond_e

    .line 458085
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v2, 0x7f021623

    const v3, 0x7f0818ee

    invoke-static {p1, v0, v6, v2, v3}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458086
    :cond_e
    if-nez p2, :cond_f

    .line 458087
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->m:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f02003d

    const v4, 0x7f0800cb

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458088
    :cond_f
    if-nez p2, :cond_10

    iget-object v0, p0, LX/2lX;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, LX/2lX;->e:LX/2lM;

    invoke-virtual {v0}, LX/2lM;->d()Z

    move-result v0

    if-nez v0, :cond_10

    .line 458089
    new-instance v0, LX/FBn;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 458090
    const v3, 0x7f020f17

    move v3, v3

    .line 458091
    iget-object v4, p0, LX/2lX;->e:LX/2lM;

    invoke-virtual {v4}, LX/2lM;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v6, v3, v4}, LX/FBn;-><init>(Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;ILjava/lang/CharSequence;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458092
    :cond_10
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v2, 0x7f020e65

    const v3, 0x7f083206

    invoke-static {p1, v0, v6, v2, v3}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458093
    return-object v1

    .line 458094
    :cond_11
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, LX/2lX;->f:LX/2lJ;

    iget-object v2, v2, LX/2lJ;->a:Lcom/facebook/bookmark/model/Bookmark;

    const v3, 0x7f0200a0

    const v4, 0x7f0831f4

    invoke-static {p1, v0, v2, v3, v4}, LX/FBn;->a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 458095
    :cond_12
    const v0, 0x7f083203

    goto/16 :goto_2

    .line 458096
    :cond_13
    iget-object v2, p0, LX/2lX;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_14

    iget-object v2, p0, LX/2lX;->d:LX/27w;

    invoke-virtual {v2}, LX/27w;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 458097
    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_1
.end method
