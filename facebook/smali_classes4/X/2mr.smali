.class public LX/2mr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0f8;


# instance fields
.field private a:LX/0hE;

.field public b:Landroid/app/Activity;

.field private final c:LX/0iI;


# direct methods
.method public constructor <init>(LX/0iI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 462167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462168
    iput-object p1, p0, LX/2mr;->c:LX/0iI;

    .line 462169
    return-void
.end method

.method public static a(LX/0QB;)LX/2mr;
    .locals 1

    .prologue
    .line 462166
    invoke-static {p0}, LX/2mr;->b(LX/0QB;)LX/2mr;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2mr;
    .locals 2

    .prologue
    .line 462164
    new-instance v1, LX/2mr;

    invoke-static {p0}, LX/0iI;->a(LX/0QB;)LX/0iI;

    move-result-object v0

    check-cast v0, LX/0iI;

    invoke-direct {v1, v0}, LX/2mr;-><init>(LX/0iI;)V

    .line 462165
    return-object v1
.end method


# virtual methods
.method public final a(LX/0hE;)V
    .locals 0

    .prologue
    .line 462152
    iput-object p1, p0, LX/2mr;->a:LX/0hE;

    .line 462153
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 462163
    iget-object v0, p0, LX/2mr;->a:LX/0hE;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2mr;->a:LX/0hE;

    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()LX/0hE;
    .locals 2

    .prologue
    .line 462160
    iget-object v0, p0, LX/2mr;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2mr;->c:LX/0iI;

    iget-object v1, p0, LX/2mr;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, LX/0iI;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/2mr;->a:LX/0hE;

    .line 462161
    iget-object v0, p0, LX/2mr;->a:LX/0hE;

    return-object v0

    .line 462162
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()LX/0hE;
    .locals 1

    .prologue
    .line 462159
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()LX/0hE;
    .locals 1

    .prologue
    .line 462158
    const/4 v0, 0x0

    return-object v0
.end method

.method public final o()LX/0hE;
    .locals 1

    .prologue
    .line 462157
    const/4 v0, 0x0

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 462154
    invoke-virtual {p0}, LX/2mr;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462155
    iget-object v0, p0, LX/2mr;->a:LX/0hE;

    invoke-interface {v0}, LX/0hE;->b()Z

    move-result v0

    .line 462156
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
