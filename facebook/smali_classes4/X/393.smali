.class public LX/393;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 522222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2pa;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 522216
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 522217
    :cond_0
    const/4 v0, 0x0

    .line 522218
    :goto_0
    return-object v0

    .line 522219
    :cond_1
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 522220
    instance-of v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 522221
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLMedia;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 522215
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bs()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 522209
    invoke-static {p0}, LX/393;->a(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 522210
    if-nez v0, :cond_0

    .line 522211
    const/4 v0, 0x0

    .line 522212
    :goto_0
    return-object v0

    .line 522213
    :cond_0
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 522214
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method

.method public static c(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 522205
    invoke-static {p0}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 522206
    if-nez v0, :cond_0

    .line 522207
    const/4 v0, 0x0

    .line 522208
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 522223
    invoke-static {p0}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 522224
    if-nez v0, :cond_0

    .line 522225
    const/4 v0, 0x0

    .line 522226
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(LX/2pa;)J
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 522200
    if-nez p0, :cond_1

    .line 522201
    :cond_0
    :goto_0
    return-wide v0

    .line 522202
    :cond_1
    invoke-static {p0}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 522203
    if-eqz v2, :cond_0

    .line 522204
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->bx()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public static f(LX/2pa;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 522197
    if-nez p0, :cond_0

    .line 522198
    const/4 v0, 0x0

    .line 522199
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static m(LX/2pa;)Z
    .locals 1

    .prologue
    .line 522196
    invoke-static {p0}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/393;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    return v0
.end method

.method public static n(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 522188
    invoke-static {p0}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 522189
    invoke-static {p0}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 522190
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->be()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    .line 522191
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p0

    if-nez p0, :cond_3

    .line 522192
    :cond_0
    :goto_0
    move v1, v2

    .line 522193
    if-nez v1, :cond_2

    .line 522194
    :cond_1
    const/4 v0, 0x0

    .line 522195
    :cond_2
    return-object v0

    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->W()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static o(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2pa;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 522175
    invoke-virtual {p0}, LX/2pa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 522176
    :goto_0
    return-object v0

    .line 522177
    :cond_0
    invoke-static {p0}, LX/393;->a(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 522178
    if-nez v2, :cond_1

    move-object v0, v1

    .line 522179
    goto :goto_0

    .line 522180
    :cond_1
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 522181
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 522182
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->as()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move-object v0, v1

    .line 522183
    goto :goto_0

    .line 522184
    :cond_3
    invoke-static {v2}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 522185
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v2, v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v3, :cond_5

    :cond_4
    move-object v0, v1

    .line 522186
    goto :goto_0

    .line 522187
    :cond_5
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method

.method public static p(LX/2pa;)Z
    .locals 2

    .prologue
    .line 522172
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "DidBroadcastStatusChangeKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522173
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "DidBroadcastStatusChangeKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 522174
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(LX/2pa;)Z
    .locals 2

    .prologue
    .line 522169
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "DidScheduledLiveStartTimeChangeKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522170
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "DidScheduledLiveStartTimeChangeKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 522171
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
