.class public abstract LX/3IQ;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 546328
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 546329
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3IQ;->a:Z

    .line 546330
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 546331
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 546332
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3IQ;->a:Z

    .line 546333
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 546334
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 546335
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3IQ;->a:Z

    .line 546336
    return-void
.end method


# virtual methods
.method public final a(F)F
    .locals 1

    .prologue
    .line 546337
    invoke-virtual {p0}, LX/3IQ;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 546338
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3IQ;->a:Z

    .line 546339
    invoke-virtual {p0}, LX/3IQ;->invalidate()V

    .line 546340
    return-void
.end method
