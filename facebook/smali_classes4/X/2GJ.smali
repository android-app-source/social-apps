.class public LX/2GJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public A:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field public B:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private C:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private D:Z

.field private final E:Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;

.field private final F:Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;

.field private final G:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation
.end field

.field public final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/util/concurrent/ScheduledExecutorService;

.field public final c:LX/0SG;

.field private final d:LX/0Sy;

.field private final e:LX/0Uo;

.field private final f:LX/0Xw;

.field private final g:LX/0Un;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2VC;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Sh;

.field public final j:LX/0Zm;

.field public final k:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final l:LX/0Sj;

.field private final m:LX/03V;

.field private final n:LX/1mP;

.field private final o:LX/0Zb;

.field public final p:J

.field public final q:J

.field private final r:J

.field private final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1Eh;",
            ">;>;"
        }
    .end annotation
.end field

.field private final t:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2VA;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2VA;",
            ">;"
        }
    .end annotation
.end field

.field public final w:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final x:LX/0Vq;

.field public y:I

.field public z:LX/2VA;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;LX/0Sy;LX/0Uo;LX/0Xw;LX/0Ot;LX/0Un;LX/0Or;LX/0Sh;LX/0SG;LX/0Zm;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Sj;LX/03V;LX/1mP;LX/0Zb;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 6
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p17    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p18    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p19    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            "LX/0Uo;",
            "LX/0Xw;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1Eh;",
            ">;>;",
            "LX/0Un;",
            "LX/0Or",
            "<",
            "LX/2VC;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0SG;",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Sj;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1mP;",
            "LX/0Zb;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 388277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388278
    new-instance v2, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$RunnableEnsureBackgroundTasksScheduledInNonUiThread;

    invoke-direct {v2, p0}, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$RunnableEnsureBackgroundTasksScheduledInNonUiThread;-><init>(LX/2GJ;)V

    iput-object v2, p0, LX/2GJ;->E:Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;

    .line 388279
    new-instance v2, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$RunnableLetAllTasksRunOnceWithNoDelay;

    invoke-direct {v2, p0}, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$RunnableLetAllTasksRunOnceWithNoDelay;-><init>(LX/2GJ;)V

    iput-object v2, p0, LX/2GJ;->F:Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;

    .line 388280
    new-instance v2, LX/2GL;

    invoke-direct {v2, p0}, LX/2GL;-><init>(LX/2GJ;)V

    iput-object v2, p0, LX/2GJ;->G:LX/0TF;

    .line 388281
    invoke-virtual/range {p17 .. p17}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 388282
    invoke-virtual/range {p17 .. p17}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual/range {p18 .. p18}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 388283
    iput-object p1, p0, LX/2GJ;->a:Ljava/util/concurrent/ExecutorService;

    .line 388284
    iput-object p2, p0, LX/2GJ;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 388285
    iput-object p3, p0, LX/2GJ;->d:LX/0Sy;

    .line 388286
    iput-object p4, p0, LX/2GJ;->e:LX/0Uo;

    .line 388287
    iput-object p5, p0, LX/2GJ;->f:LX/0Xw;

    .line 388288
    iput-object p7, p0, LX/2GJ;->g:LX/0Un;

    .line 388289
    iput-object p8, p0, LX/2GJ;->h:LX/0Or;

    .line 388290
    iput-object p9, p0, LX/2GJ;->i:LX/0Sh;

    .line 388291
    move-object/from16 v0, p10

    iput-object v0, p0, LX/2GJ;->c:LX/0SG;

    .line 388292
    move-object/from16 v0, p11

    iput-object v0, p0, LX/2GJ;->j:LX/0Zm;

    .line 388293
    move-object/from16 v0, p12

    iput-object v0, p0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 388294
    move-object/from16 v0, p13

    iput-object v0, p0, LX/2GJ;->l:LX/0Sj;

    .line 388295
    move-object/from16 v0, p14

    iput-object v0, p0, LX/2GJ;->m:LX/03V;

    .line 388296
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2GJ;->n:LX/1mP;

    .line 388297
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2GJ;->o:LX/0Zb;

    .line 388298
    invoke-virtual/range {p17 .. p17}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, LX/2GJ;->p:J

    .line 388299
    invoke-virtual/range {p18 .. p18}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, LX/2GJ;->q:J

    .line 388300
    invoke-virtual/range {p19 .. p19}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, LX/2GJ;->r:J

    .line 388301
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    iput-object v2, p0, LX/2GJ;->w:Ljava/util/Map;

    .line 388302
    iput-object p6, p0, LX/2GJ;->s:LX/0Ot;

    .line 388303
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, LX/2GJ;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 388304
    const/4 v2, 0x0

    iput v2, p0, LX/2GJ;->y:I

    .line 388305
    new-instance v2, LX/2GM;

    invoke-direct {v2, p0}, LX/2GM;-><init>(LX/2GJ;)V

    iput-object v2, p0, LX/2GJ;->x:LX/0Vq;

    .line 388306
    iget-object v2, p0, LX/2GJ;->d:LX/0Sy;

    iget-object v3, p0, LX/2GJ;->x:LX/0Vq;

    invoke-virtual {v2, v3}, LX/0Sy;->a(LX/0Vq;)V

    .line 388307
    iget-object v2, p0, LX/2GJ;->f:LX/0Xw;

    new-instance v3, LX/2GN;

    invoke-direct {v3, p0}, LX/2GN;-><init>(LX/2GJ;)V

    new-instance v4, Landroid/content/IntentFilter;

    sget-object v5, LX/0Uo;->a:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 388308
    iget-object v2, p0, LX/2GJ;->e:LX/0Uo;

    invoke-virtual {v2}, LX/0Uo;->j()Z

    move-result v2

    iput-boolean v2, p0, LX/2GJ;->D:Z

    .line 388309
    iget-object v2, p0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x270001

    const/16 v4, 0xa

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 388310
    return-void

    .line 388311
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 388312
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method private a(J)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 388275
    iget-object v0, p0, LX/2GJ;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$3;

    const-string v2, "BackgroundTaskRunner"

    const-string v3, "runAnyBackgroundTasksDelayed"

    invoke-direct {v1, p0, v2, v3}, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$3;-><init>(LX/2GJ;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2GJ;->A:Ljava/util/concurrent/Future;

    .line 388276
    return-void
.end method

.method public static a(LX/2GJ;LX/2VA;Ljava/lang/Throwable;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 388273
    const-string v0, "wantsToBeRunNow"

    invoke-static {p0, v0, p1, p2}, LX/2GJ;->a(LX/2GJ;Ljava/lang/String;LX/2VA;Ljava/lang/Throwable;)V

    .line 388274
    return-void
.end method

.method public static a(LX/2GJ;Ljava/lang/String;LX/2VA;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 388268
    invoke-static {p3}, LX/1mP;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388269
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "background_task_exception"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "type"

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "msg"

    invoke-virtual {p3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 388270
    iget-object v1, p0, LX/2GJ;->o:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 388271
    :goto_0
    return-void

    .line 388272
    :cond_0
    iget-object v0, p0, LX/2GJ;->m:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BackgroundTaskRunner_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Task threw exception"

    invoke-virtual {v0, v1, v2, p3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(LX/2GJ;Ljava/lang/Iterable;)Ljava/util/Collection;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/1Eh;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "LX/2Tv",
            "<",
            "LX/1Eh;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 388242
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 388243
    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    .line 388244
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Eh;

    .line 388245
    new-instance v2, LX/2Tv;

    invoke-direct {v2, v0}, LX/2Tv;-><init>(Ljava/lang/Object;)V

    .line 388246
    invoke-interface {v0}, LX/1Eh;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388247
    invoke-interface {v0}, LX/1Eh;->e()I

    move-result v5

    .line 388248
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 388249
    if-nez v0, :cond_0

    .line 388250
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 388251
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388252
    :cond_0
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 388253
    :cond_1
    invoke-interface {v4}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v5

    .line 388254
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Tv;

    .line 388255
    iget-object v1, v0, LX/2Tv;->a:Ljava/lang/Object;

    check-cast v1, LX/1Eh;

    invoke-interface {v1}, LX/1Eh;->c()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 388256
    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Tv;

    .line 388257
    if-nez v2, :cond_3

    .line 388258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing node for dependency: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 388259
    :cond_3
    iget-object v1, v2, LX/2Tv;->a:Ljava/lang/Object;

    check-cast v1, LX/1Eh;

    invoke-interface {v1}, LX/1Eh;->e()I

    move-result v8

    iget-object v1, v0, LX/2Tv;->a:Ljava/lang/Object;

    check-cast v1, LX/1Eh;

    invoke-interface {v1}, LX/1Eh;->e()I

    move-result v1

    if-ge v8, v1, :cond_4

    .line 388260
    const-string v8, "%s has priority %d but depends on %s with lesser priority %d"

    const/4 v1, 0x4

    new-array v9, v1, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v1, v0, LX/2Tv;->a:Ljava/lang/Object;

    check-cast v1, LX/1Eh;

    invoke-interface {v1}, LX/1Eh;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v10

    const/4 v10, 0x1

    iget-object v1, v0, LX/2Tv;->a:Ljava/lang/Object;

    check-cast v1, LX/1Eh;

    invoke-interface {v1}, LX/1Eh;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v9, v10

    const/4 v10, 0x2

    iget-object v1, v2, LX/2Tv;->a:Ljava/lang/Object;

    check-cast v1, LX/1Eh;

    invoke-interface {v1}, LX/1Eh;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v10

    const/4 v10, 0x3

    iget-object v1, v2, LX/2Tv;->a:Ljava/lang/Object;

    check-cast v1, LX/1Eh;

    invoke-interface {v1}, LX/1Eh;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 388261
    iget-object v2, p0, LX/2GJ;->m:LX/03V;

    const-string v8, "BackgroundTaskRunner"

    invoke-virtual {v2, v8, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 388262
    :cond_4
    invoke-virtual {v2, v0}, LX/2Tv;->a(LX/2Tv;)V

    goto/16 :goto_1

    .line 388263
    :cond_5
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 388264
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iget-object v2, v0, LX/2Tv;->a:Ljava/lang/Object;

    check-cast v2, LX/1Eh;

    invoke-interface {v2}, LX/1Eh;->e()I

    move-result v2

    if-le v8, v2, :cond_6

    .line 388265
    invoke-interface {v4, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Tv;

    .line 388266
    invoke-virtual {v1, v0}, LX/2Tv;->a(LX/2Tv;)V

    goto :goto_2

    .line 388267
    :cond_7
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/2GJ;LX/2VA;)Z
    .locals 3

    .prologue
    .line 388214
    invoke-static {p0}, LX/2GJ;->o(LX/2GJ;)V

    .line 388215
    iget-object v0, p0, LX/2GJ;->u:Ljava/util/Map;

    move-object v1, v0

    .line 388216
    iget-object v0, p1, LX/2VA;->a:LX/1Eh;

    move-object v0, v0

    .line 388217
    invoke-interface {v0}, LX/1Eh;->c()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 388218
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2VA;

    .line 388219
    invoke-virtual {v0}, LX/2VA;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388220
    const/4 v0, 0x1

    .line 388221
    :goto_0
    move v0, v0

    .line 388222
    if-nez v0, :cond_2

    invoke-direct {p0, p1}, LX/2GJ;->d(LX/2VA;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 388223
    iget-object v0, p1, LX/2VA;->a:LX/1Eh;

    move-object v0, v0

    .line 388224
    invoke-interface {v0}, LX/1Eh;->d()LX/0Rf;

    move-result-object v0

    .line 388225
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 388226
    iget-object v2, p0, LX/2GJ;->w:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388227
    const/4 v0, 0x0

    .line 388228
    :goto_1
    move v0, v0

    .line 388229
    if-eqz v0, :cond_2

    .line 388230
    iget-object v0, p0, LX/2GJ;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2VC;

    .line 388231
    iget-object v1, p1, LX/2VA;->a:LX/1Eh;

    move-object v1, v1

    .line 388232
    const/4 p0, 0x1

    .line 388233
    invoke-interface {v1}, LX/1Eh;->h()Ljava/util/Set;

    move-result-object v2

    .line 388234
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_5

    move v2, p0

    .line 388235
    :goto_2
    move v0, v2

    .line 388236
    move v0, v0

    .line 388237
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_1

    .line 388238
    :cond_5
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2VD;

    .line 388239
    invoke-static {v0, v2}, LX/2VC;->a(LX/2VC;LX/2VD;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 388240
    const/4 v2, 0x0

    goto :goto_2

    :cond_7
    move v2, p0

    .line 388241
    goto :goto_2
.end method

.method private d(LX/2VA;)Z
    .locals 3

    .prologue
    .line 388207
    iget-object v0, p0, LX/2GJ;->g:LX/0Un;

    .line 388208
    iget-object v1, p1, LX/2VA;->a:LX/1Eh;

    move-object v1, v1

    .line 388209
    invoke-interface {v1}, LX/1Eh;->b()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 388210
    iget-object p1, v0, LX/0Un;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {p1, v2}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 388211
    const/4 v2, 0x1

    .line 388212
    :goto_0
    move v0, v2

    .line 388213
    return v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static e(LX/2GJ;)V
    .locals 2

    .prologue
    .line 388205
    iget-object v0, p0, LX/2GJ;->F:Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;

    iget-object v1, p0, LX/2GJ;->a:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, v1}, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;->a(Ljava/util/concurrent/ExecutorService;)V

    .line 388206
    return-void
.end method

.method public static f(LX/2GJ;)V
    .locals 2

    .prologue
    .line 388313
    iget-object v0, p0, LX/2GJ;->E:Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;

    iget-object v1, p0, LX/2GJ;->a:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, v1}, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$SingularlyQueuedRunnable;->a(Ljava/util/concurrent/ExecutorService;)V

    .line 388314
    return-void
.end method

.method public static g(LX/2VA;)I
    .locals 1

    .prologue
    .line 388203
    iget-object v0, p0, LX/2VA;->a:LX/1Eh;

    move-object v0, v0

    .line 388204
    invoke-interface {v0}, LX/1Eh;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public static g(LX/2GJ;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 388201
    iget-object v0, p0, LX/2GJ;->i:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 388202
    return-void
.end method

.method public static h(LX/2GJ;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 388192
    invoke-static {p0}, LX/2GJ;->g(LX/2GJ;)V

    .line 388193
    iget-object v0, p0, LX/2GJ;->A:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 388194
    iget-object v0, p0, LX/2GJ;->A:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 388195
    iput-object v2, p0, LX/2GJ;->A:Ljava/util/concurrent/Future;

    .line 388196
    :cond_0
    iget-object v0, p0, LX/2GJ;->B:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    .line 388197
    iget-object v0, p0, LX/2GJ;->B:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 388198
    iput-object v2, p0, LX/2GJ;->B:Ljava/util/concurrent/Future;

    .line 388199
    :cond_1
    invoke-direct {p0}, LX/2GJ;->i()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LX/2GJ;->a(J)V

    .line 388200
    return-void
.end method

.method private i()J
    .locals 14
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 388175
    iget-object v0, p0, LX/2GJ;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 388176
    iget-wide v0, p0, LX/2GJ;->r:J

    add-long v2, v6, v0

    .line 388177
    invoke-static {p0}, LX/2GJ;->n(LX/2GJ;)Ljava/util/List;

    move-result-object v1

    .line 388178
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2VA;

    .line 388179
    iget-boolean v9, v0, LX/2VA;->d:Z

    move v0, v9

    .line 388180
    if-nez v0, :cond_0

    move-wide v0, v4

    .line 388181
    :goto_0
    return-wide v0

    .line 388182
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2VA;

    .line 388183
    invoke-static {p0, v0}, LX/2GJ;->b(LX/2GJ;LX/2VA;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 388184
    iget-object v1, p0, LX/2GJ;->c:LX/0SG;

    .line 388185
    invoke-virtual {v0, v1}, LX/2VA;->a(LX/0SG;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 388186
    iget-wide v12, v0, LX/2VA;->c:J

    .line 388187
    :goto_2
    move-wide v0, v12

    .line 388188
    const-wide/16 v10, -0x1

    cmp-long v9, v0, v10

    if-eqz v9, :cond_4

    .line 388189
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    :goto_3
    move-wide v2, v0

    .line 388190
    goto :goto_1

    .line 388191
    :cond_3
    sub-long v0, v2, v6

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0

    :cond_4
    move-wide v0, v2

    goto :goto_3

    :cond_5
    iget-object v12, v0, LX/2VA;->a:LX/1Eh;

    invoke-interface {v12}, LX/1Eh;->f()J

    move-result-wide v12

    goto :goto_2
.end method

.method public static j(LX/2GJ;)LX/2VA;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 388168
    iget-object v0, p0, LX/2GJ;->z:LX/2VA;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 388169
    iget-object v0, p0, LX/2GJ;->z:LX/2VA;

    .line 388170
    iput-object v3, p0, LX/2GJ;->z:LX/2VA;

    .line 388171
    iget-object v1, p0, LX/2GJ;->C:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_0

    .line 388172
    iget-object v1, p0, LX/2GJ;->C:Ljava/util/concurrent/Future;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 388173
    iput-object v3, p0, LX/2GJ;->C:Ljava/util/concurrent/Future;

    .line 388174
    :cond_0
    return-object v0
.end method

.method public static n(LX/2GJ;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2VA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388166
    invoke-static {p0}, LX/2GJ;->o(LX/2GJ;)V

    .line 388167
    iget-object v0, p0, LX/2GJ;->v:Ljava/util/List;

    return-object v0
.end method

.method public static declared-synchronized o(LX/2GJ;)V
    .locals 5

    .prologue
    .line 388143
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2GJ;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 388144
    :goto_0
    monitor-exit p0

    return-void

    .line 388145
    :cond_0
    :try_start_1
    const-string v0, "backgroundTaskInitialization"

    const v1, 0x3134404d    # 2.6229998E-9f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 388146
    :try_start_2
    iget-object v0, p0, LX/2GJ;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 388147
    invoke-static {p0, v0}, LX/2GJ;->b(LX/2GJ;Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    .line 388148
    invoke-static {v1}, LX/2V9;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 388149
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 388150
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Tv;

    .line 388151
    new-instance v4, LX/2VA;

    iget-object v1, v1, LX/2Tv;->a:Ljava/lang/Object;

    check-cast v1, LX/1Eh;

    invoke-direct {v4, v1}, LX/2VA;-><init>(LX/1Eh;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 388152
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 388153
    iput-object v0, p0, LX/2GJ;->v:Ljava/util/List;

    .line 388154
    iget-object v0, p0, LX/2GJ;->v:Ljava/util/List;

    .line 388155
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 388156
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2VA;

    .line 388157
    invoke-virtual {v1}, LX/2VA;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_2

    .line 388158
    :cond_2
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    move-object v0, v1

    .line 388159
    iput-object v0, p0, LX/2GJ;->u:Ljava/util/Map;

    .line 388160
    iget-object v0, p0, LX/2GJ;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2VA;

    .line 388161
    iget-object v2, v0, LX/2VA;->a:LX/1Eh;

    move-object v0, v2

    .line 388162
    invoke-interface {v0, p0}, LX/1Eh;->a(LX/2GJ;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 388163
    :catchall_0
    move-exception v0

    const v1, -0x2b3b4b4f

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 388164
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 388165
    :cond_3
    const v0, -0x3d501f42

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;Z)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 388129
    const v5, 0x270001

    .line 388130
    invoke-static {p0}, LX/2GJ;->g(LX/2GJ;)V

    .line 388131
    invoke-static {p0}, LX/2GJ;->j(LX/2GJ;)LX/2VA;

    move-result-object v0

    .line 388132
    iget-object v1, p0, LX/2GJ;->j:LX/0Zm;

    invoke-virtual {v1}, LX/0Zm;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 388133
    invoke-static {v0}, LX/2GJ;->g(LX/2VA;)I

    move-result v2

    .line 388134
    iget-object v3, p0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v4, "exception"

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {v3, v5, v2, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 388135
    iget-object v1, p0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v3, 0x3

    invoke-interface {v1, v5, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 388136
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2VA;->b(Z)V

    .line 388137
    const-string v1, "start"

    invoke-static {p0, v1, v0, p1}, LX/2GJ;->a(LX/2GJ;Ljava/lang/String;LX/2VA;Ljava/lang/Throwable;)V

    .line 388138
    iget-object v1, p0, LX/2GJ;->c:LX/0SG;

    iget-wide v2, p0, LX/2GJ;->p:J

    iget-wide v4, p0, LX/2GJ;->q:J

    invoke-virtual/range {v0 .. v5}, LX/2VA;->a(LX/0SG;JJ)V

    .line 388139
    if-eqz p2, :cond_1

    .line 388140
    invoke-static {p0}, LX/2GJ;->h(LX/2GJ;)V

    .line 388141
    :cond_1
    return-void

    .line 388142
    :cond_2
    const-string v1, "null"

    goto :goto_0
.end method

.method public final c()V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 388123
    iget-object v0, p0, LX/2GJ;->e:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    .line 388124
    iget-boolean v1, p0, LX/2GJ;->D:Z

    if-eq v1, v0, :cond_0

    .line 388125
    iput-boolean v0, p0, LX/2GJ;->D:Z

    .line 388126
    iget-boolean v0, p0, LX/2GJ;->D:Z

    if-nez v0, :cond_0

    .line 388127
    invoke-static {p0}, LX/2GJ;->f(LX/2GJ;)V

    .line 388128
    :cond_0
    return-void
.end method

.method public final declared-synchronized d()V
    .locals 12
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 388072
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2GJ;->g(LX/2GJ;)V

    .line 388073
    iget-object v0, p0, LX/2GJ;->z:LX/2VA;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 388074
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 388075
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2GJ;->d:LX/0Sy;

    invoke-virtual {v0}, LX/0Sy;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388076
    const v9, 0x270001

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 388077
    invoke-static {p0}, LX/2GJ;->n(LX/2GJ;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    .line 388078
    :goto_1
    if-ge v2, v4, :cond_2

    .line 388079
    invoke-static {p0}, LX/2GJ;->n(LX/2GJ;)Ljava/util/List;

    move-result-object v6

    .line 388080
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 388081
    const/4 v1, 0x0

    move v5, v1

    :goto_2
    if-ge v5, v7, :cond_a

    .line 388082
    iget v1, p0, LX/2GJ;->y:I

    add-int/2addr v1, v5

    rem-int v8, v1, v7

    .line 388083
    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2VA;

    .line 388084
    const/4 v10, 0x1

    .line 388085
    iput-boolean v10, v1, LX/2VA;->d:Z

    .line 388086
    iget-object v10, p0, LX/2GJ;->c:LX/0SG;

    invoke-virtual {v1, v10}, LX/2VA;->a(LX/0SG;)Z

    move-result v10

    if-nez v10, :cond_9

    const/4 v10, 0x0

    .line 388087
    invoke-static {p0, v1}, LX/2GJ;->b(LX/2GJ;LX/2VA;)Z

    move-result v11

    if-nez v11, :cond_b

    .line 388088
    :goto_3
    move v10, v10

    .line 388089
    if-eqz v10, :cond_9

    .line 388090
    add-int/lit8 v5, v8, 0x1

    rem-int/2addr v5, v7

    iput v5, p0, LX/2GJ;->y:I

    .line 388091
    :goto_4
    move-object v5, v1

    .line 388092
    if-nez v5, :cond_4

    .line 388093
    :cond_2
    :goto_5
    move-object v0, v0

    .line 388094
    if-eqz v0, :cond_3

    .line 388095
    iget-object v1, p0, LX/2GJ;->G:LX/0TF;

    iget-object v2, p0, LX/2GJ;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 388096
    iget-object v1, p0, LX/2GJ;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$4;

    const-string v3, "BackgroundTaskRunner"

    const-string v4, "checkTaskCompletion"

    invoke-direct {v2, p0, v3, v4, v0}, Lcom/facebook/backgroundtasks/BackgroundTaskRunner$4;-><init>(LX/2GJ;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/util/concurrent/ListenableFuture;)V

    const-wide/16 v4, 0xb4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v4, v5, v0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2GJ;->C:Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388097
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 388098
    :cond_3
    :try_start_2
    invoke-static {p0}, LX/2GJ;->f(LX/2GJ;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 388099
    :cond_4
    :try_start_3
    iput-object v5, p0, LX/2GJ;->z:LX/2VA;

    .line 388100
    iget-object v1, p0, LX/2GJ;->j:LX/0Zm;

    invoke-virtual {v1}, LX/0Zm;->b()Z

    move-result v6

    .line 388101
    if-eqz v6, :cond_5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 388102
    :try_start_4
    iget-object v1, p0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v7, 0x270001

    invoke-static {v5}, LX/2GJ;->g(LX/2VA;)I

    move-result v8

    invoke-interface {v1, v7, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 388103
    :cond_5
    iget-object v1, p0, LX/2GJ;->z:LX/2VA;

    iget-object v7, p0, LX/2GJ;->l:LX/0Sj;

    .line 388104
    const-string v8, "BackgroundTask"

    iget-object v10, v1, LX/2VA;->a:LX/1Eh;

    invoke-interface {v10}, LX/1Eh;->a()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v8, v10}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v8

    iput-object v8, v1, LX/2VA;->e:LX/0cV;

    .line 388105
    iget-object v8, v1, LX/2VA;->e:LX/0cV;

    if-eqz v8, :cond_6

    .line 388106
    iget-object v8, v1, LX/2VA;->e:LX/0cV;

    invoke-interface {v8}, LX/0cV;->a()V

    .line 388107
    :cond_6
    iget-object v8, v1, LX/2VA;->a:LX/1Eh;

    invoke-interface {v8}, LX/1Eh;->j()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    move-object v1, v8
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 388108
    :try_start_5
    if-nez v1, :cond_8

    .line 388109
    if-eqz v6, :cond_7

    .line 388110
    iget-object v1, p0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v5}, LX/2GJ;->g(LX/2VA;)I

    move-result v6

    invoke-interface {v1, v9, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 388111
    :cond_7
    const/4 v1, 0x1

    invoke-virtual {v5, v1}, LX/2VA;->b(Z)V

    .line 388112
    invoke-static {p0}, LX/2GJ;->j(LX/2GJ;)LX/2VA;

    .line 388113
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_1

    .line 388114
    :catch_0
    move-exception v1

    .line 388115
    invoke-virtual {p0, v1, v3}, LX/2GJ;->a(Ljava/lang/Throwable;Z)V

    goto :goto_5

    :cond_8
    move-object v0, v1

    .line 388116
    goto :goto_5

    .line 388117
    :cond_9
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_2

    .line 388118
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 388119
    :cond_b
    :try_start_6
    iget-object v11, v1, LX/2VA;->a:LX/1Eh;

    move-object v11, v11

    .line 388120
    invoke-interface {v11}, LX/1Eh;->i()Z
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v10

    goto/16 :goto_3

    .line 388121
    :catch_1
    move-exception v11

    .line 388122
    invoke-static {p0, v1, v11}, LX/2GJ;->a(LX/2GJ;LX/2VA;Ljava/lang/Throwable;)V

    goto/16 :goto_3
.end method
