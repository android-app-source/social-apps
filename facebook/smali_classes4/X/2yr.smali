.class public LX/2yr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/CHQ;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 482757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482758
    iput-object p1, p0, LX/2yr;->a:LX/0Ot;

    .line 482759
    return-void
.end method

.method public static a(LX/0QB;)LX/2yr;
    .locals 1

    .prologue
    .line 482760
    invoke-static {p0}, LX/2yr;->b(LX/0QB;)LX/2yr;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2yr;
    .locals 2

    .prologue
    .line 482761
    new-instance v0, LX/2yr;

    const/16 v1, 0x2531

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2yr;-><init>(LX/0Ot;)V

    .line 482762
    return-object v0
.end method

.method private b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 482763
    invoke-static {p0, p1, p2}, LX/2yr;->e(LX/2yr;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 482764
    :cond_0
    :goto_0
    return-void

    .line 482765
    :cond_1
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 482766
    invoke-static {v2}, LX/1xP;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "catalog_id"

    invoke-virtual {v2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "product_id"

    invoke-virtual {v2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "product_view"

    invoke-virtual {v2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 482767
    iget-object v1, p0, LX/2yr;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    const-string v3, "catalog_id"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "product_id"

    invoke-virtual {v2, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "product_view"

    invoke-virtual {v2, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 482768
    new-instance v12, LX/CId;

    invoke-direct {v12, p1, v3}, LX/CId;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 482769
    const/4 v6, 0x1

    .line 482770
    iput-boolean v6, v12, LX/CId;->g:Z

    .line 482771
    const/4 v9, 0x0

    move-object v6, v1

    move-object v7, p1

    move-object v8, v3

    move-object v10, v4

    move-object v11, v2

    invoke-static/range {v6 .. v12}, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a(Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CId;)LX/CHQ;

    move-result-object v6

    move-object v1, v6

    .line 482772
    iput-object v1, p0, LX/2yr;->b:LX/CHQ;

    .line 482773
    const/4 v1, 0x1

    .line 482774
    :goto_1
    move v0, v1

    .line 482775
    if-nez v0, :cond_0

    .line 482776
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 482777
    invoke-static {v2}, LX/1xP;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "catalog_id"

    invoke-virtual {v2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "catalog_view"

    invoke-virtual {v2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 482778
    iget-object v1, p0, LX/2yr;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    const-string v3, "catalog_id"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "catalog_view"

    invoke-virtual {v2, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v9, 0x0

    .line 482779
    new-instance v11, LX/CId;

    invoke-direct {v11, p1, v3}, LX/CId;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 482780
    const/4 v5, 0x1

    .line 482781
    iput-boolean v5, v11, LX/CId;->g:Z

    .line 482782
    move-object v5, v1

    move-object v6, p1

    move-object v7, v3

    move-object v8, v2

    move-object v10, v9

    .line 482783
    invoke-static/range {v5 .. v11}, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a(Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CId;)LX/CHQ;

    move-result-object v5

    move-object v1, v5

    .line 482784
    iput-object v1, p0, LX/2yr;->b:LX/CHQ;

    .line 482785
    :goto_2
    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    goto :goto_2
.end method

.method private static e(LX/2yr;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 482786
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 482787
    invoke-static {v1}, LX/1xP;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "id"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 482788
    iget-object v0, p0, LX/2yr;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    const-string v2, "id"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 482789
    new-instance v2, LX/CId;

    invoke-direct {v2, p1}, LX/CId;-><init>(Landroid/content/Context;)V

    .line 482790
    iput-object v1, v2, LX/CId;->d:Ljava/lang/String;

    .line 482791
    const/4 p2, 0x1

    .line 482792
    iput-boolean p2, v2, LX/CId;->g:Z

    .line 482793
    invoke-static {v0, v1, v2}, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a(Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;Ljava/lang/String;LX/CId;)LX/CHQ;

    move-result-object v2

    move-object v0, v2

    .line 482794
    iput-object v0, p0, LX/2yr;->b:LX/CHQ;

    .line 482795
    const/4 v0, 0x1

    .line 482796
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 482797
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482798
    :goto_0
    return-void

    .line 482799
    :cond_0
    invoke-direct {p0, p1, p2}, LX/2yr;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
