.class public LX/2mj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1AV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1AV",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final b:LX/1Bv;

.field private final c:LX/1C2;


# direct methods
.method public constructor <init>(LX/1AV;LX/1Bv;LX/1C2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 461232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 461233
    iput-object p1, p0, LX/2mj;->a:LX/1AV;

    .line 461234
    iput-object p2, p0, LX/2mj;->b:LX/1Bv;

    .line 461235
    iput-object p3, p0, LX/2mj;->c:LX/1C2;

    .line 461236
    return-void
.end method

.method public static a(LX/0QB;)LX/2mj;
    .locals 6

    .prologue
    .line 461237
    const-class v1, LX/2mj;

    monitor-enter v1

    .line 461238
    :try_start_0
    sget-object v0, LX/2mj;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 461239
    sput-object v2, LX/2mj;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 461240
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461241
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 461242
    new-instance p0, LX/2mj;

    invoke-static {v0}, LX/1AV;->a(LX/0QB;)LX/1AV;

    move-result-object v3

    check-cast v3, LX/1AV;

    invoke-static {v0}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v4

    check-cast v4, LX/1Bv;

    invoke-static {v0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v5

    check-cast v5, LX/1C2;

    invoke-direct {p0, v3, v4, v5}, LX/2mj;-><init>(LX/1AV;LX/1Bv;LX/1C2;)V

    .line 461243
    move-object v0, p0

    .line 461244
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 461245
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2mj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 461246
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 461247
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/2oL;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;LX/3FX;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "LX/2oL;",
            "LX/093;",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "Lcom/facebook/video/analytics/VideoFeedStoryInfo;",
            "LX/04D;",
            "LX/3FX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 461248
    iget-object v0, p2, LX/2oL;->c:LX/2oV;

    move-object v0, v0

    .line 461249
    iget-object v1, p2, LX/2oL;->c:LX/2oV;

    move-object v1, v1

    .line 461250
    if-nez v1, :cond_0

    .line 461251
    iget-object v6, p0, LX/2mj;->b:LX/1Bv;

    iget-object v7, p0, LX/2mj;->c:LX/1C2;

    move-object v0, p7

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-interface/range {v0 .. v7}, LX/3FX;->a(LX/2oL;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;LX/1Bv;LX/1C2;)LX/2oV;

    move-result-object v0

    .line 461252
    iput-object v0, p2, LX/2oL;->c:LX/2oV;

    .line 461253
    :cond_0
    iget-object v1, p0, LX/2mj;->a:LX/1AV;

    invoke-virtual {v1, p1, v0}, LX/1AV;->a(Landroid/view/View;LX/2oV;)V

    .line 461254
    return-void
.end method
