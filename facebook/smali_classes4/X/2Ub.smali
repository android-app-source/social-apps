.class public final LX/2Ub;
.super Ljava/util/HashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 416252
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 416253
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "buddy_list"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416254
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "create_thread"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416255
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "create_thread_response"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416256
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "delete_thread_notification"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416257
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "delete_messages_notification"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416258
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "orca_message_notifications"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416259
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "friending_state_change"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416260
    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "friend_request"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416261
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "friend_requests_seen"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416262
    const/16 v0, 0x9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "graphql"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416263
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "group_msg"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416264
    const/16 v0, 0xb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "group_notifs_unseen"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416265
    const/16 v0, 0xc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "group_msgs_unseen"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416266
    const/16 v0, 0xd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "inbox"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416267
    const/16 v0, 0xe

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "action_id_notification"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416268
    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "aura_notification"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416269
    const/16 v0, 0x10

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "aura_signal"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416270
    const/16 v0, 0x11

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "friends_locations"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416271
    const/16 v0, 0x12

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "mark_thread"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416272
    const/16 v0, 0x13

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "mark_thread_response"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416273
    const/16 v0, 0x14

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "mercury"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416274
    const/16 v0, 0x15

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "messenger_sync"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416275
    const/16 v0, 0x16

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "messenger_sync_ack"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416276
    const/16 v0, 0x17

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "messenger_sync_create_queue"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416277
    const/16 v0, 0x18

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "messenger_sync_get_diffs"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416278
    const/16 v0, 0x19

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "messaging"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416279
    const/16 v0, 0x1a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "messaging_events"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416280
    const/16 v0, 0x1b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "mobile_requests_count"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416281
    const/16 v0, 0x1c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "mobile_video_encode"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416282
    const/16 v0, 0x1d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "orca_notification_updates"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416283
    const/16 v0, 0x1e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "notifications_sync"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416284
    const/16 v0, 0x1f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "notifications_read"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416285
    const/16 v0, 0x20

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "notifications_seen"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416286
    const/16 v0, 0x21

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "push_notification"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416287
    const/16 v0, 0x22

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "pp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416288
    const/16 v0, 0x23

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "orca_presence"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416289
    const/16 v0, 0x24

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "privacy_changed"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416290
    const/16 v0, 0x25

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "privacy_updates"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416291
    const/16 v0, 0x26

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "send_additional_contacts"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416292
    const/16 v0, 0x27

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "send_chat_event"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416293
    const/16 v0, 0x28

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "send_delivery_receipt"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416294
    const/16 v0, 0x29

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "send_endpoint_capabilities"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416295
    const/16 v0, 0x2a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "foreground_state"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416296
    const/16 v0, 0x2b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "aura_location"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416297
    const/16 v0, 0x2c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "send_location"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416298
    const/16 v0, 0x2d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "send_message2"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416299
    const/16 v0, 0x2e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "send_message"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416300
    const/16 v0, 0x2f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "send_message_response"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416301
    const/16 v0, 0x30

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "ping"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416302
    const/16 v0, 0x31

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "presence"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416303
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "send_push_notification_ack"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416304
    const/16 v0, 0x33

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "rich_presence"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416305
    const/16 v0, 0x34

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "send_skype"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416306
    const/16 v0, 0x35

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "typing"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416307
    const/16 v0, 0x36

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "set_client_settings"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416308
    const/16 v0, 0x37

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "shoerack_notifications"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416309
    const/16 v0, 0x38

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "orca_ticker_updates"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416310
    const/16 v0, 0x39

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "orca_typing_notifications"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416311
    const/16 v0, 0x3a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "typ"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416312
    const/16 v0, 0x3b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_ms"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416313
    const/16 v0, 0x3c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "orca_video_notifications"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416314
    const/16 v0, 0x3d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "orca_visibility_updates"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416315
    const/16 v0, 0x3e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "webrtc"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416316
    const/16 v0, 0x3f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "webrtc_response"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416317
    const/16 v0, 0x40

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "subscribe"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416318
    const/16 v0, 0x41

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_p"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416319
    const/16 v0, 0x42

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "push_ack"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416320
    const/16 v0, 0x44

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "webrtc_binary"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416321
    const/16 v0, 0x45

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_sm"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416322
    const/16 v0, 0x46

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_sm_rp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416323
    const/16 v0, 0x47

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_vs"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416324
    const/16 v0, 0x48

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_rtc"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416325
    const/16 v0, 0x49

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "echo"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416326
    const/16 v0, 0x4a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "pages_messaging"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416327
    const/16 v0, 0x4b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_omnistore_sync"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416328
    const/16 v0, 0x4c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "fbns_msg"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416329
    const/16 v0, 0x4d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_ps"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416330
    const/16 v0, 0x4e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_dr_batch"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416331
    const/16 v0, 0x4f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "fbns_reg_req"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416332
    const/16 v0, 0x50

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "fbns_reg_resp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416333
    const/16 v0, 0x51

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "omnistore_subscribe_collection"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416334
    const/16 v0, 0x52

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "fbns_unreg_req"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416335
    const/16 v0, 0x53

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "fbns_unreg_resp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416336
    const/16 v0, 0x54

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "omnistore_change_record"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416337
    const/16 v0, 0x55

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_dr_response"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416338
    const/16 v0, 0x56

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "quick_promotion_refresh"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416339
    const/16 v0, 0x57

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "v_ios"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416340
    const/16 v0, 0x58

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "pubsub"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416341
    const/16 v0, 0x59

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "get_media"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416342
    const/16 v0, 0x5a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "get_media_resp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416343
    const/16 v0, 0x5b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "mqtt_health_stats"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416344
    const/16 v0, 0x5c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_sp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416345
    const/16 v0, 0x5d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "groups_landing_updates"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416346
    const/16 v0, 0x5e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "rs"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416347
    const/16 v0, 0x5f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_sm_b"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416348
    const/16 v0, 0x60

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_sm_b_rsp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416349
    const/16 v0, 0x61

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_ms_gd"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416350
    const/16 v0, 0x62

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_rtc_multi"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416351
    const/16 v0, 0x63

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "friend_accepted"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416352
    const/16 v0, 0x64

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_tn"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416353
    const/16 v0, 0x65

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_mf_as"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416354
    const/16 v0, 0x66

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_fs"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416355
    const/16 v0, 0x67

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_tp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416356
    const/16 v0, 0x68

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_stp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416357
    const/16 v0, 0x69

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_st"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416358
    const/16 v0, 0x6a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "omni"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416359
    const/16 v0, 0x6b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_push"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416360
    const/16 v0, 0x6c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "omni_c"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416361
    const/16 v0, 0x6d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_sac"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416362
    const/16 v0, 0x6e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "omnistore_resnapshot"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416363
    const/16 v0, 0x6f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_spc"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416364
    const/16 v0, 0x70

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_callability_req"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416365
    const/16 v0, 0x71

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_callability_resp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416366
    const/16 v0, 0x74

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_ec"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416367
    const/16 v0, 0x75

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_tcp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416368
    const/16 v0, 0x76

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_tcpr"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416369
    const/16 v0, 0x77

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_ts"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416370
    const/16 v0, 0x78

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_ts_rp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416371
    const/16 v0, 0x79

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_mt_req"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416372
    const/16 v0, 0x7a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_mt_resp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416373
    const/16 v0, 0x7b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_inbox"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416374
    const/16 v0, 0x7c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "p_a_req"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416375
    const/16 v0, 0x7d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "p_a_resp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416376
    const/16 v0, 0x7e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "unsubscribe"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416377
    const/16 v0, 0x7f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_graphql_req"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416378
    const/16 v0, 0x80

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_graphql_resp"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416379
    const/16 v0, 0x81

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_app_update"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416380
    const/16 v0, 0x82

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "p_updated"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416381
    const/16 v0, 0x83

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "t_omnistore_sync_low_pri"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416382
    const/16 v0, 0x84

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "ig_send_message"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416383
    const/16 v0, 0x85

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "ig_send_message_response"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416384
    const/16 v0, 0x86

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "ig_sub_iris"

    invoke-virtual {p0, v0, v1}, LX/2Ub;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416385
    return-void
.end method
