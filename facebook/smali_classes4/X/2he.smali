.class public LX/2he;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/2h7;",
            "LX/0Yj;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2he;


# instance fields
.field public final c:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final d:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 450254
    sget-object v0, LX/2h7;->JEWEL:LX/2h7;

    new-instance v1, LX/0Yj;

    const v2, 0x2f0006

    const-string v3, "FriendRequestsHarrisonTabSwitchTTI"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    sget-object v2, LX/2h7;->NUX:LX/2h7;

    new-instance v3, LX/0Yj;

    const v4, 0x2f0007

    const-string v5, "NuxAddFriendsStepTTI"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-static {v0, v1, v2, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/2he;->a:LX/0P1;

    .line 450255
    const-class v0, LX/2he;

    sput-object v0, LX/2he;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0YP;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450257
    iput-object p1, p0, LX/2he;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 450258
    iput-object p2, p0, LX/2he;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 450259
    const v0, 0x2f0006

    invoke-virtual {p3, v0}, LX/0YP;->a(I)V

    .line 450260
    return-void
.end method

.method public static a(LX/0QB;)LX/2he;
    .locals 6

    .prologue
    .line 450261
    sget-object v0, LX/2he;->e:LX/2he;

    if-nez v0, :cond_1

    .line 450262
    const-class v1, LX/2he;

    monitor-enter v1

    .line 450263
    :try_start_0
    sget-object v0, LX/2he;->e:LX/2he;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 450264
    if-eqz v2, :cond_0

    .line 450265
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 450266
    new-instance p0, LX/2he;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0YP;->a(LX/0QB;)LX/0YP;

    move-result-object v5

    check-cast v5, LX/0YP;

    invoke-direct {p0, v3, v4, v5}, LX/2he;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0YP;)V

    .line 450267
    move-object v0, p0

    .line 450268
    sput-object v0, LX/2he;->e:LX/2he;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450269
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 450270
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 450271
    :cond_1
    sget-object v0, LX/2he;->e:LX/2he;

    return-object v0

    .line 450272
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 450273
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/2h7;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 450274
    sget-object v0, LX/2he;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yj;

    .line 450275
    if-eqz v0, :cond_0

    .line 450276
    iget-object v1, p0, LX/2he;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v2, LX/0Yj;

    invoke-direct {v2, v0}, LX/0Yj;-><init>(LX/0Yj;)V

    new-array v0, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/friending/tab/FriendRequestsTab;->l:Lcom/facebook/friending/tab/FriendRequestsTab;

    iget-object v4, v4, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v4, v0, v3

    invoke-virtual {v2, v0}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    .line 450277
    iput-boolean v5, v0, LX/0Yj;->n:Z

    .line 450278
    move-object v0, v0

    .line 450279
    invoke-interface {v1, v0, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    .line 450280
    :cond_0
    return-void
.end method

.method public final a(LX/2h7;J)V
    .locals 8

    .prologue
    .line 450281
    sget-object v0, LX/2he;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/0Yj;

    .line 450282
    if-eqz v6, :cond_0

    .line 450283
    iget-object v0, p0, LX/2he;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 450284
    iget v1, v6, LX/0Yj;->a:I

    move v1, v1

    .line 450285
    iget-object v2, v6, LX/0Yj;->d:Ljava/lang/String;

    move-object v2, v2

    .line 450286
    const-string v3, "query_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 450287
    iget-object v0, p0, LX/2he;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    .line 450288
    :cond_0
    return-void
.end method

.method public final b(LX/2h7;)V
    .locals 2

    .prologue
    .line 450289
    sget-object v0, LX/2he;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yj;

    .line 450290
    if-eqz v0, :cond_0

    .line 450291
    iget-object v1, p0, LX/2he;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;)V

    .line 450292
    :cond_0
    return-void
.end method
