.class public LX/30H;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/2Ia;

.field private final c:LX/03V;

.field private final d:LX/0SG;

.field private final e:LX/0en;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2IT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 484164
    const-class v0, LX/30H;

    sput-object v0, LX/30H;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2Ia;LX/03V;LX/0SG;LX/0en;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ia;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            "LX/0en;",
            "LX/0Ot",
            "<",
            "LX/2IT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 484165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484166
    iput-object p1, p0, LX/30H;->b:LX/2Ia;

    .line 484167
    iput-object p2, p0, LX/30H;->c:LX/03V;

    .line 484168
    iput-object p3, p0, LX/30H;->d:LX/0SG;

    .line 484169
    iput-object p4, p0, LX/30H;->e:LX/0en;

    .line 484170
    iput-object p5, p0, LX/30H;->f:LX/0Ot;

    .line 484171
    return-void
.end method

.method public static b(LX/0QB;)LX/30H;
    .locals 6

    .prologue
    .line 484172
    new-instance v0, LX/30H;

    invoke-static {p0}, LX/2Ia;->b(LX/0QB;)LX/2Ia;

    move-result-object v1

    check-cast v1, LX/2Ia;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {p0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v4

    check-cast v4, LX/0en;

    const/16 v5, 0x14a

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/30H;-><init>(LX/2Ia;LX/03V;LX/0SG;LX/0en;LX/0Ot;)V

    .line 484173
    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 484174
    iget-object v0, p0, LX/30H;->b:LX/2Ia;

    invoke-virtual {v0}, LX/2Ia;->a()LX/0Px;

    move-result-object v5

    .line 484175
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 484176
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    move v4, v2

    :goto_0
    if-ge v4, v7, :cond_5

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 484177
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 484178
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 484179
    iget-object v1, p0, LX/30H;->c:LX/03V;

    const-string v8, "assetdownload:setupLocalDirectories path_exists_and_is_a_file"

    const-string v9, "Directory path is a file: %s"

    new-array v10, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v2

    invoke-static {v9, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v8, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 484180
    :goto_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 484181
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 484182
    if-nez v1, :cond_1

    .line 484183
    iget-object v1, p0, LX/30H;->c:LX/03V;

    const-string v8, "assetdownload:setupLocalDirectories mkdirs_failed"

    const-string v9, "Failed to create directory: %s"

    new-array v10, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v2

    invoke-static {v9, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 484184
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, v3}, Ljava/io/File;->setWritable(Z)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    move v1, v3

    .line 484185
    :goto_2
    if-eqz v1, :cond_4

    .line 484186
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    :cond_3
    move v1, v2

    .line 484187
    goto :goto_2

    .line 484188
    :cond_4
    sget-object v1, LX/30H;->a:Ljava/lang/Class;

    const-string v8, "Failed to set directory writable: %s"

    new-array v9, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v2

    invoke-static {v1, v8, v9}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 484189
    :cond_5
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Ljava/io/File;Z)Ljava/io/File;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 484190
    iget-object v1, p0, LX/30H;->b:LX/2Ia;

    invoke-virtual {v1, p1}, LX/2Ia;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Z

    move-result v1

    .line 484191
    if-eqz v1, :cond_0

    if-nez p3, :cond_0

    .line 484192
    :goto_0
    return-object v0

    .line 484193
    :cond_0
    iget-object v1, p0, LX/30H;->b:LX/2Ia;

    invoke-virtual {v1, p1, v5}, LX/2Ia;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Z)Ljava/io/File;

    move-result-object v1

    .line 484194
    if-nez v1, :cond_1

    .line 484195
    sget-object v1, LX/30H;->a:Ljava/lang/Class;

    const-string v2, "destination is null; perhaps no accessible storage is available."

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 484196
    :cond_1
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 484197
    iget-object v1, p0, LX/30H;->c:LX/03V;

    const-string v2, "assetdownload:copyToLocalFile source_file_does_not_exist"

    const-string v3, "Source file: %s"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 484198
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-nez v2, :cond_3

    .line 484199
    iget-object v2, p0, LX/30H;->c:LX/03V;

    const-string v3, "assetdownload:copyToLocalFile destination_file_is_not_writable"

    const-string v4, "Destination file: %s"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 484200
    :cond_3
    :try_start_0
    iget-object v2, p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStorageConstraint:LX/6BV;

    move-object v2, v2

    .line 484201
    sget-object v3, LX/6BV;->MUST_BE_CUSTOM_LOCATION:LX/6BV;

    if-ne v2, v3, :cond_4

    .line 484202
    invoke-static {v1}, LX/1t3;->c(Ljava/io/File;)V

    .line 484203
    :goto_1
    invoke-static {p2, v1}, LX/1t3;->a(Ljava/io/File;Ljava/io/File;)V

    move-object v0, v1

    .line 484204
    goto :goto_0

    .line 484205
    :cond_4
    invoke-virtual {p0}, LX/30H;->a()LX/0Px;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 484206
    :catch_0
    move-exception v1

    .line 484207
    iget-object v2, p0, LX/30H;->c:LX/03V;

    const-string v3, "assetdownload:copyToLocalFile io_exception_during_copy"

    invoke-virtual {v1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Z
    .locals 1

    .prologue
    .line 484208
    iget-object v0, p0, LX/30H;->b:LX/2Ia;

    invoke-virtual {v0, p1}, LX/2Ia;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Z

    move-result v0

    return v0
.end method

.method public final b()J
    .locals 8

    .prologue
    .line 484209
    iget-object v0, p0, LX/30H;->b:LX/2Ia;

    invoke-virtual {v0}, LX/2Ia;->a()LX/0Px;

    move-result-object v4

    .line 484210
    const-wide/16 v2, 0x0

    .line 484211
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 484212
    iget-object v6, p0, LX/30H;->e:LX/0en;

    invoke-virtual {v6, v0}, LX/0en;->d(Ljava/io/File;)J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 484213
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 484214
    :cond_0
    return-wide v2
.end method

.method public final c()J
    .locals 8

    .prologue
    .line 484215
    iget-object v0, p0, LX/30H;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2IT;

    .line 484216
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2IT;->a(LX/2IT;LX/0ux;)LX/0Px;

    move-result-object v1

    move-object v5, v1

    .line 484217
    const-wide/16 v2, 0x0

    .line 484218
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    .line 484219
    iget-object v1, v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStorageConstraint:LX/6BV;

    move-object v1, v1

    .line 484220
    sget-object v7, LX/6BV;->MUST_BE_CUSTOM_LOCATION:LX/6BV;

    if-ne v1, v7, :cond_1

    invoke-virtual {v0}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->g()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 484221
    invoke-virtual {v0}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->g()Ljava/io/File;

    move-result-object v0

    .line 484222
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    add-long/2addr v0, v2

    .line 484223
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v0

    goto :goto_0

    .line 484224
    :cond_0
    return-wide v2

    :cond_1
    move-wide v0, v2

    goto :goto_1
.end method
