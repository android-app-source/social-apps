.class public LX/2nd;
.super LX/2ne;
.source ""

# interfaces
.implements LX/2nf;


# instance fields
.field public final a:Ljava/io/File;

.field public final b:Landroid/database/Cursor;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field public final i:I

.field private final j:I

.field public final k:I

.field private final l:I

.field private final m:LX/2kE;

.field private final n:Landroid/os/Bundle;

.field private o:LX/2kb;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Ljava/io/File;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 464927
    invoke-direct {p0, p1}, LX/2ne;-><init>(Landroid/database/Cursor;)V

    .line 464928
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iput-object v0, p0, LX/2nd;->b:Landroid/database/Cursor;

    .line 464929
    iput-object p2, p0, LX/2nd;->a:Ljava/io/File;

    .line 464930
    new-instance v0, LX/2kE;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, LX/2kE;-><init>(Ljava/io/File;LX/15j;)V

    iput-object v0, p0, LX/2nd;->m:LX/2kE;

    .line 464931
    iget-object v0, p0, LX/2nd;->b:Landroid/database/Cursor;

    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2nd;->c:I

    .line 464932
    const-string v0, "flags"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2nd;->d:I

    .line 464933
    const-string v0, "version"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2nd;->e:I

    .line 464934
    const-string v0, "sort_key"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2nd;->f:I

    .line 464935
    const-string v0, "is_optimistic"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2nd;->g:I

    .line 464936
    const-string v0, "class"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2nd;->h:I

    .line 464937
    const-string v0, "model_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2nd;->l:I

    .line 464938
    const-string v0, "offset"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2nd;->i:I

    .line 464939
    const-string v0, "file"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2nd;->j:I

    .line 464940
    const-string v0, "mutation_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2nd;->k:I

    .line 464941
    iput-object p3, p0, LX/2nd;->n:Landroid/os/Bundle;

    .line 464942
    return-void
.end method

.method private g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 464926
    iget-object v0, p0, LX/2nd;->b:Landroid/database/Cursor;

    iget v1, p0, LX/2nd;->j:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private h()[B
    .locals 2

    .prologue
    .line 464925
    iget-object v0, p0, LX/2nd;->b:Landroid/database/Cursor;

    iget v1, p0, LX/2nd;->l:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method private i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 464924
    iget-object v0, p0, LX/2nd;->b:Landroid/database/Cursor;

    iget v1, p0, LX/2nd;->h:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 464923
    iget-object v0, p0, LX/2nd;->b:Landroid/database/Cursor;

    iget v1, p0, LX/2nd;->c:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(LX/2kb;)V
    .locals 1

    .prologue
    .line 464897
    iget-object v0, p0, LX/2nd;->o:LX/2kb;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 464898
    iput-object p1, p0, LX/2nd;->o:LX/2kb;

    .line 464899
    return-void

    .line 464900
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 464922
    iget-object v0, p0, LX/2nd;->b:Landroid/database/Cursor;

    iget v1, p0, LX/2nd;->e:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()Lcom/facebook/flatbuffers/Flattenable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 464913
    :try_start_0
    iget-object v0, p0, LX/2nd;->m:LX/2kE;

    .line 464914
    iget-object v1, p0, LX/2nd;->b:Landroid/database/Cursor;

    iget v2, p0, LX/2nd;->i:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    move v1, v1

    .line 464915
    invoke-direct {p0}, LX/2nd;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, LX/2nd;->h()[B

    move-result-object v3

    invoke-direct {p0}, LX/2nd;->i()Ljava/lang/String;

    move-result-object v4

    .line 464916
    iget-object v5, p0, LX/2nd;->b:Landroid/database/Cursor;

    iget v6, p0, LX/2nd;->k:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    move-object v5, v5

    .line 464917
    invoke-virtual/range {v0 .. v5}, LX/2kE;->a(ILjava/lang/String;[BLjava/lang/String;[B)Lcom/facebook/flatbuffers/Flattenable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 464918
    :goto_0
    return-object v0

    .line 464919
    :catch_0
    move-exception v0

    .line 464920
    const-string v1, "SQLiteModelCursor"

    const-string v2, "Unable to load model"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 464921
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 464909
    invoke-super {p0}, LX/2ne;->close()V

    .line 464910
    iget-object v0, p0, LX/2nd;->o:LX/2kb;

    if-eqz v0, :cond_0

    .line 464911
    iget-object v0, p0, LX/2nd;->o:LX/2kb;

    invoke-interface {v0}, LX/2kb;->close()V

    .line 464912
    :cond_0
    return-void
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 464908
    iget-object v0, p0, LX/2nd;->b:Landroid/database/Cursor;

    iget v1, p0, LX/2nd;->d:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 464902
    iget-object v0, p0, LX/2nd;->m:LX/2kE;

    invoke-direct {p0}, LX/2nd;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/2nd;->h()[B

    move-result-object v2

    invoke-direct {p0}, LX/2nd;->i()Ljava/lang/String;

    move-result-object v3

    .line 464903
    invoke-static {v0, v1}, LX/2kE;->b(LX/2kE;Ljava/lang/String;)Ljava/nio/MappedByteBuffer;

    move-result-object v4

    .line 464904
    if-nez v4, :cond_0

    .line 464905
    :goto_0
    return-void

    .line 464906
    :cond_0
    invoke-static {v0, v2, v3}, LX/2kE;->a(LX/2kE;[BLjava/lang/String;)LX/0w5;

    move-result-object p0

    .line 464907
    invoke-static {v4, p0}, LX/1lJ;->a(Ljava/nio/ByteBuffer;LX/0w5;)V

    goto :goto_0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 464901
    iget-object v0, p0, LX/2nd;->n:Landroid/os/Bundle;

    return-object v0
.end method
