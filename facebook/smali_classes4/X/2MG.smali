.class public LX/2MG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:LX/00H;

.field private final b:LX/0Uh;

.field private final c:LX/2MH;


# direct methods
.method public constructor <init>(LX/00H;LX/0Uh;LX/2MH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396363
    iput-object p1, p0, LX/2MG;->a:LX/00H;

    .line 396364
    iput-object p2, p0, LX/2MG;->b:LX/0Uh;

    .line 396365
    iput-object p3, p0, LX/2MG;->c:LX/2MH;

    .line 396366
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 396367
    iget-object v0, p0, LX/2MG;->a:LX/00H;

    .line 396368
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 396369
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-eq v0, v1, :cond_0

    .line 396370
    :goto_0
    return-void

    .line 396371
    :cond_0
    iget-object v0, p0, LX/2MG;->b:LX/0Uh;

    const/16 v1, 0x1f8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396372
    iget-object v0, p0, LX/2MG;->c:LX/2MH;

    const-class v1, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;

    invoke-virtual {v0, v1}, LX/2MH;->a(Ljava/lang/Class;)V

    goto :goto_0

    .line 396373
    :cond_1
    iget-object v0, p0, LX/2MG;->c:LX/2MH;

    const-class v1, Lcom/facebook/messaging/connectivity/ConnectivityBroadcastReceiver;

    invoke-virtual {v0, v1}, LX/2MH;->b(Ljava/lang/Class;)V

    goto :goto_0
.end method
