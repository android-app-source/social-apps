.class public final LX/2oc;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2op;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/RichVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 0

    .prologue
    .line 467188
    iput-object p1, p0, LX/2oc;->a:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/RichVideoPlayer;B)V
    .locals 0

    .prologue
    .line 467189
    invoke-direct {p0, p1}, LX/2oc;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    return-void
.end method

.method private a(LX/2op;)V
    .locals 3

    .prologue
    .line 467190
    iget-object v0, p0, LX/2oc;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    if-nez v0, :cond_0

    .line 467191
    :goto_0
    return-void

    .line 467192
    :cond_0
    iget-object v0, p0, LX/2oc;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/RichVideoPlayer;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 467193
    iget-boolean v2, v0, LX/2oy;->l:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    :goto_1
    move v0, v2

    .line 467194
    if-eqz v0, :cond_1

    goto :goto_0

    .line 467195
    :cond_2
    iget-object v0, p0, LX/2oc;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    invoke-interface {v0, p1}, LX/3It;->a(LX/2op;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, LX/2oy;->r()Z

    move-result v2

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2op;",
            ">;"
        }
    .end annotation

    .prologue
    .line 467196
    const-class v0, LX/2op;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 467197
    check-cast p1, LX/2op;

    invoke-direct {p0, p1}, LX/2oc;->a(LX/2op;)V

    return-void
.end method
