.class public LX/34N;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 495014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495015
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/34O;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/34O;"
        }
    .end annotation

    .prologue
    .line 495016
    const v3, -0x22a42d2a    # -9.8999738E17f

    .line 495017
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 495018
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 495019
    invoke-static {p0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 495020
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 495021
    :cond_0
    sget-object v0, LX/34O;->UNSET:LX/34O;

    .line 495022
    :goto_0
    move-object v0, v0

    .line 495023
    sget-object v1, LX/34O;->UNSET:LX/34O;

    if-eq v0, v1, :cond_2

    .line 495024
    :cond_1
    :goto_1
    return-object v0

    .line 495025
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 495026
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 495027
    invoke-static {v0}, LX/1VS;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 495028
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    .line 495029
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_a

    const v3, -0x22a42d2a    # -9.8999738E17f

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    if-ne v3, v1, :cond_a

    .line 495030
    sget-object v1, LX/34O;->NCPP:LX/34O;

    .line 495031
    :goto_2
    move-object v1, v1

    .line 495032
    sget-object v2, LX/34O;->UNSET:LX/34O;

    if-eq v1, v2, :cond_3

    move-object v0, v1

    .line 495033
    goto :goto_1

    .line 495034
    :cond_3
    invoke-static {v0}, LX/34N;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/34O;

    move-result-object v1

    .line 495035
    sget-object v2, LX/34O;->UNSET:LX/34O;

    if-eq v1, v2, :cond_4

    move-object v0, v1

    .line 495036
    goto :goto_1

    .line 495037
    :cond_4
    invoke-static {v0}, LX/1VS;->d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 495038
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_5

    const v2, -0x22a42d2a    # -9.8999738E17f

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    if-eq v2, v1, :cond_c

    .line 495039
    :cond_5
    sget-object v1, LX/34O;->UNSET:LX/34O;

    .line 495040
    :goto_3
    move-object v0, v1

    .line 495041
    sget-object v1, LX/34O;->UNSET:LX/34O;

    if-ne v0, v1, :cond_1

    .line 495042
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 495043
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v1, :cond_e

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->y()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->SINGLE_CREATOR_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 495044
    sget-object v0, LX/34O;->SINGLE_CREATOR_COLLECTION_ITEM:LX/34O;

    .line 495045
    :goto_4
    move-object v0, v0

    .line 495046
    sget-object v1, LX/34O;->UNSET:LX/34O;

    if-ne v0, v1, :cond_1

    .line 495047
    sget-object v0, LX/34O;->OTHER:LX/34O;

    goto :goto_1

    .line 495048
    :cond_6
    invoke-static {v1}, LX/1VS;->d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 495049
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    if-ne v3, v1, :cond_9

    .line 495050
    invoke-static {v0}, LX/36U;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 495051
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    if-eq v3, v2, :cond_7

    const v2, -0x1e53800c

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    if-ne v2, v1, :cond_8

    .line 495052
    :cond_7
    sget-object v0, LX/34O;->GROUPER_ATTACHED_STORY:LX/34O;

    goto/16 :goto_0

    .line 495053
    :cond_8
    sget-object v1, LX/34O;->OFFER:LX/34O;

    invoke-static {v0}, LX/34N;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/34O;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/34O;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 495054
    sget-object v0, LX/34O;->GROUPER_ATTACHED_STORY:LX/34O;

    goto/16 :goto_0

    .line 495055
    :cond_9
    sget-object v0, LX/34O;->UNSET:LX/34O;

    goto/16 :goto_0

    .line 495056
    :cond_a
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->o()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 495057
    sget-object v1, LX/34O;->NCPP:LX/34O;

    goto/16 :goto_2

    .line 495058
    :cond_b
    sget-object v1, LX/34O;->UNSET:LX/34O;

    goto/16 :goto_2

    .line 495059
    :cond_c
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 495060
    if-eqz v1, :cond_d

    sget-object v2, LX/34O;->OFFER:LX/34O;

    invoke-static {v1}, LX/34N;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/34O;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/34O;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 495061
    sget-object v1, LX/34O;->GROUPER_WITH_OFFER:LX/34O;

    goto/16 :goto_3

    .line 495062
    :cond_d
    sget-object v1, LX/34O;->GROUPER:LX/34O;

    goto/16 :goto_3

    :cond_e
    sget-object v0, LX/34O;->UNSET:LX/34O;

    goto/16 :goto_4
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)LX/34O;
    .locals 3

    .prologue
    .line 495063
    invoke-static {p0}, LX/1VS;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 495064
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v1

    .line 495065
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-nez v2, :cond_1

    .line 495066
    :cond_0
    sget-object v0, LX/34O;->UNSET:LX/34O;

    .line 495067
    :goto_0
    return-object v0

    .line 495068
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 495069
    const v2, -0x22a42d2a    # -9.8999738E17f

    if-ne v2, v0, :cond_3

    .line 495070
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/34O;->NCPP:LX/34O;

    goto :goto_0

    :cond_2
    sget-object v0, LX/34O;->PAGE_LIKE:LX/34O;

    goto :goto_0

    .line 495071
    :cond_3
    const v2, -0x12bcfc94

    if-ne v2, v0, :cond_4

    .line 495072
    sget-object v0, LX/34O;->OFFER:LX/34O;

    goto :goto_0

    .line 495073
    :cond_4
    const v2, -0x1e53800c

    if-ne v2, v0, :cond_6

    .line 495074
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->o()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, LX/34O;->NCPP:LX/34O;

    goto :goto_0

    :cond_5
    sget-object v0, LX/34O;->OTHER:LX/34O;

    goto :goto_0

    .line 495075
    :cond_6
    sget-object v0, LX/34O;->UNSET:LX/34O;

    goto :goto_0
.end method
