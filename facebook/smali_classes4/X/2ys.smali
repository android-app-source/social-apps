.class public LX/2ys;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1zx;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 482803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482804
    iput-object p1, p0, LX/2ys;->a:Landroid/content/Context;

    .line 482805
    iput p2, p0, LX/2ys;->b:I

    .line 482806
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 482800
    iget-object v0, p0, LX/2ys;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, LX/2ys;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final b()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 482802
    invoke-virtual {p0}, LX/2ys;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 482801
    iget v0, p0, LX/2ys;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
