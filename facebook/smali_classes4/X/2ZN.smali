.class public LX/2ZN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2XA;
.implements LX/2ZK;


# instance fields
.field private final a:LX/0Uh;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2ae;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/117;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11A;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Z

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2cQ;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final j:LX/0WP;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/119;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/01T;LX/0Or;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0WP;LX/0Ot;)V
    .locals 1
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/2ae;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/117;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/11A;",
            ">;",
            "LX/01T;",
            "LX/0Or",
            "<",
            "LX/2cQ;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0WP;",
            "LX/0Ot",
            "<",
            "LX/119;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 422443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422444
    iput-object p1, p0, LX/2ZN;->a:LX/0Uh;

    .line 422445
    iput-object p2, p0, LX/2ZN;->b:LX/0Ot;

    .line 422446
    iput-object p3, p0, LX/2ZN;->c:LX/0Ot;

    .line 422447
    iput-object p4, p0, LX/2ZN;->d:LX/0Ot;

    .line 422448
    iput-object p5, p0, LX/2ZN;->e:LX/0Ot;

    .line 422449
    sget-object v0, LX/01T;->FB4A:LX/01T;

    if-ne p6, v0, :cond_0

    .line 422450
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 422451
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 422452
    iput-boolean v0, p0, LX/2ZN;->f:Z

    .line 422453
    iput-object p7, p0, LX/2ZN;->g:LX/0Or;

    .line 422454
    iput-object p8, p0, LX/2ZN;->h:LX/0Or;

    .line 422455
    iput-object p9, p0, LX/2ZN;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 422456
    iput-object p10, p0, LX/2ZN;->j:LX/0WP;

    .line 422457
    iput-object p11, p0, LX/2ZN;->k:LX/0Ot;

    .line 422458
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/0WS;LX/0hN;)Z
    .locals 13

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 422416
    iget-object v0, p0, LX/2ZN;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/117;

    invoke-virtual {v0}, LX/117;->b()Ljava/util/Collection;

    move-result-object v0

    .line 422417
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v7

    .line 422418
    :goto_0
    return v0

    .line 422419
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v9, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 422420
    new-instance v10, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v10, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 422421
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    .line 422422
    invoke-virtual {p1, v6, v8}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 422423
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 422424
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 422425
    const-string v0, "rank"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 422426
    const-string v0, "nux_id"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 422427
    const-string v0, "fetchTimeMs"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 422428
    const-string v0, "nux_data"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 422429
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 422430
    iget-object v0, p0, LX/2ZN;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/119;

    const-string v12, "nux_data"

    invoke-virtual {v3, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v6}, LX/119;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    .line 422431
    :goto_2
    new-instance v0, Lcom/facebook/interstitial/api/FetchInterstitialResult;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/interstitial/api/FetchInterstitialResult;-><init>(ILjava/lang/String;Landroid/os/Parcelable;J)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 422432
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "last_impression/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 422433
    invoke-virtual {p1, v0}, LX/0WS;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 422434
    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, LX/0WS;->a(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v10, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 422435
    :cond_2
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 422436
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 422437
    :cond_3
    iget-object v0, p0, LX/2ZN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    invoke-virtual {v0, v9}, LX/0iA;->a(Ljava/util/List;)V

    .line 422438
    invoke-interface {v10}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 422439
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 422440
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, LX/11b;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {p2, v1, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    goto :goto_3

    :cond_4
    move v0, v7

    .line 422441
    goto/16 :goto_0

    .line 422442
    :catch_0
    goto/16 :goto_1

    :cond_5
    move-object v3, v8

    goto :goto_2
.end method

.method public static b(LX/0QB;)LX/2ZN;
    .locals 12

    .prologue
    .line 422414
    new-instance v0, LX/2ZN;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v2, 0xbcc

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xbd2

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xbd0

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xbcb

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v6

    check-cast v6, LX/01T;

    const/16 v7, 0xbd4

    invoke-static {p0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x15e7

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0WL;->a(LX/0QB;)LX/0WP;

    move-result-object v10

    check-cast v10, LX/0WP;

    const/16 v11, 0xbd3

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v0 .. v11}, LX/2ZN;-><init>(LX/0Uh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/01T;LX/0Or;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0WP;LX/0Ot;)V

    .line 422415
    return-object v0
.end method

.method private f()LX/0WS;
    .locals 3

    .prologue
    .line 422413
    iget-object v1, p0, LX/2ZN;->j:LX/0WP;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "interstitial_"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/2ZN;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 422406
    iget-boolean v1, p0, LX/2ZN;->f:Z

    if-nez v1, :cond_0

    .line 422407
    :goto_0
    return-void

    .line 422408
    :cond_0
    invoke-direct {p0}, LX/2ZN;->f()LX/0WS;

    move-result-object v1

    .line 422409
    iget-object v2, p0, LX/2ZN;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    .line 422410
    invoke-direct {p0, v1, v2}, LX/2ZN;->a(LX/0WS;LX/0hN;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "data_stored"

    invoke-virtual {v1, v3, v0}, LX/0WS;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 422411
    :cond_2
    sget-object v1, LX/11b;->f:LX/0Tn;

    invoke-interface {v2, v1, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 422412
    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public final b()LX/2ZE;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 422459
    iget-object v0, p0, LX/2ZN;->a:LX/0Uh;

    const/16 v1, 0x193

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 422460
    const/4 v0, 0x0

    .line 422461
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2ZO;

    invoke-direct {v0, p0, v2}, LX/2ZO;-><init>(LX/2ZN;Z)V

    goto :goto_0
.end method

.method public final c()LX/2ZE;
    .locals 3

    .prologue
    .line 422405
    new-instance v0, LX/2ZO;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, LX/2ZO;-><init>(LX/2ZN;Z)V

    return-object v0
.end method

.method public final d()V
    .locals 9

    .prologue
    .line 422389
    iget-boolean v0, p0, LX/2ZN;->f:Z

    if-nez v0, :cond_0

    .line 422390
    :goto_0
    return-void

    .line 422391
    :cond_0
    invoke-direct {p0}, LX/2ZN;->f()LX/0WS;

    move-result-object v0

    invoke-virtual {v0}, LX/0WS;->b()LX/1gW;

    move-result-object v0

    .line 422392
    invoke-interface {v0}, LX/1gW;->a()LX/1gW;

    .line 422393
    iget-object v3, p0, LX/2ZN;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/117;

    invoke-virtual {v3}, LX/117;->b()Ljava/util/Collection;

    move-result-object v3

    .line 422394
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 422395
    :cond_1
    const-string v1, "data_stored"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/1gW;->a(Ljava/lang/String;Z)LX/1gW;

    .line 422396
    invoke-interface {v0}, LX/1gW;->b()Z

    goto :goto_0

    .line 422397
    :cond_2
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 422398
    invoke-static {v3}, LX/11b;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v5

    .line 422399
    iget-object v6, p0, LX/2ZN;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v7, 0x0

    invoke-interface {v6, v5, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 422400
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 422401
    invoke-interface {v0, v3, v5}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    .line 422402
    invoke-static {v3}, LX/11b;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v5

    .line 422403
    iget-object v6, p0, LX/2ZN;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 422404
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "last_impression/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, LX/2ZN;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v7, 0x0

    invoke-interface {v6, v5, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v5

    invoke-interface {v0, v3, v5, v6}, LX/1gW;->a(Ljava/lang/String;J)LX/1gW;

    goto :goto_1
.end method

.method public final dq_()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 422387
    iget-boolean v1, p0, LX/2ZN;->f:Z

    if-nez v1, :cond_0

    .line 422388
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/2ZN;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/11b;->f:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public final dr_()J
    .locals 2

    .prologue
    .line 422386
    const-wide/32 v0, 0x6ddd00

    return-wide v0
.end method

.method public final e()LX/2ZF;
    .locals 1

    .prologue
    .line 422385
    sget-object v0, LX/2ZF;->OTHER:LX/2ZF;

    return-object v0
.end method
