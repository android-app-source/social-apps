.class public final LX/2GL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/2YS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2GJ;


# direct methods
.method public constructor <init>(LX/2GJ;)V
    .locals 0

    .prologue
    .line 388353
    iput-object p1, p0, LX/2GL;->a:LX/2GJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    .line 388354
    iget-object v0, p0, LX/2GL;->a:LX/2GJ;

    const/4 v1, 0x1

    .line 388355
    const v7, 0x270001

    .line 388356
    invoke-static {v0}, LX/2GJ;->g(LX/2GJ;)V

    .line 388357
    invoke-static {v0}, LX/2GJ;->j(LX/2GJ;)LX/2VA;

    move-result-object v2

    .line 388358
    iget-object v3, v0, LX/2GJ;->j:LX/0Zm;

    invoke-virtual {v3}, LX/0Zm;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 388359
    invoke-static {v2}, LX/2GJ;->g(LX/2VA;)I

    move-result v4

    .line 388360
    iget-object v5, v0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v6, "exception"

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-interface {v5, v7, v4, v6, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 388361
    iget-object v3, v0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v5, 0x3

    invoke-interface {v3, v7, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 388362
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/2VA;->b(Z)V

    .line 388363
    const-string v3, "start"

    invoke-static {v0, v3, v2, p1}, LX/2GJ;->a(LX/2GJ;Ljava/lang/String;LX/2VA;Ljava/lang/Throwable;)V

    .line 388364
    iget-object v3, v0, LX/2GJ;->c:LX/0SG;

    iget-wide v4, v0, LX/2GJ;->p:J

    iget-wide v6, v0, LX/2GJ;->q:J

    invoke-virtual/range {v2 .. v7}, LX/2VA;->a(LX/0SG;JJ)V

    .line 388365
    if-eqz v1, :cond_1

    .line 388366
    invoke-static {v0}, LX/2GJ;->h(LX/2GJ;)V

    .line 388367
    :cond_1
    return-void

    .line 388368
    :cond_2
    const-string v3, "null"

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 388369
    check-cast p1, LX/2YS;

    .line 388370
    iget-object v0, p0, LX/2GL;->a:LX/2GJ;

    const v5, 0x270001

    .line 388371
    invoke-static {v0}, LX/2GJ;->g(LX/2GJ;)V

    .line 388372
    invoke-static {v0}, LX/2GJ;->j(LX/2GJ;)LX/2VA;

    move-result-object v1

    .line 388373
    iget-object v2, v1, LX/2VA;->a:LX/1Eh;

    move-object v2, v2

    .line 388374
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 388375
    iget-object v3, v0, LX/2GJ;->j:LX/0Zm;

    invoke-virtual {v3}, LX/0Zm;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 388376
    invoke-static {v1}, LX/2GJ;->g(LX/2VA;)I

    move-result v3

    .line 388377
    iget-object v4, v0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v4, v5, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 388378
    iget-object v2, v0, LX/2GJ;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v4, 0x2

    invoke-interface {v2, v5, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 388379
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/2VA;->b(Z)V

    .line 388380
    iget-boolean v2, p1, LX/2YS;->a:Z

    if-eqz v2, :cond_1

    .line 388381
    const/4 v2, 0x0

    iput v2, v1, LX/2VA;->b:I

    .line 388382
    :goto_0
    invoke-static {v0}, LX/2GJ;->h(LX/2GJ;)V

    .line 388383
    return-void

    .line 388384
    :cond_1
    iget-object v2, v0, LX/2GJ;->c:LX/0SG;

    iget-wide v3, v0, LX/2GJ;->p:J

    iget-wide v5, v0, LX/2GJ;->q:J

    invoke-virtual/range {v1 .. v6}, LX/2VA;->a(LX/0SG;JJ)V

    goto :goto_0
.end method
