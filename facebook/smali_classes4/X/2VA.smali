.class public LX/2VA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Eh;

.field public b:I

.field public c:J

.field public d:Z

.field public e:LX/0cV;


# direct methods
.method public constructor <init>(LX/1Eh;)V
    .locals 0

    .prologue
    .line 417197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417198
    iput-object p1, p0, LX/2VA;->a:LX/1Eh;

    .line 417199
    return-void
.end method


# virtual methods
.method public final a(LX/0SG;JJ)V
    .locals 4

    .prologue
    .line 417200
    iget v0, p0, LX/2VA;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/2VA;->b:I

    .line 417201
    iget v0, p0, LX/2VA;->b:I

    add-int/lit8 v0, v0, -0x1

    .line 417202
    :goto_0
    if-lez v0, :cond_0

    cmp-long v1, p2, p4

    if-gez v1, :cond_0

    .line 417203
    add-int/lit8 v0, v0, -0x1

    .line 417204
    const-wide/16 v2, 0x2

    mul-long/2addr p2, v2

    goto :goto_0

    .line 417205
    :cond_0
    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {p2, p3, p4, p5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/2VA;->c:J

    .line 417206
    return-void
.end method

.method public final a(LX/0SG;)Z
    .locals 4

    .prologue
    .line 417207
    invoke-virtual {p0}, LX/2VA;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/2VA;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 417208
    iget-object v0, p0, LX/2VA;->e:LX/0cV;

    if-eqz v0, :cond_0

    .line 417209
    iget-object v0, p0, LX/2VA;->e:LX/0cV;

    invoke-interface {v0}, LX/0cV;->b()V

    .line 417210
    const/4 v0, 0x0

    iput-object v0, p0, LX/2VA;->e:LX/0cV;

    .line 417211
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 417212
    iget v0, p0, LX/2VA;->b:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 417213
    iget-object v0, p0, LX/2VA;->a:LX/1Eh;

    invoke-interface {v0}, LX/1Eh;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
