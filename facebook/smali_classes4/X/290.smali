.class public LX/290;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Uo;

.field private final d:LX/297;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0Or;LX/0Uo;LX/297;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Uo;",
            "LX/297;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 375399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375400
    iput-object p1, p0, LX/290;->a:LX/0Zb;

    .line 375401
    iput-object p2, p0, LX/290;->b:LX/0Or;

    .line 375402
    iput-object p3, p0, LX/290;->c:LX/0Uo;

    .line 375403
    iput-object p4, p0, LX/290;->d:LX/297;

    .line 375404
    return-void
.end method

.method public static a(LX/290;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 375405
    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, LX/290;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375406
    const-string v0, "upload_this_event_now"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 375407
    :cond_0
    iget-object v0, p0, LX/290;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 375408
    return-void
.end method

.method public static b(LX/0QB;)LX/290;
    .locals 5

    .prologue
    .line 375409
    new-instance v3, LX/290;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const/16 v1, 0x2fd

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v1

    check-cast v1, LX/0Uo;

    invoke-static {p0}, LX/297;->b(LX/0QB;)LX/297;

    move-result-object v2

    check-cast v2, LX/297;

    invoke-direct {v3, v0, v4, v1, v2}, LX/290;-><init>(LX/0Zb;LX/0Or;LX/0Uo;LX/297;)V

    .line 375410
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 375411
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "zp_wakeup"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 375412
    const-string v1, "payload"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ct_s"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 375413
    invoke-static {p0, v0}, LX/290;->a(LX/290;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 375414
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJJ)V
    .locals 6

    .prologue
    .line 375415
    iget-object v0, p0, LX/290;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "0"

    .line 375416
    :goto_0
    iget-object v1, p0, LX/290;->d:LX/297;

    invoke-virtual {v1}, LX/297;->a()Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "0"

    .line 375417
    :goto_1
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "rule_result"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 375418
    const-string v3, "message_id"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "s_id"

    invoke-virtual {v3, v4, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "result"

    invoke-virtual {v3, v4, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "decision"

    invoke-virtual {v3, v4, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "length"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "type"

    invoke-virtual {v3, v4, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "is_g"

    invoke-static {p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "is_f"

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v3, "w"

    invoke-static {p8, p9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v3, "o"

    invoke-static/range {p10 .. p11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v3, "unr"

    invoke-static/range {p12 .. p13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v3, "g_m"

    invoke-virtual {v0, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 375419
    invoke-static {p0, v2}, LX/290;->a(LX/290;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 375420
    return-void

    .line 375421
    :cond_0
    const-string v0, "1"

    goto :goto_0

    .line 375422
    :cond_1
    const-string v1, "1"

    goto :goto_1
.end method
