.class public LX/3Pi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3Pj;


# direct methods
.method public constructor <init>(LX/3Pj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562179
    iput-object p1, p0, LX/3Pi;->a:LX/3Pj;

    .line 562180
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/4VT;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562169
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;

    .line 562170
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 562171
    :cond_0
    const/4 v0, 0x0

    .line 562172
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 562173
    new-instance p0, LX/6AH;

    invoke-direct {p0, v0}, LX/6AH;-><init>(Ljava/lang/String;)V

    .line 562174
    move-object v0, p0

    .line 562175
    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562176
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$RemoveExplicitPlaceListMutationCallModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562177
    const/4 v0, 0x0

    return-object v0
.end method
