.class public final LX/2ih;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ii;


# instance fields
.field public final synthetic a:LX/0g8;

.field public final synthetic b:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;LX/0g8;)V
    .locals 0

    .prologue
    .line 451946
    iput-object p1, p0, LX/2ih;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iput-object p2, p0, LX/2ih;->a:LX/0g8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 12

    .prologue
    .line 451947
    iget-object v0, p0, LX/2ih;->a:LX/0g8;

    invoke-interface {v0, p1}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v7

    .line 451948
    instance-of v0, v7, LX/2lq;

    if-nez v0, :cond_0

    .line 451949
    :goto_0
    return-void

    .line 451950
    :cond_0
    instance-of v0, v7, Lcom/facebook/friends/model/PersonYouMayKnow;

    if-eqz v0, :cond_1

    move-object v0, v7

    .line 451951
    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 451952
    iget-object v1, p0, LX/2ih;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->C:LX/2hb;

    .line 451953
    iget-object v8, v0, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    move-object v8, v8

    .line 451954
    if-nez v8, :cond_2

    .line 451955
    :goto_1
    iget-object v1, p0, LX/2ih;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->L:LX/2hd;

    iget-object v2, p0, LX/2ih;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v2, v2, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ay:Ljava/lang/String;

    .line 451956
    iget-object v3, v0, Lcom/facebook/friends/model/PersonYouMayKnow;->j:Ljava/lang/String;

    move-object v3, v3

    .line 451957
    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    iget-object v0, p0, LX/2ih;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    iget-object v6, v0, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    invoke-virtual/range {v1 .. v6}, LX/2hd;->d(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 451958
    :cond_1
    iget-object v1, p0, LX/2ih;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    move-object v0, v7

    check-cast v0, LX/2lq;

    invoke-interface {v0}, LX/2lr;->a()J

    move-result-wide v2

    .line 451959
    iput-wide v2, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->av:J

    .line 451960
    iget-object v0, p0, LX/2ih;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->J:LX/0gh;

    const-string v1, "tap_friends_jewel"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 451961
    iget-object v0, p0, LX/2ih;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0, v7}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->a$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Ljava/lang/Object;)V

    goto :goto_0

    .line 451962
    :cond_2
    const-string v8, "pymk_profile"

    .line 451963
    iget-object v9, v0, Lcom/facebook/friends/model/PersonYouMayKnow;->h:Ljava/lang/String;

    move-object v9, v9

    .line 451964
    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v1, v8, v9, v10}, LX/2hb;->a(LX/2hb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
