.class public final LX/3Jz;
.super LX/3Jv;
.source ""


# direct methods
.method public constructor <init>(LX/3Jx;[F)V
    .locals 0

    .prologue
    .line 548208
    invoke-direct {p0, p1, p2}, LX/3Jv;-><init>(LX/3Jx;[F)V

    .line 548209
    return-void
.end method


# virtual methods
.method public final a(LX/3Jv;FLX/9Ua;)V
    .locals 2

    .prologue
    .line 548204
    instance-of v0, p1, LX/3Jz;

    if-nez v0, :cond_0

    .line 548205
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MoveToCommand should only be interpolated with other instances of MoveToCommand"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548206
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/3Jv;->a(LX/3Jv;FLX/9Ua;)V

    .line 548207
    return-void
.end method

.method public final a(LX/9Ua;)V
    .locals 2

    .prologue
    .line 548192
    iget-object v0, p0, LX/3Jv;->a:LX/3Jx;

    iget-object v1, p0, LX/3Jv;->b:[F

    invoke-virtual {p0, p1, v0, v1}, LX/3Jz;->a(LX/9Ua;LX/3Jx;[F)V

    .line 548193
    return-void
.end method

.method public final a(LX/9Ua;LX/3Jx;[F)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 548194
    sget-object v0, LX/3Jy;->b:[I

    invoke-virtual {p2}, LX/3Jx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 548195
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "No such argument format %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548196
    :pswitch_0
    aget v0, p3, v4

    aget v1, p3, v3

    .line 548197
    iget-object v2, p1, LX/9Ua;->a:Landroid/graphics/Path;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->rMoveTo(FF)V

    .line 548198
    invoke-static {p1, v0, v1}, LX/9Ua;->f(LX/9Ua;FF)V

    .line 548199
    :goto_0
    return-void

    .line 548200
    :pswitch_1
    aget v0, p3, v4

    aget v1, p3, v3

    .line 548201
    iget-object v2, p1, LX/9Ua;->a:Landroid/graphics/Path;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 548202
    invoke-static {p1, v0, v1}, LX/9Ua;->e(LX/9Ua;FF)V

    .line 548203
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
