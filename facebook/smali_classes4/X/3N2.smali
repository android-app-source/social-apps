.class public LX/3N2;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/pm/PackageManager;

.field private final c:Landroid/app/ActivityManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 558090
    const-class v0, LX/3N2;

    sput-object v0, LX/3N2;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/pm/PackageManager;Landroid/app/ActivityManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558092
    iput-object p1, p0, LX/3N2;->b:Landroid/content/pm/PackageManager;

    .line 558093
    iput-object p2, p0, LX/3N2;->c:Landroid/app/ActivityManager;

    .line 558094
    return-void
.end method

.method public static b(LX/0QB;)LX/3N2;
    .locals 3

    .prologue
    .line 558095
    new-instance v2, LX/3N2;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    invoke-static {p0}, LX/0VU;->b(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    invoke-direct {v2, v0, v1}, LX/3N2;-><init>(Landroid/content/pm/PackageManager;Landroid/app/ActivityManager;)V

    .line 558096
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 558097
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/3N2;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 558098
    const/4 v0, 0x0

    .line 558099
    :try_start_0
    iget-object v1, p0, LX/3N2;->b:Landroid/content/pm/PackageManager;

    const/16 v2, 0x40

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 558100
    :try_start_1
    const-string v2, "SHA-1"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 558101
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 558102
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    :goto_0
    move-object v0, v0

    .line 558103
    if-nez v0, :cond_0

    .line 558104
    const/4 v0, 0x0

    .line 558105
    :goto_1
    return-object v0

    .line 558106
    :cond_0
    invoke-static {v0, p2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 558107
    :catch_0
    sget-object v1, LX/3N2;->a:Ljava/lang/Class;

    const-string v2, "Failed to read calling package\'s signature."

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 558108
    :catch_1
    sget-object v1, LX/3N2;->a:Ljava/lang/Class;

    const-string v2, "Failed to instantiate SHA-1 algorithm. It is evidently missing from this system."

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method
