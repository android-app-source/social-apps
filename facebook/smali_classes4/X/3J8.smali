.class public final enum LX/3J8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3J8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3J8;

.field public static final enum ANIMATED_GIF_VIDEO:LX/3J8;

.field public static final enum LIVE_360_VIDEO:LX/3J8;

.field public static final enum LIVE_VIDEO:LX/3J8;

.field public static final enum PREVIOUSLY_LIVE_360_VIDEO:LX/3J8;

.field public static final enum PREVIOUSLY_LIVE_VIDEO:LX/3J8;

.field public static final enum PURPLE_RAIN_VIDEO:LX/3J8;

.field public static final enum REGULAR_360_VIDEO:LX/3J8;

.field public static final enum REGULAR_VIDEO:LX/3J8;

.field public static final enum UNKNOWN_VIDEO:LX/3J8;


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 547476
    new-instance v0, LX/3J8;

    const-string v1, "UNKNOWN_VIDEO"

    invoke-direct {v0, v1, v4, v4}, LX/3J8;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3J8;->UNKNOWN_VIDEO:LX/3J8;

    .line 547477
    new-instance v0, LX/3J8;

    const-string v1, "REGULAR_VIDEO"

    invoke-direct {v0, v1, v5, v5}, LX/3J8;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    .line 547478
    new-instance v0, LX/3J8;

    const-string v1, "REGULAR_360_VIDEO"

    invoke-direct {v0, v1, v6, v6}, LX/3J8;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    .line 547479
    new-instance v0, LX/3J8;

    const-string v1, "LIVE_VIDEO"

    invoke-direct {v0, v1, v7, v7}, LX/3J8;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3J8;->LIVE_VIDEO:LX/3J8;

    .line 547480
    new-instance v0, LX/3J8;

    const-string v1, "PREVIOUSLY_LIVE_VIDEO"

    invoke-direct {v0, v1, v8, v8}, LX/3J8;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    .line 547481
    new-instance v0, LX/3J8;

    const-string v1, "ANIMATED_GIF_VIDEO"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/3J8;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3J8;->ANIMATED_GIF_VIDEO:LX/3J8;

    .line 547482
    new-instance v0, LX/3J8;

    const-string v1, "PURPLE_RAIN_VIDEO"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/3J8;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3J8;->PURPLE_RAIN_VIDEO:LX/3J8;

    .line 547483
    new-instance v0, LX/3J8;

    const-string v1, "LIVE_360_VIDEO"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/3J8;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3J8;->LIVE_360_VIDEO:LX/3J8;

    .line 547484
    new-instance v0, LX/3J8;

    const-string v1, "PREVIOUSLY_LIVE_360_VIDEO"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/3J8;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3J8;->PREVIOUSLY_LIVE_360_VIDEO:LX/3J8;

    .line 547485
    const/16 v0, 0x9

    new-array v0, v0, [LX/3J8;

    sget-object v1, LX/3J8;->UNKNOWN_VIDEO:LX/3J8;

    aput-object v1, v0, v4

    sget-object v1, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    aput-object v1, v0, v5

    sget-object v1, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    aput-object v1, v0, v6

    sget-object v1, LX/3J8;->LIVE_VIDEO:LX/3J8;

    aput-object v1, v0, v7

    sget-object v1, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/3J8;->ANIMATED_GIF_VIDEO:LX/3J8;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3J8;->PURPLE_RAIN_VIDEO:LX/3J8;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3J8;->LIVE_360_VIDEO:LX/3J8;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3J8;->PREVIOUSLY_LIVE_360_VIDEO:LX/3J8;

    aput-object v2, v0, v1

    sput-object v0, LX/3J8;->$VALUES:[LX/3J8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 547486
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 547487
    iput p3, p0, LX/3J8;->value:I

    .line 547488
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3J8;
    .locals 1

    .prologue
    .line 547489
    const-class v0, LX/3J8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3J8;

    return-object v0
.end method

.method public static values()[LX/3J8;
    .locals 1

    .prologue
    .line 547490
    sget-object v0, LX/3J8;->$VALUES:[LX/3J8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3J8;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 547491
    iget v0, p0, LX/3J8;->value:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
