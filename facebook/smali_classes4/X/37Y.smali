.class public LX/37Y;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile y:LX/37Y;


# instance fields
.field public b:LX/37e;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/37f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/38o;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/37a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final h:LX/0Uh;

.field public final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/7Iw;",
            ">;"
        }
    .end annotation
.end field

.field public j:Z

.field public k:LX/37Z;

.field public l:LX/37g;

.field public m:LX/37h;

.field public n:LX/38j;

.field public o:LX/38d;

.field public p:LX/38g;

.field private q:LX/3nE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3nE",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/7JN;

.field public s:LX/7JN;

.field public t:LX/7JN;

.field public u:Lcom/facebook/video/engine/VideoPlayerParams;

.field public v:Lcom/facebook/video/engine/VideoPlayerParams;

.field private w:Lcom/facebook/video/engine/VideoPlayerParams;

.field public x:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 501419
    const-class v0, LX/37Y;

    sput-object v0, LX/37Y;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/37Z;LX/37g;LX/37h;LX/38d;LX/38g;LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 501420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501421
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LX/37Y;->i:Ljava/util/Set;

    .line 501422
    new-instance v0, LX/38j;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/38j;-><init>(I)V

    iput-object v0, p0, LX/37Y;->n:LX/38j;

    .line 501423
    iput-object p1, p0, LX/37Y;->k:LX/37Z;

    .line 501424
    iput-object p2, p0, LX/37Y;->l:LX/37g;

    .line 501425
    iput-object p3, p0, LX/37Y;->m:LX/37h;

    .line 501426
    iput-object p4, p0, LX/37Y;->o:LX/38d;

    .line 501427
    iput-object p5, p0, LX/37Y;->p:LX/38g;

    .line 501428
    iput-object p6, p0, LX/37Y;->h:LX/0Uh;

    .line 501429
    iget-object v0, p0, LX/37Y;->m:LX/37h;

    new-instance v1, LX/38k;

    invoke-direct {v1, p0}, LX/38k;-><init>(LX/37Y;)V

    invoke-virtual {v0, v1}, LX/37h;->a(LX/38c;)V

    .line 501430
    iget-object v0, p0, LX/37Y;->l:LX/37g;

    new-instance v1, LX/38l;

    invoke-direct {v1, p0}, LX/38l;-><init>(LX/37Y;)V

    invoke-virtual {v0, v1}, LX/37g;->a(LX/38f;)V

    .line 501431
    invoke-direct {p0}, LX/37Y;->x()V

    .line 501432
    iget-object v0, p0, LX/37Y;->o:LX/38d;

    new-instance v1, LX/38n;

    invoke-direct {v1, p0}, LX/38n;-><init>(LX/37Y;)V

    .line 501433
    iget-object p0, v0, LX/38d;->e:Ljava/util/Vector;

    invoke-virtual {p0, v1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 501434
    iget-object p0, v0, LX/38d;->e:Ljava/util/Vector;

    invoke-virtual {p0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 501435
    return-void
.end method

.method public static B(LX/37Y;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 501436
    iput-object v0, p0, LX/37Y;->r:LX/7JN;

    .line 501437
    iput-object v0, p0, LX/37Y;->u:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 501438
    return-void
.end method

.method public static F(LX/37Y;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 501439
    invoke-virtual {p0}, LX/37Y;->s()LX/38j;

    move-result-object v3

    .line 501440
    iget-object v1, p0, LX/37Y;->n:LX/38j;

    invoke-virtual {v1, v3}, LX/38j;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 501441
    :cond_0
    return-void

    .line 501442
    :cond_1
    iget-object v1, p0, LX/37Y;->r:LX/7JN;

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/37Y;->r:LX/7JN;

    .line 501443
    iget-object v2, v1, LX/7JN;->a:Ljava/lang/String;

    move-object v1, v2

    .line 501444
    invoke-virtual {p0, v1}, LX/37Y;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v5, v0

    .line 501445
    :goto_0
    iget-object v1, p0, LX/37Y;->r:LX/7JN;

    if-eqz v1, :cond_2

    .line 501446
    invoke-virtual {v3}, LX/38j;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 501447
    iget-object v0, p0, LX/37Y;->r:LX/7JN;

    invoke-virtual {p0}, LX/37Y;->n()I

    move-result v1

    .line 501448
    iput v1, v0, LX/7JN;->p:I

    .line 501449
    :cond_2
    :goto_1
    iget-object v0, p0, LX/37Y;->f:LX/38o;

    iget-object v1, p0, LX/37Y;->t:LX/7JN;

    invoke-virtual {p0}, LX/37Y;->n()I

    move-result v2

    iget-object v4, p0, LX/37Y;->n:LX/38j;

    .line 501450
    invoke-virtual {v4}, LX/38j;->a()Z

    move-result v6

    .line 501451
    iget v7, v3, LX/38j;->b:I

    move v7, v7

    .line 501452
    packed-switch v7, :pswitch_data_0

    .line 501453
    :cond_3
    :goto_2
    iput-object v3, p0, LX/37Y;->n:LX/38j;

    .line 501454
    iget-object v0, p0, LX/37Y;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Iw;

    .line 501455
    invoke-interface {v0}, LX/7Iw;->dK_()V

    goto :goto_3

    .line 501456
    :cond_4
    const/4 v5, 0x0

    goto :goto_0

    .line 501457
    :cond_5
    iget v1, v3, LX/38j;->b:I

    move v1, v1

    .line 501458
    if-ne v1, v0, :cond_2

    .line 501459
    invoke-static {p0}, LX/37Y;->B(LX/37Y;)V

    goto :goto_1

    .line 501460
    :pswitch_0
    if-nez v6, :cond_3

    .line 501461
    invoke-static {v0, v1, v2}, LX/38o;->b(LX/38o;LX/7JN;I)V

    goto :goto_2

    .line 501462
    :pswitch_1
    if-eqz v6, :cond_3

    .line 501463
    invoke-static {v0, v1, v2}, LX/38o;->c(LX/38o;LX/7JN;I)V

    goto :goto_2

    .line 501464
    :pswitch_2
    if-eqz v5, :cond_6

    .line 501465
    invoke-static {v0, v1}, LX/38o;->c(LX/38o;LX/7JN;)V

    goto :goto_2

    .line 501466
    :cond_6
    if-eqz v6, :cond_3

    .line 501467
    invoke-static {v0, v1, v2}, LX/38o;->c(LX/38o;LX/7JN;I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/37Y;
    .locals 10

    .prologue
    .line 501468
    sget-object v0, LX/37Y;->y:LX/37Y;

    if-nez v0, :cond_1

    .line 501469
    const-class v1, LX/37Y;

    monitor-enter v1

    .line 501470
    :try_start_0
    sget-object v0, LX/37Y;->y:LX/37Y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 501471
    if-eqz v2, :cond_0

    .line 501472
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 501473
    new-instance v3, LX/37Y;

    invoke-static {v0}, LX/37Z;->a(LX/0QB;)LX/37Z;

    move-result-object v4

    check-cast v4, LX/37Z;

    invoke-static {v0}, LX/37g;->a(LX/0QB;)LX/37g;

    move-result-object v5

    check-cast v5, LX/37g;

    invoke-static {v0}, LX/37h;->a(LX/0QB;)LX/37h;

    move-result-object v6

    check-cast v6, LX/37h;

    invoke-static {v0}, LX/38d;->a(LX/0QB;)LX/38d;

    move-result-object v7

    check-cast v7, LX/38d;

    invoke-static {v0}, LX/38g;->a(LX/0QB;)LX/38g;

    move-result-object v8

    check-cast v8, LX/38g;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-direct/range {v3 .. v9}, LX/37Y;-><init>(LX/37Z;LX/37g;LX/37h;LX/38d;LX/38g;LX/0Uh;)V

    .line 501474
    invoke-static {v0}, LX/37e;->a(LX/0QB;)LX/37e;

    move-result-object v4

    check-cast v4, LX/37e;

    invoke-static {v0}, LX/37f;->a(LX/0QB;)LX/37f;

    move-result-object v5

    check-cast v5, LX/37f;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v7

    check-cast v7, LX/0W3;

    invoke-static {v0}, LX/38o;->a(LX/0QB;)LX/38o;

    move-result-object v8

    check-cast v8, LX/38o;

    invoke-static {v0}, LX/37a;->a(LX/0QB;)LX/37a;

    move-result-object v9

    check-cast v9, LX/37a;

    .line 501475
    iput-object v4, v3, LX/37Y;->b:LX/37e;

    iput-object v5, v3, LX/37Y;->c:LX/37f;

    iput-object v6, v3, LX/37Y;->d:Landroid/content/Context;

    iput-object v7, v3, LX/37Y;->e:LX/0W3;

    iput-object v8, v3, LX/37Y;->f:LX/38o;

    iput-object v9, v3, LX/37Y;->g:LX/37a;

    .line 501476
    move-object v0, v3

    .line 501477
    sput-object v0, LX/37Y;->y:LX/37Y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501478
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 501479
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 501480
    :cond_1
    sget-object v0, LX/37Y;->y:LX/37Y;

    return-object v0

    .line 501481
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 501482
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/37Y;LX/7JN;)V
    .locals 3

    .prologue
    .line 501483
    if-nez p1, :cond_1

    .line 501484
    :cond_0
    :goto_0
    return-void

    .line 501485
    :cond_1
    iget-object v0, p0, LX/37Y;->g:LX/37a;

    sget-object v1, LX/7JP;->b:LX/0Tn;

    .line 501486
    iget-object v2, p1, LX/7JN;->a:Ljava/lang/String;

    move-object v2, v2

    .line 501487
    invoke-virtual {v0, v1, v2}, LX/37a;->a(LX/0Tn;Ljava/lang/String;)V

    .line 501488
    iget-object v0, p0, LX/37Y;->g:LX/37a;

    sget-object v1, LX/7JP;->c:LX/0Tn;

    .line 501489
    iget-object v2, p1, LX/7JN;->b:Ljava/lang/String;

    move-object v2, v2

    .line 501490
    invoke-virtual {v0, v1, v2}, LX/37a;->a(LX/0Tn;Ljava/lang/String;)V

    .line 501491
    iget-object v0, p1, LX/7JN;->l:LX/1bf;

    move-object v0, v0

    .line 501492
    if-eqz v0, :cond_0

    .line 501493
    iget-object v0, p0, LX/37Y;->g:LX/37a;

    sget-object v1, LX/7JP;->a:LX/0Tn;

    .line 501494
    iget-object v2, p1, LX/7JN;->l:LX/1bf;

    move-object v2, v2

    .line 501495
    iget-object p0, v2, LX/1bf;->b:Landroid/net/Uri;

    move-object v2, p0

    .line 501496
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/37a;->a(LX/0Tn;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(LX/37Y;LX/7JJ;)Z
    .locals 2

    .prologue
    .line 501497
    iget-object v0, p0, LX/37Y;->p:LX/38g;

    invoke-virtual {v0}, LX/38g;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501498
    const/4 v0, 0x0

    .line 501499
    :goto_0
    return v0

    .line 501500
    :cond_0
    iget-object v0, p0, LX/37Y;->b:LX/37e;

    const-string v1, "Player is not attached"

    invoke-virtual {v0, p1, v1}, LX/37e;->a(LX/7JJ;Ljava/lang/String;)V

    .line 501501
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private x()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 501502
    iget-object v0, p0, LX/37Y;->k:LX/37Z;

    new-instance v1, LX/38m;

    invoke-direct {v1, p0}, LX/38m;-><init>(LX/37Y;)V

    .line 501503
    iget-object p0, v0, LX/37Z;->i:Ljava/util/Vector;

    invoke-virtual {p0, v1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 501504
    iget-object p0, v0, LX/37Z;->i:Ljava/util/Vector;

    invoke-virtual {p0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 501505
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 8

    .prologue
    .line 501559
    sget-object v0, LX/7JJ;->VideoCastManager_Seek:LX/7JJ;

    invoke-static {p0, v0}, LX/37Y;->a(LX/37Y;LX/7JJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501560
    :goto_0
    return-void

    .line 501561
    :cond_0
    iget-object v0, p0, LX/37Y;->p:LX/38g;

    .line 501562
    iget-object v1, v0, LX/38g;->f:LX/7JI;

    long-to-double v3, p1

    const-wide v5, 0x408f400000000000L    # 1000.0

    div-double/2addr v3, v5

    .line 501563
    iget v2, v1, LX/7JI;->g:I

    const/4 v5, 0x6

    if-eq v2, v5, :cond_1

    iget v2, v1, LX/7JI;->g:I

    const/4 v5, 0x7

    if-eq v2, v5, :cond_1

    .line 501564
    iget-object v2, v1, LX/7JI;->a:LX/38g;

    iget-object v2, v2, LX/38g;->c:LX/37e;

    sget-object v5, LX/7JJ;->FbAppPlayer_Seek:LX/7JJ;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Incorrect state: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v1, LX/7JI;->g:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, LX/37e;->a(LX/7JJ;Ljava/lang/String;)V

    .line 501565
    :cond_1
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 501566
    :try_start_0
    const-string v2, "cmd"

    const-string v6, "seek_video"

    invoke-virtual {v5, v2, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 501567
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 501568
    const-string v6, "position"

    invoke-virtual {v2, v6, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 501569
    const-string v6, "params"

    invoke-virtual {v5, v6, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 501570
    :goto_1
    const-string v2, "experience_command"

    iget-object v6, v1, LX/7JI;->j:Ljava/lang/String;

    new-instance v7, LX/7JE;

    invoke-direct {v7, v1}, LX/7JE;-><init>(LX/7JI;)V

    invoke-static {v1, v2, v6, v5, v7}, LX/7JI;->a(LX/7JI;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;LX/27U;)V

    .line 501571
    goto :goto_0

    .line 501572
    :catch_0
    move-exception v2

    .line 501573
    iget-object v6, v1, LX/7JI;->a:LX/38g;

    iget-object v6, v6, LX/38g;->c:LX/37e;

    sget-object v7, LX/7JJ;->FbAppPlayer_Seek:LX/7JJ;

    invoke-virtual {v6, v7, v2}, LX/37e;->a(LX/7JJ;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public final a(LX/384;)V
    .locals 1

    .prologue
    .line 501506
    iget-object v0, p0, LX/37Y;->l:LX/37g;

    invoke-virtual {v0, p1}, LX/37g;->a(LX/384;)V

    .line 501507
    return-void
.end method

.method public final a(LX/7Iw;)V
    .locals 1

    .prologue
    .line 501508
    iget-object v0, p0, LX/37Y;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 501509
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 501510
    iget-object v1, p0, LX/37Y;->h:LX/0Uh;

    sget v2, LX/19n;->d:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 501511
    if-eqz v1, :cond_1

    .line 501512
    iget-boolean v2, p0, LX/37Y;->x:Z

    if-nez v2, :cond_0

    .line 501513
    sget-object v2, LX/1vX;->c:LX/1vX;

    move-object v2, v2

    .line 501514
    iget-object v3, p0, LX/37Y;->d:Landroid/content/Context;

    invoke-virtual {v2, v3}, LX/1od;->a(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, LX/37Y;->x:Z

    .line 501515
    :cond_0
    iget-boolean v2, p0, LX/37Y;->x:Z

    move v2, v2

    .line 501516
    if-nez v2, :cond_1

    .line 501517
    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 501518
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(LX/7JN;Lcom/facebook/video/engine/VideoPlayerParams;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 501519
    invoke-virtual {p0}, LX/37Y;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 501520
    :cond_0
    :goto_0
    return v0

    .line 501521
    :cond_1
    invoke-virtual {p0}, LX/37Y;->g()LX/38p;

    move-result-object v1

    invoke-virtual {v1}, LX/38p;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 501522
    invoke-virtual {p0, p2}, LX/37Y;->a(Lcom/facebook/video/engine/VideoPlayerParams;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 501523
    iget-object v0, p2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    .line 501524
    invoke-virtual {p0, v0}, LX/37Y;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 501525
    invoke-virtual {p0}, LX/37Y;->s()LX/38j;

    move-result-object v0

    invoke-virtual {v0}, LX/38j;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 501526
    invoke-virtual {p0}, LX/37Y;->r()V

    .line 501527
    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 501528
    :cond_3
    iget-object v1, p0, LX/37Y;->c:LX/37f;

    invoke-virtual {p0}, LX/37Y;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/37f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 501529
    invoke-virtual {p0, p1, p2}, LX/37Y;->b(LX/7JN;Lcom/facebook/video/engine/VideoPlayerParams;)V

    .line 501530
    sget-object v0, LX/7JJ;->VideoCastManager_ChangeVideo:LX/7JJ;

    invoke-static {p0, v0}, LX/37Y;->a(LX/37Y;LX/7JJ;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 501531
    :goto_2
    goto :goto_1

    .line 501532
    :cond_4
    invoke-virtual {p0}, LX/37Y;->r()V

    .line 501533
    sget-object v0, LX/7JJ;->VideoCastManager_Skip:LX/7JJ;

    invoke-static {p0, v0}, LX/37Y;->a(LX/37Y;LX/7JJ;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 501534
    :goto_3
    goto :goto_2

    .line 501535
    :cond_5
    iget-object v0, p0, LX/37Y;->p:LX/38g;

    .line 501536
    iget-object v1, v0, LX/38g;->f:LX/7JI;

    const/4 v0, 0x5

    .line 501537
    iget v2, v1, LX/7JI;->g:I

    const/4 p1, 0x6

    if-eq v2, p1, :cond_6

    iget v2, v1, LX/7JI;->g:I

    const/4 p1, 0x7

    if-eq v2, p1, :cond_6

    iget v2, v1, LX/7JI;->g:I

    const/4 p1, 0x4

    if-eq v2, p1, :cond_6

    iget v2, v1, LX/7JI;->g:I

    if-eq v2, v0, :cond_6

    .line 501538
    iget-object v2, v1, LX/7JI;->a:LX/38g;

    iget-object v2, v2, LX/38g;->c:LX/37e;

    sget-object p1, LX/7JJ;->FbAppPlayer_Skip:LX/7JJ;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p0, "Incorrect state: "

    invoke-direct {p2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget p0, v1, LX/7JI;->g:I

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p1, p2}, LX/37e;->a(LX/7JJ;Ljava/lang/String;)V

    .line 501539
    :cond_6
    invoke-static {v1, v0}, LX/7JI;->a(LX/7JI;I)V

    .line 501540
    const-string v2, "experience_command"

    iget-object p1, v1, LX/7JI;->j:Ljava/lang/String;

    const-string p2, "{\"cmd\":\"skip_video\"}"

    new-instance p0, LX/7JF;

    invoke-direct {p0, v1}, LX/7JF;-><init>(LX/7JI;)V

    invoke-static {v1, v2, p1, p2, p0}, LX/7JI;->a(LX/7JI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/27U;)V

    .line 501541
    goto :goto_3
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 501542
    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->j:Z

    if-eqz v1, :cond_1

    .line 501543
    :cond_0
    :goto_0
    return v0

    .line 501544
    :cond_1
    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v1, :cond_2

    .line 501545
    iget-object v0, p0, LX/37Y;->e:LX/0W3;

    sget-wide v2, LX/0X5;->bm:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    .line 501546
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto :goto_0

    .line 501547
    :cond_2
    iget-boolean v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->g:Z

    if-eqz v1, :cond_3

    .line 501548
    iget-object v0, p0, LX/37Y;->e:LX/0W3;

    sget-wide v2, LX/0X5;->bn:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    .line 501549
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto :goto_0

    .line 501550
    :cond_3
    iget-object v1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 501551
    if-nez v1, :cond_0

    .line 501552
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 501553
    invoke-virtual {p0}, LX/37Y;->s()LX/38j;

    move-result-object v0

    invoke-virtual {v0}, LX/38j;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/37Y;->s:LX/7JN;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/37Y;->r:LX/7JN;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/37Y;->t:LX/7JN;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/37Y;->t:LX/7JN;

    .line 501554
    iget-object p0, v0, LX/7JN;->a:Ljava/lang/String;

    move-object v0, p0

    .line 501555
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/384;",
            ">;"
        }
    .end annotation

    .prologue
    .line 501556
    iget-object v0, p0, LX/37Y;->m:LX/37h;

    .line 501557
    iget-object p0, v0, LX/37h;->e:Ljava/util/Vector;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    move-object v0, p0

    .line 501558
    return-object v0
.end method

.method public final b(LX/7Iw;)V
    .locals 1

    .prologue
    .line 501417
    iget-object v0, p0, LX/37Y;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 501418
    return-void
.end method

.method public final b(LX/7JN;Lcom/facebook/video/engine/VideoPlayerParams;)V
    .locals 1

    .prologue
    .line 501284
    iget-object v0, p1, LX/7JN;->a:Ljava/lang/String;

    move-object v0, v0

    .line 501285
    invoke-virtual {p0, v0}, LX/37Y;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 501286
    iput-object p1, p0, LX/37Y;->s:LX/7JN;

    .line 501287
    iput-object p2, p0, LX/37Y;->v:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 501288
    iget-object v0, p0, LX/37Y;->s:LX/7JN;

    iput-object v0, p0, LX/37Y;->t:LX/7JN;

    .line 501289
    iget-object v0, p0, LX/37Y;->v:Lcom/facebook/video/engine/VideoPlayerParams;

    iput-object v0, p0, LX/37Y;->w:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 501290
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 501273
    const/4 v0, 0x0

    .line 501274
    iget-object v1, p0, LX/37Y;->r:LX/7JN;

    if-eqz v1, :cond_1

    .line 501275
    iget-object v0, p0, LX/37Y;->r:LX/7JN;

    .line 501276
    iget-object v1, v0, LX/7JN;->a:Ljava/lang/String;

    move-object v0, v1

    .line 501277
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 501278
    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 501279
    return v0

    .line 501280
    :cond_1
    iget-object v1, p0, LX/37Y;->s:LX/7JN;

    if-eqz v1, :cond_0

    .line 501281
    iget-object v0, p0, LX/37Y;->s:LX/7JN;

    .line 501282
    iget-object v1, v0, LX/7JN;->a:Ljava/lang/String;

    move-object v0, v1

    .line 501283
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 501291
    invoke-virtual {p0}, LX/37Y;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 501292
    iget-object v1, p0, LX/37Y;->f:LX/38o;

    sget-object v2, LX/38o;->a:Ljava/lang/String;

    iget-object v3, p0, LX/37Y;->r:LX/7JN;

    iget-object v0, p0, LX/37Y;->n:LX/38j;

    invoke-virtual {v0}, LX/38j;->a()Z

    move-result v4

    iget-object v0, p0, LX/37Y;->p:LX/38g;

    invoke-virtual {v0}, LX/38g;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/37Y;->n()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v2, v3, v4, v0}, LX/38o;->a(Ljava/lang/String;LX/7JN;ZI)V

    .line 501293
    iget-object v0, p0, LX/37Y;->l:LX/37g;

    invoke-virtual {v0}, LX/37g;->b()V

    .line 501294
    return-void

    .line 501295
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 7

    .prologue
    .line 501296
    iget-object v0, p0, LX/37Y;->l:LX/37g;

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 501297
    iget-object v1, v0, LX/37g;->j:Lcom/google/android/gms/cast/CastDevice;

    if-nez v1, :cond_0

    move-object v1, v2

    .line 501298
    :goto_0
    sget-object v3, LX/7J7;->a:[I

    invoke-virtual {v0}, LX/37g;->d()LX/38p;

    move-result-object v4

    .line 501299
    iget-object p0, v4, LX/38p;->a:LX/38a;

    move-object v4, p0

    .line 501300
    invoke-virtual {v4}, LX/38a;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 501301
    :goto_1
    move-object v0, v2

    .line 501302
    return-object v0

    .line 501303
    :cond_0
    iget-object v1, v0, LX/37g;->j:Lcom/google/android/gms/cast/CastDevice;

    iget-object v3, v1, Lcom/google/android/gms/cast/CastDevice;->e:Ljava/lang/String;

    move-object v1, v3

    .line 501304
    goto :goto_0

    .line 501305
    :pswitch_0
    iget-object v2, v0, LX/37g;->l:Landroid/content/Context;

    const v3, 0x7f081a6f

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 501306
    :pswitch_1
    iget-object v2, v0, LX/37g;->l:Landroid/content/Context;

    const v3, 0x7f081a6c

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 501307
    :pswitch_2
    iget-object v1, v0, LX/37g;->l:Landroid/content/Context;

    const v2, 0x7f081a70

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 501308
    :pswitch_3
    iget-object v1, v0, LX/37g;->l:Landroid/content/Context;

    const v2, 0x7f081a6d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final g()LX/38p;
    .locals 1

    .prologue
    .line 501309
    iget-object v0, p0, LX/37Y;->l:LX/37g;

    invoke-virtual {v0}, LX/37g;->d()LX/38p;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 501310
    iget-boolean v0, p0, LX/37Y;->j:Z

    if-eqz v0, :cond_1

    .line 501311
    :cond_0
    :goto_0
    return-void

    .line 501312
    :cond_1
    invoke-virtual {p0}, LX/37Y;->g()LX/38p;

    move-result-object v0

    invoke-virtual {v0}, LX/38p;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501313
    iget-object v0, p0, LX/37Y;->l:LX/37g;

    invoke-virtual {v0}, LX/37g;->a()Ljava/lang/String;

    move-result-object v0

    .line 501314
    iget-object v1, p0, LX/37Y;->k:LX/37Z;

    invoke-virtual {v1}, LX/37Z;->a()Ljava/lang/String;

    move-result-object v1

    .line 501315
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 501316
    iput-boolean v2, p0, LX/37Y;->j:Z

    .line 501317
    iget-object v1, p0, LX/37Y;->m:LX/37h;

    invoke-virtual {v1, v0}, LX/37h;->a(Ljava/lang/String;)LX/384;

    move-result-object v0

    .line 501318
    if-eqz v0, :cond_2

    .line 501319
    iget-object v1, p0, LX/37Y;->l:LX/37g;

    invoke-virtual {v1, v0}, LX/37g;->a(LX/384;)V

    .line 501320
    :cond_2
    iget-object v0, p0, LX/37Y;->q:LX/3nE;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/37Y;->q:LX/3nE;

    invoke-virtual {v0}, LX/3nE;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 501321
    iget-object v0, p0, LX/37Y;->q:LX/3nE;

    invoke-virtual {v0, v2}, LX/3nE;->cancel(Z)Z

    .line 501322
    :cond_3
    new-instance v0, LX/7JK;

    invoke-direct {v0, p0}, LX/7JK;-><init>(LX/37Y;)V

    iput-object v0, p0, LX/37Y;->q:LX/3nE;

    .line 501323
    iget-object v0, p0, LX/37Y;->q:LX/3nE;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LX/3nE;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 501324
    iget-object v0, p0, LX/37Y;->k:LX/37Z;

    invoke-virtual {v0}, LX/37Z;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 501325
    iget-object v0, p0, LX/37Y;->r:LX/7JN;

    if-eqz v0, :cond_0

    .line 501326
    iget-object v0, p0, LX/37Y;->r:LX/7JN;

    .line 501327
    iget-object p0, v0, LX/7JN;->a:Ljava/lang/String;

    move-object v0, p0

    .line 501328
    :goto_0
    return-object v0

    .line 501329
    :cond_0
    iget-object v0, p0, LX/37Y;->t:LX/7JN;

    if-eqz v0, :cond_1

    .line 501330
    iget-object v0, p0, LX/37Y;->t:LX/7JN;

    .line 501331
    iget-object p0, v0, LX/7JN;->a:Ljava/lang/String;

    move-object v0, p0

    .line 501332
    goto :goto_0

    .line 501333
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 9

    .prologue
    .line 501334
    sget-object v0, LX/7JJ;->VideoCastManager_VolumeUp:LX/7JJ;

    invoke-static {p0, v0}, LX/37Y;->a(LX/37Y;LX/7JJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501335
    const/4 v0, 0x0

    .line 501336
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/37Y;->p:LX/38g;

    const/4 v1, 0x0

    .line 501337
    invoke-virtual {v0}, LX/38g;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 501338
    :goto_1
    move v0, v1

    .line 501339
    goto :goto_0

    .line 501340
    :cond_1
    sget-object v2, LX/7Yq;->c:LX/7Yl;

    iget-object v3, v0, LX/38g;->g:LX/2wX;

    invoke-interface {v2, v3}, LX/7Yl;->b(LX/2wX;)D

    move-result-wide v3

    .line 501341
    const-wide v5, 0x3fa999999999999aL    # 0.05

    add-double/2addr v3, v5

    .line 501342
    :try_start_0
    sget-object v2, LX/7Yq;->c:LX/7Yl;

    iget-object v5, v0, LX/38g;->g:LX/2wX;

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    invoke-static {v3, v4, v7, v8}, Ljava/lang/Math;->min(DD)D

    move-result-wide v3

    invoke-interface {v2, v5, v3, v4}, LX/7Yl;->a(LX/2wX;D)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 501343
    const/4 v1, 0x1

    goto :goto_1

    .line 501344
    :catch_0
    move-exception v2

    .line 501345
    iget-object v3, v0, LX/38g;->c:LX/37e;

    sget-object v4, LX/7JJ;->CastPlayer_VolumeUp:LX/7JJ;

    invoke-virtual {v3, v4, v2}, LX/37e;->a(LX/7JJ;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public final m()Z
    .locals 9

    .prologue
    .line 501346
    sget-object v0, LX/7JJ;->VideoCastManager_VolumeDown:LX/7JJ;

    invoke-static {p0, v0}, LX/37Y;->a(LX/37Y;LX/7JJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501347
    const/4 v0, 0x0

    .line 501348
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/37Y;->p:LX/38g;

    const/4 v1, 0x0

    .line 501349
    invoke-virtual {v0}, LX/38g;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 501350
    :goto_1
    move v0, v1

    .line 501351
    goto :goto_0

    .line 501352
    :cond_1
    sget-object v2, LX/7Yq;->c:LX/7Yl;

    iget-object v3, v0, LX/38g;->g:LX/2wX;

    invoke-interface {v2, v3}, LX/7Yl;->b(LX/2wX;)D

    move-result-wide v3

    .line 501353
    const-wide v5, 0x3fa999999999999aL    # 0.05

    sub-double/2addr v3, v5

    .line 501354
    :try_start_0
    sget-object v2, LX/7Yq;->c:LX/7Yl;

    iget-object v5, v0, LX/38g;->g:LX/2wX;

    const-wide/16 v7, 0x0

    invoke-static {v3, v4, v7, v8}, Ljava/lang/Math;->max(DD)D

    move-result-wide v3

    invoke-interface {v2, v5, v3, v4}, LX/7Yl;->a(LX/2wX;D)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 501355
    const/4 v1, 0x1

    goto :goto_1

    .line 501356
    :catch_0
    move-exception v2

    .line 501357
    iget-object v3, v0, LX/38g;->c:LX/37e;

    sget-object v4, LX/7JJ;->CastPlayer_VolumeDown:LX/7JJ;

    invoke-virtual {v3, v4, v2}, LX/37e;->a(LX/7JJ;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public final n()I
    .locals 7

    .prologue
    .line 501358
    sget-object v0, LX/7JJ;->VideoCastManager_GetCurrentStreamPosition:LX/7JJ;

    invoke-static {p0, v0}, LX/37Y;->a(LX/37Y;LX/7JJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501359
    const/4 v0, 0x0

    .line 501360
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/37Y;->p:LX/38g;

    .line 501361
    iget-object v1, v0, LX/38g;->f:LX/7JI;

    .line 501362
    iget-wide v5, v1, LX/7JI;->h:D

    move-wide v1, v5

    .line 501363
    const-wide v3, 0x408f400000000000L    # 1000.0

    mul-double/2addr v1, v3

    double-to-int v1, v1

    move v0, v1

    .line 501364
    goto :goto_0
.end method

.method public final o()I
    .locals 7

    .prologue
    .line 501365
    iget-object v0, p0, LX/37Y;->p:LX/38g;

    .line 501366
    iget-object v1, v0, LX/38g;->f:LX/7JI;

    .line 501367
    iget-wide v5, v1, LX/7JI;->i:D

    move-wide v1, v5

    .line 501368
    const-wide v3, 0x408f400000000000L    # 1000.0

    mul-double/2addr v1, v3

    double-to-int v1, v1

    move v0, v1

    .line 501369
    return v0
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 501370
    sget-object v0, LX/7JJ;->VideoCastManager_TogglePlayback:LX/7JJ;

    invoke-static {p0, v0}, LX/37Y;->a(LX/37Y;LX/7JJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501371
    :goto_0
    return-void

    .line 501372
    :cond_0
    invoke-virtual {p0}, LX/37Y;->s()LX/38j;

    move-result-object v0

    .line 501373
    iget v1, v0, LX/38j;->b:I

    move v0, v1

    .line 501374
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 501375
    :pswitch_0
    invoke-virtual {p0}, LX/37Y;->r()V

    goto :goto_0

    .line 501376
    :pswitch_1
    invoke-virtual {p0}, LX/37Y;->q()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final q()V
    .locals 4

    .prologue
    .line 501377
    sget-object v0, LX/7JJ;->VideoCastManager_Pause:LX/7JJ;

    invoke-static {p0, v0}, LX/37Y;->a(LX/37Y;LX/7JJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501378
    :goto_0
    return-void

    .line 501379
    :cond_0
    iget-object v0, p0, LX/37Y;->p:LX/38g;

    .line 501380
    iget-object v1, v0, LX/38g;->f:LX/7JI;

    .line 501381
    iget v2, v1, LX/7JI;->g:I

    const/4 v3, 0x6

    if-eq v2, v3, :cond_1

    .line 501382
    iget-object v2, v1, LX/7JI;->a:LX/38g;

    iget-object v2, v2, LX/38g;->c:LX/37e;

    sget-object v3, LX/7JJ;->FbAppPlayer_Pause:LX/7JJ;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string v0, "Incorrect state: "

    invoke-direct {p0, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v1, LX/7JI;->g:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/37e;->a(LX/7JJ;Ljava/lang/String;)V

    .line 501383
    :cond_1
    const/4 v2, 0x7

    invoke-static {v1, v2}, LX/7JI;->a(LX/7JI;I)V

    .line 501384
    const-string v2, "experience_command"

    iget-object v3, v1, LX/7JI;->j:Ljava/lang/String;

    const-string p0, "{\"cmd\":\"pause_video\"}"

    new-instance v0, LX/7JD;

    invoke-direct {v0, v1}, LX/7JD;-><init>(LX/7JI;)V

    invoke-static {v1, v2, v3, p0, v0}, LX/7JI;->a(LX/7JI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/27U;)V

    .line 501385
    goto :goto_0
.end method

.method public final r()V
    .locals 6

    .prologue
    .line 501386
    sget-object v0, LX/7JJ;->VideoCastManager_Play:LX/7JJ;

    invoke-static {p0, v0}, LX/37Y;->a(LX/37Y;LX/7JJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501387
    :goto_0
    return-void

    .line 501388
    :cond_0
    iget-object v0, p0, LX/37Y;->s:LX/7JN;

    if-nez v0, :cond_2

    .line 501389
    iget-object v0, p0, LX/37Y;->r:LX/7JN;

    if-eqz v0, :cond_1

    .line 501390
    iget-object v0, p0, LX/37Y;->p:LX/38g;

    .line 501391
    iget-object v1, v0, LX/38g;->f:LX/7JI;

    invoke-virtual {v1}, LX/7JI;->g()V

    .line 501392
    goto :goto_0

    .line 501393
    :cond_1
    iget-object v0, p0, LX/37Y;->t:LX/7JN;

    if-eqz v0, :cond_2

    .line 501394
    iget-object v0, p0, LX/37Y;->t:LX/7JN;

    iput-object v0, p0, LX/37Y;->s:LX/7JN;

    .line 501395
    iget-object v0, p0, LX/37Y;->w:Lcom/facebook/video/engine/VideoPlayerParams;

    iput-object v0, p0, LX/37Y;->v:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 501396
    :cond_2
    iget-object v0, p0, LX/37Y;->s:LX/7JN;

    if-eqz v0, :cond_3

    .line 501397
    iget-object v0, p0, LX/37Y;->p:LX/38g;

    iget-object v1, p0, LX/37Y;->s:LX/7JN;

    .line 501398
    iget-object v2, v1, LX/7JN;->a:Ljava/lang/String;

    move-object v1, v2

    .line 501399
    iget-object v2, p0, LX/37Y;->s:LX/7JN;

    .line 501400
    iget-object v3, v2, LX/7JN;->k:LX/04D;

    move-object v2, v3

    .line 501401
    iget-object v3, p0, LX/37Y;->s:LX/7JN;

    .line 501402
    iget v4, v3, LX/7JN;->o:I

    move v3, v4

    .line 501403
    int-to-long v4, v3

    invoke-virtual {v0, v1, v2, v4, v5}, LX/38g;->a(Ljava/lang/String;LX/04D;J)V

    goto :goto_0

    .line 501404
    :cond_3
    iget-object v0, p0, LX/37Y;->b:LX/37e;

    sget-object v1, LX/7JJ;->VideoCastManager_Play:LX/7JJ;

    const-string v2, "Play requested with no params loaded."

    invoke-virtual {v0, v1, v2}, LX/37e;->a(LX/7JJ;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final s()LX/38j;
    .locals 2

    .prologue
    .line 501405
    iget-object v0, p0, LX/37Y;->p:LX/38g;

    .line 501406
    iget-object v1, v0, LX/38g;->f:LX/7JI;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 501407
    :goto_0
    new-instance p0, LX/38j;

    invoke-direct {p0, v1}, LX/38j;-><init>(I)V

    move-object v0, p0

    .line 501408
    return-object v0

    .line 501409
    :cond_0
    iget-object v1, v0, LX/38g;->f:LX/7JI;

    .line 501410
    iget p0, v1, LX/7JI;->g:I

    packed-switch p0, :pswitch_data_0

    .line 501411
    const/4 p0, 0x0

    :goto_1
    move v1, p0

    .line 501412
    goto :goto_0

    .line 501413
    :pswitch_0
    const/4 p0, 0x1

    goto :goto_1

    .line 501414
    :pswitch_1
    const/4 p0, 0x4

    goto :goto_1

    .line 501415
    :pswitch_2
    const/4 p0, 0x2

    goto :goto_1

    .line 501416
    :pswitch_3
    const/4 p0, 0x3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
