.class public LX/2H4;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/2Gs;

.field private final e:LX/2H0;

.field private final f:LX/12x;

.field private final g:LX/0SG;

.field private final h:LX/2Ge;

.field private final i:LX/2H3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389816
    const-class v0, LX/2H4;

    sput-object v0, LX/2H4;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Gs;LX/12x;LX/0SG;LX/2Ge;LX/2H3;LX/2H0;)V
    .locals 0
    .param p6    # LX/2Ge;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/2H3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/2H0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389807
    iput-object p1, p0, LX/2H4;->b:Landroid/content/Context;

    .line 389808
    iput-object p2, p0, LX/2H4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 389809
    iput-object p3, p0, LX/2H4;->d:LX/2Gs;

    .line 389810
    iput-object p8, p0, LX/2H4;->e:LX/2H0;

    .line 389811
    iput-object p4, p0, LX/2H4;->f:LX/12x;

    .line 389812
    iput-object p5, p0, LX/2H4;->g:LX/0SG;

    .line 389813
    iput-object p6, p0, LX/2H4;->h:LX/2Ge;

    .line 389814
    iput-object p7, p0, LX/2H4;->i:LX/2H3;

    .line 389815
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    const-wide/32 v0, 0x1b7740

    .line 389799
    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    move-wide p1, v0

    .line 389800
    :cond_0
    iget-object v0, p0, LX/2H4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 389801
    iget-object v1, p0, LX/2H4;->i:LX/2H3;

    .line 389802
    iget-object v2, v1, LX/2H3;->e:LX/0Tn;

    move-object v1, v2

    .line 389803
    invoke-interface {v0, v1, p1, p2}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 389804
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 389805
    return-void
.end method

.method private e()J
    .locals 4

    .prologue
    .line 389796
    iget-object v0, p0, LX/2H4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/2H4;->i:LX/2H3;

    .line 389797
    iget-object v2, v1, LX/2H3;->e:LX/0Tn;

    move-object v1, v2

    .line 389798
    const-wide/16 v2, 0x7530

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static f(LX/2H4;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 389792
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2H4;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/push/registration/RegistrarHelperReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 389793
    const-string v1, "com.facebook.push.registration.ACTION_TOKEN_REQUEST_RETRY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 389794
    const-string v1, "serviceType"

    iget-object v2, p0, LX/2H4;->h:LX/2Ge;

    invoke-virtual {v2}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 389795
    iget-object v1, p0, LX/2H4;->b:Landroid/content/Context;

    const/4 v2, -0x1

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 389817
    iget-object v0, p0, LX/2H4;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xa4cb80

    add-long/2addr v0, v2

    .line 389818
    :try_start_0
    iget-object v2, p0, LX/2H4;->f:LX/12x;

    const/4 v3, 0x1

    invoke-static {p0}, LX/2H4;->f(LX/2H4;)Landroid/app/PendingIntent;

    move-result-object v4

    .line 389819
    invoke-virtual {v2, v3, v0, v1, v4}, LX/12x;->c(IJLandroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 389820
    :goto_0
    return-void

    .line 389821
    :catch_0
    move-exception v0

    .line 389822
    sget-object v1, LX/2H4;->a:Ljava/lang/Class;

    const-string v2, "Exception while scheduling an alarm"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(LX/2Gh;)V
    .locals 4

    .prologue
    .line 389785
    sget-object v0, LX/2gP;->INVALID_TOKEN:LX/2gP;

    invoke-virtual {v0}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 389786
    iget-object v0, p0, LX/2H4;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 389787
    iget-object v2, p0, LX/2H4;->e:LX/2H0;

    invoke-virtual {v2}, LX/2H0;->l()J

    move-result-wide v2

    .line 389788
    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 389789
    :goto_0
    return-void

    .line 389790
    :cond_0
    iget-object v0, p0, LX/2H4;->e:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->h()V

    .line 389791
    invoke-interface {p1}, LX/2Gh;->b()V

    goto :goto_0
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 6

    .prologue
    .line 389778
    invoke-direct {p0}, LX/2H4;->e()J

    move-result-wide v0

    .line 389779
    :try_start_0
    iget-object v2, p0, LX/2H4;->f:LX/12x;

    const/4 v3, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    add-long/2addr v4, v0

    .line 389780
    invoke-virtual {v2, v3, v4, v5, p1}, LX/12x;->c(IJLandroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 389781
    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, LX/2H4;->a(J)V

    .line 389782
    :goto_0
    return-void

    .line 389783
    :catch_0
    move-exception v0

    .line 389784
    sget-object v1, LX/2H4;->a:Ljava/lang/Class;

    const-string v2, "Exception while scheduling an alarm"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 389769
    iget-object v0, p0, LX/2H4;->d:LX/2Gs;

    iget-object v1, p0, LX/2H4;->h:LX/2Ge;

    invoke-virtual {v1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2H4;->e:LX/2H0;

    invoke-virtual {v2}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, LX/2H4;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move-object v5, p2

    .line 389770
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 389771
    const-string p1, "backoff"

    invoke-interface {p0, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389772
    const-string p1, "service_type"

    invoke-interface {p0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389773
    if-eqz v5, :cond_0

    .line 389774
    const-string p1, "reason"

    invoke-interface {p0, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389775
    :cond_0
    const-string p1, "push_reg_status"

    const-string p2, "registration_id"

    invoke-static {p1, v2, p0, p2, v3}, LX/2gN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 389776
    invoke-static {v0, p0}, LX/2Gs;->a(LX/2Gs;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 389777
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 389761
    iget-object v0, p0, LX/2H4;->d:LX/2Gs;

    iget-object v1, p0, LX/2H4;->h:LX/2Ge;

    invoke-virtual {v1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2H4;->e:LX/2H0;

    invoke-virtual {v2}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v2

    .line 389762
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 389763
    const-string v4, "push_source"

    sget-object p0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, p0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, v4, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389764
    if-eqz p2, :cond_0

    .line 389765
    const-string v4, "reason"

    invoke-interface {v3, v4, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389766
    :cond_0
    const-string v4, "push_unreg_status"

    const-string p0, "registration_id"

    invoke-static {v4, p1, v3, p0, v2}, LX/2gN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 389767
    invoke-static {v0, v3}, LX/2Gs;->a(LX/2Gs;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 389768
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 389759
    iget-object v0, p0, LX/2H4;->f:LX/12x;

    invoke-static {p0}, LX/2H4;->f(LX/2H4;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 389760
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 389757
    const-wide/16 v0, 0x7530

    invoke-direct {p0, v0, v1}, LX/2H4;->a(J)V

    .line 389758
    return-void
.end method
