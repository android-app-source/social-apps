.class public LX/2Ma;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Ma;


# instance fields
.field private final a:Landroid/webkit/MimeTypeMap;

.field private final b:LX/0Rh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rh",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396943
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v0

    iput-object v0, p0, LX/2Ma;->a:Landroid/webkit/MimeTypeMap;

    .line 396944
    const-string v0, "image/webp"

    const-string v1, "webp"

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    iput-object v0, p0, LX/2Ma;->b:LX/0Rh;

    .line 396945
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ma;
    .locals 3

    .prologue
    .line 396946
    sget-object v0, LX/2Ma;->c:LX/2Ma;

    if-nez v0, :cond_1

    .line 396947
    const-class v1, LX/2Ma;

    monitor-enter v1

    .line 396948
    :try_start_0
    sget-object v0, LX/2Ma;->c:LX/2Ma;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 396949
    if-eqz v2, :cond_0

    .line 396950
    :try_start_1
    new-instance v0, LX/2Ma;

    invoke-direct {v0}, LX/2Ma;-><init>()V

    .line 396951
    move-object v0, v0

    .line 396952
    sput-object v0, LX/2Ma;->c:LX/2Ma;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396953
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 396954
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 396955
    :cond_1
    sget-object v0, LX/2Ma;->c:LX/2Ma;

    return-object v0

    .line 396956
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 396957
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 396958
    iget-object v0, p0, LX/2Ma;->b:LX/0Rh;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 396959
    if-eqz v0, :cond_0

    .line 396960
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Ma;->a:Landroid/webkit/MimeTypeMap;

    invoke-virtual {v0, p1}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 396961
    iget-object v0, p0, LX/2Ma;->b:LX/0Rh;

    invoke-virtual {v0}, LX/0Rh;->e()LX/0Rh;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 396962
    if-eqz v0, :cond_0

    .line 396963
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Ma;->a:Landroid/webkit/MimeTypeMap;

    invoke-virtual {v0, p1}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
