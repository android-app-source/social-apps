.class public LX/2Ns;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0lB;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(LX/0lB;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 400107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400108
    iput-object p1, p0, LX/2Ns;->a:LX/0lB;

    .line 400109
    iput-object p2, p0, LX/2Ns;->b:LX/03V;

    .line 400110
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ns;
    .locals 1

    .prologue
    .line 400104
    invoke-static {p0}, LX/2Ns;->b(LX/0QB;)LX/2Ns;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2Ns;
    .locals 3

    .prologue
    .line 400105
    new-instance v2, LX/2Ns;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lB;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v0, v1}, LX/2Ns;-><init>(LX/0lB;LX/03V;)V

    .line 400106
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 400079
    if-nez p1, :cond_0

    move-object v0, v1

    .line 400080
    :goto_0
    return-object v0

    .line 400081
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/2Ns;->a:LX/0lB;

    const-class v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0, p1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/28E; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 400082
    :catch_0
    move-exception v0

    .line 400083
    :goto_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 400084
    :catch_1
    iget-object v0, p0, LX/2Ns;->b:LX/03V;

    const-string v2, "XMA"

    const-string v3, "IO Exception when reading XMA from JSON string."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 400085
    goto :goto_0

    .line 400086
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 400099
    if-nez p1, :cond_0

    .line 400100
    const/4 v0, 0x0

    .line 400101
    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, LX/2Ns;->a:LX/0lB;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 400102
    :catch_0
    move-exception v0

    .line 400103
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 400087
    if-nez p1, :cond_0

    move-object v0, v1

    .line 400088
    :goto_0
    return-object v0

    .line 400089
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/2Ns;->a:LX/0lB;

    .line 400090
    iget-object v2, v0, LX/0lC;->_typeFactory:LX/0li;

    move-object v0, v2

    .line 400091
    const-class v2, Ljava/util/HashMap;

    const-class v3, Ljava/lang/String;

    const-class v4, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0, v2, v3, v4}, LX/0li;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)LX/1Xn;

    move-result-object v0

    .line 400092
    iget-object v2, p0, LX/2Ns;->a:LX/0lB;

    invoke-virtual {v2, p1, v0}, LX/0lC;->a(Ljava/lang/String;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 400093
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/28E; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 400094
    :catch_0
    move-exception v0

    .line 400095
    :goto_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 400096
    :catch_1
    iget-object v0, p0, LX/2Ns;->b:LX/03V;

    const-string v2, "XMA"

    const-string v3, "IO Exception when reading XMA from JSON string."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 400097
    goto :goto_0

    .line 400098
    :catch_2
    move-exception v0

    goto :goto_1
.end method
