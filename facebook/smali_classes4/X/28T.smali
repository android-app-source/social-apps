.class public LX/28T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/41x;",
        "Lcom/facebook/auth/component/AuthenticationResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/2Xm;

.field private final b:LX/0hw;

.field private final c:LX/0dC;


# direct methods
.method public constructor <init>(LX/2Xm;LX/0hw;LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374129
    iput-object p1, p0, LX/28T;->a:LX/2Xm;

    .line 374130
    iput-object p2, p0, LX/28T;->b:LX/0hw;

    .line 374131
    iput-object p3, p0, LX/28T;->c:LX/0dC;

    .line 374132
    return-void
.end method

.method public static a(LX/0QB;)LX/28T;
    .locals 4

    .prologue
    .line 374125
    new-instance v3, LX/28T;

    invoke-static {p0}, LX/2Xm;->b(LX/0QB;)LX/2Xm;

    move-result-object v0

    check-cast v0, LX/2Xm;

    invoke-static {p0}, LX/0hw;->b(LX/0QB;)LX/0hw;

    move-result-object v1

    check-cast v1, LX/0hw;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v2

    check-cast v2, LX/0dC;

    invoke-direct {v3, v0, v1, v2}, LX/28T;-><init>(LX/2Xm;LX/0hw;LX/0dC;)V

    .line 374126
    move-object v0, v3

    .line 374127
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 374071
    check-cast p1, LX/41x;

    .line 374072
    iget-object v0, p1, LX/41x;->a:Lcom/facebook/auth/credentials/OpenIDLoginCredentials;

    .line 374073
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 374074
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "adid"

    iget-object v4, p0, LX/28T;->b:LX/0hw;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/0hw;->a(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374075
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374076
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "device_id"

    iget-object v4, p0, LX/28T;->c:LX/0dC;

    invoke-virtual {v4}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374077
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "email"

    .line 374078
    iget-object v4, v0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->a:Ljava/lang/String;

    move-object v4, v4

    .line 374079
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374080
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "password"

    const-string v4, "OPENID"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374081
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "openid_flow"

    const-string v4, "android_login"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374082
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "openid_provider"

    .line 374083
    iget-object v4, v0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->b:Lcom/facebook/openidconnect/model/OpenIDCredential;

    move-object v4, v4

    .line 374084
    iget-object v4, v4, Lcom/facebook/openidconnect/model/OpenIDCredential;->b:LX/4gy;

    iget-object v4, v4, LX/4gy;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374085
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "openid_token"

    .line 374086
    iget-object v4, v0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->b:Lcom/facebook/openidconnect/model/OpenIDCredential;

    move-object v4, v4

    .line 374087
    iget-object v4, v4, Lcom/facebook/openidconnect/model/OpenIDCredential;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374088
    iget-object v2, v0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->c:LX/41D;

    move-object v2, v2

    .line 374089
    invoke-virtual {v2}, LX/41D;->getServerValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 374090
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "credentials_type"

    .line 374091
    iget-object v4, v0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->c:LX/41D;

    move-object v0, v4

    .line 374092
    invoke-virtual {v0}, LX/41D;->getServerValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374093
    :cond_0
    iget-boolean v0, p1, LX/41x;->d:Z

    if-eqz v0, :cond_1

    .line 374094
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "generate_session_cookies"

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374095
    :cond_1
    iget-object v0, p1, LX/41x;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 374096
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "error_detail_type"

    iget-object v3, p1, LX/41x;->e:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374097
    :cond_2
    iget-object v0, p1, LX/41x;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 374098
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "machine_id"

    iget-object v3, p1, LX/41x;->b:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374099
    :goto_0
    iget-object v0, p1, LX/41x;->c:Landroid/location/Location;

    if-eqz v0, :cond_3

    .line 374100
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_latitude"

    iget-object v3, p1, LX/41x;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374101
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_longitude"

    iget-object v3, p1, LX/41x;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374102
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_location_accuracy_m"

    iget-object v3, p1, LX/41x;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374103
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "login_location_timestamp_ms"

    iget-object v3, p1, LX/41x;->c:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374104
    :cond_3
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    sget-object v2, LX/11I;->AUTHENTICATE:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 374105
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 374106
    move-object v0, v0

    .line 374107
    const-string v2, "POST"

    .line 374108
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 374109
    move-object v0, v0

    .line 374110
    const-string v2, "method/auth.login"

    .line 374111
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 374112
    move-object v0, v0

    .line 374113
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 374114
    move-object v0, v0

    .line 374115
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 374116
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 374117
    move-object v0, v0

    .line 374118
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 374119
    :cond_4
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "generate_machine_id"

    const-string v3, "1"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 374120
    check-cast p1, LX/41x;

    .line 374121
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 374122
    iget-object v0, p1, LX/41x;->a:Lcom/facebook/auth/credentials/OpenIDLoginCredentials;

    .line 374123
    iget-object v1, v0, Lcom/facebook/auth/credentials/OpenIDLoginCredentials;->a:Ljava/lang/String;

    move-object v0, v1

    .line 374124
    iget-object v1, p0, LX/28T;->a:LX/2Xm;

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v2

    iget-boolean v3, p1, LX/41x;->d:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2Xm;->a(LX/0lF;Ljava/lang/String;ZLjava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method
