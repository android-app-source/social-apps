.class public final LX/38H;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/os/Bundle;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/383;)V
    .locals 2

    .prologue
    .line 503095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 503096
    if-nez p1, :cond_0

    .line 503097
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "descriptor must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503098
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    iget-object v1, p1, LX/383;->a:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, LX/38H;->a:Landroid/os/Bundle;

    .line 503099
    invoke-static {p1}, LX/383;->p(LX/383;)V

    .line 503100
    iget-object v0, p1, LX/383;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 503101
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, LX/383;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/38H;->b:Ljava/util/ArrayList;

    .line 503102
    :cond_1
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 503090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 503091
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/38H;->a:Landroid/os/Bundle;

    .line 503092
    iget-object v0, p0, LX/38H;->a:Landroid/os/Bundle;

    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 503093
    iget-object v0, p0, LX/38H;->a:Landroid/os/Bundle;

    const-string v1, "name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 503094
    return-void
.end method

.method private a(Landroid/content/IntentFilter;)LX/38H;
    .locals 2

    .prologue
    .line 503077
    if-nez p1, :cond_0

    .line 503078
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "filter must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503079
    :cond_0
    iget-object v0, p0, LX/38H;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 503080
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/38H;->b:Ljava/util/ArrayList;

    .line 503081
    :cond_1
    iget-object v0, p0, LX/38H;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 503082
    iget-object v0, p0, LX/38H;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 503083
    :cond_2
    return-object p0
.end method


# virtual methods
.method public final a()LX/383;
    .locals 4

    .prologue
    .line 503074
    iget-object v0, p0, LX/38H;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 503075
    iget-object v0, p0, LX/38H;->a:Landroid/os/Bundle;

    const-string v1, "controlFilters"

    iget-object v2, p0, LX/38H;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 503076
    :cond_0
    new-instance v0, LX/383;

    iget-object v1, p0, LX/38H;->a:Landroid/os/Bundle;

    iget-object v2, p0, LX/38H;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, LX/383;-><init>(Landroid/os/Bundle;Ljava/util/List;)V

    return-object v0
.end method

.method public final a(I)LX/38H;
    .locals 2

    .prologue
    .line 503072
    iget-object v0, p0, LX/38H;->a:Landroid/os/Bundle;

    const-string v1, "playbackType"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 503073
    return-object p0
.end method

.method public final a(Ljava/util/Collection;)LX/38H;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/IntentFilter;",
            ">;)",
            "LX/38H;"
        }
    .end annotation

    .prologue
    .line 503084
    if-nez p1, :cond_0

    .line 503085
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "filters must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503086
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 503087
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    .line 503088
    invoke-direct {p0, v0}, LX/38H;->a(Landroid/content/IntentFilter;)LX/38H;

    goto :goto_0

    .line 503089
    :cond_1
    return-object p0
.end method

.method public final b(I)LX/38H;
    .locals 2

    .prologue
    .line 503070
    iget-object v0, p0, LX/38H;->a:Landroid/os/Bundle;

    const-string v1, "playbackStream"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 503071
    return-object p0
.end method

.method public final c(I)LX/38H;
    .locals 2

    .prologue
    .line 503068
    iget-object v0, p0, LX/38H;->a:Landroid/os/Bundle;

    const-string v1, "volume"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 503069
    return-object p0
.end method

.method public final d(I)LX/38H;
    .locals 2

    .prologue
    .line 503066
    iget-object v0, p0, LX/38H;->a:Landroid/os/Bundle;

    const-string v1, "volumeMax"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 503067
    return-object p0
.end method

.method public final e(I)LX/38H;
    .locals 2

    .prologue
    .line 503062
    iget-object v0, p0, LX/38H;->a:Landroid/os/Bundle;

    const-string v1, "volumeHandling"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 503063
    return-object p0
.end method

.method public final f(I)LX/38H;
    .locals 2

    .prologue
    .line 503064
    iget-object v0, p0, LX/38H;->a:Landroid/os/Bundle;

    const-string v1, "presentationDisplayId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 503065
    return-object p0
.end method
