.class public LX/2FJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2FJ;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4bv;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lorg/apache/http/conn/scheme/SocketFactory;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1Mg;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Or;LX/1Mg;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/SslSocketFactory;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/4bv;",
            ">;",
            "LX/0Or",
            "<",
            "Lorg/apache/http/conn/scheme/SocketFactory;",
            ">;",
            "Lcom/facebook/http/config/NetworkConfig;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 386799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386800
    iput-object p1, p0, LX/2FJ;->a:LX/0Ot;

    .line 386801
    iput-object p2, p0, LX/2FJ;->b:LX/0Or;

    .line 386802
    iput-object p3, p0, LX/2FJ;->c:LX/1Mg;

    .line 386803
    return-void
.end method

.method public static a(LX/0QB;)LX/2FJ;
    .locals 6

    .prologue
    .line 386804
    sget-object v0, LX/2FJ;->d:LX/2FJ;

    if-nez v0, :cond_1

    .line 386805
    const-class v1, LX/2FJ;

    monitor-enter v1

    .line 386806
    :try_start_0
    sget-object v0, LX/2FJ;->d:LX/2FJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 386807
    if-eqz v2, :cond_0

    .line 386808
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 386809
    new-instance v4, LX/2FJ;

    const/16 v3, 0x24f8

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x165b

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/1Mg;->a(LX/0QB;)LX/1Mg;

    move-result-object v3

    check-cast v3, LX/1Mg;

    invoke-direct {v4, v5, p0, v3}, LX/2FJ;-><init>(LX/0Ot;LX/0Or;LX/1Mg;)V

    .line 386810
    move-object v0, v4

    .line 386811
    sput-object v0, LX/2FJ;->d:LX/2FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 386812
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 386813
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 386814
    :cond_1
    sget-object v0, LX/2FJ;->d:LX/2FJ;

    return-object v0

    .line 386815
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 386816
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 386817
    iget-object v0, p0, LX/2FJ;->c:LX/1Mg;

    new-instance v1, LX/2FQ;

    invoke-direct {v1, p0}, LX/2FQ;-><init>(LX/2FJ;)V

    .line 386818
    iget-object v2, v0, LX/1Mg;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 386819
    iget-boolean v2, v0, LX/1Mg;->g:Z

    const/4 p0, 0x1

    if-ne v2, p0, :cond_0

    iget-boolean v2, v0, LX/1Mg;->h:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, LX/1Mg;->i:Z

    if-nez v2, :cond_0

    iget-object v2, v0, LX/1Mg;->j:Lorg/apache/http/HttpHost;

    sget-object p0, LX/1Mg;->e:Lorg/apache/http/HttpHost;

    invoke-static {v2, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 386820
    :cond_0
    invoke-virtual {v1}, LX/2FQ;->a()V

    .line 386821
    :cond_1
    return-void
.end method
