.class public LX/2DB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2DD;

.field private final b:LX/2DH;


# direct methods
.method public constructor <init>(LX/2DD;LX/2DH;)V
    .locals 0

    .prologue
    .line 383736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383737
    iput-object p1, p0, LX/2DB;->a:LX/2DD;

    .line 383738
    iput-object p2, p0, LX/2DB;->b:LX/2DH;

    .line 383739
    return-void
.end method


# virtual methods
.method public final a(LX/0nA;)V
    .locals 2

    .prologue
    .line 383744
    iget-object v0, p0, LX/2DB;->a:LX/2DD;

    invoke-virtual {v0, p1}, LX/2DD;->a(LX/0nA;)V

    .line 383745
    iget-object v0, p0, LX/2DB;->b:LX/2DH;

    iget-object v1, p0, LX/2DB;->a:LX/2DD;

    invoke-virtual {v1}, LX/2DD;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2DH;->a(Ljava/lang/Object;)V

    .line 383746
    iget-object v0, p0, LX/2DB;->b:LX/2DH;

    invoke-interface {v0}, LX/2DH;->a()V

    .line 383747
    iget-object v0, p0, LX/2DB;->a:LX/2DD;

    .line 383748
    iget-object v1, v0, LX/2DD;->b:LX/2Db;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/2DD;->b:LX/2Db;

    iget v1, v1, LX/2Db;->f:I

    iget p1, v0, LX/2DD;->d:I

    if-lt v1, p1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 383749
    if-eqz v0, :cond_0

    .line 383750
    iget-object v0, p0, LX/2DB;->b:LX/2DH;

    invoke-interface {v0}, LX/2DH;->b()V

    .line 383751
    :cond_0
    iget-object v0, p0, LX/2DB;->a:LX/2DD;

    .line 383752
    iget-object v1, v0, LX/2DD;->b:LX/2Db;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/2DD;->b:LX/2Db;

    iget v1, v1, LX/2Db;->f:I

    iget p1, v0, LX/2DD;->e:I

    if-lt v1, p1, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 383753
    if-eqz v0, :cond_1

    .line 383754
    iget-object v0, p0, LX/2DB;->a:LX/2DD;

    invoke-virtual {v0}, LX/2DD;->d()V

    .line 383755
    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(LX/2u6;)V
    .locals 1

    .prologue
    .line 383756
    iget-object v0, p0, LX/2DB;->a:LX/2DD;

    invoke-virtual {v0, p1}, LX/2DD;->a(LX/2u6;)V

    .line 383757
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 383742
    iget-object v0, p0, LX/2DB;->b:LX/2DH;

    invoke-interface {v0, p1}, LX/2DH;->a(Ljava/lang/String;)V

    .line 383743
    return-void
.end method

.method public final b(LX/2u6;)V
    .locals 1

    .prologue
    .line 383740
    iget-object v0, p0, LX/2DB;->a:LX/2DD;

    invoke-virtual {v0, p1}, LX/2DD;->a(LX/2u6;)V

    .line 383741
    return-void
.end method
