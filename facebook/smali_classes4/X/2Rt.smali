.class public LX/2Rt;
.super LX/2QZ;
.source ""


# instance fields
.field private b:LX/2QR;


# direct methods
.method public constructor <init>(LX/2QR;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410320
    const-string v0, "platform_cleanup_cached_webdialogs"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 410321
    iput-object p1, p0, LX/2Rt;->b:LX/2QR;

    .line 410322
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 410306
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v0

    .line 410307
    if-nez v2, :cond_0

    .line 410308
    sget-object v0, LX/1nY;->CACHE_DISK_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 410309
    :goto_0
    return-object v0

    .line 410310
    :cond_0
    const-string v0, "platform_urls_to_delete"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 410311
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 410312
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 410313
    iget-object v5, p0, LX/2Rt;->b:LX/2QR;

    invoke-virtual {v5, v0}, LX/2QR;->b(Ljava/lang/String;)Z

    .line 410314
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 410315
    :cond_1
    const-string v0, "delete_orphaned_files_flag"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 410316
    iget-object v0, p0, LX/2Rt;->b:LX/2QR;

    invoke-virtual {v0}, LX/2QR;->e()V

    .line 410317
    :cond_2
    iget-object v0, p0, LX/2Rt;->b:LX/2QR;

    invoke-virtual {v0}, LX/2QR;->f()V

    .line 410318
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 410319
    goto :goto_0
.end method
