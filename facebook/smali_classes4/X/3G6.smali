.class public LX/3G6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3G4;

.field public final b:LX/399;

.field public final c:LX/3Fz;

.field public final d:LX/37X;

.field private final e:Ljava/util/concurrent/locks/ReadWriteLock;

.field public final f:I

.field private final g:LX/4V2;


# direct methods
.method public constructor <init>(LX/3G4;LX/399;LX/3Fz;LX/37X;Ljava/util/concurrent/locks/ReadWriteLock;ILX/4V2;)V
    .locals 0

    .prologue
    .line 540450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540451
    iput-object p1, p0, LX/3G6;->a:LX/3G4;

    .line 540452
    iput-object p2, p0, LX/3G6;->b:LX/399;

    .line 540453
    iput-object p3, p0, LX/3G6;->c:LX/3Fz;

    .line 540454
    iput-object p4, p0, LX/3G6;->d:LX/37X;

    .line 540455
    iput-object p5, p0, LX/3G6;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 540456
    iput p6, p0, LX/3G6;->f:I

    .line 540457
    iput-object p7, p0, LX/3G6;->g:LX/4V2;

    .line 540458
    return-void
.end method


# virtual methods
.method public final a()LX/3G4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 540464
    iget-object v0, p0, LX/3G6;->a:LX/3G4;

    return-object v0
.end method

.method public final b()LX/399;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 540463
    iget-object v0, p0, LX/3G6;->b:LX/399;

    return-object v0
.end method

.method public final c()LX/3Fz;
    .locals 1

    .prologue
    .line 540462
    iget-object v0, p0, LX/3G6;->c:LX/3Fz;

    return-object v0
.end method

.method public final d()LX/37X;
    .locals 1

    .prologue
    .line 540465
    iget-object v0, p0, LX/3G6;->d:LX/37X;

    return-object v0
.end method

.method public final e()Ljava/util/concurrent/locks/ReadWriteLock;
    .locals 1

    .prologue
    .line 540461
    iget-object v0, p0, LX/3G6;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 540460
    iget v0, p0, LX/3G6;->f:I

    return v0
.end method

.method public final g()LX/4V2;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 540459
    iget-object v0, p0, LX/3G6;->g:LX/4V2;

    return-object v0
.end method
