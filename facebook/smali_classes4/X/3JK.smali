.class public LX/3JK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 547653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/InputStream;)LX/3K3;
    .locals 14

    .prologue
    .line 547654
    new-instance v0, Landroid/util/JsonReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 547655
    invoke-virtual {v0}, Landroid/util/JsonReader;->beginObject()V

    .line 547656
    new-instance v3, LX/3JL;

    invoke-direct {v3}, LX/3JL;-><init>()V

    .line 547657
    :goto_0
    invoke-virtual {v0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 547658
    invoke-virtual {v0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v4

    .line 547659
    const/4 v2, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 547660
    invoke-virtual {v0}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 547661
    :sswitch_0
    const-string v5, "frame_rate"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x0

    goto :goto_1

    :sswitch_1
    const-string v5, "animation_frame_count"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :sswitch_2
    const-string v5, "features"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x2

    goto :goto_1

    :sswitch_3
    const-string v5, "animation_groups"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x3

    goto :goto_1

    :sswitch_4
    const-string v5, "canvas_size"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x4

    goto :goto_1

    :sswitch_5
    const-string v5, "key"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x5

    goto :goto_1

    .line 547662
    :pswitch_0
    invoke-virtual {v0}, Landroid/util/JsonReader;->nextInt()I

    move-result v2

    iput v2, v3, LX/3JL;->a:I

    goto :goto_0

    .line 547663
    :pswitch_1
    invoke-virtual {v0}, Landroid/util/JsonReader;->nextInt()I

    move-result v2

    iput v2, v3, LX/3JL;->b:I

    goto :goto_0

    .line 547664
    :pswitch_2
    sget-object v2, LX/3Jn;->a:LX/3JO;

    invoke-virtual {v2, v0}, LX/3JO;->a(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v3, LX/3JL;->c:Ljava/util/List;

    goto :goto_0

    .line 547665
    :pswitch_3
    sget-object v2, LX/3JM;->a:LX/3JO;

    invoke-virtual {v2, v0}, LX/3JO;->a(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v3, LX/3JL;->d:Ljava/util/List;

    goto :goto_0

    .line 547666
    :pswitch_4
    invoke-static {v0}, LX/3JX;->a(Landroid/util/JsonReader;)[F

    move-result-object v2

    iput-object v2, v3, LX/3JL;->e:[F

    goto :goto_0

    .line 547667
    :pswitch_5
    invoke-virtual {v0}, Landroid/util/JsonReader;->nextInt()I

    move-result v2

    iput v2, v3, LX/3JL;->f:I

    goto/16 :goto_0

    .line 547668
    :cond_1
    invoke-virtual {v0}, Landroid/util/JsonReader;->endObject()V

    .line 547669
    new-instance v6, LX/3K3;

    iget v7, v3, LX/3JL;->a:I

    iget v8, v3, LX/3JL;->b:I

    iget-object v9, v3, LX/3JL;->c:Ljava/util/List;

    iget-object v10, v3, LX/3JL;->d:Ljava/util/List;

    iget-object v11, v3, LX/3JL;->e:[F

    iget v12, v3, LX/3JL;->f:I

    const/4 v13, 0x0

    invoke-direct/range {v6 .. v13}, LX/3K3;-><init>(IILjava/util/List;Ljava/util/List;[FIB)V

    move-object v2, v6

    .line 547670
    move-object v0, v2

    .line 547671
    return-object v0

    :sswitch_data_0
    .sparse-switch
        -0x6f9e4578 -> :sswitch_4
        -0x11531bc3 -> :sswitch_2
        -0x1020c78e -> :sswitch_0
        0x19e5f -> :sswitch_5
        0x1e797eef -> :sswitch_3
        0x4d912b82 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
