.class public final enum LX/2Zh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Zh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Zh;

.field public static final enum SINGLE_REQUEST:LX/2Zh;

.field public static final enum SINGLE_REQUEST_BY_KEY:LX/2Zh;

.field public static final enum UNRESTRICTED:LX/2Zh;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 422890
    new-instance v0, LX/2Zh;

    const-string v1, "UNRESTRICTED"

    invoke-direct {v0, v1, v2}, LX/2Zh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Zh;->UNRESTRICTED:LX/2Zh;

    .line 422891
    new-instance v0, LX/2Zh;

    const-string v1, "SINGLE_REQUEST_BY_KEY"

    invoke-direct {v0, v1, v3}, LX/2Zh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Zh;->SINGLE_REQUEST_BY_KEY:LX/2Zh;

    .line 422892
    new-instance v0, LX/2Zh;

    const-string v1, "SINGLE_REQUEST"

    invoke-direct {v0, v1, v4}, LX/2Zh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Zh;->SINGLE_REQUEST:LX/2Zh;

    .line 422893
    const/4 v0, 0x3

    new-array v0, v0, [LX/2Zh;

    sget-object v1, LX/2Zh;->UNRESTRICTED:LX/2Zh;

    aput-object v1, v0, v2

    sget-object v1, LX/2Zh;->SINGLE_REQUEST_BY_KEY:LX/2Zh;

    aput-object v1, v0, v3

    sget-object v1, LX/2Zh;->SINGLE_REQUEST:LX/2Zh;

    aput-object v1, v0, v4

    sput-object v0, LX/2Zh;->$VALUES:[LX/2Zh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 422894
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Zh;
    .locals 1

    .prologue
    .line 422895
    const-class v0, LX/2Zh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Zh;

    return-object v0
.end method

.method public static values()[LX/2Zh;
    .locals 1

    .prologue
    .line 422896
    sget-object v0, LX/2Zh;->$VALUES:[LX/2Zh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Zh;

    return-object v0
.end method
