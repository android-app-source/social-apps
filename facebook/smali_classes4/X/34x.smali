.class public LX/34x;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile f:LX/34x;


# instance fields
.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Ljava/util/regex/Pattern;

.field private e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 496227
    const-class v0, LX/34x;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/34x;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 496188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496189
    const/4 v0, -0x1

    iput v0, p0, LX/34x;->e:I

    .line 496190
    return-void
.end method

.method public static a(LX/0QB;)LX/34x;
    .locals 5

    .prologue
    .line 496191
    sget-object v0, LX/34x;->f:LX/34x;

    if-nez v0, :cond_1

    .line 496192
    const-class v1, LX/34x;

    monitor-enter v1

    .line 496193
    :try_start_0
    sget-object v0, LX/34x;->f:LX/34x;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 496194
    if-eqz v2, :cond_0

    .line 496195
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 496196
    new-instance p0, LX/34x;

    invoke-direct {p0}, LX/34x;-><init>()V

    .line 496197
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    .line 496198
    iput-object v3, p0, LX/34x;->b:LX/03V;

    iput-object v4, p0, LX/34x;->c:LX/0W3;

    .line 496199
    move-object v0, p0

    .line 496200
    sput-object v0, LX/34x;->f:LX/34x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496201
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 496202
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 496203
    :cond_1
    sget-object v0, LX/34x;->f:LX/34x;

    return-object v0

    .line 496204
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 496205
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 496206
    iget-object v0, p0, LX/34x;->c:LX/0W3;

    sget-wide v2, LX/0X5;->dF:J

    invoke-interface {v0, v2, v3}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v0

    .line 496207
    iget-object v1, p0, LX/34x;->c:LX/0W3;

    sget-wide v2, LX/0X5;->dG:J

    invoke-interface {v1, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p0, LX/34x;->e:I

    .line 496208
    if-eqz v0, :cond_0

    .line 496209
    :try_start_0
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/34x;->d:Ljava/util/regex/Pattern;
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 496210
    :goto_0
    return-void

    .line 496211
    :catch_0
    move-exception v0

    .line 496212
    iput-object v5, p0, LX/34x;->d:Ljava/util/regex/Pattern;

    .line 496213
    iput v4, p0, LX/34x;->e:I

    .line 496214
    iget-object v1, p0, LX/34x;->b:LX/03V;

    sget-object v2, LX/34x;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/regex/PatternSyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 496215
    :cond_0
    iput-object v5, p0, LX/34x;->d:Ljava/util/regex/Pattern;

    .line 496216
    iput v4, p0, LX/34x;->e:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 496217
    iget-object v0, p0, LX/34x;->c:LX/0W3;

    sget-wide v2, LX/0X5;->dK:J

    invoke-interface {v0, v2, v3}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v0

    .line 496218
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 496219
    :goto_0
    return-object p1

    :cond_0
    move-object p1, v0

    goto :goto_0
.end method

.method public final declared-synchronized a()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 496220
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/34x;->d:Ljava/util/regex/Pattern;

    if-nez v0, :cond_0

    .line 496221
    invoke-direct {p0}, LX/34x;->g()V

    .line 496222
    :cond_0
    iget-object v0, p0, LX/34x;->d:Ljava/util/regex/Pattern;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 496223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 496224
    iget v0, p0, LX/34x;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 496225
    invoke-direct {p0}, LX/34x;->g()V

    .line 496226
    :cond_0
    iget v0, p0, LX/34x;->e:I

    return v0
.end method
