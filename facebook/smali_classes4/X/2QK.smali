.class public LX/2QK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Landroid/content/pm/Signature;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Xu;Landroid/content/pm/PackageManager;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Xu",
            "<",
            "Landroid/content/pm/Signature;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/pm/PackageManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 407857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407858
    iput-object p1, p0, LX/2QK;->a:Ljava/lang/String;

    .line 407859
    iput-object p2, p0, LX/2QK;->b:LX/0Xu;

    .line 407860
    iput-object p3, p0, LX/2QK;->c:Landroid/content/pm/PackageManager;

    .line 407861
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 407862
    :try_start_0
    iget-object v0, p0, LX/2QK;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, LX/2QK;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 407863
    iget v1, v0, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    and-int/lit8 v1, v1, 0xf

    if-eq v1, v7, :cond_0

    .line 407864
    const-string v0, "Access denied: permission \'%s\' is not of signature protection level."

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, LX/2QK;->a:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v6, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 407865
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 407866
    :catch_0
    const-string v0, "Access denied: \'%s\' permission is missing."

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, LX/2QK;->a:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v6, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 407867
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 407868
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/2QK;->c:Landroid/content/pm/PackageManager;

    iget-object v2, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    const/16 v3, 0x40

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 407869
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v2, :cond_1

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v2, v2

    if-nez v2, :cond_2

    .line 407870
    :cond_1
    const-string v1, "Access denied: permission \'%s\' owner package \'%s\' has no signatures."

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, LX/2QK;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-static {v6, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 407871
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 407872
    :catch_1
    const-string v1, "Access denied: permission \'%s\' is declared in \'%s\' package which is missing."

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, LX/2QK;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-static {v6, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 407873
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 407874
    :cond_2
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v2, v2

    if-le v2, v5, :cond_3

    .line 407875
    const-string v1, "Access denied: permission \'%s\' owner package \'%s\' has multiple signatures."

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, LX/2QK;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-static {v6, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 407876
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 407877
    :cond_3
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v1, v1, v4

    .line 407878
    iget-object v2, p0, LX/2QK;->b:LX/0Xu;

    iget-object v3, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v1, v3}, LX/0Xu;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, LX/2QK;->b:LX/0Xu;

    const-string v3, "*|all_packages|*"

    invoke-interface {v2, v1, v3}, LX/0Xu;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 407879
    const-string v1, "Access denied: permission \'%s\' is declared by an untrusted package \'%s\'."

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, LX/2QK;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-static {v6, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 407880
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 407881
    :cond_4
    return-void
.end method
