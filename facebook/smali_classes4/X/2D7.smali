.class public abstract LX/2D7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<TT;",
            "LX/2DZ;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 383703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383704
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2D7;->a:Ljava/util/HashMap;

    .line 383705
    return-void
.end method

.method public static a(Z)LX/2D7;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/2D7",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 383693
    if-eqz p0, :cond_0

    .line 383694
    invoke-static {}, LX/3zw;->a()LX/2D7;

    move-result-object v0

    .line 383695
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/2D8;->a()LX/2D7;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/Object;)LX/2DZ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "LX/2DZ;"
        }
    .end annotation

    .prologue
    .line 383696
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2D7;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2DZ;

    .line 383697
    if-nez v0, :cond_0

    .line 383698
    invoke-virtual {p0, p1}, LX/2D7;->b(Ljava/lang/Object;)LX/2DZ;

    move-result-object v0

    .line 383699
    iget-object v1, p0, LX/2D7;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383700
    :cond_0
    invoke-virtual {v0}, LX/2DZ;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383701
    monitor-exit p0

    return-object v0

    .line 383702
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract b(Ljava/lang/Object;)LX/2DZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "LX/2DZ;"
        }
    .end annotation
.end method
