.class public LX/2EH;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/F9o;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0TD;

.field public final e:LX/0lB;

.field public final f:LX/2Ne;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 385123
    const-class v0, LX/2EH;

    sput-object v0, LX/2EH;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/F9o;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0TD;LX/0lB;LX/2Ne;)V
    .locals 0
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 385124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385125
    iput-object p1, p0, LX/2EH;->b:LX/F9o;

    .line 385126
    iput-object p2, p0, LX/2EH;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 385127
    iput-object p3, p0, LX/2EH;->d:LX/0TD;

    .line 385128
    iput-object p4, p0, LX/2EH;->e:LX/0lB;

    .line 385129
    iput-object p5, p0, LX/2EH;->f:LX/2Ne;

    .line 385130
    return-void
.end method
