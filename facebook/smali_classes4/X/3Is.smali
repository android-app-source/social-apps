.class public final LX/3Is;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3It;


# instance fields
.field public final synthetic a:LX/2oL;

.field public final synthetic b:LX/1Pe;

.field public final synthetic c:LX/3EE;

.field public final synthetic d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;LX/2oL;LX/1Pe;LX/3EE;)V
    .locals 0

    .prologue
    .line 547261
    iput-object p1, p0, LX/3Is;->d:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    iput-object p2, p0, LX/3Is;->a:LX/2oL;

    iput-object p3, p0, LX/3Is;->b:LX/1Pe;

    iput-object p4, p0, LX/3Is;->c:LX/3EE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 547246
    return-void
.end method

.method public final a(LX/2op;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 547253
    iget-object v0, p0, LX/3Is;->a:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->b()LX/2oO;

    move-result-object v0

    .line 547254
    if-eqz v0, :cond_0

    .line 547255
    invoke-virtual {v0}, LX/2oO;->h()V

    .line 547256
    :cond_0
    iget-object v0, p0, LX/3Is;->a:LX/2oL;

    invoke-virtual {v0, v3}, LX/2oL;->a(I)V

    .line 547257
    iget-object v0, p0, LX/3Is;->a:LX/2oL;

    .line 547258
    iput-boolean v1, v0, LX/2oL;->a:Z

    .line 547259
    iget-object v0, p0, LX/3Is;->b:LX/1Pe;

    check-cast v0, LX/1Pq;

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/3Is;->c:LX/3EE;

    iget-object v2, v2, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 547260
    return-void
.end method

.method public final a(LX/2oq;)V
    .locals 0

    .prologue
    .line 547252
    return-void
.end method

.method public final a(LX/2or;)V
    .locals 2

    .prologue
    .line 547247
    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->ERROR_IO:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->PLAYBACK_EXCEPTION:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->SERVER_DIED:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->UNSUPPORTED:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->DISMISS:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547248
    :cond_0
    iget-object v0, p0, LX/3Is;->a:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->b()LX/2oO;

    move-result-object v0

    .line 547249
    if-eqz v0, :cond_1

    .line 547250
    invoke-virtual {v0}, LX/2oO;->j()V

    .line 547251
    :cond_1
    return-void
.end method
