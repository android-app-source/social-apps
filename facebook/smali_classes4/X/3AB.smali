.class public LX/3AB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field private final a:Z

.field public final b:LX/3AC;

.field public final c:LX/3AD;

.field public final d:LX/3AE;

.field private final e:LX/3AF;

.field public final f:LX/17W;

.field public final g:Landroid/content/Context;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/3AC;LX/3AD;LX/3AE;LX/3AF;LX/17W;Landroid/content/Context;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsSeeTranslationMenuEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/3AC;",
            "LX/3AD;",
            "LX/3AE;",
            "LX/3AF;",
            "LX/17W;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 524856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524857
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/3AB;->a:Z

    .line 524858
    iput-object p2, p0, LX/3AB;->b:LX/3AC;

    .line 524859
    iput-object p3, p0, LX/3AB;->c:LX/3AD;

    .line 524860
    iput-object p4, p0, LX/3AB;->d:LX/3AE;

    .line 524861
    iput-object p5, p0, LX/3AB;->e:LX/3AF;

    .line 524862
    iput-object p6, p0, LX/3AB;->f:LX/17W;

    .line 524863
    iput-object p7, p0, LX/3AB;->g:Landroid/content/Context;

    .line 524864
    iget-object v0, p0, LX/3AB;->g:Landroid/content/Context;

    const v1, 0x7f081110

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/3AB;->h:LX/0Ot;

    .line 524865
    iget-object v0, p0, LX/3AB;->g:Landroid/content/Context;

    const v1, 0x7f081111

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/3AB;->i:LX/0Ot;

    .line 524866
    iget-object v0, p0, LX/3AB;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/res/Resources;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/3AB;->j:LX/0Ot;

    .line 524867
    iget-object v0, p0, LX/3AB;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0920

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/res/Resources;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/3AB;->k:LX/0Ot;

    .line 524868
    return-void
.end method

.method public static a(LX/1De;Ljava/lang/CharSequence;II)LX/1Di;
    .locals 3
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 524869
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1ne;->p(I)LX/1ne;

    move-result-object v0

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-virtual {v0, v1}, LX/1ne;->i(F)LX/1ne;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a0159

    invoke-virtual {v0, v1}, LX/1ne;->v(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x5

    const v2, 0x7f0b0917

    invoke-interface {v0, v1, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/3AB;LX/1De;Ljava/lang/CharSequence;I)LX/1Di;
    .locals 1

    .prologue
    .line 524870
    iget-object v0, p0, LX/3AB;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    invoke-static {p1, p2, p3, v0}, LX/3AB;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/3AB;
    .locals 11

    .prologue
    .line 524871
    const-class v1, LX/3AB;

    monitor-enter v1

    .line 524872
    :try_start_0
    sget-object v0, LX/3AB;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 524873
    sput-object v2, LX/3AB;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 524874
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524875
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 524876
    new-instance v3, LX/3AB;

    const/16 v4, 0x14a4

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    .line 524877
    new-instance v7, LX/3AC;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-direct {v7, v5, v6}, LX/3AC;-><init>(LX/0tX;LX/0Sh;)V

    .line 524878
    move-object v5, v7

    .line 524879
    check-cast v5, LX/3AC;

    invoke-static {v0}, LX/3AD;->b(LX/0QB;)LX/3AD;

    move-result-object v6

    check-cast v6, LX/3AD;

    invoke-static {v0}, LX/3AE;->b(LX/0QB;)LX/3AE;

    move-result-object v7

    check-cast v7, LX/3AE;

    invoke-static {v0}, LX/3AF;->a(LX/0QB;)LX/3AF;

    move-result-object v8

    check-cast v8, LX/3AF;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v9

    check-cast v9, LX/17W;

    const-class v10, Landroid/content/Context;

    invoke-interface {v0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-direct/range {v3 .. v10}, LX/3AB;-><init>(LX/0Or;LX/3AC;LX/3AD;LX/3AE;LX/3AF;LX/17W;Landroid/content/Context;)V

    .line 524880
    move-object v0, v3

    .line 524881
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 524882
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3AB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 524883
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 524884
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;LX/1np;)LX/1Dg;
    .locals 8
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Ps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "LX/1np",
            "<",
            "LX/3AH;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x2

    .line 524885
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 524886
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 524887
    check-cast p3, LX/1Pr;

    new-instance v1, LX/3AG;

    invoke-direct {v1, v0}, LX/3AG;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3AH;

    .line 524888
    iput-object v1, p4, LX/1np;->a:Ljava/lang/Object;

    .line 524889
    invoke-virtual {v1}, LX/3AH;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v4

    .line 524890
    invoke-virtual {v1}, LX/3AH;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 524891
    iget-object v1, p0, LX/3AB;->e:LX/3AF;

    .line 524892
    invoke-static {v4}, LX/9JZ;->a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 524893
    invoke-virtual {v1, v5}, LX/3AF;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    move-object v1, v5

    .line 524894
    iget-object v4, p0, LX/3AB;->e:LX/3AF;

    invoke-virtual {v4, v0}, LX/3AF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    .line 524895
    iget-boolean v5, p0, LX/3AB;->a:Z

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    .line 524896
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v6

    if-nez v6, :cond_7

    .line 524897
    :cond_0
    :goto_0
    move v0, v5

    .line 524898
    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    .line 524899
    :goto_1
    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    const/4 p4, 0x2

    const/4 p3, 0x1

    .line 524900
    const v3, 0x7f0a0427

    iget-object v2, p0, LX/3AB;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->intValue()I

    move-result v2

    invoke-static {p1, v4, v3, v2}, LX/3AB;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1Di;

    move-result-object v2

    .line 524901
    if-eqz v0, :cond_2

    .line 524902
    const v3, 0x44c5ee4

    const/4 v5, 0x0

    invoke-static {p1, v3, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 524903
    invoke-interface {v2, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    .line 524904
    :cond_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const v6, 0x7f0a0042

    invoke-virtual {v5, v6}, LX/25Q;->i(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, p3}, LX/1Di;->j(I)LX/1Di;

    move-result-object v5

    const/4 v6, 0x5

    const/4 p2, 0x6

    invoke-interface {v5, v6, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x4

    invoke-interface {v5, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x3

    invoke-interface {v5, v6, p4}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v5

    .line 524905
    const v6, 0x7f0a0158

    invoke-static {p0, p1, v1, v6}, LX/3AB;->a(LX/3AB;LX/1De;Ljava/lang/CharSequence;I)LX/1Di;

    move-result-object v6

    move-object v6, v6

    .line 524906
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 524907
    :goto_3
    return-object v0

    :cond_3
    move v0, v3

    .line 524908
    goto/16 :goto_1

    :cond_4
    move v0, v3

    .line 524909
    goto/16 :goto_2

    .line 524910
    :cond_5
    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v3

    iget-object v4, p0, LX/3AB;->g:Landroid/content/Context;

    iget-object v0, p0, LX/3AB;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, LX/3AB;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 524911
    new-instance v5, LX/1zO;

    invoke-direct {v5, v4}, LX/1zO;-><init>(Landroid/content/Context;)V

    .line 524912
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne v3, v7, :cond_8

    .line 524913
    :goto_4
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v7

    .line 524914
    const/4 p2, 0x0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result p3

    const/16 p4, 0x21

    invoke-interface {v7, v5, p2, p3, p4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 524915
    move-object v0, v7

    .line 524916
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a0042

    invoke-static {p0, p1, v0, v3}, LX/3AB;->a(LX/3AB;LX/1De;Ljava/lang/CharSequence;I)LX/1Di;

    move-result-object v0

    .line 524917
    const v3, 0x5a8b92f

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 524918
    invoke-interface {v0, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 524919
    iget-boolean v0, v1, LX/3AH;->b:Z

    move v0, v0

    .line 524920
    if-eqz v0, :cond_6

    invoke-static {p1}, LX/5Jt;->c(LX/1De;)LX/5Jr;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    iget-object v0, p0, LX/3AB;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    iget-object v0, p0, LX/3AB;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    :goto_5
    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b0917

    invoke-interface {v0, v6, v1}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x5

    const v2, 0x7f0b0917

    invoke-interface {v0, v1, v2}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_3

    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    :cond_7
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->n()Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v6

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne v6, p2, :cond_0

    const/4 v5, 0x1

    goto/16 :goto_0

    :cond_8
    move-object v0, v2

    .line 524921
    goto/16 :goto_4
.end method
