.class public final synthetic LX/2Vm;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 418074
    invoke-static {}, LX/14P;->values()[LX/14P;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/2Vm;->b:[I

    :try_start_0
    sget-object v0, LX/2Vm;->b:[I

    sget-object v1, LX/14P;->CONSERVATIVE:LX/14P;

    invoke-virtual {v1}, LX/14P;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    :try_start_1
    sget-object v0, LX/2Vm;->b:[I

    sget-object v1, LX/14P;->RETRY_SAFE:LX/14P;

    invoke-virtual {v1}, LX/14P;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    .line 418075
    :goto_1
    invoke-static {}, LX/14V;->values()[LX/14V;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/2Vm;->a:[I

    :try_start_2
    sget-object v0, LX/2Vm;->a:[I

    sget-object v1, LX/14V;->PROD:LX/14V;

    invoke-virtual {v1}, LX/14V;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    sget-object v0, LX/2Vm;->a:[I

    sget-object v1, LX/14V;->BOOTSTRAP:LX/14V;

    invoke-virtual {v1}, LX/14V;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    goto :goto_3

    :catch_1
    goto :goto_2

    :catch_2
    goto :goto_1

    :catch_3
    goto :goto_0
.end method
