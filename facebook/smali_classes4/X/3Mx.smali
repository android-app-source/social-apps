.class public LX/3Mx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/03V;

.field private final b:LX/0Zb;

.field public final c:LX/0WJ;

.field public final d:LX/0Uo;


# direct methods
.method public constructor <init>(LX/03V;LX/0Zb;LX/0WJ;LX/0Uo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 556988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 556989
    iput-object p1, p0, LX/3Mx;->a:LX/03V;

    .line 556990
    iput-object p2, p0, LX/3Mx;->b:LX/0Zb;

    .line 556991
    iput-object p3, p0, LX/3Mx;->c:LX/0WJ;

    .line 556992
    iput-object p4, p0, LX/3Mx;->d:LX/0Uo;

    .line 556993
    return-void
.end method

.method public static a(LX/0QB;)LX/3Mx;
    .locals 1

    .prologue
    .line 556987
    invoke-static {p0}, LX/3Mx;->b(LX/0QB;)LX/3Mx;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/3Mx;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 556979
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "type"

    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "reason"

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "ThreadsModel"

    .line 556980
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 556981
    move-object v2, v0

    .line 556982
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 556983
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "extra_"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_1

    .line 556984
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 556985
    :cond_1
    iget-object v0, p0, LX/3Mx;->b:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 556986
    return-void
.end method

.method public static b(LX/0QB;)LX/3Mx;
    .locals 5

    .prologue
    .line 556977
    new-instance v4, LX/3Mx;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v2

    check-cast v2, LX/0WJ;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v3

    check-cast v3, LX/0Uo;

    invoke-direct {v4, v0, v1, v2, v3}, LX/3Mx;-><init>(LX/03V;LX/0Zb;LX/0WJ;LX/0Uo;)V

    .line 556978
    return-object v4
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 556975
    iget-object v0, p0, LX/3Mx;->a:LX/03V;

    const-string v1, "graphql_type_unsupported"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to support graphql message of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 556976
    return-void
.end method

.method public final a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 556973
    iget-object v0, p0, LX/3Mx;->a:LX/03V;

    const-string v1, "xma_handling_failed"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to handle XMA with id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 556974
    return-void
.end method

.method public final a(Ljava/lang/Long;)V
    .locals 4
    .param p1    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 556952
    iget-object v0, p0, LX/3Mx;->a:LX/03V;

    const-string v1, "threads_fetch_no_folder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected no-folder thread. Id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 556953
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 556970
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 556971
    iget-object v0, p0, LX/3Mx;->a:LX/03V;

    const-string v1, "gql_threads_null"

    invoke-static {v1, p1}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 556972
    return-void
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 556962
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 556963
    const-string v1, "gql_threads_null"

    .line 556964
    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p2, v0

    .line 556965
    if-nez v3, :cond_0

    .line 556966
    iget-object v0, p0, LX/3Mx;->a:LX/03V;

    invoke-static {v1, p1}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/03V;->a(LX/0VG;)V

    .line 556967
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 556968
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 556969
    :cond_1
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 556960
    iget-object v0, p0, LX/3Mx;->a:LX/03V;

    const-string v1, "graphql_type_unsupported"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to support graphql attachment of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 556961
    return-void
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 556958
    const-string v0, "failed_fetch_thread_list_communication"

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {p0, v0, p1, v1}, LX/3Mx;->a(LX/3Mx;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 556959
    return-void
.end method

.method public final d(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 556956
    const-string v0, "failed_fetch_more_threads_communication"

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {p0, v0, p1, v1}, LX/3Mx;->a(LX/3Mx;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 556957
    return-void
.end method

.method public final g(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 556954
    iget-object v0, p0, LX/3Mx;->a:LX/03V;

    const-string v1, "failed_fetch_threads"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 556955
    return-void
.end method
