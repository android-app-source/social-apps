.class public LX/33I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ad;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0X0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0X0",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/33H;

.field private final d:LX/0W3;

.field private final e:Z

.field public f:LX/0ad;

.field public final g:LX/0Wx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 492364
    const-class v0, LX/33I;

    sput-object v0, LX/33I;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0W3;LX/33H;LX/0X0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0W3;",
            "LX/33H;",
            "LX/0X0",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 492365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 492366
    iput-object p1, p0, LX/33I;->d:LX/0W3;

    .line 492367
    iput-object p2, p0, LX/33I;->c:LX/33H;

    .line 492368
    iget-object v0, p0, LX/33I;->c:LX/33H;

    .line 492369
    iget-boolean v1, v0, LX/33H;->c:Z

    move v0, v1

    .line 492370
    iput-boolean v0, p0, LX/33I;->e:Z

    .line 492371
    iget-object v0, p1, LX/0W3;->a:LX/0Wx;

    move-object v0, v0

    .line 492372
    iput-object v0, p0, LX/33I;->g:LX/0Wx;

    .line 492373
    iput-object p3, p0, LX/33I;->a:LX/0X0;

    .line 492374
    iget-boolean v0, p2, LX/33H;->c:Z

    move v0, v0

    .line 492375
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 492376
    return-void
.end method

.method private static a(LX/33I;LX/0c0;LX/0c1;DLjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(",
            "LX/0c0;",
            "LX/0c1;",
            "D",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 492377
    invoke-static {p3, p4}, LX/0c2;->a(D)Z

    move-result v0

    .line 492378
    if-eqz v0, :cond_1

    .line 492379
    :cond_0
    :goto_0
    return-object p6

    .line 492380
    :cond_1
    iget-boolean v0, p0, LX/33I;->e:Z

    if-eqz v0, :cond_0

    .line 492381
    invoke-static {p3, p4}, LX/0c2;->b(D)I

    move-result v0

    .line 492382
    iget-object v1, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v1, v0}, LX/33H;->a(I)J

    move-result-wide v2

    .line 492383
    invoke-static {v2, v3}, LX/0X6;->a(J)I

    move-result v0

    .line 492384
    sget-object v1, LX/0c0;->Cached:LX/0c0;

    if-ne p1, v1, :cond_3

    .line 492385
    iget-object v1, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v1, v0}, LX/0W3;->a(I)LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    .line 492386
    :goto_1
    sget-object v1, LX/0c1;->On:LX/0c1;

    if-ne p2, v1, :cond_2

    .line 492387
    const-string v1, "auto"

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v1, v4}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 492388
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, LX/0X7;->b(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 492389
    invoke-virtual {p5}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    .line 492390
    const/4 v1, 0x0

    array-length v4, v0

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_0

    .line 492391
    aget-object v1, v0, v2

    .line 492392
    invoke-virtual {v1}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move-object p6, v1

    .line 492393
    goto :goto_0

    .line 492394
    :cond_3
    iget-object v0, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    goto :goto_1

    .line 492395
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2
.end method

.method private a(LX/0c0;J)V
    .locals 4

    .prologue
    .line 492396
    invoke-static {p2, p3}, LX/0X6;->a(J)I

    move-result v0

    .line 492397
    sget-object v1, LX/0c0;->Cached:LX/0c0;

    if-ne p1, v1, :cond_0

    .line 492398
    iget-object v1, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v1, v0}, LX/0W3;->a(I)LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    .line 492399
    :goto_0
    const-string v1, "man"

    const/4 v2, 0x1

    invoke-virtual {v0, p2, p3, v1, v2}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 492400
    return-void

    .line 492401
    :cond_0
    iget-object v0, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    goto :goto_0
.end method

.method private static c(LX/33I;IJ)V
    .locals 12

    .prologue
    .line 492402
    shl-int/lit8 v2, p1, 0x1

    goto/16 :goto_2

    :goto_0
    or-int/2addr v1, v2

    move v0, v1

    .line 492403
    const/4 v1, 0x0

    invoke-static {p1, v1}, LX/0c2;->d(IZ)J

    move-result-wide v4

    .line 492404
    iget-object v1, p0, LX/33I;->f:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    const/16 v6, -0x3e7

    invoke-interface {v1, v2, v3, v0, v6}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    int-to-long v8, v0

    .line 492405
    iget-object v1, p0, LX/33I;->f:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    const-wide/16 v6, -0x3e7

    invoke-interface/range {v1 .. v7}, LX/0ad;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v10

    .line 492406
    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    const-wide/16 v6, 0x0

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, LX/33I;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v4

    .line 492407
    cmp-long v0, v8, v4

    if-eqz v0, :cond_0

    cmp-long v0, v10, v4

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    move v2, v0

    .line 492408
    :goto_1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x2

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x3

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v1

    .line 492409
    iget-object v0, p0, LX/33I;->g:LX/0Wx;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2, p3}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "|"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, p3}, LX/0X6;->a(J)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p2, p3}, LX/0X6;->b(J)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "|"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 492410
    return-void

    .line 492411
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1

    :goto_2
    const/4 v1, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(FF)F
    .locals 2

    .prologue
    .line 492412
    sget-object v0, LX/0c0;->Cached:LX/0c0;

    sget-object v1, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/33I;->a(LX/0c0;LX/0c1;FF)F

    move-result v0

    return v0
.end method

.method public final a(LX/0c0;LX/0c1;FF)F
    .locals 6

    .prologue
    .line 492413
    invoke-static {p3}, LX/0c2;->a(F)Z

    move-result v0

    .line 492414
    if-eqz v0, :cond_1

    .line 492415
    :cond_0
    :goto_0
    return p4

    .line 492416
    :cond_1
    iget-boolean v0, p0, LX/33I;->e:Z

    if-eqz v0, :cond_0

    .line 492417
    invoke-static {p3}, LX/0c2;->b(F)I

    move-result v0

    .line 492418
    iget-object v1, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v1, v0}, LX/33H;->a(I)J

    move-result-wide v2

    .line 492419
    invoke-static {v2, v3}, LX/0X6;->a(J)I

    move-result v0

    .line 492420
    sget-object v1, LX/0c0;->Cached:LX/0c0;

    if-ne p1, v1, :cond_3

    .line 492421
    iget-object v1, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v1, v0}, LX/0W3;->a(I)LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    .line 492422
    :goto_1
    sget-object v1, LX/0c1;->On:LX/0c1;

    if-ne p2, v1, :cond_2

    .line 492423
    const-string v1, "auto"

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v1, v4}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 492424
    :cond_2
    float-to-double v4, p4

    invoke-virtual {v0, v2, v3, v4, v5}, LX/0X7;->c(JD)D

    move-result-wide v0

    double-to-float p4, v0

    goto :goto_0

    .line 492425
    :cond_3
    iget-object v0, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    goto :goto_1
.end method

.method public final a(II)I
    .locals 2

    .prologue
    .line 492459
    sget-object v0, LX/0c0;->Cached:LX/0c0;

    sget-object v1, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/33I;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    return v0
.end method

.method public final a(LX/0c0;II)I
    .locals 1

    .prologue
    .line 492426
    sget-object v0, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, p1, v0, p2, p3}, LX/33I;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    return v0
.end method

.method public final a(LX/0c0;LX/0c1;II)I
    .locals 5

    .prologue
    .line 492427
    invoke-static {p3}, LX/0c2;->a(I)Z

    move-result v0

    .line 492428
    if-eqz v0, :cond_1

    .line 492429
    :cond_0
    :goto_0
    return p4

    .line 492430
    :cond_1
    iget-boolean v0, p0, LX/33I;->e:Z

    if-eqz v0, :cond_0

    .line 492431
    shr-int/lit8 v0, p3, 0x1

    move v0, v0

    .line 492432
    iget-object v1, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v1, v0}, LX/33H;->a(I)J

    move-result-wide v2

    .line 492433
    invoke-static {v2, v3}, LX/0X6;->a(J)I

    move-result v0

    .line 492434
    sget-object v1, LX/0c0;->Cached:LX/0c0;

    if-ne p1, v1, :cond_3

    .line 492435
    iget-object v1, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v1, v0}, LX/0W3;->a(I)LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    .line 492436
    :goto_1
    sget-object v1, LX/0c1;->On:LX/0c1;

    if-ne p2, v1, :cond_2

    .line 492437
    const-string v1, "auto"

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v1, v4}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 492438
    :cond_2
    const/4 v1, 0x1

    invoke-static {v0, v2, v3, p4, v1}, LX/0X7;->a(LX/0X7;JIZ)I

    move-result v1

    move p4, v1

    .line 492439
    goto :goto_0

    .line 492440
    :cond_3
    iget-object v0, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    goto :goto_1
.end method

.method public final a(JJ)J
    .locals 9

    .prologue
    .line 492441
    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->On:LX/0c1;

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v1 .. v7}, LX/33I;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(LX/0c0;JJ)J
    .locals 8

    .prologue
    .line 492442
    sget-object v3, LX/0c1;->On:LX/0c1;

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-virtual/range {v1 .. v7}, LX/33I;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(LX/0c0;LX/0c1;JJ)J
    .locals 5

    .prologue
    .line 492443
    invoke-static {p3, p4}, LX/0c2;->a(J)Z

    move-result v0

    .line 492444
    if-eqz v0, :cond_1

    .line 492445
    :cond_0
    :goto_0
    return-wide p5

    .line 492446
    :cond_1
    iget-boolean v0, p0, LX/33I;->e:Z

    if-eqz v0, :cond_0

    .line 492447
    invoke-static {p3, p4}, LX/0c2;->b(J)I

    move-result v0

    .line 492448
    iget-object v1, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v1, v0}, LX/33H;->a(I)J

    move-result-wide v2

    .line 492449
    invoke-static {v2, v3}, LX/0X6;->a(J)I

    move-result v0

    .line 492450
    sget-object v1, LX/0c0;->Cached:LX/0c0;

    if-ne p1, v1, :cond_3

    .line 492451
    iget-object v1, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v1, v0}, LX/0W3;->a(I)LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    .line 492452
    :goto_1
    sget-object v1, LX/0c1;->On:LX/0c1;

    if-ne p2, v1, :cond_2

    .line 492453
    const-string v1, "auto"

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v1, v4}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 492454
    :cond_2
    invoke-virtual {v0, v2, v3, p5, p6}, LX/0X7;->c(JJ)J

    move-result-wide p5

    goto :goto_0

    .line 492455
    :cond_3
    iget-object v0, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    goto :goto_1
.end method

.method public final a(DLjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(D",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 492456
    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->On:LX/0c1;

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p4

    invoke-static/range {v1 .. v7}, LX/33I;->a(LX/33I;LX/0c0;LX/0c1;DLjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    return-object v0
.end method

.method public final a(CILandroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 492362
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/33I;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 492363
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p3, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(CLjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 492242
    sget-object v0, LX/0c0;->Cached:LX/0c0;

    sget-object v1, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/33I;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0c0;CILandroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 492457
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/33I;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 492458
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p4, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 492243
    sget-object v0, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, p1, v0, p2, p3}, LX/33I;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 492244
    invoke-static {p3}, LX/0c2;->a(C)Z

    move-result v0

    .line 492245
    if-eqz v0, :cond_1

    .line 492246
    :cond_0
    :goto_0
    return-object p4

    .line 492247
    :cond_1
    iget-boolean v0, p0, LX/33I;->e:Z

    if-eqz v0, :cond_0

    .line 492248
    shr-int/lit8 v0, p3, 0x1

    move v0, v0

    .line 492249
    iget-object v1, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v1, v0}, LX/33H;->a(I)J

    move-result-wide v2

    .line 492250
    invoke-static {v2, v3}, LX/0X6;->a(J)I

    move-result v0

    .line 492251
    sget-object v1, LX/0c0;->Cached:LX/0c0;

    if-ne p1, v1, :cond_3

    .line 492252
    iget-object v1, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v1, v0}, LX/0W3;->a(I)LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    .line 492253
    :goto_1
    sget-object v1, LX/0c1;->On:LX/0c1;

    if-ne p2, v1, :cond_2

    .line 492254
    const-string v1, "auto"

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v1, v4}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 492255
    :cond_2
    invoke-virtual {v0, v2, v3, p4}, LX/0X7;->b(JLjava/lang/String;)Ljava/lang/String;

    move-result-object p4

    goto :goto_0

    .line 492256
    :cond_3
    iget-object v0, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    goto :goto_1
.end method

.method public final a(IJ)V
    .locals 10

    .prologue
    .line 492257
    iget-object v0, p0, LX/33I;->f:LX/0ad;

    if-nez v0, :cond_0

    .line 492258
    :goto_0
    return-void

    .line 492259
    :cond_0
    invoke-static {p2, p3}, LX/0X6;->c(J)LX/0oE;

    move-result-object v0

    .line 492260
    sget-object v1, LX/5od;->a:[I

    invoke-virtual {v0}, LX/0oE;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 492261
    :pswitch_0
    shl-int/lit8 v4, p1, 0x1

    goto/16 :goto_6

    :goto_1
    or-int/2addr v3, v4

    int-to-char v3, v3

    move v2, v3

    .line 492262
    iget-object v3, p0, LX/33I;->f:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    const-string v6, ""

    invoke-interface {v3, v4, v5, v2, v6}, LX/0ad;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 492263
    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    const-string v5, ""

    invoke-virtual {p0, v3, v4, v2, v5}, LX/33I;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 492264
    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 492265
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 492266
    iget-object v2, p0, LX/33I;->g:LX/0Wx;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2, p3}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "|"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, p3}, LX/0X6;->a(J)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p2, p3}, LX/0X6;->b(J)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {v2 .. v8}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 492267
    goto :goto_0

    .line 492268
    :pswitch_1
    const/4 v2, 0x0

    .line 492269
    shl-int/lit8 v3, p1, 0x1

    add-int/lit16 v4, v3, -0x8000

    goto/16 :goto_7

    :goto_2
    or-int/2addr v3, v4

    int-to-short v3, v3

    move v3, v3

    .line 492270
    iget-object v4, p0, LX/33I;->f:LX/0ad;

    sget-object v5, LX/0c0;->Cached:LX/0c0;

    sget-object v6, LX/0c1;->Off:LX/0c1;

    invoke-interface {v4, v5, v6, v3, v2}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v8

    .line 492271
    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    invoke-virtual {p0, v4, v5, v3, v2}, LX/33I;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v5

    .line 492272
    if-ne v8, v5, :cond_1

    const/4 v2, 0x1

    move v4, v2

    .line 492273
    :goto_3
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 492274
    iget-object v2, p0, LX/33I;->g:LX/0Wx;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2, p3}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "|"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, p3}, LX/0X6;->a(J)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p2, p3}, LX/0X6;->b(J)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v8}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    invoke-interface/range {v2 .. v8}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 492275
    goto/16 :goto_0

    .line 492276
    :pswitch_2
    const/4 v2, 0x0

    const v7, -0x3b864000    # -999.0f

    .line 492277
    shl-int/lit8 v4, p1, 0x1

    goto :goto_8

    :goto_4
    or-int/2addr v3, v4

    int-to-float v3, v3

    move v3, v3

    .line 492278
    iget-object v4, p0, LX/33I;->f:LX/0ad;

    sget-object v5, LX/0c0;->Cached:LX/0c0;

    sget-object v6, LX/0c1;->Off:LX/0c1;

    invoke-interface {v4, v5, v6, v3, v7}, LX/0ad;->a(LX/0c0;LX/0c1;FF)F

    move-result v8

    .line 492279
    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    invoke-virtual {p0, v4, v5, v3, v7}, LX/33I;->a(LX/0c0;LX/0c1;FF)F

    move-result v5

    .line 492280
    cmpl-float v3, v8, v5

    if-nez v3, :cond_2

    const/4 v2, 0x1

    move v4, v2

    .line 492281
    :goto_5
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 492282
    iget-object v2, p0, LX/33I;->g:LX/0Wx;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2, p3}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "|"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, p3}, LX/0X6;->a(J)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p2, p3}, LX/0X6;->b(J)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v8}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v8

    invoke-interface/range {v2 .. v8}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 492283
    goto/16 :goto_0

    .line 492284
    :pswitch_3
    invoke-static {p0, p1, p2, p3}, LX/33I;->c(LX/33I;IJ)V

    goto/16 :goto_0

    :goto_6
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_1
    move v4, v2

    .line 492285
    goto/16 :goto_3

    :goto_7
    goto/16 :goto_2

    :cond_2
    move v4, v2

    .line 492286
    goto :goto_5

    :goto_8
    const/4 v3, 0x0

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/0c0;C)V
    .locals 2

    .prologue
    .line 492287
    shr-int/lit8 v0, p2, 0x1

    move v0, v0

    .line 492288
    iget-object v1, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v1, v0}, LX/33H;->a(I)J

    move-result-wide v0

    .line 492289
    invoke-direct {p0, p1, v0, v1}, LX/33I;->a(LX/0c0;J)V

    .line 492290
    return-void
.end method

.method public final a(LX/0c0;I)V
    .locals 2

    .prologue
    .line 492291
    shr-int/lit8 v0, p2, 0x1

    move v0, v0

    .line 492292
    iget-object v1, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v1, v0}, LX/33H;->a(I)J

    move-result-wide v0

    .line 492293
    invoke-direct {p0, p1, v0, v1}, LX/33I;->a(LX/0c0;J)V

    .line 492294
    return-void
.end method

.method public final a(LX/0c0;ICLjava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 492295
    iget-object v0, p0, LX/33I;->a:LX/0X0;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0X0;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492296
    :goto_0
    return-void

    .line 492297
    :cond_0
    sget-object v0, LX/0c1;->Off:LX/0c1;

    invoke-virtual {p0, p1, v0, p3, p5}, LX/33I;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 492298
    iget-object v0, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v0, p2}, LX/33H;->a(I)J

    move-result-wide v6

    .line 492299
    if-nez p4, :cond_2

    if-nez v3, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 492300
    :goto_1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x1

    aput-object v3, v0, v1

    const/4 v1, 0x2

    aput-object p4, v0, v1

    const/4 v1, 0x3

    aput-object p5, v0, v1

    const/4 v1, 0x4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v0, v1

    .line 492301
    iget-object v0, p0, LX/33I;->g:LX/0Wx;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v7}, LX/0X6;->a(J)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v7}, LX/0X6;->b(J)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "oldValue: %s defaultValue: %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p4, v7, v8

    const/4 v8, 0x1

    aput-object p5, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 492302
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1

    :cond_2
    invoke-virtual {p4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v2, v0

    goto :goto_1
.end method

.method public final a(LX/0c0;IDLjava/lang/Class;Ljava/lang/Enum;Ljava/lang/Enum;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(",
            "LX/0c0;",
            "ID",
            "Ljava/lang/Class",
            "<TT;>;TT;TT;)V"
        }
    .end annotation

    .prologue
    .line 492303
    iget-object v2, p0, LX/33I;->a:LX/0X0;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0X0;->a(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 492304
    :goto_0
    return-void

    .line 492305
    :cond_0
    sget-object v5, LX/0c1;->Off:LX/0c1;

    move-object v3, p0

    move-object v4, p1

    move-wide/from16 v6, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p7

    invoke-static/range {v3 .. v9}, LX/33I;->a(LX/33I;LX/0c0;LX/0c1;DLjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v5

    .line 492306
    iget-object v2, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v2, p2}, LX/33H;->a(I)J

    move-result-wide v8

    .line 492307
    move-object/from16 v0, p6

    if-ne v0, v5, :cond_1

    const/4 v2, 0x1

    move v4, v2

    .line 492308
    :goto_1
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v5}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    const/4 v3, 0x2

    invoke-virtual/range {p6 .. p6}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    const/4 v3, 0x3

    invoke-virtual/range {p7 .. p7}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    const/4 v3, 0x4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v2, v3

    .line 492309
    iget-object v2, p0, LX/33I;->g:LX/0Wx;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v8, v9}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ":"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v9}, LX/0X6;->a(J)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v9}, LX/0X6;->b(J)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "oldValue: %s defaultValue: %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual/range {p6 .. p6}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-virtual/range {p7 .. p7}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-interface/range {v2 .. v8}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 492310
    :cond_1
    const/4 v2, 0x0

    move v4, v2

    goto :goto_1
.end method

.method public final a(LX/0c0;IFFF)V
    .locals 10

    .prologue
    .line 492311
    iget-object v0, p0, LX/33I;->a:LX/0X0;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0X0;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492312
    :goto_0
    return-void

    .line 492313
    :cond_0
    sget-object v0, LX/0c1;->Off:LX/0c1;

    invoke-virtual {p0, p1, v0, p3, p5}, LX/33I;->a(LX/0c0;LX/0c1;FF)F

    move-result v3

    .line 492314
    iget-object v0, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v0, p2}, LX/33H;->a(I)J

    move-result-wide v6

    .line 492315
    sub-float v0, v3, p4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3727c5ac    # 1.0E-5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 492316
    :goto_1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x1

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x2

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x3

    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v0, v1

    .line 492317
    iget-object v0, p0, LX/33I;->g:LX/0Wx;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v7}, LX/0X6;->a(J)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v7}, LX/0X6;->b(J)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "oldValue: %.2f defaultValue: %.2f"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 492318
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1
.end method

.method public final a(LX/0c0;IIII)V
    .locals 10

    .prologue
    .line 492319
    iget-object v0, p0, LX/33I;->a:LX/0X0;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0X0;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492320
    :goto_0
    return-void

    .line 492321
    :cond_0
    sget-object v0, LX/0c1;->Off:LX/0c1;

    invoke-virtual {p0, p1, v0, p3, p5}, LX/33I;->a(LX/0c0;LX/0c1;II)I

    move-result v3

    .line 492322
    iget-object v0, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v0, p2}, LX/33H;->a(I)J

    move-result-wide v6

    .line 492323
    if-ne p4, v3, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 492324
    :goto_1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v0, v1

    .line 492325
    iget-object v0, p0, LX/33I;->g:LX/0Wx;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v7}, LX/0X6;->a(J)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v7}, LX/0X6;->b(J)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "oldValue: %d defaultValue: %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 492326
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1
.end method

.method public final a(LX/0c0;IJJJ)V
    .locals 11

    .prologue
    .line 492327
    iget-object v0, p0, LX/33I;->a:LX/0X0;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0X0;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492328
    :goto_0
    return-void

    .line 492329
    :cond_0
    sget-object v3, LX/0c1;->Off:LX/0c1;

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p7

    invoke-virtual/range {v1 .. v7}, LX/33I;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v4

    .line 492330
    iget-object v0, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v0, p2}, LX/33H;->a(I)J

    move-result-wide v6

    .line 492331
    cmp-long v0, p5, v4

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 492332
    :goto_1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x2

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x3

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v1

    .line 492333
    iget-object v0, p0, LX/33I;->g:LX/0Wx;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v7}, LX/0X6;->a(J)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v7}, LX/0X6;->b(J)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "oldValue: %d defaultValue: %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 492334
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1
.end method

.method public final a(LX/0c0;ISZZ)V
    .locals 10

    .prologue
    .line 492335
    iget-object v0, p0, LX/33I;->a:LX/0X0;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0X0;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492336
    :goto_0
    return-void

    .line 492337
    :cond_0
    sget-object v0, LX/0c1;->Off:LX/0c1;

    invoke-virtual {p0, p1, v0, p3, p5}, LX/33I;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v3

    .line 492338
    iget-object v0, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v0, p2}, LX/33H;->a(I)J

    move-result-wide v6

    .line 492339
    if-ne p4, v3, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 492340
    :goto_1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x2

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x3

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v0, v1

    .line 492341
    iget-object v0, p0, LX/33I;->g:LX/0Wx;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v7}, LX/0X6;->a(J)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v7}, LX/0X6;->b(J)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "oldValue: %s defaultValue: %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 492342
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1
.end method

.method public final a(LX/0c0;S)V
    .locals 2

    .prologue
    .line 492343
    invoke-static {p2}, LX/0c2;->b(S)I

    move-result v0

    .line 492344
    iget-object v1, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v1, v0}, LX/33H;->a(I)J

    move-result-wide v0

    .line 492345
    invoke-direct {p0, p1, v0, v1}, LX/33I;->a(LX/0c0;J)V

    .line 492346
    return-void
.end method

.method public final a(LX/0c0;LX/0c1;SZ)Z
    .locals 5

    .prologue
    .line 492347
    invoke-static {p3}, LX/0c2;->a(S)Z

    move-result v0

    .line 492348
    if-eqz v0, :cond_1

    .line 492349
    :cond_0
    :goto_0
    return p4

    .line 492350
    :cond_1
    iget-boolean v0, p0, LX/33I;->e:Z

    if-eqz v0, :cond_0

    .line 492351
    invoke-static {p3}, LX/0c2;->b(S)I

    move-result v0

    .line 492352
    iget-object v1, p0, LX/33I;->c:LX/33H;

    invoke-virtual {v1, v0}, LX/33H;->a(I)J

    move-result-wide v2

    .line 492353
    invoke-static {v2, v3}, LX/0X6;->a(J)I

    move-result v0

    .line 492354
    sget-object v1, LX/0c0;->Cached:LX/0c0;

    if-ne p1, v1, :cond_3

    .line 492355
    iget-object v1, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v1, v0}, LX/0W3;->a(I)LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    .line 492356
    :goto_1
    sget-object v1, LX/0c1;->On:LX/0c1;

    if-ne p2, v1, :cond_2

    .line 492357
    const-string v1, "auto"

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v1, v4}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 492358
    :cond_2
    invoke-virtual {v0, v2, v3, p4}, LX/0X7;->b(JZ)Z

    move-result p4

    goto :goto_0

    .line 492359
    :cond_3
    iget-object v0, p0, LX/33I;->d:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    goto :goto_1
.end method

.method public final a(LX/0c0;SZ)Z
    .locals 1

    .prologue
    .line 492360
    sget-object v0, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, p1, v0, p2, p3}, LX/33I;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    return v0
.end method

.method public final a(SZ)Z
    .locals 2

    .prologue
    .line 492361
    sget-object v0, LX/0c0;->Cached:LX/0c0;

    sget-object v1, LX/0c1;->On:LX/0c1;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/33I;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    return v0
.end method
