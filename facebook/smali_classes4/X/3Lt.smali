.class public LX/3Lt;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final k:[Ljava/lang/String;


# instance fields
.field public final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:I

.field private final g:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/Locale;

.field private final j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 552251
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Lt;->a:Ljava/lang/String;

    .line 552252
    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Lt;->b:Ljava/lang/String;

    .line 552253
    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Lt;->c:Ljava/lang/String;

    .line 552254
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\uac15\uc804"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "\ub0a8\uad81"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "\ub3c5\uace0"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "\ub3d9\ubc29"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "\ub9dd\uc808"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\uc0ac\uacf5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "\uc11c\ubb38"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "\uc120\uc6b0"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "\uc18c\ubd09"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\uc5b4\uae08"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "\uc7a5\uace1"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "\uc81c\uac08"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "\ud669\ubcf4"

    aput-object v2, v0, v1

    sput-object v0, LX/3Lt;->k:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 4

    .prologue
    .line 552323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 552324
    if-eqz p5, :cond_0

    :goto_0
    iput-object p5, p0, LX/3Lt;->i:Ljava/util/Locale;

    .line 552325
    iget-object v0, p0, LX/3Lt;->i:Ljava/util/Locale;

    invoke-static {p1, v0}, LX/3Lt;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/3Lt;->d:Ljava/util/HashSet;

    .line 552326
    iget-object v0, p0, LX/3Lt;->i:Ljava/util/Locale;

    invoke-static {p2, v0}, LX/3Lt;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/3Lt;->g:Ljava/util/HashSet;

    .line 552327
    iget-object v0, p0, LX/3Lt;->i:Ljava/util/Locale;

    invoke-static {p3, v0}, LX/3Lt;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/3Lt;->e:Ljava/util/HashSet;

    .line 552328
    iget-object v0, p0, LX/3Lt;->i:Ljava/util/Locale;

    invoke-static {p4, v0}, LX/3Lt;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/3Lt;->h:Ljava/util/HashSet;

    .line 552329
    iget-object v0, p0, LX/3Lt;->i:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3Lt;->j:Ljava/lang/String;

    .line 552330
    const/4 v0, 0x0

    .line 552331
    iget-object v1, p0, LX/3Lt;->e:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 552332
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v1, :cond_2

    .line 552333
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_2
    move v1, v0

    .line 552334
    goto :goto_1

    .line 552335
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p5

    goto :goto_0

    .line 552336
    :cond_1
    iput v1, p0, LX/3Lt;->f:I

    .line 552337
    return-void

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public static a(LX/3Lt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Ljava/lang/String;
    .locals 12
    .param p0    # LX/3Lt;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 552255
    if-nez p1, :cond_5

    const/4 v10, 0x0

    .line 552256
    :goto_0
    if-nez p2, :cond_6

    const/4 v9, 0x0

    .line 552257
    :goto_1
    if-nez p3, :cond_7

    const/4 v8, 0x0

    .line 552258
    :goto_2
    if-nez p4, :cond_8

    const/4 v7, 0x0

    .line 552259
    :goto_3
    if-nez p5, :cond_9

    const/4 v0, 0x0

    .line 552260
    :goto_4
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const/4 v1, 0x1

    .line 552261
    :goto_5
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    const/4 v2, 0x1

    .line 552262
    :goto_6
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    const/4 v3, 0x1

    .line 552263
    :goto_7
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    const/4 v4, 0x1

    .line 552264
    :goto_8
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e

    const/4 v5, 0x1

    move v11, v5

    .line 552265
    :goto_9
    const/4 v6, 0x1

    .line 552266
    const/4 v5, 0x0

    .line 552267
    if-eqz v1, :cond_0

    move-object v5, v10

    .line 552268
    :cond_0
    if-eqz v2, :cond_1

    .line 552269
    if-eqz v5, :cond_f

    .line 552270
    const/4 v6, 0x0

    .line 552271
    :cond_1
    :goto_a
    if-eqz v3, :cond_2

    .line 552272
    if-eqz v5, :cond_10

    .line 552273
    const/4 v6, 0x0

    .line 552274
    :cond_2
    :goto_b
    if-eqz v4, :cond_3

    .line 552275
    if-eqz v5, :cond_11

    .line 552276
    const/4 v6, 0x0

    .line 552277
    :cond_3
    :goto_c
    if-eqz v11, :cond_4

    .line 552278
    if-eqz v5, :cond_12

    .line 552279
    const/4 v6, 0x0

    .line 552280
    :cond_4
    :goto_d
    if-eqz v6, :cond_13

    move-object v0, v5

    .line 552281
    :goto_e
    return-object v0

    .line 552282
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 552283
    :cond_6
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 552284
    :cond_7
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    .line 552285
    :cond_8
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 552286
    :cond_9
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 552287
    :cond_a
    const/4 v1, 0x0

    goto :goto_5

    .line 552288
    :cond_b
    const/4 v2, 0x0

    goto :goto_6

    .line 552289
    :cond_c
    const/4 v3, 0x0

    goto :goto_7

    .line 552290
    :cond_d
    const/4 v4, 0x0

    goto :goto_8

    .line 552291
    :cond_e
    const/4 v5, 0x0

    move v11, v5

    goto :goto_9

    :cond_f
    move-object v5, v9

    .line 552292
    goto :goto_a

    :cond_10
    move-object v5, v8

    .line 552293
    goto :goto_b

    :cond_11
    move-object v5, v7

    .line 552294
    goto :goto_c

    .line 552295
    :cond_12
    invoke-direct {p0, v0}, LX/3Lt;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_d

    .line 552296
    :cond_13
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 552297
    if-eqz v1, :cond_14

    .line 552298
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552299
    :cond_14
    if-eqz v2, :cond_16

    .line 552300
    if-eqz v1, :cond_15

    .line 552301
    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 552302
    :cond_15
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552303
    :cond_16
    if-eqz v3, :cond_1a

    .line 552304
    if-nez v1, :cond_17

    if-eqz v2, :cond_19

    .line 552305
    :cond_17
    if-eqz p7, :cond_18

    .line 552306
    const/16 v6, 0x2c

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 552307
    :cond_18
    if-eqz p6, :cond_19

    .line 552308
    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 552309
    :cond_19
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552310
    :cond_1a
    if-eqz v4, :cond_1d

    .line 552311
    if-nez v1, :cond_1b

    if-nez v2, :cond_1b

    if-eqz v3, :cond_1c

    .line 552312
    :cond_1b
    if-eqz p6, :cond_1c

    .line 552313
    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 552314
    :cond_1c
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552315
    :cond_1d
    if-eqz v11, :cond_21

    .line 552316
    if-nez v1, :cond_1e

    if-nez v2, :cond_1e

    if-nez v3, :cond_1e

    if-eqz v4, :cond_20

    .line 552317
    :cond_1e
    if-eqz p8, :cond_1f

    .line 552318
    const/16 v1, 0x2c

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 552319
    :cond_1f
    if-eqz p6, :cond_20

    .line 552320
    const/16 v1, 0x20

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 552321
    :cond_20
    invoke-direct {p0, v0}, LX/3Lt;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552322
    :cond_21
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_e
.end method

.method private static a(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/HashSet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 552244
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 552245
    if-eqz p0, :cond_0

    .line 552246
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 552247
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 552248
    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 552249
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 552250
    :cond_0
    return-object v1
.end method

.method public static a(Ljava/lang/Character$UnicodeBlock;)Z
    .locals 1

    .prologue
    .line 552338
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->BASIC_LATIN:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_1_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_A:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_B:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_ADDITIONAL:Ljava/lang/Character$UnicodeBlock;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v1, 0x2e

    .line 552238
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 552239
    if-eqz v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v1, :cond_1

    .line 552240
    :cond_0
    :goto_0
    return-object p1

    .line 552241
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 552242
    iget-object v1, p0, LX/3Lt;->e:Ljava/util/HashSet;

    iget-object v2, p0, LX/3Lt;->i:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object p1, v0

    .line 552243
    goto :goto_0
.end method

.method public static c(Ljava/lang/Character$UnicodeBlock;)Z
    .locals 1

    .prologue
    .line 552222
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HANGUL_SYLLABLES:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HANGUL_JAMO:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HANGUL_COMPATIBILITY_JAMO:Ljava/lang/Character$UnicodeBlock;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljava/lang/Character$UnicodeBlock;)Z
    .locals 1

    .prologue
    .line 552237
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->KATAKANA:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->KATAKANA_PHONETIC_EXTENSIONS:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HALFWIDTH_AND_FULLWIDTH_FORMS:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HIRAGANA:Ljava/lang/Character$UnicodeBlock;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 5

    .prologue
    const/4 v1, 0x5

    const/4 v0, 0x4

    const/4 v2, 0x3

    .line 552223
    if-nez p1, :cond_4

    .line 552224
    sget-object v3, LX/3Lt;->a:Ljava/lang/String;

    iget-object v4, p0, LX/3Lt;->j:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move p1, v0

    .line 552225
    :cond_0
    :goto_0
    return p1

    .line 552226
    :cond_1
    sget-object v0, LX/3Lt;->b:Ljava/lang/String;

    iget-object v3, p0, LX/3Lt;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move p1, v1

    .line 552227
    goto :goto_0

    .line 552228
    :cond_2
    sget-object v0, LX/3Lt;->c:Ljava/lang/String;

    iget-object v1, p0, LX/3Lt;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move p1, v2

    .line 552229
    goto :goto_0

    .line 552230
    :cond_3
    const/4 p1, 0x1

    goto :goto_0

    .line 552231
    :cond_4
    const/4 v3, 0x2

    if-ne p1, v3, :cond_0

    .line 552232
    sget-object v3, LX/3Lt;->a:Ljava/lang/String;

    iget-object v4, p0, LX/3Lt;->j:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    move p1, v0

    .line 552233
    goto :goto_0

    .line 552234
    :cond_5
    sget-object v0, LX/3Lt;->b:Ljava/lang/String;

    iget-object v3, p0, LX/3Lt;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move p1, v1

    .line 552235
    goto :goto_0

    :cond_6
    move p1, v2

    .line 552236
    goto :goto_0
.end method
