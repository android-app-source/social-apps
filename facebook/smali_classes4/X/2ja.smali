.class public LX/2ja;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2jb;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Landroid/os/Bundle;

.field public c:LX/03V;

.field private d:LX/Cfp;

.field private e:J

.field private f:LX/1P1;

.field public g:LX/2j3;

.field public h:J

.field public i:I

.field public final j:LX/1vC;

.field public k:Ljava/lang/String;

.field public final l:LX/2jY;

.field public m:Z

.field public n:Z

.field public o:J

.field public final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Cfc;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Cfq;",
            ">;"
        }
    .end annotation
.end field

.field public final v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 454040
    const-class v0, LX/2ja;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2ja;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2jY;LX/1P1;LX/03V;LX/2j3;LX/1vC;)V
    .locals 4
    .param p1    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1P1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    .line 454041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454042
    iput-wide v2, p0, LX/2ja;->h:J

    .line 454043
    const/4 v0, 0x0

    iput v0, p0, LX/2ja;->i:I

    .line 454044
    iput-boolean v1, p0, LX/2ja;->m:Z

    .line 454045
    iput-boolean v1, p0, LX/2ja;->n:Z

    .line 454046
    iput-wide v2, p0, LX/2ja;->o:J

    .line 454047
    iput-object p3, p0, LX/2ja;->c:LX/03V;

    .line 454048
    iput-object p2, p0, LX/2ja;->f:LX/1P1;

    .line 454049
    iput-object p4, p0, LX/2ja;->g:LX/2j3;

    .line 454050
    iput-object p5, p0, LX/2ja;->j:LX/1vC;

    .line 454051
    iput-object p1, p0, LX/2ja;->l:LX/2jY;

    .line 454052
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2ja;->p:Ljava/util/List;

    .line 454053
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2ja;->q:Ljava/util/List;

    .line 454054
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2ja;->r:Ljava/util/List;

    .line 454055
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2ja;->s:Ljava/util/List;

    .line 454056
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2ja;->t:Ljava/util/List;

    .line 454057
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2ja;->v:Ljava/util/List;

    .line 454058
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2ja;->w:Ljava/util/List;

    .line 454059
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2ja;->u:Ljava/util/Map;

    .line 454060
    return-void
.end method

.method public static a(LX/2ja;II)V
    .locals 3

    .prologue
    .line 454061
    iget-object v0, p0, LX/2ja;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cfq;

    .line 454062
    const/4 v2, 0x0

    .line 454063
    invoke-virtual {v0}, LX/Cfq;->g()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 454064
    iget p0, v0, LX/Cfq;->c:I

    move p0, p0

    .line 454065
    if-ltz p0, :cond_0

    if-ltz p1, :cond_0

    if-gez p2, :cond_2

    .line 454066
    :cond_0
    :goto_1
    move v2, v2

    .line 454067
    iput-boolean v2, v0, LX/Cfq;->h:Z

    .line 454068
    goto :goto_0

    .line 454069
    :cond_1
    return-void

    :cond_2
    iget p0, v0, LX/Cfq;->d:I

    if-lt p0, p1, :cond_0

    iget p0, v0, LX/Cfq;->e:I

    if-gt p0, p2, :cond_0

    const/4 v2, 0x1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 454070
    invoke-direct/range {p0 .. p1}, LX/2ja;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 454071
    iget-object v1, p0, LX/2ja;->g:LX/2j3;

    iget-object v2, p0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v3}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/2ja;->b:Landroid/os/Bundle;

    iget-object v5, p0, LX/2ja;->k:Ljava/lang/String;

    invoke-direct/range {p0 .. p1}, LX/2ja;->d(Ljava/lang/String;)I

    move-result v8

    iget-object v7, p0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v7}, LX/2jY;->u()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v7, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p4

    move-object/from16 v12, p5

    move-object/from16 v13, p3

    move-object/from16 v14, p6

    invoke-virtual/range {v1 .. v14}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;)V

    .line 454072
    iget-object v1, p0, LX/2ja;->p:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454073
    if-eqz p2, :cond_0

    .line 454074
    iget-object v1, p0, LX/2ja;->q:Ljava/util/List;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454075
    :cond_0
    iget-object v1, p0, LX/2ja;->r:Ljava/util/List;

    move-object/from16 v0, p3

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454076
    new-instance v5, LX/Cfp;

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    invoke-direct/range {v5 .. v9}, LX/Cfp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    iput-object v5, p0, LX/2ja;->d:LX/Cfp;

    .line 454077
    return-void
.end method

.method public static b(LX/2ja;IJ)V
    .locals 18

    .prologue
    .line 454078
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->k()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/2ja;->h:J

    sub-long v8, v2, v4

    .line 454079
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/2ja;->o:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-wide v2, v0, LX/2ja;->o:J

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/2ja;->h:J

    sub-long v10, v2, v4

    .line 454080
    :goto_0
    move-object/from16 v0, p0

    iget-wide v2, v0, LX/2ja;->o:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-wide v2, v0, LX/2ja;->o:J

    sub-long v14, p2, v2

    .line 454081
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->p()J

    move-result-wide v2

    sub-long v16, p2, v2

    .line 454082
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2ja;->g:LX/2j3;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->f()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, LX/2ja;->i:I

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->j()J

    move-result-wide v12

    move/from16 v7, p1

    invoke-virtual/range {v3 .. v17}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;IIJJJJJ)V

    .line 454083
    return-void

    .line 454084
    :cond_0
    const-wide/16 v10, 0x0

    goto :goto_0

    .line 454085
    :cond_1
    const-wide/16 v14, 0x0

    goto :goto_1
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 454086
    iget-object v0, p0, LX/2ja;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cfq;

    .line 454087
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 454088
    :cond_0
    iget-object p0, v0, LX/Cfq;->b:Ljava/lang/String;

    move-object v0, p0

    .line 454089
    goto :goto_0
.end method

.method private d(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 454090
    iget-object v0, p0, LX/2ja;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cfq;

    .line 454091
    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    .line 454092
    :cond_0
    iget p0, v0, LX/Cfq;->c:I

    move v0, p0

    .line 454093
    goto :goto_0
.end method

.method public static e(LX/2ja;Ljava/lang/String;)LX/Cfq;
    .locals 2

    .prologue
    .line 454094
    iget-object v0, p0, LX/2ja;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cfq;

    .line 454095
    if-nez v0, :cond_0

    .line 454096
    new-instance v0, LX/Cfq;

    invoke-direct {v0, p1}, LX/Cfq;-><init>(Ljava/lang/String;)V

    .line 454097
    iget-object v1, p0, LX/2ja;->u:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454098
    :cond_0
    return-object v0
.end method

.method public static f(LX/2ja;J)V
    .locals 10

    .prologue
    .line 454148
    iget-wide v0, p0, LX/2ja;->e:J

    sub-long v2, p1, v0

    .line 454149
    iput-wide p1, p0, LX/2ja;->e:J

    .line 454150
    iget-object v0, p0, LX/2ja;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cfq;

    .line 454151
    iget-wide v4, p0, LX/2ja;->e:J

    .line 454152
    iget-boolean v7, v0, LX/Cfq;->h:Z

    if-eqz v7, :cond_0

    iget-wide v7, v0, LX/Cfq;->j:J

    cmp-long v7, v4, v7

    if-gtz v7, :cond_2

    .line 454153
    :cond_0
    :goto_1
    goto :goto_0

    .line 454154
    :cond_1
    return-void

    .line 454155
    :cond_2
    iget-wide v7, v0, LX/Cfq;->i:J

    add-long/2addr v7, v2

    iput-wide v7, v0, LX/Cfq;->i:J

    .line 454156
    iput-wide v4, v0, LX/Cfq;->j:J

    goto :goto_1
.end method

.method public static g(LX/2ja;J)V
    .locals 3

    .prologue
    .line 454099
    invoke-static {p0, p1, p2}, LX/2ja;->f(LX/2ja;J)V

    .line 454100
    iget-object v0, p0, LX/2ja;->f:LX/1P1;

    if-eqz v0, :cond_0

    .line 454101
    iget-object v0, p0, LX/2ja;->f:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    iget-object v1, p0, LX/2ja;->f:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->n()I

    move-result v1

    invoke-static {p0, v0, v1}, LX/2ja;->a(LX/2ja;II)V

    .line 454102
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(JJ)V
    .locals 19

    .prologue
    .line 454103
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2ja;->g:LX/2j3;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->f()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->e()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->c()LX/8Y9;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/2ja;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/2ja;->w:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/2ja;->p:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/2ja;->q:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2ja;->r:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v17

    move-wide/from16 v10, p1

    move-wide/from16 v12, p3

    invoke-virtual/range {v3 .. v17}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8Y9;Ljava/lang/String;Ljava/lang/Iterable;JJLjava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;I)V

    .line 454104
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->n()Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->n()Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/ReactionQueryParams;->f()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 454105
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->g:LX/2j3;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v3}, LX/2jY;->f()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v4}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v5}, LX/2jY;->e()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v6}, LX/2jY;->c()LX/8Y9;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, LX/2ja;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v8}, LX/2jY;->n()Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/ReactionQueryParams;->f()Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/2ja;->w:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/2ja;->p:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/2ja;->q:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2ja;->r:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2ja;->r:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v17

    move-wide/from16 v10, p1

    move-wide/from16 v12, p3

    invoke-virtual/range {v2 .. v17}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8Y9;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Iterable;JJLjava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;I)V

    .line 454106
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/2ja;->e()V

    .line 454107
    return-void
.end method

.method public final a(JJJJJJ)V
    .locals 25

    .prologue
    .line 454108
    move-object/from16 v0, p0

    iget-object v5, v0, LX/2ja;->g:LX/2j3;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v4}, LX/2jY;->f()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v4}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v4}, LX/2jY;->e()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v4, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v4}, LX/2jY;->c()LX/8Y9;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2ja;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2ja;->w:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2ja;->t:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2ja;->s:Ljava/util/List;

    sub-long v18, p9, p7

    move-wide/from16 v14, p1

    move-wide/from16 v16, p3

    move-wide/from16 v20, p5

    move-wide/from16 v22, p11

    invoke-virtual/range {v5 .. v23}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8Y9;Ljava/lang/String;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;JJJJJ)V

    .line 454109
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2ja;->f:LX/1P1;

    if-eqz v4, :cond_0

    .line 454110
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2ja;->f:LX/1P1;

    invoke-virtual {v4}, LX/1P1;->l()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2ja;->f:LX/1P1;

    invoke-virtual {v5}, LX/1P1;->n()I

    move-result v5

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, LX/2ja;->a(LX/2ja;II)V

    .line 454111
    :cond_0
    move-wide/from16 v0, p9

    move-object/from16 v2, p0

    iput-wide v0, v2, LX/2ja;->e:J

    .line 454112
    move-wide/from16 v0, p9

    move-object/from16 v2, p0

    iput-wide v0, v2, LX/2ja;->h:J

    .line 454113
    return-void
.end method

.method public final a(LX/1P1;JZ)V
    .locals 0

    .prologue
    .line 454034
    iput-object p1, p0, LX/2ja;->f:LX/1P1;

    .line 454035
    if-eqz p4, :cond_0

    .line 454036
    invoke-static {p0, p2, p3}, LX/2ja;->g(LX/2ja;J)V

    .line 454037
    :cond_0
    return-void
.end method

.method public final a(LX/9qX;)V
    .locals 2

    .prologue
    .line 454114
    iget-object v0, p0, LX/2ja;->v:Ljava/util/List;

    invoke-interface {p1}, LX/9qX;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454115
    :goto_0
    return-void

    .line 454116
    :cond_0
    iget-object v0, p0, LX/2ja;->v:Ljava/util/List;

    invoke-interface {p1}, LX/9qX;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454117
    iget-object v0, p0, LX/2ja;->w:Ljava/util/List;

    invoke-interface {p1}, LX/9qX;->m()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(LX/9qX;II)V
    .locals 7

    .prologue
    .line 454118
    invoke-interface {p1}, LX/9qX;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454119
    :goto_0
    return-void

    .line 454120
    :cond_0
    invoke-interface {p1}, LX/9qX;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/2ja;->e(LX/2ja;Ljava/lang/String;)LX/Cfq;

    move-result-object v0

    .line 454121
    invoke-virtual {v0}, LX/Cfq;->g()Z

    move-result v1

    if-nez v1, :cond_1

    .line 454122
    invoke-interface {p1}, LX/9qX;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/9qX;->m()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v5, 0x0

    .line 454123
    iput-object v1, v0, LX/Cfq;->b:Ljava/lang/String;

    .line 454124
    iput p3, v0, LX/Cfq;->d:I

    .line 454125
    iput p3, v0, LX/Cfq;->e:I

    .line 454126
    iput p2, v0, LX/Cfq;->c:I

    .line 454127
    iput-object v2, v0, LX/Cfq;->f:Ljava/lang/String;

    .line 454128
    const/4 v3, 0x0

    iput-boolean v3, v0, LX/Cfq;->g:Z

    .line 454129
    iput-wide v5, v0, LX/Cfq;->i:J

    .line 454130
    iput-wide v5, v0, LX/Cfq;->j:J

    .line 454131
    goto :goto_0

    .line 454132
    :cond_1
    iget v1, v0, LX/Cfq;->d:I

    invoke-static {v1, p3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, LX/Cfq;->d:I

    .line 454133
    iget v1, v0, LX/Cfq;->e:I

    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, LX/Cfq;->e:I

    .line 454134
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 454135
    invoke-static {p0, p1}, LX/2ja;->e(LX/2ja;Ljava/lang/String;)LX/Cfq;

    move-result-object v0

    .line 454136
    iget p0, v0, LX/Cfq;->l:I

    invoke-static {p2, p0}, Ljava/lang/Math;->max(II)I

    move-result p0

    iput p0, v0, LX/Cfq;->l:I

    .line 454137
    return-void
.end method

.method public final a(Ljava/lang/String;JJJ)V
    .locals 20

    .prologue
    .line 454138
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2ja;->g:LX/2j3;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    if-nez v2, :cond_0

    const-string v4, "NO_SESSION_ID"

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    if-nez v2, :cond_1

    const-string v5, "ANDROID_COMPOSER"

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    if-nez v2, :cond_2

    const/4 v6, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    if-nez v2, :cond_3

    const/4 v7, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v8, v0, LX/2ja;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/2ja;->w:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2ja;->t:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2ja;->s:Ljava/util/List;

    move-wide/from16 v12, p2

    move-wide/from16 v14, p4

    move-wide/from16 v16, p6

    move-object/from16 v18, p1

    invoke-virtual/range {v3 .. v18}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8Y9;Ljava/lang/String;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;JJJLjava/lang/String;)V

    .line 454139
    return-void

    .line 454140
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->f()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->e()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->c()LX/8Y9;

    move-result-object v7

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;LX/Cfd;)V
    .locals 1

    .prologue
    .line 454038
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/2ja;->a(Ljava/lang/String;LX/Cfd;Ljava/lang/String;)V

    .line 454039
    return-void
.end method

.method public final a(Ljava/lang/String;LX/Cfd;Ljava/lang/String;)V
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 454141
    iget-object v0, p0, LX/2ja;->g:LX/2j3;

    iget-object v1, p0, LX/2ja;->l:LX/2jY;

    .line 454142
    iget-object v2, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v2

    .line 454143
    iget-object v2, p0, LX/2ja;->l:LX/2jY;

    .line 454144
    iget-object v3, v2, LX/2jY;->b:Ljava/lang/String;

    move-object v2, v3

    .line 454145
    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    .line 454146
    iget-object p0, v0, LX/2j3;->a:LX/0Zb;

    sget-object p1, LX/Cff;->REACTION_HEADER_INTERACTION:LX/Cff;

    const-string p2, "reaction_overlay"

    invoke-static {p1, v1, p2, v2}, LX/2j3;->a(LX/Cff;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "context_row_type"

    invoke-virtual {p1, p2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "place_id"

    invoke-virtual {p1, p2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "interaction_type"

    iget-object p3, v4, LX/Cfd;->name:Ljava/lang/String;

    invoke-virtual {p1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 454147
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 453928
    invoke-direct {p0, p1}, LX/2ja;->d(Ljava/lang/String;)I

    move-result v5

    .line 453929
    invoke-static {p0, p1}, LX/2ja;->e(LX/2ja;Ljava/lang/String;)LX/Cfq;

    move-result-object v7

    .line 453930
    iget-boolean v0, v7, LX/Cfq;->g:Z

    move v0, v0

    .line 453931
    if-nez v0, :cond_0

    .line 453932
    iget-object v0, p0, LX/2ja;->g:LX/2j3;

    iget-object v1, p0, LX/2ja;->l:LX/2jY;

    .line 453933
    iget-object v2, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v2

    .line 453934
    iget-object v2, p0, LX/2ja;->l:LX/2jY;

    .line 453935
    iget-object v3, v2, LX/2jY;->b:Ljava/lang/String;

    move-object v2, v3

    .line 453936
    iget-object v3, v7, LX/Cfq;->b:Ljava/lang/String;

    move-object v3, v3

    .line 453937
    move-object v4, p1

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 453938
    const/4 v0, 0x1

    .line 453939
    iput-boolean v0, v7, LX/Cfq;->g:Z

    .line 453940
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8

    .prologue
    .line 453941
    invoke-static {p0, p1}, LX/2ja;->e(LX/2ja;Ljava/lang/String;)LX/Cfq;

    move-result-object v3

    .line 453942
    iget-object v0, p0, LX/2ja;->g:LX/2j3;

    iget-object v1, p0, LX/2ja;->l:LX/2jY;

    .line 453943
    iget-object v2, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v2

    .line 453944
    iget-object v2, p0, LX/2ja;->l:LX/2jY;

    .line 453945
    iget-object v4, v2, LX/2jY;->b:Ljava/lang/String;

    move-object v2, v4

    .line 453946
    iget-object v4, v3, LX/Cfq;->b:Ljava/lang/String;

    move-object v3, v4

    .line 453947
    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 453948
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;)V
    .locals 14

    .prologue
    .line 453949
    iget-object v0, p0, LX/2ja;->g:LX/2j3;

    iget-object v1, p0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v1}, LX/2jY;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/2ja;->b:Landroid/os/Bundle;

    iget-object v4, p0, LX/2ja;->k:Ljava/lang/String;

    invoke-direct {p0, p1}, LX/2ja;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, p1}, LX/2ja;->d(Ljava/lang/String;)I

    move-result v7

    const/4 v9, 0x0

    iget-object v6, p0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v6}, LX/2jY;->u()Ljava/lang/String;

    move-result-object v10

    const/4 v13, 0x0

    move-object v6, p1

    move-object/from16 v8, p2

    move-object/from16 v11, p4

    move-object/from16 v12, p3

    invoke-virtual/range {v0 .. v13}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;)V

    .line 453950
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V
    .locals 15

    .prologue
    .line 453951
    invoke-direct/range {p0 .. p1}, LX/2ja;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 453952
    iget-object v1, p0, LX/2ja;->g:LX/2j3;

    iget-object v2, p0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v2}, LX/2jY;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v3}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/2ja;->b:Landroid/os/Bundle;

    iget-object v5, p0, LX/2ja;->k:Ljava/lang/String;

    invoke-direct/range {p0 .. p1}, LX/2ja;->d(Ljava/lang/String;)I

    move-result v8

    const/4 v10, 0x0

    iget-object v7, p0, LX/2ja;->l:LX/2jY;

    invoke-virtual {v7}, LX/2jY;->u()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p3

    iget-object v12, v0, LX/Cfl;->b:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v13, v0, LX/Cfl;->a:LX/Cfc;

    move-object/from16 v0, p3

    iget-object v7, v0, LX/Cfl;->d:Landroid/content/Intent;

    if-eqz v7, :cond_0

    move-object/from16 v0, p3

    iget-object v7, v0, LX/Cfl;->d:Landroid/content/Intent;

    const-string v9, "event_suggestion_token"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    :goto_0
    move-object/from16 v7, p1

    move-object/from16 v9, p2

    invoke-virtual/range {v1 .. v14}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;)V

    .line 453953
    iget-object v1, p0, LX/2ja;->p:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 453954
    iget-object v1, p0, LX/2ja;->q:Ljava/util/List;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 453955
    iget-object v1, p0, LX/2ja;->r:Ljava/util/List;

    move-object/from16 v0, p3

    iget-object v2, v0, LX/Cfl;->a:LX/Cfc;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 453956
    new-instance v5, LX/Cfp;

    move-object/from16 v0, p3

    iget-object v9, v0, LX/Cfl;->a:LX/Cfc;

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    invoke-direct/range {v5 .. v9}, LX/Cfp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    iput-object v5, p0, LX/2ja;->d:LX/Cfp;

    .line 453957
    return-void

    .line 453958
    :cond_0
    const/4 v14, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 453959
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 453960
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 453961
    iget-object v3, p4, LX/Cfl;->a:LX/Cfc;

    iget-object v5, p4, LX/Cfl;->b:Ljava/lang/String;

    iget-object v0, p4, LX/Cfl;->d:Landroid/content/Intent;

    if-nez v0, :cond_0

    const/4 v6, 0x0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 453962
    return-void

    .line 453963
    :cond_0
    iget-object v0, p4, LX/Cfl;->d:Landroid/content/Intent;

    const-string v1, "event_suggestion_token"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 453964
    iget-object v0, p0, LX/2ja;->g:LX/2j3;

    iget-object v1, p0, LX/2ja;->l:LX/2jY;

    .line 453965
    iget-object v2, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v2

    .line 453966
    iget-object v2, p0, LX/2ja;->l:LX/2jY;

    .line 453967
    iget-object p0, v2, LX/2jY;->b:Ljava/lang/String;

    move-object v2, p0

    .line 453968
    invoke-virtual {v0, v1, v2, p1}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 453969
    return-void
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453970
    iget-object v0, p0, LX/2ja;->p:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 453971
    invoke-static {p0, p1}, LX/2ja;->e(LX/2ja;Ljava/lang/String;)LX/Cfq;

    move-result-object v0

    .line 453972
    iget p0, v0, LX/Cfq;->k:I

    invoke-static {p2, p0}, Ljava/lang/Math;->max(II)I

    move-result p0

    iput p0, v0, LX/Cfq;->k:I

    .line 453973
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 453974
    iget-object v0, p0, LX/2ja;->l:LX/2jY;

    .line 453975
    iget-object p0, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, p0

    .line 453976
    return-object v0
.end method

.method public final c(J)V
    .locals 6

    .prologue
    .line 453977
    iget-boolean v0, p0, LX/2ja;->m:Z

    if-eqz v0, :cond_0

    .line 453978
    iget-object v0, p0, LX/2ja;->g:LX/2j3;

    iget-object v1, p0, LX/2ja;->l:LX/2jY;

    .line 453979
    iget-object v2, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v1, v2

    .line 453980
    iget-object v2, p0, LX/2ja;->l:LX/2jY;

    .line 453981
    iget-object v3, v2, LX/2jY;->b:Ljava/lang/String;

    move-object v2, v3

    .line 453982
    iget-object v3, v0, LX/2j3;->a:LX/0Zb;

    sget-object v4, LX/Cff;->REACTION_FORWARD_SCROLL:LX/Cff;

    const-string v5, "reaction_attachment"

    invoke-static {v4, v1, v5, v2}, LX/2j3;->a(LX/Cff;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "time_to_scroll"

    invoke-virtual {v4, v5, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 453983
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2ja;->m:Z

    .line 453984
    :cond_0
    return-void
.end method

.method public final d()LX/0lF;
    .locals 11

    .prologue
    .line 453985
    iget-object v0, p0, LX/2ja;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 453986
    new-instance v2, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v1}, LX/162;-><init>(LX/0mC;)V

    .line 453987
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Cfq;

    .line 453988
    iget-wide v9, v1, LX/Cfq;->i:J

    move-wide v5, v9

    .line 453989
    const-wide/16 v7, 0x0

    cmp-long v4, v5, v7

    if-lez v4, :cond_0

    .line 453990
    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 453991
    iget-object v5, v1, LX/Cfq;->a:Ljava/lang/String;

    move-object v5, v5

    .line 453992
    new-instance v6, LX/10w;

    .line 453993
    iget-wide v9, v1, LX/Cfq;->i:J

    move-wide v7, v9

    .line 453994
    invoke-direct {v6, v7, v8}, LX/10w;-><init>(J)V

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;LX/0lF;)LX/0lF;

    move-result-object v1

    .line 453995
    invoke-virtual {v2, v1}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 453996
    :cond_1
    move-object v0, v2

    .line 453997
    return-object v0
.end method

.method public final d(J)V
    .locals 2

    .prologue
    .line 453998
    iget-object v0, p0, LX/2ja;->d:LX/Cfp;

    if-eqz v0, :cond_0

    .line 453999
    iget-object v0, p0, LX/2ja;->d:LX/Cfp;

    .line 454000
    iput-wide p1, v0, LX/Cfp;->d:J

    .line 454001
    :cond_0
    invoke-static {p0, p1, p2}, LX/2ja;->f(LX/2ja;J)V

    .line 454002
    return-void
.end method

.method public final e()V
    .locals 15

    .prologue
    .line 454003
    iget-object v0, p0, LX/2ja;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v6

    .line 454004
    iget-object v0, p0, LX/2ja;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cfq;

    .line 454005
    invoke-virtual {v0}, LX/Cfq;->i()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 454006
    iget-object v1, p0, LX/2ja;->g:LX/2j3;

    iget-object v2, p0, LX/2ja;->l:LX/2jY;

    .line 454007
    iget-object v3, v2, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v3

    .line 454008
    iget-object v3, p0, LX/2ja;->l:LX/2jY;

    .line 454009
    iget-object v4, v3, LX/2jY;->b:Ljava/lang/String;

    move-object v3, v4

    .line 454010
    iget-object v4, v0, LX/Cfq;->a:Ljava/lang/String;

    move-object v4, v4

    .line 454011
    iget-object v5, v0, LX/Cfq;->f:Ljava/lang/String;

    move-object v5, v5

    .line 454012
    iget v7, v0, LX/Cfq;->c:I

    move v7, v7

    .line 454013
    iget v8, v0, LX/Cfq;->k:I

    move v8, v8

    .line 454014
    iget v9, v0, LX/Cfq;->l:I

    move v9, v9

    .line 454015
    invoke-virtual {v0}, LX/Cfq;->i()J

    move-result-wide v10

    .line 454016
    iget-object v12, v0, LX/Cfq;->b:Ljava/lang/String;

    move-object v12, v12

    .line 454017
    iget-object v13, p0, LX/2ja;->k:Ljava/lang/String;

    invoke-virtual/range {v1 .. v13}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIJLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 454018
    :cond_1
    return-void
.end method

.method public final e(J)V
    .locals 11

    .prologue
    .line 454019
    iget-object v0, p0, LX/2ja;->d:LX/Cfp;

    if-eqz v0, :cond_0

    .line 454020
    iget-object v1, p0, LX/2ja;->g:LX/2j3;

    iget-object v0, p0, LX/2ja;->l:LX/2jY;

    .line 454021
    iget-object v2, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v2

    .line 454022
    iget-object v0, p0, LX/2ja;->l:LX/2jY;

    .line 454023
    iget-object v3, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v3, v3

    .line 454024
    iget-object v0, p0, LX/2ja;->d:LX/Cfp;

    iget-object v4, v0, LX/Cfp;->b:Ljava/lang/String;

    iget-object v0, p0, LX/2ja;->d:LX/Cfp;

    iget-object v5, v0, LX/Cfp;->e:Ljava/lang/String;

    iget-object v0, p0, LX/2ja;->d:LX/Cfp;

    iget-wide v6, v0, LX/Cfp;->d:J

    sub-long v6, p1, v6

    iget-object v0, p0, LX/2ja;->d:LX/Cfp;

    iget-object v8, v0, LX/Cfp;->a:Ljava/lang/String;

    iget-object v0, p0, LX/2ja;->d:LX/Cfp;

    iget-object v9, v0, LX/Cfp;->e:Ljava/lang/String;

    iget-object v0, p0, LX/2ja;->d:LX/Cfp;

    iget-object v10, v0, LX/Cfp;->c:LX/Cfc;

    invoke-virtual/range {v1 .. v10}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;LX/Cfc;)V

    .line 454025
    const/4 v0, 0x0

    iput-object v0, p0, LX/2ja;->d:LX/Cfp;

    .line 454026
    :cond_0
    iget-object v0, p0, LX/2ja;->f:LX/1P1;

    if-eqz v0, :cond_1

    .line 454027
    iget-object v0, p0, LX/2ja;->f:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    iget-object v1, p0, LX/2ja;->f:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->n()I

    move-result v1

    invoke-static {p0, v0, v1}, LX/2ja;->a(LX/2ja;II)V

    .line 454028
    :cond_1
    iput-wide p1, p0, LX/2ja;->e:J

    .line 454029
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 454030
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2ja;->m:Z

    .line 454031
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 454032
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2ja;->n:Z

    .line 454033
    return-void
.end method
