.class public LX/296;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/296;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/1MZ;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/290;

.field public final e:LX/0So;

.field private final f:LX/0YZ;

.field public g:Z


# direct methods
.method public constructor <init>(LX/1MZ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;Ljava/util/concurrent/ExecutorService;LX/290;LX/0So;)V
    .locals 3
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 375646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375647
    new-instance v0, LX/291;

    invoke-direct {v0, p0}, LX/291;-><init>(LX/296;)V

    iput-object v0, p0, LX/296;->f:LX/0YZ;

    .line 375648
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/296;->g:Z

    .line 375649
    iput-object p1, p0, LX/296;->b:LX/1MZ;

    .line 375650
    iput-object p2, p0, LX/296;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 375651
    invoke-interface {p3}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    iget-object v2, p0, LX/296;->f:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 375652
    iput-object p4, p0, LX/296;->c:Ljava/util/concurrent/ExecutorService;

    .line 375653
    iput-object p5, p0, LX/296;->d:LX/290;

    .line 375654
    iput-object p6, p0, LX/296;->e:LX/0So;

    .line 375655
    iget-object v0, p0, LX/296;->b:LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->d()Z

    move-result v0

    iput-boolean v0, p0, LX/296;->g:Z

    .line 375656
    iget-boolean v0, p0, LX/296;->g:Z

    if-eqz v0, :cond_0

    .line 375657
    invoke-static {p0}, LX/296;->a$redex0(LX/296;)V

    .line 375658
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/296;
    .locals 10

    .prologue
    .line 375659
    sget-object v0, LX/296;->h:LX/296;

    if-nez v0, :cond_1

    .line 375660
    const-class v1, LX/296;

    monitor-enter v1

    .line 375661
    :try_start_0
    sget-object v0, LX/296;->h:LX/296;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 375662
    if-eqz v2, :cond_0

    .line 375663
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 375664
    new-instance v3, LX/296;

    invoke-static {v0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v4

    check-cast v4, LX/1MZ;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/290;->b(LX/0QB;)LX/290;

    move-result-object v8

    check-cast v8, LX/290;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v9

    check-cast v9, LX/0So;

    invoke-direct/range {v3 .. v9}, LX/296;-><init>(LX/1MZ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;Ljava/util/concurrent/ExecutorService;LX/290;LX/0So;)V

    .line 375665
    move-object v0, v3

    .line 375666
    sput-object v0, LX/296;->h:LX/296;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375667
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 375668
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 375669
    :cond_1
    sget-object v0, LX/296;->h:LX/296;

    return-object v0

    .line 375670
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 375671
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/296;)V
    .locals 6

    .prologue
    .line 375672
    iget-object v0, p0, LX/296;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2gS;->c:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375673
    iget-object v0, p0, LX/296;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2gS;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 375674
    if-eqz v0, :cond_0

    .line 375675
    iget-object v1, p0, LX/296;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2gS;->c:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 375676
    iget-object v1, p0, LX/296;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 375677
    iget-object v1, p0, LX/296;->d:LX/290;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/290;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 375678
    :cond_0
    return-void
.end method


# virtual methods
.method public final clearUserData()V
    .locals 2

    .prologue
    .line 375679
    iget-object v0, p0, LX/296;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2gS;->c:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 375680
    return-void
.end method
