.class public LX/2Ft;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Fp;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/0Yb;

.field public d:LX/2Fu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 387373
    const-class v0, LX/2Ft;

    sput-object v0, LX/2Ft;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387382
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 387380
    sget-wide v0, LX/0X5;->T:J

    return-wide v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 387377
    iget-object v0, p0, LX/2Ft;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v2, LX/Hlv;

    invoke-direct {v2, p0}, LX/Hlv;-><init>(LX/2Ft;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2Ft;->c:LX/0Yb;

    .line 387378
    iget-object v0, p0, LX/2Ft;->c:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 387379
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 387374
    iget-object v0, p0, LX/2Ft;->c:LX/0Yb;

    if-nez v0, :cond_0

    .line 387375
    :goto_0
    return-void

    .line 387376
    :cond_0
    iget-object v0, p0, LX/2Ft;->c:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    goto :goto_0
.end method
