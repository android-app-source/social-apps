.class public abstract LX/32s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Ay;
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0xe3f25398e43ca8dL


# instance fields
.field public final _isRequired:Z

.field public _managedReferenceName:Ljava/lang/String;

.field public final _nullProvider:LX/4qC;

.field public final _propName:Ljava/lang/String;

.field public _propertyIndex:I

.field public final _type:LX/0lJ;

.field public _valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final _valueTypeDeserializer:LX/4qw;

.field public _viewMatcher:LX/4s1;

.field public final _wrapperName:LX/2Vb;

.field public final transient b:LX/0lQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 491291
    new-instance v0, Lcom/fasterxml/jackson/databind/jsontype/impl/FailingDeserializer;

    const-string v1, "No _valueDeserializer assigned"

    invoke-direct {v0, v1}, Lcom/fasterxml/jackson/databind/jsontype/impl/FailingDeserializer;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/32s;->a:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    return-void
.end method

.method public constructor <init>(LX/2Aq;LX/0lJ;LX/4qw;LX/0lQ;)V
    .locals 7

    .prologue
    .line 491226
    invoke-virtual {p1}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/2Aq;->b()LX/2Vb;

    move-result-object v3

    invoke-virtual {p1}, LX/2Aq;->s()Z

    move-result v6

    move-object v0, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LX/32s;-><init>(Ljava/lang/String;LX/0lJ;LX/2Vb;LX/4qw;LX/0lQ;Z)V

    .line 491227
    return-void
.end method

.method public constructor <init>(LX/32s;)V
    .locals 1

    .prologue
    .line 491228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491229
    const/4 v0, -0x1

    iput v0, p0, LX/32s;->_propertyIndex:I

    .line 491230
    iget-object v0, p1, LX/32s;->_propName:Ljava/lang/String;

    iput-object v0, p0, LX/32s;->_propName:Ljava/lang/String;

    .line 491231
    iget-object v0, p1, LX/32s;->_type:LX/0lJ;

    iput-object v0, p0, LX/32s;->_type:LX/0lJ;

    .line 491232
    iget-object v0, p1, LX/32s;->_wrapperName:LX/2Vb;

    iput-object v0, p0, LX/32s;->_wrapperName:LX/2Vb;

    .line 491233
    iget-boolean v0, p1, LX/32s;->_isRequired:Z

    iput-boolean v0, p0, LX/32s;->_isRequired:Z

    .line 491234
    iget-object v0, p1, LX/32s;->b:LX/0lQ;

    iput-object v0, p0, LX/32s;->b:LX/0lQ;

    .line 491235
    iget-object v0, p1, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iput-object v0, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 491236
    iget-object v0, p1, LX/32s;->_valueTypeDeserializer:LX/4qw;

    iput-object v0, p0, LX/32s;->_valueTypeDeserializer:LX/4qw;

    .line 491237
    iget-object v0, p1, LX/32s;->_nullProvider:LX/4qC;

    iput-object v0, p0, LX/32s;->_nullProvider:LX/4qC;

    .line 491238
    iget-object v0, p1, LX/32s;->_managedReferenceName:Ljava/lang/String;

    iput-object v0, p0, LX/32s;->_managedReferenceName:Ljava/lang/String;

    .line 491239
    iget v0, p1, LX/32s;->_propertyIndex:I

    iput v0, p0, LX/32s;->_propertyIndex:I

    .line 491240
    iget-object v0, p1, LX/32s;->_viewMatcher:LX/4s1;

    iput-object v0, p0, LX/32s;->_viewMatcher:LX/4s1;

    .line 491241
    return-void
.end method

.method public constructor <init>(LX/32s;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/32s;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 491242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491243
    const/4 v1, -0x1

    iput v1, p0, LX/32s;->_propertyIndex:I

    .line 491244
    iget-object v1, p1, LX/32s;->_propName:Ljava/lang/String;

    iput-object v1, p0, LX/32s;->_propName:Ljava/lang/String;

    .line 491245
    iget-object v1, p1, LX/32s;->_type:LX/0lJ;

    iput-object v1, p0, LX/32s;->_type:LX/0lJ;

    .line 491246
    iget-object v1, p1, LX/32s;->_wrapperName:LX/2Vb;

    iput-object v1, p0, LX/32s;->_wrapperName:LX/2Vb;

    .line 491247
    iget-boolean v1, p1, LX/32s;->_isRequired:Z

    iput-boolean v1, p0, LX/32s;->_isRequired:Z

    .line 491248
    iget-object v1, p1, LX/32s;->b:LX/0lQ;

    iput-object v1, p0, LX/32s;->b:LX/0lQ;

    .line 491249
    iget-object v1, p1, LX/32s;->_valueTypeDeserializer:LX/4qw;

    iput-object v1, p0, LX/32s;->_valueTypeDeserializer:LX/4qw;

    .line 491250
    iget-object v1, p1, LX/32s;->_managedReferenceName:Ljava/lang/String;

    iput-object v1, p0, LX/32s;->_managedReferenceName:Ljava/lang/String;

    .line 491251
    iget v1, p1, LX/32s;->_propertyIndex:I

    iput v1, p0, LX/32s;->_propertyIndex:I

    .line 491252
    if-nez p2, :cond_0

    .line 491253
    iput-object v0, p0, LX/32s;->_nullProvider:LX/4qC;

    .line 491254
    sget-object v0, LX/32s;->a:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iput-object v0, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 491255
    :goto_0
    iget-object v0, p1, LX/32s;->_viewMatcher:LX/4s1;

    iput-object v0, p0, LX/32s;->_viewMatcher:LX/4s1;

    .line 491256
    return-void

    .line 491257
    :cond_0
    invoke-virtual {p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->getNullValue()Ljava/lang/Object;

    move-result-object v1

    .line 491258
    if-nez v1, :cond_1

    :goto_1
    iput-object v0, p0, LX/32s;->_nullProvider:LX/4qC;

    .line 491259
    iput-object p2, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    goto :goto_0

    .line 491260
    :cond_1
    new-instance v0, LX/4qC;

    iget-object v2, p0, LX/32s;->_type:LX/0lJ;

    invoke-direct {v0, v2, v1}, LX/4qC;-><init>(LX/0lJ;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public constructor <init>(LX/32s;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 491261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491262
    const/4 v0, -0x1

    iput v0, p0, LX/32s;->_propertyIndex:I

    .line 491263
    iput-object p2, p0, LX/32s;->_propName:Ljava/lang/String;

    .line 491264
    iget-object v0, p1, LX/32s;->_type:LX/0lJ;

    iput-object v0, p0, LX/32s;->_type:LX/0lJ;

    .line 491265
    iget-object v0, p1, LX/32s;->_wrapperName:LX/2Vb;

    iput-object v0, p0, LX/32s;->_wrapperName:LX/2Vb;

    .line 491266
    iget-boolean v0, p1, LX/32s;->_isRequired:Z

    iput-boolean v0, p0, LX/32s;->_isRequired:Z

    .line 491267
    iget-object v0, p1, LX/32s;->b:LX/0lQ;

    iput-object v0, p0, LX/32s;->b:LX/0lQ;

    .line 491268
    iget-object v0, p1, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iput-object v0, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 491269
    iget-object v0, p1, LX/32s;->_valueTypeDeserializer:LX/4qw;

    iput-object v0, p0, LX/32s;->_valueTypeDeserializer:LX/4qw;

    .line 491270
    iget-object v0, p1, LX/32s;->_nullProvider:LX/4qC;

    iput-object v0, p0, LX/32s;->_nullProvider:LX/4qC;

    .line 491271
    iget-object v0, p1, LX/32s;->_managedReferenceName:Ljava/lang/String;

    iput-object v0, p0, LX/32s;->_managedReferenceName:Ljava/lang/String;

    .line 491272
    iget v0, p1, LX/32s;->_propertyIndex:I

    iput v0, p0, LX/32s;->_propertyIndex:I

    .line 491273
    iget-object v0, p1, LX/32s;->_viewMatcher:LX/4s1;

    iput-object v0, p0, LX/32s;->_viewMatcher:LX/4s1;

    .line 491274
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0lJ;LX/2Vb;LX/4qw;LX/0lQ;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 491275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491276
    const/4 v0, -0x1

    iput v0, p0, LX/32s;->_propertyIndex:I

    .line 491277
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 491278
    :cond_0
    const-string v0, ""

    iput-object v0, p0, LX/32s;->_propName:Ljava/lang/String;

    .line 491279
    :goto_0
    iput-object p2, p0, LX/32s;->_type:LX/0lJ;

    .line 491280
    iput-object p3, p0, LX/32s;->_wrapperName:LX/2Vb;

    .line 491281
    iput-boolean p6, p0, LX/32s;->_isRequired:Z

    .line 491282
    iput-object p5, p0, LX/32s;->b:LX/0lQ;

    .line 491283
    iput-object v1, p0, LX/32s;->_viewMatcher:LX/4s1;

    .line 491284
    iput-object v1, p0, LX/32s;->_nullProvider:LX/4qC;

    .line 491285
    if-eqz p4, :cond_1

    .line 491286
    invoke-virtual {p4, p0}, LX/4qw;->a(LX/2Ay;)LX/4qw;

    move-result-object p4

    .line 491287
    :cond_1
    iput-object p4, p0, LX/32s;->_valueTypeDeserializer:LX/4qw;

    .line 491288
    sget-object v0, LX/32s;->a:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iput-object v0, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 491289
    return-void

    .line 491290
    :cond_2
    sget-object v0, LX/16J;->a:LX/16J;

    invoke-virtual {v0, p1}, LX/16J;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/32s;->_propName:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Exception;)Ljava/io/IOException;
    .locals 3

    .prologue
    .line 491201
    instance-of v0, p0, Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 491202
    check-cast p0, Ljava/io/IOException;

    throw p0

    .line 491203
    :cond_0
    instance-of v0, p0, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_1

    .line 491204
    check-cast p0, Ljava/lang/RuntimeException;

    throw p0

    .line 491205
    :cond_1
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 491206
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    .line 491207
    :cond_2
    new-instance v0, LX/28E;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method public final a()LX/0lJ;
    .locals 1

    .prologue
    .line 491292
    iget-object v0, p0, LX/32s;->_type:LX/0lJ;

    return-object v0
.end method

.method public abstract a(Ljava/lang/String;)LX/32s;
.end method

.method public final a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 491293
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 491294
    sget-object v1, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v0, v1, :cond_1

    .line 491295
    iget-object v0, p0, LX/32s;->_nullProvider:LX/4qC;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 491296
    :goto_0
    return-object v0

    .line 491297
    :cond_0
    iget-object v0, p0, LX/32s;->_nullProvider:LX/4qC;

    invoke-virtual {v0, p2}, LX/4qC;->a(LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 491298
    :cond_1
    iget-object v0, p0, LX/32s;->_valueTypeDeserializer:LX/4qw;

    if-eqz v0, :cond_2

    .line 491299
    iget-object v0, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iget-object v1, p0, LX/32s;->_valueTypeDeserializer:LX/4qw;

    invoke-virtual {v0, p1, p2, v1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserializeWithType(LX/15w;LX/0n3;LX/4qw;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 491300
    :cond_2
    iget-object v0, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 491301
    iget v0, p0, LX/32s;->_propertyIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 491302
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Property \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 491303
    iget-object v2, p0, LX/32s;->_propName:Ljava/lang/String;

    move-object v2, v2

    .line 491304
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' already had index ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/32s;->_propertyIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), trying to assign "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 491305
    :cond_0
    iput p1, p0, LX/32s;->_propertyIndex:I

    .line 491306
    return-void
.end method

.method public abstract a(LX/15w;LX/0n3;Ljava/lang/Object;)V
.end method

.method public final a(Ljava/lang/Exception;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 491307
    instance-of v0, p1, Ljava/lang/IllegalArgumentException;

    if-eqz v0, :cond_2

    .line 491308
    if-nez p2, :cond_0

    const-string v0, "[NULL]"

    .line 491309
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Problem deserializing property \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 491310
    iget-object v2, p0, LX/32s;->_propName:Ljava/lang/String;

    move-object v2, v2

    .line 491311
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 491312
    const-string v2, "\' (expected type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, LX/32s;->a()LX/0lJ;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 491313
    const-string v2, "; actual type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 491314
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 491315
    if-eqz v0, :cond_1

    .line 491316
    const-string v2, ", problem: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 491317
    :goto_1
    new-instance v0, LX/28E;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p1}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v0

    .line 491318
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 491319
    :cond_1
    const-string v0, " (no error message provided)"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 491320
    :cond_2
    invoke-static {p1}, LX/32s;->a(Ljava/lang/Exception;)Ljava/io/IOException;

    .line 491321
    return-void
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method public final a([Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 491222
    if-nez p1, :cond_0

    .line 491223
    const/4 v0, 0x0

    iput-object v0, p0, LX/32s;->_viewMatcher:LX/4s1;

    .line 491224
    :goto_0
    return-void

    .line 491225
    :cond_0
    invoke-static {p1}, LX/4s1;->a([Ljava/lang/Class;)LX/4s1;

    move-result-object v0

    iput-object v0, p0, LX/32s;->_viewMatcher:LX/4s1;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 491221
    iget-object v0, p0, LX/32s;->_viewMatcher:LX/4s1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/32s;->_viewMatcher:LX/4s1;

    invoke-virtual {v0, p1}, LX/4s1;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b()LX/2An;
.end method

.method public abstract b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "LX/32s;"
        }
    .end annotation
.end method

.method public abstract b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public c()I
    .locals 1

    .prologue
    .line 491220
    const/4 v0, -0x1

    return v0
.end method

.method public d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 491219
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 491218
    invoke-virtual {p0}, LX/32s;->b()LX/2An;

    move-result-object v0

    invoke-virtual {v0}, LX/2An;->i()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 491217
    iget-object v0, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    sget-object v1, LX/32s;->a:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 491216
    iget-object v0, p0, LX/32s;->_valueTypeDeserializer:LX/4qw;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 491212
    iget-object v0, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 491213
    sget-object v1, LX/32s;->a:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-ne v0, v1, :cond_0

    .line 491214
    const/4 v0, 0x0

    .line 491215
    :cond_0
    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 491211
    iget-object v0, p0, LX/32s;->_viewMatcher:LX/4s1;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 491208
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[property \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 491209
    iget-object v1, p0, LX/32s;->_propName:Ljava/lang/String;

    move-object v1, v1

    .line 491210
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\']"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
