.class public LX/3BW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/location/LocationListener;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/location/LocationManager;

.field public d:Landroid/location/Location;

.field public e:Z

.field public f:LX/67t;

.field public g:LX/3BW;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 528679
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, LX/3BW;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 528675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528676
    iput-object p1, p0, LX/3BW;->b:Landroid/content/Context;

    .line 528677
    iget-object v0, p0, LX/3BW;->b:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, LX/3BW;->c:Landroid/location/LocationManager;

    .line 528678
    return-void
.end method

.method public static a(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 528654
    if-nez p0, :cond_1

    .line 528655
    :cond_0
    :goto_0
    return v2

    .line 528656
    :cond_1
    if-nez p1, :cond_2

    move v2, v1

    .line 528657
    goto :goto_0

    .line 528658
    :cond_2
    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 528659
    const-wide/32 v6, 0x1d4c0

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    move v2, v1

    .line 528660
    goto :goto_0

    .line 528661
    :cond_3
    const-wide/32 v6, -0x1d4c0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    .line 528662
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_5

    move v0, v1

    .line 528663
    :goto_1
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    .line 528664
    if-gtz v3, :cond_6

    move v5, v1

    .line 528665
    :goto_2
    const/16 v4, 0xc8

    if-le v3, v4, :cond_7

    move v3, v1

    .line 528666
    :goto_3
    invoke-virtual {p0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v4

    .line 528667
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v6

    .line 528668
    if-nez v4, :cond_9

    if-nez v6, :cond_8

    move v4, v1

    .line 528669
    :goto_4
    if-nez v5, :cond_4

    if-eqz v0, :cond_0

    if-nez v3, :cond_0

    if-eqz v4, :cond_0

    :cond_4
    move v2, v1

    .line 528670
    goto :goto_0

    :cond_5
    move v0, v2

    .line 528671
    goto :goto_1

    :cond_6
    move v5, v2

    .line 528672
    goto :goto_2

    :cond_7
    move v3, v2

    .line 528673
    goto :goto_3

    :cond_8
    move v4, v2

    .line 528674
    goto :goto_4

    :cond_9
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto :goto_4
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 528635
    iget-object v0, p0, LX/3BW;->c:Landroid/location/LocationManager;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v1

    .line 528636
    iget-object v0, p0, LX/3BW;->d:Landroid/location/Location;

    .line 528637
    if-eqz v1, :cond_0

    .line 528638
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 528639
    iget-object v3, p0, LX/3BW;->c:Landroid/location/LocationManager;

    invoke-virtual {v3, v0}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 528640
    invoke-static {v0, v1}, LX/3BW;->a(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v3

    if-eqz v3, :cond_4

    :goto_1
    move-object v1, v0

    .line 528641
    goto :goto_0

    :cond_0
    move-object v1, v0

    .line 528642
    :cond_1
    if-eqz v1, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x6ddd00

    cmp-long v0, v2, v4

    if-gez v0, :cond_3

    .line 528643
    iput-object v1, p0, LX/3BW;->d:Landroid/location/Location;

    .line 528644
    iget-object v0, p0, LX/3BW;->f:LX/67t;

    if-eqz v0, :cond_2

    .line 528645
    iget-object v0, p0, LX/3BW;->f:LX/67t;

    iget-object v1, p0, LX/3BW;->d:Landroid/location/Location;

    invoke-interface {v0, v1}, LX/67t;->a(Landroid/location/Location;)V

    .line 528646
    :cond_2
    :goto_2
    new-instance v6, Landroid/location/Criteria;

    invoke-direct {v6}, Landroid/location/Criteria;-><init>()V

    .line 528647
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {v6, v0}, Landroid/location/Criteria;->setAccuracy(I)V

    .line 528648
    iget-object v0, p0, LX/3BW;->c:Landroid/location/LocationManager;

    iget-object v1, p0, LX/3BW;->c:Landroid/location/LocationManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v6, v2}, Landroid/location/LocationManager;->getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x32

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 528649
    :goto_3
    const/4 v0, 0x2

    :try_start_1
    invoke-virtual {v6, v0}, Landroid/location/Criteria;->setAccuracy(I)V

    .line 528650
    iget-object v0, p0, LX/3BW;->c:Landroid/location/LocationManager;

    iget-object v1, p0, LX/3BW;->c:Landroid/location/LocationManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v6, v2}, Landroid/location/LocationManager;->getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x32

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 528651
    :goto_4
    return-void

    .line 528652
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, LX/3BW;->d:Landroid/location/Location;

    goto :goto_2

    .line 528653
    :catch_0
    goto :goto_4

    :catch_1
    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 528680
    iget-object v0, p0, LX/3BW;->g:LX/3BW;

    if-nez v0, :cond_0

    .line 528681
    iput-object p0, p0, LX/3BW;->g:LX/3BW;

    .line 528682
    :cond_0
    iput-boolean p1, p0, LX/3BW;->e:Z

    .line 528683
    if-eqz p1, :cond_4

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_4

    iget-object v0, p0, LX/3BW;->b:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/3BW;->b:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    .line 528684
    :cond_1
    iget-object v0, p0, LX/3BW;->b:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_2

    .line 528685
    sget-object v0, LX/31U;->v:LX/31U;

    const-string v1, "Context is not an instance of activity"

    invoke-virtual {v0, v1}, LX/31U;->a(Ljava/lang/String;)V

    .line 528686
    :goto_0
    return-void

    .line 528687
    :cond_2
    sget-object v0, LX/3BU;->d:LX/31d;

    if-nez v0, :cond_3

    .line 528688
    sget-object v0, LX/31U;->v:LX/31U;

    const-string v1, "Runtime permission manager not provided"

    invoke-virtual {v0, v1}, LX/31U;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 528689
    :cond_3
    sget-object v1, LX/3BU;->d:LX/31d;

    iget-object v0, p0, LX/3BW;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    sget-object v2, LX/3BW;->a:[Ljava/lang/String;

    new-instance v3, LX/68q;

    invoke-direct {v3, p0}, LX/68q;-><init>(LX/3BW;)V

    .line 528690
    iget-object p0, v1, LX/31d;->b:LX/0i4;

    invoke-virtual {p0, v0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object p0

    .line 528691
    new-instance p1, LX/6aI;

    invoke-direct {p1, v1, v3}, LX/6aI;-><init>(LX/31d;LX/68q;)V

    invoke-virtual {p0, v2, p1}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 528692
    goto :goto_0

    .line 528693
    :cond_4
    iget-boolean v0, p0, LX/3BW;->e:Z

    if-eqz v0, :cond_5

    .line 528694
    iget-object v0, p0, LX/3BW;->g:LX/3BW;

    invoke-virtual {v0}, LX/3BW;->a()V

    goto :goto_0

    .line 528695
    :cond_5
    iget-object v0, p0, LX/3BW;->g:LX/3BW;

    invoke-virtual {v0}, LX/3BW;->b()V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 528633
    iget-object v0, p0, LX/3BW;->c:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 528634
    return-void
.end method

.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 528628
    iget-object v0, p0, LX/3BW;->d:Landroid/location/Location;

    invoke-static {p1, v0}, LX/3BW;->a(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528629
    iput-object p1, p0, LX/3BW;->d:Landroid/location/Location;

    .line 528630
    iget-object v0, p0, LX/3BW;->f:LX/67t;

    if-eqz v0, :cond_0

    .line 528631
    iget-object v0, p0, LX/3BW;->f:LX/67t;

    iget-object v1, p0, LX/3BW;->d:Landroid/location/Location;

    invoke-interface {v0, v1}, LX/67t;->a(Landroid/location/Location;)V

    .line 528632
    :cond_0
    return-void
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 528627
    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 528626
    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 528625
    return-void
.end method
