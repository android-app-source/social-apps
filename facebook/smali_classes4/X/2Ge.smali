.class public final enum LX/2Ge;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Ge;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Ge;

.field public static final enum ADM:LX/2Ge;

.field public static final enum FBNS:LX/2Ge;

.field public static final enum FBNS_LITE:LX/2Ge;

.field public static final enum GCM:LX/2Ge;

.field public static final enum NNA:LX/2Ge;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 389032
    new-instance v0, LX/2Ge;

    const-string v1, "ADM"

    invoke-direct {v0, v1, v2}, LX/2Ge;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ge;->ADM:LX/2Ge;

    .line 389033
    new-instance v0, LX/2Ge;

    const-string v1, "NNA"

    invoke-direct {v0, v1, v3}, LX/2Ge;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ge;->NNA:LX/2Ge;

    .line 389034
    new-instance v0, LX/2Ge;

    const-string v1, "GCM"

    invoke-direct {v0, v1, v4}, LX/2Ge;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ge;->GCM:LX/2Ge;

    .line 389035
    new-instance v0, LX/2Ge;

    const-string v1, "FBNS"

    invoke-direct {v0, v1, v5}, LX/2Ge;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ge;->FBNS:LX/2Ge;

    .line 389036
    new-instance v0, LX/2Ge;

    const-string v1, "FBNS_LITE"

    invoke-direct {v0, v1, v6}, LX/2Ge;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ge;->FBNS_LITE:LX/2Ge;

    .line 389037
    const/4 v0, 0x5

    new-array v0, v0, [LX/2Ge;

    sget-object v1, LX/2Ge;->ADM:LX/2Ge;

    aput-object v1, v0, v2

    sget-object v1, LX/2Ge;->NNA:LX/2Ge;

    aput-object v1, v0, v3

    sget-object v1, LX/2Ge;->GCM:LX/2Ge;

    aput-object v1, v0, v4

    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    aput-object v1, v0, v5

    sget-object v1, LX/2Ge;->FBNS_LITE:LX/2Ge;

    aput-object v1, v0, v6

    sput-object v0, LX/2Ge;->$VALUES:[LX/2Ge;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 389038
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Ge;
    .locals 1

    .prologue
    .line 389039
    const-class v0, LX/2Ge;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Ge;

    return-object v0
.end method

.method public static values()[LX/2Ge;
    .locals 1

    .prologue
    .line 389040
    sget-object v0, LX/2Ge;->$VALUES:[LX/2Ge;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Ge;

    return-object v0
.end method
