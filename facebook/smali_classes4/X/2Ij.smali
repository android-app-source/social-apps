.class public LX/2Ij;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2Ij;


# instance fields
.field private final a:LX/1GD;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ez;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ez;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ez;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1GD;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/imagepipeline/module/NativeMemoryChunkPoolStatsTracker;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/imagepipeline/module/SmallByteArrayPoolStatsTracker;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/imagepipeline/module/BitmapPoolStatsTracker;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1GD;",
            "LX/0Or",
            "<",
            "LX/1Ez;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1Ez;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1Ez;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392108
    iput-object p1, p0, LX/2Ij;->a:LX/1GD;

    .line 392109
    iput-object p2, p0, LX/2Ij;->b:LX/0Or;

    .line 392110
    iput-object p3, p0, LX/2Ij;->c:LX/0Or;

    .line 392111
    iput-object p4, p0, LX/2Ij;->d:LX/0Or;

    .line 392112
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ij;
    .locals 7

    .prologue
    .line 392113
    sget-object v0, LX/2Ij;->e:LX/2Ij;

    if-nez v0, :cond_1

    .line 392114
    const-class v1, LX/2Ij;

    monitor-enter v1

    .line 392115
    :try_start_0
    sget-object v0, LX/2Ij;->e:LX/2Ij;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 392116
    if-eqz v2, :cond_0

    .line 392117
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 392118
    new-instance v4, LX/2Ij;

    invoke-static {v0}, LX/1GD;->a(LX/0QB;)LX/1GD;

    move-result-object v3

    check-cast v3, LX/1GD;

    const/16 v5, 0xba3

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0xba4

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0xba2

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, v6, p0}, LX/2Ij;-><init>(LX/1GD;LX/0Or;LX/0Or;LX/0Or;)V

    .line 392119
    move-object v0, v4

    .line 392120
    sput-object v0, LX/2Ij;->e:LX/2Ij;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392121
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 392122
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392123
    :cond_1
    sget-object v0, LX/2Ij;->e:LX/2Ij;

    return-object v0

    .line 392124
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 392125
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 2

    .prologue
    .line 392126
    monitor-enter p0

    :try_start_0
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "image_pipeline_counters"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 392127
    iget-object v0, p0, LX/2Ij;->a:LX/1GD;

    invoke-virtual {v0, v1}, LX/1GD;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 392128
    iget-object v0, p0, LX/2Ij;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ez;

    invoke-interface {v0, v1}, LX/1Ez;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 392129
    iget-object v0, p0, LX/2Ij;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ez;

    invoke-interface {v0, v1}, LX/1Ez;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 392130
    iget-object v0, p0, LX/2Ij;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ez;

    invoke-interface {v0, v1}, LX/1Ez;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 392131
    const-string v0, "image_pipeline"

    .line 392132
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 392133
    monitor-exit p0

    return-object v1

    .line 392134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
