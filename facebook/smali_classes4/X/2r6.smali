.class public final LX/2r6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/0g2;


# direct methods
.method public constructor <init>(LX/0g2;)V
    .locals 0

    .prologue
    .line 471711
    iput-object p1, p0, LX/2r6;->a:LX/0g2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x5a49bfb5

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 471712
    iget-object v1, p0, LX/2r6;->a:LX/0g2;

    .line 471713
    iget-object v3, v1, LX/0g2;->S:LX/1NJ;

    .line 471714
    iget p0, v3, LX/1NJ;->k:I

    if-ltz p0, :cond_0

    iget p0, v3, LX/1NJ;->k:I

    iget-object p1, v3, LX/1NJ;->h:LX/1UQ;

    invoke-interface {p1}, LX/1Qr;->d()I

    move-result p1

    if-ge p0, p1, :cond_0

    iget-object p0, v3, LX/1NJ;->h:LX/1UQ;

    if-eqz p0, :cond_0

    iget-object p0, v3, LX/1NJ;->i:LX/0g5;

    if-nez p0, :cond_1

    .line 471715
    :cond_0
    sget-object p0, LX/1lq;->HideNSBP:LX/1lq;

    invoke-virtual {v3, p0}, LX/1NJ;->a(LX/1lq;)V

    .line 471716
    :goto_0
    const v1, -0x5da5d0a4

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 471717
    :cond_1
    iget-object p0, v3, LX/1NJ;->h:LX/1UQ;

    iget p1, v3, LX/1NJ;->k:I

    invoke-interface {p0, p1}, LX/1Qr;->m_(I)I

    move-result p0

    .line 471718
    iget-object p1, v3, LX/1NJ;->h:LX/1UQ;

    iget v1, v3, LX/1NJ;->k:I

    invoke-interface {p1, v1}, LX/1Qr;->n_(I)I

    move-result p1

    .line 471719
    iget-object v1, v3, LX/1NJ;->i:LX/0g5;

    sub-int/2addr p1, p0

    div-int/lit8 p1, p1, 0x2

    add-int/2addr p0, p1

    invoke-virtual {v1, p0}, LX/0g7;->d(I)V

    .line 471720
    const-string p0, "fresh_feed_new_stories_below_pill_tapped"

    invoke-static {v3, p0}, LX/1NJ;->a(LX/1NJ;Ljava/lang/String;)V

    .line 471721
    sget-object p0, LX/0rj;->NEW_STORIES_BELOW_PILL_TAPPED:LX/0rj;

    invoke-static {v3, p0}, LX/1NJ;->a(LX/1NJ;LX/0rj;)V

    .line 471722
    sget-object p0, LX/1lq;->HideNSBP:LX/1lq;

    invoke-virtual {v3, p0}, LX/1NJ;->a(LX/1lq;)V

    goto :goto_0
.end method
