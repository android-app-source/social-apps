.class public final LX/234;
.super LX/22z;
.source ""


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Lcom/facebook/api/feed/FetchFeedParams;

.field public final synthetic c:LX/0rS;

.field public final synthetic d:Z

.field public final synthetic e:Lcom/facebook/feed/data/FeedDataLoader;

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feed/data/FeedDataLoader;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/api/feed/FetchFeedParams;LX/0rS;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 361765
    iput-object p1, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iput-object p2, p0, LX/234;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    iput-object p4, p0, LX/234;->c:LX/0rS;

    iput-boolean p5, p0, LX/234;->d:Z

    invoke-direct {p0}, LX/22z;-><init>()V

    .line 361766
    iput v0, p0, LX/234;->f:I

    .line 361767
    iput-boolean v0, p0, LX/234;->g:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    .line 361768
    const-string v0, "FeedDataLoader.loadAfterDataSuccess"

    const v1, -0x23943753

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 361769
    :try_start_0
    iget-object v0, p0, LX/234;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    const v2, -0x4101f462

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 361770
    iget-object v0, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361771
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v1

    .line 361772
    sget-object v1, LX/0gf;->INITIALIZATION_RERANK:LX/0gf;

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/234;->f:I

    if-nez v0, :cond_0

    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-boolean v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->x:Z

    if-nez v0, :cond_0

    .line 361773
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->T:LX/0pW;

    .line 361774
    iget-object v1, v0, LX/0pW;->a:LX/0Yi;

    .line 361775
    sget-object v0, LX/0ao;->ASYNC_NO_CACHE:LX/0ao;

    invoke-static {v1, v0}, LX/0Yi;->a(LX/0Yi;LX/0ao;)V

    .line 361776
    :cond_0
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 361777
    iget-object v2, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361778
    iget-object v3, v2, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v2, v3

    .line 361779
    sget-object v3, LX/0gf;->SKIP_TAIL_GAP:LX/0gf;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, LX/234;->c:LX/0rS;

    sget-object v3, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-eq v2, v3, :cond_2

    .line 361780
    :cond_1
    iget-object v2, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-static {v2}, Lcom/facebook/feed/data/FeedDataLoader;->T(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 361781
    :cond_2
    iget-object v2, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-static {v2}, Lcom/facebook/feed/data/FeedDataLoader;->V(Lcom/facebook/feed/data/FeedDataLoader;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 361782
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/23E;->AFTER:LX/23E;

    invoke-static {v0, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;LX/23E;)V

    .line 361783
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-static {v0}, Lcom/facebook/feed/data/FeedDataLoader;->Y(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 361784
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/0rj;->TAIL_FETCH_BACKGROUND_SUCCEED:LX/0rj;

    invoke-virtual {v0, v1}, LX/0gD;->a(LX/0rj;)V

    .line 361785
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    const/4 v1, 0x0

    .line 361786
    iput v1, v0, Lcom/facebook/feed/data/FeedDataLoader;->C:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361787
    const v0, 0x6c593c44

    invoke-static {v0}, LX/02m;->a(I)V

    .line 361788
    :goto_0
    return-void

    .line 361789
    :cond_3
    :try_start_1
    iget-object v2, p0, LX/234;->c:LX/0rS;

    sget-object v3, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne v2, v3, :cond_4

    iget v2, p0, LX/234;->f:I

    if-nez v2, :cond_4

    .line 361790
    iget-object v2, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v2, v2, Lcom/facebook/feed/data/FeedDataLoader;->ah:LX/22j;

    iget-object v3, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361791
    iget-boolean v4, v3, Lcom/facebook/api/feed/FetchFeedParams;->m:Z

    move v3, v4

    .line 361792
    const/4 v4, 0x1

    .line 361793
    iput-boolean v4, v2, LX/22j;->b:Z

    .line 361794
    if-eqz v3, :cond_4

    .line 361795
    iput-boolean v4, v2, LX/22j;->c:Z

    .line 361796
    :cond_4
    iget v2, p0, LX/234;->f:I

    const/4 v6, 0x0

    .line 361797
    if-lez v2, :cond_e

    .line 361798
    iget-object v7, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    .line 361799
    iput v6, v7, Lcom/facebook/feed/data/FeedDataLoader;->C:I

    .line 361800
    :cond_5
    :goto_1
    move v2, v6

    .line 361801
    if-eqz v2, :cond_6

    .line 361802
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Too many tail fetches returned empty results, aborting."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/234;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361803
    const v0, -0x2bb6cc43

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 361804
    :cond_6
    :try_start_2
    iget-object v2, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v3, LX/0rj;->TAIL_FETCH_SUCCEED:LX/0rj;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Freshness:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/234;->c:LX/0rS;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gD;->a(LX/0rj;Ljava/lang/String;)V

    .line 361805
    const/4 v2, 0x1

    move v2, v2

    .line 361806
    if-eqz v2, :cond_b

    iget-object v2, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361807
    iget-object v3, v2, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v2, v3

    .line 361808
    sget-object v3, LX/0gf;->SKIP_TAIL_GAP:LX/0gf;

    if-ne v2, v3, :cond_b

    .line 361809
    iget v2, p0, LX/234;->f:I

    if-nez v2, :cond_8

    .line 361810
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/0rj;->TAIL_FETCH_SKIP_FINISHED_WITH_NO_RESULTS:LX/0rj;

    invoke-virtual {v0, v1}, LX/0gD;->a(LX/0rj;)V

    .line 361811
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->aw:LX/22w;

    invoke-virtual {v0}, LX/22w;->finishedWithDataOnce()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 361812
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/22w;->FINISHED_WITH_DATA_AT_LEAST_ONCE:LX/22w;

    .line 361813
    iput-object v1, v0, Lcom/facebook/feed/data/FeedDataLoader;->aw:LX/22w;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 361814
    :goto_2
    const v0, -0x71ccb86b

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 361815
    :cond_7
    :try_start_3
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/22w;->FINISHED_WITH_NO_DATA:LX/22w;

    .line 361816
    iput-object v1, v0, Lcom/facebook/feed/data/FeedDataLoader;->aw:LX/22w;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 361817
    goto :goto_2

    .line 361818
    :catchall_0
    move-exception v0

    const v1, -0x632ead30

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 361819
    :cond_8
    :try_start_4
    iget-object v2, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v3, LX/0rj;->TAIL_FETCH_SKIP_FINISHED_WITH_RESULTS:LX/0rj;

    invoke-virtual {v2, v3}, LX/0gD;->a(LX/0rj;)V

    .line 361820
    iget-object v2, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v3, LX/22w;->FINISHED_WITH_DATA:LX/22w;

    .line 361821
    iput-object v3, v2, Lcom/facebook/feed/data/FeedDataLoader;->aw:LX/22w;

    .line 361822
    :goto_3
    iget-boolean v2, p0, LX/234;->d:Z

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/234;->c:LX/0rS;

    sget-object v3, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne v2, v3, :cond_c

    iget v2, p0, LX/234;->f:I

    if-nez v2, :cond_c

    .line 361823
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-virtual {v0}, Lcom/facebook/feed/data/FeedDataLoader;->m()V

    .line 361824
    :cond_9
    :goto_4
    iget-boolean v0, p0, LX/234;->g:Z

    if-nez v0, :cond_a

    .line 361825
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->al:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rW;

    iget-object v1, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361826
    iget-object v2, v1, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    move-object v1, v2

    .line 361827
    iget-object v2, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361828
    iget-object v3, v2, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    move-object v2, v3

    .line 361829
    invoke-virtual {v0, v1, v2}, LX/0rW;->a(LX/0Px;LX/0Px;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 361830
    :cond_a
    const v0, 0x888c50d

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 361831
    :cond_b
    :try_start_5
    iget-object v2, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-static {v2}, Lcom/facebook/feed/data/FeedDataLoader;->Y(Lcom/facebook/feed/data/FeedDataLoader;)V

    goto :goto_3

    .line 361832
    :cond_c
    iget-object v2, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a(J)Z

    .line 361833
    iget-object v0, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361834
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v1

    .line 361835
    sget-object v1, LX/0gf;->INITIALIZATION:LX/0gf;

    if-eq v0, v1, :cond_d

    iget-object v0, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361836
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v1

    .line 361837
    sget-object v1, LX/0gf;->INITIALIZATION_RERANK:LX/0gf;

    if-ne v0, v1, :cond_9

    .line 361838
    :cond_d
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-static {v0}, Lcom/facebook/feed/data/FeedDataLoader;->P(Lcom/facebook/feed/data/FeedDataLoader;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    .line 361839
    :cond_e
    iget-object v7, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    .line 361840
    iget v8, v7, Lcom/facebook/feed/data/FeedDataLoader;->C:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lcom/facebook/feed/data/FeedDataLoader;->C:I

    .line 361841
    iget-object v7, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v7, v7, Lcom/facebook/feed/data/FeedDataLoader;->ab:LX/0Zb;

    const-string v8, "feed_fetch_no_results_tail"

    invoke-interface {v7, v8, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v7

    .line 361842
    invoke-virtual {v7}, LX/0oG;->a()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 361843
    const-string v8, "freshness"

    iget-object v9, p0, LX/234;->c:LX/0rS;

    invoke-virtual {v9}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 361844
    invoke-virtual {v7}, LX/0oG;->d()V

    .line 361845
    :cond_f
    iget-object v7, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget v7, v7, Lcom/facebook/feed/data/FeedDataLoader;->C:I

    iget-object v8, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v8, v8, LX/0gD;->a:LX/0oy;

    .line 361846
    iget v10, v8, LX/0oy;->E:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_10

    .line 361847
    iget-object v10, v8, LX/0oy;->a:LX/0W3;

    sget-wide v12, LX/0X5;->gF:J

    const/16 v11, 0xa

    invoke-interface {v10, v12, v13, v11}, LX/0W4;->a(JI)I

    move-result v10

    iput v10, v8, LX/0oy;->E:I

    .line 361848
    :cond_10
    iget v10, v8, LX/0oy;->E:I

    move v8, v10

    .line 361849
    if-lt v7, v8, :cond_5

    iget-object v7, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    .line 361850
    iget-object v8, v7, LX/0gD;->q:LX/0Uh;

    move-object v7, v8

    .line 361851
    const/16 v8, 0x457

    invoke-virtual {v7, v8, v6}, LX/0Uh;->a(IZ)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v6, 0x1

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 361852
    check-cast p1, Lcom/facebook/api/feed/FetchFeedResult;

    .line 361853
    iget v0, p0, LX/234;->f:I

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/234;->f:I

    .line 361854
    iget-boolean v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    move v0, v0

    .line 361855
    iput-boolean v0, p0, LX/234;->g:Z

    .line 361856
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v0}, LX/0r0;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    .line 361857
    iget-object v1, v0, LX/0r8;->d:LX/230;

    move-object v0, v1

    .line 361858
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    .line 361859
    iget-object v1, v0, LX/0r8;->d:LX/230;

    move-object v0, v1

    .line 361860
    iget-boolean v1, v0, LX/230;->g:Z

    move v0, v1

    .line 361861
    if-eqz v0, :cond_1

    .line 361862
    :cond_0
    :goto_0
    return-void

    .line 361863
    :cond_1
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-static {v0}, Lcom/facebook/feed/data/FeedDataLoader;->V(Lcom/facebook/feed/data/FeedDataLoader;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 361864
    iget-object v0, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/234;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361865
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v1

    .line 361866
    sget-object v1, LX/0gf;->INITIALIZATION_RERANK:LX/0gf;

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-boolean v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->x:Z

    if-nez v0, :cond_2

    .line 361867
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 361868
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->k()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 361869
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->T:LX/0pW;

    .line 361870
    iget-object v1, v0, LX/0pW;->a:LX/0Yi;

    .line 361871
    sget-object v0, LX/0ao;->ASYNC_CACHE_UNSEEN_STORY:LX/0ao;

    invoke-static {v1, v0}, LX/0Yi;->a(LX/0Yi;LX/0ao;)V

    .line 361872
    :cond_2
    :goto_1
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v2, LX/23E;->AFTER:LX/23E;

    const/4 v3, 0x1

    iget-object v1, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v1, v1, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    move-object v1, p1

    .line 361873
    invoke-static/range {v0 .. v5}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedResult;LX/23E;ZJ)V

    .line 361874
    goto :goto_0

    .line 361875
    :cond_3
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->T:LX/0pW;

    .line 361876
    iget-object v1, v0, LX/0pW;->a:LX/0Yi;

    .line 361877
    sget-object v0, LX/0ao;->ASYNC_CACHE_SEEN_STORY:LX/0ao;

    invoke-static {v1, v0}, LX/0Yi;->a(LX/0Yi;LX/0ao;)V

    .line 361878
    goto :goto_1
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 361879
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    const/4 v1, 0x0

    .line 361880
    iput v1, v0, Lcom/facebook/feed/data/FeedDataLoader;->C:I

    .line 361881
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/0rj;->TAIL_FETCH_FAILED:LX/0rj;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Freshness:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/234;->c:LX/0rS;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gD;->a(LX/0rj;Ljava/lang/String;)V

    .line 361882
    iget-object v0, p0, LX/234;->e:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/23E;->AFTER:LX/23E;

    invoke-static {v0, p1, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;Ljava/lang/Throwable;LX/23E;)V

    .line 361883
    iget-object v0, p0, LX/234;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 361884
    return-void
.end method
