.class public LX/2V0;
.super LX/1Eg;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0SG;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0Uh;

.field private final e:LX/2V1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 416980
    const-class v0, LX/2V0;

    sput-object v0, LX/2V0;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/2V1;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416974
    const-string v0, "selfupdate_start_operation_task"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 416975
    iput-object p2, p0, LX/2V0;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 416976
    iput-object p1, p0, LX/2V0;->b:LX/0SG;

    .line 416977
    iput-object p3, p0, LX/2V0;->d:LX/0Uh;

    .line 416978
    iput-object p4, p0, LX/2V0;->e:LX/2V1;

    .line 416979
    return-void
.end method

.method private static a(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 416970
    if-eqz p0, :cond_0

    .line 416971
    sget-object v0, LX/2V0;->a:Ljava/lang/Class;

    invoke-static {v0, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 416972
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 416973
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/2V0;
    .locals 5

    .prologue
    .line 416968
    new-instance v4, LX/2V0;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {p0}, LX/2V1;->b(LX/0QB;)LX/2V1;

    move-result-object v3

    check-cast v3, LX/2V1;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2V0;-><init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/2V1;)V

    .line 416969
    return-object v4
.end method

.method private k()Lcom/facebook/appupdate/ReleaseInfo;
    .locals 21
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 416944
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2V0;->d:LX/0Uh;

    const/16 v3, 0x493

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 416945
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2V0;->e:LX/2V1;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/2V1;->b(Z)LX/Fk1;

    move-result-object v12

    .line 416946
    if-eqz v12, :cond_1

    .line 416947
    new-instance v2, Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v3, v12, LX/Fk1;->e:Ljava/lang/String;

    iget v4, v12, LX/Fk1;->f:I

    iget-object v5, v12, LX/Fk1;->c:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, v12, LX/Fk1;->h:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v10, v12, LX/Fk1;->i:Ljava/lang/String;

    iget-object v11, v12, LX/Fk1;->g:Ljava/lang/String;

    iget-wide v12, v12, LX/Fk1;->d:J

    const-string v14, "oxygen"

    invoke-direct/range {v2 .. v14}, Lcom/facebook/appupdate/ReleaseInfo;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 416948
    :goto_0
    return-object v2

    .line 416949
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2V0;->e:LX/2V1;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/2V1;->a(Z)LX/Fjz;

    move-result-object v18

    .line 416950
    if-eqz v18, :cond_1

    move-object/from16 v0, v18

    iget-object v2, v0, LX/Fjz;->f:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 416951
    new-instance v2, Lcom/facebook/appupdate/ReleaseInfo;

    move-object/from16 v0, v18

    iget-object v3, v0, LX/Fjz;->c:Ljava/lang/String;

    move-object/from16 v0, v18

    iget v4, v0, LX/Fjz;->d:I

    move-object/from16 v0, v18

    iget-object v5, v0, LX/Fjz;->f:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v6, v0, LX/Fjz;->m:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v7, v0, LX/Fjz;->o:Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, LX/Fjz;->b()Z

    move-result v8

    move-object/from16 v0, v18

    iget-object v9, v0, LX/Fjz;->h:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, v18

    iget-object v12, v0, LX/Fjz;->i:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v13, v0, LX/Fjz;->g:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-wide v14, v0, LX/Fjz;->l:J

    move-object/from16 v0, v18

    iget-wide v0, v0, LX/Fjz;->n:J

    move-wide/from16 v16, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, LX/Fjz;->p:J

    move-wide/from16 v18, v0

    const-string v20, "fql"

    invoke-direct/range {v2 .. v20}, Lcom/facebook/appupdate/ReleaseInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V

    goto :goto_0

    .line 416952
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    .line 416960
    iget-object v0, p0, LX/2V0;->d:LX/0Uh;

    const/16 v1, 0x48f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416961
    invoke-direct {p0}, LX/2V0;->k()Lcom/facebook/appupdate/ReleaseInfo;

    move-result-object v0

    .line 416962
    if-eqz v0, :cond_0

    .line 416963
    iget-object v1, p0, LX/2V0;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/3fU;->d:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/appupdate/ReleaseInfo;->a()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 416964
    :goto_0
    iget-object v0, p0, LX/2V0;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3fU;->c:LX/0Tn;

    iget-object v2, p0, LX/2V0;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 416965
    return-void

    .line 416966
    :cond_0
    const-string v0, "Skipping update because no update was found."

    invoke-static {p1, v0}, LX/2V0;->a(ZLjava/lang/String;)V

    goto :goto_0

    .line 416967
    :cond_1
    const-string v0, "Skipping update because selfupdate is not enabled."

    invoke-static {p1, v0}, LX/2V0;->a(ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public final f()J
    .locals 6

    .prologue
    .line 416957
    iget-object v0, p0, LX/2V0;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3fU;->c:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 416958
    iget-object v2, p0, LX/2V0;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/3fU;->b:LX/0Tn;

    const-wide/32 v4, 0x2932e00

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 416959
    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416956
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 4

    .prologue
    .line 416955
    invoke-virtual {p0}, LX/1Eg;->f()J

    move-result-wide v0

    iget-object v2, p0, LX/2V0;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416953
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/2V0;->a(Z)V

    .line 416954
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
