.class public LX/2FZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/2FZ;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Xl;

.field private final c:LX/2EL;

.field private final d:LX/2Fa;

.field public final e:LX/2Fb;

.field public final f:LX/0Sh;

.field public final g:LX/03V;

.field private final h:LX/2aZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Xl;LX/2EL;LX/2Fa;LX/2Fb;LX/0Sh;LX/03V;LX/2aZ;)V
    .locals 0
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387036
    iput-object p1, p0, LX/2FZ;->a:Landroid/content/Context;

    .line 387037
    iput-object p2, p0, LX/2FZ;->b:LX/0Xl;

    .line 387038
    iput-object p3, p0, LX/2FZ;->c:LX/2EL;

    .line 387039
    iput-object p4, p0, LX/2FZ;->d:LX/2Fa;

    .line 387040
    iput-object p5, p0, LX/2FZ;->e:LX/2Fb;

    .line 387041
    iput-object p6, p0, LX/2FZ;->f:LX/0Sh;

    .line 387042
    iput-object p7, p0, LX/2FZ;->g:LX/03V;

    .line 387043
    iput-object p8, p0, LX/2FZ;->h:LX/2aZ;

    .line 387044
    return-void
.end method

.method public static a(LX/0QB;)LX/2FZ;
    .locals 12

    .prologue
    .line 387045
    sget-object v0, LX/2FZ;->i:LX/2FZ;

    if-nez v0, :cond_1

    .line 387046
    const-class v1, LX/2FZ;

    monitor-enter v1

    .line 387047
    :try_start_0
    sget-object v0, LX/2FZ;->i:LX/2FZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387048
    if-eqz v2, :cond_0

    .line 387049
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 387050
    new-instance v3, LX/2FZ;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v0}, LX/2EL;->a(LX/0QB;)LX/2EL;

    move-result-object v6

    check-cast v6, LX/2EL;

    invoke-static {v0}, LX/2Fa;->a(LX/0QB;)LX/2Fa;

    move-result-object v7

    check-cast v7, LX/2Fa;

    .line 387051
    new-instance v9, LX/2Fb;

    invoke-static {v0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v8

    check-cast v8, Landroid/app/NotificationManager;

    invoke-direct {v9, v8}, LX/2Fb;-><init>(Landroid/app/NotificationManager;)V

    .line 387052
    move-object v8, v9

    .line 387053
    check-cast v8, LX/2Fb;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v9

    check-cast v9, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, LX/2aZ;->a(LX/0QB;)LX/2aZ;

    move-result-object v11

    check-cast v11, LX/2aZ;

    invoke-direct/range {v3 .. v11}, LX/2FZ;-><init>(Landroid/content/Context;LX/0Xl;LX/2EL;LX/2Fa;LX/2Fb;LX/0Sh;LX/03V;LX/2aZ;)V

    .line 387054
    move-object v0, v3

    .line 387055
    sput-object v0, LX/2FZ;->i:LX/2FZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387056
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387057
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387058
    :cond_1
    sget-object v0, LX/2FZ;->i:LX/2FZ;

    return-object v0

    .line 387059
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387060
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2FZ;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 387061
    iget-object v0, p0, LX/2FZ;->c:LX/2EL;

    .line 387062
    iget-object v1, v0, LX/2EL;->k:LX/2EQ;

    move-object v0, v1

    .line 387063
    sget-object v1, LX/2EQ;->CAPTIVE_PORTAL:LX/2EQ;

    if-ne v0, v1, :cond_0

    .line 387064
    iget-object v0, p0, LX/2FZ;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, LX/2FZ;->d:LX/2Fa;

    invoke-virtual {v3}, LX/2Fa;->a()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v0, v4, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 387065
    new-instance v1, LX/2HB;

    iget-object v2, p0, LX/2FZ;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/2HB;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0212e2

    invoke-virtual {v1, v2}, LX/2HB;->a(I)LX/2HB;

    move-result-object v1

    .line 387066
    iput v4, v1, LX/2HB;->j:I

    .line 387067
    move-object v1, v1

    .line 387068
    iput-object v0, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 387069
    move-object v0, v1

    .line 387070
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/2HB;->a(J)LX/2HB;

    move-result-object v0

    iget-object v1, p0, LX/2FZ;->a:Landroid/content/Context;

    const v2, 0x7f080588

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    iget-object v1, p0, LX/2FZ;->a:Landroid/content/Context;

    const v2, 0x7f080586

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    .line 387071
    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    const/16 v6, 0x271b

    .line 387072
    iget-object v5, p0, LX/2FZ;->e:LX/2Fb;

    invoke-virtual {v5, v6}, LX/2Fb;->a(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 387073
    iget-object v5, p0, LX/2FZ;->e:LX/2Fb;

    invoke-virtual {v5, v6, v0}, LX/2Fb;->a(ILandroid/app/Notification;)Z

    .line 387074
    :goto_0
    return-void

    .line 387075
    :cond_0
    iget-object v5, p0, LX/2FZ;->e:LX/2Fb;

    const/16 v6, 0x271b

    invoke-virtual {v5, v6}, LX/2Fb;->a(I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 387076
    iget-object v5, p0, LX/2FZ;->f:LX/0Sh;

    new-instance v6, Lcom/facebook/messaging/captiveportal/CaptivePortalNotificationManager$2;

    invoke-direct {v6, p0}, Lcom/facebook/messaging/captiveportal/CaptivePortalNotificationManager$2;-><init>(LX/2FZ;)V

    const-wide/16 v7, 0xbb8

    invoke-virtual {v5, v6, v7, v8}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    .line 387077
    :cond_1
    goto :goto_0

    .line 387078
    :cond_2
    iget-object v5, p0, LX/2FZ;->f:LX/0Sh;

    new-instance v6, Lcom/facebook/messaging/captiveportal/CaptivePortalNotificationManager$3;

    invoke-direct {v6, p0, v0}, Lcom/facebook/messaging/captiveportal/CaptivePortalNotificationManager$3;-><init>(LX/2FZ;Landroid/app/Notification;)V

    const-wide/16 v7, 0xbb8

    invoke-virtual {v5, v6, v7, v8}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 387079
    iget-object v0, p0, LX/2FZ;->h:LX/2aZ;

    invoke-virtual {v0}, LX/2aZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387080
    :goto_0
    return-void

    .line 387081
    :cond_0
    iget-object v0, p0, LX/2FZ;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.netchecker.ACTION_NETCHECK_STATE_CHANGED"

    new-instance v2, LX/2zb;

    invoke-direct {v2, p0}, LX/2zb;-><init>(LX/2FZ;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 387082
    invoke-static {p0}, LX/2FZ;->a$redex0(LX/2FZ;)V

    goto :goto_0
.end method
