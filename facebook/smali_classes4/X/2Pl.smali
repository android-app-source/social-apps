.class public LX/2Pl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mFbAppType:LX/00H;

.field private final mGatekeeperStore:LX/0Uh;

.field private final mMqttProtocolProvider:Lcom/facebook/omnistore/MqttProtocolProvider;

.field private final mOmnistoreErrorReporter:Lcom/facebook/omnistore/OmnistoreErrorReporter;

.field private final mViewerContextUserIdProvider:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mXAnalyticsProvider:LX/29a;


# direct methods
.method public constructor <init>(Lcom/facebook/omnistore/MqttProtocolProvider;Lcom/facebook/omnistore/OmnistoreErrorReporter;LX/29a;Landroid/content/Context;LX/00H;LX/0Or;LX/0Uh;)V
    .locals 0
    .param p1    # Lcom/facebook/omnistore/MqttProtocolProvider;
        .annotation runtime Lcom/facebook/omnistore/module/OverrideMqttProtocolProvider;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/omnistore/MqttProtocolProvider;",
            "Lcom/facebook/omnistore/OmnistoreErrorReporter;",
            "Lcom/facebook/xanalytics/XAnalyticsProvider;",
            "Landroid/content/Context;",
            "LX/00H;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 407192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407193
    iput-object p1, p0, LX/2Pl;->mMqttProtocolProvider:Lcom/facebook/omnistore/MqttProtocolProvider;

    .line 407194
    iput-object p2, p0, LX/2Pl;->mOmnistoreErrorReporter:Lcom/facebook/omnistore/OmnistoreErrorReporter;

    .line 407195
    iput-object p3, p0, LX/2Pl;->mXAnalyticsProvider:LX/29a;

    .line 407196
    iput-object p4, p0, LX/2Pl;->mContext:Landroid/content/Context;

    .line 407197
    iput-object p5, p0, LX/2Pl;->mFbAppType:LX/00H;

    .line 407198
    iput-object p6, p0, LX/2Pl;->mViewerContextUserIdProvider:LX/0Or;

    .line 407199
    iput-object p7, p0, LX/2Pl;->mGatekeeperStore:LX/0Uh;

    .line 407200
    return-void
.end method

.method public static createInstance__com_facebook_omnistore_module_DefaultOmnistoreOpener__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pl;
    .locals 8

    .prologue
    .line 407190
    new-instance v0, LX/2Pl;

    invoke-static {p0}, LX/2Pm;->getInstance__com_facebook_omnistore_MqttProtocolProvider__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/omnistore/MqttProtocolProvider;

    move-result-object v1

    check-cast v1, Lcom/facebook/omnistore/MqttProtocolProvider;

    invoke-static {p0}, LX/2Pq;->getInstance__com_facebook_omnistore_logger_FbOmnistoreErrorReporter__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pq;

    move-result-object v2

    check-cast v2, Lcom/facebook/omnistore/OmnistoreErrorReporter;

    invoke-static {p0}, LX/29a;->a(LX/0QB;)LX/29a;

    move-result-object v3

    check-cast v3, LX/29a;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const-class v5, LX/00H;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/00H;

    const/16 v6, 0x15e8

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-direct/range {v0 .. v7}, LX/2Pl;-><init>(Lcom/facebook/omnistore/MqttProtocolProvider;Lcom/facebook/omnistore/OmnistoreErrorReporter;LX/29a;Landroid/content/Context;LX/00H;LX/0Or;LX/0Uh;)V

    .line 407191
    return-object v0
.end method


# virtual methods
.method public getDbFileName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 407183
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "omnistore_"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/2Pl;->mViewerContextUserIdProvider:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_v01.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOmnistoreDirectory()Ljava/io/File;
    .locals 3

    .prologue
    .line 407189
    iget-object v0, p0, LX/2Pl;->mContext:Landroid/content/Context;

    const-string v1, "omnistore"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public openOmnistoreInstance()Lcom/facebook/omnistore/Omnistore;
    .locals 15

    .prologue
    const/4 v12, 0x0

    .line 407184
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, LX/2Pl;->getOmnistoreDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {p0}, LX/2Pl;->getDbFileName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 407185
    iget-object v1, p0, LX/2Pl;->mContext:Landroid/content/Context;

    iget-object v2, p0, LX/2Pl;->mFbAppType:LX/00H;

    invoke-virtual {v2}, LX/00H;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, LX/2Pu;->getDeviceId(Landroid/content/Context;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v1

    .line 407186
    if-eqz v1, :cond_0

    :goto_0
    iget-object v2, p0, LX/2Pl;->mMqttProtocolProvider:Lcom/facebook/omnistore/MqttProtocolProvider;

    iget-object v3, p0, LX/2Pl;->mOmnistoreErrorReporter:Lcom/facebook/omnistore/OmnistoreErrorReporter;

    iget-object v4, p0, LX/2Pl;->mXAnalyticsProvider:LX/29a;

    .line 407187
    iget-object v5, v4, LX/29a;->c:Lcom/facebook/xanalytics/XAnalyticsNative;

    move-object v4, v5

    .line 407188
    iget-object v5, p0, LX/2Pl;->mGatekeeperStore:LX/0Uh;

    const/16 v6, 0x227

    invoke-virtual {v5, v6, v12}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x1

    :goto_1
    iget-object v6, p0, LX/2Pl;->mGatekeeperStore:LX/0Uh;

    const/16 v7, 0x197

    invoke-virtual {v6, v7, v12}, LX/0Uh;->a(IZ)Z

    move-result v6

    iget-object v7, p0, LX/2Pl;->mGatekeeperStore:LX/0Uh;

    const/16 v8, 0x19b

    invoke-virtual {v7, v8, v12}, LX/0Uh;->a(IZ)Z

    move-result v7

    iget-object v8, p0, LX/2Pl;->mGatekeeperStore:LX/0Uh;

    const/16 v9, 0x19a

    invoke-virtual {v8, v9, v12}, LX/0Uh;->a(IZ)Z

    move-result v8

    iget-object v9, p0, LX/2Pl;->mGatekeeperStore:LX/0Uh;

    const/16 v10, 0x229

    invoke-virtual {v9, v10, v12}, LX/0Uh;->a(IZ)Z

    move-result v9

    iget-object v10, p0, LX/2Pl;->mGatekeeperStore:LX/0Uh;

    const/16 v11, 0x228

    invoke-virtual {v10, v11, v12}, LX/0Uh;->a(IZ)Z

    move-result v10

    iget-object v11, p0, LX/2Pl;->mGatekeeperStore:LX/0Uh;

    const/16 v13, 0x198

    invoke-virtual {v11, v13, v12}, LX/0Uh;->a(IZ)Z

    move-result v11

    iget-object v13, p0, LX/2Pl;->mGatekeeperStore:LX/0Uh;

    const/16 v14, 0x59b

    invoke-virtual {v13, v14, v12}, LX/0Uh;->a(IZ)Z

    move-result v13

    invoke-static/range {v0 .. v13}, Lcom/facebook/omnistore/Omnistore;->open(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/omnistore/MqttProtocolProvider;Lcom/facebook/omnistore/OmnistoreErrorReporter;Lcom/facebook/xanalytics/XAnalyticsNative;ZZZZZZZZZ)Lcom/facebook/omnistore/Omnistore;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v1, ""

    goto :goto_0

    :cond_1
    move v5, v12

    goto :goto_1
.end method
