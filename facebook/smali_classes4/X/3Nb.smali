.class public LX/3Nb;
.super LX/3Nc;
.source ""


# static fields
.field private static final i:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/3Nf;

.field public j:Z

.field public k:Landroid/view/View$OnFocusChangeListener;

.field public l:LX/3O8;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 559510
    const-class v0, LX/3Nb;

    sput-object v0, LX/3Nb;->i:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/3LG;I)V
    .locals 0

    .prologue
    .line 559502
    invoke-direct {p0, p1, p2, p3}, LX/3Nc;-><init>(Landroid/content/Context;LX/3LG;I)V

    .line 559503
    const p1, 0x7f0d2054

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, LX/3Nf;

    iput-object p1, p0, LX/3Nb;->a:LX/3Nf;

    .line 559504
    iget-object p1, p0, LX/3Nb;->a:LX/3Nf;

    new-instance p2, LX/3O0;

    invoke-direct {p2, p0}, LX/3O0;-><init>(LX/3Nb;)V

    invoke-virtual {p1, p2}, LX/3Nf;->a(Landroid/view/View$OnClickListener;)V

    .line 559505
    iget-object p1, p0, LX/3Nb;->a:LX/3Nf;

    new-instance p2, LX/3O1;

    invoke-direct {p2, p0}, LX/3O1;-><init>(LX/3Nb;)V

    .line 559506
    iput-object p2, p1, LX/3Nf;->a:LX/3O1;

    .line 559507
    iget-object p1, p0, LX/3Nb;->a:LX/3Nf;

    new-instance p2, LX/3O2;

    invoke-direct {p2, p0}, LX/3O2;-><init>(LX/3Nb;)V

    .line 559508
    iget-object p0, p1, LX/3Nf;->d:Landroid/widget/ImageView;

    invoke-virtual {p0, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 559509
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 559496
    const-string v0, ""

    invoke-virtual {p0, v0}, LX/3Nb;->setSearchBoxText(Ljava/lang/String;)V

    .line 559497
    iget-object v0, p0, LX/3Nb;->a:LX/3Nf;

    invoke-virtual {v0}, LX/3Nf;->c()V

    .line 559498
    iget-object v0, p0, LX/3Nc;->g:LX/3Ne;

    .line 559499
    iget-object v1, v0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v1

    .line 559500
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setSelection(I)V

    .line 559501
    return-void
.end method

.method public final a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 559492
    invoke-super {p0, p1, p2}, LX/3Nc;->a(Landroid/view/View;Z)V

    .line 559493
    iget-object v0, p0, LX/3Nb;->k:Landroid/view/View$OnFocusChangeListener;

    if-eqz v0, :cond_0

    .line 559494
    iget-object v0, p0, LX/3Nb;->k:Landroid/view/View$OnFocusChangeListener;

    invoke-interface {v0, p1, p2}, Landroid/view/View$OnFocusChangeListener;->onFocusChange(Landroid/view/View;Z)V

    .line 559495
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 559490
    iget-object v0, p0, LX/3Nb;->a:LX/3Nf;

    invoke-virtual {v0, p1}, LX/3Nf;->setSearchText(Ljava/lang/String;)V

    .line 559491
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 559489
    iget-boolean v0, p0, LX/3Nb;->j:Z

    return v0
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 559487
    iget-boolean v1, p0, LX/3Nb;->j:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, LX/3Nb;->a:LX/3Nf;

    invoke-virtual {v1}, LX/3Nf;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 559488
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/3Nc;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 559483
    iget-boolean v0, p0, LX/3Nb;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/3Nb;->getSearchBoxText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 559484
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 559485
    iget-object v0, p0, LX/3Nb;->a:LX/3Nf;

    invoke-virtual {v0}, LX/3Nf;->c()V

    .line 559486
    :cond_0
    invoke-super {p0, p1}, LX/3Nc;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public getSearchBar()LX/3Nf;
    .locals 1

    .prologue
    .line 559475
    iget-object v0, p0, LX/3Nb;->a:LX/3Nf;

    return-object v0
.end method

.method public bridge synthetic getSearchBar()LX/3Ng;
    .locals 1

    .prologue
    .line 559482
    invoke-virtual {p0}, LX/3Nb;->getSearchBar()LX/3Nf;

    move-result-object v0

    return-object v0
.end method

.method public getSearchBoxText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 559481
    iget-object v0, p0, LX/3Nb;->a:LX/3Nf;

    invoke-virtual {v0}, LX/3Nf;->getSearchText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setSearchBoxText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 559479
    iget-object v0, p0, LX/3Nb;->a:LX/3Nf;

    invoke-virtual {v0, p1}, LX/3Nf;->setSearchText(Ljava/lang/String;)V

    .line 559480
    return-void
.end method

.method public setSearchHint(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 559476
    iget-object v0, p0, LX/3Nb;->a:LX/3Nf;

    .line 559477
    iget-object p0, v0, LX/3Nf;->c:Landroid/widget/EditText;

    invoke-virtual {p0, p1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 559478
    return-void
.end method
