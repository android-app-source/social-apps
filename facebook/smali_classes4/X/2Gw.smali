.class public LX/2Gw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 389629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389630
    return-void
.end method

.method public static a(LX/0Or;LX/1wY;LX/03V;)LX/1wZ;
    .locals 3
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/location/IsForceAndroidPlatformImplEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1wY;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")",
            "LX/1wZ;"
        }
    .end annotation

    .prologue
    .line 389631
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x0

    .line 389632
    :try_start_0
    invoke-virtual {p1}, LX/1wY;->a()I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 389633
    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 389634
    :cond_0
    :goto_0
    move v0, v0

    .line 389635
    if-nez v0, :cond_2

    .line 389636
    :cond_1
    sget-object v0, LX/1wZ;->ANDROID_PLATFORM:LX/1wZ;

    .line 389637
    :goto_1
    return-object v0

    :cond_2
    sget-object v0, LX/1wZ;->GOOGLE_PLAY:LX/1wZ;

    goto :goto_1

    .line 389638
    :catch_0
    move-exception v1

    .line 389639
    invoke-static {v1}, LX/3KX;->a(Ljava/lang/RuntimeException;)V

    .line 389640
    const-string v2, "passive_location_provider"

    const-string p0, "GooglePlayServicesUtil error"

    invoke-virtual {p2, v2, p0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(LX/0Or;LX/1wY;LX/0Or;LX/0Or;LX/03V;)LX/6Z9;
    .locals 2
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/location/IsForceAndroidPlatformImplEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1wY;",
            "LX/0Or",
            "<",
            "LX/3KD;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6ZA;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")",
            "LX/6Z9;"
        }
    .end annotation

    .prologue
    .line 389641
    sget-object v0, LX/6ZI;->a:[I

    invoke-static {p0, p1, p4}, LX/2Gw;->a(LX/0Or;LX/1wY;LX/03V;)LX/1wZ;

    move-result-object v1

    invoke-virtual {v1}, LX/1wZ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 389642
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 389643
    :pswitch_0
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Z9;

    .line 389644
    :goto_0
    return-object v0

    :pswitch_1
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Z9;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
