.class public final LX/2yp;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/7CW;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 482716
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 482717
    sput-object v0, LX/2yp;->b:Ljava/util/Set;

    const-string v1, "our"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 482718
    sget-object v0, LX/2yp;->b:Ljava/util/Set;

    const-string v1, "tools"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 482719
    sget-object v0, LX/2yp;->b:Ljava/util/Set;

    const-string v1, "fiddle"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 482720
    sget-object v0, LX/2yp;->b:Ljava/util/Set;

    const-string v1, "interngraph"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 482721
    return-void
.end method

.method public constructor <init>(LX/7CW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 482739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482740
    iput-object p1, p0, LX/2yp;->a:LX/7CW;

    .line 482741
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 482736
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 482737
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 482738
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/2yq;)Z
    .locals 1

    .prologue
    .line 482735
    const-string v0, "m.facebook.com"

    invoke-static {p0, v0}, LX/2yp;->a(LX/2yq;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static a(LX/2yq;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 482742
    iget-object v0, p0, LX/2yq;->b:Ljava/lang/String;

    move-object v0, v0

    .line 482743
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 2
    .param p0    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 482731
    invoke-static {p0}, LX/2yp;->d(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 482732
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 482733
    const-string v1, "/l.php"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "/si/ajax/l/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/2yo;->f(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 482734
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/2yq;)Z
    .locals 1

    .prologue
    .line 482730
    const-string v0, "facebook.com"

    invoke-static {p0, v0}, LX/2yp;->a(LX/2yq;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/net/Uri;)Z
    .locals 1
    .param p0    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 482726
    invoke-static {p0}, LX/2yq;->a(Landroid/net/Uri;)LX/2yq;

    move-result-object v0

    .line 482727
    if-nez v0, :cond_0

    .line 482728
    const/4 v0, 0x0

    .line 482729
    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, LX/2yp;->a(LX/2yq;)Z

    move-result v0

    goto :goto_0
.end method

.method public static d(Landroid/net/Uri;)Z
    .locals 1
    .param p0    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 482722
    invoke-static {p0}, LX/2yq;->a(Landroid/net/Uri;)LX/2yq;

    move-result-object v0

    .line 482723
    if-nez v0, :cond_0

    .line 482724
    const/4 v0, 0x0

    .line 482725
    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, LX/2yp;->b(LX/2yq;)Z

    move-result v0

    goto :goto_0
.end method
