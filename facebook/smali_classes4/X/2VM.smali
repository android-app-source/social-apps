.class public LX/2VM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/2VM;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/2VN;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3ym;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0cb;

.field public final i:LX/2VP;

.field private final j:LX/2VU;

.field private final k:LX/0SG;

.field private final l:LX/0pb;

.field private final m:LX/0ZV;

.field public final n:LX/03V;

.field private final o:LX/0cc;

.field public final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ae;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0dC;

.field public final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 417338
    const-class v0, LX/2VM;

    sput-object v0, LX/2VM;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2VN;LX/0Ot;LX/0Or;LX/0cb;LX/2VP;LX/2VU;LX/0SG;LX/0pb;LX/0ZV;LX/03V;LX/0cc;LX/0Ot;LX/0dC;LX/0Ot;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/abtest/qe/annotations/LoggedInUserIdHash;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/abtest/qe/annotations/IsUserTrustedWithQEInternals;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2VN;",
            "LX/0Ot",
            "<",
            "LX/3ym;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0cb;",
            "LX/2VP;",
            "LX/2VU;",
            "LX/0SG;",
            "LX/0pb;",
            "LX/0ZV;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0cc;",
            "LX/0Ot",
            "<",
            "LX/0ae;",
            ">;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0Ot",
            "<",
            "LX/0lB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 417339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417340
    iput-object p1, p0, LX/2VM;->b:LX/0Or;

    .line 417341
    iput-object p2, p0, LX/2VM;->c:LX/0Or;

    .line 417342
    iput-object p3, p0, LX/2VM;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 417343
    iput-object p4, p0, LX/2VM;->e:LX/2VN;

    .line 417344
    iput-object p5, p0, LX/2VM;->f:LX/0Ot;

    .line 417345
    iput-object p6, p0, LX/2VM;->g:LX/0Or;

    .line 417346
    iput-object p7, p0, LX/2VM;->h:LX/0cb;

    .line 417347
    iput-object p8, p0, LX/2VM;->i:LX/2VP;

    .line 417348
    iput-object p9, p0, LX/2VM;->j:LX/2VU;

    .line 417349
    iput-object p10, p0, LX/2VM;->k:LX/0SG;

    .line 417350
    iput-object p11, p0, LX/2VM;->l:LX/0pb;

    .line 417351
    iput-object p12, p0, LX/2VM;->m:LX/0ZV;

    .line 417352
    iput-object p13, p0, LX/2VM;->n:LX/03V;

    .line 417353
    iput-object p14, p0, LX/2VM;->o:LX/0cc;

    .line 417354
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2VM;->p:LX/0Ot;

    .line 417355
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2VM;->q:LX/0dC;

    .line 417356
    move-object/from16 v0, p17

    iput-object v0, p0, LX/2VM;->r:LX/0Ot;

    .line 417357
    return-void
.end method

.method public static a(Ljava/lang/String;LX/0e6;Ljava/lang/Object;)LX/2Vj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "LX/0e6",
            "<TK;TV;>;TK;)",
            "LX/2Vj;"
        }
    .end annotation

    .prologue
    .line 417358
    invoke-static {p1, p2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    .line 417359
    iput-object p0, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 417360
    move-object v0, v0

    .line 417361
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;
    .locals 3
    .param p0    # Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 417362
    if-nez p0, :cond_0

    .line 417363
    const/4 v0, 0x0

    .line 417364
    :goto_0
    return-object v0

    .line 417365
    :cond_0
    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;->e:Ljava/lang/String;

    move-object v0, v0

    .line 417366
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 417367
    const-string v0, ""

    .line 417368
    :cond_1
    new-instance v1, LX/2WQ;

    invoke-direct {v1}, LX/2WQ;-><init>()V

    .line 417369
    iget-object v2, p0, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;->a:Ljava/lang/String;

    move-object v2, v2

    .line 417370
    invoke-virtual {v1, v2}, LX/2WQ;->e(Ljava/lang/String;)LX/2WQ;

    move-result-object v1

    .line 417371
    iget-object v2, p0, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;->b:Ljava/lang/String;

    move-object v2, v2

    .line 417372
    invoke-virtual {v1, v2}, LX/2WQ;->f(Ljava/lang/String;)LX/2WQ;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2WQ;->g(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    .line 417373
    iget-boolean v1, p0, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;->c:Z

    move v1, v1

    .line 417374
    invoke-virtual {v0, v1}, LX/2WQ;->c(Z)LX/2WQ;

    move-result-object v0

    .line 417375
    iget-boolean v1, p0, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;->d:Z

    move v1, v1

    .line 417376
    invoke-virtual {v0, v1}, LX/2WQ;->d(Z)LX/2WQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2WQ;->h(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;->f()LX/0P1;

    move-result-object v1

    .line 417377
    iput-object v1, v0, LX/2WQ;->g:Ljava/util/Map;

    .line 417378
    move-object v0, v0

    .line 417379
    invoke-virtual {v0}, LX/2WQ;->a()Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/2VM;LX/0Tn;LX/0Tn;)V
    .locals 4

    .prologue
    .line 417380
    iget-object v0, p0, LX/2VM;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 417381
    iget-object v1, p0, LX/2VM;->k:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, p1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 417382
    invoke-static {}, LX/0pb;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p2, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 417383
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 417384
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 417385
    if-eqz p2, :cond_0

    .line 417386
    sget-object v0, LX/2VF;->b:LX/0Tn;

    sget-object v1, LX/2VF;->c:LX/0Tn;

    invoke-static {p0, v0, v1}, LX/2VM;->a(LX/2VM;LX/0Tn;LX/0Tn;)V

    .line 417387
    :goto_0
    return-void

    .line 417388
    :cond_0
    sget-object v0, LX/2ab;->c:LX/0Tn;

    sget-object v1, LX/2ab;->d:LX/0Tn;

    invoke-static {p0, v0, v1}, LX/2VM;->a(LX/2VM;LX/0Tn;LX/0Tn;)V

    .line 417389
    iget-object v0, p0, LX/2VM;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 417390
    sget-object v1, LX/2ab;->b:LX/0Tn;

    iget-object v2, p0, LX/2VM;->m:LX/0ZV;

    .line 417391
    invoke-virtual {v2}, LX/0ZV;->a()Ljava/util/List;

    move-result-object p0

    invoke-static {p1, p0}, LX/0ZV;->a(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, LX/0ZV;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p0

    move-object v2, p0

    .line 417392
    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 417393
    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2VM;
    .locals 20

    .prologue
    .line 417394
    new-instance v2, LX/2VM;

    const/16 v3, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x15e2

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/2VN;->a(LX/0QB;)LX/2VN;

    move-result-object v6

    check-cast v6, LX/2VN;

    const/16 v7, 0x166a

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1437

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/0ca;->a(LX/0QB;)LX/0ca;

    move-result-object v9

    check-cast v9, LX/0cb;

    invoke-static/range {p0 .. p0}, LX/2VP;->a(LX/0QB;)LX/2VP;

    move-result-object v10

    check-cast v10, LX/2VP;

    invoke-static/range {p0 .. p0}, LX/2VU;->a(LX/0QB;)LX/2VU;

    move-result-object v11

    check-cast v11, LX/2VU;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0pb;->a(LX/0QB;)LX/0pb;

    move-result-object v13

    check-cast v13, LX/0pb;

    invoke-static/range {p0 .. p0}, LX/0ZV;->a(LX/0QB;)LX/0ZV;

    move-result-object v14

    check-cast v14, LX/0ZV;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v15

    check-cast v15, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0cc;->a(LX/0QB;)LX/0cc;

    move-result-object v16

    check-cast v16, LX/0cc;

    const/16 v17, 0x1032

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0dB;->a(LX/0QB;)LX/0dC;

    move-result-object v18

    check-cast v18, LX/0dC;

    const/16 v19, 0x2ba

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-direct/range {v2 .. v19}, LX/2VM;-><init>(LX/0Or;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2VN;LX/0Ot;LX/0Or;LX/0cb;LX/2VP;LX/2VU;LX/0SG;LX/0pb;LX/0ZV;LX/03V;LX/0cc;LX/0Ot;LX/0dC;LX/0Ot;)V

    .line 417395
    return-object v2
.end method


# virtual methods
.method public final a(ZII)Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZII)",
            "Ljava/util/Collection",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 417396
    iget-object v0, p0, LX/2VM;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 417397
    if-eqz p1, :cond_1

    .line 417398
    iget-object v1, p0, LX/2VM;->q:LX/0dC;

    invoke-virtual {v1}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v4

    .line 417399
    if-eqz v4, :cond_0

    move v1, v2

    :goto_0
    const-string v2, "Device Id must be available"

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    move-object v1, v4

    .line 417400
    :goto_1
    invoke-direct {p0, v0, p1}, LX/2VM;->a(Ljava/lang/String;Z)V

    .line 417401
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 417402
    new-instance v0, LX/2VX;

    invoke-direct {v0}, LX/2VX;-><init>()V

    .line 417403
    iput-object v1, v0, LX/2VX;->a:Ljava/lang/String;

    .line 417404
    move-object v0, v0

    .line 417405
    iput-boolean p1, v0, LX/2VX;->d:Z

    .line 417406
    move-object v5, v0

    .line 417407
    if-eqz p1, :cond_d

    iget-object v0, p0, LX/2VM;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ae;

    invoke-interface {v0}, LX/0ae;->e()Ljava/lang/Iterable;

    move-result-object v0

    .line 417408
    :goto_2
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    move-object v2, v0

    .line 417409
    invoke-virtual {v2}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v3

    .line 417410
    :goto_3
    move-object v0, v0

    .line 417411
    return-object v0

    :cond_0
    move v1, v3

    .line 417412
    goto :goto_0

    .line 417413
    :cond_1
    iget-object v1, p0, LX/2VM;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 417414
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    :goto_4
    const-string v3, "UID an UID Hash must be available"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_4

    .line 417415
    :cond_3
    invoke-virtual {v2}, LX/0Rf;->size()I

    .line 417416
    new-instance v6, LX/0cA;

    invoke-direct {v6}, LX/0cA;-><init>()V

    .line 417417
    if-lez p2, :cond_c

    .line 417418
    if-ge p3, p2, :cond_4

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 417419
    invoke-virtual {v2}, LX/0Rf;->size()I

    move-result v0

    div-int/2addr v0, p2

    .line 417420
    mul-int v4, p3, v0

    .line 417421
    add-int/lit8 v7, p2, -0x1

    if-ne p3, v7, :cond_5

    invoke-virtual {v2}, LX/0Rf;->size()I

    move-result v0

    .line 417422
    :goto_6
    invoke-virtual {v2}, LX/0Py;->asList()LX/0Px;

    move-result-object v7

    move v2, v4

    .line 417423
    :goto_7
    if-ge v2, v0, :cond_6

    .line 417424
    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 417425
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 417426
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    .line 417427
    :cond_5
    add-int/2addr v0, v4

    goto :goto_6

    .line 417428
    :cond_6
    invoke-virtual {v6}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    .line 417429
    :goto_8
    invoke-virtual {v0}, LX/0Rf;->size()I

    .line 417430
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 417431
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    :goto_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 417432
    if-eqz p1, :cond_9

    const/4 v2, 0x0

    .line 417433
    :goto_a
    if-nez v2, :cond_8

    .line 417434
    const-string v2, ""

    .line 417435
    :cond_8
    iget-object v7, v5, LX/2VX;->b:LX/0Pz;

    new-instance v1, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;

    invoke-direct {v1, v0, v2}, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 417436
    iget-object v2, p0, LX/2VM;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 417437
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "qe_"

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_9

    .line 417438
    :cond_9
    iget-object v2, p0, LX/2VM;->p:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ae;

    invoke-interface {v2, v0}, LX/0ae;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 417439
    iget-object v2, p0, LX/2VM;->p:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ae;

    sget-object v7, LX/0oc;->ASSIGNED:LX/0oc;

    invoke-interface {v2, v7, v0}, LX/0ae;->c(LX/0oc;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 417440
    :goto_b
    move-object v2, v2

    .line 417441
    goto :goto_a

    .line 417442
    :cond_a
    const-string v0, "sync_user_experiments"

    iget-object v2, p0, LX/2VM;->e:LX/2VN;

    .line 417443
    iget-object v6, v5, LX/2VX;->b:LX/0Pz;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    iput-object v6, v5, LX/2VX;->c:LX/0Px;

    .line 417444
    new-instance v6, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;

    invoke-direct {v6, v5}, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;-><init>(LX/2VX;)V

    move-object v5, v6

    .line 417445
    invoke-static {v0, v2, v5}, LX/2VM;->a(Ljava/lang/String;LX/0e6;Ljava/lang/Object;)LX/2Vj;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417446
    if-nez p1, :cond_b

    iget-object v0, p0, LX/2VM;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 417447
    const-string v2, "sync_meta"

    iget-object v0, p0, LX/2VM;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-static {v2, v0, v4}, LX/2VM;->a(Ljava/lang/String;LX/0e6;Ljava/lang/Object;)LX/2Vj;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_b
    move-object v0, v3

    .line 417448
    goto/16 :goto_3

    :cond_c
    move-object v0, v2

    goto/16 :goto_8

    .line 417449
    :cond_d
    iget-object v0, p0, LX/2VM;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ae;

    invoke-interface {v0}, LX/0ae;->d()Ljava/lang/Iterable;

    move-result-object v0

    goto/16 :goto_2

    .line 417450
    :cond_e
    iget-object v2, p0, LX/2VM;->h:LX/0cb;

    invoke-interface {v2, v0}, LX/0cb;->c(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v2

    .line 417451
    if-nez v2, :cond_f

    const/4 v2, 0x0

    goto :goto_b

    .line 417452
    :cond_f
    iget-object v7, v2, LX/2Wx;->b:Ljava/lang/String;

    move-object v2, v7

    .line 417453
    goto :goto_b
.end method

.method public final a(ZZ)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Ljava/util/Collection",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 417454
    invoke-virtual {p0, p2, v0, v0}, LX/2VM;->a(ZII)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 417455
    iget-object v0, p0, LX/2VM;->j:LX/2VU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2VU;->a(Z)V

    .line 417456
    return-void
.end method

.method public final a(Ljava/util/Map;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 417457
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 417458
    const-string v0, "sync_user_experiments"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/abtest/qe/protocol/sync/user/SyncMultiQuickExperimentUserInfoResult;

    .line 417459
    if-nez v0, :cond_6

    move-object v0, v1

    .line 417460
    :goto_0
    move-object v0, v0

    .line 417461
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 417462
    const-string v1, "sync_meta"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    .line 417463
    if-nez v1, :cond_9

    move-object v1, v4

    .line 417464
    :goto_1
    move-object v1, v1

    .line 417465
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 417466
    invoke-static {}, LX/0pb;->a()Ljava/lang/String;

    move-result-object v5

    .line 417467
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 417468
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 417469
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 417470
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 417471
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;

    invoke-static {v2, v5}, LX/2VM;->a(Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v9

    .line 417472
    if-eqz v9, :cond_0

    .line 417473
    iget-object v2, p0, LX/2VM;->p:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ae;

    invoke-interface {v2, v3}, LX/0ae;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 417474
    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 417475
    :goto_3
    invoke-interface {v1, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 417476
    invoke-interface {v4, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 417477
    :cond_1
    invoke-virtual {v6, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 417478
    :cond_2
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 417479
    iget-object v8, p0, LX/2VM;->h:LX/0cb;

    invoke-interface {v8, v2}, LX/0cb;->c(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v2

    .line 417480
    if-eqz v2, :cond_3

    .line 417481
    new-instance v8, LX/2WQ;

    invoke-direct {v8}, LX/2WQ;-><init>()V

    .line 417482
    iget-object v9, v2, LX/2Wx;->a:Ljava/lang/String;

    move-object v9, v9

    .line 417483
    invoke-virtual {v8, v9}, LX/2WQ;->e(Ljava/lang/String;)LX/2WQ;

    move-result-object v8

    .line 417484
    iget-object v9, v2, LX/2Wx;->e:Ljava/lang/String;

    move-object v9, v9

    .line 417485
    invoke-virtual {v8, v9}, LX/2WQ;->f(Ljava/lang/String;)LX/2WQ;

    move-result-object v8

    .line 417486
    iget-object v9, v2, LX/2Wx;->b:Ljava/lang/String;

    move-object v9, v9

    .line 417487
    invoke-virtual {v8, v9}, LX/2WQ;->g(Ljava/lang/String;)LX/2WQ;

    move-result-object v8

    .line 417488
    iget-boolean v9, v2, LX/2Wx;->c:Z

    move v9, v9

    .line 417489
    invoke-virtual {v8, v9}, LX/2WQ;->c(Z)LX/2WQ;

    move-result-object v8

    .line 417490
    iget-boolean v9, v2, LX/2Wx;->d:Z

    move v9, v9

    .line 417491
    invoke-virtual {v8, v9}, LX/2WQ;->d(Z)LX/2WQ;

    move-result-object v8

    invoke-virtual {v8, v5}, LX/2WQ;->h(Ljava/lang/String;)LX/2WQ;

    move-result-object v8

    .line 417492
    iget-object v9, v2, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    move-object v2, v9

    .line 417493
    iput-object v2, v8, LX/2WQ;->g:Ljava/util/Map;

    .line 417494
    move-object v2, v8

    .line 417495
    invoke-virtual {v2}, LX/2WQ;->a()Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 417496
    :cond_4
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 417497
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 417498
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 417499
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    .line 417500
    iget-object v8, v5, LX/2Wx;->a:Ljava/lang/String;

    move-object v8, v8

    .line 417501
    invoke-interface {v6, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 417502
    :cond_5
    move-object v5, v6

    .line 417503
    iget-object v2, p0, LX/2VM;->p:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ae;

    invoke-interface {v2, v5, p2}, LX/0ae;->a(Ljava/util/Map;Z)V

    .line 417504
    iget-object v2, p0, LX/2VM;->i:LX/2VP;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-static {v4, v5}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/2VP;->a(Ljava/util/Collection;Ljava/lang/Iterable;)V

    .line 417505
    invoke-virtual {p0}, LX/2VM;->a()V

    .line 417506
    return-void

    .line 417507
    :cond_6
    iget-object v2, v0, Lcom/facebook/abtest/qe/protocol/sync/user/SyncMultiQuickExperimentUserInfoResult;->a:Ljava/util/ArrayList;

    move-object v3, v2

    .line 417508
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_6
    if-ge v2, v4, :cond_8

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;

    .line 417509
    if-eqz v0, :cond_7

    .line 417510
    iget-object v5, v0, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;->a:Ljava/lang/String;

    move-object v5, v5

    .line 417511
    invoke-interface {v1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417512
    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_8
    move-object v0, v1

    .line 417513
    goto/16 :goto_0

    .line 417514
    :cond_9
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v6, v2

    :goto_7
    if-ge v6, v7, :cond_b

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationEdgeModel;

    .line 417515
    invoke-virtual {v2}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationEdgeModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-virtual {v2}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationEdgeModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->o()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 417516
    invoke-virtual {v2}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationEdgeModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    move-result-object v3

    .line 417517
    invoke-virtual {v2}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationEdgeModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->o()Ljava/lang/String;

    move-result-object v5

    const-string v8, "qe_"

    invoke-virtual {v5, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 417518
    invoke-virtual {v2}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationEdgeModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    move-result-object v3

    .line 417519
    new-instance v5, LX/3yg;

    invoke-direct {v5}, LX/3yg;-><init>()V

    .line 417520
    invoke-virtual {v3}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->j()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    move-result-object v8

    iput-object v8, v5, LX/3yg;->a:Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    .line 417521
    invoke-virtual {v3}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->k()Z

    move-result v8

    iput-boolean v8, v5, LX/3yg;->b:Z

    .line 417522
    invoke-virtual {v3}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->l()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v5, LX/3yg;->c:Ljava/lang/String;

    .line 417523
    invoke-virtual {v3}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->m()Z

    move-result v8

    iput-boolean v8, v5, LX/3yg;->d:Z

    .line 417524
    invoke-virtual {v3}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->n()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v5, LX/3yg;->e:Ljava/lang/String;

    .line 417525
    invoke-virtual {v3}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->o()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v5, LX/3yg;->f:Ljava/lang/String;

    .line 417526
    invoke-virtual {v3}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->p()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v5, LX/3yg;->g:Ljava/lang/String;

    .line 417527
    invoke-virtual {v3}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->q()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    move-result-object v8

    iput-object v8, v5, LX/3yg;->h:Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    .line 417528
    invoke-virtual {v3}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->r()I

    move-result v8

    iput v8, v5, LX/3yg;->i:I

    .line 417529
    move-object v3, v5

    .line 417530
    invoke-virtual {v2}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationEdgeModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->o()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x3

    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 417531
    iput-object v5, v3, LX/3yg;->f:Ljava/lang/String;

    .line 417532
    move-object v3, v3

    .line 417533
    invoke-virtual {v3}, LX/3yg;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    move-result-object v3

    move-object v5, v3

    .line 417534
    :goto_8
    :try_start_0
    invoke-virtual {v5}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->o()Ljava/lang/String;

    move-result-object v8

    iget-object v3, p0, LX/2VM;->r:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lB;

    invoke-virtual {v3, v5}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v8, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 417535
    :cond_a
    :goto_9
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_7

    .line 417536
    :catch_0
    move-exception v3

    .line 417537
    iget-object v5, p0, LX/2VM;->n:LX/03V;

    const-string v8, "qe_write_json_failed"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "node name: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationEdgeModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v8, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_9

    :cond_b
    move-object v1, v4

    .line 417538
    goto/16 :goto_1

    :cond_c
    move-object v5, v3

    goto :goto_8
.end method
