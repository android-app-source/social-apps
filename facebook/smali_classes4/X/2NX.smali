.class public final enum LX/2NX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2NX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2NX;

.field public static final enum BLUE:LX/2NX;

.field public static final enum WHITE:LX/2NX;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 399874
    new-instance v0, LX/2NX;

    const-string v1, "WHITE"

    invoke-direct {v0, v1, v2}, LX/2NX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2NX;->WHITE:LX/2NX;

    .line 399875
    new-instance v0, LX/2NX;

    const-string v1, "BLUE"

    invoke-direct {v0, v1, v3}, LX/2NX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2NX;->BLUE:LX/2NX;

    .line 399876
    const/4 v0, 0x2

    new-array v0, v0, [LX/2NX;

    sget-object v1, LX/2NX;->WHITE:LX/2NX;

    aput-object v1, v0, v2

    sget-object v1, LX/2NX;->BLUE:LX/2NX;

    aput-object v1, v0, v3

    sput-object v0, LX/2NX;->$VALUES:[LX/2NX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 399877
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2NX;
    .locals 1

    .prologue
    .line 399878
    const-class v0, LX/2NX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2NX;

    return-object v0
.end method

.method public static values()[LX/2NX;
    .locals 1

    .prologue
    .line 399873
    sget-object v0, LX/2NX;->$VALUES:[LX/2NX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2NX;

    return-object v0
.end method
