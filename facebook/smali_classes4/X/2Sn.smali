.class public LX/2Sn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Sn;


# instance fields
.field public final a:LX/2Sc;


# direct methods
.method public constructor <init>(LX/2Sc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413289
    iput-object p1, p0, LX/2Sn;->a:LX/2Sc;

    .line 413290
    return-void
.end method

.method public static a(LX/0QB;)LX/2Sn;
    .locals 4

    .prologue
    .line 413291
    sget-object v0, LX/2Sn;->b:LX/2Sn;

    if-nez v0, :cond_1

    .line 413292
    const-class v1, LX/2Sn;

    monitor-enter v1

    .line 413293
    :try_start_0
    sget-object v0, LX/2Sn;->b:LX/2Sn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413294
    if-eqz v2, :cond_0

    .line 413295
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413296
    new-instance p0, LX/2Sn;

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v3

    check-cast v3, LX/2Sc;

    invoke-direct {p0, v3}, LX/2Sn;-><init>(LX/2Sc;)V

    .line 413297
    move-object v0, p0

    .line 413298
    sput-object v0, LX/2Sn;->b:LX/2Sn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413299
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413300
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413301
    :cond_1
    sget-object v0, LX/2Sn;->b:LX/2Sn;

    return-object v0

    .line 413302
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413303
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;
    .locals 5

    .prologue
    .line 413304
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 413305
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 413306
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 413307
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 413308
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    const-string v2, "Missing id for user"

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 413309
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 413310
    new-instance v1, LX/7C4;

    sget-object v2, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Missing name for user with id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v1

    .line 413311
    :cond_1
    new-instance v1, LX/CwR;

    invoke-direct {v1}, LX/CwR;-><init>()V

    .line 413312
    iput-object v0, v1, LX/CwR;->b:Ljava/lang/String;

    .line 413313
    move-object v0, v1

    .line 413314
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->f()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 413315
    iput-object v1, v0, LX/CwR;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 413316
    move-object v0, v0

    .line 413317
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 413318
    iput-object v1, v0, LX/CwR;->a:Ljava/lang/String;

    .line 413319
    move-object v0, v0

    .line 413320
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 413321
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 413322
    iput-object v1, v0, LX/CwR;->e:Landroid/net/Uri;

    .line 413323
    :cond_2
    const/4 v1, 0x0

    .line 413324
    iput-boolean v1, v0, LX/CwR;->d:Z

    .line 413325
    invoke-virtual {v0}, LX/CwR;->j()Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method
