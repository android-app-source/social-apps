.class public abstract enum LX/2hm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2hm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2hm;

.field public static final enum CONTACTS:LX/2hm;

.field public static final enum SEARCH:LX/2hm;

.field public static final enum SEE_ALL_FRIENDS:LX/2hm;

.field public static final enum SUGGESTIONS:LX/2hm;


# instance fields
.field public final analyticsName:Ljava/lang/String;

.field public final colorRes:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field public final drawableRes:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final labelRes:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 450811
    new-instance v0, LX/2hn;

    const-string v1, "CONTACTS"

    const v3, 0x7f0a00d1

    const v4, 0x7f02077f

    const v5, 0x7f080fa7

    const-string v6, "friends_tab_contacts"

    invoke-direct/range {v0 .. v6}, LX/2hn;-><init>(Ljava/lang/String;IIIILjava/lang/String;)V

    sput-object v0, LX/2hm;->CONTACTS:LX/2hm;

    .line 450812
    new-instance v3, LX/2ho;

    const-string v4, "SEARCH"

    const v6, 0x7f0a00d3

    const v7, 0x7f02091b

    const v8, 0x7f080fa5

    const-string v9, "friends_tab_search"

    move v5, v10

    invoke-direct/range {v3 .. v9}, LX/2ho;-><init>(Ljava/lang/String;IIIILjava/lang/String;)V

    sput-object v3, LX/2hm;->SEARCH:LX/2hm;

    .line 450813
    new-instance v3, LX/2hp;

    const-string v4, "SEE_ALL_FRIENDS"

    const v6, 0x7f0a00d4

    const v7, 0x7f020897

    const v8, 0x7f080fa6

    const-string v9, "friends_tab_see_all_friends"

    move v5, v11

    invoke-direct/range {v3 .. v9}, LX/2hp;-><init>(Ljava/lang/String;IIIILjava/lang/String;)V

    sput-object v3, LX/2hm;->SEE_ALL_FRIENDS:LX/2hm;

    .line 450814
    new-instance v3, LX/2hq;

    const-string v4, "SUGGESTIONS"

    const v6, 0x7f0a08c3

    const v7, 0x7f0208be

    const v8, 0x7f080fa4

    const-string v9, "friends_tab_suggestions"

    move v5, v12

    invoke-direct/range {v3 .. v9}, LX/2hq;-><init>(Ljava/lang/String;IIIILjava/lang/String;)V

    sput-object v3, LX/2hm;->SUGGESTIONS:LX/2hm;

    .line 450815
    const/4 v0, 0x4

    new-array v0, v0, [LX/2hm;

    sget-object v1, LX/2hm;->CONTACTS:LX/2hm;

    aput-object v1, v0, v2

    sget-object v1, LX/2hm;->SEARCH:LX/2hm;

    aput-object v1, v0, v10

    sget-object v1, LX/2hm;->SEE_ALL_FRIENDS:LX/2hm;

    aput-object v1, v0, v11

    sget-object v1, LX/2hm;->SUGGESTIONS:LX/2hm;

    aput-object v1, v0, v12

    sput-object v0, LX/2hm;->$VALUES:[LX/2hm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIILjava/lang/String;)V
    .locals 0
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 450819
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 450820
    iput p3, p0, LX/2hm;->colorRes:I

    .line 450821
    iput p4, p0, LX/2hm;->drawableRes:I

    .line 450822
    iput p5, p0, LX/2hm;->labelRes:I

    .line 450823
    iput-object p6, p0, LX/2hm;->analyticsName:Ljava/lang/String;

    .line 450824
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;IIIILjava/lang/String;Lcom/facebook/friending/jewel/FriendingJewelSproutLauncher$1;)V
    .locals 0

    .prologue
    .line 450818
    invoke-direct/range {p0 .. p6}, LX/2hm;-><init>(Ljava/lang/String;IIIILjava/lang/String;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2hm;
    .locals 1

    .prologue
    .line 450817
    const-class v0, LX/2hm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2hm;

    return-object v0
.end method

.method public static values()[LX/2hm;
    .locals 1

    .prologue
    .line 450816
    sget-object v0, LX/2hm;->$VALUES:[LX/2hm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2hm;

    return-object v0
.end method


# virtual methods
.method public abstract getSproutLauncher(LX/2hl;)Ljava/lang/Runnable;
.end method
