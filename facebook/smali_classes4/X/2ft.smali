.class public final enum LX/2ft;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2ft;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2ft;

.field public static final enum NONE:LX/2ft;

.field public static final enum WAIT_FOR_OFF_PEAK_DATA_HAPPY_HOUR:LX/2ft;

.field public static final enum WAIT_FOR_WIFI:LX/2ft;

.field private static final mReverseIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/2ft;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 447641
    new-instance v1, LX/2ft;

    const-string v2, "NONE"

    invoke-direct {v1, v2, v0, v0}, LX/2ft;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2ft;->NONE:LX/2ft;

    .line 447642
    new-instance v1, LX/2ft;

    const-string v2, "WAIT_FOR_OFF_PEAK_DATA_HAPPY_HOUR"

    invoke-direct {v1, v2, v3, v3}, LX/2ft;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2ft;->WAIT_FOR_OFF_PEAK_DATA_HAPPY_HOUR:LX/2ft;

    .line 447643
    new-instance v1, LX/2ft;

    const-string v2, "WAIT_FOR_WIFI"

    invoke-direct {v1, v2, v4, v4}, LX/2ft;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/2ft;->WAIT_FOR_WIFI:LX/2ft;

    .line 447644
    const/4 v1, 0x3

    new-array v1, v1, [LX/2ft;

    sget-object v2, LX/2ft;->NONE:LX/2ft;

    aput-object v2, v1, v0

    sget-object v2, LX/2ft;->WAIT_FOR_OFF_PEAK_DATA_HAPPY_HOUR:LX/2ft;

    aput-object v2, v1, v3

    sget-object v2, LX/2ft;->WAIT_FOR_WIFI:LX/2ft;

    aput-object v2, v1, v4

    sput-object v1, LX/2ft;->$VALUES:[LX/2ft;

    .line 447645
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, LX/2ft;->mReverseIndex:Ljava/util/Map;

    .line 447646
    invoke-static {}, LX/2ft;->values()[LX/2ft;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 447647
    sget-object v4, LX/2ft;->mReverseIndex:Ljava/util/Map;

    iget v5, v3, LX/2ft;->mValue:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447648
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 447649
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 447650
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 447651
    iput p3, p0, LX/2ft;->mValue:I

    .line 447652
    return-void
.end method

.method public static fromVal(I)LX/2ft;
    .locals 2

    .prologue
    .line 447653
    sget-object v0, LX/2ft;->mReverseIndex:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 447654
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid download status value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 447655
    :cond_0
    sget-object v0, LX/2ft;->mReverseIndex:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ft;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/2ft;
    .locals 1

    .prologue
    .line 447656
    const-class v0, LX/2ft;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2ft;

    return-object v0
.end method

.method public static values()[LX/2ft;
    .locals 1

    .prologue
    .line 447657
    sget-object v0, LX/2ft;->$VALUES:[LX/2ft;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2ft;

    return-object v0
.end method
