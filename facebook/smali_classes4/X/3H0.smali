.class public final enum LX/3H0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3H0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3H0;

.field public static final enum LIVE:LX/3H0;

.field public static final enum NONE:LX/3H0;

.field public static final enum NONLIVE:LX/3H0;

.field public static final enum NONLIVE_POST_ROLL:LX/3H0;

.field public static final enum VOD:LX/3H0;


# instance fields
.field private final mInstreamVideoAdType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 542614
    new-instance v0, LX/3H0;

    const-string v1, "NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v3, v2}, LX/3H0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3H0;->NONE:LX/3H0;

    .line 542615
    new-instance v0, LX/3H0;

    const-string v1, "LIVE"

    const-string v2, "live"

    invoke-direct {v0, v1, v4, v2}, LX/3H0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3H0;->LIVE:LX/3H0;

    .line 542616
    new-instance v0, LX/3H0;

    const-string v1, "VOD"

    const-string v2, "vod"

    invoke-direct {v0, v1, v5, v2}, LX/3H0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3H0;->VOD:LX/3H0;

    .line 542617
    new-instance v0, LX/3H0;

    const-string v1, "NONLIVE"

    const-string v2, "nonlive"

    invoke-direct {v0, v1, v6, v2}, LX/3H0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3H0;->NONLIVE:LX/3H0;

    .line 542618
    new-instance v0, LX/3H0;

    const-string v1, "NONLIVE_POST_ROLL"

    const-string v2, "nonlive_post_roll"

    invoke-direct {v0, v1, v7, v2}, LX/3H0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3H0;->NONLIVE_POST_ROLL:LX/3H0;

    .line 542619
    const/4 v0, 0x5

    new-array v0, v0, [LX/3H0;

    sget-object v1, LX/3H0;->NONE:LX/3H0;

    aput-object v1, v0, v3

    sget-object v1, LX/3H0;->LIVE:LX/3H0;

    aput-object v1, v0, v4

    sget-object v1, LX/3H0;->VOD:LX/3H0;

    aput-object v1, v0, v5

    sget-object v1, LX/3H0;->NONLIVE:LX/3H0;

    aput-object v1, v0, v6

    sget-object v1, LX/3H0;->NONLIVE_POST_ROLL:LX/3H0;

    aput-object v1, v0, v7

    sput-object v0, LX/3H0;->$VALUES:[LX/3H0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 542611
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 542612
    iput-object p3, p0, LX/3H0;->mInstreamVideoAdType:Ljava/lang/String;

    .line 542613
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3H0;
    .locals 1

    .prologue
    .line 542620
    const-class v0, LX/3H0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3H0;

    return-object v0
.end method

.method public static values()[LX/3H0;
    .locals 1

    .prologue
    .line 542610
    sget-object v0, LX/3H0;->$VALUES:[LX/3H0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3H0;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 542609
    iget-object v0, p0, LX/3H0;->mInstreamVideoAdType:Ljava/lang/String;

    return-object v0
.end method
