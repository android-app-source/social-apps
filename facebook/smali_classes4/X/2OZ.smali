.class public LX/2OZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final a:LX/2OW;

.field public final b:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2OW;)V
    .locals 1

    .prologue
    .line 402421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402422
    iput-object p1, p0, LX/2OZ;->a:LX/2OW;

    .line 402423
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/2OZ;->b:LX/01J;

    .line 402424
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/2OZ;->c:LX/01J;

    .line 402425
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 402412
    iget-object v0, p0, LX/2OZ;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402413
    iget-object v0, p0, LX/2OZ;->b:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 402414
    if-nez v0, :cond_1

    move-object v0, v1

    .line 402415
    :cond_0
    :goto_0
    return-object v0

    .line 402416
    :cond_1
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v3, v2

    .line 402417
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 402418
    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v5, p2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 402419
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 402420
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402410
    iget-object v0, p0, LX/2OZ;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402411
    iget-object v0, p0, LX/2OZ;->c:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/user/model/User;)V
    .locals 7
    .param p2    # Lcom/facebook/user/model/User;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 402386
    iget-object v0, p0, LX/2OZ;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402387
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 402388
    invoke-virtual {p0, v0}, LX/2OZ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 402389
    iget-object v1, p0, LX/2OZ;->a:LX/2OW;

    invoke-virtual {v1}, LX/2OW;->b()V

    .line 402390
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v3, v1

    .line 402391
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 402392
    iget-object v5, p0, LX/2OZ;->c:LX/01J;

    iget-object v6, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402393
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 402394
    :cond_0
    iget-object v1, p0, LX/2OZ;->b:LX/01J;

    .line 402395
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v2, v2

    .line 402396
    invoke-virtual {v1, v2, p1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402397
    if-eqz p2, :cond_1

    .line 402398
    iget-boolean v1, p2, Lcom/facebook/user/model/User;->o:Z

    move v1, v1

    .line 402399
    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x3

    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 402400
    :cond_2
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v1, v1

    .line 402401
    invoke-static {v1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(LX/0Px;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 402402
    if-nez v0, :cond_4

    const-string v0, "Unknown"

    .line 402403
    :goto_1
    const-string v1, "MessagesOutOfOrderInCache"

    .line 402404
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    move-result v2

    const/16 v3, 0x64

    if-le v2, v3, :cond_6

    .line 402405
    const-string v2, "Thread messages is not in order in cache"

    .line 402406
    :goto_2
    move-object v0, v2

    .line 402407
    invoke-static {v1, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 402408
    :cond_3
    return-void

    .line 402409
    :cond_4
    iget-object v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Thread messages is not in order in cache, isCanonicalThread="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", messagesCollection="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 5

    .prologue
    .line 402376
    iget-object v0, p0, LX/2OZ;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402377
    iget-object v0, p0, LX/2OZ;->b:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 402378
    if-nez v0, :cond_1

    .line 402379
    :cond_0
    return-void

    .line 402380
    :cond_1
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v3, v1

    .line 402381
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 402382
    iget-object p1, p0, LX/2OZ;->c:LX/01J;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 402383
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 402384
    iget-object v0, p0, LX/2OZ;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402385
    iget-object v0, p0, LX/2OZ;->b:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/MessagesCollection;

    return-object v0
.end method
