.class public LX/3Hf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/3Hf;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ti;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/res/Resources;

.field public final d:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;LX/0Ot;Landroid/content/res/Resources;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/0ti;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 544465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 544466
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/3Hf;->d:Landroid/util/LruCache;

    .line 544467
    iput-object p1, p0, LX/3Hf;->a:LX/0SG;

    .line 544468
    iput-object p2, p0, LX/3Hf;->b:LX/0Ot;

    .line 544469
    iput-object p3, p0, LX/3Hf;->c:Landroid/content/res/Resources;

    .line 544470
    return-void
.end method

.method public static a(LX/0QB;)LX/3Hf;
    .locals 6

    .prologue
    .line 544452
    sget-object v0, LX/3Hf;->f:LX/3Hf;

    if-nez v0, :cond_1

    .line 544453
    const-class v1, LX/3Hf;

    monitor-enter v1

    .line 544454
    :try_start_0
    sget-object v0, LX/3Hf;->f:LX/3Hf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 544455
    if-eqz v2, :cond_0

    .line 544456
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 544457
    new-instance v5, LX/3Hf;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    const/16 v4, 0xafb

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {v5, v3, p0, v4}, LX/3Hf;-><init>(LX/0SG;LX/0Ot;Landroid/content/res/Resources;)V

    .line 544458
    move-object v0, v5

    .line 544459
    sput-object v0, LX/3Hf;->f:LX/3Hf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 544460
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 544461
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 544462
    :cond_1
    sget-object v0, LX/3Hf;->f:LX/3Hf;

    return-object v0

    .line 544463
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 544464
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;LX/6TW;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 10
    .param p2    # Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/6TW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 544396
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v6

    .line 544397
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-nez v0, :cond_1

    .line 544398
    :cond_0
    :goto_0
    return-object p1

    .line 544399
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 544400
    if-eqz v1, :cond_0

    .line 544401
    if-nez p2, :cond_3

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 544402
    :goto_1
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/4XB;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/4XB;

    move-result-object v4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v5, v0, :cond_4

    move v0, v2

    .line 544403
    :goto_2
    iput-boolean v0, v4, LX/4XB;->ar:Z

    .line 544404
    move-object v0, v4

    .line 544405
    iput-object v5, v0, LX/4XB;->k:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 544406
    move-object v0, v0

    .line 544407
    if-eqz p3, :cond_2

    .line 544408
    invoke-interface {p3}, LX/6TW;->j()I

    move-result v4

    .line 544409
    iput v4, v0, LX/4XB;->bb:I

    .line 544410
    move-object v4, v0

    .line 544411
    invoke-interface {p3}, LX/6TW;->b()I

    move-result v7

    .line 544412
    iput v7, v4, LX/4XB;->e:I

    .line 544413
    move-object v4, v4

    .line 544414
    invoke-interface {p3}, LX/6TW;->e()I

    move-result v7

    .line 544415
    iput v7, v4, LX/4XB;->N:I

    .line 544416
    move-object v4, v4

    .line 544417
    invoke-interface {p3}, LX/6TW;->c()I

    move-result v7

    .line 544418
    iput v7, v4, LX/4XB;->j:I

    .line 544419
    move-object v4, v4

    .line 544420
    invoke-interface {p3}, LX/6TW;->dv_()I

    move-result v7

    .line 544421
    iput v7, v4, LX/4XB;->O:I

    .line 544422
    move-object v4, v4

    .line 544423
    invoke-interface {p3}, LX/6TW;->m()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 544424
    iput-object v7, v4, LX/4XB;->bg:Ljava/lang/String;

    .line 544425
    move-object v4, v4

    .line 544426
    invoke-interface {p3}, LX/6TW;->k()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 544427
    iput-object v7, v4, LX/4XB;->bc:Ljava/lang/String;

    .line 544428
    move-object v4, v4

    .line 544429
    invoke-interface {p3}, LX/6TW;->dw_()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 544430
    iput-object v7, v4, LX/4XB;->aZ:Ljava/lang/String;

    .line 544431
    move-object v4, v4

    .line 544432
    invoke-interface {p3}, LX/6TW;->l()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 544433
    iput-object v7, v4, LX/4XB;->bd:Ljava/lang/String;

    .line 544434
    :cond_2
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 544435
    invoke-static {v6}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v4

    .line 544436
    iput-object v0, v4, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 544437
    move-object v0, v4

    .line 544438
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 544439
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    iget-object v7, p0, LX/3Hf;->c:Landroid/content/res/Resources;

    const v8, 0x7f080dd6

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v2, v3

    invoke-virtual {v7, v8, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 544440
    iput-object v2, v0, LX/173;->f:Ljava/lang/String;

    .line 544441
    move-object v0, v0

    .line 544442
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 544443
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 544444
    iput-object v2, v0, LX/23u;->k:LX/0Px;

    .line 544445
    move-object v0, v0

    .line 544446
    iput-object v3, v0, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 544447
    move-object v0, v0

    .line 544448
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    .line 544449
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/3Hf;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)V

    goto/16 :goto_0

    :cond_3
    move-object v5, p2

    .line 544450
    goto/16 :goto_1

    :cond_4
    move v0, v3

    .line 544451
    goto/16 :goto_2
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)V
    .locals 2

    .prologue
    .line 544389
    iget-object v0, p0, LX/3Hf;->d:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 544390
    invoke-virtual {p0, v0, p5}, LX/3Hf;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    move v0, v0

    .line 544391
    if-eqz v0, :cond_0

    .line 544392
    iget-object v0, p0, LX/3Hf;->d:Landroid/util/LruCache;

    invoke-virtual {v0, p1, p5}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544393
    new-instance v1, LX/6PT;

    invoke-direct {v1, p1, p2, p3, p4}, LX/6PT;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 544394
    iget-object v0, p0, LX/3Hf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ti;

    invoke-virtual {v0, v1}, LX/0ti;->a(LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 544395
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/6TW;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .param p2    # LX/6TW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 544298
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, v0, p2}, LX/3Hf;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;LX/6TW;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-interface {p2}, LX/6TW;->d()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .param p2    # Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 544388
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3Hf;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;LX/6TW;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 12
    .param p2    # Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 544316
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v6

    .line 544317
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-nez v0, :cond_1

    .line 544318
    :cond_0
    :goto_0
    return-object p1

    .line 544319
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 544320
    if-eqz v1, :cond_0

    .line 544321
    if-nez p2, :cond_6

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 544322
    :goto_1
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/4XB;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/4XB;

    move-result-object v4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v5, v0, :cond_7

    const/4 v0, 0x1

    .line 544323
    :goto_2
    iput-boolean v0, v4, LX/4XB;->ar:Z

    .line 544324
    move-object v0, v4

    .line 544325
    iput-object v5, v0, LX/4XB;->k:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 544326
    move-object v4, v0

    .line 544327
    if-eqz p3, :cond_8

    invoke-virtual {p3}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 544328
    invoke-virtual {p3}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel;

    .line 544329
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel;->a()Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;

    move-result-object v2

    move-object v10, v2

    move-object v2, v0

    move-object v0, v10

    .line 544330
    :goto_3
    if-eqz p3, :cond_2

    .line 544331
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->j()I

    move-result v7

    .line 544332
    iput v7, v4, LX/4XB;->bb:I

    .line 544333
    move-object v7, v4

    .line 544334
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->b()I

    move-result v8

    .line 544335
    iput v8, v7, LX/4XB;->e:I

    .line 544336
    move-object v7, v7

    .line 544337
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->e()I

    move-result v8

    .line 544338
    iput v8, v7, LX/4XB;->N:I

    .line 544339
    move-object v7, v7

    .line 544340
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->c()I

    move-result v8

    .line 544341
    iput v8, v7, LX/4XB;->j:I

    .line 544342
    move-object v7, v7

    .line 544343
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->dv_()I

    move-result v8

    .line 544344
    iput v8, v7, LX/4XB;->O:I

    .line 544345
    move-object v7, v7

    .line 544346
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->m()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 544347
    iput-object v8, v7, LX/4XB;->bg:Ljava/lang/String;

    .line 544348
    move-object v7, v7

    .line 544349
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 544350
    iput-object v8, v7, LX/4XB;->bc:Ljava/lang/String;

    .line 544351
    move-object v7, v7

    .line 544352
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->dw_()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 544353
    iput-object v8, v7, LX/4XB;->aZ:Ljava/lang/String;

    .line 544354
    move-object v7, v7

    .line 544355
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->l()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 544356
    iput-object v8, v7, LX/4XB;->bd:Ljava/lang/String;

    .line 544357
    move-object v7, v7

    .line 544358
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->o()I

    move-result v8

    .line 544359
    iput v8, v7, LX/4XB;->bO:I

    .line 544360
    move-object v7, v7

    .line 544361
    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel$MediaModel;->n()I

    move-result v0

    .line 544362
    iput v0, v7, LX/4XB;->S:I

    .line 544363
    :cond_2
    invoke-virtual {v4}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 544364
    invoke-static {v6}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v4

    .line 544365
    iput-object v0, v4, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 544366
    move-object v0, v4

    .line 544367
    if-eqz v2, :cond_3

    .line 544368
    invoke-virtual {v2}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel$AttachmentsModel;->j()LX/0Px;

    move-result-object v2

    .line 544369
    iput-object v2, v0, LX/39x;->p:LX/0Px;

    .line 544370
    :cond_3
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 544371
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p3}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 544372
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    invoke-virtual {p3}, Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 544373
    iput-object v2, v0, LX/173;->f:Ljava/lang/String;

    .line 544374
    move-object v0, v0

    .line 544375
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 544376
    :cond_4
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    iget-object v2, p0, LX/3Hf;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v8

    .line 544377
    iput-wide v8, v0, LX/23u;->G:J

    .line 544378
    move-object v0, v0

    .line 544379
    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 544380
    iput-object v2, v0, LX/23u;->k:LX/0Px;

    .line 544381
    move-object v0, v0

    .line 544382
    if-eqz v3, :cond_5

    .line 544383
    iput-object v3, v0, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 544384
    :cond_5
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    .line 544385
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/3Hf;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)V

    goto/16 :goto_0

    :cond_6
    move-object v5, p2

    .line 544386
    goto/16 :goto_1

    :cond_7
    move v0, v2

    .line 544387
    goto/16 :goto_2

    :cond_8
    move-object v0, v3

    move-object v2, v3

    goto/16 :goto_3
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z
    .locals 5
    .param p1    # Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 544299
    if-ne p1, p2, :cond_0

    move v0, v2

    .line 544300
    :goto_0
    return v0

    .line 544301
    :cond_0
    if-nez p1, :cond_1

    move v0, v3

    .line 544302
    goto :goto_0

    .line 544303
    :cond_1
    iget-object v0, p0, LX/3Hf;->e:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 544304
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, LX/3Hf;->e:Ljava/util/Map;

    .line 544305
    iget-object v0, p0, LX/3Hf;->e:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544306
    iget-object v0, p0, LX/3Hf;->e:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544307
    iget-object v0, p0, LX/3Hf;->e:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544308
    iget-object v0, p0, LX/3Hf;->e:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_CANCELED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544309
    iget-object v0, p0, LX/3Hf;->e:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544310
    iget-object v0, p0, LX/3Hf;->e:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544311
    iget-object v0, p0, LX/3Hf;->e:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544312
    iget-object v0, p0, LX/3Hf;->e:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544313
    :cond_2
    iget-object v0, p0, LX/3Hf;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 544314
    iget-object v1, p0, LX/3Hf;->e:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 544315
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v1, v0, :cond_4

    :cond_3
    move v0, v3

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_0
.end method
