.class public LX/2IC;
.super LX/1r7;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1r7",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/301;


# direct methods
.method public constructor <init>(LX/301;)V
    .locals 0

    .prologue
    .line 391514
    iput-object p1, p0, LX/2IC;->a:LX/301;

    .line 391515
    invoke-direct {p0, p1}, LX/1r7;-><init>(Ljava/util/Map;)V

    .line 391516
    return-void
.end method

.method private a(LX/0Rl;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<-TK;>;)Z"
        }
    .end annotation

    .prologue
    .line 391517
    iget-object v0, p0, LX/2IC;->a:LX/301;

    iget-object v0, v0, LX/300;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, LX/2IC;->a:LX/301;

    iget-object v1, v1, LX/300;->b:LX/0Rl;

    invoke-static {p1}, LX/0PM;->a(LX/0Rl;)LX/0Rl;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0Rl;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 391518
    iget-object v0, p0, LX/2IC;->a:LX/301;

    invoke-virtual {v0, p1}, LX/301;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391519
    iget-object v0, p0, LX/2IC;->a:LX/301;

    iget-object v0, v0, LX/300;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 391520
    const/4 v0, 0x1

    .line 391521
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 391522
    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2IC;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 391523
    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v0

    invoke-static {v0}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2IC;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 391524
    invoke-virtual {p0}, LX/2IC;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 391525
    invoke-virtual {p0}, LX/2IC;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
