.class public LX/2Bb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:LX/2BN;


# direct methods
.method public constructor <init>(LX/2BN;)V
    .locals 0
    .param p1    # LX/2BN;
        .annotation runtime Lcom/facebook/katana/provider/legacykeyvalue/UserValuesManagerBackend;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 381370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381371
    iput-object p1, p0, LX/2Bb;->a:LX/2BN;

    .line 381372
    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 381373
    const-string v0, "current_account"

    const/4 v1, 0x0

    .line 381374
    invoke-static {p0}, LX/2Bb;->c(Landroid/content/Context;)LX/2Bb;

    move-result-object v2

    .line 381375
    iget-object v3, v2, LX/2Bb;->a:LX/2BN;

    .line 381376
    iget-object v4, v3, LX/2BN;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v3, v0}, LX/2BN;->b(LX/2BN;Ljava/lang/String;)LX/0Tn;

    move-result-object p0

    const/4 v2, 0x0

    invoke-interface {v4, p0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 381377
    if-nez p0, :cond_2

    .line 381378
    iget-object v4, v3, LX/2BN;->c:LX/2BL;

    invoke-virtual {v4, v0}, LX/2BL;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 381379
    if-nez p0, :cond_1

    const-string v4, "kvm_null_flag"

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v3, v0, v4, v2}, LX/2BN;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 381380
    :try_start_0
    iget-object v4, v3, LX/2BN;->c:LX/2BL;

    invoke-virtual {v4, v0}, LX/2BL;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v4, p0

    .line 381381
    :goto_1
    if-eqz v4, :cond_0

    const-string p0, "kvm_null_flag"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 381382
    :cond_0
    :goto_2
    move-object v3, v1

    .line 381383
    move-object v2, v3

    .line 381384
    move-object v0, v2

    .line 381385
    return-object v0

    :cond_1
    move-object v4, p0

    .line 381386
    goto :goto_0

    :catch_0
    :cond_2
    move-object v4, p0

    goto :goto_1

    :cond_3
    move-object v1, v4

    .line 381387
    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 381388
    if-nez p1, :cond_0

    .line 381389
    invoke-static {p0}, LX/2Bb;->c(Landroid/content/Context;)LX/2Bb;

    move-result-object v0

    const-string v1, "current_account"

    .line 381390
    iget-object p0, v0, LX/2Bb;->a:LX/2BN;

    .line 381391
    iget-object p1, p0, LX/2BN;->c:LX/2BL;

    invoke-virtual {p1, v1}, LX/2BL;->b(Ljava/lang/String;)V

    .line 381392
    iget-object p1, p0, LX/2BN;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p1

    invoke-static {p0, v1}, LX/2BN;->b(LX/2BN;Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-interface {p1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object p1

    invoke-interface {p1}, LX/0hN;->commit()V

    .line 381393
    :goto_0
    return-void

    .line 381394
    :cond_0
    invoke-static {p0}, LX/2Bb;->c(Landroid/content/Context;)LX/2Bb;

    move-result-object v0

    const-string v1, "current_account"

    .line 381395
    iget-object v2, v0, LX/2Bb;->a:LX/2BN;

    const/4 p0, 0x1

    invoke-virtual {v2, v1, p1, p0}, LX/2BN;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 381396
    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)LX/2Bb;
    .locals 3

    .prologue
    .line 381397
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 381398
    new-instance v2, LX/2Bb;

    .line 381399
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2BJ;->b(LX/0QB;)LX/2BL;

    move-result-object p0

    check-cast p0, LX/2BL;

    invoke-static {v1, p0}, LX/2BK;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2BL;)LX/2BN;

    move-result-object v1

    move-object v1, v1

    .line 381400
    check-cast v1, LX/2BN;

    invoke-direct {v2, v1}, LX/2Bb;-><init>(LX/2BN;)V

    .line 381401
    move-object v0, v2

    .line 381402
    check-cast v0, LX/2Bb;

    return-object v0
.end method
