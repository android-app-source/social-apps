.class public final enum LX/3FC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3FC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3FC;

.field public static final enum END_BLE_SCAN_FAIL:LX/3FC;

.field public static final enum END_BLE_SCAN_SUCCESS:LX/3FC;

.field public static final enum END_PAGE_LOOKUP_FAIL:LX/3FC;

.field public static final enum END_PAGE_LOOKUP_SUCCESS:LX/3FC;

.field public static final enum END_SCAN_SESSION_FAIL:LX/3FC;

.field public static final enum END_SCAN_SESSION_SUCCESS:LX/3FC;

.field public static final enum EVENT_TIP_CLICK:LX/3FC;

.field public static final enum EVENT_TIP_VIEW:LX/3FC;

.field public static final enum FEED_UNIT_CLICK:LX/3FC;

.field public static final enum FEED_UNIT_VPV:LX/3FC;

.field public static final enum GPS_LOCATION_CHECK_FAILED:LX/3FC;

.field public static final enum GPS_LOCATION_CHECK_SKIPPED:LX/3FC;

.field public static final enum GPS_LOCATION_REPORTED:LX/3FC;

.field public static final enum PAGE_NEARBY_INSERTED:LX/3FC;

.field public static final enum PAGE_NEARBY_INSERTED_2:LX/3FC;

.field public static final enum POST_COMPOSE_TOOLTIP_SEEN:LX/3FC;

.field public static final enum PRESENCE_CHANGED:LX/3FC;

.field public static final enum PRESENCE_STAYED_THE_SAME:LX/3FC;

.field public static final enum SEARCH_NULL_STATE_CLICK:LX/3FC;

.field public static final enum SEARCH_NULL_STATE_VPV:LX/3FC;

.field public static final enum START_BLE_SCAN:LX/3FC;

.field public static final enum START_PAGE_LOOKUP:LX/3FC;

.field public static final enum START_SCAN_SESSION:LX/3FC;


# instance fields
.field private final mEventName:Ljava/lang/String;

.field private final mResetSession:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 538645
    new-instance v0, LX/3FC;

    const-string v1, "START_SCAN_SESSION"

    const-string v2, "gravity_start_scan_session"

    invoke-direct {v0, v1, v5, v2, v4}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/3FC;->START_SCAN_SESSION:LX/3FC;

    .line 538646
    new-instance v0, LX/3FC;

    const-string v1, "END_SCAN_SESSION_SUCCESS"

    const-string v2, "gravity_end_scan_session_success"

    invoke-direct {v0, v1, v4, v2}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->END_SCAN_SESSION_SUCCESS:LX/3FC;

    .line 538647
    new-instance v0, LX/3FC;

    const-string v1, "END_SCAN_SESSION_FAIL"

    const-string v2, "gravity_end_scan_session_fail"

    invoke-direct {v0, v1, v6, v2}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->END_SCAN_SESSION_FAIL:LX/3FC;

    .line 538648
    new-instance v0, LX/3FC;

    const-string v1, "START_BLE_SCAN"

    const-string v2, "gravity_start_ble_scan"

    invoke-direct {v0, v1, v7, v2}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->START_BLE_SCAN:LX/3FC;

    .line 538649
    new-instance v0, LX/3FC;

    const-string v1, "END_BLE_SCAN_SUCCESS"

    const-string v2, "gravity_finish_ble_scan"

    invoke-direct {v0, v1, v8, v2}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->END_BLE_SCAN_SUCCESS:LX/3FC;

    .line 538650
    new-instance v0, LX/3FC;

    const-string v1, "END_BLE_SCAN_FAIL"

    const/4 v2, 0x5

    const-string v3, "gravity_finish_ble_scan_fail"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->END_BLE_SCAN_FAIL:LX/3FC;

    .line 538651
    new-instance v0, LX/3FC;

    const-string v1, "PRESENCE_CHANGED"

    const/4 v2, 0x6

    const-string v3, "gravity_location_changed"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->PRESENCE_CHANGED:LX/3FC;

    .line 538652
    new-instance v0, LX/3FC;

    const-string v1, "PRESENCE_STAYED_THE_SAME"

    const/4 v2, 0x7

    const-string v3, "gravity_no_location_change"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->PRESENCE_STAYED_THE_SAME:LX/3FC;

    .line 538653
    new-instance v0, LX/3FC;

    const-string v1, "PAGE_NEARBY_INSERTED"

    const/16 v2, 0x8

    const-string v3, "gravity_page_nearby_has_shown"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->PAGE_NEARBY_INSERTED:LX/3FC;

    .line 538654
    new-instance v0, LX/3FC;

    const-string v1, "PAGE_NEARBY_INSERTED_2"

    const/16 v2, 0x9

    const-string v3, "gravity_feed_unit_gen"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->PAGE_NEARBY_INSERTED_2:LX/3FC;

    .line 538655
    new-instance v0, LX/3FC;

    const-string v1, "START_PAGE_LOOKUP"

    const/16 v2, 0xa

    const-string v3, "gravity_start_page_lookup"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->START_PAGE_LOOKUP:LX/3FC;

    .line 538656
    new-instance v0, LX/3FC;

    const-string v1, "END_PAGE_LOOKUP_SUCCESS"

    const/16 v2, 0xb

    const-string v3, "gravity_page_lookup_success"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->END_PAGE_LOOKUP_SUCCESS:LX/3FC;

    .line 538657
    new-instance v0, LX/3FC;

    const-string v1, "END_PAGE_LOOKUP_FAIL"

    const/16 v2, 0xc

    const-string v3, "gravity_page_lookup_fail"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->END_PAGE_LOOKUP_FAIL:LX/3FC;

    .line 538658
    new-instance v0, LX/3FC;

    const-string v1, "GPS_LOCATION_REPORTED"

    const/16 v2, 0xd

    const-string v3, "gravity_gps_location_reported"

    invoke-direct {v0, v1, v2, v3, v4}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/3FC;->GPS_LOCATION_REPORTED:LX/3FC;

    .line 538659
    new-instance v0, LX/3FC;

    const-string v1, "GPS_LOCATION_CHECK_SKIPPED"

    const/16 v2, 0xe

    const-string v3, "gravity_gps_foreground_check_skipped"

    invoke-direct {v0, v1, v2, v3, v4}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/3FC;->GPS_LOCATION_CHECK_SKIPPED:LX/3FC;

    .line 538660
    new-instance v0, LX/3FC;

    const-string v1, "GPS_LOCATION_CHECK_FAILED"

    const/16 v2, 0xf

    const-string v3, "gravity_gps_foreground_check_failed"

    invoke-direct {v0, v1, v2, v3, v4}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/3FC;->GPS_LOCATION_CHECK_FAILED:LX/3FC;

    .line 538661
    new-instance v0, LX/3FC;

    const-string v1, "FEED_UNIT_VPV"

    const/16 v2, 0x10

    const-string v3, "gravity_feed_unit_vpv"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->FEED_UNIT_VPV:LX/3FC;

    .line 538662
    new-instance v0, LX/3FC;

    const-string v1, "FEED_UNIT_CLICK"

    const/16 v2, 0x11

    const-string v3, "gravity_feed_unit_click"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->FEED_UNIT_CLICK:LX/3FC;

    .line 538663
    new-instance v0, LX/3FC;

    const-string v1, "EVENT_TIP_VIEW"

    const/16 v2, 0x12

    const-string v3, "event_tip_view"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->EVENT_TIP_VIEW:LX/3FC;

    .line 538664
    new-instance v0, LX/3FC;

    const-string v1, "EVENT_TIP_CLICK"

    const/16 v2, 0x13

    const-string v3, "event_tip_click"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->EVENT_TIP_CLICK:LX/3FC;

    .line 538665
    new-instance v0, LX/3FC;

    const-string v1, "SEARCH_NULL_STATE_VPV"

    const/16 v2, 0x14

    const-string v3, "gravity_search_null_state_vpv"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->SEARCH_NULL_STATE_VPV:LX/3FC;

    .line 538666
    new-instance v0, LX/3FC;

    const-string v1, "SEARCH_NULL_STATE_CLICK"

    const/16 v2, 0x15

    const-string v3, "gravity_search_null_state_click"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->SEARCH_NULL_STATE_CLICK:LX/3FC;

    .line 538667
    new-instance v0, LX/3FC;

    const-string v1, "POST_COMPOSE_TOOLTIP_SEEN"

    const/16 v2, 0x16

    const-string v3, "gravity_post_compose_tooltip_seen"

    invoke-direct {v0, v1, v2, v3}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3FC;->POST_COMPOSE_TOOLTIP_SEEN:LX/3FC;

    .line 538668
    const/16 v0, 0x17

    new-array v0, v0, [LX/3FC;

    sget-object v1, LX/3FC;->START_SCAN_SESSION:LX/3FC;

    aput-object v1, v0, v5

    sget-object v1, LX/3FC;->END_SCAN_SESSION_SUCCESS:LX/3FC;

    aput-object v1, v0, v4

    sget-object v1, LX/3FC;->END_SCAN_SESSION_FAIL:LX/3FC;

    aput-object v1, v0, v6

    sget-object v1, LX/3FC;->START_BLE_SCAN:LX/3FC;

    aput-object v1, v0, v7

    sget-object v1, LX/3FC;->END_BLE_SCAN_SUCCESS:LX/3FC;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/3FC;->END_BLE_SCAN_FAIL:LX/3FC;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3FC;->PRESENCE_CHANGED:LX/3FC;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3FC;->PRESENCE_STAYED_THE_SAME:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3FC;->PAGE_NEARBY_INSERTED:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/3FC;->PAGE_NEARBY_INSERTED_2:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/3FC;->START_PAGE_LOOKUP:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/3FC;->END_PAGE_LOOKUP_SUCCESS:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/3FC;->END_PAGE_LOOKUP_FAIL:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/3FC;->GPS_LOCATION_REPORTED:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/3FC;->GPS_LOCATION_CHECK_SKIPPED:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/3FC;->GPS_LOCATION_CHECK_FAILED:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/3FC;->FEED_UNIT_VPV:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/3FC;->FEED_UNIT_CLICK:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/3FC;->EVENT_TIP_VIEW:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/3FC;->EVENT_TIP_CLICK:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/3FC;->SEARCH_NULL_STATE_VPV:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/3FC;->SEARCH_NULL_STATE_CLICK:LX/3FC;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/3FC;->POST_COMPOSE_TOOLTIP_SEEN:LX/3FC;

    aput-object v2, v0, v1

    sput-object v0, LX/3FC;->$VALUES:[LX/3FC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 538643
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/3FC;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    .line 538644
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 538635
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 538636
    iput-object p3, p0, LX/3FC;->mEventName:Ljava/lang/String;

    .line 538637
    iput-boolean p4, p0, LX/3FC;->mResetSession:Z

    .line 538638
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3FC;
    .locals 1

    .prologue
    .line 538642
    const-class v0, LX/3FC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3FC;

    return-object v0
.end method

.method public static values()[LX/3FC;
    .locals 1

    .prologue
    .line 538641
    sget-object v0, LX/3FC;->$VALUES:[LX/3FC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3FC;

    return-object v0
.end method


# virtual methods
.method public final createHoneyClientEvent()Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 538640
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v1, p0, LX/3FC;->mEventName:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final shouldResetSession()Z
    .locals 1

    .prologue
    .line 538639
    iget-boolean v0, p0, LX/3FC;->mResetSession:Z

    return v0
.end method
