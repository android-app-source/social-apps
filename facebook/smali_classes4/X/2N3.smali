.class public LX/2N3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final m:Ljava/lang/Object;


# instance fields
.field private final b:LX/2N4;

.field public final c:LX/2Nt;

.field public final d:LX/18V;

.field private final e:LX/2Nu;

.field private final f:LX/0Sh;

.field private final g:LX/0Uh;

.field public final h:LX/0TD;

.field private final i:LX/0Xl;

.field private final j:LX/0Yb;

.field private final k:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/6ed;",
            ">;>;"
        }
    .end annotation
.end field

.field public final l:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 398228
    const-class v0, LX/2N3;

    sput-object v0, LX/2N3;->a:Ljava/lang/Class;

    .line 398229
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2N3;->m:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2N4;LX/2Nt;LX/18V;LX/2Nu;LX/0Sh;LX/0Uh;LX/0TD;LX/0Xl;)V
    .locals 4
    .param p7    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p8    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/32 v2, 0x7b98a000

    .line 398214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398215
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/2N3;->k:LX/0QI;

    .line 398216
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/2N3;->l:LX/0QI;

    .line 398217
    iput-object p1, p0, LX/2N3;->b:LX/2N4;

    .line 398218
    iput-object p2, p0, LX/2N3;->c:LX/2Nt;

    .line 398219
    iput-object p3, p0, LX/2N3;->d:LX/18V;

    .line 398220
    iput-object p4, p0, LX/2N3;->e:LX/2Nu;

    .line 398221
    iput-object p5, p0, LX/2N3;->f:LX/0Sh;

    .line 398222
    iput-object p6, p0, LX/2N3;->g:LX/0Uh;

    .line 398223
    iput-object p7, p0, LX/2N3;->h:LX/0TD;

    .line 398224
    iput-object p8, p0, LX/2N3;->i:LX/0Xl;

    .line 398225
    iget-object v0, p0, LX/2N3;->i:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    sget-object v1, LX/0aY;->n:Ljava/lang/String;

    new-instance v2, LX/2Nv;

    invoke-direct {v2, p0}, LX/2Nv;-><init>(LX/2N3;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2N3;->j:LX/0Yb;

    .line 398226
    iget-object v0, p0, LX/2N3;->j:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 398227
    return-void
.end method

.method public static a(LX/0QB;)LX/2N3;
    .locals 7

    .prologue
    .line 398187
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 398188
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 398189
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 398190
    if-nez v1, :cond_0

    .line 398191
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 398192
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 398193
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 398194
    sget-object v1, LX/2N3;->m:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 398195
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 398196
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 398197
    :cond_1
    if-nez v1, :cond_4

    .line 398198
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 398199
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 398200
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/2N3;->b(LX/0QB;)LX/2N3;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 398201
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 398202
    if-nez v1, :cond_2

    .line 398203
    sget-object v0, LX/2N3;->m:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N3;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 398204
    :goto_1
    if-eqz v0, :cond_3

    .line 398205
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 398206
    :goto_3
    check-cast v0, LX/2N3;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 398207
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 398208
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 398209
    :catchall_1
    move-exception v0

    .line 398210
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 398211
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 398212
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 398213
    :cond_2
    :try_start_8
    sget-object v0, LX/2N3;->m:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N3;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 14

    .prologue
    .line 398068
    iget-object v5, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 398069
    iget-object v0, p0, LX/2N3;->l:LX/0QI;

    invoke-interface {v0, v5}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 398070
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 398071
    :cond_0
    invoke-direct {p0, v5}, LX/2N3;->a(Ljava/lang/String;)V

    .line 398072
    :goto_0
    return-void

    .line 398073
    :cond_1
    iget-object v6, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    .line 398074
    iget-object v0, p0, LX/2N3;->e:LX/2Nu;

    .line 398075
    const-string v1, "messenger_update_message_phase_two_start"

    invoke-static {v1, v5, v6}, LX/2Nu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 398076
    iget-object v2, v0, LX/2Nu;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 398077
    const/4 v2, 0x0

    .line 398078
    const/4 v1, 0x0

    .line 398079
    const/4 v0, 0x0

    .line 398080
    :try_start_0
    iget-object v3, p0, LX/2N3;->h:LX/0TD;

    new-instance v4, LX/FHw;

    invoke-direct {v4, p0, p1, v6}, LX/FHw;-><init>(LX/2N3;Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)V

    invoke-interface {v3, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v7

    move v4, v0

    .line 398081
    :goto_1
    const-wide/16 v8, 0x1388

    :try_start_1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v3, 0x1f2c8a5d

    invoke-static {v7, v8, v9, v0, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 398082
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    .line 398083
    iget-object v0, p0, LX/2N3;->e:LX/2Nu;

    .line 398084
    const-string v2, "messenger_update_message_phase_two_succeeded"

    invoke-static {v2, v5, v6}, LX/2Nu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 398085
    const-string v3, "retry_count"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 398086
    iget-object v3, v0, LX/2Nu;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 398087
    :goto_3
    invoke-direct {p0, v5}, LX/2N3;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 398088
    :catch_0
    move-exception v2

    .line 398089
    const/4 v0, 0x0

    :try_start_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2

    .line 398090
    :catch_1
    move-exception v3

    .line 398091
    const/4 v0, 0x5

    if-ge v1, v0, :cond_2

    .line 398092
    add-int/lit8 v1, v1, 0x1

    .line 398093
    int-to-long v8, v4

    int-to-long v10, v1

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long/2addr v8, v10

    long-to-int v0, v8

    .line 398094
    int-to-long v8, v0

    .line 398095
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    .line 398096
    move v4, v0

    goto :goto_1

    .line 398097
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    move-object v2, v3

    .line 398098
    goto :goto_2

    .line 398099
    :catch_2
    move-exception v2

    .line 398100
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2

    .line 398101
    :cond_3
    iget-object v0, p0, LX/2N3;->e:LX/2Nu;

    .line 398102
    const-string v3, "messenger_update_message_phase_two_failed"

    invoke-static {v3, v5, v6}, LX/2Nu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 398103
    const-string v4, "retry_count"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 398104
    if-eqz v2, :cond_4

    .line 398105
    const-string v4, "exception"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 398106
    :cond_4
    iget-object v4, v0, LX/2Nu;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 398107
    goto :goto_3
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 398184
    iget-object v0, p0, LX/2N3;->k:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 398185
    iget-object v0, p0, LX/2N3;->l:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 398186
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/2N3;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 398171
    monitor-enter p0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 398172
    :cond_0
    :try_start_0
    sget-object v0, LX/2N3;->a:Ljava/lang/Class;

    const-string v1, "messageId: %s, offlineThreadingId: %s, can\'t handle message sent"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398173
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 398174
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/2N3;->k:LX/0QI;

    invoke-interface {v0, p2}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 398175
    if-eqz v0, :cond_1

    .line 398176
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398177
    iget-object v0, p0, LX/2N3;->f:LX/0Sh;

    const-string v1, "Need to run DbFetchThreadHandler on non UI thread"

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 398178
    iget-object v0, p0, LX/2N3;->b:LX/2N4;

    invoke-virtual {v0, p1}, LX/2N4;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 398179
    if-eqz v0, :cond_1

    .line 398180
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 398181
    sget-object v1, LX/2N3;->a:Ljava/lang/Class;

    const-string v2, "offlineThreadingId doesn\'t match, expected: %s, actual: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 398182
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 398183
    :cond_3
    :try_start_2
    invoke-direct {p0, v0}, LX/2N3;->a(Lcom/facebook/messaging/model/messages/Message;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/2N3;
    .locals 9

    .prologue
    .line 398169
    new-instance v0, LX/2N3;

    invoke-static {p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v1

    check-cast v1, LX/2N4;

    invoke-static {p0}, LX/2Nt;->a(LX/0QB;)LX/2Nt;

    move-result-object v2

    check-cast v2, LX/2Nt;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    invoke-static {p0}, LX/2Nu;->a(LX/0QB;)LX/2Nu;

    move-result-object v4

    check-cast v4, LX/2Nu;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v8

    check-cast v8, LX/0Xl;

    invoke-direct/range {v0 .. v8}, LX/2N3;-><init>(LX/2N4;LX/2Nt;LX/18V;LX/2Nu;LX/0Sh;LX/0Uh;LX/0TD;LX/0Xl;)V

    .line 398170
    return-object v0
.end method

.method private static c(LX/2N3;Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 398164
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->PHOTO:LX/2MK;

    if-ne v1, v2, :cond_1

    .line 398165
    iget-object v1, p0, LX/2N3;->g:LX/0Uh;

    const/16 v2, 0x1f0

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 398166
    :cond_0
    :goto_0
    return v0

    .line 398167
    :cond_1
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-ne v1, v2, :cond_0

    .line 398168
    iget-object v1, p0, LX/2N3;->g:LX/0Uh;

    const/16 v2, 0x1f1

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    .line 398137
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/2N3;->c(LX/2N3;Lcom/facebook/ui/media/attachments/MediaResource;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 398138
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 398139
    :cond_1
    :try_start_1
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    .line 398140
    if-nez v1, :cond_2

    .line 398141
    sget-object v0, LX/2N3;->a:Ljava/lang/Class;

    const-string v1, "Offline threading id is null, can\'t remove hi-res upload"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 398142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 398143
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/2N3;->k:LX/0QI;

    invoke-interface {v0, v1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 398144
    if-eqz v0, :cond_0

    .line 398145
    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 398146
    if-nez p2, :cond_3

    .line 398147
    iget-object v2, p0, LX/2N3;->l:LX/0QI;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v1, v3}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 398148
    iget-object v2, p0, LX/2N3;->e:LX/2Nu;

    .line 398149
    const-string v4, "messenger_media_upload_phase_two_succeeded"

    invoke-static {v4, p1}, LX/2Nu;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 398150
    const-string v5, "elapsed_time"

    invoke-static {v2, p1}, LX/2Nu;->c(LX/2Nu;Lcom/facebook/ui/media/attachments/MediaResource;)J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 398151
    iget-object v5, v2, LX/2Nu;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 398152
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398153
    iget-object v0, p0, LX/2N3;->f:LX/0Sh;

    const-string v2, "Need to run DbFetchThreadHandler on non UI thread"

    invoke-virtual {v0, v2}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 398154
    iget-object v0, p0, LX/2N3;->b:LX/2N4;

    invoke-virtual {v0, v1}, LX/2N4;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 398155
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 398156
    invoke-direct {p0, v0}, LX/2N3;->a(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_0

    .line 398157
    :cond_3
    iget-object v2, p0, LX/2N3;->e:LX/2Nu;

    .line 398158
    const-string v4, "messenger_media_upload_phase_two_failed"

    invoke-static {v4, p1}, LX/2Nu;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 398159
    const-string v5, "elapsed_time"

    invoke-static {v2, p1}, LX/2Nu;->c(LX/2Nu;Lcom/facebook/ui/media/attachments/MediaResource;)J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 398160
    if-eqz p2, :cond_4

    .line 398161
    const-string v5, "exception"

    invoke-virtual {v4, v5, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 398162
    :cond_4
    iget-object v5, v2, LX/2Nu;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 398163
    goto :goto_1
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 398128
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->PHOTO:LX/2MK;

    if-eq v1, v2, :cond_1

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-eq v1, v2, :cond_1

    .line 398129
    :cond_0
    :goto_0
    return v0

    .line 398130
    :cond_1
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 398131
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 398132
    sget-object v1, LX/2N3;->a:Ljava/lang/Class;

    const-string v2, "OfflineThreadingId is null, can\'t use two phase send. Uri: %s, Source: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    aput-object v4, v3, v0

    const/4 v4, 0x1

    iget-object v5, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 398133
    :cond_2
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v2, LX/5zj;->MONTAGE:LX/5zj;

    if-eq v1, v2, :cond_3

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v2, LX/5zj;->MONTAGE_BACK:LX/5zj;

    if-eq v1, v2, :cond_3

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v2, LX/5zj;->MONTAGE_FRONT:LX/5zj;

    if-ne v1, v2, :cond_4

    .line 398134
    :cond_3
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-eq v1, v2, :cond_0

    .line 398135
    iget-object v1, p0, LX/2N3;->g:LX/0Uh;

    const/16 v2, 0x189

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    goto :goto_0

    .line 398136
    :cond_4
    invoke-static {p0, p1}, LX/2N3;->c(LX/2N3;Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    goto :goto_0
.end method

.method public final declared-synchronized b(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 398108
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/2N3;->c(LX/2N3;Lcom/facebook/ui/media/attachments/MediaResource;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 398109
    :goto_0
    monitor-exit p0

    return v0

    .line 398110
    :cond_0
    :try_start_1
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    .line 398111
    if-nez v0, :cond_1

    .line 398112
    sget-object v0, LX/2N3;->a:Ljava/lang/Class;

    const-string v2, "Offline threading id is null, can\'t add hi-res upload"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 398113
    goto :goto_0

    .line 398114
    :cond_1
    :try_start_2
    iget-object v2, p0, LX/2N3;->k:LX/0QI;

    new-instance v3, LX/FHv;

    invoke-direct {v3, p0, v0}, LX/FHv;-><init>(LX/2N3;Ljava/lang/String;)V

    invoke-interface {v2, v0, v3}, LX/0QI;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 398115
    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 398116
    iget-object v0, p0, LX/2N3;->e:LX/2Nu;

    .line 398117
    iget-object v4, v0, LX/2Nu;->b:LX/0QI;

    invoke-static {}, LX/0SW;->createStarted()LX/0SW;

    move-result-object v5

    invoke-interface {v4, p1, v5}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 398118
    const-string v4, "messenger_media_upload_phase_two_start"

    invoke-static {v4, p1}, LX/2Nu;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 398119
    const-string v5, "file_size_bytes"

    iget-wide v6, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    invoke-virtual {v4, v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 398120
    const-string v5, "width"

    iget v6, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 398121
    const-string v5, "height"

    iget v6, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 398122
    iget-object v5, v0, LX/2Nu;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 398123
    const/4 v0, 0x1

    goto :goto_0

    .line 398124
    :catch_0
    move-exception v0

    .line 398125
    :try_start_3
    sget-object v2, LX/2N3;->a:Ljava/lang/Class;

    const-string v3, "It\'s quite impossible but creating pending uploads list failed."

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 398126
    goto :goto_0

    .line 398127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
