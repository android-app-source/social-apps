.class public LX/3Q3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3H7;

.field public final b:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field private final c:LX/3Cm;

.field public final d:LX/1rn;

.field public final e:LX/1Ck;

.field public final f:LX/0xX;

.field public final g:LX/3Q4;


# direct methods
.method public constructor <init>(LX/3H7;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/3Cm;LX/1rn;LX/1Ck;LX/0xX;LX/3Q4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562998
    iput-object p1, p0, LX/3Q3;->a:LX/3H7;

    .line 562999
    iput-object p2, p0, LX/3Q3;->b:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 563000
    iput-object p3, p0, LX/3Q3;->c:LX/3Cm;

    .line 563001
    iput-object p4, p0, LX/3Q3;->d:LX/1rn;

    .line 563002
    iput-object p5, p0, LX/3Q3;->e:LX/1Ck;

    .line 563003
    iput-object p6, p0, LX/3Q3;->f:LX/0xX;

    .line 563004
    iput-object p7, p0, LX/3Q3;->g:LX/3Q4;

    .line 563005
    return-void
.end method

.method public static a(LX/0QB;)LX/3Q3;
    .locals 1

    .prologue
    .line 563006
    invoke-static {p0}, LX/3Q3;->b(LX/0QB;)LX/3Q3;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(LX/3Q3;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 563007
    invoke-static {p1}, LX/3Q3;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 4
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 563008
    if-nez p0, :cond_0

    .line 563009
    const/4 v0, 0x0

    .line 563010
    :goto_0
    return-object v0

    .line 563011
    :cond_0
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 563012
    invoke-static {p0}, LX/3Cm;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 563013
    if-eqz v0, :cond_2

    .line 563014
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    .line 563015
    :goto_1
    move-object v1, v0

    .line 563016
    if-eqz v1, :cond_1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 563017
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x578ec37f

    if-ne v0, v2, :cond_1

    .line 563018
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 563019
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/3Q3;
    .locals 8

    .prologue
    .line 563020
    new-instance v0, LX/3Q3;

    invoke-static {p0}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v1

    check-cast v1, LX/3H7;

    invoke-static {p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v2

    check-cast v2, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static {p0}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v3

    check-cast v3, LX/3Cm;

    invoke-static {p0}, LX/1rn;->a(LX/0QB;)LX/1rn;

    move-result-object v4

    check-cast v4, LX/1rn;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v6

    check-cast v6, LX/0xX;

    invoke-static {p0}, LX/3Q4;->a(LX/0QB;)LX/3Q4;

    move-result-object v7

    check-cast v7, LX/3Q4;

    invoke-direct/range {v0 .. v7}, LX/3Q3;-><init>(LX/3H7;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/3Cm;LX/1rn;LX/1Ck;LX/0xX;LX/3Q4;)V

    .line 563021
    return-object v0
.end method
