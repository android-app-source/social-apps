.class public LX/2Gn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Go;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:LX/2Gn;


# instance fields
.field public final a:LX/2H7;

.field public final c:LX/2H4;

.field private final d:LX/2Gs;

.field public final e:LX/2H0;

.field public final f:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

.field public final g:LX/09m;

.field private final h:LX/00H;

.field public final i:LX/09k;

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389460
    const-class v0, LX/2Gn;

    sput-object v0, LX/2Gn;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2Gp;LX/2Gq;LX/2Gr;LX/2Gs;Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/00H;Landroid/content/Context;LX/09k;LX/0Or;Ljava/util/concurrent/ExecutorService;)V
    .locals 3
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p10    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Gp;",
            "LX/2Gq;",
            "LX/2Gr;",
            "LX/2Gs;",
            "Lcom/facebook/push/registration/FacebookPushServerRegistrar;",
            "LX/00H;",
            "Landroid/content/Context;",
            "LX/09k;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389449
    iput-object p4, p0, LX/2Gn;->d:LX/2Gs;

    .line 389450
    iput-object p5, p0, LX/2Gn;->f:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    .line 389451
    iput-object p6, p0, LX/2Gn;->h:LX/00H;

    .line 389452
    iput-object p8, p0, LX/2Gn;->i:LX/09k;

    .line 389453
    iput-object p9, p0, LX/2Gn;->j:LX/0Or;

    .line 389454
    iput-object p10, p0, LX/2Gn;->k:Ljava/util/concurrent/ExecutorService;

    .line 389455
    new-instance v0, LX/09m;

    new-instance v1, LX/2Gz;

    invoke-direct {v1, p0}, LX/2Gz;-><init>(LX/2Gn;)V

    invoke-direct {v0, p7, v1}, LX/09m;-><init>(Landroid/content/Context;LX/08e;)V

    iput-object v0, p0, LX/2Gn;->g:LX/09m;

    .line 389456
    sget-object v0, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {p2, v0}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v0

    iput-object v0, p0, LX/2Gn;->e:LX/2H0;

    .line 389457
    sget-object v0, LX/2Ge;->FBNS_LITE:LX/2Ge;

    sget-object v1, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {p3, v1}, LX/2Gr;->a(LX/2Ge;)LX/2H3;

    move-result-object v1

    iget-object v2, p0, LX/2Gn;->e:LX/2H0;

    invoke-virtual {p1, v0, v1, v2}, LX/2Gp;->a(LX/2Ge;LX/2H3;LX/2H0;)LX/2H4;

    move-result-object v0

    iput-object v0, p0, LX/2Gn;->c:LX/2H4;

    .line 389458
    new-instance v0, LX/2H6;

    invoke-direct {v0, p0}, LX/2H6;-><init>(LX/2Gn;)V

    iput-object v0, p0, LX/2Gn;->a:LX/2H7;

    .line 389459
    return-void
.end method

.method public static a(LX/0QB;)LX/2Gn;
    .locals 14

    .prologue
    .line 389435
    sget-object v0, LX/2Gn;->l:LX/2Gn;

    if-nez v0, :cond_1

    .line 389436
    const-class v1, LX/2Gn;

    monitor-enter v1

    .line 389437
    :try_start_0
    sget-object v0, LX/2Gn;->l:LX/2Gn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389438
    if-eqz v2, :cond_0

    .line 389439
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 389440
    new-instance v3, LX/2Gn;

    const-class v4, LX/2Gp;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2Gp;

    invoke-static {v0}, LX/2Gq;->a(LX/0QB;)LX/2Gq;

    move-result-object v5

    check-cast v5, LX/2Gq;

    invoke-static {v0}, LX/2Gr;->a(LX/0QB;)LX/2Gr;

    move-result-object v6

    check-cast v6, LX/2Gr;

    invoke-static {v0}, LX/2Gs;->a(LX/0QB;)LX/2Gs;

    move-result-object v7

    check-cast v7, LX/2Gs;

    invoke-static {v0}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/0QB;)Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    move-result-object v8

    check-cast v8, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    const-class v9, LX/00H;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/00H;

    const-class v10, Landroid/content/Context;

    invoke-interface {v0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {v0}, LX/09k;->a(LX/0QB;)LX/09k;

    move-result-object v11

    check-cast v11, LX/09k;

    const/16 v12, 0x15e7

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v3 .. v13}, LX/2Gn;-><init>(LX/2Gp;LX/2Gq;LX/2Gr;LX/2Gs;Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/00H;Landroid/content/Context;LX/09k;LX/0Or;Ljava/util/concurrent/ExecutorService;)V

    .line 389441
    move-object v0, v3

    .line 389442
    sput-object v0, LX/2Gn;->l:LX/2Gn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389443
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389444
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389445
    :cond_1
    sget-object v0, LX/2Gn;->l:LX/2Gn;

    return-object v0

    .line 389446
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389447
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static f(LX/2Gn;)LX/2H9;
    .locals 1

    .prologue
    .line 389428
    iget-object v0, p0, LX/2Gn;->e:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v0

    .line 389429
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389430
    sget-object v0, LX/2H9;->NONE:LX/2H9;

    .line 389431
    :goto_0
    return-object v0

    .line 389432
    :cond_0
    iget-object v0, p0, LX/2Gn;->e:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 389433
    sget-object v0, LX/2H9;->UPGRADED:LX/2H9;

    goto :goto_0

    .line 389434
    :cond_1
    sget-object v0, LX/2H9;->CURRENT:LX/2H9;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 389461
    iget-object v0, p0, LX/2Gn;->k:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/push/fbnslite/FbnsLiteRegistrar$3;

    invoke-direct {v1, p0}, Lcom/facebook/push/fbnslite/FbnsLiteRegistrar$3;-><init>(LX/2Gn;)V

    const v2, -0x7483d0e7

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 389462
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 389397
    const/4 v0, 0x0

    .line 389398
    invoke-static {p0}, LX/2Gn;->f(LX/2Gn;)LX/2H9;

    move-result-object v1

    .line 389399
    sget-object v2, LX/2H9;->NONE:LX/2H9;

    if-ne v1, v2, :cond_0

    .line 389400
    :goto_0
    return-void

    .line 389401
    :cond_0
    iget-object v1, p0, LX/2Gn;->g:LX/09m;

    invoke-virtual {v1}, LX/09m;->d()V

    .line 389402
    invoke-virtual {p0}, LX/2Gn;->e()V

    .line 389403
    iget-object v1, p0, LX/2Gn;->c:LX/2H4;

    sget-object v2, LX/Cem;->ATTEMPT:LX/Cem;

    invoke-virtual {v2}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/2H4;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 389404
    iget-object v1, p0, LX/2Gn;->f:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v2, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {v1, v2, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/String;)Z

    move-result v1

    .line 389405
    if-eqz v1, :cond_1

    .line 389406
    iget-object v1, p0, LX/2Gn;->e:LX/2H0;

    invoke-virtual {v1}, LX/2H0;->h()V

    .line 389407
    iget-object v1, p0, LX/2Gn;->c:LX/2H4;

    sget-object v2, LX/Cem;->SUCCESS:LX/Cem;

    invoke-virtual {v2}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/2H4;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 389408
    :cond_1
    iget-object v1, p0, LX/2Gn;->c:LX/2H4;

    sget-object v2, LX/Cem;->FAILED:LX/Cem;

    invoke-virtual {v2}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/2H4;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 389387
    iget-object v0, p0, LX/2Gn;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389388
    :goto_0
    return-void

    .line 389389
    :cond_0
    invoke-static {p0}, LX/2Gn;->f(LX/2Gn;)LX/2H9;

    move-result-object v0

    .line 389390
    iget-object v1, p0, LX/2Gn;->d:LX/2Gs;

    sget-object v2, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {v2}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/2H9;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/2Gn;->e:LX/2H0;

    invoke-virtual {v4}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/2Gs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 389391
    sget-object v1, LX/Cet;->a:[I

    invoke-virtual {v0}, LX/2H9;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 389392
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, LX/2H9;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 389393
    :pswitch_0
    if-eqz p1, :cond_1

    .line 389394
    iget-object v0, p0, LX/2Gn;->f:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->FBNS_LITE:LX/2Ge;

    iget-object v2, p0, LX/2Gn;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_0

    .line 389395
    :cond_1
    iget-object v0, p0, LX/2Gn;->f:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->FBNS_LITE:LX/2Ge;

    iget-object v2, p0, LX/2Gn;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b(LX/2Ge;LX/2H7;)V

    goto :goto_0

    .line 389396
    :pswitch_1
    invoke-virtual {p0}, LX/2Gn;->b()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 389409
    iget-object v0, p0, LX/2Gn;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389410
    :goto_0
    return-void

    .line 389411
    :cond_0
    iget-object v0, p0, LX/2Gn;->c:LX/2H4;

    sget-object v1, LX/2gP;->ATTEMPT:LX/2gP;

    invoke-virtual {v1}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 389412
    iget-object v0, p0, LX/2Gn;->g:LX/09m;

    iget-object v1, p0, LX/2Gn;->h:LX/00H;

    invoke-virtual {v1}, LX/00H;->c()Ljava/lang/String;

    move-result-object v1

    .line 389413
    invoke-static {v0}, LX/09m;->e(LX/09m;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 389414
    const-string v4, "shared_flag"

    const/4 v5, -0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 389415
    iget-object v5, v0, LX/09m;->a:Landroid/content/Context;

    invoke-static {v5}, LX/04u;->d(Landroid/content/Context;)Z

    move-result v5

    .line 389416
    if-eqz v5, :cond_1

    iget-object v6, v0, LX/09m;->b:LX/08e;

    invoke-virtual {v6}, LX/08e;->c()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 389417
    iget-object v3, v0, LX/09m;->a:Landroid/content/Context;

    iget-object v4, v0, LX/09m;->a:Landroid/content/Context;

    invoke-static {v4}, LX/04u;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, LX/09o;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 389418
    :goto_1
    iget-object v0, p0, LX/2Gn;->c:LX/2H4;

    sget-object v1, LX/2gP;->SUCCESS:LX/2gP;

    invoke-virtual {v1}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 389419
    iget-object v0, p0, LX/2Gn;->c:LX/2H4;

    invoke-virtual {v0}, LX/2H4;->a()V

    goto :goto_0

    .line 389420
    :cond_1
    if-nez v5, :cond_2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    iget-object v4, v0, LX/09m;->a:Landroid/content/Context;

    invoke-static {v4}, LX/04u;->c(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 389421
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "register_and_stop"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 389422
    invoke-static {v3}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 389423
    :cond_2
    iget-object v3, v0, LX/09m;->a:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v3, v1, v4}, LX/09o;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final c()LX/2H7;
    .locals 1

    .prologue
    .line 389427
    iget-object v0, p0, LX/2Gn;->a:LX/2H7;

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 389424
    iget-object v0, p0, LX/2Gn;->c:LX/2H4;

    invoke-virtual {v0}, LX/2H4;->c()V

    .line 389425
    iget-object v0, p0, LX/2Gn;->c:LX/2H4;

    invoke-virtual {v0}, LX/2H4;->d()V

    .line 389426
    return-void
.end method
