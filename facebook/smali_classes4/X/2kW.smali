.class public LX/2kW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2kJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        "TResponseModel:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
        "<TTEdge;TTUserInfo;>;",
        "LX/2kJ",
        "<TTEdge;>;"
    }
.end annotation


# instance fields
.field public final a:Landroid/os/Handler;

.field private final b:LX/1mR;

.field public final c:LX/0Sh;

.field public final d:LX/03V;

.field public final e:LX/1rs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1rs",
            "<TTEdge;TTUserInfo;TTResponseModel;>;"
        }
    .end annotation
.end field

.field private final f:LX/2kZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kZ",
            "<TTEdge;TTUserInfo;TTResponseModel;>;"
        }
    .end annotation
.end field

.field public final g:LX/2kY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kY",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation
.end field

.field public final h:LX/2jy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2jy",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final i:LX/2jq;

.field private final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:LX/1s3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionControllerPreprocessor",
            "<TTEdge;TTUserInfo;TTResponseModel;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3DR",
            "<TTUserInfo;>;>;"
        }
    .end annotation
.end field

.field public final m:LX/2kV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kV",
            "<TTEdge;TTUserInfo;TTResponseModel;>;"
        }
    .end annotation
.end field

.field private final n:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public volatile o:LX/2kM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kM",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field public volatile p:Z


# direct methods
.method public constructor <init>(LX/1rs;LX/2jy;LX/2jq;JZLjava/lang/String;LX/1s3;Landroid/os/Handler;LX/2kV;LX/1mR;LX/0Sh;LX/03V;LX/2kX;)V
    .locals 10
    .param p1    # LX/1rs;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/2jy;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/2jq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/1s3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p10    # LX/2kV;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1rs",
            "<TTEdge;TTUserInfo;TTResponseModel;>;",
            "LX/2jy",
            "<TTEdge;>;",
            "LX/2jq;",
            "JZ",
            "Ljava/lang/String;",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionControllerPreprocessor",
            "<TTEdge;TTUserInfo;TTResponseModel;>;",
            "Landroid/os/Handler;",
            "LX/2kV;",
            "LX/1mR;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2kX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 456285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456286
    new-instance v2, LX/2kY;

    invoke-direct {v2}, LX/2kY;-><init>()V

    iput-object v2, p0, LX/2kW;->g:LX/2kY;

    .line 456287
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v2, p0, LX/2kW;->l:Ljava/util/concurrent/atomic/AtomicReference;

    .line 456288
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, LX/2kW;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 456289
    sget-object v2, LX/2kL;->a:LX/2kM;

    iput-object v2, p0, LX/2kW;->o:LX/2kM;

    .line 456290
    iput-object p1, p0, LX/2kW;->e:LX/1rs;

    .line 456291
    move-object/from16 v0, p10

    iput-object v0, p0, LX/2kW;->m:LX/2kV;

    .line 456292
    iput-object p2, p0, LX/2kW;->h:LX/2jy;

    .line 456293
    iput-object p3, p0, LX/2kW;->i:LX/2jq;

    .line 456294
    move-object/from16 v0, p7

    iput-object v0, p0, LX/2kW;->j:Ljava/lang/String;

    .line 456295
    move-object/from16 v0, p9

    iput-object v0, p0, LX/2kW;->a:Landroid/os/Handler;

    .line 456296
    move-object/from16 v0, p11

    iput-object v0, p0, LX/2kW;->b:LX/1mR;

    .line 456297
    move-object/from16 v0, p12

    iput-object v0, p0, LX/2kW;->c:LX/0Sh;

    .line 456298
    move-object/from16 v0, p13

    iput-object v0, p0, LX/2kW;->d:LX/03V;

    .line 456299
    move-object/from16 v0, p8

    iput-object v0, p0, LX/2kW;->k:LX/1s3;

    .line 456300
    invoke-interface {p2}, LX/2jy;->a()Ljava/lang/String;

    move-result-object v7

    move-object v3, p1

    move-wide v4, p4

    move/from16 v6, p6

    move-object/from16 v8, p7

    invoke-static/range {v3 .. v8}, LX/2kX;->a(LX/1rs;JZLjava/lang/String;Ljava/lang/String;)LX/2kZ;

    move-result-object v2

    iput-object v2, p0, LX/2kW;->f:LX/2kZ;

    .line 456301
    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 456281
    iget-object v0, p0, LX/2kW;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456282
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 456283
    :goto_0
    return-void

    .line 456284
    :cond_0
    iget-object v0, p0, LX/2kW;->a:Landroid/os/Handler;

    const v1, 0x1f33d1d1

    invoke-static {v0, p1, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public static b(LX/2kW;LX/2nj;LX/3DP;ILjava/lang/Object;)V
    .locals 10
    .param p3    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "ITTUserInfo;)V"
        }
    .end annotation

    .prologue
    .line 456255
    invoke-direct {p0, p3, p4}, LX/2kW;->d(ILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 456256
    :cond_0
    :goto_0
    return-void

    .line 456257
    :cond_1
    iget-object v0, p0, LX/2kW;->m:LX/2kV;

    iget-object v5, p0, LX/2kW;->o:LX/2kM;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/2kV;->a(LX/2nj;LX/3DP;ILjava/lang/Object;LX/2kM;)LX/3DR;

    move-result-object v0

    .line 456258
    if-eqz v0, :cond_0

    .line 456259
    iget-object v1, p0, LX/2kW;->c:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 456260
    iget-object v1, p0, LX/2kW;->g:LX/2kY;

    invoke-virtual {v1, p1, p2, p4}, LX/2kY;->a(LX/2nj;LX/3DP;Ljava/lang/Object;)V

    .line 456261
    :goto_1
    iget-object v1, p0, LX/2kW;->h:LX/2jy;

    invoke-interface {v1, p1, p2, p3}, LX/2jy;->a(LX/2nj;LX/3DP;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 456262
    iget-object v1, p0, LX/2kW;->m:LX/2kV;

    iget-object v2, p0, LX/2kW;->f:LX/2kZ;

    iget-object v3, p0, LX/2kW;->h:LX/2jy;

    const/4 v6, 0x1

    .line 456263
    iget-object v7, v2, LX/2kZ;->d:LX/1rs;

    invoke-interface {v7, v0, p4}, LX/1rs;->a(LX/3DR;Ljava/lang/Object;)LX/0gW;

    move-result-object v7

    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    .line 456264
    iget-wide v8, v2, LX/2kZ;->e:J

    invoke-static {v8, v9}, LX/5Ma;->a(J)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 456265
    iget-wide v8, v2, LX/2kZ;->e:J

    invoke-virtual {v7, v8, v9}, LX/0zO;->a(J)LX/0zO;

    .line 456266
    :cond_2
    iget-boolean v8, v2, LX/2kZ;->c:Z

    if-eqz v8, :cond_3

    .line 456267
    iput-boolean v6, v7, LX/0zO;->q:Z

    .line 456268
    move-object v8, v7

    .line 456269
    iget-object v9, v2, LX/2kZ;->a:Ljava/lang/String;

    .line 456270
    iput-object v9, v8, LX/0zO;->y:Ljava/lang/String;

    .line 456271
    invoke-interface {v3}, LX/2jy;->e()Z

    move-result v8

    if-nez v8, :cond_6

    .line 456272
    :goto_2
    iput-boolean v6, v7, LX/0zO;->p:Z

    .line 456273
    :cond_3
    iget-object v6, v2, LX/2kZ;->b:Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 456274
    iget-object v6, v2, LX/2kZ;->b:Ljava/lang/String;

    invoke-static {v6}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v6

    .line 456275
    iput-object v6, v7, LX/0zO;->d:Ljava/util/Set;

    .line 456276
    :cond_4
    move-object v2, v7

    .line 456277
    new-instance v3, LX/95P;

    invoke-direct {v3, p0, v0, p4}, LX/95P;-><init>(LX/2kW;LX/3DR;Ljava/lang/Object;)V

    move-object v3, v3

    .line 456278
    invoke-virtual {v1, v0, v2, v3}, LX/2kV;->a(LX/3DR;LX/0zO;LX/0Ve;)V

    goto :goto_0

    .line 456279
    :cond_5
    iget-object v1, p0, LX/2kW;->a:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/controller/connectioncontroller/ConnectionControllerImpl$3;

    invoke-direct {v2, p0, p1, p2, p4}, Lcom/facebook/controller/connectioncontroller/ConnectionControllerImpl$3;-><init>(LX/2kW;LX/2nj;LX/3DP;Ljava/lang/Object;)V

    const v3, 0x622e7a09

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_1

    .line 456280
    :cond_6
    const/4 v6, 0x0

    goto :goto_2
.end method

.method private d(ILjava/lang/Object;)Z
    .locals 8
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITTUserInfo;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 456250
    iget-boolean v0, p0, LX/2kW;->p:Z

    if-eqz v0, :cond_0

    .line 456251
    const/4 v0, 0x0

    .line 456252
    :goto_0
    return v0

    .line 456253
    :cond_0
    iget-object v7, p0, LX/2kW;->l:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, LX/3DR;

    sget-object v1, LX/2nj;->a:LX/2nj;

    sget-object v2, LX/3DP;->FIRST:LX/3DP;

    move-object v4, v3

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LX/3DR;-><init>(LX/2nj;LX/3DP;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    invoke-virtual {v7, v3, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 456254
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 456246
    iget-object v0, p0, LX/2kW;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456247
    iget-object v0, p0, LX/2kW;->h:LX/2jy;

    invoke-interface {v0, p0}, LX/2jy;->a(LX/2kJ;)V

    .line 456248
    iget-object v0, p0, LX/2kW;->h:LX/2jy;

    invoke-interface {v0}, LX/2jy;->b()V

    .line 456249
    :cond_0
    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITTUserInfo;)V"
        }
    .end annotation

    .prologue
    .line 456243
    iget-object v0, p0, LX/2kW;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 456244
    iget-object v0, p0, LX/2kW;->o:LX/2kM;

    invoke-interface {v0}, LX/2kM;->a()LX/2nj;

    move-result-object v0

    sget-object v1, LX/3DP;->FIRST:LX/3DP;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/2kW;->a(LX/2nj;LX/3DP;ILjava/lang/Object;)V

    .line 456245
    return-void
.end method

.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<TTEdge;>;",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 456239
    invoke-interface {p4}, LX/2kM;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    invoke-interface {p4}, LX/2kM;->c()I

    .line 456240
    iput-object p4, p0, LX/2kW;->o:LX/2kM;

    .line 456241
    iget-object v0, p0, LX/2kW;->g:LX/2kY;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/2kY;->a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V

    .line 456242
    return-void
.end method

.method public final a(LX/0Rl;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<TTEdge;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 456237
    iget-object v0, p0, LX/2kW;->h:LX/2jy;

    invoke-interface {v0, p1, p2}, LX/2jy;->a(LX/0Rl;Ljava/lang/String;)V

    .line 456238
    return-void
.end method

.method public final a(LX/1vq;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vq",
            "<TTEdge;TTUserInfo;>;)V"
        }
    .end annotation

    .prologue
    .line 456302
    iget-object v0, p0, LX/2kW;->g:LX/2kY;

    invoke-virtual {v0, p1}, LX/2kY;->a(LX/1vq;)V

    .line 456303
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 456221
    iget-object v0, p0, LX/2kW;->o:LX/2kM;

    .line 456222
    iput-object p1, p0, LX/2kW;->o:LX/2kM;

    .line 456223
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/2kW;->p:Z

    .line 456224
    invoke-interface {p1}, LX/2kM;->d()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    invoke-interface {p1}, LX/2kM;->c()I

    .line 456225
    invoke-interface {p1}, LX/2kM;->c()I

    move-result v1

    if-lez v1, :cond_0

    .line 456226
    new-instance v1, Lcom/facebook/controller/connectioncontroller/ConnectionControllerImpl$4;

    invoke-direct {v1, p0, p1, v0}, Lcom/facebook/controller/connectioncontroller/ConnectionControllerImpl$4;-><init>(LX/2kW;LX/2kM;LX/2kM;)V

    invoke-direct {p0, v1}, LX/2kW;->a(Ljava/lang/Runnable;)V

    .line 456227
    :cond_0
    iget-object v0, p0, LX/2kW;->l:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3DR;

    move-object v0, v0

    .line 456228
    if-eqz v0, :cond_2

    .line 456229
    invoke-interface {p1}, LX/2kM;->c()I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/2kW;->i:LX/2jq;

    sget-object v2, LX/2jq;->CHECK_SERVER_FOR_NEWDATA:LX/2jq;

    if-ne v1, v2, :cond_2

    .line 456230
    :cond_1
    invoke-interface {p1}, LX/2kM;->a()LX/2nj;

    move-result-object v1

    .line 456231
    iget-object v2, v0, LX/3DR;->b:LX/3DP;

    move-object v2, v2

    .line 456232
    iget v3, v0, LX/3DR;->e:I

    move v3, v3

    .line 456233
    iget-object v4, v0, LX/3DR;->f:Ljava/lang/Object;

    move-object v0, v4

    .line 456234
    invoke-static {p0, v1, v2, v3, v0}, LX/2kW;->b(LX/2kW;LX/2nj;LX/3DP;ILjava/lang/Object;)V

    .line 456235
    :cond_2
    new-instance v0, Lcom/facebook/controller/connectioncontroller/ConnectionControllerImpl$5;

    invoke-direct {v0, p0, p1}, Lcom/facebook/controller/connectioncontroller/ConnectionControllerImpl$5;-><init>(LX/2kW;LX/2kM;)V

    invoke-direct {p0, v0}, LX/2kW;->a(Ljava/lang/Runnable;)V

    .line 456236
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;)V
    .locals 3
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 456214
    iget-object v0, p0, LX/2kW;->m:LX/2kV;

    invoke-virtual {v0, p1, p2}, LX/2kV;->b(LX/2nj;LX/3DP;)LX/3DR;

    move-result-object v0

    .line 456215
    iget-object v1, p0, LX/2kW;->g:LX/2kY;

    if-eqz v0, :cond_0

    .line 456216
    iget-object v2, v0, LX/3DR;->f:Ljava/lang/Object;

    move-object v0, v2

    .line 456217
    :goto_0
    invoke-virtual {v1, p1, p2, v0}, LX/2kY;->b(LX/2nj;LX/3DP;Ljava/lang/Object;)V

    .line 456218
    iget-object v0, p0, LX/2kW;->m:LX/2kV;

    invoke-virtual {v0, p1, p2}, LX/2kV;->a(LX/2nj;LX/3DP;)V

    .line 456219
    return-void

    .line 456220
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/2nj;LX/3DP;ILjava/lang/Object;)V
    .locals 1
    .param p4    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "ITTUserInfo;)V"
        }
    .end annotation

    .prologue
    .line 456211
    iget-object v0, p0, LX/2kW;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 456212
    invoke-static {p0, p1, p2, p3, p4}, LX/2kW;->b(LX/2kW;LX/2nj;LX/3DP;ILjava/lang/Object;)V

    .line 456213
    return-void
.end method

.method public final a(Ljava/lang/String;LX/3Cf;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/3Cf",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 456208
    iget-object v0, p0, LX/2kW;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 456209
    iget-object v0, p0, LX/2kW;->h:LX/2jy;

    invoke-interface {v0, p1, p2}, LX/2jy;->a(Ljava/lang/String;LX/3Cf;)V

    .line 456210
    return-void
.end method

.method public final b(ILjava/lang/Object;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITTUserInfo;)V"
        }
    .end annotation

    .prologue
    .line 456205
    iget-object v0, p0, LX/2kW;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 456206
    iget-object v0, p0, LX/2kW;->o:LX/2kM;

    invoke-interface {v0}, LX/2kM;->b()LX/2nj;

    move-result-object v0

    sget-object v1, LX/3DP;->FIRST:LX/3DP;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/2kW;->a(LX/2nj;LX/3DP;ILjava/lang/Object;)V

    .line 456207
    return-void
.end method

.method public final b(LX/1vq;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vq",
            "<TTEdge;TTUserInfo;>;)V"
        }
    .end annotation

    .prologue
    .line 456203
    iget-object v0, p0, LX/2kW;->g:LX/2kY;

    invoke-virtual {v0, p1}, LX/2kY;->b(LX/1vq;)V

    .line 456204
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 456196
    iget-object v0, p0, LX/2kW;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 456197
    iget-object v0, p0, LX/2kW;->m:LX/2kV;

    invoke-virtual {v0}, LX/2kV;->a()V

    .line 456198
    iget-object v0, p0, LX/2kW;->m:LX/2kV;

    invoke-virtual {v0}, LX/2kV;->b()V

    .line 456199
    iget-object v0, p0, LX/2kW;->h:LX/2jy;

    invoke-interface {v0, p0}, LX/2jy;->b(LX/2kJ;)V

    .line 456200
    sget-object v0, LX/2kL;->a:LX/2kM;

    iput-object v0, p0, LX/2kW;->o:LX/2kM;

    .line 456201
    iget-object v0, p0, LX/2kW;->h:LX/2jy;

    invoke-interface {v0}, LX/2jy;->c()V

    .line 456202
    return-void
.end method

.method public final c(ILjava/lang/Object;)V
    .locals 3
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITTUserInfo;)V"
        }
    .end annotation

    .prologue
    .line 456187
    iget-object v0, p0, LX/2kW;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 456188
    iget-boolean v0, p0, LX/2kW;->p:Z

    if-nez v0, :cond_0

    .line 456189
    :goto_0
    return-void

    .line 456190
    :cond_0
    iget-object v0, p0, LX/2kW;->j:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 456191
    sget-object v0, LX/2nj;->a:LX/2nj;

    sget-object v1, LX/3DP;->FIRST:LX/3DP;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/2kW;->a(LX/2nj;LX/3DP;ILjava/lang/Object;)V

    goto :goto_0

    .line 456192
    :cond_1
    iget-object v0, p0, LX/2kW;->b:LX/1mR;

    iget-object v1, p0, LX/2kW;->j:Ljava/lang/String;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 456193
    new-instance v1, LX/95O;

    invoke-direct {v1, p0, p1, p2}, LX/95O;-><init>(LX/2kW;ILjava/lang/Object;)V

    .line 456194
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 456195
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 456183
    iget-object v0, p0, LX/2kW;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 456184
    iget-object v0, p0, LX/2kW;->b:LX/1mR;

    iget-object v1, p0, LX/2kW;->j:Ljava/lang/String;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 456185
    :cond_0
    iget-object v0, p0, LX/2kW;->h:LX/2jy;

    invoke-interface {v0}, LX/2jy;->d()V

    .line 456186
    return-void
.end method
