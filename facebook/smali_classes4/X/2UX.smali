.class public LX/2UX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416159
    iput-object p1, p0, LX/2UX;->a:LX/0Zb;

    .line 416160
    return-void
.end method

.method public static a(LX/0QB;)LX/2UX;
    .locals 1

    .prologue
    .line 416151
    invoke-static {p0}, LX/2UX;->b(LX/0QB;)LX/2UX;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2UX;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 416152
    const-string v0, "mswitch_accounts"

    .line 416153
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 416154
    iget-object v0, p0, LX/2UX;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 416155
    return-void
.end method

.method public static b(LX/0QB;)LX/2UX;
    .locals 2

    .prologue
    .line 416156
    new-instance v1, LX/2UX;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/2UX;-><init>(LX/0Zb;)V

    .line 416157
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 416144
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0}, LX/2UX;->a(LX/2UX;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 416145
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 416146
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 416147
    if-eqz p3, :cond_0

    .line 416148
    const-string v1, "extra"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 416149
    :cond_0
    invoke-static {p0, v0}, LX/2UX;->a(LX/2UX;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 416150
    return-void
.end method
