.class public final LX/2gz;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 449660
    iput-object p1, p0, LX/2gz;->a:Ljava/lang/String;

    iput-object p2, p0, LX/2gz;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 449661
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 449662
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 449663
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;

    .line 449664
    iget-object v1, v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;->a:Ljava/lang/String;

    .line 449665
    iget-object v0, v0, Lcom/facebook/katana/activity/codegenerator/data/FetchCodeResult;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 449666
    sget-object v0, LX/1CA;->e:LX/0Tn;

    iget-object v4, p0, LX/2gz;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 449667
    iget-object v4, p0, LX/2gz;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    invoke-interface {v4, v0, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 449668
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 449669
    sget-object v0, LX/1CA;->f:LX/0Tn;

    iget-object v2, p0, LX/2gz;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 449670
    iget-object v2, p0, LX/2gz;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    invoke-interface {v2, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 449671
    :cond_0
    return-void
.end method
