.class public final LX/2uo;
.super LX/AU5;
.source ""

# interfaces
.implements LX/AU6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AU5;",
        "LX/AU6",
        "<",
        "LX/BNy;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/Object;

.field private static final c:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 476978
    const/16 v0, 0x33

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "section_name"

    aput-object v1, v0, v3

    const-string v1, "node_id"

    aput-object v1, v0, v6

    const-string v1, "time_saved_ms"

    aput-object v1, v0, v4

    const-string v1, "graphql_cursor"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "group_title"

    aput-object v2, v0, v1

    const-string v1, "picture_uri"

    aput-object v1, v0, v5

    const/4 v1, 0x6

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "subtitle"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "type_display_extras"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "saved_state"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "is_item_viewed"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "url"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "permalink_id"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "permalink_url"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "source_story_id"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "source_message_other_user_id"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "source_message_thread_fbid"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "source_container_category"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "source_actor_name"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "source_actor_short_name"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "source_actor_picture_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "fb_link_url"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "is_playable"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "playlist"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "preferredPlayableUrlString"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "playable_url"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "playable_duration_ms"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "last_played_position_ms"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "target_graphql_object_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "global_share_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "global_share_graphql_object_type"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "instant_article_id"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "instant_article_canonical_url"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "instant_article_last_read_block_id"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "can_viewer_share_event"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "can_viewer_share_product"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "is_spherical"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "spherical_preferred_fov"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "spherical_fullscreen_aspect_ratio"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "spherical_inline_aspect_ratio"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "spherical_playable_url_sd_string"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "spherical_playable_url_hd_string"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "initial_view_heading_degrees"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "initial_view_pitch_degrees"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "initial_view_roll_degrees"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "media_content_size"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "section_name_server"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "is_download_client"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "saved_video_channel_id "

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "item_attribution"

    aput-object v2, v0, v1

    sput-object v0, LX/2uo;->a:[Ljava/lang/String;

    .line 476979
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "item.inserters.NetworkFetch"

    aput-object v1, v0, v3

    const-string v1, "item"

    aput-object v1, v0, v6

    sget-object v1, LX/2uo;->a:[Ljava/lang/String;

    aput-object v1, v0, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, LX/BO0;->a:[Ljava/lang/String;

    aput-object v2, v0, v1

    new-array v1, v4, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v5

    const/4 v1, 0x6

    const/4 v2, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, LX/2uo;->b:[Ljava/lang/Object;

    .line 476980
    const-class v0, LX/BNx;

    sput-object v0, LX/2uo;->c:Ljava/lang/Object;

    return-void

    .line 476981
    :array_0
    .array-data 4
        0x1
        0x0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 476982
    sget-object v0, LX/2uo;->b:[Ljava/lang/Object;

    sget-object v1, LX/2uo;->c:Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, LX/AU5;-><init>([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 476983
    return-void
.end method


# virtual methods
.method public final a(LX/AU7;)LX/AU2;
    .locals 2

    .prologue
    .line 476984
    new-instance v0, LX/BNz;

    invoke-direct {v0, p1}, LX/BNz;-><init>(LX/AU7;)V

    return-object v0
.end method
