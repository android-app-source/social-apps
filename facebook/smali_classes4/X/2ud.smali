.class public final LX/2ud;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:J

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 476044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 476045
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/2ud;->c:J

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)LX/2ud;
    .locals 2

    .prologue
    .line 476046
    iget-object v0, p1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    iput-object v0, p0, LX/2ud;->a:Ljava/lang/String;

    .line 476047
    iget-object v0, p1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    iput-object v0, p0, LX/2ud;->b:Ljava/lang/String;

    .line 476048
    iget-wide v0, p1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastLogout:J

    iput-wide v0, p0, LX/2ud;->c:J

    .line 476049
    iget-object v0, p1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->unseenCountsAccessToken:Ljava/lang/String;

    iput-object v0, p0, LX/2ud;->d:Ljava/lang/String;

    .line 476050
    iget-wide v0, p1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->lastUnseenTimestamp:J

    iput-wide v0, p0, LX/2ud;->e:J

    .line 476051
    return-object p0
.end method

.method public final f()Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;
    .locals 1

    .prologue
    .line 476052
    new-instance v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;-><init>(LX/2ud;)V

    return-object v0
.end method
