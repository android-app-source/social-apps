.class public LX/2t5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iE;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0tX;

.field private final d:LX/0xB;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/33u;

.field private final g:LX/0gX;

.field private final h:LX/0W3;

.field private i:LX/0gM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 474616
    const-class v0, LX/2t5;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2t5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0xB;LX/0Ot;LX/33u;LX/0gX;LX/0W3;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tX;",
            "LX/0xB;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/33u;",
            "LX/0gX;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 474631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474632
    iput-object p1, p0, LX/2t5;->b:Ljava/util/concurrent/ExecutorService;

    .line 474633
    iput-object p2, p0, LX/2t5;->c:LX/0tX;

    .line 474634
    iput-object p3, p0, LX/2t5;->d:LX/0xB;

    .line 474635
    iput-object p4, p0, LX/2t5;->e:LX/0Ot;

    .line 474636
    iput-object p5, p0, LX/2t5;->f:LX/33u;

    .line 474637
    iput-object p6, p0, LX/2t5;->g:LX/0gX;

    .line 474638
    iput-object p7, p0, LX/2t5;->h:LX/0W3;

    .line 474639
    return-void
.end method

.method public static a$redex0(LX/2t5;Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;Z)V
    .locals 4

    .prologue
    .line 474617
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 474618
    :cond_0
    invoke-static {p0}, LX/2t5;->h(LX/2t5;)V

    .line 474619
    :cond_1
    :goto_0
    return-void

    .line 474620
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 474621
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;->j()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 474622
    iget-object v1, p0, LX/2t5;->d:LX/0xB;

    sget-object v2, LX/12j;->MARKETPLACE:LX/12j;

    invoke-virtual {v1, v2}, LX/0xB;->a(LX/12j;)I

    move-result v1

    .line 474623
    iget-object v2, p0, LX/2t5;->d:LX/0xB;

    sget-object v3, LX/12j;->MARKETPLACE:LX/12j;

    invoke-virtual {v2, v3, v0}, LX/0xB;->a(LX/12j;I)V

    .line 474624
    if-nez p3, :cond_3

    if-eq v1, v0, :cond_1

    .line 474625
    :cond_3
    iget-object v0, p0, LX/2t5;->f:LX/33u;

    invoke-virtual {v0}, LX/33u;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 474626
    iget-object v0, p0, LX/2t5;->f:LX/33u;

    invoke-virtual {v0}, LX/33u;->c()LX/33y;

    move-result-object v0

    invoke-virtual {v0}, LX/33y;->k()LX/5pX;

    move-result-object v0

    .line 474627
    if-eqz v0, :cond_4

    .line 474628
    const-class v1, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    const-string v1, "MarketplaceNotificationsUpdate"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474629
    :cond_4
    goto :goto_0

    .line 474630
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/2t5;
    .locals 8

    .prologue
    .line 474612
    new-instance v0, LX/2t5;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v3

    check-cast v3, LX/0xB;

    const/16 v4, 0x259

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/33u;->a(LX/0QB;)LX/33u;

    move-result-object v5

    check-cast v5, LX/33u;

    invoke-static {p0}, LX/0gX;->a(LX/0QB;)LX/0gX;

    move-result-object v6

    check-cast v6, LX/0gX;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v7

    check-cast v7, LX/0W3;

    invoke-direct/range {v0 .. v7}, LX/2t5;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0xB;LX/0Ot;LX/33u;LX/0gX;LX/0W3;)V

    .line 474613
    return-object v0
.end method

.method public static h(LX/2t5;)V
    .locals 3

    .prologue
    .line 474614
    iget-object v0, p0, LX/2t5;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/2t5;->a:Ljava/lang/String;

    const-string v2, "Illegal result format for unseen batch request."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 474615
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 474608
    new-instance v0, LX/34C;

    invoke-direct {v0}, LX/34C;-><init>()V

    move-object v0, v0

    .line 474609
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 474610
    iget-object v1, p0, LX/2t5;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/2t6;

    invoke-direct {v1, p0}, LX/2t6;-><init>(LX/2t5;)V

    iget-object v2, p0, LX/2t5;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 474611
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 474606
    iget-object v0, p0, LX/2t5;->d:LX/0xB;

    sget-object v1, LX/12j;->MARKETPLACE:LX/12j;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0xB;->a(LX/12j;I)V

    .line 474607
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 474604
    invoke-virtual {p0}, LX/2t5;->e()V

    .line 474605
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 474596
    iget-object v0, p0, LX/2t5;->h:LX/0W3;

    sget-wide v2, LX/0X5;->jP:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    .line 474597
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2t5;->i:LX/0gM;

    if-eqz v0, :cond_1

    .line 474598
    :cond_0
    :goto_0
    return-void

    .line 474599
    :cond_1
    new-instance v0, LX/4Gv;

    invoke-direct {v0}, LX/4Gv;-><init>()V

    .line 474600
    new-instance v1, LX/GzS;

    invoke-direct {v1}, LX/GzS;-><init>()V

    move-object v1, v1

    .line 474601
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 474602
    :try_start_0
    iget-object v0, p0, LX/2t5;->g:LX/0gX;

    new-instance v2, LX/GzW;

    invoke-direct {v2, p0}, LX/GzW;-><init>(LX/2t5;)V

    invoke-virtual {v0, v1, v2}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v0

    iput-object v0, p0, LX/2t5;->i:LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 474603
    :catch_0
    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 474590
    iget-object v0, p0, LX/2t5;->i:LX/0gM;

    if-eqz v0, :cond_0

    .line 474591
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 474592
    iget-object v1, p0, LX/2t5;->i:LX/0gM;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 474593
    iget-object v1, p0, LX/2t5;->g:LX/0gX;

    invoke-virtual {v1, v0}, LX/0gX;->a(Ljava/util/Set;)V

    .line 474594
    const/4 v0, 0x0

    iput-object v0, p0, LX/2t5;->i:LX/0gM;

    .line 474595
    :cond_0
    return-void
.end method
