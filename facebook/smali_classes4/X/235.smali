.class public final LX/235;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/231;


# direct methods
.method public constructor <init>(LX/231;)V
    .locals 0

    .prologue
    .line 361885
    iput-object p1, p0, LX/235;->a:LX/231;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 361886
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 361887
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x0

    .line 361888
    if-nez p1, :cond_1

    .line 361889
    :cond_0
    :goto_0
    return-void

    .line 361890
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 361891
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 361892
    if-eqz v0, :cond_0

    .line 361893
    const/4 v1, 0x0

    .line 361894
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->k:Ljava/util/Map;

    move-object v2, v2

    .line 361895
    if-eqz v2, :cond_2

    .line 361896
    const-string v1, "comment_id"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 361897
    :cond_2
    iget-object v2, p0, LX/235;->a:LX/231;

    iget-object v2, v2, LX/231;->m:LX/0tn;

    invoke-virtual {v2, v0, v4, v1}, LX/0tn;->a(Lcom/facebook/graphql/model/GraphQLFeedback;ZLjava/lang/String;)V

    .line 361898
    iget-object v2, p0, LX/235;->a:LX/231;

    iget-object v2, v2, LX/231;->n:LX/0ad;

    sget-short v3, LX/0fe;->v:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 361899
    iget-object v2, p0, LX/235;->a:LX/231;

    iget-object v2, v2, LX/231;->m:LX/0tn;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3, v1}, LX/0tn;->a(Lcom/facebook/graphql/model/GraphQLFeedback;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 361900
    return-void
.end method
