.class public final enum LX/2Si;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Si;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Si;

.field public static final enum COLD_START:LX/2Si;

.field public static final enum NONE:LX/2Si;

.field public static final enum WARM_START:LX/2Si;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 413135
    new-instance v0, LX/2Si;

    const-string v1, "COLD_START"

    invoke-direct {v0, v1, v2}, LX/2Si;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Si;->COLD_START:LX/2Si;

    .line 413136
    new-instance v0, LX/2Si;

    const-string v1, "WARM_START"

    invoke-direct {v0, v1, v3}, LX/2Si;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Si;->WARM_START:LX/2Si;

    .line 413137
    new-instance v0, LX/2Si;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/2Si;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Si;->NONE:LX/2Si;

    .line 413138
    const/4 v0, 0x3

    new-array v0, v0, [LX/2Si;

    sget-object v1, LX/2Si;->COLD_START:LX/2Si;

    aput-object v1, v0, v2

    sget-object v1, LX/2Si;->WARM_START:LX/2Si;

    aput-object v1, v0, v3

    sget-object v1, LX/2Si;->NONE:LX/2Si;

    aput-object v1, v0, v4

    sput-object v0, LX/2Si;->$VALUES:[LX/2Si;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 413139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Si;
    .locals 1

    .prologue
    .line 413140
    const-class v0, LX/2Si;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Si;

    return-object v0
.end method

.method public static values()[LX/2Si;
    .locals 1

    .prologue
    .line 413141
    sget-object v0, LX/2Si;->$VALUES:[LX/2Si;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Si;

    return-object v0
.end method
