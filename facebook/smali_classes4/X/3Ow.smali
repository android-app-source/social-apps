.class public final LX/3Ow;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/3LT;


# direct methods
.method public constructor <init>(LX/3LT;)V
    .locals 0

    .prologue
    .line 560949
    iput-object p1, p0, LX/3Ow;->a:LX/3LT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x366eb575

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 560950
    iget-object v1, p0, LX/3Ow;->a:LX/3LT;

    .line 560951
    iget-object v3, v1, LX/3LT;->e:LX/3LR;

    const-class v4, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    const-string v5, "editFavorites"

    const/4 v6, 0x0

    .line 560952
    const/4 v8, 0x0

    .line 560953
    invoke-virtual {v3, v5}, LX/3LR;->a(Ljava/lang/String;)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v7

    .line 560954
    if-eqz v7, :cond_3

    .line 560955
    :goto_0
    move-object v7, v7

    .line 560956
    if-nez v7, :cond_2

    .line 560957
    :cond_0
    :goto_1
    move-object v3, v6

    .line 560958
    check-cast v3, Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;

    .line 560959
    if-eqz v3, :cond_1

    .line 560960
    iget-object v4, v1, LX/3LT;->c:LX/0fK;

    new-instance v5, LX/Jh7;

    invoke-direct {v5, v1, v3}, LX/Jh7;-><init>(LX/3LT;Lcom/facebook/messaging/contacts/favorites/DivebarEditFavoritesFragment;)V

    .line 560961
    iput-object v5, v4, LX/0fK;->m:LX/10s;

    .line 560962
    :cond_1
    const v1, -0x69001741

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 560963
    :cond_2
    invoke-static {v3}, LX/3LR;->e(LX/3LR;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 560964
    invoke-static {v3}, LX/3LR;->a(LX/3LR;)LX/0gc;

    move-result-object v8

    .line 560965
    invoke-virtual {v8}, LX/0gc;->a()LX/0hH;

    move-result-object v9

    .line 560966
    const v10, 0x7f040056

    const v11, 0x7f040092

    const p0, 0x7f040055

    const p1, 0x7f040093

    invoke-virtual {v9, v10, v11, p0, p1}, LX/0hH;->a(IIII)LX/0hH;

    .line 560967
    invoke-static {v3}, LX/3LR;->d(LX/3LR;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 560968
    const v10, 0x7f0d002f

    .line 560969
    :goto_2
    move v10, v10

    .line 560970
    invoke-virtual {v9, v10, v7, v5}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 560971
    invoke-virtual {v9, v6}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    .line 560972
    invoke-virtual {v9}, LX/0hH;->b()I

    .line 560973
    invoke-virtual {v8}, LX/0gc;->b()Z

    move-object v6, v7

    .line 560974
    goto :goto_1

    .line 560975
    :cond_3
    invoke-static {v3}, LX/3LR;->e(LX/3LR;)Z

    move-result v7

    if-nez v7, :cond_4

    move-object v7, v8

    .line 560976
    goto :goto_0

    .line 560977
    :cond_4
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/base/fragment/FbFragment;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 560978
    :catch_0
    move-object v7, v8

    goto :goto_0

    .line 560979
    :cond_5
    invoke-static {v3}, LX/3LR;->c(LX/3LR;)Landroid/app/Activity;

    move-result-object v10

    .line 560980
    instance-of v11, v10, LX/0fD;

    if-eqz v11, :cond_6

    .line 560981
    check-cast v10, LX/0fD;

    invoke-interface {v10}, LX/0fD;->e()LX/0fK;

    move-result-object v10

    .line 560982
    instance-of v11, v10, LX/0fL;

    if-eqz v11, :cond_6

    .line 560983
    check-cast v10, LX/0fL;

    .line 560984
    invoke-virtual {v10}, LX/0fL;->j()I

    move-result v10

    goto :goto_2

    .line 560985
    :cond_6
    const v10, 0x7f0d207e

    goto :goto_2
.end method
