.class public LX/2f0;
.super LX/2Ip;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/2Ip;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/1Fa;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/1Fa;",
            ">;",
            "Ljava/lang/ref/WeakReference",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 446129
    invoke-direct {p0}, LX/2Ip;-><init>()V

    .line 446130
    iput-object p1, p0, LX/2f0;->a:Ljava/lang/ref/WeakReference;

    .line 446131
    iput-object p2, p0, LX/2f0;->b:Ljava/lang/ref/WeakReference;

    .line 446132
    iput-object p3, p0, LX/2f0;->c:Ljava/lang/ref/WeakReference;

    .line 446133
    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 446134
    check-cast p1, LX/2f2;

    const/4 v4, 0x0

    .line 446135
    iget-object v0, p0, LX/2f0;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 446136
    iget-object v1, p0, LX/2f0;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Fa;

    .line 446137
    iget-object v2, p0, LX/2f0;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Pq;

    .line 446138
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    iget-wide v6, p1, LX/2f2;->a:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, LX/1Fa;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 446139
    :cond_0
    :goto_0
    return-void

    .line 446140
    :cond_1
    new-instance v5, LX/2en;

    invoke-interface {v1}, LX/1Fa;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, LX/2eo;->a(LX/1Fa;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-direct {v5, v3, v1}, LX/2en;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    move-object v1, v2

    .line 446141
    check-cast v1, LX/1Pr;

    invoke-interface {v1, v5, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2ep;

    .line 446142
    iget-boolean v3, v1, LX/2ep;->b:Z

    .line 446143
    iget-object v1, v1, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 446144
    iget-object v6, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v6, v1, :cond_0

    .line 446145
    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v6, :cond_2

    move v3, v4

    :cond_2
    move-object v1, v2

    .line 446146
    check-cast v1, LX/1Pr;

    new-instance v6, LX/2ep;

    iget-object v7, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v6, v7, v3}, LX/2ep;-><init>(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-interface {v1, v5, v6}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 446147
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v4

    invoke-interface {v2, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method
