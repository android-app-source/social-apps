.class public abstract LX/3Ga;
.super LX/3Gb;
.source ""


# instance fields
.field public a:LX/2pa;

.field private final b:Landroid/view/ViewStub;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 541327
    invoke-direct {p0, p1, p2, p3}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 541328
    invoke-virtual {p0}, LX/3Ga;->getStubLayout()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 541329
    const v0, 0x7f0d161a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/3Ga;->b:Landroid/view/ViewStub;

    .line 541330
    iget-object v0, p0, LX/3Ga;->b:Landroid/view/ViewStub;

    invoke-virtual {p0}, LX/3Ga;->getLayoutToInflate()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 541331
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Ga;->c:Z

    .line 541332
    return-void
.end method


# virtual methods
.method public final a(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V
    .locals 0

    .prologue
    .line 541324
    iput-object p3, p0, LX/3Ga;->a:LX/2pa;

    .line 541325
    invoke-super {p0, p1, p2, p3}, LX/3Gb;->a(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    .line 541326
    return-void
.end method

.method public final b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V
    .locals 0

    .prologue
    .line 541321
    iput-object p3, p0, LX/3Ga;->a:LX/2pa;

    .line 541322
    invoke-super {p0, p1, p2, p3}, LX/3Gb;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    .line 541323
    return-void
.end method

.method public abstract b(LX/2pa;)Z
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 541313
    iget-boolean v0, p0, LX/3Ga;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Ga;->a:LX/2pa;

    invoke-virtual {p0, v0}, LX/3Ga;->b(LX/2pa;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541314
    iget-object v0, p0, LX/3Ga;->b:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 541315
    iget-object v1, p0, LX/2oy;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 541316
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541317
    invoke-virtual {p0, v0}, LX/3Ga;->setupViews(Landroid/view/View;)V

    .line 541318
    iget-object v0, p0, LX/3Ga;->a:LX/2pa;

    invoke-virtual {p0, v0}, LX/3Ga;->setupPlugin(LX/2pa;)V

    .line 541319
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3Ga;->c:Z

    .line 541320
    :cond_0
    iget-boolean v0, p0, LX/3Ga;->c:Z

    return v0
.end method

.method public abstract getLayoutToInflate()I
.end method

.method public getStubLayout()I
    .locals 1

    .prologue
    .line 541312
    const v0, 0x7f031407

    return v0
.end method

.method public abstract setupPlugin(LX/2pa;)V
.end method

.method public abstract setupViews(Landroid/view/View;)V
.end method
