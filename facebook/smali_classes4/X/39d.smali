.class public LX/39d;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20W;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/39d",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/20W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 523501
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 523502
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/39d;->b:LX/0Zi;

    .line 523503
    iput-object p1, p0, LX/39d;->a:LX/0Ot;

    .line 523504
    return-void
.end method

.method public static a(LX/0QB;)LX/39d;
    .locals 4

    .prologue
    .line 523505
    const-class v1, LX/39d;

    monitor-enter v1

    .line 523506
    :try_start_0
    sget-object v0, LX/39d;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 523507
    sput-object v2, LX/39d;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523508
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523509
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 523510
    new-instance v3, LX/39d;

    const/16 p0, 0x866

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/39d;-><init>(LX/0Ot;)V

    .line 523511
    move-object v0, v3

    .line 523512
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 523513
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/39d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523514
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 523515
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 523516
    check-cast p2, LX/39h;

    .line 523517
    iget-object v0, p0, LX/39d;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20W;

    iget-object v2, p2, LX/39h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/39h;->b:LX/1zt;

    iget-object v4, p2, LX/39h;->c:LX/1Wk;

    iget-boolean v5, p2, LX/39h;->d:Z

    iget-object v6, p2, LX/39h;->e:LX/1Pr;

    move-object v1, p1

    .line 523518
    iget-object v7, v0, LX/20W;->a:LX/39j;

    invoke-virtual {v7, v1}, LX/39j;->c(LX/1De;)LX/39l;

    move-result-object v7

    invoke-virtual {v7, v2}, LX/39l;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39l;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/39l;->a(LX/1Pr;)LX/39l;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/39l;->a(LX/1Wk;)LX/39l;

    move-result-object p0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p1, LX/20X;->LIKE:LX/20X;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p1

    .line 523519
    iget-object v7, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 523520
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/39l;->b(Ljava/lang/String;)LX/39l;

    move-result-object v7

    sget-object p0, LX/20X;->LIKE:LX/20X;

    invoke-static {p0}, LX/39m;->a(LX/20X;)Landroid/util/SparseArray;

    move-result-object p0

    invoke-virtual {v7, p0}, LX/39l;->a(Landroid/util/SparseArray;)LX/39l;

    move-result-object p0

    .line 523521
    iget v7, v3, LX/1zt;->e:I

    move v7, v7

    .line 523522
    if-eqz v7, :cond_0

    sget-object v7, LX/1zt;->d:LX/1zt;

    if-ne v3, v7, :cond_2

    .line 523523
    :cond_0
    iget-object v7, v0, LX/20W;->c:LX/1VI;

    invoke-static {v1, v7}, LX/20W;->a(Landroid/content/Context;LX/1VI;)I

    move-result p1

    .line 523524
    const v7, 0x7f080fc8

    invoke-virtual {p0, v7}, LX/39l;->i(I)LX/39l;

    move-result-object v7

    invoke-virtual {v7, p1}, LX/39l;->j(I)LX/39l;

    move-result-object v7

    const p2, 0x7f081994

    invoke-virtual {v7, p2}, LX/39l;->h(I)LX/39l;

    .line 523525
    if-eqz v5, :cond_1

    .line 523526
    iget-object v7, v0, LX/20W;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1vg;

    invoke-virtual {v7, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v7

    invoke-virtual {v7, p1}, LX/2xv;->i(I)LX/2xv;

    move-result-object v7

    const p1, 0x7f0219c6

    invoke-virtual {v7, p1}, LX/2xv;->h(I)LX/2xv;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/39l;->a(LX/1n6;)LX/39l;

    .line 523527
    :cond_1
    :goto_0
    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 523528
    return-object v0

    .line 523529
    :cond_2
    iget v7, v3, LX/1zt;->e:I

    move v7, v7

    .line 523530
    const/4 p1, 0x1

    if-ne v7, p1, :cond_3

    .line 523531
    const v7, 0x7f0102a2

    const p1, -0xa76f01

    invoke-static {v1, v7, p1}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result p1

    .line 523532
    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const p2, 0x7f080fc8

    invoke-virtual {v7, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 523533
    invoke-virtual {p0, v7}, LX/39l;->b(Ljava/lang/CharSequence;)LX/39l;

    move-result-object p2

    invoke-virtual {p2, p1}, LX/39l;->j(I)LX/39l;

    move-result-object p2

    invoke-static {v1, v7}, LX/20W;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, LX/39l;->a(Ljava/lang/CharSequence;)LX/39l;

    .line 523534
    if-eqz v5, :cond_1

    .line 523535
    iget-object v7, v0, LX/20W;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1vg;

    invoke-virtual {v7, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v7

    invoke-virtual {v7, p1}, LX/2xv;->i(I)LX/2xv;

    move-result-object v7

    const p1, 0x7f0219c6

    invoke-virtual {v7, p1}, LX/2xv;->h(I)LX/2xv;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/39l;->a(LX/1n6;)LX/39l;

    goto :goto_0

    .line 523536
    :cond_3
    iget-object v7, v3, LX/1zt;->f:Ljava/lang/String;

    move-object v7, v7

    .line 523537
    invoke-virtual {p0, v7}, LX/39l;->b(Ljava/lang/CharSequence;)LX/39l;

    move-result-object v7

    .line 523538
    iget p1, v3, LX/1zt;->g:I

    move p1, p1

    .line 523539
    invoke-virtual {v7, p1}, LX/39l;->j(I)LX/39l;

    move-result-object v7

    .line 523540
    iget-object p1, v3, LX/1zt;->f:Ljava/lang/String;

    move-object p1, p1

    .line 523541
    invoke-static {v1, p1}, LX/20W;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, p1}, LX/39l;->a(Ljava/lang/CharSequence;)LX/39l;

    .line 523542
    if-eqz v5, :cond_1

    .line 523543
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v7

    invoke-virtual {v3}, LX/1zt;->e()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v7, p1}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/39l;->a(LX/1n6;)LX/39l;

    move-result-object v7

    const p1, 0x7f0b0fd7

    .line 523544
    iget-object p2, v7, LX/39l;->a:LX/39k;

    invoke-virtual {v7, p1}, LX/1Dp;->e(I)I

    move-result v0

    iput v0, p2, LX/39k;->n:I

    .line 523545
    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 523546
    invoke-static {}, LX/1dS;->b()V

    .line 523547
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/39i;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/39d",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 523548
    new-instance v1, LX/39h;

    invoke-direct {v1, p0}, LX/39h;-><init>(LX/39d;)V

    .line 523549
    iget-object v2, p0, LX/39d;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/39i;

    .line 523550
    if-nez v2, :cond_0

    .line 523551
    new-instance v2, LX/39i;

    invoke-direct {v2, p0}, LX/39i;-><init>(LX/39d;)V

    .line 523552
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/39i;->a$redex0(LX/39i;LX/1De;IILX/39h;)V

    .line 523553
    move-object v1, v2

    .line 523554
    move-object v0, v1

    .line 523555
    return-object v0
.end method
