.class public LX/2O2;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 400398
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;
    .locals 3

    .prologue
    .line 400399
    sget-object v0, LX/2O2;->a:Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;

    if-nez v0, :cond_1

    .line 400400
    const-class v1, LX/2O2;

    monitor-enter v1

    .line 400401
    :try_start_0
    sget-object v0, LX/2O2;->a:Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 400402
    if-eqz v2, :cond_0

    .line 400403
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 400404
    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object p0

    check-cast p0, LX/0W9;

    invoke-static {p0}, LX/2FB;->a(LX/0W9;)Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;

    move-result-object p0

    move-object v0, p0

    .line 400405
    sput-object v0, LX/2O2;->a:Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400406
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 400407
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 400408
    :cond_1
    sget-object v0, LX/2O2;->a:Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;

    return-object v0

    .line 400409
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 400410
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 400411
    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-static {v0}, LX/2FB;->a(LX/0W9;)Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;

    move-result-object v0

    return-object v0
.end method
