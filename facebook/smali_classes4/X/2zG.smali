.class public LX/2zG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Qu;
.implements LX/5Qv;
.implements LX/2rg;
.implements LX/2rh;
.implements LX/5Qw;
.implements LX/2ri;
.implements LX/5Qx;
.implements LX/5Qy;
.implements LX/5Qz;
.implements LX/5R0;
.implements LX/5R1;
.implements LX/5RE;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2sV;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AP9;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APB;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/31w;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APF;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APD;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AP7;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AP6;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APN;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APL;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APM;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2sH;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APP;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APG;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APH;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APA;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/31x;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APC;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APQ;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APx;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APh;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/API;",
            ">;"
        }
    .end annotation
.end field

.field private final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/APJ;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0il",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            ">;"
        }
    .end annotation
.end field

.field private final z:LX/0in;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0in",
            "<",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0il;LX/0in;)V
    .locals 1
    .param p25    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p26    # LX/0in;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2sV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AP9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APF;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/31w;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AP7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AP6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APN;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2sH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/31x;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/API;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/APJ;",
            ">;",
            "LX/0il",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            ">;",
            "LX/0in",
            "<",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483187
    iput-object p1, p0, LX/2zG;->a:LX/0Ot;

    .line 483188
    iput-object p2, p0, LX/2zG;->b:LX/0Ot;

    .line 483189
    iput-object p3, p0, LX/2zG;->c:LX/0Ot;

    .line 483190
    iput-object p6, p0, LX/2zG;->f:LX/0Ot;

    .line 483191
    iput-object p7, p0, LX/2zG;->g:LX/0Ot;

    .line 483192
    iput-object p4, p0, LX/2zG;->e:LX/0Ot;

    .line 483193
    iput-object p5, p0, LX/2zG;->d:LX/0Ot;

    .line 483194
    iput-object p8, p0, LX/2zG;->h:LX/0Ot;

    .line 483195
    iput-object p9, p0, LX/2zG;->i:LX/0Ot;

    .line 483196
    iput-object p10, p0, LX/2zG;->j:LX/0Ot;

    .line 483197
    iput-object p11, p0, LX/2zG;->k:LX/0Ot;

    .line 483198
    iput-object p12, p0, LX/2zG;->l:LX/0Ot;

    .line 483199
    iput-object p13, p0, LX/2zG;->m:LX/0Ot;

    .line 483200
    iput-object p14, p0, LX/2zG;->n:LX/0Ot;

    .line 483201
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2zG;->o:LX/0Ot;

    .line 483202
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2zG;->p:LX/0Ot;

    .line 483203
    move-object/from16 v0, p17

    iput-object v0, p0, LX/2zG;->q:LX/0Ot;

    .line 483204
    move-object/from16 v0, p18

    iput-object v0, p0, LX/2zG;->r:LX/0Ot;

    .line 483205
    move-object/from16 v0, p19

    iput-object v0, p0, LX/2zG;->s:LX/0Ot;

    .line 483206
    move-object/from16 v0, p20

    iput-object v0, p0, LX/2zG;->t:LX/0Ot;

    .line 483207
    move-object/from16 v0, p21

    iput-object v0, p0, LX/2zG;->u:LX/0Ot;

    .line 483208
    move-object/from16 v0, p22

    iput-object v0, p0, LX/2zG;->v:LX/0Ot;

    .line 483209
    move-object/from16 v0, p23

    iput-object v0, p0, LX/2zG;->w:LX/0Ot;

    .line 483210
    move-object/from16 v0, p24

    iput-object v0, p0, LX/2zG;->x:LX/0Ot;

    .line 483211
    move-object/from16 v0, p25

    iput-object v0, p0, LX/2zG;->y:LX/0il;

    .line 483212
    move-object/from16 v0, p26

    iput-object v0, p0, LX/2zG;->z:LX/0in;

    .line 483213
    return-void
.end method

.method public static J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;
    .locals 1

    .prologue
    .line 483215
    iget-object v0, p0, LX/2zG;->y:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    return-object v0
.end method

.method public static K(LX/2zG;)LX/AQ9;
    .locals 1

    .prologue
    .line 483216
    iget-object v0, p0, LX/2zG;->z:LX/0in;

    invoke-interface {v0}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ9;

    return-object v0
.end method

.method private L()Z
    .locals 4

    .prologue
    .line 483217
    iget-object v0, p0, LX/2zG;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AP6;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    .line 483218
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v3

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->SHARED:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-ne v3, p0, :cond_0

    .line 483219
    const/4 v3, 0x0

    .line 483220
    :goto_0
    move v0, v3

    .line 483221
    return v0

    :cond_0
    invoke-static {v0, v1}, LX/AP6;->a(LX/AP6;LX/2rw;)Z

    move-result v3

    goto :goto_0
.end method


# virtual methods
.method public final B()Z
    .locals 4

    .prologue
    .line 483222
    iget-object v0, p0, LX/2zG;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APA;

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v1

    .line 483223
    iget-object p0, v1, LX/AQ9;->Q:LX/ARN;

    move-object v1, p0

    .line 483224
    const/4 v3, 0x0

    .line 483225
    invoke-static {v1}, LX/APA;->c(LX/ARN;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/APA;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short p0, LX/1EB;->Q:S

    invoke-interface {v2, p0, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 483226
    return v0

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method public final C()Z
    .locals 6

    .prologue
    .line 483227
    iget-object v0, p0, LX/2zG;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-static {v3}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v3

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableFriendTagging()Z

    move-result v4

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v5

    .line 483228
    iget-object p0, v5, LX/AQ9;->s:LX/ARN;

    move-object v5, p0

    .line 483229
    invoke-static/range {v0 .. v5}, LX/APL;->a(LX/2rw;ZZZZLX/ARN;)Z

    move-result v0

    return v0
.end method

.method public final D()Z
    .locals 9

    .prologue
    .line 483230
    iget-object v0, p0, LX/2zG;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APM;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-static {v2}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v3

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v4

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v5

    const/4 v6, 0x0

    .line 483231
    sget-object v7, LX/2rw;->PAGE:LX/2rw;

    if-ne v1, v7, :cond_0

    if-nez v2, :cond_1

    .line 483232
    :cond_0
    :goto_0
    move v0, v6

    .line 483233
    return v0

    .line 483234
    :cond_1
    if-eqz v3, :cond_2

    if-eqz v4, :cond_0

    .line 483235
    :cond_2
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->hasTaggableProducts()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 483236
    iget-object v7, v0, LX/APM;->a:LX/0ad;

    sget-object v8, LX/0c0;->Live:LX/0c0;

    sget-short p0, LX/1EB;->N:S

    invoke-interface {v7, v8, p0, v6}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v6

    goto :goto_0
.end method

.method public final E()Z
    .locals 4

    .prologue
    .line 483262
    iget-object v0, p0, LX/2zG;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APN;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v3

    .line 483263
    iget-object v0, v3, LX/AQ9;->D:LX/ARN;

    move-object v3, v0

    .line 483264
    invoke-static {v2, v1, v3}, LX/APN;->c(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ZLX/ARN;)LX/03R;

    move-result-object v0

    .line 483265
    sget-object p0, LX/03R;->UNSET:LX/03R;

    if-eq v0, p0, :cond_1

    .line 483266
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    .line 483267
    :goto_1
    move v0, v0

    .line 483268
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object p0, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v0, p0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final H()Z
    .locals 8

    .prologue
    .line 483237
    iget-object v0, p0, LX/2zG;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p0}, LX/2zG;->t()Z

    move-result v0

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v1

    .line 483238
    iget-object v2, v1, LX/AQ9;->v:LX/ARN;

    move-object v1, v2

    .line 483239
    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v3

    invoke-direct {p0}, LX/2zG;->L()Z

    move-result v4

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v5

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v6

    invoke-interface {v6}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->canViewerEditPostMedia()Z

    move-result v6

    const/4 v7, 0x0

    .line 483240
    if-nez v0, :cond_1

    .line 483241
    :cond_0
    :goto_0
    move v0, v7

    .line 483242
    return v0

    .line 483243
    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, LX/ARN;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 483244
    :cond_2
    sget-object p0, LX/5Rn;->SAVE_DRAFT:LX/5Rn;

    if-eq v2, p0, :cond_0

    .line 483245
    if-eqz v3, :cond_3

    if-eqz v4, :cond_0

    .line 483246
    :cond_3
    if-eqz v5, :cond_4

    if-eqz v6, :cond_0

    .line 483247
    :cond_4
    const/4 v7, 0x1

    goto :goto_0
.end method

.method public final I()LX/5RF;
    .locals 9

    .prologue
    .line 483269
    iget-object v0, p0, LX/2zG;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APh;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v3

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v4

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v5

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v6

    invoke-interface {v6}, LX/0jA;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v6

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v7

    invoke-interface {v7}, LX/0jC;->getStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    move-result-object v7

    const/4 p0, 0x1

    .line 483270
    if-eqz v7, :cond_0

    .line 483271
    sget-object v8, LX/5RF;->STORYLINE:LX/5RF;

    .line 483272
    :goto_0
    move-object v0, v8

    .line 483273
    return-object v0

    .line 483274
    :cond_0
    if-eqz v6, :cond_1

    .line 483275
    sget-object v8, LX/5RF;->SLIDESHOW:LX/5RF;

    goto :goto_0

    .line 483276
    :cond_1
    if-eqz v3, :cond_2

    .line 483277
    sget-object v8, LX/5RF;->STICKER:LX/5RF;

    goto :goto_0

    .line 483278
    :cond_2
    invoke-static {v2}, LX/9J0;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 483279
    sget-object v8, LX/5RF;->GIF_VIDEO:LX/5RF;

    goto :goto_0

    .line 483280
    :cond_3
    if-eqz v2, :cond_4

    .line 483281
    sget-object v8, LX/5RF;->SHARE_ATTACHMENT:LX/5RF;

    goto :goto_0

    .line 483282
    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->h()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 483283
    sget-object v8, LX/5RF;->MINUTIAE:LX/5RF;

    goto :goto_0

    .line 483284
    :cond_5
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 483285
    if-nez v4, :cond_8

    if-eqz v5, :cond_8

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->b()Z

    move-result v8

    if-nez v8, :cond_8

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v8

    if-eqz v8, :cond_8

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->r()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v8

    if-eqz v8, :cond_8

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_8

    const/4 v8, 0x0

    .line 483286
    iget-object p0, v0, LX/APh;->a:LX/0ad;

    sget-short v1, LX/5HH;->c:S

    invoke-interface {p0, v1, v8}, LX/0ad;->a(SZ)Z

    move-result p0

    if-nez p0, :cond_6

    iget-object p0, v0, LX/APh;->a:LX/0ad;

    sget-short v1, LX/5HH;->e:S

    invoke-interface {p0, v1, v8}, LX/0ad;->a(SZ)Z

    move-result p0

    if-eqz p0, :cond_7

    :cond_6
    const/4 v8, 0x1

    :cond_7
    move v8, v8

    .line 483287
    if-eqz v8, :cond_8

    .line 483288
    sget-object v8, LX/5RF;->CHECKIN:LX/5RF;

    goto :goto_0

    .line 483289
    :cond_8
    sget-object v8, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    goto :goto_0

    .line 483290
    :cond_9
    invoke-static {v1}, LX/7kq;->k(LX/0Px;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 483291
    sget-object v8, LX/5RF;->GIF_VIDEO:LX/5RF;

    goto :goto_0

    .line 483292
    :cond_a
    invoke-static {v1}, LX/7kq;->o(LX/0Px;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 483293
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v8

    if-ne v8, p0, :cond_b

    sget-object v8, LX/5RF;->SINGLE_VIDEO:LX/5RF;

    goto/16 :goto_0

    :cond_b
    sget-object v8, LX/5RF;->MULTIPLE_VIDEOS:LX/5RF;

    goto/16 :goto_0

    .line 483294
    :cond_c
    invoke-static {v1}, LX/7kq;->m(LX/0Px;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 483295
    sget-object v8, LX/5RF;->MULTIMEDIA:LX/5RF;

    goto/16 :goto_0

    .line 483296
    :cond_d
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v8

    if-ne v8, p0, :cond_e

    sget-object v8, LX/5RF;->SINGLE_PHOTO:LX/5RF;

    goto/16 :goto_0

    :cond_e
    sget-object v8, LX/5RF;->MULTIPLE_PHOTOS:LX/5RF;

    goto/16 :goto_0
.end method

.method public final a()Z
    .locals 15

    .prologue
    .line 483257
    iget-object v0, p0, LX/2zG;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APx;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->t()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->hasPrivacyChanged()Z

    move-result v3

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v6

    invoke-interface {v6}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v6

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v7

    invoke-interface {v7}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v7

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v8

    invoke-interface {v8}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v8

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v9

    invoke-interface {v9}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v9

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v10

    invoke-interface {v10}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v10

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v11

    invoke-interface {v11}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v11

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v12

    invoke-interface {v12}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v12

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v13

    .line 483258
    iget-object v14, v13, LX/AQ9;->l:LX/ARN;

    move-object v13, v14

    .line 483259
    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v14

    .line 483260
    iget-object p0, v14, LX/AQ9;->k:LX/ARN;

    move-object v14, p0

    .line 483261
    invoke-virtual/range {v0 .. v14}, LX/APx;->a(LX/0Px;Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/ARN;LX/ARN;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 13

    .prologue
    .line 483297
    iget-object v0, p0, LX/2zG;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APx;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->hasPrivacyChanged()Z

    move-result v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v5

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v6

    invoke-interface {v6}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v6

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v7

    invoke-interface {v7}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v7

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v8

    invoke-interface {v8}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v8

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v9

    invoke-interface {v9}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v9

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v10

    invoke-interface {v10}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v10

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v11

    .line 483298
    iget-object v12, v11, LX/AQ9;->l:LX/ARN;

    move-object v11, v12

    .line 483299
    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v12

    .line 483300
    iget-object p0, v12, LX/AQ9;->k:LX/ARN;

    move-object v12, p0

    .line 483301
    invoke-virtual/range {v0 .. v12}, LX/APx;->a(LX/0Px;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/ARN;LX/ARN;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 12

    .prologue
    .line 483254
    iget-object v0, p0, LX/2zG;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APx;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->hasPrivacyChanged()Z

    move-result v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v5

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v6

    invoke-interface {v6}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v6

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v7

    invoke-interface {v7}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v7

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v8

    invoke-interface {v8}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v8

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v9

    invoke-interface {v9}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v9

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v10

    invoke-interface {v10}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v10

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v11

    .line 483255
    iget-object p0, v11, LX/AQ9;->k:LX/ARN;

    move-object v11, p0

    .line 483256
    invoke-virtual/range {v0 .. v11}, LX/APx;->a(LX/0Px;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;LX/ARN;)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 10

    .prologue
    .line 483251
    iget-object v0, p0, LX/2zG;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AP6;

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v1

    .line 483252
    iget-object v2, v1, LX/AQ9;->f:LX/ARN;

    move-object v1, v2

    .line 483253
    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    iget-wide v4, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v4

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v5

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v6

    invoke-interface {v6}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableAttachToAlbum()Z

    move-result v6

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v7

    invoke-interface {v7}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v7

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v8

    invoke-interface {v8}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v8

    if-eqz v8, :cond_0

    const/4 v8, 0x1

    :goto_0
    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v9

    invoke-interface {v9}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v9

    invoke-static {v9}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v9

    invoke-virtual/range {v0 .. v9}, LX/AP6;->a(LX/ARN;LX/2rw;Ljava/lang/String;LX/5Rn;ZZLX/0Px;ZZ)Z

    move-result v0

    return v0

    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 3

    .prologue
    .line 483248
    iget-object v0, p0, LX/2zG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v1

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v2

    .line 483249
    iget-object p0, v2, LX/AQ9;->n:LX/ARN;

    move-object v2, p0

    .line 483250
    invoke-static {v0, v1, v2}, LX/2sV;->a(ZZLX/ARN;)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 18

    .prologue
    .line 483214
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2zG;->s:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/APQ;

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v4

    invoke-virtual {v4}, LX/AQ9;->g()LX/ARN;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v6

    invoke-interface {v6}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v6

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v7

    invoke-interface {v7}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v8

    invoke-interface {v8}, LX/0iy;->getMarketplaceId()J

    move-result-wide v8

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v10

    invoke-interface {v10}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v11

    invoke-interface {v11}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v12

    invoke-interface {v12}, LX/0iw;->isFeedOnlyPost()Z

    move-result v12

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v13

    invoke-interface {v13}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v14

    invoke-interface {v14}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v15

    invoke-interface {v15}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v17

    invoke-virtual/range {v2 .. v17}, LX/APQ;->a(Ljava/lang/String;LX/ARN;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;JLcom/facebook/ipc/composer/model/ProductItemAttachment;Lcom/facebook/ipc/composer/intent/ComposerTargetData;ZLcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/share/model/ComposerAppAttribution;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Z

    move-result v2

    return v2
.end method

.method public final j()Z
    .locals 4

    .prologue
    .line 483127
    iget-object v0, p0, LX/2zG;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 483128
    :goto_0
    invoke-static {v2}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result p0

    if-eqz p0, :cond_2

    if-eqz v3, :cond_0

    iget-object p0, v3, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-nez p0, :cond_2

    :cond_0
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    const/4 p0, 0x1

    :goto_1
    move v0, p0

    .line 483129
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final k()Z
    .locals 7

    .prologue
    .line 483139
    iget-object v0, p0, LX/2zG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AP9;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-static {v2}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v3

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v5

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v6

    .line 483140
    iget-object p0, v6, LX/AQ9;->j:LX/ARN;

    move-object v6, p0

    .line 483141
    invoke-virtual/range {v0 .. v6}, LX/AP9;->a(LX/2rw;ZZZLX/5Rn;LX/ARN;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 483142
    iget-object v0, p0, LX/2zG;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v0

    .line 483143
    iget-object p0, v0, LX/AQ9;->Q:LX/ARN;

    move-object v0, p0

    .line 483144
    invoke-static {v0}, LX/APA;->c(LX/ARN;)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 483145
    iget-object v0, p0, LX/2zG;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APA;

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v1

    .line 483146
    iget-object v0, v1, LX/AQ9;->Q:LX/ARN;

    move-object v1, v0

    .line 483147
    invoke-static {v1}, LX/APA;->c(LX/ARN;)Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 5

    .prologue
    .line 483130
    iget-object v0, p0, LX/2zG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APB;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {p0}, LX/2zG;->h()Z

    move-result v2

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v3

    .line 483131
    iget-object p0, v3, LX/AQ9;->p:LX/ARN;

    move-object v3, p0

    .line 483132
    const/4 p0, 0x0

    .line 483133
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->f()Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v4

    if-nez v4, :cond_0

    move v4, p0

    .line 483134
    :goto_0
    move v0, v4

    .line 483135
    return v0

    .line 483136
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->e()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v4

    if-nez v4, :cond_1

    if-eqz v2, :cond_1

    if-eqz v3, :cond_2

    invoke-interface {v3}, LX/ARN;->a()Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    move v4, p0

    .line 483137
    goto :goto_0

    .line 483138
    :cond_2
    iget-object v4, v0, LX/APB;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/9kA;

    invoke-virtual {v4}, LX/9kA;->a()Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x1

    goto :goto_0

    :cond_3
    move v4, p0

    goto :goto_0
.end method

.method public final o()Z
    .locals 4

    .prologue
    .line 483148
    iget-object v0, p0, LX/2zG;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APC;

    invoke-virtual {p0}, LX/2zG;->h()Z

    move-result v1

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    .line 483149
    if-eqz v1, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->g()LX/0Px;

    move-result-object p0

    if-eqz p0, :cond_2

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->g()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_2

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object p0

    if-nez p0, :cond_2

    const/4 p0, 0x0

    .line 483150
    if-eqz v2, :cond_3

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 483151
    iget-object v1, v0, LX/APC;->a:LX/0ad;

    sget-short v3, LX/1EB;->af:S

    invoke-interface {v1, v3, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, LX/APC;->a:LX/0ad;

    sget-short v3, LX/1EB;->ae:S

    invoke-interface {v1, v3, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 p0, 0x1

    .line 483152
    :cond_1
    :goto_0
    move p0, p0

    .line 483153
    if-eqz p0, :cond_2

    const/4 p0, 0x1

    :goto_1
    move v0, p0

    .line 483154
    return v0

    :cond_2
    const/4 p0, 0x0

    goto :goto_1

    :cond_3
    iget-object v1, v0, LX/APC;->a:LX/0ad;

    sget-short v3, LX/1EB;->ae:S

    invoke-interface {v1, v3, p0}, LX/0ad;->a(SZ)Z

    move-result p0

    goto :goto_0
.end method

.method public final p()Z
    .locals 6

    .prologue
    .line 483155
    iget-object v0, p0, LX/2zG;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APF;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v2

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v3

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v5

    .line 483156
    iget-object p0, v5, LX/AQ9;->i:LX/ARN;

    move-object v5, p0

    .line 483157
    invoke-virtual/range {v0 .. v5}, LX/APF;->a(LX/2rw;ZZZLX/ARN;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 4

    .prologue
    .line 483158
    iget-object v0, p0, LX/2zG;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APD;

    invoke-virtual {p0}, LX/2zG;->H()Z

    move-result v1

    const/4 v2, 0x0

    .line 483159
    if-nez v1, :cond_0

    .line 483160
    :goto_0
    move v0, v2

    .line 483161
    return v0

    :cond_0
    iget-object v3, v0, LX/APD;->d:LX/0Uh;

    const/16 p0, 0x267

    invoke-virtual {v3, p0, v2}, LX/0Uh;->a(IZ)Z

    move-result v2

    goto :goto_0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 483162
    iget-object v0, p0, LX/2zG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/31w;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v0, v1}, LX/31w;->a(LX/2rw;)Z

    move-result v0

    return v0
.end method

.method public final t()Z
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 483163
    iget-object v0, p0, LX/2zG;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APD;

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v1

    .line 483164
    iget-object v2, v1, LX/AQ9;->c:LX/ARN;

    move-object v1, v2

    .line 483165
    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v3, v8

    :goto_0
    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisablePhotos()Z

    move-result v4

    if-nez v4, :cond_1

    move v4, v8

    :goto_1
    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v5

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v6

    invoke-interface {v6}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->canViewerEditPostMedia()Z

    move-result v6

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v7

    invoke-interface {v7}, LX/0jA;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v7

    if-eqz v7, :cond_2

    move v7, v8

    :goto_2
    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v10

    invoke-interface {v10}, LX/0jC;->getStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    move-result-object v10

    if-eqz v10, :cond_3

    :goto_3
    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v9

    invoke-interface {v9}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v9

    invoke-virtual/range {v0 .. v9}, LX/APD;->a(LX/ARN;LX/2rw;ZZZZZZLX/0Px;)Z

    move-result v0

    return v0

    :cond_0
    move v3, v9

    goto :goto_0

    :cond_1
    move v4, v9

    goto :goto_1

    :cond_2
    move v7, v9

    goto :goto_2

    :cond_3
    move v8, v9

    goto :goto_3
.end method

.method public final u()Z
    .locals 3

    .prologue
    .line 483166
    invoke-virtual {p0}, LX/2zG;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2zG;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-static {v1}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v1

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    .line 483167
    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    sget-object p0, LX/2rw;->PAGE:LX/2rw;

    if-ne v2, p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 483168
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 483169
    iget-object v0, p0, LX/2zG;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APA;

    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v1

    .line 483170
    iget-object v0, v1, LX/AQ9;->Q:LX/ARN;

    move-object v1, v0

    .line 483171
    invoke-static {v1}, LX/APA;->c(LX/ARN;)Z

    move-result v0

    return v0
.end method

.method public final w()Z
    .locals 5

    .prologue
    .line 483172
    iget-object v0, p0, LX/2zG;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APG;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-static {v1}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v1

    const/4 v2, 0x0

    .line 483173
    iget-object v3, v0, LX/APG;->a:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-short p0, LX/1EB;->O:S

    invoke-interface {v3, v4, p0, v2}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 483174
    :cond_0
    move v0, v1

    .line 483175
    return v0
.end method

.method public final x()Z
    .locals 3

    .prologue
    .line 483176
    iget-object v0, p0, LX/2zG;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-static {p0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v2

    .line 483177
    iget-object p0, v2, LX/AQ9;->L:LX/ARN;

    move-object v2, p0

    .line 483178
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a()Z

    move-result p0

    if-eqz p0, :cond_2

    if-nez v0, :cond_2

    if-eqz v2, :cond_0

    invoke-interface {v2}, LX/ARN;->a()Z

    move-result p0

    if-eqz p0, :cond_2

    :cond_0
    const/4 p0, 0x1

    :goto_1
    move v0, p0

    .line 483179
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 483180
    iget-object v0, p0, LX/2zG;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/API;

    .line 483181
    iget-object p0, v0, LX/API;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/7Dh;

    invoke-virtual {p0}, LX/7Dh;->d()Z

    move-result p0

    move v0, p0

    .line 483182
    return v0
.end method

.method public final z()Z
    .locals 1

    .prologue
    .line 483183
    iget-object v0, p0, LX/2zG;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/APJ;

    .line 483184
    iget-object p0, v0, LX/APJ;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/7Dh;

    invoke-virtual {p0}, LX/7Dh;->b()Z

    move-result p0

    move v0, p0

    .line 483185
    return v0
.end method
