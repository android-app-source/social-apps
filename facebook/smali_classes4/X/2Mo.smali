.class public LX/2Mo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final d:Ljava/util/regex/Pattern;

.field private static volatile k:LX/2Mo;


# instance fields
.field private final a:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "LX/0ZL",
            "<",
            "LX/CMv;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Tn;

.field public final c:LX/0Tn;

.field public final e:Landroid/content/Context;

.field public final f:LX/0SG;

.field private final g:LX/0kb;

.field public final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final i:LX/12x;

.field private final j:LX/0Sg;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 397558
    const-string v0, "mobile"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/2Mo;->d:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/0kb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/12x;LX/0Sg;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397572
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "network_bandwidth/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2Mo;->b:LX/0Tn;

    .line 397573
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "networks"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2Mo;->c:LX/0Tn;

    .line 397574
    iput-object p1, p0, LX/2Mo;->e:Landroid/content/Context;

    .line 397575
    iput-object p2, p0, LX/2Mo;->f:LX/0SG;

    .line 397576
    iput-object p3, p0, LX/2Mo;->g:LX/0kb;

    .line 397577
    iput-object p4, p0, LX/2Mo;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 397578
    iput-object p5, p0, LX/2Mo;->i:LX/12x;

    .line 397579
    iput-object p6, p0, LX/2Mo;->j:LX/0Sg;

    .line 397580
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0xf

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/2Mo;->a:LX/0QI;

    .line 397581
    iget-object v0, p0, LX/2Mo;->j:LX/0Sg;

    const-string v1, "SetupPurgeNetworkBandwidthSharedPrefsBroadcastReceiver"

    new-instance v2, Lcom/facebook/messaging/media/bandwidth/MediaBandwidthManager$1;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/media/bandwidth/MediaBandwidthManager$1;-><init>(LX/2Mo;)V

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_LOW_PRIORITY:LX/0VZ;

    sget-object v4, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 397582
    return-void
.end method

.method private static declared-synchronized a(LX/2Mo;Ljava/lang/String;)LX/0ZL;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0ZL",
            "<",
            "LX/CMv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 397559
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Mo;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ZL;

    .line 397560
    if-nez v0, :cond_1

    .line 397561
    new-instance v1, LX/0ZL;

    const/16 v0, 0xf

    invoke-direct {v1, v0}, LX/0ZL;-><init>(I)V

    .line 397562
    iget-object v2, p0, LX/2Mo;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, LX/2Mo;->b:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397563
    iget-object v2, p0, LX/2Mo;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, LX/2Mo;->b:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v3, ""

    invoke-interface {v2, v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 397564
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 397565
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 397566
    invoke-static {}, LX/CMv;->values()[LX/CMv;

    move-result-object v3

    aget-object v4, v2, v0

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, LX/0ZL;->a(Ljava/lang/Object;)V

    .line 397567
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 397568
    :cond_0
    iget-object v0, p0, LX/2Mo;->a:LX/0QI;

    invoke-interface {v0, p1, v1}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    .line 397569
    :cond_1
    monitor-exit p0

    return-object v0

    .line 397570
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/0QB;)LX/2Mo;
    .locals 10

    .prologue
    .line 397533
    sget-object v0, LX/2Mo;->k:LX/2Mo;

    if-nez v0, :cond_1

    .line 397534
    const-class v1, LX/2Mo;

    monitor-enter v1

    .line 397535
    :try_start_0
    sget-object v0, LX/2Mo;->k:LX/2Mo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 397536
    if-eqz v2, :cond_0

    .line 397537
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 397538
    new-instance v3, LX/2Mo;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v8

    check-cast v8, LX/12x;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v9

    check-cast v9, LX/0Sg;

    invoke-direct/range {v3 .. v9}, LX/2Mo;-><init>(Landroid/content/Context;LX/0SG;LX/0kb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/12x;LX/0Sg;)V

    .line 397539
    move-object v0, v3

    .line 397540
    sput-object v0, LX/2Mo;->k:LX/2Mo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397541
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 397542
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397543
    :cond_1
    sget-object v0, LX/2Mo;->k:LX/2Mo;

    return-object v0

    .line 397544
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 397545
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static h(LX/2Mo;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 397546
    iget-object v0, p0, LX/2Mo;->g:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v0

    .line 397547
    if-eqz v0, :cond_2

    .line 397548
    const-string v1, "WIFI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 397549
    iget-object v0, p0, LX/2Mo;->g:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->o()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 397550
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "W"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 397551
    :goto_1
    return-object v0

    .line 397552
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 397553
    :cond_1
    sget-object v1, LX/2Mo;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 397554
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "M"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2Mo;->g:LX/0kb;

    .line 397555
    iget-object v2, v1, LX/0kb;->c:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 397556
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 397557
    :cond_2
    const-string v0, "N"

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 397528
    invoke-static {p0}, LX/2Mo;->h(LX/2Mo;)Ljava/lang/String;

    move-result-object v0

    .line 397529
    monitor-enter p0

    .line 397530
    :try_start_0
    invoke-static {p0, v0}, LX/2Mo;->a(LX/2Mo;Ljava/lang/String;)LX/0ZL;

    move-result-object v0

    .line 397531
    sget-object v1, LX/CMv;->FAILING:LX/CMv;

    invoke-virtual {v0, v1}, LX/0ZL;->a(Ljava/lang/Object;)V

    .line 397532
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(JJ)V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 397504
    long-to-double v0, p1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v0, v2

    long-to-double v2, p3

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    mul-double/2addr v0, v2

    .line 397505
    invoke-static {p0}, LX/2Mo;->h(LX/2Mo;)Ljava/lang/String;

    move-result-object v2

    .line 397506
    monitor-enter p0

    .line 397507
    :try_start_0
    invoke-static {p0, v2}, LX/2Mo;->a(LX/2Mo;Ljava/lang/String;)LX/0ZL;

    move-result-object v3

    .line 397508
    const-wide v5, 0x4062c00000000000L    # 150.0

    cmpg-double v5, v0, v5

    if-gez v5, :cond_2

    .line 397509
    sget-object v5, LX/CMv;->POOR:LX/CMv;

    .line 397510
    :goto_0
    move-object v0, v5

    .line 397511
    invoke-virtual {v3, v0}, LX/0ZL;->a(Ljava/lang/Object;)V

    .line 397512
    iget-object v1, p0, LX/2Mo;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v0, p0, LX/2Mo;->b:LX/0Tn;

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 397513
    iget-object v0, p0, LX/2Mo;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/2Mo;->c:LX/0Tn;

    const-string v4, ""

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 397514
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 397515
    iget-object v1, p0, LX/2Mo;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v4, p0, LX/2Mo;->c:LX/0Tn;

    invoke-interface {v1, v4, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 397516
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, LX/0ZL;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CMv;

    invoke-virtual {v0}, LX/CMv;->ordinal()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 397517
    const/4 v0, 0x1

    move v1, v0

    :goto_1
    invoke-virtual {v3}, LX/0ZL;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 397518
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, LX/0ZL;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CMv;

    invoke-virtual {v0}, LX/CMv;->ordinal()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397519
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 397520
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 397521
    iget-object v0, p0, LX/2Mo;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    iget-object v0, p0, LX/2Mo;->b:LX/0Tn;

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v4, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 397522
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 397523
    :cond_2
    const-wide v5, 0x4081300000000000L    # 550.0

    cmpg-double v5, v0, v5

    if-gez v5, :cond_3

    .line 397524
    sget-object v5, LX/CMv;->MODERATE:LX/CMv;

    goto/16 :goto_0

    .line 397525
    :cond_3
    const-wide v5, 0x409f400000000000L    # 2000.0

    cmpg-double v5, v0, v5

    if-gez v5, :cond_4

    .line 397526
    sget-object v5, LX/CMv;->GOOD:LX/CMv;

    goto/16 :goto_0

    .line 397527
    :cond_4
    sget-object v5, LX/CMv;->EXCELLENT:LX/CMv;

    goto/16 :goto_0
.end method

.method public final b()LX/CMt;
    .locals 12

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const/4 v3, 0x0

    .line 397486
    invoke-static {p0}, LX/2Mo;->h(LX/2Mo;)Ljava/lang/String;

    move-result-object v0

    .line 397487
    monitor-enter p0

    .line 397488
    :try_start_0
    invoke-static {p0, v0}, LX/2Mo;->a(LX/2Mo;Ljava/lang/String;)LX/0ZL;

    move-result-object v0

    .line 397489
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0ZL;->d()I

    move-result v1

    if-nez v1, :cond_1

    .line 397490
    :cond_0
    new-instance v0, LX/CMt;

    sget-object v1, LX/CMv;->UNKNOWN:LX/CMv;

    sget-object v2, LX/CMs;->UNKNOWN:LX/CMs;

    invoke-direct {v0, v1, v2}, LX/CMt;-><init>(LX/CMv;LX/CMs;)V

    monitor-exit p0

    .line 397491
    :goto_0
    return-object v0

    .line 397492
    :cond_1
    invoke-virtual {v0}, LX/0ZL;->b()Ljava/util/List;

    move-result-object v5

    .line 397493
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397494
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 397495
    sget-object v4, LX/CMs;->LOW:LX/CMs;

    .line 397496
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CMv;

    .line 397497
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CMv;

    .line 397498
    invoke-virtual {v0}, LX/CMv;->ordinal()I

    move-result v7

    invoke-virtual {v1}, LX/CMv;->ordinal()I

    move-result v1

    sub-int v1, v7, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v1, v2

    move v2, v1

    .line 397499
    goto :goto_1

    .line 397500
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 397501
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    const/4 v6, 0x1

    if-le v1, v6, :cond_3

    int-to-double v6, v2

    mul-double/2addr v6, v10

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    int-to-double v8, v1

    div-double/2addr v6, v8

    cmpg-double v1, v6, v10

    if-gtz v1, :cond_3

    .line 397502
    sget-object v1, LX/CMs;->HIGH:LX/CMs;

    .line 397503
    :goto_2
    new-instance v2, LX/CMt;

    invoke-direct {v2, v0, v1}, LX/CMt;-><init>(LX/CMv;LX/CMs;)V

    move-object v0, v2

    goto :goto_0

    :cond_3
    move-object v1, v4

    goto :goto_2
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 397479
    iget-object v1, p0, LX/2Mo;->g:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 397480
    :cond_0
    :goto_0
    return v0

    .line 397481
    :cond_1
    invoke-virtual {p0}, LX/2Mo;->b()LX/CMt;

    move-result-object v1

    .line 397482
    iget-object v2, v1, LX/CMt;->b:LX/CMs;

    .line 397483
    iget-object v1, v1, LX/CMt;->a:LX/CMv;

    .line 397484
    sget-object v3, LX/CMs;->HIGH:LX/CMs;

    if-ne v2, v3, :cond_0

    sget-object v2, LX/CMv;->EXCELLENT:LX/CMv;

    if-ne v1, v2, :cond_0

    .line 397485
    const/4 v0, 0x1

    goto :goto_0
.end method
