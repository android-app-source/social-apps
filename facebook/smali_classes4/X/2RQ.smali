.class public LX/2RQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2RR;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2RQ;
    .locals 1

    .prologue
    .line 409824
    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2RQ;
    .locals 2

    .prologue
    .line 409820
    new-instance v0, LX/2RQ;

    invoke-direct {v0}, LX/2RQ;-><init>()V

    .line 409821
    const/16 v1, 0x41c

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    .line 409822
    iput-object v1, v0, LX/2RQ;->a:LX/0Or;

    .line 409823
    return-object v0
.end method


# virtual methods
.method public final a()LX/2RR;
    .locals 1

    .prologue
    .line 409826
    iget-object v0, p0, LX/2RQ;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2RR;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/2RR;
    .locals 2

    .prologue
    .line 409816
    invoke-virtual {p0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 409817
    iput-object v1, v0, LX/2RR;->d:Ljava/util/Collection;

    .line 409818
    move-object v0, v0

    .line 409819
    return-object v0
.end method

.method public final a(Ljava/util/Collection;I)LX/2RR;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/2RU;",
            ">;I)",
            "LX/2RR;"
        }
    .end annotation

    .prologue
    .line 409804
    invoke-virtual {p0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    .line 409805
    iput-object p1, v0, LX/2RR;->b:Ljava/util/Collection;

    .line 409806
    move-object v0, v0

    .line 409807
    sget-object v1, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    .line 409808
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 409809
    move-object v0, v0

    .line 409810
    const/4 v1, 0x1

    .line 409811
    iput-boolean v1, v0, LX/2RR;->o:Z

    .line 409812
    move-object v0, v0

    .line 409813
    iput p2, v0, LX/2RR;->p:I

    .line 409814
    move-object v0, v0

    .line 409815
    return-object v0
.end method

.method public final b(Ljava/util/Collection;)LX/2RR;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/2RR;"
        }
    .end annotation

    .prologue
    .line 409800
    invoke-virtual {p0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/user/model/UserKey;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    .line 409801
    iput-object v1, v0, LX/2RR;->d:Ljava/util/Collection;

    .line 409802
    move-object v0, v0

    .line 409803
    return-object v0
.end method

.method public final c(Ljava/util/Collection;I)LX/2RR;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/2RU;",
            ">;I)",
            "LX/2RR;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 409787
    invoke-virtual {p0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    .line 409788
    iput-object p1, v0, LX/2RR;->b:Ljava/util/Collection;

    .line 409789
    move-object v0, v0

    .line 409790
    sget-object v1, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    .line 409791
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 409792
    move-object v0, v0

    .line 409793
    iput-boolean v2, v0, LX/2RR;->o:Z

    .line 409794
    move-object v0, v0

    .line 409795
    iput-boolean v2, v0, LX/2RR;->g:Z

    .line 409796
    move-object v0, v0

    .line 409797
    iput p2, v0, LX/2RR;->p:I

    .line 409798
    move-object v0, v0

    .line 409799
    return-object v0
.end method
