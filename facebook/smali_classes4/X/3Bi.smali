.class public LX/3Bi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Sj;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Sa;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1PJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Sj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Sa;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1PJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 528892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528893
    iput-object p2, p0, LX/3Bi;->b:LX/0Ot;

    .line 528894
    iput-object p1, p0, LX/3Bi;->a:LX/0Ot;

    .line 528895
    iput-object p3, p0, LX/3Bi;->c:LX/0Ot;

    .line 528896
    iput-object p4, p0, LX/3Bi;->d:LX/0Ot;

    .line 528897
    return-void
.end method

.method public static a(LX/0QB;)LX/3Bi;
    .locals 7

    .prologue
    .line 528898
    const-class v1, LX/3Bi;

    monitor-enter v1

    .line 528899
    :try_start_0
    sget-object v0, LX/3Bi;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 528900
    sput-object v2, LX/3Bi;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 528901
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528902
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 528903
    new-instance v3, LX/3Bi;

    const/16 v4, 0x327a

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1129

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x154

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x699

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, LX/3Bi;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 528904
    move-object v0, v3

    .line 528905
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 528906
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3Bi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528907
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 528908
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1vY;Lcom/facebook/analytics/logger/HoneyClientEvent;)Landroid/util/SparseArray;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1vY;",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            ")",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 528909
    invoke-static {p0}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPlace;)LX/1y5;

    move-result-object v1

    .line 528910
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v5

    .line 528911
    invoke-static {p2}, LX/1vZ;->a(LX/1vY;)Landroid/util/SparseArray;

    move-result-object v0

    .line 528912
    const/4 v3, 0x0

    const-string v4, "tap_story_attachment"

    move-object v2, p3

    .line 528913
    if-eqz v1, :cond_0

    .line 528914
    const p0, 0x7f0d006a

    invoke-virtual {v0, p0, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 528915
    const p0, 0x7f0d0068

    invoke-virtual {v0, p0, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 528916
    const p0, 0x7f0d0069

    invoke-virtual {v0, p0, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 528917
    const p0, 0x7f0d0070

    invoke-virtual {v0, p0, v4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 528918
    const p0, 0x7f0d0071

    invoke-virtual {v0, p0, v5}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 528919
    :cond_0
    return-object v0
.end method

.method private static a(LX/1Pm;Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/Boolean;)V
    .locals 2
    .param p3    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pm;",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 528920
    new-instance v1, LX/3Bj;

    invoke-direct {v1, p1}, LX/3Bj;-><init>(Lcom/facebook/graphql/model/GraphQLPlace;)V

    .line 528921
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 528922
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528923
    :goto_0
    return-void

    .line 528924
    :cond_0
    invoke-static {p2}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    invoke-interface {p0, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    .line 528925
    if-nez p3, :cond_1

    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 528926
    :goto_1
    invoke-interface {p0, v1, v0}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    goto :goto_0

    .line 528927
    :cond_1
    invoke-static {p3}, LX/03R;->valueOf(Ljava/lang/Boolean;)LX/03R;

    move-result-object v0

    goto :goto_1
.end method

.method public static final a(LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pm;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 528928
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 528929
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 528930
    :goto_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPlace;->aa()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    .line 528931
    :goto_1
    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-eq v0, v2, :cond_0

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v2

    if-ne v2, v1, :cond_0

    .line 528932
    const/4 v2, 0x0

    invoke-static {p0, p2, p1, v2}, LX/3Bi;->a(LX/1Pm;Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/Boolean;)V

    .line 528933
    :cond_0
    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v0, v2, :cond_3

    :goto_2
    return v1

    .line 528934
    :cond_1
    new-instance v0, LX/3Bj;

    invoke-direct {v0, p2}, LX/3Bj;-><init>(Lcom/facebook/graphql/model/GraphQLPlace;)V

    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v1

    invoke-interface {p0, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    goto :goto_0

    .line 528935
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 528936
    :cond_3
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v1

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;Landroid/content/Context;LX/1Pm;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            "Landroid/content/Context;",
            "LX/1Pm;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 528937
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPlace;->aa()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    if-nez v0, :cond_1

    .line 528938
    :cond_0
    :goto_0
    return v2

    .line 528939
    :cond_1
    invoke-static {p5, p2, p3}, LX/3Bi;->a(LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v3

    .line 528940
    if-eqz v3, :cond_2

    .line 528941
    iget-object v0, p0, LX/3Bi;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79m;

    invoke-virtual {v0}, LX/79m;->a()V

    .line 528942
    iget-object v0, p0, LX/3Bi;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Sj;

    const-string v4, "toggle_button"

    const-string v5, "native_story"

    invoke-virtual {v0, p2, v4, v5}, LX/1Sj;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 528943
    :goto_1
    iget-object v0, p0, LX/3Bi;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1PJ;

    invoke-virtual {v0}, LX/1PJ;->a()V

    .line 528944
    if-nez v3, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p5, p3, p2, v0}, LX/3Bi;->a(LX/1Pm;Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/Boolean;)V

    .line 528945
    if-nez v3, :cond_0

    move v2, v1

    goto :goto_0

    .line 528946
    :cond_2
    iget-object v0, p0, LX/3Bi;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Sa;

    invoke-virtual {v0, p4}, LX/1Sa;->a(Landroid/content/Context;)V

    .line 528947
    iget-object v0, p0, LX/3Bi;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Sj;

    const-string v4, "toggle_button"

    const-string v5, "native_story"

    invoke-virtual {v0, p2, v4, v5}, LX/1Sj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 528948
    iget-object v0, p0, LX/3Bi;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79m;

    invoke-virtual {v0, p1}, LX/79m;->a(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    move v0, v2

    .line 528949
    goto :goto_2
.end method
