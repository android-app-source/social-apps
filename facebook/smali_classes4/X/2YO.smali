.class public abstract LX/2YO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RESPONSE:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/2YR;",
        "TRESPONSE;>;"
    }
.end annotation


# instance fields
.field private final a:LX/14S;

.field private b:Z


# direct methods
.method public constructor <init>(LX/14S;)V
    .locals 1

    .prologue
    .line 420873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420874
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2YO;->b:Z

    .line 420875
    iput-object p1, p0, LX/2YO;->a:LX/14S;

    .line 420876
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 420877
    check-cast p1, LX/2YR;

    .line 420878
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 420879
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "message"

    iget-object v2, p1, LX/2YR;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420880
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "compressed"

    const-string v2, "0"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420881
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420882
    iget-boolean v0, p1, LX/2YR;->b:Z

    if-eqz v0, :cond_0

    .line 420883
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "multi_batch"

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420884
    :cond_0
    new-instance v0, LX/14N;

    const-string v1, "sendAnalyticsLog"

    const-string v2, "POST"

    const-string v3, "logging_client_events"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    iget-object v6, p0, LX/2YO;->a:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method
