.class public final LX/2YZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/2ZP;


# direct methods
.method public constructor <init>(LX/2ZP;)V
    .locals 0

    .prologue
    .line 421067
    iput-object p1, p0, LX/2YZ;->a:LX/2ZP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421068
    iget-object v0, p0, LX/2YZ;->a:LX/2ZP;

    iget-object v0, v0, LX/2ZP;->a:LX/2ZQ;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetchFacewebUriMap"

    .line 421069
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 421070
    move-object v0, v0

    .line 421071
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 421072
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 421061
    const-string v0, "fetchFacewebUriMap"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 421062
    sget-boolean v1, LX/007;->j:Z

    move v1, v1

    .line 421063
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 421064
    const-string v1, "fb:\\/\\/"

    const-string v2, "fb-work:\\/\\/"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 421065
    :goto_0
    iget-object v0, p0, LX/2YZ;->a:LX/2ZP;

    iget-object v0, v0, LX/2ZP;->c:Landroid/content/Context;

    invoke-static {v0}, LX/2ZY;->b(Landroid/content/Context;)LX/2Zg;

    move-result-object v0

    iget-object v1, p0, LX/2YZ;->a:LX/2ZP;

    iget-object v1, v1, LX/2ZP;->c:Landroid/content/Context;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v5, p0, LX/2YZ;->a:LX/2ZP;

    iget-object v5, v5, LX/2ZP;->b:LX/2ZX;

    invoke-virtual {v5, v4}, LX/2ZX;->a(Ljava/lang/String;)LX/0cG;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/2Zg;->a(Landroid/content/Context;ZLjava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 421066
    return-void

    :cond_0
    move-object v4, v0

    goto :goto_0
.end method
