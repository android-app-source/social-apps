.class public final LX/3G1;
.super LX/3G2;
.source ""


# instance fields
.field public g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "LX/0zP;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0w7;

.field public i:LX/0jT;

.field public j:LX/399;

.field public k:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 540352
    invoke-direct {p0}, LX/3G2;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Rf;)LX/3G1;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/3G1;"
        }
    .end annotation

    .prologue
    .line 540350
    iput-object p1, p0, LX/3G1;->k:LX/0Rf;

    .line 540351
    return-object p0
.end method

.method public final a(LX/0jT;)LX/3G1;
    .locals 1

    .prologue
    .line 540346
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 540347
    iput-object p1, p0, LX/3G1;->i:LX/0jT;

    .line 540348
    return-object p0

    .line 540349
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0w7;)LX/3G1;
    .locals 0

    .prologue
    .line 540353
    iput-object p1, p0, LX/3G1;->h:LX/0w7;

    .line 540354
    return-object p0
.end method

.method public final a(LX/399;)LX/3G1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/399",
            "<*>;)",
            "LX/3G1;"
        }
    .end annotation

    .prologue
    .line 540335
    iput-object p1, p0, LX/3G1;->j:LX/399;

    .line 540336
    iget-object v0, p1, LX/399;->a:LX/0zP;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 540337
    iput-object v0, p0, LX/3G1;->g:Ljava/lang/Class;

    .line 540338
    iget-object v0, p1, LX/399;->a:LX/0zP;

    .line 540339
    iget-object v1, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v1

    .line 540340
    iput-object v0, p0, LX/3G1;->h:LX/0w7;

    .line 540341
    iget-object v0, p1, LX/399;->c:LX/0jT;

    move-object v0, v0

    .line 540342
    invoke-virtual {p0, v0}, LX/3G1;->a(LX/0jT;)LX/3G1;

    .line 540343
    iget-object v0, p1, LX/399;->b:LX/0Rf;

    move-object v0, v0

    .line 540344
    iput-object v0, p0, LX/3G1;->k:LX/0Rf;

    .line 540345
    return-object p0
.end method

.method public final a(Ljava/lang/Class;)LX/3G1;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/0zP;",
            ">;)",
            "LX/3G1;"
        }
    .end annotation

    .prologue
    .line 540333
    iput-object p1, p0, LX/3G1;->g:Ljava/lang/Class;

    .line 540334
    return-object p0
.end method

.method public final synthetic a()LX/3G3;
    .locals 1

    .prologue
    .line 540332
    invoke-virtual {p0}, LX/3G1;->b()LX/3G4;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/3G4;
    .locals 18

    .prologue
    .line 540329
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3G1;->g:Ljava/lang/Class;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 540330
    new-instance v3, LX/3G4;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/3G2;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/3G2;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/3G2;->d:J

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/3G2;->c:J

    move-object/from16 v0, p0

    iget v10, v0, LX/3G2;->e:I

    move-object/from16 v0, p0

    iget v11, v0, LX/3G2;->f:I

    move-object/from16 v0, p0

    iget-object v12, v0, LX/3G1;->g:Ljava/lang/Class;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/3G1;->h:LX/0w7;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/3G1;->i:LX/0jT;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/3G1;->k:LX/0Rf;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3G1;->j:LX/399;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-direct/range {v3 .. v17}, LX/3G4;-><init>(Ljava/lang/String;Ljava/lang/String;JJIILjava/lang/Class;LX/0w7;LX/0jT;LX/0Rf;LX/399;B)V

    return-object v3

    .line 540331
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
