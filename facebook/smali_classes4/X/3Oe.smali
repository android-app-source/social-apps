.class public final LX/3Oe;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/3Mm;


# direct methods
.method public constructor <init>(LX/3Mm;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 560643
    iput-object p1, p0, LX/3Oe;->a:LX/3Mm;

    .line 560644
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 560645
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 560646
    iget v2, p1, Landroid/os/Message;->what:I

    .line 560647
    sparse-switch v2, :sswitch_data_0

    .line 560648
    :goto_0
    return-void

    .line 560649
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/3Of;

    .line 560650
    :try_start_0
    iget-object v1, p0, LX/3Oe;->a:LX/3Mm;

    iget-object v3, v0, LX/3Of;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, LX/3Mm;->b(Ljava/lang/CharSequence;)LX/39y;

    move-result-object v1

    iput-object v1, v0, LX/3Of;->c:LX/39y;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 560651
    iget-object v1, p0, LX/3Oe;->a:LX/3Mm;

    iget-object v1, v1, LX/3Mm;->c:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 560652
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 560653
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 560654
    :goto_1
    iget-object v0, p0, LX/3Oe;->a:LX/3Mm;

    iget-object v1, v0, LX/3Mm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 560655
    :try_start_1
    iget-object v0, p0, LX/3Oe;->a:LX/3Mm;

    iget-object v0, v0, LX/3Mm;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 560656
    iget-object v0, p0, LX/3Oe;->a:LX/3Mm;

    iget-object v0, v0, LX/3Mm;->b:Landroid/os/Handler;

    const v2, -0x21524111

    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 560657
    iget-object v2, p0, LX/3Oe;->a:LX/3Mm;

    iget-object v2, v2, LX/3Mm;->b:Landroid/os/Handler;

    const-wide/16 v4, 0xbb8

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 560658
    :cond_0
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 560659
    :catch_0
    move-exception v1

    .line 560660
    :try_start_2
    new-instance v3, LX/39y;

    invoke-direct {v3}, LX/39y;-><init>()V

    iput-object v3, v0, LX/3Of;->c:LX/39y;

    .line 560661
    const-string v3, "AbstractCustomFilter"

    const-string v4, "An exception occurred during performFiltering()!"

    invoke-static {v3, v4, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 560662
    iget-object v1, p0, LX/3Oe;->a:LX/3Mm;

    iget-object v1, v1, LX/3Mm;->c:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 560663
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 560664
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    .line 560665
    :catchall_1
    move-exception v1

    iget-object v3, p0, LX/3Oe;->a:LX/3Mm;

    iget-object v3, v3, LX/3Mm;->c:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 560666
    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 560667
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    throw v1

    .line 560668
    :sswitch_1
    iget-object v0, p0, LX/3Oe;->a:LX/3Mm;

    iget-object v1, v0, LX/3Mm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 560669
    :try_start_3
    iget-object v0, p0, LX/3Oe;->a:LX/3Mm;

    iget-object v0, v0, LX/3Mm;->b:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 560670
    iget-object v0, p0, LX/3Oe;->a:LX/3Mm;

    iget-object v0, v0, LX/3Mm;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 560671
    iget-object v0, p0, LX/3Oe;->a:LX/3Mm;

    const/4 v2, 0x0

    .line 560672
    iput-object v2, v0, LX/3Mm;->b:Landroid/os/Handler;

    .line 560673
    :cond_1
    monitor-exit v1

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0

    :sswitch_data_0
    .sparse-switch
        -0x2f2f0ff3 -> :sswitch_0
        -0x21524111 -> :sswitch_1
    .end sparse-switch
.end method
