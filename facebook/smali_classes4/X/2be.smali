.class public LX/2be;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 439375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 439376
    const/4 v0, 0x0

    .line 439377
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_a

    .line 439378
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 439379
    :goto_0
    return v1

    .line 439380
    :cond_0
    const-string v11, "translation_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 439381
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    .line 439382
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 439383
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 439384
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 439385
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 439386
    const-string v11, "source_dialect"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 439387
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 439388
    :cond_2
    const-string v11, "source_dialect_name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 439389
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 439390
    :cond_3
    const-string v11, "target_dialect"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 439391
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 439392
    :cond_4
    const-string v11, "target_dialect_name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 439393
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 439394
    :cond_5
    const-string v11, "translation"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 439395
    invoke-static {p0, p1}, LX/4U2;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 439396
    :cond_6
    const-string v11, "auto_translated_message"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 439397
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 439398
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 439399
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 439400
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 439401
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 439402
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 439403
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 439404
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 439405
    if-eqz v0, :cond_9

    .line 439406
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 439407
    :cond_9
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 439408
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v3, v1

    move-object v4, v0

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v0, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 439409
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 439410
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 439411
    if-eqz v0, :cond_0

    .line 439412
    const-string v1, "source_dialect"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439413
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 439414
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 439415
    if-eqz v0, :cond_1

    .line 439416
    const-string v1, "source_dialect_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439417
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 439418
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 439419
    if-eqz v0, :cond_2

    .line 439420
    const-string v1, "target_dialect"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439421
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 439422
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 439423
    if-eqz v0, :cond_3

    .line 439424
    const-string v1, "target_dialect_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439425
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 439426
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 439427
    if-eqz v0, :cond_4

    .line 439428
    const-string v1, "translation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439429
    invoke-static {p0, v0, p2, p3}, LX/4U2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 439430
    :cond_4
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 439431
    if-eqz v0, :cond_5

    .line 439432
    const-string v0, "translation_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439433
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 439434
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 439435
    if-eqz v0, :cond_6

    .line 439436
    const-string v1, "auto_translated_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439437
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 439438
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 439439
    return-void
.end method
