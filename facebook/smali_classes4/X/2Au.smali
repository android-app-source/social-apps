.class public abstract LX/2Au;
.super LX/2An;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _paramAnnotations:[LX/0lP;


# direct methods
.method public constructor <init>(LX/0lP;[LX/0lP;)V
    .locals 0

    .prologue
    .line 378388
    invoke-direct {p0, p1}, LX/2An;-><init>(LX/0lP;)V

    .line 378389
    iput-object p2, p0, LX/2Au;->_paramAnnotations:[LX/0lP;

    .line 378390
    return-void
.end method

.method private a(I)LX/0lP;
    .locals 1

    .prologue
    .line 378384
    iget-object v0, p0, LX/2Au;->_paramAnnotations:[LX/0lP;

    if-eqz v0, :cond_0

    .line 378385
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/2Au;->_paramAnnotations:[LX/0lP;

    array-length v0, v0

    if-gt p1, v0, :cond_0

    .line 378386
    iget-object v0, p0, LX/2Au;->_paramAnnotations:[LX/0lP;

    aget-object v0, v0, p1

    .line 378387
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1Y3;[Ljava/lang/reflect/TypeVariable;)LX/0lJ;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Y3;",
            "[",
            "Ljava/lang/reflect/TypeVariable",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 378373
    if-eqz p2, :cond_1

    array-length v0, p2

    if-lez v0, :cond_1

    .line 378374
    invoke-virtual {p1}, LX/1Y3;->a()LX/1Y3;

    move-result-object p1

    .line 378375
    array-length v3, p2

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, p2, v1

    .line 378376
    invoke-interface {v4}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v0

    .line 378377
    invoke-virtual {p1, v0}, LX/1Y3;->b(Ljava/lang/String;)V

    .line 378378
    invoke-interface {v4}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v2

    .line 378379
    if-nez v0, :cond_0

    invoke-static {}, LX/0li;->c()LX/0lJ;

    move-result-object v0

    .line 378380
    :goto_1
    invoke-interface {v4}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, v0}, LX/1Y3;->a(Ljava/lang/String;LX/0lJ;)V

    .line 378381
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 378382
    :cond_0
    invoke-virtual {p1, v0}, LX/1Y3;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    goto :goto_1

    .line 378383
    :cond_1
    invoke-virtual {p0}, LX/0lO;->c()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/1Y3;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILX/0lP;)LX/2Vd;
    .locals 1

    .prologue
    .line 378371
    iget-object v0, p0, LX/2Au;->_paramAnnotations:[LX/0lP;

    aput-object p2, v0, p1

    .line 378372
    invoke-virtual {p0, p1}, LX/2Au;->c(I)LX/2Vd;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract a([Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Ljava/lang/annotation/Annotation;",
            ">(",
            "Ljava/lang/Class",
            "<TA;>;)TA;"
        }
    .end annotation

    .prologue
    .line 378370
    iget-object v0, p0, LX/2An;->b:LX/0lP;

    invoke-virtual {v0, p1}, LX/0lP;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/annotation/Annotation;)V
    .locals 2

    .prologue
    .line 378364
    iget-object v0, p0, LX/2Au;->_paramAnnotations:[LX/0lP;

    aget-object v0, v0, p1

    .line 378365
    if-nez v0, :cond_0

    .line 378366
    new-instance v0, LX/0lP;

    invoke-direct {v0}, LX/0lP;-><init>()V

    .line 378367
    iget-object v1, p0, LX/2Au;->_paramAnnotations:[LX/0lP;

    aput-object v0, v1, p1

    .line 378368
    :cond_0
    invoke-static {v0, p2}, LX/0lP;->c(LX/0lP;Ljava/lang/annotation/Annotation;)V

    .line 378369
    return-void
.end method

.method public abstract b(I)Ljava/lang/reflect/Type;
.end method

.method public final c(I)LX/2Vd;
    .locals 3

    .prologue
    .line 378363
    new-instance v0, LX/2Vd;

    invoke-virtual {p0, p1}, LX/2Au;->b(I)Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-direct {p0, p1}, LX/2Au;->a(I)LX/0lP;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, LX/2Vd;-><init>(LX/2Au;Ljava/lang/reflect/Type;LX/0lP;I)V

    return-object v0
.end method

.method public abstract h()Ljava/lang/Object;
.end method
