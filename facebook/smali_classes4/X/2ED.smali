.class public LX/2ED;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Uh;

.field private final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public final d:Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;

.field private final e:LX/0Xl;

.field private final f:LX/0Uo;

.field public final g:LX/0ad;

.field private h:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public i:J

.field public j:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 385075
    const-class v0, LX/2ED;

    sput-object v0, LX/2ED;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Xl;Landroid/content/Context;LX/0Uh;Ljava/util/concurrent/ScheduledExecutorService;LX/0ad;Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;LX/0Uo;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/CpuSpinCheckerScheduledExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 385076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385077
    iput-object p3, p0, LX/2ED;->b:LX/0Uh;

    .line 385078
    iput-object p4, p0, LX/2ED;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 385079
    iput-object p6, p0, LX/2ED;->d:Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;

    .line 385080
    iput-object p1, p0, LX/2ED;->e:LX/0Xl;

    .line 385081
    iput-object p5, p0, LX/2ED;->g:LX/0ad;

    .line 385082
    iput-object p7, p0, LX/2ED;->f:LX/0Uo;

    .line 385083
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/2ED;J)V
    .locals 7

    .prologue
    .line 385084
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2ED;->h:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2ED;->h:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 385085
    :cond_0
    iget-object v0, p0, LX/2ED;->c:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/2ED;->d:Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;

    const-wide/16 v2, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide v4, p1

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2ED;->h:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385086
    monitor-exit p0

    return-void

    .line 385087
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 4

    .prologue
    .line 385088
    iget-object v0, p0, LX/2ED;->g:LX/0ad;

    sget-short v1, LX/2EK;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 385089
    if-eqz v0, :cond_0

    .line 385090
    iget-object v0, p0, LX/2ED;->g:LX/0ad;

    sget v1, LX/2EK;->d:I

    const/16 v2, 0x3c

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/2ED;->i:J

    .line 385091
    iget-object v0, p0, LX/2ED;->g:LX/0ad;

    sget v1, LX/2EK;->c:I

    const/16 v2, 0x384

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/2ED;->j:J

    .line 385092
    iget-object v0, p0, LX/2ED;->d:Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;

    invoke-virtual {v0}, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->a()V

    .line 385093
    iget-object v0, p0, LX/2ED;->f:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 385094
    iget-object v0, p0, LX/2ED;->d:Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;

    const/4 v1, 0x0

    .line 385095
    iput-boolean v1, v0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->r:Z

    .line 385096
    iget-wide v0, p0, LX/2ED;->j:J

    invoke-static {p0, v0, v1}, LX/2ED;->a$redex0(LX/2ED;J)V

    .line 385097
    :goto_0
    iget-object v0, p0, LX/2ED;->e:LX/0Xl;

    .line 385098
    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v3, LX/Ee1;

    invoke-direct {v3, p0}, LX/Ee1;-><init>(LX/2ED;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v3, LX/Ee0;

    invoke-direct {v3, p0}, LX/Ee0;-><init>(LX/2ED;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 385099
    :cond_0
    return-void

    .line 385100
    :cond_1
    iget-object v0, p0, LX/2ED;->d:Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;

    const/4 v1, 0x1

    .line 385101
    iput-boolean v1, v0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->r:Z

    .line 385102
    iget-wide v0, p0, LX/2ED;->i:J

    invoke-static {p0, v0, v1}, LX/2ED;->a$redex0(LX/2ED;J)V

    goto :goto_0
.end method
