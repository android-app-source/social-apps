.class public LX/2DC;
.super LX/2DD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2DD",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Ljava/io/File;

.field private final e:LX/2D7;


# direct methods
.method public constructor <init>(IILX/0mm;LX/0Zh;Ljava/io/File;LX/2D7;)V
    .locals 0

    .prologue
    .line 383758
    invoke-direct {p0, p1, p2, p3, p4}, LX/2DD;-><init>(IILX/0mm;LX/0Zh;)V

    .line 383759
    iput-object p5, p0, LX/2DC;->d:Ljava/io/File;

    .line 383760
    iput-object p6, p0, LX/2DC;->e:LX/2D7;

    .line 383761
    return-void
.end method

.method private a(Ljava/io/File;LX/2DZ;)LX/2Da;
    .locals 3

    .prologue
    .line 383762
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 383763
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to create parent directories for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383764
    :cond_0
    new-instance v0, LX/2Da;

    invoke-direct {v0, p0, p1, p2}, LX/2Da;-><init>(LX/2DC;Ljava/io/File;LX/2DZ;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/2Db;
    .locals 4

    .prologue
    .line 383765
    iget-object v0, p0, LX/2DC;->d:Ljava/io/File;

    .line 383766
    new-instance v1, Ljava/io/File;

    .line 383767
    if-eqz p1, :cond_3

    :goto_0
    move-object v2, p1

    .line 383768
    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, v1

    .line 383769
    invoke-static {v0}, LX/2D9;->b(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 383770
    const/4 v1, 0x0

    .line 383771
    iget-object v2, p0, LX/2DC;->e:LX/2D7;

    invoke-virtual {v2, v0}, LX/2D7;->a(Ljava/lang/Object;)LX/2DZ;

    move-result-object v2

    .line 383772
    :try_start_0
    invoke-virtual {v2, p0}, LX/2DZ;->d(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 383773
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "Couldn\'t lock newly created file"

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383774
    :catchall_0
    move-exception v0

    if-nez v1, :cond_0

    .line 383775
    invoke-virtual {v2, p0}, LX/2DZ;->f(Ljava/lang/Object;)V

    .line 383776
    invoke-virtual {v2}, LX/2DZ;->a()V

    :cond_0
    throw v0

    .line 383777
    :cond_1
    :try_start_1
    invoke-direct {p0, v0, v2}, LX/2DC;->a(Ljava/io/File;LX/2DZ;)LX/2Da;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 383778
    :goto_1
    if-nez v0, :cond_2

    .line 383779
    invoke-virtual {v2, p0}, LX/2DZ;->f(Ljava/lang/Object;)V

    .line 383780
    invoke-virtual {v2}, LX/2DZ;->a()V

    .line 383781
    :cond_2
    return-object v0

    .line 383782
    :catch_0
    :try_start_2
    invoke-direct {p0, v0, v2}, LX/2DC;->a(Ljava/io/File;LX/2DZ;)LX/2Da;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_1

    :cond_3
    const-string p1, "null"

    goto :goto_0
.end method

.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 383783
    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    if-nez v0, :cond_0

    .line 383784
    const/4 v0, 0x0

    .line 383785
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    check-cast v0, LX/2Da;

    iget-object v0, v0, LX/2Da;->a:Ljava/io/File;

    goto :goto_0
.end method
