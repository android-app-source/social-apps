.class public final LX/2vZ;
.super LX/0gS;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 478325
    invoke-direct {p0}, LX/0gS;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2vb;)LX/2vZ;
    .locals 1

    .prologue
    .line 478328
    const-string v0, "base_station_coordinates"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 478329
    return-object p0
.end method

.method public final a(Ljava/lang/Integer;)LX/2vZ;
    .locals 1

    .prologue
    .line 478326
    const-string v0, "base_station_id"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 478327
    return-object p0
.end method

.method public final b(Ljava/lang/Integer;)LX/2vZ;
    .locals 1

    .prologue
    .line 478313
    const-string v0, "network_id"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 478314
    return-object p0
.end method

.method public final c(Ljava/lang/Integer;)LX/2vZ;
    .locals 1

    .prologue
    .line 478323
    const-string v0, "system_id"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 478324
    return-object p0
.end method

.method public final d(Ljava/lang/Integer;)LX/2vZ;
    .locals 1

    .prologue
    .line 478330
    const-string v0, "cdma_rssi_dbm"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 478331
    return-object p0
.end method

.method public final e(Ljava/lang/Integer;)LX/2vZ;
    .locals 1

    .prologue
    .line 478321
    const-string v0, "cdma_ecio_db10"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 478322
    return-object p0
.end method

.method public final f(Ljava/lang/Integer;)LX/2vZ;
    .locals 1

    .prologue
    .line 478319
    const-string v0, "evdo_rssi_dbm"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 478320
    return-object p0
.end method

.method public final g(Ljava/lang/Integer;)LX/2vZ;
    .locals 1

    .prologue
    .line 478317
    const-string v0, "evdo_ecio_db10"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 478318
    return-object p0
.end method

.method public final h(Ljava/lang/Integer;)LX/2vZ;
    .locals 1

    .prologue
    .line 478315
    const-string v0, "evdo_signal_to_noise"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 478316
    return-object p0
.end method
