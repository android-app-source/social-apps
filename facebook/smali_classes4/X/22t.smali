.class public LX/22t;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/22u;

.field private final b:LX/0pV;


# direct methods
.method public constructor <init>(LX/0pV;)V
    .locals 1

    .prologue
    .line 361346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361347
    sget-object v0, LX/22u;->NOT_LOADING:LX/22u;

    iput-object v0, p0, LX/22t;->a:LX/22u;

    .line 361348
    iput-object p1, p0, LX/22t;->b:LX/0pV;

    .line 361349
    return-void
.end method

.method private static a(LX/22t;LX/22u;)V
    .locals 4

    .prologue
    .line 361355
    iget-object v0, p0, LX/22t;->b:LX/0pV;

    const-string v1, "TailLoaderStatus"

    sget-object v2, LX/0rj;->STATUS_CHANGED:LX/0rj;

    invoke-virtual {p1}, LX/22u;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0pV;->a(Ljava/lang/String;LX/0rj;Ljava/lang/String;)V

    .line 361356
    iput-object p1, p0, LX/22t;->a:LX/22u;

    .line 361357
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 361358
    sget-object v0, LX/22u;->NOT_LOADING:LX/22u;

    invoke-static {p0, v0}, LX/22t;->a(LX/22t;LX/22u;)V

    .line 361359
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 361351
    iget-object v0, p0, LX/22t;->a:LX/22u;

    sget-object v1, LX/22u;->NOT_LOADING:LX/22u;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 361352
    sget-object v0, LX/22u;->LOADING:LX/22u;

    invoke-static {p0, v0}, LX/22t;->a(LX/22t;LX/22u;)V

    .line 361353
    return-void

    .line 361354
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 361350
    iget-object v0, p0, LX/22t;->a:LX/22u;

    sget-object v1, LX/22u;->LOADING:LX/22u;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
