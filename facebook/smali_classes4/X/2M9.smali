.class public LX/2M9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2MA;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile k:LX/2M9;


# instance fields
.field public final b:LX/1MZ;

.field public final c:LX/0kb;

.field private final d:LX/0Xl;

.field private final e:LX/2EL;

.field private final f:Ljava/util/concurrent/ScheduledExecutorService;

.field private g:Landroid/content/ContentResolver;

.field public h:LX/2MB;

.field private i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/2MB;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 396328
    const-class v0, LX/2M9;

    sput-object v0, LX/2M9;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1MZ;LX/0kb;LX/0Xl;LX/2EL;Ljava/util/concurrent/ScheduledExecutorService;Landroid/content/ContentResolver;)V
    .locals 1
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForLightweightTaskHandlerThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396319
    iput-object p1, p0, LX/2M9;->b:LX/1MZ;

    .line 396320
    iput-object p2, p0, LX/2M9;->c:LX/0kb;

    .line 396321
    iput-object p3, p0, LX/2M9;->d:LX/0Xl;

    .line 396322
    iput-object p4, p0, LX/2M9;->e:LX/2EL;

    .line 396323
    iput-object p5, p0, LX/2M9;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 396324
    iput-object p6, p0, LX/2M9;->g:Landroid/content/ContentResolver;

    .line 396325
    sget-object v0, LX/2MB;->CONNECTED:LX/2MB;

    iput-object v0, p0, LX/2M9;->h:LX/2MB;

    .line 396326
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2M9;->i:LX/0am;

    .line 396327
    return-void
.end method

.method public static a(LX/0QB;)LX/2M9;
    .locals 10

    .prologue
    .line 396305
    sget-object v0, LX/2M9;->k:LX/2M9;

    if-nez v0, :cond_1

    .line 396306
    const-class v1, LX/2M9;

    monitor-enter v1

    .line 396307
    :try_start_0
    sget-object v0, LX/2M9;->k:LX/2M9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 396308
    if-eqz v2, :cond_0

    .line 396309
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 396310
    new-instance v3, LX/2M9;

    invoke-static {v0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v4

    check-cast v4, LX/1MZ;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/2EL;->a(LX/0QB;)LX/2EL;

    move-result-object v7

    check-cast v7, LX/2EL;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v9

    check-cast v9, Landroid/content/ContentResolver;

    invoke-direct/range {v3 .. v9}, LX/2M9;-><init>(LX/1MZ;LX/0kb;LX/0Xl;LX/2EL;Ljava/util/concurrent/ScheduledExecutorService;Landroid/content/ContentResolver;)V

    .line 396311
    move-object v0, v3

    .line 396312
    sput-object v0, LX/2M9;->k:LX/2M9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396313
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 396314
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 396315
    :cond_1
    sget-object v0, LX/2M9;->k:LX/2M9;

    return-object v0

    .line 396316
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 396317
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static synthetic a(LX/2M9;LX/1Mb;)LX/2MB;
    .locals 1

    .prologue
    .line 396300
    sget-object v0, LX/2Z7;->a:[I

    invoke-virtual {p1}, LX/1Mb;->ordinal()I

    move-result p0

    aget v0, v0, p0

    packed-switch v0, :pswitch_data_0

    .line 396301
    sget-object v0, LX/2MB;->WAITING_TO_CONNECT:LX/2MB;

    :goto_0
    move-object v0, v0

    .line 396302
    return-object v0

    .line 396303
    :pswitch_0
    sget-object v0, LX/2MB;->CONNECTED:LX/2MB;

    goto :goto_0

    .line 396304
    :pswitch_1
    sget-object v0, LX/2MB;->CONNECTING:LX/2MB;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a$redex0(LX/2M9;LX/2MB;)V
    .locals 5

    .prologue
    .line 396279
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Handling potential change to: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 396280
    iget-object v0, p0, LX/2M9;->j:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 396281
    iget-object v0, p0, LX/2M9;->j:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 396282
    :cond_0
    iget-object v0, p0, LX/2M9;->h:LX/2MB;

    sget-object v1, LX/2MB;->CONNECTED:LX/2MB;

    if-ne v0, v1, :cond_1

    .line 396283
    new-instance v0, Lcom/facebook/messaging/connectivity/MqttBackedConnectionStatusMonitor$4;

    invoke-direct {v0, p0, p1}, Lcom/facebook/messaging/connectivity/MqttBackedConnectionStatusMonitor$4;-><init>(LX/2M9;LX/2MB;)V

    .line 396284
    iget-object v1, p0, LX/2M9;->f:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x1388

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v0, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2M9;->j:Ljava/util/concurrent/ScheduledFuture;

    .line 396285
    :goto_0
    return-void

    .line 396286
    :cond_1
    invoke-static {p0, p1}, LX/2M9;->b$redex0(LX/2M9;LX/2MB;)V

    goto :goto_0
.end method

.method public static b$redex0(LX/2M9;LX/2MB;)V
    .locals 3

    .prologue
    .line 396296
    iget-object v0, p0, LX/2M9;->h:LX/2MB;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2M9;->i:LX/0am;

    .line 396297
    iput-object p1, p0, LX/2M9;->h:LX/2MB;

    .line 396298
    iget-object v0, p0, LX/2M9;->d:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.orca.CONNECTIVITY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 396299
    return-void
.end method


# virtual methods
.method public final a()LX/2MB;
    .locals 2

    .prologue
    .line 396291
    iget-object v0, p0, LX/2M9;->h:LX/2MB;

    sget-object v1, LX/2MB;->CONNECTING:LX/2MB;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/2M9;->e:LX/2EL;

    .line 396292
    iget-object v1, v0, LX/2EL;->k:LX/2EQ;

    move-object v0, v1

    .line 396293
    sget-object v1, LX/2EQ;->CAPTIVE_PORTAL:LX/2EQ;

    if-ne v0, v1, :cond_0

    .line 396294
    sget-object v0, LX/2MB;->CONNECTED_CAPTIVE_PORTAL:LX/2MB;

    .line 396295
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2M9;->h:LX/2MB;

    goto :goto_0
.end method

.method public final a(LX/8Bk;)LX/2MB;
    .locals 1

    .prologue
    .line 396290
    invoke-virtual {p0}, LX/2M9;->a()LX/2MB;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 396289
    iget-object v0, p0, LX/2M9;->b:LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->f()Z

    move-result v0

    return v0
.end method

.method public final b(LX/8Bk;)Z
    .locals 1

    .prologue
    .line 396288
    invoke-virtual {p0}, LX/2M9;->c()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 396287
    iget-object v0, p0, LX/2M9;->h:LX/2MB;

    sget-object v1, LX/2MB;->CONNECTED:LX/2MB;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/8Bk;)Z
    .locals 1

    .prologue
    .line 396278
    invoke-virtual {p0}, LX/2M9;->d()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 396277
    iget-object v0, p0, LX/2M9;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2M9;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/2MB;->CONNECTED:LX/2MB;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 396272
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_2

    .line 396273
    iget-object v2, p0, LX/2M9;->g:Landroid/content/ContentResolver;

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    .line 396274
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 396275
    goto :goto_0

    .line 396276
    :cond_2
    iget-object v2, p0, LX/2M9;->g:Landroid/content/ContentResolver;

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 396266
    iget-object v0, p0, LX/2M9;->d:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v2, LX/2MD;

    invoke-direct {v2, p0}, LX/2MD;-><init>(LX/2M9;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    new-instance v2, LX/2ME;

    invoke-direct {v2, p0}, LX/2ME;-><init>(LX/2M9;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.netchecker.ACTION_NETCHECK_STATE_CHANGED"

    new-instance v2, LX/2MF;

    invoke-direct {v2, p0}, LX/2MF;-><init>(LX/2M9;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 396267
    iget-object v0, p0, LX/2M9;->c:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 396268
    sget-object v0, LX/2MB;->NO_INTERNET:LX/2MB;

    invoke-static {p0, v0}, LX/2M9;->b$redex0(LX/2M9;LX/2MB;)V

    .line 396269
    :cond_0
    :goto_0
    return-void

    .line 396270
    :cond_1
    iget-object v0, p0, LX/2M9;->b:LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->b()LX/1Mb;

    move-result-object v0

    sget-object v1, LX/1Mb;->DISCONNECTED:LX/1Mb;

    if-ne v0, v1, :cond_0

    .line 396271
    sget-object v0, LX/2MB;->WAITING_TO_CONNECT:LX/2MB;

    invoke-static {p0, v0}, LX/2M9;->b$redex0(LX/2M9;LX/2MB;)V

    goto :goto_0
.end method
