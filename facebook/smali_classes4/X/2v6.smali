.class public LX/2v6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile n:LX/2v6;


# instance fields
.field public final b:LX/0s6;

.field public final c:LX/03V;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:Landroid/content/ContentResolver;

.field private final f:Landroid/os/Handler;

.field private final g:LX/0lB;

.field private final h:Ljava/util/concurrent/ExecutorService;

.field private final i:Ljava/util/concurrent/ExecutorService;

.field private final j:LX/0Zb;

.field private k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/6Qc;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6Qd;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field

.field private m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 477402
    const-class v0, LX/2v6;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2v6;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0s6;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/ContentResolver;LX/0Zb;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;LX/0lB;)V
    .locals 1
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p7    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 477273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 477274
    iput-object p1, p0, LX/2v6;->b:LX/0s6;

    .line 477275
    iput-object p2, p0, LX/2v6;->c:LX/03V;

    .line 477276
    iput-object p3, p0, LX/2v6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 477277
    iput-object p4, p0, LX/2v6;->e:Landroid/content/ContentResolver;

    .line 477278
    iput-object p6, p0, LX/2v6;->h:Ljava/util/concurrent/ExecutorService;

    .line 477279
    iput-object p8, p0, LX/2v6;->i:Ljava/util/concurrent/ExecutorService;

    .line 477280
    iput-object p7, p0, LX/2v6;->f:Landroid/os/Handler;

    .line 477281
    iput-object p9, p0, LX/2v6;->g:LX/0lB;

    .line 477282
    iput-object p5, p0, LX/2v6;->j:LX/0Zb;

    .line 477283
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/2v6;->k:Ljava/util/Map;

    .line 477284
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/2v6;->l:Ljava/util/Set;

    .line 477285
    return-void
.end method

.method public static a(LX/0QB;)LX/2v6;
    .locals 13

    .prologue
    .line 477403
    sget-object v0, LX/2v6;->n:LX/2v6;

    if-nez v0, :cond_1

    .line 477404
    const-class v1, LX/2v6;

    monitor-enter v1

    .line 477405
    :try_start_0
    sget-object v0, LX/2v6;->n:LX/2v6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 477406
    if-eqz v2, :cond_0

    .line 477407
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 477408
    new-instance v3, LX/2v6;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v4

    check-cast v4, LX/0s6;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v7

    check-cast v7, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v10

    check-cast v10, Landroid/os/Handler;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v12

    check-cast v12, LX/0lB;

    invoke-direct/range {v3 .. v12}, LX/2v6;-><init>(LX/0s6;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/ContentResolver;LX/0Zb;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;LX/0lB;)V

    .line 477409
    move-object v0, v3

    .line 477410
    sput-object v0, LX/2v6;->n:LX/2v6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477411
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 477412
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 477413
    :cond_1
    sget-object v0, LX/2v6;->n:LX/2v6;

    return-object v0

    .line 477414
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 477415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2v6;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;)LX/6Qc;
    .locals 14

    .prologue
    .line 477416
    iget-object v13, p0, LX/2v6;->k:Ljava/util/Map;

    monitor-enter v13

    .line 477417
    :try_start_0
    invoke-static {p1}, LX/2v6;->d(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 477418
    new-instance v1, LX/6Qc;

    iget-object v3, p0, LX/2v6;->f:Landroid/os/Handler;

    const-wide/16 v10, 0x0

    move-object v2, p0

    move-object v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v12, p7

    invoke-direct/range {v1 .. v12}, LX/6Qc;-><init>(LX/2v6;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLX/162;)V

    .line 477419
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/6Qc;->a(Z)V

    .line 477420
    iget-object v2, p0, LX/2v6;->k:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477421
    monitor-exit v13

    return-object v1

    .line 477422
    :catchall_0
    move-exception v0

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(LX/2v6;J)LX/6Qf;
    .locals 15
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 477423
    const/4 v12, 0x0

    .line 477424
    const/4 v9, 0x0

    .line 477425
    invoke-static/range {p1 .. p2}, LX/6na;->a(J)Landroid/net/Uri;

    move-result-object v1

    .line 477426
    const-wide/16 v10, -0x1

    .line 477427
    const/4 v8, 0x0

    .line 477428
    const/4 v7, 0x0

    .line 477429
    const/4 v6, 0x0

    .line 477430
    :try_start_0
    iget-object v0, p0, LX/2v6;->e:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    move-object v0, v12

    .line 477431
    :goto_0
    if-nez v1, :cond_0

    .line 477432
    const-string v0, "Error: returned cursor is null"

    .line 477433
    :cond_0
    if-eqz v0, :cond_1

    .line 477434
    iget-object v1, p0, LX/2v6;->c:LX/03V;

    sget-object v2, LX/2v6;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 477435
    const/4 v1, 0x0

    .line 477436
    :goto_1
    return-object v1

    .line 477437
    :catch_0
    move-exception v0

    .line 477438
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Security Exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v9

    goto :goto_0

    .line 477439
    :cond_1
    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 477440
    const-string v2, "state"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 477441
    const-string v2, "should_track_changes"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 477442
    const-string v2, "is_cancelable"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 477443
    if-nez v0, :cond_6

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 477444
    :cond_2
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 477445
    invoke-interface {v1, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 477446
    invoke-interface {v1, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v5, 0x1

    .line 477447
    :goto_2
    invoke-interface {v1, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v6, 0x1

    .line 477448
    :goto_3
    cmp-long v0, v2, p1

    if-eqz v0, :cond_3

    .line 477449
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 477450
    :cond_3
    :goto_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 477451
    new-instance v1, LX/6Qf;

    invoke-direct/range {v1 .. v6}, LX/6Qf;-><init>(JIZZ)V

    goto :goto_1

    .line 477452
    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    .line 477453
    :cond_5
    const/4 v6, 0x0

    goto :goto_3

    .line 477454
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_6
    move v5, v7

    move v4, v8

    move-wide v2, v10

    goto :goto_4
.end method

.method private static a$redex0(LX/2v6;LX/6Qc;)V
    .locals 6

    .prologue
    .line 477455
    invoke-static {p0}, LX/2v6;->c(LX/2v6;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 477456
    iget-object v0, p0, LX/2v6;->c:LX/03V;

    sget-object v1, LX/2v6;->a:Ljava/lang/String;

    const-string v2, "failed to save content observer to cache"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 477457
    :goto_0
    return-void

    .line 477458
    :cond_0
    iget-object v0, p0, LX/2v6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 477459
    invoke-virtual {p1}, LX/6Qc;->d()Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;

    move-result-object v2

    .line 477460
    :try_start_0
    sget-object v0, LX/6Qx;->b:LX/0Tn;

    iget-object v3, v2, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->storyCacheId:Ljava/lang/String;

    invoke-static {v3}, LX/2v6;->d(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v3, p0, LX/2v6;->g:LX/0lB;

    invoke-virtual {v3, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 477461
    :goto_1
    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_0

    .line 477462
    :catch_0
    move-exception v0

    .line 477463
    iget-object v2, p0, LX/2v6;->c:LX/03V;

    sget-object v3, LX/2v6;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static b(LX/2v6;LX/6Qc;)V
    .locals 6

    .prologue
    .line 477464
    invoke-static {p0}, LX/2v6;->c(LX/2v6;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 477465
    iget-object v0, p0, LX/2v6;->c:LX/03V;

    sget-object v1, LX/2v6;->a:Ljava/lang/String;

    const-string v2, "failed to remove content observer from cache"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 477466
    :goto_0
    return-void

    .line 477467
    :cond_0
    iget-object v0, p0, LX/2v6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 477468
    invoke-virtual {p1}, LX/6Qc;->d()Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;

    move-result-object v0

    .line 477469
    sget-object v2, LX/6Qx;->b:LX/0Tn;

    iget-object v0, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->storyCacheId:Ljava/lang/String;

    invoke-static {v0}, LX/2v6;->d(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 477470
    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method private static c(LX/2v6;)Z
    .locals 3

    .prologue
    .line 477471
    :try_start_0
    iget-object v0, p0, LX/2v6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 477472
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 477473
    :catch_0
    move-exception v0

    .line 477474
    iget-object v1, p0, LX/2v6;->c:LX/03V;

    sget-object v2, LX/2v6;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 477475
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 477476
    sget-object v0, LX/51r;->a:LX/51l;

    move-object v0, v0

    .line 477477
    invoke-virtual {v0}, LX/51l;->a()LX/51h;

    move-result-object v1

    invoke-interface {v1, p0}, LX/51h;->a(Ljava/lang/CharSequence;)LX/51h;

    move-result-object v1

    invoke-interface {v1}, LX/51h;->a()LX/51o;

    move-result-object v1

    move-object v0, v1

    .line 477478
    invoke-virtual {v0}, LX/51o;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 477377
    invoke-static {p0}, LX/2v6;->c(LX/2v6;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 477378
    iget-object v0, p0, LX/2v6;->c:LX/03V;

    sget-object v1, LX/2v6;->a:Ljava/lang/String;

    const-string v2, "failed to load content observers from cache"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 477379
    :goto_0
    return-void

    .line 477380
    :cond_0
    iget-object v0, p0, LX/2v6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/6Qx;->b:LX/0Tn;

    invoke-interface {v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    .line 477381
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 477382
    :try_start_0
    iget-object v3, p0, LX/2v6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 477383
    if-eqz v0, :cond_1

    .line 477384
    :try_start_1
    iget-object v3, p0, LX/2v6;->g:LX/0lB;

    const-class v4, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;

    invoke-virtual {v3, v0, v4}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 477385
    :goto_2
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->storyCacheId:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 477386
    :try_start_2
    iget-object v3, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->storyCacheId:Ljava/lang/String;

    invoke-static {v3}, LX/2v6;->d(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 477387
    iget-object v3, p0, LX/2v6;->k:Ljava/util/Map;

    monitor-enter v3
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    .line 477388
    :try_start_3
    iget-object v6, p0, LX/2v6;->k:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_2

    .line 477389
    new-instance v6, LX/6Qc;

    iget-object v7, p0, LX/2v6;->f:Landroid/os/Handler;

    invoke-direct {v6, p0, v7, v0}, LX/6Qc;-><init>(LX/2v6;Landroid/os/Handler;Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;)V

    .line 477390
    iget-wide v8, v0, Lcom/facebook/directinstall/feed/DirectInstallProgressContentObserverData;->updateId:J

    invoke-static {v8, v9}, LX/6na;->a(J)Landroid/net/Uri;

    move-result-object v0

    .line 477391
    iget-object v7, p0, LX/2v6;->e:Landroid/content/ContentResolver;

    const/4 v8, 0x0

    invoke-virtual {v7, v0, v8, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 477392
    iget-object v0, p0, LX/2v6;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 477393
    iget-object v0, p0, LX/2v6;->k:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477394
    :cond_2
    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_0

    .line 477395
    :catch_0
    move-exception v0

    .line 477396
    iget-object v3, p0, LX/2v6;->c:LX/03V;

    sget-object v4, LX/2v6;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 477397
    :catch_1
    move-exception v0

    .line 477398
    iget-object v3, p0, LX/2v6;->c:LX/03V;

    sget-object v4, LX/2v6;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 477399
    :catch_2
    move-exception v0

    .line 477400
    iget-object v3, p0, LX/2v6;->c:LX/03V;

    sget-object v4, LX/2v6;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_2

    .line 477401
    :cond_3
    iget-object v0, p0, LX/2v6;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/47G;Ljava/util/Map;)LX/6Qc;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/47G;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "LX/6Qc;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 477479
    if-nez p2, :cond_0

    move-object v0, v1

    .line 477480
    :goto_0
    instance-of v2, v0, LX/162;

    if-eqz v2, :cond_1

    .line 477481
    check-cast v0, LX/162;

    move-object v7, v0

    .line 477482
    :goto_1
    iget-object v1, p1, LX/47G;->i:Ljava/lang/String;

    iget-object v2, p1, LX/47G;->e:Ljava/lang/String;

    iget-object v3, p1, LX/47G;->a:Ljava/lang/String;

    iget-object v4, p1, LX/47G;->o:Ljava/lang/String;

    iget-object v5, p1, LX/47G;->h:Ljava/lang/String;

    iget-object v6, p1, LX/47G;->l:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/2v6;->a(LX/2v6;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;)LX/6Qc;

    move-result-object v0

    .line 477483
    return-object v0

    .line 477484
    :cond_0
    const-string v0, "tracking"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 477485
    :cond_1
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 477486
    :try_start_0
    iget-object v2, p0, LX/2v6;->g:LX/0lB;

    check-cast v0, Ljava/lang/String;

    const-class v3, LX/162;

    invoke-virtual {v2, v0, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/162;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v7, v0

    .line 477487
    goto :goto_1

    :catch_0
    :cond_2
    move-object v7, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)LX/6Qc;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 477369
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477370
    const/4 v0, 0x0

    .line 477371
    :goto_0
    return-object v0

    .line 477372
    :cond_0
    invoke-static {p1}, LX/2v6;->d(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 477373
    iget-object v1, p0, LX/2v6;->k:Ljava/util/Map;

    monitor-enter v1

    .line 477374
    :try_start_0
    iget-object v2, p0, LX/2v6;->k:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Qc;

    .line 477375
    monitor-exit v1

    goto :goto_0

    .line 477376
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()V
    .locals 11

    .prologue
    .line 477358
    iget-boolean v0, p0, LX/2v6;->m:Z

    if-nez v0, :cond_0

    .line 477359
    invoke-direct {p0}, LX/2v6;->d()V

    .line 477360
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2v6;->m:Z

    .line 477361
    :cond_0
    iget-object v7, p0, LX/2v6;->k:Ljava/util/Map;

    monitor-enter v7

    .line 477362
    :try_start_0
    iget-object v0, p0, LX/2v6;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 477363
    iget-object v1, p0, LX/2v6;->k:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/6Qc;

    .line 477364
    iget-wide v9, v6, LX/6Qc;->h:J

    move-wide v2, v9

    .line 477365
    iget-object v0, v6, LX/6Qc;->b:Ljava/lang/String;

    move-object v4, v0

    .line 477366
    iget-object v0, v6, LX/6Qc;->c:Ljava/lang/String;

    move-object v5, v0

    .line 477367
    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, LX/2v6;->a(JLjava/lang/String;Ljava/lang/String;LX/6Qc;)V

    goto :goto_0

    .line 477368
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;LX/6Qc;)V
    .locals 9

    .prologue
    .line 477356
    iget-object v7, p0, LX/2v6;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$1;-><init>(LX/2v6;JLjava/lang/String;Ljava/lang/String;LX/6Qc;)V

    const v1, 0x211b4dab

    invoke-static {v7, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 477357
    return-void
.end method

.method public final a(LX/6Qc;J)V
    .locals 9

    .prologue
    .line 477344
    iget-object v7, p0, LX/2v6;->k:Ljava/util/Map;

    monitor-enter v7

    .line 477345
    :try_start_0
    invoke-virtual {p1}, LX/6Qc;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 477346
    invoke-static {p2, p3}, LX/6na;->a(J)Landroid/net/Uri;

    move-result-object v0

    .line 477347
    invoke-virtual {p1}, LX/6Qc;->g()Z

    move-result v8

    if-nez v8, :cond_1

    const/4 v8, 0x1

    :goto_0
    invoke-static {v8}, LX/0PB;->checkState(Z)V

    .line 477348
    iput-wide p2, p1, LX/6Qc;->h:J

    .line 477349
    iget-object v1, p0, LX/2v6;->e:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 477350
    invoke-static {p0, p1}, LX/2v6;->a$redex0(LX/2v6;LX/6Qc;)V

    .line 477351
    :cond_0
    iget-object v0, p1, LX/6Qc;->b:Ljava/lang/String;

    move-object v4, v0

    .line 477352
    iget-object v0, p1, LX/6Qc;->c:Ljava/lang/String;

    move-object v5, v0

    .line 477353
    move-object v1, p0

    move-wide v2, p2

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, LX/2v6;->a(JLjava/lang/String;Ljava/lang/String;LX/6Qc;)V

    .line 477354
    monitor-exit v7

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 477355
    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public final a(LX/6Qd;)V
    .locals 1

    .prologue
    .line 477342
    iget-object v0, p0, LX/2v6;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 477343
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6VZ;)V
    .locals 7
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 477331
    iget-object v6, p0, LX/2v6;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/facebook/directinstall/feed/progress/DirectInstallProgressDispatcher$2;-><init>(LX/2v6;Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;Ljava/lang/String;Ljava/lang/String;LX/6VZ;)V

    const v1, 0x78b46cf4

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 477332
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "neko_di_progress_update_event"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "story_cache_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "progress"

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "update_id"

    if-eqz p2, :cond_3

    :goto_0
    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 477333
    if-eqz p6, :cond_0

    .line 477334
    const-string v2, "tracking"

    iget-object v0, p6, LX/6VZ;->e:LX/162;

    if-eqz v0, :cond_4

    iget-object v0, p6, LX/6VZ;->e:LX/162;

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 477335
    :cond_0
    const-string v0, "package_name"

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string p4, ""

    :cond_1
    invoke-virtual {v1, v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 477336
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p5, ""

    .line 477337
    :cond_2
    iput-object p5, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 477338
    iget-object v0, p0, LX/2v6;->j:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 477339
    return-void

    .line 477340
    :cond_3
    const-string p2, ""

    goto :goto_0

    .line 477341
    :cond_4
    const-string v0, ""

    goto :goto_1
.end method

.method public final b(LX/6Qd;)V
    .locals 1

    .prologue
    .line 477329
    iget-object v0, p0, LX/2v6;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 477330
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 477321
    invoke-static {p1}, LX/2v6;->d(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 477322
    iget-object v2, p0, LX/2v6;->k:Ljava/util/Map;

    monitor-enter v2

    .line 477323
    :try_start_0
    iget-object v0, p0, LX/2v6;->k:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Qc;

    .line 477324
    if-eqz v0, :cond_0

    .line 477325
    iget-object v3, p0, LX/2v6;->k:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 477326
    invoke-virtual {v0}, LX/6Qc;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 477327
    iget-object v1, p0, LX/2v6;->e:Landroid/content/ContentResolver;

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 477328
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(Ljava/lang/String;)LX/6Qf;
    .locals 17
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 477286
    invoke-static {}, LX/6na;->a()Landroid/net/Uri;

    move-result-object v3

    .line 477287
    const/4 v9, 0x0

    .line 477288
    const/4 v8, 0x0

    .line 477289
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2v6;->e:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    move-object/from16 v16, v2

    move-object v2, v9

    move-object/from16 v9, v16

    .line 477290
    :goto_0
    if-nez v9, :cond_0

    .line 477291
    const-string v2, "Error: returned cursor is null"

    .line 477292
    :cond_0
    if-eqz v2, :cond_1

    .line 477293
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2v6;->c:LX/03V;

    sget-object v4, LX/2v6;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 477294
    const/4 v2, 0x0

    .line 477295
    :goto_1
    return-object v2

    .line 477296
    :catch_0
    move-exception v2

    .line 477297
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Security Exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/SecurityException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v9, v8

    goto :goto_0

    .line 477298
    :cond_1
    const-string v3, "_id"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 477299
    const-string v3, "package_name"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 477300
    const-string v3, "state"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 477301
    const-string v3, "should_track_changes"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 477302
    const-string v3, "is_cancelable"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 477303
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 477304
    if-nez v2, :cond_4

    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 477305
    :cond_2
    invoke-interface {v9, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 477306
    invoke-interface {v9, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 477307
    invoke-interface {v9, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 477308
    invoke-interface {v9, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_5

    const/4 v7, 0x1

    .line 477309
    :goto_2
    invoke-interface {v9, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_6

    const/4 v8, 0x1

    .line 477310
    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 477311
    new-instance v3, LX/6Qf;

    invoke-direct/range {v3 .. v8}, LX/6Qf;-><init>(JIZZ)V

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 477312
    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 477313
    :cond_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 477314
    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 477315
    const/4 v2, 0x0

    goto :goto_1

    .line 477316
    :cond_5
    const/4 v7, 0x0

    goto :goto_2

    .line 477317
    :cond_6
    const/4 v8, 0x0

    goto :goto_3

    .line 477318
    :catchall_0
    move-exception v2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v2

    .line 477319
    :cond_7
    new-instance v2, LX/6Qe;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/6Qe;-><init>(LX/2v6;)V

    invoke-static {v15, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 477320
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v15, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Qf;

    goto/16 :goto_1
.end method
