.class public LX/329;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0lS;

.field public final b:Z

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/32s;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4qP;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/32s;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/320;

.field public h:LX/4qD;

.field public i:LX/4q5;

.field public j:Z

.field public k:LX/2At;

.field public l:LX/2zN;


# direct methods
.method public constructor <init>(LX/0lS;LX/0mu;)V
    .locals 1

    .prologue
    .line 489508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 489509
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/329;->c:Ljava/util/Map;

    .line 489510
    iput-object p1, p0, LX/329;->a:LX/0lS;

    .line 489511
    sget-object v0, LX/0m6;->DEFAULT_VIEW_INCLUSION:LX/0m6;

    invoke-virtual {p2, v0}, LX/0m4;->a(LX/0m6;)Z

    move-result v0

    iput-boolean v0, p0, LX/329;->b:Z

    .line 489512
    return-void
.end method


# virtual methods
.method public final a(LX/0lJ;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Ljava/lang/String;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 489488
    iget-object v0, p0, LX/329;->k:LX/2At;

    if-nez v0, :cond_0

    .line 489489
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Builder class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/329;->a:LX/0lS;

    invoke-virtual {v2}, LX/0lS;->b()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not have build method \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "()\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 489490
    :cond_0
    iget-object v0, p0, LX/329;->k:LX/2At;

    invoke-virtual {v0}, LX/2At;->o()Ljava/lang/Class;

    move-result-object v0

    .line 489491
    iget-object v2, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v2

    .line 489492
    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 489493
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Build method \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/329;->k:LX/2At;

    invoke-virtual {v3}, LX/2At;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has bad return type ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "), not compatible with POJO type ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 489494
    iget-object v2, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v2

    .line 489495
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 489496
    :cond_1
    iget-object v0, p0, LX/329;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 489497
    new-instance v3, LX/32t;

    invoke-direct {v3, v0}, LX/32t;-><init>(Ljava/util/Collection;)V

    .line 489498
    invoke-virtual {v3}, LX/32t;->a()LX/32t;

    .line 489499
    iget-boolean v2, p0, LX/329;->b:Z

    if-nez v2, :cond_5

    move v7, v1

    .line 489500
    :goto_0
    if-nez v7, :cond_3

    .line 489501
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    .line 489502
    invoke-virtual {v0}, LX/32s;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    move v7, v1

    .line 489503
    :cond_3
    iget-object v0, p0, LX/329;->h:LX/4qD;

    if-eqz v0, :cond_4

    .line 489504
    new-instance v0, LX/4qE;

    iget-object v2, p0, LX/329;->h:LX/4qD;

    invoke-direct {v0, v2, v1}, LX/4qE;-><init>(LX/4qD;Z)V

    .line 489505
    invoke-virtual {v3, v0}, LX/32t;->a(LX/32s;)LX/32t;

    move-result-object v3

    .line 489506
    :cond_4
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/BuilderBasedDeserializer;

    iget-object v2, p0, LX/329;->a:LX/0lS;

    iget-object v4, p0, LX/329;->e:Ljava/util/HashMap;

    iget-object v5, p0, LX/329;->f:Ljava/util/HashSet;

    iget-boolean v6, p0, LX/329;->j:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/fasterxml/jackson/databind/deser/BuilderBasedDeserializer;-><init>(LX/329;LX/0lS;LX/32t;Ljava/util/Map;Ljava/util/HashSet;ZZ)V

    return-object v0

    .line 489507
    :cond_5
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public final a(LX/2At;LX/2zN;)V
    .locals 0

    .prologue
    .line 489485
    iput-object p1, p0, LX/329;->k:LX/2At;

    .line 489486
    iput-object p2, p0, LX/329;->l:LX/2zN;

    .line 489487
    return-void
.end method

.method public final a(LX/32s;)V
    .locals 2

    .prologue
    .line 489481
    iget-object v0, p0, LX/329;->c:Ljava/util/Map;

    .line 489482
    iget-object v1, p1, LX/32s;->_propName:Ljava/lang/String;

    move-object v1, v1

    .line 489483
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489484
    return-void
.end method

.method public final a(LX/4q5;)V
    .locals 2

    .prologue
    .line 489477
    iget-object v0, p0, LX/329;->i:LX/4q5;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 489478
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "_anySetter already set to non-null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 489479
    :cond_0
    iput-object p1, p0, LX/329;->i:LX/4q5;

    .line 489480
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 489513
    iget-object v0, p0, LX/329;->f:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 489514
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/329;->f:Ljava/util/HashSet;

    .line 489515
    :cond_0
    iget-object v0, p0, LX/329;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 489516
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0lJ;LX/0lQ;LX/2An;Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 489473
    iget-object v0, p0, LX/329;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 489474
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/329;->d:Ljava/util/List;

    .line 489475
    :cond_0
    iget-object v6, p0, LX/329;->d:Ljava/util/List;

    new-instance v0, LX/4qP;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/4qP;-><init>(Ljava/lang/String;LX/0lJ;LX/0lQ;LX/2An;Ljava/lang/Object;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 489476
    return-void
.end method

.method public final a(Ljava/lang/String;LX/32s;)V
    .locals 2

    .prologue
    .line 489465
    iget-object v0, p0, LX/329;->e:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 489466
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/329;->e:Ljava/util/HashMap;

    .line 489467
    :cond_0
    iget-object v0, p0, LX/329;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489468
    iget-object v0, p0, LX/329;->c:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 489469
    iget-object v0, p0, LX/329;->c:Ljava/util/Map;

    .line 489470
    iget-object v1, p2, LX/32s;->_propName:Ljava/lang/String;

    move-object v1, v1

    .line 489471
    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 489472
    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/32s;
    .locals 1

    .prologue
    .line 489464
    iget-object v0, p0, LX/329;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    return-object v0
.end method

.method public final b(LX/32s;)V
    .locals 3

    .prologue
    .line 489454
    iget-object v0, p0, LX/329;->c:Ljava/util/Map;

    .line 489455
    iget-object v1, p1, LX/32s;->_propName:Ljava/lang/String;

    move-object v1, v1

    .line 489456
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    .line 489457
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    .line 489458
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate property \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 489459
    iget-object v2, p1, LX/32s;->_propName:Ljava/lang/String;

    move-object v2, v2

    .line 489460
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/329;->a:LX/0lS;

    .line 489461
    iget-object p0, v2, LX/0lS;->a:LX/0lJ;

    move-object v2, p0

    .line 489462
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 489463
    :cond_0
    return-void
.end method

.method public final f()Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 489442
    iget-object v0, p0, LX/329;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 489443
    new-instance v3, LX/32t;

    invoke-direct {v3, v0}, LX/32t;-><init>(Ljava/util/Collection;)V

    .line 489444
    invoke-virtual {v3}, LX/32t;->a()LX/32t;

    .line 489445
    iget-boolean v2, p0, LX/329;->b:Z

    if-nez v2, :cond_3

    move v7, v1

    .line 489446
    :goto_0
    if-nez v7, :cond_1

    .line 489447
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    .line 489448
    invoke-virtual {v0}, LX/32s;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    move v7, v1

    .line 489449
    :cond_1
    iget-object v0, p0, LX/329;->h:LX/4qD;

    if-eqz v0, :cond_2

    .line 489450
    new-instance v0, LX/4qE;

    iget-object v2, p0, LX/329;->h:LX/4qD;

    invoke-direct {v0, v2, v1}, LX/4qE;-><init>(LX/4qD;Z)V

    .line 489451
    invoke-virtual {v3, v0}, LX/32t;->a(LX/32s;)LX/32t;

    move-result-object v3

    .line 489452
    :cond_2
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;

    iget-object v2, p0, LX/329;->a:LX/0lS;

    iget-object v4, p0, LX/329;->e:Ljava/util/HashMap;

    iget-object v5, p0, LX/329;->f:Ljava/util/HashSet;

    iget-boolean v6, p0, LX/329;->j:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/fasterxml/jackson/databind/deser/BeanDeserializer;-><init>(LX/329;LX/0lS;LX/32t;Ljava/util/Map;Ljava/util/HashSet;ZZ)V

    return-object v0

    .line 489453
    :cond_3
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;
    .locals 3

    .prologue
    .line 489441
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;

    iget-object v1, p0, LX/329;->a:LX/0lS;

    iget-object v2, p0, LX/329;->e:Ljava/util/HashMap;

    invoke-direct {v0, p0, v1, v2}, Lcom/fasterxml/jackson/databind/deser/AbstractDeserializer;-><init>(LX/329;LX/0lS;Ljava/util/Map;)V

    return-object v0
.end method
