.class public LX/2ZC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final subscribeGenericTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6mf;",
            ">;"
        }
    .end annotation
.end field

.field public final subscribeTopics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xf

    const/4 v3, 0x1

    .line 422165
    new-instance v0, LX/1sv;

    const-string v1, "SubscribeMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/2ZC;->b:LX/1sv;

    .line 422166
    new-instance v0, LX/1sw;

    const-string v1, "subscribeTopics"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/2ZC;->c:LX/1sw;

    .line 422167
    new-instance v0, LX/1sw;

    const-string v1, "subscribeGenericTopics"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/2ZC;->d:LX/1sw;

    .line 422168
    sput-boolean v3, LX/2ZC;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/6mf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 422227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422228
    iput-object p1, p0, LX/2ZC;->subscribeTopics:Ljava/util/List;

    .line 422229
    iput-object p2, p0, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    .line 422230
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 422195
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 422196
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 422197
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 422198
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "SubscribeMessage"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 422199
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422200
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422201
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422202
    const/4 v1, 0x1

    .line 422203
    iget-object v5, p0, LX/2ZC;->subscribeTopics:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 422204
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422205
    const-string v1, "subscribeTopics"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422206
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422207
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422208
    iget-object v1, p0, LX/2ZC;->subscribeTopics:Ljava/util/List;

    if-nez v1, :cond_6

    .line 422209
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422210
    :goto_3
    const/4 v1, 0x0

    .line 422211
    :cond_0
    iget-object v5, p0, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    if-eqz v5, :cond_2

    .line 422212
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422213
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422214
    const-string v1, "subscribeGenericTopics"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422215
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422216
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422217
    iget-object v0, p0, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    if-nez v0, :cond_7

    .line 422218
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422219
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422220
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422221
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 422222
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 422223
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 422224
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 422225
    :cond_6
    iget-object v1, p0, LX/2ZC;->subscribeTopics:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 422226
    :cond_7
    iget-object v0, p0, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 422231
    invoke-virtual {p1}, LX/1su;->a()V

    .line 422232
    iget-object v0, p0, LX/2ZC;->subscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 422233
    iget-object v0, p0, LX/2ZC;->subscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 422234
    sget-object v0, LX/2ZC;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 422235
    new-instance v0, LX/1u3;

    const/16 v1, 0x8

    iget-object v2, p0, LX/2ZC;->subscribeTopics:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 422236
    iget-object v0, p0, LX/2ZC;->subscribeTopics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 422237
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    goto :goto_0

    .line 422238
    :cond_0
    iget-object v0, p0, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 422239
    iget-object v0, p0, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 422240
    sget-object v0, LX/2ZC;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 422241
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 422242
    iget-object v0, p0, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6mf;

    .line 422243
    invoke-virtual {v0, p1}, LX/6mf;->a(LX/1su;)V

    goto :goto_1

    .line 422244
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 422245
    invoke-virtual {p1}, LX/1su;->b()V

    .line 422246
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 422173
    if-nez p1, :cond_1

    .line 422174
    :cond_0
    :goto_0
    return v0

    .line 422175
    :cond_1
    instance-of v1, p1, LX/2ZC;

    if-eqz v1, :cond_0

    .line 422176
    check-cast p1, LX/2ZC;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 422177
    if-nez p1, :cond_3

    .line 422178
    :cond_2
    :goto_1
    move v0, v2

    .line 422179
    goto :goto_0

    .line 422180
    :cond_3
    iget-object v0, p0, LX/2ZC;->subscribeTopics:Ljava/util/List;

    if-eqz v0, :cond_8

    move v0, v1

    .line 422181
    :goto_2
    iget-object v3, p1, LX/2ZC;->subscribeTopics:Ljava/util/List;

    if-eqz v3, :cond_9

    move v3, v1

    .line 422182
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 422183
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 422184
    iget-object v0, p0, LX/2ZC;->subscribeTopics:Ljava/util/List;

    iget-object v3, p1, LX/2ZC;->subscribeTopics:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 422185
    :cond_5
    iget-object v0, p0, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    if-eqz v0, :cond_a

    move v0, v1

    .line 422186
    :goto_4
    iget-object v3, p1, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    if-eqz v3, :cond_b

    move v3, v1

    .line 422187
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 422188
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 422189
    iget-object v0, p0, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    iget-object v3, p1, LX/2ZC;->subscribeGenericTopics:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 422190
    goto :goto_1

    :cond_8
    move v0, v2

    .line 422191
    goto :goto_2

    :cond_9
    move v3, v2

    .line 422192
    goto :goto_3

    :cond_a
    move v0, v2

    .line 422193
    goto :goto_4

    :cond_b
    move v3, v2

    .line 422194
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 422172
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 422169
    sget-boolean v0, LX/2ZC;->a:Z

    .line 422170
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/2ZC;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 422171
    return-object v0
.end method
