.class public LX/2Tz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 414922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)LX/2EJ;
    .locals 2

    .prologue
    .line 414923
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 414924
    invoke-virtual {v0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 414925
    if-eqz p2, :cond_0

    .line 414926
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/graphics/drawable/Drawable;)LX/0ju;

    .line 414927
    :cond_0
    instance-of v1, p3, Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 414928
    check-cast p3, Ljava/lang/String;

    invoke-virtual {v0, p3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 414929
    :cond_1
    :goto_0
    invoke-virtual {v0, p4, p5}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 414930
    invoke-virtual {v0, p6, p7}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 414931
    invoke-virtual {v0, p8}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    .line 414932
    invoke-virtual {v0, p9}, LX/0ju;->a(Z)LX/0ju;

    .line 414933
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    move-object v0, v0

    .line 414934
    return-object v0

    .line 414935
    :cond_2
    instance-of v1, p3, Landroid/view/View;

    if-eqz v1, :cond_1

    .line 414936
    check-cast p3, Landroid/view/View;

    invoke-virtual {v0, p3}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    goto :goto_0
.end method
