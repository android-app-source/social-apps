.class public LX/33W;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile o:LX/33W;


# instance fields
.field private final a:LX/33X;

.field public final b:LX/17W;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/16I;

.field public final f:LX/33Y;

.field public final g:LX/33Z;

.field public final h:LX/1Ml;

.field public final i:Lcom/facebook/content/SecureContextHelper;

.field public final j:LX/33a;

.field private final k:Ljava/util/concurrent/Executor;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/33X;LX/17W;LX/0Or;LX/0Ot;LX/16I;LX/33Y;LX/33Z;LX/1Ml;Lcom/facebook/content/SecureContextHelper;LX/33a;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .param p11    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/33X;",
            "LX/17W;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/16I;",
            "LX/33Y;",
            "LX/33Z;",
            "LX/1Ml;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/33a;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 493812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 493813
    iput-object p1, p0, LX/33W;->a:LX/33X;

    .line 493814
    iput-object p2, p0, LX/33W;->b:LX/17W;

    .line 493815
    iput-object p3, p0, LX/33W;->c:LX/0Or;

    .line 493816
    iput-object p4, p0, LX/33W;->d:LX/0Ot;

    .line 493817
    iput-object p5, p0, LX/33W;->e:LX/16I;

    .line 493818
    iput-object p6, p0, LX/33W;->f:LX/33Y;

    .line 493819
    iput-object p7, p0, LX/33W;->g:LX/33Z;

    .line 493820
    iput-object p8, p0, LX/33W;->h:LX/1Ml;

    .line 493821
    iput-object p9, p0, LX/33W;->i:Lcom/facebook/content/SecureContextHelper;

    .line 493822
    iput-object p10, p0, LX/33W;->j:LX/33a;

    .line 493823
    iput-object p11, p0, LX/33W;->k:Ljava/util/concurrent/Executor;

    .line 493824
    iput-object p12, p0, LX/33W;->l:LX/0Or;

    .line 493825
    iput-object p13, p0, LX/33W;->m:LX/0Ot;

    .line 493826
    iput-object p14, p0, LX/33W;->n:LX/0Ot;

    .line 493827
    return-void
.end method

.method public static a(LX/0QB;)LX/33W;
    .locals 3

    .prologue
    .line 493802
    sget-object v0, LX/33W;->o:LX/33W;

    if-nez v0, :cond_1

    .line 493803
    const-class v1, LX/33W;

    monitor-enter v1

    .line 493804
    :try_start_0
    sget-object v0, LX/33W;->o:LX/33W;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 493805
    if-eqz v2, :cond_0

    .line 493806
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/33W;->b(LX/0QB;)LX/33W;

    move-result-object v0

    sput-object v0, LX/33W;->o:LX/33W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 493807
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 493808
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 493809
    :cond_1
    sget-object v0, LX/33W;->o:LX/33W;

    return-object v0

    .line 493810
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 493811
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/33W;LX/2nq;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;LX/3D3;)V
    .locals 12
    .param p2    # Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 493787
    new-instance v0, LX/DqD;

    invoke-virtual {p2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/8D0;->INLINE_ACTION:LX/8D0;

    invoke-direct {v0, v1, v2}, LX/DqD;-><init>(Ljava/lang/String;LX/8D0;)V

    .line 493788
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 493789
    iput-object v1, v0, LX/DqD;->b:Ljava/lang/String;

    .line 493790
    invoke-static {p0, v0}, LX/33W;->a(LX/33W;LX/DqD;)V

    .line 493791
    if-eqz p3, :cond_1

    .line 493792
    new-instance v3, LX/DqC;

    iget-object v4, p3, LX/3D3;->a:Ljava/lang/String;

    iget-object v5, p3, LX/3D3;->b:Ljava/lang/String;

    invoke-direct {v3, v4, v5, p2}, LX/DqC;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;)V

    .line 493793
    const-string v4, "inline_notification_id"

    iget-object v5, p3, LX/3D3;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 493794
    iget-object v4, p3, LX/3D3;->c:LX/2jO;

    iget-object v4, v4, LX/2jO;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/0hM;->J:LX/0Tn;

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 493795
    :cond_0
    iget-object v4, p3, LX/3D3;->c:LX/2jO;

    invoke-virtual {v4, v3}, LX/2jO;->a(LX/DqC;)V

    .line 493796
    iget-object v3, p3, LX/3D3;->c:LX/2jO;

    iget-object v3, v3, LX/2jO;->g:Landroid/os/Handler;

    iget-object v4, p3, LX/3D3;->c:LX/2jO;

    iget-object v4, v4, LX/2jO;->f:Ljava/lang/Runnable;

    invoke-static {v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 493797
    iget-object v3, p3, LX/3D3;->c:LX/2jO;

    iget-object v3, v3, LX/2jO;->g:Landroid/os/Handler;

    iget-object v4, p3, LX/3D3;->c:LX/2jO;

    iget-object v4, v4, LX/2jO;->f:Ljava/lang/Runnable;

    iget-object v5, p3, LX/3D3;->c:LX/2jO;

    iget-object v5, v5, LX/2jO;->d:LX/1rx;

    .line 493798
    iget-object v8, v5, LX/1rx;->a:LX/0ad;

    sget v9, LX/15r;->l:I

    const/4 v10, 0x5

    invoke-interface {v8, v9, v10}, LX/0ad;->a(II)I

    move-result v8

    int-to-long v8, v8

    .line 493799
    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    move-wide v5, v8

    .line 493800
    const v7, -0x7ddc63c1

    invoke-static {v3, v4, v5, v6, v7}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 493801
    :cond_1
    return-void
.end method

.method private static a(LX/33W;LX/DqD;)V
    .locals 3

    .prologue
    .line 493762
    iget-object v0, p1, LX/DqD;->e:Ljava/lang/String;

    move-object v0, v0

    .line 493763
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493764
    :cond_0
    :goto_0
    return-void

    .line 493765
    :cond_1
    iget-object v0, p0, LX/33W;->e:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 493766
    iget-object v0, p1, LX/DqD;->a:LX/Dq2;

    move-object v0, v0

    .line 493767
    if-eqz v0, :cond_0

    .line 493768
    iget-object v0, p1, LX/DqD;->a:LX/Dq2;

    move-object v0, v0

    .line 493769
    invoke-interface {v0}, LX/Dq2;->a()V

    goto :goto_0

    .line 493770
    :cond_2
    new-instance v0, LX/4HO;

    invoke-direct {v0}, LX/4HO;-><init>()V

    .line 493771
    iget-object v1, p1, LX/DqD;->e:Ljava/lang/String;

    move-object v1, v1

    .line 493772
    invoke-virtual {v0, v1}, LX/4HO;->c(Ljava/lang/String;)LX/4HO;

    move-result-object v0

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4HO;->a(Ljava/lang/String;)LX/4HO;

    move-result-object v0

    .line 493773
    iget-object v1, p1, LX/DqD;->b:Ljava/lang/String;

    move-object v1, v1

    .line 493774
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 493775
    iget-object v1, p1, LX/DqD;->b:Ljava/lang/String;

    move-object v1, v1

    .line 493776
    const-string v2, "story_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 493777
    :cond_3
    invoke-static {}, LX/BC3;->a()LX/BC2;

    move-result-object v1

    .line 493778
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 493779
    iget-object v0, p0, LX/33W;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 493780
    new-instance v1, LX/Dq3;

    invoke-direct {v1, p0, p1}, LX/Dq3;-><init>(LX/33W;LX/DqD;)V

    iget-object v2, p0, LX/33W;->k:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 493781
    iget-object v0, p0, LX/33W;->j:LX/33a;

    .line 493782
    iget-object v1, p1, LX/DqD;->h:LX/8D0;

    move-object v1, v1

    .line 493783
    iget-object v2, p1, LX/DqD;->e:Ljava/lang/String;

    move-object v2, v2

    .line 493784
    const-string p0, "native_settings_changed"

    invoke-static {p0, v1}, LX/33a;->a(Ljava/lang/String;LX/8D0;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "server_action"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 493785
    iget-object p1, v0, LX/33a;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 493786
    goto :goto_0
.end method

.method public static a(LX/33W;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 493756
    iget-object v0, p0, LX/33W;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    const-string v1, "4436"

    const-class v2, LX/Cv8;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/Cv8;

    .line 493757
    if-eqz v0, :cond_0

    .line 493758
    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ITEM_SAVED_IN_NOTIFICATIONS_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 493759
    iget-object v0, p0, LX/33W;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15W;

    .line 493760
    const-class v2, LX/0i1;

    const/4 p0, 0x0

    invoke-virtual {v0, p1, v1, v2, p0}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 493761
    :cond_0
    return-void
.end method

.method private static a(LX/33W;Landroid/content/Context;LX/BCO;LX/8D0;LX/2nq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Dq2;)V
    .locals 3
    .param p3    # LX/8D0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/2nq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 493675
    if-eqz p2, :cond_0

    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    invoke-interface {v0}, LX/BCN;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v0

    if-nez v0, :cond_1

    .line 493676
    :cond_0
    :goto_0
    return-void

    .line 493677
    :cond_1
    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    invoke-interface {v0}, LX/BCN;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v0

    .line 493678
    sget-object v1, LX/Dq1;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 493679
    :pswitch_0
    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    instance-of v0, v0, LX/BCS;

    if-nez v0, :cond_5

    .line 493680
    :goto_1
    goto :goto_0

    .line 493681
    :pswitch_1
    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    instance-of v0, v0, LX/BCS;

    if-nez v0, :cond_a

    .line 493682
    :cond_2
    :goto_2
    goto :goto_0

    .line 493683
    :pswitch_2
    if-eqz p4, :cond_0

    invoke-interface {p2}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->l()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 493684
    new-instance v0, LX/DqD;

    invoke-interface {p2}, LX/BCO;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p3}, LX/DqD;-><init>(Ljava/lang/String;LX/8D0;)V

    .line 493685
    iput-object p4, v0, LX/DqD;->f:LX/2nq;

    .line 493686
    move-object v1, v0

    .line 493687
    invoke-interface {p2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    .line 493688
    iput-object v2, v1, LX/DqD;->g:Ljava/lang/String;

    .line 493689
    move-object v1, v1

    .line 493690
    iput-object p6, v1, LX/DqD;->i:Ljava/lang/String;

    .line 493691
    move-object v1, v1

    .line 493692
    invoke-interface {p2}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->l()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    .line 493693
    iput-object v2, v1, LX/DqD;->j:Ljava/lang/String;

    .line 493694
    move-object v1, v1

    .line 493695
    iput-object p5, v1, LX/DqD;->d:Ljava/lang/String;

    .line 493696
    move-object v1, v1

    .line 493697
    iput-object p8, v1, LX/DqD;->a:LX/Dq2;

    .line 493698
    invoke-static {p0, v0}, LX/33W;->a(LX/33W;LX/DqD;)V

    goto :goto_0

    .line 493699
    :pswitch_3
    new-instance v0, LX/DqD;

    invoke-interface {p2}, LX/BCO;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p3}, LX/DqD;-><init>(Ljava/lang/String;LX/8D0;)V

    .line 493700
    iput-object p7, v0, LX/DqD;->c:Ljava/lang/String;

    .line 493701
    move-object v0, v0

    .line 493702
    iput-object p8, v0, LX/DqD;->a:LX/Dq2;

    .line 493703
    move-object v0, v0

    .line 493704
    invoke-static {p0, v0}, LX/33W;->a(LX/33W;LX/DqD;)V

    goto :goto_0

    .line 493705
    :pswitch_4
    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    invoke-interface {v0}, LX/BCN;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 493706
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/33W;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 493707
    const-string v1, "event_id"

    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v2

    invoke-interface {v2}, LX/BCN;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 493708
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->EVENTS_NOTIFICATION_SETTINGS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 493709
    iget-object v1, p0, LX/33W;->i:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 493710
    iget-object v0, p0, LX/33W;->j:LX/33a;

    sget-object v1, LX/8D0;->ENTITY_ROW:LX/8D0;

    invoke-interface {p2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object p3

    invoke-interface {p3}, LX/BCN;->d()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, v1, v2, p3}, LX/33a;->a(LX/8D0;Ljava/lang/String;Ljava/lang/String;)V

    .line 493711
    :cond_3
    goto/16 :goto_0

    .line 493712
    :pswitch_5
    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    invoke-interface {v0}, LX/BCN;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 493713
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 493714
    const-string v1, "group_feed_id"

    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v2

    invoke-interface {v2}, LX/BCN;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 493715
    iget-object v1, p0, LX/33W;->b:LX/17W;

    sget-object v2, LX/0ax;->J:Ljava/lang/String;

    invoke-virtual {v1, p1, v2, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 493716
    iget-object v0, p0, LX/33W;->j:LX/33a;

    sget-object v1, LX/8D0;->ENTITY_ROW:LX/8D0;

    invoke-interface {p2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object p3

    invoke-interface {p3}, LX/BCN;->d()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, v1, v2, p3}, LX/33a;->a(LX/8D0;Ljava/lang/String;Ljava/lang/String;)V

    .line 493717
    :cond_4
    goto/16 :goto_0

    .line 493718
    :pswitch_6
    iget-object v0, p0, LX/33W;->b:LX/17W;

    sget-object v1, LX/0ax;->df:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 493719
    iget-object v0, p0, LX/33W;->j:LX/33a;

    sget-object v1, LX/8D0;->ENTITY_ROW:LX/8D0;

    invoke-interface {p2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/33a;->a(LX/8D0;Ljava/lang/String;)V

    .line 493720
    goto/16 :goto_0

    .line 493721
    :pswitch_7
    iget-object v0, p0, LX/33W;->b:LX/17W;

    sget-object v1, LX/0ax;->R:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 493722
    iget-object v0, p0, LX/33W;->j:LX/33a;

    sget-object v1, LX/8D0;->ENTITY_ROW:LX/8D0;

    invoke-interface {p2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/33a;->a(LX/8D0;Ljava/lang/String;)V

    .line 493723
    goto/16 :goto_0

    .line 493724
    :pswitch_8
    iget-object v0, p0, LX/33W;->b:LX/17W;

    sget-object v1, LX/0ax;->cl:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 493725
    iget-object v0, p0, LX/33W;->j:LX/33a;

    sget-object v1, LX/8D0;->ENTITY_ROW:LX/8D0;

    invoke-interface {p2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/33a;->a(LX/8D0;Ljava/lang/String;)V

    .line 493726
    goto/16 :goto_0

    .line 493727
    :pswitch_9
    iget-object v0, p0, LX/33W;->h:LX/1Ml;

    .line 493728
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_b

    .line 493729
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 493730
    const-string v2, "com.android.settings"

    const-string p0, "com.android.settings.Settings$AppNotificationSettingsActivity"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 493731
    const-string v2, "app_package"

    iget-object p0, v0, LX/1Ml;->a:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 493732
    const-string v2, "app_uid"

    iget-object p0, v0, LX/1Ml;->a:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    iget p0, p0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 493733
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 493734
    iget-object v2, v0, LX/1Ml;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object p0, v0, LX/1Ml;->a:Landroid/content/Context;

    invoke-interface {v2, v1, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 493735
    :goto_3
    goto/16 :goto_0

    .line 493736
    :cond_5
    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    check-cast v0, LX/BCS;

    .line 493737
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 493738
    invoke-interface {v0}, LX/BCS;->j()LX/0Px;

    move-result-object p3

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result p4

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, p4, :cond_7

    invoke-virtual {p3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BCR;

    .line 493739
    invoke-interface {v0}, LX/BCR;->b()Ljava/lang/String;

    move-result-object p5

    if-eqz p5, :cond_6

    .line 493740
    invoke-interface {v0}, LX/BCR;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493741
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 493742
    :cond_7
    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    invoke-interface {v0}, LX/BCN;->c()LX/174;

    move-result-object v0

    if-nez v0, :cond_9

    const/4 v0, 0x0

    .line 493743
    :goto_5
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 493744
    const-string p3, "extra_option_row_set_ids"

    invoke-virtual {v1, p3, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 493745
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_8

    .line 493746
    const-string p3, "fragment_title"

    invoke-virtual {v1, p3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 493747
    :cond_8
    const-string v0, "force_create_new_activity"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 493748
    iget-object v0, p0, LX/33W;->b:LX/17W;

    sget-object v2, LX/0ax;->de:Ljava/lang/String;

    invoke-virtual {v0, p1, v2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 493749
    iget-object v0, p0, LX/33W;->j:LX/33a;

    sget-object v1, LX/8D0;->ENTITY_ROW:LX/8D0;

    invoke-interface {p2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/33a;->a(LX/8D0;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 493750
    :cond_9
    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    invoke-interface {v0}, LX/BCN;->c()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 493751
    :cond_a
    invoke-interface {p2}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    check-cast v0, LX/BCS;

    .line 493752
    invoke-interface {v0}, LX/BCS;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 493753
    const/4 v1, 0x0

    invoke-interface {p2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p1, v0, v1, v2}, LX/33W;->a(LX/33W;Landroid/content/Context;LX/BCS;LX/2nq;Ljava/lang/String;)V

    .line 493754
    iget-object v0, p0, LX/33W;->j:LX/33a;

    sget-object v1, LX/8D0;->ENTITY_ROW:LX/8D0;

    invoke-interface {p2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/33a;->a(LX/8D0;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 493755
    :cond_b
    invoke-virtual {v0}, LX/1Ml;->d()V

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static a(LX/33W;Landroid/content/Context;LX/BCS;LX/2nq;Ljava/lang/String;)V
    .locals 7
    .param p2    # LX/BCS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/2nq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 493828
    new-instance v6, LX/3Af;

    invoke-direct {v6, p1}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 493829
    iget-object v0, p0, LX/33W;->a:LX/33X;

    invoke-interface {p2}, LX/BCS;->j()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BCR;

    new-instance v5, LX/Dq0;

    invoke-direct {v5, p0, v6}, LX/Dq0;-><init>(LX/33W;LX/3Af;)V

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/33X;->a(LX/BCR;Landroid/content/Context;LX/2nq;Ljava/lang/String;LX/Dq0;)LX/Dr4;

    move-result-object v0

    .line 493830
    invoke-virtual {v6, v0}, LX/3Af;->a(LX/1OM;)V

    .line 493831
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, LX/3Af;->a(I)V

    .line 493832
    invoke-virtual {v6}, LX/3Af;->show()V

    .line 493833
    return-void
.end method

.method private static b(LX/0QB;)LX/33W;
    .locals 15

    .prologue
    .line 493673
    new-instance v0, LX/33W;

    const-class v1, LX/33X;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/33X;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v2

    check-cast v2, LX/17W;

    const/16 v3, 0xc

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xafd

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v5

    check-cast v5, LX/16I;

    invoke-static {p0}, LX/33Y;->b(LX/0QB;)LX/33Y;

    move-result-object v6

    check-cast v6, LX/33Y;

    invoke-static {p0}, LX/33Z;->a(LX/0QB;)LX/33Z;

    move-result-object v7

    check-cast v7, LX/33Z;

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v8

    check-cast v8, LX/1Ml;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/33a;->b(LX/0QB;)LX/33a;

    move-result-object v10

    check-cast v10, LX/33a;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ExecutorService;

    const/16 v12, 0xbd2

    invoke-static {p0, v12}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0xbca

    invoke-static {p0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x327a

    invoke-static {p0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v0 .. v14}, LX/33W;-><init>(LX/33X;LX/17W;LX/0Or;LX/0Ot;LX/16I;LX/33Y;LX/33Z;LX/1Ml;Lcom/facebook/content/SecureContextHelper;LX/33a;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Ot;LX/0Ot;)V

    .line 493674
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/BCO;LX/8D0;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 493671
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    invoke-static/range {v0 .. v8}, LX/33W;->a(LX/33W;Landroid/content/Context;LX/BCO;LX/8D0;LX/2nq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Dq2;)V

    .line 493672
    return-void
.end method

.method public final a(Landroid/content/Context;LX/BCO;LX/8D0;LX/2nq;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 493669
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v8, v7

    invoke-static/range {v0 .. v8}, LX/33W;->a(LX/33W;Landroid/content/Context;LX/BCO;LX/8D0;LX/2nq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Dq2;)V

    .line 493670
    return-void
.end method

.method public final a(Landroid/content/Context;LX/BCO;LX/8D0;LX/Dq2;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 493667
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, p4

    invoke-static/range {v0 .. v8}, LX/33W;->a(LX/33W;Landroid/content/Context;LX/BCO;LX/8D0;LX/2nq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Dq2;)V

    .line 493668
    return-void
.end method

.method public final a(Landroid/content/Context;LX/BCO;LX/8D0;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 493665
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v6, v4

    move-object v7, p4

    move-object v8, v4

    invoke-static/range {v0 .. v8}, LX/33W;->a(LX/33W;Landroid/content/Context;LX/BCO;LX/8D0;LX/2nq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Dq2;)V

    .line 493666
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;I)V
    .locals 6

    .prologue
    .line 493659
    iget-object v0, p0, LX/33W;->f:LX/33Y;

    const-string v1, "inline_action_undo"

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/33Y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;I)V

    .line 493660
    new-instance v0, LX/DqD;

    sget-object v1, LX/8D0;->INLINE_ACTION:LX/8D0;

    invoke-direct {v0, p1, v1}, LX/DqD;-><init>(Ljava/lang/String;LX/8D0;)V

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 493661
    iput-object v1, v0, LX/DqD;->b:Ljava/lang/String;

    .line 493662
    move-object v0, v0

    .line 493663
    invoke-static {p0, v0}, LX/33W;->a(LX/33W;LX/DqD;)V

    .line 493664
    return-void
.end method
