.class public LX/29Z;
.super LX/16B;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile c:LX/29Z;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 376379
    const-class v0, LX/29Z;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/29Z;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 376380
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 376381
    iput-object p1, p0, LX/29Z;->b:LX/0Ot;

    .line 376382
    return-void
.end method

.method public static a(LX/0QB;)LX/29Z;
    .locals 4

    .prologue
    .line 376366
    sget-object v0, LX/29Z;->c:LX/29Z;

    if-nez v0, :cond_1

    .line 376367
    const-class v1, LX/29Z;

    monitor-enter v1

    .line 376368
    :try_start_0
    sget-object v0, LX/29Z;->c:LX/29Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 376369
    if-eqz v2, :cond_0

    .line 376370
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 376371
    new-instance v3, LX/29Z;

    const/16 p0, 0x37e9

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/29Z;-><init>(LX/0Ot;)V

    .line 376372
    move-object v0, v3

    .line 376373
    sput-object v0, LX/29Z;->c:LX/29Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376374
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 376375
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 376376
    :cond_1
    sget-object v0, LX/29Z;->c:LX/29Z;

    return-object v0

    .line 376377
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 376378
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final e()V
    .locals 4

    .prologue
    .line 376362
    :try_start_0
    iget-object v0, p0, LX/29Z;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BUA;

    sget-object v1, LX/7Jb;->USER_LOGGED_OUT:LX/7Jb;

    invoke-virtual {v0, v1}, LX/BUA;->a(LX/7Jb;)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376363
    :goto_0
    return-void

    .line 376364
    :catch_0
    move-exception v0

    .line 376365
    sget-object v1, LX/29Z;->a:Ljava/lang/String;

    const-string v2, "Exception removing offline videos on logout."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
