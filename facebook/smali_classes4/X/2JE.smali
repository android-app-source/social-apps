.class public LX/2JE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Up;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:LX/0tX;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field public final g:Ljava/util/concurrent/ExecutorService;

.field public h:Landroid/webkit/CookieManager;

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final j:LX/1Bf;

.field public final k:LX/0Uh;

.field private final l:LX/0WJ;

.field private m:LX/0lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 392687
    const-class v0, LX/2JE;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2JE;->a:Ljava/lang/String;

    .line 392688
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "ATLAS"

    const-string v2, "https://cx.atdmt.com"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "LIVERAIL"

    const-string v2, "https://sync.liverail.com"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/2JE;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0tX;LX/0Or;Lcom/facebook/http/common/FbHttpRequestProcessor;Ljava/util/concurrent/ExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Bf;LX/0Uh;LX/0WJ;LX/0lC;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/cookiesync/IsInAtlasCookieSyncGK;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/1Bf;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/auth/datastore/AuthDataStore;",
            "LX/0lC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392690
    iput-object p1, p0, LX/2JE;->c:Landroid/content/Context;

    .line 392691
    iput-object p2, p0, LX/2JE;->d:LX/0tX;

    .line 392692
    iput-object p3, p0, LX/2JE;->e:LX/0Or;

    .line 392693
    iput-object p4, p0, LX/2JE;->f:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 392694
    iput-object p5, p0, LX/2JE;->g:Ljava/util/concurrent/ExecutorService;

    .line 392695
    iput-object p6, p0, LX/2JE;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 392696
    iput-object p7, p0, LX/2JE;->j:LX/1Bf;

    .line 392697
    iput-object p8, p0, LX/2JE;->k:LX/0Uh;

    .line 392698
    iput-object p9, p0, LX/2JE;->l:LX/0WJ;

    .line 392699
    iput-object p10, p0, LX/2JE;->m:LX/0lC;

    .line 392700
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 6

    .prologue
    .line 392701
    iget-object v0, p0, LX/2JE;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 392702
    iget-object v0, p0, LX/2JE;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 392703
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    iput-object v0, p0, LX/2JE;->h:Landroid/webkit/CookieManager;

    .line 392704
    iget-object v0, p0, LX/2JE;->h:Landroid/webkit/CookieManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V

    .line 392705
    iget-object v0, p0, LX/2JE;->l:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 392706
    if-eqz v0, :cond_1

    .line 392707
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v0, v1

    .line 392708
    const-string v1, ""

    .line 392709
    if-eqz v0, :cond_3

    .line 392710
    iget-object v2, p0, LX/2JE;->m:LX/0lC;

    invoke-static {v2, v0}, Lcom/facebook/auth/credentials/SessionCookie;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v3

    .line 392711
    if-eqz v3, :cond_3

    .line 392712
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/SessionCookie;

    .line 392713
    invoke-virtual {v0}, Lcom/facebook/auth/credentials/SessionCookie;->toString()Ljava/lang/String;

    move-result-object v0

    .line 392714
    const-string v5, "fr="

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 392715
    const/4 v1, 0x3

    const/16 v2, 0x3b

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 392716
    :goto_1
    const/4 v1, 0x0

    .line 392717
    iget-object v2, p0, LX/2JE;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1C0;->a:LX/0Tn;

    invoke-interface {v2, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    .line 392718
    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v1, v1

    .line 392719
    if-eqz v1, :cond_1

    .line 392720
    iget-object v2, p0, LX/2JE;->d:LX/0tX;

    .line 392721
    new-instance v1, LX/Hwd;

    invoke-direct {v1}, LX/Hwd;-><init>()V

    move-object v1, v1

    .line 392722
    const-string v3, "frcookie_value"

    invoke-virtual {v1, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/Hwd;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 392723
    new-instance v2, Lcom/facebook/cookiesync/CookieSyncer$1;

    invoke-direct {v2, p0}, Lcom/facebook/cookiesync/CookieSyncer$1;-><init>(LX/2JE;)V

    iget-object v3, p0, LX/2JE;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 392724
    :cond_1
    return-void

    .line 392725
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method
