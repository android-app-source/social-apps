.class public final LX/3Mh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mi;


# instance fields
.field private final a:LX/3Mi;

.field public final b:LX/3NY;


# direct methods
.method public constructor <init>(LX/3Mi;LX/3NY;)V
    .locals 0

    .prologue
    .line 555172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555173
    iput-object p1, p0, LX/3Mh;->a:LX/3Mi;

    .line 555174
    iput-object p2, p0, LX/3Mh;->b:LX/3NY;

    .line 555175
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 555176
    iget-object v0, p0, LX/3Mh;->a:LX/3Mi;

    invoke-interface {v0}, LX/3Mi;->a()V

    .line 555177
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 555178
    iget-object v0, p0, LX/3Mh;->a:LX/3Mi;

    invoke-interface {v0, p1}, LX/3Mi;->a(LX/0Px;)V

    .line 555179
    return-void
.end method

.method public final a(LX/3LI;)V
    .locals 1

    .prologue
    .line 555180
    iget-object v0, p0, LX/3Mh;->a:LX/3Mi;

    invoke-interface {v0, p1}, LX/3Mi;->a(LX/3LI;)V

    .line 555181
    return-void
.end method

.method public final a(LX/3Md;)V
    .locals 1

    .prologue
    .line 555182
    iget-object v0, p0, LX/3Mh;->a:LX/3Mi;

    invoke-interface {v0, p1}, LX/3Mi;->a(LX/3Md;)V

    .line 555183
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 555184
    iget-object v0, p0, LX/3Mh;->a:LX/3Mi;

    iget-object v1, p0, LX/3Mh;->b:LX/3NY;

    invoke-interface {v0, p1, v1}, LX/333;->a(Ljava/lang/CharSequence;LX/3NY;)V

    .line 555185
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;LX/3NY;)V
    .locals 2

    .prologue
    .line 555186
    iget-object v0, p0, LX/3Mh;->a:LX/3Mi;

    new-instance v1, LX/3Oc;

    invoke-direct {v1, p0, p2}, LX/3Oc;-><init>(LX/3Mh;LX/3NY;)V

    invoke-interface {v0, p1, v1}, LX/333;->a(Ljava/lang/CharSequence;LX/3NY;)V

    .line 555187
    return-void
.end method

.method public final b()LX/3Mr;
    .locals 1

    .prologue
    .line 555188
    iget-object v0, p0, LX/3Mh;->a:LX/3Mi;

    invoke-interface {v0}, LX/333;->b()LX/3Mr;

    move-result-object v0

    return-object v0
.end method
