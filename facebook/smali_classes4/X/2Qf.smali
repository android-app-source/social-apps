.class public LX/2Qf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/2Qf;


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:LX/0So;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2EM;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/ScheduledExecutorService;

.field public e:LX/0YZ;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/concurrent/ScheduledFuture;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:Z

.field private h:J


# direct methods
.method public constructor <init>(LX/0So;Ljava/util/Set;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0So;",
            "Ljava/util/Set",
            "<",
            "LX/2EM;",
            ">;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 408810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408811
    const-class v0, LX/2Qf;

    iput-object v0, p0, LX/2Qf;->a:Ljava/lang/Class;

    .line 408812
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2Qf;->f:Ljava/util/Map;

    .line 408813
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Qf;->g:Z

    .line 408814
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/2Qf;->h:J

    .line 408815
    iput-object p1, p0, LX/2Qf;->b:LX/0So;

    .line 408816
    iput-object p2, p0, LX/2Qf;->c:Ljava/util/Set;

    .line 408817
    iput-object p3, p0, LX/2Qf;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 408818
    return-void
.end method

.method public static a(LX/0QB;)LX/2Qf;
    .locals 7

    .prologue
    .line 408795
    sget-object v0, LX/2Qf;->i:LX/2Qf;

    if-nez v0, :cond_1

    .line 408796
    const-class v1, LX/2Qf;

    monitor-enter v1

    .line 408797
    :try_start_0
    sget-object v0, LX/2Qf;->i:LX/2Qf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 408798
    if-eqz v2, :cond_0

    .line 408799
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 408800
    new-instance v5, LX/2Qf;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    .line 408801
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    new-instance p0, LX/2Qh;

    invoke-direct {p0, v0}, LX/2Qh;-><init>(LX/0QB;)V

    invoke-direct {v4, v6, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v4

    .line 408802
    invoke-static {v0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v5, v3, v6, v4}, LX/2Qf;-><init>(LX/0So;Ljava/util/Set;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 408803
    move-object v0, v5

    .line 408804
    sput-object v0, LX/2Qf;->i:LX/2Qf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408805
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 408806
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 408807
    :cond_1
    sget-object v0, LX/2Qf;->i:LX/2Qf;

    return-object v0

    .line 408808
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 408809
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 2

    .prologue
    .line 408787
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2Qf;->g:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 408788
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Qf;->g:Z

    .line 408789
    iget-object v0, p0, LX/2Qf;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/2Qf;->h:J

    .line 408790
    iget-object v0, p0, LX/2Qf;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2EM;

    .line 408791
    if-eqz v0, :cond_0

    .line 408792
    invoke-interface {v0}, LX/2EM;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 408793
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 408794
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private static declared-synchronized c(LX/2Qf;)V
    .locals 2

    .prologue
    .line 408780
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2Qf;->g:Z

    if-nez v0, :cond_1

    .line 408781
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2Qf;->g:Z

    .line 408782
    iget-object v0, p0, LX/2Qf;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2EM;

    .line 408783
    if-eqz v0, :cond_0

    .line 408784
    invoke-interface {v0}, LX/2EM;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 408785
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 408786
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 5

    .prologue
    .line 408772
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Qf;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledFuture;

    .line 408773
    if-eqz v0, :cond_0

    .line 408774
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 408775
    :cond_0
    invoke-static {p0}, LX/2Qf;->c(LX/2Qf;)V

    .line 408776
    iget-object v0, p0, LX/2Qf;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/powermanagement/RadioPowerManager$TimeoutPidRunnable;

    invoke-direct {v1, p0, p1}, Lcom/facebook/powermanagement/RadioPowerManager$TimeoutPidRunnable;-><init>(LX/2Qf;I)V

    const-wide/32 v2, 0x1d4c0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    .line 408777
    iget-object v1, p0, LX/2Qf;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408778
    monitor-exit p0

    return-void

    .line 408779
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)V
    .locals 2

    .prologue
    .line 408765
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Qf;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledFuture;

    .line 408766
    if-eqz v0, :cond_0

    .line 408767
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 408768
    iget-object v0, p0, LX/2Qf;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408769
    invoke-direct {p0}, LX/2Qf;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408770
    :cond_0
    monitor-exit p0

    return-void

    .line 408771
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
