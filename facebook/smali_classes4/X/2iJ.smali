.class public final enum LX/2iJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2iJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2iJ;

.field public static final enum BIG:LX/2iJ;

.field public static final enum SMALL:LX/2iJ;


# instance fields
.field private attrEnumValue:I

.field public fillSizeDimen:I

.field public shadowDrawableResource:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 451217
    new-instance v0, LX/2iJ;

    const-string v1, "BIG"

    const v4, 0x7f0206fe

    const v5, 0x7f0b01cf

    move v3, v2

    invoke-direct/range {v0 .. v5}, LX/2iJ;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, LX/2iJ;->BIG:LX/2iJ;

    .line 451218
    new-instance v3, LX/2iJ;

    const-string v4, "SMALL"

    const v7, 0x7f0206ff

    const v8, 0x7f0b01ce

    move v5, v9

    move v6, v9

    invoke-direct/range {v3 .. v8}, LX/2iJ;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, LX/2iJ;->SMALL:LX/2iJ;

    .line 451219
    const/4 v0, 0x2

    new-array v0, v0, [LX/2iJ;

    sget-object v1, LX/2iJ;->BIG:LX/2iJ;

    aput-object v1, v0, v2

    sget-object v1, LX/2iJ;->SMALL:LX/2iJ;

    aput-object v1, v0, v9

    sput-object v0, LX/2iJ;->$VALUES:[LX/2iJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 451212
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 451213
    iput p3, p0, LX/2iJ;->attrEnumValue:I

    .line 451214
    iput p4, p0, LX/2iJ;->shadowDrawableResource:I

    .line 451215
    iput p5, p0, LX/2iJ;->fillSizeDimen:I

    .line 451216
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2iJ;
    .locals 1

    .prologue
    .line 451211
    const-class v0, LX/2iJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2iJ;

    return-object v0
.end method

.method public static values()[LX/2iJ;
    .locals 1

    .prologue
    .line 451220
    sget-object v0, LX/2iJ;->$VALUES:[LX/2iJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2iJ;

    return-object v0
.end method


# virtual methods
.method public final getAttrEnumValue()I
    .locals 1

    .prologue
    .line 451210
    iget v0, p0, LX/2iJ;->attrEnumValue:I

    return v0
.end method

.method public final getFillRadius(Landroid/content/res/Resources;)I
    .locals 1

    .prologue
    .line 451209
    iget v0, p0, LX/2iJ;->fillSizeDimen:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public final getFullSize(Landroid/content/res/Resources;)I
    .locals 3

    .prologue
    .line 451208
    iget v0, p0, LX/2iJ;->fillSizeDimen:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    const v2, 0x7f0b01cd

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method
