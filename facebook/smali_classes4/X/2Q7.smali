.class public final LX/2Q7;
.super LX/2Q8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/2Q8",
        "<TK;TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 407710
    invoke-static {}, LX/2Q9;->a()LX/2QB;

    move-result-object v0

    invoke-virtual {v0}, LX/2QB;->c()LX/2QD;

    move-result-object v0

    invoke-virtual {v0}, LX/2QD;->b()LX/0vX;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Q8;-><init>(LX/0Xu;)V

    .line 407711
    return-void
.end method

.method private a(LX/0Xu;)LX/2Q7;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<+TK;+TV;>;)",
            "LX/2Q7",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 407725
    invoke-interface {p1}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 407726
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p0, v2, v0}, LX/2Q7;->a(Ljava/lang/Object;Ljava/lang/Iterable;)LX/2Q7;

    goto :goto_0

    .line 407727
    :cond_0
    return-object p0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)LX/2Q7;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)",
            "LX/2Q7",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 407723
    iget-object v0, p0, LX/2Q8;->a:LX/0Xu;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 407724
    return-object p0
.end method


# virtual methods
.method public final a()LX/2Q6;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2Q6",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 407716
    iget-object v0, p0, LX/2Q8;->b:Ljava/util/Comparator;

    if-eqz v0, :cond_1

    .line 407717
    invoke-static {}, LX/2Q9;->a()LX/2QB;

    move-result-object v0

    invoke-virtual {v0}, LX/2QB;->c()LX/2QD;

    move-result-object v0

    invoke-virtual {v0}, LX/2QD;->b()LX/0vX;

    move-result-object v1

    .line 407718
    iget-object v0, p0, LX/2Q8;->b:Ljava/util/Comparator;

    invoke-static {v0}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v0

    invoke-virtual {v0}, LX/1sm;->e()LX/1sm;

    move-result-object v0

    iget-object v2, p0, LX/2Q8;->a:LX/0Xu;

    invoke-interface {v2}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1sm;->d(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    .line 407719
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 407720
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v1, v3, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 407721
    :cond_0
    iput-object v1, p0, LX/2Q7;->a:LX/0Xu;

    .line 407722
    :cond_1
    iget-object v0, p0, LX/2Q8;->a:LX/0Xu;

    iget-object v1, p0, LX/2Q8;->c:Ljava/util/Comparator;

    invoke-static {v0, v1}, LX/2Q6;->b(LX/0Xu;Ljava/util/Comparator;)LX/2Q6;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)LX/2Q7;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/lang/Iterable",
            "<+TV;>;)",
            "LX/2Q7",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 407728
    iget-object v0, p0, LX/2Q8;->a:LX/0Xu;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 407729
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 407730
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 407731
    :cond_0
    return-object p0
.end method

.method public final synthetic b()LX/18f;
    .locals 1

    .prologue
    .line 407715
    invoke-virtual {p0}, LX/2Q7;->a()LX/2Q6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(LX/0Xu;)LX/2Q8;
    .locals 1

    .prologue
    .line 407714
    invoke-direct {p0, p1}, LX/2Q7;->a(LX/0Xu;)LX/2Q7;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)LX/2Q8;
    .locals 1

    .prologue
    .line 407713
    invoke-virtual {p0, p1, p2}, LX/2Q7;->a(Ljava/lang/Object;Ljava/lang/Iterable;)LX/2Q7;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)LX/2Q8;
    .locals 1

    .prologue
    .line 407712
    invoke-direct {p0, p1, p2}, LX/2Q7;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/2Q7;

    move-result-object v0

    return-object v0
.end method
