.class public final enum LX/2gR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2gR;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2gR;

.field public static final enum REQUEST_RECEIVE:LX/2gR;

.field public static final enum REQUEST_SEND:LX/2gR;

.field public static final enum RESPONSE_RECEIVE:LX/2gR;

.field public static final enum RESPONSE_SEND:LX/2gR;


# instance fields
.field public final encodedName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 448321
    new-instance v0, LX/2gR;

    const-string v1, "REQUEST_SEND"

    const-string v2, "#rqsend"

    invoke-direct {v0, v1, v3, v2}, LX/2gR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2gR;->REQUEST_SEND:LX/2gR;

    .line 448322
    new-instance v0, LX/2gR;

    const-string v1, "REQUEST_RECEIVE"

    const-string v2, "#rqrecv"

    invoke-direct {v0, v1, v4, v2}, LX/2gR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2gR;->REQUEST_RECEIVE:LX/2gR;

    .line 448323
    new-instance v0, LX/2gR;

    const-string v1, "RESPONSE_SEND"

    const-string v2, "#rpsend"

    invoke-direct {v0, v1, v5, v2}, LX/2gR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2gR;->RESPONSE_SEND:LX/2gR;

    .line 448324
    new-instance v0, LX/2gR;

    const-string v1, "RESPONSE_RECEIVE"

    const-string v2, "#rprecv"

    invoke-direct {v0, v1, v6, v2}, LX/2gR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    .line 448325
    const/4 v0, 0x4

    new-array v0, v0, [LX/2gR;

    sget-object v1, LX/2gR;->REQUEST_SEND:LX/2gR;

    aput-object v1, v0, v3

    sget-object v1, LX/2gR;->REQUEST_RECEIVE:LX/2gR;

    aput-object v1, v0, v4

    sget-object v1, LX/2gR;->RESPONSE_SEND:LX/2gR;

    aput-object v1, v0, v5

    sget-object v1, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    aput-object v1, v0, v6

    sput-object v0, LX/2gR;->$VALUES:[LX/2gR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 448326
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 448327
    iput-object p3, p0, LX/2gR;->encodedName:Ljava/lang/String;

    .line 448328
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2gR;
    .locals 1

    .prologue
    .line 448329
    const-class v0, LX/2gR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2gR;

    return-object v0
.end method

.method public static values()[LX/2gR;
    .locals 1

    .prologue
    .line 448330
    sget-object v0, LX/2gR;->$VALUES:[LX/2gR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2gR;

    return-object v0
.end method
