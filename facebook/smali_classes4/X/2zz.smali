.class public final LX/2zz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Rl;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "B:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Rl",
        "<TA;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final f:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<TA;+TB;>;"
        }
    .end annotation
.end field

.field public final p:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<TB;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Rl;LX/0QK;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<TB;>;",
            "LX/0QK",
            "<TA;+TB;>;)V"
        }
    .end annotation

    .prologue
    .line 483981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483982
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rl;

    iput-object v0, p0, LX/2zz;->p:LX/0Rl;

    .line 483983
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QK;

    iput-object v0, p0, LX/2zz;->f:LX/0QK;

    .line 483984
    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)Z"
        }
    .end annotation

    .prologue
    .line 483985
    iget-object v0, p0, LX/2zz;->p:LX/0Rl;

    iget-object v1, p0, LX/2zz;->f:LX/0QK;

    invoke-interface {v1, p1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 483986
    instance-of v1, p1, LX/2zz;

    if-eqz v1, :cond_0

    .line 483987
    check-cast p1, LX/2zz;

    .line 483988
    iget-object v1, p0, LX/2zz;->f:LX/0QK;

    iget-object v2, p1, LX/2zz;->f:LX/0QK;

    invoke-interface {v1, v2}, LX/0QK;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2zz;->p:LX/0Rl;

    iget-object v2, p1, LX/2zz;->p:LX/0Rl;

    invoke-interface {v1, v2}, LX/0Rl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 483989
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 483990
    iget-object v0, p0, LX/2zz;->f:LX/0QK;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, LX/2zz;->p:LX/0Rl;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 483991
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/2zz;->p:LX/0Rl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2zz;->f:LX/0QK;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
