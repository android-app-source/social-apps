.class public LX/2bZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 436966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 436967
    const/4 v12, 0x0

    .line 436968
    const/4 v11, 0x0

    .line 436969
    const/4 v10, 0x0

    .line 436970
    const/4 v9, 0x0

    .line 436971
    const/4 v8, 0x0

    .line 436972
    const/4 v7, 0x0

    .line 436973
    const/4 v6, 0x0

    .line 436974
    const/4 v5, 0x0

    .line 436975
    const/4 v4, 0x0

    .line 436976
    const/4 v3, 0x0

    .line 436977
    const/4 v2, 0x0

    .line 436978
    const/4 v1, 0x0

    .line 436979
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 436980
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 436981
    const/4 v1, 0x0

    .line 436982
    :goto_0
    return v1

    .line 436983
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 436984
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_a

    .line 436985
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 436986
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 436987
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 436988
    const-string v14, "done_button_label"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 436989
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 436990
    :cond_2
    const-string v14, "empty_tags_allowed"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 436991
    const/4 v3, 0x1

    .line 436992
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 436993
    :cond_3
    const-string v14, "enabled"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 436994
    const/4 v2, 0x1

    .line 436995
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 436996
    :cond_4
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 436997
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 436998
    :cond_5
    const-string v14, "subtitle"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 436999
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 437000
    :cond_6
    const-string v14, "title"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 437001
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 437002
    :cond_7
    const-string v14, "titleForSummary"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 437003
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 437004
    :cond_8
    const-string v14, "url"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 437005
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 437006
    :cond_9
    const-string v14, "is_uf_eligible"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 437007
    const/4 v1, 0x1

    .line 437008
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v4

    goto/16 :goto_1

    .line 437009
    :cond_a
    const/16 v13, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 437010
    const/4 v13, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 437011
    if-eqz v3, :cond_b

    .line 437012
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 437013
    :cond_b
    if-eqz v2, :cond_c

    .line 437014
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->a(IZ)V

    .line 437015
    :cond_c
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 437016
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 437017
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 437018
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 437019
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 437020
    if-eqz v1, :cond_d

    .line 437021
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 437022
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 437023
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 437024
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 437025
    if-eqz v0, :cond_0

    .line 437026
    const-string v1, "done_button_label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 437027
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 437028
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 437029
    if-eqz v0, :cond_1

    .line 437030
    const-string v1, "empty_tags_allowed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 437031
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 437032
    :cond_1
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 437033
    if-eqz v0, :cond_2

    .line 437034
    const-string v1, "enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 437035
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 437036
    :cond_2
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 437037
    if-eqz v0, :cond_3

    .line 437038
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 437039
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 437040
    :cond_3
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 437041
    if-eqz v0, :cond_4

    .line 437042
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 437043
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 437044
    :cond_4
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 437045
    if-eqz v0, :cond_5

    .line 437046
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 437047
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 437048
    :cond_5
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 437049
    if-eqz v0, :cond_6

    .line 437050
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 437051
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 437052
    :cond_6
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 437053
    if-eqz v0, :cond_7

    .line 437054
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 437055
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 437056
    :cond_7
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 437057
    if-eqz v0, :cond_8

    .line 437058
    const-string v1, "is_uf_eligible"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 437059
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 437060
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 437061
    return-void
.end method
