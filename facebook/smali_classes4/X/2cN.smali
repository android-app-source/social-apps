.class public LX/2cN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2cF;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Pk;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/12L;

.field private final d:LX/0yH;

.field private final e:LX/0yP;

.field private f:LX/0yJ;

.field private g:Lcom/facebook/omnistore/Collection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 440740
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2cN;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/12L;LX/0yH;LX/0yP;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Pk;",
            ">;",
            "LX/12L;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "Lcom/facebook/zero/sdk/token/ZeroTokenFetcher;",
            ")V"
        }
    .end annotation

    .prologue
    .line 440678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440679
    iput-object p1, p0, LX/2cN;->a:LX/0Or;

    .line 440680
    iput-object p2, p0, LX/2cN;->b:LX/0Or;

    .line 440681
    iput-object p3, p0, LX/2cN;->c:LX/12L;

    .line 440682
    iput-object p4, p0, LX/2cN;->d:LX/0yH;

    .line 440683
    iput-object p5, p0, LX/2cN;->e:LX/0yP;

    .line 440684
    new-instance v0, LX/2cO;

    invoke-direct {v0, p0}, LX/2cO;-><init>(LX/2cN;)V

    iput-object v0, p0, LX/2cN;->f:LX/0yJ;

    .line 440685
    iget-object v0, p0, LX/2cN;->e:LX/0yP;

    iget-object v1, p0, LX/2cN;->f:LX/0yJ;

    invoke-virtual {v0, v1}, LX/0yP;->a(LX/0yJ;)V

    .line 440686
    return-void
.end method

.method public static a(LX/0QB;)LX/2cN;
    .locals 13

    .prologue
    .line 440711
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 440712
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 440713
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 440714
    if-nez v1, :cond_0

    .line 440715
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 440716
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 440717
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 440718
    sget-object v1, LX/2cN;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 440719
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 440720
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 440721
    :cond_1
    if-nez v1, :cond_4

    .line 440722
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 440723
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 440724
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 440725
    new-instance v7, LX/2cN;

    const/16 v8, 0x15e8

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0xea6

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/12L;->a(LX/0QB;)LX/12L;

    move-result-object v10

    check-cast v10, LX/12L;

    invoke-static {v0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v11

    check-cast v11, LX/0yH;

    invoke-static {v0}, LX/0yO;->b(LX/0QB;)LX/0yO;

    move-result-object v12

    check-cast v12, LX/0yP;

    invoke-direct/range {v7 .. v12}, LX/2cN;-><init>(LX/0Or;LX/0Or;LX/12L;LX/0yH;LX/0yP;)V

    .line 440726
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 440727
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 440728
    if-nez v1, :cond_2

    .line 440729
    sget-object v0, LX/2cN;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cN;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 440730
    :goto_1
    if-eqz v0, :cond_3

    .line 440731
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 440732
    :goto_3
    check-cast v0, LX/2cN;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 440733
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 440734
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 440735
    :catchall_1
    move-exception v0

    .line 440736
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 440737
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 440738
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 440739
    :cond_2
    :try_start_8
    sget-object v0, LX/2cN;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cN;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final indexObject(Ljava/lang/String;Ljava/lang/String;Ljava/nio/ByteBuffer;)Lcom/facebook/omnistore/IndexedFields;
    .locals 1

    .prologue
    .line 440710
    new-instance v0, Lcom/facebook/omnistore/IndexedFields;

    invoke-direct {v0}, Lcom/facebook/omnistore/IndexedFields;-><init>()V

    return-object v0
.end method

.method public final onCollectionAvailable(Lcom/facebook/omnistore/Collection;)V
    .locals 0

    .prologue
    .line 440708
    iput-object p1, p0, LX/2cN;->g:Lcom/facebook/omnistore/Collection;

    .line 440709
    return-void
.end method

.method public final onCollectionInvalidated()V
    .locals 1

    .prologue
    .line 440706
    const/4 v0, 0x0

    iput-object v0, p0, LX/2cN;->g:Lcom/facebook/omnistore/Collection;

    .line 440707
    return-void
.end method

.method public final onDeltasReceived(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/omnistore/Delta;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 440692
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 440693
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/omnistore/Delta;

    .line 440694
    iget-object v2, v0, Lcom/facebook/omnistore/Delta;->mType:Lcom/facebook/omnistore/Delta$Type;

    move-object v2, v2

    .line 440695
    sget-object v3, Lcom/facebook/omnistore/Delta$Type;->SAVE:Lcom/facebook/omnistore/Delta$Type;

    if-ne v2, v3, :cond_2

    .line 440696
    iget-object v2, v0, Lcom/facebook/omnistore/Delta;->mStatus:Lcom/facebook/omnistore/Delta$Status;

    move-object v2, v2

    .line 440697
    sget-object v3, Lcom/facebook/omnistore/Delta$Status;->PERSISTED_REMOTE:Lcom/facebook/omnistore/Delta$Status;

    if-ne v2, v3, :cond_2

    .line 440698
    iget-object v1, v0, Lcom/facebook/omnistore/Delta;->mBlob:Ljava/nio/ByteBuffer;

    move-object v0, v1

    .line 440699
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 440700
    if-nez v0, :cond_1

    .line 440701
    iget-object v0, p0, LX/2cN;->c:LX/12L;

    invoke-virtual {v0, v4}, LX/12L;->b(Z)V

    .line 440702
    :cond_0
    :goto_1
    return-void

    .line 440703
    :cond_1
    if-ne v0, v4, :cond_0

    .line 440704
    iget-object v0, p0, LX/2cN;->c:LX/12L;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/12L;->b(Z)V

    goto :goto_1

    .line 440705
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method public final provideSubscriptionInfo(Lcom/facebook/omnistore/Omnistore;)LX/2cJ;
    .locals 2

    .prologue
    .line 440687
    iget-object v0, p0, LX/2cN;->d:LX/0yH;

    sget-object v1, LX/0yY;->DIALTONE_TOGGLE_FB4A_SERVER_STICKY:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 440688
    sget-object v0, LX/2cJ;->IGNORED_INFO:LX/2cJ;

    move-object v0, v0

    .line 440689
    :goto_0
    return-object v0

    .line 440690
    :cond_0
    const-string v0, "dialtone_user_prefs"

    invoke-virtual {p1, v0}, Lcom/facebook/omnistore/Omnistore;->createCollectionNameBuilder(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v1

    iget-object v0, p0, LX/2cN;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/omnistore/CollectionName$Builder;->addSegment(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/omnistore/CollectionName$Builder;->build()Lcom/facebook/omnistore/CollectionName;

    move-result-object v0

    .line 440691
    invoke-static {v0}, LX/2cJ;->forOpenSubscription(Lcom/facebook/omnistore/CollectionName;)LX/2cJ;

    move-result-object v0

    goto :goto_0
.end method
