.class public final LX/2r2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/0iA;

.field public final d:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0iA;Landroid/content/ComponentName;)V
    .locals 0
    .param p4    # Landroid/content/ComponentName;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 471616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 471617
    iput-object p1, p0, LX/2r2;->a:Landroid/content/Context;

    .line 471618
    iput-object p2, p0, LX/2r2;->b:Lcom/facebook/content/SecureContextHelper;

    .line 471619
    iput-object p3, p0, LX/2r2;->c:LX/0iA;

    .line 471620
    iput-object p4, p0, LX/2r2;->d:Landroid/content/ComponentName;

    .line 471621
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/apptab/state/TabTag;Lcom/facebook/katana/fragment/FbChromeFragment;)V
    .locals 5
    .param p2    # Lcom/facebook/katana/fragment/FbChromeFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 471622
    if-eqz p2, :cond_1

    .line 471623
    invoke-virtual {p2}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 471624
    instance-of v0, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    if-eqz v0, :cond_1

    .line 471625
    :cond_0
    :goto_0
    return-void

    .line 471626
    :cond_1
    sget-object v0, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 471627
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object p2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, p2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 471628
    :goto_1
    move-object v0, v0

    .line 471629
    if-eqz v0, :cond_0

    .line 471630
    iget-object v1, p0, LX/2r2;->c:LX/0iA;

    const-class v4, LX/13D;

    invoke-virtual {v1, v0, v4}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/13D;

    .line 471631
    if-nez v1, :cond_6

    .line 471632
    :goto_2
    goto :goto_0

    .line 471633
    :cond_2
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 471634
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object p2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_NOTIFICATIONS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, p2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    goto :goto_1

    .line 471635
    :cond_3
    sget-object v0, Lcom/facebook/friending/tab/FriendRequestsTab;->l:Lcom/facebook/friending/tab/FriendRequestsTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 471636
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object p2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_FRIEND_REQUESTS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, p2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    goto :goto_1

    .line 471637
    :cond_4
    sget-object v0, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 471638
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object p2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_MORE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, p2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    goto :goto_1

    .line 471639
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 471640
    :cond_6
    sget-object v4, LX/1wS;->a:Ljava/lang/String;

    invoke-virtual {v1}, LX/13D;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 471641
    iget-object v4, p0, LX/2r2;->a:Landroid/content/Context;

    invoke-virtual {v1, v4}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 471642
    if-nez v1, :cond_7

    .line 471643
    goto :goto_2

    .line 471644
    :cond_7
    iget-object v2, p0, LX/2r2;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, LX/2r2;->a:Landroid/content/Context;

    invoke-interface {v2, v1, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 471645
    goto :goto_2

    .line 471646
    :cond_8
    sget-object v4, LX/3kR;->a:Ljava/lang/String;

    invoke-virtual {v1}, LX/13D;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 471647
    iget-object v4, p0, LX/2r2;->a:Landroid/content/Context;

    invoke-virtual {v1, v4}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 471648
    if-nez v1, :cond_9

    .line 471649
    goto :goto_2

    .line 471650
    :cond_9
    iget-object v2, p0, LX/2r2;->d:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 471651
    const-string v2, "target_fragment"

    sget-object v4, LX/0cQ;->QUICK_PROMOTION_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 471652
    const-string v2, "target_tab_name"

    invoke-virtual {p1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 471653
    iget-object v2, p0, LX/2r2;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, LX/2r2;->a:Landroid/content/Context;

    invoke-interface {v2, v1, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 471654
    goto/16 :goto_2

    .line 471655
    :cond_a
    goto/16 :goto_2
.end method
