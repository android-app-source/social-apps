.class public final enum LX/37O;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/37O;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/37O;

.field public static final enum AVAILABLE:LX/37O;

.field public static final enum EXPIRED:LX/37O;

.field public static final enum SOLD:LX/37O;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 501017
    new-instance v0, LX/37O;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v2}, LX/37O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/37O;->AVAILABLE:LX/37O;

    new-instance v0, LX/37O;

    const-string v1, "SOLD"

    invoke-direct {v0, v1, v3}, LX/37O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/37O;->SOLD:LX/37O;

    new-instance v0, LX/37O;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v4}, LX/37O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/37O;->EXPIRED:LX/37O;

    .line 501018
    const/4 v0, 0x3

    new-array v0, v0, [LX/37O;

    sget-object v1, LX/37O;->AVAILABLE:LX/37O;

    aput-object v1, v0, v2

    sget-object v1, LX/37O;->SOLD:LX/37O;

    aput-object v1, v0, v3

    sget-object v1, LX/37O;->EXPIRED:LX/37O;

    aput-object v1, v0, v4

    sput-object v0, LX/37O;->$VALUES:[LX/37O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 501016
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/37O;
    .locals 1

    .prologue
    .line 501014
    const-class v0, LX/37O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/37O;

    return-object v0
.end method

.method public static values()[LX/37O;
    .locals 1

    .prologue
    .line 501015
    sget-object v0, LX/37O;->$VALUES:[LX/37O;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/37O;

    return-object v0
.end method
