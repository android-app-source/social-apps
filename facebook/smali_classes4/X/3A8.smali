.class public final LX/3A8;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 524786
    const-class v1, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;

    const v0, 0x21807552

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "GetLoggedInUserQuery"

    const-string v6, "ae685d4e19adb45eb980d54da914f73c"

    const-string v7, "viewer"

    const-string v8, "10155069966731729"

    const-string v9, "10155259088891729"

    .line 524787
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 524788
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 524789
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 524790
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 524791
    sparse-switch v0, :sswitch_data_0

    .line 524792
    :goto_0
    return-object p1

    .line 524793
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 524794
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 524795
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 524796
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 524797
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4e92d738 -> :sswitch_3
        -0x476da071 -> :sswitch_0
        -0x2195f02a -> :sswitch_2
        0x1e3c9ce6 -> :sswitch_4
        0x59bc79fc -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 524798
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 524799
    :goto_1
    return v0

    .line 524800
    :pswitch_0
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_1
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 524801
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 524802
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
