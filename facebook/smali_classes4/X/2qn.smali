.class public final enum LX/2qn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2qn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2qn;

.field public static final enum UNSET:LX/2qn;

.field public static final enum ZOOMING:LX/2qn;

.field public static final enum ZOOM_ENDED:LX/2qn;

.field public static final enum ZOOM_STARTED:LX/2qn;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 471449
    new-instance v0, LX/2qn;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v2}, LX/2qn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qn;->UNSET:LX/2qn;

    .line 471450
    new-instance v0, LX/2qn;

    const-string v1, "ZOOM_STARTED"

    invoke-direct {v0, v1, v3}, LX/2qn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qn;->ZOOM_STARTED:LX/2qn;

    .line 471451
    new-instance v0, LX/2qn;

    const-string v1, "ZOOMING"

    invoke-direct {v0, v1, v4}, LX/2qn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qn;->ZOOMING:LX/2qn;

    .line 471452
    new-instance v0, LX/2qn;

    const-string v1, "ZOOM_ENDED"

    invoke-direct {v0, v1, v5}, LX/2qn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2qn;->ZOOM_ENDED:LX/2qn;

    .line 471453
    const/4 v0, 0x4

    new-array v0, v0, [LX/2qn;

    sget-object v1, LX/2qn;->UNSET:LX/2qn;

    aput-object v1, v0, v2

    sget-object v1, LX/2qn;->ZOOM_STARTED:LX/2qn;

    aput-object v1, v0, v3

    sget-object v1, LX/2qn;->ZOOMING:LX/2qn;

    aput-object v1, v0, v4

    sget-object v1, LX/2qn;->ZOOM_ENDED:LX/2qn;

    aput-object v1, v0, v5

    sput-object v0, LX/2qn;->$VALUES:[LX/2qn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 471454
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2qn;
    .locals 1

    .prologue
    .line 471455
    const-class v0, LX/2qn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2qn;

    return-object v0
.end method

.method public static values()[LX/2qn;
    .locals 1

    .prologue
    .line 471456
    sget-object v0, LX/2qn;->$VALUES:[LX/2qn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2qn;

    return-object v0
.end method
