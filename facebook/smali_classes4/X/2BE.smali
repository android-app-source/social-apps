.class public final LX/2BE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 378919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378920
    return-void
.end method

.method public static a(LX/0m2;)V
    .locals 5

    .prologue
    .line 378907
    sget-object v0, LX/0nr;->NON_NULL:LX/0nr;

    invoke-virtual {p0}, LX/0m2;->b()LX/0nr;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0nr;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 378908
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Currently, we only support serialization inclusion %s. You are using %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, LX/0nr;->NON_NULL:LX/0nr;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, LX/0m2;->b()LX/0nr;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 378909
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 378910
    :cond_0
    return-void
.end method

.method private static a(LX/0nX;LX/0my;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 378921
    if-nez p2, :cond_0

    .line 378922
    :goto_0
    return-void

    .line 378923
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 378924
    const-class v1, LX/0gT;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 378925
    check-cast p2, LX/0gT;

    .line 378926
    invoke-interface {p2, p0, p1}, LX/0gT;->serialize(LX/0nX;LX/0my;)V

    .line 378927
    goto :goto_0

    .line 378928
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Class;->isEnum()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 378929
    check-cast p2, Ljava/lang/Enum;

    invoke-static {p0, p2}, LX/2BE;->a(LX/0nX;Ljava/lang/Enum;)V

    goto :goto_0

    .line 378930
    :cond_2
    const-class v1, Ljava/util/Collection;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 378931
    check-cast p2, Ljava/util/Collection;

    invoke-static {p0, p1, p2}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/util/Collection;)V

    goto :goto_0

    .line 378932
    :cond_3
    invoke-virtual {p0, p2}, LX/0nX;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(LX/0nX;LX/0my;Ljava/lang/String;LX/0gT;)V
    .locals 0
    .param p3    # LX/0gT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 378941
    if-nez p3, :cond_0

    .line 378942
    :goto_0
    return-void

    .line 378943
    :cond_0
    invoke-virtual {p0, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 378944
    invoke-interface {p3, p0, p1}, LX/0gT;->serialize(LX/0nX;LX/0my;)V

    .line 378945
    goto :goto_0
.end method

.method public static a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 378933
    if-nez p3, :cond_0

    .line 378934
    :goto_0
    return-void

    .line 378935
    :cond_0
    invoke-virtual {p0, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 378936
    invoke-static {p0, p1, p3}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0nX;",
            "LX/0my;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 378937
    if-nez p3, :cond_0

    .line 378938
    :goto_0
    return-void

    .line 378939
    :cond_0
    invoke-virtual {p0, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 378940
    invoke-static {p0, p1, p3}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public static a(LX/0nX;LX/0my;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0nX;",
            "LX/0my;",
            "Ljava/util/Collection",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 378911
    if-nez p2, :cond_0

    .line 378912
    :goto_0
    return-void

    .line 378913
    :cond_0
    invoke-virtual {p0}, LX/0nX;->d()V

    .line 378914
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 378915
    invoke-static {p0, p1, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/Object;)V

    goto :goto_1

    .line 378916
    :cond_1
    invoke-virtual {p0}, LX/0nX;->e()V

    goto :goto_0
.end method

.method public static a(LX/0nX;Ljava/lang/Enum;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0nX;",
            "Ljava/lang/Enum",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 378917
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 378918
    return-void
.end method

.method public static a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 378891
    if-nez p2, :cond_0

    .line 378892
    :goto_0
    return-void

    .line 378893
    :cond_0
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 378894
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, LX/0nX;->a(Z)V

    goto :goto_0
.end method

.method public static a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V
    .locals 2

    .prologue
    .line 378887
    if-nez p2, :cond_0

    .line 378888
    :goto_0
    return-void

    .line 378889
    :cond_0
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 378890
    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(D)V

    goto :goto_0
.end method

.method public static a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V
    .locals 1

    .prologue
    .line 378883
    if-nez p2, :cond_0

    .line 378884
    :goto_0
    return-void

    .line 378885
    :cond_0
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 378886
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p0, v0}, LX/0nX;->a(F)V

    goto :goto_0
.end method

.method public static a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 378895
    if-nez p2, :cond_0

    .line 378896
    :goto_0
    return-void

    .line 378897
    :cond_0
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 378898
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LX/0nX;->b(I)V

    goto :goto_0
.end method

.method public static a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 2

    .prologue
    .line 378899
    if-nez p2, :cond_0

    .line 378900
    :goto_0
    return-void

    .line 378901
    :cond_0
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 378902
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(J)V

    goto :goto_0
.end method

.method public static a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 378903
    if-nez p2, :cond_0

    .line 378904
    :goto_0
    return-void

    .line 378905
    :cond_0
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 378906
    invoke-virtual {p0, p2}, LX/0nX;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
