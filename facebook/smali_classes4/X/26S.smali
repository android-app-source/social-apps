.class public final LX/26S;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0lx;

.field public final b:LX/26S;

.field public final c:I


# direct methods
.method public constructor <init>(LX/0lx;LX/26S;)V
    .locals 1

    .prologue
    .line 372208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372209
    iput-object p1, p0, LX/26S;->a:LX/0lx;

    .line 372210
    iput-object p2, p0, LX/26S;->b:LX/26S;

    .line 372211
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, LX/26S;->c:I

    .line 372212
    return-void

    .line 372213
    :cond_0
    iget v0, p2, LX/26S;->c:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(III)LX/0lx;
    .locals 3

    .prologue
    .line 372214
    iget-object v0, p0, LX/26S;->a:LX/0lx;

    invoke-virtual {v0}, LX/0lx;->hashCode()I

    move-result v0

    if-ne v0, p1, :cond_1

    .line 372215
    iget-object v0, p0, LX/26S;->a:LX/0lx;

    invoke-virtual {v0, p2, p3}, LX/0lx;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372216
    iget-object v0, p0, LX/26S;->a:LX/0lx;

    .line 372217
    :cond_0
    :goto_0
    return-object v0

    .line 372218
    :cond_1
    iget-object v0, p0, LX/26S;->b:LX/26S;

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_3

    .line 372219
    iget-object v0, v1, LX/26S;->a:LX/0lx;

    .line 372220
    invoke-virtual {v0}, LX/0lx;->hashCode()I

    move-result v2

    if-ne v2, p1, :cond_2

    .line 372221
    invoke-virtual {v0, p2, p3}, LX/0lx;->a(II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 372222
    :cond_2
    iget-object v0, v1, LX/26S;->b:LX/26S;

    move-object v1, v0

    goto :goto_1

    .line 372223
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I[II)LX/0lx;
    .locals 3

    .prologue
    .line 372224
    iget-object v0, p0, LX/26S;->a:LX/0lx;

    invoke-virtual {v0}, LX/0lx;->hashCode()I

    move-result v0

    if-ne v0, p1, :cond_1

    .line 372225
    iget-object v0, p0, LX/26S;->a:LX/0lx;

    invoke-virtual {v0, p2, p3}, LX/0lx;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372226
    iget-object v0, p0, LX/26S;->a:LX/0lx;

    .line 372227
    :cond_0
    :goto_0
    return-object v0

    .line 372228
    :cond_1
    iget-object v0, p0, LX/26S;->b:LX/26S;

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_3

    .line 372229
    iget-object v0, v1, LX/26S;->a:LX/0lx;

    .line 372230
    invoke-virtual {v0}, LX/0lx;->hashCode()I

    move-result v2

    if-ne v2, p1, :cond_2

    .line 372231
    invoke-virtual {v0, p2, p3}, LX/0lx;->a([II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 372232
    :cond_2
    iget-object v0, v1, LX/26S;->b:LX/26S;

    move-object v1, v0

    goto :goto_1

    .line 372233
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
