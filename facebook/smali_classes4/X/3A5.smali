.class public LX/3A5;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0So;

.field private final c:Landroid/content/res/Resources;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 524743
    const-class v0, LX/3A5;

    sput-object v0, LX/3A5;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0So;Landroid/content/Context;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0So;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/2Oi;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 524744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524745
    iput-object p1, p0, LX/3A5;->b:LX/0So;

    .line 524746
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/3A5;->c:Landroid/content/res/Resources;

    .line 524747
    iput-object p3, p0, LX/3A5;->d:LX/0Or;

    .line 524748
    return-void
.end method

.method public static b(LX/0QB;)LX/3A5;
    .locals 4

    .prologue
    .line 524749
    new-instance v2, LX/3A5;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v3, 0x12c7

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/3A5;-><init>(LX/0So;Landroid/content/Context;LX/0Or;)V

    .line 524750
    return-object v2
.end method


# virtual methods
.method public final a(LX/0lF;)LX/79S;
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 524751
    const-string v0, "voip_info"

    invoke-virtual {p1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 524752
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Expected response body to contain a voip_info field."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 524753
    :cond_0
    const-string v0, "voip_info"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    .line 524754
    const-string v0, "is_callable"

    invoke-virtual {v9, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 524755
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Expected voip_info to contain is_callable field."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 524756
    :cond_1
    const-string v0, "is_callable"

    invoke-virtual {v9, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v10

    .line 524757
    const-string v0, "is_callable_webrtc"

    invoke-virtual {v9, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0, v8}, LX/16N;->a(LX/0lF;Z)Z

    move-result v11

    .line 524758
    const-string v0, "has_permission"

    invoke-virtual {v9, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0, v8}, LX/16N;->a(LX/0lF;Z)Z

    move-result v12

    .line 524759
    const-string v0, "mobile"

    invoke-static {v10}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "desktop"

    invoke-static {v11}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    const-string v4, "permission"

    invoke-static {v12}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v6

    .line 524760
    if-nez v10, :cond_2

    if-eqz v11, :cond_3

    :cond_2
    const/4 v1, 0x1

    .line 524761
    :goto_0
    if-nez v1, :cond_6

    const-string v0, "reason_description"

    invoke-virtual {v9, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 524762
    const-string v0, "reason_description"

    invoke-virtual {v9, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 524763
    :goto_1
    if-nez v1, :cond_5

    .line 524764
    if-eqz v12, :cond_4

    .line 524765
    sget-object v3, LX/79O;->b:Ljava/lang/String;

    .line 524766
    :goto_2
    new-instance v0, LX/79S;

    iget-object v4, p0, LX/3A5;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    invoke-direct/range {v0 .. v6}, LX/79S;-><init>(ZLjava/lang/String;Ljava/lang/String;JLX/0P1;)V

    return-object v0

    :cond_3
    move v1, v8

    .line 524767
    goto :goto_0

    .line 524768
    :cond_4
    sget-object v3, LX/79O;->a:Ljava/lang/String;

    goto :goto_2

    :cond_5
    move-object v3, v7

    goto :goto_2

    :cond_6
    move-object v2, v7

    goto :goto_1
.end method
