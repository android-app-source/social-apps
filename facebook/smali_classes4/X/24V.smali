.class public final LX/24V;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/24W;

.field public b:F

.field public c:F

.field public d:I

.field public e:I

.field public f:F

.field public g:F

.field public h:F

.field public i:LX/24X;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 367528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 367526
    invoke-direct {p0}, LX/24V;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 367527
    iget v0, p0, LX/24V;->d:I

    if-lez v0, :cond_0

    iget v0, p0, LX/24V;->d:I

    :goto_0
    return v0

    :cond_0
    int-to-float v0, p1

    iget v1, p0, LX/24V;->g:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method public final a()[I
    .locals 2

    .prologue
    .line 367522
    sget-object v0, LX/4oX;->a:[I

    iget-object v1, p0, LX/24V;->i:LX/24X;

    invoke-virtual {v1}, LX/24X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 367523
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 367524
    :goto_0
    return-object v0

    :pswitch_0
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        -0x1000000
        -0x1000000
        0x0
    .end array-data

    .line 367525
    :array_1
    .array-data 4
        -0x1000000
        -0x1000000
        0x0
    .end array-data

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 367518
    iget v0, p0, LX/24V;->e:I

    if-lez v0, :cond_0

    iget v0, p0, LX/24V;->e:I

    :goto_0
    return v0

    :cond_0
    int-to-float v0, p1

    iget v1, p0, LX/24V;->h:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method public final b()[F
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 367519
    sget-object v0, LX/4oX;->a:[I

    iget-object v1, p0, LX/24V;->i:LX/24X;

    invoke-virtual {v1}, LX/24X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 367520
    const/4 v0, 0x4

    new-array v0, v0, [F

    iget v1, p0, LX/24V;->f:F

    sub-float v1, v4, v1

    iget v2, p0, LX/24V;->c:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v5

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    aput v1, v0, v6

    iget v1, p0, LX/24V;->f:F

    sub-float v1, v4, v1

    div-float/2addr v1, v5

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x2

    iget v2, p0, LX/24V;->f:F

    add-float/2addr v2, v4

    div-float/2addr v2, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, LX/24V;->f:F

    add-float/2addr v2, v4

    iget v3, p0, LX/24V;->c:F

    add-float/2addr v2, v3

    div-float/2addr v2, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    aput v2, v0, v1

    .line 367521
    :goto_0
    return-object v0

    :pswitch_0
    const/4 v0, 0x3

    new-array v0, v0, [F

    aput v3, v0, v6

    iget v1, p0, LX/24V;->f:F

    invoke-static {v1, v4}, Ljava/lang/Math;->min(FF)F

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x2

    iget v2, p0, LX/24V;->f:F

    iget v3, p0, LX/24V;->c:F

    add-float/2addr v2, v3

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    aput v2, v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
