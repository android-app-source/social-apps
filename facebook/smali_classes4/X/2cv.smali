.class public LX/2cv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0lC;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 442389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442390
    iput-object p1, p0, LX/2cv;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 442391
    iput-object p2, p0, LX/2cv;->b:LX/0lC;

    .line 442392
    return-void
.end method

.method public static b(LX/0QB;)LX/2cv;
    .locals 3

    .prologue
    .line 442403
    new-instance v2, LX/2cv;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-direct {v2, v0, v1}, LX/2cv;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;)V

    .line 442404
    return-object v2
.end method


# virtual methods
.method public final a()LX/3C9;
    .locals 2

    .prologue
    .line 442402
    new-instance v0, LX/3lg;

    iget-object v1, p0, LX/2cv;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/3lg;-><init>(LX/2cv;LX/0hN;)V

    return-object v0
.end method

.method public final a(LX/2d5;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2d5",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 442393
    iget-object v1, p0, LX/2cv;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 442394
    iget-object v2, p1, LX/2d5;->a:LX/0Tn;

    move-object v2, v2

    .line 442395
    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 442396
    if-nez v1, :cond_0

    .line 442397
    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, LX/2cv;->b:LX/0lC;

    .line 442398
    iget-object v2, p1, LX/2d5;->b:Ljava/lang/Class;

    move-object v2, v2

    .line 442399
    invoke-virtual {v0, v1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 442400
    :catch_0
    move-exception v0

    .line 442401
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
