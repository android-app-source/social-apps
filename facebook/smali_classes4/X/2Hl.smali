.class public final LX/2Hl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/2C2;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/2C2;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 391061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391062
    iput-object p1, p0, LX/2Hl;->a:LX/0QB;

    .line 391063
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 391064
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2Hl;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 391065
    packed-switch p2, :pswitch_data_0

    .line 391066
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 391067
    :pswitch_0
    invoke-static {p1}, LX/2zl;->getInstance__com_facebook_omnistore_mqtt_ConnectionStarter__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2zl;

    move-result-object v0

    .line 391068
    :goto_0
    return-object v0

    .line 391069
    :pswitch_1
    invoke-static {p1}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v0

    goto :goto_0

    .line 391070
    :pswitch_2
    invoke-static {p1}, LX/2C1;->a(LX/0QB;)LX/2C1;

    move-result-object v0

    goto :goto_0

    .line 391071
    :pswitch_3
    invoke-static {p1}, LX/29A;->a(LX/0QB;)LX/29A;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 391072
    const/4 v0, 0x4

    return v0
.end method
