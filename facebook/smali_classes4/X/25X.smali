.class public abstract LX/25X;
.super LX/25Y;
.source ""


# instance fields
.field private final a:F

.field public final b:Landroid/view/animation/LinearInterpolator;

.field public final c:Landroid/view/animation/DecelerateInterpolator;

.field public d:Landroid/graphics/PointF;

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 369642
    invoke-direct {p0}, LX/25Y;-><init>()V

    .line 369643
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, LX/25X;->b:Landroid/view/animation/LinearInterpolator;

    .line 369644
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, LX/25X;->c:Landroid/view/animation/DecelerateInterpolator;

    .line 369645
    iput v1, p0, LX/25X;->e:I

    iput v1, p0, LX/25X;->f:I

    .line 369646
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/25X;->a(Landroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, LX/25X;->a:F

    .line 369647
    return-void
.end method

.method private static a(II)I
    .locals 2

    .prologue
    .line 369669
    sub-int v0, p0, p1

    .line 369670
    mul-int v1, p0, v0

    if-gtz v1, :cond_0

    .line 369671
    const/4 v0, 0x0

    .line 369672
    :cond_0
    return v0
.end method

.method public static a(IIIII)I
    .locals 2

    .prologue
    .line 369659
    packed-switch p4, :pswitch_data_0

    .line 369660
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "snap preference should be one of the constants defined in SmoothScroller, starting with SNAP_"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369661
    :pswitch_0
    sub-int v0, p2, p0

    .line 369662
    :cond_0
    :goto_0
    return v0

    .line 369663
    :pswitch_1
    sub-int v0, p3, p1

    goto :goto_0

    .line 369664
    :pswitch_2
    sub-int v0, p2, p0

    .line 369665
    if-gtz v0, :cond_0

    .line 369666
    sub-int v0, p3, p1

    .line 369667
    if-ltz v0, :cond_0

    .line 369668
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/util/DisplayMetrics;)F
    .locals 2

    .prologue
    .line 369658
    const/high16 v0, 0x41c80000    # 25.0f

    iget v1, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public a(Landroid/view/View;I)I
    .locals 5

    .prologue
    .line 369648
    iget-object v0, p0, LX/25Y;->c:LX/1OR;

    move-object v1, v0

    .line 369649
    invoke-virtual {v1}, LX/1OR;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 369650
    const/4 v0, 0x0

    .line 369651
    :goto_0
    return v0

    .line 369652
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 369653
    invoke-static {p1}, LX/1OR;->i(Landroid/view/View;)I

    move-result v2

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v2, v3

    .line 369654
    invoke-static {p1}, LX/1OR;->k(Landroid/view/View;)I

    move-result v3

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v3

    .line 369655
    invoke-virtual {v1}, LX/1OR;->y()I

    move-result v3

    .line 369656
    invoke-virtual {v1}, LX/1OR;->w()I

    move-result v4

    invoke-virtual {v1}, LX/1OR;->A()I

    move-result v1

    sub-int v1, v4, v1

    .line 369657
    invoke-static {v2, v0, v3, v1, p2}, LX/25X;->a(IIIII)I

    move-result v0

    goto :goto_0
.end method

.method public abstract a(I)Landroid/graphics/PointF;
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 369673
    const/4 v0, 0x0

    iput v0, p0, LX/25X;->f:I

    iput v0, p0, LX/25X;->e:I

    .line 369674
    const/4 v0, 0x0

    iput-object v0, p0, LX/25X;->d:Landroid/graphics/PointF;

    .line 369675
    return-void
.end method

.method public final a(IILX/25Z;)V
    .locals 3

    .prologue
    .line 369601
    invoke-virtual {p0}, LX/25Y;->i()I

    move-result v0

    if-nez v0, :cond_1

    .line 369602
    invoke-virtual {p0}, LX/25Y;->e()V

    .line 369603
    :cond_0
    :goto_0
    return-void

    .line 369604
    :cond_1
    iget v0, p0, LX/25X;->e:I

    invoke-static {v0, p1}, LX/25X;->a(II)I

    move-result v0

    iput v0, p0, LX/25X;->e:I

    .line 369605
    iget v0, p0, LX/25X;->f:I

    invoke-static {v0, p2}, LX/25X;->a(II)I

    move-result v0

    iput v0, p0, LX/25X;->f:I

    .line 369606
    iget v0, p0, LX/25X;->e:I

    if-nez v0, :cond_0

    iget v0, p0, LX/25X;->f:I

    if-nez v0, :cond_0

    .line 369607
    const p2, 0x461c4000    # 10000.0f

    const/4 v2, 0x0

    const p1, 0x3f99999a    # 1.2f

    .line 369608
    iget v0, p0, LX/25Y;->a:I

    move v0, v0

    .line 369609
    invoke-virtual {p0, v0}, LX/25X;->a(I)Landroid/graphics/PointF;

    move-result-object v0

    .line 369610
    if-eqz v0, :cond_2

    iget v1, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_3

    iget v1, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_3

    .line 369611
    :cond_2
    const-string v0, "LinearSmoothScroller"

    const-string v1, "To support smooth scrolling, you should override \nLayoutManager#computeScrollVectorForPosition.\nFalling back to instant scroll"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 369612
    iget v0, p0, LX/25Y;->a:I

    move v0, v0

    .line 369613
    iput v0, p3, LX/25Z;->d:I

    .line 369614
    invoke-virtual {p0}, LX/25Y;->e()V

    .line 369615
    :goto_1
    goto :goto_0

    .line 369616
    :cond_3
    invoke-static {v0}, LX/25Y;->a(Landroid/graphics/PointF;)V

    .line 369617
    iput-object v0, p0, LX/25X;->d:Landroid/graphics/PointF;

    .line 369618
    iget v1, v0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, p2

    float-to-int v1, v1

    iput v1, p0, LX/25X;->e:I

    .line 369619
    iget v0, v0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, LX/25X;->f:I

    .line 369620
    const/16 v0, 0x2710

    invoke-virtual {p0, v0}, LX/25X;->b(I)I

    move-result v0

    .line 369621
    iget v1, p0, LX/25X;->e:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iget v2, p0, LX/25X;->f:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iget-object p1, p0, LX/25X;->b:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {p3, v1, v2, v0, p1}, LX/25Z;->a(IIILandroid/view/animation/Interpolator;)V

    goto :goto_1
.end method

.method public final a(Landroid/view/View;LX/25Z;)V
    .locals 8

    .prologue
    .line 369622
    invoke-virtual {p0}, LX/25X;->b()I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/25X;->a(Landroid/view/View;I)I

    move-result v0

    .line 369623
    invoke-virtual {p0}, LX/25X;->c()I

    move-result v1

    .line 369624
    iget-object v2, p0, LX/25Y;->c:LX/1OR;

    move-object v3, v2

    .line 369625
    invoke-virtual {v3}, LX/1OR;->h()Z

    move-result v2

    if-nez v2, :cond_1

    .line 369626
    const/4 v2, 0x0

    .line 369627
    :goto_0
    move v1, v2

    .line 369628
    mul-int v2, v0, v0

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 369629
    invoke-virtual {p0, v2}, LX/25X;->b(I)I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x3fd57a786c22680aL    # 0.3356

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    move v2, v4

    .line 369630
    if-lez v2, :cond_0

    .line 369631
    neg-int v0, v0

    neg-int v1, v1

    iget-object v3, p0, LX/25X;->c:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {p2, v0, v1, v2, v3}, LX/25Z;->a(IIILandroid/view/animation/Interpolator;)V

    .line 369632
    :cond_0
    return-void

    .line 369633
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, LX/1a3;

    .line 369634
    invoke-static {p1}, LX/1OR;->j(Landroid/view/View;)I

    move-result v4

    iget v5, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v4, v5

    .line 369635
    invoke-static {p1}, LX/1OR;->l(Landroid/view/View;)I

    move-result v5

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v2, v5

    .line 369636
    invoke-virtual {v3}, LX/1OR;->z()I

    move-result v5

    .line 369637
    invoke-virtual {v3}, LX/1OR;->x()I

    move-result v6

    invoke-virtual {v3}, LX/1OR;->B()I

    move-result v3

    sub-int v3, v6, v3

    .line 369638
    invoke-static {v4, v2, v5, v3, v1}, LX/25X;->a(IIIII)I

    move-result v2

    goto :goto_0
.end method

.method public b()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 369639
    iget-object v0, p0, LX/25X;->d:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/25X;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/25X;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public b(I)I
    .locals 2

    .prologue
    .line 369640
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LX/25X;->a:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public c()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 369641
    iget-object v0, p0, LX/25X;->d:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/25X;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/25X;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method
