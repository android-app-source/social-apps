.class public LX/3L7;
.super LX/3L8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3L8",
        "<",
        "LX/3Nt;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3L7;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 549944
    invoke-direct {p0}, LX/3L8;-><init>()V

    .line 549945
    return-void
.end method

.method public static a(LX/0QB;)LX/3L7;
    .locals 3

    .prologue
    .line 549932
    sget-object v0, LX/3L7;->a:LX/3L7;

    if-nez v0, :cond_1

    .line 549933
    const-class v1, LX/3L7;

    monitor-enter v1

    .line 549934
    :try_start_0
    sget-object v0, LX/3L7;->a:LX/3L7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 549935
    if-eqz v2, :cond_0

    .line 549936
    :try_start_1
    new-instance v0, LX/3L7;

    invoke-direct {v0}, LX/3L7;-><init>()V

    .line 549937
    move-object v0, v0

    .line 549938
    sput-object v0, LX/3L7;->a:LX/3L7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 549939
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 549940
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 549941
    :cond_1
    sget-object v0, LX/3L7;->a:LX/3L7;

    return-object v0

    .line 549942
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 549943
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/3OO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549931
    sget-object v0, LX/3Nt;->CONTACT_ROW:LX/3Nt;

    return-object v0
.end method

.method public final a(LX/3OQ;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549928
    sget-object v0, LX/Ji5;->f:LX/3OQ;

    if-ne p1, v0, :cond_0

    .line 549929
    sget-object v0, LX/3Nt;->LOADING_MORE:LX/3Nt;

    .line 549930
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/3OU;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549925
    sget-object v0, LX/3Nt;->NEARBY_FRIENDS:LX/3Nt;

    return-object v0
.end method

.method public final a(LX/3Ou;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549927
    sget-object v0, LX/3Nt;->FAVORITES_SECTION_HEADER:LX/3Nt;

    return-object v0
.end method

.method public final a(LX/DAO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549926
    sget-object v0, LX/3Nt;->CAMERA_ROLL_ROW:LX/3Nt;

    return-object v0
.end method

.method public final a(LX/DAU;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549946
    sget-object v0, LX/3Nt;->GROUP_ROW:LX/3Nt;

    return-object v0
.end method

.method public final a(LX/DAV;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549920
    sget-object v0, LX/3Nt;->MESSAGE_SEARCH_RESULT_ROW:LX/3Nt;

    return-object v0
.end method

.method public final a(LX/DAW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549918
    sget-object v0, LX/3Nt;->MONTAGE_ROW:LX/3Nt;

    return-object v0
.end method

.method public final a(LX/DAX;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549919
    sget-object v0, LX/3Nt;->PAYMENT_ELIGIBLE_FOOTER:LX/3Nt;

    return-object v0
.end method

.method public final a(LX/DAY;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549921
    sget-object v0, LX/3Nt;->PAYMENT_ROW:LX/3Nt;

    return-object v0
.end method

.method public final a(LX/DAa;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549922
    sget-object v0, LX/3Nt;->SECTION_HEADER:LX/3Nt;

    return-object v0
.end method

.method public final a(LX/DAb;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549923
    sget-object v0, LX/3Nt;->SECTION_SPLITTER:LX/3Nt;

    return-object v0
.end method

.method public final a(LX/DAj;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549924
    sget-object v0, LX/3Nt;->NEARBY_FRIENDS_MORE_ROW:LX/3Nt;

    return-object v0
.end method
