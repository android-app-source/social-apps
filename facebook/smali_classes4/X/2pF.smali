.class public LX/2pF;
.super LX/2ol;
.source ""


# instance fields
.field public final a:D


# direct methods
.method public constructor <init>(D)V
    .locals 1

    .prologue
    .line 467883
    invoke-direct {p0}, LX/2ol;-><init>()V

    .line 467884
    iput-wide p1, p0, LX/2pF;->a:D

    .line 467885
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 467886
    const-string v0, "%s: minAspectRatio=%f"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, LX/2pF;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
