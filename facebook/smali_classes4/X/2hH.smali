.class public final LX/2hH;
.super LX/2hI;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 0

    .prologue
    .line 449939
    iput-object p1, p0, LX/2hH;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-direct {p0}, LX/2hI;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 449940
    check-cast p1, LX/2iD;

    .line 449941
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/2hH;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    if-nez v0, :cond_1

    .line 449942
    :cond_0
    :goto_0
    return-void

    .line 449943
    :cond_1
    iget-object v0, p0, LX/2hH;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-wide v2, p1, LX/2iD;->a:J

    iget-boolean v1, p1, LX/2iD;->b:Z

    .line 449944
    iget-object v4, v0, LX/2iK;->k:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {v4, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 449945
    :cond_2
    :goto_1
    goto :goto_0

    .line 449946
    :cond_3
    iget-object v4, v0, LX/2iK;->k:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {v4, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2no;

    .line 449947
    iget-boolean p0, v4, LX/2no;->e:Z

    move p0, p0

    .line 449948
    if-eq p0, v1, :cond_2

    .line 449949
    iput-boolean v1, v4, LX/2no;->e:Z

    .line 449950
    invoke-virtual {v0}, LX/2iK;->m()V

    .line 449951
    const v4, -0x58fd495f

    invoke-static {v0, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_1
.end method
