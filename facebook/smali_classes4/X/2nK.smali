.class public LX/2nK;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pv;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CDJ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/2nK",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CDJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463963
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 463964
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/2nK;->b:LX/0Zi;

    .line 463965
    iput-object p1, p0, LX/2nK;->a:LX/0Ot;

    .line 463966
    return-void
.end method

.method public static a(LX/0QB;)LX/2nK;
    .locals 4

    .prologue
    .line 463921
    const-class v1, LX/2nK;

    monitor-enter v1

    .line 463922
    :try_start_0
    sget-object v0, LX/2nK;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 463923
    sput-object v2, LX/2nK;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 463924
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463925
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 463926
    new-instance v3, LX/2nK;

    const/16 p0, 0x21ea

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2nK;-><init>(LX/0Ot;)V

    .line 463927
    move-object v0, v3

    .line 463928
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 463929
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2nK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463930
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 463931
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 463972
    invoke-static {}, LX/1dS;->b()V

    .line 463973
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2f74128

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 463967
    check-cast p6, LX/CDH;

    .line 463968
    iget-object v1, p0, LX/2nK;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v1, p6, LX/CDH;->f:LX/1no;

    .line 463969
    iget v2, v1, LX/1no;->a:I

    iput v2, p5, LX/1no;->a:I

    .line 463970
    iget v2, v1, LX/1no;->b:I

    iput v2, p5, LX/1no;->b:I

    .line 463971
    const/16 v1, 0x1f

    const v2, -0x3c3318bc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 463947
    iget-object v0, p0, LX/2nK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 463948
    new-instance v0, LX/7zk;

    sget-object p0, LX/CDJ;->a:LX/7zi;

    invoke-direct {v0, p1, p0}, LX/7zk;-><init>(Landroid/content/Context;LX/7zi;)V

    move-object v0, v0

    .line 463949
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 8

    .prologue
    .line 463950
    check-cast p2, LX/CDH;

    .line 463951
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 463952
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 463953
    iget-object v0, p0, LX/2nK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDJ;

    iget-object v2, p2, LX/CDH;->a:LX/3EE;

    iget-object v3, p2, LX/CDH;->b:LX/1Pe;

    iget-object v4, p2, LX/CDH;->d:Ljava/lang/Boolean;

    move-object v1, p1

    .line 463954
    invoke-static {v1}, LX/CD8;->c(LX/1De;)LX/CD6;

    move-result-object v7

    iget-object p0, v0, LX/CDJ;->c:LX/361;

    invoke-virtual {p0, v1}, LX/361;->c(LX/1De;)LX/CDK;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/CDK;->a(LX/3EE;)LX/CDK;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/CDK;->a(LX/1Pe;)LX/CDK;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    invoke-virtual {v7, p0}, LX/CD6;->a(LX/1X1;)LX/CD6;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->d()LX/1X1;

    move-result-object v7

    .line 463955
    iget-object p0, v0, LX/CDJ;->b:LX/1Z4;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, v7, p1, v5, v6}, LX/1Z4;->a(LX/1X1;ZLX/1np;LX/1np;)V

    .line 463956
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 463957
    check-cast v0, LX/1dV;

    iput-object v0, p2, LX/CDH;->e:LX/1dV;

    .line 463958
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 463959
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 463960
    check-cast v0, LX/1no;

    iput-object v0, p2, LX/CDH;->f:LX/1no;

    .line 463961
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 463962
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 463946
    const/4 v0, 0x1

    return v0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 463945
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 463941
    check-cast p3, LX/CDH;

    .line 463942
    iget-object v0, p0, LX/2nK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDJ;

    check-cast p2, LX/7zk;

    iget-object v1, p3, LX/CDH;->e:LX/1dV;

    .line 463943
    iget-object p0, v0, LX/CDJ;->b:LX/1Z4;

    invoke-virtual {p0, p2, v1}, LX/1Z4;->a(LX/7zk;LX/1dV;)V

    .line 463944
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 463933
    iget-object v0, p0, LX/2nK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDJ;

    check-cast p2, LX/7zk;

    .line 463934
    iget-object p0, v0, LX/CDJ;->b:LX/1Z4;

    .line 463935
    iget-object p1, p2, LX/7zk;->b:LX/3nq;

    move-object p1, p1

    .line 463936
    invoke-virtual {p1}, LX/3nq;->a()V

    .line 463937
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, LX/3nq;->setVisibility(I)V

    .line 463938
    iget-object v0, p0, LX/1Z4;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 463939
    iget-object p1, p0, LX/1Z4;->b:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 463940
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 463932
    const/16 v0, 0xf

    return v0
.end method
