.class public LX/27j;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0W9;

.field public final c:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0W9;Landroid/telephony/TelephonyManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 373079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373080
    iput-object p1, p0, LX/27j;->a:LX/0Zb;

    .line 373081
    iput-object p2, p0, LX/27j;->b:LX/0W9;

    .line 373082
    iput-object p3, p0, LX/27j;->c:Landroid/telephony/TelephonyManager;

    .line 373083
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 373084
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "language_switcher_login"

    .line 373085
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 373086
    move-object v0, v0

    .line 373087
    return-object v0
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 373088
    iget-object v0, p0, LX/27j;->a:LX/0Zb;

    const-string v1, "language_switcher_login_locale_clicked"

    invoke-static {v1}, LX/27j;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 373089
    return-void
.end method
