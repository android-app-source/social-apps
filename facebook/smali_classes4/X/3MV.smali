.class public LX/3MV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3MV;


# instance fields
.field private a:LX/3MG;

.field private final b:LX/2N9;


# direct methods
.method public constructor <init>(LX/3MG;LX/2N9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 555024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555025
    iput-object p1, p0, LX/3MV;->a:LX/3MG;

    .line 555026
    iput-object p2, p0, LX/3MV;->b:LX/2N9;

    .line 555027
    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/graphql/threads/UserInfoInterfaces$CustomerDataFrag;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserCustomTag;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v12, 0x10

    .line 555015
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 555016
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 555017
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    .line 555018
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v9, :cond_1

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel$NodesModel;

    .line 555019
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel$NodesModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel$NodesModel$CustomTagModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 555020
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel$NodesModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel$NodesModel$CustomTagModel;

    move-result-object v5

    .line 555021
    new-instance v0, Lcom/facebook/user/model/UserCustomTag;

    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel$NodesModel$CustomTagModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel$NodesModel$CustomTagModel;->dk_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel$NodesModel$CustomTagModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v10

    long-to-int v3, v10

    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel$NodesModel$CustomTagModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v10

    long-to-int v4, v10

    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel$CustomTagLinksModel$NodesModel$CustomTagModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v10

    long-to-int v5, v10

    invoke-direct/range {v0 .. v5}, Lcom/facebook/user/model/UserCustomTag;-><init>(Ljava/lang/String;Ljava/lang/String;III)V

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 555022
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 555023
    :cond_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/3MV;
    .locals 5

    .prologue
    .line 555002
    sget-object v0, LX/3MV;->c:LX/3MV;

    if-nez v0, :cond_1

    .line 555003
    const-class v1, LX/3MV;

    monitor-enter v1

    .line 555004
    :try_start_0
    sget-object v0, LX/3MV;->c:LX/3MV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 555005
    if-eqz v2, :cond_0

    .line 555006
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 555007
    new-instance p0, LX/3MV;

    invoke-static {v0}, LX/3MG;->a(LX/0QB;)LX/3MG;

    move-result-object v3

    check-cast v3, LX/3MG;

    invoke-static {v0}, LX/2N9;->b(LX/0QB;)LX/2N9;

    move-result-object v4

    check-cast v4, LX/2N9;

    invoke-direct {p0, v3, v4}, LX/3MV;-><init>(LX/3MG;LX/2N9;)V

    .line 555008
    move-object v0, p0

    .line 555009
    sput-object v0, LX/3MV;->c:LX/3MV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 555010
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 555011
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 555012
    :cond_1
    sget-object v0, LX/3MV;->c:LX/3MV;

    return-object v0

    .line 555013
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 555014
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/3MV;Ljava/lang/String;Lcom/facebook/user/model/User;Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;Z)Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;"
        }
    .end annotation

    .prologue
    .line 554998
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 554999
    if-nez p2, :cond_0

    .line 555000
    invoke-static {p1}, LX/3MV;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    .line 555001
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/facebook/user/model/User;->d()Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/user/model/User;->s()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lcom/facebook/user/model/User;->L()Z

    move-result v8

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, p4

    move-object v7, p5

    move/from16 v9, p6

    invoke-static/range {v0 .. v9}, LX/3MV;->a(LX/3MV;Ljava/lang/String;Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/3MV;Ljava/lang/String;Lcom/facebook/user/model/User;Ljava/util/Map;Ljava/util/Map;Z)Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;Z)",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;"
        }
    .end annotation

    .prologue
    .line 554684
    if-nez p2, :cond_0

    .line 554685
    invoke-static {p1}, LX/3MV;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    .line 554686
    :goto_0
    return-object v0

    .line 554687
    :cond_0
    iget-object v0, p2, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v0

    .line 554688
    invoke-virtual {p2}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/user/model/User;->s()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    .line 554689
    iget-boolean v0, p2, Lcom/facebook/user/model/User;->t:Z

    move v8, v0

    .line 554690
    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, p4

    move v9, p5

    invoke-static/range {v0 .. v9}, LX/3MV;->a(LX/3MV;Ljava/lang/String;Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/3MV;Ljava/lang/String;Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .locals 9
    .param p5    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/UserKey;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ)",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;"
        }
    .end annotation

    .prologue
    .line 554983
    if-nez p3, :cond_0

    .line 554984
    iget-object v3, p0, LX/3MV;->b:LX/2N9;

    const-string v4, "GQUserConverter.getThreadParticipantsFromModel"

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v4, v2}, LX/2N9;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 554985
    :cond_0
    new-instance v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-object v3, p2

    move-object v4, p3

    move-object v5, p6

    move-object/from16 v6, p7

    move-object v7, p1

    move/from16 v8, p8

    invoke-direct/range {v2 .. v8}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 554986
    new-instance v4, LX/6fz;

    invoke-direct {v4}, LX/6fz;-><init>()V

    .line 554987
    invoke-virtual {v4, v2}, LX/6fz;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6fz;

    .line 554988
    invoke-interface {p4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 554989
    if-eqz v2, :cond_1

    .line 554990
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, LX/6fz;->c(J)LX/6fz;

    .line 554991
    :cond_1
    invoke-interface {p5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 554992
    if-eqz v2, :cond_2

    .line 554993
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, LX/6fz;->a(J)LX/6fz;

    .line 554994
    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, LX/6fz;->b(J)LX/6fz;

    .line 554995
    :cond_2
    move/from16 v0, p9

    invoke-virtual {v4, v0}, LX/6fz;->a(Z)LX/6fz;

    .line 554996
    invoke-virtual {v4}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v2

    return-object v2

    .line 554997
    :cond_3
    const-string v2, "null_key"

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .locals 4

    .prologue
    .line 554978
    const-string v0, "GQLUserConverter"

    const-string v1, "User with id %s not found in users list"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 554979
    new-instance v0, LX/6fz;

    invoke-direct {v0}, LX/6fz;-><init>()V

    new-instance v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-static {p0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, p0, v3}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/lang/String;)V

    .line 554980
    iput-object v1, v0, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 554981
    move-object v0, v0

    .line 554982
    invoke-virtual {v0}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/5bb;)Lcom/facebook/user/model/Name;
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v1, 0x0

    .line 554968
    invoke-interface {p0}, LX/5ba;->a()Ljava/lang/String;

    move-result-object v3

    .line 554969
    if-eqz v3, :cond_2

    .line 554970
    invoke-interface {p0}, LX/5bb;->b()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    move-object v0, v1

    move-object v2, v1

    :cond_0
    :goto_0
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 554971
    invoke-virtual {v6, v5, v11}, LX/15i;->j(II)I

    move-result v7

    .line 554972
    invoke-virtual {v6, v5, v11}, LX/15i;->j(II)I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v6, v5, v9}, LX/15i;->j(II)I

    move-result v9

    add-int/2addr v8, v9

    .line 554973
    const-class v9, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-virtual {v6, v5, v12, v9, v1}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v9

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->FIRST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    if-ne v9, v10, :cond_1

    .line 554974
    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 554975
    :cond_1
    const-class v9, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-virtual {v6, v5, v12, v9, v1}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->LAST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    if-ne v5, v6, :cond_0

    .line 554976
    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    move-object v2, v1

    .line 554977
    :cond_3
    new-instance v1, Lcom/facebook/user/model/Name;

    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquare;
    .locals 2
    .param p0    # Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 554957
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 554958
    if-eqz p0, :cond_0

    .line 554959
    invoke-static {p0}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 554960
    :cond_0
    if-eqz p1, :cond_1

    .line 554961
    invoke-static {p1}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 554962
    :cond_1
    if-eqz p2, :cond_2

    .line 554963
    invoke-static {p2}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 554964
    :cond_2
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 554965
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 554966
    const/4 v0, 0x0

    .line 554967
    :goto_0
    return-object v0

    :cond_3
    new-instance v0, Lcom/facebook/user/model/PicSquare;

    invoke-direct {v0, v1}, Lcom/facebook/user/model/PicSquare;-><init>(LX/0Px;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;
    .locals 3

    .prologue
    .line 554956
    new-instance v0, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;->a()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    return-object v0
.end method

.method private static a(Lcom/facebook/user/model/User;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 554951
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v0

    if-nez v0, :cond_1

    .line 554952
    :cond_0
    const/4 v0, 0x0

    .line 554953
    :goto_0
    return-object v0

    .line 554954
    :cond_1
    iget-object p0, v0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v0, p0

    .line 554955
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;",
            ">;)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 554946
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 554947
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 554948
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->B()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 554949
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;->B()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0}, LX/3MV;->a(LX/5Vu;)Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 554950
    :cond_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 554926
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 554927
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 554928
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 554929
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;->a()LX/0Px;

    move-result-object v1

    .line 554930
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 554931
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;

    .line 554932
    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->j()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    move-result-object v3

    .line 554933
    if-eqz v3, :cond_1

    .line 554934
    invoke-virtual {p0, v3}, LX/3MV;->a(LX/5Vu;)Lcom/facebook/user/model/User;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 554935
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v6, v3

    .line 554936
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_3

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 554937
    iget-object v8, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v8, v8

    .line 554938
    invoke-virtual {v4, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554939
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 554940
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->l()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_0

    .line 554941
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x1f5d54e8

    invoke-static {v1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_2
    invoke-static {v0}, LX/CMm;->a(LX/3Sb;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_3
    if-ge v1, v6, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 554942
    iget-object v7, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v7, v7

    .line 554943
    invoke-virtual {v4, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554944
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2

    .line 554945
    :cond_5
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;)Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 7

    .prologue
    .line 554903
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 554904
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 554905
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 554906
    const v2, -0x3f14c798

    if-ne v0, v2, :cond_2

    .line 554907
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->e()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v0

    .line 554908
    if-eqz v0, :cond_4

    .line 554909
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 554910
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 554911
    :cond_0
    :goto_0
    move-object v0, v0

    .line 554912
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 554913
    iget-object v1, p0, LX/3MV;->a:LX/3MG;

    invoke-virtual {v1, v0}, LX/3MG;->a(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v4

    .line 554914
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 554915
    iget-object v1, v4, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 554916
    invoke-virtual {v4}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/user/model/User;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v4

    .line 554917
    iget-object v6, v4, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v4, v6

    .line 554918
    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 554919
    :goto_1
    return-object v0

    .line 554920
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmsMessagingParticipants must have an associated phone number"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 554921
    :cond_2
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v2, Lcom/facebook/user/model/UserKey;

    sget-object v3, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v2, v3, v5}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-direct {v0, v2, v1}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    goto :goto_1

    .line 554922
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 554923
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 554924
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel;->a()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ParticipantInfoModel$MessagingActorModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 554925
    invoke-static {v0}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/5Vu;)Lcom/facebook/user/model/User;
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 554733
    new-instance v4, LX/0XI;

    invoke-direct {v4}, LX/0XI;-><init>()V

    .line 554734
    invoke-interface {p1}, LX/5Vr;->ap()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v0

    .line 554735
    if-eqz v0, :cond_0

    .line 554736
    iget-object v1, p0, LX/3MV;->a:LX/3MG;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3MG;->a(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 554737
    :goto_0
    return-object v0

    .line 554738
    :cond_0
    sget-object v0, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-interface {p1}, LX/5Vr;->B()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 554739
    invoke-interface {p1}, LX/5Vr;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 554740
    const-string v0, "GQLUserConverter"

    const-string v1, "Got a user of an unsupported graphql type: %d"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-interface {p1}, LX/5Vr;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v0, v1, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 554741
    const-string v0, "user"

    .line 554742
    iput-object v0, v4, LX/0XI;->y:Ljava/lang/String;

    .line 554743
    :goto_1
    invoke-interface {p1}, LX/5Vu;->ad()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 554744
    invoke-interface {p1}, LX/5Vu;->ad()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v0

    invoke-static {v0}, LX/3MV;->a(LX/5bb;)Lcom/facebook/user/model/Name;

    move-result-object v0

    .line 554745
    iput-object v0, v4, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 554746
    invoke-interface {p1}, LX/5Vu;->ad()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;->c()LX/5bb;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 554747
    invoke-interface {p1}, LX/5Vu;->ad()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$StructuredNameModel;->c()LX/5bb;

    move-result-object v0

    invoke-static {v0}, LX/3MV;->a(LX/5bb;)Lcom/facebook/user/model/Name;

    move-result-object v0

    .line 554748
    iput-object v0, v4, LX/0XI;->k:Lcom/facebook/user/model/Name;

    .line 554749
    :cond_1
    :goto_2
    invoke-interface {p1}, LX/5Vr;->ab()Ljava/lang/String;

    move-result-object v0

    .line 554750
    iput-object v0, v4, LX/0XI;->l:Ljava/lang/String;

    .line 554751
    invoke-interface {p1}, LX/5Vr;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 554752
    new-instance v1, Lcom/facebook/user/model/UserEmailAddress;

    invoke-interface {p1}, LX/5Vr;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;I)V

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 554753
    iput-object v0, v4, LX/0XI;->c:Ljava/util/List;

    .line 554754
    :cond_2
    invoke-interface {p1}, LX/5Vr;->au()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 554755
    invoke-interface {p1}, LX/5Vr;->au()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;

    move-result-object v0

    invoke-static {v0}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;)LX/0Px;

    move-result-object v0

    .line 554756
    iput-object v0, v4, LX/0XI;->e:LX/0Px;

    .line 554757
    :cond_3
    invoke-interface {p1}, LX/5Vr;->am()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v0

    invoke-interface {p1}, LX/5Vr;->an()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v1

    invoke-interface {p1}, LX/5Vr;->ao()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v5

    invoke-static {v0, v1, v5}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    .line 554758
    iput-object v0, v4, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 554759
    invoke-interface {p1}, LX/5Vr;->t()D

    move-result-wide v0

    double-to-float v0, v0

    .line 554760
    iput v0, v4, LX/0XI;->t:F

    .line 554761
    invoke-interface {p1}, LX/5Vr;->J()Z

    move-result v0

    .line 554762
    iput-boolean v0, v4, LX/0XI;->z:Z

    .line 554763
    invoke-interface {p1}, LX/5Vr;->L()Z

    move-result v0

    .line 554764
    iput-boolean v0, v4, LX/0XI;->I:Z

    .line 554765
    invoke-interface {p1}, LX/5Vr;->K()Z

    move-result v0

    .line 554766
    iput-boolean v0, v4, LX/0XI;->M:Z

    .line 554767
    invoke-interface {p1}, LX/5Vr;->E()Z

    move-result v0

    .line 554768
    iput-boolean v0, v4, LX/0XI;->A:Z

    .line 554769
    invoke-interface {p1}, LX/5Vr;->D()Z

    move-result v0

    .line 554770
    iput-boolean v0, v4, LX/0XI;->P:Z

    .line 554771
    invoke-interface {p1}, LX/5Vr;->G()Z

    move-result v0

    .line 554772
    iput-boolean v0, v4, LX/0XI;->Q:Z

    .line 554773
    invoke-interface {p1}, LX/5Vr;->I()Z

    move-result v0

    .line 554774
    iput-boolean v0, v4, LX/0XI;->R:Z

    .line 554775
    invoke-interface {p1}, LX/5Vr;->o()Z

    move-result v0

    .line 554776
    iput-boolean v0, v4, LX/0XI;->am:Z

    .line 554777
    invoke-interface {p1}, LX/5Vr;->M()Z

    move-result v0

    .line 554778
    iput-boolean v0, v4, LX/0XI;->F:Z

    .line 554779
    invoke-interface {p1}, LX/5Vr;->S()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, LX/5Vr;->S()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 554780
    :goto_3
    iput-wide v0, v4, LX/0XI;->X:J

    .line 554781
    invoke-interface {p1}, LX/5Vr;->n()Z

    move-result v0

    .line 554782
    iput-boolean v0, v4, LX/0XI;->Y:Z

    .line 554783
    invoke-interface {p1}, LX/5Vu;->V()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    move v0, v2

    .line 554784
    :goto_4
    iput-boolean v0, v4, LX/0XI;->H:Z

    .line 554785
    iput-boolean v3, v4, LX/0XI;->V:Z

    .line 554786
    invoke-interface {p1}, LX/5Vr;->H()Z

    move-result v0

    .line 554787
    iput-boolean v0, v4, LX/0XI;->W:Z

    .line 554788
    invoke-interface {p1}, LX/5Vr;->k()Z

    move-result v0

    .line 554789
    iput-boolean v0, v4, LX/0XI;->ag:Z

    .line 554790
    invoke-interface {p1}, LX/5Vr;->aq()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 554791
    invoke-interface {p1}, LX/5Vr;->aq()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;->a()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v0

    invoke-static {v0}, LX/6Ok;->a(Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;)LX/0XK;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0XI;->a(LX/0XK;)LX/0XI;

    .line 554792
    :cond_4
    invoke-interface {p1}, LX/5Vr;->s()Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 554793
    sget-object v0, LX/CMn;->a:[I

    invoke-interface {p1}, LX/5Vr;->s()Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 554794
    sget-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_UNKNOWN:LX/4nY;

    .line 554795
    iput-object v0, v4, LX/0XI;->B:LX/4nY;

    .line 554796
    :cond_5
    :goto_5
    invoke-interface {p1}, LX/5Vr;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 554797
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 554798
    invoke-interface {p1}, LX/5Vr;->r()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v1, v3

    :goto_6
    if-ge v1, v7, :cond_9

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    .line 554799
    sget-object v8, LX/CMn;->b:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->ordinal()I

    move-result v0

    aget v0, v8, v0

    packed-switch v0, :pswitch_data_1

    .line 554800
    :goto_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 554801
    :sswitch_0
    const-string v0, "user"

    .line 554802
    iput-object v0, v4, LX/0XI;->y:Ljava/lang/String;

    .line 554803
    goto/16 :goto_1

    .line 554804
    :sswitch_1
    const-string v0, "page"

    .line 554805
    iput-object v0, v4, LX/0XI;->y:Ljava/lang/String;

    .line 554806
    goto/16 :goto_1

    .line 554807
    :sswitch_2
    const-string v0, "event"

    .line 554808
    iput-object v0, v4, LX/0XI;->y:Ljava/lang/String;

    .line 554809
    goto/16 :goto_1

    .line 554810
    :sswitch_3
    const-string v0, "group"

    .line 554811
    iput-object v0, v4, LX/0XI;->y:Ljava/lang/String;

    .line 554812
    goto/16 :goto_1

    .line 554813
    :cond_6
    invoke-interface {p1}, LX/5Vr;->U()Ljava/lang/String;

    move-result-object v0

    .line 554814
    iput-object v0, v4, LX/0XI;->h:Ljava/lang/String;

    .line 554815
    goto/16 :goto_2

    .line 554816
    :cond_7
    const-wide/16 v0, 0x0

    goto/16 :goto_3

    :cond_8
    move v0, v3

    .line 554817
    goto/16 :goto_4

    .line 554818
    :pswitch_0
    sget-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_AGENT:LX/4nY;

    .line 554819
    iput-object v0, v4, LX/0XI;->B:LX/4nY;

    .line 554820
    goto :goto_5

    .line 554821
    :pswitch_1
    sget-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_BANK:LX/4nY;

    .line 554822
    iput-object v0, v4, LX/0XI;->B:LX/4nY;

    .line 554823
    goto :goto_5

    .line 554824
    :pswitch_2
    sget-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_BUSINESS:LX/4nY;

    .line 554825
    iput-object v0, v4, LX/0XI;->B:LX/4nY;

    .line 554826
    goto :goto_5

    .line 554827
    :pswitch_3
    sget-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_RIDE_SHARE:LX/4nY;

    .line 554828
    iput-object v0, v4, LX/0XI;->B:LX/4nY;

    .line 554829
    goto :goto_5

    .line 554830
    :pswitch_4
    sget-object v0, LX/4nY;->COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION:LX/4nY;

    .line 554831
    iput-object v0, v4, LX/0XI;->B:LX/4nY;

    .line 554832
    goto :goto_5

    .line 554833
    :pswitch_5
    sget-object v0, LX/4nX;->COMMERCE_FAQ_ENABLED:LX/4nX;

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_7

    .line 554834
    :pswitch_6
    sget-object v0, LX/4nX;->IN_MESSENGER_SHOPPING_ENABLED:LX/4nX;

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_7

    .line 554835
    :pswitch_7
    sget-object v0, LX/4nX;->COMMERCE_NUX_ENABLED:LX/4nX;

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_7

    .line 554836
    :pswitch_8
    sget-object v0, LX/4nX;->STRUCTURED_MENU_ENABLED:LX/4nX;

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_7

    .line 554837
    :pswitch_9
    sget-object v0, LX/4nX;->USER_CONTROL_TOPIC_MANAGE_ENABLED:LX/4nX;

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_7

    .line 554838
    :pswitch_a
    sget-object v0, LX/4nX;->NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED:LX/4nX;

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_7

    .line 554839
    :cond_9
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 554840
    iput-object v0, v4, LX/0XI;->C:LX/0Px;

    .line 554841
    :cond_a
    invoke-interface {p1}, LX/5Vr;->Q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 554842
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 554843
    invoke-interface {p1}, LX/5Vr;->Q()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v1, v3

    .line 554844
    :goto_8
    if-ge v1, v7, :cond_b

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    .line 554845
    invoke-static {v0}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 554846
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 554847
    :cond_b
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 554848
    iput-object v0, v4, LX/0XI;->U:LX/0Px;

    .line 554849
    :cond_c
    invoke-interface {p1}, LX/5Vr;->R()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 554850
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 554851
    invoke-interface {p1}, LX/5Vr;->R()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v1, v3

    .line 554852
    :goto_9
    if-ge v1, v7, :cond_d

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    .line 554853
    invoke-static {v0}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 554854
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 554855
    :cond_d
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 554856
    iput-object v0, v4, LX/0XI;->Z:LX/0Px;

    .line 554857
    :cond_e
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 554858
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    .line 554859
    iput-wide v0, v4, LX/0XI;->an:J

    .line 554860
    invoke-interface {p1}, LX/5Vu;->ag()Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;

    move-result-object v0

    .line 554861
    if-eqz v0, :cond_f

    .line 554862
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 554863
    if-nez v0, :cond_10

    .line 554864
    :goto_a
    move-object v1, v5

    .line 554865
    iput-object v1, v4, LX/0XI;->ai:Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    .line 554866
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 554867
    iput-object v1, v4, LX/0XI;->ad:Ljava/lang/String;

    .line 554868
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;->c()I

    move-result v1

    .line 554869
    iput v1, v4, LX/0XI;->ae:I

    .line 554870
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;->dl_()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_f

    .line 554871
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;->dl_()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v5, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0XI;->l(Ljava/lang/String;)LX/0XI;

    .line 554872
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;->dl_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 554873
    iput-object v0, v4, LX/0XI;->ac:Ljava/lang/String;

    .line 554874
    :cond_f
    invoke-virtual {v4}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    goto/16 :goto_0

    .line 554875
    :cond_10
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 554876
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;->d()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v6, v7

    .line 554877
    :goto_b
    if-ge v6, v10, :cond_11

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    .line 554878
    invoke-static {v1}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v1

    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 554879
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_b

    .line 554880
    :cond_11
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;->e()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_12

    .line 554881
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;->e()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 554882
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;->e()LX/1vs;

    move-result-object v6

    iget-object v9, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 554883
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;->e()LX/1vs;

    move-result-object v10

    iget-object v11, v10, LX/1vs;->a:LX/15i;

    iget v10, v10, LX/1vs;->b:I

    .line 554884
    new-instance p0, LX/4gi;

    invoke-direct {p0}, LX/4gi;-><init>()V

    const/4 p1, 0x2

    invoke-virtual {v5, v1, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 554885
    iput-object v1, p0, LX/4gi;->b:Ljava/lang/String;

    .line 554886
    move-object v1, p0

    .line 554887
    const/4 v5, 0x1

    invoke-virtual {v9, v6, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 554888
    iput-object v5, v1, LX/4gi;->c:Ljava/lang/String;

    .line 554889
    move-object v1, v1

    .line 554890
    invoke-virtual {v11, v10, v7}, LX/15i;->j(II)I

    move-result v5

    .line 554891
    iput v5, v1, LX/4gi;->a:I

    .line 554892
    move-object v1, v1

    .line 554893
    invoke-virtual {v1}, LX/4gi;->a()Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;

    move-result-object v1

    .line 554894
    :goto_c
    new-instance v5, LX/4gk;

    invoke-direct {v5}, LX/4gk;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessengerExtensionModel;->a()Z

    move-result v6

    .line 554895
    iput-boolean v6, v5, LX/4gk;->a:Z

    .line 554896
    move-object v5, v5

    .line 554897
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 554898
    iput-object v6, v5, LX/4gk;->b:LX/0Px;

    .line 554899
    move-object v5, v5

    .line 554900
    iput-object v1, v5, LX/4gk;->c:Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;

    .line 554901
    move-object v1, v5

    .line 554902
    invoke-virtual {v1}, LX/4gk;->a()Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    move-result-object v5

    goto/16 :goto_a

    :cond_12
    move-object v1, v5

    goto :goto_c

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3f14c798 -> :sswitch_0
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_2
        0x41e065f -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final a(LX/0Px;Ljava/util/Map;Ljava/util/Map;Ljava/util/HashSet;)Ljava/util/List;
    .locals 18
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThreadParticipantsFromModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/graphql/threads/UserInfoInterfaces$MessagingActorInfo$;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 554703
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 554704
    invoke-virtual/range {p1 .. p1}, LX/0Px;->size()I

    move-result v17

    const/4 v2, 0x0

    move v15, v2

    :goto_0
    move/from16 v0, v17

    if-ge v15, v0, :cond_6

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;

    .line 554705
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorInfoModel;->a()LX/5Vu;

    move-result-object v3

    .line 554706
    if-eqz v3, :cond_2

    .line 554707
    invoke-interface {v3}, LX/5Vr;->B()Ljava/lang/String;

    move-result-object v7

    .line 554708
    invoke-interface {v3}, LX/5Vr;->ap()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;

    move-result-object v2

    .line 554709
    if-eqz v2, :cond_3

    .line 554710
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 554711
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3MV;->a:LX/3MG;

    invoke-virtual {v2, v6}, LX/3MG;->a(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v5

    .line 554712
    new-instance v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v5}, Lcom/facebook/user/model/User;->d()Lcom/facebook/user/model/UserKey;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/user/model/User;->s()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 554713
    :goto_1
    new-instance v4, LX/6fz;

    invoke-direct {v4}, LX/6fz;-><init>()V

    .line 554714
    invoke-virtual {v4, v2}, LX/6fz;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6fz;

    .line 554715
    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 554716
    if-eqz v2, :cond_0

    .line 554717
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, LX/6fz;->c(J)LX/6fz;

    .line 554718
    :cond_0
    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 554719
    if-eqz v2, :cond_1

    .line 554720
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, LX/6fz;->a(J)LX/6fz;

    .line 554721
    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, LX/6fz;->b(J)LX/6fz;

    .line 554722
    :cond_1
    move-object/from16 v0, p4

    invoke-virtual {v0, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v4, v2}, LX/6fz;->a(Z)LX/6fz;

    .line 554723
    invoke-virtual {v4}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 554724
    :cond_2
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto/16 :goto_0

    .line 554725
    :cond_3
    new-instance v9, Lcom/facebook/user/model/UserKey;

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v9, v2, v7}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 554726
    const/4 v11, 0x0

    .line 554727
    invoke-interface {v3}, LX/5Vr;->w()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 554728
    invoke-interface {v3}, LX/5Vr;->w()LX/0Px;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v11, v2

    .line 554729
    :cond_4
    invoke-interface {v3}, LX/5Vr;->U()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    .line 554730
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3MV;->b:LX/2N9;

    const-string v4, "GQUserConverter.getThreadParticipantsFromModel"

    invoke-virtual {v2, v4, v7}, LX/2N9;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 554731
    :cond_5
    new-instance v8, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-interface {v3}, LX/5Vr;->U()Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-interface {v3}, LX/5Vr;->E()Z

    move-result v14

    invoke-direct/range {v8 .. v14}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v2, v8

    goto/16 :goto_1

    .line 554732
    :cond_6
    return-object v16
.end method

.method public final a(LX/2uF;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/HashSet;)Ljava/util/List;
    .locals 10
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThreadParticipantsFromUsers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2uF;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 554691
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 554692
    invoke-virtual {p1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 554693
    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorIdModel$MessagingActorIdOnlyModel;

    invoke-virtual {v1, v0, v9, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 554694
    const-class v2, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorIdModel$MessagingActorIdOnlyModel;

    invoke-virtual {v1, v0, v9, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorIdModel$MessagingActorIdOnlyModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$MessagingActorIdModel$MessagingActorIdOnlyModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 554695
    const-string v0, "No user id"

    invoke-static {v1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554696
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 554697
    invoke-static {v2}, LX/3MV;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v5

    .line 554698
    if-nez v5, :cond_1

    .line 554699
    invoke-virtual {p5, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, LX/3MV;->a(LX/3MV;Ljava/lang/String;Lcom/facebook/user/model/User;Ljava/util/Map;Ljava/util/Map;Z)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    .line 554700
    :goto_1
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 554701
    :cond_1
    invoke-virtual {p5, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v6}, LX/3MV;->a(LX/3MV;Ljava/lang/String;Lcom/facebook/user/model/User;Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;Z)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    goto :goto_1

    .line 554702
    :cond_2
    return-object v7
.end method
