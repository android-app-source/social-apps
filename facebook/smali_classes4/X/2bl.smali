.class public LX/2bl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2bl;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/deeplinking/annotations/IsDeepLinkingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 439634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439635
    iput-object p1, p0, LX/2bl;->a:Landroid/content/Context;

    .line 439636
    iput-object p2, p0, LX/2bl;->b:LX/0Or;

    .line 439637
    return-void
.end method

.method public static a(LX/0QB;)LX/2bl;
    .locals 5

    .prologue
    .line 439640
    sget-object v0, LX/2bl;->c:LX/2bl;

    if-nez v0, :cond_1

    .line 439641
    const-class v1, LX/2bl;

    monitor-enter v1

    .line 439642
    :try_start_0
    sget-object v0, LX/2bl;->c:LX/2bl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 439643
    if-eqz v2, :cond_0

    .line 439644
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 439645
    new-instance v4, LX/2bl;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 p0, 0x311

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/2bl;-><init>(Landroid/content/Context;LX/0Or;)V

    .line 439646
    move-object v0, v4

    .line 439647
    sput-object v0, LX/2bl;->c:LX/2bl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439648
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 439649
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 439650
    :cond_1
    sget-object v0, LX/2bl;->c:LX/2bl;

    return-object v0

    .line 439651
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 439652
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2bl;)V
    .locals 7

    .prologue
    .line 439653
    iget-object v0, p0, LX/2bl;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 439654
    if-eqz v0, :cond_1

    move v1, v2

    .line 439655
    :goto_0
    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, p0, LX/2bl;->a:Landroid/content/Context;

    const-class v6, Lcom/facebook/deeplinking/activity/DeepLinkingAliasActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 439656
    iget-object v5, p0, LX/2bl;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v4, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 439657
    new-instance v1, Landroid/content/ComponentName;

    iget-object v4, p0, LX/2bl;->a:Landroid/content/Context;

    const-class v5, Lcom/facebook/deeplinking/activity/DeepLinkingLegacyAliasActivity;

    invoke-direct {v1, v4, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 439658
    if-nez v0, :cond_0

    move v3, v2

    .line 439659
    :cond_0
    iget-object v4, p0, LX/2bl;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v1, v3, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 439660
    return-void

    :cond_1
    move v1, v3

    .line 439661
    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 0

    .prologue
    .line 439638
    invoke-static {p0}, LX/2bl;->a$redex0(LX/2bl;)V

    .line 439639
    return-void
.end method
