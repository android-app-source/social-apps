.class public LX/3LW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/3Kv;

.field public final d:LX/0tX;

.field public final e:LX/0yD;

.field public final f:LX/03V;

.field public final g:Lcom/facebook/content/SecureContextHelper;

.field public final h:LX/3LQ;

.field public final i:LX/0ad;

.field public final j:LX/3LX;

.field public final k:LX/0kL;

.field public final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/3OL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Ck;LX/3Kv;LX/0tX;LX/0yD;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/3LQ;LX/0ad;LX/3LX;LX/0kL;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550691
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3LW;->l:Ljava/util/Map;

    .line 550692
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3LW;->m:Ljava/util/Map;

    .line 550693
    iput-object p1, p0, LX/3LW;->a:Landroid/content/Context;

    .line 550694
    iput-object p2, p0, LX/3LW;->b:LX/1Ck;

    .line 550695
    iput-object p3, p0, LX/3LW;->c:LX/3Kv;

    .line 550696
    iput-object p4, p0, LX/3LW;->d:LX/0tX;

    .line 550697
    iput-object p5, p0, LX/3LW;->e:LX/0yD;

    .line 550698
    iput-object p6, p0, LX/3LW;->f:LX/03V;

    .line 550699
    iput-object p7, p0, LX/3LW;->g:Lcom/facebook/content/SecureContextHelper;

    .line 550700
    iput-object p8, p0, LX/3LW;->h:LX/3LQ;

    .line 550701
    iput-object p9, p0, LX/3LW;->i:LX/0ad;

    .line 550702
    iput-object p10, p0, LX/3LW;->j:LX/3LX;

    .line 550703
    iput-object p11, p0, LX/3LW;->k:LX/0kL;

    .line 550704
    return-void
.end method

.method public static a(LX/0QB;)LX/3LW;
    .locals 13

    .prologue
    .line 550705
    new-instance v1, LX/3LW;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0}, LX/3Kv;->b(LX/0QB;)LX/3Kv;

    move-result-object v4

    check-cast v4, LX/3Kv;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {p0}, LX/0yD;->a(LX/0QB;)LX/0yD;

    move-result-object v6

    check-cast v6, LX/0yD;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/3LQ;->a(LX/0QB;)LX/3LQ;

    move-result-object v9

    check-cast v9, LX/3LQ;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {p0}, LX/3LX;->b(LX/0QB;)LX/3LX;

    move-result-object v11

    check-cast v11, LX/3LX;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    invoke-direct/range {v1 .. v12}, LX/3LW;-><init>(Landroid/content/Context;LX/1Ck;LX/3Kv;LX/0tX;LX/0yD;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/3LQ;LX/0ad;LX/3LX;LX/0kL;)V

    .line 550706
    move-object v0, v1

    .line 550707
    return-object v0
.end method


# virtual methods
.method public final a(LX/0Pz;Ljava/util/List;LX/0P1;Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/3OQ;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/6Lx;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 550708
    new-instance v0, LX/DAa;

    iget-object v1, p0, LX/3LW;->a:Landroid/content/Context;

    const v2, 0x7f08384c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DAa;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 550709
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 550710
    iget-object v0, p0, LX/3LW;->m:Ljava/util/Map;

    .line 550711
    iget-object v2, v1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 550712
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3OL;

    .line 550713
    sget-object v0, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    if-eq v3, v0, :cond_0

    sget-object v0, LX/3OL;->INTERACTED:LX/3OL;

    if-eq v3, v0, :cond_0

    .line 550714
    iget-object v0, p0, LX/3LW;->h:LX/3LQ;

    .line 550715
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v2

    .line 550716
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "friends_nearby_divebar_wave"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "background_location"

    .line 550717
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 550718
    move-object v4, v4

    .line 550719
    const-string v5, "target_id"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "action"

    const-string p2, "impression"

    invoke-virtual {v4, v5, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "wave_state"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 550720
    iget-object v5, v0, LX/3LQ;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 550721
    :cond_0
    iget-object v0, p0, LX/3LW;->c:LX/3Kv;

    iget-object v2, p0, LX/3LW;->l:Ljava/util/Map;

    .line 550722
    iget-object v4, v1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 550723
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 550724
    iget-object v4, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v4

    .line 550725
    new-instance v5, LX/JHx;

    invoke-direct {v5, p0, v3, v4}, LX/JHx;-><init>(LX/3LW;LX/3OL;Ljava/lang/String;)V

    move-object v4, v5

    .line 550726
    iget-object v5, v1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v5, v5

    .line 550727
    invoke-virtual {p3, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/6Lx;

    .line 550728
    const/4 v7, 0x1

    sget-object p2, LX/3OH;->NEARBY_FRIENDS:LX/3OH;

    invoke-static {v0, v1, v7, p2}, LX/3Kv;->a(LX/3Kv;Lcom/facebook/user/model/User;ZLX/3OH;)LX/3OJ;

    move-result-object v7

    sget-object p2, LX/3ON;->ONE_LINE:LX/3ON;

    .line 550729
    iput-object p2, v7, LX/3OJ;->b:LX/3ON;

    .line 550730
    move-object v7, v7

    .line 550731
    iput-object v2, v7, LX/3OJ;->j:Ljava/lang/String;

    .line 550732
    move-object v7, v7

    .line 550733
    iput-object v3, v7, LX/3OJ;->k:LX/3OL;

    .line 550734
    move-object v7, v7

    .line 550735
    iput-object v4, v7, LX/3OJ;->l:LX/DHx;

    .line 550736
    move-object v7, v7

    .line 550737
    iput-object v5, v7, LX/3OJ;->p:LX/6Lx;

    .line 550738
    move-object v7, v7

    .line 550739
    invoke-virtual {v7}, LX/3OJ;->a()LX/3OO;

    move-result-object v7

    move-object v0, v7

    .line 550740
    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 550741
    iget-object v0, v1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 550742
    invoke-interface {p4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 550743
    :cond_1
    new-instance v0, LX/DAj;

    iget-object v1, p0, LX/3LW;->a:Landroid/content/Context;

    const v2, 0x7f08387b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 550744
    new-instance v2, LX/JHy;

    invoke-direct {v2, p0}, LX/JHy;-><init>(LX/3LW;)V

    move-object v2, v2

    .line 550745
    invoke-direct {v0, v1, v2}, LX/DAj;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 550746
    return-void
.end method
