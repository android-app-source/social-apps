.class public LX/2MC;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 396345
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 396346
    return-void
.end method

.method public static a(LX/2M5;)LX/2MA;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 396343
    iget-object v0, p0, LX/2M5;->e:LX/2MA;

    move-object v0, v0

    .line 396344
    return-object v0
.end method

.method public static a(LX/8Bs;)LX/8Br;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/connectivity/annotations/ConnectionStatusForMessengerHomeFragment;
    .end annotation

    .prologue
    .line 396342
    sget-object v0, LX/8Bx;->MESSENGER_HOME_FRAGMENT:LX/8Bx;

    invoke-virtual {p0, v0}, LX/8Bs;->a(LX/8Bx;)LX/8Br;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/00H;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/connectivity/annotations/IsConnStatusBannerEnabled;
    .end annotation

    .prologue
    .line 396347
    iget-object v0, p0, LX/00H;->j:LX/01T;

    move-object v0, v0

    .line 396348
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/8Bs;)LX/8Br;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/connectivity/annotations/ConnectionStatusForThreadViewFragment;
    .end annotation

    .prologue
    .line 396341
    sget-object v0, LX/8Bx;->THREAD_VIEW:LX/8Bx;

    invoke-virtual {p0, v0}, LX/8Bs;->a(LX/8Bx;)LX/8Br;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/8Bs;)LX/8Br;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/connectivity/annotations/ConnectionStatusForBusinessActivity;
    .end annotation

    .prologue
    .line 396340
    sget-object v0, LX/8Bx;->BUSINESS_ACTIVITY:LX/8Bx;

    invoke-virtual {p0, v0}, LX/8Bs;->a(LX/8Bx;)LX/8Br;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/8Bs;)LX/8Br;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/connectivity/annotations/ConnectionStatusForGamesSelectionActivity;
    .end annotation

    .prologue
    .line 396339
    sget-object v0, LX/8Bx;->GAMES_SELECTION_ACTIVITY:LX/8Bx;

    invoke-virtual {p0, v0}, LX/8Bs;->a(LX/8Bx;)LX/8Br;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 396338
    return-void
.end method
