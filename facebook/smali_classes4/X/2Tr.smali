.class public LX/2Tr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile e:LX/2Tr;


# instance fields
.field private final b:LX/0aU;

.field private final c:Landroid/content/Context;

.field public final d:LX/12x;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 414801
    const-class v0, LX/2Tr;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Tr;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0aU;LX/12x;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414803
    iput-object p1, p0, LX/2Tr;->c:Landroid/content/Context;

    .line 414804
    iput-object p2, p0, LX/2Tr;->b:LX/0aU;

    .line 414805
    iput-object p3, p0, LX/2Tr;->d:LX/12x;

    .line 414806
    return-void
.end method

.method public static a(LX/0QB;)LX/2Tr;
    .locals 6

    .prologue
    .line 414807
    sget-object v0, LX/2Tr;->e:LX/2Tr;

    if-nez v0, :cond_1

    .line 414808
    const-class v1, LX/2Tr;

    monitor-enter v1

    .line 414809
    :try_start_0
    sget-object v0, LX/2Tr;->e:LX/2Tr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 414810
    if-eqz v2, :cond_0

    .line 414811
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 414812
    new-instance p0, LX/2Tr;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v4

    check-cast v4, LX/0aU;

    invoke-static {v0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v5

    check-cast v5, LX/12x;

    invoke-direct {p0, v3, v4, v5}, LX/2Tr;-><init>(Landroid/content/Context;LX/0aU;LX/12x;)V

    .line 414813
    move-object v0, p0

    .line 414814
    sput-object v0, LX/2Tr;->e:LX/2Tr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414815
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 414816
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 414817
    :cond_1
    sget-object v0, LX/2Tr;->e:LX/2Tr;

    return-object v0

    .line 414818
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 414819
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(LX/2Tr;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 414820
    iget-object v0, p0, LX/2Tr;->b:LX/0aU;

    const-string v1, "ZERO_IP_TEST_ACTION"

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 414821
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 414822
    iget-object v0, p0, LX/2Tr;->c:Landroid/content/Context;

    invoke-static {v0, v2, v1, v2}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 414823
    iget-object v0, p0, LX/2Tr;->d:LX/12x;

    invoke-static {p0}, LX/2Tr;->d(LX/2Tr;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 414824
    return-void
.end method
