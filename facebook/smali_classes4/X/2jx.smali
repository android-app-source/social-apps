.class public LX/2jx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2jy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/2jy",
        "<TTEdge;>;"
    }
.end annotation


# instance fields
.field private final a:LX/2kH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/graphql/cursor/CursorPriorityQueue$Listener",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:J

.field private final f:LX/2js;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2js",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final g:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<TTEdge;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:I

.field public final i:LX/0Sh;

.field private final j:Ljava/util/concurrent/Executor;

.field public final k:Ljava/util/concurrent/Executor;

.field public final l:LX/2k1;

.field private final m:LX/2kT;

.field public final n:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final o:LX/1BA;

.field public final p:LX/0SG;

.field public final q:LX/03V;

.field private final r:LX/15j;

.field public final s:LX/2kI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kI",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field private final t:Ljava/lang/Object;

.field public final u:LX/2kK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kK",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public volatile v:Z

.field public volatile w:Z

.field private volatile x:Z

.field public volatile y:LX/2kM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kM",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field public z:LX/2kb;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLX/2js;LX/0QK;ILX/0Sh;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/2jz;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1BA;LX/0SG;LX/03V;LX/0ox;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/2js;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "LX/2js",
            "<TTEdge;>;",
            "LX/0QK",
            "<TTEdge;",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "LX/2jz;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "LX/0SG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ox;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 454536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454537
    new-instance v1, LX/2kH;

    invoke-direct {v1, p0}, LX/2kH;-><init>(LX/2jx;)V

    iput-object v1, p0, LX/2jx;->a:LX/2kH;

    .line 454538
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, LX/2jx;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 454539
    new-instance v1, LX/2kI;

    invoke-direct {v1}, LX/2kI;-><init>()V

    iput-object v1, p0, LX/2jx;->s:LX/2kI;

    .line 454540
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, LX/2jx;->t:Ljava/lang/Object;

    .line 454541
    new-instance v1, LX/2kK;

    iget-object v2, p0, LX/2jx;->a:LX/2kH;

    invoke-direct {v1, v2}, LX/2kK;-><init>(LX/2kH;)V

    iput-object v1, p0, LX/2jx;->u:LX/2kK;

    .line 454542
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2jx;->x:Z

    .line 454543
    sget-object v1, LX/2kL;->a:LX/2kM;

    iput-object v1, p0, LX/2jx;->y:LX/2kM;

    .line 454544
    iput-object p1, p0, LX/2jx;->c:Ljava/lang/String;

    .line 454545
    invoke-static {p1}, LX/2kN;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/2jx;->d:Ljava/lang/String;

    .line 454546
    iput-wide p2, p0, LX/2jx;->e:J

    .line 454547
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2js;

    iput-object v1, p0, LX/2jx;->f:LX/2js;

    .line 454548
    iput-object p5, p0, LX/2jx;->g:LX/0QK;

    .line 454549
    iput p6, p0, LX/2jx;->h:I

    .line 454550
    iput-object p7, p0, LX/2jx;->i:LX/0Sh;

    .line 454551
    iput-object p8, p0, LX/2jx;->j:Ljava/util/concurrent/Executor;

    .line 454552
    iput-object p9, p0, LX/2jx;->k:Ljava/util/concurrent/Executor;

    .line 454553
    invoke-virtual/range {p10 .. p10}, LX/2jz;->a()LX/2k1;

    move-result-object v1

    iput-object v1, p0, LX/2jx;->l:LX/2k1;

    .line 454554
    iget-object v1, p0, LX/2jx;->l:LX/2k1;

    iget-object v2, p0, LX/2jx;->d:Ljava/lang/String;

    new-instance v3, LX/2kQ;

    invoke-direct {v3, p0}, LX/2kQ;-><init>(LX/2jx;)V

    invoke-interface {v1, v2, v3}, LX/2k1;->a(Ljava/lang/String;LX/2kR;)LX/2kT;

    move-result-object v1

    iput-object v1, p0, LX/2jx;->m:LX/2kT;

    .line 454555
    move-object/from16 v0, p11

    iput-object v0, p0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 454556
    move-object/from16 v0, p12

    iput-object v0, p0, LX/2jx;->o:LX/1BA;

    .line 454557
    move-object/from16 v0, p13

    iput-object v0, p0, LX/2jx;->p:LX/0SG;

    .line 454558
    move-object/from16 v0, p14

    iput-object v0, p0, LX/2jx;->q:LX/03V;

    .line 454559
    sget-object v1, LX/2kU;->a:LX/0Tn;

    iget-object v2, p0, LX/2jx;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    move-object/from16 v0, p15

    invoke-virtual {v0, v1}, LX/0ox;->a(LX/0Tn;)LX/15j;

    move-result-object v1

    iput-object v1, p0, LX/2jx;->r:LX/15j;

    .line 454560
    return-void
.end method

.method private a(LX/5Mb;ZLjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;J)LX/2nf;
    .locals 9
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5Mb",
            "<TTEdge;>;Z",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/cursor/edgestore/PageInfo;",
            "J)",
            "LX/2nf;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 454406
    iget-object v0, p1, LX/5Mb;->b:LX/0Px;

    move-object v0, v0

    .line 454407
    invoke-static {v0}, LX/9JA;->b(LX/0Px;)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 454408
    if-nez v2, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 454409
    invoke-static {v0}, LX/9JA;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 454410
    invoke-static {v0}, LX/9JA;->b(LX/0Px;)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 454411
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 454412
    :cond_0
    new-instance v3, LX/95c;

    iget-object v0, p0, LX/2jx;->f:LX/2js;

    iget-object v1, p0, LX/2jx;->g:LX/0QK;

    invoke-direct {v3, p1, p3, v0, v1}, LX/95c;-><init>(LX/5Mb;Ljava/lang/String;LX/2js;LX/0QK;)V

    .line 454413
    invoke-virtual {v3}, LX/95c;->b()V

    .line 454414
    iget-object v0, p0, LX/2jx;->l:LX/2k1;

    iget-object v1, p0, LX/2jx;->z:LX/2kb;

    move-wide v4, p5

    move-object v6, p4

    move v7, p2

    invoke-interface/range {v0 .. v7}, LX/2k1;->a(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)LX/2nf;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/2nf;Landroid/os/Bundle;)Landroid/util/SparseArray;
    .locals 8
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2nf;",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/util/SparseArray",
            "<TTEdge;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 454415
    const-string v2, "INSERTED_ROW_IDS"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v4

    .line 454416
    if-eqz v4, :cond_0

    array-length v2, v4

    if-nez v2, :cond_1

    .line 454417
    :cond_0
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 454418
    :goto_0
    return-object v0

    .line 454419
    :cond_1
    invoke-static {v4}, Ljava/util/Arrays;->sort([J)V

    .line 454420
    new-instance v2, Landroid/util/SparseArray;

    array-length v3, v4

    invoke-direct {v2, v3}, Landroid/util/SparseArray;-><init>(I)V

    .line 454421
    invoke-interface {p0}, LX/2nf;->moveToLast()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 454422
    :cond_2
    invoke-interface {p0}, LX/2nf;->a()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v3

    .line 454423
    if-ltz v3, :cond_5

    array-length v5, v4

    if-ge v3, v5, :cond_5

    move v3, v0

    .line 454424
    :goto_1
    if-eqz v3, :cond_3

    .line 454425
    invoke-interface {p0}, LX/2nf;->getPosition()I

    move-result v3

    invoke-interface {p0}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 454426
    :cond_3
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    array-length v5, v4

    if-eq v3, v5, :cond_4

    .line 454427
    invoke-interface {p0}, LX/2nf;->moveToPrevious()Z

    move-result v3

    if-nez v3, :cond_2

    .line 454428
    :cond_4
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    array-length v5, v4

    if-ne v3, v5, :cond_6

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Found "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " inserted but expected "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    move-object v0, v2

    .line 454429
    goto :goto_0

    :cond_5
    move v3, v1

    .line 454430
    goto :goto_1

    :cond_6
    move v0, v1

    .line 454431
    goto :goto_2
.end method

.method public static a(Ljava/lang/String;[JLX/2nf;Z)Landroid/util/SparseArray;
    .locals 8
    .param p1    # [J
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "[J",
            "LX/2nf;",
            "Z)",
            "Landroid/util/SparseArray",
            "<TTEdge;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 454432
    if-eqz p1, :cond_3

    array-length v0, p1

    if-lez v0, :cond_3

    .line 454433
    invoke-static {p1}, Ljava/util/Arrays;->sort([J)V

    .line 454434
    invoke-static {p1}, Ljava/util/Arrays;->toString([J)Ljava/lang/String;

    .line 454435
    invoke-interface {p2}, LX/2nf;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 454436
    new-instance v4, Landroid/util/SparseArray;

    array-length v0, p1

    invoke-direct {v4, v0}, Landroid/util/SparseArray;-><init>(I)V

    move v0, v1

    .line 454437
    :cond_0
    invoke-interface {p2}, LX/2nf;->a()J

    move-result-wide v6

    invoke-static {p1, v6, v7}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v3

    .line 454438
    if-ltz v3, :cond_4

    array-length v5, p1

    if-ge v3, v5, :cond_4

    const/4 v3, 0x1

    .line 454439
    :goto_0
    if-eqz v3, :cond_1

    .line 454440
    add-int/lit8 v3, v0, 0x1

    .line 454441
    invoke-interface {p2}, LX/2nf;->getPosition()I

    move-result v5

    .line 454442
    if-eqz p3, :cond_5

    .line 454443
    invoke-interface {p2}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 454444
    :goto_1
    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    move v0, v3

    .line 454445
    :cond_1
    array-length v3, p1

    if-ge v0, v3, :cond_2

    invoke-interface {p2}, LX/2nf;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move-object v2, v4

    .line 454446
    :cond_3
    return-object v2

    :cond_4
    move v3, v1

    .line 454447
    goto :goto_0

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method

.method public static a$redex0(LX/2jx;LX/2kM;)LX/2kM;
    .locals 3
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<TTEdge;>;)",
            "LX/2kM",
            "<TTEdge;>;"
        }
    .end annotation

    .prologue
    .line 454448
    iget-object v0, p0, LX/2jx;->i:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 454449
    instance-of v0, p1, LX/2nh;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 454450
    check-cast v0, LX/2nh;

    invoke-virtual {v0}, LX/2nh;->m()V

    .line 454451
    :cond_0
    iget-object v1, p0, LX/2jx;->y:LX/2kM;

    .line 454452
    iput-object p1, p0, LX/2jx;->y:LX/2kM;

    .line 454453
    instance-of v0, v1, LX/2nh;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 454454
    check-cast v0, LX/2nh;

    invoke-virtual {v0}, LX/2nh;->m()V

    .line 454455
    instance-of v0, p1, LX/2nh;

    if-nez v0, :cond_1

    .line 454456
    const-string v0, "ConnectionController"

    const-string v2, "Swapped out last DB cursor for session"

    invoke-static {v0, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 454457
    :cond_1
    return-object v1
.end method

.method public static a$redex0(LX/2jx;LX/2nf;Ljava/util/ArrayList;)LX/2nh;
    .locals 9
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nf;",
            "Ljava/util/ArrayList",
            "<TTEdge;>;)",
            "LX/2nh",
            "<TTEdge;>;"
        }
    .end annotation

    .prologue
    .line 454458
    iget v0, p0, LX/2jx;->h:I

    if-lez v0, :cond_3

    .line 454459
    if-nez p2, :cond_0

    .line 454460
    iget-object v0, p0, LX/2jx;->y:LX/2kM;

    instance-of v0, v0, LX/2ng;

    if-eqz v0, :cond_2

    .line 454461
    iget-object v0, p0, LX/2jx;->y:LX/2kM;

    check-cast v0, LX/2ng;

    .line 454462
    new-instance v1, Ljava/util/ArrayList;

    iget-object p2, v0, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-direct {v1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object p2, v1

    .line 454463
    :cond_0
    :goto_0
    new-instance v0, LX/2ng;

    iget-object v1, p0, LX/2jx;->i:LX/0Sh;

    invoke-direct {v0, p1, p2, v1}, LX/2ng;-><init>(LX/2nf;Ljava/util/ArrayList;LX/0Sh;)V

    .line 454464
    :goto_1
    iget-object v2, v0, LX/2nh;->a:LX/2nf;

    instance-of v2, v2, LX/2nd;

    if-nez v2, :cond_4

    .line 454465
    :cond_1
    :goto_2
    return-object v0

    .line 454466
    :cond_2
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 454467
    :cond_3
    new-instance v0, LX/2nh;

    iget-object v1, p0, LX/2jx;->i:LX/0Sh;

    invoke-direct {v0, p1, v1}, LX/2nh;-><init>(LX/2nf;LX/0Sh;)V

    goto :goto_1

    .line 454468
    :cond_4
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    mul-double/2addr v2, v4

    double-to-int v2, v2

    .line 454469
    iget-object v3, v0, LX/2nh;->a:LX/2nf;

    invoke-interface {v3, v2}, LX/2nf;->moveToPosition(I)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_5
    move v3, v2

    .line 454470
    :try_start_0
    iget-object v2, v0, LX/2nh;->a:LX/2nf;

    check-cast v2, LX/2nd;

    invoke-virtual {v2}, LX/2nd;->e()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 454471
    :goto_3
    iget-object v4, v0, LX/2nh;->a:LX/2nf;

    add-int/lit8 v2, v3, 0xa

    invoke-interface {v4, v2}, LX/2nf;->moveToPosition(I)Z

    move-result v3

    if-nez v3, :cond_5

    goto :goto_2

    .line 454472
    :catch_0
    move-exception v2

    .line 454473
    const-class v4, LX/2nh;

    const-string v5, "Model file at row %d is not valid"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v2, v5, v6}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method

.method public static a$redex0(LX/2jx;LX/2ng;I)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2ng",
            "<TTEdge;>;I)",
            "Ljava/util/ArrayList",
            "<TTEdge;>;"
        }
    .end annotation

    .prologue
    .line 454474
    iget-object v1, p0, LX/2jx;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 454475
    :try_start_0
    iget-object v0, p1, LX/2ng;->e:LX/2nn;

    move-object v0, v0

    .line 454476
    invoke-virtual {v0, p2}, LX/2nn;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 454477
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a$redex0(LX/2jx;I)V
    .locals 3

    .prologue
    .line 454478
    iget-object v0, p0, LX/2jx;->s:LX/2kI;

    iget-object v1, p0, LX/2jx;->y:LX/2kM;

    invoke-virtual {v0, v1}, LX/2kI;->a(LX/2kM;)V

    .line 454479
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2jx;->x:Z

    .line 454480
    iget-object v0, p0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x920002

    const/16 v2, 0xbf

    invoke-interface {v0, v1, p1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 454481
    return-void
.end method

.method public static a$redex0(LX/2jx;LX/2nj;LX/3DP;IZ)V
    .locals 7

    .prologue
    .line 454482
    iget-object v0, p0, LX/2jx;->y:LX/2kM;

    instance-of v0, v0, LX/2ng;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 454483
    new-instance v0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;

    iget-object v2, p0, LX/2jx;->s:LX/2kI;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$FillNextBatchWorkerRunnable;-><init>(LX/2jx;LX/2kI;LX/2nj;LX/3DP;IZ)V

    invoke-direct {p0, v0}, LX/2jx;->b(Ljava/lang/Runnable;)V

    .line 454484
    return-void
.end method

.method public static a$redex0(LX/2jx;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 454485
    iget-object v0, p0, LX/2jx;->k:Ljava/util/concurrent/Executor;

    const v1, -0x42f989cb

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 454486
    return-void
.end method

.method public static b(LX/2kM;Landroid/os/Bundle;LX/2nn;)Ljava/util/ArrayList;
    .locals 5
    .param p2    # LX/2nn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2kM;",
            "Landroid/os/Bundle;",
            "Lcom/facebook/controller/connectioncontroller/store/diskstore/ConnectionCache",
            "<TTEdge;>;)",
            "Ljava/util/ArrayList",
            "<",
            "LX/3CY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 454487
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 454488
    const-string v0, "DELETED_ROW_IDS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    .line 454489
    instance-of v2, p0, LX/2nh;

    if-eqz v2, :cond_1

    .line 454490
    check-cast p0, LX/2nh;

    invoke-virtual {p0}, LX/2nh;->e()LX/2nf;

    move-result-object v2

    .line 454491
    const-string v3, "Delete"

    const/4 v4, 0x0

    invoke-static {v3, v0, v2, v4}, LX/2jx;->a(Ljava/lang/String;[JLX/2nf;Z)Landroid/util/SparseArray;

    move-result-object v2

    .line 454492
    if-eqz v2, :cond_1

    .line 454493
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 454494
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 454495
    if-eqz p2, :cond_2

    .line 454496
    const/4 p0, 0x1

    const/4 p1, 0x0

    .line 454497
    iget-boolean v4, p2, LX/2nn;->b:Z

    if-nez v4, :cond_3

    move v4, p0

    :goto_1
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 454498
    iget-object v4, p2, LX/2nn;->a:LX/2ng;

    iget-object v4, v4, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 454499
    iget-object v4, p2, LX/2nn;->a:LX/2ng;

    iget-object v4, v4, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 454500
    :goto_2
    move v4, p0

    .line 454501
    :goto_3
    move v3, v4

    .line 454502
    if-eqz v3, :cond_0

    .line 454503
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, LX/3CY;->b(II)LX/3CY;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454504
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 454505
    :cond_1
    return-object v1

    :cond_2
    const/4 v4, 0x1

    goto :goto_3

    :cond_3
    move v4, p1

    .line 454506
    goto :goto_1

    :cond_4
    move p0, p1

    .line 454507
    goto :goto_2
.end method

.method public static b(LX/2nh;Landroid/os/Bundle;LX/2nn;Landroid/util/SparseArray;)Ljava/util/ArrayList;
    .locals 6
    .param p2    # LX/2nn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2nh;",
            "Landroid/os/Bundle;",
            "Lcom/facebook/controller/connectioncontroller/store/diskstore/ConnectionCache",
            "<TTEdge;>;",
            "Landroid/util/SparseArray",
            "<TTEdge;>;)",
            "Ljava/util/ArrayList",
            "<",
            "LX/3CY;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 454508
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 454509
    if-nez p3, :cond_0

    .line 454510
    const-string v0, "CHANGED_ROW_IDS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    .line 454511
    const-string v2, "Update"

    invoke-virtual {p0}, LX/2nh;->e()LX/2nf;

    move-result-object v3

    invoke-static {v2, v0, v3, v4}, LX/2jx;->a(Ljava/lang/String;[JLX/2nf;Z)Landroid/util/SparseArray;

    move-result-object p3

    .line 454512
    :cond_0
    if-eqz p3, :cond_2

    .line 454513
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p3}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 454514
    invoke-virtual {p3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 454515
    invoke-virtual {p3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    .line 454516
    if-eqz p2, :cond_3

    .line 454517
    const/4 p0, 0x1

    const/4 p1, 0x0

    .line 454518
    iget-boolean v5, p2, LX/2nn;->b:Z

    if-nez v5, :cond_4

    move v5, p0

    :goto_1
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 454519
    if-eqz v3, :cond_5

    move v5, p0

    :goto_2
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 454520
    iget-object v5, p2, LX/2nn;->a:LX/2ng;

    iget-object v5, v5, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_6

    .line 454521
    iget-object v5, p2, LX/2nn;->a:LX/2ng;

    iget-object v5, v5, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v5, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 454522
    invoke-virtual {p2}, LX/2nn;->e()V

    .line 454523
    :goto_3
    move v5, p0

    .line 454524
    :goto_4
    move v3, v5

    .line 454525
    if-eqz v3, :cond_1

    .line 454526
    invoke-static {v2, v2, v4}, LX/3CY;->a(III)LX/3CY;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454527
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 454528
    :cond_2
    return-object v1

    :cond_3
    const/4 v5, 0x1

    goto :goto_4

    :cond_4
    move v5, p1

    .line 454529
    goto :goto_1

    :cond_5
    move v5, p1

    .line 454530
    goto :goto_2

    :cond_6
    move p0, p1

    .line 454531
    goto :goto_3
.end method

.method private b(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 454532
    iget-object v0, p0, LX/2jx;->j:Ljava/util/concurrent/Executor;

    const v1, -0x120d97e4

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 454533
    return-void
.end method

.method private b(Z)V
    .locals 0

    .prologue
    .line 454534
    iput-boolean p1, p0, LX/2jx;->w:Z

    .line 454535
    return-void
.end method

.method public static b(LX/2kM;LX/2nh;Landroid/os/Bundle;LX/03V;LX/1BA;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2kM",
            "<TTEdge;>;",
            "LX/2nh",
            "<TTEdge;>;",
            "Landroid/os/Bundle;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 454388
    invoke-virtual {p1}, LX/2nh;->k()I

    move-result v2

    .line 454389
    instance-of v0, p0, LX/2nh;

    if-eqz v0, :cond_1

    .line 454390
    const-string v0, "SESSION_VERSION"

    const/4 v3, -0x1

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 454391
    check-cast p0, LX/2nh;

    invoke-virtual {p0}, LX/2nh;->k()I

    move-result v4

    .line 454392
    add-int/lit8 v0, v4, 0x1

    if-eq v0, v2, :cond_1

    .line 454393
    const/16 v0, 0xaf

    .line 454394
    const-string v5, "ConnectionController"

    const-string v6, "Session version error during update, new=%d, change=%d, old=%d"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v6, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v5, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 454395
    :goto_0
    const v2, 0x920009

    invoke-virtual {p4, v2, v0}, LX/1BA;->a(IS)V

    .line 454396
    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static b$redex0(LX/2jx;LX/2kM;)V
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 454397
    iget-object v0, p0, LX/2jx;->i:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 454398
    instance-of v0, p1, LX/2nh;

    if-eqz v0, :cond_0

    .line 454399
    check-cast p1, LX/2nh;

    invoke-virtual {p1}, LX/2nh;->close()V

    .line 454400
    :cond_0
    return-void
.end method

.method public static declared-synchronized g(LX/2jx;)V
    .locals 1

    .prologue
    .line 454401
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2jx;->z:LX/2kb;

    if-eqz v0, :cond_0

    .line 454402
    iget-object v0, p0, LX/2jx;->z:LX/2kb;

    invoke-interface {v0}, LX/2kb;->close()V

    .line 454403
    const/4 v0, 0x0

    iput-object v0, p0, LX/2jx;->z:LX/2kb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454404
    :cond_0
    monitor-exit p0

    return-void

    .line 454405
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static j(LX/2jx;)V
    .locals 1

    .prologue
    .line 454336
    invoke-static {p0}, LX/2jx;->k(LX/2jx;)LX/2nn;

    move-result-object v0

    .line 454337
    if-eqz v0, :cond_0

    .line 454338
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/2nn;->b:Z

    .line 454339
    :cond_0
    return-void
.end method

.method public static k(LX/2jx;)LX/2nn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/controller/connectioncontroller/store/diskstore/ConnectionCache",
            "<TTEdge;>;"
        }
    .end annotation

    .prologue
    .line 454332
    iget-object v0, p0, LX/2jx;->y:LX/2kM;

    instance-of v0, v0, LX/2ng;

    if-eqz v0, :cond_0

    .line 454333
    iget-object v0, p0, LX/2jx;->y:LX/2kM;

    check-cast v0, LX/2ng;

    .line 454334
    iget-object p0, v0, LX/2ng;->e:LX/2nn;

    move-object v0, p0

    .line 454335
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(LX/2jx;)Z
    .locals 1

    .prologue
    .line 454331
    iget-boolean v0, p0, LX/2jx;->w:Z

    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 454330
    iget-object v0, p0, LX/2jx;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0Rl;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<TTEdge;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 454297
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 454298
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 454299
    new-instance v0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$2;-><init>(LX/2jx;LX/0Rl;Ljava/lang/String;)V

    invoke-direct {p0, v0}, LX/2jx;->b(Ljava/lang/Runnable;)V

    .line 454300
    return-void
.end method

.method public final a(LX/2kJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kJ",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 454295
    iget-object v0, p0, LX/2jx;->s:LX/2kI;

    invoke-virtual {v0, p1}, LX/2kI;->a(LX/2kJ;)V

    .line 454296
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;LX/5Mb;JLX/3Cb;)V
    .locals 22
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "LX/5Mb",
            "<TTEdge;>;J",
            "LX/3Cb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 454302
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2jx;->i:LX/0Sh;

    invoke-virtual {v4}, LX/0Sh;->b()V

    .line 454303
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/2jx;->x:Z

    if-nez v4, :cond_0

    .line 454304
    :goto_0
    return-void

    .line 454305
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2jx;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v15

    .line 454306
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x920001

    invoke-interface {v4, v5, v15}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 454307
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x920001

    const-string v6, "DiskConnectionStore"

    invoke-interface {v4, v5, v15, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 454308
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x920001

    move-object/from16 v0, p0

    iget-object v6, v0, LX/2jx;->d:Ljava/lang/String;

    invoke-interface {v4, v5, v15, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 454309
    invoke-virtual/range {p1 .. p1}, LX/2nj;->b()LX/2nk;

    move-result-object v4

    .line 454310
    sget-object v5, LX/2nk;->INITIAL:LX/2nk;

    if-eq v4, v5, :cond_1

    invoke-static/range {p0 .. p0}, LX/2jx;->m(LX/2jx;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v7, 0x1

    .line 454311
    :goto_1
    move-object/from16 v0, p1

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2}, LX/95e;->a(LX/2nj;J)Ljava/lang/String;

    move-result-object v8

    .line 454312
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2, v8}, LX/95e;->a(LX/2nj;LX/3DP;LX/5Mb;Ljava/lang/String;)Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v9

    .line 454313
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x920001

    const/16 v6, 0x17

    invoke-interface {v4, v5, v15, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 454314
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/2jx;->e:J

    invoke-static {v4, v5}, LX/5Ma;->a(J)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/2jx;->e:J

    const-wide/16 v10, 0x3e8

    mul-long/2addr v10, v4

    :goto_2
    move-object/from16 v5, p0

    move-object/from16 v6, p3

    .line 454315
    invoke-direct/range {v5 .. v11}, LX/2jx;->a(LX/5Mb;ZLjava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;J)LX/2nf;

    move-result-object v14

    .line 454316
    if-nez v14, :cond_4

    .line 454317
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x920001

    const/4 v6, 0x3

    invoke-interface {v4, v5, v15, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 454318
    new-instance v4, LX/5Md;

    invoke-direct {v4}, LX/5Md;-><init>()V

    throw v4

    .line 454319
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    .line 454320
    :cond_3
    const-wide/32 v10, 0x19bfcc00

    goto :goto_2

    .line 454321
    :cond_4
    invoke-interface {v14}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 454322
    move-object/from16 v0, p0

    iget-object v5, v0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x920001

    const/16 v8, 0x1e

    invoke-interface {v5, v6, v15, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 454323
    invoke-static/range {p0 .. p0}, LX/2jx;->m(LX/2jx;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 454324
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, LX/2jx;->b(Z)V

    .line 454325
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x920001

    const/16 v8, 0x1a

    invoke-interface {v5, v6, v15, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 454326
    invoke-static {v14, v4}, LX/2jx;->a(LX/2nf;Landroid/os/Bundle;)Landroid/util/SparseArray;

    move-result-object v16

    .line 454327
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x920001

    const/16 v6, 0xbd

    invoke-interface {v4, v5, v15, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 454328
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2jx;->u:LX/2kK;

    new-instance v8, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/2jx;->s:LX/2kI;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2jx;->o:LX/1BA;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/2jx;->q:LX/03V;

    move-object/from16 v9, p0

    move-object/from16 v17, p1

    move-object/from16 v18, p2

    move-object/from16 v19, p6

    move/from16 v20, v7

    invoke-direct/range {v8 .. v20}, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$AddPageRunnable;-><init>(LX/2jx;LX/2kI;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1BA;LX/03V;LX/2nf;ILandroid/util/SparseArray;LX/2nj;LX/3DP;LX/3Cb;Z)V

    invoke-virtual {v4, v14, v8}, LX/2kK;->a(LX/2nf;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final a(LX/5Mb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5Mb",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 454301
    return-void
.end method

.method public final a(Ljava/lang/String;LX/3Cf;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/3Cf",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 454340
    iget-object v0, p0, LX/2jx;->i:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 454341
    :try_start_0
    iget-object v0, p0, LX/2jx;->l:LX/2k1;

    iget-object v1, p0, LX/2jx;->z:LX/2kb;

    invoke-interface {v0, v1, p1}, LX/2k1;->a(LX/2kb;Ljava/lang/String;)LX/2nf;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    const/4 v1, 0x0

    .line 454342
    :try_start_1
    invoke-interface {v2}, LX/2nf;->moveToFirst()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_2

    .line 454343
    :cond_0
    :try_start_2
    invoke-interface {v2}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 454344
    if-eqz v0, :cond_1

    invoke-interface {p2, v0}, LX/3Cf;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 454345
    invoke-interface {p2, v0}, LX/3Cf;->b(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 454346
    :cond_1
    :goto_0
    :try_start_3
    invoke-interface {v2}, LX/2nf;->moveToNext()Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 454347
    :cond_2
    if-eqz v2, :cond_3

    :try_start_4
    invoke-interface {v2}, LX/2nf;->close()V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_2

    .line 454348
    :cond_3
    :goto_1
    return-void

    .line 454349
    :catch_0
    move-exception v0

    .line 454350
    :try_start_5
    const-string v3, "ConnectionController"

    const-string v4, "Error visiting edges with tag hint"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v0, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    .line 454351
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 454352
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_2
    if-eqz v2, :cond_4

    if-eqz v1, :cond_5

    :try_start_7
    invoke-interface {v2}, LX/2nf;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_2

    :cond_4
    :goto_3
    :try_start_8
    throw v0
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_2

    :catch_2
    move-exception v0

    .line 454353
    const-string v1, "ConnectionController"

    const-string v2, "Error in visitEdgesWithTagHint"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 454354
    :catch_3
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-interface {v2}, LX/2nf;->close()V
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_2

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public final a(LX/2nj;LX/3DP;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 454355
    iget-object v1, p1, LX/2nj;->b:Ljava/lang/String;

    move-object v1, v1

    .line 454356
    invoke-static {v1}, LX/3DQ;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 454357
    const/4 v0, 0x0

    .line 454358
    :cond_0
    :goto_0
    return v0

    .line 454359
    :cond_1
    iget-boolean v1, p0, LX/2jx;->v:Z

    move v1, v1

    .line 454360
    if-nez v1, :cond_0

    .line 454361
    iput-boolean v0, p0, LX/2jx;->v:Z

    .line 454362
    invoke-static {p0, p1, p2, p3, v0}, LX/2jx;->a$redex0(LX/2jx;LX/2nj;LX/3DP;IZ)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTEdge;TTEdge;)Z"
        }
    .end annotation

    .prologue
    .line 454363
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "replaceEdge not implemented yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 9

    .prologue
    .line 454364
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2jx;->z:LX/2kb;

    if-nez v0, :cond_0

    .line 454365
    iget-object v0, p0, LX/2jx;->l:LX/2k1;

    iget-object v1, p0, LX/2jx;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/2k1;->a(Ljava/lang/String;)LX/2kb;

    move-result-object v0

    iput-object v0, p0, LX/2jx;->z:LX/2kb;

    .line 454366
    :cond_0
    iget-boolean v0, p0, LX/2jx;->x:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 454367
    :goto_0
    monitor-exit p0

    return-void

    .line 454368
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2jx;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v8

    .line 454369
    iget-object v0, p0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x920002

    invoke-interface {v0, v1, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 454370
    iget-object v0, p0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x920002

    const-string v2, "DiskConnectionStore"

    invoke-interface {v0, v1, v8, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 454371
    iget-object v0, p0, LX/2jx;->y:LX/2kM;

    sget-object v1, LX/2kL;->a:LX/2kM;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 454372
    new-instance v0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;

    iget-object v2, p0, LX/2jx;->s:LX/2kI;

    iget-object v3, p0, LX/2jx;->n:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v4, p0, LX/2jx;->r:LX/15j;

    iget-object v5, p0, LX/2jx;->q:LX/03V;

    iget-object v6, p0, LX/2jx;->l:LX/2k1;

    iget-object v7, p0, LX/2jx;->u:LX/2kK;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$InitializeRunnable;-><init>(LX/2jx;LX/2kI;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/15j;LX/03V;LX/2k1;LX/2kK;I)V

    invoke-direct {p0, v0}, LX/2jx;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 454373
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 454374
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(LX/2kJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kJ",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 454375
    iget-object v0, p0, LX/2jx;->s:LX/2kI;

    invoke-virtual {v0, p1}, LX/2kI;->b(LX/2kJ;)V

    .line 454376
    return-void
.end method

.method public final declared-synchronized c()V
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 454377
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2jx;->i:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 454378
    iget-object v0, p0, LX/2jx;->m:LX/2kT;

    invoke-interface {v0}, LX/2kT;->close()V

    .line 454379
    invoke-static {p0}, LX/2jx;->k(LX/2jx;)LX/2nn;

    .line 454380
    iget-object v0, p0, LX/2jx;->y:LX/2kM;

    invoke-static {p0, v0}, LX/2jx;->b$redex0(LX/2jx;LX/2kM;)V

    .line 454381
    sget-object v0, LX/2kL;->a:LX/2kM;

    iput-object v0, p0, LX/2jx;->y:LX/2kM;

    .line 454382
    invoke-static {p0}, LX/2jx;->g(LX/2jx;)V

    .line 454383
    iget-object v0, p0, LX/2jx;->u:LX/2kK;

    invoke-virtual {v0}, LX/2kK;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454384
    monitor-exit p0

    return-void

    .line 454385
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 454386
    new-instance v0, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$3;

    invoke-direct {v0, p0}, Lcom/facebook/controller/connectioncontroller/store/diskstore/DiskConnectionStore$3;-><init>(LX/2jx;)V

    invoke-direct {p0, v0}, LX/2jx;->b(Ljava/lang/Runnable;)V

    .line 454387
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 454329
    const/4 v0, 0x1

    return v0
.end method
