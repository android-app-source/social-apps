.class public LX/2fl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 447425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447426
    return-void
.end method

.method public static final a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLSavedState;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLSavedState;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447418
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 447419
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 447420
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, -0x3625f733

    if-ne v4, v5, :cond_0

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 447421
    invoke-static {v0, p1}, LX/2fl;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 447422
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 447423
    :cond_0
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 447424
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Px;Lcom/facebook/graphql/model/GraphQLEvent;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLEvent;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447405
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 447406
    :cond_0
    :goto_0
    return-object p0

    .line 447407
    :cond_1
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 447408
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 447409
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 447410
    invoke-static {v0}, LX/4Ys;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/4Ys;

    move-result-object v0

    .line 447411
    iput-object p1, v0, LX/4Ys;->K:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 447412
    move-object v0, v0

    .line 447413
    invoke-virtual {v0}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 447414
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 447415
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 447416
    :cond_2
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 447417
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    goto :goto_0
.end method

.method public static final a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447378
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 447379
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 447380
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 447381
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 447382
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 447383
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v4

    .line 447384
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    :cond_0
    move v6, v4

    .line 447385
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v()Lcom/facebook/graphql/model/GraphQLCoupon;

    move-result-object v7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->v()Lcom/facebook/graphql/model/GraphQLCoupon;

    move-result-object p0

    invoke-static {v7, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    .line 447386
    if-eqz v3, :cond_5

    if-eqz v6, :cond_5

    if-eqz v7, :cond_5

    :goto_3
    move v3, v4

    .line 447387
    if-eqz v3, :cond_1

    move-object v0, p1

    :cond_1
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 447388
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_3
    move v3, v5

    .line 447389
    goto :goto_1

    :cond_4
    move v6, v5

    .line 447390
    goto :goto_2

    :cond_5
    move v4, v5

    .line 447391
    goto :goto_3
.end method

.method public static a(LX/0QB;)LX/2fl;
    .locals 1

    .prologue
    .line 447402
    new-instance v0, LX/2fl;

    invoke-direct {v0}, LX/2fl;-><init>()V

    .line 447403
    move-object v0, v0

    .line 447404
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 2

    .prologue
    .line 447395
    invoke-static {p0}, LX/4Ys;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/4Ys;

    move-result-object v0

    .line 447396
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    .line 447397
    iput-object p1, v1, LX/4XR;->pI:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 447398
    move-object v1, v1

    .line 447399
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 447400
    iput-object v1, v0, LX/4Ys;->af:Lcom/facebook/graphql/model/GraphQLNode;

    .line 447401
    invoke-virtual {v0}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z
    .locals 2

    .prologue
    .line 447392
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x41c8fa46

    if-ne v0, v1, :cond_0

    .line 447393
    const/4 v0, 0x1

    .line 447394
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
