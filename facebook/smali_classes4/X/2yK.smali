.class public LX/2yK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final d:LX/0ih;

.field public static final e:LX/0ih;

.field public static final f:LX/0ih;


# instance fields
.field public a:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2yL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 481646
    sget-object v0, LX/0ig;->y:LX/0ih;

    sput-object v0, LX/2yK;->d:LX/0ih;

    .line 481647
    sget-object v0, LX/0ig;->z:LX/0ih;

    sput-object v0, LX/2yK;->e:LX/0ih;

    .line 481648
    sget-object v0, LX/0ig;->A:LX/0ih;

    sput-object v0, LX/2yK;->f:LX/0ih;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 481619
    return-void
.end method

.method public static a(LX/0QB;)LX/2yK;
    .locals 1

    .prologue
    .line 481645
    invoke-static {p0}, LX/2yK;->b(LX/0QB;)LX/2yK;

    move-result-object v0

    return-object v0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 481634
    packed-switch p0, :pswitch_data_0

    .line 481635
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported funnel action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 481636
    :pswitch_1
    const-string v0, "page_swipe"

    .line 481637
    :goto_0
    return-object v0

    .line 481638
    :pswitch_2
    const-string v0, "page_swipe_end"

    goto :goto_0

    .line 481639
    :pswitch_3
    const-string v0, "see_all"

    goto :goto_0

    .line 481640
    :pswitch_4
    const-string v0, "enter_channel"

    goto :goto_0

    .line 481641
    :pswitch_5
    const-string v0, "share_article"

    goto :goto_0

    .line 481642
    :pswitch_6
    const-string v0, "save_article"

    goto :goto_0

    .line 481643
    :pswitch_7
    const-string v0, "enter_article"

    goto :goto_0

    .line 481644
    :pswitch_8
    const-string v0, "like_or_follow_click"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(LX/2yK;Lcom/facebook/graphql/model/GraphQLStorySet;)S
    .locals 2

    .prologue
    .line 481633
    iget-object v0, p0, LX/2yK;->c:LX/2yL;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2yL;->a(Ljava/lang/String;)S

    move-result v0

    return v0
.end method

.method public static a(LX/2yK;LX/0ih;SLjava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ih;",
            "S",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 481630
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 481631
    iget-object v2, p0, LX/2yK;->a:LX/0if;

    int-to-long v4, p2

    invoke-virtual {v2, p1, v4, v5, v0}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    goto :goto_0

    .line 481632
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/2yK;
    .locals 4

    .prologue
    .line 481622
    new-instance v3, LX/2yK;

    invoke-direct {v3}, LX/2yK;-><init>()V

    .line 481623
    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    invoke-static {p0}, LX/1fv;->a(LX/0QB;)LX/1fv;

    move-result-object v1

    check-cast v1, LX/1fv;

    .line 481624
    new-instance v2, LX/2yL;

    invoke-direct {v2}, LX/2yL;-><init>()V

    .line 481625
    move-object v2, v2

    .line 481626
    move-object v2, v2

    .line 481627
    check-cast v2, LX/2yL;

    .line 481628
    iput-object v0, v3, LX/2yK;->a:LX/0if;

    iput-object v1, v3, LX/2yK;->b:LX/1fv;

    iput-object v2, v3, LX/2yK;->c:LX/2yL;

    .line 481629
    return-object v3
.end method


# virtual methods
.method public final a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;I)V
    .locals 4

    .prologue
    .line 481620
    iget-object v0, p0, LX/2yK;->a:LX/0if;

    invoke-static {p0, p2}, LX/2yK;->a(LX/2yK;Lcom/facebook/graphql/model/GraphQLStorySet;)S

    move-result v1

    int-to-long v2, v1

    invoke-static {p3}, LX/2yK;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v2, v3, v1}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 481621
    return-void
.end method

.method public final a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 481616
    iget-object v0, p0, LX/2yK;->a:LX/0if;

    invoke-static {p0, p2}, LX/2yK;->a(LX/2yK;Lcom/facebook/graphql/model/GraphQLStorySet;)S

    move-result v1

    int-to-long v2, v1

    invoke-static {p3}, LX/2yK;->a(I)Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 481617
    return-void
.end method
