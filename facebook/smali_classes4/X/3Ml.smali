.class public abstract LX/3Ml;
.super LX/3Mm;
.source ""

# interfaces
.implements LX/3Mi;


# instance fields
.field public a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/3Md;

.field private c:LX/3LI;

.field public d:Z


# direct methods
.method public constructor <init>(LX/0Zr;)V
    .locals 1

    .prologue
    .line 555420
    invoke-direct {p0, p1}, LX/3Mm;-><init>(LX/0Zr;)V

    .line 555421
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3Ml;->d:Z

    .line 555422
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 555414
    iget-object v0, p0, LX/3Ml;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 555415
    if-eqz p1, :cond_0

    .line 555416
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserIdentifier;

    .line 555417
    iget-object v3, p0, LX/3Ml;->a:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 555418
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 555419
    :cond_0
    return-void
.end method

.method public final a(LX/3LI;)V
    .locals 1

    .prologue
    .line 555411
    iput-object p1, p0, LX/3Ml;->c:LX/3LI;

    .line 555412
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/3Ml;->a:Ljava/util/Set;

    .line 555413
    return-void
.end method

.method public final a(LX/3Md;)V
    .locals 0

    .prologue
    .line 555423
    iput-object p1, p0, LX/3Ml;->b:LX/3Md;

    .line 555424
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;LX/39y;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 555397
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 555398
    iget-object v0, p2, LX/39y;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 555399
    :goto_0
    return-void

    .line 555400
    :cond_0
    iget-object v0, p2, LX/39y;->a:Ljava/lang/Object;

    check-cast v0, LX/3Og;

    .line 555401
    iget-object v1, p0, LX/3Ml;->c:LX/3LI;

    invoke-interface {v1, p1, v0}, LX/3LI;->a(Ljava/lang/CharSequence;LX/3Og;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 555402
    iput-boolean p1, p0, LX/3Ml;->d:Z

    .line 555403
    return-void
.end method

.method public a(Lcom/facebook/user/model/UserIdentifier;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 555404
    iget-object v0, p0, LX/3Ml;->a:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Ml;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 555405
    :cond_0
    :goto_0
    return v1

    .line 555406
    :cond_1
    iget-boolean v0, p0, LX/3Ml;->d:Z

    if-eqz v0, :cond_3

    .line 555407
    iget-object v0, p0, LX/3Ml;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserIdentifier;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v0, v2, :cond_2

    const/4 v0, 0x1

    .line 555408
    :goto_1
    iget-object v1, p0, LX/3Ml;->a:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v1, v0

    .line 555409
    goto :goto_0

    :cond_2
    move v0, v1

    .line 555410
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method
