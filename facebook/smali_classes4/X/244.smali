.class public LX/244;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/244;


# instance fields
.field public a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366500
    iput-object p1, p0, LX/244;->a:LX/0tX;

    .line 366501
    return-void
.end method

.method public static a(LX/0QB;)LX/244;
    .locals 4

    .prologue
    .line 366502
    sget-object v0, LX/244;->b:LX/244;

    if-nez v0, :cond_1

    .line 366503
    const-class v1, LX/244;

    monitor-enter v1

    .line 366504
    :try_start_0
    sget-object v0, LX/244;->b:LX/244;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 366505
    if-eqz v2, :cond_0

    .line 366506
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 366507
    new-instance p0, LX/244;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p0, v3}, LX/244;-><init>(LX/0tX;)V

    .line 366508
    move-object v0, p0

    .line 366509
    sput-object v0, LX/244;->b:LX/244;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366510
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 366511
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 366512
    :cond_1
    sget-object v0, LX/244;->b:LX/244;

    return-object v0

    .line 366513
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 366514
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
