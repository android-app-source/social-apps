.class public LX/2FB;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 386618
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 386619
    return-void
.end method

.method public static a(Lcom/facebook/compactdiskmodule/AndroidXAnalyticsLogger;)Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386621
    new-instance v0, Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    invoke-direct {v0, p0}, Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;-><init>(Lcom/facebook/compactdisk/XAnalyticsLogger;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/compactdisk/FileUtilsHolder;)Lcom/facebook/compactdisk/AttributeStoreHolder;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386622
    new-instance v0, Lcom/facebook/compactdisk/AttributeStoreHolder;

    invoke-direct {v0, p0}, Lcom/facebook/compactdisk/AttributeStoreHolder;-><init>(Lcom/facebook/compactdisk/FileUtilsHolder;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;)Lcom/facebook/compactdisk/DiskSizeCalculator;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386623
    new-instance v0, Lcom/facebook/compactdisk/DiskSizeCalculator;

    invoke-direct {v0, p0}, Lcom/facebook/compactdisk/DiskSizeCalculator;-><init>(Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/AttributeStoreHolder;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;
    .locals 6
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386624
    new-instance v0, Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;-><init>(Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/AttributeStoreHolder;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/compactdisk/FileUtilsHolder;)Lcom/facebook/compactdisk/ExperimentManager;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386625
    new-instance v0, Lcom/facebook/compactdisk/ExperimentManager;

    invoke-direct {v0, p0, p1}, Lcom/facebook/compactdisk/ExperimentManager;-><init>(Landroid/content/Context;Lcom/facebook/compactdisk/FileUtilsHolder;)V

    return-object v0
.end method

.method public static a()Lcom/facebook/compactdisk/FileUtilsHolder;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386626
    new-instance v0, Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-direct {v0}, Lcom/facebook/compactdisk/FileUtilsHolder;-><init>()V

    return-object v0
.end method

.method public static a(Lcom/facebook/compactdisk/TaskQueueFactoryHolder;)Lcom/facebook/compactdisk/LazyDispatcher;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386627
    new-instance v0, Lcom/facebook/compactdisk/LazyDispatcher;

    invoke-direct {v0, p0}, Lcom/facebook/compactdisk/LazyDispatcher;-><init>(Lcom/facebook/compactdisk/TaskQueueFactoryHolder;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/2O6;)Lcom/facebook/compactdisk/PrivacyGuard;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386620
    new-instance v0, Lcom/facebook/compactdisk/PrivacyGuard;

    invoke-direct {v0, p0, p1}, Lcom/facebook/compactdisk/PrivacyGuard;-><init>(Landroid/content/Context;LX/2O6;)V

    return-object v0
.end method

.method public static a(LX/0W9;)Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;
    .locals 3
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386617
    new-instance v0, Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;

    invoke-virtual {p0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Lcom/facebook/compactdisk/AttributeStoreHolder;Lcom/facebook/compactdisk/Configuration;Lcom/facebook/compactdisk/DiskSizeCalculator;Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;Lcom/facebook/compactdisk/ConfigurationOverrides;Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/LazyDispatcher;Lcom/facebook/compactdisk/ExperimentManager;Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;Lcom/facebook/compactdisk/TaskQueueFactoryHolder;Lcom/facebook/compactdisk/TrashCollector;Lcom/facebook/compactdisk/PrivacyGuard;LX/2O6;)Lcom/facebook/compactdisk/StoreManagerFactory;
    .locals 16
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386616
    new-instance v0, Lcom/facebook/compactdisk/StoreManagerFactory;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/facebook/compactdisk/StoreManagerFactory;-><init>(Landroid/content/Context;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Lcom/facebook/compactdisk/AttributeStoreHolder;Lcom/facebook/compactdisk/Configuration;Lcom/facebook/compactdisk/DiskSizeCalculator;Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;Lcom/facebook/compactdisk/ConfigurationOverrides;Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/LazyDispatcher;Lcom/facebook/compactdisk/ExperimentManager;Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;Lcom/facebook/compactdisk/TaskQueueFactoryHolder;Lcom/facebook/compactdisk/TrashCollector;Lcom/facebook/compactdisk/PrivacyGuard;LX/2O6;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/ScheduledExecutorService;)Lcom/facebook/compactdisk/TaskQueueFactoryHolder;
    .locals 1
    .param p0    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386610
    new-instance v0, Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    invoke-direct {v0, p0}, Lcom/facebook/compactdisk/TaskQueueFactoryHolder;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Lcom/facebook/compactdisk/TaskQueueFactoryHolder;)Lcom/facebook/compactdisk/TrashCollector;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386615
    new-instance v0, Lcom/facebook/compactdisk/TrashCollector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/compactdisk/TrashCollector;-><init>(Landroid/content/Context;Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Lcom/facebook/compactdisk/TaskQueueFactoryHolder;)V

    return-object v0
.end method

.method public static b()LX/2O6;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386614
    new-instance v0, LX/2O6;

    invoke-direct {v0}, LX/2O6;-><init>()V

    return-object v0
.end method

.method public static c()Lcom/facebook/compactdisk/Configuration;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386613
    new-instance v0, Lcom/facebook/compactdisk/Configuration;

    invoke-direct {v0}, Lcom/facebook/compactdisk/Configuration;-><init>()V

    return-object v0
.end method

.method public static d()Lcom/facebook/compactdisk/ConfigurationOverrides;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 386612
    new-instance v0, Lcom/facebook/compactdisk/ConfigurationOverrides;

    invoke-direct {v0}, Lcom/facebook/compactdisk/ConfigurationOverrides;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 386611
    return-void
.end method
