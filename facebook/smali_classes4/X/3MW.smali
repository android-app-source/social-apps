.class public LX/3MW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Uh;

.field public final b:LX/0SI;

.field private final c:LX/3MX;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/0SI;LX/3MX;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/sync/annotations/IsFetchUserCustomTagEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0SI;",
            "LX/3MX;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 555047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555048
    iput-object p1, p0, LX/3MW;->a:LX/0Uh;

    .line 555049
    iput-object p2, p0, LX/3MW;->b:LX/0SI;

    .line 555050
    iput-object p3, p0, LX/3MW;->c:LX/3MX;

    .line 555051
    iput-object p4, p0, LX/3MW;->d:LX/0Or;

    .line 555052
    return-void
.end method

.method public static a(LX/0QB;)LX/3MW;
    .locals 1

    .prologue
    .line 555053
    invoke-static {p0}, LX/3MW;->b(LX/0QB;)LX/3MW;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/3MW;)Z
    .locals 3

    .prologue
    .line 555046
    iget-object v0, p0, LX/3MW;->a:LX/0Uh;

    const/16 v1, 0x4e7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static b(LX/0QB;)LX/3MW;
    .locals 5

    .prologue
    .line 555044
    new-instance v3, LX/3MW;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v1

    check-cast v1, LX/0SI;

    invoke-static {p0}, LX/3MX;->a(LX/0QB;)LX/3MX;

    move-result-object v2

    check-cast v2, LX/3MX;

    const/16 v4, 0x1530

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v3, v0, v1, v2, v4}, LX/3MW;-><init>(LX/0Uh;LX/0SI;LX/3MX;LX/0Or;)V

    .line 555045
    return-object v3
.end method


# virtual methods
.method public final a(LX/4a1;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 3
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4a1;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 555028
    invoke-static {}, LX/5bZ;->a()LX/5bY;

    move-result-object v1

    .line 555029
    invoke-virtual {p0, v1}, LX/3MW;->a(LX/0gW;)V

    .line 555030
    const-string v0, "include_full_user_info"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 555031
    const-string v0, "user_fbids"

    invoke-virtual {v1, v0, p1}, LX/0gW;->a(Ljava/lang/String;LX/4a1;)LX/0gW;

    .line 555032
    const-string v0, "exclude_email_addresses"

    invoke-static {p0}, LX/3MW;->a(LX/3MW;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 555033
    const-string v2, "include_customer_data"

    iget-object v0, p0, LX/3MW;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 555034
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 555035
    iget-object v1, p0, LX/3MW;->b:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 555036
    iput-object v1, v0, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 555037
    iput-object p2, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 555038
    return-object v0
.end method

.method public final a(LX/0gW;)V
    .locals 4

    .prologue
    .line 555039
    iget-object v0, p0, LX/3MW;->c:LX/3MX;

    sget-object v1, LX/3MY;->HUGE:LX/3MY;

    invoke-virtual {v0, v1}, LX/3MX;->a(LX/3MY;)I

    move-result v0

    .line 555040
    iget-object v1, p0, LX/3MW;->c:LX/3MX;

    sget-object v2, LX/3MY;->BIG:LX/3MY;

    invoke-virtual {v1, v2}, LX/3MX;->a(LX/3MY;)I

    move-result v1

    .line 555041
    iget-object v2, p0, LX/3MW;->c:LX/3MX;

    sget-object v3, LX/3MY;->SMALL:LX/3MY;

    invoke-virtual {v2, v3}, LX/3MX;->a(LX/3MY;)I

    move-result v2

    .line 555042
    const-string v3, "profile_pic_large_size"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v3, "profile_pic_medium_size"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_pic_small_size"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 555043
    return-void
.end method
