.class public LX/3QY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/2Og;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 565277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565278
    return-void
.end method

.method public static a(LX/0QB;)LX/3QY;
    .locals 3

    .prologue
    .line 565272
    new-instance v2, LX/3QY;

    invoke-direct {v2}, LX/3QY;-><init>()V

    .line 565273
    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v0

    check-cast v0, LX/2Og;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    .line 565274
    iput-object v0, v2, LX/3QY;->a:LX/2Og;

    iput-object v1, v2, LX/3QY;->b:LX/0Uh;

    .line 565275
    move-object v0, v2

    .line 565276
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 3
    .param p1    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 565260
    iget-object v0, p0, LX/3QY;->a:LX/2Og;

    invoke-virtual {v0, p1}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 565261
    iget-object v1, p0, LX/3QY;->b:LX/0Uh;

    const/16 v2, 0x17f

    const/4 p1, 0x0

    invoke-virtual {v1, v2, p1}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v1, v1

    .line 565262
    if-eqz v1, :cond_1

    const/4 v1, 0x0

    .line 565263
    if-nez v0, :cond_2

    .line 565264
    :cond_0
    :goto_0
    move v1, v1

    .line 565265
    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 565266
    return v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 565267
    :cond_2
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 565268
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->T:LX/03R;

    invoke-virtual {v2, v1}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-nez v2, :cond_3

    .line 565269
    iget-object v2, p0, LX/3QY;->b:LX/0Uh;

    const/16 p1, 0x17d

    const/4 v0, 0x0

    invoke-virtual {v2, p1, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v2, v2

    .line 565270
    if-eqz v2, :cond_0

    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 565271
    :cond_4
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    goto :goto_0
.end method
