.class public LX/3Kv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2CH;

.field private final b:LX/3Kw;

.field private final c:LX/3Kx;

.field public final d:LX/3Ky;

.field public final e:LX/3L2;

.field private final f:LX/1tu;

.field private final g:LX/3L5;


# direct methods
.method public constructor <init>(LX/2CH;LX/3Kw;LX/3Kx;LX/3Ky;LX/3L2;LX/1tu;LX/3L5;)V
    .locals 0
    .param p5    # LX/3L2;
        .annotation runtime Lcom/facebook/messaging/ui/name/DefaultThreadNameViewComputer;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 549463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549464
    iput-object p1, p0, LX/3Kv;->a:LX/2CH;

    .line 549465
    iput-object p2, p0, LX/3Kv;->b:LX/3Kw;

    .line 549466
    iput-object p3, p0, LX/3Kv;->c:LX/3Kx;

    .line 549467
    iput-object p4, p0, LX/3Kv;->d:LX/3Ky;

    .line 549468
    iput-object p5, p0, LX/3Kv;->e:LX/3L2;

    .line 549469
    iput-object p6, p0, LX/3Kv;->f:LX/1tu;

    .line 549470
    iput-object p7, p0, LX/3Kv;->g:LX/3L5;

    .line 549471
    return-void
.end method

.method public static a(LX/0QB;)LX/3Kv;
    .locals 1

    .prologue
    .line 549462
    invoke-static {p0}, LX/3Kv;->b(LX/0QB;)LX/3Kv;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/3Kv;Lcom/facebook/user/model/User;ZLX/3OH;)LX/3OJ;
    .locals 12

    .prologue
    .line 549472
    new-instance v0, LX/3OJ;

    invoke-direct {v0}, LX/3OJ;-><init>()V

    .line 549473
    iput-object p1, v0, LX/3OJ;->a:Lcom/facebook/user/model/User;

    .line 549474
    move-object v0, v0

    .line 549475
    sget-object v1, LX/3ON;->ONE_LINE:LX/3ON;

    .line 549476
    iput-object v1, v0, LX/3OJ;->b:LX/3ON;

    .line 549477
    move-object v0, v0

    .line 549478
    iput-boolean p2, v0, LX/3OJ;->h:Z

    .line 549479
    move-object v0, v0

    .line 549480
    iput-object p3, v0, LX/3OJ;->r:LX/3OI;

    .line 549481
    move-object v1, v0

    .line 549482
    if-eqz p2, :cond_0

    .line 549483
    iget-object v0, p0, LX/3Kv;->a:LX/2CH;

    .line 549484
    iget-object v2, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 549485
    invoke-virtual {v0, v2}, LX/2CH;->c(Lcom/facebook/user/model/UserKey;)Z

    move-result v0

    .line 549486
    iget-object v2, p0, LX/3Kv;->a:LX/2CH;

    .line 549487
    iget-object v3, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 549488
    invoke-virtual {v2, v3}, LX/2CH;->e(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/LastActive;

    move-result-object v2

    .line 549489
    iget-object v3, p0, LX/3Kv;->a:LX/2CH;

    .line 549490
    iget-object v4, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 549491
    invoke-virtual {v3, v4}, LX/2CH;->d(Lcom/facebook/user/model/UserKey;)LX/3Ox;

    move-result-object v3

    .line 549492
    iget-object v4, p0, LX/3Kv;->b:LX/3Kw;

    .line 549493
    iget-object v5, v4, LX/3Kw;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2CH;

    .line 549494
    iget-object v6, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v6, v6

    .line 549495
    invoke-virtual {v5, v6}, LX/2CH;->d(Lcom/facebook/user/model/UserKey;)LX/3Ox;

    move-result-object v5

    .line 549496
    iget-object v6, v5, LX/3Ox;->d:LX/03R;

    move-object v5, v6

    .line 549497
    invoke-virtual {v5}, LX/03R;->isSet()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 549498
    invoke-virtual {v5}, LX/03R;->asBoolean()Z

    move-result v5

    .line 549499
    :goto_0
    move v4, v5

    .line 549500
    iput-boolean v0, v1, LX/3OJ;->e:Z

    .line 549501
    move-object v5, v1

    .line 549502
    iget-object v6, p0, LX/3Kv;->f:LX/1tu;

    .line 549503
    iget-wide v10, v3, LX/3Ox;->g:J

    move-wide v8, v10

    .line 549504
    invoke-virtual {v6, v8, v9}, LX/1tu;->a(J)Z

    move-result v6

    .line 549505
    iput-boolean v6, v5, LX/3OJ;->f:Z

    .line 549506
    move-object v5, v5

    .line 549507
    iget-boolean v6, v3, LX/3Ox;->c:Z

    move v6, v6

    .line 549508
    iput-boolean v6, v5, LX/3OJ;->g:Z

    .line 549509
    move-object v5, v5

    .line 549510
    if-nez v0, :cond_1

    iget-object v0, p0, LX/3Kv;->c:LX/3Kx;

    sget-object v6, LX/3P0;->SHORT:LX/3P0;

    sget-object v7, LX/3P1;->UPPER_CASE:LX/3P1;

    invoke-virtual {v0, v2, v3, v6, v7}, LX/3Kx;->a(Lcom/facebook/user/model/LastActive;LX/3Ox;LX/3P0;LX/3P1;)Ljava/lang/String;

    move-result-object v0

    .line 549511
    :goto_1
    iput-object v0, v5, LX/3OJ;->i:Ljava/lang/String;

    .line 549512
    move-object v2, v5

    .line 549513
    if-eqz v4, :cond_2

    sget-object v0, LX/3OM;->ON_MESSENGER:LX/3OM;

    .line 549514
    :goto_2
    iput-object v0, v2, LX/3OJ;->q:LX/3OM;

    .line 549515
    :cond_0
    return-object v1

    .line 549516
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    sget-object v0, LX/3OM;->ON_FACEBOOK:LX/3OM;

    goto :goto_2

    .line 549517
    :cond_3
    iget-boolean v5, p1, Lcom/facebook/user/model/User;->s:Z

    move v5, v5

    .line 549518
    if-nez v5, :cond_4

    .line 549519
    iget-boolean v5, p1, Lcom/facebook/user/model/User;->t:Z

    move v5, v5

    .line 549520
    if-eqz v5, :cond_5

    :cond_4
    const/4 v5, 0x1

    goto :goto_0

    :cond_5
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3Kv;
    .locals 8

    .prologue
    .line 549460
    new-instance v0, LX/3Kv;

    invoke-static {p0}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v1

    check-cast v1, LX/2CH;

    invoke-static {p0}, LX/3Kw;->b(LX/0QB;)LX/3Kw;

    move-result-object v2

    check-cast v2, LX/3Kw;

    invoke-static {p0}, LX/3Kx;->a(LX/0QB;)LX/3Kx;

    move-result-object v3

    check-cast v3, LX/3Kx;

    invoke-static {p0}, LX/3Ky;->a(LX/0QB;)LX/3Ky;

    move-result-object v4

    check-cast v4, LX/3Ky;

    invoke-static {p0}, LX/3Kz;->b(LX/0QB;)LX/3L2;

    move-result-object v5

    check-cast v5, LX/3L2;

    invoke-static {p0}, LX/1tu;->a(LX/0QB;)LX/1tu;

    move-result-object v6

    check-cast v6, LX/1tu;

    invoke-static {p0}, LX/3L5;->b(LX/0QB;)LX/3L5;

    move-result-object v7

    check-cast v7, LX/3L5;

    invoke-direct/range {v0 .. v7}, LX/3Kv;-><init>(LX/2CH;LX/3Kw;LX/3Kx;LX/3Ky;LX/3L2;LX/1tu;LX/3L5;)V

    .line 549461
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/User;LX/3OH;LX/6Lx;)LX/3OO;
    .locals 2
    .param p3    # LX/6Lx;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 549454
    const/4 v0, 0x1

    invoke-static {p0, p1, v0, p2}, LX/3Kv;->a(LX/3Kv;Lcom/facebook/user/model/User;ZLX/3OH;)LX/3OJ;

    move-result-object v0

    sget-object v1, LX/3ON;->ONE_LINE:LX/3ON;

    .line 549455
    iput-object v1, v0, LX/3OJ;->b:LX/3ON;

    .line 549456
    move-object v0, v0

    .line 549457
    iput-object p3, v0, LX/3OJ;->p:LX/6Lx;

    .line 549458
    move-object v0, v0

    .line 549459
    invoke-virtual {v0}, LX/3OJ;->a()LX/3OO;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/DAT;)LX/DAU;
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 549447
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, v3

    .line 549448
    iget-object v7, v0, LX/3Kv;->d:LX/3Ky;

    invoke-virtual {v7, v1}, LX/3Ky;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/FOO;

    move-result-object v7

    .line 549449
    iget-object v8, v0, LX/3Kv;->e:LX/3L2;

    const/4 v9, 0x3

    invoke-virtual {v8, v7, v9}, LX/3L2;->a(LX/FON;I)Ljava/lang/CharSequence;

    move-result-object v8

    .line 549450
    new-instance v7, LX/DAU;

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    move-object v8, v1

    move-object v9, v2

    move-object v11, v6

    move-object v12, v3

    invoke-direct/range {v7 .. v12}, LX/DAU;-><init>(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/3OI;Ljava/lang/String;LX/DAS;LX/Ddk;)V

    .line 549451
    goto :goto_0

    .line 549452
    :goto_0
    move-object v0, v7

    .line 549453
    return-object v0
.end method
