.class public LX/330;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 491516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491517
    return-void
.end method

.method public static a(LX/0QB;)LX/330;
    .locals 3

    .prologue
    .line 491518
    const-class v1, LX/330;

    monitor-enter v1

    .line 491519
    :try_start_0
    sget-object v0, LX/330;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 491520
    sput-object v2, LX/330;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 491521
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491522
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 491523
    new-instance v0, LX/330;

    invoke-direct {v0}, LX/330;-><init>()V

    .line 491524
    move-object v0, v0

    .line 491525
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 491526
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/330;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491527
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 491528
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
