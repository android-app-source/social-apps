.class public LX/31k;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/text/style/ClickableSpan;


# instance fields
.field public final b:Landroid/graphics/Path;

.field public final c:Landroid/graphics/Region;

.field private final d:Landroid/graphics/Path;

.field private final e:Landroid/graphics/Region;

.field private final f:Landroid/graphics/Region;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 488019
    new-instance v0, LX/31n;

    invoke-direct {v0}, LX/31n;-><init>()V

    sput-object v0, LX/31k;->a:Landroid/text/style/ClickableSpan;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 488020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488021
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/31k;->b:Landroid/graphics/Path;

    .line 488022
    new-instance v0, Landroid/graphics/Region;

    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    iput-object v0, p0, LX/31k;->c:Landroid/graphics/Region;

    .line 488023
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/31k;->d:Landroid/graphics/Path;

    .line 488024
    new-instance v0, Landroid/graphics/Region;

    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    iput-object v0, p0, LX/31k;->e:Landroid/graphics/Region;

    .line 488025
    new-instance v0, Landroid/graphics/Region;

    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    iput-object v0, p0, LX/31k;->f:Landroid/graphics/Region;

    .line 488026
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;FFF)Landroid/text/style/ClickableSpan;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 488027
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 488028
    check-cast v0, Landroid/widget/TextView;

    .line 488029
    invoke-virtual {v0}, Landroid/widget/TextView;->hasSelection()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 488030
    sget-object v7, LX/31k;->a:Landroid/text/style/ClickableSpan;

    .line 488031
    :cond_0
    :goto_0
    return-object v7

    .line 488032
    :cond_1
    invoke-virtual {v0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 488033
    :cond_2
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 488034
    check-cast p1, Landroid/widget/TextView;

    .line 488035
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    .line 488036
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    .line 488037
    :goto_1
    if-eqz v3, :cond_0

    .line 488038
    iget-object v0, p0, LX/31k;->f:Landroid/graphics/Region;

    invoke-virtual {v3}, Landroid/text/Layout;->getWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/text/Layout;->getHeight()I

    move-result v5

    invoke-virtual {v0, v1, v1, v4, v5}, Landroid/graphics/Region;->set(IIII)Z

    .line 488039
    iget-object v0, p0, LX/31k;->d:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 488040
    iget-object v0, p0, LX/31k;->d:Landroid/graphics/Path;

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, p2, p3, p4, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 488041
    iget-object v0, p0, LX/31k;->e:Landroid/graphics/Region;

    iget-object v4, p0, LX/31k;->d:Landroid/graphics/Path;

    iget-object v5, p0, LX/31k;->f:Landroid/graphics/Region;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 488042
    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v0

    const-class v4, Landroid/text/style/ClickableSpan;

    invoke-virtual {v2, v1, v0, v4}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, [Landroid/text/style/ClickableSpan;

    .line 488043
    array-length v9, v6

    move v8, v1

    :goto_2
    if-ge v8, v9, :cond_0

    aget-object v1, v6, v8

    .line 488044
    iget-object v4, p0, LX/31k;->e:Landroid/graphics/Region;

    iget-object v5, p0, LX/31k;->f:Landroid/graphics/Region;

    move-object v0, p0

    .line 488045
    invoke-virtual {v2, v1}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result p1

    invoke-virtual {v2, v1}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result p2

    iget-object p3, v0, LX/31k;->b:Landroid/graphics/Path;

    invoke-virtual {v3, p1, p2, p3}, Landroid/text/Layout;->getSelectionPath(IILandroid/graphics/Path;)V

    .line 488046
    iget-object p1, v0, LX/31k;->c:Landroid/graphics/Region;

    iget-object p2, v0, LX/31k;->b:Landroid/graphics/Path;

    invoke-virtual {p1, p2, v5}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 488047
    iget-object p1, v0, LX/31k;->c:Landroid/graphics/Region;

    sget-object p2, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v4, p2}, Landroid/graphics/Region;->op(Landroid/graphics/Region;Landroid/graphics/Region$Op;)Z

    move-result p1

    move v0, p1

    .line 488048
    if-eqz v0, :cond_4

    .line 488049
    if-eqz v7, :cond_5

    .line 488050
    sget-object v7, LX/31k;->a:Landroid/text/style/ClickableSpan;

    goto :goto_0

    .line 488051
    :cond_3
    instance-of v0, p1, Lcom/facebook/fbui/widget/text/TextLayoutView;

    if-eqz v0, :cond_0

    .line 488052
    check-cast p1, Lcom/facebook/fbui/widget/text/TextLayoutView;

    .line 488053
    invoke-virtual {p1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    .line 488054
    invoke-virtual {p1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    goto :goto_1

    :cond_4
    move-object v1, v7

    .line 488055
    :cond_5
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move-object v7, v1

    goto :goto_2
.end method
