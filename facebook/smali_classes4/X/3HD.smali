.class public LX/3HD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0sa;

.field public final b:LX/0se;

.field public final c:LX/0SG;

.field public final d:LX/0ad;

.field public final e:LX/3HE;

.field public f:LX/0sf;

.field public g:LX/0tE;

.field public h:LX/0tG;

.field public i:LX/0tI;


# direct methods
.method public constructor <init>(LX/0sa;LX/0se;LX/0SG;LX/3HE;LX/0sf;LX/0tE;LX/0tG;LX/0tI;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543097
    iput-object p1, p0, LX/3HD;->a:LX/0sa;

    .line 543098
    iput-object p2, p0, LX/3HD;->b:LX/0se;

    .line 543099
    iput-object p3, p0, LX/3HD;->c:LX/0SG;

    .line 543100
    iput-object p4, p0, LX/3HD;->e:LX/3HE;

    .line 543101
    iput-object p5, p0, LX/3HD;->f:LX/0sf;

    .line 543102
    iput-object p6, p0, LX/3HD;->g:LX/0tE;

    .line 543103
    iput-object p7, p0, LX/3HD;->h:LX/0tG;

    .line 543104
    iput-object p8, p0, LX/3HD;->i:LX/0tI;

    .line 543105
    iput-object p9, p0, LX/3HD;->d:LX/0ad;

    .line 543106
    return-void
.end method

.method public static a(LX/0QB;)LX/3HD;
    .locals 11

    .prologue
    .line 543107
    new-instance v1, LX/3HD;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v2

    check-cast v2, LX/0sa;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v3

    check-cast v3, LX/0se;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/3HE;->b(LX/0QB;)LX/3HE;

    move-result-object v5

    check-cast v5, LX/3HE;

    invoke-static {p0}, LX/0sf;->a(LX/0QB;)LX/0sf;

    move-result-object v6

    check-cast v6, LX/0sf;

    invoke-static {p0}, LX/0tE;->b(LX/0QB;)LX/0tE;

    move-result-object v7

    check-cast v7, LX/0tE;

    invoke-static {p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v8

    check-cast v8, LX/0tG;

    invoke-static {p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v9

    check-cast v9, LX/0tI;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-direct/range {v1 .. v10}, LX/3HD;-><init>(LX/0sa;LX/0se;LX/0SG;LX/3HE;LX/0sf;LX/0tE;LX/0tG;LX/0tI;LX/0ad;)V

    .line 543108
    move-object v0, v1

    .line 543109
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;LX/3HP;)LX/0zO;
    .locals 6
    .param p2    # LX/3HP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;",
            "Lcom/facebook/api/ufiservices/common/FeedbackCacheProvider;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 543110
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->c:LX/5H2;

    move-object v0, v0

    .line 543111
    sget-object v1, LX/6BN;->a:[I

    invoke-virtual {v0}, LX/5H2;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 543112
    new-instance v0, LX/6AU;

    invoke-direct {v0}, LX/6AU;-><init>()V

    move-object v0, v0

    .line 543113
    :goto_0
    const/4 v1, 0x1

    .line 543114
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 543115
    const-string v1, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "likers_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543116
    if-eqz p1, :cond_1

    .line 543117
    const-string v1, "media_id"

    .line 543118
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 543119
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "max_comments"

    .line 543120
    iget v3, p1, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->b:I

    move v3, v3

    .line 543121
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543122
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->c:LX/5H2;

    move-object v1, v1

    .line 543123
    sget-object v2, LX/5H2;->SIMPLE_WITH_ATTRIBUTION:LX/5H2;

    if-ne v1, v2, :cond_0

    .line 543124
    const-string v1, "image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 543125
    :cond_0
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->e:LX/21y;

    move-object v1, v1

    .line 543126
    if-eqz v1, :cond_1

    .line 543127
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->e:LX/21y;

    move-object v1, v1

    .line 543128
    sget-object v2, LX/21y;->DEFAULT_ORDER:LX/21y;

    invoke-virtual {v1, v2}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 543129
    const-string v1, "comment_order"

    .line 543130
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->e:LX/21y;

    move-object v2, v2

    .line 543131
    iget-object v2, v2, LX/21y;->toString:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543132
    :cond_1
    const-string v1, "angora_attachment_cover_image_size"

    iget-object v2, p0, LX/3HD;->a:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543133
    const-string v1, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543134
    const-string v1, "fetch_reshare_counts"

    iget-object v2, p0, LX/3HD;->d:LX/0ad;

    sget-short v3, LX/0wn;->aF:S

    const/4 v5, 0x0

    invoke-interface {v2, v3, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 543135
    iget-object v1, p0, LX/3HD;->g:LX/0tE;

    invoke-virtual {v1, v0}, LX/0tE;->b(LX/0gW;)V

    .line 543136
    iget-object v1, p0, LX/3HD;->h:LX/0tG;

    invoke-virtual {v1, v0}, LX/0tG;->a(LX/0gW;)V

    .line 543137
    iget-object v1, p0, LX/3HD;->i:LX/0tI;

    invoke-virtual {v1, v0}, LX/0tI;->a(LX/0gW;)V

    .line 543138
    iget-object v1, p0, LX/3HD;->b:LX/0se;

    invoke-virtual {v1, v0}, LX/0se;->a(LX/0gW;)LX/0gW;

    move-result-object v0

    move-object v0, v0

    .line 543139
    const-class v1, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-static {v1}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v1

    move-object v1, v1

    .line 543140
    invoke-static {v0, v1}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object v0

    .line 543141
    iput-boolean v4, v0, LX/0zO;->p:Z

    .line 543142
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;->d:LX/0rS;

    move-object v1, v1

    .line 543143
    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v1, v2, :cond_2

    .line 543144
    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 543145
    :goto_1
    const-wide/32 v2, 0x3f480

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    .line 543146
    iput-boolean v4, v0, LX/0zO;->p:Z

    .line 543147
    new-instance v1, LX/6BO;

    iget-object v2, p0, LX/3HD;->f:LX/0sf;

    invoke-virtual {v2, v0}, LX/0sf;->a(LX/0zO;)LX/1ks;

    move-result-object v2

    invoke-direct {v1, p0, p1, v2, p2}, LX/6BO;-><init>(LX/3HD;Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;LX/1ks;LX/3HP;)V

    move-object v1, v1

    .line 543148
    iput-object v1, v0, LX/0zO;->g:LX/1kt;

    .line 543149
    return-object v0

    .line 543150
    :cond_2
    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    goto :goto_1

    .line 543151
    :pswitch_0
    new-instance v0, LX/6AT;

    invoke-direct {v0}, LX/6AT;-><init>()V

    move-object v0, v0

    .line 543152
    goto/16 :goto_0

    .line 543153
    :pswitch_1
    new-instance v0, LX/6AV;

    invoke-direct {v0}, LX/6AV;-><init>()V

    move-object v0, v0

    .line 543154
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
