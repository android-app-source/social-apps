.class public LX/2BT;
.super LX/0Tw;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/2BT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 379119
    const-class v0, LX/2BT;

    sput-object v0, LX/2BT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 379120
    const-string v0, "legacy_key_value"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/0Tw;-><init>(Ljava/lang/String;I)V

    .line 379121
    return-void
.end method

.method public static a(LX/0QB;)LX/2BT;
    .locals 3

    .prologue
    .line 379122
    sget-object v0, LX/2BT;->b:LX/2BT;

    if-nez v0, :cond_1

    .line 379123
    const-class v1, LX/2BT;

    monitor-enter v1

    .line 379124
    :try_start_0
    sget-object v0, LX/2BT;->b:LX/2BT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 379125
    if-eqz v2, :cond_0

    .line 379126
    :try_start_1
    new-instance v0, LX/2BT;

    invoke-direct {v0}, LX/2BT;-><init>()V

    .line 379127
    move-object v0, v0

    .line 379128
    sput-object v0, LX/2BT;->b:LX/2BT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 379129
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 379130
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 379131
    :cond_1
    sget-object v0, LX/2BT;->b:LX/2BT;

    return-object v0

    .line 379132
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 379133
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 379134
    const-string v0, "CREATE TABLE key_value (_id INTEGER PRIMARY KEY,key TEXT UNIQUE,value TEXT);"

    const v1, -0x37a19516

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7bee558

    invoke-static {v0}, LX/03h;->a(I)V

    .line 379135
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 379136
    const-string v0, "DROP TABLE IF EXISTS key_value"

    const v1, 0x46904f1b

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x2617b4f0

    invoke-static {v0}, LX/03h;->a(I)V

    .line 379137
    invoke-virtual {p0, p1}, LX/2BT;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 379138
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 379139
    const-string v0, "key_value"

    invoke-virtual {p1, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 379140
    return-void
.end method
