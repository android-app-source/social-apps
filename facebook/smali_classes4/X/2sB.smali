.class public LX/2sB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 472415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 472431
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 472432
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 472433
    :goto_0
    return v1

    .line 472434
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_4

    .line 472435
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 472436
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 472437
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 472438
    const-string v12, "rating_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 472439
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v8

    move v10, v8

    move v8, v6

    goto :goto_1

    .line 472440
    :cond_1
    const-string v12, "scale"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 472441
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v9, v7

    move v7, v6

    goto :goto_1

    .line 472442
    :cond_2
    const-string v12, "value"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 472443
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 472444
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 472445
    :cond_4
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 472446
    if-eqz v8, :cond_5

    .line 472447
    invoke-virtual {p1, v1, v10, v1}, LX/186;->a(III)V

    .line 472448
    :cond_5
    if-eqz v7, :cond_6

    .line 472449
    invoke-virtual {p1, v6, v9, v1}, LX/186;->a(III)V

    .line 472450
    :cond_6
    if-eqz v0, :cond_7

    .line 472451
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 472452
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move v8, v1

    move-wide v2, v4

    move v9, v1

    move v10, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 472416
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 472417
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 472418
    if-eqz v0, :cond_0

    .line 472419
    const-string v1, "rating_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 472420
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 472421
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 472422
    if-eqz v0, :cond_1

    .line 472423
    const-string v1, "scale"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 472424
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 472425
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 472426
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 472427
    const-string v2, "value"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 472428
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 472429
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 472430
    return-void
.end method
