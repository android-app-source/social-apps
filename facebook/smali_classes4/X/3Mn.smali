.class public LX/3Mn;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:Landroid/net/Uri;

.field private static final e:[Ljava/lang/String;

.field private static final f:Ljava/util/regex/Pattern;


# instance fields
.field private final g:Landroid/content/Context;

.field public final h:LX/01T;

.field public final i:LX/2Oq;

.field private final j:LX/3Lw;

.field public final k:LX/1Ml;

.field private final l:LX/3Mo;

.field private final m:Lcom/facebook/messaging/sms/SmsThreadManager;

.field public final n:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 555602
    const-string v0, "content://mms-sms/canonical-addresses"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/3Mn;->a:Landroid/net/Uri;

    .line 555603
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    sput-object v0, LX/3Mn;->b:[Ljava/lang/String;

    .line 555604
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "data1"

    aput-object v1, v0, v3

    sput-object v0, LX/3Mn;->c:[Ljava/lang/String;

    .line 555605
    sget-object v0, LX/2Uo;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "simple"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/3Mn;->d:Landroid/net/Uri;

    .line 555606
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "date"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "recipient_ids"

    aput-object v2, v0, v1

    sput-object v0, LX/3Mn;->e:[Ljava/lang/String;

    .line 555607
    const-string v0, "\\s+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/3Mn;->f:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/01T;LX/2Oq;LX/3Lw;LX/1Ml;LX/3Mo;Lcom/facebook/messaging/sms/SmsThreadManager;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 555588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555589
    iput-object p1, p0, LX/3Mn;->g:Landroid/content/Context;

    .line 555590
    iput-object p2, p0, LX/3Mn;->h:LX/01T;

    .line 555591
    iput-object p3, p0, LX/3Mn;->i:LX/2Oq;

    .line 555592
    iput-object p4, p0, LX/3Mn;->j:LX/3Lw;

    .line 555593
    iput-object p5, p0, LX/3Mn;->k:LX/1Ml;

    .line 555594
    iput-object p6, p0, LX/3Mn;->l:LX/3Mo;

    .line 555595
    iput-object p7, p0, LX/3Mn;->m:Lcom/facebook/messaging/sms/SmsThreadManager;

    .line 555596
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 555597
    const-string v1, "android.permission.READ_CONTACTS"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 555598
    iget-object v1, p0, LX/3Mn;->j:LX/3Lw;

    invoke-virtual {v1}, LX/3Lw;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 555599
    const-string v1, "android.permission.READ_SMS"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 555600
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, LX/3Mn;->n:[Ljava/lang/String;

    .line 555601
    return-void
.end method

.method private static a(LX/3Mn;J)Ljava/lang/Long;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 555575
    const-string v0, "_id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 555576
    :try_start_0
    iget-object v0, p0, LX/3Mn;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/3Mn;->d:Landroid/net/Uri;

    sget-object v2, LX/3Mn;->e:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 555577
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 555578
    const-string v0, "date"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 555579
    if-eqz v1, :cond_0

    .line 555580
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 555581
    :cond_0
    :goto_0
    return-object v0

    .line 555582
    :cond_1
    if-eqz v1, :cond_2

    .line 555583
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v6

    .line 555584
    goto :goto_0

    .line 555585
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v6, :cond_3

    .line 555586
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 555587
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_1
.end method

.method public static a(LX/3Mn;Ljava/util/Set;Ljava/util/List;Ljava/util/Set;I)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 555549
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 555550
    :cond_0
    :goto_0
    return-object p2

    .line 555551
    :cond_1
    invoke-static {}, LX/0uu;->b()LX/0uw;

    move-result-object v6

    .line 555552
    const/4 v8, 0x0

    .line 555553
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 555554
    const-string v4, "recipient_ids"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "% "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 555555
    const-string v4, "recipient_ids"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " %"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 555556
    const-string v4, "recipient_ids"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "% "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " %"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    goto :goto_1

    .line 555557
    :cond_2
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Mn;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, LX/3Mn;->d:Landroid/net/Uri;

    sget-object v4, LX/3Mn;->e:[Ljava/lang/String;

    invoke-virtual {v6}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 555558
    if-eqz v9, :cond_4

    .line 555559
    :try_start_1
    const-string v2, "_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 555560
    const-string v2, "recipient_ids"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 555561
    const-string v2, "date"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 555562
    const/4 v2, 0x0

    .line 555563
    :cond_3
    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    move/from16 v0, p4

    if-ge v2, v0, :cond_4

    .line 555564
    invoke-interface {v9, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 555565
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 555566
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3Mn;->m:Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v3

    .line 555567
    if-eqz v3, :cond_3

    .line 555568
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 555569
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 555570
    :cond_4
    if-eqz v9, :cond_0

    .line 555571
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 555572
    :catchall_0
    move-exception v2

    move-object v3, v8

    :goto_3
    if-eqz v3, :cond_5

    .line 555573
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2

    .line 555574
    :catchall_1
    move-exception v2

    move-object v3, v9

    goto :goto_3
.end method

.method public static a(LX/3Mn;Ljava/lang/String;)Ljava/util/Set;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v1, 0x0

    .line 555460
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 555461
    sget-object v0, LX/3Mn;->f:Ljava/util/regex/Pattern;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    move-result-object v2

    .line 555462
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v3

    .line 555463
    const/4 v6, 0x0

    .line 555464
    array-length v4, v2

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v2, v0

    .line 555465
    new-array v8, v13, [LX/0ux;

    const-string v9, "display_name"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v9

    aput-object v9, v8, v1

    const-string v9, "display_name"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "% "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, "%"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v5}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    aput-object v5, v8, v12

    invoke-static {v8}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 555466
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 555467
    :cond_0
    new-array v0, v13, [LX/0ux;

    aput-object v3, v0, v1

    new-array v2, v13, [LX/0ux;

    const-string v3, "mimetype"

    const-string v4, "vnd.android.cursor.item/email_v2"

    invoke-static {v3, v4}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v1, "mimetype"

    const-string v3, "vnd.android.cursor.item/phone_v2"

    invoke-static {v1, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 555468
    :try_start_0
    iget-object v0, p0, LX/3Mn;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, LX/3Mn;->c:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const-string v5, "contact_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 555469
    if-eqz v1, :cond_4

    .line 555470
    :try_start_1
    const-string v0, "data1"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 555471
    :cond_1
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 555472
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 555473
    invoke-static {v2}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 555474
    invoke-static {v2}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 555475
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    .line 555476
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 555477
    :cond_3
    :try_start_2
    invoke-static {v2}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 555478
    invoke-interface {v7, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 555479
    :cond_4
    if-eqz v1, :cond_5

    .line 555480
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 555481
    :cond_5
    return-object v7

    .line 555482
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method

.method public static a(LX/3Mn;Ljava/lang/String;Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 555537
    const-string v0, "address"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "%"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 555538
    :try_start_0
    iget-object v0, p0, LX/3Mn;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/3Mn;->a:Landroid/net/Uri;

    sget-object v2, LX/3Mn;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 555539
    if-eqz v1, :cond_1

    .line 555540
    :try_start_1
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 555541
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 555542
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 555543
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 555544
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 555545
    :cond_1
    if-eqz v1, :cond_2

    .line 555546
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 555547
    :cond_2
    return-void

    .line 555548
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public static a(LX/3Mn;Ljava/util/Set;Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 555523
    invoke-static {}, LX/0uu;->b()LX/0uw;

    move-result-object v4

    .line 555524
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 555525
    const-string v2, "address"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "%"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "%"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    goto :goto_0

    .line 555526
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/3Mn;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/3Mn;->a:Landroid/net/Uri;

    sget-object v2, LX/3Mn;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 555527
    if-eqz v1, :cond_2

    .line 555528
    :try_start_1
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 555529
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 555530
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 555531
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 555532
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 555533
    :cond_2
    if-eqz v1, :cond_3

    .line 555534
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 555535
    :cond_3
    return-void

    .line 555536
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method

.method public static b(LX/0QB;)LX/3Mn;
    .locals 8

    .prologue
    .line 555521
    new-instance v0, LX/3Mn;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v2

    check-cast v2, LX/01T;

    invoke-static {p0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v3

    check-cast v3, LX/2Oq;

    invoke-static {p0}, LX/3Lw;->b(LX/0QB;)LX/3Lw;

    move-result-object v4

    check-cast v4, LX/3Lw;

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v5

    check-cast v5, LX/1Ml;

    invoke-static {p0}, LX/3Mo;->a(LX/0QB;)LX/3Mo;

    move-result-object v6

    check-cast v6, LX/3Mo;

    invoke-static {p0}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(LX/0QB;)Lcom/facebook/messaging/sms/SmsThreadManager;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-direct/range {v0 .. v7}, LX/3Mn;-><init>(Landroid/content/Context;LX/01T;LX/2Oq;LX/3Lw;LX/1Ml;LX/3Mo;Lcom/facebook/messaging/sms/SmsThreadManager;)V

    .line 555522
    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/util/List;Ljava/util/Set;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 555504
    const/4 v1, 0x0

    .line 555505
    :try_start_0
    iget-object v0, p0, LX/3Mn;->l:LX/3Mo;

    invoke-virtual {v0, p1}, LX/3Mo;->b(Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 555506
    if-eqz v7, :cond_1

    .line 555507
    const/4 v0, 0x0

    .line 555508
    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    if-ge v0, p4, :cond_1

    .line 555509
    sget-object v1, LX/3MM;->a:LX/0U1;

    invoke-virtual {v1, v7}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v2

    .line 555510
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 555511
    iget-object v1, p0, LX/3Mn;->m:Lcom/facebook/messaging/sms/SmsThreadManager;

    sget-object v4, LX/3MM;->e:LX/0U1;

    invoke-virtual {v4, v7}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v2, v3}, LX/3Mn;->a(LX/3Mn;J)Ljava/lang/Long;

    move-result-object v5

    sget-object v6, LX/3MM;->d:LX/0U1;

    invoke-virtual {v6, v7}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 555512
    if-eqz v1, :cond_0

    .line 555513
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 555514
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 555515
    :cond_1
    if-eqz v7, :cond_2

    .line 555516
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 555517
    :cond_2
    return-void

    .line 555518
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_3

    .line 555519
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 555520
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 555483
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 555484
    const/4 v1, 0x0

    .line 555485
    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    iget-object v3, p0, LX/3Mn;->h:LX/01T;

    invoke-virtual {v2, v3}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 555486
    :cond_0
    :goto_0
    move v1, v1

    .line 555487
    if-nez v1, :cond_1

    .line 555488
    :goto_1
    return-object v0

    .line 555489
    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 555490
    invoke-direct {p0, p1, v0, v1, p2}, LX/3Mn;->b(Ljava/lang/String;Ljava/util/List;Ljava/util/Set;I)V

    .line 555491
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 555492
    invoke-static {p1}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v3

    .line 555493
    invoke-static {p1}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz v3, :cond_6

    .line 555494
    :cond_2
    if-eqz v3, :cond_5

    .line 555495
    :goto_2
    invoke-static {p0, p1, v2}, LX/3Mn;->a(LX/3Mn;Ljava/lang/String;Ljava/util/Set;)V

    .line 555496
    :cond_3
    :goto_3
    invoke-static {p0, v2, v0, v1, p2}, LX/3Mn;->a(LX/3Mn;Ljava/util/Set;Ljava/util/List;Ljava/util/Set;I)Ljava/util/List;

    .line 555497
    goto :goto_1

    .line 555498
    :cond_4
    iget-object v2, p0, LX/3Mn;->k:LX/1Ml;

    iget-object v3, p0, LX/3Mn;->n:[Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 555499
    iget-object v1, p0, LX/3Mn;->i:LX/2Oq;

    invoke-virtual {v1}, LX/2Oq;->a()Z

    move-result v1

    goto :goto_0

    .line 555500
    :cond_5
    invoke-static {p1}, LX/3Lx;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 555501
    :cond_6
    invoke-static {p0, p1}, LX/3Mn;->a(LX/3Mn;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v3

    .line 555502
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 555503
    invoke-static {p0, v3, v2}, LX/3Mn;->a(LX/3Mn;Ljava/util/Set;Ljava/util/Set;)V

    goto :goto_3
.end method
