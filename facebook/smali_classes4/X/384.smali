.class public final LX/384;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/380;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z

.field private g:Z

.field private final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field private n:Landroid/view/Display;

.field private o:I

.field public p:Landroid/os/Bundle;

.field public q:LX/383;


# direct methods
.method public constructor <init>(LX/380;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 502876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 502877
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/384;->h:Ljava/util/ArrayList;

    .line 502878
    const/4 v0, -0x1

    iput v0, p0, LX/384;->o:I

    .line 502879
    iput-object p1, p0, LX/384;->a:LX/380;

    .line 502880
    iput-object p2, p0, LX/384;->b:Ljava/lang/String;

    .line 502881
    iput-object p3, p0, LX/384;->c:Ljava/lang/String;

    .line 502882
    return-void
.end method


# virtual methods
.method public final a(LX/383;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 502883
    const/4 v0, 0x0

    .line 502884
    iget-object v2, p0, LX/384;->q:LX/383;

    if-eq v2, p1, :cond_a

    .line 502885
    iput-object p1, p0, LX/384;->q:LX/383;

    .line 502886
    if-eqz p1, :cond_a

    .line 502887
    iget-object v2, p0, LX/384;->d:Ljava/lang/String;

    invoke-virtual {p1}, LX/383;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/37i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 502888
    invoke-virtual {p1}, LX/383;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/384;->d:Ljava/lang/String;

    move v0, v1

    .line 502889
    :cond_0
    iget-object v2, p0, LX/384;->e:Ljava/lang/String;

    invoke-virtual {p1}, LX/383;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/37i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 502890
    invoke-virtual {p1}, LX/383;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/384;->e:Ljava/lang/String;

    move v0, v1

    .line 502891
    :cond_1
    iget-boolean v2, p0, LX/384;->f:Z

    invoke-virtual {p1}, LX/383;->d()Z

    move-result v3

    if-eq v2, v3, :cond_c

    .line 502892
    invoke-virtual {p1}, LX/383;->d()Z

    move-result v0

    iput-boolean v0, p0, LX/384;->f:Z

    .line 502893
    :goto_0
    iget-boolean v0, p0, LX/384;->g:Z

    invoke-virtual {p1}, LX/383;->e()Z

    move-result v2

    if-eq v0, v2, :cond_2

    .line 502894
    invoke-virtual {p1}, LX/383;->e()Z

    move-result v0

    iput-boolean v0, p0, LX/384;->g:Z

    .line 502895
    or-int/lit8 v1, v1, 0x1

    .line 502896
    :cond_2
    iget-object v0, p0, LX/384;->h:Ljava/util/ArrayList;

    invoke-virtual {p1}, LX/383;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 502897
    iget-object v0, p0, LX/384;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 502898
    iget-object v0, p0, LX/384;->h:Ljava/util/ArrayList;

    invoke-virtual {p1}, LX/383;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 502899
    or-int/lit8 v1, v1, 0x1

    .line 502900
    :cond_3
    iget v0, p0, LX/384;->i:I

    invoke-virtual {p1}, LX/383;->g()I

    move-result v2

    if-eq v0, v2, :cond_4

    .line 502901
    invoke-virtual {p1}, LX/383;->g()I

    move-result v0

    iput v0, p0, LX/384;->i:I

    .line 502902
    or-int/lit8 v1, v1, 0x1

    .line 502903
    :cond_4
    iget v0, p0, LX/384;->j:I

    invoke-virtual {p1}, LX/383;->h()I

    move-result v2

    if-eq v0, v2, :cond_5

    .line 502904
    invoke-virtual {p1}, LX/383;->h()I

    move-result v0

    iput v0, p0, LX/384;->j:I

    .line 502905
    or-int/lit8 v1, v1, 0x1

    .line 502906
    :cond_5
    iget v0, p0, LX/384;->k:I

    invoke-virtual {p1}, LX/383;->k()I

    move-result v2

    if-eq v0, v2, :cond_6

    .line 502907
    invoke-virtual {p1}, LX/383;->k()I

    move-result v0

    iput v0, p0, LX/384;->k:I

    .line 502908
    or-int/lit8 v1, v1, 0x3

    .line 502909
    :cond_6
    iget v0, p0, LX/384;->l:I

    invoke-virtual {p1}, LX/383;->i()I

    move-result v2

    if-eq v0, v2, :cond_7

    .line 502910
    invoke-virtual {p1}, LX/383;->i()I

    move-result v0

    iput v0, p0, LX/384;->l:I

    .line 502911
    or-int/lit8 v1, v1, 0x3

    .line 502912
    :cond_7
    iget v0, p0, LX/384;->m:I

    invoke-virtual {p1}, LX/383;->j()I

    move-result v2

    if-eq v0, v2, :cond_8

    .line 502913
    invoke-virtual {p1}, LX/383;->j()I

    move-result v0

    iput v0, p0, LX/384;->m:I

    .line 502914
    or-int/lit8 v1, v1, 0x3

    .line 502915
    :cond_8
    iget v0, p0, LX/384;->o:I

    invoke-virtual {p1}, LX/383;->l()I

    move-result v2

    if-eq v0, v2, :cond_9

    .line 502916
    invoke-virtual {p1}, LX/383;->l()I

    move-result v0

    iput v0, p0, LX/384;->o:I

    .line 502917
    const/4 v0, 0x0

    iput-object v0, p0, LX/384;->n:Landroid/view/Display;

    .line 502918
    or-int/lit8 v1, v1, 0x5

    .line 502919
    :cond_9
    iget-object v0, p0, LX/384;->p:Landroid/os/Bundle;

    invoke-virtual {p1}, LX/383;->m()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v2}, LX/37i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 502920
    invoke-virtual {p1}, LX/383;->m()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, LX/384;->p:Landroid/os/Bundle;

    .line 502921
    or-int/lit8 v0, v1, 0x1

    .line 502922
    :cond_a
    :goto_1
    return v0

    :cond_b
    move v0, v1

    goto :goto_1

    :cond_c
    move v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/38T;)Z
    .locals 7
    .param p1    # LX/38T;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 502923
    if-nez p1, :cond_0

    .line 502924
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502925
    :cond_0
    invoke-static {}, LX/37i;->b()V

    .line 502926
    iget-object v0, p0, LX/384;->h:Ljava/util/ArrayList;

    const/4 v3, 0x0

    .line 502927
    if-eqz v0, :cond_3

    .line 502928
    invoke-static {p1}, LX/38T;->e(LX/38T;)V

    .line 502929
    iget-object v1, p1, LX/38T;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    .line 502930
    if-eqz v6, :cond_3

    .line 502931
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p0

    move v5, v3

    .line 502932
    :goto_0
    if-ge v5, p0, :cond_3

    .line 502933
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/IntentFilter;

    .line 502934
    if-eqz v1, :cond_2

    move v4, v3

    .line 502935
    :goto_1
    if-ge v4, v6, :cond_2

    .line 502936
    iget-object v2, p1, LX/38T;->c:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 502937
    const/4 v1, 0x1

    .line 502938
    :goto_2
    move v0, v1

    .line 502939
    return v0

    .line 502940
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 502941
    :cond_2
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    :cond_3
    move v1, v3

    .line 502942
    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 502943
    if-nez p1, :cond_0

    .line 502944
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "category must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502945
    :cond_0
    invoke-static {}, LX/37i;->b()V

    .line 502946
    iget-object v0, p0, LX/384;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 502947
    :goto_0
    if-ge v2, v3, :cond_2

    .line 502948
    iget-object v0, p0, LX/384;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    invoke-virtual {v0, p1}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502949
    const/4 v0, 0x1

    .line 502950
    :goto_1
    return v0

    .line 502951
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 502952
    goto :goto_1
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 502953
    invoke-static {}, LX/37i;->b()V

    .line 502954
    sget-object v0, LX/37i;->a:LX/37j;

    invoke-virtual {v0, p0}, LX/37j;->a(LX/384;)V

    .line 502955
    return-void
.end method

.method public final m()LX/37v;
    .locals 1

    .prologue
    .line 502956
    iget-object v0, p0, LX/384;->a:LX/380;

    .line 502957
    invoke-static {}, LX/37i;->b()V

    .line 502958
    iget-object p0, v0, LX/380;->a:LX/37v;

    move-object v0, p0

    .line 502959
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 502960
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaRouter.RouteInfo{ uniqueId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/384;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/384;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/384;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/384;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connecting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/384;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/384;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/384;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeHandling="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/384;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/384;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/384;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presentationDisplayId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/384;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extras="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/384;->p:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", providerPackageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/384;->a:LX/380;

    invoke-virtual {v1}, LX/380;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
