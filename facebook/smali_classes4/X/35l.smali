.class public LX/35l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/Ap5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Ap5",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/35i;

.field public final c:LX/1qb;

.field public final d:LX/C0D;

.field private final e:LX/1qa;


# direct methods
.method public constructor <init>(LX/Ap5;LX/C0D;LX/1qb;LX/35i;LX/1qa;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497593
    iput-object p1, p0, LX/35l;->a:LX/Ap5;

    .line 497594
    iput-object p3, p0, LX/35l;->c:LX/1qb;

    .line 497595
    iput-object p2, p0, LX/35l;->d:LX/C0D;

    .line 497596
    iput-object p4, p0, LX/35l;->b:LX/35i;

    .line 497597
    iput-object p5, p0, LX/35l;->e:LX/1qa;

    .line 497598
    return-void
.end method

.method public static a(LX/0QB;)LX/35l;
    .locals 9

    .prologue
    .line 497599
    const-class v1, LX/35l;

    monitor-enter v1

    .line 497600
    :try_start_0
    sget-object v0, LX/35l;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 497601
    sput-object v2, LX/35l;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 497602
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497603
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 497604
    new-instance v3, LX/35l;

    invoke-static {v0}, LX/Ap5;->a(LX/0QB;)LX/Ap5;

    move-result-object v4

    check-cast v4, LX/Ap5;

    invoke-static {v0}, LX/C0D;->b(LX/0QB;)LX/C0D;

    move-result-object v5

    check-cast v5, LX/C0D;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v6

    check-cast v6, LX/1qb;

    invoke-static {v0}, LX/35i;->a(LX/0QB;)LX/35i;

    move-result-object v7

    check-cast v7, LX/35i;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v8

    check-cast v8, LX/1qa;

    invoke-direct/range {v3 .. v8}, LX/35l;-><init>(LX/Ap5;LX/C0D;LX/1qb;LX/35i;LX/1qa;)V

    .line 497605
    move-object v0, v3

    .line 497606
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 497607
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/35l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497608
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 497609
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/35i;LX/1V4;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/35i;",
            "LX/1V4;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 497591
    invoke-virtual {p1, p0}, LX/35i;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/1V4;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
