.class public LX/2Nz;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/StoreManagerFactory;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/StoreManagerFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 400363
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method private a()Lcom/facebook/compactdisk/StoreManagerFactory;
    .locals 15

    .prologue
    .line 400362
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/2FE;->a(LX/0QB;)Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    move-result-object v1

    check-cast v1, Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    invoke-static {p0}, LX/2FD;->a(LX/0QB;)Lcom/facebook/compactdisk/AttributeStoreHolder;

    move-result-object v2

    check-cast v2, Lcom/facebook/compactdisk/AttributeStoreHolder;

    invoke-static {p0}, LX/2O0;->a(LX/0QB;)Lcom/facebook/compactdisk/Configuration;

    move-result-object v3

    check-cast v3, Lcom/facebook/compactdisk/Configuration;

    invoke-static {p0}, LX/2F8;->a(LX/0QB;)Lcom/facebook/compactdisk/DiskSizeCalculator;

    move-result-object v4

    check-cast v4, Lcom/facebook/compactdisk/DiskSizeCalculator;

    invoke-static {p0}, LX/2F9;->a(LX/0QB;)Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    move-result-object v5

    check-cast v5, Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    invoke-static {p0}, LX/2O1;->a(LX/0QB;)Lcom/facebook/compactdisk/ConfigurationOverrides;

    move-result-object v6

    check-cast v6, Lcom/facebook/compactdisk/ConfigurationOverrides;

    invoke-static {p0}, LX/2FA;->a(LX/0QB;)Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object v7

    check-cast v7, Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-static {p0}, LX/2IJ;->a(LX/0QB;)Lcom/facebook/compactdisk/LazyDispatcher;

    move-result-object v8

    check-cast v8, Lcom/facebook/compactdisk/LazyDispatcher;

    invoke-static {p0}, LX/2zW;->a(LX/0QB;)Lcom/facebook/compactdisk/ExperimentManager;

    move-result-object v9

    check-cast v9, Lcom/facebook/compactdisk/ExperimentManager;

    invoke-static {p0}, LX/2O2;->a(LX/0QB;)Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;

    move-result-object v10

    check-cast v10, Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;

    invoke-static {p0}, LX/30E;->a(LX/0QB;)Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    move-result-object v11

    check-cast v11, Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    invoke-static {p0}, LX/2O3;->a(LX/0QB;)Lcom/facebook/compactdisk/TrashCollector;

    move-result-object v12

    check-cast v12, Lcom/facebook/compactdisk/TrashCollector;

    invoke-static {p0}, LX/2O4;->a(LX/0QB;)Lcom/facebook/compactdisk/PrivacyGuard;

    move-result-object v13

    check-cast v13, Lcom/facebook/compactdisk/PrivacyGuard;

    invoke-static {p0}, LX/2O5;->a(LX/0QB;)LX/2O6;

    move-result-object v14

    check-cast v14, LX/2O6;

    invoke-static/range {v0 .. v14}, LX/2FB;->a(Landroid/content/Context;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Lcom/facebook/compactdisk/AttributeStoreHolder;Lcom/facebook/compactdisk/Configuration;Lcom/facebook/compactdisk/DiskSizeCalculator;Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;Lcom/facebook/compactdisk/ConfigurationOverrides;Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/LazyDispatcher;Lcom/facebook/compactdisk/ExperimentManager;Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;Lcom/facebook/compactdisk/TaskQueueFactoryHolder;Lcom/facebook/compactdisk/TrashCollector;Lcom/facebook/compactdisk/PrivacyGuard;LX/2O6;)Lcom/facebook/compactdisk/StoreManagerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/StoreManagerFactory;
    .locals 3

    .prologue
    .line 400364
    sget-object v0, LX/2Nz;->a:Lcom/facebook/compactdisk/StoreManagerFactory;

    if-nez v0, :cond_1

    .line 400365
    const-class v1, LX/2Nz;

    monitor-enter v1

    .line 400366
    :try_start_0
    sget-object v0, LX/2Nz;->a:Lcom/facebook/compactdisk/StoreManagerFactory;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 400367
    if-eqz v2, :cond_0

    .line 400368
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2Nz;->b(LX/0QB;)Lcom/facebook/compactdisk/StoreManagerFactory;

    move-result-object v0

    sput-object v0, LX/2Nz;->a:Lcom/facebook/compactdisk/StoreManagerFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400369
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 400370
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 400371
    :cond_1
    sget-object v0, LX/2Nz;->a:Lcom/facebook/compactdisk/StoreManagerFactory;

    return-object v0

    .line 400372
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 400373
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/compactdisk/StoreManagerFactory;
    .locals 15

    .prologue
    .line 400361
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/2FE;->a(LX/0QB;)Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    move-result-object v1

    check-cast v1, Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    invoke-static {p0}, LX/2FD;->a(LX/0QB;)Lcom/facebook/compactdisk/AttributeStoreHolder;

    move-result-object v2

    check-cast v2, Lcom/facebook/compactdisk/AttributeStoreHolder;

    invoke-static {p0}, LX/2O0;->a(LX/0QB;)Lcom/facebook/compactdisk/Configuration;

    move-result-object v3

    check-cast v3, Lcom/facebook/compactdisk/Configuration;

    invoke-static {p0}, LX/2F8;->a(LX/0QB;)Lcom/facebook/compactdisk/DiskSizeCalculator;

    move-result-object v4

    check-cast v4, Lcom/facebook/compactdisk/DiskSizeCalculator;

    invoke-static {p0}, LX/2F9;->a(LX/0QB;)Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    move-result-object v5

    check-cast v5, Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;

    invoke-static {p0}, LX/2O1;->a(LX/0QB;)Lcom/facebook/compactdisk/ConfigurationOverrides;

    move-result-object v6

    check-cast v6, Lcom/facebook/compactdisk/ConfigurationOverrides;

    invoke-static {p0}, LX/2FA;->a(LX/0QB;)Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object v7

    check-cast v7, Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-static {p0}, LX/2IJ;->a(LX/0QB;)Lcom/facebook/compactdisk/LazyDispatcher;

    move-result-object v8

    check-cast v8, Lcom/facebook/compactdisk/LazyDispatcher;

    invoke-static {p0}, LX/2zW;->a(LX/0QB;)Lcom/facebook/compactdisk/ExperimentManager;

    move-result-object v9

    check-cast v9, Lcom/facebook/compactdisk/ExperimentManager;

    invoke-static {p0}, LX/2O2;->a(LX/0QB;)Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;

    move-result-object v10

    check-cast v10, Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;

    invoke-static {p0}, LX/30E;->a(LX/0QB;)Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    move-result-object v11

    check-cast v11, Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    invoke-static {p0}, LX/2O3;->a(LX/0QB;)Lcom/facebook/compactdisk/TrashCollector;

    move-result-object v12

    check-cast v12, Lcom/facebook/compactdisk/TrashCollector;

    invoke-static {p0}, LX/2O4;->a(LX/0QB;)Lcom/facebook/compactdisk/PrivacyGuard;

    move-result-object v13

    check-cast v13, Lcom/facebook/compactdisk/PrivacyGuard;

    invoke-static {p0}, LX/2O5;->a(LX/0QB;)LX/2O6;

    move-result-object v14

    check-cast v14, LX/2O6;

    invoke-static/range {v0 .. v14}, LX/2FB;->a(Landroid/content/Context;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Lcom/facebook/compactdisk/AttributeStoreHolder;Lcom/facebook/compactdisk/Configuration;Lcom/facebook/compactdisk/DiskSizeCalculator;Lcom/facebook/compactdisk/DiskSizeCalculatorHolder;Lcom/facebook/compactdisk/ConfigurationOverrides;Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/LazyDispatcher;Lcom/facebook/compactdisk/ExperimentManager;Lcom/facebook/compactdisk/StoreDirectoryNameBuilderFactory;Lcom/facebook/compactdisk/TaskQueueFactoryHolder;Lcom/facebook/compactdisk/TrashCollector;Lcom/facebook/compactdisk/PrivacyGuard;LX/2O6;)Lcom/facebook/compactdisk/StoreManagerFactory;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 400360
    invoke-direct {p0}, LX/2Nz;->a()Lcom/facebook/compactdisk/StoreManagerFactory;

    move-result-object v0

    return-object v0
.end method
