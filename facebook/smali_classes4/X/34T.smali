.class public LX/34T;
.super Landroid/text/style/ReplacementSpan;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/graphics/Rect;

.field private final d:I

.field private final e:Landroid/graphics/Paint$FontMetricsInt;

.field private final f:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;I)V
    .locals 1
    .param p2    # I
        .annotation build Lcom/facebook/widget/text/FbImageSpan$FbImageSpanAlignment;
        .end annotation
    .end param

    .prologue
    .line 495167
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 495168
    new-instance v0, Landroid/graphics/Paint$FontMetricsInt;

    invoke-direct {v0}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    iput-object v0, p0, LX/34T;->e:Landroid/graphics/Paint$FontMetricsInt;

    .line 495169
    iput-object p1, p0, LX/34T;->f:Landroid/graphics/drawable/Drawable;

    .line 495170
    iput p2, p0, LX/34T;->d:I

    .line 495171
    invoke-direct {p0}, LX/34T;->a()V

    .line 495172
    return-void
.end method

.method public static final a(I)I
    .locals 1
    .annotation build Lcom/facebook/widget/text/FbImageSpan$FbImageSpanAlignment;
    .end annotation

    .prologue
    .line 495163
    packed-switch p0, :pswitch_data_0

    .line 495164
    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 495165
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 495166
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/graphics/Paint$FontMetricsInt;)I
    .locals 2

    .prologue
    .line 495157
    iget v0, p0, LX/34T;->d:I

    packed-switch v0, :pswitch_data_0

    .line 495158
    :pswitch_0
    iget v0, p0, LX/34T;->b:I

    neg-int v0, v0

    :goto_0
    return v0

    .line 495159
    :pswitch_1
    iget v0, p1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v1, p0, LX/34T;->b:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 495160
    :pswitch_2
    iget v0, p1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v1, p1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v0, v1

    .line 495161
    iget v1, p0, LX/34T;->b:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 495162
    iget v1, p1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    add-int/2addr v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private a()V
    .locals 1

    .prologue
    .line 495153
    iget-object v0, p0, LX/34T;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, LX/34T;->c:Landroid/graphics/Rect;

    .line 495154
    iget-object v0, p0, LX/34T;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, LX/34T;->a:I

    .line 495155
    iget-object v0, p0, LX/34T;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, p0, LX/34T;->b:I

    .line 495156
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 2

    .prologue
    .line 495147
    iget-object v0, p0, LX/34T;->e:Landroid/graphics/Paint$FontMetricsInt;

    invoke-virtual {p9, v0}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    .line 495148
    iget-object v0, p0, LX/34T;->e:Landroid/graphics/Paint$FontMetricsInt;

    invoke-direct {p0, v0}, LX/34T;->a(Landroid/graphics/Paint$FontMetricsInt;)I

    move-result v0

    add-int/2addr v0, p7

    .line 495149
    int-to-float v1, v0

    invoke-virtual {p1, p5, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 495150
    iget-object v1, p0, LX/34T;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 495151
    neg-float v1, p5

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 495152
    return-void
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 3

    .prologue
    .line 495132
    invoke-direct {p0}, LX/34T;->a()V

    .line 495133
    if-nez p5, :cond_0

    .line 495134
    iget v0, p0, LX/34T;->a:I

    .line 495135
    :goto_0
    return v0

    .line 495136
    :cond_0
    invoke-direct {p0, p5}, LX/34T;->a(Landroid/graphics/Paint$FontMetricsInt;)I

    move-result v0

    .line 495137
    iget v1, p0, LX/34T;->b:I

    add-int/2addr v1, v0

    .line 495138
    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    if-ge v0, v2, :cond_1

    .line 495139
    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 495140
    :cond_1
    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    if-ge v0, v2, :cond_2

    .line 495141
    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 495142
    :cond_2
    iget v0, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    if-le v1, v0, :cond_3

    .line 495143
    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 495144
    :cond_3
    iget v0, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    if-le v1, v0, :cond_4

    .line 495145
    iput v1, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 495146
    :cond_4
    iget v0, p0, LX/34T;->a:I

    goto :goto_0
.end method
