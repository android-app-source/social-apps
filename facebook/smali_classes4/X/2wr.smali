.class public LX/2wr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2wq;


# instance fields
.field public final a:LX/2wm;

.field public final b:Ljava/util/concurrent/locks/Lock;

.field public final c:Landroid/content/Context;

.field public final d:LX/1od;

.field public e:Lcom/google/android/gms/common/ConnectionResult;

.field public f:I

.field public g:I

.field public h:I

.field private final i:Landroid/os/Bundle;

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2vo;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/3KI;

.field private l:I

.field public m:Z

.field public n:Z

.field public o:LX/4st;

.field public p:Z

.field public q:Z

.field public final r:LX/2wA;

.field private final s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/2vs",
            "<*>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/2vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vq",
            "<+",
            "LX/3KI;",
            "LX/2w4;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/concurrent/Future",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2wm;LX/2wA;Ljava/util/Map;LX/1od;LX/2vq;Ljava/util/concurrent/locks/Lock;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wm;",
            "LX/2wA;",
            "Ljava/util/Map",
            "<",
            "LX/2vs",
            "<*>;",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1od;",
            "LX/2vq",
            "<+",
            "LX/3KI;",
            "LX/2w4;",
            ">;",
            "Ljava/util/concurrent/locks/Lock;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LX/2wr;->g:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/2wr;->i:Landroid/os/Bundle;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2wr;->j:Ljava/util/Set;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2wr;->u:Ljava/util/ArrayList;

    iput-object p1, p0, LX/2wr;->a:LX/2wm;

    iput-object p2, p0, LX/2wr;->r:LX/2wA;

    iput-object p3, p0, LX/2wr;->s:Ljava/util/Map;

    iput-object p4, p0, LX/2wr;->d:LX/1od;

    iput-object p5, p0, LX/2wr;->t:LX/2vq;

    iput-object p6, p0, LX/2wr;->b:Ljava/util/concurrent/locks/Lock;

    iput-object p7, p0, LX/2wr;->c:Landroid/content/Context;

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, LX/2wr;->k:LX/3KI;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2wr;->k:LX/3KI;

    invoke-interface {v0}, LX/2wJ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, LX/2wr;->k:LX/3KI;

    invoke-interface {v0}, LX/3KI;->j_()V

    :cond_0
    iget-object v0, p0, LX/2wr;->k:LX/3KI;

    invoke-interface {v0}, LX/2wJ;->f()V

    const/4 v0, 0x0

    iput-object v0, p0, LX/2wr;->o:LX/4st;

    :cond_1
    return-void
.end method

.method public static a(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/2wr;->d:LX/1od;

    iget v2, p1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v2, v2

    const/4 p0, 0x0

    invoke-virtual {v1, p0, v2, p0}, LX/1od;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    move-object v1, p0

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;LX/2vs;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "LX/2vs",
            "<*>;I)V"
        }
    .end annotation

    const/4 v0, 0x2

    if-eq p3, v0, :cond_1

    invoke-virtual {p2}, LX/2vs;->a()LX/2vr;

    move-result-object v0

    invoke-virtual {v0}, LX/2vr;->a()I

    move-result v0

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-ne p3, v2, :cond_2

    invoke-static {p0, p1}, LX/2wr;->a(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    :goto_0
    move v1, v1

    if-eqz v1, :cond_1

    iput-object p1, p0, LX/2wr;->e:Lcom/google/android/gms/common/ConnectionResult;

    iput v0, p0, LX/2wr;->f:I

    :cond_1
    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->b:Ljava/util/Map;

    invoke-virtual {p2}, LX/2vs;->d()LX/2vo;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_2
    iget-object v3, p0, LX/2wr;->e:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v3, :cond_3

    iget v3, p0, LX/2wr;->f:I

    if-ge v0, v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public static b(LX/2wr;I)Z
    .locals 6

    iget v0, p0, LX/2wr;->g:I

    if-eq v0, p1, :cond_0

    const-string v0, "GoogleApiClientConnecting"

    iget-object v1, p0, LX/2wr;->a:LX/2wm;

    iget-object v1, v1, LX/2wm;->g:LX/2wW;

    invoke-virtual {v1}, LX/2wW;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "GoogleApiClientConnecting"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected callback in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "GoogleApiClientConnecting"

    iget v1, p0, LX/2wr;->h:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "mRemainingConnections="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "GoogleApiClientConnecting"

    iget v1, p0, LX/2wr;->g:I

    invoke-static {v1}, LX/2wr;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, LX/2wr;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x46

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "GoogleApiClient connecting is in step "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but received callback for step "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-static {p0, v0}, LX/2wr;->c(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b$redex0(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, LX/2wr;->l:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, LX/2wr;->l:I

    if-ne v1, v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "STEP_SERVICE_BINDINGS_AND_SIGN_IN"

    goto :goto_0

    :pswitch_1
    const-string v0, "STEP_GETTING_REMOTE_SERVICE"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1

    invoke-direct {p0}, LX/2wr;->i()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, LX/2wr;->a(Z)V

    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    invoke-virtual {v0, p1}, LX/2wm;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->h:LX/2wY;

    invoke-interface {v0, p1}, LX/2wY;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/2wr;)Z
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, LX/2wr;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/2wr;->h:I

    iget v1, p0, LX/2wr;->h:I

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, LX/2wr;->h:I

    if-gez v1, :cond_1

    const-string v1, "GoogleApiClientConnecting"

    iget-object v2, p0, LX/2wr;->a:LX/2wm;

    iget-object v2, v2, LX/2wm;->g:LX/2wW;

    invoke-virtual {v2}, LX/2wW;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "GoogleApiClientConnecting"

    const-string v2, "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect."

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-static {v1, v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-static {p0, v1}, LX/2wr;->c(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, LX/2wr;->e:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/2wr;->a:LX/2wm;

    iget v2, p0, LX/2wr;->f:I

    iput v2, v1, LX/2wm;->f:I

    iget-object v1, p0, LX/2wr;->e:Lcom/google/android/gms/common/ConnectionResult;

    invoke-static {p0, v1}, LX/2wr;->c(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static e(LX/2wr;)V
    .locals 4

    iget v0, p0, LX/2wr;->h:I

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, LX/2wr;->m:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/2wr;->n:Z

    if-eqz v0, :cond_0

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, LX/2wr;->g:I

    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, LX/2wr;->h:I

    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vo;

    iget-object v3, p0, LX/2wr;->a:LX/2wm;

    iget-object v3, v3, LX/2wm;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {p0}, LX/2wr;->d(LX/2wr;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, LX/2wr;->g(LX/2wr;)V

    goto :goto_1

    :cond_4
    iget-object v3, p0, LX/2wr;->a:LX/2wm;

    iget-object v3, v3, LX/2wm;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wJ;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, LX/2wr;->u:Ljava/util/ArrayList;

    sget-object v2, LX/2wu;->a:Ljava/util/concurrent/ExecutorService;

    move-object v2, v2

    new-instance v3, Lcom/google/android/gms/internal/zzpw$zzc;

    invoke-direct {v3, p0, v1}, Lcom/google/android/gms/internal/zzpw$zzc;-><init>(LX/2wr;Ljava/util/ArrayList;)V

    const v1, 0x51299a1c

    invoke-static {v2, v3, v1}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    goto :goto_0
.end method

.method public static g(LX/2wr;)V
    .locals 3

    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    invoke-virtual {v0}, LX/2wm;->i()V

    sget-object v0, LX/2wu;->a:Ljava/util/concurrent/ExecutorService;

    move-object v0, v0

    new-instance v1, Lcom/google/android/gms/internal/zzpw$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/zzpw$1;-><init>(LX/2wr;)V

    const v2, 0x631d1b8d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    iget-object v0, p0, LX/2wr;->k:LX/3KI;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/2wr;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2wr;->k:LX/3KI;

    iget-object v1, p0, LX/2wr;->o:LX/4st;

    iget-boolean v2, p0, LX/2wr;->q:Z

    invoke-interface {v0, v1, v2}, LX/3KI;->a(LX/4st;Z)V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2wr;->a(Z)V

    :cond_1
    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vo;

    iget-object v2, p0, LX/2wr;->a:LX/2wm;

    iget-object v2, v2, LX/2wm;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wJ;

    invoke-interface {v0}, LX/2wJ;->f()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/2wr;->i:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, LX/2wr;->a:LX/2wm;

    iget-object v1, v1, LX/2wm;->h:LX/2wY;

    invoke-interface {v1, v0}, LX/2wY;->a(Landroid/os/Bundle;)V

    return-void

    :cond_3
    iget-object v0, p0, LX/2wr;->i:Landroid/os/Bundle;

    goto :goto_1
.end method

.method public static h(LX/2wr;)V
    .locals 6

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2wr;->m:Z

    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->g:LX/2wW;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    iput-object v1, v0, LX/2wW;->d:Ljava/util/Set;

    iget-object v0, p0, LX/2wr;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vo;

    iget-object v2, p0, LX/2wr;->a:LX/2wm;

    iget-object v2, v2, LX/2wm;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/2wr;->a:LX/2wm;

    iget-object v2, v2, LX/2wm;->b:Ljava/util/Map;

    new-instance v3, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v4, 0x11

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private i()V
    .locals 3

    iget-object v0, p0, LX/2wr;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, LX/2wr;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method


# virtual methods
.method public final a(LX/2we;)LX/2we;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LX/2wK;",
            "R::",
            "LX/2NW;",
            "T:",
            "LX/2we",
            "<TR;TA;>;>(TT;)TT;"
        }
    .end annotation

    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->g:LX/2wW;

    iget-object v0, v0, LX/2wW;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-object p1
.end method

.method public final a()V
    .locals 10

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iput-boolean v4, p0, LX/2wr;->m:Z

    const/4 v0, 0x0

    iput-object v0, p0, LX/2wr;->e:Lcom/google/android/gms/common/ConnectionResult;

    iput v4, p0, LX/2wr;->g:I

    const/4 v0, 0x2

    iput v0, p0, LX/2wr;->l:I

    iput-boolean v4, p0, LX/2wr;->n:Z

    iput-boolean v4, p0, LX/2wr;->p:Z

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, LX/2wr;->s:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v4

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vs;

    iget-object v1, p0, LX/2wr;->a:LX/2wm;

    iget-object v1, v1, LX/2wm;->a:Ljava/util/Map;

    invoke-virtual {v0}, LX/2vs;->d()LX/2vo;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2wJ;

    iget-object v2, p0, LX/2wr;->s:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v0}, LX/2vs;->a()LX/2vr;

    move-result-object v2

    invoke-virtual {v2}, LX/2vr;->a()I

    move-result v2

    if-ne v2, v5, :cond_2

    move v2, v5

    :goto_1
    or-int/2addr v2, v3

    invoke-interface {v1}, LX/2wJ;->n()Z

    move-result v3

    if-eqz v3, :cond_1

    iput-boolean v5, p0, LX/2wr;->m:Z

    iget v3, p0, LX/2wr;->l:I

    if-ge v8, v3, :cond_0

    iput v8, p0, LX/2wr;->l:I

    :cond_0
    if-eqz v8, :cond_1

    iget-object v3, p0, LX/2wr;->j:Ljava/util/Set;

    invoke-virtual {v0}, LX/2vs;->d()LX/2vo;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v3, LX/2ws;

    invoke-direct {v3, p0, v0, v8}, LX/2ws;-><init>(LX/2wr;LX/2vs;I)V

    invoke-interface {v7, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v3, v2

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_1

    :cond_3
    if-eqz v3, :cond_4

    iput-boolean v4, p0, LX/2wr;->m:Z

    :cond_4
    iget-boolean v0, p0, LX/2wr;->m:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/2wr;->r:LX/2wA;

    iget-object v1, p0, LX/2wr;->a:LX/2wm;

    iget-object v1, v1, LX/2wm;->g:LX/2wW;

    invoke-virtual {v1}, LX/2wW;->p()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/2wA;->j:Ljava/lang/Integer;

    new-instance v5, LX/4uh;

    invoke-direct {v5, p0}, LX/4uh;-><init>(LX/2wr;)V

    iget-object v0, p0, LX/2wr;->t:LX/2vq;

    iget-object v1, p0, LX/2wr;->c:Landroid/content/Context;

    iget-object v2, p0, LX/2wr;->a:LX/2wm;

    iget-object v2, v2, LX/2wm;->g:LX/2wW;

    invoke-virtual {v2}, LX/2wX;->c()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, LX/2wr;->r:LX/2wA;

    iget-object v4, p0, LX/2wr;->r:LX/2wA;

    iget-object v6, v4, LX/2wA;->i:LX/2w4;

    move-object v4, v6

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, LX/2vq;->a(Landroid/content/Context;Landroid/os/Looper;LX/2wA;Ljava/lang/Object;LX/1qf;LX/1qg;)LX/2wJ;

    move-result-object v0

    check-cast v0, LX/3KI;

    iput-object v0, p0, LX/2wr;->k:LX/3KI;

    :cond_5
    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, LX/2wr;->h:I

    iget-object v0, p0, LX/2wr;->u:Ljava/util/ArrayList;

    sget-object v1, LX/2wu;->a:Ljava/util/concurrent/ExecutorService;

    move-object v1, v1

    new-instance v2, Lcom/google/android/gms/internal/zzpw$zzb;

    invoke-direct {v2, p0, v7}, Lcom/google/android/gms/internal/zzpw$zzb;-><init>(LX/2wr;Ljava/util/Map;)V

    const v3, 0x26c71fbc    # 1.3817E-15f

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(I)V
    .locals 3

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-static {p0, v0}, LX/2wr;->c(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/2wr;->b(LX/2wr;I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, LX/2wr;->i:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_2
    invoke-static {p0}, LX/2wr;->d(LX/2wr;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/2wr;->g(LX/2wr;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;LX/2vs;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "LX/2vs",
            "<*>;I)V"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/2wr;->b(LX/2wr;I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1, p2, p3}, LX/2wr;->b(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;LX/2vs;I)V

    invoke-static {p0}, LX/2wr;->d(LX/2wr;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/2wr;->g(LX/2wr;)V

    goto :goto_0
.end method

.method public final b(LX/2we;)LX/2we;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LX/2wK;",
            "T:",
            "LX/2we",
            "<+",
            "LX/2NW;",
            "TA;>;>(TT;)TT;"
        }
    .end annotation

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GoogleApiClient is not connected yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()Z
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, LX/2wr;->i()V

    invoke-direct {p0, v2}, LX/2wr;->a(Z)V

    iget-object v0, p0, LX/2wr;->a:LX/2wm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2wm;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    return v2
.end method

.method public final c()V
    .locals 0

    return-void
.end method
