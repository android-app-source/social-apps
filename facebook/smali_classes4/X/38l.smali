.class public final LX/38l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/38f;


# instance fields
.field public final synthetic a:LX/37Y;


# direct methods
.method public constructor <init>(LX/37Y;)V
    .locals 0

    .prologue
    .line 521293
    iput-object p1, p0, LX/38l;->a:LX/37Y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 521305
    iget-object v0, p0, LX/38l;->a:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->g()LX/38p;

    move-result-object v0

    .line 521306
    invoke-virtual {v0}, LX/38p;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 521307
    iget-object v0, p0, LX/38l;->a:LX/37Y;

    iget-object v0, v0, LX/37Y;->k:LX/37Z;

    iget-object v1, p0, LX/38l;->a:LX/37Y;

    iget-object v1, v1, LX/37Y;->l:LX/37g;

    .line 521308
    iget-object v2, v1, LX/37g;->m:LX/2wX;

    move-object v1, v2

    .line 521309
    iget-object v2, v0, LX/37Z;->g:LX/37b;

    .line 521310
    iget-object v3, v2, LX/37b;->b:LX/37c;

    sget-object v4, LX/37c;->CONNECTED:LX/37c;

    if-ne v3, v4, :cond_5

    const/4 v3, 0x1

    :goto_0
    move v2, v3

    .line 521311
    if-eqz v2, :cond_3

    .line 521312
    :cond_0
    :goto_1
    iget-object v0, p0, LX/38l;->a:LX/37Y;

    iget-object v0, v0, LX/37Y;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Iw;

    .line 521313
    invoke-interface {v0}, LX/7Iw;->b()V

    goto :goto_2

    .line 521314
    :cond_1
    invoke-virtual {v0}, LX/38p;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521315
    iget-object v0, p0, LX/38l;->a:LX/37Y;

    .line 521316
    iget-object v1, v0, LX/37Y;->p:LX/38g;

    const/4 v5, 0x0

    .line 521317
    invoke-virtual {v1}, LX/38g;->a()Z

    move-result v2

    if-nez v2, :cond_7

    .line 521318
    :goto_3
    invoke-static {v0}, LX/37Y;->B(LX/37Y;)V

    .line 521319
    iget-object v0, p0, LX/38l;->a:LX/37Y;

    iget-object v0, v0, LX/37Y;->k:LX/37Z;

    invoke-virtual {v0}, LX/37Z;->b()V

    .line 521320
    iget-object v0, p0, LX/38l;->a:LX/37Y;

    iget-object v0, v0, LX/37Y;->g:LX/37a;

    invoke-virtual {v0}, LX/37a;->a()V

    goto :goto_1

    .line 521321
    :cond_2
    return-void

    .line 521322
    :cond_3
    iput-object v1, v0, LX/37Z;->j:LX/2wX;

    .line 521323
    iget-object v2, v0, LX/37Z;->c:LX/37d;

    .line 521324
    iget-object v3, v2, LX/37d;->a:Ljava/lang/String;

    move-object v2, v3

    .line 521325
    iget-object v3, v0, LX/37Z;->g:LX/37b;

    .line 521326
    iget-object v4, v3, LX/37b;->b:LX/37c;

    sget-object v5, LX/37c;->CONNECTING:LX/37c;

    if-ne v4, v5, :cond_6

    const/4 v4, 0x1

    :goto_4
    move v3, v4

    .line 521327
    if-eqz v3, :cond_4

    .line 521328
    sget-object v3, LX/7Yq;->c:LX/7Yl;

    invoke-virtual {v0}, LX/37Z;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v1, v2, v4}, LX/7Yl;->b(LX/2wX;Ljava/lang/String;Ljava/lang/String;)LX/2wg;

    move-result-object v2

    new-instance v3, LX/7J4;

    invoke-direct {v3, v0}, LX/7J4;-><init>(LX/37Z;)V

    invoke-virtual {v2, v3}, LX/2wg;->a(LX/27U;)V

    goto :goto_1

    .line 521329
    :cond_4
    iget-object v3, v0, LX/37Z;->g:LX/37b;

    sget-object v4, LX/37c;->CONNECTING:LX/37c;

    invoke-static {v3, v4}, LX/37b;->a$redex0(LX/37b;LX/37c;)V

    .line 521330
    sget-object v3, LX/7Yq;->c:LX/7Yl;

    const/4 v4, 0x0

    invoke-interface {v3, v1, v2, v4}, LX/7Yl;->a(LX/2wX;Ljava/lang/String;Z)LX/2wg;

    move-result-object v2

    new-instance v3, LX/7J5;

    invoke-direct {v3, v0}, LX/7J5;-><init>(LX/37Z;)V

    invoke-virtual {v2, v3}, LX/2wg;->a(LX/27U;)V

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_0

    :cond_6
    const/4 v4, 0x0

    goto :goto_4

    .line 521331
    :cond_7
    iget-object v2, v1, LX/38g;->f:LX/7JI;

    .line 521332
    iget-object v3, v2, LX/7JI;->c:Ljava/lang/String;

    move-object v2, v3

    .line 521333
    :try_start_0
    sget-object v3, LX/7Yq;->c:LX/7Yl;

    iget-object v4, v1, LX/38g;->g:LX/2wX;

    invoke-interface {v3, v4, v2}, LX/7Yl;->b(LX/2wX;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 521334
    :goto_5
    iput-object v5, v1, LX/38g;->f:LX/7JI;

    .line 521335
    iput-object v5, v1, LX/38g;->g:LX/2wX;

    .line 521336
    new-instance v2, LX/38i;

    invoke-direct {v2}, LX/38i;-><init>()V

    iput-object v2, v1, LX/38g;->h:LX/38i;

    goto :goto_3

    .line 521337
    :catch_0
    move-exception v2

    .line 521338
    :goto_6
    iget-object v3, v1, LX/38g;->c:LX/37e;

    sget-object v4, LX/7JJ;->CastPlayer_RemoveMessageReceivedCallbacks:LX/7JJ;

    invoke-virtual {v3, v4, v2}, LX/37e;->a(LX/7JJ;Ljava/lang/Exception;)V

    goto :goto_5

    .line 521339
    :catch_1
    move-exception v2

    goto :goto_6
.end method

.method public final a(ILjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 521294
    const/16 v2, 0x7d5

    if-ne p1, v2, :cond_0

    .line 521295
    iget-object v2, p0, LX/38l;->a:LX/37Y;

    invoke-virtual {v2}, LX/37Y;->g()LX/38p;

    move-result-object v2

    invoke-virtual {v2}, LX/38p;->c()Z

    move-result v2

    .line 521296
    if-nez v2, :cond_3

    .line 521297
    iget-object v2, p0, LX/38l;->a:LX/37Y;

    invoke-virtual {v2}, LX/37Y;->g()LX/38p;

    move-result-object v2

    invoke-virtual {v2}, LX/38p;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/38l;->a:LX/37Y;

    invoke-virtual {v2}, LX/37Y;->s()LX/38j;

    move-result-object v2

    invoke-virtual {v2}, LX/38j;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/38l;->a:LX/37Y;

    iget-object v2, v2, LX/37Y;->s:LX/7JN;

    if-nez v2, :cond_2

    iget-object v2, p0, LX/38l;->a:LX/37Y;

    iget-object v2, v2, LX/37Y;->r:LX/7JN;

    if-nez v2, :cond_2

    move v2, v0

    .line 521298
    :goto_0
    if-nez v2, :cond_3

    .line 521299
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 521300
    iget-object v0, p0, LX/38l;->a:LX/37Y;

    iget-object v0, v0, LX/37Y;->c:LX/37f;

    invoke-virtual {v0, p1, p2}, LX/37f;->a(ILjava/lang/String;)V

    .line 521301
    :cond_1
    iget-object v0, p0, LX/38l;->a:LX/37Y;

    iget-object v0, v0, LX/37Y;->k:LX/37Z;

    invoke-virtual {v0, p1, p2}, LX/37Z;->a(ILjava/lang/String;)V

    .line 521302
    return-void

    :cond_2
    move v2, v1

    .line 521303
    goto :goto_0

    :cond_3
    move v0, v1

    .line 521304
    goto :goto_1
.end method
