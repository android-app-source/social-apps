.class public LX/2y3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1qb;


# direct methods
.method public constructor <init>(LX/1qb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 480579
    iput-object p1, p0, LX/2y3;->a:LX/1qb;

    .line 480580
    return-void
.end method

.method public static a(LX/0QB;)LX/2y3;
    .locals 4

    .prologue
    .line 480565
    const-class v1, LX/2y3;

    monitor-enter v1

    .line 480566
    :try_start_0
    sget-object v0, LX/2y3;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480567
    sput-object v2, LX/2y3;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480568
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480569
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480570
    new-instance p0, LX/2y3;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v3

    check-cast v3, LX/1qb;

    invoke-direct {p0, v3}, LX/2y3;-><init>(LX/1qb;)V

    .line 480571
    move-object v0, p0

    .line 480572
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480573
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2y3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480574
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480575
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLImage;)Z
    .locals 2
    .param p1    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 480577
    iget-object v0, p0, LX/2y3;->a:LX/1qb;

    const v1, 0x3ff745d1

    invoke-virtual {v0, p1, v1}, LX/1qb;->a(Lcom/facebook/graphql/model/GraphQLImage;F)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 1

    .prologue
    .line 480576
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
