.class public final LX/3BC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/net/Uri;

.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;II)V
    .locals 0

    .prologue
    .line 527530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 527531
    iput-object p1, p0, LX/3BC;->a:Landroid/net/Uri;

    .line 527532
    iput p2, p0, LX/3BC;->b:I

    .line 527533
    iput p3, p0, LX/3BC;->c:I

    .line 527534
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 527535
    iget-object v0, p0, LX/3BC;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 527536
    instance-of v1, p1, LX/3BC;

    if-nez v1, :cond_1

    .line 527537
    :cond_0
    :goto_0
    return v0

    .line 527538
    :cond_1
    check-cast p1, LX/3BC;

    .line 527539
    iget-object v1, p0, LX/3BC;->a:Landroid/net/Uri;

    iget-object v2, p1, LX/3BC;->a:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, LX/3BC;->b:I

    iget v2, p1, LX/3BC;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/3BC;->c:I

    iget v2, p1, LX/3BC;->c:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 527540
    iget-object v0, p0, LX/3BC;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    .line 527541
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/3BC;->b:I

    add-int/2addr v0, v1

    .line 527542
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/3BC;->c:I

    add-int/2addr v0, v1

    .line 527543
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 527544
    const/4 v0, 0x0

    const-string v1, "%dx%d %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LX/3BC;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, LX/3BC;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/3BC;->a:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
