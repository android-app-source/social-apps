.class public LX/2gT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final additional_contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 448389
    new-instance v0, LX/1sv;

    const-string v1, "AdditionalContacts"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/2gT;->b:LX/1sv;

    .line 448390
    new-instance v0, LX/1sw;

    const-string v1, "additional_contacts"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/2gT;->c:LX/1sw;

    .line 448391
    sput-boolean v3, LX/2gT;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 448386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448387
    iput-object p1, p0, LX/2gT;->additional_contacts:Ljava/util/List;

    .line 448388
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 448366
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 448367
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 448368
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 448369
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AdditionalContacts"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 448370
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448371
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448372
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448373
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448374
    const-string v4, "additional_contacts"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448375
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448376
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448377
    iget-object v0, p0, LX/2gT;->additional_contacts:Ljava/util/List;

    if-nez v0, :cond_3

    .line 448378
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448379
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448380
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448381
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 448382
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 448383
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 448384
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 448385
    :cond_3
    iget-object v0, p0, LX/2gT;->additional_contacts:Ljava/util/List;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    .line 448392
    invoke-virtual {p1}, LX/1su;->a()V

    .line 448393
    iget-object v0, p0, LX/2gT;->additional_contacts:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 448394
    sget-object v0, LX/2gT;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 448395
    new-instance v0, LX/1u3;

    const/16 v1, 0xa

    iget-object v2, p0, LX/2gT;->additional_contacts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 448396
    iget-object v0, p0, LX/2gT;->additional_contacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 448397
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, LX/1su;->a(J)V

    goto :goto_0

    .line 448398
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 448399
    invoke-virtual {p1}, LX/1su;->b()V

    .line 448400
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 448351
    if-nez p1, :cond_1

    .line 448352
    :cond_0
    :goto_0
    return v0

    .line 448353
    :cond_1
    instance-of v1, p1, LX/2gT;

    if-eqz v1, :cond_0

    .line 448354
    check-cast p1, LX/2gT;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 448355
    if-nez p1, :cond_3

    .line 448356
    :cond_2
    :goto_1
    move v0, v2

    .line 448357
    goto :goto_0

    .line 448358
    :cond_3
    iget-object v0, p0, LX/2gT;->additional_contacts:Ljava/util/List;

    if-eqz v0, :cond_6

    move v0, v1

    .line 448359
    :goto_2
    iget-object v3, p1, LX/2gT;->additional_contacts:Ljava/util/List;

    if-eqz v3, :cond_7

    move v3, v1

    .line 448360
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 448361
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 448362
    iget-object v0, p0, LX/2gT;->additional_contacts:Ljava/util/List;

    iget-object v3, p1, LX/2gT;->additional_contacts:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 448363
    goto :goto_1

    :cond_6
    move v0, v2

    .line 448364
    goto :goto_2

    :cond_7
    move v3, v2

    .line 448365
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 448350
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 448347
    sget-boolean v0, LX/2gT;->a:Z

    .line 448348
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/2gT;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 448349
    return-object v0
.end method
