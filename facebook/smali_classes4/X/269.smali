.class public final LX/269;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/0yc;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/269;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 370743
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 370744
    return-void
.end method

.method public static a(LX/0QB;)LX/269;
    .locals 4

    .prologue
    .line 370745
    sget-object v0, LX/269;->a:LX/269;

    if-nez v0, :cond_1

    .line 370746
    const-class v1, LX/269;

    monitor-enter v1

    .line 370747
    :try_start_0
    sget-object v0, LX/269;->a:LX/269;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 370748
    if-eqz v2, :cond_0

    .line 370749
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 370750
    new-instance v3, LX/269;

    const/16 p0, 0x4e0

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/269;-><init>(LX/0Ot;)V

    .line 370751
    move-object v0, v3

    .line 370752
    sput-object v0, LX/269;->a:LX/269;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 370753
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 370754
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 370755
    :cond_1
    sget-object v0, LX/269;->a:LX/269;

    return-object v0

    .line 370756
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 370757
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 370758
    check-cast p3, LX/0yc;

    .line 370759
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 370760
    const-string v1, "com.facebook.zero.ZERO_RATING_DISABLED_ON_WIFI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 370761
    invoke-virtual {p3}, LX/0yc;->g()V

    .line 370762
    :cond_0
    :goto_0
    return-void

    .line 370763
    :cond_1
    const-string v1, "com.facebook.zero.ZERO_RATING_STATE_UNREGISTERED_REASON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 370764
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "unregistered_reason"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 370765
    invoke-virtual {p3, v0}, LX/0yc;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 370766
    :cond_2
    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370767
    invoke-virtual {p3}, LX/0yc;->h()V

    goto :goto_0
.end method
