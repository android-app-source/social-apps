.class public LX/2Pk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0Up;
.implements Lcom/facebook/omnistore/Omnistore$CollectionIndexerFunction;
.implements Lcom/facebook/omnistore/Omnistore$DeltaReceivedCallback;
.implements Lcom/facebook/omnistore/Omnistore$StoredProcedureResultCallback;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile sInstance__com_facebook_omnistore_module_OmnistoreComponentManager__INJECTED_BY_TemplateInjector:LX/2Pk;


# instance fields
.field private final mActiveComponents:LX/1Ei;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ei",
            "<",
            "Lcom/facebook/omnistore/CollectionName;",
            "LX/2cF;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mComponentMutex"
    .end annotation
.end field

.field public final mBackgroundExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mComponentMutex:Ljava/lang/Object;

.field private final mCounterLogger:LX/13Q;

.field public final mFbAppType:LX/00H;

.field private final mIdleExecutorService:Ljava/util/concurrent/ExecutorService;

.field public final mInitTimeBugReportInfo:LX/2Pt;

.field public volatile mIsOmnistoreStarted:Z

.field private final mLoggedInUserAuthDataStore:LX/0WJ;

.field public mOmnistore:Lcom/facebook/omnistore/Omnistore;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mOmnistoreMutex"
    .end annotation
.end field

.field public final mOmnistoreMutex:Ljava/lang/Object;

.field private final mOmnistoreOpener:LX/2Pl;

.field private final mStartOmnistoreRunnable:Ljava/lang/Runnable;

.field public final mStartupOmnistoreComponents:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2cF;",
            ">;"
        }
    .end annotation
.end field

.field private final mStoredProcedureComponentMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/omnistore/module/OmnistoreStoredProcedureComponent;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mStoredProcedureMutex"
    .end annotation
.end field

.field private final mStoredProcedureComponents:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/omnistore/module/OmnistoreStoredProcedureComponent;",
            ">;"
        }
    .end annotation
.end field

.field private final mStoredProcedureMutex:Ljava/lang/Object;

.field private final mViewerContextUserIdProvider:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Pl;Ljava/util/Set;Ljava/util/Set;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;LX/13Q;Ljava/util/concurrent/ExecutorService;LX/2Pt;LX/00H;LX/0WJ;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/omnistore/module/OmnistoreOpener;",
            "Ljava/util/Set",
            "<",
            "LX/2cF;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/omnistore/module/OmnistoreStoredProcedureComponent;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/13Q;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/2Pt;",
            "LX/00H;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406988
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/2Pk;->mOmnistoreMutex:Ljava/lang/Object;

    .line 406989
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/2Pk;->mComponentMutex:Ljava/lang/Object;

    .line 406990
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/2Pk;->mStoredProcedureMutex:Ljava/lang/Object;

    .line 406991
    new-instance v0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$1;

    invoke-direct {v0, p0}, Lcom/facebook/omnistore/module/OmnistoreComponentManager$1;-><init>(LX/2Pk;)V

    iput-object v0, p0, LX/2Pk;->mStartOmnistoreRunnable:Ljava/lang/Runnable;

    .line 406992
    iput-object p1, p0, LX/2Pk;->mOmnistoreOpener:LX/2Pl;

    .line 406993
    iput-object p2, p0, LX/2Pk;->mStartupOmnistoreComponents:Ljava/util/Set;

    .line 406994
    iput-object p3, p0, LX/2Pk;->mStoredProcedureComponents:Ljava/util/Set;

    .line 406995
    iput-object p4, p0, LX/2Pk;->mViewerContextUserIdProvider:LX/0Or;

    .line 406996
    iput-object p5, p0, LX/2Pk;->mBackgroundExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    .line 406997
    iput-object p6, p0, LX/2Pk;->mCounterLogger:LX/13Q;

    .line 406998
    iput-object p7, p0, LX/2Pk;->mIdleExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 406999
    iput-object p8, p0, LX/2Pk;->mInitTimeBugReportInfo:LX/2Pt;

    .line 407000
    iput-object p9, p0, LX/2Pk;->mFbAppType:LX/00H;

    .line 407001
    iput-object p10, p0, LX/2Pk;->mLoggedInUserAuthDataStore:LX/0WJ;

    .line 407002
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, LX/1Ei;->a(I)LX/1Ei;

    move-result-object v0

    iput-object v0, p0, LX/2Pk;->mActiveComponents:LX/1Ei;

    .line 407003
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2Pk;->mStoredProcedureComponentMap:Ljava/util/HashMap;

    .line 407004
    return-void
.end method

.method public static getInstance__com_facebook_omnistore_module_OmnistoreComponentManager__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pk;
    .locals 14

    .prologue
    .line 407170
    sget-object v0, LX/2Pk;->sInstance__com_facebook_omnistore_module_OmnistoreComponentManager__INJECTED_BY_TemplateInjector:LX/2Pk;

    if-nez v0, :cond_1

    .line 407171
    const-class v1, LX/2Pk;

    monitor-enter v1

    .line 407172
    :try_start_0
    sget-object v0, LX/2Pk;->sInstance__com_facebook_omnistore_module_OmnistoreComponentManager__INJECTED_BY_TemplateInjector:LX/2Pk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 407173
    if-eqz v2, :cond_0

    .line 407174
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 407175
    new-instance v3, LX/2Pk;

    invoke-static {v0}, LX/2Pl;->createInstance__com_facebook_omnistore_module_DefaultOmnistoreOpener__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pl;

    move-result-object v4

    check-cast v4, LX/2Pl;

    invoke-static {v0}, LX/2Pr;->getSet(LX/0QB;)Ljava/util/Set;

    move-result-object v5

    invoke-static {v0}, LX/2Ps;->getSet(LX/0QB;)Ljava/util/Set;

    move-result-object v6

    const/16 v7, 0x15e8

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v9

    check-cast v9, LX/13Q;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/2Pt;->getInstance__com_facebook_omnistore_module_OmnistoreInitTimeBugReportInfo__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pt;

    move-result-object v11

    check-cast v11, LX/2Pt;

    const-class v12, LX/00H;

    invoke-interface {v0, v12}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/00H;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v13

    check-cast v13, LX/0WJ;

    invoke-direct/range {v3 .. v13}, LX/2Pk;-><init>(LX/2Pl;Ljava/util/Set;Ljava/util/Set;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;LX/13Q;Ljava/util/concurrent/ExecutorService;LX/2Pt;LX/00H;LX/0WJ;)V

    .line 407176
    move-object v0, v3

    .line 407177
    sput-object v0, LX/2Pk;->sInstance__com_facebook_omnistore_module_OmnistoreComponentManager__INJECTED_BY_TemplateInjector:LX/2Pk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 407178
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 407179
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 407180
    :cond_1
    sget-object v0, LX/2Pk;->sInstance__com_facebook_omnistore_module_OmnistoreComponentManager__INJECTED_BY_TemplateInjector:LX/2Pk;

    return-object v0

    .line 407181
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 407182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public checkComponentSubscription(LX/2cF;)V
    .locals 2

    .prologue
    .line 407163
    iget-object v0, p0, LX/2Pk;->mStartupOmnistoreComponents:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "All component instances must be registered in the OmnistoreComponent multibind"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 407164
    iget-object v1, p0, LX/2Pk;->mOmnistoreMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 407165
    :try_start_0
    iget-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    if-nez v0, :cond_0

    .line 407166
    monitor-exit v1

    .line 407167
    :goto_0
    return-void

    .line 407168
    :cond_0
    invoke-virtual {p0, p1}, LX/2Pk;->maybeUpdateComponent(LX/2cF;)V

    .line 407169
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearUserData()V
    .locals 4

    .prologue
    .line 407137
    iget-object v1, p0, LX/2Pk;->mOmnistoreMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 407138
    :try_start_0
    iget-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    if-nez v0, :cond_0

    .line 407139
    monitor-exit v1

    .line 407140
    :goto_0
    return-void

    .line 407141
    :cond_0
    iget-object v2, p0, LX/2Pk;->mComponentMutex:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407142
    :try_start_1
    new-instance v0, Ljava/util/HashMap;

    iget-object v3, p0, LX/2Pk;->mActiveComponents:LX/1Ei;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 407143
    iget-object v3, p0, LX/2Pk;->mActiveComponents:LX/1Ei;

    invoke-virtual {v3}, LX/1Ei;->clear()V

    .line 407144
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 407145
    :try_start_2
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 407146
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 407147
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cF;

    invoke-interface {v0}, LX/2cF;->onCollectionInvalidated()V

    goto :goto_1

    .line 407148
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 407149
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0

    .line 407150
    :cond_1
    iget-object v2, p0, LX/2Pk;->mStoredProcedureMutex:Ljava/lang/Object;

    monitor-enter v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 407151
    :try_start_5
    iget-object v0, p0, LX/2Pk;->mStoredProcedureComponents:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PI;

    .line 407152
    invoke-virtual {v0}, LX/2PI;->onSenderInvalidated()V

    goto :goto_2

    .line 407153
    :catchall_2
    move-exception v0

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 407154
    :cond_2
    :try_start_7
    iget-object v0, p0, LX/2Pk;->mStoredProcedureComponentMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 407155
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 407156
    :try_start_8
    iget-object v0, p0, LX/2Pk;->mInitTimeBugReportInfo:LX/2Pt;

    .line 407157
    const-string v2, "omnistore_removed"

    invoke-static {v0, v2}, LX/2Pt;->logPoint(LX/2Pt;Ljava/lang/String;)V

    .line 407158
    iget-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-virtual {v0}, Lcom/facebook/omnistore/Omnistore;->remove()V

    .line 407159
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    .line 407160
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Pk;->mIsOmnistoreStarted:Z

    .line 407161
    iget-object v0, p0, LX/2Pk;->mCounterLogger:LX/13Q;

    const-string v2, "omnistore_remove_called"

    invoke-virtual {v0, v2}, LX/13Q;->a(Ljava/lang/String;)V

    .line 407162
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0
.end method

.method public getBugReportFiles(Ljava/io/File;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407115
    iget-object v2, p0, LX/2Pk;->mOmnistoreMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 407116
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 407117
    iget-object v1, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    if-nez v1, :cond_0

    .line 407118
    monitor-exit v2

    .line 407119
    :goto_0
    return-object v0

    .line 407120
    :cond_0
    new-instance v3, Ljava/io/File;

    const-string v1, "omnistore.txt"

    invoke-direct {v3, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 407121
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    .line 407122
    :try_start_1
    new-instance v5, Ljava/io/PrintWriter;

    invoke-direct {v5, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 407123
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v7, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-virtual {v7}, Lcom/facebook/omnistore/Omnistore;->getDebugInfo()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 407124
    iget-object v6, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-virtual {v6}, Lcom/facebook/omnistore/Omnistore;->getDebugInfo()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 407125
    invoke-virtual {v5}, Ljava/io/PrintWriter;->flush()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 407126
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 407127
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407128
    iget-object v1, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/omnistore/Omnistore;->writeBugReport(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 407129
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 407130
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 407131
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 407132
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_2
    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_3
    :try_start_5
    throw v0

    .line 407133
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 407134
    :catch_1
    move-exception v3

    :try_start_6
    invoke-static {v1, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_1
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    goto :goto_3

    .line 407135
    :cond_2
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 407136
    :catchall_2
    move-exception v0

    goto :goto_2
.end method

.method public getDebugInfo()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 407112
    iget-object v1, p0, LX/2Pk;->mOmnistoreMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 407113
    :try_start_0
    iget-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-virtual {v0}, Lcom/facebook/omnistore/Omnistore;->getDebugInfo()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 407114
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getIndexedFields(Lcom/facebook/omnistore/CollectionName;Ljava/lang/String;Ljava/lang/String;Ljava/nio/ByteBuffer;)Lcom/facebook/omnistore/IndexedFields;
    .locals 2

    .prologue
    .line 407106
    iget-object v1, p0, LX/2Pk;->mComponentMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 407107
    :try_start_0
    iget-object v0, p0, LX/2Pk;->mActiveComponents:LX/1Ei;

    invoke-virtual {v0, p1}, LX/1Ei;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cF;

    .line 407108
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407109
    if-nez v0, :cond_0

    new-instance v0, Lcom/facebook/omnistore/IndexedFields;

    invoke-direct {v0}, Lcom/facebook/omnistore/IndexedFields;-><init>()V

    :goto_0
    return-object v0

    .line 407110
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 407111
    :cond_0
    invoke-interface {v0, p2, p3, p4}, LX/2cF;->indexObject(Ljava/lang/String;Ljava/lang/String;Ljava/nio/ByteBuffer;)Lcom/facebook/omnistore/IndexedFields;

    move-result-object v0

    goto :goto_0
.end method

.method public init()V
    .locals 11

    .prologue
    .line 407078
    iget-object v0, p0, LX/2Pk;->mViewerContextUserIdProvider:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 407079
    sget-object v7, LX/2Pu;->APP_ID_MAP:Ljava/util/Map;

    iget-object v8, p0, LX/2Pk;->mFbAppType:LX/00H;

    invoke-virtual {v8}, LX/00H;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    move v1, v7

    .line 407080
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2Pk;->mLoggedInUserAuthDataStore:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 407081
    :cond_0
    if-eqz v0, :cond_1

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/2Pk;->mLoggedInUserAuthDataStore:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 407082
    const-string v0, "OmnistoreComponentManager_login"

    const-string v1, "ViewerContext and AuthDataStore should have some data"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 407083
    :cond_1
    :goto_0
    return-void

    .line 407084
    :cond_2
    iget-object v1, p0, LX/2Pk;->mOmnistoreMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 407085
    :try_start_0
    iget-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    if-eqz v0, :cond_3

    .line 407086
    monitor-exit v1

    goto :goto_0

    .line 407087
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 407088
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/2Pk;->mInitTimeBugReportInfo:LX/2Pt;

    .line 407089
    const-string v2, "omnistore_open"

    invoke-static {v0, v2}, LX/2Pt;->logPoint(LX/2Pt;Ljava/lang/String;)V

    .line 407090
    iget-object v0, p0, LX/2Pk;->mOmnistoreOpener:LX/2Pl;

    invoke-virtual {v0}, LX/2Pl;->openOmnistoreInstance()Lcom/facebook/omnistore/Omnistore;

    move-result-object v0

    iput-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    .line 407091
    iget-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-virtual {v0, p0}, Lcom/facebook/omnistore/Omnistore;->addDeltaReceivedCallback(Lcom/facebook/omnistore/Omnistore$DeltaReceivedCallback;)V

    .line 407092
    iget-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-virtual {v0, p0}, Lcom/facebook/omnistore/Omnistore;->setCollectionIndexerFunction(Lcom/facebook/omnistore/Omnistore$CollectionIndexerFunction;)V

    .line 407093
    iget-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-virtual {v0, p0}, Lcom/facebook/omnistore/Omnistore;->addStoredProcedureResultCallback(Lcom/facebook/omnistore/Omnistore$StoredProcedureResultCallback;)V

    .line 407094
    iget-object v2, p0, LX/2Pk;->mStoredProcedureMutex:Ljava/lang/Object;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 407095
    :try_start_2
    iget-object v0, p0, LX/2Pk;->mStoredProcedureComponents:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PI;

    .line 407096
    iget v4, v0, LX/2PI;->c:I

    move v4, v4

    .line 407097
    iget-object v5, p0, LX/2Pk;->mStoredProcedureComponentMap:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407098
    iget-object v5, p0, LX/2Pk;->mInitTimeBugReportInfo:LX/2Pt;

    .line 407099
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "omnistore_sp_available_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/2Pt;->logPoint(LX/2Pt;Ljava/lang/String;)V

    .line 407100
    new-instance v5, LX/2cC;

    invoke-direct {v5, p0, v4}, LX/2cC;-><init>(LX/2Pk;I)V

    invoke-virtual {v0, v5}, LX/2PI;->onSenderAvailable(LX/2cC;)V

    goto :goto_1

    .line 407101
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_4
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 407102
    :try_start_5
    iget-object v0, p0, LX/2Pk;->mStartupOmnistoreComponents:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cF;

    .line 407103
    invoke-virtual {p0, v0}, LX/2Pk;->maybeUpdateComponent(LX/2cF;)V

    goto :goto_2

    .line 407104
    :cond_5
    iget-object v0, p0, LX/2Pk;->mIdleExecutorService:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, LX/2Pk;->mStartOmnistoreRunnable:Ljava/lang/Runnable;

    const v3, -0x4acea3e3

    invoke-static {v0, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 407105
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0
.end method

.method public maybeUpdateComponent(LX/2cF;)V
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 407034
    iget-object v5, p0, LX/2Pk;->mOmnistoreMutex:Ljava/lang/Object;

    monitor-enter v5

    .line 407035
    :try_start_0
    iget-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 407036
    iget-object v0, p0, LX/2Pk;->mLoggedInUserAuthDataStore:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 407037
    iget-object v0, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-interface {p1, v0}, LX/2cF;->provideSubscriptionInfo(Lcom/facebook/omnistore/Omnistore;)LX/2cJ;

    move-result-object v6

    .line 407038
    sget-object v0, LX/2cL;->$SwitchMap$com$facebook$omnistore$module$OmnistoreComponent$SubscriptionState:[I

    iget-object v1, v6, LX/2cJ;->subscriptionState:LX/2cK;

    invoke-virtual {v1}, LX/2cK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 407039
    const/4 v0, 0x0

    const-string v1, "Unexpected Subscription action: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, v6, LX/2cJ;->subscriptionState:LX/2cK;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 407040
    monitor-exit v5

    :goto_0
    return-void

    .line 407041
    :pswitch_0
    iget-object v1, p0, LX/2Pk;->mComponentMutex:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407042
    :try_start_1
    iget-object v0, p0, LX/2Pk;->mActiveComponents:LX/1Ei;

    invoke-virtual {v0}, LX/1Ei;->a_()LX/0Ri;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Ri;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 407043
    :goto_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 407044
    if-eqz v3, :cond_0

    .line 407045
    :try_start_2
    iget-object v0, p0, LX/2Pk;->mInitTimeBugReportInfo:LX/2Pt;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 407046
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "omnistore_collection_ignored_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/2Pt;->logPoint(LX/2Pt;Ljava/lang/String;)V

    .line 407047
    invoke-interface {p1}, LX/2cF;->onCollectionInvalidated()V

    .line 407048
    :cond_0
    monitor-exit v5

    goto :goto_0

    .line 407049
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move v3, v2

    .line 407050
    goto :goto_1

    .line 407051
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0

    .line 407052
    :pswitch_1
    iget-object v0, v6, LX/2cJ;->collectionName:Lcom/facebook/omnistore/CollectionName;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/omnistore/CollectionName;

    .line 407053
    iget-object v1, p0, LX/2Pk;->mComponentMutex:Ljava/lang/Object;

    monitor-enter v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 407054
    :try_start_5
    iget-object v2, p0, LX/2Pk;->mActiveComponents:LX/1Ei;

    invoke-virtual {v2}, LX/1Ei;->a_()LX/0Ri;

    move-result-object v2

    invoke-interface {v2, p1}, LX/0Ri;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 407055
    iget-object v2, p0, LX/2Pk;->mInitTimeBugReportInfo:LX/2Pt;

    .line 407056
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "omnistore_collection_unsubscribed_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/omnistore/CollectionName;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/2Pt;->logPoint(LX/2Pt;Ljava/lang/String;)V

    .line 407057
    invoke-interface {p1}, LX/2cF;->onCollectionInvalidated()V

    .line 407058
    :cond_2
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 407059
    :try_start_6
    iget-object v1, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-virtual {v1, v0}, Lcom/facebook/omnistore/Omnistore;->unsubscribeCollection(Lcom/facebook/omnistore/CollectionName;)V

    .line 407060
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 407061
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v0

    .line 407062
    :pswitch_2
    iget-object v0, v6, LX/2cJ;->collectionName:Lcom/facebook/omnistore/CollectionName;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/omnistore/CollectionName;

    .line 407063
    iget-object v7, p0, LX/2Pk;->mComponentMutex:Ljava/lang/Object;

    monitor-enter v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 407064
    :try_start_9
    iget-object v1, p0, LX/2Pk;->mActiveComponents:LX/1Ei;

    invoke-virtual {v1, v0, p1}, LX/1Ei;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2cF;

    .line 407065
    if-nez v1, :cond_5

    move v4, v3

    .line 407066
    :goto_2
    if-eq p1, v1, :cond_3

    if-eqz v4, :cond_6

    :cond_3
    move v1, v3

    :goto_3
    const-string v2, "Two components are trying to use the same collection name: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v3, v8

    invoke-static {v1, v2, v3}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 407067
    monitor-exit v7
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 407068
    :try_start_a
    iget-object v2, p0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    iget-object v1, v6, LX/2cJ;->subscriptionParams:LX/2cY;

    if-nez v1, :cond_7

    new-instance v1, LX/2cX;

    invoke-direct {v1}, LX/2cX;-><init>()V

    invoke-virtual {v1}, LX/2cX;->build()LX/2cY;

    move-result-object v1

    :goto_4
    invoke-virtual {v2, v0, v1}, Lcom/facebook/omnistore/Omnistore;->subscribeCollection(Lcom/facebook/omnistore/CollectionName;LX/2cY;)Lcom/facebook/omnistore/Collection;

    move-result-object v1

    .line 407069
    if-eqz v4, :cond_4

    .line 407070
    iget-object v2, p0, LX/2Pk;->mInitTimeBugReportInfo:LX/2Pt;

    .line 407071
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "omnistore_collection_available_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/omnistore/CollectionName;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/2Pt;->logPoint(LX/2Pt;Ljava/lang/String;)V

    .line 407072
    invoke-interface {p1, v1}, LX/2cF;->onCollectionAvailable(Lcom/facebook/omnistore/Collection;)V

    .line 407073
    :cond_4
    monitor-exit v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    :cond_5
    move v4, v2

    .line 407074
    goto :goto_2

    :cond_6
    move v1, v2

    .line 407075
    goto :goto_3

    .line 407076
    :catchall_3
    move-exception v0

    :try_start_b
    monitor-exit v7
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :try_start_c
    throw v0

    .line 407077
    :cond_7
    iget-object v1, v6, LX/2cJ;->subscriptionParams:LX/2cY;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onDeltaReceived([Lcom/facebook/omnistore/Delta;)V
    .locals 7

    .prologue
    .line 407015
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 407016
    array-length v3, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, p1, v1

    .line 407017
    iget-object v0, v4, Lcom/facebook/omnistore/Delta;->mStatus:Lcom/facebook/omnistore/Delta$Status;

    move-object v0, v0

    .line 407018
    sget-object v5, Lcom/facebook/omnistore/Delta$Status;->LOCALLY_COMMITTED:Lcom/facebook/omnistore/Delta$Status;

    if-eq v0, v5, :cond_0

    sget-object v5, Lcom/facebook/omnistore/Delta$Status;->PERSISTED_REMOTE:Lcom/facebook/omnistore/Delta$Status;

    if-ne v0, v5, :cond_2

    .line 407019
    :cond_0
    iget-object v0, v4, Lcom/facebook/omnistore/Delta;->mCollectionName:Lcom/facebook/omnistore/CollectionName;

    move-object v5, v0

    .line 407020
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 407021
    if-nez v0, :cond_1

    .line 407022
    new-instance v0, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 407023
    invoke-virtual {v2, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407024
    :cond_1
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407025
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 407026
    :cond_3
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 407027
    iget-object v3, p0, LX/2Pk;->mComponentMutex:Ljava/lang/Object;

    monitor-enter v3

    .line 407028
    :try_start_0
    iget-object v1, p0, LX/2Pk;->mActiveComponents:LX/1Ei;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/1Ei;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2cF;

    .line 407029
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407030
    if-eqz v1, :cond_4

    .line 407031
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v1, v0}, LX/2cF;->onDeltasReceived(Ljava/util/List;)V

    goto :goto_1

    .line 407032
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 407033
    :cond_5
    return-void
.end method

.method public onStoredProcedureResult(ILjava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    .line 407008
    iget-object v1, p0, LX/2Pk;->mStoredProcedureMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 407009
    :try_start_0
    iget-object v0, p0, LX/2Pk;->mStoredProcedureComponentMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PI;

    .line 407010
    if-nez v0, :cond_0

    .line 407011
    monitor-exit v1

    .line 407012
    :goto_0
    return-void

    .line 407013
    :cond_0
    invoke-virtual {v0, p2}, LX/2PI;->onStoredProcedureResult(Ljava/nio/ByteBuffer;)V

    .line 407014
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startOmnistoreIfNotYet()V
    .locals 3

    .prologue
    .line 407005
    iget-boolean v0, p0, LX/2Pk;->mIsOmnistoreStarted:Z

    if-eqz v0, :cond_0

    .line 407006
    :goto_0
    return-void

    .line 407007
    :cond_0
    iget-object v0, p0, LX/2Pk;->mBackgroundExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/2Pk;->mStartOmnistoreRunnable:Ljava/lang/Runnable;

    const v2, 0x257034fd

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
