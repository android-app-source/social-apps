.class public LX/2Cn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Cn;


# instance fields
.field private final a:LX/1qw;

.field public final b:LX/0aG;


# direct methods
.method public constructor <init>(LX/0aG;LX/1qw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 383225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383226
    iput-object p1, p0, LX/2Cn;->b:LX/0aG;

    .line 383227
    iput-object p2, p0, LX/2Cn;->a:LX/1qw;

    .line 383228
    return-void
.end method

.method public static a(LX/0QB;)LX/2Cn;
    .locals 5

    .prologue
    .line 383229
    sget-object v0, LX/2Cn;->c:LX/2Cn;

    if-nez v0, :cond_1

    .line 383230
    const-class v1, LX/2Cn;

    monitor-enter v1

    .line 383231
    :try_start_0
    sget-object v0, LX/2Cn;->c:LX/2Cn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 383232
    if-eqz v2, :cond_0

    .line 383233
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 383234
    new-instance p0, LX/2Cn;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {v0}, LX/1qw;->a(LX/0QB;)LX/1qw;

    move-result-object v4

    check-cast v4, LX/1qw;

    invoke-direct {p0, v3, v4}, LX/2Cn;-><init>(LX/0aG;LX/1qw;)V

    .line 383235
    move-object v0, p0

    .line 383236
    sput-object v0, LX/2Cn;->c:LX/2Cn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 383237
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 383238
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 383239
    :cond_1
    sget-object v0, LX/2Cn;->c:LX/2Cn;

    return-object v0

    .line 383240
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 383241
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 2

    .prologue
    .line 383242
    iget-object v0, p0, LX/2Cn;->a:LX/1qw;

    new-instance v1, Lcom/facebook/api/feedcache/FeedCacheHelper$1;

    invoke-direct {v1, p0}, Lcom/facebook/api/feedcache/FeedCacheHelper$1;-><init>(LX/2Cn;)V

    invoke-virtual {v0, v1}, LX/1qw;->a(LX/1rV;)V

    .line 383243
    return-void
.end method
