.class public final enum LX/24P;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/24P;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/24P;

.field public static final enum DISMISSED:LX/24P;

.field public static final enum MAXIMIZED:LX/24P;

.field public static final enum MINIMIZED:LX/24P;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 367141
    new-instance v0, LX/24P;

    const-string v1, "MAXIMIZED"

    const-string v2, "maximized"

    invoke-direct {v0, v1, v3, v2}, LX/24P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/24P;->MAXIMIZED:LX/24P;

    .line 367142
    new-instance v0, LX/24P;

    const-string v1, "MINIMIZED"

    const-string v2, "minimized"

    invoke-direct {v0, v1, v4, v2}, LX/24P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/24P;->MINIMIZED:LX/24P;

    .line 367143
    new-instance v0, LX/24P;

    const-string v1, "DISMISSED"

    const-string v2, "dismissed"

    invoke-direct {v0, v1, v5, v2}, LX/24P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/24P;->DISMISSED:LX/24P;

    .line 367144
    const/4 v0, 0x3

    new-array v0, v0, [LX/24P;

    sget-object v1, LX/24P;->MAXIMIZED:LX/24P;

    aput-object v1, v0, v3

    sget-object v1, LX/24P;->MINIMIZED:LX/24P;

    aput-object v1, v0, v4

    sget-object v1, LX/24P;->DISMISSED:LX/24P;

    aput-object v1, v0, v5

    sput-object v0, LX/24P;->$VALUES:[LX/24P;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 367148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 367149
    iput-object p3, p0, LX/24P;->mName:Ljava/lang/String;

    .line 367150
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/24P;
    .locals 1

    .prologue
    .line 367147
    const-class v0, LX/24P;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/24P;

    return-object v0
.end method

.method public static values()[LX/24P;
    .locals 1

    .prologue
    .line 367146
    sget-object v0, LX/24P;->$VALUES:[LX/24P;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/24P;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 367145
    iget-object v0, p0, LX/24P;->mName:Ljava/lang/String;

    return-object v0
.end method
