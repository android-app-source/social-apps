.class public LX/3MT;
.super LX/0Tv;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 554620
    const-string v0, "sms_matching_schema"

    const/4 v1, 0x6

    new-instance v2, LX/3MR;

    invoke-direct {v2}, LX/3MR;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 554621
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 554622
    :goto_0
    if-ge p2, p3, :cond_1

    .line 554623
    add-int/lit8 v0, p2, 0x1

    .line 554624
    const/4 p0, 0x1

    if-ne p2, p0, :cond_2

    .line 554625
    const-string p0, "match_table"

    sget-object p2, LX/3MR;->d:LX/0U1;

    invoke-static {p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object p0

    const p2, 0x7a3d7d82

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const p0, 0x4bf7f3e8    # 3.2499664E7f

    invoke-static {p0}, LX/03h;->a(I)V

    .line 554626
    const-string p0, "match_table"

    sget-object p2, LX/3MR;->f:LX/0U1;

    invoke-static {p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object p0

    const p2, 0x245aa3c6

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const p0, 0x73b912e

    invoke-static {p0}, LX/03h;->a(I)V

    .line 554627
    :cond_0
    :goto_1
    move p2, v0

    .line 554628
    goto :goto_0

    .line 554629
    :cond_1
    return-void

    .line 554630
    :cond_2
    const/4 p0, 0x2

    if-ne p2, p0, :cond_3

    .line 554631
    const-string p0, "match_table"

    sget-object p2, LX/3MR;->e:LX/0U1;

    invoke-static {p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object p0

    const p2, -0x4fb168c

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const p0, 0x546847d4

    invoke-static {p0}, LX/03h;->a(I)V

    .line 554632
    goto :goto_1

    .line 554633
    :cond_3
    const/4 p0, 0x3

    if-ne p2, p0, :cond_4

    .line 554634
    const-string p0, "match_table"

    sget-object p2, LX/3MR;->g:LX/0U1;

    invoke-static {p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object p0

    const p2, -0x605bc864

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const p0, 0x4682ee1a

    invoke-static {p0}, LX/03h;->a(I)V

    .line 554635
    const-string p0, "match_table"

    sget-object p2, LX/3MR;->h:LX/0U1;

    invoke-static {p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object p0

    const p2, -0x1b2cb44b

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const p0, 0x3ad1aa76

    invoke-static {p0}, LX/03h;->a(I)V

    .line 554636
    goto :goto_1

    .line 554637
    :cond_4
    const/4 p0, 0x4

    if-ne p2, p0, :cond_5

    .line 554638
    const-string p0, "match_table"

    sget-object p2, LX/3MR;->i:LX/0U1;

    invoke-static {p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object p0

    const p2, 0x5e900d7e

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const p0, 0x937ae27

    invoke-static {p0}, LX/03h;->a(I)V

    .line 554639
    goto :goto_1

    .line 554640
    :cond_5
    const/4 p0, 0x5

    if-ne p2, p0, :cond_0

    .line 554641
    const-string p0, "match_table"

    sget-object p2, LX/3MR;->j:LX/0U1;

    invoke-static {p0, p2}, LX/0Tz;->a(Ljava/lang/String;LX/0U1;)Ljava/lang/String;

    move-result-object p0

    const p2, -0x5cdaa3a5

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const p0, 0x55daeb2c

    invoke-static {p0}, LX/03h;->a(I)V

    .line 554642
    goto/16 :goto_1
.end method
