.class public final LX/2FF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1iq;


# instance fields
.field public final synthetic a:LX/2Ei;


# direct methods
.method public constructor <init>(LX/2Ei;)V
    .locals 0

    .prologue
    .line 386656
    iput-object p1, p0, LX/2FF;->a:LX/2Ei;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    .line 386657
    iget-object v0, p0, LX/2FF;->a:LX/2Ei;

    const v7, 0x6e0004

    const/4 v2, -0x1

    const/high16 v5, -0x40800000    # -1.0f

    .line 386658
    iget-object v1, v0, LX/2Ei;->g:LX/0u7;

    invoke-virtual {v1}, LX/0u7;->a()F

    move-result v3

    .line 386659
    iget-boolean v1, v0, LX/2Ei;->m:Z

    if-nez v1, :cond_0

    iget-object v1, v0, LX/2Ei;->g:LX/0u7;

    invoke-virtual {v1}, LX/0u7;->d()LX/454;

    move-result-object v1

    sget-object v4, LX/454;->NOT_PLUGGED:LX/454;

    if-ne v1, v4, :cond_0

    cmpl-float v1, v3, v5

    if-eqz v1, :cond_0

    iget v1, v0, LX/2Ei;->b:F

    cmpl-float v1, v1, v5

    if-nez v1, :cond_3

    .line 386660
    :cond_0
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const-wide/16 v5, -0x1

    invoke-virtual {v1, v7, v5, v6}, LX/1BA;->a(IJ)V

    .line 386661
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/2Ei;->m:Z

    move v1, v2

    .line 386662
    :goto_0
    iget-object v4, v0, LX/2Ei;->f:LX/1BA;

    const v5, 0x6e001e

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v4, v5, v1, v2}, LX/1BA;->a(IJ)V

    .line 386663
    iput v3, v0, LX/2Ei;->b:F

    .line 386664
    iget-object v0, p0, LX/2FF;->a:LX/2Ei;

    .line 386665
    invoke-static {}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->b()LX/2EG;

    move-result-object v1

    .line 386666
    if-nez v1, :cond_4

    .line 386667
    :goto_1
    iget-object v0, p0, LX/2FF;->a:LX/2Ei;

    invoke-virtual {v0}, LX/2Ei;->b()V

    .line 386668
    iget-object v0, p0, LX/2FF;->a:LX/2Ei;

    invoke-virtual {v0}, LX/2Ei;->d()V

    .line 386669
    iget-object v0, p0, LX/2FF;->a:LX/2Ei;

    invoke-virtual {v0}, LX/2Ei;->e()V

    .line 386670
    iget-object v0, p0, LX/2FF;->a:LX/2Ei;

    const-wide/16 v1, 0x0

    .line 386671
    sget-object v3, LX/35U;->a:LX/35U;

    move-object v3, v3

    .line 386672
    if-nez v3, :cond_5

    .line 386673
    sget-object v1, LX/2Ei;->d:Ljava/lang/String;

    const-string v2, "The single instance of LocationStats is null"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 386674
    :goto_2
    iget-object v0, p0, LX/2FF;->a:LX/2Ei;

    .line 386675
    invoke-static {}, LX/0Bx;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 386676
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e0031

    .line 386677
    sget-object v3, LX/0Bx;->c:LX/0By;

    move-object v3, v3

    .line 386678
    iget-object v5, v3, LX/0By;->a:Landroid/util/SparseArray;

    invoke-static {v5}, LX/0By;->a(Landroid/util/SparseArray;)J

    move-result-wide v5

    iget-object v7, v3, LX/0By;->c:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v9, 0x0

    invoke-virtual {v7, v9, v10}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    move-result-wide v7

    add-long/2addr v5, v7

    move-wide v3, v5

    .line 386679
    invoke-virtual {v1, v2, v3, v4}, LX/1BA;->a(IJ)V

    .line 386680
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e0032

    .line 386681
    sget-object v3, LX/0Bx;->c:LX/0By;

    move-object v3, v3

    .line 386682
    iget-object v5, v3, LX/0By;->b:Landroid/util/SparseArray;

    invoke-static {v5}, LX/0By;->a(Landroid/util/SparseArray;)J

    move-result-wide v5

    iget-object v7, v3, LX/0By;->d:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v9, 0x0

    invoke-virtual {v7, v9, v10}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    move-result-wide v7

    add-long/2addr v5, v7

    move-wide v3, v5

    .line 386683
    invoke-virtual {v1, v2, v3, v4}, LX/1BA;->a(IJ)V

    .line 386684
    :cond_1
    iget-object v0, p0, LX/2FF;->a:LX/2Ei;

    .line 386685
    iget-object v1, v0, LX/2Ei;->k:LX/Ee3;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/2Ei;->k:LX/Ee3;

    const/4 v2, 0x0

    .line 386686
    :try_start_0
    invoke-virtual {v1}, LX/Ee3;->b()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 386687
    :goto_3
    move v1, v2

    .line 386688
    if-nez v1, :cond_7

    .line 386689
    :cond_2
    :goto_4
    return-void

    .line 386690
    :cond_3
    iget v1, v0, LX/2Ei;->b:F

    sub-float/2addr v1, v3

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 386691
    iget-object v4, v0, LX/2Ei;->f:LX/1BA;

    int-to-long v5, v1

    invoke-virtual {v4, v7, v5, v6}, LX/1BA;->a(IJ)V

    .line 386692
    iget v4, v0, LX/2Ei;->r:I

    mul-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x64

    goto/16 :goto_0

    .line 386693
    :cond_4
    invoke-virtual {v1}, LX/2EG;->a()D

    move-result-wide v3

    iget-object v2, v0, LX/2Ei;->a:LX/2EG;

    invoke-virtual {v2}, LX/2EG;->a()D

    move-result-wide v5

    sub-double/2addr v3, v5

    .line 386694
    invoke-virtual {v1}, LX/2EG;->b()D

    move-result-wide v5

    iget-object v2, v0, LX/2Ei;->a:LX/2EG;

    invoke-virtual {v2}, LX/2EG;->b()D

    move-result-wide v7

    sub-double/2addr v5, v7

    .line 386695
    invoke-virtual {v1}, LX/2EG;->d()D

    move-result-wide v7

    iget-object v2, v0, LX/2Ei;->a:LX/2EG;

    invoke-virtual {v2}, LX/2EG;->d()D

    move-result-wide v9

    sub-double/2addr v7, v9

    .line 386696
    invoke-virtual {v1}, LX/2EG;->e()D

    move-result-wide v9

    iget-object v2, v0, LX/2Ei;->a:LX/2EG;

    invoke-virtual {v2}, LX/2EG;->e()D

    move-result-wide v11

    sub-double/2addr v9, v11

    .line 386697
    iget-object v2, v0, LX/2Ei;->f:LX/1BA;

    const v11, 0x6e0001

    double-to-long v13, v3

    invoke-virtual {v2, v11, v13, v14}, LX/1BA;->a(IJ)V

    .line 386698
    iget-object v2, v0, LX/2Ei;->f:LX/1BA;

    const v11, 0x6e0002

    double-to-long v13, v5

    invoke-virtual {v2, v11, v13, v14}, LX/1BA;->a(IJ)V

    .line 386699
    iget-object v2, v0, LX/2Ei;->f:LX/1BA;

    const v11, 0x6e0003

    add-double/2addr v3, v5

    double-to-long v3, v3

    invoke-virtual {v2, v11, v3, v4}, LX/1BA;->a(IJ)V

    .line 386700
    iput-object v1, v0, LX/2Ei;->a:LX/2EG;

    .line 386701
    iget-object v2, v0, LX/2Ei;->f:LX/1BA;

    const v3, 0x6e001a

    double-to-long v5, v7

    invoke-virtual {v2, v3, v5, v6}, LX/1BA;->a(IJ)V

    .line 386702
    iget-object v2, v0, LX/2Ei;->f:LX/1BA;

    const v3, 0x6e001b

    double-to-long v5, v9

    invoke-virtual {v2, v3, v5, v6}, LX/1BA;->a(IJ)V

    .line 386703
    iget-object v2, v0, LX/2Ei;->f:LX/1BA;

    const v3, 0x6e001c

    add-double v5, v7, v9

    double-to-long v5, v5

    invoke-virtual {v2, v3, v5, v6}, LX/1BA;->a(IJ)V

    .line 386704
    iput-object v1, v0, LX/2Ei;->a:LX/2EG;

    goto/16 :goto_1

    .line 386705
    :cond_5
    invoke-virtual {v3}, LX/35U;->b()Ljava/util/Map;

    move-result-object v3

    .line 386706
    new-instance v9, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v9, v4}, LX/162;-><init>(LX/0mC;)V

    .line 386707
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v3, v1

    move-wide v5, v1

    move-wide v7, v1

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 386708
    new-instance v11, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v11, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 386709
    const-string v12, "context"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v11, v12, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 386710
    const-string v12, "coarse"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/35V;

    invoke-virtual {v2}, LX/35V;->a()J

    move-result-wide v13

    invoke-virtual {v11, v12, v13, v14}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 386711
    const-string v12, "medium"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/35V;

    invoke-virtual {v2}, LX/35V;->b()J

    move-result-wide v13

    invoke-virtual {v11, v12, v13, v14}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 386712
    const-string v12, "fine"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/35V;

    invoke-virtual {v2}, LX/35V;->c()J

    move-result-wide v13

    invoke-virtual {v11, v12, v13, v14}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 386713
    invoke-virtual {v9, v11}, LX/162;->a(LX/0lF;)LX/162;

    .line 386714
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/35V;

    invoke-virtual {v2}, LX/35V;->a()J

    move-result-wide v11

    add-long/2addr v7, v11

    .line 386715
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/35V;

    invoke-virtual {v2}, LX/35V;->b()J

    move-result-wide v11

    add-long/2addr v5, v11

    .line 386716
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/35V;

    invoke-virtual {v1}, LX/35V;->c()J

    move-result-wide v1

    add-long/2addr v1, v3

    move-wide v3, v1

    .line 386717
    goto :goto_5

    .line 386718
    :cond_6
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e0017

    invoke-virtual {v1, v2, v7, v8}, LX/1BA;->a(IJ)V

    .line 386719
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e0018

    invoke-virtual {v1, v2, v5, v6}, LX/1BA;->a(IJ)V

    .line 386720
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e0019

    invoke-virtual {v1, v2, v3, v4}, LX/1BA;->a(IJ)V

    .line 386721
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e0016

    invoke-virtual {v9}, LX/162;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1BA;->a(ILjava/lang/String;)V

    goto/16 :goto_2

    .line 386722
    :cond_7
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e0049

    iget-object v3, v0, LX/2Ei;->k:LX/Ee3;

    .line 386723
    iget-object v5, v3, LX/Ee3;->b:[J

    const/4 v6, 0x0

    aget-wide v5, v5, v6

    move-wide v3, v5

    .line 386724
    invoke-virtual {v1, v2, v3, v4}, LX/1BA;->a(IJ)V

    .line 386725
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e004a

    iget-object v3, v0, LX/2Ei;->k:LX/Ee3;

    .line 386726
    iget-object v5, v3, LX/Ee3;->b:[J

    const/4 v6, 0x1

    aget-wide v5, v5, v6

    move-wide v3, v5

    .line 386727
    invoke-virtual {v1, v2, v3, v4}, LX/1BA;->a(IJ)V

    .line 386728
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e004d

    iget-object v3, v0, LX/2Ei;->k:LX/Ee3;

    .line 386729
    iget-object v5, v3, LX/Ee3;->b:[J

    const/4 v6, 0x2

    aget-wide v5, v5, v6

    move-wide v3, v5

    .line 386730
    invoke-virtual {v1, v2, v3, v4}, LX/1BA;->a(IJ)V

    .line 386731
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e004b

    iget-object v3, v0, LX/2Ei;->k:LX/Ee3;

    .line 386732
    iget-object v5, v3, LX/Ee3;->b:[J

    const/4 v6, 0x3

    aget-wide v5, v5, v6

    move-wide v3, v5

    .line 386733
    invoke-virtual {v1, v2, v3, v4}, LX/1BA;->a(IJ)V

    .line 386734
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e0050

    iget-object v3, v0, LX/2Ei;->k:LX/Ee3;

    .line 386735
    iget-object v5, v3, LX/Ee3;->b:[J

    const/4 v6, 0x4

    aget-wide v5, v5, v6

    move-wide v3, v5

    .line 386736
    invoke-virtual {v1, v2, v3, v4}, LX/1BA;->a(IJ)V

    .line 386737
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e004f

    iget-object v3, v0, LX/2Ei;->k:LX/Ee3;

    .line 386738
    iget-object v5, v3, LX/Ee3;->b:[J

    const/4 v6, 0x5

    aget-wide v5, v5, v6

    move-wide v3, v5

    .line 386739
    invoke-virtual {v1, v2, v3, v4}, LX/1BA;->a(IJ)V

    .line 386740
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e004c

    iget-object v3, v0, LX/2Ei;->k:LX/Ee3;

    .line 386741
    iget-object v5, v3, LX/Ee3;->b:[J

    const/4 v6, 0x6

    aget-wide v5, v5, v6

    move-wide v3, v5

    .line 386742
    invoke-virtual {v1, v2, v3, v4}, LX/1BA;->a(IJ)V

    .line 386743
    iget-object v1, v0, LX/2Ei;->f:LX/1BA;

    const v2, 0x6e004e

    iget-object v3, v0, LX/2Ei;->k:LX/Ee3;

    .line 386744
    iget-object v5, v3, LX/Ee3;->b:[J

    const/4 v6, 0x7

    aget-wide v5, v5, v6

    move-wide v3, v5

    .line 386745
    invoke-virtual {v1, v2, v3, v4}, LX/1BA;->a(IJ)V

    goto/16 :goto_4

    .line 386746
    :catch_0
    move-exception v3

    .line 386747
    sget-object v4, LX/Ee3;->c:Ljava/lang/String;

    const-string v5, "Failed to get a snapshot"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v4, v3, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3
.end method
