.class public LX/2ns;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 465355
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "videohome/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 465356
    sput-object v0, LX/2ns;->a:LX/0Tn;

    const-string v1, "count_of_user_was_exposed_to_video_home_tab_icon"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2ns;->b:LX/0Tn;

    .line 465357
    sget-object v0, LX/2ns;->a:LX/0Tn;

    const-string v1, "has_user_entered_video_home_tab"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2ns;->c:LX/0Tn;

    .line 465358
    sget-object v0, LX/2ns;->a:LX/0Tn;

    const-string v1, "has_user_seen_notif_hscroll_section"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2ns;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 465359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465360
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 465361
    sget-object v0, LX/2ns;->b:LX/0Tn;

    sget-object v1, LX/2ns;->c:LX/0Tn;

    sget-object v2, LX/2ns;->d:LX/0Tn;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
