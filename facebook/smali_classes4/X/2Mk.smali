.class public LX/2Mk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397193
    iput-object p1, p0, LX/2Mk;->a:LX/0Or;

    .line 397194
    return-void
.end method

.method public static A(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397195
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 397196
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_DELETE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v1, p0, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 397197
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static B(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397198
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 397199
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_NOTIFY:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v1, p0, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 397200
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2Mk;
    .locals 4

    .prologue
    .line 397201
    const-class v1, LX/2Mk;

    monitor-enter v1

    .line 397202
    :try_start_0
    sget-object v0, LX/2Mk;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 397203
    sput-object v2, LX/2Mk;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 397204
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397205
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 397206
    new-instance v3, LX/2Mk;

    const/16 p0, 0x12cd

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2Mk;-><init>(LX/0Or;)V

    .line 397207
    move-object v0, v3

    .line 397208
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 397209
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2Mk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397210
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 397211
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397212
    sget-object v0, Lcom/facebook/messaging/model/messages/Publicity;->b:Lcom/facebook/messaging/model/messages/Publicity;

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->s:Lcom/facebook/messaging/model/messages/Publicity;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/model/messages/Publicity;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Lcom/facebook/messaging/model/messages/Message;)J
    .locals 8

    .prologue
    .line 397213
    iget-wide v4, p0, Lcom/facebook/messaging/model/messages/Message;->d:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    move v0, v4

    .line 397214
    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/facebook/messaging/model/messages/Message;->d:J

    iget-wide v2, p0, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-wide v0, p0, Lcom/facebook/messaging/model/messages/Message;->d:J

    :goto_1
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/facebook/messaging/model/messages/Message;->c:J

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)J
    .locals 4

    .prologue
    .line 397215
    invoke-static {p0}, LX/2Mk;->b(Lcom/facebook/messaging/model/messages/Message;)J

    move-result-wide v0

    invoke-static {p1}, LX/2Mk;->b(Lcom/facebook/messaging/model/messages/Message;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 397187
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397216
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->ADMIN:LX/2uW;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 397217
    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->b()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 397218
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397219
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->ADMIN:LX/2uW;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397220
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->ADMIN:LX/2uW;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397221
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->ADMIN:LX/2uW;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397222
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->ADMIN:LX/2uW;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397188
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 397189
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->STARTED_SHARING_VIDEO:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-eq v1, p0, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->PARTICIPANT_JOINED_GROUP_CALL:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v1, p0, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 397190
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 397191
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 397159
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    .line 397160
    sget-object p0, LX/2uW;->REGULAR:LX/2uW;

    if-eq v0, p0, :cond_0

    sget-object p0, LX/2uW;->PENDING_SEND:LX/2uW;

    if-eq v0, p0, :cond_0

    sget-object p0, LX/2uW;->FAILED_SEND:LX/2uW;

    if-eq v0, p0, :cond_0

    sget-object p0, LX/2uW;->GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:LX/2uW;

    if-eq v0, p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 397161
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static p(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397162
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 397163
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->GAME_SCORE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v1, p0, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 397164
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static q(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 397165
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 397166
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 397167
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static u(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397168
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v0}, LX/4m9;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    const-string v1, "227878347358915"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397169
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 397170
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_CREATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v1, p0, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 397171
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static y(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397172
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 397173
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-eq v1, p0, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_TIME:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-eq v1, p0, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_TITLE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-eq v1, p0, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_UPDATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v1, p0, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 397174
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static z(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397175
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 397176
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->LIGHTWEIGHT_EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    if-ne v1, p0, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 397177
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/user/model/UserKey;
    .locals 1

    .prologue
    .line 397178
    iget-object v0, p0, LX/2Mk;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    return-object v0
.end method

.method public final k(Lcom/facebook/messaging/model/messages/Message;)LX/6hS;
    .locals 2

    .prologue
    .line 397179
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->CALL_LOG:LX/2uW;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 397180
    new-instance v1, LX/6hR;

    iget-object v0, p0, LX/2Mk;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-direct {v1, v0}, LX/6hR;-><init>(Lcom/facebook/user/model/UserKey;)V

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/6hR;->a(LX/0Px;)LX/6hR;

    move-result-object v0

    invoke-virtual {v0}, LX/6hR;->a()LX/6hS;

    move-result-object v0

    .line 397181
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 397182
    invoke-virtual {p0, p1}, LX/2Mk;->k(Lcom/facebook/messaging/model/messages/Message;)LX/6hS;

    move-result-object v1

    .line 397183
    if-eqz v1, :cond_0

    .line 397184
    iget-boolean v2, v1, LX/6hS;->d:Z

    if-eqz v2, :cond_0

    iget-boolean v1, v1, LX/6hS;->c:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 397185
    :cond_0
    return v0
.end method

.method public final s(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 397186
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    iget-object v1, p0, LX/2Mk;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
