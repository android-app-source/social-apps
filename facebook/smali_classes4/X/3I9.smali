.class public final LX/3I9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3IA;
.implements LX/3IB;


# instance fields
.field public final synthetic a:LX/3I7;


# direct methods
.method public constructor <init>(LX/3I7;)V
    .locals 0

    .prologue
    .line 545604
    iput-object p1, p0, LX/3I9;->a:LX/3I7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 545605
    return-void
.end method

.method public final a(FF)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 545606
    iget-object v0, p0, LX/3I9;->a:LX/3I7;

    iget-boolean v0, v0, LX/3I7;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 545607
    iget-object v0, p0, LX/3I9;->a:LX/3I7;

    .line 545608
    iput-boolean v2, v0, LX/3I7;->e:Z

    .line 545609
    :cond_0
    iget-object v0, p0, LX/3I9;->a:LX/3I7;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lr;

    invoke-direct {v1, v2, p1, p2}, LX/7Lr;-><init>(IFF)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 545610
    return-void
.end method

.method public final a(F)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 545611
    iget-object v0, p0, LX/3I9;->a:LX/3I7;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lq;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LX/7Lq;-><init>(IF)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 545612
    iget-object v0, p0, LX/3I9;->a:LX/3I7;

    .line 545613
    iput-boolean v3, v0, LX/3I7;->f:Z

    .line 545614
    return v3
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 545615
    iget-object v0, p0, LX/3I9;->a:LX/3I7;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lr;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LX/7Lr;-><init>(I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 545616
    return-void
.end method

.method public final b(FF)V
    .locals 3

    .prologue
    .line 545617
    iget-object v0, p0, LX/3I9;->a:LX/3I7;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lr;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1, p2}, LX/7Lr;-><init>(IFF)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 545618
    iget-object v0, p0, LX/3I9;->a:LX/3I7;

    const/4 v1, 0x0

    .line 545619
    iput-boolean v1, v0, LX/3I7;->e:Z

    .line 545620
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 545621
    iget-object v0, p0, LX/3I9;->a:LX/3I7;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lq;

    invoke-direct {v1, v2}, LX/7Lq;-><init>(I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 545622
    return v2
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 545623
    iget-object v0, p0, LX/3I9;->a:LX/3I7;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lq;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, LX/7Lq;-><init>(I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 545624
    iget-object v0, p0, LX/3I9;->a:LX/3I7;

    const/4 v1, 0x0

    .line 545625
    iput-boolean v1, v0, LX/3I7;->f:Z

    .line 545626
    return-void
.end method
