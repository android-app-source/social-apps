.class public LX/32p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0lS;

.field public final b:Z

.field public c:LX/2Au;

.field public d:LX/2Au;

.field public e:LX/2Au;

.field public f:LX/2Au;

.field public g:LX/2Au;

.field public h:LX/2Au;

.field public i:LX/2Au;

.field public j:[LX/4q3;

.field public k:LX/2Au;

.field public l:[LX/4q3;

.field public m:LX/2Vd;


# direct methods
.method public constructor <init>(LX/0lS;Z)V
    .locals 1

    .prologue
    .line 491081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491082
    const/4 v0, 0x0

    iput-object v0, p0, LX/32p;->l:[LX/4q3;

    .line 491083
    iput-object p1, p0, LX/32p;->a:LX/0lS;

    .line 491084
    iput-boolean p2, p0, LX/32p;->b:Z

    .line 491085
    return-void
.end method

.method private a(LX/2An;)LX/2An;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/2An;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 491078
    if-eqz p1, :cond_0

    iget-boolean v0, p0, LX/32p;->b:Z

    if-eqz v0, :cond_0

    .line 491079
    invoke-virtual {p1}, LX/0lO;->a()Ljava/lang/reflect/AnnotatedElement;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Member;

    invoke-static {v0}, LX/1Xw;->a(Ljava/lang/reflect/Member;)V

    .line 491080
    :cond_0
    return-object p1
.end method

.method private a(LX/2Au;LX/2Au;Ljava/lang/String;)LX/2Au;
    .locals 3

    .prologue
    .line 491074
    if-eqz p2, :cond_0

    .line 491075
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 491076
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Conflicting "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " creators: already had "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", encountered "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 491077
    :cond_0
    invoke-direct {p0, p1}, LX/32p;->a(LX/2An;)LX/2An;

    move-result-object v0

    check-cast v0, LX/2Au;

    return-object v0
.end method

.method private a(LX/2Vc;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 491072
    invoke-direct {p0, p1}, LX/32p;->a(LX/2An;)LX/2An;

    move-result-object v0

    check-cast v0, LX/2Au;

    iput-object v0, p0, LX/32p;->c:LX/2Au;

    .line 491073
    return-void
.end method


# virtual methods
.method public final a(LX/0mu;)LX/320;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 491047
    new-instance v0, LX/32q;

    iget-object v1, p0, LX/32p;->a:LX/0lS;

    .line 491048
    iget-object v3, v1, LX/0lS;->a:LX/0lJ;

    move-object v1, v3

    .line 491049
    invoke-direct {v0, p1, v1}, LX/32q;-><init>(LX/0mu;LX/0lJ;)V

    .line 491050
    iget-object v1, p0, LX/32p;->i:LX/2Au;

    if-nez v1, :cond_0

    .line 491051
    const/4 v3, 0x0

    .line 491052
    :goto_0
    iget-object v1, p0, LX/32p;->c:LX/2Au;

    iget-object v2, p0, LX/32p;->i:LX/2Au;

    iget-object v4, p0, LX/32p;->j:[LX/4q3;

    iget-object v5, p0, LX/32p;->k:LX/2Au;

    iget-object v6, p0, LX/32p;->l:[LX/4q3;

    invoke-virtual/range {v0 .. v6}, LX/32q;->a(LX/2Au;LX/2Au;LX/0lJ;[LX/4q3;LX/2Au;[LX/4q3;)V

    .line 491053
    iget-object v1, p0, LX/32p;->d:LX/2Au;

    .line 491054
    iput-object v1, v0, LX/32q;->_fromStringCreator:LX/2Au;

    .line 491055
    iget-object v1, p0, LX/32p;->e:LX/2Au;

    .line 491056
    iput-object v1, v0, LX/32q;->_fromIntCreator:LX/2Au;

    .line 491057
    iget-object v1, p0, LX/32p;->f:LX/2Au;

    .line 491058
    iput-object v1, v0, LX/32q;->_fromLongCreator:LX/2Au;

    .line 491059
    iget-object v1, p0, LX/32p;->g:LX/2Au;

    .line 491060
    iput-object v1, v0, LX/32q;->_fromDoubleCreator:LX/2Au;

    .line 491061
    iget-object v1, p0, LX/32p;->h:LX/2Au;

    .line 491062
    iput-object v1, v0, LX/32q;->_fromBooleanCreator:LX/2Au;

    .line 491063
    iget-object v1, p0, LX/32p;->m:LX/2Vd;

    .line 491064
    iput-object v1, v0, LX/32q;->_incompleteParameter:LX/2Vd;

    .line 491065
    return-object v0

    .line 491066
    :cond_0
    iget-object v1, p0, LX/32p;->j:[LX/4q3;

    if-eqz v1, :cond_2

    .line 491067
    iget-object v1, p0, LX/32p;->j:[LX/4q3;

    array-length v3, v1

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_2

    .line 491068
    iget-object v4, p0, LX/32p;->j:[LX/4q3;

    aget-object v4, v4, v1

    if-nez v4, :cond_1

    .line 491069
    :goto_2
    iget-object v2, p0, LX/32p;->a:LX/0lS;

    invoke-virtual {v2}, LX/0lS;->f()LX/1Y3;

    move-result-object v2

    .line 491070
    iget-object v3, p0, LX/32p;->i:LX/2Au;

    invoke-virtual {v3, v1}, LX/2Au;->b(I)Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1Y3;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v3

    goto :goto_0

    .line 491071
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public final a(LX/2Au;)V
    .locals 1

    .prologue
    .line 491043
    instance-of v0, p1, LX/2Vc;

    if-eqz v0, :cond_0

    .line 491044
    check-cast p1, LX/2Vc;

    invoke-direct {p0, p1}, LX/32p;->a(LX/2Vc;)V

    .line 491045
    :goto_0
    return-void

    .line 491046
    :cond_0
    invoke-direct {p0, p1}, LX/32p;->a(LX/2An;)LX/2An;

    move-result-object v0

    check-cast v0, LX/2Au;

    iput-object v0, p0, LX/32p;->c:LX/2Au;

    goto :goto_0
.end method

.method public final a(LX/2Au;[LX/4q3;)V
    .locals 2

    .prologue
    .line 491037
    iget-object v0, p0, LX/32p;->i:LX/2Au;

    const-string v1, "delegate"

    invoke-direct {p0, p1, v0, v1}, LX/32p;->a(LX/2Au;LX/2Au;Ljava/lang/String;)LX/2Au;

    move-result-object v0

    iput-object v0, p0, LX/32p;->i:LX/2Au;

    .line 491038
    iput-object p2, p0, LX/32p;->j:[LX/4q3;

    .line 491039
    return-void
.end method

.method public final a(LX/2Vd;)V
    .locals 1

    .prologue
    .line 491040
    iget-object v0, p0, LX/32p;->m:LX/2Vd;

    if-nez v0, :cond_0

    .line 491041
    iput-object p1, p0, LX/32p;->m:LX/2Vd;

    .line 491042
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 491036
    iget-object v0, p0, LX/32p;->c:LX/2Au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/2Au;)V
    .locals 2

    .prologue
    .line 491013
    iget-object v0, p0, LX/32p;->d:LX/2Au;

    const-string v1, "String"

    invoke-direct {p0, p1, v0, v1}, LX/32p;->a(LX/2Au;LX/2Au;Ljava/lang/String;)LX/2Au;

    move-result-object v0

    iput-object v0, p0, LX/32p;->d:LX/2Au;

    .line 491014
    return-void
.end method

.method public final b(LX/2Au;[LX/4q3;)V
    .locals 6

    .prologue
    .line 491023
    iget-object v0, p0, LX/32p;->k:LX/2Au;

    const-string v1, "property-based"

    invoke-direct {p0, p1, v0, v1}, LX/32p;->a(LX/2Au;LX/2Au;Ljava/lang/String;)LX/2Au;

    move-result-object v0

    iput-object v0, p0, LX/32p;->k:LX/2Au;

    .line 491024
    array-length v0, p2

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 491025
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 491026
    const/4 v0, 0x0

    array-length v3, p2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 491027
    aget-object v0, p2, v1

    .line 491028
    iget-object v4, v0, LX/32s;->_propName:Ljava/lang/String;

    move-object v4, v4

    .line 491029
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    aget-object v0, p2, v1

    invoke-virtual {v0}, LX/32s;->d()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 491030
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 491031
    if-eqz v0, :cond_1

    .line 491032
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Duplicate creator property \""

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" (index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " vs "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 491033
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 491034
    :cond_2
    iput-object p2, p0, LX/32p;->l:[LX/4q3;

    .line 491035
    return-void
.end method

.method public final c(LX/2Au;)V
    .locals 2

    .prologue
    .line 491021
    iget-object v0, p0, LX/32p;->e:LX/2Au;

    const-string v1, "int"

    invoke-direct {p0, p1, v0, v1}, LX/32p;->a(LX/2Au;LX/2Au;Ljava/lang/String;)LX/2Au;

    move-result-object v0

    iput-object v0, p0, LX/32p;->e:LX/2Au;

    .line 491022
    return-void
.end method

.method public final d(LX/2Au;)V
    .locals 2

    .prologue
    .line 491019
    iget-object v0, p0, LX/32p;->f:LX/2Au;

    const-string v1, "long"

    invoke-direct {p0, p1, v0, v1}, LX/32p;->a(LX/2Au;LX/2Au;Ljava/lang/String;)LX/2Au;

    move-result-object v0

    iput-object v0, p0, LX/32p;->f:LX/2Au;

    .line 491020
    return-void
.end method

.method public final e(LX/2Au;)V
    .locals 2

    .prologue
    .line 491017
    iget-object v0, p0, LX/32p;->g:LX/2Au;

    const-string v1, "double"

    invoke-direct {p0, p1, v0, v1}, LX/32p;->a(LX/2Au;LX/2Au;Ljava/lang/String;)LX/2Au;

    move-result-object v0

    iput-object v0, p0, LX/32p;->g:LX/2Au;

    .line 491018
    return-void
.end method

.method public final f(LX/2Au;)V
    .locals 2

    .prologue
    .line 491015
    iget-object v0, p0, LX/32p;->h:LX/2Au;

    const-string v1, "boolean"

    invoke-direct {p0, p1, v0, v1}, LX/32p;->a(LX/2Au;LX/2Au;Ljava/lang/String;)LX/2Au;

    move-result-object v0

    iput-object v0, p0, LX/32p;->h:LX/2Au;

    .line 491016
    return-void
.end method
