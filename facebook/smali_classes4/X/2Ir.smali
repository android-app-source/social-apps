.class public final enum LX/2Ir;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Ir;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Ir;

.field public static final enum DEFAULT:LX/2Ir;

.field public static final enum FAILED:LX/2Ir;

.field public static final enum FINISHED:LX/2Ir;

.field public static final enum UPLOADING:LX/2Ir;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 392273
    new-instance v0, LX/2Ir;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/2Ir;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ir;->DEFAULT:LX/2Ir;

    .line 392274
    new-instance v0, LX/2Ir;

    const-string v1, "UPLOADING"

    invoke-direct {v0, v1, v3}, LX/2Ir;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ir;->UPLOADING:LX/2Ir;

    .line 392275
    new-instance v0, LX/2Ir;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v4}, LX/2Ir;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ir;->FINISHED:LX/2Ir;

    .line 392276
    new-instance v0, LX/2Ir;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/2Ir;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ir;->FAILED:LX/2Ir;

    .line 392277
    const/4 v0, 0x4

    new-array v0, v0, [LX/2Ir;

    sget-object v1, LX/2Ir;->DEFAULT:LX/2Ir;

    aput-object v1, v0, v2

    sget-object v1, LX/2Ir;->UPLOADING:LX/2Ir;

    aput-object v1, v0, v3

    sget-object v1, LX/2Ir;->FINISHED:LX/2Ir;

    aput-object v1, v0, v4

    sget-object v1, LX/2Ir;->FAILED:LX/2Ir;

    aput-object v1, v0, v5

    sput-object v0, LX/2Ir;->$VALUES:[LX/2Ir;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 392278
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Ir;
    .locals 1

    .prologue
    .line 392279
    const-class v0, LX/2Ir;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Ir;

    return-object v0
.end method

.method public static values()[LX/2Ir;
    .locals 1

    .prologue
    .line 392280
    sget-object v0, LX/2Ir;->$VALUES:[LX/2Ir;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Ir;

    return-object v0
.end method
