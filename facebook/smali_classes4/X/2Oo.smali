.class public LX/2Oo;
.super Ljava/io/IOException;
.source ""

# interfaces
.implements LX/2Op;


# instance fields
.field private result:Lcom/facebook/http/protocol/ApiErrorResult;


# direct methods
.method public constructor <init>(Lcom/facebook/http/protocol/ApiErrorResult;)V
    .locals 2

    .prologue
    .line 403203
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[code] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [message]: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [extra]: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 403204
    iput-object p1, p0, LX/2Oo;->result:Lcom/facebook/http/protocol/ApiErrorResult;

    .line 403205
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 403206
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    iput-object v0, p0, LX/2Oo;->result:Lcom/facebook/http/protocol/ApiErrorResult;

    .line 403207
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 403208
    iget-object v0, p0, LX/2Oo;->result:Lcom/facebook/http/protocol/ApiErrorResult;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 403209
    return-void
.end method


# virtual methods
.method public a()Lcom/facebook/http/protocol/ApiErrorResult;
    .locals 1

    .prologue
    .line 403210
    iget-object v0, p0, LX/2Oo;->result:Lcom/facebook/http/protocol/ApiErrorResult;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 403211
    iget-object v0, p0, LX/2Oo;->result:Lcom/facebook/http/protocol/ApiErrorResult;

    .line 403212
    iget-object p0, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserTitle:Ljava/lang/String;

    move-object v0, p0

    .line 403213
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 403214
    iget-object v0, p0, LX/2Oo;->result:Lcom/facebook/http/protocol/ApiErrorResult;

    .line 403215
    iget-object p0, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserMessage:Ljava/lang/String;

    move-object v0, p0

    .line 403216
    return-object v0
.end method

.method public synthetic getExtraData()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 403217
    invoke-virtual {p0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    return-object v0
.end method
