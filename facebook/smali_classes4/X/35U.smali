.class public LX/35U;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/35U;


# instance fields
.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/35V;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 497172
    new-instance v0, LX/35U;

    invoke-direct {v0}, LX/35U;-><init>()V

    sput-object v0, LX/35U;->a:LX/35U;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 497173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497174
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/35U;->b:Ljava/util/HashMap;

    .line 497175
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;LX/0yF;J)V
    .locals 6

    .prologue
    .line 497176
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/35U;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35V;

    .line 497177
    if-nez v0, :cond_0

    .line 497178
    iget-object v0, p0, LX/35U;->b:Ljava/util/HashMap;

    new-instance v1, LX/35V;

    invoke-direct {v1}, LX/35V;-><init>()V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497179
    :cond_0
    sget-object v0, LX/35W;->a:[I

    invoke-virtual {p2}, LX/0yF;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 497180
    :goto_0
    monitor-exit p0

    return-void

    .line 497181
    :pswitch_0
    :try_start_1
    iget-object v0, p0, LX/35U;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35V;

    .line 497182
    iget-wide v3, v0, LX/35V;->c:J

    add-long/2addr v3, p3

    iput-wide v3, v0, LX/35V;->c:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497183
    goto :goto_0

    .line 497184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 497185
    :pswitch_1
    :try_start_2
    iget-object v0, p0, LX/35U;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35V;

    .line 497186
    iget-wide v3, v0, LX/35V;->b:J

    add-long/2addr v3, p3

    iput-wide v3, v0, LX/35V;->b:J

    .line 497187
    goto :goto_0

    .line 497188
    :pswitch_2
    iget-object v0, p0, LX/35U;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35V;

    .line 497189
    iget-wide v3, v0, LX/35V;->a:J

    add-long/2addr v3, p3

    iput-wide v3, v0, LX/35V;->a:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 497190
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized b()Ljava/util/Map;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/35V;",
            ">;"
        }
    .end annotation

    .prologue
    .line 497191
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/35U;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 497192
    iget-object v1, p0, LX/35U;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 497193
    monitor-exit p0

    return-object v0

    .line 497194
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
