.class public final LX/3Ce;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Cf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Cf",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 530291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530292
    const/4 v0, 0x0

    iput v0, p0, LX/3Ce;->a:I

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 530293
    check-cast p1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    .line 530294
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 530295
    check-cast p1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    .line 530296
    invoke-static {p1}, LX/3Cu;->b(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530297
    iget v0, p0, LX/3Ce;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/3Ce;->a:I

    .line 530298
    :cond_0
    return-void
.end method
