.class public final LX/3Kb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qf;
.implements LX/1qg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O::",
        "LX/2w7;",
        ">",
        "Ljava/lang/Object;",
        "LX/1qf;",
        "LX/1qg;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4um;

.field public final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/4uJ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2wJ;

.field private final d:LX/2wK;

.field public final e:LX/4uL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4uL",
            "<TO;>;"
        }
    .end annotation
.end field

.field public final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/2wd;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/4uQ;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "LX/2we;",
            ">;>;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:Lcom/google/android/gms/common/ConnectionResult;


# direct methods
.method public constructor <init>(LX/4um;LX/4sV;)V
    .locals 9
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4sV",
            "<TO;>;)V"
        }
    .end annotation

    iput-object p1, p0, LX/3Kb;->a:LX/4um;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/3Kb;->b:Ljava/util/Queue;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/3Kb;->f:Landroid/util/SparseArray;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/3Kb;->g:Ljava/util/Set;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/3Kb;->h:Landroid/util/SparseArray;

    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kb;->j:Lcom/google/android/gms/common/ConnectionResult;

    goto :goto_2

    :goto_0
    move-object v0, v1

    iput-object v0, p0, LX/3Kb;->c:LX/2wJ;

    iget-object v0, p0, LX/3Kb;->c:LX/2wJ;

    instance-of v0, v0, LX/2xP;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Kb;->c:LX/2wJ;

    check-cast v0, LX/2xP;

    iget-object v1, v0, LX/2xP;->d:LX/3KQ;

    move-object v0, v1

    iput-object v0, p0, LX/3Kb;->d:LX/2wK;

    :goto_1
    iget-object v0, p2, LX/4sV;->e:LX/4uL;

    move-object v0, v0

    iput-object v0, p0, LX/3Kb;->e:LX/4uL;

    return-void

    :cond_0
    iget-object v0, p0, LX/3Kb;->c:LX/2wJ;

    iput-object v0, p0, LX/3Kb;->d:LX/2wK;

    goto :goto_1

    :goto_2
    iget-object v1, p2, LX/4sV;->c:LX/2vs;

    move-object v1, v1

    invoke-virtual {v1}, LX/2vs;->b()LX/2vq;

    move-result-object v1

    iget-object v2, p2, LX/4sV;->a:Landroid/content/Context;

    move-object v2, v2

    iget-object v3, p0, LX/3Kb;->a:LX/4um;

    iget-object v3, v3, LX/4um;->m:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    iget-object v4, p2, LX/4sV;->a:Landroid/content/Context;

    move-object v4, v4

    invoke-static {v4}, LX/2wA;->a(Landroid/content/Context;)LX/2wA;

    move-result-object v4

    iget-object v5, p2, LX/4sV;->d:LX/2w7;

    move-object v5, v5

    move-object v6, p0

    move-object v7, p0

    invoke-virtual/range {v1 .. v7}, LX/2vq;->a(Landroid/content/Context;Landroid/os/Looper;LX/2wA;Ljava/lang/Object;LX/1qf;LX/1qg;)LX/2wJ;

    move-result-object v1

    goto :goto_0
.end method

.method public static a$redex0(LX/3Kb;Lcom/google/android/gms/common/api/Status;)V
    .locals 2
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, LX/3Kb;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4uJ;

    invoke-virtual {v0, p1}, LX/4uJ;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, LX/3Kb;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    return-void
.end method

.method public static b(LX/3Kb;LX/4uJ;)V
    .locals 5
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    const/4 v4, 0x1

    iget-object v1, p0, LX/3Kb;->f:Landroid/util/SparseArray;

    invoke-virtual {p1, v1}, LX/4uJ;->a(Landroid/util/SparseArray;)V

    iget v1, p1, LX/4uJ;->b:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :try_start_0
    iget-object v1, p0, LX/3Kb;->h:Landroid/util/SparseArray;

    iget v2, p1, LX/4uJ;->a:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    if-nez v1, :cond_3

    new-instance v1, LX/026;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, LX/026;-><init>(I)V

    iget-object v2, p0, LX/3Kb;->h:Landroid/util/SparseArray;

    iget v3, p1, LX/4uJ;->a:I

    invoke-virtual {v2, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v3, v1

    :goto_0
    move-object v0, p1

    check-cast v0, LX/4uK;

    move-object v1, v0

    iget-object v2, v1, LX/4uK;->c:LX/2we;

    move-object v0, v2

    check-cast v0, LX/4ut;

    move-object v1, v0

    invoke-interface {v1}, LX/4ut;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    :try_start_1
    iget-object v1, p0, LX/3Kb;->d:LX/2wK;

    invoke-virtual {p1, v1}, LX/4uJ;->a(LX/2wK;)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    return-void

    :catch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Listener registration methods must implement ListenerApiMethod"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget v1, p1, LX/4uJ;->b:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    :try_start_2
    iget-object v1, p0, LX/3Kb;->h:Landroid/util/SparseArray;

    iget v2, p1, LX/4uJ;->a:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    move-object v0, p1

    check-cast v0, LX/4uK;

    move-object v2, v0

    iget-object v2, v2, LX/4uK;->c:LX/2we;

    check-cast v2, LX/4ut;

    if-eqz v1, :cond_2

    invoke-interface {v2}, LX/4ut;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Listener unregistration methods must implement ListenerApiMethod"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    :try_start_3
    const-string v1, "GoogleApiManager"

    const-string v2, "Received call to unregister a listener without a matching registration call."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_2
    iget-object v1, p0, LX/3Kb;->c:LX/2wJ;

    invoke-interface {v1}, LX/2wJ;->f()V

    invoke-virtual {p0, v4}, LX/3Kb;->a(I)V

    goto :goto_2

    :cond_3
    move-object v3, v1

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, LX/3Kb;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4uQ;

    iget-object v2, p0, LX/3Kb;->e:LX/4uL;

    invoke-virtual {v0, v2, p1}, LX/4uQ;->a(LX/4uL;Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, LX/3Kb;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public static f(LX/3Kb;)V
    .locals 3
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    iget-boolean v0, p0, LX/3Kb;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget-object v0, v0, LX/4um;->m:Landroid/os/Handler;

    const/16 v1, 0x9

    iget-object v2, p0, LX/3Kb;->e:LX/4uL;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget-object v0, v0, LX/4um;->m:Landroid/os/Handler;

    const/16 v1, 0x8

    iget-object v2, p0, LX/3Kb;->e:LX/4uL;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Kb;->i:Z

    :cond_0
    return-void
.end method

.method public static h(LX/3Kb;)V
    .locals 4

    const/16 v3, 0xa

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget-object v0, v0, LX/4um;->m:Landroid/os/Handler;

    iget-object v1, p0, LX/3Kb;->e:LX/4uL;

    invoke-virtual {v0, v3, v1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget-object v0, v0, LX/4um;->m:Landroid/os/Handler;

    iget-object v1, p0, LX/3Kb;->a:LX/4um;

    iget-object v1, v1, LX/4um;->m:Landroid/os/Handler;

    iget-object v2, p0, LX/3Kb;->e:LX/4uL;

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, LX/3Kb;->a:LX/4um;

    iget-wide v2, v2, LX/4um;->c:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public static j(LX/3Kb;)V
    .locals 5
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, LX/3Kb;->c:LX/2wJ;

    invoke-interface {v0}, LX/2wJ;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Kb;->c:LX/2wJ;

    invoke-interface {v0}, LX/2wJ;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LX/3Kb;->c:LX/2wJ;

    invoke-interface {v0}, LX/2wJ;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget v0, v0, LX/4um;->h:I

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget-object v1, p0, LX/3Kb;->a:LX/4um;

    iget-object v1, v1, LX/4um;->g:LX/1vX;

    iget-object v2, p0, LX/3Kb;->a:LX/4um;

    iget-object v2, v2, LX/4um;->f:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/1od;->a(Landroid/content/Context;)I

    move-result v1

    iput v1, v0, LX/4um;->h:I

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget v0, v0, LX/4um;->h:I

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    iget-object v1, p0, LX/3Kb;->a:LX/4um;

    iget v1, v1, LX/4um;->h:I

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, LX/3Kb;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/3Kb;->c:LX/2wJ;

    new-instance v1, LX/4ul;

    iget-object v2, p0, LX/3Kb;->a:LX/4um;

    iget-object v3, p0, LX/3Kb;->c:LX/2wJ;

    iget-object v4, p0, LX/3Kb;->e:LX/4uL;

    invoke-direct {v1, v2, v3, v4}, LX/4ul;-><init>(LX/4um;LX/2wJ;LX/4uL;)V

    invoke-interface {v0, v1}, LX/2wJ;->a(LX/2wt;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    const/4 v0, 0x0

    iput-object v0, p0, LX/3Kb;->j:Lcom/google/android/gms/common/ConnectionResult;

    return-void
.end method

.method public final a(I)V
    .locals 4
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, LX/3Kb;->a()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3Kb;->i:Z

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget-object v0, v0, LX/4um;->m:Landroid/os/Handler;

    iget-object v1, p0, LX/3Kb;->a:LX/4um;

    iget-object v1, v1, LX/4um;->m:Landroid/os/Handler;

    const/16 v2, 0x8

    iget-object v3, p0, LX/3Kb;->e:LX/4uL;

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, LX/3Kb;->a:LX/4um;

    iget-wide v2, v2, LX/4um;->a:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget-object v0, v0, LX/4um;->m:Landroid/os/Handler;

    iget-object v1, p0, LX/3Kb;->a:LX/4um;

    iget-object v1, v1, LX/4um;->m:Landroid/os/Handler;

    const/16 v2, 0x9

    iget-object v3, p0, LX/3Kb;->e:LX/4uL;

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, LX/3Kb;->a:LX/4um;

    iget-wide v2, v2, LX/4um;->b:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    const/4 v1, -0x1

    iput v1, v0, LX/4um;->h:I

    return-void
.end method

.method public final a(IZ)V
    .locals 4
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, LX/3Kb;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4uJ;

    iget v2, v0, LX/4uJ;->a:I

    if-ne v2, p1, :cond_0

    iget v2, v0, LX/4uJ;->b:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, LX/4uJ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/3Kb;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wd;

    invoke-virtual {v0}, LX/2wd;->a()V

    iget-object v0, p0, LX/3Kb;->h:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->delete(I)V

    if-nez p2, :cond_2

    iget-object v0, p0, LX/3Kb;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget-object v0, v0, LX/4um;->o:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, p0, LX/3Kb;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/3Kb;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, LX/3Kb;->f(LX/3Kb;)V

    iget-object v0, p0, LX/3Kb;->c:LX/2wJ;

    invoke-interface {v0}, LX/2wJ;->f()V

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget-object v0, v0, LX/4um;->j:Ljava/util/Map;

    iget-object v1, p0, LX/3Kb;->e:LX/4uL;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, LX/4um;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget-object v0, v0, LX/4um;->l:Ljava/util/Set;

    iget-object v2, p0, LX/3Kb;->e:LX/4uL;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, LX/3Kb;->a()V

    sget-object v0, Lcom/google/android/gms/common/ConnectionResult;->a:Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {p0, v0}, LX/3Kb;->b(Lcom/google/android/gms/common/ConnectionResult;)V

    invoke-static {p0}, LX/3Kb;->f(LX/3Kb;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/3Kb;->h:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, LX/3Kb;->h:Landroid/util/SparseArray;

    iget-object v2, p0, LX/3Kb;->h:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2we;

    :try_start_0
    iget-object v3, p0, LX/3Kb;->d:LX/2wK;

    invoke-virtual {v0, v3}, LX/2we;->a(LX/2wK;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    iget-object v0, p0, LX/3Kb;->c:LX/2wJ;

    invoke-interface {v0}, LX/2wJ;->f()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3Kb;->a(I)V

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    :goto_2
    iget-object v0, p0, LX/3Kb;->c:LX/2wJ;

    invoke-interface {v0}, LX/2wJ;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3Kb;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/3Kb;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4uJ;

    invoke-static {p0, v0}, LX/3Kb;->b(LX/3Kb;LX/4uJ;)V

    goto :goto_2

    :cond_2
    invoke-static {p0}, LX/3Kb;->h(LX/3Kb;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p0}, LX/3Kb;->a()V

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    const/4 v1, -0x1

    iput v1, v0, LX/4um;->h:I

    invoke-direct {p0, p1}, LX/3Kb;->b(Lcom/google/android/gms/common/ConnectionResult;)V

    iget-object v0, p0, LX/3Kb;->f:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    iget-object v1, p0, LX/3Kb;->b:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object p1, p0, LX/3Kb;->j:Lcom/google/android/gms/common/ConnectionResult;

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, LX/4um;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :goto_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, LX/3Kb;->a:LX/4um;

    invoke-virtual {v1, p1, v0}, LX/4um;->a(Lcom/google/android/gms/common/ConnectionResult;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v0, v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3Kb;->i:Z

    :cond_2
    iget-boolean v0, p0, LX/3Kb;->i:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3Kb;->a:LX/4um;

    iget-object v0, v0, LX/4um;->m:Landroid/os/Handler;

    iget-object v1, p0, LX/3Kb;->a:LX/4um;

    iget-object v1, v1, LX/4um;->m:Landroid/os/Handler;

    const/16 v2, 0x8

    iget-object v3, p0, LX/3Kb;->e:LX/4uL;

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, LX/3Kb;->a:LX/4um;

    iget-wide v2, v2, LX/4um;->a:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x11

    iget-object v2, p0, LX/3Kb;->e:LX/4uL;

    iget-object v3, v2, LX/4uL;->a:LX/2vs;

    iget-object v2, v3, LX/2vs;->e:Ljava/lang/String;

    move-object v3, v2

    move-object v2, v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "API: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not available on this device."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    invoke-static {p0, v0}, LX/3Kb;->a$redex0(LX/3Kb;Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
