.class public LX/2wd;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:[LX/2we;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/2we",
            "<**>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2we",
            "<**>;>;"
        }
    .end annotation
.end field

.field private final c:LX/2wj;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/2vo",
            "<*>;",
            "LX/2wJ;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/4v4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [LX/2we;

    sput-object v0, LX/2wd;->b:[LX/2we;

    return-void
.end method

.method public constructor <init>(LX/2vo;LX/2wJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2vo",
            "<*>;",
            "LX/2wJ;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/2wd;->a:Ljava/util/Set;

    new-instance v0, LX/2wi;

    invoke-direct {v0, p0}, LX/2wi;-><init>(LX/2wd;)V

    iput-object v0, p0, LX/2wd;->c:LX/2wj;

    const/4 v0, 0x0

    iput-object v0, p0, LX/2wd;->e:LX/4v4;

    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/2wd;->d:Ljava/util/Map;

    iget-object v0, p0, LX/2wd;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/2vo",
            "<*>;",
            "LX/2wJ;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/2wd;->a:Ljava/util/Set;

    new-instance v0, LX/2wi;

    invoke-direct {v0, p0}, LX/2wi;-><init>(LX/2wd;)V

    iput-object v0, p0, LX/2wd;->c:LX/2wj;

    const/4 v0, 0x0

    iput-object v0, p0, LX/2wd;->e:LX/4v4;

    iput-object p1, p0, LX/2wd;->d:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    const/4 v6, 0x0

    iget-object v0, p0, LX/2wd;->a:Ljava/util/Set;

    sget-object v1, LX/2wd;->b:[LX/2we;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2we;

    array-length v3, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    invoke-virtual {v4, v6}, LX/2we;->a(LX/2wj;)V

    invoke-virtual {v4}, LX/2wg;->b()Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {v4}, LX/2wf;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2wd;->a:Ljava/util/Set;

    invoke-interface {v1, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, LX/2wg;->a(LX/27U;)V

    iget-object v1, p0, LX/2wd;->d:Ljava/util/Map;

    iget-object v5, v4, LX/2we;->d:LX/2vo;

    move-object v5, v5

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2wJ;

    invoke-interface {v1}, LX/2wJ;->s()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v4}, LX/2wf;->g()Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v5, LX/4v3;

    invoke-direct {v5, v4, v6, v1}, LX/4v3;-><init>(LX/2we;LX/4sW;Landroid/os/IBinder;)V

    invoke-virtual {v4, v5}, LX/2we;->a(LX/2wj;)V

    :goto_2
    iget-object v1, p0, LX/2wd;->a:Ljava/util/Set;

    invoke-interface {v1, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-void

    :cond_3
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance v5, LX/4v3;

    invoke-direct {v5, v4, v6, v1}, LX/4v3;-><init>(LX/2we;LX/4sW;Landroid/os/IBinder;)V

    invoke-virtual {v4, v5}, LX/2we;->a(LX/2wj;)V

    const/4 v7, 0x0

    :try_start_0
    invoke-interface {v1, v5, v7}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    invoke-virtual {v4}, LX/2wf;->h()V

    invoke-virtual {v4}, LX/2wg;->b()Ljava/lang/Integer;

    invoke-virtual {v6}, LX/4sW;->a()V

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/2we;->a(LX/2wj;)V

    invoke-virtual {v4}, LX/2wf;->h()V

    invoke-virtual {v4}, LX/2wg;->b()Ljava/lang/Integer;

    invoke-virtual {v6}, LX/4sW;->a()V

    goto :goto_2
.end method

.method public final a(LX/2we;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LX/2wK;",
            ">(",
            "LX/2we",
            "<+",
            "LX/2NW;",
            "TA;>;)V"
        }
    .end annotation

    iget-object v0, p0, LX/2wd;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, LX/2wd;->c:LX/2wj;

    invoke-virtual {p1, v0}, LX/2we;->a(LX/2wj;)V

    return-void
.end method
