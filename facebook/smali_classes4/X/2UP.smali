.class public LX/2UP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0if;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415956
    iput-object p1, p0, LX/2UP;->a:LX/0Zb;

    .line 415957
    iput-object p2, p0, LX/2UP;->b:LX/0if;

    .line 415958
    return-void
.end method

.method public static a(LX/0QB;)LX/2UP;
    .locals 5

    .prologue
    .line 415944
    const-class v1, LX/2UP;

    monitor-enter v1

    .line 415945
    :try_start_0
    sget-object v0, LX/2UP;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 415946
    sput-object v2, LX/2UP;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 415947
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415948
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 415949
    new-instance p0, LX/2UP;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    invoke-direct {p0, v3, v4}, LX/2UP;-><init>(LX/0Zb;LX/0if;)V

    .line 415950
    move-object v0, p0

    .line 415951
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 415952
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2UP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415953
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 415954
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/95K;)V
    .locals 3

    .prologue
    .line 415942
    iget-object v0, p0, LX/2UP;->b:LX/0if;

    sget-object v1, LX/0ig;->p:LX/0ih;

    invoke-virtual {p1}, LX/95K;->getEventName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 415943
    return-void
.end method

.method public final a(LX/95K;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 415940
    iget-object v0, p0, LX/2UP;->b:LX/0if;

    sget-object v1, LX/0ig;->p:LX/0ih;

    invoke-virtual {p1}, LX/95K;->getEventName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 415941
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 415912
    iget-object v0, p0, LX/2UP;->b:LX/0if;

    sget-object v1, LX/0ig;->p:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 415913
    return-void
.end method

.method public final a(Ljava/lang/String;IIIII)V
    .locals 4

    .prologue
    .line 415934
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "ccu_upload"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 415935
    const-string v1, "app_type"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "data_type"

    const-string v3, "ccu_upload_operation"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "phonebook_size"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ccu_add_count"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ccu_modify_count"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ccu_delete_count"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "ccu_upload_size"

    invoke-virtual {v1, v2, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 415936
    const-string v2, "contacts_upload"

    move-object v2, v2

    .line 415937
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 415938
    iget-object v1, p0, LX/2UP;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 415939
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 415928
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "ccu_no_emails_no_phones_contact"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 415929
    const-string v1, "contact_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "contact_action"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 415930
    const-string v2, "contacts_upload"

    move-object v2, v2

    .line 415931
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 415932
    iget-object v1, p0, LX/2UP;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 415933
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 415922
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "ccu_invalid_contact_id"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 415923
    const-string v1, "duplicate_contact_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "contact_id"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 415924
    const-string v2, "contacts_upload"

    move-object v2, v2

    .line 415925
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 415926
    iget-object v1, p0, LX/2UP;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 415927
    return-void
.end method

.method public final a(ZZZLjava/lang/String;JJ)V
    .locals 3

    .prologue
    .line 415916
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "ccu_server_check_client"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 415917
    const-string v1, "ccu_server_check_success"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "out_of_sync"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "will_upload"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "last_upload_client_hash"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "last_upload_attempt_time"

    invoke-virtual {v1, v2, p5, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "last_upload_success_time"

    invoke-virtual {v1, v2, p7, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 415918
    const-string v2, "contacts_upload"

    move-object v2, v2

    .line 415919
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 415920
    iget-object v1, p0, LX/2UP;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 415921
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 415914
    iget-object v0, p0, LX/2UP;->b:LX/0if;

    sget-object v1, LX/0ig;->p:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 415915
    return-void
.end method
