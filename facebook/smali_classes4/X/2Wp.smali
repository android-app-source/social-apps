.class public LX/2Wp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/01U;

.field private final c:LX/01T;

.field private final d:LX/0aU;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/01U;LX/01T;LX/0aU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 419340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419341
    iput-object p1, p0, LX/2Wp;->a:Landroid/content/Context;

    .line 419342
    iput-object p2, p0, LX/2Wp;->b:LX/01U;

    .line 419343
    iput-object p3, p0, LX/2Wp;->c:LX/01T;

    .line 419344
    iput-object p4, p0, LX/2Wp;->d:LX/0aU;

    .line 419345
    return-void
.end method

.method public static a(LX/2Wp;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 419333
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2Wp;->d:LX/0aU;

    invoke-virtual {v1, p1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 419334
    const-string v1, "extra_product"

    iget-object v2, p0, LX/2Wp;->c:LX/01T;

    invoke-virtual {v2}, LX/01T;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 419335
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 419336
    iget-object v1, p0, LX/2Wp;->a:Landroid/content/Context;

    iget-object v2, p0, LX/2Wp;->b:LX/01U;

    invoke-virtual {v2}, LX/01U;->getPermission()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 419337
    return-void
.end method

.method public static b(LX/0QB;)LX/2Wp;
    .locals 5

    .prologue
    .line 419338
    new-instance v4, LX/2Wp;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0XV;->b(LX/0QB;)LX/01U;

    move-result-object v1

    check-cast v1, LX/01U;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v2

    check-cast v2, LX/01T;

    invoke-static {p0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v3

    check-cast v3, LX/0aU;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2Wp;-><init>(Landroid/content/Context;LX/01U;LX/01T;LX/0aU;)V

    .line 419339
    return-object v4
.end method
