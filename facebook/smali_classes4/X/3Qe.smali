.class public LX/3Qe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/3Qf;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:LX/3Qg;

.field public static final c:LX/3Qh;

.field public static final d:LX/3Qi;

.field public static final e:LX/3Qj;

.field public static final f:LX/3Qk;

.field private static volatile k:LX/3Qe;


# instance fields
.field public g:I

.field public final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/7B6;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/11i;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private j:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 565637
    new-instance v0, LX/3Qf;

    invoke-direct {v0}, LX/3Qf;-><init>()V

    sput-object v0, LX/3Qe;->a:LX/3Qf;

    .line 565638
    new-instance v0, LX/3Qg;

    invoke-direct {v0}, LX/3Qg;-><init>()V

    sput-object v0, LX/3Qe;->b:LX/3Qg;

    .line 565639
    new-instance v0, LX/3Qh;

    invoke-direct {v0}, LX/3Qh;-><init>()V

    sput-object v0, LX/3Qe;->c:LX/3Qh;

    .line 565640
    new-instance v0, LX/3Qi;

    invoke-direct {v0}, LX/3Qi;-><init>()V

    sput-object v0, LX/3Qe;->d:LX/3Qi;

    .line 565641
    new-instance v0, LX/3Qj;

    invoke-direct {v0}, LX/3Qj;-><init>()V

    sput-object v0, LX/3Qe;->e:LX/3Qj;

    .line 565642
    new-instance v0, LX/3Qk;

    invoke-direct {v0}, LX/3Qk;-><init>()V

    sput-object v0, LX/3Qe;->f:LX/3Qk;

    return-void
.end method

.method public constructor <init>(LX/11i;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 565643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565644
    iput v1, p0, LX/3Qe;->g:I

    .line 565645
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/3Qe;->h:Ljava/util/HashMap;

    .line 565646
    iput-boolean v1, p0, LX/3Qe;->j:Z

    .line 565647
    iput-object p1, p0, LX/3Qe;->i:LX/11i;

    .line 565648
    return-void
.end method

.method public static a(LX/0QB;)LX/3Qe;
    .locals 4

    .prologue
    .line 565649
    sget-object v0, LX/3Qe;->k:LX/3Qe;

    if-nez v0, :cond_1

    .line 565650
    const-class v1, LX/3Qe;

    monitor-enter v1

    .line 565651
    :try_start_0
    sget-object v0, LX/3Qe;->k:LX/3Qe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 565652
    if-eqz v2, :cond_0

    .line 565653
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 565654
    new-instance p0, LX/3Qe;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-direct {p0, v3}, LX/3Qe;-><init>(LX/11i;)V

    .line 565655
    move-object v0, p0

    .line 565656
    sput-object v0, LX/3Qe;->k:LX/3Qe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565657
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 565658
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565659
    :cond_1
    sget-object v0, LX/3Qe;->k:LX/3Qe;

    return-object v0

    .line 565660
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 565661
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3Qe;Ljava/lang/String;Ljava/lang/String;LX/7B6;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 565662
    invoke-static {p0}, LX/3Qe;->g(LX/3Qe;)LX/11o;

    move-result-object v1

    .line 565663
    iget-object v0, p0, LX/3Qe;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 565664
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 565665
    :cond_0
    :goto_0
    return-void

    .line 565666
    :cond_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v3, -0x797725d4

    invoke-static {v1, p1, v2, v4, v3}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 565667
    if-eqz p2, :cond_0

    .line 565668
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v2, -0x37d6eb10

    invoke-static {v1, p2, v0, v4, v2}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    goto :goto_0
.end method

.method private declared-synchronized f()LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 565669
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->a:LX/3Qf;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized g(LX/3Qe;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 565670
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->b:LX/3Qg;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized h(LX/3Qe;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 565671
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->d:LX/3Qi;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized i()LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 565672
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->e:LX/3Qj;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized j(LX/3Qe;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 565673
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/3Qe;->f()LX/11o;

    move-result-object v0

    .line 565674
    if-nez v0, :cond_0

    .line 565675
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->a:LX/3Qf;

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 565676
    :cond_0
    monitor-exit p0

    return-object v0

    .line 565677
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized k(LX/3Qe;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 565678
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Qe;->g(LX/3Qe;)LX/11o;

    move-result-object v0

    .line 565679
    if-nez v0, :cond_0

    .line 565680
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->b:LX/3Qg;

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 565681
    :cond_0
    monitor-exit p0

    return-object v0

    .line 565682
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized l(LX/3Qe;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 565683
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Qe;->h(LX/3Qe;)LX/11o;

    move-result-object v0

    .line 565684
    if-nez v0, :cond_0

    .line 565685
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->d:LX/3Qi;

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 565686
    :cond_0
    monitor-exit p0

    return-object v0

    .line 565687
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized m(LX/3Qe;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 565688
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/3Qe;->i()LX/11o;

    move-result-object v0

    .line 565689
    if-nez v0, :cond_0

    .line 565690
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->e:LX/3Qj;

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 565691
    :cond_0
    monitor-exit p0

    return-object v0

    .line 565692
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 565626
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Qe;->j(LX/3Qe;)LX/11o;

    move-result-object v0

    .line 565627
    const-string v1, "time_to_load_bootstrap_from_search_button_clicked"

    const v2, 0x56e41d0f

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 565628
    iget-boolean v1, p0, LX/3Qe;->j:Z

    if-eqz v1, :cond_0

    .line 565629
    const-string v1, "time_to_load_bootstrap_from_search_button_clicked"

    const v2, -0x1df6cac2

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 565630
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->a:LX/3Qf;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565631
    :cond_0
    monitor-exit p0

    return-void

    .line 565632
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 565633
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Qe;->j(LX/3Qe;)LX/11o;

    move-result-object v0

    .line 565634
    const/4 v1, 0x0

    const-string v2, "time_since_last_fetch"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    const v3, 0x236fe97f

    invoke-static {v0, p3, v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565635
    monitor-exit p0

    return-void

    .line 565636
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 565553
    invoke-direct {p0}, LX/3Qe;->f()LX/11o;

    move-result-object v0

    .line 565554
    if-nez v0, :cond_0

    .line 565555
    :goto_0
    return-void

    .line 565556
    :cond_0
    const v1, -0x3f1ad642

    invoke-static {v0, p1, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;LX/0P1;)V
    .locals 3
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 565557
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Qe;->m(LX/3Qe;)LX/11o;

    move-result-object v0

    .line 565558
    const-string v1, "time_to_delta_load_bootstrap"

    const v2, 0x2ad99549

    invoke-static {v0, v1, p1, p2, v2}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 565559
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->e:LX/3Qj;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565560
    monitor-exit p0

    return-void

    .line 565561
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 565562
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/3Qe;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565563
    monitor-exit p0

    return-void

    .line 565564
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ZLjava/lang/Exception;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 565565
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/3Qe;->f()LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 565566
    if-nez v1, :cond_0

    .line 565567
    :goto_0
    monitor-exit p0

    return-void

    .line 565568
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    const-string v3, "delta_sync"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    const-string v5, "error"

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v3, v4, v5, v0}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    const v3, 0x5da841c7

    invoke-static {v1, p3, v2, v0, v3}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 565569
    const/4 v0, 0x0

    const-string v2, "source"

    const-string v3, "post_fetch"

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    const v3, -0x67441bf3

    invoke-static {v1, p3, v0, v2, v3}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 565570
    const-string v0, "time_to_load_bootstrap_from_search_button_clicked"

    invoke-interface {v1, v0}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 565571
    const-string v0, "time_to_load_bootstrap_from_search_button_clicked"

    const v2, -0xded24af

    invoke-static {v1, v0, v2}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 565572
    :cond_1
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->a:LX/3Qf;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 565573
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 565574
    :cond_2
    :try_start_2
    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_1
.end method

.method public final declared-synchronized b()V
    .locals 3

    .prologue
    .line 565575
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Qe;->l(LX/3Qe;)LX/11o;

    move-result-object v0

    .line 565576
    const-string v1, "time_to_prefetch_bootstrap"

    const v2, -0x6cb98a57

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565577
    monitor-exit p0

    return-void

    .line 565578
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 565579
    invoke-direct {p0}, LX/3Qe;->f()LX/11o;

    move-result-object v0

    .line 565580
    if-nez v0, :cond_0

    .line 565581
    :goto_0
    return-void

    .line 565582
    :cond_0
    const v1, -0x46dccb28

    invoke-static {v0, p1, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method

.method public final declared-synchronized c()V
    .locals 3

    .prologue
    .line 565583
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Qe;->l(LX/3Qe;)LX/11o;

    move-result-object v0

    .line 565584
    const-string v1, "time_to_prefetch_bootstrap"

    const v2, 0x7db4f932

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 565585
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->d:LX/3Qi;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565586
    monitor-exit p0

    return-void

    .line 565587
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 565588
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/3Qe;->j:Z

    .line 565589
    invoke-direct {p0}, LX/3Qe;->f()LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 565590
    if-nez v0, :cond_0

    .line 565591
    :goto_0
    monitor-exit p0

    return-void

    .line 565592
    :cond_0
    const v1, 0x51a677c1

    :try_start_1
    invoke-static {v0, p1, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 565593
    const-string v1, "time_to_load_bootstrap_from_search_button_clicked"

    invoke-interface {v0, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 565594
    const-string v1, "time_to_load_bootstrap_from_search_button_clicked"

    const v2, 0x6a0ceafa

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 565595
    :cond_1
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->a:LX/3Qf;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 565596
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 3

    .prologue
    .line 565597
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Qe;->h(LX/3Qe;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 565598
    if-nez v0, :cond_0

    .line 565599
    :goto_0
    monitor-exit p0

    return-void

    .line 565600
    :cond_0
    :try_start_1
    const-string v1, "time_to_prefetch_bootstrap"

    const v2, -0x3773b456

    invoke-static {v0, v1, v2}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 565601
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->d:LX/3Qi;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 565602
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 565603
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/3Qe;->f()LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 565604
    if-nez v0, :cond_0

    .line 565605
    :goto_0
    monitor-exit p0

    return-void

    .line 565606
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    const-string v2, "source"

    const-string v3, "network"

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    const v3, -0x2106d270

    invoke-static {v0, p1, v1, v2, v3}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 565607
    const-string v1, "time_to_load_bootstrap_from_search_button_clicked"

    invoke-interface {v0, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 565608
    const-string v1, "time_to_load_bootstrap_from_search_button_clicked"

    const v2, 0x5b01967

    invoke-static {v0, v1, v2}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 565609
    :cond_1
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->a:LX/3Qf;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 565610
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 2

    .prologue
    .line 565611
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Qe;->g(LX/3Qe;)LX/11o;

    move-result-object v0

    .line 565612
    if-eqz v0, :cond_0

    .line 565613
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->b:LX/3Qg;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565614
    :cond_0
    monitor-exit p0

    return-void

    .line 565615
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 565616
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/3Qe;->m(LX/3Qe;)LX/11o;

    move-result-object v0

    .line 565617
    const-string v1, "time_to_delta_load_bootstrap"

    const/4 v2, 0x0

    const v3, 0x36ff3cda

    invoke-static {v0, v1, p1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565618
    monitor-exit p0

    return-void

    .line 565619
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 565620
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/3Qe;->i()LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 565621
    if-nez v0, :cond_0

    .line 565622
    :goto_0
    monitor-exit p0

    return-void

    .line 565623
    :cond_0
    :try_start_1
    const-string v1, "time_to_delta_load_bootstrap"

    const/4 v2, 0x0

    const v3, -0x5239a007

    invoke-static {v0, v1, p1, v2, v3}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 565624
    iget-object v0, p0, LX/3Qe;->i:LX/11i;

    sget-object v1, LX/3Qe;->e:LX/3Qj;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 565625
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
