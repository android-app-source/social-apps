.class public LX/2N9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 398586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398587
    iput-object p1, p0, LX/2N9;->a:LX/0Zb;

    .line 398588
    return-void
.end method

.method public static a(LX/0QB;)LX/2N9;
    .locals 1

    .prologue
    .line 398589
    invoke-static {p0}, LX/2N9;->b(LX/0QB;)LX/2N9;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2N9;
    .locals 2

    .prologue
    .line 398590
    new-instance v1, LX/2N9;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/2N9;-><init>(LX/0Zb;)V

    .line 398591
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 398592
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "missing_sender_name"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 398593
    const-string v1, "path"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 398594
    const-string v1, "id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 398595
    iget-object v1, p0, LX/2N9;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 398596
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 398597
    iget-object v0, p0, LX/2N9;->a:LX/0Zb;

    const-string v1, "fetch_thread"

    invoke-interface {v0, v1, p1}, LX/0Zb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 398598
    return-void
.end method
