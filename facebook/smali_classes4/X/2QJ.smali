.class public final LX/2QJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation
.end field

.field public b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/1pz;


# direct methods
.method public constructor <init>(LX/1pz;)V
    .locals 1

    .prologue
    .line 407844
    iput-object p1, p0, LX/2QJ;->c:LX/1pz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407845
    iget-object v0, p0, LX/2QJ;->c:LX/1pz;

    iget-object v0, v0, LX/1pz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, LX/2QJ;->a:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 407846
    iget-object v0, p0, LX/2QJ;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 407847
    iget-object v0, p0, LX/2QJ;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 407848
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    iput-object v1, p0, LX/2QJ;->b:Ljava/util/Collection;

    .line 407849
    iget-object v1, p0, LX/2QJ;->c:LX/1pz;

    .line 407850
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 407851
    iget-object p0, v1, LX/1pz;->b:LX/0Xs;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {p0, v3, v2}, LX/0Xs;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v2

    invoke-static {v3, v2}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    move-object v0, v2

    .line 407852
    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 407853
    iget-object v0, p0, LX/2QJ;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 407854
    iget-object v0, p0, LX/2QJ;->c:LX/1pz;

    iget-object v0, v0, LX/1pz;->b:LX/0Xs;

    iget-object v1, p0, LX/2QJ;->b:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-static {v0, v1}, LX/0Xs;->b(LX/0Xs;I)I

    .line 407855
    iget-object v0, p0, LX/2QJ;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 407856
    return-void
.end method
