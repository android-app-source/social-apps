.class public LX/2sf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 473797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 473813
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 473814
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 473815
    :goto_0
    return v1

    .line 473816
    :cond_0
    const-string v8, "length"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 473817
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    .line 473818
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 473819
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 473820
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 473821
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 473822
    const-string v8, "entity_with_image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 473823
    invoke-static {p0, p1}, LX/2sg;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 473824
    :cond_2
    const-string v8, "offset"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 473825
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 473826
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 473827
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 473828
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 473829
    if-eqz v3, :cond_5

    .line 473830
    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 473831
    :cond_5
    if-eqz v0, :cond_6

    .line 473832
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 473833
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 473798
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 473799
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 473800
    if-eqz v0, :cond_0

    .line 473801
    const-string v1, "entity_with_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 473802
    invoke-static {p0, v0, p2, p3}, LX/2sg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 473803
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 473804
    if-eqz v0, :cond_1

    .line 473805
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 473806
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 473807
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 473808
    if-eqz v0, :cond_2

    .line 473809
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 473810
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 473811
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 473812
    return-void
.end method
