.class public LX/28k;
.super LX/16B;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/28k;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0dc;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0pQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 374577
    const-class v0, LX/28k;

    sput-object v0, LX/28k;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Ljava/util/Set",
            "<",
            "LX/0dc;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/0pQ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374572
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 374573
    iput-object p1, p0, LX/28k;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 374574
    iput-object p2, p0, LX/28k;->c:Ljava/util/Set;

    .line 374575
    iput-object p3, p0, LX/28k;->d:Ljava/util/Set;

    .line 374576
    return-void
.end method

.method public static a(LX/0QB;)LX/28k;
    .locals 8

    .prologue
    .line 374556
    sget-object v0, LX/28k;->e:LX/28k;

    if-nez v0, :cond_1

    .line 374557
    const-class v1, LX/28k;

    monitor-enter v1

    .line 374558
    :try_start_0
    sget-object v0, LX/28k;->e:LX/28k;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 374559
    if-eqz v2, :cond_0

    .line 374560
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 374561
    new-instance v4, LX/28k;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 374562
    new-instance v5, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    new-instance v7, LX/28l;

    invoke-direct {v7, v0}, LX/28l;-><init>(LX/0QB;)V

    invoke-direct {v5, v6, v7}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v5, v5

    .line 374563
    new-instance v6, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance p0, LX/28m;

    invoke-direct {p0, v0}, LX/28m;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    .line 374564
    invoke-direct {v4, v3, v5, v6}, LX/28k;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/Set;Ljava/util/Set;)V

    .line 374565
    move-object v0, v4

    .line 374566
    sput-object v0, LX/28k;->e:LX/28k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374567
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 374568
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 374569
    :cond_1
    sget-object v0, LX/28k;->e:LX/28k;

    return-object v0

    .line 374570
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 374571
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final g()V
    .locals 3

    .prologue
    .line 374546
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 374547
    iget-object v0, p0, LX/28k;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0dc;

    .line 374548
    invoke-interface {v0}, LX/0dc;->b()LX/0Rf;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 374549
    :cond_0
    iget-object v0, p0, LX/28k;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    .line 374550
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 374551
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 374552
    iget-object v0, p0, LX/28k;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pQ;

    .line 374553
    invoke-interface {v0}, LX/0pQ;->a()LX/0Rf;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 374554
    :cond_0
    iget-object v0, p0, LX/28k;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    .line 374555
    return-void
.end method
