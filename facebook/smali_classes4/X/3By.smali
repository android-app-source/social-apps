.class public LX/3By;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile c:LX/3By;


# instance fields
.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 529346
    const-class v0, LX/3By;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3By;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 529367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529368
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3By;->b:Ljava/util/Map;

    .line 529369
    return-void
.end method

.method public static a(LX/0QB;)LX/3By;
    .locals 3

    .prologue
    .line 529355
    sget-object v0, LX/3By;->c:LX/3By;

    if-nez v0, :cond_1

    .line 529356
    const-class v1, LX/3By;

    monitor-enter v1

    .line 529357
    :try_start_0
    sget-object v0, LX/3By;->c:LX/3By;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 529358
    if-eqz v2, :cond_0

    .line 529359
    :try_start_1
    new-instance v0, LX/3By;

    invoke-direct {v0}, LX/3By;-><init>()V

    .line 529360
    move-object v0, v0

    .line 529361
    sput-object v0, LX/3By;->c:LX/3By;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529362
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 529363
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 529364
    :cond_1
    sget-object v0, LX/3By;->c:LX/3By;

    return-object v0

    .line 529365
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 529366
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 529347
    if-nez p3, :cond_0

    .line 529348
    sget-object v0, LX/3By;->a:Ljava/lang/String;

    const-string v1, "Cannot put null value for cacheKey=%s and key=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 529349
    :goto_0
    return-void

    .line 529350
    :cond_0
    iget-object v0, p0, LX/3By;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 529351
    if-nez v0, :cond_1

    .line 529352
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 529353
    iget-object v1, p0, LX/3By;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 529354
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
