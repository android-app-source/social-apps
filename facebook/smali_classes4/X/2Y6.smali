.class public LX/2Y6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "LX/2Wd;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/2Y2;

.field private final b:LX/2Y7;

.field private final c:LX/2Xz;

.field private final d:I

.field private final e:J

.field private final f:J

.field private g:LX/2D7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2D7",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:LX/2Wd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I


# direct methods
.method public constructor <init>(Ljava/io/File;LX/2Y2;LX/2Xz;I)V
    .locals 4

    .prologue
    .line 420681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420682
    new-instance v0, LX/2Y7;

    new-instance v1, LX/2Y9;

    invoke-direct {v1, p1}, LX/2Y9;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, LX/2Y7;-><init>(LX/2Y9;)V

    iput-object v0, p0, LX/2Y6;->b:LX/2Y7;

    .line 420683
    iput-object p2, p0, LX/2Y6;->a:LX/2Y2;

    .line 420684
    iput-object p3, p0, LX/2Y6;->c:LX/2Xz;

    .line 420685
    iput p4, p0, LX/2Y6;->d:I

    .line 420686
    invoke-static {}, LX/2D9;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x7

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/2Y6;->e:J

    .line 420687
    invoke-static {}, LX/2D9;->b()J

    move-result-wide v0

    const-wide/16 v2, 0xa8

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/2Y6;->f:J

    .line 420688
    return-void
.end method

.method private a(LX/2Wb;)Z
    .locals 3

    .prologue
    .line 420675
    instance-of v0, p1, LX/2Wa;

    if-eqz v0, :cond_0

    .line 420676
    iget-wide v0, p0, LX/2Y6;->e:J

    invoke-static {p1, v0, v1}, LX/2Y6;->a(LX/2Wb;J)Z

    move-result v0

    .line 420677
    :goto_0
    return v0

    .line 420678
    :cond_0
    instance-of v0, p1, LX/2YL;

    if-eqz v0, :cond_1

    .line 420679
    iget-wide v0, p0, LX/2Y6;->f:J

    invoke-static {p1, v0, v1}, LX/2Y6;->a(LX/2Wb;J)Z

    move-result v0

    goto :goto_0

    .line 420680
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "directoryNode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(LX/2Wb;J)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    .line 420669
    const/4 v1, -0x1

    .line 420670
    :try_start_0
    iget-object v7, p0, LX/2YB;->a:Ljava/io/File;

    move-object v7, v7

    .line 420671
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v7

    .line 420672
    :goto_0
    move-wide v2, v7

    .line 420673
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 420674
    :cond_0
    :goto_1
    return v0

    :cond_1
    cmp-long v1, v2, p1

    if-ltz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    int-to-long v7, v1

    goto :goto_0
.end method

.method private a(Ljava/io/File;LX/2D7;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "LX/2D7",
            "<",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 420657
    invoke-virtual {p2, p1}, LX/2D7;->a(Ljava/lang/Object;)LX/2DZ;

    move-result-object v1

    .line 420658
    :try_start_0
    invoke-virtual {v1, p0}, LX/2DZ;->d(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 420659
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420660
    invoke-virtual {v1, p0}, LX/2DZ;->a(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420661
    :try_start_2
    invoke-virtual {v1, p0}, LX/2DZ;->f(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 420662
    invoke-virtual {v1}, LX/2DZ;->a()V

    const/4 v0, 0x1

    .line 420663
    :goto_0
    return v0

    .line 420664
    :cond_0
    :try_start_3
    invoke-virtual {v1, p0}, LX/2DZ;->f(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 420665
    :cond_1
    invoke-virtual {v1}, LX/2DZ;->a()V

    .line 420666
    const/4 v0, 0x0

    goto :goto_0

    .line 420667
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v1, p0}, LX/2DZ;->f(Ljava/lang/Object;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 420668
    :catchall_1
    move-exception v0

    invoke-virtual {v1}, LX/2DZ;->a()V

    throw v0
.end method

.method private b()LX/2Wd;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 420689
    new-instance v7, LX/2YJ;

    invoke-direct {v7}, LX/2YJ;-><init>()V

    .line 420690
    new-instance v8, Ljava/util/ArrayList;

    const/4 v0, 0x4

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    move-object v2, v5

    move v6, v4

    .line 420691
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    iget v0, p0, LX/2Y6;->d:I

    if-ge v6, v0, :cond_7

    :cond_1
    iget-object v0, p0, LX/2Y6;->b:LX/2Y7;

    invoke-virtual {v0}, LX/2Y7;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 420692
    iget-object v0, p0, LX/2Y6;->b:LX/2Y7;

    invoke-virtual {v0}, LX/2Y7;->a()LX/2YK;

    move-result-object v1

    .line 420693
    iget-object v0, v1, LX/2YK;->a:LX/2YB;

    .line 420694
    iget v9, v1, LX/2YK;->b:I

    packed-switch v9, :pswitch_data_0

    .line 420695
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "eventType="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, v1, LX/2YK;->b:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420696
    :pswitch_0
    instance-of v1, v0, LX/2YI;

    if-eqz v1, :cond_3

    .line 420697
    iget-object v1, v0, LX/2YB;->a:Ljava/io/File;

    move-object v0, v1

    .line 420698
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, LX/0WQ;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    invoke-static {v0}, LX/2D7;->a(Z)LX/2D7;

    move-result-object v0

    iput-object v0, p0, LX/2Y6;->g:LX/2D7;

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1

    .line 420699
    :cond_3
    instance-of v1, v0, LX/2Wb;

    if-eqz v1, :cond_0

    .line 420700
    check-cast v0, LX/2Wb;

    .line 420701
    invoke-direct {p0, v0}, LX/2Y6;->a(LX/2Wb;)Z

    move-result v1

    if-eqz v1, :cond_a

    :goto_2
    move-object v2, v0

    .line 420702
    goto :goto_0

    .line 420703
    :pswitch_1
    if-eqz v2, :cond_5

    .line 420704
    iget-object v1, v0, LX/2YB;->a:Ljava/io/File;

    move-object v1, v1

    .line 420705
    invoke-static {v1}, LX/2Y6;->b(Ljava/io/File;)V

    .line 420706
    iget-object v1, v2, LX/2YB;->a:Ljava/io/File;

    move-object v1, v1

    .line 420707
    iget-object v9, v0, LX/2YB;->a:Ljava/io/File;

    move-object v9, v9

    .line 420708
    invoke-virtual {v1, v9}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v2, v5

    .line 420709
    :cond_4
    :goto_3
    instance-of v1, v0, LX/2YI;

    if-eqz v1, :cond_0

    .line 420710
    iget-object v1, p0, LX/2Y6;->c:LX/2Xz;

    .line 420711
    iget-object v9, v0, LX/2YB;->a:Ljava/io/File;

    move-object v0, v9

    .line 420712
    iget-object v9, p0, LX/2Y6;->j:Ljava/io/File;

    .line 420713
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    .line 420714
    iget-object v11, v1, LX/2Xz;->a:LX/2Xy;

    iget-object v11, v11, LX/2Xy;->e:LX/01J;

    invoke-virtual {v11, v10, v9}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420715
    iput-object v5, p0, LX/2Y6;->j:Ljava/io/File;

    goto/16 :goto_0

    :cond_5
    move-object v1, v0

    .line 420716
    check-cast v1, LX/2YA;

    .line 420717
    iget-object v9, v7, LX/2YJ;->a:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420718
    goto :goto_3

    .line 420719
    :pswitch_2
    if-eqz v2, :cond_6

    .line 420720
    iget-object v1, v0, LX/2YB;->a:Ljava/io/File;

    move-object v0, v1

    .line 420721
    iget-object v1, p0, LX/2Y6;->g:LX/2D7;

    invoke-direct {p0, v0, v1}, LX/2Y6;->a(Ljava/io/File;LX/2D7;)Z

    goto/16 :goto_0

    .line 420722
    :cond_6
    new-instance v1, LX/2fL;

    iget-object v9, p0, LX/2Y6;->a:LX/2Y2;

    .line 420723
    iget-object v10, v0, LX/2YB;->a:Ljava/io/File;

    move-object v10, v10

    .line 420724
    iget-object v11, p0, LX/2Y6;->g:LX/2D7;

    invoke-direct {v1, v9, v10, v11}, LX/2fL;-><init>(LX/2Y2;Ljava/io/File;LX/2D7;)V

    .line 420725
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420726
    int-to-long v10, v6

    .line 420727
    iget-object v6, v1, LX/2fL;->f:Ljava/io/File;

    move-object v1, v6

    .line 420728
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v12

    add-long/2addr v10, v12

    long-to-int v1, v10

    .line 420729
    iget-object v6, v0, LX/2YB;->a:Ljava/io/File;

    move-object v0, v6

    .line 420730
    iput-object v0, p0, LX/2Y6;->j:Ljava/io/File;

    move v6, v1

    .line 420731
    goto/16 :goto_0

    .line 420732
    :cond_7
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 420733
    if-lez v0, :cond_8

    .line 420734
    iget v1, p0, LX/2Y6;->k:I

    add-int/2addr v1, v0

    iput v1, p0, LX/2Y6;->k:I

    .line 420735
    if-le v0, v3, :cond_9

    .line 420736
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    .line 420737
    new-instance v0, LX/2fM;

    iget-object v1, p0, LX/2Y6;->a:LX/2Y2;

    invoke-direct {v0, v8, v1}, LX/2fM;-><init>(Ljava/util/List;LX/2Y2;)V

    .line 420738
    :goto_4
    new-instance v5, LX/2YP;

    invoke-direct {v5, v0, v7}, LX/2YP;-><init>(LX/2Wd;LX/2YJ;)V

    .line 420739
    :cond_8
    return-object v5

    .line 420740
    :cond_9
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Wd;

    goto :goto_4

    :cond_a
    move-object v0, v2

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 420653
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    .line 420654
    const-string v0, "FileBatchPayloadIterator"

    const-string v1, "%s: not a directory, deleting anyway..."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 420655
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 420656
    return-void
.end method

.method public static b(Ljava/io/File;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 420650
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 420651
    invoke-static {p0}, LX/2Y6;->b(Ljava/io/File;)V

    .line 420652
    :cond_0
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 420646
    iget-boolean v1, p0, LX/2Y6;->h:Z

    if-nez v1, :cond_0

    .line 420647
    iput-boolean v0, p0, LX/2Y6;->h:Z

    .line 420648
    invoke-direct {p0}, LX/2Y6;->b()LX/2Wd;

    move-result-object v1

    iput-object v1, p0, LX/2Y6;->i:LX/2Wd;

    .line 420649
    :cond_0
    iget-object v1, p0, LX/2Y6;->i:LX/2Wd;

    if-eqz v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 420639
    invoke-virtual {p0}, LX/2Y6;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 420640
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 420641
    :cond_0
    iget-object v0, p0, LX/2Y6;->i:LX/2Wd;

    .line 420642
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2Y6;->h:Z

    .line 420643
    const/4 v1, 0x0

    iput-object v1, p0, LX/2Y6;->i:LX/2Wd;

    .line 420644
    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 420645
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "File removal should be accomplished via markSuccessful"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
