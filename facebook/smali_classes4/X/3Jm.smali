.class public LX/3Jm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 548006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;LX/3JT;)LX/3Jf;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3Jf;",
            ">;",
            "LX/3JT;",
            ")",
            "LX/3Jf;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 548007
    if-nez p0, :cond_0

    move-object v0, v1

    .line 548008
    :goto_0
    return-object v0

    .line 548009
    :cond_0
    const/4 v2, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    :goto_1
    if-ge v2, v4, :cond_3

    .line 548010
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jf;

    .line 548011
    iget-object v5, v0, LX/3Jf;->b:LX/3JT;

    move-object v0, v5

    .line 548012
    if-ne v0, p1, :cond_1

    move v0, v2

    .line 548013
    :goto_2
    if-ne v0, v3, :cond_2

    move-object v0, v1

    .line 548014
    goto :goto_0

    .line 548015
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 548016
    :cond_2
    invoke-interface {p0, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jf;

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_2
.end method
