.class public LX/2G2;
.super LX/0Vx;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2G2;


# direct methods
.method public constructor <init>(LX/2WA;)V
    .locals 0
    .param p1    # LX/2WA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387765
    invoke-direct {p0, p1}, LX/0Vx;-><init>(LX/2WA;)V

    .line 387766
    return-void
.end method

.method public static a(LX/0QB;)LX/2G2;
    .locals 4

    .prologue
    .line 387752
    sget-object v0, LX/2G2;->b:LX/2G2;

    if-nez v0, :cond_1

    .line 387753
    const-class v1, LX/2G2;

    monitor-enter v1

    .line 387754
    :try_start_0
    sget-object v0, LX/2G2;->b:LX/2G2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387755
    if-eqz v2, :cond_0

    .line 387756
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 387757
    new-instance p0, LX/2G2;

    invoke-static {v0}, LX/0Ws;->a(LX/0QB;)LX/2WA;

    move-result-object v3

    check-cast v3, LX/2WA;

    invoke-direct {p0, v3}, LX/2G2;-><init>(LX/2WA;)V

    .line 387758
    move-object v0, p0

    .line 387759
    sput-object v0, LX/2G2;->b:LX/2G2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387760
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387761
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387762
    :cond_1
    sget-object v0, LX/2G2;->b:LX/2G2;

    return-object v0

    .line 387763
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387764
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 387751
    const-string v0, "generic_analytic_counters"

    return-object v0
.end method
