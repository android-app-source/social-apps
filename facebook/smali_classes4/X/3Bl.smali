.class public LX/3Bl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:I

.field private b:Z

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 528959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528960
    iput v0, p0, LX/3Bl;->a:I

    .line 528961
    iput-boolean v0, p0, LX/3Bl;->b:Z

    .line 528962
    iput-object p1, p0, LX/3Bl;->d:Ljava/lang/Integer;

    .line 528963
    iput-object p2, p0, LX/3Bl;->c:Ljava/lang/String;

    .line 528964
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 528965
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/3Bl;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/3Bl;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 528966
    monitor-exit p0

    return-void

    .line 528967
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 528968
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/3Bl;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/3Bl;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 528969
    monitor-exit p0

    return-void

    .line 528970
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 528971
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/3Bl;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 528972
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget v1, p0, LX/3Bl;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 528973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 528974
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/3Bl;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 528975
    monitor-exit p0

    return-void

    .line 528976
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
