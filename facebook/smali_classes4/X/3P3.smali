.class public final LX/3P3;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/divebar/contacts/DivebarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V
    .locals 0

    .prologue
    .line 561039
    iput-object p1, p0, LX/3P3;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 561040
    sget-object v0, Lcom/facebook/divebar/contacts/DivebarFragment;->a:Ljava/lang/Class;

    const-string v1, "Failed to load chat contexts"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 561041
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 561042
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 561043
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/server/FetchChatContextResult;

    .line 561044
    iget-object v1, p0, LX/3P3;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    .line 561045
    iget-object p0, v0, Lcom/facebook/contacts/server/FetchChatContextResult;->a:LX/0P1;

    move-object v0, p0

    .line 561046
    iput-object v0, v1, Lcom/facebook/divebar/contacts/DivebarFragment;->U:LX/0P1;

    .line 561047
    invoke-static {v1}, Lcom/facebook/divebar/contacts/DivebarFragment;->r(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    .line 561048
    return-void
.end method
