.class public LX/2S9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2S9;


# instance fields
.field private final b:LX/0W3;

.field private final c:LX/2SA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2SA",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/text/SimpleDateFormat;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 411865
    const-class v0, LX/2S9;

    sput-object v0, LX/2S9;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0W3;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 411860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 411861
    const/16 v0, 0x3e8

    invoke-static {v0}, LX/2SA;->a(I)LX/2SA;

    move-result-object v0

    iput-object v0, p0, LX/2S9;->c:LX/2SA;

    .line 411862
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd HH:mm:ss.SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, LX/2S9;->d:Ljava/text/SimpleDateFormat;

    .line 411863
    iput-object p1, p0, LX/2S9;->b:LX/0W3;

    .line 411864
    return-void
.end method

.method public static a(LX/0QB;)LX/2S9;
    .locals 4

    .prologue
    .line 411815
    sget-object v0, LX/2S9;->e:LX/2S9;

    if-nez v0, :cond_1

    .line 411816
    const-class v1, LX/2S9;

    monitor-enter v1

    .line 411817
    :try_start_0
    sget-object v0, LX/2S9;->e:LX/2S9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 411818
    if-eqz v2, :cond_0

    .line 411819
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 411820
    new-instance p0, LX/2S9;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/2S9;-><init>(LX/0W3;)V

    .line 411821
    move-object v0, p0

    .line 411822
    sput-object v0, LX/2S9;->e:LX/2S9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 411823
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 411824
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 411825
    :cond_1
    sget-object v0, LX/2S9;->e:LX/2S9;

    return-object v0

    .line 411826
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 411827
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/io/File;)Landroid/net/Uri;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 411841
    new-instance v0, Ljava/io/File;

    const-string v1, "rtc_log.txt"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 411842
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 411843
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 411844
    :try_start_1
    new-instance v5, Ljava/io/PrintWriter;

    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v5, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 411845
    :try_start_2
    iget-object v0, p0, LX/2S9;->c:LX/2SA;

    invoke-virtual {v0}, LX/306;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 411846
    invoke-virtual {v5, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    goto :goto_0

    .line 411847
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 411848
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v5}, Ljava/io/PrintWriter;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_2
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 411849
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 411850
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v2, :cond_2

    :try_start_7
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_2

    :goto_4
    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_2

    :catch_2
    move-exception v0

    .line 411851
    sget-object v1, LX/2S9;->a:Ljava/lang/Class;

    const-string v2, "Cannot create/open trace file"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 411852
    :goto_5
    return-object v3

    .line 411853
    :cond_0
    :try_start_9
    invoke-virtual {v5}, Ljava/io/PrintWriter;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 411854
    :try_start_a
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_5

    .line 411855
    :catch_3
    move-exception v5

    :try_start_b
    invoke-static {v1, v5}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 411856
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 411857
    :cond_1
    invoke-virtual {v5}, Ljava/io/PrintWriter;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto :goto_2

    .line 411858
    :catch_4
    move-exception v1

    :try_start_c
    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_2
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_2

    goto :goto_4

    .line 411859
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 411866
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 411867
    iget-object v1, p0, LX/2S9;->d:Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411868
    const-string v1, "> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411869
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411870
    iget-object v1, p0, LX/2S9;->c:LX/2SA;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/306;->add(Ljava/lang/Object;)Z

    .line 411871
    return-void
.end method

.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 411833
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 411834
    :try_start_0
    iget-object v1, p0, LX/2S9;->c:LX/2SA;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2S9;->c:LX/2SA;

    invoke-virtual {v1}, LX/306;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 411835
    invoke-direct {p0, p1}, LX/2S9;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 411836
    const-string v2, "rtc_log.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411837
    :cond_0
    return-object v0

    .line 411838
    :catch_0
    move-exception v0

    .line 411839
    sget-object v1, LX/2S9;->a:Ljava/lang/Class;

    const-string v2, "Exception saving rtc trace"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 411840
    throw v0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 411828
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 411829
    iget-object v1, p0, LX/2S9;->c:LX/2SA;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2S9;->c:LX/2SA;

    invoke-virtual {v1}, LX/306;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 411830
    invoke-direct {p0, p1}, LX/2S9;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 411831
    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v3, "rtc_log.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "text/plain"

    invoke-direct {v2, v3, v1, v4}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 411832
    :cond_0
    return-object v0
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 411814
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 411813
    iget-object v0, p0, LX/2S9;->b:LX/0W3;

    sget-wide v2, LX/0X5;->bh:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
