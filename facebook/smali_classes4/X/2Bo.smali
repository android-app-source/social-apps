.class public LX/2Bo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/42P;",
        "Lcom/facebook/auth/component/AuthenticationResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;

.field private final b:LX/2Xm;


# direct methods
.method public constructor <init>(LX/0dC;LX/2Xm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 381493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381494
    iput-object p1, p0, LX/2Bo;->a:LX/0dC;

    .line 381495
    iput-object p2, p0, LX/2Bo;->b:LX/2Xm;

    .line 381496
    return-void
.end method

.method public static a(LX/0QB;)LX/2Bo;
    .locals 3

    .prologue
    .line 381497
    new-instance v2, LX/2Bo;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v0

    check-cast v0, LX/0dC;

    invoke-static {p0}, LX/2Xm;->b(LX/0QB;)LX/2Xm;

    move-result-object v1

    check-cast v1, LX/2Xm;

    invoke-direct {v2, v0, v1}, LX/2Bo;-><init>(LX/0dC;LX/2Xm;)V

    .line 381498
    move-object v0, v2

    .line 381499
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 381500
    check-cast p1, LX/42P;

    .line 381501
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 381502
    const-string v1, "format"

    const-string v2, "json"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381503
    const-string v1, "account_id"

    iget-object v2, p1, LX/42P;->a:Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    iget-object v2, v2, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381504
    iget-object v1, p1, LX/42P;->a:Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    iget-object v1, v1, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->f:Ljava/util/Calendar;

    if-eqz v1, :cond_0

    .line 381505
    const-string v1, "birthday"

    iget-object v2, p1, LX/42P;->a:Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    iget-object v2, v2, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->f:Ljava/util/Calendar;

    .line 381506
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 381507
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 381508
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381509
    :cond_0
    const-string v1, "device_id"

    iget-object v2, p0, LX/2Bo;->a:LX/0dC;

    invoke-virtual {v2}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381510
    iget-boolean v1, p1, LX/42P;->b:Z

    if-eqz v1, :cond_1

    .line 381511
    const-string v1, "generate_session_cookies"

    const-string v2, "1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381512
    :cond_1
    iget-object v1, p1, LX/42P;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 381513
    const-string v1, "machine_id"

    iget-object v2, p1, LX/42P;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381514
    :goto_0
    const-string v1, "code"

    iget-object v2, p1, LX/42P;->a:Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    iget-object v2, v2, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381515
    const-string v1, "account_recovery_id"

    iget-object v2, p1, LX/42P;->a:Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    iget-object v2, v2, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381516
    iget-object v1, p1, LX/42P;->a:Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    iget-object v1, v1, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->e:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 381517
    const-string v1, "new_account_recovery_id"

    iget-object v2, p1, LX/42P;->a:Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    iget-object v2, v2, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381518
    :cond_2
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/11I;->LOGIN_MESSENGER_CREDS_BYPASS:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 381519
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 381520
    move-object v1, v1

    .line 381521
    const-string v2, "POST"

    .line 381522
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 381523
    move-object v1, v1

    .line 381524
    const-string v2, "method/user.bypassLoginWithConfirmedMessengerCredentials"

    .line 381525
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 381526
    move-object v1, v1

    .line 381527
    invoke-virtual {v1, v0}, LX/14O;->a(Ljava/util/Map;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 381528
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 381529
    move-object v0, v0

    .line 381530
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 381531
    :cond_3
    const-string v1, "generate_machine_id"

    const-string v2, "1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 381532
    check-cast p1, LX/42P;

    .line 381533
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 381534
    iget-object v0, p1, LX/42P;->a:Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    iget-object v0, v0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;->b:Ljava/lang/String;

    .line 381535
    iget-object v1, p0, LX/2Bo;->b:LX/2Xm;

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v2

    iget-boolean v3, p1, LX/42P;->b:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2Xm;->a(LX/0lF;Ljava/lang/String;ZLjava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method
