.class public LX/2TL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2TL;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414076
    iput-object p1, p0, LX/2TL;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 414077
    return-void
.end method

.method public static a(LX/0QB;)LX/2TL;
    .locals 4

    .prologue
    .line 414078
    sget-object v0, LX/2TL;->b:LX/2TL;

    if-nez v0, :cond_1

    .line 414079
    const-class v1, LX/2TL;

    monitor-enter v1

    .line 414080
    :try_start_0
    sget-object v0, LX/2TL;->b:LX/2TL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 414081
    if-eqz v2, :cond_0

    .line 414082
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 414083
    new-instance p0, LX/2TL;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/2TL;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 414084
    move-object v0, p0

    .line 414085
    sput-object v0, LX/2TL;->b:LX/2TL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414086
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 414087
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 414088
    :cond_1
    sget-object v0, LX/2TL;->b:LX/2TL;

    return-object v0

    .line 414089
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 414090
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 414091
    iget-object v0, p0, LX/2TL;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2TR;->i:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method
