.class public LX/2Os;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2Os;


# instance fields
.field private final a:LX/0lC;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/03V;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/Jcq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lC;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 403388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403389
    iput-object p1, p0, LX/2Os;->a:LX/0lC;

    .line 403390
    iput-object p2, p0, LX/2Os;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 403391
    iput-object p3, p0, LX/2Os;->c:LX/03V;

    .line 403392
    iput-object p4, p0, LX/2Os;->d:LX/0Or;

    .line 403393
    return-void
.end method

.method public static a(LX/0QB;)LX/2Os;
    .locals 7

    .prologue
    .line 403375
    sget-object v0, LX/2Os;->f:LX/2Os;

    if-nez v0, :cond_1

    .line 403376
    const-class v1, LX/2Os;

    monitor-enter v1

    .line 403377
    :try_start_0
    sget-object v0, LX/2Os;->f:LX/2Os;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 403378
    if-eqz v2, :cond_0

    .line 403379
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 403380
    new-instance v6, LX/2Os;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/2Os;-><init>(LX/0lC;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;LX/0Or;)V

    .line 403381
    move-object v0, v6

    .line 403382
    sput-object v0, LX/2Os;->f:LX/2Os;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403383
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 403384
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 403385
    :cond_1
    sget-object v0, LX/2Os;->f:LX/2Os;

    return-object v0

    .line 403386
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 403387
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;
    .locals 3

    .prologue
    .line 403370
    :try_start_0
    iget-object v0, p0, LX/2Os;->a:LX/0lC;

    const-class v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    invoke-virtual {v0, p1, v1}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 403371
    :goto_0
    return-object v0

    .line 403372
    :catch_0
    move-exception v0

    .line 403373
    iget-object v1, p0, LX/2Os;->c:LX/03V;

    const-string v2, "Corrupt MessengerAccountInfo Read"

    invoke-virtual {v1, v2, p1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 403374
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/2Os;)V
    .locals 1

    .prologue
    .line 403324
    iget-object v0, p0, LX/2Os;->e:LX/Jcq;

    if-eqz v0, :cond_0

    .line 403325
    iget-object v0, p0, LX/2Os;->e:LX/Jcq;

    .line 403326
    iget-boolean p0, v0, LX/Jcq;->o:Z

    if-eqz p0, :cond_1

    .line 403327
    :cond_0
    :goto_0
    return-void

    .line 403328
    :cond_1
    invoke-static {v0}, LX/Jcq;->f(LX/Jcq;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 403363
    invoke-static {p1}, LX/2Vv;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    .line 403364
    monitor-enter p0

    .line 403365
    :try_start_0
    iget-object v2, p0, LX/2Os;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 403366
    if-eqz v1, :cond_0

    .line 403367
    invoke-direct {p0, v1}, LX/2Os;->c(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v0

    monitor-exit p0

    .line 403368
    :goto_0
    return-object v0

    .line 403369
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 403355
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x5

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 403356
    monitor-enter p0

    .line 403357
    :try_start_0
    iget-object v0, p0, LX/2Os;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2Vv;->e:LX/0Tn;

    invoke-interface {v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 403358
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, LX/2Os;->c(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v0

    .line 403359
    if-eqz v0, :cond_0

    .line 403360
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 403361
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403362
    return-object v1
.end method

.method public final a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V
    .locals 4

    .prologue
    .line 403346
    iget-object v0, p1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-static {v0}, LX/2Vv;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 403347
    monitor-enter p0

    .line 403348
    :try_start_0
    iget-object v1, p0, LX/2Os;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v2, p0, LX/2Os;->a:LX/0lC;

    invoke-virtual {v2, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403349
    :goto_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403350
    invoke-static {p0}, LX/2Os;->d(LX/2Os;)V

    .line 403351
    return-void

    .line 403352
    :catch_0
    move-exception v0

    .line 403353
    :try_start_2
    iget-object v1, p0, LX/2Os;->c:LX/03V;

    const-string v2, "Corrupt MessengerAccountInfo Write"

    const-string v3, ""

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 403354
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 403339
    invoke-static {p1}, LX/2Vv;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 403340
    monitor-enter p0

    .line 403341
    :try_start_0
    iget-object v1, p0, LX/2Os;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 403342
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403343
    invoke-static {p0}, LX/2Os;->d(LX/2Os;)V

    .line 403344
    return-void

    .line 403345
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 403338
    invoke-virtual {p0}, LX/2Os;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 403329
    iget-object v0, p0, LX/2Os;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 403330
    const/4 v1, 0x0

    .line 403331
    if-eqz v0, :cond_2

    .line 403332
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 403333
    invoke-virtual {p0, v1}, LX/2Os;->a(Ljava/lang/String;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v1

    .line 403334
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->name:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 403335
    :cond_0
    invoke-static {v0}, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->a(Lcom/facebook/user/model/User;)Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    move-result-object v0

    .line 403336
    invoke-virtual {p0, v0}, LX/2Os;->a(Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;)V

    .line 403337
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
