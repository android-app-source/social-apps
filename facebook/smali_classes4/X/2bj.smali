.class public LX/2bj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Zb;

.field public final b:Lcom/facebook/common/time/RealtimeSinceBootClock;

.field public c:J


# direct methods
.method public constructor <init>(LX/0Zb;Lcom/facebook/common/time/RealtimeSinceBootClock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 439582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439583
    iput-object p1, p0, LX/2bj;->a:LX/0Zb;

    .line 439584
    iput-object p2, p0, LX/2bj;->b:Lcom/facebook/common/time/RealtimeSinceBootClock;

    .line 439585
    return-void
.end method

.method public static a(LX/0QB;)LX/2bj;
    .locals 5

    .prologue
    .line 439586
    const-class v1, LX/2bj;

    monitor-enter v1

    .line 439587
    :try_start_0
    sget-object v0, LX/2bj;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 439588
    sput-object v2, LX/2bj;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 439589
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439590
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 439591
    new-instance p0, LX/2bj;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-direct {p0, v3, v4}, LX/2bj;-><init>(LX/0Zb;Lcom/facebook/common/time/RealtimeSinceBootClock;)V

    .line 439592
    move-object v0, p0

    .line 439593
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 439594
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2bj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439595
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 439596
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 439597
    iget-object v0, p0, LX/2bj;->a:LX/0Zb;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 439598
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 439599
    const-string v1, "background_location"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "session_id"

    iget-wide v2, p0, LX/2bj;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 439600
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0yG;)V
    .locals 4

    .prologue
    .line 439601
    iget-object v0, p0, LX/2bj;->a:LX/0Zb;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 439602
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 439603
    const-string v1, "background_location"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "session_id"

    iget-wide v2, p0, LX/2bj;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v0

    const-string v1, "location_state"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 439604
    :cond_0
    return-void
.end method
