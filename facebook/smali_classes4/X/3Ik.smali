.class public LX/3Ik;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 547084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547085
    return-void
.end method

.method public static a(LX/1PT;)LX/04D;
    .locals 1

    .prologue
    .line 547086
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/3Ik;->a(LX/1PT;Ljava/lang/String;)LX/04D;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1PT;Ljava/lang/String;)LX/04D;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 547087
    if-nez p0, :cond_0

    .line 547088
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    .line 547089
    :goto_0
    return-object v0

    .line 547090
    :cond_0
    sget-object v0, LX/3Il;->a:[I

    invoke-interface {p0}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    invoke-virtual {v1}, LX/1Qt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 547091
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    goto :goto_0

    .line 547092
    :pswitch_0
    sget-object v0, LX/04D;->FEED:LX/04D;

    goto :goto_0

    .line 547093
    :pswitch_1
    invoke-static {p1}, LX/3In;->a(Ljava/lang/String;)LX/04D;

    move-result-object v0

    goto :goto_0

    .line 547094
    :pswitch_2
    sget-object v0, LX/04D;->USER_TIMELINE:LX/04D;

    goto :goto_0

    .line 547095
    :pswitch_3
    sget-object v0, LX/04D;->PROFILE_VIDEO_HUB:LX/04D;

    goto :goto_0

    .line 547096
    :pswitch_4
    sget-object v0, LX/04D;->PAGE_TIMELINE:LX/04D;

    goto :goto_0

    .line 547097
    :pswitch_5
    sget-object v0, LX/04D;->GROUP:LX/04D;

    goto :goto_0

    .line 547098
    :pswitch_6
    sget-object v0, LX/04D;->EVENT:LX/04D;

    goto :goto_0

    .line 547099
    :pswitch_7
    sget-object v0, LX/04D;->VIDEO_HOME:LX/04D;

    goto :goto_0

    .line 547100
    :pswitch_8
    sget-object v0, LX/04D;->SEARCH_RESULTS:LX/04D;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method
