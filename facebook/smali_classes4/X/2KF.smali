.class public LX/2KF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:Ljava/security/SecureRandom;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field public final f:LX/0Tn;

.field private final g:I

.field public final h:Ljava/util/Date;

.field public final i:Ljava/util/Date;

.field private final j:LX/F6o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/security/SecureRandom;LX/2Cm;)V
    .locals 9
    .param p3    # Ljava/security/SecureRandom;
        .annotation runtime Lcom/facebook/common/random/FixedSecureRandom;
        .end annotation
    .end param
    .param p4    # LX/2Cm;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 393570
    invoke-virtual {p4}, LX/2Cm;->name()Ljava/lang/String;

    move-result-object v4

    iget v5, p4, LX/2Cm;->threshold:I

    iget-object v6, p4, LX/2Cm;->startDate:Ljava/util/Date;

    iget-object v7, p4, LX/2Cm;->endDate:Ljava/util/Date;

    iget-object v8, p4, LX/2Cm;->condition:LX/F6o;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v8}, LX/2KF;-><init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/security/SecureRandom;Ljava/lang/String;ILjava/util/Date;Ljava/util/Date;LX/F6o;)V

    .line 393571
    return-void
.end method

.method private constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/security/SecureRandom;Ljava/lang/String;ILjava/util/Date;Ljava/util/Date;LX/F6o;)V
    .locals 1
    .param p8    # LX/F6o;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 393572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393573
    iput-object p1, p0, LX/2KF;->a:LX/0SG;

    .line 393574
    iput-object p2, p0, LX/2KF;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 393575
    iput-object p3, p0, LX/2KF;->c:Ljava/security/SecureRandom;

    .line 393576
    iput-object p4, p0, LX/2KF;->d:Ljava/lang/String;

    .line 393577
    invoke-static {p4}, LX/2KF;->getSystemPropertyKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2KF;->e:Ljava/lang/String;

    .line 393578
    invoke-static {p4}, LX/2KF;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    iput-object v0, p0, LX/2KF;->f:LX/0Tn;

    .line 393579
    iput p5, p0, LX/2KF;->g:I

    .line 393580
    iput-object p6, p0, LX/2KF;->h:Ljava/util/Date;

    .line 393581
    iput-object p7, p0, LX/2KF;->i:Ljava/util/Date;

    .line 393582
    iput-object p8, p0, LX/2KF;->j:LX/F6o;

    .line 393583
    return-void
.end method

.method private static a(Ljava/lang/String;)LX/0Tn;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 393584
    sget-object v0, LX/285;->b:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static getSystemPropertyKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 393585
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "fb.exp."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/03R;
    .locals 9

    .prologue
    const/16 v4, 0x2710

    .line 393586
    iget-object v0, p0, LX/2KF;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->getInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 393587
    invoke-static {}, LX/0i9;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 393588
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 393589
    :goto_0
    return-object v0

    .line 393590
    :cond_0
    if-eqz v0, :cond_3

    .line 393591
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 393592
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 393593
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_2

    .line 393594
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 393595
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 393596
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0

    .line 393597
    :cond_3
    iget-object v0, p0, LX/2KF;->e:Ljava/lang/String;

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, LX/011;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 393598
    const-wide/16 v2, 0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_4

    .line 393599
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 393600
    :cond_4
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_5

    .line 393601
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 393602
    :cond_5
    const-wide/16 v2, 0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    .line 393603
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0

    .line 393604
    :cond_6
    iget v0, p0, LX/2KF;->g:I

    if-nez v0, :cond_7

    .line 393605
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0

    .line 393606
    :cond_7
    iget-object v0, p0, LX/2KF;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 393607
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0

    .line 393608
    :cond_8
    iget-object v0, p0, LX/2KF;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/2KF;->f:LX/0Tn;

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 393609
    if-ltz v0, :cond_9

    if-lt v0, v4, :cond_a

    .line 393610
    :cond_9
    iget-object v0, p0, LX/2KF;->c:Ljava/security/SecureRandom;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v0

    .line 393611
    iget-object v1, p0, LX/2KF;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    iget-object v2, p0, LX/2KF;->f:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 393612
    move v0, v0

    .line 393613
    :cond_a
    if-ltz v0, :cond_c

    if-ge v0, v4, :cond_c

    .line 393614
    new-instance v5, Ljava/util/Date;

    iget-object v6, p0, LX/2KF;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v7

    invoke-direct {v5, v7, v8}, Ljava/util/Date;-><init>(J)V

    .line 393615
    iget-object v6, p0, LX/2KF;->h:Ljava/util/Date;

    invoke-virtual {v5, v6}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v6

    if-nez v6, :cond_b

    iget-object v6, p0, LX/2KF;->i:Ljava/util/Date;

    invoke-virtual {v5, v6}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v5

    if-eqz v5, :cond_10

    :cond_b
    const/4 v5, 0x1

    :goto_1
    move v1, v5

    .line 393616
    if-nez v1, :cond_c

    iget-object v1, p0, LX/2KF;->j:LX/F6o;

    if-eqz v1, :cond_d

    iget-object v1, p0, LX/2KF;->j:LX/F6o;

    invoke-interface {v1}, LX/F6o;->a()Z

    move-result v1

    if-nez v1, :cond_d

    .line 393617
    :cond_c
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto/16 :goto_0

    .line 393618
    :cond_d
    iget v1, p0, LX/2KF;->g:I

    if-ge v0, v1, :cond_e

    .line 393619
    sget-object v0, LX/03R;->YES:LX/03R;

    goto/16 :goto_0

    .line 393620
    :cond_e
    iget v1, p0, LX/2KF;->g:I

    mul-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_f

    .line 393621
    sget-object v0, LX/03R;->NO:LX/03R;

    goto/16 :goto_0

    .line 393622
    :cond_f
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto/16 :goto_0

    :cond_10
    const/4 v5, 0x0

    goto :goto_1
.end method
