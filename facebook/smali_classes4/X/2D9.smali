.class public LX/2D9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation


# static fields
.field public static final a:J

.field public static final b:J

.field public static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 383713
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/2D9;->a:J

    .line 383714
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/2D9;->b:J

    .line 383715
    new-instance v0, LX/2DA;

    invoke-direct {v0}, LX/2DA;-><init>()V

    sput-object v0, LX/2D9;->c:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 383716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383717
    return-void
.end method

.method public static a()J
    .locals 4

    .prologue
    .line 383718
    invoke-static {}, LX/2D9;->c()J

    move-result-wide v0

    sget-wide v2, LX/2D9;->a:J

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public static b()J
    .locals 4

    .prologue
    .line 383719
    invoke-static {}, LX/2D9;->c()J

    move-result-wide v0

    sget-wide v2, LX/2D9;->b:J

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public static b(Ljava/io/File;)Ljava/io/File;
    .locals 11

    .prologue
    .line 383720
    new-instance v0, Ljava/io/File;

    .line 383721
    new-instance v3, Ljava/io/File;

    .line 383722
    new-instance v7, Ljava/io/File;

    invoke-static {}, LX/2D9;->a()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, p0, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v4, v7

    .line 383723
    invoke-static {}, LX/2D9;->b()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v1, v3

    .line 383724
    invoke-static {}, LX/2D9;->c()J

    move-result-wide v3

    .line 383725
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "batch-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".json"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v3, v5

    .line 383726
    move-object v2, v3

    .line 383727
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static c()J
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.System.currentTimeMillis"
        }
    .end annotation

    .prologue
    .line 383728
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method
