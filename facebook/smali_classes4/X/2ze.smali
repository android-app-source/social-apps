.class public final LX/2ze;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source ""


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 483690
    const-string v0, "app_updates_db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 483691
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 483692
    const-string v0, "CREATE TABLE app_updates (id INTEGER PRIMARY KEY, data BLOB)"

    const v1, -0x4e8b731d

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6735a3ee

    invoke-static {v0}, LX/03h;->a(I)V

    .line 483693
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 483694
    const-string v0, "DROP TABLE IF EXISTS app_updates"

    const v1, 0x263a5527

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x52c0c841

    invoke-static {v0}, LX/03h;->a(I)V

    .line 483695
    invoke-virtual {p0, p1}, LX/2ze;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 483696
    return-void
.end method
