.class public LX/2Jc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/K3Z;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/K3Z;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 393092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393093
    iput-object p1, p0, LX/2Jc;->b:LX/0Or;

    .line 393094
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 393089
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 393091
    sget-object v0, LX/2Jm;->INTERVAL:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<",
            "LX/K3Z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 393095
    iget-object v0, p0, LX/2Jc;->b:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 2

    .prologue
    .line 393090
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Jh;->BACKGROUND:LX/2Jh;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jh;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jq;->LOGGED_IN:LX/2Jq;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jq;)LX/2Jk;

    move-result-object v0

    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 393088
    const-wide v0, 0x90321000L

    return-wide v0
.end method
