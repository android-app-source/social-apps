.class public LX/26O;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/26N;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/26L;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/26L",
            "<TT;>;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:I

.field public f:[Landroid/graphics/Rect;

.field public g:[Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/26L;LX/0yc;)V
    .locals 11
    .param p2    # LX/26L;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/26L",
            "<TT;>;",
            "LX/0yc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 372069
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372070
    iput-object p1, p0, LX/26O;->a:Landroid/content/Context;

    .line 372071
    iput-object p2, p0, LX/26O;->b:LX/26L;

    .line 372072
    const/high16 v10, 0x3f000000    # 0.5f

    .line 372073
    iget-object v0, p0, LX/26O;->b:LX/26L;

    invoke-interface {v0}, LX/26L;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    .line 372074
    iget-object v0, p0, LX/26O;->a:Landroid/content/Context;

    iget-object v1, p0, LX/26O;->b:LX/26L;

    invoke-interface {v1}, LX/26L;->b()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/26O;->c:I

    .line 372075
    iget-object v0, p0, LX/26O;->b:LX/26L;

    invoke-interface {v0}, LX/26L;->c()I

    move-result v0

    iput v0, p0, LX/26O;->d:I

    .line 372076
    new-array v0, v3, [Landroid/graphics/Rect;

    iput-object v0, p0, LX/26O;->f:[Landroid/graphics/Rect;

    .line 372077
    new-array v0, v3, [Landroid/graphics/PointF;

    iput-object v0, p0, LX/26O;->g:[Landroid/graphics/PointF;

    .line 372078
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 372079
    iget-object v0, p0, LX/26O;->b:LX/26L;

    invoke-interface {v0}, LX/26L;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26N;

    .line 372080
    invoke-interface {v0}, LX/26N;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 372081
    iget-object v4, p0, LX/26O;->b:LX/26L;

    invoke-interface {v4, v0}, LX/26L;->a(LX/26N;)I

    move-result v4

    .line 372082
    iget-object v5, p0, LX/26O;->b:LX/26L;

    invoke-interface {v5, v0}, LX/26L;->b(LX/26N;)I

    move-result v5

    .line 372083
    iget-object v6, p0, LX/26O;->b:LX/26L;

    invoke-interface {v6, v0}, LX/26L;->c(LX/26N;)I

    move-result v6

    .line 372084
    iget-object v7, p0, LX/26O;->b:LX/26L;

    invoke-interface {v7, v0}, LX/26L;->d(LX/26N;)I

    move-result v0

    .line 372085
    iget-object v7, p0, LX/26O;->f:[Landroid/graphics/Rect;

    new-instance v8, Landroid/graphics/Rect;

    add-int/2addr v6, v4

    add-int v9, v5, v0

    invoke-direct {v8, v4, v5, v6, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v8, v7, v2

    .line 372086
    iget v4, p0, LX/26O;->e:I

    add-int/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/26O;->e:I

    .line 372087
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v0

    .line 372088
    :goto_1
    iget-object v4, p0, LX/26O;->g:[Landroid/graphics/PointF;

    if-eqz v0, :cond_1

    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v6

    double-to-float v5, v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v6

    double-to-float v0, v6

    invoke-direct {v1, v5, v0}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v0, v1

    :goto_2
    aput-object v0, v4, v2

    .line 372089
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 372090
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 372091
    :cond_1
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v10, v10}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_2

    .line 372092
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 372110
    iget v0, p0, LX/26O;->c:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, p1, v0

    .line 372111
    iget v1, p0, LX/26O;->d:I

    div-int/2addr v0, v1

    .line 372112
    iget v1, p0, LX/26O;->e:I

    mul-int/2addr v0, v1

    .line 372113
    iget v1, p0, LX/26O;->c:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method

.method public final a(II)I
    .locals 1

    .prologue
    .line 372107
    iget-object v0, p0, LX/26O;->f:[Landroid/graphics/Rect;

    aget-object v0, v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-nez v0, :cond_0

    .line 372108
    const/4 p2, 0x0

    .line 372109
    :cond_0
    return p2
.end method

.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 372106
    iget-object v0, p0, LX/26O;->b:LX/26L;

    invoke-interface {v0}, LX/26L;->a()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(IILandroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 372098
    iget v0, p0, LX/26O;->c:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, p1, v0

    .line 372099
    iget v1, p0, LX/26O;->d:I

    div-int/2addr v0, v1

    .line 372100
    iget v1, p0, LX/26O;->c:I

    iget-object v2, p0, LX/26O;->f:[Landroid/graphics/Rect;

    aget-object v2, v2, p2

    iget v2, v2, Landroid/graphics/Rect;->left:I

    mul-int/2addr v2, v0

    add-int/2addr v1, v2

    .line 372101
    iget v2, p0, LX/26O;->c:I

    iget-object v3, p0, LX/26O;->f:[Landroid/graphics/Rect;

    aget-object v3, v3, p2

    iget v3, v3, Landroid/graphics/Rect;->top:I

    mul-int/2addr v3, v0

    add-int/2addr v2, v3

    .line 372102
    iget-object v3, p0, LX/26O;->f:[Landroid/graphics/Rect;

    aget-object v3, v3, p2

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    mul-int/2addr v3, v0

    .line 372103
    iget-object v4, p0, LX/26O;->f:[Landroid/graphics/Rect;

    aget-object v4, v4, p2

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    mul-int/2addr v0, v4

    .line 372104
    add-int/2addr v3, v1

    add-int/2addr v0, v2

    invoke-virtual {p3, v1, v2, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 372105
    return-void
.end method

.method public final b(II)I
    .locals 2

    .prologue
    .line 372094
    iget-object v0, p0, LX/26O;->f:[Landroid/graphics/Rect;

    aget-object v0, v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/26O;->f:[Landroid/graphics/Rect;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int/2addr v0, v1

    .line 372095
    iget v1, p0, LX/26O;->d:I

    if-ne v0, v1, :cond_0

    .line 372096
    const/4 p2, 0x0

    .line 372097
    :cond_0
    return p2
.end method

.method public final b(I)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 372093
    iget-object v0, p0, LX/26O;->g:[Landroid/graphics/PointF;

    aget-object v0, v0, p1

    return-object v0
.end method
