.class public LX/2L0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile n:LX/2L0;


# instance fields
.field public final b:LX/0aG;

.field private final c:LX/2L1;

.field private final d:LX/0kb;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field private final f:LX/0SG;

.field public final g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final h:LX/0tX;

.field public final i:LX/1dx;

.field public final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2Pa;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/1dw;

.field private final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2LB;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0SI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 394390
    const-class v0, LX/2L0;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2L0;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/2L1;LX/0kb;Ljava/util/concurrent/ExecutorService;LX/0SG;LX/0tX;LX/1dx;Ljava/util/Set;LX/1dw;LX/0SI;)V
    .locals 2
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/2L1;",
            "LX/0kb;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0SG;",
            "LX/0tX;",
            "LX/1dx;",
            "Ljava/util/Set",
            "<",
            "LX/2Pa;",
            ">;",
            "LX/1dw;",
            "LX/0SI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394392
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/2L0;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 394393
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2L0;->l:Ljava/util/List;

    .line 394394
    iput-object p1, p0, LX/2L0;->b:LX/0aG;

    .line 394395
    iput-object p2, p0, LX/2L0;->c:LX/2L1;

    .line 394396
    iput-object p3, p0, LX/2L0;->d:LX/0kb;

    .line 394397
    iput-object p4, p0, LX/2L0;->e:Ljava/util/concurrent/ExecutorService;

    .line 394398
    iput-object p5, p0, LX/2L0;->f:LX/0SG;

    .line 394399
    iput-object p6, p0, LX/2L0;->h:LX/0tX;

    .line 394400
    iput-object p7, p0, LX/2L0;->i:LX/1dx;

    .line 394401
    iput-object p8, p0, LX/2L0;->j:Ljava/util/Set;

    .line 394402
    iput-object p9, p0, LX/2L0;->k:LX/1dw;

    .line 394403
    iput-object p10, p0, LX/2L0;->m:LX/0SI;

    .line 394404
    return-void
.end method

.method public static a(LX/0QB;)LX/2L0;
    .locals 14

    .prologue
    .line 394405
    sget-object v0, LX/2L0;->n:LX/2L0;

    if-nez v0, :cond_1

    .line 394406
    const-class v1, LX/2L0;

    monitor-enter v1

    .line 394407
    :try_start_0
    sget-object v0, LX/2L0;->n:LX/2L0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394408
    if-eqz v2, :cond_0

    .line 394409
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 394410
    new-instance v3, LX/2L0;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/2L1;->a(LX/0QB;)LX/2L1;

    move-result-object v5

    check-cast v5, LX/2L1;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {v0}, LX/1dx;->a(LX/0QB;)LX/1dx;

    move-result-object v10

    check-cast v10, LX/1dx;

    .line 394411
    new-instance v11, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v12

    new-instance v13, LX/2L6;

    invoke-direct {v13, v0}, LX/2L6;-><init>(LX/0QB;)V

    invoke-direct {v11, v12, v13}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v11, v11

    .line 394412
    invoke-static {v0}, LX/1dw;->a(LX/0QB;)LX/1dw;

    move-result-object v12

    check-cast v12, LX/1dw;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v13

    check-cast v13, LX/0SI;

    invoke-direct/range {v3 .. v13}, LX/2L0;-><init>(LX/0aG;LX/2L1;LX/0kb;Ljava/util/concurrent/ExecutorService;LX/0SG;LX/0tX;LX/1dx;Ljava/util/Set;LX/1dw;LX/0SI;)V

    .line 394413
    move-object v0, v3

    .line 394414
    sput-object v0, LX/2L0;->n:LX/2L0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394415
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394416
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394417
    :cond_1
    sget-object v0, LX/2L0;->n:LX/2L0;

    return-object v0

    .line 394418
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394419
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2L0;LX/3G3;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 394424
    iget-object v0, p0, LX/2L0;->c:LX/2L1;

    .line 394425
    iget-object v1, p1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2L1;->a(Ljava/lang/String;)V

    .line 394426
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 394427
    iget-object v0, p0, LX/2L0;->i:LX/1dx;

    invoke-virtual {v0, p2, p1}, LX/1dx;->a(Ljava/lang/String;LX/3G3;)V

    .line 394428
    :cond_0
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 394420
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2L0;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2LB;

    .line 394421
    invoke-interface {v0, p1}, LX/2LB;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 394422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 394423
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private static b(LX/2L0;LX/3G3;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 14
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3G3;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 394312
    instance-of v0, p1, LX/4gw;

    if-eqz v0, :cond_1

    .line 394313
    check-cast p1, LX/4gw;

    .line 394314
    iget-object v4, p0, LX/2L0;->k:LX/1dw;

    iget-object v3, p0, LX/2L0;->b:LX/0aG;

    .line 394315
    iget-object v5, p1, LX/4gw;->h:Ljava/lang/String;

    iget-object v6, p1, LX/4gw;->i:Landroid/os/Bundle;

    const v7, 0x6cc3ed6

    invoke-static {v3, v5, v6, v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v5

    move-object v5, v5

    .line 394316
    iget-object v6, p1, LX/3G3;->b:Ljava/lang/String;

    iget-wide v7, p1, LX/3G3;->d:J

    iget-wide v9, p1, LX/3G3;->e:J

    iget v11, p1, LX/3G3;->f:I

    iget v12, p1, LX/3G3;->g:I

    sget-object v13, LX/4Vg;->NEVER_FINISH:LX/4Vg;

    invoke-virtual/range {v4 .. v13}, LX/1dw;->a(LX/1MF;Ljava/lang/String;JJIILX/4Vg;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 394317
    iget-object v4, p0, LX/2L0;->i:LX/1dx;

    invoke-virtual {v4, p1}, LX/1dx;->b(LX/3G3;)V

    .line 394318
    iget-object v4, p1, LX/4gw;->j:LX/3G4;

    move-object v4, v4

    .line 394319
    if-eqz v4, :cond_0

    .line 394320
    :try_start_0
    iget-object v4, p1, LX/4gw;->j:LX/3G4;

    move-object v4, v4

    .line 394321
    invoke-virtual {v4}, LX/3G4;->d()LX/399;

    move-result-object v4

    .line 394322
    new-instance v5, LX/4Vh;

    invoke-direct {v5, p0, v4}, LX/4Vh;-><init>(LX/2L0;LX/399;)V

    .line 394323
    iget-object v4, p0, LX/2L0;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v5, v4}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 394324
    :cond_0
    :goto_0
    move-object v0, v3

    .line 394325
    :goto_1
    return-object v0

    .line 394326
    :cond_1
    instance-of v0, p1, LX/3G4;

    if-eqz v0, :cond_4

    .line 394327
    check-cast p1, LX/3G4;

    .line 394328
    iget-object v0, p0, LX/2L0;->h:LX/0tX;

    sget-object v1, LX/3Fz;->c:LX/3Fz;

    invoke-virtual {v0, p1, v1}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 394329
    iget-object v0, p0, LX/2L0;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Pa;

    .line 394330
    invoke-interface {v0, p1}, LX/2Pa;->a(LX/3G4;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 394331
    invoke-interface {v0, p1}, LX/2Pa;->b(LX/3G4;)LX/0TF;

    move-result-object v3

    .line 394332
    invoke-interface {v0}, LX/2Pa;->a()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-static {v1, v3, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_2

    .line 394333
    :cond_3
    iget-object v0, p0, LX/2L0;->i:LX/1dx;

    invoke-virtual {v0, p1}, LX/1dx;->b(LX/3G3;)V

    .line 394334
    move-object v0, v1

    .line 394335
    goto :goto_1

    .line 394336
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Don\'t know how to submit a PendingRequest of type"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    goto :goto_0
.end method

.method public static b(LX/2L0;LX/2LJ;LX/0Px;)V
    .locals 13
    .param p1    # LX/2LJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2LJ;",
            "LX/0Px",
            "<",
            "LX/3G3;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 394371
    iget-object v0, p0, LX/2L0;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 394372
    if-nez p2, :cond_3

    .line 394373
    invoke-virtual {p0}, LX/2L0;->a()LX/0Px;

    move-result-object v2

    .line 394374
    :goto_0
    iget-object v0, p0, LX/2L0;->d:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 394375
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v4

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3G3;

    .line 394376
    const/4 v5, 0x0

    invoke-static {p0, v0, v5}, LX/2L0;->a(LX/2L0;LX/3G3;Ljava/lang/String;)V

    .line 394377
    invoke-static {p0, v0}, LX/2L0;->b(LX/2L0;LX/3G3;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 394378
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 394379
    :cond_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 394380
    :goto_2
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 394381
    iget-object v1, p0, LX/2L0;->i:LX/1dx;

    move-object v5, p1

    .line 394382
    new-instance v8, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v9, "offline_mode_queue_processing_finished"

    invoke-direct {v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v9, "offline"

    .line 394383
    iput-object v9, v8, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 394384
    move-object v8, v8

    .line 394385
    const-string v9, "time_taken_ms"

    iget-object v10, v1, LX/1dx;->a:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    sub-long/2addr v10, v6

    invoke-virtual {v8, v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "requests_submitted"

    invoke-virtual {v8, v9, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "requests_dropped"

    iget v10, v1, LX/1dx;->d:I

    invoke-virtual {v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "requests_still_pending"

    invoke-virtual {v8, v9, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "requests_total"

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "trigger"

    invoke-virtual {v5}, LX/2LJ;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    .line 394386
    iget-object v9, v1, LX/1dx;->b:LX/0Zb;

    invoke-interface {v9, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 394387
    const/4 v8, 0x0

    iput v8, v1, LX/1dx;->d:I

    .line 394388
    :cond_1
    return-void

    .line 394389
    :cond_2
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    move v3, v4

    move v4, v0

    goto :goto_2

    :cond_3
    move-object v2, p2

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/3G3;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 394350
    iget-object v1, p0, LX/2L0;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    .line 394351
    iget-object v1, p0, LX/2L0;->c:LX/2L1;

    invoke-virtual {v1}, LX/2L1;->a()LX/0Px;

    move-result-object v3

    .line 394352
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 394353
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v7, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3G3;

    .line 394354
    invoke-virtual {v0, v4, v5}, LX/3G3;->a(J)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 394355
    const-string v8, "offline_mode_operation_expired"

    invoke-static {p0, v0, v8}, LX/2L0;->a(LX/2L0;LX/3G3;Ljava/lang/String;)V

    .line 394356
    iget-object v0, v0, LX/3G3;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/2L0;->a(Ljava/lang/String;)V

    .line 394357
    add-int/lit8 v0, v1, 0x1

    .line 394358
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 394359
    :cond_0
    const/4 v8, 0x0

    .line 394360
    iget v9, v0, LX/3G3;->g:I

    if-gtz v9, :cond_5

    .line 394361
    :cond_1
    :goto_2
    move v8, v8

    .line 394362
    if-eqz v8, :cond_2

    .line 394363
    const-string v8, "offline_mode_operation_retry_limit_reached"

    invoke-static {p0, v0, v8}, LX/2L0;->a(LX/2L0;LX/3G3;Ljava/lang/String;)V

    .line 394364
    iget-object v0, v0, LX/3G3;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/2L0;->a(Ljava/lang/String;)V

    .line 394365
    add-int/lit8 v0, v1, 0x1

    goto :goto_1

    .line 394366
    :cond_2
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    goto :goto_1

    .line 394367
    :cond_3
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 394368
    iget-object v0, p0, LX/2L0;->i:LX/1dx;

    .line 394369
    iput v1, v0, LX/1dx;->d:I

    .line 394370
    :cond_4
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_5
    iget v9, v0, LX/3G3;->f:I

    iget v10, v0, LX/3G3;->g:I

    if-lt v9, v10, :cond_1

    const/4 v8, 0x1

    goto :goto_2
.end method

.method public final declared-synchronized a(LX/2LB;)V
    .locals 1

    .prologue
    .line 394347
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2L0;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394348
    monitor-exit p0

    return-void

    .line 394349
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/2LJ;)V
    .locals 1

    .prologue
    .line 394345
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/2L0;->a(LX/2LJ;LX/0Px;)V

    .line 394346
    return-void
.end method

.method public final a(LX/2LJ;LX/0Px;)V
    .locals 3
    .param p2    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2LJ;",
            "LX/0Px",
            "<",
            "LX/3G3;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 394342
    iget-object v0, p0, LX/2L0;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394343
    iget-object v0, p0, LX/2L0;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/graphql/executor/offlinemutations/OfflineObliviousOperationsExecutor$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/graphql/executor/offlinemutations/OfflineObliviousOperationsExecutor$1;-><init>(LX/2L0;LX/2LJ;LX/0Px;)V

    const v2, 0x5a018483

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 394344
    :cond_0
    return-void
.end method

.method public final a(LX/3G3;)V
    .locals 4

    .prologue
    .line 394337
    iget-object v0, p0, LX/2L0;->c:LX/2L1;

    invoke-virtual {v0, p1}, LX/2L1;->a(LX/3G3;)LX/0Px;

    move-result-object v2

    .line 394338
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 394339
    invoke-direct {p0, v0}, LX/2L0;->a(Ljava/lang/String;)V

    .line 394340
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 394341
    :cond_0
    return-void
.end method
