.class public LX/3D1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3D1;


# instance fields
.field private final a:LX/3D2;

.field public b:LX/5OM;


# direct methods
.method public constructor <init>(LX/3D2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 531310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 531311
    iput-object p1, p0, LX/3D1;->a:LX/3D2;

    .line 531312
    return-void
.end method

.method public static a(LX/0QB;)LX/3D1;
    .locals 4

    .prologue
    .line 531313
    sget-object v0, LX/3D1;->c:LX/3D1;

    if-nez v0, :cond_1

    .line 531314
    const-class v1, LX/3D1;

    monitor-enter v1

    .line 531315
    :try_start_0
    sget-object v0, LX/3D1;->c:LX/3D1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 531316
    if-eqz v2, :cond_0

    .line 531317
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 531318
    new-instance p0, LX/3D1;

    invoke-static {v0}, LX/3D2;->a(LX/0QB;)LX/3D2;

    move-result-object v3

    check-cast v3, LX/3D2;

    invoke-direct {p0, v3}, LX/3D1;-><init>(LX/3D2;)V

    .line 531319
    move-object v0, p0

    .line 531320
    sput-object v0, LX/3D1;->c:LX/3D1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 531321
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 531322
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 531323
    :cond_1
    sget-object v0, LX/3D1;->c:LX/3D1;

    return-object v0

    .line 531324
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 531325
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 531326
    iget-object v0, p0, LX/3D1;->b:LX/5OM;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3D1;->b:LX/5OM;

    .line 531327
    iget-boolean v1, v0, LX/0ht;->r:Z

    move v0, v1

    .line 531328
    if-eqz v0, :cond_0

    .line 531329
    iget-object v0, p0, LX/3D1;->b:LX/5OM;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 531330
    :cond_0
    return-void
.end method

.method public final a(LX/2nq;Landroid/content/Context;Landroid/view/View;LX/3D3;I)V
    .locals 7

    .prologue
    .line 531331
    iget-object v0, p0, LX/3D1;->a:LX/3D2;

    invoke-virtual {v0, p1}, LX/3D2;->a(LX/2nq;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 531332
    :goto_0
    return-void

    .line 531333
    :cond_0
    iget-object v0, p0, LX/3D1;->a:LX/3D2;

    .line 531334
    new-instance v1, LX/5OM;

    invoke-direct {v1, p2}, LX/5OM;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/3D1;->b:LX/5OM;

    .line 531335
    iget-object v1, p0, LX/3D1;->b:LX/5OM;

    new-instance v2, LX/Dq4;

    invoke-direct {v2, p0}, LX/Dq4;-><init>(LX/3D1;)V

    .line 531336
    iput-object v2, v1, LX/0ht;->H:LX/2dD;

    .line 531337
    iget-object v1, p0, LX/3D1;->b:LX/5OM;

    move-object v1, v1

    .line 531338
    const-string v5, "long_press"

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, LX/3D2;->a(LX/5OM;LX/2nq;Landroid/view/View;LX/3D3;Ljava/lang/String;I)Z

    goto :goto_0
.end method
