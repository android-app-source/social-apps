.class public LX/25m;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/25m;


# instance fields
.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/25k;",
            ">;"
        }
    .end annotation
.end field

.field private final c:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 369945
    new-instance v0, LX/25n;

    invoke-direct {v0, v1, v1}, LX/25n;-><init>(Landroid/util/SparseArray;[B)V

    sput-object v0, LX/25m;->a:LX/25m;

    return-void
.end method

.method public constructor <init>(Landroid/util/SparseArray;[B)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "LX/25k;",
            ">;[B)V"
        }
    .end annotation

    .prologue
    .line 369946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369947
    iput-object p1, p0, LX/25m;->b:Landroid/util/SparseArray;

    .line 369948
    iput-object p2, p0, LX/25m;->c:[B

    .line 369949
    return-void
.end method


# virtual methods
.method public a(ILX/0XJ;)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 369950
    iget-object v0, p0, LX/25m;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/25k;

    .line 369951
    if-nez v0, :cond_0

    .line 369952
    const/4 v0, 0x0

    .line 369953
    :goto_0
    return-object v0

    .line 369954
    :cond_0
    iget-object v1, v0, LX/25k;->b:Landroid/util/SparseIntArray;

    invoke-virtual {p2}, LX/0XJ;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v1

    if-gez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 369955
    if-eqz v1, :cond_1

    .line 369956
    sget-object p2, LX/0XJ;->UNKNOWN:LX/0XJ;

    .line 369957
    :cond_1
    const/4 v1, 0x0

    .line 369958
    move v2, v1

    .line 369959
    :goto_2
    invoke-virtual {p2}, LX/0XJ;->ordinal()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 369960
    iget-object v3, v0, LX/25k;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 369961
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 369962
    :cond_2
    move v1, v2

    .line 369963
    iget-object v2, p0, LX/25m;->c:[B

    iget v3, v0, LX/25k;->a:I

    add-int/2addr v1, v3

    iget-object v0, v0, LX/25k;->b:Landroid/util/SparseIntArray;

    invoke-virtual {p2}, LX/0XJ;->ordinal()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    invoke-static {v2, v1, v0}, LX/25l;->a([BII)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
