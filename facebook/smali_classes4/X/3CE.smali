.class public LX/3CE;
.super LX/18f;
.source ""

# interfaces
.implements LX/0Xv;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/18f",
        "<TK;TV;>;",
        "LX/0Xv",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "Not needed in emulated source"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0P1;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<TK;",
            "LX/0Px",
            "<TV;>;>;I)V"
        }
    .end annotation

    .prologue
    .line 530023
    invoke-direct {p0, p1, p2}, LX/18f;-><init>(LX/0P1;I)V

    .line 530024
    return-void
.end method

.method public static b(LX/0Xu;)LX/3CE;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Xu",
            "<+TK;+TV;>;)",
            "LX/3CE",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 530008
    invoke-interface {p0}, LX/0Xu;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 530009
    sget-object v0, LX/4xT;->a:LX/4xT;

    move-object v0, v0

    .line 530010
    :cond_0
    :goto_0
    return-object v0

    .line 530011
    :cond_1
    instance-of v0, p0, LX/3CE;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 530012
    check-cast v0, LX/3CE;

    .line 530013
    invoke-virtual {v0}, LX/18f;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 530014
    :cond_2
    new-instance v3, LX/0P2;

    invoke-interface {p0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v3, v0}, LX/0P2;-><init>(I)V

    .line 530015
    const/4 v0, 0x0

    .line 530016
    invoke-interface {p0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 530017
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 530018
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 530019
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 530020
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    add-int/2addr v0, v2

    :goto_2
    move v2, v0

    .line 530021
    goto :goto_1

    .line 530022
    :cond_3
    new-instance v0, LX/3CE;

    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-direct {v0, v1, v2}, LX/3CE;-><init>(LX/0P1;I)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 10
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 529985
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 529986
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v4

    .line 529987
    if-gez v4, :cond_0

    .line 529988
    new-instance v0, Ljava/io/InvalidObjectException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid key count "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 529989
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v5

    move v2, v1

    move v3, v1

    .line 529990
    :goto_0
    if-ge v2, v4, :cond_3

    .line 529991
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v6

    .line 529992
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v7

    .line 529993
    if-gtz v7, :cond_1

    .line 529994
    new-instance v0, Ljava/io/InvalidObjectException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid value count "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 529995
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    move v0, v1

    .line 529996
    :goto_1
    if-ge v0, v7, :cond_2

    .line 529997
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 529998
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 529999
    :cond_2
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 530000
    add-int/2addr v3, v7

    .line 530001
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 530002
    :cond_3
    :try_start_0
    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 530003
    sget-object v1, LX/4yN;->a:LX/50W;

    invoke-virtual {v1, p0, v0}, LX/50W;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 530004
    sget-object v0, LX/4yN;->b:LX/50W;

    invoke-virtual {v0, p0, v3}, LX/50W;->a(Ljava/lang/Object;I)V

    .line 530005
    return-void

    .line 530006
    :catch_0
    move-exception v0

    .line 530007
    new-instance v1, Ljava/io/InvalidObjectException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/io/InvalidObjectException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/InvalidObjectException;

    throw v0
.end method

.method private static t()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TV;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 529984
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 529981
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 529982
    invoke-static {p0, p1}, LX/50X;->a(LX/0Xu;Ljava/io/ObjectOutputStream;)V

    .line 529983
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 530025
    invoke-virtual {p0, p1}, LX/3CE;->e(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 529980
    invoke-static {}, LX/3CE;->t()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 529979
    invoke-virtual {p0, p1}, LX/3CE;->e(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 529978
    invoke-static {}, LX/3CE;->t()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/Object;)LX/0Px;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LX/0Px",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 529974
    iget-object v0, p0, LX/18f;->b:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 529975
    if-nez v0, :cond_0

    .line 529976
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 529977
    :cond_0
    return-object v0
.end method

.method public final synthetic h(Ljava/lang/Object;)LX/0Py;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 529972
    invoke-virtual {p0, p1}, LX/3CE;->e(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i(Ljava/lang/Object;)LX/0Py;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 529973
    invoke-static {}, LX/3CE;->t()LX/0Px;

    move-result-object v0

    return-object v0
.end method
