.class public LX/23t;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/23t",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365549
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 365550
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/23t;->b:LX/0Zi;

    .line 365551
    iput-object p1, p0, LX/23t;->a:LX/0Ot;

    .line 365552
    return-void
.end method

.method public static a(LX/0QB;)LX/23t;
    .locals 4

    .prologue
    .line 365510
    const-class v1, LX/23t;

    monitor-enter v1

    .line 365511
    :try_start_0
    sget-object v0, LX/23t;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 365512
    sput-object v2, LX/23t;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 365513
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365514
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 365515
    new-instance v3, LX/23t;

    const/16 p0, 0x7f3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/23t;-><init>(LX/0Ot;)V

    .line 365516
    move-object v0, v3

    .line 365517
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 365518
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/23t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365519
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 365520
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 12

    .prologue
    .line 365521
    check-cast p2, LX/23y;

    .line 365522
    iget-object v0, p0, LX/23t;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;

    iget-object v1, p2, LX/23y;->e:LX/Byw;

    iget-object v2, p2, LX/23y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 365523
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    const v4, 0x7f0d01ce

    invoke-static {v3, v4}, LX/5JR;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    .line 365524
    iget-object v4, v1, LX/Byw;->d:LX/1aZ;

    iget-object v5, v1, LX/Byw;->c:LX/1bf;

    invoke-static {v3, v4, v5}, LX/26D;->a(Landroid/view/View;LX/1aZ;LX/1bf;)LX/9hN;

    move-result-object v5

    .line 365525
    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->e:LX/26D;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v7, v1, LX/Byw;->b:I

    iget-object v8, v1, LX/Byw;->a:LX/26M;

    iget-object v9, v1, LX/Byw;->c:LX/1bf;

    sget-object v10, LX/74S;->NEWSFEED:LX/74S;

    const/4 v11, 0x1

    move-object v6, v2

    invoke-virtual/range {v3 .. v11}, LX/26D;->a(Landroid/content/Context;LX/9hN;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/26M;LX/1bf;LX/74S;Z)V

    .line 365526
    return-void
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 365527
    invoke-static {}, LX/1dS;->b()V

    .line 365528
    iget v0, p1, LX/1dQ;->b:I

    .line 365529
    packed-switch v0, :pswitch_data_0

    .line 365530
    :goto_0
    return-object v2

    .line 365531
    :pswitch_0
    check-cast p2, LX/8ww;

    .line 365532
    iget-object v0, p2, LX/8ww;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/23t;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x4a6c9e2b
        :pswitch_0
    .end packed-switch
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 365533
    check-cast p4, LX/23y;

    .line 365534
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 365535
    iget-object v0, p0, LX/23t;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;

    iget-object v3, p4, LX/23y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p4, LX/23y;->b:LX/1Pe;

    iget-object v5, p4, LX/23y;->c:Landroid/graphics/Rect;

    iget-boolean v6, p4, LX/23y;->d:Z

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentSpec;->a(LX/1De;ILcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;Landroid/graphics/Rect;ZLX/1np;)LX/1Dg;

    move-result-object v1

    .line 365536
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 365537
    check-cast v0, LX/Byw;

    iput-object v0, p4, LX/23y;->e:LX/Byw;

    .line 365538
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 365539
    return-object v1
.end method

.method public final c(LX/1De;)LX/240;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/23t",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 365540
    new-instance v1, LX/23y;

    invoke-direct {v1, p0}, LX/23y;-><init>(LX/23t;)V

    .line 365541
    iget-object v2, p0, LX/23t;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/240;

    .line 365542
    if-nez v2, :cond_0

    .line 365543
    new-instance v2, LX/240;

    invoke-direct {v2, p0}, LX/240;-><init>(LX/23t;)V

    .line 365544
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/240;->a$redex0(LX/240;LX/1De;IILX/23y;)V

    .line 365545
    move-object v1, v2

    .line 365546
    move-object v0, v1

    .line 365547
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 365548
    const/4 v0, 0x1

    return v0
.end method
