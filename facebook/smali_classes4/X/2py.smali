.class public final LX/2py;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16Y;
.implements LX/2pz;
.implements LX/2q0;
.implements LX/2q1;
.implements LX/2q2;
.implements LX/09B;
.implements LX/09C;
.implements LX/09D;
.implements LX/09E;


# instance fields
.field public final synthetic a:LX/2px;

.field public b:LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/11o",
            "<",
            "LX/09X;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field private d:Z

.field public e:I

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/server/VideoServer$RequestLiveInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:J


# direct methods
.method public constructor <init>(LX/2px;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 469265
    iput-object p1, p0, LX/2py;->a:LX/2px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 469266
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2py;->c:Z

    .line 469267
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2py;->d:Z

    .line 469268
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2py;->f:Ljava/util/Set;

    .line 469269
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/2py;->g:Ljava/util/List;

    .line 469270
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/2py;->h:Ljava/util/List;

    .line 469271
    return-void
.end method

.method public static a(JJ)LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 469264
    const-string v0, "from"

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "to"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/2py;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 469262
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/2py;->a(LX/2py;Ljava/lang/String;LX/0P1;)V

    .line 469263
    return-void
.end method

.method public static declared-synchronized a(LX/2py;Ljava/lang/String;LX/0P1;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 469257
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2py;->b:LX/11o;

    if-eqz v0, :cond_0

    .line 469258
    iget-object v0, p0, LX/2py;->b:LX/11o;

    const/4 v1, 0x0

    const v2, -0x4fed9834

    invoke-static {v0, p1, v1, p2, v2}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 469259
    iget-object v0, p0, LX/2py;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469260
    :cond_0
    monitor-exit p0

    return-void

    .line 469261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 469245
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/2py;->b:LX/11o;

    if-eqz v1, :cond_1

    .line 469246
    iget-object v1, p0, LX/2py;->f:Ljava/util/Set;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 469247
    invoke-static {p0, v0}, LX/2py;->b(LX/2py;Ljava/lang/String;)V

    .line 469248
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 469249
    :cond_0
    invoke-direct {p0, p1}, LX/2py;->b(Z)LX/0P1;

    move-result-object v0

    .line 469250
    iget-object v1, p0, LX/2py;->a:LX/2px;

    iget-object v1, v1, LX/2px;->a:LX/11i;

    sget-object v2, LX/09X;->a:LX/09X;

    invoke-interface {v1, v2, v0}, LX/11i;->b(LX/0Pq;LX/0P1;)V

    .line 469251
    const/4 v0, 0x0

    iput-object v0, p0, LX/2py;->b:LX/11o;

    .line 469252
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2py;->d:Z

    .line 469253
    iget-object v0, p0, LX/2py;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 469254
    iget-object v0, p0, LX/2py;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469255
    :cond_1
    monitor-exit p0

    return-void

    .line 469256
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Z)LX/0P1;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 469223
    const-wide/16 v12, 0x0

    .line 469224
    const-wide/16 v10, 0x0

    .line 469225
    const/4 v9, 0x0

    .line 469226
    const/4 v8, 0x0

    .line 469227
    const/4 v7, 0x0

    .line 469228
    const/4 v6, 0x0

    .line 469229
    const-wide/16 v4, 0x0

    .line 469230
    const-wide/16 v2, 0x0

    .line 469231
    move-object/from16 v0, p0

    iget-object v14, v0, LX/2py;->g:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move-wide v14, v12

    move-wide v12, v10

    move v10, v9

    move v9, v8

    move v8, v7

    move/from16 v20, v6

    move-wide v6, v4

    move-wide v4, v2

    move/from16 v3, v20

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7Pl;

    .line 469232
    invoke-virtual {v2}, LX/7Pl;->a()J

    move-result-wide v18

    add-long v14, v14, v18

    .line 469233
    invoke-virtual {v2}, LX/7Pl;->b()J

    move-result-wide v18

    add-long v12, v12, v18

    .line 469234
    invoke-virtual {v2}, LX/7Pl;->c()I

    move-result v11

    add-int/2addr v11, v10

    .line 469235
    invoke-virtual {v2}, LX/7Pl;->d()I

    move-result v10

    add-int/2addr v10, v9

    .line 469236
    invoke-virtual {v2}, LX/7Pl;->e()I

    move-result v9

    add-int/2addr v9, v8

    .line 469237
    invoke-virtual {v2}, LX/7Pl;->f()I

    move-result v8

    add-int/2addr v8, v3

    .line 469238
    invoke-virtual {v2}, LX/7Pl;->g()J

    move-result-wide v18

    add-long v4, v4, v18

    .line 469239
    invoke-virtual {v2}, LX/7Pl;->h()J

    move-result-wide v2

    add-long/2addr v2, v6

    move-wide v6, v2

    move v3, v8

    move v8, v9

    move v9, v10

    move v10, v11

    .line 469240
    goto :goto_0

    .line 469241
    :cond_0
    const-wide/16 v16, -0x1

    .line 469242
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2py;->h:Ljava/util/List;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2py;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 469243
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2py;->h:Ljava/util/List;

    const/4 v11, 0x0

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 469244
    :cond_1
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v11

    const-string v18, "started"

    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/2py;->d:Z

    if-eqz v2, :cond_2

    const-string v2, "1"

    :goto_1
    move-object/from16 v0, v18

    invoke-virtual {v11, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v11, "request_count"

    move-object/from16 v0, p0

    iget-object v0, v0, LX/2py;->g:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v2, v11, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v11, "bytes_served"

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v11, "bytes_downloaded"

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v11, "first_missing_cache_byte"

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v11, "network_count"

    move-object/from16 v0, p0

    iget v12, v0, LX/2py;->e:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v11, "player_version"

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2py;->a:LX/2px;

    iget-object v12, v12, LX/2px;->f:Ljava/lang/String;

    invoke-virtual {v2, v11, v12}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v11, "player_type"

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2py;->a:LX/2px;

    iget-object v12, v12, LX/2px;->g:Ljava/lang/String;

    invoke-virtual {v2, v11, v12}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v11, "video_atom_size"

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2py;->a:LX/2px;

    iget v12, v12, LX/2px;->i:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v11, "video_bitrate"

    move-object/from16 v0, p0

    iget-object v12, v0, LX/2py;->a:LX/2px;

    iget v12, v12, LX/2px;->j:I

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v11, "fetch_bitrate_est"

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/2py;->i:J

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v11, "intercepted"

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v11, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v10, "reused"

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v10, v9}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v9, "reused_later"

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v9, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v8, "reused_past"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v3, "reused_distance"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v3, "reused_later_distance"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v3, "psr_cancelled"

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v4, "streaming_format"

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2py;->a:LX/2px;

    iget-object v2, v2, LX/2px;->k:Ljava/lang/String;

    if-nez v2, :cond_3

    const-string v2, ""

    :goto_2
    invoke-virtual {v3, v4, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v4, "video_id"

    move-object/from16 v0, p0

    iget-object v2, v0, LX/2py;->a:LX/2px;

    iget-object v2, v2, LX/2px;->h:Ljava/lang/String;

    if-nez v2, :cond_4

    const-string v2, ""

    :goto_3
    invoke-virtual {v3, v4, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    return-object v2

    :cond_2
    const-string v2, "0"

    goto/16 :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2py;->a:LX/2px;

    iget-object v2, v2, LX/2px;->k:Ljava/lang/String;

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2py;->a:LX/2px;

    iget-object v2, v2, LX/2px;->h:Ljava/lang/String;

    goto :goto_3
.end method

.method public static b(LX/2py;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 469221
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/2py;->b(Ljava/lang/String;LX/0P1;)V

    .line 469222
    return-void
.end method

.method private declared-synchronized b(Ljava/lang/String;LX/0P1;)V
    .locals 3
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 469216
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2py;->b:LX/11o;

    if-eqz v0, :cond_0

    .line 469217
    iget-object v0, p0, LX/2py;->b:LX/11o;

    const/4 v1, 0x0

    const v2, -0x59a0682

    invoke-static {v0, p1, v1, p2, v2}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 469218
    iget-object v0, p0, LX/2py;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469219
    :cond_0
    monitor-exit p0

    return-void

    .line 469220
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 469211
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2py;->b:LX/11o;

    if-eqz v0, :cond_0

    .line 469212
    iget-object v0, p0, LX/2py;->b:LX/11o;

    const/4 v1, 0x0

    const v2, -0x19f5a67d

    invoke-static {v0, p1, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;I)LX/11o;

    .line 469213
    iget-object v0, p0, LX/2py;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469214
    :cond_0
    monitor-exit p0

    return-void

    .line 469215
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized j()V
    .locals 2

    .prologue
    .line 469171
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2py;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2py;->b:LX/11o;

    if-nez v0, :cond_0

    .line 469172
    iget-object v0, p0, LX/2py;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 469173
    iget-object v0, p0, LX/2py;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 469174
    iget-object v0, p0, LX/2py;->a:LX/2px;

    iget-object v0, v0, LX/2px;->a:LX/11i;

    sget-object v1, LX/09X;->a:LX/09X;

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;

    move-result-object v0

    iput-object v0, p0, LX/2py;->b:LX/11o;

    .line 469175
    const/4 v0, 0x0

    iput v0, p0, LX/2py;->e:I

    .line 469176
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2py;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469177
    :cond_0
    monitor-exit p0

    return-void

    .line 469178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 469272
    invoke-virtual {p0}, LX/2py;->i()V

    .line 469273
    return-void
.end method

.method public final a(LX/2qK;)V
    .locals 1

    .prologue
    .line 469179
    invoke-direct {p0}, LX/2py;->j()V

    .line 469180
    const-string v0, "VideoPSR"

    invoke-static {p0, v0}, LX/2py;->a(LX/2py;Ljava/lang/String;)V

    .line 469181
    const-string v0, "VideoStallTime"

    invoke-static {p0, v0}, LX/2py;->a(LX/2py;Ljava/lang/String;)V

    .line 469182
    return-void
.end method

.method public final a(LX/2qL;)V
    .locals 1

    .prologue
    .line 469183
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2py;->d:Z

    .line 469184
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2py;->a(Z)V

    .line 469185
    return-void
.end method

.method public final a(LX/2qN;)V
    .locals 1

    .prologue
    .line 469186
    const-string v0, "Buffering"

    invoke-static {p0, v0}, LX/2py;->a(LX/2py;Ljava/lang/String;)V

    .line 469187
    return-void
.end method

.method public final declared-synchronized a(LX/2qO;)V
    .locals 6

    .prologue
    .line 469188
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2py;->b:LX/11o;

    if-eqz v0, :cond_0

    .line 469189
    const-string v0, "PrepareToDataRequest"

    invoke-static {p0, v0}, LX/2py;->b(LX/2py;Ljava/lang/String;)V

    .line 469190
    const-string v0, "RequestLock"

    invoke-static {p0, v0}, LX/2py;->a(LX/2py;Ljava/lang/String;)V

    .line 469191
    iget-object v0, p0, LX/2py;->g:Ljava/util/List;

    iget-object v1, p1, LX/2qO;->a:LX/7Pl;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 469192
    iget-object v0, p0, LX/2py;->h:Ljava/util/List;

    iget-wide v2, p1, LX/2qO;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 469193
    sget-wide v4, LX/1Lv;->c:J

    move-wide v0, v4

    .line 469194
    iput-wide v0, p0, LX/2py;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469195
    :cond_0
    monitor-exit p0

    return-void

    .line 469196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 469197
    const-string v0, "Allocation"

    invoke-static {p0, v0}, LX/2py;->a(LX/2py;Ljava/lang/String;)V

    .line 469198
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 469199
    const-string v0, "Allocation"

    invoke-static {p0, v0}, LX/2py;->b(LX/2py;Ljava/lang/String;)V

    .line 469200
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 469201
    invoke-direct {p0}, LX/2py;->j()V

    .line 469202
    const-string v0, "Preparation"

    invoke-static {p0, v0}, LX/2py;->a(LX/2py;Ljava/lang/String;)V

    .line 469203
    const-string v0, "PrepareToDataRequest"

    invoke-static {p0, v0}, LX/2py;->a(LX/2py;Ljava/lang/String;)V

    .line 469204
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 469205
    const-string v0, "Preparation"

    invoke-static {p0, v0}, LX/2py;->b(LX/2py;Ljava/lang/String;)V

    .line 469206
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 469207
    const-string v0, "VideoPSR"

    invoke-direct {p0, v0}, LX/2py;->c(Ljava/lang/String;)V

    .line 469208
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/2py;->a(Z)V

    .line 469209
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2py;->c:Z

    .line 469210
    return-void
.end method
