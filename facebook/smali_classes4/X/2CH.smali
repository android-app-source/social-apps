.class public LX/2CH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements LX/2C2;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile R:LX/2CH;

.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final A:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public final B:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public final C:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public final D:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public volatile E:Z

.field private volatile F:Z

.field public final G:LX/2AK;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field private H:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private I:Z

.field public J:LX/2AL;

.field public K:LX/2AL;

.field public L:LX/3Ol;

.field public M:LX/2AZ;

.field private N:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public O:Ljava/util/concurrent/ScheduledFuture;

.field public P:J

.field public Q:J

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2gU;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1MZ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2RB;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final i:LX/0Xl;

.field private final j:LX/0dN;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/03V;

.field public final n:LX/13Q;

.field public final o:LX/1BA;

.field private final p:LX/0Zb;

.field private final q:LX/2AY;

.field public final r:LX/2AW;

.field public final s:Ljava/util/concurrent/ScheduledExecutorService;

.field public final t:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0Yb;

.field private final v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2a7;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/9l7;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field

.field public final x:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "LX/3Mg;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final y:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/3hU;",
            ">;"
        }
    .end annotation
.end field

.field public final z:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 382389
    const-class v0, LX/2CH;

    sput-object v0, LX/2CH;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;LX/0Ot;LX/0Or;LX/03V;LX/13Q;LX/1BA;LX/0Zb;LX/2AW;LX/2AY;LX/2AZ;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;LX/0Ot;)V
    .locals 7
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p8    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/push/prefs/IsMobileOnlineAvailabilityEnabled;
        .end annotation
    .end param
    .param p19    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2gU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1MZ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2RB;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/13Q;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "LX/0Zb;",
            "LX/2AW;",
            "LX/2AY;",
            "LX/2AZ;",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Ot",
            "<",
            "LX/2a7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 382390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382391
    new-instance v2, Lcom/facebook/presence/DefaultPresenceManager$1;

    invoke-direct {v2, p0}, Lcom/facebook/presence/DefaultPresenceManager$1;-><init>(LX/2CH;)V

    iput-object v2, p0, LX/2CH;->z:Ljava/lang/Runnable;

    .line 382392
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, LX/2CH;->H:Ljava/util/Set;

    .line 382393
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/2CH;->P:J

    .line 382394
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/2CH;->Q:J

    .line 382395
    iput-object p1, p0, LX/2CH;->b:LX/0Ot;

    .line 382396
    iput-object p2, p0, LX/2CH;->c:LX/0Ot;

    .line 382397
    iput-object p3, p0, LX/2CH;->d:LX/0Or;

    .line 382398
    iput-object p4, p0, LX/2CH;->e:LX/0Ot;

    .line 382399
    iput-object p5, p0, LX/2CH;->f:LX/0Ot;

    .line 382400
    iput-object p6, p0, LX/2CH;->g:LX/0Ot;

    .line 382401
    iput-object p7, p0, LX/2CH;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 382402
    iput-object p8, p0, LX/2CH;->i:LX/0Xl;

    .line 382403
    move-object/from16 v0, p9

    iput-object v0, p0, LX/2CH;->k:LX/0Ot;

    .line 382404
    move-object/from16 v0, p10

    iput-object v0, p0, LX/2CH;->l:LX/0Or;

    .line 382405
    move-object/from16 v0, p11

    iput-object v0, p0, LX/2CH;->m:LX/03V;

    .line 382406
    move-object/from16 v0, p12

    iput-object v0, p0, LX/2CH;->n:LX/13Q;

    .line 382407
    move-object/from16 v0, p13

    iput-object v0, p0, LX/2CH;->o:LX/1BA;

    .line 382408
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2CH;->r:LX/2AW;

    .line 382409
    move-object/from16 v0, p14

    iput-object v0, p0, LX/2CH;->p:LX/0Zb;

    .line 382410
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2CH;->q:LX/2AY;

    .line 382411
    move-object/from16 v0, p17

    iput-object v0, p0, LX/2CH;->M:LX/2AZ;

    .line 382412
    move-object/from16 v0, p19

    iput-object v0, p0, LX/2CH;->s:Ljava/util/concurrent/ScheduledExecutorService;

    .line 382413
    move-object/from16 v0, p18

    iput-object v0, p0, LX/2CH;->t:LX/0Or;

    .line 382414
    move-object/from16 v0, p20

    iput-object v0, p0, LX/2CH;->v:LX/0Ot;

    .line 382415
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v2

    iput-object v2, p0, LX/2CH;->w:LX/0Xu;

    .line 382416
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    iput-object v2, p0, LX/2CH;->x:Ljava/util/concurrent/ConcurrentMap;

    .line 382417
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    iput-object v2, p0, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    .line 382418
    new-instance v2, LX/2AK;

    invoke-direct {v2}, LX/2AK;-><init>()V

    iput-object v2, p0, LX/2CH;->G:LX/2AK;

    .line 382419
    new-instance v3, LX/0aq;

    iget-object v2, p0, LX/2CH;->t:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0W3;

    sget-wide v4, LX/0X5;->fY:J

    const/16 v6, 0xa

    invoke-interface {v2, v4, v5, v6}, LX/0W4;->a(JI)I

    move-result v2

    invoke-direct {v3, v2}, LX/0aq;-><init>(I)V

    iput-object v3, p0, LX/2CH;->A:LX/0aq;

    .line 382420
    new-instance v3, LX/0aq;

    iget-object v2, p0, LX/2CH;->t:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0W3;

    sget-wide v4, LX/0X5;->fZ:J

    const/16 v6, 0xa

    invoke-interface {v2, v4, v5, v6}, LX/0W4;->a(JI)I

    move-result v2

    invoke-direct {v3, v2}, LX/0aq;-><init>(I)V

    iput-object v3, p0, LX/2CH;->B:LX/0aq;

    .line 382421
    new-instance v2, LX/0UE;

    invoke-direct {v2}, LX/0UE;-><init>()V

    iput-object v2, p0, LX/2CH;->C:LX/0UE;

    .line 382422
    new-instance v2, LX/0UE;

    invoke-direct {v2}, LX/0UE;-><init>()V

    iput-object v2, p0, LX/2CH;->D:LX/0UE;

    .line 382423
    new-instance v2, LX/0UE;

    invoke-direct {v2}, LX/0UE;-><init>()V

    iput-object v2, p0, LX/2CH;->N:LX/0UE;

    .line 382424
    sget-object v2, LX/2AL;->TP_DISABLED:LX/2AL;

    invoke-static {p0, v2}, LX/2CH;->a$redex0(LX/2CH;LX/2AL;)V

    .line 382425
    iget-object v2, p0, LX/2CH;->i:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.orca.contacts.CONTACTS_UPLOAD_STATE_CHANGED"

    new-instance v4, LX/2AM;

    invoke-direct {v4, p0}, LX/2AM;-><init>(LX/2CH;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.contacts.ACTION_CONTACT_SYNC_PROGRESS"

    new-instance v4, LX/2CI;

    invoke-direct {v4, p0}, LX/2CI;-><init>(LX/2CH;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.contacts.ACTION_CONTACT_ADDED"

    new-instance v4, LX/2AN;

    invoke-direct {v4, p0}, LX/2AN;-><init>(LX/2CH;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    new-instance v4, LX/2CJ;

    invoke-direct {v4, p0}, LX/2CJ;-><init>(LX/2CH;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.presence.ACTION_PUSH_RECEIVED"

    new-instance v4, LX/2AO;

    invoke-direct {v4, p0}, LX/2AO;-><init>(LX/2CH;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.presence.ACTION_OTHER_USER_TYPING_CHANGED"

    new-instance v4, LX/2AP;

    invoke-direct {v4, p0}, LX/2AP;-><init>(LX/2CH;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.presence.ACTION_PRESENCE_RECEIVED"

    new-instance v4, LX/2CK;

    invoke-direct {v4, p0}, LX/2CK;-><init>(LX/2CH;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.presence.ACTION_PUSH_STATE_RECEIVED"

    new-instance v4, LX/2AQ;

    invoke-direct {v4, p0}, LX/2AQ;-><init>(LX/2CH;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    invoke-interface {v2}, LX/0YX;->a()LX/0Yb;

    move-result-object v2

    iput-object v2, p0, LX/2CH;->u:LX/0Yb;

    .line 382426
    new-instance v2, LX/2CL;

    invoke-direct {v2, p0}, LX/2CL;-><init>(LX/2CH;)V

    iput-object v2, p0, LX/2CH;->j:LX/0dN;

    .line 382427
    sget-object v2, LX/2AL;->MQTT_DISCONNECTED:LX/2AL;

    iput-object v2, p0, LX/2CH;->J:LX/2AL;

    .line 382428
    return-void
.end method

.method public static a(LX/0QB;)LX/2CH;
    .locals 3

    .prologue
    .line 382429
    sget-object v0, LX/2CH;->R:LX/2CH;

    if-nez v0, :cond_1

    .line 382430
    const-class v1, LX/2CH;

    monitor-enter v1

    .line 382431
    :try_start_0
    sget-object v0, LX/2CH;->R:LX/2CH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 382432
    if-eqz v2, :cond_0

    .line 382433
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2CH;->b(LX/0QB;)LX/2CH;

    move-result-object v0

    sput-object v0, LX/2CH;->R:LX/2CH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382434
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 382435
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 382436
    :cond_1
    sget-object v0, LX/2CH;->R:LX/2CH;

    return-object v0

    .line 382437
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 382438
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2CH;JLjava/lang/String;Lcom/facebook/user/model/LastActive;)V
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 382439
    if-nez p4, :cond_1

    .line 382440
    :cond_0
    :goto_0
    return-void

    .line 382441
    :cond_1
    iget-wide v7, p4, Lcom/facebook/user/model/LastActive;->a:J

    move-wide v0, v7

    .line 382442
    sub-long v2, p1, v0

    const-wide/32 v4, 0x2bf20

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 382443
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "presence_stale"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "stale_active_time_ms"

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "new_active_time_ms"

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "other_user_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "source"

    const-string v2, "typing"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "presence_staleness"

    .line 382444
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 382445
    move-object v0, v0

    .line 382446
    iget-object v1, p0, LX/2CH;->p:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 382447
    iget-object v0, p0, LX/2CH;->n:LX/13Q;

    const-string v1, "presence_typing_stale"

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V

    .line 382448
    iget-object v0, p0, LX/2CH;->o:LX/1BA;

    const v1, 0x59000c

    invoke-virtual {v0, v1}, LX/1BA;->a(I)V

    goto :goto_0
.end method

.method public static a(LX/2CH;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 382449
    iget-object v0, p0, LX/2CH;->r:LX/2AW;

    .line 382450
    iget-boolean v1, v0, LX/2AW;->b:Z

    move v0, v1

    .line 382451
    if-nez v0, :cond_0

    iget-object v0, p0, LX/2CH;->r:LX/2AW;

    .line 382452
    iget-boolean v1, v0, LX/2AW;->a:Z

    move v0, v1

    .line 382453
    if-eqz v0, :cond_1

    .line 382454
    :cond_0
    iget-object v0, p0, LX/2CH;->n:LX/13Q;

    invoke-virtual {v0, p1, p3, p4}, LX/13Q;->a(Ljava/lang/String;J)V

    .line 382455
    :goto_0
    return-void

    .line 382456
    :cond_1
    iget-object v0, p0, LX/2CH;->n:LX/13Q;

    invoke-virtual {v0, p2, p3, p4}, LX/13Q;->a(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method private static a(LX/3hU;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 382457
    iput-boolean v0, p0, LX/3hU;->d:Z

    .line 382458
    iput-boolean v0, p0, LX/3hU;->a:Z

    .line 382459
    iput v0, p0, LX/3hU;->f:I

    .line 382460
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/3hU;->e:J

    .line 382461
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3hU;->g:J

    .line 382462
    return-void
.end method

.method public static a$redex0(LX/2CH;LX/2AL;)V
    .locals 1

    .prologue
    .line 382463
    iget-object v0, p0, LX/2CH;->G:LX/2AK;

    iput-object p1, v0, LX/2AK;->e:LX/2AL;

    .line 382464
    iput-object p1, p0, LX/2CH;->K:LX/2AL;

    .line 382465
    return-void
.end method

.method public static a$redex0(LX/2CH;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 382466
    iget-object v0, p0, LX/2CH;->n:LX/13Q;

    const-string v1, "presence_map_reset_on_topic_unsubscribe"

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V

    .line 382467
    iget-object v0, p0, LX/2CH;->o:LX/1BA;

    const v1, 0x59000a

    invoke-virtual {v0, v1}, LX/1BA;->a(I)V

    .line 382468
    const-string v0, "/t_p"

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2CH;->r:LX/2AW;

    .line 382469
    iget-boolean v1, v0, LX/2AW;->b:Z

    move v0, v1

    .line 382470
    if-eqz v0, :cond_0

    .line 382471
    invoke-direct {p0}, LX/2CH;->v()V

    .line 382472
    :goto_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/2CH;->b(LX/2CH;Z)V

    .line 382473
    return-void

    .line 382474
    :cond_0
    invoke-static {p0}, LX/2CH;->w(LX/2CH;)V

    goto :goto_0
.end method

.method public static declared-synchronized a$redex0(LX/2CH;Z)Z
    .locals 4

    .prologue
    .line 382475
    monitor-enter p0

    .line 382476
    :try_start_0
    iget-object v0, p0, LX/2CH;->L:LX/3Ol;

    if-nez v0, :cond_0

    .line 382477
    new-instance v0, LX/3Ol;

    const-string v1, "/t_p"

    const/4 v2, 0x0

    sget-object v3, LX/2C3;->APP_USE:LX/2C3;

    invoke-direct {v0, v1, v2, v3}, LX/3Ol;-><init>(Ljava/lang/String;ILX/2C3;)V

    iput-object v0, p0, LX/2CH;->L:LX/3Ol;

    .line 382478
    iget-object v0, p0, LX/2CH;->M:LX/2AZ;

    iget-object v1, p0, LX/2CH;->L:LX/3Ol;

    .line 382479
    iget-object v2, v0, LX/2AZ;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 382480
    :cond_0
    iget-object v0, p0, LX/2CH;->L:LX/3Ol;

    .line 382481
    iget-boolean v1, v0, LX/3Ol;->c:Z

    move v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382482
    if-ne v0, p1, :cond_1

    .line 382483
    const/4 v0, 0x0

    .line 382484
    :goto_0
    monitor-exit p0

    return v0

    .line 382485
    :cond_1
    if-eqz p1, :cond_2

    .line 382486
    :try_start_1
    iget-object v0, p0, LX/2CH;->L:LX/3Ol;

    .line 382487
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/3Ol;->c:Z

    .line 382488
    :goto_1
    iget-object v0, p0, LX/2CH;->q:LX/2AY;

    invoke-virtual {v0}, LX/2AY;->a()V

    .line 382489
    const/4 v0, 0x1

    goto :goto_0

    .line 382490
    :cond_2
    iget-object v0, p0, LX/2CH;->L:LX/3Ol;

    .line 382491
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/3Ol;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382492
    goto :goto_1

    .line 382493
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(LX/0QB;)LX/2CH;
    .locals 23

    .prologue
    .line 382494
    new-instance v2, LX/2CH;

    const/16 v3, 0x1021

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x100f

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xfa4

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x271

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x140f

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1430

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    const/16 v11, 0x2e3

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x155d

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-static/range {p0 .. p0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v14

    check-cast v14, LX/13Q;

    invoke-static/range {p0 .. p0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v15

    check-cast v15, LX/1BA;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v16

    check-cast v16, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/2AW;->a(LX/0QB;)LX/2AW;

    move-result-object v17

    check-cast v17, LX/2AW;

    invoke-static/range {p0 .. p0}, LX/2AY;->a(LX/0QB;)LX/2AY;

    move-result-object v18

    check-cast v18, LX/2AY;

    invoke-static/range {p0 .. p0}, LX/2AZ;->a(LX/0QB;)LX/2AZ;

    move-result-object v19

    check-cast v19, LX/2AZ;

    const/16 v20, 0xdf4

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v21

    check-cast v21, Ljava/util/concurrent/ScheduledExecutorService;

    const/16 v22, 0xfa7

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    invoke-direct/range {v2 .. v22}, LX/2CH;-><init>(LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;LX/0Ot;LX/0Or;LX/03V;LX/13Q;LX/1BA;LX/0Zb;LX/2AW;LX/2AY;LX/2AZ;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;LX/0Ot;)V

    .line 382495
    return-object v2
.end method

.method public static b(LX/2CH;Z)V
    .locals 4

    .prologue
    .line 382624
    if-eqz p1, :cond_0

    .line 382625
    new-instance v2, LX/0UE;

    iget-object v0, p0, LX/2CH;->w:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->q()LX/1M1;

    move-result-object v0

    invoke-direct {v2, v0}, LX/0UE;-><init>(Ljava/util/Collection;)V

    .line 382626
    const/4 v0, 0x0

    invoke-virtual {v2}, LX/0UE;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 382627
    invoke-virtual {v2, v1}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 382628
    invoke-static {p0, v0}, LX/2CH;->h(LX/2CH;Lcom/facebook/user/model/UserKey;)V

    .line 382629
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 382630
    :cond_0
    iget-object v0, p0, LX/2CH;->x:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Mg;

    .line 382631
    invoke-virtual {v0}, LX/3Mg;->a()V

    goto :goto_1

    .line 382632
    :cond_1
    return-void
.end method

.method public static g(LX/2CH;Lcom/facebook/user/model/UserKey;)LX/3hU;
    .locals 2

    .prologue
    .line 382496
    iget-object v0, p0, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hU;

    .line 382497
    if-nez v0, :cond_0

    .line 382498
    new-instance v1, LX/3hU;

    invoke-direct {v1}, LX/3hU;-><init>()V

    .line 382499
    iput-object p1, v1, LX/3hU;->h:Lcom/facebook/user/model/UserKey;

    .line 382500
    iget-object v0, p0, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hU;

    .line 382501
    if-nez v0, :cond_0

    move-object v0, v1

    .line 382502
    :cond_0
    return-object v0
.end method

.method public static h(LX/2CH;Lcom/facebook/user/model/UserKey;)V
    .locals 4

    .prologue
    .line 382609
    iget-object v0, p0, LX/2CH;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 382610
    iget-object v0, p0, LX/2CH;->w:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->f(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 382611
    invoke-virtual {p0, p1}, LX/2CH;->d(Lcom/facebook/user/model/UserKey;)LX/3Ox;

    move-result-object v2

    .line 382612
    iget-object v0, p0, LX/2CH;->w:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 382613
    const/4 v0, 0x0

    move v1, v0

    .line 382614
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382615
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9l7;

    .line 382616
    invoke-virtual {v0, p1, v2}, LX/9l7;->a(Lcom/facebook/user/model/UserKey;LX/3Ox;)Z

    move-result v0

    .line 382617
    if-nez v0, :cond_2

    .line 382618
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 382619
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 382620
    goto :goto_0

    .line 382621
    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, p0, LX/2CH;->w:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 382622
    iget-object v0, p0, LX/2CH;->w:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->d(Ljava/lang/Object;)Ljava/util/Collection;

    .line 382623
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private j()V
    .locals 11

    .prologue
    .line 382579
    iget-object v0, p0, LX/2CH;->H:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, LX/2CH;->I:Z

    if-eqz v0, :cond_4

    .line 382580
    const-wide/16 v9, -0x1

    .line 382581
    iget-object v1, p0, LX/2CH;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 382582
    iget-wide v1, p0, LX/2CH;->Q:J

    cmp-long v1, v1, v9

    if-eqz v1, :cond_0

    .line 382583
    const-string v2, "android_generic_presence_interval_test"

    const-string v3, "android_generic_presence_interval_control"

    iget-object v1, p0, LX/2CH;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v5

    iget-wide v7, p0, LX/2CH;->Q:J

    sub-long/2addr v5, v7

    invoke-static {p0, v2, v3, v5, v6}, LX/2CH;->a(LX/2CH;Ljava/lang/String;Ljava/lang/String;J)V

    .line 382584
    iput-wide v9, p0, LX/2CH;->Q:J

    .line 382585
    :cond_0
    const-string v1, "android_generic_presence_active_count_test"

    const-string v2, "android_generic_presence_active_count_control"

    invoke-virtual {p0}, LX/2CH;->d()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v3

    int-to-long v3, v3

    invoke-static {p0, v1, v2, v3, v4}, LX/2CH;->a(LX/2CH;Ljava/lang/String;Ljava/lang/String;J)V

    .line 382586
    invoke-static {p0}, LX/2CH;->o(LX/2CH;)V

    .line 382587
    iget-object v1, p0, LX/2CH;->r:LX/2AW;

    .line 382588
    iget-boolean v2, v1, LX/2AW;->b:Z

    move v1, v2

    .line 382589
    if-nez v1, :cond_1

    iget-object v1, p0, LX/2CH;->r:LX/2AW;

    .line 382590
    iget-boolean v2, v1, LX/2AW;->a:Z

    move v1, v2

    .line 382591
    if-eqz v1, :cond_2

    .line 382592
    :cond_1
    iget-object v2, p0, LX/2CH;->s:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v3, p0, LX/2CH;->z:Ljava/lang/Runnable;

    iget-object v1, p0, LX/2CH;->t:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W3;

    sget-wide v5, LX/0X5;->fX:J

    const-wide/16 v7, 0x12c

    invoke-interface {v1, v5, v6, v7, v8}, LX/0W4;->a(JJ)J

    move-result-wide v5

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v5, v6, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, p0, LX/2CH;->O:Ljava/util/concurrent/ScheduledFuture;

    .line 382593
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2CH;->I:Z

    .line 382594
    :cond_3
    :goto_0
    return-void

    .line 382595
    :cond_4
    iget-object v0, p0, LX/2CH;->H:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, LX/2CH;->I:Z

    if-nez v0, :cond_3

    .line 382596
    iget-object v1, p0, LX/2CH;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 382597
    invoke-static {p0}, LX/2CH;->o(LX/2CH;)V

    .line 382598
    iget-object v1, p0, LX/2CH;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    iput-wide v1, p0, LX/2CH;->Q:J

    .line 382599
    iget-object v1, p0, LX/2CH;->r:LX/2AW;

    .line 382600
    iget-boolean v2, v1, LX/2AW;->b:Z

    move v1, v2

    .line 382601
    if-nez v1, :cond_6

    iget-object v1, p0, LX/2CH;->r:LX/2AW;

    .line 382602
    iget-boolean v2, v1, LX/2AW;->a:Z

    move v1, v2

    .line 382603
    if-nez v1, :cond_6

    .line 382604
    :cond_5
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2CH;->I:Z

    goto :goto_0

    .line 382605
    :cond_6
    const/4 v1, 0x1

    invoke-static {p0, v1}, LX/2CH;->a$redex0(LX/2CH;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 382606
    iget-object v1, p0, LX/2CH;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    iput-wide v1, p0, LX/2CH;->P:J

    .line 382607
    sget-object v1, LX/2AL;->TP_WAITING_FOR_FULL_LIST:LX/2AL;

    invoke-static {p0, v1}, LX/2CH;->a$redex0(LX/2CH;LX/2AL;)V

    .line 382608
    invoke-virtual {p0}, LX/2CH;->b()V

    goto :goto_1
.end method

.method public static declared-synchronized m(LX/2CH;)Z
    .locals 2

    .prologue
    .line 382573
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2CH;->L:LX/3Ol;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 382574
    const/4 v0, 0x0

    .line 382575
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2CH;->L:LX/3Ol;

    .line 382576
    iget-boolean v1, v0, LX/3Ol;->c:Z

    move v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382577
    goto :goto_0

    .line 382578
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static o(LX/2CH;)V
    .locals 2

    .prologue
    .line 382569
    iget-object v0, p0, LX/2CH;->O:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 382570
    iget-object v0, p0, LX/2CH;->O:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 382571
    const/4 v0, 0x0

    iput-object v0, p0, LX/2CH;->O:Ljava/util/concurrent/ScheduledFuture;

    .line 382572
    :cond_0
    return-void
.end method

.method public static q(LX/2CH;)V
    .locals 1

    .prologue
    .line 382566
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2CH;->E:Z

    .line 382567
    invoke-virtual {p0}, LX/2CH;->c()V

    .line 382568
    return-void
.end method

.method public static r(LX/2CH;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 382523
    iget-object v0, p0, LX/2CH;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 382524
    iget-object v0, p0, LX/2CH;->A:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->c()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/2CH;->B:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->c()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/2CH;->C:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/2CH;->D:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 382525
    iget-boolean v0, p0, LX/2CH;->F:Z

    if-eqz v0, :cond_0

    .line 382526
    :goto_0
    return-void

    .line 382527
    :cond_0
    iput-boolean v10, p0, LX/2CH;->F:Z

    .line 382528
    :goto_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 382529
    iget-object v4, p0, LX/2CH;->N:LX/0UE;

    monitor-enter v4

    .line 382530
    :try_start_0
    new-instance v5, LX/0UE;

    invoke-direct {v5}, LX/0UE;-><init>()V

    .line 382531
    iget-object v0, p0, LX/2CH;->N:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->clear()V

    .line 382532
    iget-object v0, p0, LX/2CH;->A:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 382533
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v6

    sget-object v7, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v6, v7, :cond_1

    .line 382534
    iget-object v6, p0, LX/2CH;->N:LX/0UE;

    invoke-virtual {v6, v0}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 382535
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 382536
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 382537
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 382538
    :cond_2
    iput-boolean v2, p0, LX/2CH;->F:Z

    goto :goto_1

    .line 382539
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/2CH;->B:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 382540
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v6

    sget-object v7, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v6, v7, :cond_4

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 382541
    iget-object v6, p0, LX/2CH;->N:LX/0UE;

    invoke-virtual {v6, v0}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 382542
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 382543
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 382544
    :cond_5
    iget-object v0, p0, LX/2CH;->C:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->size()I

    move-result v6

    move v1, v2

    :goto_4
    if-ge v1, v6, :cond_7

    .line 382545
    iget-object v0, p0, LX/2CH;->C:LX/0UE;

    invoke-virtual {v0, v1}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 382546
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v7

    sget-object v8, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v7, v8, :cond_6

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 382547
    iget-object v7, p0, LX/2CH;->N:LX/0UE;

    invoke-virtual {v7, v0}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 382548
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 382549
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382550
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 382551
    :cond_7
    iget-object v0, p0, LX/2CH;->D:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->size()I

    move-result v6

    move v1, v2

    :goto_5
    if-ge v1, v6, :cond_9

    .line 382552
    iget-object v0, p0, LX/2CH;->D:LX/0UE;

    invoke-virtual {v0, v1}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 382553
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v7

    sget-object v8, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v7, v8, :cond_8

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 382554
    iget-object v7, p0, LX/2CH;->N:LX/0UE;

    invoke-virtual {v7, v0}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 382555
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382556
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 382557
    :cond_9
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382558
    new-instance v0, LX/2gT;

    invoke-direct {v0, v3}, LX/2gT;-><init>(Ljava/util/List;)V

    .line 382559
    :try_start_2
    new-instance v1, LX/1so;

    new-instance v3, LX/1sp;

    invoke-direct {v3}, LX/1sp;-><init>()V

    invoke-direct {v1, v3}, LX/1so;-><init>(LX/1sq;)V

    .line 382560
    invoke-virtual {v1, v0}, LX/1so;->a(LX/1u2;)[B
    :try_end_2
    .catch LX/7H0; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    .line 382561
    array-length v1, v0

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [B

    .line 382562
    array-length v3, v0

    invoke-static {v0, v2, v1, v10, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 382563
    iget-object v0, p0, LX/2CH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gU;

    const-string v2, "/t_sac"

    sget-object v3, LX/2I2;->FIRE_AND_FORGET:LX/2I2;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, LX/2gU;->a(Ljava/lang/String;[BLX/2I2;LX/76H;)I

    goto/16 :goto_0

    .line 382564
    :catch_0
    move-exception v0

    .line 382565
    sget-object v1, LX/2CH;->a:Ljava/lang/Class;

    const-string v2, "/t_sac serialization error"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public static t(LX/2CH;)V
    .locals 4

    .prologue
    .line 382509
    iget-object v0, p0, LX/2CH;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2RB;

    .line 382510
    iget-object v1, v0, LX/2RB;->b:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    sget-object v2, LX/2RU;->FACEBOOK_FRIENDS_TYPES:LX/0Px;

    .line 382511
    iput-object v2, v1, LX/2RR;->b:Ljava/util/Collection;

    .line 382512
    move-object v1, v1

    .line 382513
    iget-object v2, v0, LX/2RB;->a:LX/2RC;

    sget-object v3, LX/2RV;->USER:LX/2RV;

    invoke-virtual {v2, v1, v3}, LX/2RC;->a(LX/2RR;LX/2RV;)Landroid/database/Cursor;

    move-result-object v1

    .line 382514
    new-instance v2, LX/2TY;

    invoke-direct {v2, v1}, LX/2TY;-><init>(Landroid/database/Cursor;)V

    move-object v1, v2

    .line 382515
    :goto_0
    :try_start_0
    invoke-virtual {v1}, LX/2TZ;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382516
    invoke-virtual {v1}, LX/2TZ;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hT;

    .line 382517
    iget-object v2, v0, LX/3hT;->a:Lcom/facebook/user/model/UserKey;

    .line 382518
    invoke-static {p0, v2}, LX/2CH;->g(LX/2CH;Lcom/facebook/user/model/UserKey;)LX/3hU;

    move-result-object v2

    .line 382519
    iget-boolean v3, v0, LX/3hT;->b:Z

    iput-boolean v3, v2, LX/3hU;->b:Z

    .line 382520
    iget-boolean v0, v0, LX/3hT;->c:Z

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, v2, LX/3hU;->c:LX/03R;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 382521
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2TZ;->close()V

    throw v0

    :cond_0
    invoke-virtual {v1}, LX/2TZ;->close()V

    .line 382522
    return-void
.end method

.method private v()V
    .locals 5

    .prologue
    .line 382504
    iget-object v1, p0, LX/2CH;->N:LX/0UE;

    monitor-enter v1

    .line 382505
    :try_start_0
    iget-object v0, p0, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hU;

    .line 382506
    iget-object v3, p0, LX/2CH;->N:LX/0UE;

    iget-object v4, v0, LX/3hU;->h:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v3, v4}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 382507
    invoke-static {v0}, LX/2CH;->a(LX/3hU;)V

    goto :goto_0

    .line 382508
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static w(LX/2CH;)V
    .locals 2

    .prologue
    .line 382386
    iget-object v0, p0, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hU;

    .line 382387
    invoke-static {v0}, LX/2CH;->a(LX/3hU;)V

    goto :goto_0

    .line 382388
    :cond_0
    return-void
.end method

.method public static z(LX/2CH;)Z
    .locals 1

    .prologue
    .line 382503
    invoke-virtual {p0}, LX/2CH;->f()LX/3CA;

    move-result-object v0

    invoke-virtual {v0}, LX/3CA;->shouldShowPresence()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/3Mg;)V
    .locals 2

    .prologue
    .line 382249
    iget-object v0, p0, LX/2CH;->x:Ljava/util/concurrent/ConcurrentMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382250
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 12
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    .line 382254
    iget-object v2, p0, LX/2CH;->G:LX/2AK;

    iget-object v0, p0, LX/2CH;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    iput-wide v4, v2, LX/2AK;->a:J

    .line 382255
    const-string v0, "extra_topic_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 382256
    const-string v0, "extra_presence_map"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/presence/PresenceList;

    .line 382257
    const-string v3, "extra_full_list"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 382258
    iget-object v4, v0, Lcom/facebook/presence/PresenceList;->a:LX/0Px;

    move-object v4, v4

    .line 382259
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 382260
    iget-object v0, p0, LX/2CH;->n:LX/13Q;

    const-string v5, "presence_mqtt_receive"

    invoke-virtual {v0, v5}, LX/13Q;->a(Ljava/lang/String;)V

    .line 382261
    iget-object v0, p0, LX/2CH;->n:LX/13Q;

    const-string v5, "presence_mqtt_receive_item_count"

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v0, v5, v6, v7}, LX/13Q;->a(Ljava/lang/String;J)V

    .line 382262
    iget-object v0, p0, LX/2CH;->o:LX/1BA;

    const v5, 0x59000e

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v0, v5, v6, v7}, LX/1BA;->a(IJ)V

    .line 382263
    sget-object v0, LX/2AL;->PRESENCE_MAP_RECEIVED:LX/2AL;

    iput-object v0, p0, LX/2CH;->J:LX/2AL;

    .line 382264
    if-eqz v3, :cond_2

    .line 382265
    if-eqz v2, :cond_4

    const-string v0, "/t_p"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 382266
    iget-object v0, p0, LX/2CH;->r:LX/2AW;

    .line 382267
    iget-boolean v2, v0, LX/2AW;->b:Z

    move v0, v2

    .line 382268
    if-nez v0, :cond_0

    iget-object v0, p0, LX/2CH;->r:LX/2AW;

    .line 382269
    iget-boolean v2, v0, LX/2AW;->a:Z

    move v0, v2

    .line 382270
    if-eqz v0, :cond_1

    :cond_0
    iget-wide v6, p0, LX/2CH;->P:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    .line 382271
    iget-object v0, p0, LX/2CH;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 382272
    iget-object v0, p0, LX/2CH;->n:LX/13Q;

    const-string v2, "android_generic_presence_delay"

    iget-wide v8, p0, LX/2CH;->P:J

    sub-long/2addr v6, v8

    invoke-virtual {v0, v2, v6, v7}, LX/13Q;->a(Ljava/lang/String;J)V

    .line 382273
    :cond_1
    sget-object v0, LX/2AL;->TP_FULL_LIST_RECEIVED:LX/2AL;

    invoke-static {p0, v0}, LX/2CH;->a$redex0(LX/2CH;LX/2AL;)V

    .line 382274
    invoke-static {p0}, LX/2CH;->w(LX/2CH;)V

    .line 382275
    :goto_0
    iget-object v0, p0, LX/2CH;->G:LX/2AK;

    iget-object v2, p0, LX/2CH;->G:LX/2AK;

    iget-wide v6, v2, LX/2AK;->a:J

    iput-wide v6, v0, LX/2AK;->b:J

    .line 382276
    iget-object v0, p0, LX/2CH;->G:LX/2AK;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    iput v2, v0, LX/2AK;->c:I

    .line 382277
    :cond_2
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_6

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/presence/PresenceItem;

    .line 382278
    iget-object v5, v0, Lcom/facebook/presence/PresenceItem;->a:Lcom/facebook/user/model/UserKey;

    invoke-static {p0, v5}, LX/2CH;->g(LX/2CH;Lcom/facebook/user/model/UserKey;)LX/3hU;

    move-result-object v5

    .line 382279
    iget-boolean v6, v0, Lcom/facebook/presence/PresenceItem;->b:Z

    iput-boolean v6, v5, LX/3hU;->d:Z

    .line 382280
    iget v6, v0, Lcom/facebook/presence/PresenceItem;->d:I

    iput v6, v5, LX/3hU;->f:I

    .line 382281
    iget-wide v6, v0, Lcom/facebook/presence/PresenceItem;->c:J

    cmp-long v6, v6, v10

    if-ltz v6, :cond_3

    .line 382282
    iget-wide v6, v0, Lcom/facebook/presence/PresenceItem;->c:J

    iput-wide v6, v5, LX/3hU;->e:J

    .line 382283
    :cond_3
    iget-object v6, v0, Lcom/facebook/presence/PresenceItem;->e:Ljava/lang/Long;

    if-eqz v6, :cond_5

    .line 382284
    iget-object v6, v0, Lcom/facebook/presence/PresenceItem;->e:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, v5, LX/3hU;->g:J

    .line 382285
    :goto_2
    iget-object v0, v0, Lcom/facebook/presence/PresenceItem;->a:Lcom/facebook/user/model/UserKey;

    invoke-static {p0, v0}, LX/2CH;->h(LX/2CH;Lcom/facebook/user/model/UserKey;)V

    .line 382286
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 382287
    :cond_4
    invoke-direct {p0}, LX/2CH;->v()V

    goto :goto_0

    .line 382288
    :cond_5
    iput-wide v10, v5, LX/3hU;->g:J

    goto :goto_2

    .line 382289
    :cond_6
    iget-object v0, p0, LX/2CH;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2a7;

    .line 382290
    iget-object v1, v0, LX/2a7;->d:LX/0Uh;

    const/16 v2, 0x248

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_7

    .line 382291
    :goto_3
    iget-object v0, p0, LX/2CH;->G:LX/2AK;

    invoke-virtual {p0}, LX/2CH;->d()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    iput v1, v0, LX/2AK;->f:I

    .line 382292
    invoke-static {p0, v3}, LX/2CH;->b(LX/2CH;Z)V

    .line 382293
    invoke-direct {p0}, LX/2CH;->j()V

    .line 382294
    return-void

    .line 382295
    :cond_7
    new-instance v1, Lcom/facebook/presence/PresenceAccuracyExpHandler$2;

    invoke-direct {v1, v0}, Lcom/facebook/presence/PresenceAccuracyExpHandler$2;-><init>(LX/2a7;)V

    .line 382296
    iget-object v2, v0, LX/2a7;->c:Ljava/util/concurrent/Executor;

    const v4, -0x88af3c8

    invoke-static {v2, v1, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_3
.end method

.method public final a(Lcom/facebook/user/model/UserKey;LX/9l7;)V
    .locals 1

    .prologue
    .line 382297
    iget-object v0, p0, LX/2CH;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 382298
    iget-object v0, p0, LX/2CH;->w:LX/0Xu;

    invoke-interface {v0, p1, p2}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 382299
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 382251
    iget-object v0, p0, LX/2CH;->H:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 382252
    invoke-direct {p0}, LX/2CH;->j()V

    .line 382253
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 382300
    iget-object v0, p0, LX/2CH;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382301
    :goto_0
    return-void

    .line 382302
    :cond_0
    iget-object v0, p0, LX/2CH;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    new-instance v1, Lcom/facebook/presence/DefaultPresenceManager$13;

    invoke-direct {v1, p0}, Lcom/facebook/presence/DefaultPresenceManager$13;-><init>(LX/2CH;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final b(LX/3Mg;)V
    .locals 1

    .prologue
    .line 382303
    iget-object v0, p0, LX/2CH;->x:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 382304
    return-void
.end method

.method public final b(Lcom/facebook/user/model/UserKey;)V
    .locals 1

    .prologue
    .line 382305
    iget-object v0, p0, LX/2CH;->D:LX/0UE;

    invoke-virtual {v0, p1}, LX/0UE;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 382306
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2CH;->A:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2CH;->B:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2CH;->C:LX/0UE;

    invoke-virtual {v0, p1}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 382307
    invoke-virtual {p0}, LX/2CH;->b()V

    .line 382308
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/user/model/UserKey;LX/9l7;)V
    .locals 1

    .prologue
    .line 382309
    iget-object v0, p0, LX/2CH;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 382310
    iget-object v0, p0, LX/2CH;->w:LX/0Xu;

    invoke-interface {v0, p1, p2}, LX/0Xu;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 382311
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 382312
    iget-object v0, p0, LX/2CH;->H:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 382313
    invoke-direct {p0}, LX/2CH;->j()V

    .line 382314
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 382315
    iget-boolean v0, p0, LX/2CH;->E:Z

    if-eqz v0, :cond_0

    .line 382316
    :goto_0
    return-void

    .line 382317
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2CH;->E:Z

    .line 382318
    iget-object v0, p0, LX/2CH;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    new-instance v1, LX/2R2;

    invoke-direct {v1, p0}, LX/2R2;-><init>(LX/2CH;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 382319
    new-instance v2, LX/2R3;

    invoke-direct {v2, p0}, LX/2R3;-><init>(LX/2CH;)V

    iget-object v0, p0, LX/2CH;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final c(Lcom/facebook/user/model/UserKey;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 382320
    invoke-static {p0}, LX/2CH;->z(LX/2CH;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 382321
    :goto_0
    return v0

    .line 382322
    :cond_0
    iget-object v0, p0, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hU;

    .line 382323
    if-eqz v0, :cond_1

    iget-boolean v0, v0, LX/3hU;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final d(Lcom/facebook/user/model/UserKey;)LX/3Ox;
    .locals 5

    .prologue
    .line 382324
    iget-object v0, p0, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hU;

    .line 382325
    if-nez v0, :cond_0

    .line 382326
    sget-object v0, LX/3Ox;->a:LX/3Ox;

    .line 382327
    :goto_0
    return-object v0

    .line 382328
    :cond_0
    invoke-static {p0}, LX/2CH;->z(LX/2CH;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 382329
    iget-boolean v1, v0, LX/3hU;->d:Z

    if-eqz v1, :cond_1

    .line 382330
    sget-object v2, LX/3Oz;->AVAILABLE:LX/3Oz;

    .line 382331
    iget v1, v0, LX/3hU;->f:I

    .line 382332
    :goto_1
    new-instance v3, LX/3Oy;

    invoke-direct {v3}, LX/3Oy;-><init>()V

    .line 382333
    iput-object v2, v3, LX/3Oy;->a:LX/3Oz;

    .line 382334
    move-object v2, v3

    .line 382335
    iget-boolean v3, v0, LX/3hU;->b:Z

    .line 382336
    iput-boolean v3, v2, LX/3Oy;->b:Z

    .line 382337
    move-object v2, v2

    .line 382338
    iget-object v3, v0, LX/3hU;->c:LX/03R;

    .line 382339
    iput-object v3, v2, LX/3Oy;->c:LX/03R;

    .line 382340
    move-object v2, v2

    .line 382341
    iget-boolean v3, v0, LX/3hU;->a:Z

    .line 382342
    iput-boolean v3, v2, LX/3Oy;->d:Z

    .line 382343
    move-object v2, v2

    .line 382344
    iput v1, v2, LX/3Oy;->e:I

    .line 382345
    move-object v1, v2

    .line 382346
    iget-wide v2, v0, LX/3hU;->g:J

    .line 382347
    iput-wide v2, v1, LX/3Oy;->f:J

    .line 382348
    move-object v0, v1

    .line 382349
    invoke-virtual {v0}, LX/3Oy;->g()LX/3Ox;

    move-result-object v0

    goto :goto_0

    .line 382350
    :cond_1
    sget-object v2, LX/3Oz;->NONE:LX/3Oz;

    .line 382351
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final d()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 382352
    invoke-static {p0}, LX/2CH;->z(LX/2CH;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 382353
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 382354
    :goto_0
    return-object v0

    .line 382355
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 382356
    iget-object v0, p0, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 382357
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3hU;

    iget-boolean v1, v1, LX/3hU;->d:Z

    if-eqz v1, :cond_1

    .line 382358
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 382359
    goto :goto_0
.end method

.method public final e(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/LastActive;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 382360
    iget-object v0, p0, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hU;

    .line 382361
    if-nez v0, :cond_0

    move-object v0, v1

    .line 382362
    :goto_0
    return-object v0

    .line 382363
    :cond_0
    iget-wide v2, v0, LX/3hU;->e:J

    .line 382364
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    .line 382365
    const-wide v4, 0x20c49ba5e353f7L

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 382366
    iget-object v0, p0, LX/2CH;->m:LX/03V;

    const-string v4, "PresenceManagerError"

    const-string v5, "getLastActiveForUser invalid last active (overflow): %d seconds for user %s"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 382367
    goto :goto_0

    .line 382368
    :cond_1
    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, v2

    .line 382369
    iget-object v0, p0, LX/2CH;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    .line 382370
    const-wide v8, 0x39ef8b000L

    cmp-long v0, v6, v8

    if-lez v0, :cond_2

    .line 382371
    iget-object v0, p0, LX/2CH;->m:LX/03V;

    const-string v4, "PresenceManagerError"

    const-string v5, "getLastActiveForUser stale last active: %d seconds for user %s"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 382372
    goto :goto_0

    .line 382373
    :cond_2
    new-instance v0, Lcom/facebook/user/model/LastActive;

    invoke-direct {v0, v4, v5}, Lcom/facebook/user/model/LastActive;-><init>(J)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 382374
    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 382375
    iget-object v0, p0, LX/2CH;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final f()LX/3CA;
    .locals 1

    .prologue
    .line 382376
    invoke-virtual {p0}, LX/2CH;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/3CA;->ENABLED:LX/3CA;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/3CA;->DISABLED:LX/3CA;

    goto :goto_0
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 382377
    iget-object v0, p0, LX/2CH;->u:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 382378
    iget-object v0, p0, LX/2CH;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1qz;->a:LX/0Tn;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    iget-object v2, p0, LX/2CH;->j:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;LX/0dN;)V

    .line 382379
    return-void
.end method

.method public final onAppActive()V
    .locals 3

    .prologue
    .line 382380
    iget-object v0, p0, LX/2CH;->s:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/presence/DefaultPresenceManager$11;

    invoke-direct {v1, p0}, Lcom/facebook/presence/DefaultPresenceManager$11;-><init>(LX/2CH;)V

    const v2, -0x43b52cdc

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 382381
    return-void
.end method

.method public final onAppStopped()V
    .locals 3

    .prologue
    .line 382382
    iget-object v0, p0, LX/2CH;->s:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/presence/DefaultPresenceManager$12;

    invoke-direct {v1, p0}, Lcom/facebook/presence/DefaultPresenceManager$12;-><init>(LX/2CH;)V

    const v2, -0x570c691d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 382383
    return-void
.end method

.method public final onDeviceActive()V
    .locals 0

    .prologue
    .line 382384
    return-void
.end method

.method public final onDeviceStopped()V
    .locals 0

    .prologue
    .line 382385
    return-void
.end method
