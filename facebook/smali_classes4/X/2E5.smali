.class public LX/2E5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile d:LX/2E5;


# instance fields
.field public final b:Landroid/content/Context;

.field public c:LX/12x;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 384955
    const-class v0, LX/2E5;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2E5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/12x;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384957
    iput-object p1, p0, LX/2E5;->b:Landroid/content/Context;

    .line 384958
    iput-object p2, p0, LX/2E5;->c:LX/12x;

    .line 384959
    return-void
.end method

.method public static a(LX/0QB;)LX/2E5;
    .locals 5

    .prologue
    .line 384960
    sget-object v0, LX/2E5;->d:LX/2E5;

    if-nez v0, :cond_1

    .line 384961
    const-class v1, LX/2E5;

    monitor-enter v1

    .line 384962
    :try_start_0
    sget-object v0, LX/2E5;->d:LX/2E5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 384963
    if-eqz v2, :cond_0

    .line 384964
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 384965
    new-instance p0, LX/2E5;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v4

    check-cast v4, LX/12x;

    invoke-direct {p0, v3, v4}, LX/2E5;-><init>(Landroid/content/Context;LX/12x;)V

    .line 384966
    move-object v0, p0

    .line 384967
    sput-object v0, LX/2E5;->d:LX/2E5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384968
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 384969
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 384970
    :cond_1
    sget-object v0, LX/2E5;->d:LX/2E5;

    return-object v0

    .line 384971
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 384972
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/2E5;)V
    .locals 4

    .prologue
    .line 384973
    :try_start_0
    iget-object v0, p0, LX/2E5;->b:Landroid/content/Context;

    invoke-static {v0}, LX/2E6;->a(Landroid/content/Context;)LX/2E6;

    move-result-object v0

    .line 384974
    const-class v1, Lcom/facebook/bugreporter/scheduler/GCMBugReportService;

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, v0, LX/2E6;->b:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/2E6;->b(LX/2E6;Ljava/lang/String;)V

    invoke-static {v0}, LX/2E6;->a(LX/2E6;)Landroid/content/Intent;

    move-result-object v3

    if-nez v3, :cond_0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 384975
    :goto_0
    return-void

    .line 384976
    :catch_0
    move-exception v0

    .line 384977
    sget-object v1, LX/2E5;->a:Ljava/lang/String;

    const-string v2, "Unexpected exception while scheduling using GCM scheduler. t9592389."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const-string p0, "scheduler_action"

    const-string v1, "CANCEL_ALL"

    invoke-virtual {v3, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p0, "component"

    invoke-virtual {v3, p0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object p0, v0, LX/2E6;->b:Landroid/content/Context;

    invoke-virtual {p0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static e(LX/2E5;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 384978
    :try_start_0
    sget-object v1, LX/1vX;->c:LX/1vX;

    move-object v1, v1

    .line 384979
    iget-object v2, p0, LX/2E5;->b:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/1od;->a(Landroid/content/Context;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 384980
    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 384981
    :cond_0
    :goto_0
    return v0

    .line 384982
    :catch_0
    move-exception v1

    .line 384983
    sget-object v2, LX/2E5;->a:Ljava/lang/String;

    const-string v3, "Unexpected exception while scheduling using GCM scheduler. GCM causes Package manager to die sometimes. t9600529."

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static f()Z
    .locals 2

    .prologue
    .line 384984
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)Z
    .locals 12

    .prologue
    .line 384985
    invoke-static {}, LX/2E5;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384986
    const/4 v3, 0x0

    .line 384987
    :try_start_0
    iget-object v0, p0, LX/2E5;->b:Landroid/content/Context;

    const-string v1, "jobscheduler"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 384988
    sget-object v1, LX/2E5;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->cancel(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 384989
    :cond_0
    :goto_0
    invoke-static {p0}, LX/2E5;->e(LX/2E5;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384990
    invoke-static {p0}, LX/2E5;->c(LX/2E5;)V

    .line 384991
    :cond_1
    const/4 v3, 0x0

    .line 384992
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2E5;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/bugreporter/scheduler/AlarmsBroadcastReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 384993
    const-string v1, "com.facebook.bugreporter.scheduler.AlarmsBroadcastReceiver.RETRY_UPLOAD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 384994
    iget-object v1, p0, LX/2E5;->b:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 384995
    iget-object v1, p0, LX/2E5;->c:LX/12x;

    invoke-virtual {v1, v0}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 384996
    const/4 v0, 0x0

    .line 384997
    invoke-static {}, LX/2E5;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 384998
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 384999
    new-instance v6, Landroid/content/ComponentName;

    iget-object v3, p0, LX/2E5;->b:Landroid/content/Context;

    const-class v7, Lcom/facebook/bugreporter/scheduler/LollipopBugReportService;

    invoke-direct {v6, v3, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 385000
    iget-object v3, p0, LX/2E5;->b:Landroid/content/Context;

    const-string v7, "jobscheduler"

    invoke-virtual {v3, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/job/JobScheduler;

    .line 385001
    new-instance v7, Landroid/app/job/JobInfo$Builder;

    const v8, 0x7f0d00f7

    invoke-direct {v7, v8, v6}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    invoke-virtual {v7, v4}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v7

    const-wide/32 v9, 0xea60

    mul-long/2addr v9, p1

    invoke-virtual {v7, v9, v10}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/app/job/JobInfo$Builder;->setRequiresDeviceIdle(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v7

    .line 385002
    :try_start_1
    invoke-virtual {v3, v7}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3

    move-result v3

    if-lez v3, :cond_5

    move v3, v4

    .line 385003
    :goto_1
    move v0, v3

    .line 385004
    :cond_2
    if-nez v0, :cond_3

    invoke-static {p0}, LX/2E5;->e(LX/2E5;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 385005
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 385006
    const-wide/16 v5, 0x3c

    mul-long/2addr v5, p1

    .line 385007
    const-wide/16 v7, 0x1e

    add-long/2addr v7, v5

    .line 385008
    new-instance v9, LX/2E9;

    invoke-direct {v9}, LX/2E9;-><init>()V

    const-class v10, Lcom/facebook/bugreporter/scheduler/GCMBugReportService;

    invoke-virtual {v9, v10}, LX/2E9;->a(Ljava/lang/Class;)LX/2E9;

    move-result-object v9

    const-string v10, "GCMBugReportService"

    iput-object v10, v9, LX/2EA;->c:Ljava/lang/String;

    move-object v9, v9

    .line 385009
    invoke-virtual {v9, v5, v6, v7, v8}, LX/2E9;->a(JJ)LX/2E9;

    move-result-object v5

    iput-boolean v3, v5, LX/2EA;->d:Z

    move-object v5, v5

    .line 385010
    iput v4, v5, LX/2EA;->a:I

    move-object v5, v5

    .line 385011
    iput-boolean v4, v5, LX/2EA;->f:Z

    move-object v5, v5

    .line 385012
    iput-boolean v4, v5, LX/2EA;->e:Z

    move-object v5, v5

    .line 385013
    invoke-virtual {v5}, LX/2E9;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v5

    .line 385014
    iget-object v6, p0, LX/2E5;->b:Landroid/content/Context;

    invoke-static {v6}, LX/2E6;->a(Landroid/content/Context;)LX/2E6;

    move-result-object v6

    .line 385015
    :try_start_2
    invoke-virtual {v6, v5}, LX/2E6;->a(Lcom/google/android/gms/gcm/Task;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_4

    .line 385016
    :goto_2
    move v0, v3

    .line 385017
    :cond_3
    if-nez v0, :cond_4

    .line 385018
    const/4 v6, 0x0

    .line 385019
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, LX/2E5;->b:Landroid/content/Context;

    const-class v5, Lcom/facebook/bugreporter/scheduler/AlarmsBroadcastReceiver;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 385020
    const-string v4, "com.facebook.bugreporter.scheduler.AlarmsBroadcastReceiver.RETRY_UPLOAD"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 385021
    iget-object v4, p0, LX/2E5;->b:Landroid/content/Context;

    invoke-static {v4, v6, v3, v6}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 385022
    iget-object v4, p0, LX/2E5;->c:LX/12x;

    const/4 v5, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    const-wide/32 v9, 0xea60

    mul-long/2addr v9, p1

    add-long/2addr v7, v9

    invoke-virtual {v4, v5, v7, v8, v3}, LX/12x;->b(IJLandroid/app/PendingIntent;)V

    .line 385023
    const/4 v3, 0x1

    move v0, v3

    .line 385024
    :cond_4
    return v0

    .line 385025
    :catch_0
    move-exception v0

    .line 385026
    sget-object v1, LX/2E5;->a:Ljava/lang/String;

    const-string v2, "Unexpected exception while canceling using post-lollipop scheduler. t9587579."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 385027
    :catch_1
    move-exception v0

    .line 385028
    sget-object v1, LX/2E5;->a:Ljava/lang/String;

    const-string v2, "Unexpected exception while canceling using post-lollipop scheduler. The mBinder is null for some reason in JobSchedulerImpl.java. t9587579."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_5
    move v3, v5

    .line 385029
    goto/16 :goto_1

    .line 385030
    :catch_2
    move-exception v3

    .line 385031
    iget-object v4, p0, LX/2E5;->b:Landroid/content/Context;

    invoke-static {v4, v6, v3}, LX/2EB;->a(Landroid/content/Context;Landroid/content/ComponentName;Ljava/lang/IllegalArgumentException;)V

    :goto_3
    move v3, v5

    .line 385032
    goto/16 :goto_1

    .line 385033
    :catch_3
    move-exception v3

    .line 385034
    sget-object v4, LX/2E5;->a:Ljava/lang/String;

    const-string v6, "Unexpected exception while scheduling using post-lollipop scheduler. The mBinder is null for some reason in JobSchedulerImpl.java. t9587579."

    new-array v7, v5, [Ljava/lang/Object;

    invoke-static {v4, v3, v6, v7}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 385035
    :catch_4
    move-exception v3

    .line 385036
    iget-object v6, p0, LX/2E5;->b:Landroid/content/Context;

    new-instance v7, Landroid/content/ComponentName;

    iget-object v8, p0, LX/2E5;->b:Landroid/content/Context;

    iget-object v9, v5, Lcom/google/android/gms/gcm/Task;->a:Ljava/lang/String;

    move-object v5, v9

    .line 385037
    invoke-direct {v7, v8, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {v6, v7, v3}, LX/2EB;->a(Landroid/content/Context;Landroid/content/ComponentName;Ljava/lang/IllegalArgumentException;)V

    move v3, v4

    .line 385038
    goto :goto_2
.end method
