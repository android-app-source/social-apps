.class public LX/30Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final P:Ljava/lang/Object;

.field private static final a:LX/30b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/30b",
            "<",
            "LX/EjO;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/30b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/30b",
            "<",
            "LX/Eju;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Ljava/lang/String;


# instance fields
.field public A:LX/Ejw;

.field private B:LX/444;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/444",
            "<",
            "LX/EjO;",
            "LX/Eju;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public C:Z

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field public M:J

.field public N:I

.field public O:J

.field public final e:LX/0Sh;

.field public final f:LX/30e;

.field public final g:LX/30d;

.field public final h:LX/0SG;

.field private final i:LX/2Is;

.field public final j:LX/2J1;

.field private final k:LX/2JC;

.field public final l:LX/1wU;

.field private final m:Ljava/util/concurrent/Executor;

.field public final n:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final o:LX/0tX;

.field private final p:LX/0ad;

.field public final q:LX/0kL;

.field private final r:Landroid/telephony/TelephonyManager;

.field private final s:LX/0dC;

.field public t:LX/35c;

.field public u:Ljava/lang/String;

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public w:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public x:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/EjI;",
            ">;"
        }
    .end annotation
.end field

.field public y:Z

.field public z:LX/EjP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 484876
    new-instance v0, LX/30a;

    invoke-direct {v0}, LX/30a;-><init>()V

    sput-object v0, LX/30Z;->a:LX/30b;

    .line 484877
    new-instance v0, LX/2Io;

    invoke-direct {v0}, LX/2Io;-><init>()V

    sput-object v0, LX/30Z;->b:LX/30b;

    .line 484878
    new-instance v0, LX/30c;

    invoke-direct {v0}, LX/30c;-><init>()V

    sput-object v0, LX/30Z;->c:Ljava/util/Comparator;

    .line 484879
    const-class v0, LX/30Z;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/30Z;->d:Ljava/lang/String;

    .line 484880
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/30Z;->P:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/30d;LX/30e;LX/0SG;LX/2Is;LX/2J1;LX/2JC;LX/1wU;Ljava/util/concurrent/ExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0tX;LX/0ad;Landroid/telephony/TelephonyManager;LX/0kL;LX/0dC;)V
    .locals 4
    .param p9    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 484852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484853
    sget-object v2, LX/35c;->a:LX/35c;

    iput-object v2, p0, LX/30Z;->t:LX/35c;

    .line 484854
    const/4 v2, 0x0

    iput-object v2, p0, LX/30Z;->z:LX/EjP;

    .line 484855
    const/4 v2, 0x0

    iput-object v2, p0, LX/30Z;->A:LX/Ejw;

    .line 484856
    const/4 v2, 0x0

    iput-object v2, p0, LX/30Z;->B:LX/444;

    .line 484857
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/30Z;->C:Z

    .line 484858
    const/4 v2, 0x0

    iput v2, p0, LX/30Z;->N:I

    .line 484859
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/30Z;->O:J

    .line 484860
    iput-object p1, p0, LX/30Z;->e:LX/0Sh;

    .line 484861
    iput-object p2, p0, LX/30Z;->g:LX/30d;

    .line 484862
    iput-object p3, p0, LX/30Z;->f:LX/30e;

    .line 484863
    iput-object p4, p0, LX/30Z;->h:LX/0SG;

    .line 484864
    iput-object p5, p0, LX/30Z;->i:LX/2Is;

    .line 484865
    iput-object p6, p0, LX/30Z;->j:LX/2J1;

    .line 484866
    iput-object p7, p0, LX/30Z;->k:LX/2JC;

    .line 484867
    iput-object p8, p0, LX/30Z;->l:LX/1wU;

    .line 484868
    iput-object p9, p0, LX/30Z;->m:Ljava/util/concurrent/Executor;

    .line 484869
    iput-object p10, p0, LX/30Z;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 484870
    iput-object p11, p0, LX/30Z;->o:LX/0tX;

    .line 484871
    move-object/from16 v0, p12

    iput-object v0, p0, LX/30Z;->p:LX/0ad;

    .line 484872
    move-object/from16 v0, p13

    iput-object v0, p0, LX/30Z;->r:Landroid/telephony/TelephonyManager;

    .line 484873
    move-object/from16 v0, p14

    iput-object v0, p0, LX/30Z;->q:LX/0kL;

    .line 484874
    move-object/from16 v0, p15

    iput-object v0, p0, LX/30Z;->s:LX/0dC;

    .line 484875
    return-void
.end method

.method public static a(LX/0QB;)LX/30Z;
    .locals 7

    .prologue
    .line 484825
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 484826
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 484827
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 484828
    if-nez v1, :cond_0

    .line 484829
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 484830
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 484831
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 484832
    sget-object v1, LX/30Z;->P:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 484833
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 484834
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 484835
    :cond_1
    if-nez v1, :cond_4

    .line 484836
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 484837
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 484838
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/30Z;->b(LX/0QB;)LX/30Z;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 484839
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 484840
    if-nez v1, :cond_2

    .line 484841
    sget-object v0, LX/30Z;->P:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30Z;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 484842
    :goto_1
    if-eqz v0, :cond_3

    .line 484843
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 484844
    :goto_3
    check-cast v0, LX/30Z;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 484845
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 484846
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 484847
    :catchall_1
    move-exception v0

    .line 484848
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 484849
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 484850
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 484851
    :cond_2
    :try_start_8
    sget-object v0, LX/30Z;->P:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30Z;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 484821
    invoke-static {p0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 484822
    const-string v0, ":"

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 484823
    sget-object v1, LX/51s;->a:LX/51l;

    move-object v1, v1

    .line 484824
    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v0, v2}, LX/51l;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51o;

    move-result-object v0

    invoke-virtual {v0}, LX/51o;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/15i;I)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 484803
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 484804
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 484805
    const v0, -0x121a803

    invoke-static {p1, p2, v8, v0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 484806
    invoke-virtual {v4, v3, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 484807
    const/4 v6, 0x0

    invoke-virtual {v4, v3, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 484808
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 484809
    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 484810
    new-instance v4, LX/Eju;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-direct {v4, v6, v7, v3}, LX/Eju;-><init>(JLjava/lang/String;)V

    sget-object v3, LX/Ejt;->ADD:LX/Ejt;

    .line 484811
    iput-object v3, v4, LX/Eju;->c:LX/Ejt;

    .line 484812
    move-object v3, v4

    .line 484813
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 484814
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 484815
    :cond_1
    iget-object v3, p0, LX/30Z;->g:LX/30d;

    iget-object v4, p0, LX/30Z;->u:Ljava/lang/String;

    .line 484816
    iget-object v6, v3, LX/30d;->a:LX/0Zb;

    sget-object v7, LX/Ejs;->CCU_INVALID_CONTACT_ID:LX/Ejs;

    invoke-virtual {v7}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/30d;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p1, "contact_id"

    invoke-virtual {v7, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p1, "ccu_session_id"

    invoke-virtual {v7, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 484817
    goto :goto_1

    .line 484818
    :cond_2
    iget-object v0, p0, LX/30Z;->j:LX/2J1;

    invoke-virtual {v0}, LX/2J1;->a()V

    .line 484819
    iget-object v0, p0, LX/30Z;->j:LX/2J1;

    invoke-virtual {v0, v1}, LX/2J1;->a(Ljava/util/List;)V

    .line 484820
    return-void
.end method

.method private a(Ljava/lang/Boolean;Z)V
    .locals 16

    .prologue
    .line 484791
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->g:LX/30d;

    sget-object v3, LX/Ejs;->CLOSE_SESSION_START:LX/Ejs;

    invoke-virtual {v3}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/30d;->c(Ljava/lang/String;)V

    .line 484792
    if-eqz p2, :cond_0

    .line 484793
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->f:LX/30e;

    invoke-virtual {v2}, LX/30e;->h()V

    .line 484794
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->g:LX/30d;

    move-object/from16 v0, p0

    iget v5, v0, LX/30Z;->K:I

    move-object/from16 v0, p0

    iget v6, v0, LX/30Z;->L:I

    move-object/from16 v0, p0

    iget v7, v0, LX/30Z;->H:I

    move-object/from16 v0, p0

    iget v8, v0, LX/30Z;->I:I

    move-object/from16 v0, p0

    iget v9, v0, LX/30Z;->J:I

    move-object/from16 v0, p0

    iget v10, v0, LX/30Z;->D:I

    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->t:LX/35c;

    iget v11, v2, LX/35c;->e:I

    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/30Z;->O:J

    sub-long/2addr v12, v14

    move-object/from16 v0, p0

    iget v14, v0, LX/30Z;->N:I

    move-object/from16 v0, p0

    iget-object v15, v0, LX/30Z;->u:Ljava/lang/String;

    move/from16 v4, p2

    invoke-virtual/range {v3 .. v15}, LX/30d;->a(ZIIIIIIIJILjava/lang/String;)V

    .line 484795
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2vf;->b:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 484796
    new-instance v3, LX/4Dm;

    invoke-direct {v3}, LX/4Dm;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/4Dm;->a(Ljava/lang/String;)LX/4Dm;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/4Dm;->b(Ljava/lang/String;)LX/4Dm;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/4Dm;->a(Ljava/lang/Boolean;)LX/4Dm;

    move-result-object v2

    .line 484797
    invoke-static {}, LX/EjY;->a()LX/EjX;

    move-result-object v3

    .line 484798
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 484799
    new-instance v2, LX/EjZ;

    invoke-direct {v2}, LX/EjZ;-><init>()V

    invoke-virtual {v2}, LX/EjZ;->a()Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;

    move-result-object v2

    .line 484800
    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->o:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 484801
    new-instance v3, LX/EjH;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v3, v0, v1}, LX/EjH;-><init>(LX/30Z;Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->m:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 484802
    return-void
.end method

.method private a(Z)V
    .locals 17

    .prologue
    .line 484740
    new-instance v2, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->t:LX/35c;

    iget v3, v3, LX/35c;->d:I

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(I)V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/30Z;->w:Ljava/util/Set;

    .line 484741
    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/30Z;->x:Ljava/util/Queue;

    .line 484742
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/30Z;->y:Z

    .line 484743
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 484744
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 484745
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->t:LX/35c;

    iget v0, v2, LX/35c;->b:I

    move/from16 v16, v0

    .line 484746
    const/4 v8, 0x0

    .line 484747
    const/4 v3, 0x0

    .line 484748
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->B:LX/444;

    invoke-virtual {v2}, LX/0Rr;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 484749
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->B:LX/444;

    invoke-virtual {v2}, LX/0Rr;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/443;

    .line 484750
    move-object/from16 v0, p0

    invoke-static {v0, v2, v4, v5}, LX/30Z;->a(LX/30Z;LX/443;LX/0Pz;LX/0Pz;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 484751
    add-int/lit8 v8, v8, 0x1

    move/from16 v0, v16

    if-lt v8, v0, :cond_6

    .line 484752
    new-instance v2, LX/EjI;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct/range {v2 .. v6}, LX/EjI;-><init>(ILjava/util/List;Ljava/util/List;Z)V

    .line 484753
    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->g:LX/30d;

    move-object/from16 v0, p0

    iget v9, v0, LX/30Z;->E:I

    move-object/from16 v0, p0

    iget v10, v0, LX/30Z;->F:I

    move-object/from16 v0, p0

    iget v11, v0, LX/30Z;->G:I

    move-object/from16 v0, p0

    iget-object v5, v0, LX/30Z;->h:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/30Z;->O:J

    sub-long v12, v6, v12

    move-object/from16 v0, p0

    iget v14, v0, LX/30Z;->N:I

    move-object/from16 v0, p0

    iget-object v15, v0, LX/30Z;->u:Ljava/lang/String;

    move/from16 v5, p1

    move v6, v3

    move/from16 v7, v16

    invoke-virtual/range {v4 .. v15}, LX/30d;->a(ZIIIIIIJILjava/lang/String;)V

    .line 484754
    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->w:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/30Z;->t:LX/35c;

    iget v5, v5, LX/35c;->d:I

    if-ge v4, v5, :cond_1

    .line 484755
    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->w:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 484756
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v2, v1}, LX/30Z;->a$redex0(LX/30Z;LX/EjI;Z)V

    .line 484757
    :goto_1
    add-int/lit8 v3, v3, 0x1

    .line 484758
    const/4 v8, 0x0

    .line 484759
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 484760
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 484761
    move-object/from16 v0, p0

    iget v5, v0, LX/30Z;->H:I

    move-object/from16 v0, p0

    iget v6, v0, LX/30Z;->E:I

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, LX/30Z;->H:I

    .line 484762
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, LX/30Z;->E:I

    .line 484763
    move-object/from16 v0, p0

    iget v5, v0, LX/30Z;->I:I

    move-object/from16 v0, p0

    iget v6, v0, LX/30Z;->F:I

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, LX/30Z;->I:I

    .line 484764
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, LX/30Z;->F:I

    .line 484765
    move-object/from16 v0, p0

    iget v5, v0, LX/30Z;->J:I

    move-object/from16 v0, p0

    iget v6, v0, LX/30Z;->G:I

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iput v5, v0, LX/30Z;->J:I

    .line 484766
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, LX/30Z;->G:I

    :goto_2
    move-object v5, v2

    .line 484767
    goto/16 :goto_0

    .line 484768
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->x:Ljava/util/Queue;

    invoke-interface {v4, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 484769
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->z:LX/EjP;

    invoke-virtual {v3}, LX/EjP;->d()V

    .line 484770
    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->A:LX/Ejw;

    invoke-virtual {v3}, LX/2TZ;->close()V

    throw v2

    .line 484771
    :cond_2
    if-lez v8, :cond_5

    .line 484772
    :try_start_1
    new-instance v2, LX/EjI;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct/range {v2 .. v6}, LX/EjI;-><init>(ILjava/util/List;Ljava/util/List;Z)V

    .line 484773
    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->g:LX/30d;

    move-object/from16 v0, p0

    iget v9, v0, LX/30Z;->E:I

    move-object/from16 v0, p0

    iget v10, v0, LX/30Z;->F:I

    move-object/from16 v0, p0

    iget v11, v0, LX/30Z;->G:I

    move-object/from16 v0, p0

    iget-object v5, v0, LX/30Z;->h:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/30Z;->O:J

    sub-long v12, v6, v12

    move-object/from16 v0, p0

    iget v14, v0, LX/30Z;->N:I

    move-object/from16 v0, p0

    iget-object v15, v0, LX/30Z;->u:Ljava/lang/String;

    move/from16 v5, p1

    move v6, v3

    move/from16 v7, v16

    invoke-virtual/range {v4 .. v15}, LX/30d;->a(ZIIIIIIJILjava/lang/String;)V

    .line 484774
    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->w:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/30Z;->t:LX/35c;

    iget v5, v5, LX/35c;->d:I

    if-ge v4, v5, :cond_4

    .line 484775
    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->w:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 484776
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v2, v1}, LX/30Z;->a$redex0(LX/30Z;LX/EjI;Z)V

    .line 484777
    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, LX/30Z;->H:I

    move-object/from16 v0, p0

    iget v4, v0, LX/30Z;->E:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->H:I

    .line 484778
    move-object/from16 v0, p0

    iget v2, v0, LX/30Z;->I:I

    move-object/from16 v0, p0

    iget v4, v0, LX/30Z;->F:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->I:I

    .line 484779
    move-object/from16 v0, p0

    iget v2, v0, LX/30Z;->J:I

    move-object/from16 v0, p0

    iget v4, v0, LX/30Z;->G:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->J:I

    .line 484780
    add-int/lit8 v2, v3, 0x1

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->K:I

    .line 484781
    :goto_4
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/30Z;->y:Z

    .line 484782
    move-object/from16 v0, p0

    iget v2, v0, LX/30Z;->H:I

    move-object/from16 v0, p0

    iget v4, v0, LX/30Z;->I:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, LX/30Z;->J:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->L:I

    .line 484783
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v4, LX/2vf;->b:LX/0Tn;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/30Z;->v:Ljava/util/List;

    invoke-static {v5}, LX/30Z;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v4, v5}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 484784
    if-nez v8, :cond_3

    if-nez v3, :cond_3

    .line 484785
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v2, v1}, LX/30Z;->a(Ljava/lang/Boolean;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 484786
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->z:LX/EjP;

    invoke-virtual {v2}, LX/EjP;->d()V

    .line 484787
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->A:LX/Ejw;

    invoke-virtual {v2}, LX/2TZ;->close()V

    .line 484788
    return-void

    .line 484789
    :cond_4
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->x:Ljava/util/Queue;

    invoke-interface {v4, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 484790
    :cond_5
    move-object/from16 v0, p0

    iput v3, v0, LX/30Z;->K:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    :cond_6
    move-object v2, v5

    goto/16 :goto_2
.end method

.method private static a(LX/30Z;LX/443;LX/0Pz;LX/0Pz;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/443",
            "<",
            "LX/EjO;",
            "LX/Eju;",
            ">;",
            "LX/0Pz",
            "<",
            "LX/EjO;",
            ">;",
            "LX/0Pz",
            "<",
            "LX/Eju;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 484881
    iget-object v0, p1, LX/443;->a:Ljava/lang/Object;

    check-cast v0, LX/EjO;

    .line 484882
    iget-object v1, p1, LX/443;->b:Ljava/lang/Object;

    check-cast v1, LX/Eju;

    .line 484883
    const/4 v2, 0x0

    .line 484884
    if-nez v0, :cond_3

    .line 484885
    new-instance v0, LX/EjO;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, v1, LX/Eju;->a:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, LX/EjO;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/EjN;->REMOVE:LX/EjN;

    invoke-virtual {v3}, LX/EjN;->toString()Ljava/lang/String;

    move-result-object v3

    .line 484886
    iput-object v3, v0, LX/EjO;->g:Ljava/lang/String;

    .line 484887
    move-object v0, v0

    .line 484888
    sget-object v3, LX/Ejt;->REMOVE:LX/Ejt;

    .line 484889
    iput-object v3, v1, LX/Eju;->c:LX/Ejt;

    .line 484890
    iget v3, p0, LX/30Z;->F:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/30Z;->F:I

    .line 484891
    :cond_0
    :goto_0
    sget-object v3, LX/EjN;->REMOVE:LX/EjN;

    invoke-virtual {v3}, LX/EjN;->toString()Ljava/lang/String;

    move-result-object v3

    .line 484892
    iget-object v4, v0, LX/EjO;->g:Ljava/lang/String;

    move-object v4, v4

    .line 484893
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 484894
    iget-object v3, p0, LX/30Z;->v:Ljava/util/List;

    invoke-virtual {v0}, LX/EjO;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 484895
    :cond_1
    iget-object v3, v0, LX/EjO;->g:Ljava/lang/String;

    move-object v3, v3

    .line 484896
    if-eqz v3, :cond_2

    .line 484897
    const/4 v2, 0x1

    .line 484898
    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 484899
    invoke-virtual {p3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    :cond_2
    move v0, v2

    .line 484900
    return v0

    .line 484901
    :cond_3
    if-nez v1, :cond_4

    .line 484902
    iget v3, p0, LX/30Z;->D:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/30Z;->D:I

    iget-object v4, p0, LX/30Z;->t:LX/35c;

    iget v4, v4, LX/35c;->e:I

    if-gt v3, v4, :cond_0

    .line 484903
    sget-object v1, LX/EjN;->ADD:LX/EjN;

    invoke-virtual {v1}, LX/EjN;->toString()Ljava/lang/String;

    move-result-object v1

    .line 484904
    iput-object v1, v0, LX/EjO;->g:Ljava/lang/String;

    .line 484905
    new-instance v1, LX/Eju;

    invoke-virtual {v0}, LX/EjO;->a()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, LX/EjO;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v4, v5, v3}, LX/Eju;-><init>(JLjava/lang/String;)V

    sget-object v3, LX/Ejt;->ADD:LX/Ejt;

    .line 484906
    iput-object v3, v1, LX/Eju;->c:LX/Ejt;

    .line 484907
    move-object v1, v1

    .line 484908
    iget v3, p0, LX/30Z;->E:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/30Z;->E:I

    goto :goto_0

    .line 484909
    :cond_4
    iget v3, p0, LX/30Z;->D:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/30Z;->D:I

    iget-object v4, p0, LX/30Z;->t:LX/35c;

    iget v4, v4, LX/35c;->e:I

    if-le v3, v4, :cond_5

    .line 484910
    new-instance v0, LX/EjO;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, v1, LX/Eju;->a:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, LX/EjO;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/EjN;->REMOVE:LX/EjN;

    invoke-virtual {v3}, LX/EjN;->toString()Ljava/lang/String;

    move-result-object v3

    .line 484911
    iput-object v3, v0, LX/EjO;->g:Ljava/lang/String;

    .line 484912
    move-object v0, v0

    .line 484913
    sget-object v3, LX/Ejt;->REMOVE:LX/Ejt;

    .line 484914
    iput-object v3, v1, LX/Eju;->c:LX/Ejt;

    .line 484915
    iget v3, p0, LX/30Z;->F:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/30Z;->F:I

    goto/16 :goto_0

    .line 484916
    :cond_5
    invoke-virtual {v0}, LX/EjO;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, LX/Eju;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 484917
    sget-object v1, LX/EjN;->UPDATE:LX/EjN;

    invoke-virtual {v1}, LX/EjN;->toString()Ljava/lang/String;

    move-result-object v1

    .line 484918
    iput-object v1, v0, LX/EjO;->g:Ljava/lang/String;

    .line 484919
    new-instance v1, LX/Eju;

    invoke-virtual {v0}, LX/EjO;->a()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, LX/EjO;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v4, v5, v3}, LX/Eju;-><init>(JLjava/lang/String;)V

    sget-object v3, LX/Ejt;->UPDATE:LX/Ejt;

    .line 484920
    iput-object v3, v1, LX/Eju;->c:LX/Ejt;

    .line 484921
    move-object v1, v1

    .line 484922
    iget v3, p0, LX/30Z;->G:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/30Z;->G:I

    goto/16 :goto_0
.end method

.method public static a$redex0(LX/30Z;LX/EjI;Ljava/lang/Boolean;Z)V
    .locals 3

    .prologue
    .line 484731
    iget-object v0, p0, LX/30Z;->w:Ljava/util/Set;

    iget v1, p1, LX/EjI;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 484732
    iget-object v0, p0, LX/30Z;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget-object v1, p0, LX/30Z;->t:LX/35c;

    iget v1, v1, LX/35c;->d:I

    if-ge v0, v1, :cond_1

    iget-object v0, p0, LX/30Z;->x:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 484733
    iget-object v0, p0, LX/30Z;->x:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EjI;

    .line 484734
    iget-object v1, p0, LX/30Z;->w:Ljava/util/Set;

    iget v2, v0, LX/EjI;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 484735
    invoke-static {p0, v0, p3}, LX/30Z;->a$redex0(LX/30Z;LX/EjI;Z)V

    .line 484736
    :cond_0
    :goto_0
    return-void

    .line 484737
    :cond_1
    iget-boolean v0, p0, LX/30Z;->y:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/30Z;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/30Z;->x:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 484738
    if-eqz v0, :cond_0

    .line 484739
    invoke-direct {p0, p2, p3}, LX/30Z;->a(Ljava/lang/Boolean;Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a$redex0(LX/30Z;LX/EjI;Z)V
    .locals 10

    .prologue
    .line 484702
    new-instance v0, LX/4Df;

    invoke-direct {v0}, LX/4Df;-><init>()V

    iget-object v1, p0, LX/30Z;->u:Ljava/lang/String;

    .line 484703
    const-string v2, "session_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 484704
    move-object v0, v0

    .line 484705
    iget v1, p1, LX/EjI;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 484706
    const-string v2, "batch_index"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 484707
    move-object v0, v0

    .line 484708
    iget-object v1, p1, LX/EjI;->b:Ljava/util/List;

    invoke-static {v1}, LX/EjO;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 484709
    const-string v2, "contacts"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 484710
    move-object v0, v0

    .line 484711
    new-instance v1, LX/EjQ;

    invoke-direct {v1}, LX/EjQ;-><init>()V

    move-object v1, v1

    .line 484712
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 484713
    new-instance v0, LX/EjS;

    invoke-direct {v0}, LX/EjS;-><init>()V

    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v9, 0x0

    .line 484714
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 484715
    iget-object v4, v0, LX/EjS;->b:LX/0Px;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 484716
    iget-object v6, v0, LX/EjS;->c:LX/2uF;

    invoke-static {v6, v3}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v6

    .line 484717
    const/4 v8, 0x3

    invoke-virtual {v3, v8}, LX/186;->c(I)V

    .line 484718
    iget v8, v0, LX/EjS;->a:I

    invoke-virtual {v3, v9, v8, v9}, LX/186;->a(III)V

    .line 484719
    invoke-virtual {v3, v7, v4}, LX/186;->b(II)V

    .line 484720
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 484721
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 484722
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 484723
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 484724
    invoke-virtual {v4, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 484725
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 484726
    new-instance v4, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;

    invoke-direct {v4, v3}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;-><init>(LX/15i;)V

    .line 484727
    move-object v0, v4

    .line 484728
    iget-object v2, p0, LX/30Z;->o:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 484729
    new-instance v1, LX/EjG;

    invoke-direct {v1, p0, p2, p1}, LX/EjG;-><init>(LX/30Z;ZLX/EjI;)V

    iget-object v2, p0, LX/30Z;->m:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 484730
    return-void
.end method

.method private static b(LX/0QB;)LX/30Z;
    .locals 16

    .prologue
    .line 484700
    new-instance v0, LX/30Z;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/30d;->a(LX/0QB;)LX/30d;

    move-result-object v2

    check-cast v2, LX/30d;

    invoke-static/range {p0 .. p0}, LX/30e;->a(LX/0QB;)LX/30e;

    move-result-object v3

    check-cast v3, LX/30e;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/2Is;->a(LX/0QB;)LX/2Is;

    move-result-object v5

    check-cast v5, LX/2Is;

    invoke-static/range {p0 .. p0}, LX/2J1;->a(LX/0QB;)LX/2J1;

    move-result-object v6

    check-cast v6, LX/2J1;

    invoke-static/range {p0 .. p0}, LX/2JC;->a(LX/0QB;)LX/2JC;

    move-result-object v7

    check-cast v7, LX/2JC;

    invoke-static/range {p0 .. p0}, LX/1wU;->a(LX/0QB;)LX/1wU;

    move-result-object v8

    check-cast v8, LX/1wU;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0e7;->a(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v13

    check-cast v13, Landroid/telephony/TelephonyManager;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v14

    check-cast v14, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/0dB;->a(LX/0QB;)LX/0dC;

    move-result-object v15

    check-cast v15, LX/0dC;

    invoke-direct/range {v0 .. v15}, LX/30Z;-><init>(LX/0Sh;LX/30d;LX/30e;LX/0SG;LX/2Is;LX/2J1;LX/2JC;LX/1wU;Ljava/util/concurrent/ExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0tX;LX/0ad;Landroid/telephony/TelephonyManager;LX/0kL;LX/0dC;)V

    .line 484701
    return-object v0
.end method

.method private d()I
    .locals 3

    .prologue
    .line 484562
    iget-object v0, p0, LX/30Z;->p:LX/0ad;

    sget v1, LX/94h;->a:I

    iget-object v2, p0, LX/30Z;->t:LX/35c;

    iget v2, v2, LX/35c;->b:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;ZLjava/lang/String;Ljava/util/List;I)V
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/Eju;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 484656
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 484657
    if-eqz v0, :cond_2

    .line 484658
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 484659
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 484660
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_4

    .line 484661
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 484662
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 484663
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_7

    .line 484664
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 484665
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/30Z;->u:Ljava/lang/String;

    .line 484666
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 484667
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 484668
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 484669
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v1, v0}, LX/35c;->a(LX/15i;I)LX/35c;

    move-result-object v0

    iput-object v0, p0, LX/30Z;->t:LX/35c;

    .line 484670
    if-nez p2, :cond_6

    .line 484671
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 484672
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    move-result v2

    .line 484673
    if-nez v2, :cond_0

    .line 484674
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 484675
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-direct {p0, v1, v0}, LX/30Z;->a(LX/15i;I)V

    .line 484676
    :cond_0
    iget-object v1, p0, LX/30Z;->g:LX/30d;

    iget-wide v4, p0, LX/30Z;->M:J

    iget-object v0, p0, LX/30Z;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    iget-wide v8, p0, LX/30Z;->O:J

    sub-long/2addr v6, v8

    iget v8, p0, LX/30Z;->N:I

    iget-object v9, p0, LX/30Z;->u:Ljava/lang/String;

    move-object v3, p3

    .line 484677
    iget-object v0, v1, LX/30d;->a:LX/0Zb;

    sget-object p1, LX/Ejs;->CCU_CREATE_SESSION_CHECK_SYNC:LX/Ejs;

    invoke-virtual {p1}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/30d;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p3, "in_sync"

    invoke-virtual {p1, p3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p3, "root_hash"

    invoke-virtual {p1, p3, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p3, "last_upload_success_time"

    invoke-virtual {p1, p3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p3, "time_spent"

    invoke-virtual {p1, p3, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p3, "num_of_retries"

    invoke-virtual {p1, p3, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p3, "ccu_session_id"

    invoke-virtual {p1, p3, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 484678
    if-eqz v2, :cond_5

    const-string v0, "delta_upload_in_sync"

    .line 484679
    :goto_2
    iget-object v1, p0, LX/30Z;->j:LX/2J1;

    invoke-virtual {v1, p4}, LX/2J1;->a(Ljava/util/List;)V

    .line 484680
    iget-object v1, p0, LX/30Z;->g:LX/30d;

    invoke-virtual {v1, v0}, LX/30d;->b(Ljava/lang/String;)V

    .line 484681
    iget-object v1, p0, LX/30Z;->g:LX/30d;

    sget-object v2, LX/Ejs;->CREATE_SESSION_SUCCESS:LX/Ejs;

    invoke-virtual {v2}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/30d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 484682
    invoke-direct {p0, p2}, LX/30Z;->a(Z)V

    .line 484683
    :goto_3
    return-void

    .line 484684
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 484685
    :cond_5
    const-string v0, "delta_upload_out_of_sync"

    goto :goto_2

    .line 484686
    :cond_6
    const-string v1, "full_upload"

    .line 484687
    iget-object v0, p0, LX/30Z;->f:LX/30e;

    invoke-virtual {v0, p5}, LX/30e;->b(I)V

    .line 484688
    iget-object v2, p0, LX/30Z;->f:LX/30e;

    .line 484689
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 484690
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/30e;->a(Ljava/util/List;)V

    .line 484691
    iget-object v2, p0, LX/30Z;->f:LX/30e;

    .line 484692
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 484693
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/30e;->c(Ljava/util/List;)V

    move-object v0, v1

    goto :goto_2

    .line 484694
    :cond_7
    if-eqz p2, :cond_8

    .line 484695
    iget-object v0, p0, LX/30Z;->f:LX/30e;

    invoke-virtual {v0}, LX/30e;->i()V

    .line 484696
    :cond_8
    iget-object v0, p0, LX/30Z;->g:LX/30d;

    const-string v1, "create_session_fail"

    const-string v2, "create session result is null"

    iget-wide v4, p0, LX/30Z;->M:J

    iget-object v3, p0, LX/30Z;->h:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    iget-wide v8, p0, LX/30Z;->O:J

    sub-long/2addr v6, v8

    iget v8, p0, LX/30Z;->N:I

    const/4 v9, 0x0

    move v3, p2

    invoke-virtual/range {v0 .. v9}, LX/30d;->a(Ljava/lang/String;Ljava/lang/String;ZJJILjava/lang/String;)V

    .line 484697
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/30Z;->C:Z

    .line 484698
    iget-object v0, p0, LX/30Z;->z:LX/EjP;

    invoke-virtual {v0}, LX/EjP;->d()V

    .line 484699
    iget-object v0, p0, LX/30Z;->A:LX/Ejw;

    invoke-virtual {v0}, LX/2TZ;->close()V

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ContinuousContactUploadSettingValue;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ContinuousContactUploadSettingSourceValue;
        .end annotation
    .end param

    .prologue
    .line 484654
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/30Z;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 484655
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ContinuousContactUploadSettingValue;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ContinuousContactUploadSettingSourceValue;
        .end annotation
    .end param

    .prologue
    .line 484628
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 484629
    new-instance v0, LX/4Do;

    invoke-direct {v0}, LX/4Do;-><init>()V

    .line 484630
    const-string v1, "status"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 484631
    move-object v0, v0

    .line 484632
    const-string v1, "source"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 484633
    move-object v0, v0

    .line 484634
    iget-object v1, p0, LX/30Z;->s:LX/0dC;

    invoke-virtual {v1}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v1

    .line 484635
    const-string v2, "phone_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 484636
    move-object v0, v0

    .line 484637
    new-instance v1, LX/Ejm;

    invoke-direct {v1}, LX/Ejm;-><init>()V

    move-object v1, v1

    .line 484638
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 484639
    new-instance v0, LX/Ejo;

    invoke-direct {v0}, LX/Ejo;-><init>()V

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 484640
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 484641
    iget-object v4, v0, LX/Ejo;->a:Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 484642
    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 484643
    invoke-virtual {v3, v6, v4}, LX/186;->b(II)V

    .line 484644
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 484645
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 484646
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 484647
    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 484648
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 484649
    new-instance v4, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;

    invoke-direct {v4, v3}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;-><init>(LX/15i;)V

    .line 484650
    move-object v0, v4

    .line 484651
    iget-object v2, p0, LX/30Z;->o:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 484652
    new-instance v1, LX/EjE;

    invoke-direct {v1, p0, p1, p2, p3}, LX/EjE;-><init>(LX/30Z;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v2, p0, LX/30Z;->m:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 484653
    return-void
.end method

.method public final a(Ljava/lang/String;ZI)Z
    .locals 16
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ContactUploadSource;
        .end annotation
    .end param

    .prologue
    .line 484563
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/30Z;->C:Z

    if-eqz v2, :cond_0

    .line 484564
    const/4 v2, 0x1

    .line 484565
    :goto_0
    return v2

    .line 484566
    :cond_0
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/30Z;->C:Z

    .line 484567
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/30Z;->O:J

    .line 484568
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, LX/30Z;->N:I

    .line 484569
    invoke-static/range {p2 .. p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 484570
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->g:LX/30d;

    invoke-virtual {v2}, LX/30d;->a()V

    .line 484571
    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->g:LX/30d;

    if-eqz p2, :cond_4

    const-string v2, "full_upload"

    :goto_1
    invoke-virtual {v3, v2}, LX/30d;->b(Ljava/lang/String;)V

    .line 484572
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->g:LX/30d;

    sget-object v3, LX/Ejs;->CREATE_SESSION_START:LX/Ejs;

    invoke-virtual {v3}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/30d;->c(Ljava/lang/String;)V

    .line 484573
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/30Z;->v:Ljava/util/List;

    .line 484574
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->i:LX/2Is;

    invoke-virtual {v2}, LX/2Is;->a()LX/EjP;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/30Z;->z:LX/EjP;

    .line 484575
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->k:LX/2JC;

    invoke-virtual {v2}, LX/2JC;->a()LX/Ejw;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/30Z;->A:LX/Ejw;

    .line 484576
    if-eqz p2, :cond_1

    .line 484577
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->f:LX/30e;

    invoke-virtual {v2}, LX/30e;->c()V

    .line 484578
    sget-object v2, LX/35c;->a:LX/35c;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/30Z;->t:LX/35c;

    .line 484579
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->j:LX/2J1;

    invoke-virtual {v2}, LX/2J1;->a()V

    .line 484580
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->z:LX/EjP;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->A:LX/Ejw;

    if-nez v2, :cond_5

    .line 484581
    :cond_2
    if-eqz p2, :cond_3

    .line 484582
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->f:LX/30e;

    invoke-virtual {v2}, LX/30e;->h()V

    .line 484583
    :cond_3
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/30Z;->C:Z

    .line 484584
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 484585
    :cond_4
    const-string v2, "delta_upload"

    goto :goto_1

    .line 484586
    :cond_5
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->D:I

    .line 484587
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->E:I

    .line 484588
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->F:I

    .line 484589
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->G:I

    .line 484590
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->H:I

    .line 484591
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->I:I

    .line 484592
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->J:I

    .line 484593
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->K:I

    .line 484594
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/30Z;->L:I

    .line 484595
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2vf;->a:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, LX/30Z;->M:J

    .line 484596
    invoke-static {}, LX/444;->newBuilder()LX/445;

    move-result-object v2

    sget-object v3, LX/30Z;->a:LX/30b;

    invoke-virtual {v2, v3}, LX/445;->b(LX/30b;)LX/445;

    move-result-object v2

    sget-object v3, LX/30Z;->b:LX/30b;

    invoke-virtual {v2, v3}, LX/445;->a(LX/30b;)LX/445;

    move-result-object v2

    sget-object v3, LX/30Z;->c:Ljava/util/Comparator;

    invoke-virtual {v2, v3}, LX/445;->a(Ljava/util/Comparator;)LX/445;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->z:LX/EjP;

    invoke-virtual {v2, v3}, LX/445;->a(Ljava/util/Iterator;)LX/445;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->A:LX/Ejw;

    invoke-virtual {v2, v3}, LX/445;->b(Ljava/util/Iterator;)LX/445;

    move-result-object v2

    invoke-virtual {v2}, LX/445;->a()LX/444;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/30Z;->B:LX/444;

    .line 484597
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2vf;->b:LX/0Tn;

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 484598
    new-instance v2, LX/4Dn;

    invoke-direct {v2}, LX/4Dn;-><init>()V

    invoke-virtual {v2, v11}, LX/4Dn;->f(Ljava/lang/String;)LX/4Dn;

    move-result-object v2

    invoke-virtual {v2, v11}, LX/4Dn;->g(Ljava/lang/String;)LX/4Dn;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/4Dn;->a(Ljava/lang/String;)LX/4Dn;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->s:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4Dn;->b(Ljava/lang/String;)LX/4Dn;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->r:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4Dn;->c(Ljava/lang/String;)LX/4Dn;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->r:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4Dn;->d(Ljava/lang/String;)LX/4Dn;

    move-result-object v12

    .line 484599
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 484600
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v13

    .line 484601
    const/4 v2, 0x0

    .line 484602
    if-eqz p2, :cond_8

    .line 484603
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->f:LX/30e;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/30Z;->z:LX/EjP;

    invoke-virtual {v5}, LX/EjP;->c()I

    move-result v5

    invoke-virtual {v3, v5}, LX/30e;->a(I)V

    .line 484604
    invoke-direct/range {p0 .. p0}, LX/30Z;->d()I

    move-result v5

    .line 484605
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->B:LX/444;

    invoke-virtual {v3}, LX/0Rr;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    add-int/lit8 v3, v2, 0x1

    if-ge v2, v5, :cond_6

    .line 484606
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->B:LX/444;

    invoke-virtual {v2}, LX/0Rr;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/443;

    .line 484607
    move-object/from16 v0, p0

    invoke-static {v0, v2, v4, v13}, LX/30Z;->a(LX/30Z;LX/443;LX/0Pz;LX/0Pz;)Z

    move v2, v3

    .line 484608
    goto :goto_2

    :cond_6
    move v2, v3

    .line 484609
    :cond_7
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 484610
    invoke-static {v3}, LX/EjO;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v12, v3}, LX/4Dn;->a(Ljava/util/List;)LX/4Dn;

    move-result-object v3

    const-string v4, "FULL"

    invoke-virtual {v3, v4}, LX/4Dn;->e(Ljava/lang/String;)LX/4Dn;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4Dn;->b(Ljava/lang/Boolean;)LX/4Dn;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4Dn;->a(Ljava/lang/Boolean;)LX/4Dn;

    .line 484611
    move-object/from16 v0, p0

    iget v3, v0, LX/30Z;->E:I

    move-object/from16 v0, p0

    iput v3, v0, LX/30Z;->H:I

    .line 484612
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, LX/30Z;->E:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v10, v2

    .line 484613
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->g:LX/30d;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->t:LX/35c;

    iget v6, v3, LX/35c;->b:I

    move-object/from16 v0, p0

    iget v7, v0, LX/30Z;->H:I

    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->h:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/30Z;->O:J

    sub-long/2addr v8, v14

    move/from16 v3, p2

    move/from16 v4, p3

    invoke-virtual/range {v2 .. v9}, LX/30d;->a(ZILjava/lang/String;IIJ)V

    .line 484614
    invoke-static {}, LX/Ejd;->a()LX/Ejc;

    move-result-object v2

    .line 484615
    const-string v3, "input"

    invoke-virtual {v2, v3, v12}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 484616
    new-instance v3, LX/Eje;

    invoke-direct {v3}, LX/Eje;-><init>()V

    invoke-virtual {v3}, LX/Eje;->a()Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    move-result-object v3

    .line 484617
    move-object/from16 v0, p0

    iget-object v4, v0, LX/30Z;->o:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 484618
    new-instance v2, LX/EjF;

    move-object/from16 v3, p0

    move/from16 v4, p2

    move-object v5, v11

    move-object v6, v13

    move v7, v10

    invoke-direct/range {v2 .. v7}, LX/EjF;-><init>(LX/30Z;ZLjava/lang/String;LX/0Pz;I)V

    move-object/from16 v0, p0

    iget-object v3, v0, LX/30Z;->m:Ljava/util/concurrent/Executor;

    invoke-static {v8, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 484619
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 484620
    :catch_0
    move-exception v2

    move-object v4, v2

    .line 484621
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->f:LX/30e;

    invoke-virtual {v2}, LX/30e;->i()V

    .line 484622
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->z:LX/EjP;

    invoke-virtual {v2}, LX/EjP;->d()V

    .line 484623
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->A:LX/Ejw;

    invoke-virtual {v2}, LX/2TZ;->close()V

    .line 484624
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/30Z;->C:Z

    .line 484625
    move-object/from16 v0, p0

    iget-object v2, v0, LX/30Z;->g:LX/30d;

    const-string v3, "create_session_process_data_fail"

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/30Z;->M:J

    move-object/from16 v0, p0

    iget-object v8, v0, LX/30Z;->h:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/30Z;->O:J

    sub-long/2addr v8, v10

    move-object/from16 v0, p0

    iget v10, v0, LX/30Z;->N:I

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, LX/30d;->a(Ljava/lang/String;Ljava/lang/String;ZJJILjava/lang/String;)V

    .line 484626
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 484627
    :cond_8
    const-string v3, "DELTA"

    invoke-virtual {v12, v3}, LX/4Dn;->e(Ljava/lang/String;)LX/4Dn;

    move v10, v2

    goto/16 :goto_3
.end method
