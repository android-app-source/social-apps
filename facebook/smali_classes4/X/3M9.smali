.class public final LX/3M9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final mCountryCallingCodeToRegionCodeTable:LX/3MA;

.field public final mRegionCodeToCountryCallingCodeMap:LX/3MB;

.field private mRegionCodeToCountryCallingCodeMapAddendum:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 553527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 553528
    new-instance v0, LX/3MA;

    invoke-direct {v0}, LX/3MA;-><init>()V

    iput-object v0, p0, LX/3M9;->mCountryCallingCodeToRegionCodeTable:LX/3MA;

    .line 553529
    new-instance v0, LX/3MB;

    invoke-direct {v0}, LX/3MB;-><init>()V

    iput-object v0, p0, LX/3M9;->mRegionCodeToCountryCallingCodeMap:LX/3MB;

    .line 553530
    return-void
.end method


# virtual methods
.method public final addRegionCodeToCountryCallingCodeMapping(Ljava/lang/String;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 553564
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    if-ltz p2, :cond_0

    const/16 v1, 0x7fff

    if-le p2, v1, :cond_1

    .line 553565
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 553566
    :cond_1
    const/4 v2, -0x1

    .line 553567
    invoke-virtual {p0, p1, v2}, LX/3M9;->getCountryCallingCodeForRegionCode(Ljava/lang/String;I)I

    move-result v1

    .line 553568
    if-eq v1, v2, :cond_5

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 553569
    if-eqz v1, :cond_2

    .line 553570
    :goto_1
    return v0

    .line 553571
    :cond_2
    monitor-enter p0

    .line 553572
    :try_start_0
    iget-object v1, p0, LX/3M9;->mRegionCodeToCountryCallingCodeMapAddendum:Ljava/util/Map;

    if-nez v1, :cond_3

    .line 553573
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/3M9;->mRegionCodeToCountryCallingCodeMapAddendum:Ljava/util/Map;

    .line 553574
    :cond_3
    iget-object v1, p0, LX/3M9;->mRegionCodeToCountryCallingCodeMapAddendum:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 553575
    monitor-exit p0

    goto :goto_1

    .line 553576
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 553577
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/3M9;->mRegionCodeToCountryCallingCodeMapAddendum:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553578
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 553579
    const/4 v0, 0x1

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getCountryCallingCodeForRegionCode(Ljava/lang/String;I)I
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 553553
    sget-object v1, LX/3MB;->REGION_CODE_KEY_COLUMN:[Ljava/lang/String;

    invoke-static {v1, p1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 553554
    if-gez v1, :cond_0

    const/4 v1, -0x1

    :cond_0
    move v0, v1

    .line 553555
    if-ltz v0, :cond_1

    .line 553556
    sget-object p0, LX/3MB;->COUNTRY_CALLING_CODE_VALUE_COLUMN:[S

    aget-short p0, p0, v0

    move p2, p0

    .line 553557
    :goto_0
    return p2

    .line 553558
    :cond_1
    monitor-enter p0

    .line 553559
    :try_start_0
    iget-object v0, p0, LX/3M9;->mRegionCodeToCountryCallingCodeMapAddendum:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 553560
    iget-object v0, p0, LX/3M9;->mRegionCodeToCountryCallingCodeMapAddendum:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 553561
    if-eqz v0, :cond_2

    .line 553562
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    monitor-exit p0

    goto :goto_0

    .line 553563
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final getRegionCodesForCountryCallingCode(I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 553542
    iget-object v0, p0, LX/3M9;->mCountryCallingCodeToRegionCodeTable:LX/3MA;

    invoke-virtual {v0, p1}, LX/3MA;->firstIndexOfKey(I)I

    move-result v1

    .line 553543
    if-gez v1, :cond_1

    .line 553544
    const/4 v0, 0x0

    .line 553545
    :cond_0
    return-object v0

    .line 553546
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 553547
    sget-object v3, LX/3MA;->COUNTRY_CALLING_CODE_KEY_COLUMN:[S

    array-length v3, v3

    move v2, v3

    .line 553548
    :goto_0
    if-ge v1, v2, :cond_0

    .line 553549
    sget-object v4, LX/3MA;->COUNTRY_CALLING_CODE_KEY_COLUMN:[S

    aget-short v4, v4, v1

    move v3, v4

    .line 553550
    if-ne v3, p1, :cond_0

    .line 553551
    iget-object v3, p0, LX/3M9;->mCountryCallingCodeToRegionCodeTable:LX/3MA;

    invoke-virtual {v3, v1}, LX/3MA;->valueAt(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 553552
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final isValidCountryCallingCode(I)Z
    .locals 2

    .prologue
    .line 553539
    sget-object v1, LX/3MA;->COUNTRY_CALLING_CODE_KEY_COLUMN:[S

    int-to-short p0, p1

    invoke-static {v1, p0}, Ljava/util/Arrays;->binarySearch([SS)I

    move-result v1

    .line 553540
    if-ltz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 553541
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final isValidRegionCode(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 553531
    if-nez p1, :cond_1

    .line 553532
    :cond_0
    :goto_0
    return v0

    .line 553533
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/3M9;->getCountryCallingCodeForRegionCode(Ljava/lang/String;I)I

    move-result v2

    .line 553534
    if-eq v2, v3, :cond_0

    .line 553535
    invoke-virtual {p0, v2}, LX/3M9;->getRegionCodesForCountryCallingCode(I)Ljava/util/List;

    move-result-object v2

    .line 553536
    if-eqz v2, :cond_0

    .line 553537
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v1, :cond_2

    const-string v3, "001"

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    .line 553538
    goto :goto_0
.end method
