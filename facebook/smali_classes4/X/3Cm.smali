.class public LX/3Cm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/String;

.field private static final o:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field private static final p:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

.field private static final q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile r:LX/3Cm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1b4;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1nG;

.field public final d:LX/1Uj;

.field private final e:LX/1zC;

.field private final f:LX/03V;

.field private final g:LX/17Y;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/2lA;

.field public final j:LX/3Cn;

.field public final k:Z

.field public final l:LX/3Cr;

.field public final m:LX/1EV;

.field public final n:LX/0gK;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 530801
    const-class v0, LX/3Cm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Cm;->b:Ljava/lang/String;

    .line 530802
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NOTIFICATION_TARGET:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sput-object v0, LX/3Cm;->o:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 530803
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sput-object v0, LX/3Cm;->p:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 530804
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE_LARGE_IMAGE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/3Cm;->q:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(LX/1nG;LX/1Uj;LX/1zC;LX/17Y;LX/03V;LX/0Or;LX/2lA;LX/3Cn;Ljava/lang/Boolean;LX/3Cr;LX/1EV;LX/0gK;)V
    .locals 1
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/groups/feed/abtest/IsFoFInviteEnabled;
        .end annotation
    .end param
    .param p9    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/work/groups/multicompany/gk/IsWorkMultiCompanyGroupsEnabledGk;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nG;",
            "Lcom/facebook/text/CustomFontUtil;",
            "LX/1zC;",
            "LX/17Y;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2lA;",
            "LX/3Cn;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/notifications/actionlink/ActionLinkUrlMap;",
            "LX/1EV;",
            "LX/0gK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 530787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530788
    iput-object p1, p0, LX/3Cm;->c:LX/1nG;

    .line 530789
    iput-object p2, p0, LX/3Cm;->d:LX/1Uj;

    .line 530790
    iput-object p3, p0, LX/3Cm;->e:LX/1zC;

    .line 530791
    iput-object p4, p0, LX/3Cm;->g:LX/17Y;

    .line 530792
    iput-object p5, p0, LX/3Cm;->f:LX/03V;

    .line 530793
    iput-object p6, p0, LX/3Cm;->h:LX/0Or;

    .line 530794
    iput-object p7, p0, LX/3Cm;->i:LX/2lA;

    .line 530795
    iput-object p8, p0, LX/3Cm;->j:LX/3Cn;

    .line 530796
    invoke-virtual {p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/3Cm;->k:Z

    .line 530797
    iput-object p10, p0, LX/3Cm;->l:LX/3Cr;

    .line 530798
    iput-object p11, p0, LX/3Cm;->m:LX/1EV;

    .line 530799
    iput-object p12, p0, LX/3Cm;->n:LX/0gK;

    .line 530800
    return-void
.end method

.method public static a(LX/0QB;)LX/3Cm;
    .locals 3

    .prologue
    .line 530777
    sget-object v0, LX/3Cm;->r:LX/3Cm;

    if-nez v0, :cond_1

    .line 530778
    const-class v1, LX/3Cm;

    monitor-enter v1

    .line 530779
    :try_start_0
    sget-object v0, LX/3Cm;->r:LX/3Cm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 530780
    if-eqz v2, :cond_0

    .line 530781
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/3Cm;->b(LX/0QB;)LX/3Cm;

    move-result-object v0

    sput-object v0, LX/3Cm;->r:LX/3Cm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530782
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 530783
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 530784
    :cond_1
    sget-object v0, LX/3Cm;->r:LX/3Cm;

    return-object v0

    .line 530785
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 530786
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 530773
    if-nez p0, :cond_1

    .line 530774
    :cond_0
    :goto_0
    return-object v0

    .line 530775
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ba()Ljava/lang/String;

    move-result-object v1

    .line 530776
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/1H1;->d(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 530767
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 530768
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 530769
    if-eqz v1, :cond_1

    const-string v2, "fbrpc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, LX/5O8;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 530770
    :cond_0
    const-string v1, "ref"

    const-string v2, "notifications_view"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/1H1;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 530771
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    .line 530772
    :cond_1
    return-object p0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Landroid/text/style/CharacterStyle;Ljava/lang/String;LX/1yL;)V
    .locals 4

    .prologue
    .line 530760
    :try_start_0
    invoke-static {p2, p3}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v0

    .line 530761
    invoke-static {p1}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v1

    .line 530762
    iget v2, v0, LX/1yN;->a:I

    move v2, v2

    .line 530763
    invoke-virtual {v0}, LX/1yN;->c()I

    move-result v0

    const/16 v3, 0x11

    invoke-virtual {p0, v1, v2, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 530764
    :goto_0
    return-void

    .line 530765
    :catch_0
    move-exception v0

    .line 530766
    const-string v1, "NotificationStoryHelper"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 530503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    .line 530504
    invoke-static {v0}, LX/3Cm;->a(LX/0Px;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 530505
    :cond_0
    :goto_0
    return-void

    .line 530506
    :cond_1
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 530507
    const/4 v4, 0x0

    .line 530508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v3

    .line 530509
    invoke-static {v3}, LX/3Cm;->a(LX/0Px;)Z

    move-result v5

    if-nez v5, :cond_7

    move-object v3, v4

    .line 530510
    :goto_1
    move-object v4, v3

    .line 530511
    if-eqz v4, :cond_6

    .line 530512
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->R()LX/0Px;

    move-result-object v3

    .line 530513
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bF()LX/0Px;

    move-result-object v2

    .line 530514
    :goto_2
    if-eqz v3, :cond_2

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    if-eqz v2, :cond_4

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    const/4 v1, 0x1

    .line 530515
    :cond_4
    if-eqz v1, :cond_0

    .line 530516
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 530517
    const-string v1, "notification_launch_source"

    invoke-virtual {p2, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 530518
    new-instance v1, LX/5QQ;

    invoke-direct {v1}, LX/5QQ;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/5QQ;->a(Ljava/lang/String;)LX/5QQ;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/5QQ;->b(Ljava/lang/String;)LX/5QQ;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/5QQ;->a(LX/0Px;)LX/5QQ;

    move-result-object v0

    .line 530519
    if-eqz v2, :cond_5

    .line 530520
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 530521
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 530522
    iget-object v3, v0, LX/5QQ;->a:Landroid/os/Bundle;

    const-string v4, "group_feed_hoisted_comment_ids"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 530523
    :cond_5
    move-object v0, v0

    .line 530524
    iget-object v1, v0, LX/5QQ;->a:Landroid/os/Bundle;

    move-object v0, v1

    .line 530525
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_6
    move-object v3, v2

    goto :goto_2

    .line 530526
    :cond_7
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 530527
    if-nez v3, :cond_8

    move-object v3, v4

    .line 530528
    goto :goto_1

    .line 530529
    :cond_8
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v3

    const v4, 0x2a27654

    invoke-static {v3, v4}, LX/1VX;->a(Ljava/util/List;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    goto :goto_1
.end method

.method public static a(LX/0Px;)Z
    .locals 2
    .param p0    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 530759
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/3Cm;
    .locals 13

    .prologue
    .line 530805
    new-instance v0, LX/3Cm;

    invoke-static {p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v1

    check-cast v1, LX/1nG;

    invoke-static {p0}, LX/1Uj;->a(LX/0QB;)LX/1Uj;

    move-result-object v2

    check-cast v2, LX/1Uj;

    invoke-static {p0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v3

    check-cast v3, LX/1zC;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17Y;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const/16 v6, 0x14bc

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/2lA;->a(LX/0QB;)LX/2lA;

    move-result-object v7

    check-cast v7, LX/2lA;

    invoke-static {p0}, LX/3Cn;->b(LX/0QB;)LX/3Cn;

    move-result-object v8

    check-cast v8, LX/3Cn;

    invoke-static {p0}, LX/1nN;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-static {p0}, LX/3Cr;->b(LX/0QB;)LX/3Cr;

    move-result-object v10

    check-cast v10, LX/3Cr;

    invoke-static {p0}, LX/1EV;->b(LX/0QB;)LX/1EV;

    move-result-object v11

    check-cast v11, LX/1EV;

    invoke-static {p0}, LX/0gK;->b(LX/0QB;)LX/0gK;

    move-result-object v12

    check-cast v12, LX/0gK;

    invoke-direct/range {v0 .. v12}, LX/3Cm;-><init>(LX/1nG;LX/1Uj;LX/1zC;LX/17Y;LX/03V;LX/0Or;LX/2lA;LX/3Cn;Ljava/lang/Boolean;LX/3Cr;LX/1EV;LX/0gK;)V

    .line 530806
    const/16 v1, 0x52d

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    .line 530807
    iput-object v1, v0, LX/3Cm;->a:LX/0Or;

    .line 530808
    return-object v0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 530752
    invoke-static {p0}, LX/17E;->w(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 530753
    invoke-static {p0}, LX/17E;->w(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 530754
    :goto_0
    return-object v0

    .line 530755
    :cond_0
    invoke-static {p0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 530756
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 530757
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    sget-object v3, LX/3Cm;->o:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 530758
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 530744
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v1

    .line 530745
    :goto_0
    return-object v0

    .line 530746
    :cond_1
    invoke-static {p2}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 530747
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 530748
    invoke-virtual {p0, p2}, LX/3Cm;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 530749
    :cond_2
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 530750
    invoke-virtual {p0, p2}, LX/3Cm;->c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 530751
    :cond_3
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v0, v1

    goto :goto_0

    :cond_4
    iget-object v1, p0, LX/3Cm;->g:LX/17Y;

    invoke-interface {v1, p1, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;Landroid/widget/TextView;)Landroid/text/Spannable;
    .locals 7
    .param p3    # Landroid/widget/TextView;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 530718
    invoke-virtual {p0, p1, p2}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 530719
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 530720
    :cond_0
    iget-object v0, p0, LX/3Cm;->f:LX/03V;

    const-string v1, "notification"

    const-string v2, "Could not format notification text since it was null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 530721
    new-instance v0, Landroid/text/SpannableStringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 530722
    :cond_1
    :goto_0
    return-object v0

    .line 530723
    :cond_2
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 530724
    if-eqz p3, :cond_3

    .line 530725
    iget-object v2, p0, LX/3Cm;->e:LX/1zC;

    invoke-virtual {p3}, Landroid/widget/TextView;->getTextSize()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v0, v3}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 530726
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 530727
    iget-object v2, p0, LX/3Cm;->d:LX/1Uj;

    invoke-virtual {v2}, LX/1Uj;->a()Landroid/text/style/MetricAffectingSpan;

    move-result-object v2

    move-object v2, v2

    .line 530728
    iget-object v3, p0, LX/3Cm;->d:LX/1Uj;

    invoke-virtual {v3}, LX/1Uj;->b()Landroid/text/style/MetricAffectingSpan;

    move-result-object v3

    move-object v3, v3

    .line 530729
    invoke-static {v3}, Landroid/text/style/CharacterStyle;->wrap(Landroid/text/style/CharacterStyle;)Landroid/text/style/CharacterStyle;

    move-result-object v3

    .line 530730
    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 530731
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 530732
    const/4 v4, 0x0

    .line 530733
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 530734
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    move v5, v4

    :goto_1
    if-ge v5, p1, :cond_4

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 530735
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p2

    invoke-static {v3}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntityAtRange;)LX/1yL;

    move-result-object v3

    invoke-static {v0, v2, p2, v3}, LX/3Cm;->a(Landroid/text/SpannableStringBuilder;Landroid/text/style/CharacterStyle;Ljava/lang/String;LX/1yL;)V

    .line 530736
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    .line 530737
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 530738
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    :goto_2
    if-ge v4, v6, :cond_5

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    .line 530739
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;)LX/1yL;

    move-result-object v3

    invoke-static {v0, v2, p1, v3}, LX/3Cm;->a(Landroid/text/SpannableStringBuilder;Landroid/text/style/CharacterStyle;Ljava/lang/String;LX/1yL;)V

    .line 530740
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 530741
    :cond_5
    iget-boolean v3, p0, LX/3Cm;->k:Z

    if-eqz v3, :cond_6

    .line 530742
    iget-object v3, p0, LX/3Cm;->j:LX/3Cn;

    invoke-virtual {v3, v1, v0}, LX/3Cn;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Landroid/text/SpannableStringBuilder;)V

    .line 530743
    :cond_6
    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 4

    .prologue
    .line 530703
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 530704
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 530705
    const/4 v0, 0x0

    .line 530706
    sget-object v1, LX/3DH;->a:[I

    invoke-virtual {p2}, LX/3DG;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 530707
    :goto_0
    if-nez v0, :cond_0

    .line 530708
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 530709
    :cond_0
    if-nez v0, :cond_2

    .line 530710
    invoke-static {p1}, LX/16y;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 530711
    iget-object v0, p0, LX/3Cm;->f:LX/03V;

    sget-object v1, LX/3Cm;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Notification story text is null for notification ID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", location: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 530712
    :cond_1
    const-string v0, ""

    invoke-static {v0}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 530713
    :cond_2
    return-object v0

    .line 530714
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    .line 530715
    :pswitch_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    .line 530716
    :pswitch_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    .line 530717
    :pswitch_3
    invoke-static {p1}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;)Landroid/text/Spannable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 530702
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;Landroid/widget/TextView;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 9
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 530547
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 530548
    invoke-static {p1}, LX/3Cm;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 530549
    if-nez v1, :cond_1

    .line 530550
    :cond_0
    :goto_0
    return-object v0

    .line 530551
    :cond_1
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 530552
    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 530553
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 530554
    :cond_2
    :goto_1
    move-object v0, v3

    .line 530555
    :cond_3
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 530556
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 530557
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 530558
    const-string v2, "notification"

    .line 530559
    if-nez v0, :cond_20

    .line 530560
    :cond_4
    :goto_2
    move-object v0, v0

    .line 530561
    invoke-static {v0}, LX/3Cm;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 530562
    :cond_5
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 530563
    iget-object v0, p0, LX/3Cm;->c:LX/1nG;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-static {v2}, LX/2yc;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1nG;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;)Ljava/lang/String;

    move-result-object v0

    .line 530564
    invoke-static {v0}, LX/5O8;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 530565
    invoke-static {v0}, LX/3Cm;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 530566
    :cond_6
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 530567
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    .line 530568
    :cond_7
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 530569
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ba()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 530570
    :cond_8
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 530571
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 530572
    iget-object v2, p0, LX/3Cm;->l:LX/3Cr;

    const/4 v6, 0x0

    .line 530573
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    if-nez v8, :cond_18

    .line 530574
    :cond_9
    :goto_3
    move-object v2, v6

    .line 530575
    if-nez v2, :cond_a

    move-object v2, v3

    .line 530576
    :goto_4
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_b

    move-object v3, v2

    .line 530577
    goto/16 :goto_1

    .line 530578
    :cond_a
    invoke-interface {v2, v0, v1}, LX/BAH;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 530579
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_1

    .line 530580
    :sswitch_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    .line 530581
    sget-object v2, LX/0ax;->hm:Ljava/lang/String;

    const-string v3, "article="

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x8

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530582
    :sswitch_1
    iget-object v2, p0, LX/3Cm;->n:LX/0gK;

    invoke-virtual {v2}, LX/0gK;->a()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bA()Ljava/lang/String;

    move-result-object v2

    .line 530583
    :goto_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bm()Ljava/lang/String;

    move-result-object v0

    .line 530584
    sget-object v3, LX/0ax;->gv:Ljava/lang/String;

    const-string v4, "{id}"

    invoke-virtual {v3, v4, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "{reply_id}"

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530585
    :cond_c
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bn()Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 530586
    :sswitch_2
    sget-object v3, LX/0ax;->eP:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aG()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aG()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v2

    :goto_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aE()Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    move-result-object v4

    if-eqz v4, :cond_e

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aE()Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    move-result-object v0

    :goto_7
    invoke-static {v3, v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    :cond_d
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    goto :goto_6

    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    goto :goto_7

    .line 530587
    :sswitch_3
    sget-object v3, LX/0ax;->ak:Ljava/lang/String;

    goto/16 :goto_1

    .line 530588
    :sswitch_4
    const/4 v5, 0x0

    .line 530589
    sget-object v2, LX/0ax;->dX:Ljava/lang/String;

    const-string v3, "notification"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 530590
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_19

    :cond_f
    move-object v2, v3

    .line 530591
    :goto_8
    move-object v3, v2

    .line 530592
    goto/16 :goto_1

    .line 530593
    :sswitch_5
    sget-object v0, LX/0ax;->fz:Ljava/lang/String;

    const-string v2, "notification"

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530594
    :sswitch_6
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 530595
    sget-object v0, LX/0ax;->gM:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    const-string v3, "notification"

    invoke-static {v0, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530596
    :sswitch_7
    sget-object v2, LX/0ax;->fn:Ljava/lang/String;

    .line 530597
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_17

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_17

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_17

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_17

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 530598
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Ph;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    .line 530599
    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 530600
    :goto_9
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "source"

    const-string v3, "notification"

    invoke-static {v0, v2, v3}, LX/1H1;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530601
    :sswitch_8
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 530602
    if-eqz v2, :cond_10

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 530603
    :goto_a
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v4, 0x41e065f

    if-ne v0, v4, :cond_2

    .line 530604
    iget-object v0, p0, LX/3Cm;->m:LX/1EV;

    invoke-virtual {v0}, LX/1EV;->a()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 530605
    sget-object v0, LX/0ax;->gd:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    :cond_10
    move-object v0, v3

    .line 530606
    goto :goto_a

    .line 530607
    :cond_11
    sget-object v0, LX/0ax;->ab:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530608
    :sswitch_9
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 530609
    sget-object v0, LX/0ax;->eI:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530610
    :sswitch_a
    iget-object v2, p0, LX/3Cm;->h:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 530611
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    .line 530612
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 530613
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "group_id"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 530614
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, LX/0ax;->C:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530615
    :sswitch_b
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    .line 530616
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 530617
    sget-object v2, LX/0ax;->C:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530618
    :sswitch_c
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->j()Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 530619
    sget-object v0, LX/0ax;->cs:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530620
    :sswitch_d
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 530621
    sget-object v2, LX/0ax;->cD:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530622
    :sswitch_e
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 530623
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroup;->p()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v2

    if-eqz v2, :cond_13

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroup;->p()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_13

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroup;->p()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_13

    .line 530624
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroup;->p()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_b
    if-ge v5, v4, :cond_13

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLConfiguration;

    .line 530625
    if-eqz v2, :cond_12

    const-string v6, "gk_groups_flagged_reported_post_queue"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLConfiguration;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLConfiguration;->a()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 530626
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v0

    .line 530627
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://m.facebook.com/groups/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "?view=flaggedreportedposts"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 530628
    goto/16 :goto_1

    .line 530629
    :cond_12
    add-int/lit8 v5, v5, 0x1

    goto :goto_b

    .line 530630
    :cond_13
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v0

    .line 530631
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://m.facebook.com/groups/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "?view=reported"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 530632
    goto/16 :goto_1

    .line 530633
    :sswitch_f
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 530634
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v0

    .line 530635
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://m.facebook.com/groups/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "?view=pending"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 530636
    goto/16 :goto_1

    .line 530637
    :sswitch_10
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 530638
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->O()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v0

    .line 530639
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://m.facebook.com/groups/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "?view=requests"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 530640
    goto/16 :goto_1

    .line 530641
    :sswitch_11
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 530642
    sget-object v0, LX/0ax;->aN:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530643
    :sswitch_12
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530644
    :sswitch_13
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 530645
    sget-object v2, LX/0ax;->B:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530646
    :sswitch_14
    iget-object v2, p0, LX/3Cm;->i:LX/2lA;

    .line 530647
    invoke-static {v2}, LX/2lA;->g(LX/2lA;)Z

    move-result v4

    if-nez v4, :cond_1f

    .line 530648
    const/4 v4, 0x0

    .line 530649
    :goto_c
    move v2, v4

    .line 530650
    if-eqz v2, :cond_2

    .line 530651
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530652
    :sswitch_15
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 530653
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 530654
    if-eqz v2, :cond_14

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_14

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLACE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v2, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    move v2, v4

    .line 530655
    :goto_d
    if-eqz v2, :cond_15

    .line 530656
    sget-object v2, LX/0ax;->bz:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    :cond_14
    move v2, v5

    .line 530657
    goto :goto_d

    .line 530658
    :cond_15
    const-string v2, "[]"

    .line 530659
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v3

    if-eqz v3, :cond_16

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_16

    .line 530660
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bf()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;->a()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/47C;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 530661
    :cond_16
    sget-object v3, LX/0ax;->iC:Ljava/lang/String;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v5

    const-string v0, "notification"

    aput-object v0, v6, v4

    aput-object v2, v6, v7

    invoke-static {v3, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530662
    :sswitch_16
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 530663
    sget-object v0, LX/0ax;->eY:Ljava/lang/String;

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 530664
    :sswitch_17
    sget-object v0, LX/0ax;->gh:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/Object;

    const-string v3, "notification"

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    :cond_17
    move-object v0, v2

    goto/16 :goto_9

    .line 530665
    :cond_18
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v8

    sparse-switch v8, :sswitch_data_1

    goto/16 :goto_3

    .line 530666
    :sswitch_18
    iget-object v6, v2, LX/3Cr;->d:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BAH;

    goto/16 :goto_3

    .line 530667
    :sswitch_19
    iget-object v6, v2, LX/3Cr;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BAH;

    goto/16 :goto_3

    .line 530668
    :sswitch_1a
    iget-object v6, v2, LX/3Cr;->b:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BAH;

    goto/16 :goto_3

    .line 530669
    :sswitch_1b
    iget-object v6, v2, LX/3Cr;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BAH;

    goto/16 :goto_3

    .line 530670
    :sswitch_1c
    iget-object v6, v2, LX/3Cr;->e:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BAH;

    goto/16 :goto_3

    .line 530671
    :sswitch_1d
    iget-object v6, v2, LX/3Cr;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BAH;

    goto/16 :goto_3

    .line 530672
    :sswitch_1e
    iget-object v6, v2, LX/3Cr;->g:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BAH;

    goto/16 :goto_3

    .line 530673
    :sswitch_1f
    iget-object v6, v2, LX/3Cr;->h:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BAH;

    goto/16 :goto_3

    .line 530674
    :sswitch_20
    iget-object v6, v2, LX/3Cr;->i:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BAH;

    goto/16 :goto_3

    .line 530675
    :cond_19
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v6

    .line 530676
    if-nez v6, :cond_1a

    move-object v2, v3

    .line 530677
    goto/16 :goto_8

    .line 530678
    :cond_1a
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 530679
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    move v4, v5

    :goto_e
    if-ge v4, v8, :cond_1c

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 530680
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 530681
    if-eqz v2, :cond_1b

    .line 530682
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 530683
    if-eqz v2, :cond_1b

    .line 530684
    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 530685
    :cond_1b
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_e

    .line 530686
    :cond_1c
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 530687
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1e

    .line 530688
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x41e86a4

    if-ne v3, v4, :cond_1d

    sget-object v3, LX/0ax;->ea:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "notification:"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const-string v5, "invite_notification"

    invoke-static {v3, v2, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    :cond_1d
    sget-object v3, LX/0ax;->dY:Ljava/lang/String;

    invoke-static {v2}, LX/47C;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "notification"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bt()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v2, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    :cond_1e
    move-object v2, v3

    .line 530689
    goto/16 :goto_8

    :cond_1f
    const/4 v4, 0x1

    goto/16 :goto_c

    .line 530690
    :cond_20
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 530691
    invoke-virtual {v3}, Landroid/net/Uri;->isHierarchical()Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "fbrpc"

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 530692
    const-string v4, "market_uri"

    invoke-virtual {v3, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 530693
    if-eqz v4, :cond_4

    .line 530694
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 530695
    invoke-virtual {v4}, Landroid/net/Uri;->isHierarchical()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 530696
    const-string v5, "referrer"

    invoke-virtual {v4, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 530697
    if-eqz v5, :cond_4

    .line 530698
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&fb_source="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 530699
    const-string v6, "referrer"

    invoke-static {v4, v6, v5}, LX/1H1;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 530700
    const-string v5, "market_uri"

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v5, v4}, LX/1H1;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 530701
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6a91d325 -> :sswitch_0
        -0x6355565a -> :sswitch_6
        -0x3aa2b488 -> :sswitch_a
        -0x35379ed7 -> :sswitch_f
        -0x338a76f9 -> :sswitch_1
        -0x206c147d -> :sswitch_d
        -0x11f22ab2 -> :sswitch_5
        -0x10514377 -> :sswitch_7
        -0x55b63fa -> :sswitch_c
        0x2a27654 -> :sswitch_b
        0x41e86a4 -> :sswitch_4
        0x1884e273 -> :sswitch_12
        0x1b069610 -> :sswitch_11
        0x1c9fe8cf -> :sswitch_e
        0x311b6b64 -> :sswitch_9
        0x541539be -> :sswitch_15
        0x578ec37f -> :sswitch_16
        0x58950000 -> :sswitch_14
        0x5a1cd4ef -> :sswitch_13
        0x5fefdd9e -> :sswitch_17
        0x64a42027 -> :sswitch_2
        0x6acb18bf -> :sswitch_8
        0x7456e83b -> :sswitch_10
        0x7b695f65 -> :sswitch_3
        0x7b878d4d -> :sswitch_4
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x6c1fe582 -> :sswitch_18
        -0x542a5e8b -> :sswitch_1b
        -0x45ccdb7e -> :sswitch_20
        -0xa8a0985 -> :sswitch_1c
        0xf0b12c7 -> :sswitch_1d
        0x1cf9d8ef -> :sswitch_1f
        0x3961c2df -> :sswitch_1a
        0x503cf8a4 -> :sswitch_19
        0x565a1ae9 -> :sswitch_1e
    .end sparse-switch
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 530530
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 530531
    :goto_0
    return-object v0

    .line 530532
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    .line 530533
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v1

    .line 530534
    goto :goto_0

    .line 530535
    :cond_2
    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 530536
    goto :goto_0

    .line 530537
    :cond_3
    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 530538
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v1

    .line 530539
    goto :goto_0

    .line 530540
    :cond_4
    iget-object v0, p0, LX/3Cm;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1b4;

    .line 530541
    invoke-virtual {v0}, LX/1b4;->h()Z

    move-result v3

    if-nez v3, :cond_5

    move-object v0, v1

    .line 530542
    goto :goto_0

    .line 530543
    :cond_5
    iget-object v3, v0, LX/1b4;->a:LX/0ad;

    sget-short v5, LX/1v6;->N:S

    const/4 p0, 0x0

    invoke-interface {v3, v5, p0}, LX/0ad;->a(SZ)Z

    move-result v3

    move v0, v3

    .line 530544
    if-eqz v0, :cond_6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v1

    .line 530545
    goto :goto_0

    .line 530546
    :cond_6
    sget-object v0, LX/0ax;->eY:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
