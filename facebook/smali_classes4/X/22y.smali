.class public final LX/22y;
.super LX/22z;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/api/feed/FetchFeedParams;

.field public final synthetic b:LX/0gf;

.field public final synthetic c:Z

.field public final synthetic d:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic e:LX/0rS;

.field public final synthetic f:Lcom/facebook/feed/data/FeedDataLoader;

.field private g:I

.field private h:Z

.field private i:Z

.field private final j:I

.field private final k:LX/0gf;

.field private final l:J

.field private m:J


# direct methods
.method public constructor <init>(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedParams;LX/0gf;ZLcom/google/common/util/concurrent/SettableFuture;LX/0rS;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 361387
    iput-object p1, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iput-object p2, p0, LX/22y;->a:Lcom/facebook/api/feed/FetchFeedParams;

    iput-object p3, p0, LX/22y;->b:LX/0gf;

    iput-boolean p4, p0, LX/22y;->c:Z

    iput-object p5, p0, LX/22y;->d:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p6, p0, LX/22y;->e:LX/0rS;

    invoke-direct {p0}, LX/22z;-><init>()V

    .line 361388
    iput v0, p0, LX/22y;->g:I

    .line 361389
    iput-boolean v0, p0, LX/22y;->h:Z

    .line 361390
    iput-boolean v0, p0, LX/22y;->i:Z

    .line 361391
    iget-object v0, p0, LX/22y;->a:Lcom/facebook/api/feed/FetchFeedParams;

    if-nez v0, :cond_0

    const/16 v0, 0xa

    :goto_0
    iput v0, p0, LX/22y;->j:I

    .line 361392
    iget-object v0, p0, LX/22y;->b:LX/0gf;

    iput-object v0, p0, LX/22y;->k:LX/0gf;

    .line 361393
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/22y;->l:J

    .line 361394
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/22y;->m:J

    return-void

    .line 361395
    :cond_0
    iget-object v0, p0, LX/22y;->a:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361396
    iget v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v0, v1

    .line 361397
    goto :goto_0
.end method

.method private a(Z)V
    .locals 10

    .prologue
    .line 361398
    iget-object v0, p0, LX/22y;->k:LX/0gf;

    invoke-virtual {v0}, LX/0gf;->isManual()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 361399
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/22y;->l:J

    sub-long v2, v0, v2

    .line 361400
    iget-wide v0, p0, LX/22y;->m:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 361401
    iput-wide v2, p0, LX/22y;->m:J

    .line 361402
    :cond_0
    new-instance v0, LX/AkT;

    iget-object v1, p0, LX/22y;->k:LX/0gf;

    iget-wide v4, p0, LX/22y;->m:J

    iget-object v6, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-wide v6, v6, Lcom/facebook/feed/data/FeedDataLoader;->au:J

    .line 361403
    const/4 v8, 0x1

    move v9, v8

    .line 361404
    move v8, p1

    invoke-direct/range {v0 .. v9}, LX/AkT;-><init>(LX/0gf;JJJZZ)V

    .line 361405
    iget-object v1, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v1, v1, Lcom/facebook/feed/data/FeedDataLoader;->an:LX/0qa;

    invoke-virtual {v1, v0}, LX/0qa;->a(LX/AkT;)V

    .line 361406
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 361407
    new-instance v1, Lcom/facebook/api/feed/FetchFeedResultCount;

    iget-object v0, p0, LX/22y;->a:Lcom/facebook/api/feed/FetchFeedParams;

    iget v2, p0, LX/22y;->g:I

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feed/FetchFeedResultCount;-><init>(Lcom/facebook/api/feed/FetchFeedParams;I)V

    .line 361408
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->Z:LX/0pl;

    invoke-virtual {v0}, LX/0pl;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, LX/1lq;->ShowNewStoryPill:LX/1lq;

    .line 361409
    :goto_0
    iget-object v2, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v2, v2, LX/0gD;->m:LX/0qk;

    iget v3, p0, LX/22y;->g:I

    iget-object v4, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v4, v4, LX/0gD;->b:LX/0fz;

    invoke-virtual {v4}, LX/0fz;->i()I

    move-result v4

    sget-object v5, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v2, v3, v0, v4, v5}, LX/0qk;->a(ILX/1lq;ILX/0ta;)V

    .line 361410
    iput-boolean v6, p0, LX/22y;->h:Z

    .line 361411
    invoke-direct {p0, v7}, LX/22y;->a(Z)V

    .line 361412
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-boolean v2, p0, LX/22y;->c:Z

    invoke-static {v0, v2, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;ZLcom/facebook/api/feed/FetchFeedResultCount;)V

    .line 361413
    iget-object v0, p0, LX/22y;->d:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, -0x2b0d4062

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 361414
    iget-object v0, p0, LX/22y;->a:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361415
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v1

    .line 361416
    sget-object v1, LX/0gf;->INITIALIZATION:LX/0gf;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->Z:LX/0pl;

    invoke-virtual {v0}, LX/0pl;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361417
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->T:LX/0pW;

    .line 361418
    iget-object v1, v0, LX/0pW;->a:LX/0Yi;

    .line 361419
    sget-object v0, LX/0ao;->ASYNC_FRESH_FAST_NETWORK_FETCH:LX/0ao;

    invoke-static {v1, v0}, LX/0Yi;->a(LX/0Yi;LX/0ao;)V

    .line 361420
    :cond_0
    iget v0, p0, LX/22y;->g:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_5

    iget-object v0, p0, LX/22y;->a:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361421
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v1

    .line 361422
    sget-object v1, LX/0gf;->INITIALIZATION:LX/0gf;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-boolean v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->x:Z

    if-eqz v0, :cond_5

    .line 361423
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    invoke-virtual {v0}, Lcom/facebook/feed/data/FeedDataLoader;->b()V

    .line 361424
    :cond_1
    :goto_1
    iget-boolean v0, p0, LX/22y;->c:Z

    if-eqz v0, :cond_2

    iget v0, p0, LX/22y;->g:I

    if-nez v0, :cond_2

    .line 361425
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v2, LX/0gf;->AUTO_REFRESH:LX/0gf;

    invoke-virtual {v0, v1, v2, v6}, Lcom/facebook/feed/data/FeedDataLoader;->a(LX/0rS;LX/0gf;Z)LX/0uO;

    .line 361426
    :cond_2
    iget-boolean v0, p0, LX/22y;->i:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->ae:LX/0ad;

    sget-short v1, LX/0fe;->ai:S

    invoke-interface {v0, v1, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 361427
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->al:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rW;

    iget-object v1, p0, LX/22y;->a:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361428
    iget-object v2, v1, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    move-object v1, v2

    .line 361429
    iget-object v2, p0, LX/22y;->a:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361430
    iget-object v3, v2, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    move-object v2, v3

    .line 361431
    invoke-virtual {v0, v1, v2}, LX/0rW;->a(LX/0Px;LX/0Px;)V

    .line 361432
    :cond_3
    return-void

    .line 361433
    :cond_4
    sget-object v0, LX/1lq;->AvoidNewStoryPill:LX/1lq;

    goto/16 :goto_0

    .line 361434
    :cond_5
    iget v0, p0, LX/22y;->g:I

    if-nez v0, :cond_1

    iget-object v0, p0, LX/22y;->a:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361435
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v1

    .line 361436
    sget-object v1, LX/0gf;->INITIALIZATION:LX/0gf;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, LX/22y;->a:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361437
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v1

    .line 361438
    sget-object v1, LX/0gf;->INITIALIZATION_RERANK:LX/0gf;

    if-ne v0, v1, :cond_1

    .line 361439
    :cond_6
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->ab:LX/0Zb;

    const-string v1, "android_no_available_stories"

    invoke-interface {v0, v1, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 361440
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 361441
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 361442
    check-cast p1, Lcom/facebook/api/feed/FetchFeedResult;

    const/4 v3, 0x1

    .line 361443
    iput-boolean v3, p0, LX/22y;->h:Z

    .line 361444
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Freshness:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/22y;->e:LX/0rS;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 361445
    iget-object v1, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v2, LX/0rj;->HEAD_FETCH_CHUNKED_SUCCEED:LX/0rj;

    invoke-virtual {v1, v2, v0}, LX/0gD;->a(LX/0rj;Ljava/lang/String;)V

    .line 361446
    iget v0, p0, LX/22y;->g:I

    if-nez v0, :cond_0

    .line 361447
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v4, p0, LX/22y;->l:J

    sub-long/2addr v0, v4

    iput-wide v0, p0, LX/22y;->m:J

    .line 361448
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->ar:LX/0qf;

    invoke-virtual {v0, p1}, LX/0qf;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 361449
    :cond_0
    iget v0, p0, LX/22y;->g:I

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/22y;->g:I

    .line 361450
    iget-boolean v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    move v0, v0

    .line 361451
    iput-boolean v0, p0, LX/22y;->i:Z

    .line 361452
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v2, LX/23E;->BEFORE:LX/23E;

    iget-object v1, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v1, v1, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    move-object v1, p1

    .line 361453
    invoke-static/range {v0 .. v5}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedResult;LX/23E;ZJ)V

    .line 361454
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    .line 361455
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/22y;->a(Z)V

    .line 361456
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/0rj;->HEAD_FETCH_FAILED:LX/0rj;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Freshness:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/22y;->e:LX/0rS;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gD;->a(LX/0rj;Ljava/lang/String;)V

    .line 361457
    iget-boolean v0, p0, LX/22y;->h:Z

    if-nez v0, :cond_2

    .line 361458
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->av:LX/22v;

    sget-object v1, LX/22v;->SCHEDULED_RERANKED:LX/22v;

    if-ne v0, v1, :cond_0

    .line 361459
    goto :goto_1

    :cond_0
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget v1, p0, LX/22y;->j:I

    iget-object v2, p0, LX/22y;->a:Lcom/facebook/api/feed/FetchFeedParams;

    .line 361460
    iget-object v3, v2, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v2, v3

    .line 361461
    iget-object v4, v0, Lcom/facebook/feed/data/FeedDataLoader;->av:LX/22v;

    sget-object v5, LX/22v;->SCHEDULED_RERANKED:LX/22v;

    if-eq v4, v5, :cond_3

    .line 361462
    const/4 v4, 0x1

    move v4, v4

    .line 361463
    if-eqz v4, :cond_3

    iget-object v4, v0, Lcom/facebook/feed/data/FeedDataLoader;->B:LX/0gf;

    invoke-virtual {v4}, LX/0gf;->isManual()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v4}, LX/0fz;->x()Z

    move-result v4

    if-nez v4, :cond_3

    .line 361464
    invoke-static {v0}, Lcom/facebook/feed/data/FeedDataLoader;->U(Lcom/facebook/feed/data/FeedDataLoader;)V

    .line 361465
    sget-object v4, LX/0rj;->ON_FAILURE_PTR_RERANKING_START:LX/0rj;

    invoke-virtual {v0, v4}, LX/0gD;->a(LX/0rj;)V

    .line 361466
    invoke-virtual {v0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v4

    sget-object v5, LX/0gf;->RERANK:LX/0gf;

    invoke-virtual {v0}, LX/0gD;->E()J

    move-result-wide v6

    invoke-static {v4, v1, v5, v6, v7}, LX/0qz;->a(Lcom/facebook/api/feedtype/FeedType;ILX/0gf;J)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v4

    .line 361467
    const-wide/16 v6, 0x0

    invoke-static {v0, v4, v6, v7, v2}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedParams;JLX/0gf;)V

    .line 361468
    const/4 v4, 0x1

    .line 361469
    :goto_0
    move v0, v4

    .line 361470
    if-eqz v0, :cond_1

    .line 361471
    :goto_1
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, Lcom/facebook/feed/data/FeedDataLoader;->u:LX/0r8;

    invoke-virtual {v0}, LX/0r8;->b()V

    .line 361472
    :goto_2
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v0, v0, LX/0gD;->m:LX/0qk;

    iget-object v1, p0, LX/22y;->b:LX/0gf;

    invoke-virtual {v0, v1, p1}, LX/0qk;->a(LX/0gf;Ljava/lang/Throwable;)V

    .line 361473
    return-void

    .line 361474
    :cond_1
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    sget-object v1, LX/23E;->BEFORE:LX/23E;

    invoke-static {v0, p1, v1}, Lcom/facebook/feed/data/FeedDataLoader;->a$redex0(Lcom/facebook/feed/data/FeedDataLoader;Ljava/lang/Throwable;LX/23E;)V

    .line 361475
    iget-object v0, p0, LX/22y;->f:Lcom/facebook/feed/data/FeedDataLoader;

    iget-boolean v1, p0, LX/22y;->c:Z

    invoke-static {v0, v1}, Lcom/facebook/feed/data/FeedDataLoader;->b$redex0(Lcom/facebook/feed/data/FeedDataLoader;Z)V

    .line 361476
    iget-object v0, p0, LX/22y;->d:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_2

    .line 361477
    :cond_2
    invoke-virtual {p0}, LX/22y;->a()V

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method
