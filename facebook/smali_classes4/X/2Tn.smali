.class public final LX/2Tn;
.super LX/0aB;
.source ""


# instance fields
.field public final synthetic a:LX/2Tm;


# direct methods
.method public constructor <init>(LX/2Tm;)V
    .locals 0

    .prologue
    .line 414752
    iput-object p1, p0, LX/2Tn;->a:LX/2Tm;

    invoke-direct {p0}, LX/0aB;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Uh;I)V
    .locals 7

    .prologue
    .line 414753
    iget-object v0, p0, LX/2Tn;->a:LX/2Tm;

    iget-object v0, v0, LX/2Tm;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 414754
    iget-object v2, p0, LX/2Tn;->a:LX/2Tm;

    iget-wide v2, v2, LX/2Tm;->u:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 414755
    :goto_0
    return-void

    .line 414756
    :cond_0
    iget-object v2, p0, LX/2Tn;->a:LX/2Tm;

    .line 414757
    iput-wide v0, v2, LX/2Tm;->u:J

    .line 414758
    :try_start_0
    iget-object v0, p0, LX/2Tn;->a:LX/2Tm;

    iget-object v0, v0, LX/2Tm;->g:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;

    move-result-object v1

    .line 414759
    new-instance v0, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 414760
    const-string v2, "endpoint_capabilities"

    iget-object v3, p0, LX/2Tn;->a:LX/2Tm;

    iget-wide v4, v3, LX/2Tm;->u:J

    invoke-virtual {v0, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 414761
    :try_start_1
    const-string v2, "/send_endpoint_capabilities"

    sget-object v3, LX/2I2;->FIRE_AND_FORGET:LX/2I2;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2gV;->a(Ljava/lang/String;LX/0lF;LX/2I2;LX/76H;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414762
    invoke-virtual {v1}, LX/2gV;->f()V

    goto :goto_0

    :catch_0
    goto :goto_0

    :catch_1
    invoke-virtual {v1}, LX/2gV;->f()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0
.end method
