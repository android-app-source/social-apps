.class public LX/38w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/2yJ;

.field private final b:LX/2yN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2yN",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:LX/0ad;

.field public final d:LX/0bH;


# direct methods
.method public constructor <init>(LX/2yJ;LX/0bH;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521740
    iput-object p1, p0, LX/38w;->a:LX/2yJ;

    .line 521741
    iget-object v0, p0, LX/38w;->a:LX/2yJ;

    invoke-virtual {v0}, LX/2yJ;->a()LX/2yN;

    move-result-object v0

    iput-object v0, p0, LX/38w;->b:LX/2yN;

    .line 521742
    iput-object p2, p0, LX/38w;->d:LX/0bH;

    .line 521743
    iput-object p3, p0, LX/38w;->c:LX/0ad;

    .line 521744
    return-void
.end method

.method public static b(LX/0QB;)LX/38w;
    .locals 4

    .prologue
    .line 521745
    new-instance v3, LX/38w;

    invoke-static {p0}, LX/2yJ;->a(LX/0QB;)LX/2yJ;

    move-result-object v0

    check-cast v0, LX/2yJ;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v1

    check-cast v1, LX/0bH;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-direct {v3, v0, v1, v2}, LX/38w;-><init>(LX/2yJ;LX/0bH;LX/0ad;)V

    .line 521746
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 521747
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 521748
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2yg;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v7

    .line 521749
    if-eqz v7, :cond_1

    move-object v0, p2

    check-cast v0, LX/1Pr;

    new-instance v1, LX/2yU;

    invoke-direct {v1, p3}, LX/2yU;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v0, v1}, LX/1Pr;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2yV;

    move-object v5, v0

    :goto_0
    move-object v0, p2

    .line 521750
    check-cast v0, LX/1Pr;

    new-instance v1, LX/2yh;

    invoke-direct {v1, p3}, LX/2yh;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-static {p3}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/2yi;

    .line 521751
    invoke-static {p3}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 521752
    iget-object v1, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 521753
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 521754
    :cond_0
    :goto_1
    iget-object v0, p0, LX/38w;->a:LX/2yJ;

    iget-object v3, p0, LX/38w;->b:LX/2yN;

    move-object v1, p1

    move-object v2, p3

    move-object v4, p4

    move-object v8, p2

    invoke-virtual/range {v0 .. v8}, LX/2yJ;->onClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yN;Ljava/lang/String;LX/2yV;LX/2yi;ZLX/1Pq;)V

    .line 521755
    return-void

    .line 521756
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 521757
    :cond_2
    iget-object v1, p0, LX/38w;->c:LX/0ad;

    sget-short v2, LX/C1Z;->b:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    .line 521758
    iget-object v1, p0, LX/38w;->d:LX/0bH;

    new-instance v2, LX/1Zg;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->EVENT_VIEW_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    invoke-direct {v2, v0, v3}, LX/1Zg;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1
.end method
