.class public LX/2IS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2IS;


# instance fields
.field private final a:LX/2IT;

.field private final b:LX/30H;

.field private final c:LX/0SG;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2IT;LX/30H;LX/0SG;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/assetdownload/IsInAssetDownloadMainGatekeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2IT;",
            "LX/30H;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391684
    iput-object p1, p0, LX/2IS;->a:LX/2IT;

    .line 391685
    iput-object p2, p0, LX/2IS;->b:LX/30H;

    .line 391686
    iput-object p3, p0, LX/2IS;->c:LX/0SG;

    .line 391687
    iput-object p4, p0, LX/2IS;->d:LX/0Or;

    .line 391688
    return-void
.end method

.method public static a(LX/0QB;)LX/2IS;
    .locals 7

    .prologue
    .line 391689
    sget-object v0, LX/2IS;->e:LX/2IS;

    if-nez v0, :cond_1

    .line 391690
    const-class v1, LX/2IS;

    monitor-enter v1

    .line 391691
    :try_start_0
    sget-object v0, LX/2IS;->e:LX/2IS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391692
    if-eqz v2, :cond_0

    .line 391693
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391694
    new-instance v6, LX/2IS;

    invoke-static {v0}, LX/2IT;->b(LX/0QB;)LX/2IT;

    move-result-object v3

    check-cast v3, LX/2IT;

    invoke-static {v0}, LX/30H;->b(LX/0QB;)LX/30H;

    move-result-object v4

    check-cast v4, LX/30H;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const/16 p0, 0x144d

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/2IS;-><init>(LX/2IT;LX/30H;LX/0SG;LX/0Or;)V

    .line 391695
    move-object v0, v6

    .line 391696
    sput-object v0, LX/2IS;->e:LX/2IS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391697
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391698
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391699
    :cond_1
    sget-object v0, LX/2IS;->e:LX/2IS;

    return-object v0

    .line 391700
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391701
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 391702
    iget-object v0, p0, LX/2IS;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 391703
    const/4 v0, 0x0

    .line 391704
    :goto_0
    return-object v0

    .line 391705
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "assetdownload_db_and_fs_stats"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 391706
    const-string v1, "assetdownload"

    .line 391707
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 391708
    const-string v1, "db_configs_total"

    iget-object v2, p0, LX/2IS;->a:LX/2IT;

    .line 391709
    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/2IT;->b(LX/2IT;LX/0ux;)I

    move-result v3

    move v2, v3

    .line 391710
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 391711
    const-string v1, "db_configs_finished"

    iget-object v2, p0, LX/2IS;->a:LX/2IT;

    .line 391712
    sget-object v8, LX/2IX;->f:LX/0U1;

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v8

    invoke-static {v2, v8}, LX/2IT;->b(LX/2IT;LX/0ux;)I

    move-result v8

    move v2, v8

    .line 391713
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 391714
    const-string v1, "db_configs_in_quarantine"

    iget-object v2, p0, LX/2IS;->a:LX/2IT;

    iget-object v3, p0, LX/2IS;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/32 v6, 0xf731400

    sub-long/2addr v4, v6

    iget-object v3, p0, LX/2IS;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    .line 391715
    const/4 v3, 0x2

    new-array v3, v3, [LX/0ux;

    const/4 v8, 0x0

    sget-object v9, LX/2IX;->g:LX/0U1;

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v9

    aput-object v9, v3, v8

    const/4 v8, 0x1

    sget-object v9, LX/2IX;->g:LX/0U1;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v9

    aput-object v9, v3, v8

    invoke-static {v3}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v3

    .line 391716
    invoke-static {v2, v3}, LX/2IT;->b(LX/2IT;LX/0ux;)I

    move-result v3

    move v2, v3

    .line 391717
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 391718
    const-string v1, "fs_total_dir_size"

    iget-object v2, p0, LX/2IS;->b:LX/30H;

    invoke-virtual {v2}, LX/30H;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 391719
    const-string v1, "fs_total_custom_location_size"

    iget-object v2, p0, LX/2IS;->b:LX/30H;

    invoke-virtual {v2}, LX/30H;->c()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_0
.end method
