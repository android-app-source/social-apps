.class public final enum LX/2VD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2VD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2VD;

.field public static final enum NETWORK_CONNECTIVITY:LX/2VD;

.field public static final enum USER_IN_APP:LX/2VD;

.field public static final enum USER_LOGGED_IN:LX/2VD;

.field public static final enum USER_LOGGED_IN_ALLOWING_LOGGING_OUT:LX/2VD;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 417246
    new-instance v0, LX/2VD;

    const-string v1, "NETWORK_CONNECTIVITY"

    invoke-direct {v0, v1, v2}, LX/2VD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    .line 417247
    new-instance v0, LX/2VD;

    const-string v1, "USER_LOGGED_IN"

    invoke-direct {v0, v1, v3}, LX/2VD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    .line 417248
    new-instance v0, LX/2VD;

    const-string v1, "USER_LOGGED_IN_ALLOWING_LOGGING_OUT"

    invoke-direct {v0, v1, v4}, LX/2VD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2VD;->USER_LOGGED_IN_ALLOWING_LOGGING_OUT:LX/2VD;

    .line 417249
    new-instance v0, LX/2VD;

    const-string v1, "USER_IN_APP"

    invoke-direct {v0, v1, v5}, LX/2VD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2VD;->USER_IN_APP:LX/2VD;

    .line 417250
    const/4 v0, 0x4

    new-array v0, v0, [LX/2VD;

    sget-object v1, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    aput-object v1, v0, v2

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    aput-object v1, v0, v3

    sget-object v1, LX/2VD;->USER_LOGGED_IN_ALLOWING_LOGGING_OUT:LX/2VD;

    aput-object v1, v0, v4

    sget-object v1, LX/2VD;->USER_IN_APP:LX/2VD;

    aput-object v1, v0, v5

    sput-object v0, LX/2VD;->$VALUES:[LX/2VD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 417251
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2VD;
    .locals 1

    .prologue
    .line 417252
    const-class v0, LX/2VD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2VD;

    return-object v0
.end method

.method public static values()[LX/2VD;
    .locals 1

    .prologue
    .line 417253
    sget-object v0, LX/2VD;->$VALUES:[LX/2VD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2VD;

    return-object v0
.end method
