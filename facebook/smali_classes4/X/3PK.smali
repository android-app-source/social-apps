.class public LX/3PK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3PL;


# direct methods
.method public constructor <init>(LX/3PL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 561997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 561998
    iput-object p1, p0, LX/3PK;->a:LX/3PL;

    .line 561999
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/4VT;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 561977
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel;

    const/4 v1, 0x0

    .line 561978
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel$StoryModel;

    move-result-object v2

    .line 561979
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel$StoryModel;->k()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel$StoryModel;->k()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel$StoryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 561980
    :goto_0
    return-object v0

    .line 561981
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel$StoryModel;->j()LX/0Px;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;

    .line 561982
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->a()LX/0Px;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLACE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v3, v4}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object v0, v1

    .line 561983
    goto :goto_0

    .line 561984
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel$StoryModel;->k()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForPlaceListConversionModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 561985
    if-nez v0, :cond_3

    .line 561986
    const/4 v2, 0x0

    .line 561987
    :goto_1
    move-object v0, v2

    .line 561988
    new-instance v2, LX/6A1;

    invoke-direct {v2, v1, v0}, LX/6A1;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 561989
    move-object v0, v2

    .line 561990
    goto :goto_0

    .line 561991
    :cond_3
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    .line 561992
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->a()LX/0Px;

    move-result-object v3

    .line 561993
    iput-object v3, v2, LX/39x;->p:LX/0Px;

    .line 561994
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$PlaceListStoryAttachmentConversionFieldsModel;->b()LX/580;

    move-result-object v3

    invoke-static {v3}, LX/5Hb;->a(LX/580;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    .line 561995
    iput-object v3, v2, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 561996
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    goto :goto_1
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 561975
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$ConvertToPlaceListStoryMutationCallModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 561976
    const/4 v0, 0x0

    return-object v0
.end method
