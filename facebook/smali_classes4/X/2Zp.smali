.class public LX/2Zp;
.super LX/2h2;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h2;",
        "LX/0e6",
        "<",
        "Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;",
        "Lcom/facebook/zero/sdk/token/ZeroToken;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0yV;

.field private final c:LX/1p3;

.field private final d:LX/2gy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 423081
    const-class v0, LX/2Zp;

    sput-object v0, LX/2Zp;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0yV;LX/1p3;LX/2gy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 423076
    invoke-direct {p0}, LX/2h2;-><init>()V

    .line 423077
    iput-object p1, p0, LX/2Zp;->b:LX/0yV;

    .line 423078
    iput-object p2, p0, LX/2Zp;->c:LX/1p3;

    .line 423079
    iput-object p3, p0, LX/2Zp;->d:LX/2gy;

    .line 423080
    return-void
.end method

.method private static a(LX/1pN;)Lcom/facebook/zero/sdk/token/ZeroToken;
    .locals 15

    .prologue
    .line 423039
    invoke-virtual/range {p0 .. p0}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 423040
    if-nez v0, :cond_0

    .line 423041
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Expected response to be a struct"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 423042
    :cond_0
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v1

    if-nez v1, :cond_1

    .line 423043
    sget-object v0, Lcom/facebook/zero/sdk/token/ZeroToken;->a:Lcom/facebook/zero/sdk/token/ZeroToken;

    .line 423044
    :goto_0
    return-object v0

    .line 423045
    :cond_1
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 423046
    const-string v2, "status"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const-string v3, "unknown"

    invoke-static {v2, v3}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    .line 423047
    const-string v2, "reg_status"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const-string v3, "unknown"

    invoke-static {v2, v3}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 423048
    const-string v3, "carrier_name"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    const-string v4, ""

    invoke-static {v3, v4}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 423049
    const-string v4, "carrier_id"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    const-string v5, ""

    invoke-static {v4, v5}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 423050
    const-string v5, "carrier_logo_url"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    const-string v6, ""

    invoke-static {v5, v6}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 423051
    const-string v6, "ttl"

    invoke-virtual {v0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    const/16 v7, 0xe10

    invoke-static {v6, v7}, LX/16N;->a(LX/0lF;I)I

    move-result v6

    .line 423052
    const-string v7, "unregistered_reason"

    invoke-virtual {v0, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    const-string v8, "unavailable"

    invoke-static {v7, v8}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 423053
    const-string v7, "enabled_ui_features"

    invoke-virtual {v0, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 423054
    const-string v7, "enabled_ui_features"

    invoke-virtual {v0, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/0yV;->a(LX/0lF;)LX/0Px;

    move-result-object v7

    invoke-static {v7}, LX/0yY;->fromStrings(Ljava/lang/Iterable;)LX/0Rf;

    move-result-object v7

    .line 423055
    :goto_1
    const-string v8, "rewrite_rules"

    invoke-virtual {v0, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    .line 423056
    if-eqz v8, :cond_3

    .line 423057
    invoke-static {v8}, LX/1p3;->a(LX/0lF;)LX/0Px;

    move-result-object v8

    .line 423058
    :goto_2
    const-string v10, "backup_rules"

    invoke-virtual {v0, v10}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    .line 423059
    if-eqz v10, :cond_4

    .line 423060
    invoke-static {v10}, LX/1p3;->a(LX/0lF;)LX/0Px;

    move-result-object v10

    .line 423061
    :goto_3
    const-string v11, "pool_pricing_map"

    invoke-virtual {v0, v11}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v11

    .line 423062
    if-eqz v11, :cond_5

    .line 423063
    invoke-static {v11}, LX/2gy;->a(LX/0lF;)LX/0P1;

    move-result-object v14

    .line 423064
    :goto_4
    const-string v11, "token_hash"

    invoke-virtual {v0, v11}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v11

    const-string v12, ""

    invoke-static {v11, v12}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 423065
    const-string v12, "request_time"

    invoke-virtual {v0, v12}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v12

    const/4 v13, 0x0

    invoke-static {v12, v13}, LX/16N;->a(LX/0lF;I)I

    move-result v12

    .line 423066
    const-string v13, "fast_hash"

    invoke-virtual {v0, v13}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v13, ""

    invoke-static {v0, v13}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 423067
    new-instance v0, Lcom/facebook/zero/sdk/token/ZeroToken;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/zero/sdk/token/ZeroToken;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Rf;LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;ILjava/lang/String;LX/0P1;)V

    goto/16 :goto_0

    .line 423068
    :cond_2
    sget-object v7, LX/0Re;->a:LX/0Re;

    move-object v7, v7

    .line 423069
    goto :goto_1

    .line 423070
    :cond_3
    sget-object v8, LX/0Q7;->a:LX/0Px;

    move-object v8, v8

    .line 423071
    goto :goto_2

    .line 423072
    :cond_4
    sget-object v10, LX/0Q7;->a:LX/0Px;

    move-object v10, v10

    .line 423073
    goto :goto_3

    .line 423074
    :cond_5
    sget-object v11, LX/0Rg;->a:LX/0Rg;

    move-object v14, v11

    .line 423075
    goto :goto_4
.end method

.method public static b(LX/0QB;)LX/2Zp;
    .locals 4

    .prologue
    .line 423037
    new-instance v3, LX/2Zp;

    invoke-static {p0}, LX/0yV;->b(LX/0QB;)LX/0yV;

    move-result-object v0

    check-cast v0, LX/0yV;

    invoke-static {p0}, LX/1p3;->b(LX/0QB;)LX/1p3;

    move-result-object v1

    check-cast v1, LX/1p3;

    invoke-static {p0}, LX/2gy;->b(LX/0QB;)LX/2gy;

    move-result-object v2

    check-cast v2, LX/2gy;

    invoke-direct {v3, v0, v1, v2}, LX/2Zp;-><init>(LX/0yV;LX/1p3;LX/2gy;)V

    .line 423038
    return-object v3
.end method

.method private static b(Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;)Ljava/util/List;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 423003
    invoke-static {p0}, LX/2h2;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;)Ljava/util/List;

    move-result-object v1

    .line 423004
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "dialtone_enabled"

    .line 423005
    iget-object v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->a:LX/0yi;

    move-object v0, v0

    .line 423006
    sget-object v4, LX/0yi;->DIALTONE:LX/0yi;

    if-ne v0, v4, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423007
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "needs_backup_rules"

    .line 423008
    iget-boolean v0, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->b:Z

    move v0, v0

    .line 423009
    if-eqz v0, :cond_1

    const-string v0, "true"

    :goto_1
    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423010
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "token_hash"

    .line 423011
    iget-object v3, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 423012
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423013
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "request_reason"

    .line 423014
    iget-object v3, p0, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;->d:LX/32P;

    move-object v3, v3

    .line 423015
    invoke-virtual {v3}, LX/32P;->getRequestString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423016
    return-object v1

    .line 423017
    :cond_0
    const-string v0, "false"

    goto :goto_0

    .line 423018
    :cond_1
    const-string v0, "false"

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 423020
    check-cast p1, Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;

    .line 423021
    invoke-static {p1}, LX/2Zp;->b(Lcom/facebook/zero/sdk/request/FetchZeroTokenRequestParams;)Ljava/util/List;

    move-result-object v0

    .line 423022
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    const-string v2, "fetchZeroToken"

    .line 423023
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 423024
    move-object v1, v1

    .line 423025
    const-string v2, "GET"

    .line 423026
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 423027
    move-object v1, v1

    .line 423028
    const-string v2, "method/mobile.zeroCampaign"

    .line 423029
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 423030
    move-object v1, v1

    .line 423031
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 423032
    move-object v0, v1

    .line 423033
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 423034
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 423035
    move-object v0, v0

    .line 423036
    sget-object v1, LX/14P;->RETRY_SAFE:LX/14P;

    invoke-virtual {v0, v1}, LX/14O;->a(LX/14P;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 423019
    invoke-static {p2}, LX/2Zp;->a(LX/1pN;)Lcom/facebook/zero/sdk/token/ZeroToken;

    move-result-object v0

    return-object v0
.end method
