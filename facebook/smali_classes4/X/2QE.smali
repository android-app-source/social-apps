.class public final LX/2QE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QR;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0QR",
        "<",
        "Ljava/util/Set",
        "<TV;>;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field private final expectedValuesPerKey:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 407782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407783
    const-string v0, "expectedValuesPerKey"

    invoke-static {p1, v0}, LX/0P6;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2QE;->expectedValuesPerKey:I

    .line 407784
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 407785
    iget v0, p0, LX/2QE;->expectedValuesPerKey:I

    .line 407786
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-static {v0}, LX/0PM;->b(I)I

    move-result p0

    invoke-direct {v1, p0}, Ljava/util/LinkedHashSet;-><init>(I)V

    move-object v0, v1

    .line 407787
    return-object v0
.end method
