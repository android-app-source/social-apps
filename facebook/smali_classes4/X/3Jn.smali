.class public LX/3Jn;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3JO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3JO",
            "<",
            "LX/3K1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 548017
    new-instance v0, LX/3Jo;

    invoke-direct {v0}, LX/3Jo;-><init>()V

    sput-object v0, LX/3Jn;->a:LX/3JO;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 548018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/util/JsonReader;)LX/3K1;
    .locals 5

    .prologue
    .line 548019
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginObject()V

    .line 548020
    new-instance v1, LX/3Jp;

    invoke-direct {v1}, LX/3Jp;-><init>()V

    .line 548021
    :goto_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 548022
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 548023
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 548024
    invoke-virtual {p0}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 548025
    :sswitch_0
    const-string v3, "name"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "fill_color"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v3, "stroke_color"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v3, "stroke_width"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v3, "from_frame"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_5
    const-string v3, "to_frame"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_6
    const-string v3, "key_frames"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_7
    const-string v3, "timing_curves"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_8
    const-string v3, "animation_group"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_9
    const-string v3, "feature_animations"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_a
    const-string v3, "effects"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xa

    goto :goto_1

    :sswitch_b
    const-string v3, "stroke_line_cap"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xb

    goto/16 :goto_1

    :sswitch_c
    const-string v3, "class"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_1

    :sswitch_d
    const-string v3, "masking"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_1

    .line 548026
    :pswitch_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/3Jp;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 548027
    :pswitch_1
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, LX/3Jp;->b:I

    goto/16 :goto_0

    .line 548028
    :pswitch_2
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, LX/3Jp;->c:I

    goto/16 :goto_0

    .line 548029
    :pswitch_3
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextDouble()D

    move-result-wide v2

    double-to-float v0, v2

    iput v0, v1, LX/3Jp;->d:F

    goto/16 :goto_0

    .line 548030
    :pswitch_4
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextDouble()D

    move-result-wide v2

    double-to-float v0, v2

    iput v0, v1, LX/3Jp;->e:F

    goto/16 :goto_0

    .line 548031
    :pswitch_5
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextDouble()D

    move-result-wide v2

    double-to-float v0, v2

    iput v0, v1, LX/3Jp;->f:F

    goto/16 :goto_0

    .line 548032
    :pswitch_6
    sget-object v0, LX/3Jq;->a:LX/3JO;

    invoke-virtual {v0, p0}, LX/3JO;->a(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, LX/3Jp;->g:Ljava/util/List;

    goto/16 :goto_0

    .line 548033
    :pswitch_7
    invoke-static {p0}, LX/3JX;->b(Landroid/util/JsonReader;)[[[F

    move-result-object v0

    iput-object v0, v1, LX/3Jp;->h:[[[F

    goto/16 :goto_0

    .line 548034
    :pswitch_8
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextInt()I

    move-result v0

    iput v0, v1, LX/3Jp;->i:I

    goto/16 :goto_0

    .line 548035
    :pswitch_9
    sget-object v0, LX/3JQ;->a:LX/3JO;

    invoke-virtual {v0, p0}, LX/3JO;->a(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, LX/3Jp;->l:Ljava/util/List;

    goto/16 :goto_0

    .line 548036
    :pswitch_a
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginObject()V

    .line 548037
    new-instance v2, LX/3k9;

    invoke-direct {v2}, LX/3k9;-><init>()V

    .line 548038
    :goto_2
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 548039
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v3

    .line 548040
    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :cond_1
    :goto_3
    packed-switch v0, :pswitch_data_2

    .line 548041
    invoke-virtual {p0}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_2

    .line 548042
    :pswitch_b
    const-string v4, "gradient"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x0

    goto :goto_3

    .line 548043
    :pswitch_c
    invoke-static {p0}, LX/3kA;->a(Landroid/util/JsonReader;)LX/3kK;

    move-result-object v0

    iput-object v0, v2, LX/3k9;->a:LX/3kK;

    goto :goto_2

    .line 548044
    :cond_2
    invoke-virtual {p0}, Landroid/util/JsonReader;->endObject()V

    .line 548045
    new-instance v0, LX/3kN;

    iget-object v3, v2, LX/3k9;->a:LX/3kK;

    invoke-direct {v0, v3}, LX/3kN;-><init>(LX/3kK;)V

    move-object v0, v0

    .line 548046
    move-object v0, v0

    .line 548047
    iput-object v0, v1, LX/3Jp;->m:LX/3kN;

    goto/16 :goto_0

    .line 548048
    :pswitch_d
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Paint$Cap;->valueOf(Ljava/lang/String;)Landroid/graphics/Paint$Cap;

    move-result-object v0

    iput-object v0, v1, LX/3Jp;->j:Landroid/graphics/Paint$Cap;

    goto/16 :goto_0

    .line 548049
    :pswitch_e
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/3Jp;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 548050
    :pswitch_f
    invoke-static {p0}, LX/3Jn;->a(Landroid/util/JsonReader;)LX/3K1;

    move-result-object v0

    iput-object v0, v1, LX/3Jp;->k:LX/3K1;

    goto/16 :goto_0

    .line 548051
    :cond_3
    invoke-virtual {p0}, Landroid/util/JsonReader;->endObject()V

    .line 548052
    invoke-virtual {v1}, LX/3Jp;->a()LX/3K1;

    move-result-object v0

    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x72a13dfc -> :sswitch_8
        -0x6d4f86fe -> :sswitch_a
        -0x6baac368 -> :sswitch_4
        -0x5b3ddd07 -> :sswitch_7
        -0x5634ed57 -> :sswitch_5
        -0xf7a8164 -> :sswitch_2
        -0xec7e659 -> :sswitch_1
        -0xe638301 -> :sswitch_3
        0x337a8b -> :sswitch_0
        0x5a5a978 -> :sswitch_c
        0x912ca86 -> :sswitch_6
        0x2238da58 -> :sswitch_9
        0x2b437f2e -> :sswitch_b
        0x32141976 -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x557f730
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_c
    .end packed-switch
.end method
