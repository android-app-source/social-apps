.class public LX/24y;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 367875
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367876
    return-void
.end method

.method public static a(LX/24x;)Landroid/os/Parcelable$Creator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/24x",
            "<TT;>;)",
            "Landroid/os/Parcelable$Creator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 367877
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_0

    .line 367878
    new-instance v0, LX/24z;

    invoke-direct {v0, p0}, LX/24z;-><init>(LX/24x;)V

    move-object v0, v0

    .line 367879
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/3rB;

    invoke-direct {v0, p0}, LX/3rB;-><init>(LX/24x;)V

    goto :goto_0
.end method
