.class public LX/3QM;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:J

.field public static b:J


# instance fields
.field private final c:LX/13O;

.field private final d:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 564283
    const-wide/16 v0, 0x7530

    sput-wide v0, LX/3QM;->a:J

    .line 564284
    const-wide/32 v0, 0x2bf20

    sput-wide v0, LX/3QM;->b:J

    return-void
.end method

.method public constructor <init>(LX/13O;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 564285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564286
    iput-object p1, p0, LX/3QM;->c:LX/13O;

    .line 564287
    iput-object p2, p0, LX/3QM;->d:LX/0SG;

    .line 564288
    return-void
.end method

.method public static a(LX/0QB;)LX/3QM;
    .locals 1

    .prologue
    .line 564289
    invoke-static {p0}, LX/3QM;->b(LX/0QB;)LX/3QM;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3QM;
    .locals 3

    .prologue
    .line 564290
    new-instance v2, LX/3QM;

    invoke-static {p0}, LX/13O;->a(LX/0QB;)LX/13O;

    move-result-object v0

    check-cast v0, LX/13O;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v0, v1}, LX/3QM;-><init>(LX/13O;LX/0SG;)V

    .line 564291
    return-object v2
.end method


# virtual methods
.method public final a(J)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    .line 564292
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Should only be called for One-to-one threads!"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 564293
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 564294
    iget-object v2, p0, LX/3QM;->c:LX/13O;

    sget-object v3, LX/77X;->THREAD_ACTIVITY:LX/77X;

    invoke-virtual {v3}, LX/77X;->toEventName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/13O;->b(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    .line 564295
    iget-object v4, p0, LX/3QM;->d:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    sget-wide v4, LX/3QM;->a:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 564296
    iget-object v2, p0, LX/3QM;->c:LX/13O;

    sget-object v3, LX/77X;->THREAD_ACTIVITY:LX/77X;

    invoke-virtual {v3}, LX/77X;->toEventName()Ljava/lang/String;

    move-result-object v3

    .line 564297
    iget-object v7, v2, LX/13O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    .line 564298
    invoke-static {v3, v0}, LX/13O;->f(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;

    move-result-object v8

    invoke-interface {v7, v8, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 564299
    invoke-static {v3, v0}, LX/13O;->g(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-interface {v7, v8, v9, v10}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 564300
    invoke-interface {v7}, LX/0hN;->commit()V

    .line 564301
    invoke-static {v2, v3, v0}, LX/13O;->e(LX/13O;Ljava/lang/String;Ljava/lang/String;)V

    .line 564302
    :goto_1
    return-void

    .line 564303
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 564304
    :cond_1
    iget-object v1, p0, LX/3QM;->c:LX/13O;

    sget-object v2, LX/77X;->THREAD_ACTIVITY:LX/77X;

    invoke-virtual {v2}, LX/77X;->toEventName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/13O;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
