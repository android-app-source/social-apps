.class public abstract LX/2Ti;
.super Ljava/util/AbstractCollection;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractCollection",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 414563
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Xu",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 414564
    invoke-virtual {p0}, LX/2Ti;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->g()V

    .line 414565
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 414566
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    .line 414567
    check-cast p1, Ljava/util/Map$Entry;

    .line 414568
    invoke-virtual {p0}, LX/2Ti;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0Xu;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 414569
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 414570
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    .line 414571
    check-cast p1, Ljava/util/Map$Entry;

    .line 414572
    invoke-virtual {p0}, LX/2Ti;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0Xu;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 414573
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 414574
    invoke-virtual {p0}, LX/2Ti;->a()LX/0Xu;

    move-result-object v0

    invoke-interface {v0}, LX/0Xu;->f()I

    move-result v0

    return v0
.end method
