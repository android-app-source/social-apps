.class public final enum LX/34O;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/34O;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/34O;

.field public static final enum GROUPER:LX/34O;

.field public static final enum GROUPER_ATTACHED_STORY:LX/34O;

.field public static final enum GROUPER_WITH_OFFER:LX/34O;

.field public static final enum NCPP:LX/34O;

.field public static final enum OFFER:LX/34O;

.field public static final enum OTHER:LX/34O;

.field public static final enum PAGE_LIKE:LX/34O;

.field public static final enum SINGLE_CREATOR_COLLECTION_ITEM:LX/34O;

.field public static final enum UNSET:LX/34O;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 495078
    new-instance v0, LX/34O;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v3}, LX/34O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/34O;->UNSET:LX/34O;

    .line 495079
    new-instance v0, LX/34O;

    const-string v1, "GROUPER"

    invoke-direct {v0, v1, v4}, LX/34O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/34O;->GROUPER:LX/34O;

    .line 495080
    new-instance v0, LX/34O;

    const-string v1, "OFFER"

    invoke-direct {v0, v1, v5}, LX/34O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/34O;->OFFER:LX/34O;

    .line 495081
    new-instance v0, LX/34O;

    const-string v1, "GROUPER_WITH_OFFER"

    invoke-direct {v0, v1, v6}, LX/34O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/34O;->GROUPER_WITH_OFFER:LX/34O;

    .line 495082
    new-instance v0, LX/34O;

    const-string v1, "GROUPER_ATTACHED_STORY"

    invoke-direct {v0, v1, v7}, LX/34O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/34O;->GROUPER_ATTACHED_STORY:LX/34O;

    .line 495083
    new-instance v0, LX/34O;

    const-string v1, "PAGE_LIKE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/34O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/34O;->PAGE_LIKE:LX/34O;

    .line 495084
    new-instance v0, LX/34O;

    const-string v1, "NCPP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/34O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/34O;->NCPP:LX/34O;

    .line 495085
    new-instance v0, LX/34O;

    const-string v1, "SINGLE_CREATOR_COLLECTION_ITEM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/34O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/34O;->SINGLE_CREATOR_COLLECTION_ITEM:LX/34O;

    .line 495086
    new-instance v0, LX/34O;

    const-string v1, "OTHER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/34O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/34O;->OTHER:LX/34O;

    .line 495087
    const/16 v0, 0x9

    new-array v0, v0, [LX/34O;

    sget-object v1, LX/34O;->UNSET:LX/34O;

    aput-object v1, v0, v3

    sget-object v1, LX/34O;->GROUPER:LX/34O;

    aput-object v1, v0, v4

    sget-object v1, LX/34O;->OFFER:LX/34O;

    aput-object v1, v0, v5

    sget-object v1, LX/34O;->GROUPER_WITH_OFFER:LX/34O;

    aput-object v1, v0, v6

    sget-object v1, LX/34O;->GROUPER_ATTACHED_STORY:LX/34O;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/34O;->PAGE_LIKE:LX/34O;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/34O;->NCPP:LX/34O;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/34O;->SINGLE_CREATOR_COLLECTION_ITEM:LX/34O;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/34O;->OTHER:LX/34O;

    aput-object v2, v0, v1

    sput-object v0, LX/34O;->$VALUES:[LX/34O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 495088
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/34O;
    .locals 1

    .prologue
    .line 495077
    const-class v0, LX/34O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/34O;

    return-object v0
.end method

.method public static values()[LX/34O;
    .locals 1

    .prologue
    .line 495076
    sget-object v0, LX/34O;->$VALUES:[LX/34O;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/34O;

    return-object v0
.end method
