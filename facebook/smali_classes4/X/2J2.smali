.class public final LX/2J2;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 392519
    sget-object v0, LX/2J3;->a:LX/0U1;

    sget-object v1, LX/2J3;->b:LX/0U1;

    sget-object v2, LX/2J3;->c:LX/0U1;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2J2;->a:LX/0Px;

    .line 392520
    const-string v0, "contacts_indexed_data"

    const-string v1, "contacts_type_index"

    sget-object v2, LX/2J3;->a:LX/0U1;

    sget-object v3, LX/2J3;->b:LX/0U1;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2J2;->b:Ljava/lang/String;

    .line 392521
    const-string v0, "contacts_indexed_data"

    const-string v1, "contacts_data_index"

    sget-object v2, LX/2J3;->a:LX/0U1;

    sget-object v3, LX/2J3;->c:LX/0U1;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2J2;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 392522
    const-string v0, "contacts_indexed_data"

    sget-object v1, LX/2J2;->a:LX/0Px;

    invoke-direct {p0, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 392523
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 392524
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 392525
    sget-object v0, LX/2J2;->b:Ljava/lang/String;

    const v1, 0x327f3c9a

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x5c2c1a0f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392526
    sget-object v0, LX/2J2;->c:Ljava/lang/String;

    const v1, -0x7e0abbc9

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x71323c40

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392527
    return-void
.end method
