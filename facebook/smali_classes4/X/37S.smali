.class public final LX/37S;
.super LX/37T;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

.field private final b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field private final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 501092
    iput-object p1, p0, LX/37S;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;

    invoke-direct {p0}, LX/37T;-><init>()V

    .line 501093
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 501094
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aQ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, LX/37S;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 501095
    iput-object p2, p0, LX/37S;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 501096
    new-instance v0, LX/37U;

    invoke-direct {v0, p1, p2, p3}, LX/37U;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    iput-object v0, p0, LX/37S;->d:LX/1KL;

    .line 501097
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 2

    .prologue
    .line 501098
    iget-object v0, p0, LX/37S;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/37S;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/37S;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 501099
    :cond_0
    const/4 v0, 0x0

    .line 501100
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v0

    iget-object v1, p0, LX/37S;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 501101
    iget-object v0, p0, LX/37S;->d:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 501102
    iget-object v0, p0, LX/37S;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 501103
    iget-object v0, p0, LX/37S;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 501104
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 501105
    check-cast v0, LX/0jW;

    return-object v0
.end method
