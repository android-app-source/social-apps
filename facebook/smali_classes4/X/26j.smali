.class public LX/26j;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Uh;

.field private final b:Z


# direct methods
.method public constructor <init>(LX/0Uh;LX/01T;Ljava/lang/Boolean;)V
    .locals 1
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 372301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372302
    iput-object p1, p0, LX/26j;->a:LX/0Uh;

    .line 372303
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    if-ne p2, v0, :cond_0

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/26j;->b:Z

    .line 372304
    return-void

    .line 372305
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/26j;
    .locals 1

    .prologue
    .line 372306
    invoke-static {p0}, LX/26j;->b(LX/0QB;)LX/26j;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/26j;
    .locals 4

    .prologue
    .line 372307
    new-instance v3, LX/26j;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v1

    check-cast v1, LX/01T;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-direct {v3, v0, v1, v2}, LX/26j;-><init>(LX/0Uh;LX/01T;Ljava/lang/Boolean;)V

    .line 372308
    return-object v3
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 372309
    iget-boolean v1, p0, LX/26j;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/26j;->a:LX/0Uh;

    const/16 v2, 0x1eb

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
