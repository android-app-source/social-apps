.class public LX/2EJ;
.super Landroid/app/Dialog;
.source ""

# interfaces
.implements Landroid/content/DialogInterface;


# instance fields
.field public a:LX/4BW;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 385315
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/2EJ;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, LX/2EJ;-><init>(Landroid/content/Context;I)V

    .line 385316
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 385312
    invoke-static {p1, p2}, LX/2EJ;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 385313
    new-instance v0, LX/4BW;

    invoke-virtual {p0}, LX/2EJ;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, LX/2EJ;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, LX/4BW;-><init>(Landroid/content/Context;Landroid/content/DialogInterface;Landroid/view/Window;)V

    iput-object v0, p0, LX/2EJ;->a:LX/4BW;

    .line 385314
    return-void
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 385297
    if-ne p1, v3, :cond_1

    .line 385298
    const p1, 0x7f0e024b

    .line 385299
    :cond_0
    :goto_0
    return p1

    .line 385300
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 385301
    const p1, 0x7f0e024c

    goto :goto_0

    .line 385302
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 385303
    const p1, 0x7f0e024b

    goto :goto_0

    .line 385304
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 385305
    const p1, 0x7f0e024c

    goto :goto_0

    .line 385306
    :cond_4
    const/4 v0, 0x5

    if-ne p1, v0, :cond_5

    .line 385307
    const p1, 0x7f0e024b

    goto :goto_0

    .line 385308
    :cond_5
    const/high16 v0, 0x1000000

    if-ge p1, v0, :cond_0

    .line 385309
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 385310
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010220

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 385311
    iget p1, v0, Landroid/util/TypedValue;->resourceId:I

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 385290
    iget-object v0, p0, LX/2EJ;->a:LX/4BW;

    .line 385291
    packed-switch p1, :pswitch_data_0

    .line 385292
    const/4 p0, 0x0

    :goto_0
    move-object v0, p0

    .line 385293
    return-object v0

    .line 385294
    :pswitch_0
    iget-object p0, v0, LX/4BW;->m:Landroid/widget/Button;

    goto :goto_0

    .line 385295
    :pswitch_1
    iget-object p0, v0, LX/4BW;->p:Landroid/widget/Button;

    goto :goto_0

    .line 385296
    :pswitch_2
    iget-object p0, v0, LX/4BW;->s:Landroid/widget/Button;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 385287
    iget-object v0, p0, LX/2EJ;->a:LX/4BW;

    .line 385288
    iget-object p0, v0, LX/4BW;->f:Landroid/widget/ListView;

    move-object v0, p0

    .line 385289
    return-object v0
.end method

.method public final a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 2

    .prologue
    .line 385134
    iget-object v0, p0, LX/2EJ;->a:LX/4BW;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, LX/4BW;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 385135
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 385285
    iget-object v0, p0, LX/2EJ;->a:LX/4BW;

    invoke-virtual {v0, p1}, LX/4BW;->c(Landroid/view/View;)V

    .line 385286
    return-void
.end method

.method public a(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    .line 385283
    iget-object v0, p0, LX/2EJ;->a:LX/4BW;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, LX/4BW;->a(Landroid/view/View;IIII)V

    .line 385284
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 385281
    iget-object v0, p0, LX/2EJ;->a:LX/4BW;

    invoke-virtual {v0, p1}, LX/4BW;->b(Ljava/lang/CharSequence;)V

    .line 385282
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 385279
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1, p2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 385280
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 385277
    const/4 v0, -0x2

    invoke-virtual {p0, v0, p1, p2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 385278
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 385147
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 385148
    iget-object v0, p0, LX/2EJ;->a:LX/4BW;

    const/high16 v3, 0x20000

    .line 385149
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 385150
    iget-object v1, v0, LX/4BW;->g:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/4BW;->g:Landroid/view/View;

    invoke-static {v1}, LX/4BW;->d(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 385151
    :cond_0
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    invoke-virtual {v1, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 385152
    :cond_1
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    iget v2, v0, LX/4BW;->G:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->setContentView(I)V

    .line 385153
    const/high16 v10, 0x20000

    const/16 v9, 0x8

    const/4 v8, -0x1

    const/4 v3, 0x0

    .line 385154
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    const v2, 0x7f0d10cc

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 385155
    const/16 v5, 0x8

    const/4 v11, 0x0

    const/4 v6, -0x1

    .line 385156
    iget-object v2, v0, LX/4BW;->c:Landroid/view/Window;

    const v4, 0x7f0d10ce

    invoke-virtual {v2, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, v0, LX/4BW;->v:Landroid/widget/ScrollView;

    .line 385157
    iget-object v2, v0, LX/4BW;->v:Landroid/widget/ScrollView;

    invoke-virtual {v2, v11}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 385158
    iget-object v2, v0, LX/4BW;->c:Landroid/view/Window;

    const v4, 0x7f0d0578

    invoke-virtual {v2, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, LX/4BW;->x:Landroid/widget/TextView;

    .line 385159
    iget-object v2, v0, LX/4BW;->x:Landroid/widget/TextView;

    if-nez v2, :cond_d

    .line 385160
    :cond_2
    :goto_0
    const/16 v7, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 385161
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    const v2, 0x7f0d10d7

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, v0, LX/4BW;->m:Landroid/widget/Button;

    .line 385162
    iget-object v1, v0, LX/4BW;->m:Landroid/widget/Button;

    iget-object v2, v0, LX/4BW;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 385163
    iget-object v1, v0, LX/4BW;->n:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 385164
    iget-object v1, v0, LX/4BW;->m:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    move v2, v4

    .line 385165
    :goto_1
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    const v6, 0x7f0d10d6

    invoke-virtual {v1, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, v0, LX/4BW;->p:Landroid/widget/Button;

    .line 385166
    iget-object v1, v0, LX/4BW;->p:Landroid/widget/Button;

    iget-object v6, v0, LX/4BW;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 385167
    iget-object v1, v0, LX/4BW;->q:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 385168
    iget-object v1, v0, LX/4BW;->p:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 385169
    :goto_2
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    const v6, 0x7f0d10d5

    invoke-virtual {v1, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, v0, LX/4BW;->s:Landroid/widget/Button;

    .line 385170
    iget-object v1, v0, LX/4BW;->s:Landroid/widget/Button;

    iget-object v6, v0, LX/4BW;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 385171
    iget-object v1, v0, LX/4BW;->t:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 385172
    iget-object v1, v0, LX/4BW;->s:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 385173
    :goto_3
    iget-object v1, v0, LX/4BW;->a:Landroid/content/Context;

    const/4 v6, 0x1

    .line 385174
    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    .line 385175
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v11

    const p0, 0x7f0102d8

    invoke-virtual {v11, p0, v7, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 385176
    iget v7, v7, Landroid/util/TypedValue;->data:I

    if-eqz v7, :cond_1e

    :goto_4
    move v1, v6

    .line 385177
    if-eqz v1, :cond_3

    .line 385178
    if-ne v2, v5, :cond_1b

    .line 385179
    iget-object v1, v0, LX/4BW;->m:Landroid/widget/Button;

    invoke-static {v1}, LX/4BW;->a(Landroid/widget/Button;)V

    .line 385180
    :cond_3
    :goto_5
    if-eqz v2, :cond_1d

    :goto_6
    move v2, v5

    .line 385181
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    const v4, 0x7f0d10ca

    invoke-virtual {v1, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 385182
    iget-object v4, v0, LX/4BW;->a:Landroid/content/Context;

    const/4 v5, 0x0

    sget-object v6, LX/03r;->AlertDialog:[I

    const v7, 0x7f010221

    invoke-virtual {v4, v5, v6, v7, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 385183
    const/4 v6, 0x1

    const/16 p1, 0x8

    const/4 v5, 0x0

    .line 385184
    iget-object v7, v0, LX/4BW;->y:Landroid/view/View;

    if-eqz v7, :cond_1f

    .line 385185
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v11, -0x1

    const/4 p0, -0x2

    invoke-direct {v7, v11, p0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 385186
    iget-object v11, v0, LX/4BW;->y:Landroid/view/View;

    invoke-virtual {v1, v11, v5, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 385187
    iget-object v5, v0, LX/4BW;->c:Landroid/view/Window;

    const v7, 0x7f0d10cb

    invoke-virtual {v5, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 385188
    invoke-virtual {v5, p1}, Landroid/view/View;->setVisibility(I)V

    .line 385189
    :goto_7
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    const v5, 0x7f0d10d4

    invoke-virtual {v1, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 385190
    if-nez v2, :cond_4

    .line 385191
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 385192
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    const v2, 0x7f0d10d1

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 385193
    if-eqz v1, :cond_4

    .line 385194
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 385195
    :cond_4
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    const v2, 0x7f0d10d3

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 385196
    iget-object v5, v0, LX/4BW;->g:Landroid/view/View;

    .line 385197
    if-eqz v5, :cond_b

    const/4 v2, 0x1

    .line 385198
    :goto_8
    if-eqz v2, :cond_5

    invoke-static {v5}, LX/4BW;->d(Landroid/view/View;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 385199
    :cond_5
    iget-object v5, v0, LX/4BW;->c:Landroid/view/Window;

    invoke-virtual {v5, v10, v10}, Landroid/view/Window;->setFlags(II)V

    .line 385200
    :cond_6
    if-eqz v2, :cond_c

    .line 385201
    iget-object v2, v0, LX/4BW;->c:Landroid/view/Window;

    const v5, 0x7f0d02a3

    invoke-virtual {v2, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 385202
    iget-object v5, v0, LX/4BW;->g:Landroid/view/View;

    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v6, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v5, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 385203
    iget-boolean v5, v0, LX/4BW;->l:Z

    if-eqz v5, :cond_7

    .line 385204
    iget v5, v0, LX/4BW;->h:I

    iget v6, v0, LX/4BW;->i:I

    iget v7, v0, LX/4BW;->j:I

    iget v8, v0, LX/4BW;->k:I

    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 385205
    :cond_7
    iget-object v2, v0, LX/4BW;->f:Landroid/widget/ListView;

    if-eqz v2, :cond_8

    .line 385206
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 385207
    :cond_8
    :goto_9
    iget-object v1, v0, LX/4BW;->A:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_9

    .line 385208
    iget-object v1, v0, LX/4BW;->c:Landroid/view/Window;

    const v2, 0x7f0d10c9

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, LX/4BW;->z:Landroid/widget/ImageView;

    .line 385209
    iget-object v1, v0, LX/4BW;->z:Landroid/widget/ImageView;

    iget-object v2, v0, LX/4BW;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 385210
    iget-object v1, v0, LX/4BW;->z:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 385211
    :cond_9
    iget-object v1, v0, LX/4BW;->f:Landroid/widget/ListView;

    if-eqz v1, :cond_a

    iget-object v1, v0, LX/4BW;->E:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_a

    .line 385212
    iget-object v1, v0, LX/4BW;->f:Landroid/widget/ListView;

    iget-object v2, v0, LX/4BW;->E:Landroid/widget/ListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 385213
    iget v1, v0, LX/4BW;->F:I

    if-ltz v1, :cond_a

    .line 385214
    iget-object v1, v0, LX/4BW;->f:Landroid/widget/ListView;

    iget v2, v0, LX/4BW;->F:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 385215
    iget-object v1, v0, LX/4BW;->f:Landroid/widget/ListView;

    iget v2, v0, LX/4BW;->F:I

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 385216
    :cond_a
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 385217
    return-void

    :cond_b
    move v2, v3

    .line 385218
    goto/16 :goto_8

    .line 385219
    :cond_c
    invoke-virtual {v1, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_9

    .line 385220
    :cond_d
    iget-object v2, v0, LX/4BW;->e:Ljava/lang/CharSequence;

    if-eqz v2, :cond_13

    .line 385221
    iget-object v2, v0, LX/4BW;->x:Landroid/widget/TextView;

    iget-object v4, v0, LX/4BW;->e:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 385222
    :goto_a
    iget-object v2, v0, LX/4BW;->c:Landroid/view/Window;

    const v4, 0x7f0d10cf

    invoke-virtual {v2, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 385223
    iget-object v4, v0, LX/4BW;->D:Landroid/view/View;

    if-eqz v4, :cond_e

    .line 385224
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v4, v6, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 385225
    iget-object v5, v0, LX/4BW;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0091

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 385226
    iget-object v6, v0, LX/4BW;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0092

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    .line 385227
    iget-object v7, v0, LX/4BW;->D:Landroid/view/View;

    invoke-virtual {v7, v5, v6, v5, v11}, Landroid/view/View;->setPadding(IIII)V

    .line 385228
    iget-object v5, v0, LX/4BW;->D:Landroid/view/View;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    invoke-virtual {v2, v5, v6, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 385229
    :cond_e
    iget-object v2, v0, LX/4BW;->C:Ljava/lang/CharSequence;

    if-eqz v2, :cond_f

    .line 385230
    iget-object v2, v0, LX/4BW;->c:Landroid/view/Window;

    const v4, 0x7f0d10d0

    invoke-virtual {v2, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, LX/4BW;->B:Landroid/widget/TextView;

    .line 385231
    iget-object v2, v0, LX/4BW;->B:Landroid/widget/TextView;

    iget-object v4, v0, LX/4BW;->C:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 385232
    iget-object v2, v0, LX/4BW;->B:Landroid/widget/TextView;

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 385233
    :cond_f
    iget-object v2, v0, LX/4BW;->c:Landroid/view/Window;

    const v4, 0x7f0d10cd

    invoke-virtual {v2, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 385234
    iget-object v4, v0, LX/4BW;->c:Landroid/view/Window;

    const v5, 0x7f0d10d2

    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 385235
    if-nez v2, :cond_10

    if-eqz v4, :cond_2

    .line 385236
    :cond_10
    iget-object v5, v0, LX/4BW;->e:Ljava/lang/CharSequence;

    if-nez v5, :cond_11

    iget-object v5, v0, LX/4BW;->B:Landroid/widget/TextView;

    if-eqz v5, :cond_15

    .line 385237
    :cond_11
    iget-object v5, v0, LX/4BW;->v:Landroid/widget/ScrollView;

    invoke-virtual {v5}, Landroid/widget/ScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v5

    .line 385238
    if-eqz v5, :cond_12

    .line 385239
    new-instance v6, LX/4BO;

    invoke-direct {v6, v0, v2, v4}, LX/4BO;-><init>(LX/4BW;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v5, v6}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 385240
    :cond_12
    iget-object v5, v0, LX/4BW;->v:Landroid/widget/ScrollView;

    new-instance v6, Lcom/facebook/fbui/dialog/AlertController$3;

    invoke-direct {v6, v0, v2, v4}, Lcom/facebook/fbui/dialog/AlertController$3;-><init>(LX/4BW;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v5, v6}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 385241
    :cond_13
    iget-object v2, v0, LX/4BW;->x:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 385242
    iget-object v2, v0, LX/4BW;->v:Landroid/widget/ScrollView;

    iget-object v4, v0, LX/4BW;->x:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->removeView(Landroid/view/View;)V

    .line 385243
    iget-object v2, v0, LX/4BW;->f:Landroid/widget/ListView;

    if-eqz v2, :cond_14

    .line 385244
    iget-object v2, v0, LX/4BW;->v:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 385245
    iget-object v4, v0, LX/4BW;->v:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 385246
    iget-object v4, v0, LX/4BW;->f:Landroid/widget/ListView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_a

    .line 385247
    :cond_14
    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_a

    .line 385248
    :cond_15
    iget-object v5, v0, LX/4BW;->f:Landroid/widget/ListView;

    if-eqz v5, :cond_16

    .line 385249
    iget-object v5, v0, LX/4BW;->f:Landroid/widget/ListView;

    new-instance v6, LX/4BP;

    invoke-direct {v6, v0, v2, v4}, LX/4BP;-><init>(LX/4BW;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 385250
    iget-object v5, v0, LX/4BW;->f:Landroid/widget/ListView;

    new-instance v6, Lcom/facebook/fbui/dialog/AlertController$5;

    invoke-direct {v6, v0, v2, v4}, Lcom/facebook/fbui/dialog/AlertController$5;-><init>(LX/4BW;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 385251
    :cond_16
    if-eqz v2, :cond_17

    .line 385252
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 385253
    :cond_17
    if-eqz v4, :cond_2

    .line 385254
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 385255
    :cond_18
    iget-object v1, v0, LX/4BW;->m:Landroid/widget/Button;

    iget-object v2, v0, LX/4BW;->n:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 385256
    iget-object v1, v0, LX/4BW;->m:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    move v2, v5

    .line 385257
    goto/16 :goto_1

    .line 385258
    :cond_19
    iget-object v1, v0, LX/4BW;->p:Landroid/widget/Button;

    iget-object v6, v0, LX/4BW;->q:Ljava/lang/CharSequence;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 385259
    iget-object v1, v0, LX/4BW;->p:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 385260
    or-int/lit8 v2, v2, 0x2

    goto/16 :goto_2

    .line 385261
    :cond_1a
    iget-object v1, v0, LX/4BW;->s:Landroid/widget/Button;

    iget-object v6, v0, LX/4BW;->t:Ljava/lang/CharSequence;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 385262
    iget-object v1, v0, LX/4BW;->s:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 385263
    or-int/lit8 v2, v2, 0x4

    goto/16 :goto_3

    .line 385264
    :cond_1b
    const/4 v1, 0x2

    if-ne v2, v1, :cond_1c

    .line 385265
    iget-object v1, v0, LX/4BW;->p:Landroid/widget/Button;

    invoke-static {v1}, LX/4BW;->a(Landroid/widget/Button;)V

    goto/16 :goto_5

    .line 385266
    :cond_1c
    const/4 v1, 0x4

    if-ne v2, v1, :cond_3

    .line 385267
    iget-object v1, v0, LX/4BW;->s:Landroid/widget/Button;

    invoke-static {v1}, LX/4BW;->a(Landroid/widget/Button;)V

    goto/16 :goto_5

    :cond_1d
    move v5, v4

    .line 385268
    goto/16 :goto_6

    :cond_1e
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 385269
    :cond_1f
    iget-object v7, v0, LX/4BW;->d:Ljava/lang/CharSequence;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_20

    move v7, v6

    .line 385270
    :goto_b
    if-eqz v7, :cond_21

    .line 385271
    iget-object v5, v0, LX/4BW;->c:Landroid/view/Window;

    const v7, 0x7f0d10cb

    invoke-virtual {v5, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v0, LX/4BW;->w:Landroid/widget/TextView;

    .line 385272
    iget-object v5, v0, LX/4BW;->w:Landroid/widget/TextView;

    iget-object v7, v0, LX/4BW;->d:Ljava/lang/CharSequence;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    :cond_20
    move v7, v5

    .line 385273
    goto :goto_b

    .line 385274
    :cond_21
    iget-object v6, v0, LX/4BW;->c:Landroid/view/Window;

    const v7, 0x7f0d10cb

    invoke-virtual {v6, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 385275
    invoke-virtual {v6, p1}, Landroid/view/View;->setVisibility(I)V

    .line 385276
    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_7
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 385143
    iget-object v0, p0, LX/2EJ;->a:LX/4BW;

    .line 385144
    iget-object v1, v0, LX/4BW;->v:Landroid/widget/ScrollView;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/4BW;->v:Landroid/widget/ScrollView;

    invoke-virtual {v1, p2}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 385145
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 385146
    :goto_1
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 385139
    iget-object v0, p0, LX/2EJ;->a:LX/4BW;

    .line 385140
    iget-object v1, v0, LX/4BW;->v:Landroid/widget/ScrollView;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/4BW;->v:Landroid/widget/ScrollView;

    invoke-virtual {v1, p2}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 385141
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 385142
    :goto_1
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 385136
    invoke-super {p0, p1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 385137
    iget-object v0, p0, LX/2EJ;->a:LX/4BW;

    invoke-virtual {v0, p1}, LX/4BW;->a(Ljava/lang/CharSequence;)V

    .line 385138
    return-void
.end method
