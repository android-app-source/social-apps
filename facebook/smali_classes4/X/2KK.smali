.class public LX/2KK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static q:Z

.field private static volatile v:LX/2KK;


# instance fields
.field public final c:LX/0Zb;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Xl;

.field public final f:LX/0Xl;

.field public final g:LX/0kb;

.field private final h:LX/0Sh;

.field private final i:LX/0SG;

.field public final j:LX/0gX;

.field public final k:LX/0tX;

.field public final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Uo;

.field public n:LX/12x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0gM;

.field public r:LX/0Yb;

.field public s:Landroid/app/PendingIntent;

.field public final t:LX/0Yb;

.field public u:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 393662
    const-class v0, LX/2KK;

    sput-object v0, LX/2KK;->b:Ljava/lang/Class;

    .line 393663
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/2KK;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ACTION_INEXACT_ALARM"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2KK;->a:Ljava/lang/String;

    .line 393664
    const/4 v0, 0x0

    sput-boolean v0, LX/2KK;->q:Z

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0Ot;LX/0Xl;LX/0Xl;LX/0kb;LX/0Sh;LX/0SG;LX/0gX;LX/0tX;LX/0Or;LX/0Uo;)V
    .locals 3
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/GlobalFbBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ">;",
            "LX/0Xl;",
            "LX/0Xl;",
            "LX/0kb;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0SG;",
            "LX/0gX;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "LX/0Uo;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 393665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393666
    const-string v0, ""

    iput-object v0, p0, LX/2KK;->u:Ljava/lang/String;

    .line 393667
    iput-object p1, p0, LX/2KK;->c:LX/0Zb;

    .line 393668
    iput-object p2, p0, LX/2KK;->d:LX/0Ot;

    .line 393669
    iput-object p3, p0, LX/2KK;->e:LX/0Xl;

    .line 393670
    iput-object p4, p0, LX/2KK;->f:LX/0Xl;

    .line 393671
    iput-object p5, p0, LX/2KK;->g:LX/0kb;

    .line 393672
    iput-object p6, p0, LX/2KK;->h:LX/0Sh;

    .line 393673
    iput-object p7, p0, LX/2KK;->i:LX/0SG;

    .line 393674
    iput-object p8, p0, LX/2KK;->j:LX/0gX;

    .line 393675
    iput-object p9, p0, LX/2KK;->k:LX/0tX;

    .line 393676
    iput-object p10, p0, LX/2KK;->l:LX/0Or;

    .line 393677
    iput-object p11, p0, LX/2KK;->m:LX/0Uo;

    .line 393678
    new-instance v0, LX/2KL;

    invoke-direct {v0, p0}, LX/2KL;-><init>(LX/2KK;)V

    .line 393679
    iget-object v1, p0, LX/2KK;->e:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2KK;->t:LX/0Yb;

    .line 393680
    return-void
.end method

.method public static a(LX/0QB;)LX/2KK;
    .locals 15

    .prologue
    .line 393681
    sget-object v0, LX/2KK;->v:LX/2KK;

    if-nez v0, :cond_1

    .line 393682
    const-class v1, LX/2KK;

    monitor-enter v1

    .line 393683
    :try_start_0
    sget-object v0, LX/2KK;->v:LX/2KK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 393684
    if-eqz v2, :cond_0

    .line 393685
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 393686
    new-instance v3, LX/2KK;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    const/16 v5, 0xb45

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/0aW;->a(LX/0QB;)LX/0aW;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v8

    check-cast v8, LX/0kb;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v9

    check-cast v9, LX/0Sh;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {v0}, LX/0gX;->a(LX/0QB;)LX/0gX;

    move-result-object v11

    check-cast v11, LX/0gX;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v12

    check-cast v12, LX/0tX;

    const/16 v13, 0xdf4

    invoke-static {v0, v13}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v14

    check-cast v14, LX/0Uo;

    invoke-direct/range {v3 .. v14}, LX/2KK;-><init>(LX/0Zb;LX/0Ot;LX/0Xl;LX/0Xl;LX/0kb;LX/0Sh;LX/0SG;LX/0gX;LX/0tX;LX/0Or;LX/0Uo;)V

    .line 393687
    invoke-static {v0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v4

    check-cast v4, LX/12x;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    .line 393688
    iput-object v4, v3, LX/2KK;->n:LX/12x;

    iput-object v5, v3, LX/2KK;->o:Landroid/content/Context;

    .line 393689
    move-object v0, v3

    .line 393690
    sput-object v0, LX/2KK;->v:LX/2KK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 393691
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 393692
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 393693
    :cond_1
    sget-object v0, LX/2KK;->v:LX/2KK;

    return-object v0

    .line 393694
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 393695
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2KK;LX/EmC;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 393696
    iget-object v0, p0, LX/2KK;->h:LX/0Sh;

    new-instance v1, LX/EmB;

    invoke-direct {v1, p0}, LX/EmB;-><init>(LX/2KK;)V

    const/4 v2, 0x1

    new-array v2, v2, [LX/EmC;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 393697
    return-void
.end method

.method public static declared-synchronized b(LX/2KK;Z)V
    .locals 7

    .prologue
    .line 393698
    monitor-enter p0

    .line 393699
    :try_start_0
    iget-object v3, p0, LX/2KK;->l:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W3;

    sget-wide v5, LX/0X5;->cw:J

    invoke-interface {v3, v5, v6}, LX/0W4;->a(J)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 393700
    iget-object v3, p0, LX/2KK;->l:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W3;

    sget-wide v5, LX/0X5;->cw:J

    invoke-interface {v3, v5, v6}, LX/0W4;->a(J)Z

    move-result v3

    move v0, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 393701
    if-nez v0, :cond_0

    .line 393702
    :goto_0
    monitor-exit p0

    return-void

    .line 393703
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2KK;->g:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 393704
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 393705
    new-instance v0, LX/EmF;

    invoke-direct {v0}, LX/EmF;-><init>()V

    move-object v0, v0

    .line 393706
    new-instance v1, LX/4E5;

    invoke-direct {v1}, LX/4E5;-><init>()V

    const/4 v2, 0x0

    .line 393707
    const-string v3, "internal_ip"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 393708
    move-object v1, v1

    .line 393709
    const/4 v2, 0x0

    .line 393710
    const-string v3, "internal_netmask"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 393711
    move-object v1, v1

    .line 393712
    const-string v2, "0"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 393713
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 393714
    iget-object v1, p0, LX/2KK;->k:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 393715
    const-string v1, "request_devices"

    invoke-static {p0, v1}, LX/2KK;->b$redex0(LX/2KK;Ljava/lang/String;)V

    .line 393716
    new-instance v1, LX/Em9;

    invoke-direct {v1, p0, p1}, LX/Em9;-><init>(LX/2KK;Z)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 393717
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 393718
    :cond_1
    :try_start_2
    invoke-static {p0}, LX/2KK;->f(LX/2KK;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static b$redex0(LX/2KK;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 393719
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "ott_wilde_tcp_device_discovery"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 393720
    const-string v1, "ott_device_discovery"

    .line 393721
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 393722
    const-string v1, "event_type"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 393723
    iget-object v1, p0, LX/2KK;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 393724
    return-void
.end method

.method public static c$redex0(LX/2KK;)V
    .locals 1

    .prologue
    .line 393725
    iget-object v0, p0, LX/2KK;->t:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 393726
    return-void
.end method

.method public static f(LX/2KK;)V
    .locals 2

    .prologue
    .line 393727
    iget-object v0, p0, LX/2KK;->p:LX/0gM;

    if-eqz v0, :cond_0

    .line 393728
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 393729
    iget-object v1, p0, LX/2KK;->p:LX/0gM;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 393730
    iget-object v1, p0, LX/2KK;->j:LX/0gX;

    invoke-virtual {v1, v0}, LX/0gX;->a(Ljava/util/Set;)V

    .line 393731
    const/4 v0, 0x0

    iput-object v0, p0, LX/2KK;->p:LX/0gM;

    .line 393732
    :cond_0
    return-void
.end method

.method public static j(LX/2KK;)Z
    .locals 4

    .prologue
    .line 393733
    iget-object v0, p0, LX/2KK;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->cz:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 393734
    iget-object v0, p0, LX/2KK;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->cz:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public static k(LX/2KK;)J
    .locals 4

    .prologue
    const v1, 0x5265c00

    .line 393735
    iget-object v0, p0, LX/2KK;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->cy:J

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    .line 393736
    iget-object v0, p0, LX/2KK;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->cy:J

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method


# virtual methods
.method public final init()V
    .locals 14

    .prologue
    .line 393737
    iget-object v0, p0, LX/2KK;->m:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393738
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/2KK;->b(LX/2KK;Z)V

    .line 393739
    invoke-static {p0}, LX/2KK;->c$redex0(LX/2KK;)V

    .line 393740
    :cond_0
    iget-object v0, p0, LX/2KK;->e:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v2, LX/2KM;

    invoke-direct {v2, p0}, LX/2KM;-><init>(LX/2KK;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 393741
    iget-object v0, p0, LX/2KK;->e:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v2, LX/2KN;

    invoke-direct {v2, p0}, LX/2KN;-><init>(LX/2KK;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 393742
    iget-object v10, p0, LX/2KK;->l:LX/0Or;

    invoke-interface {v10}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0W3;

    sget-wide v12, LX/0X5;->cx:J

    invoke-interface {v10, v12, v13}, LX/0W4;->a(J)Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 393743
    iget-object v10, p0, LX/2KK;->l:LX/0Or;

    invoke-interface {v10}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0W3;

    sget-wide v12, LX/0X5;->cx:J

    invoke-interface {v10, v12, v13}, LX/0W4;->a(J)Z

    move-result v10

    move v3, v10

    .line 393744
    if-nez v3, :cond_1

    .line 393745
    :goto_0
    return-void

    .line 393746
    :cond_1
    iget-object v3, p0, LX/2KK;->r:LX/0Yb;

    if-nez v3, :cond_2

    .line 393747
    iget-object v3, p0, LX/2KK;->f:LX/0Xl;

    invoke-interface {v3}, LX/0Xl;->a()LX/0YX;

    move-result-object v3

    sget-object v4, LX/2KK;->a:Ljava/lang/String;

    new-instance v5, LX/Em8;

    invoke-direct {v5, p0}, LX/Em8;-><init>(LX/2KK;)V

    invoke-interface {v3, v4, v5}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v3

    invoke-interface {v3}, LX/0YX;->a()LX/0Yb;

    move-result-object v3

    iput-object v3, p0, LX/2KK;->r:LX/0Yb;

    .line 393748
    iget-object v3, p0, LX/2KK;->r:LX/0Yb;

    invoke-virtual {v3}, LX/0Yb;->b()V

    .line 393749
    :cond_2
    iget-object v3, p0, LX/2KK;->s:Landroid/app/PendingIntent;

    if-nez v3, :cond_3

    .line 393750
    new-instance v3, Landroid/content/Intent;

    sget-object v4, LX/2KK;->a:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 393751
    iget-object v4, p0, LX/2KK;->o:Landroid/content/Context;

    const/4 v5, 0x0

    const/high16 v6, 0x8000000

    invoke-static {v4, v5, v3, v6}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    iput-object v3, p0, LX/2KK;->s:Landroid/app/PendingIntent;

    .line 393752
    :cond_3
    iget-object v3, p0, LX/2KK;->n:LX/12x;

    iget-object v4, p0, LX/2KK;->s:Landroid/app/PendingIntent;

    invoke-virtual {v3, v4}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 393753
    iget-object v3, p0, LX/2KK;->n:LX/12x;

    invoke-static {p0}, LX/2KK;->j(LX/2KK;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x2

    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    invoke-static {p0}, LX/2KK;->k(LX/2KK;)J

    move-result-wide v7

    add-long/2addr v5, v7

    invoke-static {p0}, LX/2KK;->k(LX/2KK;)J

    move-result-wide v7

    iget-object v9, p0, LX/2KK;->s:Landroid/app/PendingIntent;

    invoke-virtual/range {v3 .. v9}, LX/12x;->a(IJJLandroid/app/PendingIntent;)V

    .line 393754
    invoke-static {p0}, LX/2KK;->k(LX/2KK;)J

    invoke-static {p0}, LX/2KK;->j(LX/2KK;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto :goto_0

    .line 393755
    :cond_4
    const/4 v4, 0x3

    goto :goto_1
.end method
