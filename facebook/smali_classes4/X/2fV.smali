.class public LX/2fV;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/2fY;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 446604
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 446605
    new-instance v0, LX/2o5;

    invoke-direct {v0}, LX/2o5;-><init>()V

    sput-object v0, LX/2fV;->a:LX/2fY;

    .line 446606
    :goto_0
    return-void

    .line 446607
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 446608
    new-instance v0, LX/2fW;

    invoke-direct {v0}, LX/2fW;-><init>()V

    sput-object v0, LX/2fV;->a:LX/2fY;

    goto :goto_0

    .line 446609
    :cond_1
    new-instance v0, LX/2fX;

    invoke-direct {v0}, LX/2fX;-><init>()V

    sput-object v0, LX/2fV;->a:LX/2fY;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 446610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 446611
    return-void
.end method

.method public static a(Landroid/view/accessibility/AccessibilityEvent;)LX/2fO;
    .locals 1

    .prologue
    .line 446612
    new-instance v0, LX/2fO;

    invoke-direct {v0, p0}, LX/2fO;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method
