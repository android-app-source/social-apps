.class public LX/3A0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final H:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile J:LX/3A0;

.field private static final b:Ljava/lang/String;


# instance fields
.field public final A:LX/3E7;

.field private final B:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3E8;",
            ">;"
        }
    .end annotation
.end field

.field private final C:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final D:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/ECp;",
            ">;"
        }
    .end annotation
.end field

.field private E:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79G;",
            ">;"
        }
    .end annotation
.end field

.field private F:LX/3Eb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public G:LX/2EJ;

.field public I:J

.field public a:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3GN;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/2Tm;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/ECY;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/2S7;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/concurrent/ExecutorService;

.field private final j:Landroid/telephony/TelephonyManager;

.field private final k:LX/0yH;

.field private final l:LX/1tu;

.field public final m:Landroid/content/Context;

.field public final n:Lcom/facebook/content/SecureContextHelper;

.field public final o:LX/2S7;

.field private final p:LX/1MZ;

.field private final q:LX/0ka;

.field private final r:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/3A1;

.field private final u:LX/0So;

.field private final v:LX/2CH;

.field private final w:Lcom/facebook/presence/ThreadPresenceManager;

.field private final x:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final y:LX/0ad;

.field private final z:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 524373
    const-class v0, LX/3A0;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3A0;->b:Ljava/lang/String;

    .line 524374
    const-string v0, "PresenceNotLoaded"

    const-string v1, "NotCallable"

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3A0;->H:LX/0Px;

    return-void
.end method

.method private constructor <init>(LX/0Ot;LX/0Or;LX/2Tm;LX/0Or;LX/2S7;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Or;LX/0Or;Landroid/telephony/TelephonyManager;LX/0yH;LX/1tu;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/2S7;LX/1MZ;LX/0ka;LX/3A1;LX/0Or;LX/0So;LX/2CH;Lcom/facebook/presence/ThreadPresenceManager;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2gI;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 2
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/rtcpresence/annotations/IsVoipEnabledForUser;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/rtc/annotations/IsVoipVideoEnabled;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/rtcpresence/annotations/IsVoipBlockedByCountry;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3GN;",
            ">;",
            "LX/0Or",
            "<",
            "LX/EDx;",
            ">;",
            "LX/2Tm;",
            "LX/0Or",
            "<",
            "LX/ECY;",
            ">;",
            "LX/2S7;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/telephony/TelephonyManager;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "Lcom/facebook/push/mqtt/capability/MqttVoipCapability;",
            "Landroid/content/Context;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/2S7;",
            "LX/1MZ;",
            "LX/0ka;",
            "LX/3A1;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0So;",
            "Lcom/facebook/presence/PresenceManager;",
            "Lcom/facebook/presence/ThreadPresenceManager;",
            "LX/0ad;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2gI;",
            "LX/0Or",
            "<",
            "LX/3E8;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;",
            ">;",
            "LX/0Or",
            "<",
            "LX/ECp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 524375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524376
    iput-object p1, p0, LX/3A0;->c:LX/0Ot;

    .line 524377
    iput-object p2, p0, LX/3A0;->d:LX/0Or;

    .line 524378
    iput-object p3, p0, LX/3A0;->e:LX/2Tm;

    .line 524379
    iput-object p4, p0, LX/3A0;->f:LX/0Or;

    .line 524380
    iput-object p5, p0, LX/3A0;->g:LX/2S7;

    .line 524381
    iput-object p7, p0, LX/3A0;->h:LX/0Or;

    .line 524382
    iput-object p6, p0, LX/3A0;->i:Ljava/util/concurrent/ExecutorService;

    .line 524383
    iput-object p10, p0, LX/3A0;->j:Landroid/telephony/TelephonyManager;

    .line 524384
    iput-object p11, p0, LX/3A0;->k:LX/0yH;

    .line 524385
    iput-object p12, p0, LX/3A0;->l:LX/1tu;

    .line 524386
    iput-object p13, p0, LX/3A0;->m:Landroid/content/Context;

    .line 524387
    move-object/from16 v0, p14

    iput-object v0, p0, LX/3A0;->n:Lcom/facebook/content/SecureContextHelper;

    .line 524388
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3A0;->o:LX/2S7;

    .line 524389
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3A0;->p:LX/1MZ;

    .line 524390
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3A0;->q:LX/0ka;

    .line 524391
    iput-object p8, p0, LX/3A0;->r:LX/0Or;

    .line 524392
    iput-object p9, p0, LX/3A0;->s:LX/0Or;

    .line 524393
    move-object/from16 v0, p18

    iput-object v0, p0, LX/3A0;->t:LX/3A1;

    .line 524394
    move-object/from16 v0, p19

    iput-object v0, p0, LX/3A0;->x:LX/0Or;

    .line 524395
    move-object/from16 v0, p20

    iput-object v0, p0, LX/3A0;->u:LX/0So;

    .line 524396
    move-object/from16 v0, p21

    iput-object v0, p0, LX/3A0;->v:LX/2CH;

    .line 524397
    move-object/from16 v0, p22

    iput-object v0, p0, LX/3A0;->w:Lcom/facebook/presence/ThreadPresenceManager;

    .line 524398
    move-object/from16 v0, p23

    iput-object v0, p0, LX/3A0;->y:LX/0ad;

    .line 524399
    move-object/from16 v0, p24

    iput-object v0, p0, LX/3A0;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 524400
    new-instance v1, LX/35Y;

    invoke-direct {v1, p0}, LX/35Y;-><init>(LX/3A0;)V

    move-object/from16 v0, p25

    invoke-virtual {v0, p7, v1}, LX/2gI;->a(LX/0Or;LX/35Y;)LX/3E7;

    move-result-object v1

    iput-object v1, p0, LX/3A0;->A:LX/3E7;

    .line 524401
    move-object/from16 v0, p26

    iput-object v0, p0, LX/3A0;->B:LX/0Or;

    .line 524402
    move-object/from16 v0, p27

    iput-object v0, p0, LX/3A0;->C:LX/0Or;

    .line 524403
    move-object/from16 v0, p28

    iput-object v0, p0, LX/3A0;->D:LX/0Or;

    .line 524404
    return-void
.end method

.method public static a(LX/0QB;)LX/3A0;
    .locals 3

    .prologue
    .line 524405
    sget-object v0, LX/3A0;->J:LX/3A0;

    if-nez v0, :cond_1

    .line 524406
    const-class v1, LX/3A0;

    monitor-enter v1

    .line 524407
    :try_start_0
    sget-object v0, LX/3A0;->J:LX/3A0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 524408
    if-eqz v2, :cond_0

    .line 524409
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/3A0;->b(LX/0QB;)LX/3A0;

    move-result-object v0

    sput-object v0, LX/3A0;->J:LX/3A0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 524410
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 524411
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 524412
    :cond_1
    sget-object v0, LX/3A0;->J:LX/3A0;

    return-object v0

    .line 524413
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 524414
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/3A0;Landroid/content/Context;Lcom/facebook/user/model/UserKey;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)LX/EFc;
    .locals 9

    .prologue
    .line 524415
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v1

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    if-eq v1, v2, :cond_1

    .line 524416
    :cond_0
    sget-object v1, LX/EFc;->UNABLE_TO_CALL:LX/EFc;

    .line 524417
    :goto_0
    return-object v1

    .line 524418
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 524419
    iget-object v3, p0, LX/3A0;->s:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 524420
    const v1, 0x7f08074c

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0807a5

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v1, v2}, LX/3A0;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 524421
    iget-object v1, p0, LX/3A0;->o:LX/2S7;

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/79O;->l:Ljava/lang/String;

    move/from16 v0, p9

    invoke-virtual {v1, v2, p6, v0, v3}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 524422
    sget-object v1, LX/EFc;->UNABLE_TO_CALL:LX/EFc;

    goto :goto_0

    .line 524423
    :cond_2
    iget-object v3, p0, LX/3A0;->q:LX/0ka;

    invoke-virtual {v3}, LX/0ka;->c()Landroid/net/NetworkInfo;

    move-result-object v3

    .line 524424
    iget-object v4, p0, LX/3A0;->y:LX/0ad;

    sget-object v5, LX/0c0;->Cached:LX/0c0;

    sget-short v6, LX/3Dx;->ah:S

    const/4 v7, 0x1

    invoke-interface {v4, v5, v6, v7}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v4

    .line 524425
    if-eqz v4, :cond_4

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v3

    if-nez v3, :cond_4

    .line 524426
    :cond_3
    const v1, 0x7f08074b

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 524427
    const v2, 0x7f0807a8

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 524428
    invoke-direct {p0, p1, v1, v2}, LX/3A0;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 524429
    iget-object v1, p0, LX/3A0;->o:LX/2S7;

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/79O;->f:Ljava/lang/String;

    move/from16 v0, p9

    invoke-virtual {v1, v2, p6, v0, v3}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 524430
    sget-object v1, LX/EFc;->UNABLE_TO_CALL:LX/EFc;

    goto :goto_0

    .line 524431
    :cond_4
    iget-object v3, p0, LX/3A0;->y:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-short v5, LX/3Dx;->ag:S

    const/4 v6, 0x1

    invoke-interface {v3, v4, v5, v6}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v3

    .line 524432
    if-eqz v3, :cond_5

    iget-object v3, p0, LX/3A0;->p:LX/1MZ;

    invoke-virtual {v3}, LX/1MZ;->e()Z

    move-result v3

    if-nez v3, :cond_5

    .line 524433
    iget-object v1, p0, LX/3A0;->A:LX/3E7;

    invoke-virtual {v1}, LX/3E7;->a()V

    .line 524434
    const v1, 0x7f08074b

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 524435
    const v2, 0x7f0807a8

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 524436
    invoke-direct {p0, p1, v1, v2}, LX/3A0;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 524437
    iget-object v1, p0, LX/3A0;->o:LX/2S7;

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/79O;->g:Ljava/lang/String;

    move/from16 v0, p9

    invoke-virtual {v1, v2, p6, v0, v3}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 524438
    sget-object v1, LX/EFc;->UNABLE_TO_CALL:LX/EFc;

    goto/16 :goto_0

    .line 524439
    :cond_5
    if-nez p3, :cond_7

    .line 524440
    sget-object v3, LX/3A0;->H:LX/0Px;

    invoke-virtual {v3, p5}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 524441
    if-nez v3, :cond_6

    sget-object v4, LX/79O;->a:Ljava/lang/String;

    invoke-virtual {v4, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, LX/3A0;->y:LX/0ad;

    sget-short v5, LX/3Dx;->bY:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 524442
    const/4 v3, 0x1

    .line 524443
    :cond_6
    if-nez v3, :cond_7

    .line 524444
    const/4 v1, 0x0

    invoke-direct {p0, p1, p4, v1}, LX/3A0;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 524445
    iget-object v1, p0, LX/3A0;->o:LX/2S7;

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    move/from16 v0, p9

    invoke-virtual {v1, v2, p6, v0, p5}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 524446
    sget-object v1, LX/EFc;->UNABLE_TO_CALL:LX/EFc;

    goto/16 :goto_0

    .line 524447
    :cond_7
    if-eqz p9, :cond_8

    iget-object v3, p0, LX/3A0;->r:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v6, 0x1

    .line 524448
    :goto_1
    invoke-direct {p0, p2, p5}, LX/3A0;->a(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 524449
    invoke-direct {p0, p2}, LX/3A0;->a(Lcom/facebook/user/model/UserKey;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 524450
    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, LX/3A0;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object v3, p6

    move-wide/from16 v4, p7

    .line 524451
    invoke-static/range {v1 .. v8}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a(JLjava/lang/String;JZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/rtc/helpers/RtcCallStartParams;

    move-result-object v1

    .line 524452
    invoke-static {p0, v1}, LX/3A0;->g(LX/3A0;Lcom/facebook/rtc/helpers/RtcCallStartParams;)Z

    move-result v1

    .line 524453
    if-nez v1, :cond_9

    .line 524454
    const v1, 0x7f08074d

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1, v1}, LX/3A0;->a(LX/3A0;Landroid/content/Context;Ljava/lang/String;)V

    .line 524455
    iget-object v1, p0, LX/3A0;->o:LX/2S7;

    const/4 v2, 0x0

    sget-object v3, LX/79O;->i:Ljava/lang/String;

    invoke-virtual {v1, v8, p6, v2, v3}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 524456
    sget-object v1, LX/EFc;->UNABLE_TO_CALL:LX/EFc;

    goto/16 :goto_0

    .line 524457
    :cond_8
    const/4 v6, 0x0

    goto :goto_1

    .line 524458
    :cond_9
    sget-object v1, LX/EFc;->CALL_STARTED:LX/EFc;

    goto/16 :goto_0

    :cond_a
    move-object v3, p6

    move-wide/from16 v4, p7

    .line 524459
    invoke-static/range {v1 .. v7}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a(JLjava/lang/String;JZLjava/lang/String;)Lcom/facebook/rtc/helpers/RtcCallStartParams;

    move-result-object v1

    .line 524460
    invoke-direct {p0, v1}, LX/3A0;->d(Lcom/facebook/rtc/helpers/RtcCallStartParams;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 524461
    const v1, 0x7f08074d

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1, v1}, LX/3A0;->a(LX/3A0;Landroid/content/Context;Ljava/lang/String;)V

    .line 524462
    iget-object v1, p0, LX/3A0;->o:LX/2S7;

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/79O;->i:Ljava/lang/String;

    move/from16 v0, p9

    invoke-virtual {v1, v2, p6, v0, v3}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 524463
    sget-object v1, LX/EFc;->UNABLE_TO_CALL:LX/EFc;

    goto/16 :goto_0

    .line 524464
    :cond_b
    sget-object v1, LX/EFc;->CALL_STARTED:LX/EFc;

    goto/16 :goto_0
.end method

.method private a(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    .line 524465
    iget-object v0, p0, LX/3A0;->t:LX/3A1;

    invoke-virtual {v0, p1}, LX/3A1;->a(Lcom/facebook/user/model/UserKey;)LX/79S;

    move-result-object v1

    .line 524466
    iget-object v0, p0, LX/3A0;->v:LX/2CH;

    invoke-virtual {v0, p1}, LX/2CH;->d(Lcom/facebook/user/model/UserKey;)LX/3Ox;

    move-result-object v0

    .line 524467
    iget-object v2, v0, LX/3Ox;->b:LX/3Oz;

    move-object v0, v2

    .line 524468
    sget-object v2, LX/3Oz;->AVAILABLE:LX/3Oz;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    .line 524469
    :goto_0
    iget-object v2, p0, LX/3A0;->w:Lcom/facebook/presence/ThreadPresenceManager;

    invoke-virtual {v2, p1}, Lcom/facebook/presence/ThreadPresenceManager;->b(Lcom/facebook/user/model/UserKey;)Z

    move-result v2

    .line 524470
    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    .line 524471
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 524472
    const-string v4, "disabled_reason_id"

    invoke-virtual {v3, v4, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 524473
    :cond_0
    const-string v4, "presence_cache_age"

    iget-object v5, p0, LX/3A0;->u:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    .line 524474
    iget-wide v10, v1, LX/79S;->e:J

    move-wide v8, v10

    .line 524475
    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 524476
    const-string v4, "active"

    if-eqz v0, :cond_2

    const-string v0, "1"

    :goto_1
    invoke-virtual {v3, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 524477
    const-string v4, "copresent"

    if-eqz v2, :cond_3

    const-string v0, "1"

    :goto_2
    invoke-virtual {v3, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 524478
    iget-object v0, v1, LX/79S;->f:LX/0P1;

    move-object v0, v0

    .line 524479
    invoke-virtual {v3, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 524480
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-static {v0}, LX/16N;->a(Ljava/util/Map;)LX/0m9;

    move-result-object v0

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 524481
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 524482
    :cond_2
    const-string v0, "0"

    goto :goto_1

    .line 524483
    :cond_3
    const-string v0, "0"

    goto :goto_2
.end method

.method private static a(LX/3A0;LX/0Ot;LX/0Ot;LX/3Eb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3A0;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/79G;",
            ">;",
            "LX/3Eb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 524484
    iput-object p1, p0, LX/3A0;->a:LX/0Ot;

    iput-object p2, p0, LX/3A0;->E:LX/0Ot;

    iput-object p3, p0, LX/3A0;->F:LX/3Eb;

    return-void
.end method

.method private static a(LX/3A0;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 524370
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3A0;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 524371
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 524485
    if-nez p2, :cond_0

    .line 524486
    const v0, 0x7f08074a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 524487
    :cond_0
    if-nez p3, :cond_1

    .line 524488
    const v0, 0x7f080749

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 524489
    :cond_1
    new-instance v0, LX/31Y;

    invoke-direct {v0, p1}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p3}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v2, 0x7f080016

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/EFZ;

    invoke-direct {v3, p0}, LX/EFZ;-><init>(LX/3A0;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, LX/3A0;->G:LX/2EJ;

    .line 524490
    iget-object v0, p0, LX/3A0;->G:LX/2EJ;

    new-instance v2, LX/EFa;

    invoke-direct {v2, p0}, LX/EFa;-><init>(LX/3A0;)V

    invoke-virtual {v0, v2}, LX/2EJ;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 524491
    const/4 v0, 0x0

    .line 524492
    :try_start_0
    iget-object v2, p0, LX/3A0;->G:LX/2EJ;

    invoke-virtual {v2}, LX/2EJ;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 524493
    :goto_0
    if-eqz v0, :cond_2

    .line 524494
    const/4 v0, 0x0

    iput-object v0, p0, LX/3A0;->G:LX/2EJ;

    .line 524495
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/rtc/activities/RtcDialogActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.facebook.rtc.activities.intent.action.ACTION_DIALOG"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "MESSAGE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "TITLE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 524496
    iget-object v1, p0, LX/3A0;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/3A0;->m:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 524497
    :cond_2
    return-void

    .line 524498
    :catch_0
    move v0, v1

    .line 524499
    goto :goto_0

    .line 524500
    :catch_1
    move v0, v1

    goto :goto_0
.end method

.method private a(Lcom/facebook/rtc/helpers/RtcCallStartParams;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 524501
    iget-object v0, p0, LX/3A0;->k:LX/0yH;

    sget-object v1, LX/0yY;->VOIP_CALL_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524502
    invoke-static {p1, p2}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a(Lcom/facebook/rtc/helpers/RtcCallStartParams;Ljava/lang/String;)Lcom/facebook/rtc/helpers/RtcCallStartParams;

    move-result-object v0

    .line 524503
    new-instance v1, Landroid/content/Intent;

    iget-object p1, p0, LX/3A0;->m:Landroid/content/Context;

    const-class p2, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;

    invoke-direct {v1, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 524504
    const-string p1, "ACTION_START_CALL"

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 524505
    const-string p1, "EXTRA_CALL_PARAMS"

    invoke-virtual {v1, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 524506
    const/high16 p1, 0x10000000

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 524507
    iget-object p1, p0, LX/3A0;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object p2, p0, LX/3A0;->m:Landroid/content/Context;

    invoke-interface {p1, v1, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 524508
    :goto_0
    return-void

    .line 524509
    :cond_0
    invoke-static {p1, p2}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a(Lcom/facebook/rtc/helpers/RtcCallStartParams;Ljava/lang/String;)Lcom/facebook/rtc/helpers/RtcCallStartParams;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/3A0;->a(Lcom/facebook/rtc/helpers/RtcCallStartParams;Z)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)V
    .locals 8
    .param p3    # [Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 524521
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 524522
    :cond_0
    sget-object v0, LX/3A0;->b:Ljava/lang/String;

    const-string v1, "Cannot start group call flow due to not well-formed input parameters"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524523
    :cond_1
    :goto_0
    return-void

    .line 524524
    :cond_2
    if-eqz p2, :cond_3

    array-length v0, p2

    if-nez v0, :cond_4

    .line 524525
    :cond_3
    sget-object v0, LX/3A0;->b:Ljava/lang/String;

    const-string v1, "Cannot start group call flow empty thread participants"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 524526
    :cond_4
    invoke-static {p1}, LX/3A0;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 524527
    const-string v4, ""

    invoke-static {p2}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    if-nez p3, :cond_5

    .line 524528
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 524529
    :goto_1
    const/4 v7, 0x0

    move-object v0, p5

    move v1, p4

    move-object v3, p1

    invoke-static/range {v0 .. v7}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;)Lcom/facebook/rtc/helpers/RtcCallStartParams;

    move-result-object v1

    .line 524530
    iget-object v0, p0, LX/3A0;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79G;

    .line 524531
    invoke-static {v0}, LX/79G;->f(LX/79G;)V

    .line 524532
    const-string v3, "UI_START_CALL"

    invoke-static {p4, p5}, LX/79G;->b(ZLjava/lang/String;)LX/1rQ;

    move-result-object v4

    invoke-static {v0, v3, v4}, LX/79G;->a(LX/79G;Ljava/lang/String;LX/1rQ;)V

    .line 524533
    invoke-static {p0, v1}, LX/3A0;->g(LX/3A0;Lcom/facebook/rtc/helpers/RtcCallStartParams;)Z

    move-result v0

    .line 524534
    if-nez v0, :cond_1

    .line 524535
    const v0, 0x7f08074d

    invoke-virtual {p6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p6, v0}, LX/3A0;->a(LX/3A0;Landroid/content/Context;Ljava/lang/String;)V

    .line 524536
    iget-object v0, p0, LX/3A0;->o:LX/2S7;

    const/4 v1, 0x0

    sget-object v3, LX/79O;->i:Ljava/lang/String;

    invoke-virtual {v0, v2, p5, v1, v3}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0

    .line 524537
    :cond_5
    invoke-static {p3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    goto :goto_1
.end method

.method private a(Lcom/facebook/user/model/UserKey;)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 524598
    iget-object v2, p0, LX/3A0;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/EGK;->g:LX/0Tn;

    const-string v4, "-1"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 524599
    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    .line 524600
    iget-object v1, p0, LX/3A0;->v:LX/2CH;

    const-wide/16 v7, 0x0

    .line 524601
    invoke-static {v1}, LX/2CH;->z(LX/2CH;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez p1, :cond_4

    :cond_0
    move-wide v5, v7

    .line 524602
    :goto_0
    move-wide v2, v5

    .line 524603
    sget-object v5, LX/1tp;->ONE_ON_ONE_OVER_MULTIWAY:LX/1tp;

    invoke-static {v5}, LX/1tz;->a(Ljava/lang/Enum;)J

    move-result-wide v5

    and-long/2addr v5, v2

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_8

    const/4 v5, 0x1

    :goto_1
    move v1, v5

    .line 524604
    if-nez v1, :cond_2

    .line 524605
    :cond_1
    :goto_2
    return v0

    .line 524606
    :cond_2
    iget-object v1, p0, LX/3A0;->y:LX/0ad;

    sget-short v2, LX/3Dx;->i:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_2

    .line 524607
    :cond_3
    if-ne v2, v1, :cond_1

    move v0, v1

    goto :goto_2

    .line 524608
    :cond_4
    iget-object v5, v1, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v5, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/3hU;

    .line 524609
    if-eqz v5, :cond_5

    iget-wide v9, v5, LX/3hU;->g:J

    cmp-long v6, v9, v7

    if-nez v6, :cond_6

    :cond_5
    move-wide v5, v7

    .line 524610
    goto :goto_0

    .line 524611
    :cond_6
    iget-object v6, v1, LX/2CH;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1MZ;

    invoke-virtual {v6}, LX/1MZ;->e()Z

    move-result v6

    if-nez v6, :cond_7

    move-wide v5, v7

    .line 524612
    goto :goto_0

    .line 524613
    :cond_7
    iget-wide v5, v5, LX/3hU;->g:J

    goto :goto_0

    :cond_8
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/messaging/model/threads/ThreadSummary;)[Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 524588
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    if-nez v0, :cond_1

    .line 524589
    :cond_0
    sget-object v0, LX/3A0;->b:Ljava/lang/String;

    const-string v1, "Cannot getMultiwayParticipantsFromThreadSummary due to null ThreadSummary"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524590
    const/4 v0, 0x0

    .line 524591
    :goto_0
    return-object v0

    .line 524592
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 524593
    iget-object v3, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 524594
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 524595
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 524596
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 524597
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/3A0;
    .locals 31

    .prologue
    .line 524585
    new-instance v2, LX/3A0;

    const/16 v3, 0x10dd

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x3257

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/2Tm;->a(LX/0QB;)LX/2Tm;

    move-result-object v5

    check-cast v5, LX/2Tm;

    const/16 v6, 0x324e

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static/range {p0 .. p0}, LX/2S7;->a(LX/0QB;)LX/2S7;

    move-result-object v7

    check-cast v7, LX/2S7;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    const/16 v9, 0x1568

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x1561

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x1566

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/0e7;->a(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v12

    check-cast v12, Landroid/telephony/TelephonyManager;

    invoke-static/range {p0 .. p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v13

    check-cast v13, LX/0yH;

    invoke-static/range {p0 .. p0}, LX/1tu;->a(LX/0QB;)LX/1tu;

    move-result-object v14

    check-cast v14, LX/1tu;

    const-class v15, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v16

    check-cast v16, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/2S7;->a(LX/0QB;)LX/2S7;

    move-result-object v17

    check-cast v17, LX/2S7;

    invoke-static/range {p0 .. p0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v18

    check-cast v18, LX/1MZ;

    invoke-static/range {p0 .. p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v19

    check-cast v19, LX/0ka;

    invoke-static/range {p0 .. p0}, LX/3A1;->a(LX/0QB;)LX/3A1;

    move-result-object v20

    check-cast v20, LX/3A1;

    const/16 v21, 0x19e

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v22

    check-cast v22, LX/0So;

    invoke-static/range {p0 .. p0}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v23

    check-cast v23, LX/2CH;

    invoke-static/range {p0 .. p0}, Lcom/facebook/presence/ThreadPresenceManager;->a(LX/0QB;)Lcom/facebook/presence/ThreadPresenceManager;

    move-result-object v24

    check-cast v24, Lcom/facebook/presence/ThreadPresenceManager;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v25

    check-cast v25, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v26

    check-cast v26, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v27, LX/2gI;

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/2gI;

    const/16 v28, 0x10e4

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v28

    const/16 v29, 0x10e1

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v29

    const/16 v30, 0x3250

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v30

    invoke-direct/range {v2 .. v30}, LX/3A0;-><init>(LX/0Ot;LX/0Or;LX/2Tm;LX/0Or;LX/2S7;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Or;LX/0Or;Landroid/telephony/TelephonyManager;LX/0yH;LX/1tu;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/2S7;LX/1MZ;LX/0ka;LX/3A1;LX/0Or;LX/0So;LX/2CH;Lcom/facebook/presence/ThreadPresenceManager;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2gI;LX/0Or;LX/0Or;LX/0Or;)V

    .line 524586
    const/16 v3, 0x1430

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v3, 0x3266

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/3Eb;->a(LX/0QB;)LX/3Eb;

    move-result-object v3

    check-cast v3, LX/3Eb;

    invoke-static {v2, v4, v5, v3}, LX/3A0;->a(LX/3A0;LX/0Ot;LX/0Ot;LX/3Eb;)V

    .line 524587
    return-object v2
.end method

.method private d(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 524567
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524568
    const/4 v0, 0x0

    .line 524569
    :goto_0
    return-object v0

    .line 524570
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 524571
    const-string v0, "PEER:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524572
    iget-object v0, p0, LX/3A0;->x:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 524573
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 524574
    iget-object v4, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v4, v4

    .line 524575
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 524576
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524577
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524578
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v2

    .line 524579
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524580
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 524581
    :cond_1
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v2

    .line 524582
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524583
    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524584
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private d(Lcom/facebook/rtc/helpers/RtcCallStartParams;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 524545
    invoke-static {p0}, LX/3A0;->i(LX/3A0;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 524546
    :cond_0
    :goto_0
    return v0

    .line 524547
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 524548
    invoke-virtual {p0}, LX/3A0;->e()V

    .line 524549
    invoke-virtual {p0}, LX/3A0;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 524550
    iget-object v0, p0, LX/3A0;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-static {p1, v0}, LX/EFY;->a(Lcom/facebook/rtc/helpers/RtcCallStartParams;LX/EDx;)Z

    move-result v0

    goto :goto_0

    .line 524551
    :cond_2
    invoke-static {p0}, LX/3A0;->j(LX/3A0;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 524552
    :cond_3
    iget-object v0, p0, LX/3A0;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3GN;

    .line 524553
    iget-object v1, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->k:LX/0Px;

    move-object v1, v1

    .line 524554
    iput-object v1, v0, LX/3GN;->a:LX/0Px;

    .line 524555
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_4

    iget-object v0, p0, LX/3A0;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 524556
    iget-boolean v1, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    invoke-static {v0, v1}, LX/EDx;->p(LX/EDx;Z)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 524557
    const/4 v1, 0x0

    .line 524558
    :goto_1
    move v0, v1

    .line 524559
    if-nez v0, :cond_5

    .line 524560
    :cond_4
    invoke-virtual {p0, p1}, LX/3A0;->b(Lcom/facebook/rtc/helpers/RtcCallStartParams;)V

    .line 524561
    :cond_5
    const/4 v0, 0x1

    goto :goto_0

    .line 524562
    :cond_6
    new-instance v1, Landroid/content/Intent;

    iget-object v2, v0, LX/EDx;->h:Landroid/content/Context;

    const-class v3, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 524563
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 524564
    const-string v2, "StartParams"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 524565
    iget-object v2, v0, LX/EDx;->j:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, v0, LX/EDx;->h:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 524566
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 524538
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524539
    const/4 v0, 0x0

    .line 524540
    :goto_0
    return-object v0

    .line 524541
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 524542
    const-string v1, "GROUP:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524543
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524544
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static g(LX/3A0;Lcom/facebook/rtc/helpers/RtcCallStartParams;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 524353
    invoke-static {p0}, LX/3A0;->i(LX/3A0;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 524354
    :cond_0
    :goto_0
    return v0

    .line 524355
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 524356
    invoke-virtual {p0}, LX/3A0;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 524357
    iget-object v0, p0, LX/3A0;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-static {p1, v0}, LX/EFY;->a(Lcom/facebook/rtc/helpers/RtcCallStartParams;LX/EDx;)Z

    move-result v0

    goto :goto_0

    .line 524358
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_3

    iget-object v0, p0, LX/3A0;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 524359
    iget-boolean v1, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    invoke-static {v0, v1}, LX/EDx;->p(LX/EDx;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 524360
    const/4 v1, 0x0

    .line 524361
    :goto_1
    move v0, v1

    .line 524362
    if-nez v0, :cond_4

    .line 524363
    :cond_3
    invoke-virtual {p0, p1}, LX/3A0;->c(Lcom/facebook/rtc/helpers/RtcCallStartParams;)V

    .line 524364
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 524365
    :cond_5
    new-instance v1, Landroid/content/Intent;

    iget-object v2, v0, LX/EDx;->h:Landroid/content/Context;

    const-class v3, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 524366
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 524367
    const-string v2, "StartParams"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 524368
    iget-object v2, v0, LX/EDx;->j:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, v0, LX/EDx;->h:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 524369
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private static h(Lcom/facebook/rtc/helpers/RtcCallStartParams;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 524510
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    .line 524511
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 524512
    const-string v0, "unknown"

    .line 524513
    :cond_0
    return-object v0
.end method

.method private h()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 524515
    iget-object v1, p0, LX/3A0;->y:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/3Dx;->eo:S

    invoke-interface {v1, v2, v3, v4, v0}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/3A0;->y:LX/0ad;

    sget-short v2, LX/3Dx;->E:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 524516
    :cond_0
    iget-object v1, p0, LX/3A0;->e:LX/2Tm;

    .line 524517
    invoke-virtual {v1}, LX/2Tm;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 524518
    iget-object v2, v1, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v2, v0}, Lcom/facebook/webrtc/WebrtcEngine;->setUseAppLevelCamera(Z)V

    .line 524519
    iput-boolean v0, v1, LX/2Tm;->y:Z

    .line 524520
    :cond_1
    return-void
.end method

.method private static i(LX/3A0;)Z
    .locals 1

    .prologue
    .line 524514
    iget-object v0, p0, LX/3A0;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private static j(LX/3A0;)Z
    .locals 1

    .prologue
    .line 524372
    iget-object v0, p0, LX/3A0;->j:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3A0;->j:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0c1;)I
    .locals 4

    .prologue
    .line 524191
    iget-object v0, p0, LX/3A0;->y:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget v2, LX/3Dx;->a:I

    const/4 v3, 0x0

    invoke-interface {v0, v1, p1, v2, v3}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/user/model/UserKey;Ljava/lang/String;)LX/EFc;
    .locals 12

    .prologue
    .line 524238
    iget-object v0, p0, LX/3A0;->t:LX/3A1;

    invoke-virtual {v0, p2}, LX/3A1;->a(Lcom/facebook/user/model/UserKey;)LX/79S;

    move-result-object v0

    .line 524239
    iget-boolean v1, v0, LX/79S;->a:Z

    move v4, v1

    .line 524240
    iget-object v1, v0, LX/79S;->c:Ljava/lang/String;

    move-object v5, v1

    .line 524241
    iget-object v1, v0, LX/79S;->d:Ljava/lang/String;

    move-object v6, v1

    .line 524242
    iget-wide v10, v0, LX/79S;->e:J

    move-wide v8, v10

    .line 524243
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v7, p3

    invoke-virtual/range {v1 .. v9}, LX/3A0;->b(Landroid/content/Context;Lcom/facebook/user/model/UserKey;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)LX/EFc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/user/model/UserKey;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)LX/EFc;
    .locals 11

    .prologue
    .line 524237
    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-wide/from16 v8, p7

    invoke-static/range {v1 .. v10}, LX/3A0;->a(LX/3A0;Landroid/content/Context;Lcom/facebook/user/model/UserKey;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)LX/EFc;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0lF;)V
    .locals 11

    .prologue
    .line 524222
    invoke-static {p0}, LX/3A0;->i(LX/3A0;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 524223
    :cond_0
    :goto_0
    return-void

    .line 524224
    :cond_1
    const-string v0, "uid"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    move-result-wide v2

    .line 524225
    const-string v0, "capabilities"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    move-result-wide v0

    .line 524226
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_2

    .line 524227
    sget-object v0, LX/3A0;->b:Ljava/lang/String;

    const-string v1, "No valid uid in gcm payload"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 524228
    :cond_2
    iget-object v4, p0, LX/3A0;->l:LX/1tu;

    invoke-virtual {v4, v0, v1}, LX/1tu;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524229
    iget-object v0, p0, LX/3A0;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECY;

    .line 524230
    iget-object v6, v0, LX/ECY;->t:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_3

    iget-object v6, v0, LX/ECY;->o:LX/2S3;

    invoke-virtual {v6}, LX/2S3;->a()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 524231
    :goto_1
    goto :goto_0

    .line 524232
    :cond_3
    iget-object v6, v0, LX/ECY;->o:LX/2S3;

    invoke-virtual {v6}, LX/2S3;->a()Z

    move-result v6

    if-nez v6, :cond_4

    .line 524233
    iget-object v6, v0, LX/ECY;->o:LX/2S3;

    invoke-virtual {v6}, LX/2S3;->init()V

    .line 524234
    :cond_4
    iget-object v6, v0, LX/ECY;->o:LX/2S3;

    invoke-virtual {v6}, LX/2S3;->a()Z

    move-result v6

    if-nez v6, :cond_5

    .line 524235
    iget-object v6, v0, LX/ECY;->j:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v7, Lcom/facebook/rtc/campon/RtcCampOnManager$8;

    invoke-direct {v7, v0, v2, v3}, Lcom/facebook/rtc/campon/RtcCampOnManager$8;-><init>(LX/ECY;J)V

    const-wide/16 v8, 0x5

    sget-object v10, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v6, v7, v8, v9, v10}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_1

    .line 524236
    :cond_5
    iget-object v6, v0, LX/ECY;->j:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v7, Lcom/facebook/rtc/campon/RtcCampOnManager$9;

    invoke-direct {v7, v0, v2, v3}, Lcom/facebook/rtc/campon/RtcCampOnManager$9;-><init>(LX/ECY;J)V

    const v8, 0x753cf4b2

    invoke-static {v6, v7, v8}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/messaging/model/threads/ThreadSummary;ZLjava/lang/String;)V
    .locals 6

    .prologue
    .line 524217
    iget-object v0, p0, LX/3A0;->y:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/3Dx;->aK:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    .line 524218
    if-eqz p3, :cond_0

    if-eqz v0, :cond_0

    .line 524219
    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v5, p0, LX/3A0;->m:Landroid/content/Context;

    move-object v0, p0

    move-object v1, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/3A0;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;[Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)V

    .line 524220
    :goto_0
    return-void

    .line 524221
    :cond_0
    iget-object v0, p0, LX/3A0;->D:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECp;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/ECp;->a(Landroid/content/Context;Lcom/facebook/messaging/model/threads/ThreadSummary;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)V
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 524202
    iget-object v0, p0, LX/3A0;->x:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 524203
    if-nez v0, :cond_1

    .line 524204
    sget-object v0, LX/3A0;->b:Ljava/lang/String;

    const-string v1, "Cannot join multiway call due to null viewer context"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524205
    :cond_0
    :goto_0
    return-void

    .line 524206
    :cond_1
    invoke-static {p1}, LX/3A0;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)[Ljava/lang/String;

    move-result-object v0

    .line 524207
    if-nez v0, :cond_2

    .line 524208
    sget-object v0, LX/3A0;->b:Ljava/lang/String;

    const-string v1, "Cannot join multiway call due to null participants list"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 524209
    :cond_2
    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 524210
    invoke-static {v3}, LX/3A0;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 524211
    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p4

    move v1, p3

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;)Lcom/facebook/rtc/helpers/RtcCallStartParams;

    move-result-object v1

    .line 524212
    iget-object v0, p0, LX/3A0;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79G;

    const/4 v3, 0x1

    invoke-virtual {v0, p3, v3, p4}, LX/79G;->a(ZZLjava/lang/String;)V

    .line 524213
    invoke-static {p0, v1}, LX/3A0;->g(LX/3A0;Lcom/facebook/rtc/helpers/RtcCallStartParams;)Z

    move-result v0

    .line 524214
    if-nez v0, :cond_0

    .line 524215
    const v0, 0x7f08074d

    invoke-virtual {p5, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p5, v0}, LX/3A0;->a(LX/3A0;Landroid/content/Context;Ljava/lang/String;)V

    .line 524216
    iget-object v0, p0, LX/3A0;->o:LX/2S7;

    const/4 v1, 0x0

    sget-object v3, LX/79O;->i:Ljava/lang/String;

    invoke-virtual {v0, v2, p4, v1, v3}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;[Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)V
    .locals 7
    .param p1    # Lcom/facebook/messaging/model/threads/ThreadSummary;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # [Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 524196
    if-nez p1, :cond_0

    .line 524197
    sget-object v0, LX/3A0;->b:Ljava/lang/String;

    const-string v1, "Cannot start group call flow due to not well-formed input parameters"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524198
    :goto_0
    return-void

    .line 524199
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 524200
    invoke-static {p1}, LX/3A0;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)[Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 524201
    invoke-direct/range {v0 .. v6}, LX/3A0;->a(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/rtc/helpers/RtcCallStartParams;)V
    .locals 1

    .prologue
    .line 524192
    invoke-virtual {p1}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 524193
    :goto_0
    return-void

    .line 524194
    :cond_0
    invoke-virtual {p0}, LX/3A0;->e()V

    .line 524195
    iget-object v0, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, LX/3A0;->a(Lcom/facebook/rtc/helpers/RtcCallStartParams;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/rtc/helpers/RtcCallStartParams;Z)V
    .locals 17

    .prologue
    .line 524175
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3A0;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    invoke-virtual {v2}, LX/EDx;->U()Ljava/lang/String;

    move-result-object v10

    .line 524176
    :goto_0
    invoke-direct/range {p0 .. p0}, LX/3A0;->h()V

    .line 524177
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 524178
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3A0;->e:LX/2Tm;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->b:J

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->i:Ljava/lang/String;

    new-instance v2, LX/7TR;

    invoke-direct {v2}, LX/7TR;-><init>()V

    const/4 v12, 0x0

    invoke-virtual {v2, v12}, LX/7TR;->a(Z)LX/7TR;

    move-result-object v2

    invoke-virtual {v2}, LX/7TR;->a()[B

    move-result-object v12

    invoke-virtual/range {v3 .. v12}, LX/2Tm;->a(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    .line 524179
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3A0;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/EDx;->c(Lcom/facebook/rtc/helpers/RtcCallStartParams;)V

    .line 524180
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3A0;->g:LX/2S7;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->e:J

    invoke-virtual {v2, v3, v4, v5}, LX/2S7;->a(Ljava/lang/String;J)V

    .line 524181
    if-eqz p2, :cond_0

    .line 524182
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3A0;->g:LX/2S7;

    const-string v3, "zero_rating_shown"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, LX/2S7;->b(Ljava/lang/String;Z)Z

    .line 524183
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3A0;->g:LX/2S7;

    const-string v3, "zero_rating_accepted"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, LX/2S7;->b(Ljava/lang/String;Z)Z

    .line 524184
    :cond_0
    return-void

    .line 524185
    :cond_1
    const-string v10, ""

    goto :goto_0

    .line 524186
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 524187
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3A0;->e:LX/2Tm;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->b:J

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->i:Ljava/lang/String;

    new-instance v2, LX/7TR;

    invoke-direct {v2}, LX/7TR;-><init>()V

    const/4 v12, 0x0

    invoke-virtual {v2, v12}, LX/7TR;->a(Z)LX/7TR;

    move-result-object v2

    invoke-virtual {v2}, LX/7TR;->a()[B

    move-result-object v12

    invoke-virtual/range {v3 .. v12}, LX/2Tm;->b(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    goto :goto_1

    .line 524188
    :cond_3
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    sget-object v3, LX/0c1;->On:LX/0c1;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/3A0;->a(ZLX/0c1;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 524189
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3A0;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lcom/facebook/rtc/helpers/RtcCallHandler$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1, v10}, Lcom/facebook/rtc/helpers/RtcCallHandler$3;-><init>(LX/3A0;Lcom/facebook/rtc/helpers/RtcCallStartParams;Ljava/lang/String;)V

    sget-object v4, LX/0c1;->Off:LX/0c1;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/3A0;->a(LX/0c1;)I

    move-result v4

    int-to-long v4, v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v4, v5, v6}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto/16 :goto_1

    .line 524190
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, LX/3A0;->e:LX/2Tm;

    move-object/from16 v0, p1

    iget-wide v7, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-boolean v11, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    move-object/from16 v0, p1

    iget-boolean v12, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    const/4 v13, 0x1

    const/4 v14, 0x1

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->i:Ljava/lang/String;

    new-instance v2, LX/7TR;

    invoke-direct {v2}, LX/7TR;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/7TR;->a(Z)LX/7TR;

    move-result-object v2

    invoke-virtual {v2}, LX/7TR;->a()[B

    move-result-object v16

    invoke-virtual/range {v6 .. v16}, LX/2Tm;->a(JLjava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;[B)V

    goto/16 :goto_1
.end method

.method public final a(ZLX/0c1;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 524244
    iget-object v0, p0, LX/3A0;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 524245
    :goto_0
    return v0

    .line 524246
    :cond_0
    if-eqz p1, :cond_2

    .line 524247
    iget-object v0, p0, LX/3A0;->y:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget v4, LX/3Dx;->c:I

    const/4 p1, 0x0

    invoke-interface {v0, v3, p2, v4, p1}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    move v0, v0

    .line 524248
    if-lez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 524249
    :cond_2
    invoke-virtual {p0, p2}, LX/3A0;->a(LX/0c1;)I

    move-result v0

    if-lez v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/facebook/user/model/UserKey;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)LX/EFc;
    .locals 11

    .prologue
    .line 524250
    const/4 v10, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-wide/from16 v8, p7

    invoke-static/range {v1 .. v10}, LX/3A0;->a(LX/3A0;Landroid/content/Context;Lcom/facebook/user/model/UserKey;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)LX/EFc;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0lF;)V
    .locals 11

    .prologue
    .line 524251
    iget-object v0, p0, LX/3A0;->A:LX/3E7;

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 524252
    const-string v1, "trace_info"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 524253
    const-string v1, "topic"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 524254
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 524255
    const-string v1, "RtcSignalingHandler"

    const-string v2, "Missing topic type for gcm webrtc push message"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524256
    const-string v1, "push_missing_topic"

    invoke-static {v0, v4, v1}, LX/3E7;->a(LX/3E7;Ljava/lang/String;Ljava/lang/String;)V

    .line 524257
    :cond_0
    :goto_0
    return-void

    .line 524258
    :cond_1
    const-string v1, "uid"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->c(LX/0lF;)J

    move-result-wide v7

    .line 524259
    const-wide/16 v9, 0x0

    cmp-long v1, v7, v9

    if-nez v1, :cond_2

    const-string v1, "t_rtc_multi"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 524260
    const-string v1, "RtcSignalingHandler"

    const-string v2, "No valid uid in gcm payload"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524261
    const-string v1, "push_missing_uid"

    invoke-static {v0, v4, v1}, LX/3E7;->a(LX/3E7;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 524262
    :cond_2
    const/4 v1, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_3
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 524263
    const-string v1, "RtcSignalingHandler"

    const-string v6, "Invalid topic type: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v5, v3, v2

    invoke-static {v1, v6, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 524264
    const-string v1, "push_invalid_topic"

    invoke-static {v0, v4, v1}, LX/3E7;->a(LX/3E7;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 524265
    :sswitch_0
    const-string v6, "t_rtc"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v1, v2

    goto :goto_1

    :sswitch_1
    const-string v6, "webrtc"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v1, v3

    goto :goto_1

    :sswitch_2
    const-string v6, "t_rtc_multi"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v1, 0x2

    goto :goto_1

    .line 524266
    :pswitch_0
    :try_start_0
    const-string v1, "payload"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->t()[B

    move-result-object v1

    .line 524267
    new-instance v2, LX/3ll;

    invoke-direct {v2, v4}, LX/3ll;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3}, LX/3E7;->a(LX/3E7;LX/3ll;[BZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 524268
    :catch_0
    move-exception v1

    .line 524269
    const-string v2, "RtcSignalingHandler"

    const-string v3, "Error reading gcm thrift payload"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 524270
    const-string v1, "push_missing_payload"

    invoke-static {v0, v4, v1}, LX/3E7;->a(LX/3E7;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 524271
    :pswitch_1
    const-string v1, "payload"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 524272
    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 524273
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 524274
    const-string v1, "RtcSignalingHandler"

    const-string v2, "No valid payload in gcm json payload"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524275
    const-string v1, "push_missing_payload"

    invoke-static {v0, v4, v1}, LX/3E7;->a(LX/3E7;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 524276
    :pswitch_2
    :try_start_1
    iget-object v1, v0, LX/3E7;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lC;

    const-string v2, "payload"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 524277
    const-string v2, "type"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 524278
    const-string v3, "rtc_multi_binary"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 524279
    const-string v2, "binaryPayload"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->t()[B

    move-result-object v1

    .line 524280
    invoke-static {v0, v1}, LX/3E7;->a(LX/3E7;[B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 524281
    :catch_1
    move-exception v1

    .line 524282
    const-string v2, "RtcSignalingHandler"

    const-string v3, "Error reading gcm multiway message"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 524283
    const-string v1, "push_missing_multiway_payload"

    invoke-static {v0, v4, v1}, LX/3E7;->a(LX/3E7;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2f31c173 -> :sswitch_1
        0x68f9016 -> :sswitch_0
        0x54bfdfd0 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Lcom/facebook/rtc/helpers/RtcCallStartParams;)V
    .locals 3

    .prologue
    .line 524284
    iget-object v0, p0, LX/3A0;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rtc/helpers/RtcCallHandler$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/rtc/helpers/RtcCallHandler$2;-><init>(LX/3A0;Lcom/facebook/rtc/helpers/RtcCallStartParams;)V

    const v2, -0x70b3a84f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 524285
    invoke-static {p1}, LX/3A0;->h(Lcom/facebook/rtc/helpers/RtcCallStartParams;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/3A0;->a(Lcom/facebook/rtc/helpers/RtcCallStartParams;Ljava/lang/String;)V

    .line 524286
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 524287
    iget-object v0, p0, LX/3A0;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aU()J

    move-result-wide v0

    iget-wide v2, p0, LX/3A0;->I:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/rtc/helpers/RtcCallStartParams;)V
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 524288
    invoke-virtual {p0}, LX/3A0;->e()V

    .line 524289
    invoke-direct {p0}, LX/3A0;->h()V

    .line 524290
    iget-object v1, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    sget-object v2, LX/EFe;->REGULAR:LX/EFe;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/3A0;->y:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/3Dx;->k:S

    invoke-interface {v1, v2, v3, v4, v0}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    .line 524291
    :cond_0
    iget-object v1, p0, LX/3A0;->e:LX/2Tm;

    iget-object v2, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->g:Ljava/lang/String;

    invoke-static {p1}, LX/3A0;->h(Lcom/facebook/rtc/helpers/RtcCallStartParams;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/7TR;

    invoke-direct {v4}, LX/7TR;-><init>()V

    .line 524292
    iput-boolean v0, v4, LX/7TR;->a:Z

    .line 524293
    move-object v0, v4

    .line 524294
    invoke-virtual {v0}, LX/7TR;->a()[B

    move-result-object v0

    .line 524295
    invoke-virtual {v1}, LX/2Tm;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 524296
    iget-object v4, v1, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v4, v2, v3, v0}, Lcom/facebook/webrtc/WebrtcEngine;->createConferenceHandle(Ljava/lang/String;Ljava/lang/String;[B)Lcom/facebook/webrtc/ConferenceCall;

    move-result-object v4

    .line 524297
    :goto_0
    move-object v1, v4

    .line 524298
    if-nez v1, :cond_1

    .line 524299
    sget-object v0, LX/3A0;->b:Ljava/lang/String;

    const-string v1, "Ignoring start call attempt due to null conference handle"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524300
    :goto_1
    return-void

    .line 524301
    :cond_1
    iget-object v0, p0, LX/3A0;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 524302
    if-eqz v1, :cond_2

    if-nez p1, :cond_4

    .line 524303
    :cond_2
    const-string v5, "WebrtcUiHandler"

    const-string v6, "Unable to start conference call due to null input"

    invoke-static {v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 524304
    :goto_2
    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_0

    .line 524305
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/webrtc/ConferenceCall;->callId()J

    move-result-wide v7

    const-wide/16 v9, -0x1

    move-object v6, v0

    invoke-virtual/range {v6 .. v11}, LX/EDx;->initializeCall(JJZ)V

    .line 524306
    invoke-static {v0, p1, v1}, LX/EDx;->a(LX/EDx;Lcom/facebook/rtc/helpers/RtcCallStartParams;Lcom/facebook/webrtc/ConferenceCall;)V

    .line 524307
    iget-object v5, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->m:LX/0Px;

    invoke-static {v0, v5}, LX/EDx;->a(LX/EDx;LX/0Px;)V

    .line 524308
    iget-object v5, v0, LX/EDx;->bC:LX/0ad;

    sget-object v6, LX/0c0;->Cached:LX/0c0;

    sget-object v7, LX/0c1;->Off:LX/0c1;

    sget v8, LX/3Dx;->aQ:I

    const/4 v9, 0x5

    invoke-interface {v5, v6, v7, v8, v9}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v5

    .line 524309
    invoke-virtual {v0}, LX/EDx;->bj()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-gt v6, v5, :cond_a

    move v5, v11

    .line 524310
    :goto_3
    iput-boolean v5, v0, LX/EDx;->ai:Z

    .line 524311
    iget-object v5, v0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v5}, LX/EFw;->e()I

    move-result v5

    if-ne v5, v11, :cond_5

    .line 524312
    iget-object v5, v0, LX/EDx;->v:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v6, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$6;

    invoke-direct {v6, v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$6;-><init>(LX/EDx;)V

    const-wide/16 v7, 0x3c

    sget-object v9, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v5, v6, v7, v8, v9}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v5

    iput-object v5, v0, LX/EDx;->bW:Ljava/util/concurrent/ScheduledFuture;

    .line 524313
    :cond_5
    iget-boolean v5, v0, LX/EDx;->ao:Z

    move v5, v5

    .line 524314
    if-eqz v5, :cond_6

    .line 524315
    iget-object v5, v0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v0}, LX/EDx;->U()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/EFw;->b(Ljava/lang/String;)V

    .line 524316
    iget-object v5, v0, LX/EDx;->ad:LX/EFw;

    iget-object v6, v0, LX/EDx;->cp:LX/EDw;

    iget v6, v6, LX/EDw;->a:I

    iget-object v7, v0, LX/EDx;->cp:LX/EDw;

    iget v7, v7, LX/EDw;->b:I

    iget-object v8, v0, LX/EDx;->cp:LX/EDw;

    iget v8, v8, LX/EDw;->c:I

    invoke-virtual {v5, v6, v7, v8}, LX/EFw;->a(III)V

    .line 524317
    :cond_6
    iget-boolean v5, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    if-eqz v5, :cond_b

    .line 524318
    iget-boolean v5, v0, LX/EDx;->ao:Z

    move v5, v5

    .line 524319
    if-eqz v5, :cond_b

    sget-object v5, LX/03R;->YES:LX/03R;

    :goto_4
    iput-object v5, v0, LX/EDx;->bv:LX/03R;

    .line 524320
    iget-object v6, v0, LX/EDx;->ad:LX/EFw;

    iget-boolean v5, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    if-eqz v5, :cond_c

    .line 524321
    iget-boolean v5, v0, LX/EDx;->ao:Z

    move v5, v5

    .line 524322
    if-eqz v5, :cond_c

    move v5, v11

    :goto_5
    invoke-virtual {v6, v5}, LX/EFw;->b(Z)V

    .line 524323
    iget-object v5, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    sget-object v6, LX/EFe;->GROUP_CALL_START:LX/EFe;

    if-ne v5, v6, :cond_d

    move v5, v11

    :goto_6
    iput-boolean v5, v0, LX/EDx;->cb:Z

    .line 524324
    iget-object v5, v0, LX/EDx;->bv:LX/03R;

    invoke-virtual {v5, v12}, LX/03R;->asBoolean(Z)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 524325
    sget-object v5, LX/EDv;->STARTED:LX/EDv;

    invoke-virtual {v0, v5}, LX/EDx;->a(LX/EDv;)V

    .line 524326
    :cond_7
    invoke-virtual {v0}, LX/EDx;->aU()J

    move-result-wide v5

    iput-wide v5, v0, LX/EDx;->aK:J

    .line 524327
    const-wide/16 v5, 0x0

    iput-wide v5, v0, LX/EDx;->aL:J

    .line 524328
    iget-boolean v5, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    iput-boolean v5, v0, LX/EDx;->ba:Z

    .line 524329
    invoke-static {v0}, LX/EDx;->bA(LX/EDx;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 524330
    invoke-static {v0}, LX/EDx;->bu(LX/EDx;)V

    .line 524331
    :cond_8
    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 524332
    invoke-static {v0, v11, v11}, LX/EDx;->e(LX/EDx;ZZ)V

    .line 524333
    :goto_7
    iget-object v5, v0, LX/EDx;->bC:LX/0ad;

    sget-object v6, LX/0c0;->Cached:LX/0c0;

    sget-short v7, LX/3Dx;->aK:S

    invoke-interface {v5, v6, v7, v12}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v5

    .line 524334
    iget-object v6, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    sget-object v7, LX/EFe;->GROUP_CALL_START:LX/EFe;

    if-ne v6, v7, :cond_f

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v6

    if-eqz v6, :cond_f

    if-eqz v5, :cond_f

    .line 524335
    iget-object v5, v0, LX/EDx;->bC:LX/0ad;

    sget-object v6, LX/0c0;->Cached:LX/0c0;

    sget v7, LX/3Dx;->aP:I

    const/4 v8, 0x3

    invoke-interface {v5, v6, v7, v8}, LX/0ad;->a(LX/0c0;II)I

    move-result v5

    .line 524336
    const/4 v6, 0x1

    iput-boolean v6, v0, LX/EDx;->bZ:Z

    .line 524337
    iput v5, v0, LX/EDx;->ca:I

    .line 524338
    iget-object v6, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    const/4 v6, 0x0

    move v7, v6

    :goto_8
    if-ge v7, v8, :cond_9

    iget-object v6, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/EC0;

    .line 524339
    invoke-virtual {v6, v5}, LX/EC0;->b(I)V

    .line 524340
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_8

    .line 524341
    :cond_9
    goto/16 :goto_2

    :cond_a
    move v5, v12

    .line 524342
    goto/16 :goto_3

    .line 524343
    :cond_b
    sget-object v5, LX/03R;->NO:LX/03R;

    goto/16 :goto_4

    :cond_c
    move v5, v12

    .line 524344
    goto/16 :goto_5

    :cond_d
    move v5, v12

    .line 524345
    goto/16 :goto_6

    .line 524346
    :cond_e
    invoke-virtual {v0}, LX/EDx;->switchToContactingUI()V

    goto :goto_7

    .line 524347
    :cond_f
    invoke-virtual {v0, v12}, LX/EDx;->a(Z)V

    goto/16 :goto_2
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 524348
    invoke-virtual {p0}, LX/3A0;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/3A0;->j(LX/3A0;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 524349
    iget-object v0, p0, LX/3A0;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aT()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 7

    .prologue
    .line 524350
    iget-object v0, p0, LX/3A0;->e:LX/2Tm;

    iget-object v1, p0, LX/3A0;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/webrtc/IWebrtcUiInterface;

    iget-object v2, p0, LX/3A0;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/webrtc/ConferenceCall$Listener;

    iget-object v3, p0, LX/3A0;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    iget-object v4, p0, LX/3A0;->C:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/webrtc/IWebrtcConfigInterface;

    iget-object v5, p0, LX/3A0;->o:LX/2S7;

    iget-object v6, p0, LX/3A0;->B:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;

    invoke-virtual/range {v0 .. v6}, LX/2Tm;->a(Lcom/facebook/webrtc/IWebrtcUiInterface;Lcom/facebook/webrtc/ConferenceCall$Listener;LX/EDx;Lcom/facebook/webrtc/IWebrtcConfigInterface;Lcom/facebook/webrtc/IWebrtcLoggingInterface;Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;)V

    .line 524351
    return-void
.end method

.method public final f()I
    .locals 3

    .prologue
    .line 524352
    iget-object v0, p0, LX/3A0;->y:LX/0ad;

    sget v1, LX/3Dx;->fo:I

    const/16 v2, 0xc

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method
