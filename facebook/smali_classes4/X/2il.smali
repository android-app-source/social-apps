.class public final LX/2il;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friends/model/FriendRequest;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 0

    .prologue
    .line 451974
    iput-object p1, p0, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 15

    .prologue
    .line 451975
    iget-object v0, p0, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aB:Z

    if-eqz v0, :cond_0

    .line 451976
    iget-object v0, p0, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    iget-object v1, p0, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->D:LX/2e3;

    invoke-virtual {v1}, LX/2e3;->c()I

    move-result v1

    sget-object v2, Lcom/facebook/friending/jewel/FriendRequestsFragment;->U:Lcom/facebook/common/callercontext/CallerContext;

    new-instance v3, LX/Eyi;

    invoke-direct {v3, p0}, LX/Eyi;-><init>(LX/2il;)V

    .line 451977
    iget-object v4, v0, LX/2dj;->k:LX/2dk;

    invoke-static {v0}, LX/2dj;->m(LX/2dj;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/83z;

    invoke-direct {v6, v0, v3}, LX/83z;-><init>(LX/2dj;LX/0TF;)V

    .line 451978
    invoke-static {v4, v5, v1, v2}, LX/2dk;->b(LX/2dk;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v8

    .line 451979
    invoke-static {v4}, LX/2dk;->c(LX/2dk;)LX/0QK;

    move-result-object v13

    .line 451980
    iget-object v7, v4, LX/2dk;->c:LX/2dl;

    iget-object v9, v4, LX/2dk;->d:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/1My;

    new-instance v10, LX/841;

    invoke-direct {v10, v4, v6, v13}, LX/841;-><init>(LX/2dk;LX/0TF;LX/0QK;)V

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 451981
    iget-object v12, v8, LX/0zO;->m:LX/0gW;

    move-object v12, v12

    .line 451982
    iget-object v14, v12, LX/0gW;->f:Ljava/lang/String;

    move-object v12, v14

    .line 451983
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v4, LX/2dk;->f:I

    add-int/lit8 v14, v12, 0x1

    iput v14, v4, LX/2dk;->f:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v12, "REQUESTS_TAB_REQUESTS_QUERY_TAG"

    invoke-virtual/range {v7 .. v12}, LX/2dl;->a(LX/0zO;LX/1My;LX/0TF;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 451984
    iget-object v8, v4, LX/2dk;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v7, v13, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v4, v7

    .line 451985
    invoke-static {v0, v4}, LX/2dj;->a(LX/2dj;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 451986
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    iget-object v1, p0, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->D:LX/2e3;

    invoke-virtual {v1}, LX/2e3;->c()I

    move-result v1

    sget-object v2, Lcom/facebook/friending/jewel/FriendRequestsFragment;->U:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/2dj;->a(ILcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
