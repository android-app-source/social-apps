.class public LX/2hc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0wM;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0wM;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450214
    iput-object p1, p0, LX/2hc;->a:LX/0wM;

    .line 450215
    iput-object p2, p0, LX/2hc;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 450216
    return-void
.end method

.method public static a(LX/0QB;)LX/2hc;
    .locals 1

    .prologue
    .line 450217
    invoke-static {p0}, LX/2hc;->b(LX/0QB;)LX/2hc;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2hc;
    .locals 3

    .prologue
    .line 450218
    new-instance v2, LX/2hc;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v0, v1}, LX/2hc;-><init>(LX/0wM;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 450219
    return-object v2
.end method


# virtual methods
.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 450220
    iget-object v1, p0, LX/2hc;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1nR;->f:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
