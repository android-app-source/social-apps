.class public LX/2nL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 463974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463975
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 463976
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;->p()LX/0Px;

    move-result-object v0

    .line 463977
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 463978
    :cond_0
    const/4 v0, 0x0

    .line 463979
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 463980
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;->p()LX/0Px;

    move-result-object v0

    .line 463981
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 463982
    :cond_0
    const/4 v0, 0x0

    .line 463983
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 463984
    invoke-static {p0}, LX/2nL;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;

    move-result-object v0

    .line 463985
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 463986
    invoke-static {p0}, LX/2nL;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;

    move-result-object v0

    .line 463987
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 463988
    invoke-static {p0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    .line 463989
    if-nez v1, :cond_1

    .line 463990
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->j()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 463991
    invoke-static {p0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    .line 463992
    if-nez v1, :cond_1

    .line 463993
    :cond_0
    :goto_0
    return-object v0

    .line 463994
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 463995
    if-eqz v1, :cond_0

    .line 463996
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
