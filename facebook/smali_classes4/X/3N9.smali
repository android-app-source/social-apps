.class public LX/3N9;
.super LX/3Ml;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:LX/3Mq;

.field public e:Z

.field private f:LX/2Og;

.field public g:Z

.field private h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 558346
    const-class v0, LX/3N9;

    sput-object v0, LX/3N9;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zr;LX/3Mq;LX/2Og;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558347
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 558348
    iput-object p2, p0, LX/3N9;->d:LX/3Mq;

    .line 558349
    iput-object p3, p0, LX/3N9;->f:LX/2Og;

    .line 558350
    return-void
.end method

.method public static a(LX/0QB;)LX/3N9;
    .locals 1

    .prologue
    .line 558351
    invoke-static {p0}, LX/3N9;->b(LX/0QB;)LX/3N9;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3N9;
    .locals 4

    .prologue
    .line 558352
    new-instance v3, LX/3N9;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v0

    check-cast v0, LX/0Zr;

    invoke-static {p0}, LX/3Mq;->a(LX/0QB;)LX/3Mq;

    move-result-object v1

    check-cast v1, LX/3Mq;

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v2

    check-cast v2, LX/2Og;

    invoke-direct {v3, v0, v1, v2}, LX/3N9;-><init>(LX/0Zr;LX/3Mq;LX/2Og;)V

    .line 558353
    return-object v3
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 10
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 558354
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 558355
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "starting filtering, constraint="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558356
    new-instance v3, LX/39y;

    invoke-direct {v3}, LX/39y;-><init>()V

    .line 558357
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 558358
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v1

    iput-object v1, v3, LX/39y;->a:Ljava/lang/Object;

    .line 558359
    const/4 v1, -0x1

    iput v1, v3, LX/39y;->b:I

    move-object v1, v3

    .line 558360
    :goto_1
    return-object v1

    .line 558361
    :cond_0
    const-string v1, ""

    move-object v2, v1

    goto :goto_0

    .line 558362
    :cond_1
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v6

    .line 558363
    iget-boolean v1, p0, LX/3N9;->e:Z

    if-eqz v1, :cond_2

    .line 558364
    iget-object v1, p0, LX/3N9;->f:LX/2Og;

    invoke-virtual {v1}, LX/2Og;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    :goto_2
    if-ge v5, v8, :cond_2

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 558365
    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 558366
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_2

    .line 558367
    :cond_2
    iget-object v1, p0, LX/3N9;->d:LX/3Mq;

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, LX/3Mq;->a(Ljava/lang/String;Ljava/util/Collection;)LX/6dO;

    move-result-object v5

    .line 558368
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 558369
    :cond_3
    :goto_3
    invoke-virtual {v5}, LX/6dO;->a()LX/6g6;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 558370
    invoke-virtual {v1}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v8

    .line 558371
    iget-object v1, v8, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_3

    .line 558372
    iget-object v1, v8, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 558373
    iget-object v1, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v1, v8}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v2

    .line 558374
    instance-of v1, v2, LX/DAU;

    if-eqz v1, :cond_6

    iget-boolean v1, p0, LX/3N9;->g:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, LX/3N9;->h:Z

    if-eqz v1, :cond_6

    .line 558375
    :cond_4
    move-object v0, v2

    check-cast v0, LX/DAU;

    move-object v1, v0

    .line 558376
    iget-boolean v9, p0, LX/3N9;->g:Z

    if-eqz v9, :cond_5

    .line 558377
    const/4 v9, 0x1

    .line 558378
    iput-boolean v9, v1, LX/DAU;->j:Z

    .line 558379
    const-string v9, "multiway_call_search"

    .line 558380
    iput-object v9, v1, LX/DAU;->n:Ljava/lang/String;

    .line 558381
    :cond_5
    iget-boolean v9, p0, LX/3N9;->h:Z

    if-eqz v9, :cond_6

    .line 558382
    const/4 v9, 0x1

    .line 558383
    iput-boolean v9, v1, LX/DAU;->k:Z

    .line 558384
    const-string v9, "multiway_call_search_video"

    .line 558385
    iput-object v9, v1, LX/DAU;->o:Ljava/lang/String;

    .line 558386
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v9, "adding group summary: "

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 558387
    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 558388
    add-int/lit8 v1, v4, 0x1

    const/4 v2, 0x6

    if-ge v1, v2, :cond_7

    move v4, v1

    .line 558389
    goto :goto_3

    .line 558390
    :cond_7
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 558391
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    iput v2, v3, LX/39y;->b:I

    .line 558392
    invoke-static {p1, v1}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v2

    iput-object v2, v3, LX/39y;->a:Ljava/lang/Object;

    .line 558393
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "got thread summaries: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 558394
    invoke-virtual {v5}, LX/6dO;->close()V

    move-object v1, v3

    .line 558395
    goto/16 :goto_1

    .line 558396
    :catch_0
    move-exception v1

    .line 558397
    :try_start_1
    sget-object v2, LX/3N9;->c:Ljava/lang/Class;

    const-string v4, "exception with filtering groups"

    invoke-static {v2, v4, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 558398
    const/4 v1, 0x0

    iput v1, v3, LX/39y;->b:I

    .line 558399
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v1

    iput-object v1, v3, LX/39y;->a:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 558400
    invoke-virtual {v5}, LX/6dO;->close()V

    move-object v1, v3

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    invoke-virtual {v5}, LX/6dO;->close()V

    throw v1
.end method
