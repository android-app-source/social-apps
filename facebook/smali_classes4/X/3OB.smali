.class public LX/3OB;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public final b:LX/0iA;

.field public final c:Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;

.field public d:Landroid/view/View$OnClickListener;

.field public e:Landroid/view/View;

.field public f:Landroid/view/ViewGroup;

.field public g:LX/3OA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 560166
    sget-object v0, LX/3OC;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sput-object v0, LX/3OB;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0iA;Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 560152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560153
    iput-object p1, p0, LX/3OB;->b:LX/0iA;

    .line 560154
    iput-object p2, p0, LX/3OB;->c:Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;

    .line 560155
    new-instance v0, LX/3OD;

    invoke-direct {v0, p0}, LX/3OD;-><init>(LX/3OB;)V

    iput-object v0, p0, LX/3OB;->d:Landroid/view/View$OnClickListener;

    .line 560156
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 560157
    iget-object v0, p0, LX/3OB;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 560158
    iget-object v0, p0, LX/3OB;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 560159
    iget-object v0, p0, LX/3OB;->f:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 560160
    iget-object v0, p0, LX/3OB;->f:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/3OB;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 560161
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/3OB;->e:Landroid/view/View;

    .line 560162
    :cond_1
    iget-object v0, p0, LX/3OB;->g:LX/3OA;

    if-eqz v0, :cond_2

    .line 560163
    iget-object v0, p0, LX/3OB;->g:LX/3OA;

    .line 560164
    iget-object v1, v0, LX/3OA;->a:LX/3O9;

    invoke-static {v1}, LX/3O9;->b(LX/3O9;)V

    .line 560165
    :cond_2
    return-void
.end method
