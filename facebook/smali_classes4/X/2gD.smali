.class public LX/2gD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2gD;


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/13Q;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2gU;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/9lA;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 448089
    const-class v0, LX/2gD;

    sput-object v0, LX/2gD;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/13Q;LX/0Ot;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/13Q;",
            "LX/0Ot",
            "<",
            "LX/2gU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448085
    iput-object p1, p0, LX/2gD;->b:Ljava/util/concurrent/ExecutorService;

    .line 448086
    iput-object p2, p0, LX/2gD;->c:LX/13Q;

    .line 448087
    iput-object p3, p0, LX/2gD;->d:LX/0Ot;

    .line 448088
    return-void
.end method

.method public static a(LX/0QB;)LX/2gD;
    .locals 6

    .prologue
    .line 448034
    sget-object v0, LX/2gD;->f:LX/2gD;

    if-nez v0, :cond_1

    .line 448035
    const-class v1, LX/2gD;

    monitor-enter v1

    .line 448036
    :try_start_0
    sget-object v0, LX/2gD;->f:LX/2gD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 448037
    if-eqz v2, :cond_0

    .line 448038
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 448039
    new-instance v5, LX/2gD;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v4

    check-cast v4, LX/13Q;

    const/16 p0, 0x1021

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2gD;-><init>(Ljava/util/concurrent/ExecutorService;LX/13Q;LX/0Ot;)V

    .line 448040
    move-object v0, v5

    .line 448041
    sput-object v0, LX/2gD;->f:LX/2gD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448042
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 448043
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 448044
    :cond_1
    sget-object v0, LX/2gD;->f:LX/2gD;

    return-object v0

    .line 448045
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 448046
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2gD;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 448071
    monitor-enter p0

    .line 448072
    :try_start_0
    iget-object v3, p0, LX/2gD;->e:Ljava/util/ArrayList;

    .line 448073
    const/4 v0, 0x0

    iput-object v0, p0, LX/2gD;->e:Ljava/util/ArrayList;

    .line 448074
    monitor-exit p0

    .line 448075
    if-nez v3, :cond_0

    .line 448076
    :goto_0
    return-void

    .line 448077
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 448078
    :cond_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 448079
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9lA;

    .line 448080
    iget-object v1, p0, LX/2gD;->c:LX/13Q;

    iget-object v5, v0, LX/9lA;->b:Ljava/lang/String;

    invoke-virtual {v1, v5}, LX/13Q;->a(Ljava/lang/String;)V

    .line 448081
    iget-object v1, p0, LX/2gD;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2gU;

    const-string v5, "/t_stp"

    iget-object v0, v0, LX/9lA;->a:[B

    sget-object v6, LX/2I2;->FIRE_AND_FORGET:LX/2I2;

    invoke-virtual {v1, v5, v0, v6, v7}, LX/2gU;->a(Ljava/lang/String;[BLX/2I2;LX/76H;)I

    .line 448082
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 448083
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6mg;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 448047
    :try_start_0
    new-instance v0, LX/2ZA;

    const-string v1, ""

    invoke-direct {v0, v1}, LX/2ZA;-><init>(Ljava/lang/String;)V

    .line 448048
    new-instance v1, LX/1so;

    new-instance v2, LX/1sp;

    invoke-direct {v2}, LX/1sp;-><init>()V

    invoke-direct {v1, v2}, LX/1so;-><init>(LX/1sq;)V

    .line 448049
    invoke-virtual {v1, v0}, LX/1so;->a(LX/1u2;)[B

    move-result-object v0

    .line 448050
    invoke-virtual {v1, p1}, LX/1so;->a(LX/1u2;)[B

    move-result-object v1

    .line 448051
    const/4 v2, 0x2

    new-array v2, v2, [[B

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v1, 0x0

    .line 448052
    array-length v6, v2

    move v0, v1

    move v3, v1

    :goto_0
    if-ge v0, v6, :cond_0

    aget-object v7, v2, v0

    .line 448053
    array-length v7, v7

    add-int/2addr v3, v7

    .line 448054
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 448055
    :cond_0
    new-array v6, v3, [B

    .line 448056
    array-length v7, v2

    move v0, v1

    move v3, v1

    :goto_1
    if-ge v0, v7, :cond_1

    aget-object v8, v2, v0

    .line 448057
    array-length p1, v8

    invoke-static {v8, v1, v6, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 448058
    array-length v8, v8

    add-int/2addr v3, v8

    .line 448059
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 448060
    :cond_1
    move-object v0, v6

    .line 448061
    monitor-enter p0
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    .line 448062
    :try_start_1
    iget-object v1, p0, LX/2gD;->e:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    .line 448063
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, LX/2gD;->e:Ljava/util/ArrayList;

    .line 448064
    iget-object v1, p0, LX/2gD;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/presence/ThreadPresenceSender$1;

    invoke-direct {v2, p0}, Lcom/facebook/presence/ThreadPresenceSender$1;-><init>(LX/2gD;)V

    const v3, 0x584c4d26

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 448065
    :cond_2
    iget-object v1, p0, LX/2gD;->e:Ljava/util/ArrayList;

    new-instance v2, LX/9lA;

    invoke-direct {v2, v0, p2}, LX/9lA;-><init>([BLjava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448066
    monitor-exit p0

    .line 448067
    :goto_2
    return-void

    .line 448068
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch LX/7H0; {:try_start_2 .. :try_end_2} :catch_0

    .line 448069
    :catch_0
    move-exception v0

    .line 448070
    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, LX/7H0;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, LX/7H0;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_3
    aput-object v0, v1, v4

    goto :goto_2

    :cond_3
    const-string v0, "NULL"

    goto :goto_3
.end method
