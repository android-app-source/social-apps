.class public final LX/2fL;
.super LX/2Xb;
.source ""


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation


# instance fields
.field public final f:Ljava/io/File;

.field private final g:LX/2D7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2D7",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Y2;Ljava/io/File;LX/2D7;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Y2;",
            "Ljava/io/File;",
            "LX/2D7",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 446333
    invoke-direct {p0, p1, p2}, LX/2Xb;-><init>(LX/2Y2;Ljava/lang/Object;)V

    .line 446334
    iput-object p2, p0, LX/2fL;->f:Ljava/io/File;

    .line 446335
    iput-object p3, p0, LX/2fL;->g:LX/2D7;

    .line 446336
    return-void
.end method


# virtual methods
.method public final b(Ljava/io/Writer;)V
    .locals 4

    .prologue
    .line 446337
    new-instance v1, LX/2YQ;

    new-instance v2, Ljava/io/FileInputStream;

    iget-object v0, p0, LX/2fL;->f:Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    sget-object v0, LX/2Xb;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/2YQ;-><init>(Ljava/io/InputStream;Ljava/nio/ByteBuffer;)V

    .line 446338
    :try_start_0
    sget-object v0, LX/2Xb;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [C

    .line 446339
    :goto_0
    invoke-virtual {v1, v0}, LX/2YQ;->read([C)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 446340
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3, v2}, Ljava/io/Writer;->write([CII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 446341
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2YQ;->close()V

    throw v0

    :cond_0
    invoke-virtual {v1}, LX/2YQ;->close()V

    .line 446342
    return-void
.end method

.method public final g()LX/2DZ;
    .locals 2

    .prologue
    .line 446343
    iget-object v0, p0, LX/2fL;->g:LX/2D7;

    iget-object v1, p0, LX/2fL;->f:Ljava/io/File;

    invoke-virtual {v0, v1}, LX/2D7;->a(Ljava/lang/Object;)LX/2DZ;

    move-result-object v0

    return-object v0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 446344
    iget-object v0, p0, LX/2fL;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 5

    .prologue
    .line 446345
    iget-object v0, p0, LX/2fL;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 446346
    const-string v0, "FileBatchPayloadIterator"

    const-string v1, "Failed to remove %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/2fL;->f:Ljava/io/File;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446347
    :cond_0
    return-void
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 446348
    return-void
.end method
