.class public LX/2GO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/app/ActivityManager$MemoryInfo;

.field public final b:J


# direct methods
.method public constructor <init>(Landroid/app/ActivityManager$MemoryInfo;J)V
    .locals 0

    .prologue
    .line 388396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388397
    iput-object p1, p0, LX/2GO;->a:Landroid/app/ActivityManager$MemoryInfo;

    .line 388398
    iput-wide p2, p0, LX/2GO;->b:J

    .line 388399
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 388395
    iget-object v0, p0, LX/2GO;->a:Landroid/app/ActivityManager$MemoryInfo;

    iget-wide v0, v0, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    return-wide v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 388394
    iget-object v0, p0, LX/2GO;->a:Landroid/app/ActivityManager$MemoryInfo;

    iget-wide v0, v0, Landroid/app/ActivityManager$MemoryInfo;->threshold:J

    return-wide v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 388393
    iget-object v0, p0, LX/2GO;->a:Landroid/app/ActivityManager$MemoryInfo;

    iget-boolean v0, v0, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    return v0
.end method
