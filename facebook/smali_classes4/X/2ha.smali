.class public LX/2ha;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0ad;

.field public b:LX/2h7;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450171
    const/4 v0, 0x0

    iput-object v0, p0, LX/2ha;->b:LX/2h7;

    .line 450172
    iput-object p1, p0, LX/2ha;->a:LX/0ad;

    .line 450173
    return-void
.end method

.method public static a(LX/0QB;)LX/2ha;
    .locals 4

    .prologue
    .line 450174
    const-class v1, LX/2ha;

    monitor-enter v1

    .line 450175
    :try_start_0
    sget-object v0, LX/2ha;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 450176
    sput-object v2, LX/2ha;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 450177
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450178
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 450179
    new-instance p0, LX/2ha;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/2ha;-><init>(LX/0ad;)V

    .line 450180
    move-object v0, p0

    .line 450181
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 450182
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2ha;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450183
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 450184
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Z
    .locals 3

    .prologue
    .line 450169
    iget-object v0, p0, LX/2ha;->a:LX/0ad;

    sget-short v1, LX/2nX;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
