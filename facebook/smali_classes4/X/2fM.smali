.class public final LX/2fM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Wd;


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2fL;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2Y2;

.field public volatile c:Z


# direct methods
.method public constructor <init>(Ljava/util/List;LX/2Y2;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2fL;",
            ">;",
            "LX/2Y2;",
            ")V"
        }
    .end annotation

    .prologue
    .line 446349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 446350
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446351
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "payloads cannot be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 446352
    :cond_0
    iput-object p1, p0, LX/2fM;->a:Ljava/util/List;

    .line 446353
    iput-object p2, p0, LX/2fM;->b:LX/2Y2;

    .line 446354
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 446382
    iget-object v1, p0, LX/2fM;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    .line 446383
    :goto_0
    if-ge v1, v3, :cond_0

    .line 446384
    iget-object v0, p0, LX/2fM;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2fL;

    invoke-virtual {v0}, LX/2fL;->h()I

    move-result v0

    add-int/2addr v2, v0

    .line 446385
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 446386
    :cond_0
    const/16 v0, 0x100

    move v0, v0

    .line 446387
    add-int/2addr v0, v2

    .line 446388
    return v0
.end method

.method public final a(Ljava/io/Writer;)V
    .locals 4

    .prologue
    .line 446366
    invoke-virtual {p0}, LX/2fM;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 446367
    const/4 v0, 0x0

    iget-object v1, p0, LX/2fM;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 446368
    iget-object v0, p0, LX/2fM;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2fL;

    invoke-virtual {v0}, LX/2Xb;->k()V

    .line 446369
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 446370
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2fM;->c:Z

    .line 446371
    :cond_1
    new-instance v2, LX/2fN;

    invoke-direct {v2, p1}, LX/2fN;-><init>(Ljava/io/Writer;)V

    .line 446372
    const/4 v0, 0x1

    invoke-static {v2, v0}, LX/2fN;->a(LX/2fN;I)V

    .line 446373
    const/4 v0, 0x2

    .line 446374
    iput v0, v2, LX/2fN;->b:I

    .line 446375
    iget-object v0, v2, LX/2fN;->a:Ljava/io/Writer;

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 446376
    iget-object v0, v2, LX/2fN;->a:Ljava/io/Writer;

    const-string v1, "\"batches\":["

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 446377
    const/4 v0, 0x0

    iget-object v1, p0, LX/2fM;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    .line 446378
    iget-object v0, p0, LX/2fM;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Xb;

    invoke-virtual {v2, v0}, LX/2fN;->a(LX/2Xb;)V

    .line 446379
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 446380
    :cond_2
    iget-object v0, p0, LX/2fM;->b:LX/2Y2;

    invoke-virtual {v2, v0}, LX/2fN;->a(LX/2Y2;)V

    .line 446381
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 446365
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 446360
    iget-object v0, p0, LX/2fM;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    .line 446361
    iget-object v0, p0, LX/2fM;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2fL;

    invoke-virtual {v0}, LX/2Xb;->c()V

    .line 446362
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 446363
    :cond_0
    iput-boolean v2, p0, LX/2fM;->c:Z

    .line 446364
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 446359
    iget-boolean v0, p0, LX/2fM;->c:Z

    return v0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 446355
    const/4 v0, 0x0

    iget-object v1, p0, LX/2fM;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 446356
    iget-object v0, p0, LX/2fM;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2fL;

    invoke-virtual {v0}, LX/2Xb;->e()V

    .line 446357
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 446358
    :cond_0
    return-void
.end method
