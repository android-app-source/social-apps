.class public LX/3LO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3LO;


# instance fields
.field private final a:Landroid/app/ActivityManager;


# direct methods
.method public constructor <init>(Landroid/app/ActivityManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550437
    iput-object p1, p0, LX/3LO;->a:Landroid/app/ActivityManager;

    .line 550438
    return-void
.end method

.method public static a(LX/0QB;)LX/3LO;
    .locals 4

    .prologue
    .line 550439
    sget-object v0, LX/3LO;->b:LX/3LO;

    if-nez v0, :cond_1

    .line 550440
    const-class v1, LX/3LO;

    monitor-enter v1

    .line 550441
    :try_start_0
    sget-object v0, LX/3LO;->b:LX/3LO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 550442
    if-eqz v2, :cond_0

    .line 550443
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 550444
    new-instance p0, LX/3LO;

    invoke-static {v0}, LX/0VU;->b(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager;

    invoke-direct {p0, v3}, LX/3LO;-><init>(Landroid/app/ActivityManager;)V

    .line 550445
    move-object v0, p0

    .line 550446
    sput-object v0, LX/3LO;->b:LX/3LO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 550447
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 550448
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 550449
    :cond_1
    sget-object v0, LX/3LO;->b:LX/3LO;

    return-object v0

    .line 550450
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 550451
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(II)I
    .locals 4

    .prologue
    .line 550452
    if-ge p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 550453
    iget-object v0, p0, LX/3LO;->a:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    .line 550454
    const/16 v1, 0x10

    if-gt v0, v1, :cond_1

    .line 550455
    :goto_1
    return p1

    .line 550456
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 550457
    :cond_1
    const/16 v1, 0x80

    if-lt v0, v1, :cond_2

    move p1, p2

    .line 550458
    goto :goto_1

    .line 550459
    :cond_2
    int-to-double v0, v0

    const-wide/high16 v2, 0x405c000000000000L    # 112.0

    div-double/2addr v0, v2

    sub-int v2, p2, p1

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    add-int/2addr p1, v0

    goto :goto_1
.end method
