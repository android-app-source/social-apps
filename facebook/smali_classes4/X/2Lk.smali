.class public final LX/2Lk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pg;


# instance fields
.field private final a:LX/0Pg;

.field private final b:LX/0Pg;


# direct methods
.method public constructor <init>(LX/0Pg;LX/0Pg;)V
    .locals 0

    .prologue
    .line 395370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395371
    iput-object p1, p0, LX/2Lk;->a:LX/0Pg;

    .line 395372
    iput-object p2, p0, LX/2Lk;->b:LX/0Pg;

    .line 395373
    return-void
.end method


# virtual methods
.method public final a(I)LX/0Pk;
    .locals 1

    .prologue
    .line 395374
    iget-object v0, p0, LX/2Lk;->a:LX/0Pg;

    invoke-interface {v0, p1}, LX/0Pg;->a(I)LX/0Pk;

    move-result-object v0

    .line 395375
    if-eqz v0, :cond_0

    .line 395376
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Lk;->b:LX/0Pg;

    invoke-interface {v0, p1}, LX/0Pg;->a(I)LX/0Pk;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 395377
    iget-object v0, p0, LX/2Lk;->a:LX/0Pg;

    invoke-interface {v0}, LX/0Pg;->b()I

    move-result v0

    iget-object v1, p0, LX/2Lk;->b:LX/0Pg;

    invoke-interface {v1}, LX/0Pg;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 395378
    iget-object v0, p0, LX/2Lk;->a:LX/0Pg;

    invoke-interface {v0}, LX/0Pg;->c()I

    move-result v0

    if-nez v0, :cond_0

    .line 395379
    iget-object v0, p0, LX/2Lk;->b:LX/0Pg;

    invoke-interface {v0}, LX/0Pg;->c()I

    move-result v0

    .line 395380
    :goto_0
    return v0

    .line 395381
    :cond_0
    iget-object v0, p0, LX/2Lk;->b:LX/0Pg;

    invoke-interface {v0}, LX/0Pg;->c()I

    move-result v0

    if-nez v0, :cond_1

    .line 395382
    iget-object v0, p0, LX/2Lk;->a:LX/0Pg;

    invoke-interface {v0}, LX/0Pg;->c()I

    move-result v0

    goto :goto_0

    .line 395383
    :cond_1
    iget-object v0, p0, LX/2Lk;->a:LX/0Pg;

    invoke-interface {v0}, LX/0Pg;->c()I

    move-result v0

    iget-object v1, p0, LX/2Lk;->b:LX/0Pg;

    invoke-interface {v1}, LX/0Pg;->c()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method
