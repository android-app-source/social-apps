.class public final LX/39Y;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1Wm;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Wj;

.field public c:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/1Wm;


# direct methods
.method public constructor <init>(LX/1Wm;)V
    .locals 1

    .prologue
    .line 523243
    iput-object p1, p0, LX/39Y;->d:LX/1Wm;

    .line 523244
    move-object v0, p1

    .line 523245
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 523246
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 523247
    const-string v0, "FooterBackgroundComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 523248
    if-ne p0, p1, :cond_1

    .line 523249
    :cond_0
    :goto_0
    return v0

    .line 523250
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 523251
    goto :goto_0

    .line 523252
    :cond_3
    check-cast p1, LX/39Y;

    .line 523253
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 523254
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 523255
    if-eq v2, v3, :cond_0

    .line 523256
    iget-object v2, p0, LX/39Y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/39Y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/39Y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 523257
    goto :goto_0

    .line 523258
    :cond_5
    iget-object v2, p1, LX/39Y;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 523259
    :cond_6
    iget-object v2, p0, LX/39Y;->b:LX/1Wj;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/39Y;->b:LX/1Wj;

    iget-object v3, p1, LX/39Y;->b:LX/1Wj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 523260
    goto :goto_0

    .line 523261
    :cond_8
    iget-object v2, p1, LX/39Y;->b:LX/1Wj;

    if-nez v2, :cond_7

    .line 523262
    :cond_9
    iget-object v2, p0, LX/39Y;->c:LX/1X1;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/39Y;->c:LX/1X1;

    iget-object v3, p1, LX/39Y;->c:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 523263
    goto :goto_0

    .line 523264
    :cond_a
    iget-object v2, p1, LX/39Y;->c:LX/1X1;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 523265
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/39Y;

    .line 523266
    iget-object v1, v0, LX/39Y;->c:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/39Y;->c:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/39Y;->c:LX/1X1;

    .line 523267
    return-object v0

    .line 523268
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
