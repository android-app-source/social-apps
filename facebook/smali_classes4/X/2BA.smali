.class public final enum LX/2BA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2BA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2BA;

.field public static final enum ANY:LX/2BA;

.field public static final enum ARRAY:LX/2BA;

.field public static final enum BOOLEAN:LX/2BA;

.field public static final enum NUMBER:LX/2BA;

.field public static final enum NUMBER_FLOAT:LX/2BA;

.field public static final enum NUMBER_INT:LX/2BA;

.field public static final enum OBJECT:LX/2BA;

.field public static final enum SCALAR:LX/2BA;

.field public static final enum STRING:LX/2BA;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 378696
    new-instance v0, LX/2BA;

    const-string v1, "ANY"

    invoke-direct {v0, v1, v3}, LX/2BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2BA;->ANY:LX/2BA;

    .line 378697
    new-instance v0, LX/2BA;

    const-string v1, "SCALAR"

    invoke-direct {v0, v1, v4}, LX/2BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2BA;->SCALAR:LX/2BA;

    .line 378698
    new-instance v0, LX/2BA;

    const-string v1, "ARRAY"

    invoke-direct {v0, v1, v5}, LX/2BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2BA;->ARRAY:LX/2BA;

    .line 378699
    new-instance v0, LX/2BA;

    const-string v1, "OBJECT"

    invoke-direct {v0, v1, v6}, LX/2BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2BA;->OBJECT:LX/2BA;

    .line 378700
    new-instance v0, LX/2BA;

    const-string v1, "NUMBER"

    invoke-direct {v0, v1, v7}, LX/2BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2BA;->NUMBER:LX/2BA;

    .line 378701
    new-instance v0, LX/2BA;

    const-string v1, "NUMBER_FLOAT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/2BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2BA;->NUMBER_FLOAT:LX/2BA;

    .line 378702
    new-instance v0, LX/2BA;

    const-string v1, "NUMBER_INT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/2BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2BA;->NUMBER_INT:LX/2BA;

    .line 378703
    new-instance v0, LX/2BA;

    const-string v1, "STRING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/2BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2BA;->STRING:LX/2BA;

    .line 378704
    new-instance v0, LX/2BA;

    const-string v1, "BOOLEAN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/2BA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2BA;->BOOLEAN:LX/2BA;

    .line 378705
    const/16 v0, 0x9

    new-array v0, v0, [LX/2BA;

    sget-object v1, LX/2BA;->ANY:LX/2BA;

    aput-object v1, v0, v3

    sget-object v1, LX/2BA;->SCALAR:LX/2BA;

    aput-object v1, v0, v4

    sget-object v1, LX/2BA;->ARRAY:LX/2BA;

    aput-object v1, v0, v5

    sget-object v1, LX/2BA;->OBJECT:LX/2BA;

    aput-object v1, v0, v6

    sget-object v1, LX/2BA;->NUMBER:LX/2BA;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2BA;->NUMBER_FLOAT:LX/2BA;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2BA;->NUMBER_INT:LX/2BA;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2BA;->STRING:LX/2BA;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2BA;->BOOLEAN:LX/2BA;

    aput-object v2, v0, v1

    sput-object v0, LX/2BA;->$VALUES:[LX/2BA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 378706
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2BA;
    .locals 1

    .prologue
    .line 378707
    const-class v0, LX/2BA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2BA;

    return-object v0
.end method

.method public static values()[LX/2BA;
    .locals 1

    .prologue
    .line 378708
    sget-object v0, LX/2BA;->$VALUES:[LX/2BA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2BA;

    return-object v0
.end method


# virtual methods
.method public final isNumeric()Z
    .locals 1

    .prologue
    .line 378709
    sget-object v0, LX/2BA;->NUMBER:LX/2BA;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/2BA;->NUMBER_INT:LX/2BA;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/2BA;->NUMBER_FLOAT:LX/2BA;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isStructured()Z
    .locals 1

    .prologue
    .line 378710
    sget-object v0, LX/2BA;->OBJECT:LX/2BA;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/2BA;->ARRAY:LX/2BA;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
