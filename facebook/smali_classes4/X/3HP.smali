.class public LX/3HP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3HP;


# instance fields
.field public final a:LX/0sZ;

.field public final b:LX/0si;


# direct methods
.method public constructor <init>(LX/0sZ;LX/0si;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543450
    iput-object p1, p0, LX/3HP;->a:LX/0sZ;

    .line 543451
    iput-object p2, p0, LX/3HP;->b:LX/0si;

    .line 543452
    return-void
.end method

.method public static a(LX/0QB;)LX/3HP;
    .locals 5

    .prologue
    .line 543436
    sget-object v0, LX/3HP;->c:LX/3HP;

    if-nez v0, :cond_1

    .line 543437
    const-class v1, LX/3HP;

    monitor-enter v1

    .line 543438
    :try_start_0
    sget-object v0, LX/3HP;->c:LX/3HP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 543439
    if-eqz v2, :cond_0

    .line 543440
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 543441
    new-instance p0, LX/3HP;

    invoke-static {v0}, LX/0sZ;->b(LX/0QB;)LX/0sZ;

    move-result-object v3

    check-cast v3, LX/0sZ;

    invoke-static {v0}, LX/0sh;->a(LX/0QB;)LX/0sh;

    move-result-object v4

    check-cast v4, LX/0si;

    invoke-direct {p0, v3, v4}, LX/3HP;-><init>(LX/0sZ;LX/0si;)V

    .line 543442
    move-object v0, p0

    .line 543443
    sput-object v0, LX/3HP;->c:LX/3HP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 543444
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 543445
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 543446
    :cond_1
    sget-object v0, LX/3HP;->c:LX/3HP;

    return-object v0

    .line 543447
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 543448
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(Ljava/lang/String;)Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 543433
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    const/16 v2, 0x19

    sget-object v3, LX/21x;->COMPLETE:LX/21x;

    sget-object v4, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    sget-object v5, LX/21y;->DEFAULT_ORDER:LX/21y;

    move-object v1, p0

    move-object v7, v6

    move v10, v9

    move v11, v8

    invoke-direct/range {v0 .. v11}, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;-><init>(Ljava/lang/String;ILX/21x;LX/0rS;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/1kt;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/1kt",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 543434
    invoke-static {p1}, LX/3HP;->c(Ljava/lang/String;)Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    move-result-object v0

    .line 543435
    iget-object v1, p0, LX/3HP;->a:LX/0sZ;

    iget-object v2, p0, LX/3HP;->a:LX/0sZ;

    invoke-virtual {v2, v0}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0sZ;->a(LX/0zO;)LX/1kt;

    move-result-object v0

    return-object v0
.end method
