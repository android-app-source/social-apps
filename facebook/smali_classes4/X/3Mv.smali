.class public LX/3Mv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field public a:LX/3Mw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 556597
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3Mv;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 556596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/3Mv;
    .locals 8

    .prologue
    .line 556565
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 556566
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 556567
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 556568
    if-nez v1, :cond_0

    .line 556569
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 556570
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 556571
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 556572
    sget-object v1, LX/3Mv;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 556573
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 556574
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 556575
    :cond_1
    if-nez v1, :cond_4

    .line 556576
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 556577
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 556578
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 556579
    new-instance v7, LX/3Mv;

    invoke-direct {v7}, LX/3Mv;-><init>()V

    .line 556580
    invoke-static {v0}, LX/3Mw;->b(LX/0QB;)LX/3Mw;

    move-result-object v1

    check-cast v1, LX/3Mw;

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 556581
    iput-object v1, v7, LX/3Mv;->a:LX/3Mw;

    iput-object p0, v7, LX/3Mv;->b:LX/0Or;

    .line 556582
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 556583
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 556584
    if-nez v1, :cond_2

    .line 556585
    sget-object v0, LX/3Mv;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Mv;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 556586
    :goto_1
    if-eqz v0, :cond_3

    .line 556587
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 556588
    :goto_3
    check-cast v0, LX/3Mv;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 556589
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 556590
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 556591
    :catchall_1
    move-exception v0

    .line 556592
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 556593
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 556594
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 556595
    :cond_2
    :try_start_8
    sget-object v0, LX/3Mv;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Mv;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static b(Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;)Lcom/facebook/user/model/User;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 556538
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {p0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->B()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v0

    new-instance v1, Lcom/facebook/user/model/Name;

    invoke-virtual {p0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->U()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v3, v3, v2}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 556539
    iput-object v1, v0, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 556540
    move-object v0, v0

    .line 556541
    invoke-virtual {p0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->G()Ljava/lang/String;

    move-result-object v1

    .line 556542
    iput-object v1, v0, LX/0XI;->l:Ljava/lang/String;

    .line 556543
    move-object v0, v0

    .line 556544
    invoke-virtual {p0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->D()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 556545
    iput-object v1, v0, LX/0XI;->n:Ljava/lang/String;

    .line 556546
    move-object v0, v0

    .line 556547
    invoke-virtual {p0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->r()Z

    move-result v1

    .line 556548
    iput-boolean v1, v0, LX/0XI;->z:Z

    .line 556549
    move-object v0, v0

    .line 556550
    invoke-virtual {p0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->n()D

    move-result-wide v2

    double-to-float v1, v2

    .line 556551
    iput v1, v0, LX/0XI;->t:F

    .line 556552
    move-object v0, v0

    .line 556553
    invoke-virtual {p0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->E()Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel$TimelineContextItemsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel$TimelineContextItemsModel;->a()LX/0Px;

    move-result-object v1

    .line 556554
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    move-object v1, v1

    .line 556555
    iput-object v1, v0, LX/0XI;->x:Ljava/lang/String;

    .line 556556
    move-object v0, v0

    .line 556557
    invoke-virtual {p0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->k()Z

    move-result v1

    .line 556558
    iput-boolean v1, v0, LX/0XI;->aa:Z

    .line 556559
    move-object v0, v0

    .line 556560
    invoke-virtual {p0}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->t()Z

    move-result v1

    .line 556561
    iput-boolean v1, v0, LX/0XI;->w:Z

    .line 556562
    move-object v0, v0

    .line 556563
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 556564
    return-object v0

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel$TimelineContextItemsModel$TimelineContextItemsNodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel$TimelineContextItemsModel$TimelineContextItemsNodesModel;->a()Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel$TimelineContextItemsModel$TimelineContextItemsNodesModel$TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel$TimelineContextItemsModel$TimelineContextItemsNodesModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
