.class public LX/2Pt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile sInstance__com_facebook_omnistore_module_OmnistoreInitTimeBugReportInfo__INJECTED_BY_TemplateInjector:LX/2Pt;


# instance fields
.field private final OMNISTORE_INIT_INFO_FILENAME:Ljava/lang/String;

.field public mClock:LX/0SF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final mInitTimes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/2cZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 407344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407345
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2Pt;->mInitTimes:Ljava/util/ArrayList;

    .line 407346
    const-string v0, "omnistore_init_json.txt"

    iput-object v0, p0, LX/2Pt;->OMNISTORE_INIT_INFO_FILENAME:Ljava/lang/String;

    .line 407347
    return-void
.end method

.method public static getInstance__com_facebook_omnistore_module_OmnistoreInitTimeBugReportInfo__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pt;
    .locals 4

    .prologue
    .line 407329
    sget-object v0, LX/2Pt;->sInstance__com_facebook_omnistore_module_OmnistoreInitTimeBugReportInfo__INJECTED_BY_TemplateInjector:LX/2Pt;

    if-nez v0, :cond_1

    .line 407330
    const-class v1, LX/2Pt;

    monitor-enter v1

    .line 407331
    :try_start_0
    sget-object v0, LX/2Pt;->sInstance__com_facebook_omnistore_module_OmnistoreInitTimeBugReportInfo__INJECTED_BY_TemplateInjector:LX/2Pt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 407332
    if-eqz v2, :cond_0

    .line 407333
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 407334
    new-instance p0, LX/2Pt;

    invoke-direct {p0}, LX/2Pt;-><init>()V

    .line 407335
    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SF;

    .line 407336
    iput-object v3, p0, LX/2Pt;->mClock:LX/0SF;

    .line 407337
    move-object v0, p0

    .line 407338
    sput-object v0, LX/2Pt;->sInstance__com_facebook_omnistore_module_OmnistoreInitTimeBugReportInfo__INJECTED_BY_TemplateInjector:LX/2Pt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 407339
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 407340
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 407341
    :cond_1
    sget-object v0, LX/2Pt;->sInstance__com_facebook_omnistore_module_OmnistoreInitTimeBugReportInfo__INJECTED_BY_TemplateInjector:LX/2Pt;

    return-object v0

    .line 407342
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 407343
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static logPoint(LX/2Pt;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 407327
    iget-object v0, p0, LX/2Pt;->mInitTimes:Ljava/util/ArrayList;

    new-instance v1, LX/2cZ;

    iget-object v2, p0, LX/2Pt;->mClock:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    invoke-direct {v1, v2, v3, p1}, LX/2cZ;-><init>(JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407328
    return-void
.end method

.method private writeFile(Ljava/io/File;)Landroid/net/Uri;
    .locals 11

    .prologue
    .line 407301
    new-instance v2, Ljava/io/File;

    const-string v0, "omnistore_init_json.txt"

    invoke-direct {v2, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 407302
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 407303
    iget-object v0, p0, LX/2Pt;->mInitTimes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    iget-object v0, p0, LX/2Pt;->mInitTimes:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cZ;

    .line 407304
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 407305
    :try_start_0
    const-string v6, "time"

    iget-wide v8, v0, LX/2cZ;->mTime:J

    invoke-virtual {v5, v6, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 407306
    const-string v6, "event"

    iget-object v0, v0, LX/2cZ;->mEventString:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 407307
    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 407308
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 407309
    :catch_0
    move-exception v0

    .line 407310
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 407311
    :cond_0
    new-instance v4, Ljava/io/PrintWriter;

    invoke-direct {v4, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    .line 407312
    :try_start_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 407313
    invoke-virtual {v4}, Ljava/io/PrintWriter;->close()V

    .line 407314
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 407315
    :catch_1
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 407316
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v4}, Ljava/io/PrintWriter;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :goto_2
    throw v0

    :catch_2
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_1
    invoke-virtual {v4}, Ljava/io/PrintWriter;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407324
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 407325
    const-string v1, "omnistore_init_json.txt"

    invoke-direct {p0, p1}, LX/2Pt;->writeFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407326
    return-object v0
.end method

.method public getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407319
    invoke-direct {p0, p1}, LX/2Pt;->writeFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 407320
    new-instance v1, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v2, "omnistore_init_json.txt"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "text/plain"

    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 407321
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 407322
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407323
    return-object v0
.end method

.method public prepareDataForWriting()V
    .locals 0

    .prologue
    .line 407318
    return-void
.end method

.method public shouldSendAsync()Z
    .locals 1

    .prologue
    .line 407317
    const/4 v0, 0x0

    return v0
.end method
