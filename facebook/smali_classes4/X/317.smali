.class public LX/317;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/315;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/317;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 486687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486688
    return-void
.end method

.method public static a(LX/0QB;)LX/317;
    .locals 3

    .prologue
    .line 486689
    sget-object v0, LX/317;->a:LX/317;

    if-nez v0, :cond_1

    .line 486690
    const-class v1, LX/317;

    monitor-enter v1

    .line 486691
    :try_start_0
    sget-object v0, LX/317;->a:LX/317;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 486692
    if-eqz v2, :cond_0

    .line 486693
    :try_start_1
    new-instance v0, LX/317;

    invoke-direct {v0}, LX/317;-><init>()V

    .line 486694
    move-object v0, v0

    .line 486695
    sput-object v0, LX/317;->a:LX/317;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 486696
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 486697
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 486698
    :cond_1
    sget-object v0, LX/317;->a:LX/317;

    return-object v0

    .line 486699
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 486700
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 486701
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/io/InputStream;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 486702
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/resources/impl/loading/LanguagePackInfo;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 486703
    const/4 v0, 0x0

    return v0
.end method
