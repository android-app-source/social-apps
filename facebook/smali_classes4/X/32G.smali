.class public final enum LX/32G;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/32G;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/32G;

.field public static final enum OnAppBackgrounded:LX/32G;

.field public static final enum OnCloseToDalvikHeapLimit:LX/32G;

.field public static final enum OnSystemLowMemoryWhileAppInBackground:LX/32G;

.field public static final enum OnSystemLowMemoryWhileAppInForeground:LX/32G;


# instance fields
.field private mSuggestedTrimRatio:D


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    .line 489543
    new-instance v0, LX/32G;

    const-string v1, "OnCloseToDalvikHeapLimit"

    invoke-direct {v0, v1, v6, v2, v3}, LX/32G;-><init>(Ljava/lang/String;ID)V

    sput-object v0, LX/32G;->OnCloseToDalvikHeapLimit:LX/32G;

    .line 489544
    new-instance v0, LX/32G;

    const-string v1, "OnSystemLowMemoryWhileAppInForeground"

    invoke-direct {v0, v1, v7, v2, v3}, LX/32G;-><init>(Ljava/lang/String;ID)V

    sput-object v0, LX/32G;->OnSystemLowMemoryWhileAppInForeground:LX/32G;

    .line 489545
    new-instance v0, LX/32G;

    const-string v1, "OnSystemLowMemoryWhileAppInBackground"

    invoke-direct {v0, v1, v8, v4, v5}, LX/32G;-><init>(Ljava/lang/String;ID)V

    sput-object v0, LX/32G;->OnSystemLowMemoryWhileAppInBackground:LX/32G;

    .line 489546
    new-instance v0, LX/32G;

    const-string v1, "OnAppBackgrounded"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v4, v5}, LX/32G;-><init>(Ljava/lang/String;ID)V

    sput-object v0, LX/32G;->OnAppBackgrounded:LX/32G;

    .line 489547
    const/4 v0, 0x4

    new-array v0, v0, [LX/32G;

    sget-object v1, LX/32G;->OnCloseToDalvikHeapLimit:LX/32G;

    aput-object v1, v0, v6

    sget-object v1, LX/32G;->OnSystemLowMemoryWhileAppInForeground:LX/32G;

    aput-object v1, v0, v7

    sget-object v1, LX/32G;->OnSystemLowMemoryWhileAppInBackground:LX/32G;

    aput-object v1, v0, v8

    const/4 v1, 0x3

    sget-object v2, LX/32G;->OnAppBackgrounded:LX/32G;

    aput-object v2, v0, v1

    sput-object v0, LX/32G;->$VALUES:[LX/32G;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ID)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D)V"
        }
    .end annotation

    .prologue
    .line 489550
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 489551
    iput-wide p3, p0, LX/32G;->mSuggestedTrimRatio:D

    .line 489552
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/32G;
    .locals 1

    .prologue
    .line 489553
    const-class v0, LX/32G;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/32G;

    return-object v0
.end method

.method public static values()[LX/32G;
    .locals 1

    .prologue
    .line 489549
    sget-object v0, LX/32G;->$VALUES:[LX/32G;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/32G;

    return-object v0
.end method


# virtual methods
.method public final getSuggestedTrimRatio()D
    .locals 2

    .prologue
    .line 489548
    iget-wide v0, p0, LX/32G;->mSuggestedTrimRatio:D

    return-wide v0
.end method
