.class public LX/3In;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 547202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/04D;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 547203
    const-string v0, "source_jewel"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547204
    sget-object v0, LX/04D;->PERMALINK_FROM_NOTIFICATIONS:LX/04D;

    .line 547205
    :goto_0
    return-object v0

    .line 547206
    :cond_0
    const-string v0, "source_system_tray"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "source_lockscreen"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 547207
    :cond_1
    sget-object v0, LX/04D;->PERMALINK_FROM_PUSH:LX/04D;

    goto :goto_0

    .line 547208
    :cond_2
    sget-object v0, LX/04D;->PERMALINK:LX/04D;

    goto :goto_0
.end method

.method public static a(LX/093;Ljava/util/Set;LX/1Bv;LX/1C2;LX/0lF;Ljava/lang/String;LX/04D;LX/04H;LX/098;)V
    .locals 7
    .param p1    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/093;",
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;",
            "LX/1Bv;",
            "LX/1C2;",
            "LX/0lF;",
            "Ljava/lang/String;",
            "LX/04D;",
            "LX/04H;",
            "LX/098;",
            ")V"
        }
    .end annotation

    .prologue
    .line 547209
    iget-boolean v0, p0, LX/093;->l:Z

    move v0, v0

    .line 547210
    if-eqz v0, :cond_0

    .line 547211
    :goto_0
    return-void

    .line 547212
    :cond_0
    invoke-virtual {p2}, LX/1Bv;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/093;->a(Ljava/util/Set;Ljava/lang/String;)V

    move-object v0, p3

    move-object v1, p0

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    move-object v6, p8

    .line 547213
    invoke-virtual/range {v0 .. v6}, LX/1C2;->a(LX/093;LX/0lF;Ljava/lang/String;LX/04D;LX/04H;LX/098;)LX/1C2;

    .line 547214
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/093;->a(Z)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/0Or;)V
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLVideo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 547215
    if-nez p0, :cond_1

    .line 547216
    :cond_0
    :goto_0
    return-void

    .line 547217
    :cond_1
    const/4 v0, 0x0

    .line 547218
    if-nez p0, :cond_3

    .line 547219
    :cond_2
    :goto_1
    move v0, v0

    .line 547220
    if-nez v0, :cond_0

    .line 547221
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "667066080125529"

    .line 547222
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 547223
    move-object v0, v0

    .line 547224
    invoke-virtual {v0, p1}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 547225
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 547226
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLMedia;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 547227
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    invoke-static {v0, v1}, LX/3In;->a(ZLcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(ZLcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 547228
    if-eqz p0, :cond_1

    .line 547229
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne p1, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 547230
    :cond_1
    return v0
.end method
