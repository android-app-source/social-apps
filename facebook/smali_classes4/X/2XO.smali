.class public LX/2XO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2XP;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2XO;


# instance fields
.field private final a:LX/0Xl;

.field public final b:Ljava/util/concurrent/ScheduledExecutorService;

.field public c:Ljava/util/concurrent/ScheduledFuture;

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 419874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419875
    const/4 v0, 0x0

    iput-object v0, p0, LX/2XO;->c:Ljava/util/concurrent/ScheduledFuture;

    .line 419876
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/2XO;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 419877
    iput-object p1, p0, LX/2XO;->a:LX/0Xl;

    .line 419878
    iput-object p2, p0, LX/2XO;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 419879
    return-void
.end method

.method public static a(LX/0QB;)LX/2XO;
    .locals 5

    .prologue
    .line 419880
    sget-object v0, LX/2XO;->e:LX/2XO;

    if-nez v0, :cond_1

    .line 419881
    const-class v1, LX/2XO;

    monitor-enter v1

    .line 419882
    :try_start_0
    sget-object v0, LX/2XO;->e:LX/2XO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 419883
    if-eqz v2, :cond_0

    .line 419884
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 419885
    new-instance p0, LX/2XO;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {p0, v3, v4}, LX/2XO;-><init>(LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 419886
    move-object v0, p0

    .line 419887
    sput-object v0, LX/2XO;->e:LX/2XO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 419888
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 419889
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 419890
    :cond_1
    sget-object v0, LX/2XO;->e:LX/2XO;

    return-object v0

    .line 419891
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 419892
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2XO;Z)V
    .locals 3

    .prologue
    .line 419867
    iget-object v0, p0, LX/2XO;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 419868
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 419869
    const-string v0, "com.facebook.rti.mqtt.intent.ACTION_WAKEUP"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 419870
    const-string v2, "EXTRA_SKIP_PING"

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 419871
    iget-object v0, p0, LX/2XO;->a:LX/0Xl;

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 419872
    :cond_0
    return-void

    .line 419873
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 419866
    iget-object v0, p0, LX/2XO;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method
