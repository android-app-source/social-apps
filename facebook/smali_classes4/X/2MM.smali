.class public LX/2MM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2MM;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/content/ContentResolver;

.field private final c:LX/1Er;

.field private final d:LX/0Sh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/ContentResolver;LX/1Er;LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396462
    iput-object p1, p0, LX/2MM;->a:Landroid/content/Context;

    .line 396463
    iput-object p2, p0, LX/2MM;->b:Landroid/content/ContentResolver;

    .line 396464
    iput-object p3, p0, LX/2MM;->c:LX/1Er;

    .line 396465
    iput-object p4, p0, LX/2MM;->d:LX/0Sh;

    .line 396466
    return-void
.end method

.method public static a(LX/0QB;)LX/2MM;
    .locals 7

    .prologue
    .line 396467
    sget-object v0, LX/2MM;->e:LX/2MM;

    if-nez v0, :cond_1

    .line 396468
    const-class v1, LX/2MM;

    monitor-enter v1

    .line 396469
    :try_start_0
    sget-object v0, LX/2MM;->e:LX/2MM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 396470
    if-eqz v2, :cond_0

    .line 396471
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 396472
    new-instance p0, LX/2MM;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v4

    check-cast v4, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v5

    check-cast v5, LX/1Er;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2MM;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;LX/1Er;LX/0Sh;)V

    .line 396473
    move-object v0, p0

    .line 396474
    sput-object v0, LX/2MM;->e:LX/2MM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396475
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 396476
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 396477
    :cond_1
    sget-object v0, LX/2MM;->e:LX/2MM;

    return-object v0

    .line 396478
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 396479
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2MM;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Ljava/io/File;
    .locals 6
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 396480
    iget-object v0, p0, LX/2MM;->b:Landroid/content/ContentResolver;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_data"

    aput-object v3, v2, v1

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 396481
    if-nez v1, :cond_0

    .line 396482
    :goto_0
    return-object v5

    .line 396483
    :cond_0
    :try_start_0
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 396484
    if-ltz v0, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 396485
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 396486
    if-nez v2, :cond_1

    .line 396487
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 396488
    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 396489
    const-string v3, "http"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_3

    .line 396490
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v5, v0

    goto :goto_0

    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;LX/46h;)LX/46f;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 396491
    invoke-virtual {p0, p1}, LX/2MM;->a(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v1

    .line 396492
    if-eqz v1, :cond_0

    .line 396493
    new-instance v0, LX/46f;

    invoke-direct {v0, v1, v3}, LX/46f;-><init>(Ljava/io/File;Z)V

    .line 396494
    :goto_0
    return-object v0

    .line 396495
    :cond_0
    iget-object v0, p0, LX/2MM;->c:LX/1Er;

    const-string v1, "backing_file_copy"

    const-string v2, ".tmp"

    invoke-virtual {v0, v1, v2, p2}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v1

    .line 396496
    if-nez v1, :cond_1

    .line 396497
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to create temp file"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 396498
    :cond_1
    new-instance v0, LX/46e;

    invoke-direct {v0, p0, p1}, LX/46e;-><init>(LX/2MM;Landroid/net/Uri;)V

    .line 396499
    new-array v2, v3, [LX/3AS;

    invoke-static {v1, v2}, LX/1t3;->a(Ljava/io/File;[LX/3AS;)LX/3AU;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1vI;->a(LX/3AU;)J

    .line 396500
    new-instance v0, LX/46f;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/46f;-><init>(Ljava/io/File;Z)V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;)Ljava/io/File;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 396501
    iget-object v1, p0, LX/2MM;->d:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->b()V

    .line 396502
    if-nez p1, :cond_1

    .line 396503
    :cond_0
    :goto_0
    return-object v0

    .line 396504
    :cond_1
    const-string v1, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 396505
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 396506
    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_4

    .line 396507
    iget-object v1, p0, LX/2MM;->a:Landroid/content/Context;

    invoke-static {v1, p1}, Landroid/provider/DocumentsContract;->isDocumentUri(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v1

    move v1, v1

    .line 396508
    if-eqz v1, :cond_4

    .line 396509
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 396510
    invoke-static {p1}, Landroid/provider/DocumentsContract;->getDocumentId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 396511
    const-string v1, ":"

    invoke-static {v1}, LX/2Cb;->on(Ljava/lang/String;)LX/2Cb;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/2Cb;->limit(I)LX/2Cb;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 396512
    array-length v1, v0

    if-eq v1, v3, :cond_5

    .line 396513
    const/4 v0, 0x0

    .line 396514
    :cond_3
    :goto_1
    move-object v0, v0

    .line 396515
    goto :goto_0

    .line 396516
    :cond_4
    const-string v1, "media"

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 396517
    invoke-static {p0, p1, v0, v0}, LX/2MM;->a(LX/2MM;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 396518
    :cond_5
    aget-object v0, v0, v2

    .line 396519
    const-string v1, "_id=?"

    .line 396520
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 396521
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0, v1, v2}, LX/2MM;->a(LX/2MM;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 396522
    if-nez v0, :cond_3

    .line 396523
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0, v1, v2}, LX/2MM;->a(LX/2MM;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Landroid/net/Uri;)J
    .locals 3

    .prologue
    .line 396524
    iget-object v0, p0, LX/2MM;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 396525
    :try_start_0
    invoke-virtual {p0, p1}, LX/2MM;->a(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    .line 396526
    if-eqz v0, :cond_0

    .line 396527
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 396528
    :goto_0
    return-wide v0

    .line 396529
    :cond_0
    iget-object v0, p0, LX/2MM;->b:Landroid/content/ContentResolver;

    const-string v1, "r"

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 396530
    :try_start_1
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getStatSize()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 396531
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V

    goto :goto_0

    .line 396532
    :catch_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 396533
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V

    throw v0
.end method
