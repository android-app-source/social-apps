.class public LX/2TO;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field public final h:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 414314
    const-class v0, LX/2TO;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2TO;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414316
    const/4 v0, 0x0

    iput v0, p0, LX/2TO;->b:I

    .line 414317
    const/4 v0, 0x1

    iput v0, p0, LX/2TO;->c:I

    .line 414318
    const/4 v0, 0x2

    iput v0, p0, LX/2TO;->d:I

    .line 414319
    const/4 v0, 0x3

    iput v0, p0, LX/2TO;->e:I

    .line 414320
    const/4 v0, 0x4

    iput v0, p0, LX/2TO;->f:I

    .line 414321
    const/4 v0, 0x5

    iput v0, p0, LX/2TO;->g:I

    .line 414322
    iput-object p1, p0, LX/2TO;->h:Landroid/content/Context;

    .line 414323
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)Landroid/database/Cursor;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 414324
    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "_data"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "orientation"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "mime_type"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "datetaken"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string v1, "_display_name"

    aput-object v1, v2, v0

    .line 414325
    if-nez p2, :cond_0

    .line 414326
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object p2

    .line 414327
    :cond_0
    iget-object v0, p0, LX/2TO;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {p2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const-string v5, "date_modified DESC"

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 414328
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 414329
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/photos/base/media/PhotoItem;
    .locals 12
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/facebook/photos/base/media/PhotoItem;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 414330
    const/4 v0, 0x5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 414331
    if-nez v4, :cond_0

    move-object v0, v1

    .line 414332
    :goto_0
    return-object v0

    .line 414333
    :cond_0
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 414334
    if-nez v5, :cond_1

    move-object v0, v1

    .line 414335
    goto :goto_0

    .line 414336
    :cond_1
    const/4 v0, 0x3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 414337
    if-eqz v0, :cond_2

    const-string v2, "*/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 414338
    :cond_2
    invoke-static {v5}, LX/46I;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 414339
    if-nez v0, :cond_3

    move-object v0, v1

    .line 414340
    goto :goto_0

    :cond_3
    move-object v1, v0

    .line 414341
    const/4 v0, 0x4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 414342
    invoke-static {v4, v6, v7}, LX/75E;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 414343
    if-eqz p1, :cond_5

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 414344
    :goto_1
    new-instance v0, LX/74k;

    invoke-direct {v0}, LX/74k;-><init>()V

    const/4 v8, 0x0

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 414345
    iput-wide v8, v0, LX/74k;->h:J

    .line 414346
    iget-object v10, v0, LX/74k;->k:LX/4gN;

    .line 414347
    iput-wide v8, v10, LX/4gN;->d:J

    .line 414348
    move-object v0, v0

    .line 414349
    invoke-virtual {v0, v5}, LX/74k;->a(Ljava/lang/String;)LX/74k;

    move-result-object v0

    .line 414350
    if-eqz v4, :cond_4

    .line 414351
    iget-object v5, v0, LX/74k;->k:LX/4gN;

    invoke-virtual {v5, v4}, LX/4gN;->a(Ljava/lang/String;)LX/4gN;

    .line 414352
    :cond_4
    move-object v0, v0

    .line 414353
    invoke-virtual {v0, v1}, LX/74k;->c(Ljava/lang/String;)LX/74k;

    move-result-object v0

    .line 414354
    iget-object v10, v0, LX/74k;->k:LX/4gN;

    .line 414355
    iput-wide v6, v10, LX/4gN;->b:J

    .line 414356
    move-object v0, v0

    .line 414357
    const/4 v1, 0x2

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/74k;->a(I)LX/74k;

    move-result-object v0

    .line 414358
    iput-wide v2, v0, LX/74k;->a:J

    .line 414359
    move-object v0, v0

    .line 414360
    invoke-virtual {v0}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    goto :goto_0

    .line 414361
    :cond_5
    const-wide/16 v2, -0x1

    goto :goto_1
.end method


# virtual methods
.method public a(Ljava/util/Map;Ljava/lang/String;Ljava/util/List;)Ljava/util/Map;
    .locals 7
    .param p1    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 414362
    invoke-direct {p0, p2, p3}, LX/2TO;->a(Ljava/lang/String;Ljava/util/List;)Landroid/database/Cursor;

    move-result-object v0

    .line 414363
    if-nez v0, :cond_0

    .line 414364
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 414365
    :goto_0
    return-object v0

    .line 414366
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 414367
    :cond_1
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 414368
    invoke-static {v0, p1}, LX/2TO;->b(Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v2

    .line 414369
    if-eqz v2, :cond_1

    .line 414370
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->j()J

    move-result-wide v5

    invoke-static {v3, v5, v6}, LX/75E;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v3

    .line 414371
    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 414372
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 414373
    move-object v0, v1

    .line 414374
    goto :goto_0
.end method
