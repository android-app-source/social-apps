.class public LX/2k0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0po;
.implements LX/2k1;
.implements LX/0sk;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile l:LX/2k0;


# instance fields
.field private final b:LX/2k2;

.field public final c:LX/0SG;

.field private final d:LX/2kD;

.field private final e:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final f:LX/2kF;

.field private final g:LX/0SI;

.field private final h:LX/0t9;

.field private final i:LX/0tC;

.field private final j:LX/2kC;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kC",
            "<",
            "LX/2kR;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 455146
    const-class v0, LX/2k0;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2k0;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2k2;LX/0SG;LX/0pr;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0SI;LX/0t9;LX/0tC;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 454980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454981
    new-instance v0, LX/2kC;

    invoke-direct {v0}, LX/2kC;-><init>()V

    iput-object v0, p0, LX/2k0;->j:LX/2kC;

    .line 454982
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/2k0;->k:Ljava/util/concurrent/atomic/AtomicLong;

    .line 454983
    iput-object p2, p0, LX/2k0;->b:LX/2k2;

    .line 454984
    iput-object p3, p0, LX/2k0;->c:LX/0SG;

    .line 454985
    iput-object p5, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 454986
    iput-object p6, p0, LX/2k0;->g:LX/0SI;

    .line 454987
    iput-object p7, p0, LX/2k0;->h:LX/0t9;

    .line 454988
    iput-object p8, p0, LX/2k0;->i:LX/0tC;

    .line 454989
    invoke-static {p1}, LX/2k0;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, LX/2kD;->a(Ljava/io/File;)LX/2kD;

    move-result-object v0

    iput-object v0, p0, LX/2k0;->d:LX/2kD;

    .line 454990
    new-instance v0, LX/2kF;

    invoke-direct {v0}, LX/2kF;-><init>()V

    iput-object v0, p0, LX/2k0;->f:LX/2kF;

    .line 454991
    invoke-interface {p4, p0}, LX/0pr;->a(LX/0po;)V

    .line 454992
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Ljava/lang/String;ILX/0w5;ILjava/lang/String;JLjava/util/Collection;)J
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/0w5;",
            "I",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 454993
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 454994
    sget-object v2, LX/2k9;->b:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 454995
    if-lez p4, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 454996
    sget-object v2, LX/2k9;->c:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 454997
    const-string v2, "models"

    const/4 v4, 0x0

    const v5, 0x5666979

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {p0, v2, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    const v2, -0x478dcc93

    invoke-static {v2}, LX/03h;->a(I)V

    .line 454998
    const-wide/16 v2, -0x1

    cmp-long v2, v4, v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 454999
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 455000
    sget-object v2, LX/2k5;->f:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455001
    sget-object v2, LX/2k5;->g:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455002
    sget-object v2, LX/2k5;->b:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 455003
    sget-object v2, LX/2k5;->c:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 455004
    sget-object v2, LX/2k5;->d:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p5}, LX/0w5;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455005
    sget-object v2, LX/2k5;->e:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p5}, LX/0w5;->e()[B

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 455006
    sget-object v2, LX/2k5;->h:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 455007
    sget-object v2, LX/2k5;->i:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 455008
    invoke-virtual/range {p7 .. p7}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v4, 0x20

    if-ne v2, v4, :cond_3

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 455009
    sget-object v2, LX/2k5;->j:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p7 .. p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455010
    if-eqz p10, :cond_0

    .line 455011
    sget-object v2, LX/2k5;->k:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    const-string v4, ","

    move-object/from16 v0, p10

    invoke-static {v4, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455012
    :cond_0
    const-string v2, "edges"

    const/4 v4, 0x0

    const v5, 0x64c01bef

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {p0, v2, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    const v2, 0x3da97b5f

    invoke-static {v2}, LX/03h;->a(I)V

    .line 455013
    const-wide/16 v2, -0x1

    cmp-long v2, v4, v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 455014
    if-eqz p10, :cond_6

    .line 455015
    invoke-interface/range {p10 .. p10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 455016
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 455017
    sget-object v7, LX/2kB;->a:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455018
    sget-object v2, LX/2kB;->b:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 455019
    const-string v2, "tags"

    const/4 v7, 0x0

    const v8, 0x55590138

    invoke-static {v8}, LX/03h;->a(I)V

    invoke-virtual {p0, v2, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    const v2, -0x6b508b80

    invoke-static {v2}, LX/03h;->a(I)V

    .line 455020
    const-wide/16 v8, -0x1

    cmp-long v2, v6, v8

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    goto :goto_4

    .line 455021
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 455022
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 455023
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 455024
    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    .line 455025
    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    .line 455026
    :cond_6
    return-wide v4
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIJJ)J
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 455027
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 455028
    sget-object v0, LX/2k7;->b:LX/0U1;

    .line 455029
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 455030
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455031
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x18

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 455032
    sget-object v0, LX/2k7;->c:LX/0U1;

    .line 455033
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 455034
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455035
    sget-object v0, LX/2k7;->d:LX/0U1;

    .line 455036
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 455037
    invoke-virtual {v1, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455038
    sget-object v0, LX/2k7;->e:LX/0U1;

    .line 455039
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 455040
    invoke-virtual {v1, v0, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455041
    sget-object v0, LX/2k7;->f:LX/0U1;

    .line 455042
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 455043
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 455044
    sget-object v0, LX/2k7;->g:LX/0U1;

    .line 455045
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 455046
    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 455047
    if-ltz p7, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 455048
    sget-object v0, LX/2k7;->h:LX/0U1;

    .line 455049
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 455050
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 455051
    sget-object v0, LX/2k7;->i:LX/0U1;

    .line 455052
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 455053
    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 455054
    const-wide/16 v2, 0x0

    cmp-long v0, p10, v2

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 455055
    const-wide/32 v2, 0x19bfcc00

    invoke-static {p10, p11, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 455056
    sget-object v0, LX/2k7;->j:LX/0U1;

    .line 455057
    iget-object p1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, p1

    .line 455058
    add-long/2addr v2, p8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 455059
    const-string v0, "chunks"

    const/4 v2, 0x0

    const v3, -0x68eef05b

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {p0, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const v0, -0x3f965d69

    invoke-static {v0}, LX/03h;->a(I)V

    .line 455060
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 455061
    return-wide v2

    .line 455062
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 455063
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 455064
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 455065
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private a(Ljava/lang/String;Ljava/util/Collection;)LX/1Yt;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/1Yt;"
        }
    .end annotation

    .prologue
    .line 455066
    new-instance v0, LX/1Yt;

    invoke-direct {v0}, LX/1Yt;-><init>()V

    .line 455067
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 455068
    :goto_0
    return-object v0

    .line 455069
    :cond_0
    iget-object v1, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 455070
    const/4 v1, 0x0

    .line 455071
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 455072
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 455073
    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 455074
    const-string v4, "tag"

    invoke-static {v4, p2}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v4

    .line 455075
    new-instance v5, Ljava/lang/StringBuilder;

    const-string p0, "SELECT _id FROM edges WHERE session_id = ? AND _id IN (SELECT node_id FROM tags WHERE "

    invoke-direct {v5, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    move-object v1, v3

    .line 455076
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 455077
    sget-object v3, LX/2k5;->a:LX/0U1;

    .line 455078
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 455079
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 455080
    :cond_1
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 455081
    invoke-virtual {v0, v4, v5}, LX/1Yt;->a(J)V

    .line 455082
    invoke-static {v2, v4, v5}, LX/2k0;->a(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 455083
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_1

    .line 455084
    :cond_2
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static a(LX/0QB;)LX/2k0;
    .locals 12

    .prologue
    .line 455085
    sget-object v0, LX/2k0;->l:LX/2k0;

    if-nez v0, :cond_1

    .line 455086
    const-class v1, LX/2k0;

    monitor-enter v1

    .line 455087
    :try_start_0
    sget-object v0, LX/2k0;->l:LX/2k0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 455088
    if-eqz v2, :cond_0

    .line 455089
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 455090
    new-instance v3, LX/2k0;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2k2;->a(LX/0QB;)LX/2k2;

    move-result-object v5

    check-cast v5, LX/2k2;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v7

    check-cast v7, LX/0pr;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v8

    check-cast v8, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v9

    check-cast v9, LX/0SI;

    invoke-static {v0}, LX/0t9;->b(LX/0QB;)LX/0t9;

    move-result-object v10

    check-cast v10, LX/0t9;

    invoke-static {v0}, LX/0tC;->b(LX/0QB;)LX/0tC;

    move-result-object v11

    check-cast v11, LX/0tC;

    invoke-direct/range {v3 .. v11}, LX/2k0;-><init>(Landroid/content/Context;LX/2k2;LX/0SG;LX/0pr;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0SI;LX/0t9;LX/0tC;)V

    .line 455091
    move-object v0, v3

    .line 455092
    sput-object v0, LX/2k0;->l:LX/2k0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 455093
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 455094
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 455095
    :cond_1
    sget-object v0, LX/2k0;->l:LX/2k0;

    return-object v0

    .line 455096
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 455097
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;[J)LX/9JB;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 455098
    array-length v0, p2

    if-nez v0, :cond_1

    .line 455099
    :cond_0
    :goto_0
    return-object v6

    .line 455100
    :cond_1
    new-instance v0, LX/3DU;

    iget-object v1, p0, LX/2k0;->g:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    iget-object v3, p0, LX/2k0;->d:LX/2kD;

    iget-object v4, p0, LX/2k0;->c:LX/0SG;

    iget-object v5, p0, LX/2k0;->i:LX/0tC;

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, LX/3DU;-><init>(Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/database/sqlite/SQLiteDatabase;LX/2kD;LX/0SG;LX/0tC;Ljava/lang/String;)V

    .line 455101
    iget-object v1, p0, LX/2k0;->h:LX/0t9;

    invoke-virtual {v1, v0, p2}, LX/0t9;->a(LX/2vL;[J)V

    .line 455102
    iget-object v1, v0, LX/3DU;->a:Ljava/util/HashMap;

    move-object v1, v1

    .line 455103
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v0

    if-gt v0, v7, :cond_2

    move v0, v7

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 455104
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 455105
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JB;

    move-object v6, v0

    goto :goto_0

    .line 455106
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 455107
    const-string v2, "chunks"

    const/4 v0, 0x6

    new-array v3, v0, [Ljava/lang/String;

    sget-object v0, LX/2k7;->d:LX/0U1;

    .line 455108
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 455109
    aput-object v0, v3, v1

    sget-object v0, LX/2k7;->e:LX/0U1;

    .line 455110
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 455111
    aput-object v0, v3, v5

    const/4 v0, 0x2

    sget-object v4, LX/2k7;->f:LX/0U1;

    .line 455112
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 455113
    aput-object v4, v3, v0

    const/4 v0, 0x3

    sget-object v4, LX/2k7;->g:LX/0U1;

    .line 455114
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 455115
    aput-object v4, v3, v0

    const/4 v0, 0x4

    sget-object v4, LX/2k7;->c:LX/0U1;

    .line 455116
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 455117
    aput-object v4, v3, v0

    const/4 v0, 0x5

    sget-object v4, LX/2k7;->i:LX/0U1;

    .line 455118
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 455119
    aput-object v4, v3, v0

    const-string v4, "session_id = ?"

    new-array v5, v5, [Ljava/lang/String;

    aput-object p1, v5, v1

    const-string v8, "sort_key DESC"

    move-object v0, p0

    move-object v7, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    .prologue
    .line 455120
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "graph_cursor"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Landroid/database/CharArrayBuffer;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 455121
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v2, 0x0

    iget v3, p0, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method private static a(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 455122
    if-nez p1, :cond_0

    .line 455123
    :goto_0
    return-object p0

    .line 455124
    :cond_0
    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 455125
    :cond_1
    invoke-static {p1}, LX/2k0;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    goto :goto_0

    .line 455126
    :cond_2
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {v0, p0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-static {p1}, LX/2k0;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    goto :goto_0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/HashSet;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 455127
    iget-object v0, p0, LX/2k0;->f:LX/2kF;

    invoke-virtual {v0}, LX/2kF;->a()[Ljava/lang/String;

    move-result-object v0

    .line 455128
    const-string v1, "session_id"

    invoke-static {v1, v0}, LX/0uu;->a(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v3

    .line 455129
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 455130
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "SELECT DISTINCT file FROM models WHERE _id IN (SELECT DISTINCT confirmed_model FROM edges WHERE "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ")"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 455131
    :try_start_1
    invoke-static {v4, v1}, LX/2k0;->a(Ljava/util/HashSet;Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 455132
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 455133
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "SELECT DISTINCT file FROM models WHERE _id IN (SELECT DISTINCT optimistic_model FROM edges WHERE "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 455134
    invoke-static {v4, v2}, LX/2k0;->a(Ljava/util/HashSet;Landroid/database/Cursor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 455135
    invoke-static {v2}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 455136
    return-object v4

    .line 455137
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0

    .line 455138
    :catchall_1
    move-exception v0

    invoke-static {v2}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0

    .line 455139
    :catchall_2
    move-exception v0

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)V
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/cursor/database/GraphCursorDatabase$RecordSizePrefix;
        .end annotation
    .end param

    .prologue
    .line 455140
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455141
    iget-object v0, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v0}, LX/0Tr;->e()J

    move-result-wide v0

    .line 455142
    iget-object v2, p0, LX/2k0;->d:LX/2kD;

    invoke-virtual {v2}, LX/2kD;->c()J

    move-result-wide v2

    .line 455143
    iget-object v4, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_db_size"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, p1, v5, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 455144
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_file_size"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 455145
    :cond_0
    return-void
.end method

.method public static a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 454955
    if-eqz p0, :cond_0

    .line 454956
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 454957
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 455147
    const-string v2, "edges"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/2k5;->a:LX/0U1;

    .line 455148
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 455149
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {p0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 455150
    if-ne v2, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 455151
    return-void

    :cond_0
    move v0, v1

    .line 455152
    goto :goto_0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/File;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 455153
    const-string v0, "edges"

    invoke-virtual {p0, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 455154
    const-string v0, "chunks"

    invoke-virtual {p0, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 455155
    const-string v0, "models"

    invoke-virtual {p0, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 455156
    const-string v0, "tags"

    invoke-virtual {p0, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 455157
    invoke-static {p1}, LX/39R;->c(Ljava/io/File;)V

    .line 455158
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;J)V
    .locals 12

    .prologue
    .line 455159
    if-eqz p3, :cond_0

    .line 455160
    iget-object v0, p3, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    iget-object v1, p3, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 455161
    iget-object v2, p3, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a:Ljava/lang/String;

    iget-object v3, p3, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->c:Ljava/lang/String;

    iget-object v4, p3, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    iget-boolean v5, p3, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->e:Z

    iget-boolean v6, p3, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->f:Z

    iget v7, p3, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->g:I

    iget-object v0, p0, LX/2k0;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v8

    move-object v0, p1

    move-object v1, p2

    move-wide/from16 v10, p4

    invoke-static/range {v0 .. v11}, LX/2k0;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIJJ)J

    .line 455162
    :cond_0
    return-void
.end method

.method private declared-synchronized a(Ljava/util/Collection;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 455163
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, LX/2k0;->d:LX/2kD;

    .line 455164
    iget-object v2, v1, LX/2kD;->a:Ljava/io/File;

    move-object v1, v2

    .line 455165
    const-string v2, "models"

    sget-object v3, LX/2k9;->b:LX/0U1;

    invoke-static {v0, v1, p1, v2, v3}, LX/4VQ;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/File;Ljava/util/Collection;Ljava/lang/String;LX/0U1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455166
    monitor-exit p0

    return-void

    .line 455167
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(Ljava/util/HashSet;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 455168
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 455169
    const-string v0, "file"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 455170
    :cond_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 455171
    invoke-virtual {p0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 455172
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 455173
    :cond_1
    return-void
.end method

.method private a(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/9JB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 455174
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 455175
    :cond_0
    return-void

    .line 455176
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 455177
    iget-object v1, p0, LX/2k0;->j:LX/2kC;

    invoke-virtual {v1, v0}, LX/2kC;->a(Ljava/lang/String;)LX/0Rf;

    move-result-object v2

    .line 455178
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, LX/2k0;->f:LX/2kF;

    invoke-virtual {v1, v0}, LX/2kF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 455179
    new-instance v4, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v4, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 455180
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9JB;

    iget-object v1, v1, LX/9JB;->a:LX/1Yt;

    .line 455181
    if-eqz v1, :cond_3

    invoke-virtual {v1}, LX/1Yt;->a()Z

    move-result v5

    if-nez v5, :cond_3

    .line 455182
    const-string v5, "CHANGED_ROW_IDS"

    invoke-virtual {v1}, LX/1Yt;->b()[J

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 455183
    :cond_3
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9JB;

    iget-object v1, v1, LX/9JB;->b:LX/1Yt;

    .line 455184
    if-eqz v1, :cond_4

    invoke-virtual {v1}, LX/1Yt;->a()Z

    move-result v5

    if-nez v5, :cond_4

    .line 455185
    const-string v5, "DELETED_ROW_IDS"

    invoke-virtual {v1}, LX/1Yt;->b()[J

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 455186
    :cond_4
    const-string v1, "SESSION_VERSION"

    iget-object v5, p0, LX/2k0;->f:LX/2kF;

    invoke-virtual {v5, v0}, LX/2kF;->e(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 455187
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2kR;

    .line 455188
    iget-object v2, p0, LX/2k0;->f:LX/2kF;

    invoke-virtual {v2, v0}, LX/2kF;->a(Ljava/lang/String;)LX/2kb;

    move-result-object v6

    .line 455189
    invoke-virtual {p0, v6}, LX/2k0;->b(LX/2kb;)LX/2nd;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2nd;

    .line 455190
    invoke-virtual {v2, v6}, LX/2nd;->a(LX/2kb;)V

    .line 455191
    if-eqz v2, :cond_5

    .line 455192
    invoke-virtual {v2}, LX/2nd;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 455193
    :cond_5
    invoke-interface {v1, v2}, LX/2kR;->a(LX/2nf;)V

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 27
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/graphql/cursor/edgestore/PageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 455194
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 455195
    invoke-interface/range {p0 .. p0}, Landroid/database/Cursor;->getCount()I

    move-result v16

    .line 455196
    if-nez v16, :cond_0

    move-object v4, v14

    .line 455197
    :goto_0
    return-object v4

    .line 455198
    :cond_0
    sget-object v4, LX/2k7;->d:LX/0U1;

    invoke-virtual {v4}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 455199
    sget-object v4, LX/2k7;->e:LX/0U1;

    invoke-virtual {v4}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    .line 455200
    sget-object v4, LX/2k7;->f:LX/0U1;

    invoke-virtual {v4}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    .line 455201
    sget-object v4, LX/2k7;->g:LX/0U1;

    invoke-virtual {v4}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v20

    .line 455202
    sget-object v4, LX/2k7;->c:LX/0U1;

    invoke-virtual {v4}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    .line 455203
    sget-object v4, LX/2k7;->i:LX/0U1;

    invoke-virtual {v4}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    .line 455204
    new-instance v23, Landroid/database/CharArrayBuffer;

    const/16 v4, 0x40

    move-object/from16 v0, v23

    invoke-direct {v0, v4}, Landroid/database/CharArrayBuffer;-><init>(I)V

    .line 455205
    new-instance v24, Landroid/database/CharArrayBuffer;

    const/16 v4, 0x40

    move-object/from16 v0, v24

    invoke-direct {v0, v4}, Landroid/database/CharArrayBuffer;-><init>(I)V

    .line 455206
    const/4 v8, 0x0

    .line 455207
    const/4 v9, 0x0

    .line 455208
    const-wide/16 v10, 0x0

    .line 455209
    new-instance v25, Landroid/database/CharArrayBuffer;

    const/16 v4, 0x20

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Landroid/database/CharArrayBuffer;-><init>(I)V

    .line 455210
    new-instance v26, Landroid/database/CharArrayBuffer;

    const/16 v4, 0x20

    move-object/from16 v0, v26

    invoke-direct {v0, v4}, Landroid/database/CharArrayBuffer;-><init>(I)V

    .line 455211
    const/4 v4, 0x0

    move v15, v4

    :goto_1
    move/from16 v0, v16

    if-ge v15, v0, :cond_6

    .line 455212
    move-object/from16 v0, p0

    invoke-interface {v0, v15}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 455213
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-lez v4, :cond_1

    const/4 v4, 0x1

    move v12, v4

    .line 455214
    :goto_2
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-lez v4, :cond_2

    const/4 v13, 0x1

    .line 455215
    :goto_3
    if-nez v15, :cond_3

    .line 455216
    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 455217
    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 455218
    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v25

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 455219
    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v26

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 455220
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    move v4, v13

    .line 455221
    :goto_4
    add-int/lit8 v5, v15, 0x1

    move v15, v5

    move v9, v4

    move v8, v12

    goto :goto_1

    .line 455222
    :cond_1
    const/4 v4, 0x0

    move v12, v4

    goto :goto_2

    .line 455223
    :cond_2
    const/4 v13, 0x0

    goto :goto_3

    .line 455224
    :cond_3
    if-eqz v9, :cond_4

    if-nez v12, :cond_5

    .line 455225
    :cond_4
    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 455226
    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v26

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    move v4, v13

    move v12, v8

    .line 455227
    goto :goto_4

    .line 455228
    :cond_5
    invoke-static/range {v25 .. v25}, LX/2k0;->a(Landroid/database/CharArrayBuffer;)Ljava/lang/String;

    move-result-object v4

    .line 455229
    invoke-static {v4}, LX/2nU;->a(Ljava/lang/String;)V

    .line 455230
    invoke-static/range {v26 .. v26}, LX/2k0;->a(Landroid/database/CharArrayBuffer;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v23 .. v23}, LX/2k0;->a(Landroid/database/CharArrayBuffer;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v24 .. v24}, LX/2k0;->a(Landroid/database/CharArrayBuffer;)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v4 .. v11}, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZJ)Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v4

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 455231
    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 455232
    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 455233
    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v25

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 455234
    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v26

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 455235
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    move v4, v13

    goto :goto_4

    .line 455236
    :cond_6
    invoke-static/range {v25 .. v25}, LX/2k0;->a(Landroid/database/CharArrayBuffer;)Ljava/lang/String;

    move-result-object v4

    .line 455237
    invoke-static {v4}, LX/2nU;->a(Ljava/lang/String;)V

    .line 455238
    invoke-static/range {v26 .. v26}, LX/2k0;->a(Landroid/database/CharArrayBuffer;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v23 .. v23}, LX/2k0;->a(Landroid/database/CharArrayBuffer;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v24 .. v24}, LX/2k0;->a(Landroid/database/CharArrayBuffer;)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v4 .. v11}, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZJ)Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v4

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v4, v14

    .line 455239
    goto/16 :goto_0
.end method

.method public static declared-synchronized b(LX/2k0;Ljava/lang/String;LX/2kR;)V
    .locals 1

    .prologue
    .line 455240
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2k0;->j:LX/2kC;

    invoke-virtual {v0, p1, p2}, LX/2kC;->b(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455241
    monitor-exit p0

    return-void

    .line 455242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)LX/2nd;
    .locals 4
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 455243
    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p0 .. p7}, LX/2k0;->b(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 455244
    if-nez v1, :cond_0

    .line 455245
    const/4 v0, 0x0

    .line 455246
    :goto_0
    monitor-exit p0

    return-object v0

    .line 455247
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, LX/2k0;->b(LX/2kb;)LX/2nd;

    move-result-object v0

    .line 455248
    invoke-virtual {v0}, LX/2nd;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 455249
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)LX/9JB;
    .locals 7

    .prologue
    .line 455250
    new-instance v0, LX/9JB;

    invoke-direct {v0}, LX/9JB;-><init>()V

    .line 455251
    const/4 v1, 0x0

    .line 455252
    :try_start_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 455253
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v5, 0x18

    if-ne v2, v5, :cond_2

    move v2, v3

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 455254
    const-string v2, "24"

    .line 455255
    const-string v5, "SELECT _id FROM edges WHERE session_id = ? AND SUBSTR(sort_key, 0, ? + 1) < ?"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    aput-object p1, v6, v4

    aput-object v2, v6, v3

    const/4 v2, 0x2

    aput-object p2, v6, v2

    invoke-virtual {p0, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-object v1, v2

    .line 455256
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 455257
    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 455258
    :cond_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 455259
    invoke-static {p0, v4, v5}, LX/2k0;->a(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 455260
    iget-object v3, v0, LX/9JB;->b:LX/1Yt;

    invoke-virtual {v3, v4, v5}, LX/1Yt;->a(J)V

    .line 455261
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 455262
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 455263
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v5, 0x18

    if-ne v2, v5, :cond_3

    move v2, v3

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 455264
    const-string v2, "chunks"

    const-string v5, "session_id = ? AND sort_key < ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    aput-object p1, v6, v4

    aput-object p2, v6, v3

    invoke-virtual {p0, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455265
    :cond_1
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 455266
    return-object v0

    .line 455267
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0

    :cond_2
    :try_start_1
    move v2, v4

    .line 455268
    goto :goto_0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    move v2, v4

    .line 455269
    goto :goto_1
.end method

.method private c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 454958
    const v0, 0x36e00424

    invoke-static {p1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454959
    const/4 v1, 0x0

    .line 454960
    :try_start_0
    const/4 v0, 0x1

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 454961
    const-string v0, "SELECT DISTINCT session_id FROM edges WHERE session_id LIKE \'%#________\'"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v1, v0

    .line 454962
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 454963
    const-string v0, "session_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 454964
    :cond_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 454965
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x8

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 454966
    const-string v4, "abdd154a"

    invoke-static {v0, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 454967
    iget-object v0, p0, LX/2k0;->f:LX/2kF;

    invoke-virtual {v0, v3}, LX/2kF;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 454968
    invoke-virtual {p0, v3}, LX/2k0;->b(Ljava/lang/String;)V

    .line 454969
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 454970
    invoke-static {p1}, LX/2k0;->h(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 454971
    invoke-static {p1}, LX/2k0;->i(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 454972
    :cond_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454973
    const v0, 0x30d4304c

    invoke-static {p1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454974
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 454975
    return-void

    .line 454976
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 454977
    :catchall_0
    move-exception v0

    const v2, -0x34a437c2

    invoke-static {p1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454978
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 454979
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cc_dedupe_key:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e(Ljava/lang/String;)LX/1Yt;
    .locals 7

    .prologue
    .line 454592
    new-instance v0, LX/1Yt;

    invoke-direct {v0}, LX/1Yt;-><init>()V

    .line 454593
    iget-object v1, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 454594
    const/4 v1, 0x0

    .line 454595
    :try_start_0
    const-string v3, "SELECT _id FROM edges WHERE session_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    move-object v1, v3

    .line 454596
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 454597
    sget-object v2, LX/2k5;->a:LX/0U1;

    .line 454598
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 454599
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 454600
    :cond_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 454601
    invoke-virtual {v0, v4, v5}, LX/1Yt;->a(J)V

    .line 454602
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 454603
    :cond_1
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 454604
    invoke-virtual {p0, p1}, LX/2k0;->b(Ljava/lang/String;)V

    .line 454605
    return-object v0

    .line 454606
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method private e()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 454717
    iget-object v0, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method private e(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 454698
    iget-object v0, p0, LX/2k0;->f:LX/2kF;

    invoke-virtual {v0}, LX/2kF;->a()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf([Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    .line 454699
    const v1, 0x3bfd3104

    invoke-static {p1, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454700
    const/4 v1, 0x0

    .line 454701
    :try_start_0
    iget-object v5, p0, LX/2k0;->c:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    .line 454702
    const-string v7, "SELECT session_id FROM (SELECT session_id, MAX(expiration_time) AS expiration_time FROM chunks GROUP BY session_id) WHERE expiration_time < CAST(? as INTEGER)"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v9

    invoke-virtual {p1, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move-object v1, v5

    .line 454703
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 454704
    const-string v2, "session_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 454705
    :cond_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 454706
    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 454707
    invoke-virtual {p0, v3}, LX/2k0;->b(Ljava/lang/String;)V

    .line 454708
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 454709
    invoke-static {p1}, LX/2k0;->h(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 454710
    invoke-static {p1}, LX/2k0;->i(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 454711
    :cond_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454712
    const v0, -0x29cab348

    invoke-static {p1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454713
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 454714
    return-void

    .line 454715
    :catchall_0
    move-exception v0

    const v2, 0x42e0a754

    invoke-static {p1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454716
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static f(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 454697
    const-string v0, "SELECT DISTINCT a.session_id AS session_id, a.sort_key AS sort_key, SUM(b.row_count) AS row_count FROM chunks AS a, (SELECT DISTINCT session_id, sort_key, row_count FROM chunks) AS b WHERE a.session_id = b.session_id AND a.sort_key <= b.sort_key GROUP BY a.session_id, a.sort_key ORDER BY a.sort_key DESC"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 6

    .prologue
    .line 454682
    iget-object v0, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 454683
    const/4 v1, 0x0

    .line 454684
    :try_start_0
    const-string v2, "models"

    sget-object v3, LX/2k9;->b:LX/0U1;

    invoke-static {v0, v2, v3}, LX/4VQ;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0U1;)Landroid/database/Cursor;

    move-result-object v1

    .line 454685
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 454686
    sget-object v0, LX/2k9;->b:LX/0U1;

    .line 454687
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 454688
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 454689
    :cond_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 454690
    :try_start_1
    iget-object v3, p0, LX/2k0;->d:LX/2kD;

    invoke-virtual {v3, v0}, LX/2kD;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 454691
    :goto_0
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 454692
    :cond_1
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 454693
    return-void

    .line 454694
    :catch_0
    move-exception v0

    .line 454695
    :try_start_3
    sget-object v3, LX/2k0;->a:Ljava/lang/String;

    const-string v4, "Detected corrupted file"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v0, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 454696
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 454674
    :try_start_0
    iget-object v0, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 454675
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v1

    if-nez v1, :cond_0

    .line 454676
    const-string v1, "VACUUM"

    const v2, 0xd1349be

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x29f3c15

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    .line 454677
    :cond_0
    :goto_0
    return-void

    .line 454678
    :catch_0
    move-exception v0

    .line 454679
    sget-object v1, LX/2k0;->a:Ljava/lang/String;

    const-string v2, "SQLite disk too full to vacuum"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 454680
    :catch_1
    move-exception v0

    .line 454681
    sget-object v1, LX/2k0;->a:Ljava/lang/String;

    const-string v2, "Could not vacuum, likely in a transaction or something"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private g(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 454645
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 454646
    const v0, -0x6260a2cc

    invoke-static {p1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454647
    const/4 v1, 0x0

    .line 454648
    :try_start_0
    invoke-static {p1}, LX/2k0;->f(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v1

    .line 454649
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 454650
    const-string v0, "session_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 454651
    const-string v3, "sort_key"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 454652
    const-string v4, "row_count"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 454653
    :cond_0
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 454654
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 454655
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 454656
    const-string v7, "FriendsCenter"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 454657
    const/16 v7, 0x1388

    .line 454658
    :goto_0
    move v7, v7

    .line 454659
    if-lt v6, v7, :cond_1

    .line 454660
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 454661
    invoke-static {p1, v5, v6}, LX/2k0;->c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)LX/9JB;

    move-result-object v6

    .line 454662
    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454663
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 454664
    invoke-static {p1}, LX/2k0;->h(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 454665
    :cond_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454666
    const v0, -0x18e5f9d7

    invoke-static {p1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454667
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 454668
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 454669
    invoke-direct {p0, v2}, LX/2k0;->a(Ljava/util/Map;)V

    .line 454670
    return-void

    .line 454671
    :catchall_0
    move-exception v0

    const v2, -0x1e7ecdba

    invoke-static {p1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454672
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0

    .line 454673
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/16 v7, 0x64

    goto :goto_0
.end method

.method private static h(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 454643
    const-string v1, "models"

    sget-object v2, LX/2k9;->a:LX/0U1;

    const-string v3, "edges"

    sget-object v4, LX/2k5;->b:LX/0U1;

    sget-object v5, LX/2k5;->c:LX/0U1;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/4VQ;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/0U1;Ljava/lang/String;LX/0U1;LX/0U1;)V

    .line 454644
    return-void
.end method

.method private static i(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 454641
    const-string v0, "DELETE FROM chunks WHERE session_id NOT IN (SELECT DISTINCT session_id FROM edges)"

    const v1, -0x2be4dac9

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x143250

    invoke-static {v0}, LX/03h;->a(I)V

    .line 454642
    return-void
.end method


# virtual methods
.method public final declared-synchronized U_()V
    .locals 5
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 454610
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x850018

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 454611
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x850018

    const-string v2, "GraphCursorDatabase"

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 454612
    :try_start_1
    iget-object v0, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 454613
    const v1, 0x850018

    const-string v2, "initial"

    invoke-direct {p0, v1, v2}, LX/2k0;->a(ILjava/lang/String;)V

    .line 454614
    invoke-direct {p0, v0}, LX/2k0;->a(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/HashSet;

    move-result-object v1

    .line 454615
    invoke-direct {p0, v0}, LX/2k0;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 454616
    iget-object v2, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850015

    invoke-interface {v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 454617
    :try_start_2
    invoke-direct {p0, v0}, LX/2k0;->e(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 454618
    :try_start_3
    iget-object v2, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850015

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 454619
    iget-object v2, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850017

    invoke-interface {v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 454620
    :try_start_4
    invoke-direct {p0, v1}, LX/2k0;->a(Ljava/util/Collection;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 454621
    :try_start_5
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x850017

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 454622
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x850016

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 454623
    :try_start_6
    invoke-direct {p0, v0}, LX/2k0;->g(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 454624
    :try_start_7
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x850016

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 454625
    iget-object v0, p0, LX/2k0;->i:LX/0tC;

    invoke-virtual {v0}, LX/0tC;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454626
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85001e

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 454627
    :try_start_8
    invoke-direct {p0}, LX/2k0;->f()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    .line 454628
    :try_start_9
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85001e

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 454629
    :cond_0
    invoke-direct {p0}, LX/2k0;->g()V

    .line 454630
    const v0, 0x850018

    const-string v1, "final"

    invoke-direct {p0, v0, v1}, LX/2k0;->a(ILjava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 454631
    :try_start_a
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x850018

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 454632
    :goto_0
    monitor-exit p0

    return-void

    .line 454633
    :catchall_0
    move-exception v0

    :try_start_b
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x850015

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 454634
    :catch_0
    :try_start_c
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x850018

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 454635
    :try_start_d
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x850018

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto :goto_0

    .line 454636
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 454637
    :catchall_2
    move-exception v0

    :try_start_e
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x850017

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 454638
    :catchall_3
    move-exception v0

    :try_start_f
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x850018

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 454639
    :catchall_4
    move-exception v0

    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x850016

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v0

    .line 454640
    :catchall_5
    move-exception v0

    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x85001e

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;LX/2kR;)LX/2kT;
    .locals 1

    .prologue
    .line 454607
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2k0;->j:LX/2kC;

    invoke-virtual {v0, p1, p2}, LX/2kC;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 454608
    new-instance v0, LX/2kS;

    invoke-direct {v0, p0, p1, p2}, LX/2kS;-><init>(LX/2k0;Ljava/lang/String;LX/2kR;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 454609
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)LX/2kb;
    .locals 1

    .prologue
    .line 454589
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 454590
    iget-object v0, p0, LX/2k0;->f:LX/2kF;

    invoke-virtual {v0, p1}, LX/2kF;->a(Ljava/lang/String;)LX/2kb;

    move-result-object v0

    return-object v0

    .line 454591
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(LX/2kb;)LX/2nf;
    .locals 1
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 454718
    invoke-virtual {p0, p1}, LX/2k0;->b(LX/2kb;)LX/2nd;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2kb;Ljava/lang/String;)LX/2nf;
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 454719
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 454720
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 454721
    invoke-interface {p1}, LX/2kb;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 454722
    invoke-interface {p1}, LX/2kb;->a()Ljava/lang/String;

    move-result-object v2

    .line 454723
    if-eqz p2, :cond_2

    .line 454724
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 454725
    const-string v1, "tag"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v2, v4

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    .line 454726
    :goto_1
    const-string v0, "SELECT edges._id AS _id, edges.sort_key AS sort_key, edges.version AS version, edges.flags AS flags, models.file AS file, models.offset AS offset, models.mutation_data AS mutation_data, edges.class AS class, edges.model_type AS model_type, edges.optimistic_model - edges.confirmed_model AS is_optimistic FROM edges INNER JOIN models ON edges.optimistic_model = models._id WHERE edges.session_id=?"

    .line 454727
    if-eqz v2, :cond_0

    .line 454728
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " AND edges._id IN (SELECT node_id FROM tags WHERE "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 454729
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ORDER BY edges.sort_key DESC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 454730
    new-instance v2, LX/2nd;

    invoke-virtual {v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v1, p0, LX/2k0;->d:LX/2kD;

    .line 454731
    iget-object v3, v1, LX/2kD;->a:Ljava/io/File;

    move-object v1, v3

    .line 454732
    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, LX/2nd;-><init>(Landroid/database/Cursor;Ljava/io/File;Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v2

    :cond_1
    move v0, v2

    .line 454733
    goto :goto_0

    .line 454734
    :cond_2
    const/4 v0, 0x1

    :try_start_1
    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v2, v0, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v1

    move-object v1, v0

    goto :goto_1

    .line 454735
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final synthetic a(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)LX/2nf;
    .locals 2
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 454736
    invoke-direct/range {p0 .. p7}, LX/2k0;->c(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)LX/2nd;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(LX/2kb;LX/0Rl;Ljava/lang/String;)V
    .locals 9
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2kb;",
            "LX/0Rl",
            "<TTEdge;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 454737
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 454738
    invoke-interface {p1}, LX/2kb;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 454739
    invoke-interface {p1}, LX/2kb;->a()Ljava/lang/String;

    move-result-object v4

    .line 454740
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 454741
    new-instance v0, LX/9JB;

    invoke-direct {v0}, LX/9JB;-><init>()V

    invoke-interface {v5, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454742
    const v0, -0x738b5bc9

    invoke-static {v3, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454743
    :try_start_1
    new-instance v1, LX/2nd;

    const-string v6, "SELECT edges._id AS _id, edges.sort_key AS sort_key, edges.version AS version, edges.flags AS flags, models.file AS file, models.offset AS offset, models.mutation_data AS mutation_data, edges.class AS class, edges.model_type AS model_type, edges.optimistic_model - edges.confirmed_model AS is_optimistic FROM edges INNER JOIN models ON edges.optimistic_model = models._id WHERE edges.session_id = ? AND edges._id IN (SELECT node_id FROM tags WHERE tag = ?)"

    const/4 v0, 0x2

    new-array v7, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v4, v7, v0

    const/4 v8, 0x1

    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v7, v8

    invoke-virtual {v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v6, p0, LX/2k0;->d:LX/2kD;

    .line 454744
    iget-object v7, v6, LX/2kD;->a:Ljava/io/File;

    move-object v6, v7

    .line 454745
    const/4 v7, 0x0

    invoke-direct {v1, v0, v6, v7}, LX/2nd;-><init>(Landroid/database/Cursor;Ljava/io/File;Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 454746
    :try_start_2
    invoke-virtual {v1}, LX/2nd;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 454747
    :cond_0
    invoke-virtual {v1}, LX/2nd;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 454748
    invoke-interface {p2, v0}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 454749
    invoke-virtual {v1}, LX/2nd;->a()J

    move-result-wide v6

    .line 454750
    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9JB;

    iget-object v0, v0, LX/9JB;->b:LX/1Yt;

    invoke-virtual {v0, v6, v7}, LX/1Yt;->a(J)V

    .line 454751
    invoke-static {v3, v6, v7}, LX/2k0;->a(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 454752
    :cond_1
    invoke-virtual {v1}, LX/2nd;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 454753
    :cond_2
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 454754
    const v0, 0x64eb13bc

    :try_start_3
    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454755
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 454756
    invoke-direct {p0, v5}, LX/2k0;->a(Ljava/util/Map;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 454757
    :goto_1
    monitor-exit p0

    return-void

    :cond_3
    move v0, v1

    .line 454758
    goto :goto_0

    .line 454759
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 454760
    :goto_2
    :try_start_4
    sget-object v2, LX/2k0;->a:Ljava/lang/String;

    const-string v4, "Failed to delete edge with tag hint:%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p3, v5, v6

    invoke-static {v2, v0, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 454761
    const v0, -0x73e74bc6

    :try_start_5
    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454762
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 454763
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 454764
    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_3
    const v2, 0x640cafb7

    :try_start_6
    invoke-static {v3, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454765
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 454766
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 454767
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public final declared-synchronized a(Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;Ljava/lang/String;)V
    .locals 7
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Bq;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 454768
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/3DU;

    iget-object v1, p0, LX/2k0;->g:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-direct {p0}, LX/2k0;->e()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iget-object v3, p0, LX/2k0;->d:LX/2kD;

    iget-object v4, p0, LX/2k0;->c:LX/0SG;

    iget-object v5, p0, LX/2k0;->i:LX/0tC;

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/3DU;-><init>(Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/database/sqlite/SQLiteDatabase;LX/2kD;LX/0SG;LX/0tC;Ljava/lang/String;)V

    .line 454769
    iget-object v1, p0, LX/2k0;->h:LX/0t9;

    invoke-virtual {v1, v0, p1, p2, p3}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;)V

    .line 454770
    iget-object v1, v0, LX/3DU;->a:Ljava/util/HashMap;

    move-object v0, v1

    .line 454771
    invoke-direct {p0, v0}, LX/2k0;->a(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454772
    monitor-exit p0

    return-void

    .line 454773
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 7
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 454774
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/3DU;

    iget-object v1, p0, LX/2k0;->g:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-direct {p0}, LX/2k0;->e()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iget-object v3, p0, LX/2k0;->d:LX/2kD;

    iget-object v4, p0, LX/2k0;->c:LX/0SG;

    iget-object v5, p0, LX/2k0;->i:LX/0tC;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/3DU;-><init>(Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/database/sqlite/SQLiteDatabase;LX/2kD;LX/0SG;LX/0tC;Ljava/lang/String;)V

    .line 454775
    iget-object v1, p0, LX/2k0;->h:LX/0t9;

    invoke-virtual {v1, v0, p1, p2}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 454776
    iget-object v1, v0, LX/3DU;->a:Ljava/util/HashMap;

    move-object v0, v1

    .line 454777
    invoke-direct {p0, v0}, LX/2k0;->a(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454778
    monitor-exit p0

    return-void

    .line 454779
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/2kb;)LX/2nd;
    .locals 8
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 454780
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x85001f

    invoke-interface {v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 454781
    iget-object v2, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x85001f

    const-string v4, "GraphCursorDatabase"

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 454782
    iget-object v2, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x85001f

    invoke-interface {p1}, LX/2kb;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 454783
    invoke-interface {p1}, LX/2kb;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 454784
    invoke-interface {p1}, LX/2kb;->a()Ljava/lang/String;

    move-result-object v0

    .line 454785
    invoke-direct {p0}, LX/2k0;->e()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 454786
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x85001f

    const/16 v4, 0xc9

    invoke-interface {v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 454787
    const v1, 0x647128e0

    invoke-static {v2, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454788
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x85001f

    const/16 v4, 0xca

    invoke-interface {v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 454789
    const/4 v1, 0x0

    .line 454790
    :try_start_1
    invoke-static {v2, v0}, LX/2k0;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 454791
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    .line 454792
    iget-object v3, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x85001f

    const/16 v5, 0xd9

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 454793
    invoke-static {v1}, LX/2k0;->b(Landroid/database/Cursor;)Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 454794
    :try_start_2
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 454795
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x85001f

    const/16 v5, 0xdb

    invoke-interface {v1, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 454796
    :try_start_3
    const-string v1, "SELECT edges._id AS _id, edges.sort_key AS sort_key, edges.version AS version, edges.flags AS flags, models.file AS file, models.offset AS offset, models.mutation_data AS mutation_data, edges.class AS class, edges.model_type AS model_type, edges.optimistic_model - edges.confirmed_model AS is_optimistic FROM edges INNER JOIN models ON edges.optimistic_model = models._id WHERE edges.session_id=? ORDER BY edges.sort_key DESC"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 454797
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 454798
    :try_start_4
    iget-object v4, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x85001f

    const/16 v6, 0xda

    invoke-interface {v4, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 454799
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 454800
    const-string v5, "CHANGE_NUMBER"

    iget-object v6, p0, LX/2k0;->k:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 454801
    const-string v5, "CHUNKS"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 454802
    const-string v3, "SESSION_VERSION"

    iget-object v5, p0, LX/2k0;->f:LX/2kF;

    invoke-virtual {v5, v0}, LX/2kF;->d(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 454803
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 454804
    new-instance v0, LX/2nd;

    iget-object v3, p0, LX/2k0;->d:LX/2kD;

    .line 454805
    iget-object v5, v3, LX/2kD;->a:Ljava/io/File;

    move-object v3, v5

    .line 454806
    invoke-direct {v0, v1, v3, v4}, LX/2nd;-><init>(Landroid/database/Cursor;Ljava/io/File;Landroid/os/Bundle;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 454807
    const v1, 0x761da0a1

    :try_start_5
    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454808
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x85001f

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    monitor-exit p0

    return-object v0

    :cond_0
    move v0, v1

    .line 454809
    goto/16 :goto_0

    .line 454810
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 454811
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x85001f

    const/16 v4, 0xdb

    invoke-interface {v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 454812
    :catchall_1
    move-exception v0

    const v1, -0x3c571138

    :try_start_7
    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454813
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x85001f

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 454814
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 454815
    :catchall_3
    move-exception v0

    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x85001f

    const/16 v4, 0xda

    invoke-interface {v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    throw v0
.end method

.method public final declared-synchronized b(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)Landroid/os/Bundle;
    .locals 20
    .param p3    # LX/95b;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 454816
    monitor-enter p0

    :try_start_0
    invoke-static/range {p1 .. p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 454817
    invoke-interface/range {p1 .. p1}, LX/2kb;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 454818
    if-nez p2, :cond_1

    .line 454819
    if-eqz p3, :cond_0

    invoke-interface/range {p3 .. p3}, LX/95b;->a()I

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 454820
    :cond_1
    invoke-interface/range {p1 .. p1}, LX/2kb;->a()Ljava/lang/String;

    move-result-object v10

    .line 454821
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 454822
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->k:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v4

    .line 454823
    const-string v3, "CHANGE_NUMBER"

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 454824
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850009

    invoke-interface {v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 454825
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850009

    const-string v5, "GraphCursorDatabase"

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 454826
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850009

    invoke-interface/range {p1 .. p1}, LX/2kb;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 454827
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v19

    .line 454828
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850009

    const/16 v5, 0xc9

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 454829
    const v3, -0x1a570f6b

    move-object/from16 v0, v19

    invoke-static {v0, v3}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454830
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850009

    const/16 v5, 0xca

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 454831
    new-instance v4, LX/0cA;

    invoke-direct {v4}, LX/0cA;-><init>()V

    .line 454832
    if-eqz p3, :cond_5

    .line 454833
    const/4 v3, 0x0

    :goto_2
    invoke-interface/range {p3 .. p3}, LX/95b;->a()I

    move-result v5

    if-ge v3, v5, :cond_5

    .line 454834
    move-object/from16 v0, p3

    invoke-interface {v0, v3}, LX/95b;->e(I)Ljava/lang/String;

    move-result-object v5

    .line 454835
    if-eqz v5, :cond_2

    .line 454836
    invoke-static {v5}, LX/2k0;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 454837
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 454838
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 454839
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 454840
    :cond_5
    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    .line 454841
    const/4 v9, 0x0

    .line 454842
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    .line 454843
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->d:LX/2kD;

    if-eqz p3, :cond_8

    invoke-interface/range {p3 .. p3}, LX/95b;->a()I

    move-result v4

    if-lez v4, :cond_8

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, LX/95b;->b(I)LX/0w5;

    move-result-object v5

    :goto_3
    const/4 v8, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v3 .. v8}, LX/2kD;->a(Ljava/nio/ByteBuffer;LX/0w5;JZ)Ljava/lang/String;

    move-result-object v11

    .line 454844
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850009

    const/16 v5, 0xc8

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 454845
    if-eqz p7, :cond_9

    .line 454846
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, LX/2k0;->e(Ljava/lang/String;)LX/1Yt;

    move-result-object v3

    .line 454847
    if-eqz v3, :cond_6

    invoke-virtual {v3}, LX/1Yt;->a()Z

    move-result v4

    if-nez v4, :cond_6

    .line 454848
    const-string v4, "DELETED_ROW_IDS"

    invoke-virtual {v3}, LX/1Yt;->b()[J

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 454849
    :cond_6
    invoke-static/range {v19 .. v19}, LX/2k0;->i(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 454850
    invoke-static/range {v19 .. v19}, LX/2k0;->h(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 454851
    :cond_7
    :goto_4
    if-eqz p3, :cond_e

    .line 454852
    invoke-interface/range {p3 .. p3}, LX/95b;->a()I

    move-result v3

    new-array v3, v3, [J

    .line 454853
    const/4 v4, 0x0

    :goto_5
    invoke-interface/range {p3 .. p3}, LX/95b;->a()I

    move-result v5

    if-ge v4, v5, :cond_b

    .line 454854
    move-object/from16 v0, p0

    iget-object v5, v0, LX/2k0;->g:LX/0SI;

    invoke-interface {v5}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v9

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, LX/95b;->a(I)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, LX/95b;->b(I)LX/0w5;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, LX/95b;->f(I)I

    move-result v14

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, LX/95b;->c(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, LX/95b;->d(I)Ljava/util/Collection;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, LX/95b;->e(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, LX/2k0;->a(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v18

    move-object/from16 v8, v19

    move-wide/from16 v16, v6

    invoke-static/range {v8 .. v18}, LX/2k0;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Ljava/lang/String;ILX/0w5;ILjava/lang/String;JLjava/util/Collection;)J

    move-result-wide v8

    .line 454855
    aput-wide v8, v3, v4

    .line 454856
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 454857
    :cond_8
    const/4 v5, 0x0

    goto :goto_3

    .line 454858
    :cond_9
    invoke-virtual {v12}, LX/0Rf;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 454859
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v12}, LX/2k0;->a(Ljava/lang/String;Ljava/util/Collection;)LX/1Yt;

    move-result-object v3

    .line 454860
    if-eqz v3, :cond_7

    invoke-virtual {v3}, LX/1Yt;->a()Z

    move-result v4

    if-nez v4, :cond_7

    .line 454861
    const-string v4, "DELETED_ROW_IDS"

    invoke-virtual {v3}, LX/1Yt;->b()[J

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_4

    .line 454862
    :catch_0
    move-exception v2

    .line 454863
    :try_start_2
    sget-object v3, LX/2k0;->a:Ljava/lang/String;

    const-string v4, "Failure in CURSOR_DB_PUT_MULTI"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 454864
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850009

    const/4 v5, 0x3

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 454865
    instance-of v2, v2, Landroid/database/sqlite/SQLiteFullException;

    if-eqz v2, :cond_a

    .line 454866
    invoke-virtual/range {p0 .. p0}, LX/2k0;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 454867
    :cond_a
    const v2, 0x7cc340db

    :try_start_3
    move-object/from16 v0, v19

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454868
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850009

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v2, 0x0

    .line 454869
    :goto_6
    monitor-exit p0

    return-object v2

    .line 454870
    :cond_b
    :try_start_4
    array-length v4, v3

    if-lez v4, :cond_c

    .line 454871
    const-string v4, "INSERTED_ROW_IDS"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    :cond_c
    :goto_7
    move-object/from16 v8, p0

    move-object/from16 v9, v19

    move-object/from16 v11, p6

    move-wide/from16 v12, p4

    .line 454872
    invoke-direct/range {v8 .. v13}, LX/2k0;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/facebook/graphql/cursor/edgestore/PageInfo;J)V

    .line 454873
    move-object/from16 v0, p0

    iget-object v4, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x850009

    const/16 v6, 0x1e

    invoke-interface {v4, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 454874
    const-string v4, "SESSION_VERSION"

    move-object/from16 v0, p0

    iget-object v5, v0, LX/2k0;->f:LX/2kF;

    invoke-virtual {v5, v10}, LX/2kF;->e(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 454875
    if-eqz v3, :cond_d

    .line 454876
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v3}, LX/2k0;->a(Landroid/database/sqlite/SQLiteDatabase;[J)LX/9JB;

    .line 454877
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850009

    const/16 v5, 0xcc

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 454878
    invoke-virtual/range {v19 .. v19}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 454879
    const v3, 0x16c61450

    :try_start_5
    move-object/from16 v0, v19

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454880
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850009

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_6

    .line 454881
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 454882
    :catchall_1
    move-exception v2

    const v3, -0x13fa1296

    :try_start_6
    move-object/from16 v0, v19

    invoke-static {v0, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454883
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850009

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_e
    move-object v3, v9

    goto :goto_7
.end method

.method public final declared-synchronized b()V
    .locals 5
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 454884
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85001c

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 454885
    const v0, 0x85001c

    :try_start_1
    const-string v1, "initial"

    invoke-direct {p0, v0, v1}, LX/2k0;->a(ILjava/lang/String;)V

    .line 454886
    iget-object v0, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 454887
    const v0, -0x4b90df38

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 454888
    :try_start_2
    iget-object v0, p0, LX/2k0;->f:LX/2kF;

    invoke-virtual {v0}, LX/2kF;->a()[Ljava/lang/String;

    move-result-object v0

    .line 454889
    const-string v2, "session_id"

    invoke-static {v2, v0}, LX/0uu;->b(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 454890
    const-string v2, "edges"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 454891
    const-string v2, "chunks"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 454892
    invoke-static {v1}, LX/2k0;->h(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 454893
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 454894
    const v0, -0x49090e33

    :try_start_3
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454895
    invoke-direct {p0}, LX/2k0;->g()V

    .line 454896
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2k0;->a(Ljava/util/Collection;)V

    .line 454897
    const v0, 0x85001c

    const-string v1, "final"

    invoke-direct {p0, v0, v1}, LX/2k0;->a(ILjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 454898
    :try_start_4
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85001c

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 454899
    :goto_0
    monitor-exit p0

    return-void

    .line 454900
    :catchall_0
    move-exception v0

    const v2, -0x2b543142

    :try_start_5
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 454901
    :catch_0
    :try_start_6
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85001c

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 454902
    :try_start_7
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85001c

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_0

    .line 454903
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 454904
    :catchall_2
    move-exception v0

    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x85001c

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 7
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 454905
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 454906
    new-instance v2, LX/9JB;

    invoke-direct {v2}, LX/9JB;-><init>()V

    .line 454907
    iget-object v0, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 454908
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85000a

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 454909
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85000a

    const-string v4, "GraphCursorDatabase"

    invoke-interface {v0, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 454910
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85000a

    invoke-interface {v0, v1, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 454911
    const v0, 0x4dc84484    # 4.1999168E8f

    invoke-static {v3, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 454912
    const/4 v1, 0x0

    .line 454913
    :try_start_1
    const-string v0, "SELECT _id FROM edges WHERE session_id = ? "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v3, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v1, v0

    .line 454914
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 454915
    sget-object v0, LX/2k5;->a:LX/0U1;

    .line 454916
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 454917
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 454918
    :cond_0
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 454919
    iget-object v6, v2, LX/9JB;->b:LX/1Yt;

    invoke-virtual {v6, v4, v5}, LX/1Yt;->a(J)V

    .line 454920
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 454921
    :cond_1
    :try_start_2
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    .line 454922
    const-string v0, "edges"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/2k5;->f:LX/0U1;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " = ?"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v3, v0, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 454923
    invoke-static {v3}, LX/2k0;->i(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 454924
    invoke-static {v3}, LX/2k0;->h(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 454925
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 454926
    const v0, 0x4cb0ffd2    # 9.2798608E7f

    :try_start_3
    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454927
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85000a

    const/4 v3, 0x2

    invoke-interface {v0, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 454928
    :goto_1
    iget-object v0, v2, LX/9JB;->b:LX/1Yt;

    invoke-virtual {v0}, LX/1Yt;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 454929
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 454930
    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454931
    invoke-direct {p0, v0}, LX/2k0;->a(Ljava/util/Map;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 454932
    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    move v0, v1

    .line 454933
    goto/16 :goto_0

    .line 454934
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-static {v1}, LX/2k0;->a(Landroid/database/Cursor;)V

    throw v0
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 454935
    :catch_0
    move-exception v0

    .line 454936
    :try_start_5
    sget-object v1, LX/2k0;->a:Ljava/lang/String;

    const-string v4, "Unable to delete"

    invoke-static {v1, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 454937
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x85000a

    const/4 v5, 0x3

    invoke-interface {v1, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 454938
    instance-of v0, v0, Landroid/database/sqlite/SQLiteFullException;

    if-eqz v0, :cond_4

    .line 454939
    invoke-virtual {p0}, LX/2k0;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 454940
    :cond_4
    const v0, -0x39989d0b

    :try_start_6
    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454941
    iget-object v0, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85000a

    const/4 v3, 0x2

    invoke-interface {v0, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 454942
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 454943
    :catchall_2
    move-exception v0

    const v1, 0x861397a

    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 454944
    iget-object v1, p0, LX/2k0;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x85000a

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 3
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 454945
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 454946
    iget-object v1, p0, LX/2k0;->d:LX/2kD;

    .line 454947
    iget-object v2, v1, LX/2kD;->a:Ljava/io/File;

    move-object v1, v2

    .line 454948
    invoke-static {v0, v1}, LX/2k0;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454949
    monitor-exit p0

    return-void

    .line 454950
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 1

    .prologue
    .line 454951
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2k0;->b:LX/2k2;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 454952
    iget-object v0, p0, LX/2k0;->d:LX/2kD;

    invoke-virtual {v0}, LX/2kD;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454953
    monitor-exit p0

    return-void

    .line 454954
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
