.class public LX/23L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1IZ;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field public final c:LX/23M;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 364005
    const-class v0, LX/23L;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/23L;->a:Ljava/lang/String;

    .line 364006
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "cache_key"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "width"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "height"

    aput-object v2, v0, v1

    sput-object v0, LX/23L;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 364007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 364008
    new-instance v0, LX/23M;

    invoke-direct {v0, p1}, LX/23M;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/23L;->c:LX/23M;

    .line 364009
    iput-object p2, p0, LX/23L;->d:Ljava/util/concurrent/Executor;

    .line 364010
    iput-object p3, p0, LX/23L;->e:Ljava/util/concurrent/Executor;

    .line 364011
    return-void
.end method

.method public static declared-synchronized b(LX/23L;Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/3BC;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 363979
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/23L;->c:LX/23M;

    invoke-virtual {v0}, LX/23M;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 363980
    :try_start_1
    const-string v3, "media_id = ?"

    .line 363981
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v4, v1

    .line 363982
    const-string v1, "media_variations_index"

    sget-object v2, LX/23L;->b:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v2

    .line 363983
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result v1

    if-nez v1, :cond_1

    .line 363984
    if-eqz v2, :cond_0

    .line 363985
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 363986
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v0, v8

    :goto_0
    monitor-exit p0

    return-object v0

    .line 363987
    :cond_1
    :try_start_4
    const-string v1, "cache_key"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 363988
    const-string v1, "width"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 363989
    const-string v1, "height"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 363990
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 363991
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 363992
    new-instance v6, LX/3BC;

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-direct {v6, v7, v8, v9}, LX/3BC;-><init>(Landroid/net/Uri;II)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_1

    .line 363993
    :catch_0
    move-exception v1

    move-object v8, v2

    .line 363994
    :goto_2
    :try_start_5
    sget-object v2, LX/23L;->a:Ljava/lang/String;

    const-string v3, "Error reading for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v1, v3, v4}, LX/03J;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 363995
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 363996
    :catchall_0
    move-exception v1

    move-object v2, v8

    :goto_3
    if-eqz v2, :cond_2

    .line 363997
    :try_start_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 363998
    :cond_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 363999
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 364000
    :cond_3
    if-eqz v2, :cond_4

    .line 364001
    :try_start_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 364002
    :cond_4
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v0, v1

    goto :goto_0

    .line 364003
    :catchall_2
    move-exception v1

    move-object v2, v8

    goto :goto_3

    :catchall_3
    move-exception v1

    goto :goto_3

    .line 364004
    :catch_1
    move-exception v1

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/1eg;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/1eg",
            "<",
            "Ljava/util/List",
            "<",
            "LX/3BC;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 363970
    :try_start_0
    new-instance v0, LX/35w;

    invoke-direct {v0, p0, p1}, LX/35w;-><init>(LX/23L;Ljava/lang/String;)V

    iget-object v1, p0, LX/23L;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, LX/1eg;->a(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;)LX/1eg;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 363971
    :goto_0
    return-object v0

    .line 363972
    :catch_0
    move-exception v0

    .line 363973
    sget-object v1, LX/23L;->a:Ljava/lang/String;

    const-string v2, "Failed to schedule query task for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    .line 363974
    sget-object v4, LX/03J;->a:LX/03G;

    const/4 p0, 0x5

    invoke-interface {v4, p0}, LX/03G;->b(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 363975
    sget-object v4, LX/03J;->a:LX/03G;

    invoke-static {v2, v3}, LX/03J;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v4, v1, p0, v0}, LX/03G;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 363976
    :cond_0
    invoke-static {v0}, LX/1eg;->a(Ljava/lang/Exception;)LX/1eg;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/1bh;LX/1FL;)V
    .locals 3

    .prologue
    .line 363977
    iget-object v0, p0, LX/23L;->e:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/facebook/imagepipeline/producers/MediaVariationsIndexDatabase$2;-><init>(LX/23L;Ljava/lang/String;LX/1FL;LX/1bh;)V

    const v2, 0x55e084a2

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 363978
    return-void
.end method
