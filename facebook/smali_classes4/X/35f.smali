.class public LX/35f;
.super LX/3Pr;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/35f;


# instance fields
.field private final c:LX/01T;


# direct methods
.method public constructor <init>(LX/01T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497357
    invoke-direct {p0}, LX/3Pr;-><init>()V

    .line 497358
    iput-object p1, p0, LX/35f;->c:LX/01T;

    .line 497359
    return-void
.end method

.method public static a(LX/0QB;)LX/35f;
    .locals 4

    .prologue
    .line 497360
    sget-object v0, LX/35f;->d:LX/35f;

    if-nez v0, :cond_1

    .line 497361
    const-class v1, LX/35f;

    monitor-enter v1

    .line 497362
    :try_start_0
    sget-object v0, LX/35f;->d:LX/35f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 497363
    if-eqz v2, :cond_0

    .line 497364
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 497365
    new-instance p0, LX/35f;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-direct {p0, v3}, LX/35f;-><init>(LX/01T;)V

    .line 497366
    move-object v0, p0

    .line 497367
    sput-object v0, LX/35f;->d:LX/35f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497368
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 497369
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 497370
    :cond_1
    sget-object v0, LX/35f;->d:LX/35f;

    return-object v0

    .line 497371
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 497372
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 497373
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/3RH;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "accounts/triggersso"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 497374
    const-string v1, "extra_account_switch_redirect_source"

    iget-object v2, p0, LX/35f;->c:LX/01T;

    invoke-virtual {v2}, LX/01T;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 497375
    return-object v0
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 497376
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 497377
    invoke-virtual {p0, p1}, LX/3Pr;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 497378
    return-object v0
.end method
