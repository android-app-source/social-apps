.class public final enum LX/3DO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3DO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3DO;

.field public static final enum FULL:LX/3DO;

.field public static final enum NEW_NOTIFICATIONS:LX/3DO;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 534695
    new-instance v0, LX/3DO;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v2}, LX/3DO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3DO;->FULL:LX/3DO;

    .line 534696
    new-instance v0, LX/3DO;

    const-string v1, "NEW_NOTIFICATIONS"

    invoke-direct {v0, v1, v3}, LX/3DO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    .line 534697
    const/4 v0, 0x2

    new-array v0, v0, [LX/3DO;

    sget-object v1, LX/3DO;->FULL:LX/3DO;

    aput-object v1, v0, v2

    sget-object v1, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    aput-object v1, v0, v3

    sput-object v0, LX/3DO;->$VALUES:[LX/3DO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 534698
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3DO;
    .locals 1

    .prologue
    .line 534699
    const-class v0, LX/3DO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3DO;

    return-object v0
.end method

.method public static values()[LX/3DO;
    .locals 1

    .prologue
    .line 534700
    sget-object v0, LX/3DO;->$VALUES:[LX/3DO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3DO;

    return-object v0
.end method
