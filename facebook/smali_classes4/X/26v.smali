.class public LX/26v;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 372455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Iterable;Z)[B
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/5oe;",
            ">;Z)[B"
        }
    .end annotation

    .prologue
    .line 372456
    new-instance v5, LX/186;

    const/16 v1, 0x800

    invoke-direct {v5, v1}, LX/186;-><init>(I)V

    .line 372457
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 372458
    invoke-interface/range {p0 .. p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5oe;

    .line 372459
    iget-boolean v4, v1, LX/5oe;->c:Z

    move/from16 v0, p1

    if-ne v4, v0, :cond_0

    .line 372460
    iget-object v1, v1, LX/5oe;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 372461
    :cond_1
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 372462
    array-length v2, v1

    new-array v6, v2, [I

    .line 372463
    array-length v2, v1

    new-array v7, v2, [I

    .line 372464
    invoke-static/range {p0 .. p1}, LX/26v;->b(Ljava/lang/Iterable;Z)I

    move-result v2

    new-array v8, v2, [I

    .line 372465
    const/4 v2, 0x0

    .line 372466
    invoke-interface/range {p0 .. p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v3, v2

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5oe;

    .line 372467
    iget-boolean v4, v2, LX/5oe;->c:Z

    move/from16 v0, p1

    if-ne v4, v0, :cond_2

    .line 372468
    iget-object v2, v2, LX/5oe;->b:Ljava/util/SortedMap;

    invoke-interface {v2}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v4

    .line 372469
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v10, v2, [Ljava/lang/String;

    .line 372470
    array-length v2, v10

    new-array v11, v2, [I

    .line 372471
    const/4 v2, 0x0

    .line 372472
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v4, v2

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5of;

    .line 372473
    invoke-virtual {v2}, LX/5of;->a()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v10, v4

    .line 372474
    const/4 v13, 0x3

    invoke-virtual {v5, v13}, LX/186;->c(I)V

    .line 372475
    const/4 v13, 0x1

    invoke-virtual {v2}, LX/5of;->c()I

    move-result v14

    const/4 v15, -0x1

    invoke-virtual {v5, v13, v14, v15}, LX/186;->a(III)V

    .line 372476
    const/4 v13, 0x2

    invoke-virtual {v2}, LX/5of;->b()I

    move-result v14

    const/4 v15, -0x1

    invoke-virtual {v5, v13, v14, v15}, LX/186;->a(III)V

    .line 372477
    invoke-virtual {v2}, LX/5of;->c()I

    move-result v13

    aput v3, v8, v13

    .line 372478
    if-nez v4, :cond_3

    .line 372479
    invoke-virtual {v2}, LX/5of;->c()I

    move-result v2

    aput v2, v7, v3

    .line 372480
    :cond_3
    add-int/lit8 v2, v4, 0x1

    invoke-virtual {v5}, LX/186;->d()I

    move-result v13

    aput v13, v11, v4

    move v4, v2

    .line 372481
    goto :goto_2

    .line 372482
    :cond_4
    invoke-virtual {v5, v10}, LX/186;->a([Ljava/lang/String;)I

    move-result v2

    .line 372483
    const/4 v4, 0x0

    invoke-virtual {v5, v11, v4}, LX/186;->a([IZ)I

    move-result v4

    .line 372484
    const/4 v10, 0x3

    invoke-virtual {v5, v10}, LX/186;->c(I)V

    .line 372485
    const/4 v10, 0x1

    invoke-virtual {v5, v10, v2}, LX/186;->b(II)V

    .line 372486
    const/4 v2, 0x2

    invoke-virtual {v5, v2, v4}, LX/186;->b(II)V

    .line 372487
    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v5}, LX/186;->d()I

    move-result v4

    aput v4, v6, v3

    move v3, v2

    .line 372488
    goto/16 :goto_1

    .line 372489
    :cond_5
    invoke-virtual {v5, v1}, LX/186;->a([Ljava/lang/String;)I

    move-result v1

    .line 372490
    const/4 v2, 0x0

    invoke-virtual {v5, v6, v2}, LX/186;->a([IZ)I

    move-result v2

    .line 372491
    invoke-virtual {v5, v8}, LX/186;->a([I)I

    move-result v3

    .line 372492
    invoke-virtual {v5, v7}, LX/186;->a([I)I

    move-result v4

    .line 372493
    const/4 v6, 0x5

    invoke-virtual {v5, v6}, LX/186;->c(I)V

    .line 372494
    const/4 v6, 0x1

    invoke-virtual {v5, v6, v1}, LX/186;->b(II)V

    .line 372495
    const/4 v1, 0x2

    invoke-virtual {v5, v1, v2}, LX/186;->b(II)V

    .line 372496
    const/4 v1, 0x3

    invoke-virtual {v5, v1, v3}, LX/186;->b(II)V

    .line 372497
    const/4 v1, 0x4

    invoke-virtual {v5, v1, v4}, LX/186;->b(II)V

    .line 372498
    invoke-virtual {v5}, LX/186;->d()I

    move-result v1

    invoke-virtual {v5, v1}, LX/186;->d(I)V

    .line 372499
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v1

    .line 372500
    array-length v2, v1

    add-int/lit8 v2, v2, 0xc

    .line 372501
    array-length v3, v1

    add-int/lit8 v3, v3, 0xc

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 372502
    const/4 v4, 0x0

    const v5, -0x5314ff4

    invoke-virtual {v3, v4, v5}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 372503
    const/4 v4, 0x4

    const v5, 0x20151009

    invoke-virtual {v3, v4, v5}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 372504
    const/16 v4, 0x8

    invoke-virtual {v3, v4, v2}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 372505
    const/16 v2, 0xc

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 372506
    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 372507
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    return-object v1
.end method

.method private static b(Ljava/lang/Iterable;Z)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/5oe;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    .line 372508
    const/4 v0, 0x0

    .line 372509
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oe;

    .line 372510
    iget-boolean v3, v0, LX/5oe;->c:Z

    if-ne v3, p1, :cond_0

    .line 372511
    iget-object v0, v0, LX/5oe;->b:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 372512
    goto :goto_0

    .line 372513
    :cond_1
    return v1
.end method
