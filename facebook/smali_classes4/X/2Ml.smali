.class public LX/2Ml;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Ml;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/2ML;


# direct methods
.method public constructor <init>(LX/0Zb;LX/2ML;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397224
    iput-object p1, p0, LX/2Ml;->a:LX/0Zb;

    .line 397225
    iput-object p2, p0, LX/2Ml;->b:LX/2ML;

    .line 397226
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ml;
    .locals 5

    .prologue
    .line 397227
    sget-object v0, LX/2Ml;->c:LX/2Ml;

    if-nez v0, :cond_1

    .line 397228
    const-class v1, LX/2Ml;

    monitor-enter v1

    .line 397229
    :try_start_0
    sget-object v0, LX/2Ml;->c:LX/2Ml;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 397230
    if-eqz v2, :cond_0

    .line 397231
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 397232
    new-instance p0, LX/2Ml;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/2ML;->a(LX/0QB;)LX/2ML;

    move-result-object v4

    check-cast v4, LX/2ML;

    invoke-direct {p0, v3, v4}, LX/2Ml;-><init>(LX/0Zb;LX/2ML;)V

    .line 397233
    move-object v0, p0

    .line 397234
    sput-object v0, LX/2Ml;->c:LX/2Ml;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397235
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 397236
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397237
    :cond_1
    sget-object v0, LX/2Ml;->c:LX/2Ml;

    return-object v0

    .line 397238
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 397239
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Ml;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 397240
    iget-object v0, p0, LX/2Ml;->a:LX/0Zb;

    invoke-interface {v0, p1, p2}, LX/0Zb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 397241
    return-void
.end method

.method public static b(LX/2Ml;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 397242
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 397243
    const-string v1, "offline_threading_id"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397244
    const-string v1, "media_source"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v2}, LX/5zj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397245
    const-string v1, "media_type"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v2}, LX/2MK;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397246
    const-string v1, "original_sha256"

    iget-object v2, p0, LX/2Ml;->b:LX/2ML;

    invoke-virtual {v2, p1}, LX/2ML;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397247
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;JLjava/lang/Throwable;)V
    .locals 5
    .param p5    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 397248
    invoke-static {p0, p1}, LX/2Ml;->b(LX/2Ml;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v1

    .line 397249
    const-string v0, "get_fbid_status"

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397250
    const-string v0, "elapsed_time"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397251
    if-eqz p5, :cond_0

    .line 397252
    invoke-static {p5}, LX/1Bz;->getCausalChain(Ljava/lang/Throwable;)Ljava/util/List;

    move-result-object v0

    .line 397253
    invoke-static {v0}, LX/0Ph;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 397254
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 397255
    new-instance v3, Ljava/io/PrintWriter;

    invoke-direct {v3, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v0, v3}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 397256
    const-string v0, "error_code"

    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397257
    :cond_0
    const-string v0, "messenger_get_media_fbid_failed"

    invoke-static {p0, v0, v1}, LX/2Ml;->a(LX/2Ml;Ljava/lang/String;Ljava/util/Map;)V

    .line 397258
    return-void
.end method
