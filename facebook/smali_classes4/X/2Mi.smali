.class public LX/2Mi;
.super LX/2Mj;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Mi;


# direct methods
.method public constructor <init>(LX/2Mh;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/media/upload/config/IsVideoTranscodingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Mh;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397097
    invoke-direct {p0, p1, p2}, LX/2Mj;-><init>(LX/2Md;LX/0Or;)V

    .line 397098
    return-void
.end method

.method public static a(LX/0QB;)LX/2Mi;
    .locals 5

    .prologue
    .line 397099
    sget-object v0, LX/2Mi;->a:LX/2Mi;

    if-nez v0, :cond_1

    .line 397100
    const-class v1, LX/2Mi;

    monitor-enter v1

    .line 397101
    :try_start_0
    sget-object v0, LX/2Mi;->a:LX/2Mi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 397102
    if-eqz v2, :cond_0

    .line 397103
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 397104
    new-instance v4, LX/2Mi;

    invoke-static {v0}, LX/2Mh;->b(LX/0QB;)LX/2Mh;

    move-result-object v3

    check-cast v3, LX/2Mh;

    const/16 p0, 0x1526

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/2Mi;-><init>(LX/2Mh;LX/0Or;)V

    .line 397105
    move-object v0, v4

    .line 397106
    sput-object v0, LX/2Mi;->a:LX/2Mi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397107
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 397108
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397109
    :cond_1
    sget-object v0, LX/2Mi;->a:LX/2Mi;

    return-object v0

    .line 397110
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 397111
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
