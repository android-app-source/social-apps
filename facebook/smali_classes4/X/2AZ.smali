.class public LX/2AZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2AZ;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/3Ol;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 377401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377402
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2AZ;->a:Ljava/util/Set;

    .line 377403
    return-void
.end method

.method public static a(LX/0QB;)LX/2AZ;
    .locals 3

    .prologue
    .line 377404
    sget-object v0, LX/2AZ;->b:LX/2AZ;

    if-nez v0, :cond_1

    .line 377405
    const-class v1, LX/2AZ;

    monitor-enter v1

    .line 377406
    :try_start_0
    sget-object v0, LX/2AZ;->b:LX/2AZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 377407
    if-eqz v2, :cond_0

    .line 377408
    :try_start_1
    new-instance v0, LX/2AZ;

    invoke-direct {v0}, LX/2AZ;-><init>()V

    .line 377409
    move-object v0, v0

    .line 377410
    sput-object v0, LX/2AZ;->b:LX/2AZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377411
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 377412
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 377413
    :cond_1
    sget-object v0, LX/2AZ;->b:LX/2AZ;

    return-object v0

    .line 377414
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 377415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
