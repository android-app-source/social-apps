.class public LX/2N6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0SG;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2N6;


# instance fields
.field private a:J


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 398485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398486
    return-void
.end method

.method public static a(LX/0QB;)LX/2N6;
    .locals 3

    .prologue
    .line 398487
    sget-object v0, LX/2N6;->b:LX/2N6;

    if-nez v0, :cond_1

    .line 398488
    const-class v1, LX/2N6;

    monitor-enter v1

    .line 398489
    :try_start_0
    sget-object v0, LX/2N6;->b:LX/2N6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 398490
    if-eqz v2, :cond_0

    .line 398491
    :try_start_1
    new-instance v0, LX/2N6;

    invoke-direct {v0}, LX/2N6;-><init>()V

    .line 398492
    move-object v0, v0

    .line 398493
    sput-object v0, LX/2N6;->b:LX/2N6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398494
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 398495
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 398496
    :cond_1
    sget-object v0, LX/2N6;->b:LX/2N6;

    return-object v0

    .line 398497
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 398498
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()J
    .locals 4

    .prologue
    .line 398499
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 398500
    iget-wide v2, p0, LX/2N6;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 398501
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 398502
    :cond_0
    :try_start_1
    iget-wide v0, p0, LX/2N6;->a:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/2N6;->a:J

    .line 398503
    iget-wide v0, p0, LX/2N6;->a:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 398504
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 398505
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/2N6;->a:J

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/2N6;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398506
    monitor-exit p0

    return-void

    .line 398507
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
