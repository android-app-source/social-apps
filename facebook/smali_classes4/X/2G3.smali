.class public LX/2G3;
.super LX/0Vx;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2G3;


# direct methods
.method public constructor <init>(LX/2WA;)V
    .locals 0
    .param p1    # LX/2WA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387767
    invoke-direct {p0, p1}, LX/0Vx;-><init>(LX/2WA;)V

    .line 387768
    return-void
.end method

.method public static a(LX/0QB;)LX/2G3;
    .locals 4

    .prologue
    .line 387769
    sget-object v0, LX/2G3;->b:LX/2G3;

    if-nez v0, :cond_1

    .line 387770
    const-class v1, LX/2G3;

    monitor-enter v1

    .line 387771
    :try_start_0
    sget-object v0, LX/2G3;->b:LX/2G3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387772
    if-eqz v2, :cond_0

    .line 387773
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 387774
    new-instance p0, LX/2G3;

    invoke-static {v0}, LX/0Ws;->a(LX/0QB;)LX/2WA;

    move-result-object v3

    check-cast v3, LX/2WA;

    invoke-direct {p0, v3}, LX/2G3;-><init>(LX/2WA;)V

    .line 387775
    move-object v0, p0

    .line 387776
    sput-object v0, LX/2G3;->b:LX/2G3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387777
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387778
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387779
    :cond_1
    sget-object v0, LX/2G3;->b:LX/2G3;

    return-object v0

    .line 387780
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387781
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 387782
    const-string v0, "timeline_disk_cache_eviction_counters"

    return-object v0
.end method
