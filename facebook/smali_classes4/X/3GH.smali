.class public final LX/3GH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/3GP;


# direct methods
.method public constructor <init>(LX/3GP;)V
    .locals 0

    .prologue
    .line 540882
    iput-object p1, p0, LX/3GH;->a:LX/3GP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x710bdeba

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 540883
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 540884
    const-string v2, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 540885
    const-string v1, "android.bluetooth.profile.extra.STATE"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 540886
    iget-object v2, p0, LX/3GH;->a:LX/3GP;

    iget-object v2, v2, LX/3GP;->g:LX/EDp;

    if-eqz v2, :cond_1

    .line 540887
    iget-object v2, p0, LX/3GH;->a:LX/3GP;

    iget-object v2, v2, LX/3GP;->g:LX/EDp;

    .line 540888
    iget-object v4, v2, LX/EDp;->a:LX/EDx;

    iget-boolean v4, v4, LX/EDx;->bg:Z

    if-nez v4, :cond_5

    iget-object v4, v2, LX/EDp;->a:LX/EDx;

    iget-object v4, v4, LX/EDx;->m:LX/3GP;

    invoke-virtual {v4}, LX/3GP;->b()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 540889
    iget-object v4, v2, LX/EDp;->a:LX/EDx;

    iget-object v4, v4, LX/EDx;->v:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v5, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$9$1;

    invoke-direct {v5, v2}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$9$1;-><init>(LX/EDp;)V

    const-wide/16 v6, 0x2bc

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v6, v7, v8}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 540890
    :cond_0
    :goto_0
    iget-object v4, v2, LX/EDp;->a:LX/EDx;

    invoke-static {v4}, LX/EDx;->bS(LX/EDx;)V

    .line 540891
    iget-object v4, v2, LX/EDp;->a:LX/EDx;

    invoke-static {v4}, LX/EDx;->cr(LX/EDx;)V

    .line 540892
    :cond_1
    :goto_1
    const v1, 0x33cc11fb

    invoke-static {v1, v0}, LX/02F;->e(II)V

    return-void

    .line 540893
    :cond_2
    const-string v2, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 540894
    const-string v1, "android.bluetooth.profile.extra.STATE"

    const/16 v2, 0xa

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 540895
    const/16 v2, 0xc

    if-ne v1, v2, :cond_3

    .line 540896
    iget-object v2, p0, LX/3GH;->a:LX/3GP;

    iget-object v2, v2, LX/3GP;->c:Landroid/media/AudioManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    .line 540897
    :cond_3
    iget-object v2, p0, LX/3GH;->a:LX/3GP;

    iget-object v2, v2, LX/3GP;->g:LX/EDp;

    if-eqz v2, :cond_1

    .line 540898
    iget-object v2, p0, LX/3GH;->a:LX/3GP;

    iget-object v2, v2, LX/3GP;->g:LX/EDp;

    .line 540899
    iget-object v4, v2, LX/EDp;->a:LX/EDx;

    iget-boolean v4, v4, LX/EDx;->bg:Z

    if-eqz v4, :cond_4

    const/16 v4, 0xa

    if-ne v1, v4, :cond_4

    iget-object v4, v2, LX/EDp;->a:LX/EDx;

    invoke-virtual {v4}, LX/EDx;->aT()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 540900
    iget-object v4, v2, LX/EDp;->a:LX/EDx;

    iget-object v4, v4, LX/EDx;->v:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v5, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$9$2;

    invoke-direct {v5, v2}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$9$2;-><init>(LX/EDp;)V

    const-wide/16 v6, 0x3e8

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v6, v7, v8}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 540901
    :cond_4
    goto :goto_1

    .line 540902
    :cond_5
    iget-object v4, v2, LX/EDp;->a:LX/EDx;

    iget-boolean v4, v4, LX/EDx;->bg:Z

    if-eqz v4, :cond_0

    const/4 v4, 0x3

    if-eq v1, v4, :cond_6

    if-nez v1, :cond_0

    .line 540903
    :cond_6
    iget-object v4, v2, LX/EDp;->a:LX/EDx;

    invoke-static {v4}, LX/EDx;->bD(LX/EDx;)V

    goto :goto_0
.end method
