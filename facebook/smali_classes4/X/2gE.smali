.class public final enum LX/2gE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2gE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2gE;

.field public static final enum THREAD_PRESENCE_CAPABILITY_INSTANT:LX/2gE;

.field public static final enum THREAD_PRESENCE_CAPABILITY_INSTANT_MVP:LX/2gE;

.field public static final enum THREAD_PRESENCE_CAPABILITY_MASK:LX/2gE;

.field public static final enum THREAD_PRESENCE_CAPABILITY_NONE:LX/2gE;


# instance fields
.field private value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 448200
    new-instance v0, LX/2gE;

    const-string v1, "THREAD_PRESENCE_CAPABILITY_NONE"

    invoke-direct {v0, v1, v3, v3}, LX/2gE;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2gE;->THREAD_PRESENCE_CAPABILITY_NONE:LX/2gE;

    .line 448201
    new-instance v0, LX/2gE;

    const-string v1, "THREAD_PRESENCE_CAPABILITY_INSTANT_MVP"

    invoke-direct {v0, v1, v4, v4}, LX/2gE;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2gE;->THREAD_PRESENCE_CAPABILITY_INSTANT_MVP:LX/2gE;

    .line 448202
    new-instance v0, LX/2gE;

    const-string v1, "THREAD_PRESENCE_CAPABILITY_INSTANT"

    invoke-direct {v0, v1, v5, v5}, LX/2gE;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2gE;->THREAD_PRESENCE_CAPABILITY_INSTANT:LX/2gE;

    .line 448203
    new-instance v0, LX/2gE;

    const-string v1, "THREAD_PRESENCE_CAPABILITY_MASK"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v6, v2}, LX/2gE;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2gE;->THREAD_PRESENCE_CAPABILITY_MASK:LX/2gE;

    .line 448204
    const/4 v0, 0x4

    new-array v0, v0, [LX/2gE;

    sget-object v1, LX/2gE;->THREAD_PRESENCE_CAPABILITY_NONE:LX/2gE;

    aput-object v1, v0, v3

    sget-object v1, LX/2gE;->THREAD_PRESENCE_CAPABILITY_INSTANT_MVP:LX/2gE;

    aput-object v1, v0, v4

    sget-object v1, LX/2gE;->THREAD_PRESENCE_CAPABILITY_INSTANT:LX/2gE;

    aput-object v1, v0, v5

    sget-object v1, LX/2gE;->THREAD_PRESENCE_CAPABILITY_MASK:LX/2gE;

    aput-object v1, v0, v6

    sput-object v0, LX/2gE;->$VALUES:[LX/2gE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 448197
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 448198
    iput p3, p0, LX/2gE;->value:I

    .line 448199
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2gE;
    .locals 1

    .prologue
    .line 448205
    const-class v0, LX/2gE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2gE;

    return-object v0
.end method

.method public static values()[LX/2gE;
    .locals 1

    .prologue
    .line 448196
    sget-object v0, LX/2gE;->$VALUES:[LX/2gE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2gE;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 448195
    iget v0, p0, LX/2gE;->value:I

    return v0
.end method
