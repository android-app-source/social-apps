.class public final LX/2cM;
.super Ljava/util/AbstractMap;
.source ""

# interfaces
.implements LX/0Ri;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractMap",
        "<TV;TK;>;",
        "LX/0Ri",
        "<TV;TK;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LX/1Ei;


# direct methods
.method public constructor <init>(LX/1Ei;)V
    .locals 0

    .prologue
    .line 440675
    iput-object p1, p0, LX/2cM;->this$0:LX/1Ei;

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 440676
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TK;)TK;"
        }
    .end annotation

    .prologue
    .line 440674
    iget-object v0, p0, LX/2cM;->this$0:LX/1Ei;

    const/4 v1, 0x1

    invoke-static {v0, p1, p2, v1}, LX/1Ei;->b(LX/1Ei;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a_()LX/0Ri;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Ri",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 440672
    iget-object v0, p0, LX/2cM;->this$0:LX/1Ei;

    move-object v0, v0

    .line 440673
    return-object v0
.end method

.method public final b_()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 440670
    iget-object v0, p0, LX/2cM;->this$0:LX/1Ei;

    move-object v0, v0

    .line 440671
    invoke-interface {v0}, LX/0Ri;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 440667
    iget-object v0, p0, LX/2cM;->this$0:LX/1Ei;

    move-object v0, v0

    .line 440668
    invoke-interface {v0}, LX/0Ri;->clear()V

    .line 440669
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 440665
    iget-object v0, p0, LX/2cM;->this$0:LX/1Ei;

    move-object v0, v0

    .line 440666
    invoke-interface {v0, p1}, LX/0Ri;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TV;TK;>;>;"
        }
    .end annotation

    .prologue
    .line 440677
    new-instance v0, LX/4xu;

    invoke-direct {v0, p0}, LX/4xu;-><init>(LX/2cM;)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TK;"
        }
    .end annotation

    .prologue
    .line 440664
    iget-object v0, p0, LX/2cM;->this$0:LX/1Ei;

    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v0, p1, v1}, LX/1Ei;->b$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v0

    invoke-static {v0}, LX/0PM;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 440652
    new-instance v0, LX/4xw;

    invoke-direct {v0, p0}, LX/4xw;-><init>(LX/2cM;)V

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TK;)TK;"
        }
    .end annotation

    .prologue
    .line 440663
    iget-object v0, p0, LX/2cM;->this$0:LX/1Ei;

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, LX/1Ei;->b(LX/1Ei;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TK;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 440656
    iget-object v1, p0, LX/2cM;->this$0:LX/1Ei;

    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v1, p1, v2}, LX/1Ei;->b$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v1

    .line 440657
    if-nez v1, :cond_0

    .line 440658
    :goto_0
    return-object v0

    .line 440659
    :cond_0
    iget-object v2, p0, LX/2cM;->this$0:LX/1Ei;

    invoke-static {v2, v1}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;)V

    .line 440660
    iput-object v0, v1, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    .line 440661
    iput-object v0, v1, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    .line 440662
    iget-object v0, v1, LX/0P4;->key:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 440655
    iget-object v0, p0, LX/2cM;->this$0:LX/1Ei;

    iget v0, v0, LX/1Ei;->e:I

    return v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 440654
    invoke-virtual {p0}, LX/2cM;->b_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 440653
    new-instance v0, LX/4xx;

    iget-object v1, p0, LX/2cM;->this$0:LX/1Ei;

    invoke-direct {v0, v1}, LX/4xx;-><init>(LX/1Ei;)V

    return-object v0
.end method
