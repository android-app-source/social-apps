.class public LX/2tU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Qx;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:[I

.field private static b:[J

.field private static c:I

.field private static volatile f:LX/2tU;


# instance fields
.field private final d:[J

.field private e:LX/0So;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 475079
    new-array v0, v1, [I

    sput-object v0, LX/2tU;->a:[I

    .line 475080
    new-array v0, v1, [J

    sput-object v0, LX/2tU;->b:[J

    .line 475081
    const/4 v0, 0x0

    sput v0, LX/2tU;->c:I

    return-void
.end method

.method public constructor <init>(LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 475075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475076
    const/16 v0, 0x8

    new-array v0, v0, [J

    iput-object v0, p0, LX/2tU;->d:[J

    .line 475077
    iput-object p1, p0, LX/2tU;->e:LX/0So;

    .line 475078
    return-void
.end method

.method public static a(LX/0QB;)LX/2tU;
    .locals 4

    .prologue
    .line 475062
    sget-object v0, LX/2tU;->f:LX/2tU;

    if-nez v0, :cond_1

    .line 475063
    const-class v1, LX/2tU;

    monitor-enter v1

    .line 475064
    :try_start_0
    sget-object v0, LX/2tU;->f:LX/2tU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 475065
    if-eqz v2, :cond_0

    .line 475066
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 475067
    new-instance p0, LX/2tU;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-direct {p0, v3}, LX/2tU;-><init>(LX/0So;)V

    .line 475068
    move-object v0, p0

    .line 475069
    sput-object v0, LX/2tU;->f:LX/2tU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 475070
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 475071
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 475072
    :cond_1
    sget-object v0, LX/2tU;->f:LX/2tU;

    return-object v0

    .line 475073
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 475074
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static getMultiRowPerfLoggerStats()Lorg/json/JSONObject;
    .locals 8
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 475049
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 475050
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 475051
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    move v0, v1

    .line 475052
    :goto_0
    sget-object v5, LX/2tU;->a:[I

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 475053
    sget-object v5, LX/2tU;->a:[I

    aget v5, v5, v0

    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    .line 475054
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 475055
    :cond_0
    :goto_1
    sget-object v0, LX/2tU;->b:[J

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 475056
    sget-object v0, LX/2tU;->b:[J

    aget-wide v6, v0, v1

    invoke-virtual {v4, v6, v7}, Lorg/json/JSONArray;->put(J)Lorg/json/JSONArray;

    .line 475057
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 475058
    :cond_1
    const-string v0, "markerIdCount"

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 475059
    const-string v0, "asyncSinglePrepareCount"

    sget v1, LX/2tU;->c:I

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 475060
    const-string v0, "totalTimes"

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 475061
    return-object v2
.end method

.method public static resetMultiRowPerfLoggerStats()V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/16 v1, 0x8

    .line 475029
    new-array v0, v1, [I

    sput-object v0, LX/2tU;->a:[I

    .line 475030
    const/4 v0, 0x0

    sput v0, LX/2tU;->c:I

    .line 475031
    new-array v0, v1, [J

    sput-object v0, LX/2tU;->b:[J

    .line 475032
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 8

    .prologue
    .line 475044
    const/16 v0, 0x8

    if-ge p1, v0, :cond_0

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 475045
    sget-object v0, LX/2tU;->b:[J

    aget-wide v2, v0, p1

    iget-object v1, p0, LX/2tU;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    iget-object v1, p0, LX/2tU;->d:[J

    aget-wide v6, v1, p1

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    aput-wide v2, v0, p1

    .line 475046
    iget-object v0, p0, LX/2tU;->d:[J

    const-wide v2, 0x7fffffffffffffffL

    aput-wide v2, v0, p1

    .line 475047
    return-void

    .line 475048
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILX/1PW;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 475039
    if-ne p1, v0, :cond_1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 475040
    instance-of v0, p2, LX/1Pv;

    if-eqz v0, :cond_0

    check-cast p2, LX/1Pv;

    invoke-interface {p2}, LX/1Pv;->iO_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475041
    sget v0, LX/2tU;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, LX/2tU;->c:I

    .line 475042
    :cond_0
    return-void

    .line 475043
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjava/util/concurrent/Callable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 475038
    return-void
.end method

.method public final a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;I)V
    .locals 4

    .prologue
    .line 475033
    const/16 v0, 0x8

    if-ge p2, v0, :cond_0

    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 475034
    sget-object v0, LX/2tU;->a:[I

    aget v1, v0, p2

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, p2

    .line 475035
    iget-object v0, p0, LX/2tU;->d:[J

    iget-object v1, p0, LX/2tU;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    aput-wide v2, v0, p2

    .line 475036
    return-void

    .line 475037
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
