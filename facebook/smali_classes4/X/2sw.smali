.class public abstract LX/2sw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 474027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LX/99X;)F
    .locals 1

    .prologue
    .line 474028
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public abstract a()LX/2eR;
.end method

.method public a(Lcom/facebook/widget/CustomViewPager;Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 474029
    const v0, 0x7f0b00bd

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    const v1, 0x7f0b00bd

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    const v1, 0x7f0b0917

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 474030
    mul-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 474031
    return-void
.end method

.method public abstract a(Ljava/util/List;Lcom/facebook/widget/CustomViewPager;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;",
            "Lcom/facebook/widget/CustomViewPager;",
            ")V"
        }
    .end annotation
.end method

.method public b()I
    .locals 1

    .prologue
    .line 474032
    const/4 v0, 0x2

    return v0
.end method
