.class public LX/32o;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 491002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491003
    return-void
.end method

.method public static a(LX/0lS;)LX/320;
    .locals 2

    .prologue
    .line 491004
    invoke-virtual {p0}, LX/0lS;->b()Ljava/lang/Class;

    move-result-object v0

    const-class v1, LX/28G;

    if-ne v0, v1, :cond_0

    .line 491005
    sget-object v0, LX/4qR;->a:LX/4qR;

    .line 491006
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 491007
    const-class v0, LX/0nW;

    if-ne p0, v0, :cond_0

    .line 491008
    sget-object v0, Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$TokenBufferDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$TokenBufferDeserializer;

    .line 491009
    :goto_0
    return-object v0

    .line 491010
    :cond_0
    const-class v0, LX/0lJ;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 491011
    sget-object v0, Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$JavaTypeDeserializer;->a:Lcom/fasterxml/jackson/databind/deser/std/JacksonDeserializers$JavaTypeDeserializer;

    goto :goto_0

    .line 491012
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
