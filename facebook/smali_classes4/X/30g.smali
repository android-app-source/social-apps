.class public final LX/30g;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0sv;

.field private static final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 485097
    sget-object v0, LX/2J7;->a:LX/0U1;

    sget-object v1, LX/2J7;->b:LX/0U1;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/30g;->a:LX/0Px;

    .line 485098
    new-instance v0, LX/0su;

    sget-object v1, LX/2J7;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/30g;->b:LX/0sv;

    .line 485099
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE INDEX favorite_contacts_order_index on favorite_contacts ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/2J7;->b:LX/0U1;

    .line 485100
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 485101
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ASC)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/30g;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 485102
    const-string v0, "favorite_contacts"

    sget-object v1, LX/30g;->a:LX/0Px;

    sget-object v2, LX/30g;->b:LX/0sv;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 485103
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 485104
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 485105
    sget-object v0, LX/30g;->c:Ljava/lang/String;

    const v1, -0x2c255552

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x68d36a7f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 485106
    return-void
.end method
