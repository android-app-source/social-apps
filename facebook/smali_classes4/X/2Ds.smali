.class public LX/2Ds;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;",
        "Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;

.field private final b:LX/2Dt;


# direct methods
.method public constructor <init>(LX/0dC;LX/2Dt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384735
    iput-object p1, p0, LX/2Ds;->a:LX/0dC;

    .line 384736
    iput-object p2, p0, LX/2Ds;->b:LX/2Dt;

    .line 384737
    return-void
.end method

.method public static b(LX/0QB;)LX/2Ds;
    .locals 3

    .prologue
    .line 384738
    new-instance v2, LX/2Ds;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v0

    check-cast v0, LX/0dC;

    invoke-static {p0}, LX/2Dt;->b(LX/0QB;)LX/2Dt;

    move-result-object v1

    check-cast v1, LX/2Dt;

    invoke-direct {v2, v0, v1}, LX/2Ds;-><init>(LX/0dC;LX/2Dt;)V

    .line 384739
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 384740
    check-cast p1, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;

    .line 384741
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 384742
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "q"

    .line 384743
    iget-object v2, p1, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 384744
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384745
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "friend_name"

    .line 384746
    iget-object v2, p1, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 384747
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384748
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "qs"

    .line 384749
    iget-object v2, p1, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 384750
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384751
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "summary"

    const-string v2, "true"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384752
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "device_id"

    iget-object v2, p0, LX/2Ds;->a:LX/0dC;

    invoke-virtual {v2}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384753
    iget-object v0, p0, LX/2Ds;->b:LX/2Dt;

    invoke-virtual {v0}, LX/2Dt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384754
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "sfdid"

    .line 384755
    iget-object v2, p1, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 384756
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384757
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "fdid"

    .line 384758
    iget-object v2, p1, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->f:Ljava/lang/String;

    move-object v2, v2

    .line 384759
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384760
    :cond_0
    new-instance v0, LX/14N;

    const-string v1, "accountRecoverySearch"

    const-string v2, "GET"

    const-string v3, "recover_accounts"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSONPARSER:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 384761
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 384762
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;

    return-object v0
.end method
