.class public LX/2Gt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/push/fbpushtoken/UnregisterPushTokenParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389567
    return-void
.end method

.method public static a(LX/0QB;)LX/2Gt;
    .locals 1

    .prologue
    .line 389568
    new-instance v0, LX/2Gt;

    invoke-direct {v0}, LX/2Gt;-><init>()V

    .line 389569
    move-object v0, v0

    .line 389570
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 389571
    check-cast p1, Lcom/facebook/push/fbpushtoken/UnregisterPushTokenParams;

    .line 389572
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 389573
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389574
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "token"

    .line 389575
    iget-object v2, p1, Lcom/facebook/push/fbpushtoken/UnregisterPushTokenParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 389576
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389577
    new-instance v0, LX/14N;

    sget-object v1, LX/11I;->UNREGISTER_PUSH:LX/11I;

    iget-object v1, v1, LX/11I;->requestNameString:Ljava/lang/String;

    const-string v2, "POST"

    const-string v3, "method/user.unregisterPushCallback"

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 389578
    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v0

    .line 389579
    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
