.class public final enum LX/3JT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3JT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3JT;

.field public static final enum ANCHOR_POINT:LX/3JT;

.field public static final enum OPACITY:LX/3JT;

.field public static final enum POSITION:LX/3JT;

.field public static final enum ROTATION:LX/3JT;

.field public static final enum SCALE:LX/3JT;

.field public static final enum STROKE_WIDTH:LX/3JT;

.field public static final enum X_POSITION:LX/3JT;

.field public static final enum Y_POSITION:LX/3JT;


# instance fields
.field public final mIsMatrixBased:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 547717
    new-instance v0, LX/3JT;

    const-string v1, "SCALE"

    invoke-direct {v0, v1, v4, v3}, LX/3JT;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/3JT;->SCALE:LX/3JT;

    .line 547718
    new-instance v0, LX/3JT;

    const-string v1, "ROTATION"

    invoke-direct {v0, v1, v3, v3}, LX/3JT;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/3JT;->ROTATION:LX/3JT;

    .line 547719
    new-instance v0, LX/3JT;

    const-string v1, "POSITION"

    invoke-direct {v0, v1, v5, v3}, LX/3JT;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/3JT;->POSITION:LX/3JT;

    .line 547720
    new-instance v0, LX/3JT;

    const-string v1, "X_POSITION"

    invoke-direct {v0, v1, v6, v3}, LX/3JT;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/3JT;->X_POSITION:LX/3JT;

    .line 547721
    new-instance v0, LX/3JT;

    const-string v1, "Y_POSITION"

    invoke-direct {v0, v1, v7, v3}, LX/3JT;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/3JT;->Y_POSITION:LX/3JT;

    .line 547722
    new-instance v0, LX/3JT;

    const-string v1, "ANCHOR_POINT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, LX/3JT;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/3JT;->ANCHOR_POINT:LX/3JT;

    .line 547723
    new-instance v0, LX/3JT;

    const-string v1, "STROKE_WIDTH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, LX/3JT;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/3JT;->STROKE_WIDTH:LX/3JT;

    .line 547724
    new-instance v0, LX/3JT;

    const-string v1, "OPACITY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v4}, LX/3JT;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/3JT;->OPACITY:LX/3JT;

    .line 547725
    const/16 v0, 0x8

    new-array v0, v0, [LX/3JT;

    sget-object v1, LX/3JT;->SCALE:LX/3JT;

    aput-object v1, v0, v4

    sget-object v1, LX/3JT;->ROTATION:LX/3JT;

    aput-object v1, v0, v3

    sget-object v1, LX/3JT;->POSITION:LX/3JT;

    aput-object v1, v0, v5

    sget-object v1, LX/3JT;->X_POSITION:LX/3JT;

    aput-object v1, v0, v6

    sget-object v1, LX/3JT;->Y_POSITION:LX/3JT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3JT;->ANCHOR_POINT:LX/3JT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3JT;->STROKE_WIDTH:LX/3JT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3JT;->OPACITY:LX/3JT;

    aput-object v2, v0, v1

    sput-object v0, LX/3JT;->$VALUES:[LX/3JT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 547726
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 547727
    iput-boolean p3, p0, LX/3JT;->mIsMatrixBased:Z

    .line 547728
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3JT;
    .locals 1

    .prologue
    .line 547729
    const-class v0, LX/3JT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3JT;

    return-object v0
.end method

.method public static values()[LX/3JT;
    .locals 1

    .prologue
    .line 547730
    sget-object v0, LX/3JT;->$VALUES:[LX/3JT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3JT;

    return-object v0
.end method


# virtual methods
.method public final isMatrixBased()Z
    .locals 1

    .prologue
    .line 547731
    iget-boolean v0, p0, LX/3JT;->mIsMatrixBased:Z

    return v0
.end method
