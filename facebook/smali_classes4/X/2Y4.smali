.class public final LX/2Y4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;


# direct methods
.method public constructor <init>(Lcom/facebook/flexiblesampling/SamplingPolicyConfig;)V
    .locals 1

    .prologue
    .line 420630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420631
    invoke-static {p1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    iput-object v0, p0, LX/2Y4;->a:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    .line 420632
    return-void
.end method


# virtual methods
.method public final a(LX/0n9;)V
    .locals 2

    .prologue
    .line 420633
    const-string v0, "config_checksum"

    iget-object v1, p0, LX/2Y4;->a:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    invoke-interface {v1}, Lcom/facebook/flexiblesampling/SamplingPolicyConfig;->a()Ljava/lang/String;

    move-result-object v1

    .line 420634
    invoke-static {p1, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 420635
    const-string v0, "config_version"

    iget-object v1, p0, LX/2Y4;->a:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    invoke-interface {v1}, Lcom/facebook/flexiblesampling/SamplingPolicyConfig;->b()Ljava/lang/String;

    move-result-object v1

    .line 420636
    invoke-static {p1, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 420637
    return-void
.end method
