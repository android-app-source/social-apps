.class public final LX/38b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/38c;


# instance fields
.field public final synthetic a:LX/37g;


# direct methods
.method public constructor <init>(LX/37g;)V
    .locals 0

    .prologue
    .line 521121
    iput-object p1, p0, LX/38b;->a:LX/37g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 521122
    return-void
.end method

.method public final a(LX/384;)V
    .locals 3
    .param p1    # LX/384;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 521123
    iget-object v0, p0, LX/38b;->a:LX/37g;

    invoke-virtual {v0}, LX/37g;->d()LX/38p;

    move-result-object v0

    invoke-virtual {v0}, LX/38p;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 521124
    :cond_0
    :goto_0
    return-void

    .line 521125
    :cond_1
    if-nez p1, :cond_2

    .line 521126
    iget-object v0, p0, LX/38b;->a:LX/37g;

    iget-object v0, v0, LX/37g;->d:LX/37f;

    const/16 v1, 0xd

    const-string v2, "CastDeviceConnector.onCastDeviceSelected: Failed to select"

    invoke-virtual {v0, v1, v2}, LX/37f;->a(ILjava/lang/String;)V

    .line 521127
    iget-object v0, p0, LX/38b;->a:LX/37g;

    invoke-virtual {v0}, LX/37g;->b()V

    goto :goto_0

    .line 521128
    :cond_2
    iget-object v0, p0, LX/38b;->a:LX/37g;

    invoke-virtual {v0}, LX/37g;->d()LX/38p;

    move-result-object v0

    .line 521129
    iget-object v1, v0, LX/38p;->a:LX/38a;

    sget-object v2, LX/38a;->SELECTING:LX/38a;

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 521130
    if-eqz v0, :cond_0

    .line 521131
    iget-object v0, p0, LX/38b;->a:LX/37g;

    .line 521132
    iget-object v1, p1, LX/384;->p:Landroid/os/Bundle;

    move-object v1, v1

    .line 521133
    invoke-static {v1}, Lcom/google/android/gms/cast/CastDevice;->b(Landroid/os/Bundle;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v1

    .line 521134
    iput-object v1, v0, LX/37g;->j:Lcom/google/android/gms/cast/CastDevice;

    .line 521135
    iget-object v0, p0, LX/38b;->a:LX/37g;

    invoke-static {v0}, LX/37g;->i(LX/37g;)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
