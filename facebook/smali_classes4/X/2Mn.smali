.class public LX/2Mn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2Mn;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0oz;

.field private final c:LX/0kb;

.field public final d:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "LX/FHE;",
            "LX/FHF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0oz;LX/0kb;)V
    .locals 4
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 397473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397474
    iput-object p1, p0, LX/2Mn;->a:LX/0Zb;

    .line 397475
    iput-object p2, p0, LX/2Mn;->b:LX/0oz;

    .line 397476
    iput-object p3, p0, LX/2Mn;->c:LX/0kb;

    .line 397477
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/32 v2, 0x7b98a000

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/2Mn;->d:LX/0QI;

    .line 397478
    return-void
.end method

.method public static a(LX/0QB;)LX/2Mn;
    .locals 6

    .prologue
    .line 397460
    sget-object v0, LX/2Mn;->e:LX/2Mn;

    if-nez v0, :cond_1

    .line 397461
    const-class v1, LX/2Mn;

    monitor-enter v1

    .line 397462
    :try_start_0
    sget-object v0, LX/2Mn;->e:LX/2Mn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 397463
    if-eqz v2, :cond_0

    .line 397464
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 397465
    new-instance p0, LX/2Mn;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v4

    check-cast v4, LX/0oz;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-direct {p0, v3, v4, v5}, LX/2Mn;-><init>(LX/0Zb;LX/0oz;LX/0kb;)V

    .line 397466
    move-object v0, p0

    .line 397467
    sput-object v0, LX/2Mn;->e:LX/2Mn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397468
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 397469
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397470
    :cond_1
    sget-object v0, LX/2Mn;->e:LX/2Mn;

    return-object v0

    .line 397471
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 397472
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Mn;LX/FHE;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 397457
    iget-object v0, p0, LX/2Mn;->a:LX/0Zb;

    invoke-interface {v0, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 397458
    iget-object v0, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 397459
    return-void
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V
    .locals 2

    .prologue
    .line 397455
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2, v0}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397456
    return-void
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/Throwable;)V
    .locals 1
    .param p1    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 397452
    if-eqz p1, :cond_0

    .line 397453
    const-string v0, "exception_info"

    invoke-virtual {p0, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397454
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 6

    .prologue
    .line 397433
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v1

    .line 397434
    invoke-static {}, LX/0SW;->createStarted()LX/0SW;

    move-result-object v2

    .line 397435
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "media_attachment_preparation_summary"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 397436
    const-string v0, "preparation_start"

    invoke-static {v3, v0, v2}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397437
    const-string v0, "attachment_preparation"

    .line 397438
    iput-object v0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 397439
    const-string v0, "message_offline_id"

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397440
    const-string v0, "offline_id"

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397441
    const-string v0, "media_type"

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v4}, LX/2MK;->name()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397442
    const-string v0, "mime_type"

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397443
    const-string v0, "media_send_source"

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v4}, LX/5zj;->name()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397444
    const-string v0, "duration"

    iget-wide v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-virtual {v3, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397445
    const-string v0, "original_size"

    iget-wide v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    invoke-virtual {v3, v0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397446
    const-string v0, "original_width"

    iget v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    invoke-virtual {v3, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397447
    const-string v0, "original_height"

    iget v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    invoke-virtual {v3, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397448
    const-string v4, "has_overlay"

    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397449
    iget-object v0, p0, LX/2Mn;->d:LX/0QI;

    new-instance v4, LX/FHF;

    invoke-direct {v4, v3, v2}, LX/FHF;-><init>(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/0SW;)V

    invoke-interface {v0, v1, v4}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 397450
    return-void

    .line 397451
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;IILjava/io/File;IZ)V
    .locals 4

    .prologue
    .line 397417
    const/4 v0, 0x2

    if-ne p5, v0, :cond_1

    .line 397418
    :cond_0
    :goto_0
    return-void

    .line 397419
    :cond_1
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397420
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397421
    if-eqz v0, :cond_0

    .line 397422
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397423
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397424
    const-string v2, "compression_finish"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397425
    const-string v0, "resize_skipped_from_cache"

    invoke-virtual {v1, v0, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397426
    const-string v0, "max_dimension"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397427
    const-string v0, "compression_quality"

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397428
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 397429
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 397430
    invoke-virtual {p4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, LX/436;->a(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 397431
    const-string v2, "downsized_width"

    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397432
    const-string v2, "downsized_height"

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 397407
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397408
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397409
    if-nez v0, :cond_0

    .line 397410
    :goto_0
    return-void

    .line 397411
    :cond_0
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397412
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397413
    const-string v2, "compression_finish"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397414
    const-string v0, "transcode_attempts"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397415
    const-string v0, "resize_status"

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397416
    const-string v0, "resize_strategy"

    invoke-virtual {v1, v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 397396
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397397
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397398
    if-nez v0, :cond_0

    .line 397399
    :goto_0
    return-void

    .line 397400
    :cond_0
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397401
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397402
    const-string v2, "compression_finish"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397403
    invoke-static {v1, p4}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/Throwable;)V

    .line 397404
    const-string v0, "transcode_attempts"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397405
    const-string v0, "resize_strategy"

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397406
    const-string v0, "failed_stage"

    sget-object v2, LX/FHG;->compression:LX/FHG;

    invoke-virtual {v2}, LX/FHG;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/lang/Throwable;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "I",
            "Ljava/lang/Throwable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 397385
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397386
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397387
    if-nez v0, :cond_0

    .line 397388
    :goto_0
    return-void

    .line 397389
    :cond_0
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397390
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397391
    const-string v2, "upload_finish"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397392
    const-string v0, "upload_retry_count"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397393
    invoke-static {v1, p3}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/Throwable;)V

    .line 397394
    invoke-virtual {v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397395
    const-string v0, "failed_stage"

    sget-object v2, LX/FHG;->upload:LX/FHG;

    invoke-virtual {v2}, LX/FHG;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 397290
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397291
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397292
    if-nez v0, :cond_0

    .line 397293
    :goto_0
    return-void

    .line 397294
    :cond_0
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397295
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397296
    const-string v2, "upload_finish"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397297
    const-string v0, "upload_retry_count"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397298
    invoke-virtual {v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397299
    const-string v0, "canceled_stage"

    sget-object v2, LX/FHG;->upload:LX/FHG;

    invoke-virtual {v2}, LX/FHG;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;IZ)V
    .locals 3

    .prologue
    .line 397375
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397376
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397377
    if-nez v0, :cond_0

    .line 397378
    :goto_0
    return-void

    .line 397379
    :cond_0
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397380
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397381
    const-string v2, "compression_start"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397382
    const-string v0, "estimated_new_size"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397383
    const-string v0, "should_transcode"

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397384
    const-string v0, "trimmed_or_cropped"

    invoke-static {p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;IZLjava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 397361
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 397362
    :cond_0
    :goto_0
    return-void

    .line 397363
    :cond_1
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397364
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397365
    if-eqz v0, :cond_0

    .line 397366
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397367
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397368
    const-string v2, "compression_finish"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397369
    if-eqz p3, :cond_3

    .line 397370
    if-eqz p4, :cond_2

    .line 397371
    const-string v0, "skipped_exception_info"

    invoke-virtual {v1, v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397372
    :cond_2
    const-string v0, "skipped_stage"

    sget-object v2, LX/FHG;->compression:LX/FHG;

    invoke-virtual {v2}, LX/FHG;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 397373
    :cond_3
    invoke-static {v1, p4}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/Throwable;)V

    .line 397374
    const-string v0, "failed_stage"

    sget-object v2, LX/FHG;->compression:LX/FHG;

    invoke-virtual {v2}, LX/FHG;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;JLjava/lang/String;)V
    .locals 4

    .prologue
    .line 397351
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397352
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397353
    if-nez v0, :cond_1

    .line 397354
    :cond_0
    :goto_0
    return-void

    .line 397355
    :cond_1
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397356
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397357
    const-string v2, "dedup_query_finish"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397358
    const-string v0, "dedup_status"

    invoke-virtual {v1, v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397359
    const-wide/16 v2, 0x0

    cmp-long v0, p2, v2

    if-lez v0, :cond_0

    .line 397360
    const-string v0, "dedup_fbid"

    invoke-virtual {v1, v0, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 397338
    if-eqz p2, :cond_1

    .line 397339
    :cond_0
    :goto_0
    return-void

    .line 397340
    :cond_1
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397341
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397342
    if-eqz v0, :cond_0

    .line 397343
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397344
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397345
    const-string v2, "upload_start"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397346
    const-string v0, "data_size"

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397347
    const-string v0, "connection_type"

    iget-object v2, p0, LX/2Mn;->c:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397348
    const-string v0, "current_rtt"

    iget-object v2, p0, LX/2Mn;->b:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->k()D

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397349
    const-string v0, "upload_bandwidth"

    iget-object v2, p0, LX/2Mn;->b:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->f()D

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397350
    const-string v0, "upload_connection_quality"

    iget-object v2, p0, LX/2Mn;->b:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->b()LX/0p3;

    move-result-object v2

    invoke-virtual {v2}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;ILjava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 397328
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397329
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397330
    if-nez v0, :cond_0

    .line 397331
    :goto_0
    return-void

    .line 397332
    :cond_0
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397333
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397334
    const-string v2, "upload_finish"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397335
    const-string v0, "unpublished_media_fbid"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397336
    const-string v0, "upload_retry_count"

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397337
    invoke-virtual {v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p3    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 397318
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397319
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397320
    if-nez v0, :cond_0

    .line 397321
    :goto_0
    return-void

    .line 397322
    :cond_0
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397323
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397324
    const-string v2, "dedup_query_finish"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397325
    invoke-static {v1, p3}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/Throwable;)V

    .line 397326
    const-string v0, "dedup_status"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397327
    const-string v0, "failed_stage"

    sget-object v2, LX/FHG;->dedup:LX/FHG;

    invoke-virtual {v2}, LX/FHG;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 4

    .prologue
    .line 397309
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v1

    .line 397310
    iget-object v0, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v0, v1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397311
    if-nez v0, :cond_0

    .line 397312
    :goto_0
    return-void

    .line 397313
    :cond_0
    iget-object v2, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397314
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397315
    const-string v3, "preparation_finish"

    invoke-static {v2, v3, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397316
    const-string v0, "completion_status"

    sget-object v3, LX/FHH;->success:LX/FHH;

    invoke-virtual {v3}, LX/FHH;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397317
    invoke-static {p0, v1, v2}, LX/2Mn;->a(LX/2Mn;LX/FHE;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;I)V
    .locals 3

    .prologue
    .line 397300
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v0

    .line 397301
    iget-object v1, p0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHF;

    .line 397302
    if-nez v0, :cond_0

    .line 397303
    :goto_0
    return-void

    .line 397304
    :cond_0
    iget-object v1, v0, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397305
    iget-object v0, v0, LX/FHF;->b:LX/0SW;

    .line 397306
    const-string v2, "compression_finish"

    invoke-static {v1, v2, v0}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 397307
    const-string v0, "transcode_attempts"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 397308
    const-string v0, "canceled_stage"

    sget-object v2, LX/FHG;->compression:LX/FHG;

    invoke-virtual {v2}, LX/FHG;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method
