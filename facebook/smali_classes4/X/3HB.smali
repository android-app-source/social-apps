.class public final LX/3HB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0sZ;

.field private final b:LX/0sa;

.field private final c:LX/0se;

.field private final d:LX/0tE;

.field private final e:LX/0tG;

.field private f:LX/0sU;


# direct methods
.method public constructor <init>(LX/0sZ;LX/0sa;LX/0se;LX/0tE;LX/0tG;LX/0sU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543040
    iput-object p1, p0, LX/3HB;->a:LX/0sZ;

    .line 543041
    iput-object p2, p0, LX/3HB;->b:LX/0sa;

    .line 543042
    iput-object p3, p0, LX/3HB;->c:LX/0se;

    .line 543043
    iput-object p4, p0, LX/3HB;->d:LX/0tE;

    .line 543044
    iput-object p5, p0, LX/3HB;->e:LX/0tG;

    .line 543045
    iput-object p6, p0, LX/3HB;->f:LX/0sU;

    .line 543046
    return-void
.end method

.method public static a(LX/0QB;)LX/3HB;
    .locals 8

    .prologue
    .line 543047
    new-instance v1, LX/3HB;

    invoke-static {p0}, LX/0sZ;->b(LX/0QB;)LX/0sZ;

    move-result-object v2

    check-cast v2, LX/0sZ;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v3

    check-cast v3, LX/0sa;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v4

    check-cast v4, LX/0se;

    invoke-static {p0}, LX/0tE;->b(LX/0QB;)LX/0tE;

    move-result-object v5

    check-cast v5, LX/0tE;

    invoke-static {p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v6

    check-cast v6, LX/0tG;

    invoke-static {p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v7

    check-cast v7, LX/0sU;

    invoke-direct/range {v1 .. v7}, LX/3HB;-><init>(LX/0sZ;LX/0sa;LX/0se;LX/0tE;LX/0tG;LX/0sU;)V

    .line 543048
    move-object v0, v1

    .line 543049
    return-object v0
.end method

.method public static a(LX/3HB;Lcom/facebook/api/ufiservices/common/FetchCommentsParams;LX/0gW;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/FetchCommentsParams;",
            "LX/0gW",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 543050
    const-string v0, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "likers_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543051
    if-eqz p1, :cond_2

    .line 543052
    const-string v0, "feedback_id"

    .line 543053
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 543054
    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "max_comments"

    .line 543055
    iget v2, p1, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->b:I

    move v2, v2

    .line 543056
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543057
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 543058
    if-eqz v0, :cond_0

    .line 543059
    const-string v0, "before_comments"

    .line 543060
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 543061
    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543062
    :cond_0
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 543063
    if-eqz v0, :cond_1

    .line 543064
    const-string v0, "after_comments"

    .line 543065
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 543066
    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543067
    :cond_1
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;->a:LX/21y;

    move-object v0, v0

    .line 543068
    if-eqz v0, :cond_2

    .line 543069
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;->a:LX/21y;

    move-object v0, v0

    .line 543070
    sget-object v1, LX/21y;->DEFAULT_ORDER:LX/21y;

    invoke-virtual {v0, v1}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 543071
    const-string v0, "comment_order"

    .line 543072
    iget-object v1, p1, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;->a:LX/21y;

    move-object v1, v1

    .line 543073
    iget-object v1, v1, LX/21y;->toString:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 543074
    :cond_2
    const-string v0, "angora_attachment_cover_image_size"

    iget-object v1, p0, LX/3HB;->b:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543075
    const-string v0, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543076
    const-string v0, "reading_attachment_profile_image_width"

    iget-object v1, p0, LX/3HB;->b:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543077
    const-string v0, "reading_attachment_profile_image_height"

    iget-object v1, p0, LX/3HB;->b:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 543078
    iget-object v0, p0, LX/3HB;->d:LX/0tE;

    .line 543079
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, LX/0tE;->a(LX/0gW;Z)V

    .line 543080
    iget-object v0, p0, LX/3HB;->e:LX/0tG;

    invoke-virtual {v0, p2}, LX/0tG;->a(LX/0gW;)V

    .line 543081
    iget-object v0, p0, LX/3HB;->f:LX/0sU;

    invoke-virtual {v0, p2}, LX/0sU;->a(LX/0gW;)V

    .line 543082
    iget-object v0, p0, LX/3HB;->c:LX/0se;

    invoke-virtual {v0, p2}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 543083
    return-void
.end method
