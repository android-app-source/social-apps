.class public LX/2U7;
.super LX/1Eg;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0aG;

.field public final d:LX/0SG;

.field private final e:LX/0Uh;

.field public f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 415206
    const-class v0, LX/2U7;

    sput-object v0, LX/2U7;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/0SG;LX/0Uh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415199
    const-string v0, "CONFIGURATION"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 415200
    iput-object p1, p0, LX/2U7;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 415201
    iput-object p2, p0, LX/2U7;->c:LX/0aG;

    .line 415202
    iput-object p3, p0, LX/2U7;->d:LX/0SG;

    .line 415203
    iput-object p4, p0, LX/2U7;->e:LX/0Uh;

    .line 415204
    const/4 v0, 0x1

    iput v0, p0, LX/2U7;->f:I

    .line 415205
    return-void
.end method

.method private a(JJ)Z
    .locals 11

    .prologue
    const-wide/32 v8, 0x6ddd00

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 415194
    iget-object v2, p0, LX/2U7;->e:LX/0Uh;

    const/16 v3, 0x130

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_2

    .line 415195
    sub-long v2, p1, p3

    cmp-long v2, v2, v8

    if-ltz v2, :cond_1

    .line 415196
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 415197
    goto :goto_0

    .line 415198
    :cond_2
    sub-long v2, p1, p3

    const-wide/32 v4, 0x5265c00

    iget v6, p0, LX/2U7;->f:I

    int-to-long v6, v6

    mul-long/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2U7;
    .locals 5

    .prologue
    .line 415192
    new-instance v4, LX/2U7;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2U7;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/0SG;LX/0Uh;)V

    .line 415193
    return-object v4
.end method


# virtual methods
.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415174
    sget-object v0, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    sget-object v1, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v0, 0x1

    .line 415183
    iget-object v1, p0, LX/2U7;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 415184
    iget-object v1, p0, LX/2U7;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/3g1;->a:LX/0Tn;

    invoke-interface {v1, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 415185
    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 415186
    :cond_0
    :goto_0
    return v0

    .line 415187
    :cond_1
    sub-long v4, v2, v4

    const-wide/32 v6, 0xdbba0

    cmp-long v1, v4, v6

    if-gez v1, :cond_2

    .line 415188
    const/4 v0, 0x0

    goto :goto_0

    .line 415189
    :cond_2
    iget-object v1, p0, LX/2U7;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/3g1;->b:LX/0Tn;

    invoke-interface {v1, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 415190
    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 415191
    invoke-direct {p0, v2, v3, v4, v5}, LX/2U7;->a(JJ)Z

    move-result v0

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415175
    iget-object v0, p0, LX/2U7;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3g1;->a:LX/0Tn;

    iget-object v2, p0, LX/2U7;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 415176
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 415177
    const-string v1, "forceFetch"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 415178
    iget-object v1, p0, LX/2U7;->c:LX/0aG;

    const-string v2, "configuration"

    const v3, -0x6f865ff7

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 415179
    new-instance v1, LX/3g2;

    invoke-direct {v1, p0}, LX/3g2;-><init>(LX/2U7;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 415180
    new-instance v1, LX/3f0;

    sget-object v2, LX/2U7;->a:Ljava/lang/Class;

    invoke-direct {v1, v2}, LX/3f0;-><init>(Ljava/lang/Class;)V

    .line 415181
    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 415182
    return-object v1
.end method
