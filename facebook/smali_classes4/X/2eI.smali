.class public abstract LX/2eI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2eJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E2::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
        "<TE2;>;",
        "LX/2eJ",
        "<",
        "Ljava/lang/Object;",
        "TE2;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1RC",
            "<**-TE2;*>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 445153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445154
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2eI;->a:Ljava/util/List;

    .line 445155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2eI;->b:Ljava/util/List;

    .line 445156
    invoke-virtual {p0, p0}, LX/2eI;->a(LX/2eI;)V

    .line 445157
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 445158
    iget-object v0, p0, LX/2eI;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(I)LX/1RC;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1RC",
            "<",
            "Ljava/lang/Object;",
            "*-TE2;*>;"
        }
    .end annotation

    .prologue
    .line 445159
    iget-object v0, p0, LX/2eI;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RC;

    return-object v0
.end method

.method public final a(LX/1RC;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1RC",
            "<TP;*-TE2;*>;TP;)V"
        }
    .end annotation

    .prologue
    .line 445160
    iget-object v0, p0, LX/2eI;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 445161
    iget-object v0, p0, LX/2eI;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 445162
    return-void
.end method

.method public abstract a(LX/2eI;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE2;>;)V"
        }
    .end annotation
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 445163
    iget-object v0, p0, LX/2eI;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
