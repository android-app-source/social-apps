.class public LX/2Fc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/2Fc;


# instance fields
.field public final a:LX/0go;

.field public final b:LX/2Fd;

.field public final c:LX/0pZ;

.field private final d:LX/0cb;

.field private final e:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
    .end annotation
.end field

.field private final f:LX/0Yb;


# direct methods
.method public constructor <init>(LX/0go;LX/2Fd;LX/0pZ;LX/0cb;LX/0Xl;Landroid/os/Handler;)V
    .locals 3
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p6    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387118
    iput-object p1, p0, LX/2Fc;->a:LX/0go;

    .line 387119
    iput-object p2, p0, LX/2Fc;->b:LX/2Fd;

    .line 387120
    iput-object p3, p0, LX/2Fc;->c:LX/0pZ;

    .line 387121
    iput-object p4, p0, LX/2Fc;->d:LX/0cb;

    .line 387122
    iput-object p6, p0, LX/2Fc;->e:Landroid/os/Handler;

    .line 387123
    new-instance v0, LX/2rH;

    invoke-direct {v0, p0}, LX/2rH;-><init>(LX/2Fc;)V

    .line 387124
    invoke-interface {p5}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/2Fc;->e:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2Fc;->f:LX/0Yb;

    .line 387125
    return-void
.end method

.method public static a(LX/0QB;)LX/2Fc;
    .locals 10

    .prologue
    .line 387126
    sget-object v0, LX/2Fc;->g:LX/2Fc;

    if-nez v0, :cond_1

    .line 387127
    const-class v1, LX/2Fc;

    monitor-enter v1

    .line 387128
    :try_start_0
    sget-object v0, LX/2Fc;->g:LX/2Fc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387129
    if-eqz v2, :cond_0

    .line 387130
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 387131
    new-instance v3, LX/2Fc;

    invoke-static {v0}, LX/0go;->a(LX/0QB;)LX/0go;

    move-result-object v4

    check-cast v4, LX/0go;

    invoke-static {v0}, LX/2Fd;->a(LX/0QB;)LX/2Fd;

    move-result-object v5

    check-cast v5, LX/2Fd;

    invoke-static {v0}, LX/0pY;->a(LX/0QB;)LX/0pY;

    move-result-object v6

    check-cast v6, LX/0pZ;

    invoke-static {v0}, LX/0ca;->a(LX/0QB;)LX/0ca;

    move-result-object v7

    check-cast v7, LX/0cb;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v8

    check-cast v8, LX/0Xl;

    invoke-static {v0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v9

    check-cast v9, Landroid/os/Handler;

    invoke-direct/range {v3 .. v9}, LX/2Fc;-><init>(LX/0go;LX/2Fd;LX/0pZ;LX/0cb;LX/0Xl;Landroid/os/Handler;)V

    .line 387132
    move-object v0, v3

    .line 387133
    sput-object v0, LX/2Fc;->g:LX/2Fc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387134
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387135
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387136
    :cond_1
    sget-object v0, LX/2Fc;->g:LX/2Fc;

    return-object v0

    .line 387137
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387138
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 1

    .prologue
    .line 387139
    iget-object v0, p0, LX/2Fc;->f:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 387140
    return-void
.end method
