.class public LX/2Iw;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Iw;


# direct methods
.method public constructor <init>()V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392393
    const-string v6, "contacts"

    const/16 v7, 0x48

    new-instance v0, LX/2Ix;

    invoke-direct {v0}, LX/2Ix;-><init>()V

    new-instance v1, LX/2Iy;

    invoke-direct {v1}, LX/2Iy;-><init>()V

    new-instance v2, LX/2J2;

    invoke-direct {v2}, LX/2J2;-><init>()V

    new-instance v3, LX/2J5;

    invoke-direct {v3}, LX/2J5;-><init>()V

    new-instance v4, LX/30g;

    invoke-direct {v4}, LX/30g;-><init>()V

    new-instance v5, LX/2J8;

    invoke-direct {v5}, LX/2J8;-><init>()V

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, v6, v7, v0}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 392394
    return-void
.end method

.method public static a(LX/0QB;)LX/2Iw;
    .locals 3

    .prologue
    .line 392395
    sget-object v0, LX/2Iw;->a:LX/2Iw;

    if-nez v0, :cond_1

    .line 392396
    const-class v1, LX/2Iw;

    monitor-enter v1

    .line 392397
    :try_start_0
    sget-object v0, LX/2Iw;->a:LX/2Iw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 392398
    if-eqz v2, :cond_0

    .line 392399
    :try_start_1
    new-instance v0, LX/2Iw;

    invoke-direct {v0}, LX/2Iw;-><init>()V

    .line 392400
    move-object v0, v0

    .line 392401
    sput-object v0, LX/2Iw;->a:LX/2Iw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392402
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 392403
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392404
    :cond_1
    sget-object v0, LX/2Iw;->a:LX/2Iw;

    return-object v0

    .line 392405
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 392406
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;II)I
    .locals 2

    .prologue
    .line 392407
    add-int/lit8 v0, p2, 0x1

    .line 392408
    const/16 v1, 0x44

    if-ne p2, v1, :cond_0

    .line 392409
    new-instance v1, LX/2J8;

    invoke-direct {v1}, LX/2J8;-><init>()V

    .line 392410
    invoke-virtual {v1, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 392411
    move p3, v0

    .line 392412
    :goto_0
    return p3

    .line 392413
    :cond_0
    const/16 v1, 0x45

    if-ne p2, v1, :cond_1

    .line 392414
    const-string v1, "ALTER TABLE contacts ADD COLUMN messenger_invite_priority REAL"

    const p0, -0x47013867

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x56087633

    invoke-static {v1}, LX/03h;->a(I)V

    .line 392415
    move p3, v0

    goto :goto_0

    .line 392416
    :cond_1
    const/16 v1, 0x46

    if-ne p2, v1, :cond_2

    .line 392417
    const-string v1, "ALTER TABLE contacts ADD COLUMN is_memorialized INTEGER"

    const p0, -0x532da74c

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0xf53a78c

    invoke-static {v1}, LX/03h;->a(I)V

    .line 392418
    move p3, v0

    goto :goto_0

    .line 392419
    :cond_2
    const/16 v1, 0x47

    if-ne p2, v1, :cond_3

    .line 392420
    const-string v1, "ALTER TABLE contacts ADD COLUMN viewer_connection_status TEXT"

    const p0, -0x60185500

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x50fbdb7a

    invoke-static {v1}, LX/03h;->a(I)V

    .line 392421
    move p3, v0

    goto :goto_0

    .line 392422
    :cond_3
    invoke-static {p1}, LX/2Iw;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 392423
    invoke-virtual {p0, p1}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method private static d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 392424
    const-string v0, "contact_summaries"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x439622f7

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x4c821769    # 6.8205384E7f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392425
    const-string v0, "contact_details"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x12130a50

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x305a1650

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392426
    const-string v0, "contacts_db_properties"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x59918bd7

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6b362fa

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392427
    const-string v0, "contacts"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7d8cafc6

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x37a76afd

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392428
    const-string v0, "contacts_indexed_data"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x2642413d

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x7a400ea4

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392429
    const-string v0, "ephemeral_data"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7bf86067

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x7c2c491a

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392430
    const-string v0, "favorite_contacts"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x6b8c6c7d

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x457e091f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392431
    const-string v0, "favorite_sms_contacts"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x6aea1821

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x50ff9e29

    invoke-static {v0}, LX/03h;->a(I)V

    .line 392432
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 392433
    if-le p2, p3, :cond_0

    .line 392434
    invoke-static {p1}, LX/2Iw;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 392435
    const/16 p2, 0x43

    .line 392436
    :cond_0
    :goto_0
    if-ge p2, p3, :cond_1

    .line 392437
    invoke-direct {p0, p1, p2, p3}, LX/2Iw;->b(Landroid/database/sqlite/SQLiteDatabase;II)I

    move-result p2

    goto :goto_0

    .line 392438
    :cond_1
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 392439
    return-void
.end method
