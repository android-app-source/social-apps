.class public abstract LX/25h;
.super LX/0Ye;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field private final a:[Ljava/lang/String;


# direct methods
.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 369907
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0Ye;-><init>(LX/0Sk;)V

    .line 369908
    iput-object p1, p0, LX/25h;->a:[Ljava/lang/String;

    .line 369909
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)LX/0YZ;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 369910
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 369911
    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v0

    check-cast v0, LX/0aU;

    .line 369912
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 369913
    iget-object v3, p0, LX/25h;->a:[Ljava/lang/String;

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 369914
    invoke-virtual {v0, v5}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 369915
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 369916
    new-instance v0, LX/25i;

    invoke-direct {v0, p0, v5}, LX/25i;-><init>(LX/25h;Ljava/lang/String;)V

    .line 369917
    :goto_1
    return-object v0

    .line 369918
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 369919
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public abstract a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 369920
    const/4 v0, 0x0

    return v0
.end method
