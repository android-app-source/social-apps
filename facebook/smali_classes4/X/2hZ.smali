.class public LX/2hZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/0kL;

.field public final d:LX/17Y;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0kL;LX/17Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 450108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450109
    iput-object p1, p0, LX/2hZ;->a:Landroid/content/Context;

    .line 450110
    iput-object p2, p0, LX/2hZ;->b:Lcom/facebook/content/SecureContextHelper;

    .line 450111
    iput-object p3, p0, LX/2hZ;->c:LX/0kL;

    .line 450112
    iput-object p4, p0, LX/2hZ;->d:LX/17Y;

    .line 450113
    return-void
.end method

.method public static a(LX/0QB;)LX/2hZ;
    .locals 1

    .prologue
    .line 450114
    invoke-static {p0}, LX/2hZ;->b(LX/0QB;)LX/2hZ;

    move-result-object v0

    return-object v0
.end method

.method private static a()Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 450115
    new-instance v0, LX/84U;

    invoke-direct {v0}, LX/84U;-><init>()V

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2hZ;
    .locals 5

    .prologue
    .line 450116
    new-instance v4, LX/2hZ;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v2

    check-cast v2, LX/0kL;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v3

    check-cast v3, LX/17Y;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2hZ;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0kL;LX/17Y;)V

    .line 450117
    return-object v4
.end method

.method private static c(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 450118
    instance-of v1, p0, LX/4Ua;

    move v1, v1

    .line 450119
    if-nez v1, :cond_1

    .line 450120
    :cond_0
    :goto_0
    return-object v0

    .line 450121
    :cond_1
    check-cast p0, LX/4Ua;

    .line 450122
    iget-object v1, p0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 450123
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static e(Ljava/lang/Throwable;)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 450124
    instance-of v1, p0, LX/4Ua;

    move v1, v1

    .line 450125
    if-nez v1, :cond_1

    .line 450126
    :cond_0
    :goto_0
    return v0

    .line 450127
    :cond_1
    check-cast p0, LX/4Ua;

    .line 450128
    iget-object v1, p0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 450129
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 450130
    const v0, 0x7f080039

    .line 450131
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/2hZ;->a(Ljava/lang/Throwable;ILandroid/content/DialogInterface$OnClickListener;)V

    .line 450132
    return-void
.end method

.method public final a(Ljava/lang/Throwable;ILandroid/content/DialogInterface$OnClickListener;)V
    .locals 1

    .prologue
    .line 450133
    iget-object v0, p0, LX/2hZ;->a:Landroid/content/Context;

    invoke-virtual {p0, p1, p2, p3, v0}, LX/2hZ;->a(Ljava/lang/Throwable;ILandroid/content/DialogInterface$OnClickListener;Landroid/content/Context;)V

    .line 450134
    return-void
.end method

.method public final a(Ljava/lang/Throwable;ILandroid/content/DialogInterface$OnClickListener;Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 450135
    const-class v0, Landroid/app/Activity;

    invoke-static {p4, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 450136
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 450137
    if-nez v0, :cond_0

    .line 450138
    :goto_1
    return-void

    .line 450139
    :cond_0
    invoke-static {p1}, LX/2hZ;->c(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    .line 450140
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 450141
    iget-object v0, p0, LX/2hZ;->c:LX/0kL;

    new-instance v1, LX/27k;

    invoke-direct {v1, p2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_1

    .line 450142
    :cond_1
    invoke-static {p1}, LX/2hZ;->e(Ljava/lang/Throwable;)I

    move-result v3

    .line 450143
    new-instance v4, LX/31Y;

    invoke-direct {v4, p4}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 450144
    const/4 v0, 0x0

    .line 450145
    instance-of v1, p1, LX/4Ua;

    move v1, v1

    .line 450146
    if-nez v1, :cond_7

    .line 450147
    :cond_2
    :goto_2
    move-object v5, v0

    .line 450148
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 450149
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 450150
    :goto_3
    const v0, 0x157832

    if-ne v3, v0, :cond_4

    .line 450151
    const v0, 0x7f080019

    invoke-virtual {v4, v0, p3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    invoke-static {}, LX/2hZ;->a()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 450152
    :goto_4
    invoke-virtual {v4}, LX/0ju;->b()LX/2EJ;

    goto :goto_1

    .line 450153
    :cond_3
    iget-object v0, p0, LX/2hZ;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306fc

    const/4 v6, 0x0

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 450154
    const v0, 0x7f0d12b1

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 450155
    const v1, 0x7f0d12b2

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 450156
    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 450157
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 450158
    invoke-virtual {v4, v6}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    goto :goto_3

    .line 450159
    :cond_4
    const v0, 0x15782a

    if-ne v3, v0, :cond_5

    .line 450160
    const v0, 0x7f080038

    .line 450161
    new-instance v1, LX/84V;

    invoke-direct {v1, p0}, LX/84V;-><init>(LX/2hZ;)V

    move-object v1, v1

    .line 450162
    invoke-virtual {v4, v0, v1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080022

    invoke-static {}, LX/2hZ;->a()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_4

    .line 450163
    :cond_5
    const v0, 0x7f080016

    invoke-static {}, LX/2hZ;->a()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 450164
    :cond_7
    check-cast p1, LX/4Ua;

    .line 450165
    iget-object v1, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 450166
    if-eqz v1, :cond_2

    iget-object v0, v1, Lcom/facebook/graphql/error/GraphQLError;->summary:Ljava/lang/String;

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/Throwable;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 1

    .prologue
    .line 450167
    const v0, 0x7f080039

    invoke-virtual {p0, p1, v0, p2}, LX/2hZ;->a(Ljava/lang/Throwable;ILandroid/content/DialogInterface$OnClickListener;)V

    .line 450168
    return-void
.end method
