.class public final LX/2kf;
.super LX/2kg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2kg",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        "LX/3DK;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2ke;

.field public final synthetic b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;LX/2ke;)V
    .locals 0

    .prologue
    .line 456434
    iput-object p1, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iput-object p2, p0, LX/2kf;->a:LX/2ke;

    invoke-direct {p0}, LX/2kg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/2ke;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2ke",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 456435
    iget-object v0, p0, LX/2kf;->a:LX/2ke;

    return-object v0
.end method

.method public final a(LX/2kM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 456436
    invoke-interface {p1}, LX/2kM;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 456437
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->y:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 456438
    :goto_0
    return-void

    .line 456439
    :cond_0
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    sget-object v1, LX/2ub;->FRAGMENT_LOADED:LX/2ub;

    invoke-static {v0, v1}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->a$redex0(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;LX/2ub;)V

    goto :goto_0
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 456440
    check-cast p3, LX/3DK;

    .line 456441
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 456442
    invoke-static {v0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->c(LX/2nk;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-boolean v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->K:Z

    if-nez v0, :cond_0

    .line 456443
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->v:LX/2kw;

    sget-object v1, LX/2kx;->LOADING:LX/2kx;

    invoke-virtual {v0, v1}, LX/2kw;->a(LX/2kx;)V

    .line 456444
    :cond_0
    if-nez p3, :cond_2

    .line 456445
    :cond_1
    :goto_0
    return-void

    .line 456446
    :cond_2
    iget-object v0, p3, LX/3DK;->b:LX/2ub;

    move-object v0, v0

    .line 456447
    sget-object v1, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    if-ne v0, v1, :cond_3

    .line 456448
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350005

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto :goto_0

    .line 456449
    :cond_3
    iget-object v0, p3, LX/3DK;->b:LX/2ub;

    move-object v0, v0

    .line 456450
    sget-object v1, LX/2ub;->SCROLL:LX/2ub;

    if-ne v0, v1, :cond_1

    .line 456451
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000d

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto :goto_0
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 456452
    check-cast p3, LX/3DK;

    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 456453
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    .line 456454
    iput-boolean v3, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->K:Z

    .line 456455
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 456456
    sget-object v1, LX/2nk;->INITIAL:LX/2nk;

    if-ne v0, v1, :cond_5

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 456457
    if-eqz v0, :cond_2

    .line 456458
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->y:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/H4j;

    invoke-direct {v2, p0}, LX/H4j;-><init>(LX/2kf;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 456459
    :cond_0
    :goto_1
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->B:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 456460
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->v:LX/2kw;

    iget-object v1, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v1, v1, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->v:LX/2kw;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1, v3}, LX/1OM;->a(II)V

    .line 456461
    if-nez p3, :cond_3

    .line 456462
    :cond_1
    :goto_2
    return-void

    .line 456463
    :cond_2
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 456464
    invoke-static {v0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->c(LX/2nk;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456465
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->v:LX/2kw;

    sget-object v1, LX/2kx;->FAILED:LX/2kx;

    invoke-virtual {v0, v1}, LX/2kw;->a(LX/2kx;)V

    goto :goto_1

    .line 456466
    :cond_3
    iget-object v0, p3, LX/3DK;->b:LX/2ub;

    move-object v0, v0

    .line 456467
    sget-object v1, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    if-ne v0, v1, :cond_4

    .line 456468
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350005

    invoke-interface {v0, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_2

    .line 456469
    :cond_4
    iget-object v0, p3, LX/3DK;->b:LX/2ub;

    move-object v0, v0

    .line 456470
    sget-object v1, LX/2ub;->SCROLL:LX/2ub;

    if-ne v0, v1, :cond_1

    .line 456471
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000d

    invoke-interface {v0, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 456472
    check-cast p3, LX/3DK;

    const/4 v2, 0x0

    .line 456473
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    .line 456474
    iput-boolean v2, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->K:Z

    .line 456475
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->p:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456476
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->y:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 456477
    :cond_0
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->y:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 456478
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 456479
    invoke-static {v0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->c(LX/2nk;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 456480
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->v:LX/2kw;

    sget-object v1, LX/2kx;->FINISHED:LX/2kx;

    invoke-virtual {v0, v1}, LX/2kw;->a(LX/2kx;)V

    .line 456481
    :cond_1
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->B:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 456482
    sget-boolean v0, LX/3Cc;->a:Z

    move v0, v0

    .line 456483
    if-eqz v0, :cond_2

    .line 456484
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    invoke-static {v0}, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->c(Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;)V

    .line 456485
    :cond_2
    if-nez p3, :cond_3

    .line 456486
    :goto_0
    return-void

    .line 456487
    :cond_3
    iget-object v0, p3, LX/3DK;->b:LX/2ub;

    move-object v0, v0

    .line 456488
    sget-object v1, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    if-ne v0, v1, :cond_5

    .line 456489
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x350005

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 456490
    :cond_4
    :goto_1
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v1, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v1, v1, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 456491
    iput-wide v2, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->O:J

    .line 456492
    goto :goto_0

    .line 456493
    :cond_5
    iget-object v0, p3, LX/3DK;->b:LX/2ub;

    move-object v0, v0

    .line 456494
    sget-object v1, LX/2ub;->SCROLL:LX/2ub;

    if-ne v0, v1, :cond_4

    .line 456495
    iget-object v0, p0, LX/2kf;->b:Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/notifications/connectioncontroller/NotificationsConnectionControllerFragment;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000d

    const/16 v2, 0x1b

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_1
.end method
