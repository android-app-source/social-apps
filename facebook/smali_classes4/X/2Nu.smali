.class public LX/2Nu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Nu;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "LX/0SW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 400182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400183
    iput-object p1, p0, LX/2Nu;->a:LX/0Zb;

    .line 400184
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/32 v2, 0x7b98a000

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/2Nu;->b:LX/0QI;

    .line 400185
    return-void
.end method

.method public static a(LX/0QB;)LX/2Nu;
    .locals 4

    .prologue
    .line 400169
    sget-object v0, LX/2Nu;->c:LX/2Nu;

    if-nez v0, :cond_1

    .line 400170
    const-class v1, LX/2Nu;

    monitor-enter v1

    .line 400171
    :try_start_0
    sget-object v0, LX/2Nu;->c:LX/2Nu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 400172
    if-eqz v2, :cond_0

    .line 400173
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 400174
    new-instance p0, LX/2Nu;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/2Nu;-><init>(LX/0Zb;)V

    .line 400175
    move-object v0, p0

    .line 400176
    sput-object v0, LX/2Nu;->c:LX/2Nu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400177
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 400178
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 400179
    :cond_1
    sget-object v0, LX/2Nu;->c:LX/2Nu;

    return-object v0

    .line 400180
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 400181
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 400161
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 400162
    const-string v1, "two_phase_send"

    .line 400163
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 400164
    const-string v1, "offline_threading_id"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 400165
    const-string v1, "media_type"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 400166
    const-string v1, "attachment_id"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 400167
    const-string v1, "media_fbid"

    invoke-virtual {p1}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 400168
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 400148
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 400149
    const-string v1, "two_phase_send"

    .line 400150
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 400151
    const-string v1, "offline_threading_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 400152
    const-string v1, "message_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 400153
    return-object v0
.end method

.method public static c(LX/2Nu;Lcom/facebook/ui/media/attachments/MediaResource;)J
    .locals 2

    .prologue
    .line 400154
    iget-object v0, p0, LX/2Nu;->b:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SW;

    .line 400155
    if-nez v0, :cond_0

    .line 400156
    const-wide/16 v0, 0x0

    .line 400157
    :goto_0
    return-wide v0

    .line 400158
    :cond_0
    invoke-virtual {v0}, LX/0SW;->stop()LX/0SW;

    .line 400159
    iget-object v1, p0, LX/2Nu;->b:LX/0QI;

    invoke-interface {v1, p1}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 400160
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    goto :goto_0
.end method
