.class public final LX/2pT;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V
    .locals 0

    .prologue
    .line 468354
    iput-object p1, p0, LX/2pT;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 468355
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 468356
    check-cast p1, LX/2ou;

    const/4 v4, 0x0

    .line 468357
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 468358
    iget-object v0, p0, LX/2pT;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-static {v0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->g(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V

    .line 468359
    iget-object v0, p0, LX/2pT;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->a:LX/2pS;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, LX/2pS;->sendEmptyMessageDelayed(IJ)Z

    .line 468360
    :goto_0
    return-void

    .line 468361
    :cond_0
    iget-object v0, p0, LX/2pT;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-static {v0, v4}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->a$redex0(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;Z)V

    goto :goto_0
.end method
