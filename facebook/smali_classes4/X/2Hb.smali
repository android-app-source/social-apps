.class public LX/2Hb;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final m:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static n:Ljava/lang/Boolean;


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/inject/ForAppContext;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Uo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2Cx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2Ct;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2Hc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2GC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0aU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/2D5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hlk;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 390643
    const-class v0, LX/2Hb;

    sput-object v0, LX/2Hb;->m:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390645
    return-void
.end method

.method public static b(LX/0QB;)LX/2Hb;
    .locals 13

    .prologue
    .line 390646
    new-instance v0, LX/2Hb;

    invoke-direct {v0}, LX/2Hb;-><init>()V

    .line 390647
    const-class v1, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v3

    check-cast v3, LX/0Uo;

    invoke-static {p0}, LX/2Cx;->a(LX/0QB;)LX/2Cx;

    move-result-object v4

    check-cast v4, LX/2Cx;

    invoke-static {p0}, LX/2Ct;->a(LX/0QB;)LX/2Ct;

    move-result-object v5

    check-cast v5, LX/2Ct;

    invoke-static {p0}, LX/2Hc;->a(LX/0QB;)LX/2Hc;

    move-result-object v6

    check-cast v6, LX/2Hc;

    invoke-static {p0}, LX/2GC;->a(LX/0QB;)LX/2GC;

    move-result-object v7

    check-cast v7, LX/2GC;

    invoke-static {p0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v8

    check-cast v8, LX/0aU;

    invoke-static {p0}, LX/2D1;->b(LX/0QB;)LX/2D5;

    move-result-object v9

    check-cast v9, LX/2D5;

    const/16 v10, 0x17e4

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v12

    check-cast v12, LX/0W3;

    .line 390648
    iput-object v1, v0, LX/2Hb;->a:Landroid/content/Context;

    iput-object v2, v0, LX/2Hb;->b:LX/0Sh;

    iput-object v3, v0, LX/2Hb;->c:LX/0Uo;

    iput-object v4, v0, LX/2Hb;->d:LX/2Cx;

    iput-object v5, v0, LX/2Hb;->e:LX/2Ct;

    iput-object v6, v0, LX/2Hb;->f:LX/2Hc;

    iput-object v7, v0, LX/2Hb;->g:LX/2GC;

    iput-object v8, v0, LX/2Hb;->h:LX/0aU;

    iput-object v9, v0, LX/2Hb;->i:LX/2D5;

    iput-object v10, v0, LX/2Hb;->j:LX/0Ot;

    iput-object v11, v0, LX/2Hb;->k:Lcom/facebook/content/SecureContextHelper;

    iput-object v12, v0, LX/2Hb;->l:LX/0W3;

    .line 390649
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 390650
    iget-object v0, p0, LX/2Hb;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 390651
    iget-object v0, p0, LX/2Hb;->i:LX/2D5;

    invoke-interface {v0, p1}, LX/2D5;->a(Landroid/content/Intent;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v1

    .line 390652
    if-nez v1, :cond_0

    .line 390653
    :goto_0
    return-void

    .line 390654
    :cond_0
    iget-object v0, p0, LX/2Hb;->d:LX/2Cx;

    .line 390655
    iget-object v5, v0, LX/2Cx;->b:LX/0Zb;

    const-string v6, "background_location_location_update"

    invoke-static {v6}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-static {v0, v6}, LX/2Cx;->a(LX/2Cx;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "age_ms"

    iget-object v8, v0, LX/2Cx;->a:LX/0yD;

    invoke-virtual {v8, v1}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v9

    invoke-virtual {v6, v7, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "accuracy_meters"

    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v8

    invoke-virtual {v8}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 390656
    iget-object v5, v0, LX/2Cx;->c:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v5

    iput-wide v5, v0, LX/2Cx;->d:J

    .line 390657
    sget-object v0, LX/2Hb;->n:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 390658
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 390659
    iget-object v2, p0, LX/2Hb;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    .line 390660
    if-nez v0, :cond_2

    iget-object v0, p0, LX/2Hb;->l:LX/0W3;

    sget-wide v2, LX/0X5;->ai:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, LX/2Hb;->n:Ljava/lang/Boolean;

    .line 390661
    :cond_1
    sget-object v0, LX/2Hb;->n:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 390662
    iget-object v0, p0, LX/2Hb;->k:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/2Hb;->a:Landroid/content/Context;

    iget-object v3, p0, LX/2Hb;->h:LX/0aU;

    invoke-static {v2, v3, v1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingGcmUploadSchedulerService;->a(Landroid/content/Context;LX/0aU;Lcom/facebook/location/ImmutableLocation;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, LX/2Hb;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    goto :goto_0

    .line 390663
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 390664
    :cond_3
    iget-object v0, p0, LX/2Hb;->e:LX/2Ct;

    invoke-virtual {v0}, LX/2Ct;->b()V

    .line 390665
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 390666
    iget-object v2, p0, LX/2Hb;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    .line 390667
    if-nez v0, :cond_4

    iget-object v0, p0, LX/2Hb;->l:LX/0W3;

    sget-wide v2, LX/0X5;->am:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 390668
    iget-object v0, p0, LX/2Hb;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hlk;

    invoke-virtual {v0, v1}, LX/Hlk;->a(Lcom/facebook/location/ImmutableLocation;)V

    .line 390669
    :cond_4
    iget-object v0, p0, LX/2Hb;->f:LX/2Hc;

    new-instance v2, LX/2TT;

    invoke-direct {v2}, LX/2TT;-><init>()V

    .line 390670
    iput-object v1, v2, LX/2TT;->a:Lcom/facebook/location/ImmutableLocation;

    .line 390671
    move-object v1, v2

    .line 390672
    iget-object v2, p0, LX/2Hb;->c:LX/0Uo;

    invoke-virtual {v2}, LX/0Uo;->l()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 390673
    iput-object v2, v1, LX/2TT;->b:Ljava/lang/Boolean;

    .line 390674
    move-object v1, v1

    .line 390675
    iget-object v2, p0, LX/2Hb;->g:LX/2GC;

    invoke-virtual {v2}, LX/2GC;->a()Lcom/facebook/wifiscan/WifiScanResult;

    move-result-object v2

    .line 390676
    iput-object v2, v1, LX/2TT;->c:Lcom/facebook/wifiscan/WifiScanResult;

    .line 390677
    move-object v1, v1

    .line 390678
    iget-object v2, p0, LX/2Hb;->g:LX/2GC;

    invoke-virtual {v2}, LX/2GC;->b()Ljava/util/List;

    move-result-object v2

    .line 390679
    iput-object v2, v1, LX/2TT;->d:Ljava/util/List;

    .line 390680
    move-object v1, v1

    .line 390681
    iput-object v4, v1, LX/2TT;->e:Lcom/facebook/location/GeneralCellInfo;

    .line 390682
    move-object v1, v1

    .line 390683
    iput-object v4, v1, LX/2TT;->f:Ljava/util/List;

    .line 390684
    move-object v1, v1

    .line 390685
    invoke-virtual {v1}, LX/2TT;->a()Lcom/facebook/location/LocationSignalDataPackage;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Hc;->a(Lcom/facebook/location/LocationSignalDataPackage;)V

    goto/16 :goto_0
.end method
