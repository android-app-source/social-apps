.class public final LX/375;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Landroid/net/Uri;

.field public final c:LX/374;

.field public final synthetic d:LX/1Lv;

.field private e:Z

.field private f:LX/3Dh;

.field private g:LX/3DV;


# direct methods
.method public constructor <init>(LX/1Lv;ILX/374;)V
    .locals 1

    .prologue
    .line 500640
    iput-object p1, p0, LX/375;->d:LX/1Lv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 500641
    iput p2, p0, LX/375;->a:I

    .line 500642
    iget-object v0, p3, LX/374;->b:LX/36s;

    move-object v0, v0

    .line 500643
    iget-object p1, v0, LX/36s;->d:Landroid/net/Uri;

    move-object v0, p1

    .line 500644
    iput-object v0, p0, LX/375;->b:Landroid/net/Uri;

    .line 500645
    iput-object p3, p0, LX/375;->c:LX/374;

    .line 500646
    return-void
.end method

.method private static a(LX/375;ILandroid/net/Uri;LX/2WF;LX/37C;LX/3Da;LX/3DW;ILjava/util/List;)LX/3Da;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/net/Uri;",
            "LX/2WF;",
            "LX/37C;",
            "LX/3Da;",
            "LX/3DW;",
            "I",
            "Ljava/util/List",
            "<",
            "LX/2WF;",
            ">;)",
            "LX/3Da;"
        }
    .end annotation

    .prologue
    .line 500606
    monitor-enter p0

    .line 500607
    :try_start_0
    iget-boolean v0, p0, LX/375;->e:Z

    if-eqz v0, :cond_0

    .line 500608
    new-instance v0, LX/7Q6;

    invoke-direct {v0}, LX/7Q6;-><init>()V

    throw v0

    .line 500609
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 500610
    :cond_0
    if-nez p5, :cond_2

    .line 500611
    :try_start_1
    iget-object v0, p0, LX/375;->d:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->u:LX/16V;

    new-instance v1, LX/372;

    invoke-direct {v1, p1}, LX/372;-><init>(I)V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 500612
    :goto_0
    new-instance v0, LX/3Dg;

    iget-object v1, p0, LX/375;->d:LX/1Lv;

    iget-object v1, v1, LX/1Lv;->d:LX/1Ln;

    iget-wide v4, p3, LX/2WF;->a:J

    iget-object v2, p0, LX/375;->d:LX/1Lv;

    iget-object v6, v2, LX/1Lv;->q:LX/1FQ;

    move-object v2, p4

    move-object v3, p5

    invoke-direct/range {v0 .. v6}, LX/3Dg;-><init>(LX/1Ln;LX/37C;LX/3Da;JLX/1FQ;)V

    .line 500613
    new-instance v1, LX/3Dh;

    iget-object v2, p0, LX/375;->d:LX/1Lv;

    move-object v3, v0

    move-object v4, p5

    move-object v5, p2

    move v6, p1

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object v9, p3

    invoke-direct/range {v1 .. v9}, LX/3Dh;-><init>(LX/1Lv;LX/3Dg;LX/3Da;Landroid/net/Uri;IILjava/util/List;LX/2WF;)V

    iput-object v1, p0, LX/375;->f:LX/3Dh;

    .line 500614
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 500615
    :try_start_2
    iget-wide v2, p3, LX/2WF;->a:J

    iget-wide v4, p3, LX/2WF;->b:J

    iget-object v6, p0, LX/375;->f:LX/3Dh;

    move-object/from16 v1, p6

    invoke-interface/range {v1 .. v6}, LX/3DW;->a(JJLX/3Di;)V

    .line 500616
    iget-object v0, p0, LX/375;->f:LX/3Dh;

    invoke-virtual {v0}, LX/3Dh;->a()LX/3Da;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v1

    .line 500617
    monitor-enter p0

    .line 500618
    :try_start_3
    iget-boolean v0, p0, LX/375;->e:Z

    if-nez v0, :cond_1

    .line 500619
    invoke-static {}, LX/1Lv;->b()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 500620
    :try_start_4
    iget-object v0, p0, LX/375;->f:LX/3Dh;

    iget-object v0, v0, LX/3Dh;->e:LX/3Dg;

    invoke-virtual {v0}, LX/3Dg;->a()LX/7Os;

    move-result-object v0

    .line 500621
    if-eqz v0, :cond_1

    .line 500622
    invoke-virtual {v0}, LX/7Os;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 500623
    :cond_1
    :goto_1
    const/4 v0, 0x0

    :try_start_5
    iput-object v0, p0, LX/375;->f:LX/3Dh;

    .line 500624
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    return-object v1

    .line 500625
    :cond_2
    :try_start_6
    iget-object v0, p0, LX/375;->d:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->u:LX/16V;

    new-instance v1, LX/370;

    invoke-direct {v1, p1, p3}, LX/370;-><init>(ILX/2WF;)V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 500626
    :catch_0
    move-exception v0

    .line 500627
    :try_start_7
    sget-object v2, LX/1Lv;->a:Ljava/lang/String;

    const-string v3, "Error closing readableWriter"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 500628
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v0

    .line 500629
    :catchall_2
    move-exception v0

    monitor-enter p0

    .line 500630
    :try_start_8
    iget-boolean v1, p0, LX/375;->e:Z

    if-nez v1, :cond_3

    .line 500631
    invoke-static {}, LX/1Lv;->b()Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 500632
    :try_start_9
    iget-object v1, p0, LX/375;->f:LX/3Dh;

    iget-object v1, v1, LX/3Dh;->e:LX/3Dg;

    invoke-virtual {v1}, LX/3Dg;->a()LX/7Os;

    move-result-object v1

    .line 500633
    if-eqz v1, :cond_3

    .line 500634
    invoke-virtual {v1}, LX/7Os;->b()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 500635
    :cond_3
    :goto_2
    const/4 v1, 0x0

    :try_start_a
    iput-object v1, p0, LX/375;->f:LX/3Dh;

    .line 500636
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v0

    .line 500637
    :catch_1
    move-exception v1

    .line 500638
    :try_start_b
    sget-object v2, LX/1Lv;->a:Ljava/lang/String;

    const-string v3, "Error closing readableWriter"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 500639
    :catchall_3
    move-exception v0

    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    throw v0
.end method

.method private a(LX/374;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 500701
    iget-object v0, p1, LX/374;->b:LX/36s;

    move-object v0, v0

    .line 500702
    iget-object v1, v0, LX/36s;->k:Ljava/lang/String;

    move-object v0, v1

    .line 500703
    if-nez v0, :cond_0

    .line 500704
    :goto_0
    return-void

    .line 500705
    :cond_0
    iget-object v0, p0, LX/375;->d:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->e:LX/1AA;

    .line 500706
    iget-object v1, p1, LX/374;->b:LX/36s;

    move-object v1, v1

    .line 500707
    iget-object v1, v1, LX/36s;->a:Ljava/lang/String;

    .line 500708
    iget-object v2, p1, LX/374;->b:LX/36s;

    move-object v2, v2

    .line 500709
    iget-object p0, v2, LX/36s;->k:Ljava/lang/String;

    move-object v2, p0

    .line 500710
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 500711
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 500712
    new-instance v3, LX/0Tn;

    sget-object v4, LX/1AA;->a:LX/0Tn;

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string p1, "/"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/0Tn;-><init>(LX/0To;Ljava/lang/String;)V

    .line 500713
    new-instance v4, LX/0Tn;

    invoke-direct {v4, v3, v2}, LX/0Tn;-><init>(LX/0To;Ljava/lang/String;)V

    .line 500714
    iget-object p0, v0, LX/1AA;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p0

    invoke-interface {p0, v3}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v3

    invoke-interface {v3, v4, p2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 500715
    iget-object v3, v0, LX/1AA;->c:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500716
    goto :goto_0
.end method

.method private static a(LX/375;ILX/374;)Z
    .locals 13

    .prologue
    .line 500661
    iget-object v0, p2, LX/374;->b:LX/36s;

    move-object v0, v0

    .line 500662
    iget-object v1, v0, LX/36s;->d:Landroid/net/Uri;

    move-object v1, v1

    .line 500663
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    .line 500664
    iget-object v0, p0, LX/375;->d:LX/1Lv;

    invoke-static {v0, p2}, LX/1Lv;->a$redex0(LX/1Lv;LX/374;)I

    move-result v4

    .line 500665
    if-gtz v4, :cond_0

    .line 500666
    const/4 v0, 0x1

    .line 500667
    :goto_0
    return v0

    .line 500668
    :cond_0
    const/4 v0, 0x0

    .line 500669
    iget-object v2, p0, LX/375;->d:LX/1Lv;

    iget-object v2, v2, LX/1Lv;->z:LX/19s;

    invoke-virtual {v2}, LX/19s;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 500670
    iget-object v2, p2, LX/374;->b:LX/36s;

    move-object v2, v2

    .line 500671
    iget-object v3, v2, LX/36s;->h:LX/36t;

    move-object v2, v3

    .line 500672
    sget-object v3, LX/36t;->DASH:LX/36t;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/375;->d:LX/1Lv;

    iget-object v2, v2, LX/1Lv;->z:LX/19s;

    invoke-virtual {v2}, LX/19s;->f()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 500673
    const/4 v0, 0x1

    .line 500674
    :cond_1
    :goto_1
    if-eqz v0, :cond_4

    .line 500675
    const-wide/16 v10, -0x1

    .line 500676
    const/4 v9, 0x0

    .line 500677
    :try_start_0
    iget-object v0, p0, LX/375;->d:LX/1Lv;

    iget-object v12, v0, LX/1Lv;->z:LX/19s;

    new-instance v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    .line 500678
    iget-object v2, p2, LX/374;->b:LX/36s;

    move-object v2, v2

    .line 500679
    iget-object v2, v2, LX/36s;->a:Ljava/lang/String;

    .line 500680
    iget-object v3, p2, LX/374;->b:LX/36s;

    move-object v3, v3

    .line 500681
    iget-object v5, v3, LX/36s;->j:Ljava/lang/String;

    move-object v3, v5

    .line 500682
    sget-object v5, LX/379;->FEED:LX/379;

    invoke-virtual {v5}, LX/379;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 500683
    iget-object v7, p2, LX/374;->b:LX/36s;

    move-object v7, v7

    .line 500684
    iget v7, v7, LX/36s;->b:I

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V

    invoke-virtual {v12, v0}, LX/19s;->a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    move-object v8, v9

    .line 500685
    :goto_2
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 500686
    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-lez v0, :cond_2

    .line 500687
    new-instance v0, LX/2WF;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3, v5, v6}, LX/2WF;-><init>(JJ)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 500688
    :cond_2
    iget-object v0, p0, LX/375;->d:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0A1;

    iget-object v1, p0, LX/375;->b:Landroid/net/Uri;

    int-to-long v2, v4

    .line 500689
    iget-object v4, p2, LX/374;->b:LX/36s;

    move-object v4, v4

    .line 500690
    iget-object v7, v4, LX/36s;->a:Ljava/lang/String;

    .line 500691
    iget-object v4, p2, LX/374;->c:LX/1Li;

    move-object v4, v4

    .line 500692
    invoke-virtual {v4}, LX/1Li;->name()Ljava/lang/String;

    move-result-object v9

    move-object v4, v10

    invoke-virtual/range {v0 .. v9}, LX/0A1;->a(Landroid/net/Uri;JLjava/util/List;JLjava/lang/String;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 500693
    const/4 v0, 0x1

    goto :goto_0

    .line 500694
    :cond_3
    iget-object v2, p2, LX/374;->b:LX/36s;

    move-object v2, v2

    .line 500695
    iget-object v3, v2, LX/36s;->h:LX/36t;

    move-object v2, v3

    .line 500696
    sget-object v3, LX/36t;->PROGRESSIVE:LX/36t;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, LX/375;->d:LX/1Lv;

    iget-object v2, v2, LX/1Lv;->z:LX/19s;

    invoke-virtual {v2}, LX/19s;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 500697
    const/4 v0, 0x1

    goto :goto_1

    .line 500698
    :catch_0
    move-exception v8

    move-wide v5, v10

    .line 500699
    goto :goto_2

    .line 500700
    :cond_4
    invoke-static {p0, p1, v1, v4, p2}, LX/375;->a(LX/375;ILandroid/net/Uri;ILX/374;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method private static a(LX/375;ILandroid/net/Uri;ILX/374;)Z
    .locals 22

    .prologue
    .line 500717
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/375;->d:LX/1Lv;

    iget-object v2, v2, LX/1Lv;->n:LX/1Lt;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, LX/1Lt;->a(Landroid/net/Uri;)LX/37C;

    move-result-object v12

    .line 500718
    new-instance v2, LX/3DV;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/375;->d:LX/1Lv;

    iget-object v4, v3, LX/1Lv;->p:LX/1Lw;

    const/4 v5, 0x0

    sget-object v6, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    const-string v7, "rangeRequestForVideo"

    move-object/from16 v0, p0

    iget-object v3, v0, LX/375;->d:LX/1Lv;

    iget-object v8, v3, LX/1Lv;->h:LX/03V;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/375;->d:LX/1Lv;

    iget-object v9, v3, LX/1Lv;->x:LX/0WJ;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/375;->d:LX/1Lv;

    iget-object v10, v3, LX/1Lv;->y:LX/0lC;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/375;->d:LX/1Lv;

    iget-object v11, v3, LX/1Lv;->r:LX/1Lu;

    move-object/from16 v3, p2

    invoke-direct/range {v2 .. v11}, LX/3DV;-><init>(Landroid/net/Uri;LX/1Lw;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/String;LX/03V;LX/0WJ;LX/0lC;LX/1Lu;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/375;->g:LX/3DV;

    .line 500719
    new-instance v8, LX/3DX;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/375;->g:LX/3DV;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/375;->d:LX/1Lv;

    iget-object v3, v3, LX/1Lv;->v:LX/0So;

    invoke-direct {v8, v2, v3}, LX/3DX;-><init>(LX/3DW;LX/0So;)V

    .line 500720
    move-object/from16 v0, p0

    iget-object v2, v0, LX/375;->d:LX/1Lv;

    iget-object v2, v2, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_0
    .catch LX/7Q6; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 500721
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/375;->d:LX/1Lv;

    iget-object v2, v2, LX/1Lv;->d:LX/1Ln;

    invoke-interface {v2, v12}, LX/1Ln;->b(Ljava/lang/Object;)LX/3Da;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 500722
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/375;->d:LX/1Lv;

    iget-object v2, v2, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 500723
    new-instance v2, LX/2WF;

    const-wide/16 v4, 0x0

    move/from16 v0, p3

    int-to-long v10, v0

    invoke-direct {v2, v4, v5, v10, v11}, LX/2WF;-><init>(JJ)V

    .line 500724
    if-nez v7, :cond_0

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    .line 500725
    :goto_0
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z
    :try_end_2
    .catch LX/7Q6; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v2

    if-nez v2, :cond_2

    .line 500726
    :try_start_3
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2WF;

    move-object/from16 v2, p0

    move/from16 v3, p1

    move-object/from16 v4, p2

    move-object v6, v12

    move/from16 v9, p3

    .line 500727
    invoke-static/range {v2 .. v10}, LX/375;->a(LX/375;ILandroid/net/Uri;LX/2WF;LX/37C;LX/3Da;LX/3DW;ILjava/util/List;)LX/3Da;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v7

    goto :goto_1

    .line 500728
    :catchall_0
    move-exception v2

    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, LX/375;->d:LX/1Lv;

    iget-object v3, v3, LX/1Lv;->s:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2
    :try_end_4
    .catch LX/7Q6; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 500729
    :catch_0
    invoke-static {}, LX/1Lv;->b()Ljava/lang/String;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    .line 500730
    const/4 v2, 0x1

    .line 500731
    :goto_2
    return v2

    .line 500732
    :cond_0
    :try_start_5
    invoke-interface {v7}, LX/3Da;->g()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2WF;->a(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v10

    goto :goto_0

    .line 500733
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/375;->d:LX/1Lv;

    iget-object v2, v2, LX/1Lv;->o:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/0A1;

    move/from16 v0, p3

    int-to-long v14, v0

    invoke-virtual {v8}, LX/3DX;->a()J

    move-result-wide v17

    invoke-virtual/range {p4 .. p4}, LX/374;->a()LX/36s;

    move-result-object v2

    iget-object v0, v2, LX/36s;->a:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {p4 .. p4}, LX/374;->b()LX/1Li;

    move-result-object v2

    invoke-virtual {v2}, LX/1Li;->name()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v13, p2

    move-object/from16 v16, v10

    invoke-virtual/range {v12 .. v21}, LX/0A1;->a(Landroid/net/Uri;JLjava/util/List;JLjava/lang/String;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 500734
    invoke-interface {v7}, LX/3Da;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1, v2}, LX/375;->a(LX/374;Ljava/lang/String;)V

    .line 500735
    :cond_2
    const/4 v2, 0x1

    goto :goto_2

    .line 500736
    :catchall_1
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/375;->d:LX/1Lv;

    iget-object v3, v3, LX/1Lv;->o:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0A1;

    move/from16 v0, p3

    int-to-long v4, v0

    invoke-virtual {v8}, LX/3DX;->a()J

    move-result-wide v11

    invoke-virtual/range {p4 .. p4}, LX/374;->a()LX/36s;

    move-result-object v3

    iget-object v13, v3, LX/36s;->a:Ljava/lang/String;

    const/4 v14, 0x0

    invoke-virtual/range {p4 .. p4}, LX/374;->b()LX/1Li;

    move-result-object v3

    invoke-virtual {v3}, LX/1Li;->name()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v7, p2

    move-wide v8, v4

    invoke-virtual/range {v6 .. v15}, LX/0A1;->a(Landroid/net/Uri;JLjava/util/List;JLjava/lang/String;Ljava/lang/Exception;Ljava/lang/String;)V

    throw v2
    :try_end_5
    .catch LX/7Q6; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 500737
    :catch_1
    invoke-static {}, LX/1Lv;->b()Ljava/lang/String;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    .line 500738
    const/4 v2, 0x0

    goto :goto_2

    .line 500739
    :catch_2
    invoke-static {}, LX/1Lv;->b()Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 500740
    const/4 v2, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method public final declared-synchronized a(Z)LX/7Ou;
    .locals 2

    .prologue
    .line 500654
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/375;->e:Z

    .line 500655
    iget-object v0, p0, LX/375;->f:LX/3Dh;

    if-eqz v0, :cond_1

    .line 500656
    iget-object v0, p0, LX/375;->g:LX/3DV;

    if-eqz v0, :cond_0

    .line 500657
    iget-object v0, p0, LX/375;->g:LX/3DV;

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/3DV;->a(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 500658
    :cond_0
    iget-object v0, p0, LX/375;->f:LX/3Dh;

    invoke-virtual {v0, p1}, LX/3Dh;->a(Z)LX/7Ou;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 500659
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 500660
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 500647
    iget-object v0, p0, LX/375;->d:LX/1Lv;

    invoke-static {v0, p0}, LX/1Lv;->a$redex0(LX/1Lv;LX/375;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 500648
    const/4 v0, 0x1

    .line 500649
    :goto_0
    return v0

    .line 500650
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/375;->d:LX/1Lv;

    iget-object v0, v0, LX/1Lv;->u:LX/16V;

    new-instance v1, LX/36x;

    iget v2, p0, LX/375;->a:I

    iget-object v3, p0, LX/375;->b:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, LX/36x;-><init>(ILandroid/net/Uri;)V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 500651
    iget v0, p0, LX/375;->a:I

    iget-object v1, p0, LX/375;->c:LX/374;

    invoke-static {p0, v0, v1}, LX/375;->a(LX/375;ILX/374;)Z

    move-result v0

    .line 500652
    iget-object v1, p0, LX/375;->d:LX/1Lv;

    iget-object v1, v1, LX/1Lv;->u:LX/16V;

    new-instance v2, LX/36z;

    iget v3, p0, LX/375;->a:I

    invoke-direct {v2, v3}, LX/36z;-><init>(I)V

    invoke-virtual {v1, v2}, LX/16V;->a(LX/1AD;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 500653
    iget-object v1, p0, LX/375;->d:LX/1Lv;

    invoke-static {v1, p0}, LX/1Lv;->b$redex0(LX/1Lv;LX/375;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/375;->d:LX/1Lv;

    invoke-static {v1, p0}, LX/1Lv;->b$redex0(LX/1Lv;LX/375;)V

    throw v0
.end method
