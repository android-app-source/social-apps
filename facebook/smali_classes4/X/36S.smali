.class public LX/36S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1Uj;

.field private final b:Landroid/text/Spannable;

.field private final c:LX/1qb;

.field private final d:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/1qb;Landroid/content/res/Resources;LX/1Uj;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 498829
    iput-object p1, p0, LX/36S;->c:LX/1qb;

    .line 498830
    iput-object p2, p0, LX/36S;->d:Landroid/content/res/Resources;

    .line 498831
    iput-object p3, p0, LX/36S;->a:LX/1Uj;

    .line 498832
    iget-object v0, p0, LX/36S;->d:Landroid/content/res/Resources;

    const v1, 0x7f080f85

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, LX/36S;->b:Landroid/text/Spannable;

    .line 498833
    return-void
.end method

.method public static a(LX/0QB;)LX/36S;
    .locals 6

    .prologue
    .line 498834
    const-class v1, LX/36S;

    monitor-enter v1

    .line 498835
    :try_start_0
    sget-object v0, LX/36S;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498836
    sput-object v2, LX/36S;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498837
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498838
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498839
    new-instance p0, LX/36S;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v3

    check-cast v3, LX/1qb;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1Uj;->a(LX/0QB;)LX/1Uj;

    move-result-object v5

    check-cast v5, LX/1Uj;

    invoke-direct {p0, v3, v4, v5}, LX/36S;-><init>(LX/1qb;Landroid/content/res/Resources;LX/1Uj;)V

    .line 498840
    move-object v0, p0

    .line 498841
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498842
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/36S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498843
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1

    .prologue
    .line 498845
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 498846
    invoke-static {p0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 498847
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 498848
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 498849
    iget-object v1, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 498850
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/36U;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 498851
    invoke-static {v2}, LX/36S;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-static {v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLPage;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 498852
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 498853
    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLPage;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_0
    move-object v0, v1

    .line 498854
    :goto_1
    return-object v0

    .line 498855
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    const/4 p0, 0x0

    .line 498856
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->aA()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->aA()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->aA()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLBylineFragment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLBylineFragment;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 498857
    if-eqz v1, :cond_1

    .line 498858
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 498859
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aA()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLBylineFragment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLBylineFragment;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    move-object v0, v1

    .line 498860
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 498861
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    move v2, p0

    goto :goto_2
.end method

.method private static d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 498862
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/34N;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/34O;

    move-result-object v0

    sget-object v1, LX/34O;->PAGE_LIKE:LX/34O;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Landroid/text/Spannable;"
        }
    .end annotation

    .prologue
    .line 498863
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 498864
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 498865
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 498866
    iget-object v1, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 498867
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/36U;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 498868
    invoke-static {v2}, LX/36S;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-static {v2}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v2

    if-lez v2, :cond_0

    .line 498869
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v0

    .line 498870
    iget-object v1, p0, LX/36S;->d:Landroid/content/res/Resources;

    const v2, 0x7f0810d0

    const v3, 0x7f0810d1

    invoke-static {v1, v2, v3, v0}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 498871
    :goto_0
    return-object v0

    .line 498872
    :cond_0
    invoke-static {v0}, LX/1VO;->w(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 498873
    iget-object v0, p0, LX/36S;->b:Landroid/text/Spannable;

    goto :goto_0

    .line 498874
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 498875
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0

    .line 498876
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
