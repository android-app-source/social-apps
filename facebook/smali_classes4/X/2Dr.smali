.class public LX/2Dr;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/280;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/27v;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Sh;

.field private final f:LX/0TD;

.field public final g:Landroid/content/Context;

.field public final h:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 384733
    const-class v0, LX/2Dr;

    sput-object v0, LX/2Dr;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Sh;LX/0TD;Landroid/content/Context;LX/0SG;)V
    .locals 0
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/280;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/27v;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0TD;",
            "Landroid/content/Context;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384725
    iput-object p1, p0, LX/2Dr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 384726
    iput-object p2, p0, LX/2Dr;->c:LX/0Ot;

    .line 384727
    iput-object p3, p0, LX/2Dr;->d:LX/0Ot;

    .line 384728
    iput-object p4, p0, LX/2Dr;->e:LX/0Sh;

    .line 384729
    iput-object p5, p0, LX/2Dr;->f:LX/0TD;

    .line 384730
    iput-object p6, p0, LX/2Dr;->g:Landroid/content/Context;

    .line 384731
    iput-object p7, p0, LX/2Dr;->h:LX/0SG;

    .line 384732
    return-void
.end method

.method public static b(LX/0QB;)LX/2Dr;
    .locals 8

    .prologue
    .line 384721
    new-instance v0, LX/2Dr;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v2, 0xf18

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x4c6

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    const-class v6, Landroid/content/Context;

    invoke-interface {p0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-direct/range {v0 .. v7}, LX/2Dr;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Sh;LX/0TD;Landroid/content/Context;LX/0SG;)V

    .line 384722
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/4hA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 384723
    iget-object v0, p0, LX/2Dr;->f:LX/0TD;

    new-instance v1, LX/49f;

    invoke-direct {v1, p0, p1}, LX/49f;-><init>(LX/2Dr;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
