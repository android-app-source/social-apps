.class public LX/3H7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/3H8;

.field private static s:LX/0Xm;


# instance fields
.field private final b:LX/0pf;

.field private final c:LX/0sZ;

.field public final d:LX/3H9;

.field private final e:LX/3HA;

.field private final f:LX/3HD;

.field private final g:LX/3HB;

.field private final h:LX/3HC;

.field private final i:LX/3HM;

.field public final j:LX/0tX;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/1nK;

.field private final m:LX/3HP;

.field private final n:LX/2U5;

.field public final o:LX/20j;

.field private final p:LX/0ad;

.field private final q:LX/0tO;

.field private r:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 542919
    new-instance v0, LX/3H8;

    invoke-direct {v0}, LX/3H8;-><init>()V

    sput-object v0, LX/3H7;->a:LX/3H8;

    return-void
.end method

.method public constructor <init>(LX/0sZ;LX/3H9;LX/3HA;LX/3HB;LX/3HC;LX/3HD;LX/3HM;LX/0tX;LX/0Ot;LX/1nK;LX/3HP;LX/2U5;LX/20j;LX/0pf;LX/0ad;LX/0tO;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sZ;",
            "LX/3H9;",
            "LX/3HA;",
            "LX/3HB;",
            "LX/3HC;",
            "LX/3HD;",
            "LX/3HM;",
            "LX/0tX;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;",
            "LX/1nK;",
            "LX/3HP;",
            "LX/2U5;",
            "LX/20j;",
            "LX/0pf;",
            "LX/0ad;",
            "LX/0tO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 542940
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 542941
    const/4 v1, 0x0

    iput v1, p0, LX/3H7;->r:I

    .line 542942
    iput-object p1, p0, LX/3H7;->c:LX/0sZ;

    .line 542943
    iput-object p2, p0, LX/3H7;->d:LX/3H9;

    .line 542944
    iput-object p3, p0, LX/3H7;->e:LX/3HA;

    .line 542945
    iput-object p4, p0, LX/3H7;->g:LX/3HB;

    .line 542946
    iput-object p8, p0, LX/3H7;->j:LX/0tX;

    .line 542947
    iput-object p9, p0, LX/3H7;->k:LX/0Ot;

    .line 542948
    iput-object p10, p0, LX/3H7;->l:LX/1nK;

    .line 542949
    iput-object p5, p0, LX/3H7;->h:LX/3HC;

    .line 542950
    iput-object p6, p0, LX/3H7;->f:LX/3HD;

    .line 542951
    iput-object p7, p0, LX/3H7;->i:LX/3HM;

    .line 542952
    iput-object p11, p0, LX/3H7;->m:LX/3HP;

    .line 542953
    iput-object p12, p0, LX/3H7;->n:LX/2U5;

    .line 542954
    iput-object p13, p0, LX/3H7;->o:LX/20j;

    .line 542955
    move-object/from16 v0, p14

    iput-object v0, p0, LX/3H7;->b:LX/0pf;

    .line 542956
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3H7;->p:LX/0ad;

    .line 542957
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3H7;->q:LX/0tO;

    .line 542958
    return-void
.end method

.method private static a(LX/3H7;Ljava/lang/String;LX/21y;LX/0rS;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/21y;",
            "LX/0rS;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542932
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542933
    const/4 v0, 0x0

    .line 542934
    :goto_0
    return-object v0

    .line 542935
    :cond_0
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;

    const/16 v2, 0x19

    sget-object v3, LX/5H2;->COMPLETE:LX/5H2;

    move-object v1, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;-><init>(Ljava/lang/String;ILX/5H2;LX/0rS;LX/21y;)V

    .line 542936
    iget-object v1, p0, LX/3H7;->f:LX/3HD;

    iget-object v2, p0, LX/3H7;->m:LX/3HP;

    invoke-virtual {v1, v0, v2}, LX/3HD;->a(Lcom/facebook/api/ufiservices/common/FetchSingleMediaParams;LX/3HP;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 542937
    iput-object p5, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 542938
    move-object v0, v0

    .line 542939
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/3H7;
    .locals 3

    .prologue
    .line 542924
    const-class v1, LX/3H7;

    monitor-enter v1

    .line 542925
    :try_start_0
    sget-object v0, LX/3H7;->s:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 542926
    sput-object v2, LX/3H7;->s:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 542927
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542928
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/3H7;->b(LX/0QB;)LX/3H7;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 542929
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3H7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 542930
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 542931
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLComment;LX/0rS;)Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;
    .locals 6

    .prologue
    .line 542923
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    sget-object v4, LX/21y;->DEFAULT_ORDER:LX/21y;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->A()Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLInterestingRepliesConnection;->j()LX/0Px;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v5

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;-><init>(Ljava/lang/String;ILX/0rS;LX/21y;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LX/5Go;LX/0rS;LX/21y;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/5Go;",
            "LX/0rS;",
            "LX/21y;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 542922
    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    iget-object v0, p0, LX/3H7;->q:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->b()Z

    move-result v10

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    invoke-virtual/range {v0 .. v10}, LX/3H7;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5Go;LX/0rS;Ljava/lang/String;LX/21y;ZZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/3H7;
    .locals 18

    .prologue
    .line 542920
    new-instance v1, LX/3H7;

    invoke-static/range {p0 .. p0}, LX/0sZ;->a(LX/0QB;)LX/0sZ;

    move-result-object v2

    check-cast v2, LX/0sZ;

    invoke-static/range {p0 .. p0}, LX/3H9;->a(LX/0QB;)LX/3H9;

    move-result-object v3

    check-cast v3, LX/3H9;

    invoke-static/range {p0 .. p0}, LX/3HA;->a(LX/0QB;)LX/3HA;

    move-result-object v4

    check-cast v4, LX/3HA;

    invoke-static/range {p0 .. p0}, LX/3HB;->a(LX/0QB;)LX/3HB;

    move-result-object v5

    check-cast v5, LX/3HB;

    invoke-static/range {p0 .. p0}, LX/3HC;->a(LX/0QB;)LX/3HC;

    move-result-object v6

    check-cast v6, LX/3HC;

    invoke-static/range {p0 .. p0}, LX/3HD;->a(LX/0QB;)LX/3HD;

    move-result-object v7

    check-cast v7, LX/3HD;

    invoke-static/range {p0 .. p0}, LX/3HM;->a(LX/0QB;)LX/3HM;

    move-result-object v8

    check-cast v8, LX/3HM;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    const/16 v10, 0xafc

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/1nK;->a(LX/0QB;)LX/1nK;

    move-result-object v11

    check-cast v11, LX/1nK;

    invoke-static/range {p0 .. p0}, LX/3HP;->a(LX/0QB;)LX/3HP;

    move-result-object v12

    check-cast v12, LX/3HP;

    const-class v13, LX/2U5;

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/2U5;

    invoke-static/range {p0 .. p0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v14

    check-cast v14, LX/20j;

    invoke-static/range {p0 .. p0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v15

    check-cast v15, LX/0pf;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v16

    check-cast v16, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v17

    check-cast v17, LX/0tO;

    invoke-direct/range {v1 .. v17}, LX/3H7;-><init>(LX/0sZ;LX/3H9;LX/3HA;LX/3HB;LX/3HC;LX/3HD;LX/3HM;LX/0tX;LX/0Ot;LX/1nK;LX/3HP;LX/2U5;LX/20j;LX/0pf;LX/0ad;LX/0tO;)V

    .line 542921
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/5Go;Ljava/lang/String;LX/21y;Z)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 542752
    new-instance v0, Lcom/facebook/api/story/FetchSingleStoryParams;

    sget-object v2, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    const/16 v4, 0x19

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move/from16 v9, p5

    invoke-direct/range {v0 .. v11}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;ILjava/lang/String;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 542753
    iget-object v1, p0, LX/3H7;->i:LX/3HM;

    invoke-virtual {v1, v0}, LX/3HM;->a(Lcom/facebook/api/story/FetchSingleStoryParams;)LX/0zO;

    move-result-object v0

    .line 542754
    const/4 v1, 0x0

    .line 542755
    :try_start_0
    iget-object v2, p0, LX/3H7;->j:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->b(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 542756
    :goto_0
    if-eqz v0, :cond_0

    .line 542757
    invoke-virtual {v0}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 542758
    :goto_1
    return-object v0

    .line 542759
    :catch_0
    move-exception v0

    .line 542760
    const-class v2, LX/3H7;

    const-string v3, "Feedback cache query failed"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    .line 542761
    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542901
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 542902
    new-instance v3, LX/8pi;

    invoke-direct {v3, p0, p1, v2}, LX/8pi;-><init>(LX/3H7;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 542903
    const-string v0, "FetchInterestingReplies"

    invoke-static {v0}, LX/2U5;->a(Ljava/lang/String;)LX/0v6;

    move-result-object v4

    .line 542904
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 542905
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v7, :cond_1

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 542906
    invoke-static {v0}, LX/36l;->g(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 542907
    sget-object v8, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-static {v0, v8}, LX/3H7;->a(Lcom/facebook/graphql/model/GraphQLComment;LX/0rS;)Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    move-result-object v0

    .line 542908
    iget-object v8, p0, LX/3H7;->c:LX/0sZ;

    invoke-virtual {v8, v0}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0zO;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542909
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 542910
    :cond_1
    iget-object v0, v4, LX/0v6;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    move v0, v0

    .line 542911
    if-nez v0, :cond_2

    .line 542912
    const v0, -0x4e711715

    invoke-static {v2, p1, v0}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 542913
    :goto_1
    return-object v2

    .line 542914
    :cond_2
    new-instance v0, LX/0zX;

    new-instance v1, LX/4VA;

    invoke-direct {v1}, LX/4VA;-><init>()V

    invoke-static {v5, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v1

    .line 542915
    invoke-static {v1}, LX/0v9;->b(Ljava/lang/Iterable;)LX/0v9;

    move-result-object v5

    invoke-static {v5}, LX/0v9;->e(LX/0v9;)LX/0v9;

    move-result-object v5

    move-object v1, v5

    .line 542916
    invoke-direct {v0, v1}, LX/0zX;-><init>(LX/0v9;)V

    move-object v0, v0

    .line 542917
    invoke-virtual {v0, v3}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 542918
    iget-object v0, p0, LX/3H7;->j:LX/0tX;

    invoke-virtual {v0, v4}, LX/0tX;->a(LX/0v6;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/0rS;LX/21y;ZLjava/lang/String;Ljava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0rS;",
            "LX/21y;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542900
    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v9}, LX/3H7;->a(Ljava/lang/String;LX/0rS;LX/21y;ZLjava/lang/String;Ljava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/0rS;LX/21y;ZLjava/lang/String;Ljava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13
    .param p9    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0rS;",
            "LX/21y;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542959
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 542960
    const/4 v1, 0x0

    .line 542961
    :goto_0
    return-object v1

    .line 542962
    :cond_0
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne p2, v1, :cond_1

    .line 542963
    iget-object v1, p0, LX/3H7;->l:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->b()V

    .line 542964
    :cond_1
    iget-object v1, p0, LX/3H7;->b:LX/0pf;

    invoke-virtual {v1, p1}, LX/0pf;->a(Ljava/lang/String;)LX/1g0;

    move-result-object v1

    .line 542965
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/1g0;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    if-nez v1, :cond_5

    :cond_2
    const/4 v12, 0x1

    .line 542966
    :goto_1
    new-instance v1, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    const/16 v3, 0x19

    sget-object v4, LX/21x;->COMPLETE:LX/21x;

    const/4 v9, 0x0

    move-object v2, p1

    move-object v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v10, p7

    move/from16 v11, p4

    invoke-direct/range {v1 .. v12}, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;-><init>(Ljava/lang/String;ILX/21x;LX/0rS;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    .line 542967
    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne p2, v2, :cond_3

    .line 542968
    iget-object v2, p0, LX/3H7;->l:LX/1nK;

    invoke-virtual {v2}, LX/1nK;->c()V

    .line 542969
    :cond_3
    iget-object v2, p0, LX/3H7;->c:LX/0sZ;

    invoke-virtual {v2, v1}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0zO;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, LX/0zO;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v1

    .line 542970
    if-eqz p9, :cond_4

    .line 542971
    move-object/from16 v0, p9

    invoke-virtual {v1, v0}, LX/0zO;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/0zO;

    .line 542972
    :cond_4
    iget-object v2, p0, LX/3H7;->j:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    invoke-static {v1}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0

    .line 542973
    :cond_5
    const/4 v12, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/0rS;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0rS;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542885
    sget-object v2, LX/21y;->DEFAULT_ORDER:LX/21y;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/3H7;->a(LX/3H7;Ljava/lang/String;LX/21y;LX/0rS;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    .line 542886
    if-nez v0, :cond_0

    .line 542887
    const/4 v0, 0x0

    .line 542888
    :goto_0
    return-object v0

    .line 542889
    :cond_0
    iget-object v1, p0, LX/3H7;->l:LX/1nK;

    .line 542890
    iget-object v2, v1, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v3, 0x390011

    const-string v4, "Flyout_NetworkTimePhotoId"

    invoke-interface {v2, v3, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 542891
    iget-object v1, p0, LX/3H7;->j:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 542892
    if-nez v0, :cond_1

    .line 542893
    const/4 v1, 0x0

    .line 542894
    :goto_1
    move-object v0, v1

    .line 542895
    goto :goto_0

    :cond_1
    sget-object v1, LX/0tX;->c:LX/0QK;

    .line 542896
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 542897
    iget-object v2, v0, LX/1Zp;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 542898
    new-instance v3, LX/1Zp;

    iget-object v4, v0, LX/1Zp;->b:LX/0zO;

    invoke-direct {v3, v2, v4}, LX/1Zp;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0zO;)V

    move-object v1, v3

    .line 542899
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/1zt;Ljava/lang/String;ILX/0rS;Lcom/facebook/common/callercontext/CallerContext;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/1zt;",
            "Ljava/lang/String;",
            "I",
            "LX/0rS;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542877
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move-object v6, p3

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;-><init>(Ljava/lang/String;LX/1zt;ZILjava/lang/String;Ljava/lang/String;LX/0rS;)V

    .line 542878
    iget-object v1, p0, LX/3H7;->e:LX/3HA;

    invoke-static {}, LX/5Cb;->a()LX/5Ca;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p6}, LX/3HA;->a(LX/0gW;Lcom/facebook/api/ufiservices/common/FetchReactionsParams;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 542879
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 542880
    move-object v1, v0

    .line 542881
    iget-object v0, p0, LX/3H7;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 542882
    iget-object v3, v1, LX/0zO;->m:LX/0gW;

    move-object v3, v3

    .line 542883
    iget-object v4, v3, LX/0gW;->f:Ljava/lang/String;

    move-object v3, v4

    .line 542884
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, LX/3H7;->r:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/3H7;->r:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p7, v2}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/1zt;Ljava/lang/String;ILX/0rS;Lcom/facebook/common/callercontext/CallerContext;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/1zt;",
            "Ljava/lang/String;",
            "I",
            "LX/0rS;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542875
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move-object v6, p3

    move-object v7, p5

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/facebook/api/ufiservices/common/FetchReactionsParams;-><init>(Ljava/lang/String;LX/1zt;ZILjava/lang/String;Ljava/lang/String;LX/0rS;Z)V

    .line 542876
    iget-object v1, p0, LX/3H7;->j:LX/0tX;

    iget-object v2, p0, LX/3H7;->e:LX/3HA;

    invoke-static {}, LX/5Cb;->a()LX/5Ca;

    move-result-object v3

    invoke-virtual {v2, v3, v0, p6}, LX/3HA;->a(LX/0gW;Lcom/facebook/api/ufiservices/common/FetchReactionsParams;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/21y;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/21y;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542873
    sget-object v3, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/3H7;->a(LX/3H7;Ljava/lang/String;LX/21y;LX/0rS;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    .line 542874
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/3H7;->j:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    sget-object v1, LX/3H7;->a:LX/3H8;

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/5Go;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/5Go;",
            "LX/0rS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 542872
    const/4 v2, 0x0

    sget-object v5, LX/21y;->DEFAULT_ORDER:LX/21y;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, LX/3H7;->a(Ljava/lang/String;Ljava/lang/String;LX/5Go;LX/0rS;LX/21y;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/21y;LX/5Gs;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/21y;",
            "LX/5Gs;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 542838
    sget-object v1, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    if-ne p4, v1, :cond_0

    move-object v3, p2

    .line 542839
    :goto_0
    sget-object v1, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    if-ne p4, v1, :cond_1

    move-object v4, v0

    .line 542840
    :goto_1
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;

    sget-object v5, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    move-object v1, p1

    move v2, p5

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0rS;LX/21y;LX/5Gs;)V

    .line 542841
    iget-object v1, v0, Lcom/facebook/api/ufiservices/common/FetchCommentsParams;->b:LX/5Gs;

    move-object v1, v1

    .line 542842
    sget-object v2, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    if-ne v1, v2, :cond_2

    .line 542843
    iget-object v1, p0, LX/3H7;->j:LX/0tX;

    iget-object v2, p0, LX/3H7;->g:LX/3HB;

    .line 542844
    new-instance v3, LX/5Ax;

    invoke-direct {v3}, LX/5Ax;-><init>()V

    move-object v3, v3

    .line 542845
    invoke-static {v2, v0, v3}, LX/3HB;->a(LX/3HB;Lcom/facebook/api/ufiservices/common/FetchCommentsParams;LX/0gW;)V

    .line 542846
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 542847
    iget-object v4, v0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->e:LX/0rS;

    move-object v4, v4

    .line 542848
    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v4, v5, :cond_3

    .line 542849
    sget-object v4, LX/0zS;->d:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 542850
    :goto_2
    iget-object v4, v2, LX/3HB;->a:LX/0sZ;

    invoke-virtual {v4, v0, v3}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchNodeListParams;LX/0zO;)LX/1kt;

    move-result-object v4

    .line 542851
    iput-object v4, v3, LX/0zO;->g:LX/1kt;

    .line 542852
    move-object v0, v3

    .line 542853
    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 542854
    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/8pg;

    invoke-direct {v1, p0}, LX/8pg;-><init>(LX/3H7;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 542855
    :goto_3
    return-object v0

    :cond_0
    move-object v3, v0

    .line 542856
    goto :goto_0

    :cond_1
    move-object v4, p2

    .line 542857
    goto :goto_1

    .line 542858
    :cond_2
    iget-object v1, p0, LX/3H7;->j:LX/0tX;

    iget-object v2, p0, LX/3H7;->g:LX/3HB;

    .line 542859
    new-instance v3, LX/5Ay;

    invoke-direct {v3}, LX/5Ay;-><init>()V

    move-object v3, v3

    .line 542860
    invoke-static {v2, v0, v3}, LX/3HB;->a(LX/3HB;Lcom/facebook/api/ufiservices/common/FetchCommentsParams;LX/0gW;)V

    .line 542861
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 542862
    iget-object v4, v0, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->e:LX/0rS;

    move-object v4, v4

    .line 542863
    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v4, v5, :cond_4

    .line 542864
    sget-object v4, LX/0zS;->d:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 542865
    :goto_4
    iget-object v4, v2, LX/3HB;->a:LX/0sZ;

    invoke-virtual {v4, v0, v3}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchNodeListParams;LX/0zO;)LX/1kt;

    move-result-object v4

    .line 542866
    iput-object v4, v3, LX/0zO;->g:LX/1kt;

    .line 542867
    move-object v0, v3

    .line 542868
    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 542869
    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/8ph;

    invoke-direct {v1, p0}, LX/8ph;-><init>(LX/3H7;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_3

    .line 542870
    :cond_3
    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    goto :goto_2

    .line 542871
    :cond_4
    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    goto :goto_4
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/5Go;LX/0rS;Ljava/lang/String;LX/21y;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/5Go;",
            "LX/0rS;",
            "Ljava/lang/String;",
            "LX/21y;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 542837
    const/4 v3, 0x0

    const/4 v9, 0x0

    iget-object v0, p0, LX/3H7;->q:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->b()Z

    move-result v10

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v0 .. v10}, LX/3H7;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5Go;LX/0rS;Ljava/lang/String;LX/21y;ZZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5Go;LX/0rS;Ljava/lang/String;LX/21y;ZZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/5Go;",
            "LX/0rS;",
            "Ljava/lang/String;",
            "LX/21y;",
            "ZZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 542834
    new-instance v1, Lcom/facebook/api/story/FetchSingleStoryParams;

    const/16 v5, 0x19

    move-object v2, p1

    move-object/from16 v3, p5

    move-object/from16 v4, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object v8, p2

    move-object/from16 v9, p3

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    invoke-direct/range {v1 .. v12}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;ILjava/lang/String;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 542835
    iget-object v2, p0, LX/3H7;->i:LX/3HM;

    iget-object v3, p0, LX/3H7;->m:LX/3HP;

    move-object/from16 v0, p6

    invoke-virtual {v2, v1, v0, v3}, LX/3HM;->a(Lcom/facebook/api/story/FetchSingleStoryParams;Ljava/lang/String;LX/3HP;)LX/0zO;

    move-result-object v1

    .line 542836
    iget-object v2, p0, LX/3H7;->j:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542776
    new-instance v0, LX/6BM;

    invoke-direct {v0}, LX/6BM;-><init>()V

    .line 542777
    iput-object p2, v0, LX/6BM;->a:Ljava/lang/String;

    .line 542778
    move-object v0, v0

    .line 542779
    iput-object p3, v0, LX/6BM;->g:Ljava/lang/String;

    .line 542780
    move-object v0, v0

    .line 542781
    iput-object p1, v0, LX/6BM;->b:Ljava/lang/String;

    .line 542782
    move-object v0, v0

    .line 542783
    const/16 v1, 0x19

    .line 542784
    iput v1, v0, LX/6BM;->c:I

    .line 542785
    move-object v0, v0

    .line 542786
    iput-boolean p4, v0, LX/6BM;->f:Z

    .line 542787
    move-object v0, v0

    .line 542788
    iput-boolean p5, v0, LX/6BM;->k:Z

    .line 542789
    move-object v0, v0

    .line 542790
    invoke-virtual {v0}, LX/6BM;->a()Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    move-result-object v0

    .line 542791
    iget-object v1, p0, LX/3H7;->j:LX/0tX;

    iget-object v2, p0, LX/3H7;->h:LX/3HC;

    .line 542792
    new-instance p0, LX/5BD;

    invoke-direct {p0}, LX/5BD;-><init>()V

    move-object p0, p0

    .line 542793
    const-string p1, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object p1

    const-string p2, "likers_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 542794
    const-string p1, "comment_id"

    .line 542795
    iget-object p2, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->a:Ljava/lang/String;

    move-object p2, p2

    .line 542796
    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 542797
    const-string p1, "angora_attachment_cover_image_size"

    iget-object p2, v2, LX/3HC;->a:LX/0sa;

    invoke-virtual {p2}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 542798
    const-string p1, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 542799
    const-string p1, "reading_attachment_profile_image_width"

    iget-object p2, v2, LX/3HC;->a:LX/0sa;

    invoke-virtual {p2}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 542800
    const-string p1, "reading_attachment_profile_image_height"

    iget-object p2, v2, LX/3HC;->a:LX/0sa;

    invoke-virtual {p2}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 542801
    const-string p1, "include_permalink_title"

    .line 542802
    iget-boolean p2, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->f:Z

    move p2, p2

    .line 542803
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 542804
    const-string p1, "include_comments_disabled_fields"

    .line 542805
    iget-boolean p2, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->k:Z

    move p2, p2

    .line 542806
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 542807
    const-string p1, "include_replies_in_total_count"

    iget-object p2, v2, LX/3HC;->c:LX/0ad;

    sget-short p3, LX/0wg;->i:S

    const/4 p4, 0x0

    invoke-interface {p2, p3, p4}, LX/0ad;->a(SZ)Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 542808
    const-string p1, "automatic_photo_captioning_enabled"

    iget-object p2, v2, LX/3HC;->f:LX/0sX;

    invoke-virtual {p2}, LX/0sX;->a()Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 542809
    iget-object p1, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->g:Ljava/lang/String;

    move-object p1, p1

    .line 542810
    if-eqz p1, :cond_1

    .line 542811
    const-string p1, "surround_comment_id"

    .line 542812
    iget-object p2, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->g:Ljava/lang/String;

    move-object p2, p2

    .line 542813
    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    const-string p2, "num_before_surround"

    .line 542814
    iget p3, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->c:I

    move p3, p3

    .line 542815
    add-int/lit8 p3, p3, -0x1

    div-int/lit8 p3, p3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object p1

    const-string p2, "surround_max_comments"

    .line 542816
    iget p3, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->c:I

    move p3, p3

    .line 542817
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 542818
    :goto_0
    iget-object p1, v2, LX/3HC;->e:LX/0sU;

    invoke-virtual {p1, p0}, LX/0sU;->a(LX/0gW;)V

    .line 542819
    iget-object p1, v2, LX/3HC;->d:LX/0tG;

    invoke-virtual {p1, p0}, LX/0tG;->a(LX/0gW;)V

    .line 542820
    iget-object p1, v2, LX/3HC;->g:LX/0tE;

    invoke-virtual {p1, p0}, LX/0tE;->b(LX/0gW;)V

    .line 542821
    iget-object p1, v2, LX/3HC;->b:LX/0se;

    invoke-virtual {p1, p0}, LX/0se;->a(LX/0gW;)LX/0gW;

    move-result-object p0

    move-object p0, p0

    .line 542822
    const-class p1, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {p1}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object p1

    move-object p1, p1

    .line 542823
    invoke-static {p0, p1}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object p0

    .line 542824
    sget-object p1, LX/0zS;->d:LX/0zS;

    invoke-virtual {p0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 542825
    iget-object p1, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->b:Ljava/lang/String;

    move-object p1, p1

    .line 542826
    if-eqz p1, :cond_0

    .line 542827
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    .line 542828
    iput-object p1, p0, LX/0zO;->d:Ljava/util/Set;

    .line 542829
    :cond_0
    move-object v0, p0

    .line 542830
    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 542831
    :cond_1
    const-string p1, "max_comments"

    .line 542832
    iget p2, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->c:I

    move p2, p2

    .line 542833
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542762
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 542763
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 542764
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 542765
    invoke-static {v0}, LX/36l;->g(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 542766
    sget-object v6, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    invoke-static {v0, v6}, LX/3H7;->a(Lcom/facebook/graphql/model/GraphQLComment;LX/0rS;)Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    move-result-object v0

    .line 542767
    iget-object v6, p0, LX/3H7;->j:LX/0tX;

    iget-object v7, p0, LX/3H7;->c:LX/0sZ;

    invoke-virtual {v7, v0}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0zO;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542768
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 542769
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 542770
    const v0, -0x31481930

    invoke-static {v2, p1, v0}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 542771
    :goto_1
    return-object v2

    .line 542772
    :cond_2
    invoke-static {v3}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 542773
    new-instance v1, LX/8pj;

    invoke-direct {v1, p0, p1, v2}, LX/8pj;-><init>(LX/3H7;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 542774
    sget-object v3, LX/131;->INSTANCE:LX/131;

    move-object v3, v3

    .line 542775
    invoke-static {v0, v1, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method
