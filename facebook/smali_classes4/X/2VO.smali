.class public LX/2VO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 417639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0lF;Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;)Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 417640
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 417641
    :cond_0
    :goto_0
    return-object v0

    .line 417642
    :cond_1
    const-string v1, "data"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 417643
    if-eqz v5, :cond_0

    .line 417644
    invoke-virtual {v5}, LX/0lF;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 417645
    invoke-virtual {v5}, LX/0lF;->C()I

    move-result v1

    if-nez v1, :cond_0

    .line 417646
    new-instance v0, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;

    .line 417647
    iget-object v1, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 417648
    const-string v2, "local_default_group"

    const-string v5, ""

    .line 417649
    sget-object v4, LX/0Rg;->a:LX/0Rg;

    move-object v6, v4

    .line 417650
    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;LX/0P1;)V

    goto :goto_0

    .line 417651
    :cond_2
    invoke-virtual {v5}, LX/0lF;->e()I

    move-result v1

    if-eqz v1, :cond_0

    .line 417652
    invoke-virtual {v5, v3}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    const-string v1, "group"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v2

    .line 417653
    if-nez v2, :cond_3

    .line 417654
    const-string v2, "local_default_group"

    .line 417655
    :cond_3
    invoke-virtual {v5, v3}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    const-string v1, "params"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 417656
    new-instance v0, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;

    .line 417657
    iget-object v1, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 417658
    invoke-virtual {v5, v3}, LX/0lF;->a(I)LX/0lF;

    move-result-object v4

    const-string v7, "in_experiment"

    invoke-virtual {v4, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->u()Z

    move-result v7

    invoke-virtual {v5, v3}, LX/0lF;->a(I)LX/0lF;

    move-result-object v4

    const-string v8, "in_deploy_group"

    invoke-virtual {v4, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->u()Z

    move-result v4

    invoke-virtual {v5, v3}, LX/0lF;->a(I)LX/0lF;

    move-result-object v3

    const-string v5, "hash"

    invoke-virtual {v3, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-virtual {v3}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v5

    .line 417659
    new-instance v9, LX/0P2;

    invoke-direct {v9}, LX/0P2;-><init>()V

    .line 417660
    invoke-virtual {v6}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v10

    .line 417661
    :cond_4
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 417662
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 417663
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0lF;

    const-string p0, "type"

    invoke-virtual {v8, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    .line 417664
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/0lF;->C()I

    move-result p0

    const/4 p1, 0x1

    if-eq p0, p1, :cond_5

    invoke-virtual {v8}, LX/0lF;->C()I

    move-result v8

    const/4 p0, 0x2

    if-ne v8, p0, :cond_4

    .line 417665
    :cond_5
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lF;

    const-string p0, "value"

    invoke-virtual {v3, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-virtual {v3}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v8, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 417666
    :cond_6
    invoke-virtual {v9}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    move-object v6, v3

    .line 417667
    move v3, v7

    invoke-direct/range {v0 .. v6}, Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;LX/0P1;)V

    goto/16 :goto_0
.end method
