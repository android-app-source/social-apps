.class public LX/2tD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2tD;


# instance fields
.field public a:LX/0cG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0cG",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 474821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474822
    new-instance v0, LX/0cG;

    invoke-direct {v0}, LX/0cG;-><init>()V

    iput-object v0, p0, LX/2tD;->a:LX/0cG;

    .line 474823
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/events"

    sget-object v2, LX/0ax;->cl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474824
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/chat"

    sget-object v2, LX/0ax;->ae:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474825
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/friends"

    sget-object v2, LX/0ax;->dW:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474826
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/inbox"

    sget-object v2, LX/0ax;->ae:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474827
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/newsfeed"

    sget-object v2, LX/0ax;->cL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474828
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/requests"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "requests"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474829
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/wall?user={user}"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "profile/<user>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474830
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/wall"

    sget-object v2, LX/0ax;->dg:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474831
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/info?user={user}"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "profile/<user>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474832
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/notifications"

    sget-object v2, LX/0ax;->dc:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474833
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/feedback?user={uid}&post={post_id}"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "post/<post_id>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474834
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/photos?user={uid}&album={aid}&photo={pid}"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "native_album/<aid>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474835
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/photos?user={uid}&album={aid}"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "native_album/<aid>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474836
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/photos?user={uid}&photo={pid}"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "albums/<uid>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474837
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/photos?user={uid}"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "albums/<uid>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474838
    iget-object v0, p0, LX/2tD;->a:LX/0cG;

    const-string v1, "facebook:/photos"

    sget-object v2, LX/0ax;->bY:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474839
    return-void
.end method

.method public static a(LX/0QB;)LX/2tD;
    .locals 3

    .prologue
    .line 474840
    sget-object v0, LX/2tD;->b:LX/2tD;

    if-nez v0, :cond_1

    .line 474841
    const-class v1, LX/2tD;

    monitor-enter v1

    .line 474842
    :try_start_0
    sget-object v0, LX/2tD;->b:LX/2tD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 474843
    if-eqz v2, :cond_0

    .line 474844
    :try_start_1
    new-instance v0, LX/2tD;

    invoke-direct {v0}, LX/2tD;-><init>()V

    .line 474845
    move-object v0, v0

    .line 474846
    sput-object v0, LX/2tD;->b:LX/2tD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 474847
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 474848
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 474849
    :cond_1
    sget-object v0, LX/2tD;->b:LX/2tD;

    return-object v0

    .line 474850
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 474851
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
