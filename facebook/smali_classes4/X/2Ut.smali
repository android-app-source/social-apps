.class public LX/2Ut;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;

.field private static volatile m:LX/2Ut;


# instance fields
.field public c:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/2Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMB;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3MF;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNO;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 416871
    sget-object v0, LX/2Uo;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "simple"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/2Ut;->a:Landroid/net/Uri;

    .line 416872
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "recipient_ids"

    aput-object v2, v0, v1

    sput-object v0, LX/2Ut;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 416859
    const-string v0, "SMS_LOCAL_SPAM_AND_RANKING_UPDATE"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 416860
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416861
    iput-object v0, p0, LX/2Ut;->g:LX/0Ot;

    .line 416862
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416863
    iput-object v0, p0, LX/2Ut;->i:LX/0Ot;

    .line 416864
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416865
    iput-object v0, p0, LX/2Ut;->j:LX/0Ot;

    .line 416866
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416867
    iput-object v0, p0, LX/2Ut;->k:LX/0Ot;

    .line 416868
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416869
    iput-object v0, p0, LX/2Ut;->l:LX/0Ot;

    .line 416870
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ut;
    .locals 13

    .prologue
    .line 416844
    sget-object v0, LX/2Ut;->m:LX/2Ut;

    if-nez v0, :cond_1

    .line 416845
    const-class v1, LX/2Ut;

    monitor-enter v1

    .line 416846
    :try_start_0
    sget-object v0, LX/2Ut;->m:LX/2Ut;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416847
    if-eqz v2, :cond_0

    .line 416848
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416849
    new-instance v3, LX/2Ut;

    invoke-direct {v3}, LX/2Ut;-><init>()V

    .line 416850
    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v6

    check-cast v6, LX/2Or;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    const/16 v8, 0x140f

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v9

    check-cast v9, LX/0Xl;

    const/16 v10, 0x297c

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xdac

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x29b2

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 p0, 0xd9e

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 416851
    iput-object v4, v3, LX/2Ut;->c:Landroid/content/Context;

    iput-object v5, v3, LX/2Ut;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v6, v3, LX/2Ut;->e:LX/2Or;

    iput-object v7, v3, LX/2Ut;->f:LX/0SG;

    iput-object v8, v3, LX/2Ut;->g:LX/0Ot;

    iput-object v9, v3, LX/2Ut;->h:LX/0Xl;

    iput-object v10, v3, LX/2Ut;->i:LX/0Ot;

    iput-object v11, v3, LX/2Ut;->j:LX/0Ot;

    iput-object v12, v3, LX/2Ut;->k:LX/0Ot;

    iput-object p0, v3, LX/2Ut;->l:LX/0Ot;

    .line 416852
    move-object v0, v3

    .line 416853
    sput-object v0, LX/2Ut;->m:LX/2Ut;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416854
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416855
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416856
    :cond_1
    sget-object v0, LX/2Ut;->m:LX/2Ut;

    return-object v0

    .line 416857
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416858
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static k(LX/2Ut;)V
    .locals 15

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 416806
    iget-object v0, p0, LX/2Ut;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide v2, 0x1cf7c5800L

    sub-long/2addr v0, v2

    .line 416807
    const/4 v2, 0x2

    new-array v2, v2, [LX/0ux;

    const-string v3, "message_count"

    const-string v4, "0"

    invoke-static {v3, v4}, LX/0uu;->e(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "date"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, LX/0uu;->f(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-static {v2}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 416808
    :try_start_0
    iget-object v0, p0, LX/2Ut;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/2Ut;->a:Landroid/net/Uri;

    sget-object v2, LX/2Ut;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 416809
    :try_start_1
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 416810
    const-string v0, "recipient_ids"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 416811
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 416812
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 416813
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 416814
    iget-object v0, p0, LX/2Ut;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MF;

    invoke-virtual {v0, v6}, LX/3MF;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 416815
    if-eqz v0, :cond_0

    .line 416816
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v7, :cond_2

    .line 416817
    const/4 v6, 0x0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 416818
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416819
    :goto_1
    goto :goto_0

    .line 416820
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 416821
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 416822
    :cond_2
    :try_start_2
    iget-object v8, p0, LX/2Ut;->i:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/FMB;

    const/16 v11, 0x32

    const-wide/16 v12, -0x1

    move-wide v9, v4

    invoke-virtual/range {v8 .. v13}, LX/FMB;->a(JIJ)LX/0Px;

    move-result-object v9

    .line 416823
    iget-object v8, p0, LX/2Ut;->k:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/FNO;

    iget-object v10, p0, LX/2Ut;->j:LX/0Ot;

    invoke-interface {v10}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/3MF;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10, v9}, LX/FNO;->b(Ljava/lang/String;LX/0Px;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 416824
    goto :goto_0

    .line 416825
    :cond_3
    if-eqz v1, :cond_4

    .line 416826
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 416827
    :cond_4
    return-void

    .line 416828
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2

    .line 416829
    :cond_5
    iget-object v8, p0, LX/2Ut;->i:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/FMB;

    const/16 v11, 0x32

    const-wide/16 v12, -0x1

    move-wide v9, v4

    invoke-virtual/range {v8 .. v13}, LX/FMB;->a(JIJ)LX/0Px;

    move-result-object v9

    .line 416830
    iget-object v8, p0, LX/2Ut;->k:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/FNO;

    invoke-virtual {v8, v0, v9}, LX/FNO;->a(Ljava/lang/String;LX/0Px;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/2GJ;)V
    .locals 3

    .prologue
    .line 416841
    invoke-super {p0, p1}, LX/1Eg;->a(LX/2GJ;)V

    .line 416842
    iget-object v0, p0, LX/2Ut;->h:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    sget-object v1, LX/0aY;->c:Ljava/lang/String;

    new-instance v2, LX/2VB;

    invoke-direct {v2, p0}, LX/2VB;-><init>(LX/2Ut;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 416843
    return-void
.end method

.method public final f()J
    .locals 4

    .prologue
    .line 416838
    iget-object v0, p0, LX/2Ut;->e:LX/2Or;

    invoke-virtual {v0}, LX/2Or;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 416839
    const-wide/16 v0, -0x1

    .line 416840
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/2Ut;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Kr;->X:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416837
    sget-object v0, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 416833
    iget-object v0, p0, LX/2Ut;->e:LX/2Or;

    invoke-virtual {v0}, LX/2Or;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Ut;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 416834
    :goto_0
    return v0

    .line 416835
    :cond_1
    iget-object v0, p0, LX/2Ut;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->X:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 416836
    iget-object v0, p0, LX/2Ut;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-gtz v0, :cond_2

    iget-object v0, p0, LX/2Ut;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416831
    new-instance v1, LX/FNN;

    invoke-direct {v1, p0}, LX/FNN;-><init>(LX/2Ut;)V

    .line 416832
    iget-object v0, p0, LX/2Ut;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
