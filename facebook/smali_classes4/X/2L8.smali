.class public final LX/2L8;
.super Ljava/util/LinkedHashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "B:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TA;TB;>;"
    }
.end annotation


# instance fields
.field private final maxEntries:I

.field public final synthetic this$0:LX/2L7;


# direct methods
.method public constructor <init>(LX/2L7;I)V
    .locals 3

    .prologue
    .line 394728
    iput-object p1, p0, LX/2L8;->this$0:LX/2L7;

    .line 394729
    add-int/lit8 v0, p2, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    .line 394730
    iput p2, p0, LX/2L8;->maxEntries:I

    .line 394731
    return-void
.end method


# virtual methods
.method public final removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TA;TB;>;)Z"
        }
    .end annotation

    .prologue
    .line 394732
    invoke-virtual {p0}, LX/2L8;->size()I

    move-result v0

    iget v1, p0, LX/2L8;->maxEntries:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
