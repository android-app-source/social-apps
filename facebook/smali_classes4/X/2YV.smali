.class public final LX/2YV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2Wd;

.field private final b:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/2WY;


# direct methods
.method public constructor <init>(LX/2Wd;Lcom/facebook/flexiblesampling/SamplingPolicyConfig;LX/2WY;)V
    .locals 0
    .param p2    # Lcom/facebook/flexiblesampling/SamplingPolicyConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 420999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421000
    iput-object p1, p0, LX/2YV;->a:LX/2Wd;

    .line 421001
    iput-object p2, p0, LX/2YV;->b:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    .line 421002
    iput-object p3, p0, LX/2YV;->c:LX/2WY;

    .line 421003
    return-void
.end method


# virtual methods
.method public final a(ILjava/io/InputStream;)V
    .locals 3

    .prologue
    .line 421004
    const/16 v0, 0xc8

    if-eq p1, v0, :cond_0

    .line 421005
    :try_start_0
    new-instance v0, Lorg/apache/http/client/HttpResponseException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected HTTP code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 421006
    :catch_0
    move-exception v0

    .line 421007
    :try_start_1
    iget-object v1, p0, LX/2YV;->c:LX/2WY;

    invoke-interface {v1, v0}, LX/2WY;->a(Ljava/io/IOException;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 421008
    iget-object v0, p0, LX/2YV;->a:LX/2Wd;

    invoke-interface {v0}, LX/2Wd;->c()V

    .line 421009
    invoke-virtual {p2}, Ljava/io/InputStream;->close()V

    .line 421010
    :goto_0
    return-void

    .line 421011
    :cond_0
    :try_start_2
    iget-object v0, p0, LX/2YV;->b:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    if-eqz v0, :cond_1

    .line 421012
    iget-object v0, p0, LX/2YV;->b:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    invoke-interface {v0, p2}, Lcom/facebook/flexiblesampling/SamplingPolicyConfig;->a(Ljava/io/InputStream;)V

    .line 421013
    :cond_1
    iget-object v0, p0, LX/2YV;->a:LX/2Wd;

    invoke-interface {v0}, LX/2Wd;->e()V

    .line 421014
    iget-object v0, p0, LX/2YV;->c:LX/2WY;

    invoke-interface {v0}, LX/2WY;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 421015
    iget-object v0, p0, LX/2YV;->a:LX/2Wd;

    invoke-interface {v0}, LX/2Wd;->c()V

    .line 421016
    invoke-virtual {p2}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 421017
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2YV;->a:LX/2Wd;

    invoke-interface {v1}, LX/2Wd;->c()V

    .line 421018
    invoke-virtual {p2}, Ljava/io/InputStream;->close()V

    throw v0
.end method
