.class public LX/2jz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2jz;


# instance fields
.field private final a:LX/2k0;

.field private final b:LX/2kG;

.field private final c:LX/2jp;


# direct methods
.method public constructor <init>(LX/2jp;LX/2k0;LX/2kG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 454561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454562
    iput-object p1, p0, LX/2jz;->c:LX/2jp;

    .line 454563
    iput-object p2, p0, LX/2jz;->a:LX/2k0;

    .line 454564
    iput-object p3, p0, LX/2jz;->b:LX/2kG;

    .line 454565
    return-void
.end method

.method public static a(LX/0QB;)LX/2jz;
    .locals 6

    .prologue
    .line 454566
    sget-object v0, LX/2jz;->d:LX/2jz;

    if-nez v0, :cond_1

    .line 454567
    const-class v1, LX/2jz;

    monitor-enter v1

    .line 454568
    :try_start_0
    sget-object v0, LX/2jz;->d:LX/2jz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 454569
    if-eqz v2, :cond_0

    .line 454570
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 454571
    new-instance p0, LX/2jz;

    invoke-static {v0}, LX/2jp;->a(LX/0QB;)LX/2jp;

    move-result-object v3

    check-cast v3, LX/2jp;

    invoke-static {v0}, LX/2k0;->a(LX/0QB;)LX/2k0;

    move-result-object v4

    check-cast v4, LX/2k0;

    invoke-static {v0}, LX/2kG;->a(LX/0QB;)LX/2kG;

    move-result-object v5

    check-cast v5, LX/2kG;

    invoke-direct {p0, v3, v4, v5}, LX/2jz;-><init>(LX/2jp;LX/2k0;LX/2kG;)V

    .line 454572
    move-object v0, p0

    .line 454573
    sput-object v0, LX/2jz;->d:LX/2jz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 454574
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 454575
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 454576
    :cond_1
    sget-object v0, LX/2jz;->d:LX/2jz;

    return-object v0

    .line 454577
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 454578
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2k1;
    .locals 6

    .prologue
    .line 454579
    sget-object v0, LX/2kO;->a:[I

    iget-object v1, p0, LX/2jz;->c:LX/2jp;

    .line 454580
    iget-object v2, v1, LX/2jp;->a:LX/0W3;

    sget-wide v4, LX/0X5;->bs:J

    const-string v3, "graph_cursor_database"

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 454581
    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 454582
    sget-object v2, LX/2kP;->GRAPH_CURSOR_DATABASE:LX/2kP;

    :goto_1
    move-object v1, v2

    .line 454583
    invoke-virtual {v1}, LX/2kP;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 454584
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 454585
    :pswitch_0
    iget-object v0, p0, LX/2jz;->a:LX/2k0;

    .line 454586
    :goto_2
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/2jz;->b:LX/2kG;

    goto :goto_2

    .line 454587
    :sswitch_0
    const-string v4, "simple_edge_store"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :sswitch_1
    const-string v4, "graph_cursor_database"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 454588
    :pswitch_2
    sget-object v2, LX/2kP;->SIMPLE_EDGE_STORE:LX/2kP;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0xc6b4b53 -> :sswitch_1
        0x5c9d488c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
