.class public LX/2C7;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 382061
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 382062
    return-void
.end method

.method public static a(LX/28q;Landroid/os/Handler;)LX/0Tf;
    .locals 1
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/push/mqtt/annotations/ForMqttThreadWakeup;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 382063
    const-string v0, "Mqtt_Wakeup"

    invoke-virtual {p0, v0, p1}, LX/28q;->a(Ljava/lang/String;Landroid/os/Handler;)LX/2a2;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Handler;)LX/0Tf;
    .locals 1
    .param p0    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 382060
    new-instance v0, LX/0Td;

    invoke-direct {v0, p0}, LX/0Td;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 382059
    return-void
.end method
