.class public LX/2Oa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final a:LX/2OW;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field

.field public c:J

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>(LX/2OW;)V
    .locals 2

    .prologue
    .line 402426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402427
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/2Oa;->b:Ljava/util/List;

    .line 402428
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2Oa;->c:J

    .line 402429
    iput-object p1, p0, LX/2Oa;->a:LX/2OW;

    .line 402430
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 402431
    iget-object v0, p0, LX/2Oa;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402432
    iput-wide p1, p0, LX/2Oa;->c:J

    .line 402433
    return-void
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 402434
    iget-object v0, p0, LX/2Oa;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402435
    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/2Oa;->b:Ljava/util/List;

    .line 402436
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 402437
    iget-object v0, p0, LX/2Oa;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402438
    iput-boolean p1, p0, LX/2Oa;->e:Z

    .line 402439
    return-void
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 1

    .prologue
    .line 402440
    iget-object v0, p0, LX/2Oa;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402441
    iget-object v0, p0, LX/2Oa;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
