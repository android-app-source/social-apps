.class public LX/2uc;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/2ub;",
            "LX/3DO;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/google/common/util/concurrent/SettableFuture;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 476036
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "notifications/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 476037
    sput-object v0, LX/2uc;->a:LX/0Tn;

    const-string v1, "notifications_mqtt_sync_interval"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2uc;->b:LX/0Tn;

    .line 476038
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, LX/2ub;->COLD_START:LX/2ub;

    sget-object v2, LX/3DO;->FULL:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    sget-object v2, LX/3DO;->FULL:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->BACKGROUND:LX/2ub;

    sget-object v2, LX/3DO;->FULL:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->MQTT_FULL:LX/2ub;

    sget-object v2, LX/3DO;->FULL:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->MQTT_NEW:LX/2ub;

    sget-object v2, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->PUSH:LX/2ub;

    sget-object v2, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->SCROLL:LX/2ub;

    sget-object v2, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->PAGES:LX/2ub;

    sget-object v2, LX/3DO;->FULL:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->PAGES_COUNT_CHANGED:LX/2ub;

    sget-object v2, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->GROUPS:LX/2ub;

    sget-object v2, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->FRAGMENT_LOADED:LX/2ub;

    sget-object v2, LX/3DO;->FULL:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->CONNECTIVITY:LX/2ub;

    sget-object v2, LX/3DO;->FULL:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2ub;->UNKNOWN:LX/2ub;

    sget-object v2, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/2uc;->c:LX/0P1;

    .line 476039
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 476040
    sput-object v0, LX/2uc;->d:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, LX/2ug;

    invoke-direct {v1}, LX/2ug;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 476041
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 476042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 476043
    return-void
.end method
