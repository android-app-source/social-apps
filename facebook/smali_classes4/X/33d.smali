.class public final LX/33d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/zero/sdk/token/ZeroToken;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0yi;

.field public final synthetic b:LX/0yP;


# direct methods
.method public constructor <init>(LX/0yP;LX/0yi;)V
    .locals 0

    .prologue
    .line 493926
    iput-object p1, p0, LX/33d;->b:LX/0yP;

    iput-object p2, p0, LX/33d;->a:LX/0yi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 493920
    iget-object v0, p0, LX/33d;->b:LX/0yP;

    iget-object v1, p0, LX/33d;->a:LX/0yi;

    .line 493921
    iget-object v2, v0, LX/0yP;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1pA;

    invoke-virtual {v2}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v2

    .line 493922
    instance-of p0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz p0, :cond_1

    .line 493923
    :cond_0
    :goto_0
    return-void

    .line 493924
    :cond_1
    const-string p0, "none"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 493925
    invoke-virtual {v0, p1, v1}, LX/0yP;->a(Ljava/lang/Throwable;LX/0yi;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 493917
    check-cast p1, Lcom/facebook/zero/sdk/token/ZeroToken;

    .line 493918
    iget-object v0, p0, LX/33d;->b:LX/0yP;

    iget-object v1, p0, LX/33d;->a:LX/0yi;

    invoke-virtual {v0, p1, v1}, LX/0yP;->b(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V

    .line 493919
    return-void
.end method
