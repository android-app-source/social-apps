.class public LX/3LQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3LQ;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550529
    iput-object p1, p0, LX/3LQ;->a:LX/0Zb;

    .line 550530
    return-void
.end method

.method public static a(LX/0QB;)LX/3LQ;
    .locals 4

    .prologue
    .line 550515
    sget-object v0, LX/3LQ;->b:LX/3LQ;

    if-nez v0, :cond_1

    .line 550516
    const-class v1, LX/3LQ;

    monitor-enter v1

    .line 550517
    :try_start_0
    sget-object v0, LX/3LQ;->b:LX/3LQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 550518
    if-eqz v2, :cond_0

    .line 550519
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 550520
    new-instance p0, LX/3LQ;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/3LQ;-><init>(LX/0Zb;)V

    .line 550521
    move-object v0, p0

    .line 550522
    sput-object v0, LX/3LQ;->b:LX/3LQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 550523
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 550524
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 550525
    :cond_1
    sget-object v0, LX/3LQ;->b:LX/3LQ;

    return-object v0

    .line 550526
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 550527
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 550497
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "background_location_upsell_miniphone_displayed"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "background_location"

    .line 550498
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 550499
    move-object v0, v0

    .line 550500
    const-string v1, "show_nux"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 550501
    iget-object v1, p0, LX/3LQ;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 550502
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 550509
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "background_location_upsell_miniphone_selected"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "background_location"

    .line 550510
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 550511
    move-object v0, v0

    .line 550512
    const-string v1, "show_nux"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 550513
    iget-object v1, p0, LX/3LQ;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 550514
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 550503
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "friends_nearby_divebar_wave"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "background_location"

    .line 550504
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 550505
    move-object v0, v0

    .line 550506
    const-string v1, "target_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "action"

    const-string v2, "wave_sent"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 550507
    iget-object v1, p0, LX/3LQ;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 550508
    return-void
.end method
