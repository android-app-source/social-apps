.class public LX/3Bp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Bq;


# instance fields
.field private final b:LX/2lk;


# direct methods
.method public constructor <init>(LX/2lk;)V
    .locals 0

    .prologue
    .line 529133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529134
    iput-object p1, p0, LX/3Bp;->b:LX/2lk;

    .line 529135
    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 529136
    instance-of v0, p1, LX/0jT;

    if-eqz v0, :cond_1

    .line 529137
    iget-object v0, p0, LX/3Bp;->b:LX/2lk;

    check-cast p1, LX/0jT;

    invoke-interface {v0, p1}, LX/2lk;->d(LX/0jT;)Z

    .line 529138
    :cond_0
    return-void

    .line 529139
    :cond_1
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_0

    .line 529140
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 529141
    instance-of v2, v0, LX/0jT;

    if-eqz v2, :cond_2

    .line 529142
    iget-object v2, p0, LX/3Bp;->b:LX/2lk;

    check-cast v0, LX/0jT;

    invoke-interface {v2, v0}, LX/2lk;->d(LX/0jT;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 529143
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/3Bp;->b:LX/2lk;

    invoke-interface {v0}, LX/2lk;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 529144
    :cond_0
    :goto_0
    return-object p1

    .line 529145
    :cond_1
    if-eqz p2, :cond_2

    .line 529146
    invoke-direct {p0, p1}, LX/3Bp;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 529147
    :cond_2
    instance-of v0, p1, LX/0jT;

    if-eqz v0, :cond_3

    .line 529148
    check-cast p1, LX/0jT;

    .line 529149
    iget-object v0, p0, LX/3Bp;->b:LX/2lk;

    invoke-interface {v0, p1}, LX/2lk;->b(LX/0jT;)LX/0jT;

    move-result-object p1

    goto :goto_0

    .line 529150
    :cond_3
    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 529151
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 529152
    :cond_4
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_0

    .line 529153
    check-cast p1, Ljava/util/List;

    .line 529154
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 529155
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 529156
    instance-of v5, v0, LX/0jT;

    if-eqz v5, :cond_7

    .line 529157
    check-cast v0, LX/0jT;

    .line 529158
    iget-object v5, p0, LX/3Bp;->b:LX/2lk;

    invoke-interface {v5, v0}, LX/2lk;->b(LX/0jT;)LX/0jT;

    move-result-object v5

    .line 529159
    if-nez v1, :cond_5

    if-eq v0, v5, :cond_6

    :cond_5
    const/4 v0, 0x1

    .line 529160
    :goto_2
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v0

    .line 529161
    goto :goto_1

    :cond_6
    move v0, v2

    .line 529162
    goto :goto_2

    .line 529163
    :cond_7
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 529164
    :cond_8
    if-eqz v1, :cond_0

    move-object p1, v3

    goto :goto_0
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 529165
    iget-object v0, p0, LX/3Bp;->b:LX/2lk;

    invoke-interface {v0}, LX/2lk;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 529166
    const-string v0, "ConsistencyTaggedCacheVisitor"

    return-object v0
.end method
