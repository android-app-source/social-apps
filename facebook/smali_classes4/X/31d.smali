.class public LX/31d;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/31d;


# instance fields
.field public a:LX/03V;

.field public b:LX/0i4;


# direct methods
.method public constructor <init>(LX/03V;LX/0i4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 487676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487677
    iput-object p1, p0, LX/31d;->a:LX/03V;

    .line 487678
    iput-object p2, p0, LX/31d;->b:LX/0i4;

    .line 487679
    return-void
.end method

.method public static a(LX/0QB;)LX/31d;
    .locals 5

    .prologue
    .line 487680
    sget-object v0, LX/31d;->c:LX/31d;

    if-nez v0, :cond_1

    .line 487681
    const-class v1, LX/31d;

    monitor-enter v1

    .line 487682
    :try_start_0
    sget-object v0, LX/31d;->c:LX/31d;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 487683
    if-eqz v2, :cond_0

    .line 487684
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 487685
    new-instance p0, LX/31d;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    const-class v4, LX/0i4;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/0i4;

    invoke-direct {p0, v3, v4}, LX/31d;-><init>(LX/03V;LX/0i4;)V

    .line 487686
    move-object v0, p0

    .line 487687
    sput-object v0, LX/31d;->c:LX/31d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 487688
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 487689
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 487690
    :cond_1
    sget-object v0, LX/31d;->c:LX/31d;

    return-object v0

    .line 487691
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 487692
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
