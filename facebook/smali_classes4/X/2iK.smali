.class public LX/2iK;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/2iL;
.implements LX/2iM;
.implements LX/2ht;


# instance fields
.field public final a:LX/17W;

.field private final b:LX/2ha;

.field private final c:LX/2iR;

.field public final d:LX/2iS;

.field public final e:LX/2iU;

.field private final f:LX/0ad;

.field public final g:LX/2iW;

.field public final h:LX/2iY;

.field public final i:LX/86b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/86b",
            "<",
            "LX/2no;",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/friends/model/FriendRequest;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/2no;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/FriendRequest;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2lr;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/2ib;

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nP;",
            ">;"
        }
    .end annotation
.end field

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field public z:Z


# direct methods
.method public constructor <init>(LX/17W;LX/2ha;LX/2iN;LX/2iO;LX/2iP;LX/0ad;Landroid/content/Context;LX/2h7;LX/86b;LX/1Ck;)V
    .locals 2
    .param p7    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/2h7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/86b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/1Ck;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17W;",
            "LX/2ha;",
            "LX/2iN;",
            "LX/2iO;",
            "LX/2iP;",
            "LX/0ad;",
            "Landroid/content/Context;",
            "LX/2h7;",
            "LX/86b",
            "<",
            "LX/2no;",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;",
            "LX/1Ck;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 451333
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 451334
    iput-boolean v0, p0, LX/2iK;->r:Z

    .line 451335
    iput-boolean v0, p0, LX/2iK;->s:Z

    .line 451336
    iput-boolean v1, p0, LX/2iK;->t:Z

    .line 451337
    iput-boolean v1, p0, LX/2iK;->u:Z

    .line 451338
    const v0, 0x7fffffff

    iput v0, p0, LX/2iK;->y:I

    .line 451339
    iput-boolean v1, p0, LX/2iK;->z:Z

    .line 451340
    iput-object p1, p0, LX/2iK;->a:LX/17W;

    .line 451341
    iput-object p2, p0, LX/2iK;->b:LX/2ha;

    .line 451342
    sget-object v0, LX/2iQ;->JEWEL:LX/2iQ;

    invoke-virtual {p3, v0}, LX/2iN;->a(LX/2iQ;)LX/2iR;

    move-result-object v0

    iput-object v0, p0, LX/2iK;->c:LX/2iR;

    .line 451343
    invoke-virtual {p4, p8}, LX/2iO;->a(LX/2h7;)LX/2iS;

    move-result-object v0

    iput-object v0, p0, LX/2iK;->d:LX/2iS;

    .line 451344
    invoke-virtual {p5, p7, p8, p0, p10}, LX/2iP;->a(Landroid/content/Context;LX/2h7;LX/2iM;LX/1Ck;)LX/2iU;

    move-result-object v0

    iput-object v0, p0, LX/2iK;->e:LX/2iU;

    .line 451345
    iput-object p6, p0, LX/2iK;->f:LX/0ad;

    .line 451346
    new-instance v0, LX/2iV;

    invoke-direct {v0, p0}, LX/2iV;-><init>(LX/2iK;)V

    iput-object v0, p0, LX/2iK;->g:LX/2iW;

    .line 451347
    new-instance v0, LX/2iX;

    invoke-direct {v0, p0}, LX/2iX;-><init>(LX/2iK;)V

    iput-object v0, p0, LX/2iK;->h:LX/2iY;

    .line 451348
    iget-object v0, p0, LX/2iK;->d:LX/2iS;

    new-instance v1, LX/2iZ;

    invoke-direct {v1, p0}, LX/2iZ;-><init>(LX/2iK;)V

    .line 451349
    iput-object v1, v0, LX/2iS;->n:LX/2ia;

    .line 451350
    iput-object p9, p0, LX/2iK;->i:LX/86b;

    .line 451351
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2iK;->m:Ljava/util/List;

    .line 451352
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2iK;->n:Ljava/util/List;

    .line 451353
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2iK;->o:Ljava/util/List;

    .line 451354
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2iK;->j:Ljava/util/Map;

    .line 451355
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2iK;->l:Ljava/util/Map;

    .line 451356
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2iK;->k:Ljava/util/Map;

    .line 451357
    new-instance v0, LX/2ib;

    invoke-direct {v0}, LX/2ib;-><init>()V

    iput-object v0, p0, LX/2iK;->p:LX/2ib;

    .line 451358
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2iK;->q:Ljava/util/List;

    .line 451359
    invoke-virtual {p7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 451360
    const v1, 0x7f0b08a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/2iK;->v:I

    .line 451361
    const v1, 0x7f0b2287

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/2iK;->w:I

    .line 451362
    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/2iK;->x:I

    .line 451363
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 4

    .prologue
    .line 451364
    invoke-static {p0, p1}, LX/2iK;->b(Landroid/view/ViewGroup;I)Landroid/widget/TextView;

    move-result-object v0

    .line 451365
    sget-object v1, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v2, LX/0xr;->MEDIUM:LX/0xr;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 451366
    return-object v0
.end method

.method public static a(Landroid/view/ViewGroup;II)Landroid/view/View;
    .locals 3

    .prologue
    .line 451367
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306e4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 451368
    invoke-static {v0, p1, p2}, LX/2iK;->a(Landroid/view/View;II)V

    .line 451369
    return-object v0
.end method

.method private static a(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 451370
    const v0, 0x7f0d128a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 451371
    const v1, 0x7f0d128b

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 451372
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 451373
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 451374
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 451375
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 451376
    :goto_0
    return-void

    .line 451377
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z
    .locals 1

    .prologue
    .line 451378
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/view/ViewGroup;I)Landroid/widget/TextView;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 451379
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306fe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 451380
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 451381
    return-object v0
.end method

.method private r()Z
    .locals 3

    .prologue
    .line 451382
    iget-object v0, p0, LX/2iK;->f:LX/0ad;

    sget-short v1, LX/2nX;->c:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 451383
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 451384
    iget-object v0, p0, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    .line 451385
    iget-object p0, v0, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    move-object v0, p0

    .line 451386
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 451387
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 451509
    sget-object v0, LX/2nW;->a:[I

    invoke-static {}, LX/2ie;->values()[LX/2ie;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1}, LX/2ie;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 451510
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 451511
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030c0f

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 451512
    :goto_0
    return-object v0

    .line 451513
    :pswitch_1
    invoke-direct {p0}, LX/2iK;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451514
    const v0, 0x7f080f70

    const v1, 0x7f080f90

    invoke-static {p2, v0, v1}, LX/2iK;->a(Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 451515
    goto :goto_0

    .line 451516
    :cond_0
    const v0, 0x7f080f70

    invoke-static {p2, v0}, LX/2iK;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 451517
    goto :goto_0

    .line 451518
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306e8

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/jewel/FriendRequestView;

    .line 451519
    iget-object v1, p0, LX/2iK;->g:LX/2iW;

    .line 451520
    iput-object v1, v0, Lcom/facebook/friending/jewel/FriendRequestView;->v:LX/2iW;

    .line 451521
    iget-object v1, p0, LX/2iK;->h:LX/2iY;

    .line 451522
    iput-object v1, v0, Lcom/facebook/friending/jewel/FriendRequestView;->w:LX/2iY;

    .line 451523
    move-object v0, v0

    .line 451524
    goto :goto_0

    .line 451525
    :pswitch_3
    iget-object v0, p0, LX/2iK;->b:LX/2ha;

    invoke-virtual {v0}, LX/2ha;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 451526
    const v0, 0x7f080f77

    const v1, 0x7f080f90

    invoke-static {p2, v0, v1}, LX/2iK;->a(Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 451527
    goto :goto_0

    .line 451528
    :cond_1
    const v0, 0x7f080f77

    invoke-static {p2, v0}, LX/2iK;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 451529
    goto :goto_0

    .line 451530
    :pswitch_4
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0309a4

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/common/list/FriendRequestItemView;

    .line 451531
    const v1, 0x7f0d01fe

    invoke-virtual {v0, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setId(I)V

    .line 451532
    move-object v0, v0

    .line 451533
    goto :goto_0

    .line 451534
    :pswitch_5
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0309a3

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/common/list/FriendListItemView;

    .line 451535
    const v1, 0x7f0d01fe

    invoke-virtual {v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;->setId(I)V

    .line 451536
    move-object v0, v0

    .line 451537
    goto/16 :goto_0

    .line 451538
    :pswitch_6
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312df

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 451539
    new-instance v1, LX/Ez5;

    invoke-direct {v1, p0}, LX/Ez5;-><init>(LX/2iK;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451540
    move-object v0, v0

    .line 451541
    goto/16 :goto_0

    .line 451542
    :pswitch_7
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312df

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 451543
    new-instance v1, LX/Ez6;

    invoke-direct {v1, p0}, LX/Ez6;-><init>(LX/2iK;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451544
    move-object v0, v0

    .line 451545
    goto/16 :goto_0

    .line 451546
    :pswitch_8
    const v0, 0x7f080fae

    invoke-static {p2, v0}, LX/2iK;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 451547
    goto/16 :goto_0

    .line 451548
    :pswitch_9
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306e9

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/jewel/ContactsSectionView;

    .line 451549
    new-instance v1, LX/Ez7;

    invoke-direct {v1, p0}, LX/Ez7;-><init>(LX/2iK;)V

    invoke-virtual {v0, v1}, Lcom/facebook/friending/jewel/ContactsSectionView;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451550
    move-object v0, v0

    .line 451551
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public final a(I)Lcom/facebook/friends/model/FriendRequest;
    .locals 1

    .prologue
    .line 451388
    iget-object v0, p0, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 8

    .prologue
    .line 451389
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 451390
    sget-object v0, LX/2nW;->a:[I

    invoke-static {}, LX/2ie;->values()[LX/2ie;

    move-result-object v1

    aget-object v1, v1, p4

    invoke-virtual {v1}, LX/2ie;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 451391
    :goto_0
    :pswitch_0
    return-void

    .line 451392
    :pswitch_1
    check-cast p3, Lcom/facebook/friending/jewel/FriendRequestView;

    check-cast p2, Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {p3, p2}, Lcom/facebook/friending/jewel/FriendRequestView;->a(Lcom/facebook/friends/model/FriendRequest;)V

    goto :goto_0

    .line 451393
    :pswitch_2
    iget-object v0, p0, LX/2iK;->c:LX/2iR;

    check-cast p3, Lcom/facebook/friending/common/list/FriendRequestItemView;

    check-cast p2, LX/2no;

    const/4 p0, 0x1

    .line 451394
    iget-boolean v1, p2, LX/2no;->e:Z

    move v1, v1

    .line 451395
    if-nez v1, :cond_2

    move v1, p0

    :goto_1
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 451396
    invoke-virtual {p2}, LX/2no;->b()Ljava/lang/String;

    move-result-object v1

    .line 451397
    iget-object p1, p2, LX/2no;->b:Ljava/lang/String;

    move-object p1, p1

    .line 451398
    invoke-static {p3, v1, p1}, LX/2iR;->a(Lcom/facebook/fbui/widget/contentview/ContentView;Ljava/lang/String;Ljava/lang/String;)V

    .line 451399
    invoke-virtual {p3, p0}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setFriendRequestButtonsVisible(Z)V

    .line 451400
    const v1, 0x7f080f7b

    invoke-static {v0, v1}, LX/2iR;->a(LX/2iR;I)Ljava/lang/CharSequence;

    move-result-object v1

    const p0, 0x7f080f7c

    invoke-static {v0, p0}, LX/2iR;->a(LX/2iR;I)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p3, v1, p0}, Lcom/facebook/friending/common/list/FriendRequestItemView;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 451401
    const v1, 0x7f080f78

    invoke-static {v0, v1}, LX/2iR;->a(LX/2iR;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 451402
    new-instance v1, LX/Ez8;

    invoke-direct {v1, v0, p2}, LX/Ez8;-><init>(LX/2iR;LX/2no;)V

    invoke-virtual {p3, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451403
    new-instance v1, LX/Ez9;

    invoke-direct {v1, v0, p2}, LX/Ez9;-><init>(LX/2iR;LX/2no;)V

    invoke-virtual {p3, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451404
    goto :goto_0

    .line 451405
    :pswitch_3
    iget-object v0, p0, LX/2iK;->c:LX/2iR;

    check-cast p3, Lcom/facebook/friending/common/list/FriendListItemView;

    check-cast p2, LX/2no;

    const-wide/16 v6, -0x1

    .line 451406
    iget-boolean v2, p2, LX/2no;->e:Z

    move v2, v2

    .line 451407
    invoke-static {v2}, LX/0Tp;->b(Z)V

    .line 451408
    invoke-virtual {p2}, LX/2no;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/2iR;->h:Landroid/content/res/Resources;

    const v4, 0x7f080f85

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v2, v3}, LX/2iR;->a(Lcom/facebook/fbui/widget/contentview/ContentView;Ljava/lang/String;Ljava/lang/String;)V

    .line 451409
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 451410
    sget-object v2, LX/EyR;->SECONDARY:LX/EyR;

    iget-object v3, v0, LX/2iR;->h:Landroid/content/res/Resources;

    const v4, 0x7f020b7a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Lcom/facebook/friending/common/list/FriendListItemView;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    .line 451411
    const v2, 0x7f080017

    invoke-static {v0, v2}, LX/2iR;->a(LX/2iR;I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 451412
    iget-object v2, v0, LX/2iR;->h:Landroid/content/res/Resources;

    const v3, 0x7f080f9e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 451413
    new-instance v2, LX/EzA;

    invoke-direct {v2, v0, p2}, LX/EzA;-><init>(LX/2iR;LX/2no;)V

    invoke-virtual {p3, v2}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451414
    iget-boolean v2, v0, LX/2iR;->k:Z

    if-eqz v2, :cond_1

    iget-wide v2, v0, LX/2iR;->j:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    invoke-virtual {p2}, LX/2no;->a()J

    move-result-wide v2

    iget-wide v4, v0, LX/2iR;->j:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    iget-object v2, v0, LX/2iR;->f:LX/2hc;

    const/4 v3, 0x0

    .line 451415
    invoke-virtual {v2}, LX/2hc;->c()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v2, LX/2hc;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/1nR;->g:LX/0Tn;

    invoke-interface {v4, v5, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v3, 0x1

    :cond_0
    move v2, v3

    .line 451416
    if-eqz v2, :cond_1

    .line 451417
    new-instance v2, LX/0hs;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0hs;-><init>(Landroid/content/Context;)V

    .line 451418
    const v3, 0x7f081fe9

    invoke-virtual {v2, v3}, LX/0hs;->a(I)V

    .line 451419
    const v3, 0x7f081fea

    invoke-virtual {v2, v3}, LX/0hs;->b(I)V

    .line 451420
    sget-object v3, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v2, v3}, LX/0ht;->a(LX/3AV;)V

    .line 451421
    const/16 v3, 0x1770

    .line 451422
    iput v3, v2, LX/0hs;->t:I

    .line 451423
    invoke-virtual {v2, p3}, LX/0ht;->a(Landroid/view/View;)V

    .line 451424
    iget-object v2, v0, LX/2iR;->f:LX/2hc;

    .line 451425
    iget-object v3, v2, LX/2hc;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/1nR;->g:LX/0Tn;

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 451426
    iput-wide v6, v0, LX/2iR;->j:J

    .line 451427
    :cond_1
    goto/16 :goto_0

    .line 451428
    :pswitch_4
    iget-object v0, p0, LX/2iK;->d:LX/2iS;

    check-cast p3, Lcom/facebook/fbui/widget/contentview/ContentView;

    check-cast p2, Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v0, p3, p2}, LX/2iS;->a(Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    goto/16 :goto_0

    .line 451429
    :pswitch_5
    check-cast p3, Lcom/facebook/friending/jewel/ContactsSectionView;

    .line 451430
    iget-object v0, p0, LX/2iK;->p:LX/2ib;

    .line 451431
    iget-object v1, v0, LX/2ib;->a:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 451432
    invoke-virtual {p3, v0}, Lcom/facebook/friending/jewel/ContactsSectionView;->setFaceUrls(Ljava/util/List;)V

    .line 451433
    iget-object v0, p0, LX/2iK;->p:LX/2ib;

    invoke-virtual {p3}, Lcom/facebook/friending/jewel/ContactsSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v3, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 451434
    iget v2, v0, LX/2ib;->c:I

    if-ne v2, v7, :cond_3

    .line 451435
    const v2, 0x7f080faf

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, v0, LX/2ib;->b:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, v0, LX/2ib;->b:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 451436
    :goto_2
    move-object v0, v2

    .line 451437
    invoke-virtual {p3, v0}, Lcom/facebook/friending/jewel/ContactsSectionView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 451438
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 451439
    :cond_3
    iget v2, v0, LX/2ib;->c:I

    if-ne v2, v3, :cond_4

    .line 451440
    const v2, 0x7f080fb0

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, LX/2ib;->b:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, v0, LX/2ib;->b:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, v0, LX/2ib;->b:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 451441
    :cond_4
    const v2, 0x7f080fb1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, LX/2ib;->b:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, v0, LX/2ib;->b:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    iget v4, v0, LX/2ib;->c:I

    add-int/lit8 v4, v4, -0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 451442
    iget-object v0, p0, LX/2iK;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 451443
    :cond_0
    :goto_0
    return-void

    .line 451444
    :cond_1
    const/4 v2, -0x1

    .line 451445
    if-nez p1, :cond_3

    move v1, v2

    .line 451446
    :cond_2
    :goto_1
    move v0, v1

    .line 451447
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 451448
    iget-object v1, p0, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 451449
    iget-object v0, p0, LX/2iK;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 451450
    invoke-virtual {p0}, LX/2iK;->m()V

    .line 451451
    const v0, 0x76ca1412

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 451452
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 451453
    iget-object v0, p0, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    .line 451454
    iget-object v3, v0, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    move-object v0, v3

    .line 451455
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 451456
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    move v1, v2

    .line 451457
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/2lu;Z)V
    .locals 1

    .prologue
    .line 451458
    iget-object v0, p0, LX/2iK;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    .line 451459
    if-eqz v0, :cond_0

    .line 451460
    iput-object p2, v0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    .line 451461
    if-eqz p3, :cond_0

    .line 451462
    const v0, 0x323efb7a

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 451463
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2no;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 451464
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 451465
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 451466
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2no;

    .line 451467
    iget-object v4, p0, LX/2iK;->k:Ljava/util/Map;

    invoke-virtual {v0}, LX/2no;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 451468
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 451469
    iget-object v4, p0, LX/2iK;->k:Ljava/util/Map;

    invoke-virtual {v0}, LX/2no;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 451470
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 451471
    iget-object v4, p0, LX/2iK;->l:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 451472
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 451473
    iget-object v4, p0, LX/2iK;->n:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451474
    iget-object v4, p0, LX/2iK;->l:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 451475
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 451476
    iget-object v3, p0, LX/2iK;->i:LX/86b;

    if-eqz v3, :cond_8

    .line 451477
    iget-object v3, p0, LX/2iK;->i:LX/86b;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 451478
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 451479
    iget-object v4, v3, LX/86b;->a:Ljava/util/LinkedList;

    invoke-virtual {v4, v1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 451480
    iget v4, v3, LX/86b;->b:I

    if-nez v4, :cond_5

    iget v4, v3, LX/86b;->c:I

    move v5, v4

    .line 451481
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2lr;

    .line 451482
    iget p1, v3, LX/86b;->d:I

    sub-int/2addr p1, v5

    add-int/lit8 p1, p1, 0x1

    .line 451483
    if-ltz p1, :cond_4

    iget p2, v3, LX/86b;->c:I

    rem-int/2addr p1, p2

    if-nez p1, :cond_4

    iget-object p1, v3, LX/86b;->a:Ljava/util/LinkedList;

    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_4

    .line 451484
    iget-object p1, v3, LX/86b;->a:Ljava/util/LinkedList;

    invoke-virtual {p1}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v6, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 451485
    iget p1, v3, LX/86b;->d:I

    add-int/lit8 p1, p1, 0x1

    iput p1, v3, LX/86b;->d:I

    .line 451486
    :cond_4
    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 451487
    iget v4, v3, LX/86b;->d:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, LX/86b;->d:I

    goto :goto_3

    .line 451488
    :cond_5
    iget v4, v3, LX/86b;->b:I

    move v5, v4

    goto :goto_2

    .line 451489
    :cond_6
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v1, v4

    .line 451490
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 451491
    :goto_4
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 451492
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 451493
    iget-object v1, p0, LX/2iK;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 451494
    invoke-virtual {p0}, LX/2iK;->m()V

    .line 451495
    const v0, 0x433604b9

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 451496
    :cond_7
    return-void

    .line 451497
    :cond_8
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_4
.end method

.method public final a(Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 451498
    iget-object v0, p0, LX/2iK;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 451499
    iget-object v0, p0, LX/2iK;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 451500
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 451501
    iget-object v2, p0, LX/2iK;->l:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 451502
    :cond_0
    invoke-virtual {p0}, LX/2iK;->m()V

    .line 451503
    const v0, -0x53404e17

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 451504
    return-void
.end method

.method public final b()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 451505
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 451506
    iget-object v0, p0, LX/2iK;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 451507
    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 451508
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 451309
    sget-object v3, LX/2nW;->a:[I

    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nP;

    .line 451310
    iget-object p1, v0, LX/2nP;->a:LX/2ie;

    move-object v0, p1

    .line 451311
    invoke-virtual {v0}, LX/2ie;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object p2, v2

    .line 451312
    :goto_0
    return-object p2

    .line 451313
    :pswitch_1
    iget-boolean v0, p0, LX/2iK;->t:Z

    if-nez v0, :cond_0

    move-object p2, v2

    .line 451314
    goto :goto_0

    .line 451315
    :cond_0
    const v2, 0x7f080f70

    .line 451316
    invoke-direct {p0}, LX/2iK;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f080f90

    :goto_1
    move v1, v0

    .line 451317
    :goto_2
    invoke-direct {p0}, LX/2iK;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 451318
    if-eqz p2, :cond_3

    .line 451319
    invoke-static {p2, v2, v1}, LX/2iK;->a(Landroid/view/View;II)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 451320
    goto :goto_1

    .line 451321
    :pswitch_2
    const v0, 0x7f080f77

    .line 451322
    iget-object v2, p0, LX/2iK;->b:LX/2ha;

    invoke-virtual {v2}, LX/2ha;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    const v1, 0x7f080f90

    :cond_2
    move v2, v0

    .line 451323
    goto :goto_2

    .line 451324
    :pswitch_3
    const v0, 0x7f080fae

    move v2, v0

    .line 451325
    goto :goto_2

    .line 451326
    :cond_3
    invoke-static {p3, v2, v1}, LX/2iK;->a(Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object p2

    .line 451327
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020b83

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p2, v0}, LX/1r0;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 451328
    :cond_4
    if-nez p2, :cond_5

    .line 451329
    invoke-static {p3, v2}, LX/2iK;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p2

    .line 451330
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020b83

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p2, v0}, LX/1r0;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_5
    move-object v0, p2

    .line 451331
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final b(I)Lcom/facebook/friends/model/PersonYouMayKnow;
    .locals 1

    .prologue
    .line 451332
    iget-object v0, p0, LX/2iK;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    return-object v0
.end method

.method public final b(J)Z
    .locals 3

    .prologue
    .line 451228
    iget-object v0, p0, LX/2iK;->j:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 451229
    iget v0, p0, LX/2iK;->x:I

    return v0
.end method

.method public final d(J)V
    .locals 3

    .prologue
    .line 451230
    iget-object v0, p0, LX/2iK;->l:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 451231
    :goto_0
    return-void

    .line 451232
    :cond_0
    iget-object v0, p0, LX/2iK;->l:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 451233
    iget-object v1, p0, LX/2iK;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 451234
    iget-object v1, p0, LX/2iK;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 451235
    iget-object v0, p0, LX/2iK;->l:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 451236
    invoke-virtual {p0}, LX/2iK;->m()V

    .line 451237
    const v0, -0x3a48b348

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final e(I)I
    .locals 3

    .prologue
    .line 451238
    sget-object v0, LX/2nW;->a:[I

    invoke-static {}, LX/2ie;->values()[LX/2ie;

    move-result-object v1

    invoke-virtual {p0, p1}, LX/2iK;->getItemViewType(I)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v1}, LX/2ie;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 451239
    iget v0, p0, LX/2iK;->v:I

    :goto_0
    return v0

    .line 451240
    :pswitch_0
    invoke-direct {p0}, LX/2iK;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/2iK;->w:I

    goto :goto_0

    :cond_0
    iget v0, p0, LX/2iK;->v:I

    goto :goto_0

    .line 451241
    :pswitch_1
    iget-object v0, p0, LX/2iK;->b:LX/2ha;

    invoke-virtual {v0}, LX/2ha;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, LX/2iK;->w:I

    goto :goto_0

    :cond_1
    iget v0, p0, LX/2iK;->v:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 451242
    iget-object v0, p0, LX/2iK;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 451243
    iget-object v0, p0, LX/2iK;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 451244
    iget-object v0, p0, LX/2iK;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 451245
    iget-object v0, p0, LX/2iK;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 451246
    iget-object v0, p0, LX/2iK;->i:LX/86b;

    if-eqz v0, :cond_0

    .line 451247
    iget-object v0, p0, LX/2iK;->i:LX/86b;

    .line 451248
    iget-object p0, v0, LX/86b;->a:Ljava/util/LinkedList;

    invoke-virtual {p0}, Ljava/util/LinkedList;->clear()V

    .line 451249
    const/4 p0, 0x0

    iput p0, v0, LX/86b;->d:I

    .line 451250
    :cond_0
    return-void
.end method

.method public final e(J)Z
    .locals 3

    .prologue
    .line 451251
    iget-object v0, p0, LX/2iK;->l:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final f(I)Z
    .locals 3

    .prologue
    .line 451252
    sget-object v0, LX/2nW;->a:[I

    invoke-static {}, LX/2ie;->values()[LX/2ie;

    move-result-object v1

    invoke-virtual {p0, p1}, LX/2iK;->getItemViewType(I)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v1}, LX/2ie;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 451253
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 451254
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x4 -> :sswitch_0
        0xb -> :sswitch_0
    .end sparse-switch
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 451255
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 451256
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nP;

    .line 451257
    iget-object p0, v0, LX/2nP;->b:Ljava/lang/Object;

    move-object v0, p0

    .line 451258
    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 451259
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 451260
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nP;

    .line 451261
    iget-object p0, v0, LX/2nP;->a:LX/2ie;

    move-object v0, p0

    .line 451262
    invoke-virtual {v0}, LX/2ie;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 451263
    invoke-static {}, LX/2ie;->values()[LX/2ie;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 451264
    iget-object v0, p0, LX/2iK;->p:LX/2ib;

    invoke-virtual {v0}, LX/2ib;->b()Z

    move-result v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 451265
    iget-boolean v0, p0, LX/2iK;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nP;

    .line 451266
    iget-boolean p0, v0, LX/2nP;->c:Z

    move v0, p0

    .line 451267
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 451268
    iget-object v0, p0, LX/2iK;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 451269
    iget-object v0, p0, LX/2iK;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final m()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 451270
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 451271
    iget-object v0, p0, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2iK;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2iK;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2iK;->p:LX/2ib;

    invoke-virtual {v0}, LX/2ib;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 451272
    :cond_0
    :goto_0
    return-void

    .line 451273
    :cond_1
    iget-boolean v0, p0, LX/2iK;->t:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/2iK;->s:Z

    if-eqz v0, :cond_3

    .line 451274
    :cond_2
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v1, LX/2nP;

    sget-object v3, LX/2ie;->FRIEND_REQUESTS_HEADER:LX/2ie;

    invoke-direct {v1, v3}, LX/2nP;-><init>(LX/2ie;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451275
    :cond_3
    iget-boolean v0, p0, LX/2iK;->s:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 451276
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v1, LX/2nP;

    sget-object v3, LX/2ie;->NO_REQUESTS:LX/2ie;

    invoke-direct {v1, v3}, LX/2nP;-><init>(LX/2ie;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451277
    :cond_4
    iget-object v0, p0, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    .line 451278
    iget-boolean v4, p0, LX/2iK;->z:Z

    if-nez v4, :cond_8

    add-int/lit8 v1, v1, 0x1

    iget v4, p0, LX/2iK;->y:I

    if-le v1, v4, :cond_8

    .line 451279
    const/4 v2, 0x1

    .line 451280
    :cond_5
    if-eqz v2, :cond_6

    .line 451281
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v1, LX/2nP;

    sget-object v2, LX/2ie;->FRIEND_REQUESTS_SHOW_MORE:LX/2ie;

    invoke-direct {v1, v2}, LX/2nP;-><init>(LX/2ie;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451282
    :cond_6
    iget-object v0, p0, LX/2iK;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 451283
    iget-object v0, p0, LX/2iK;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 451284
    :cond_7
    :goto_2
    iget-boolean v0, p0, LX/2iK;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2iK;->p:LX/2ib;

    invoke-virtual {v0}, LX/2ib;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451285
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v1, LX/2nP;

    sget-object v2, LX/2ie;->SEE_ALL_PYMK:LX/2ie;

    invoke-direct {v1, v2}, LX/2nP;-><init>(LX/2ie;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451286
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v1, LX/2nP;

    sget-object v2, LX/2ie;->CONTACTS_SECTION_HEADER:LX/2ie;

    invoke-direct {v1, v2}, LX/2nP;-><init>(LX/2ie;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451287
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v1, LX/2nP;

    sget-object v2, LX/2ie;->CONTACTS_SECTION:LX/2ie;

    invoke-direct {v1, v2}, LX/2nP;-><init>(LX/2ie;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 451288
    :cond_8
    iget-object v4, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v5, LX/2nP;

    sget-object v6, LX/2ie;->FRIEND_REQUEST:LX/2ie;

    invoke-direct {v5, v6, v0}, LX/2nP;-><init>(LX/2ie;Ljava/lang/Object;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 451289
    :cond_9
    iget-object v0, p0, LX/2iK;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 451290
    :cond_a
    goto :goto_2

    .line 451291
    :cond_b
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v1, LX/2nP;

    sget-object v2, LX/2ie;->PYMK_HEADER:LX/2ie;

    invoke-direct {v1, v2}, LX/2nP;-><init>(LX/2ie;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451292
    iget-object v0, p0, LX/2iK;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 451293
    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-static {v1}, LX/2iK;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z

    move-result v1

    if-eqz v1, :cond_c

    sget-object v1, LX/2ie;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/2ie;

    .line 451294
    :goto_4
    iget-object v3, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v4, LX/2nP;

    invoke-direct {v4, v1, v0}, LX/2nP;-><init>(LX/2ie;Ljava/lang/Object;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 451295
    :cond_c
    sget-object v1, LX/2ie;->PERSON_YOU_MAY_KNOW:LX/2ie;

    goto :goto_4

    .line 451296
    :cond_d
    iget-object v0, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v1, LX/2nP;

    sget-object v2, LX/2ie;->PYMK_HEADER:LX/2ie;

    invoke-direct {v1, v2}, LX/2nP;-><init>(LX/2ie;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451297
    iget-object v0, p0, LX/2iK;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_e
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2lr;

    .line 451298
    instance-of v1, v0, LX/2no;

    if-eqz v1, :cond_10

    move-object v1, v0

    .line 451299
    check-cast v1, LX/2no;

    .line 451300
    iget-boolean v3, v1, LX/2no;->e:Z

    move v1, v3

    .line 451301
    if-eqz v1, :cond_f

    sget-object v1, LX/2ie;->RESPONDED_PERSON_YOU_MAY_INVITE:LX/2ie;

    .line 451302
    :goto_6
    iget-object v3, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v4, LX/2nP;

    invoke-direct {v4, v1, v0}, LX/2nP;-><init>(LX/2ie;Ljava/lang/Object;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 451303
    :cond_f
    sget-object v1, LX/2ie;->PERSON_YOU_MAY_INVITE:LX/2ie;

    goto :goto_6

    .line 451304
    :cond_10
    instance-of v1, v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    if-eqz v1, :cond_e

    move-object v1, v0

    .line 451305
    check-cast v1, Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-static {v1}, LX/2iK;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z

    move-result v1

    if-eqz v1, :cond_11

    sget-object v1, LX/2ie;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/2ie;

    .line 451306
    :goto_7
    iget-object v3, p0, LX/2iK;->q:Ljava/util/List;

    new-instance v4, LX/2nP;

    invoke-direct {v4, v1, v0}, LX/2nP;-><init>(LX/2ie;Ljava/lang/Object;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 451307
    :cond_11
    sget-object v1, LX/2ie;->PERSON_YOU_MAY_KNOW:LX/2ie;

    goto :goto_7
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 451308
    const/4 v0, 0x0

    return v0
.end method
