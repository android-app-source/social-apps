.class public final LX/3Qv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/VideoChannelEntryPoint;
    .end annotation
.end field

.field public e:I

.field public f:I

.field public g:LX/04D;

.field public h:LX/04g;

.field public i:LX/D6I;

.field public j:Z

.field public k:LX/D4s;

.field public l:LX/0JG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:LX/19o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/03z;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 566590
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566591
    const/4 v0, -0x1

    iput v0, p0, LX/3Qv;->n:I

    .line 566592
    return-void
.end method

.method public static a(LX/3Qw;)LX/3Qv;
    .locals 2

    .prologue
    .line 566603
    new-instance v0, LX/3Qv;

    invoke-direct {v0}, LX/3Qv;-><init>()V

    .line 566604
    if-eqz p0, :cond_0

    .line 566605
    iget-object v1, p0, LX/3Qw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 566606
    iput-object v1, v0, LX/3Qv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 566607
    iget-object v1, p0, LX/3Qw;->b:Ljava/util/List;

    .line 566608
    iput-object v1, v0, LX/3Qv;->b:Ljava/util/List;

    .line 566609
    iget-object v1, p0, LX/3Qw;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/3Qv;->b(Ljava/util/List;)LX/3Qv;

    .line 566610
    iget-object v1, p0, LX/3Qw;->d:Ljava/lang/String;

    .line 566611
    iput-object v1, v0, LX/3Qv;->d:Ljava/lang/String;

    .line 566612
    iget v1, p0, LX/3Qw;->e:I

    .line 566613
    iput v1, v0, LX/3Qv;->e:I

    .line 566614
    iget v1, p0, LX/3Qw;->f:I

    .line 566615
    iput v1, v0, LX/3Qv;->f:I

    .line 566616
    iget-object v1, p0, LX/3Qw;->g:LX/04D;

    .line 566617
    iput-object v1, v0, LX/3Qv;->g:LX/04D;

    .line 566618
    iget-object v1, p0, LX/3Qw;->h:LX/04g;

    .line 566619
    iput-object v1, v0, LX/3Qv;->h:LX/04g;

    .line 566620
    iget-object v1, p0, LX/3Qw;->i:LX/D6I;

    .line 566621
    iput-object v1, v0, LX/3Qv;->i:LX/D6I;

    .line 566622
    iget-boolean v1, p0, LX/3Qw;->j:Z

    .line 566623
    iput-boolean v1, v0, LX/3Qv;->j:Z

    .line 566624
    iget-object v1, p0, LX/3Qw;->k:LX/D4s;

    .line 566625
    iput-object v1, v0, LX/3Qv;->k:LX/D4s;

    .line 566626
    iget-object v1, p0, LX/3Qw;->l:LX/0JG;

    .line 566627
    iput-object v1, v0, LX/3Qv;->l:LX/0JG;

    .line 566628
    iget-object v1, p0, LX/3Qw;->m:Ljava/lang/String;

    .line 566629
    iput-object v1, v0, LX/3Qv;->m:Ljava/lang/String;

    .line 566630
    iget v1, p0, LX/3Qw;->n:I

    .line 566631
    iput v1, v0, LX/3Qv;->n:I

    .line 566632
    iget-object v1, p0, LX/3Qw;->o:Ljava/lang/String;

    .line 566633
    iput-object v1, v0, LX/3Qv;->o:Ljava/lang/String;

    .line 566634
    iget-object v1, p0, LX/3Qw;->p:Ljava/lang/String;

    .line 566635
    iput-object v1, v0, LX/3Qv;->p:Ljava/lang/String;

    .line 566636
    iget-object v1, p0, LX/3Qw;->q:LX/19o;

    .line 566637
    iput-object v1, v0, LX/3Qv;->q:LX/19o;

    .line 566638
    iget-object v1, p0, LX/3Qw;->r:LX/03z;

    .line 566639
    iput-object v1, v0, LX/3Qv;->r:LX/03z;

    .line 566640
    iget-boolean v1, p0, LX/3Qw;->s:Z

    .line 566641
    iput-boolean v1, v0, LX/3Qv;->s:Z

    .line 566642
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/3Qv;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 566598
    iget-object v0, p0, LX/3Qv;->c:Ljava/util/List;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "videoChannelIds should not have been previously already set"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 566599
    if-eqz p1, :cond_0

    .line 566600
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/3Qv;->c:Ljava/util/List;

    .line 566601
    :cond_0
    return-object p0

    :cond_1
    move v0, v2

    .line 566602
    goto :goto_0
.end method

.method public final a()LX/3Qw;
    .locals 22

    .prologue
    .line 566597
    new-instance v1, LX/3Qw;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3Qv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/3Qv;->b:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/3Qv;->c:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/3Qv;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v6, v0, LX/3Qv;->e:I

    move-object/from16 v0, p0

    iget v7, v0, LX/3Qv;->f:I

    move-object/from16 v0, p0

    iget-object v8, v0, LX/3Qv;->g:LX/04D;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/3Qv;->h:LX/04g;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/3Qv;->i:LX/D6I;

    move-object/from16 v0, p0

    iget-boolean v11, v0, LX/3Qv;->j:Z

    move-object/from16 v0, p0

    iget-object v12, v0, LX/3Qv;->k:LX/D4s;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/3Qv;->l:LX/0JG;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/3Qv;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v15, v0, LX/3Qv;->n:I

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3Qv;->o:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3Qv;->p:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3Qv;->q:LX/19o;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/3Qv;->r:LX/03z;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/3Qv;->s:Z

    move/from16 v20, v0

    const/16 v21, 0x0

    invoke-direct/range {v1 .. v21}, LX/3Qw;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/List;Ljava/util/List;Ljava/lang/String;IILX/04D;LX/04g;LX/D6I;ZLX/D4s;LX/0JG;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/19o;LX/03z;ZB)V

    return-object v1
.end method

.method public final b(Ljava/util/List;)LX/3Qv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/3Qv;"
        }
    .end annotation

    .prologue
    .line 566593
    iget-object v0, p0, LX/3Qv;->c:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "videoChannelIds should not have been previously already set"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 566594
    iput-object p1, p0, LX/3Qv;->c:Ljava/util/List;

    .line 566595
    return-object p0

    .line 566596
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
