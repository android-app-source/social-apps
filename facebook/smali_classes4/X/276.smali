.class public LX/276;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qf;
.implements LX/1qg;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile k:LX/276;


# instance fields
.field private final b:LX/0Uh;

.field public final c:LX/0if;

.field public d:LX/2wX;

.field private e:Ljava/lang/Runnable;

.field private f:I

.field public g:Lcom/google/android/gms/auth/api/credentials/Credential;

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 372833
    const-class v0, LX/276;

    sput-object v0, LX/276;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/0if;)V
    .locals 1
    .param p1    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 372826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372827
    iput-boolean v0, p0, LX/276;->h:Z

    .line 372828
    iput-boolean v0, p0, LX/276;->i:Z

    .line 372829
    iput-boolean v0, p0, LX/276;->j:Z

    .line 372830
    iput-object p1, p0, LX/276;->b:LX/0Uh;

    .line 372831
    iput-object p2, p0, LX/276;->c:LX/0if;

    .line 372832
    return-void
.end method

.method public static a(LX/0QB;)LX/276;
    .locals 5

    .prologue
    .line 372813
    sget-object v0, LX/276;->k:LX/276;

    if-nez v0, :cond_1

    .line 372814
    const-class v1, LX/276;

    monitor-enter v1

    .line 372815
    :try_start_0
    sget-object v0, LX/276;->k:LX/276;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 372816
    if-eqz v2, :cond_0

    .line 372817
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 372818
    new-instance p0, LX/276;

    invoke-static {v0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    invoke-direct {p0, v3, v4}, LX/276;-><init>(LX/0Uh;LX/0if;)V

    .line 372819
    move-object v0, p0

    .line 372820
    sput-object v0, LX/276;->k:LX/276;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 372821
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 372822
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 372823
    :cond_1
    sget-object v0, LX/276;->k:LX/276;

    return-object v0

    .line 372824
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 372825
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private i()Z
    .locals 3

    .prologue
    .line 372812
    iget-object v0, p0, LX/276;->b:LX/0Uh;

    const/16 v1, 0x3f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static j(LX/276;)Z
    .locals 1

    .prologue
    .line 372811
    invoke-virtual {p0}, LX/276;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/276;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/276;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/276;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 372810
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 372796
    invoke-virtual {p0}, LX/276;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372797
    invoke-virtual {p0}, LX/276;->b()V

    .line 372798
    :cond_0
    iget-object v0, p0, LX/276;->c:LX/0if;

    sget-object v1, LX/0ig;->be:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 372799
    invoke-virtual {p0}, LX/276;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 372800
    iget-object v0, p0, LX/276;->c:LX/0if;

    sget-object v1, LX/0ig;->be:LX/0ih;

    const-string v2, "fbandroid_smartlock_select"

    invoke-virtual {v0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 372801
    :cond_1
    :goto_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 372802
    iget-object v1, p0, LX/276;->e:Ljava/lang/Runnable;

    iget v2, p0, LX/276;->f:I

    int-to-long v2, v2

    const v4, -0xd4316bc

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 372803
    return-void

    .line 372804
    :cond_2
    invoke-direct {p0}, LX/276;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 372805
    iget-object v0, p0, LX/276;->c:LX/0if;

    sget-object v1, LX/0ig;->be:LX/0ih;

    const-string v2, "fbandroid_smartlock_autofill"

    invoke-virtual {v0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    goto :goto_0

    .line 372806
    :cond_3
    invoke-direct {p0}, LX/276;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 372807
    iget-object v0, p0, LX/276;->c:LX/0if;

    sget-object v1, LX/0ig;->be:LX/0ih;

    const-string v2, "fbandroid_smartlock_autologin"

    invoke-virtual {v0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    goto :goto_0

    .line 372808
    :cond_4
    invoke-virtual {p0}, LX/276;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372809
    iget-object v0, p0, LX/276;->c:LX/0if;

    sget-object v1, LX/0ig;->be:LX/0ih;

    const-string v2, "fbandroid_smartlock_hints"

    invoke-virtual {v0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/app/FragmentActivity;Ljava/lang/Runnable;I)V
    .locals 4

    .prologue
    .line 372778
    invoke-static {p0}, LX/276;->j(LX/276;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 372779
    :goto_0
    return-void

    .line 372780
    :cond_0
    iget-object v0, p0, LX/276;->d:LX/2wX;

    if-nez v0, :cond_1

    .line 372781
    :try_start_0
    new-instance v0, LX/2vz;

    invoke-direct {v0, p1}, LX/2vz;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p0}, LX/2vz;->a(LX/1qf;)LX/2vz;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, LX/4uq;

    invoke-direct {v2, p1}, LX/4uq;-><init>(Landroid/app/Activity;)V

    if-ltz v1, :cond_2

    const/4 v3, 0x1

    :goto_1
    const-string p1, "clientId must be non-negative"

    invoke-static {v3, p1}, LX/1ol;->b(ZLjava/lang/Object;)V

    iput v1, v0, LX/2vz;->l:I

    iput-object p0, v0, LX/2vz;->m:LX/1qg;

    iput-object v2, v0, LX/2vz;->k:LX/4uq;

    move-object v2, v0

    move-object v1, v2

    move-object v0, v1

    .line 372782
    sget-object v1, LX/G7R;->e:LX/2vs;

    invoke-virtual {v0, v1}, LX/2vz;->a(LX/2vs;)LX/2vz;

    move-result-object v0

    invoke-virtual {v0}, LX/2vz;->b()LX/2wX;

    move-result-object v0

    iput-object v0, p0, LX/276;->d:LX/2wX;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 372783
    :cond_1
    iput-object p2, p0, LX/276;->e:Ljava/lang/Runnable;

    .line 372784
    iput p3, p0, LX/276;->f:I

    goto :goto_0

    .line 372785
    :catch_0
    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0

    .prologue
    .line 372795
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 372793
    iget-object v0, p0, LX/276;->c:LX/0if;

    sget-object v1, LX/0ig;->be:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 372794
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 372789
    invoke-static {p0}, LX/276;->j(LX/276;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 372790
    :cond_0
    :goto_0
    return-void

    .line 372791
    :cond_1
    iget-object v0, p0, LX/276;->d:LX/2wX;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/276;->d:LX/2wX;

    invoke-virtual {v0}, LX/2wX;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372792
    sget-object v0, LX/G7R;->i:LX/G7V;

    iget-object v1, p0, LX/276;->d:LX/2wX;

    invoke-interface {v0, v1}, LX/G7V;->a(LX/2wX;)LX/2wg;

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 372788
    iget-object v0, p0, LX/276;->b:LX/0Uh;

    const/16 v1, 0x42

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 372787
    iget-object v0, p0, LX/276;->b:LX/0Uh;

    const/16 v1, 0x40

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 372786
    iget-object v0, p0, LX/276;->b:LX/0Uh;

    const/16 v1, 0x41

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
