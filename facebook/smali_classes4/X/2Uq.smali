.class public LX/2Uq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/2Uq;


# instance fields
.field public final a:LX/0ad;

.field private final b:LX/0ae;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/2Or;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0W3;

.field public g:LX/03R;


# direct methods
.method public constructor <init>(LX/0ad;LX/0ae;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2Or;LX/0W3;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0ae;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/2Or;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416680
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2Uq;->g:LX/03R;

    .line 416681
    iput-object p1, p0, LX/2Uq;->a:LX/0ad;

    .line 416682
    iput-object p2, p0, LX/2Uq;->b:LX/0ae;

    .line 416683
    iput-object p3, p0, LX/2Uq;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 416684
    iput-object p4, p0, LX/2Uq;->e:LX/0Or;

    .line 416685
    iput-object p5, p0, LX/2Uq;->d:LX/2Or;

    .line 416686
    iput-object p6, p0, LX/2Uq;->f:LX/0W3;

    .line 416687
    return-void
.end method

.method public static a(LX/0QB;)LX/2Uq;
    .locals 10

    .prologue
    .line 416642
    sget-object v0, LX/2Uq;->h:LX/2Uq;

    if-nez v0, :cond_1

    .line 416643
    const-class v1, LX/2Uq;

    monitor-enter v1

    .line 416644
    :try_start_0
    sget-object v0, LX/2Uq;->h:LX/2Uq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416645
    if-eqz v2, :cond_0

    .line 416646
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416647
    new-instance v3, LX/2Uq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ae;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v7, 0x12cb

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v8

    check-cast v8, LX/2Or;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v9

    check-cast v9, LX/0W3;

    invoke-direct/range {v3 .. v9}, LX/2Uq;-><init>(LX/0ad;LX/0ae;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2Or;LX/0W3;)V

    .line 416648
    move-object v0, v3

    .line 416649
    sput-object v0, LX/2Uq;->h:LX/2Uq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416650
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416651
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416652
    :cond_1
    sget-object v0, LX/2Uq;->h:LX/2Uq;

    return-object v0

    .line 416653
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416654
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Uq;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 416678
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LX/2Uq;->b:LX/0ae;

    sget-object v1, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-interface {v0, v1, p1}, LX/0ae;->a(LX/0oc;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/2Uq;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 416675
    invoke-static {p0, p1}, LX/2Uq;->a(LX/2Uq;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416676
    iget-object v0, p0, LX/2Uq;->b:LX/0ae;

    sget-object v1, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-interface {v0, v1, p1}, LX/0ae;->b(LX/0oc;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 416677
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static p(LX/2Uq;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 416668
    iget-object v0, p0, LX/2Uq;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 416669
    if-nez v0, :cond_0

    move v0, v1

    .line 416670
    :goto_0
    return v0

    .line 416671
    :cond_0
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 416672
    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 416673
    sget-object v3, LX/3Kr;->b:LX/0Tn;

    invoke-virtual {v3, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    const-string v3, "/sms_campaign_optin"

    invoke-virtual {v2, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    move-object v0, v2

    .line 416674
    iget-object v2, p0, LX/2Uq;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 416667
    iget-object v1, p0, LX/2Uq;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->y:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2Uq;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->x:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2Uq;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->w:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2Uq;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->G:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final k()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 416666
    iget-object v1, p0, LX/2Uq;->a:LX/0ad;

    sget-short v2, LX/6jD;->Z:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, LX/2Uq;->p(LX/2Uq;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final l()Z
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 416658
    iget-object v1, p0, LX/2Uq;->a:LX/0ad;

    sget-short v2, LX/6jD;->u:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 416659
    iget-object v7, p0, LX/2Uq;->g:LX/03R;

    invoke-virtual {v7}, LX/03R;->isSet()Z

    move-result v7

    if-nez v7, :cond_0

    .line 416660
    iget-object v7, p0, LX/2Uq;->f:LX/0W3;

    sget-wide v9, LX/0X5;->ge:J

    invoke-interface {v7, v9, v10}, LX/0W4;->f(J)Ljava/lang/String;

    move-result-object v7

    .line 416661
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    sget-object v7, LX/03R;->NO:LX/03R;

    :goto_0
    iput-object v7, p0, LX/2Uq;->g:LX/03R;

    .line 416662
    :cond_0
    iget-object v7, p0, LX/2Uq;->g:LX/03R;

    invoke-virtual {v7}, LX/03R;->asBoolean()Z

    move-result v7

    move v3, v7

    .line 416663
    if-eqz v3, :cond_3

    iget-object v3, p0, LX/2Uq;->f:LX/0W3;

    sget-wide v5, LX/0X5;->gd:J

    invoke-interface {v3, v5, v6}, LX/0W4;->a(J)Z

    move-result v3

    :goto_1
    move v1, v3

    .line 416664
    if-nez v1, :cond_2

    invoke-static {p0}, LX/2Uq;->p(LX/2Uq;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0

    :cond_3
    iget-object v3, p0, LX/2Uq;->a:LX/0ad;

    sget-short v4, LX/6jD;->l:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    goto :goto_1

    .line 416665
    :cond_4
    sget-object v7, LX/03R;->YES:LX/03R;

    goto :goto_0
.end method

.method public final m()Z
    .locals 4

    .prologue
    .line 416655
    iget-object v0, p0, LX/2Uq;->d:LX/2Or;

    .line 416656
    iget-object v1, v0, LX/2Or;->a:LX/0Uh;

    const/16 v2, 0x636

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 416657
    if-nez v0, :cond_0

    invoke-static {p0}, LX/2Uq;->p(LX/2Uq;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
