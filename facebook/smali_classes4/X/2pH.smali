.class public LX/2pH;
.super LX/2oy;
.source ""


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:LX/2pI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:Z

.field private final n:Lcom/facebook/common/callercontext/CallerContext;

.field private final o:LX/1Ai;

.field public p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 467945
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/2pH;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;LX/1Ai;)V

    .line 467946
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;LX/1Ai;)V
    .locals 3
    .param p3    # LX/1Ai;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 467937
    invoke-direct {p0, p1}, LX/2oy;-><init>(Landroid/content/Context;)V

    .line 467938
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2pH;->p:Z

    .line 467939
    const-class v0, LX/2pH;

    invoke-static {v0, p0}, LX/2pH;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 467940
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, LX/2pH;->n:Lcom/facebook/common/callercontext/CallerContext;

    .line 467941
    iput-object p3, p0, LX/2pH;->o:LX/1Ai;

    .line 467942
    iget-object v0, p0, LX/2pH;->d:LX/0Uh;

    const/16 v1, 0xe7

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2pH;->f:Z

    .line 467943
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/2pL;

    invoke-direct {v1, p0}, LX/2pL;-><init>(LX/2pH;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467944
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/2pH;

    invoke-static {p0}, LX/2pI;->a(LX/0QB;)LX/2pI;

    move-result-object v1

    check-cast v1, LX/2pI;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v1, p1, LX/2pH;->b:LX/2pI;

    iput-object v2, p1, LX/2pH;->c:LX/1Ad;

    iput-object v3, p1, LX/2pH;->d:LX/0Uh;

    iput-object p0, p1, LX/2pH;->e:LX/0ad;

    return-void
.end method

.method public static getInvisibleConst(LX/2pH;)I
    .locals 1

    .prologue
    .line 467893
    iget-boolean v0, p0, LX/2pH;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static setCoverImageVisible(LX/2pH;Z)V
    .locals 2

    .prologue
    .line 467931
    iget-object v0, p0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 467932
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 467933
    iget-object v1, p0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 467934
    iget-object v0, p0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 467935
    return-void

    .line 467936
    :cond_0
    invoke-static {p0}, LX/2pH;->getInvisibleConst(LX/2pH;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(LX/2pa;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 467908
    iget-object v0, p0, LX/2pH;->e:LX/0ad;

    sget-short v1, LX/0ws;->eG:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467909
    iget-object v0, p0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundColor(I)V

    .line 467910
    :cond_0
    const/4 v0, 0x0

    .line 467911
    iget-object v1, p0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_7

    .line 467912
    :cond_1
    :goto_0
    move v0, v0

    .line 467913
    if-eqz v0, :cond_2

    .line 467914
    invoke-static {p0, v3}, LX/2pH;->setCoverImageVisible(LX/2pH;Z)V

    .line 467915
    :cond_2
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "CoverImageParamsKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 467916
    invoke-virtual {p0}, LX/2oy;->n()V

    .line 467917
    :goto_1
    return-void

    .line 467918
    :cond_3
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "CoverImageParamsKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 467919
    instance-of v1, v0, LX/1bf;

    if-nez v1, :cond_4

    .line 467920
    invoke-virtual {p0}, LX/2oy;->n()V

    goto :goto_1

    .line 467921
    :cond_4
    iget-object v1, p0, LX/2pH;->c:LX/1Ad;

    iget-object v2, p0, LX/2pH;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    check-cast v0, LX/1bf;

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v1, p0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    .line 467922
    iget-object v0, p0, LX/2pH;->o:LX/1Ai;

    if-eqz v0, :cond_5

    .line 467923
    iget-object v0, p0, LX/2pH;->c:LX/1Ad;

    iget-object v1, p0, LX/2pH;->o:LX/1Ai;

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    .line 467924
    :cond_5
    iget-object v0, p0, LX/2pH;->c:LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 467925
    instance-of v0, v1, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    if-eqz v0, :cond_6

    move-object v0, v1

    .line 467926
    check-cast v0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    sget-object v2, LX/397;->VIDEO:LX/397;

    .line 467927
    invoke-static {v0, v3, v2}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;ILX/397;)V

    .line 467928
    :cond_6
    iget-object v0, p0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_1

    :cond_7
    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->l()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    .line 467929
    iget-object v2, v1, LX/2pb;->D:LX/04G;

    move-object v1, v2

    .line 467930
    sget-object v2, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-ne v1, v2, :cond_1

    :cond_8
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 467905
    iget-object v0, p0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 467906
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/2pH;->setCoverImageVisible(LX/2pH;Z)V

    .line 467907
    return-void
.end method

.method public final dH_()V
    .locals 2

    .prologue
    .line 467896
    invoke-super {p0}, LX/2oy;->dH_()V

    .line 467897
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 467898
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 467899
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 467900
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 467901
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 467902
    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 467903
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/2pH;->setCoverImageVisible(LX/2pH;Z)V

    .line 467904
    :cond_0
    return-void
.end method

.method public setShowCoverImageOnCompletion(Z)V
    .locals 0

    .prologue
    .line 467894
    iput-boolean p1, p0, LX/2pH;->p:Z

    .line 467895
    return-void
.end method
