.class public final LX/2p0;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/VideoPlugin;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/VideoPlugin;)V
    .locals 1

    .prologue
    .line 467567
    iput-object p1, p0, LX/2p0;->a:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    .line 467568
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2p0;->b:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/plugins/VideoPlugin;B)V
    .locals 0

    .prologue
    .line 467569
    invoke-direct {p0, p1}, LX/2p0;-><init>(Lcom/facebook/video/player/plugins/VideoPlugin;)V

    return-void
.end method

.method private a(LX/2ou;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 467570
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v2, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-eq v0, v2, :cond_0

    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v2, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v2, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 467571
    :goto_0
    iget-object v2, p0, LX/2p0;->a:Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v2, v2, LX/2oy;->j:LX/2pb;

    if-eqz v2, :cond_1

    sget-object v2, LX/04D;->PROFILE_VIDEO:LX/04D;

    iget-object v3, p0, LX/2p0;->a:Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v3, v3, LX/2oy;->j:LX/2pb;

    invoke-virtual {v3}, LX/2pb;->s()LX/04D;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/04D;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 467572
    :cond_1
    iget-boolean v1, p0, LX/2p0;->b:Z

    if-eq v0, v1, :cond_2

    .line 467573
    iget-object v1, p0, LX/2p0;->a:Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/VideoPlugin;->r:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setKeepScreenOn(Z)V

    .line 467574
    iput-boolean v0, p0, LX/2p0;->b:Z

    .line 467575
    :cond_2
    iget-object v0, p0, LX/2p0;->a:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 467576
    invoke-static {v0}, Lcom/facebook/video/player/plugins/VideoPlugin;->g(Lcom/facebook/video/player/plugins/VideoPlugin;)V

    .line 467577
    return-void

    :cond_3
    move v0, v1

    .line 467578
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 467579
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 467580
    check-cast p1, LX/2ou;

    invoke-direct {p0, p1}, LX/2p0;->a(LX/2ou;)V

    return-void
.end method
