.class public LX/3QL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/00H;

.field private final b:LX/2EL;

.field private final c:LX/29E;

.field private final d:LX/0SG;

.field private final e:LX/0So;

.field public final f:LX/2bC;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/2Ly;


# direct methods
.method public constructor <init>(LX/0SG;LX/0So;LX/00H;LX/2EL;LX/29E;LX/2bC;LX/0Or;LX/2Ly;)V
    .locals 0
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/analytics/perf/BlacklistSomeLatencyAndReliabilityEvents;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0So;",
            "LX/00H;",
            "LX/2EL;",
            "LX/29E;",
            "LX/2bC;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Ly;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 564198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564199
    iput-object p1, p0, LX/3QL;->d:LX/0SG;

    .line 564200
    iput-object p2, p0, LX/3QL;->e:LX/0So;

    .line 564201
    iput-object p3, p0, LX/3QL;->a:LX/00H;

    .line 564202
    iput-object p4, p0, LX/3QL;->b:LX/2EL;

    .line 564203
    iput-object p5, p0, LX/3QL;->c:LX/29E;

    .line 564204
    iput-object p6, p0, LX/3QL;->f:LX/2bC;

    .line 564205
    iput-object p7, p0, LX/3QL;->g:LX/0Or;

    .line 564206
    iput-object p8, p0, LX/3QL;->h:LX/2Ly;

    .line 564207
    return-void
.end method

.method private static a(Lcom/facebook/messaging/model/messages/Message;)LX/0P1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 564208
    const-string v0, "client_tag_"

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    .line 564209
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v4

    .line 564210
    if-eqz v1, :cond_0

    .line 564211
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 564212
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v4, v3, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 564213
    :cond_0
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    move-object v0, v2

    .line 564214
    return-object v0
.end method

.method public static a(LX/0QB;)LX/3QL;
    .locals 1

    .prologue
    .line 564215
    invoke-static {p0}, LX/3QL;->b(LX/0QB;)LX/3QL;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/3QL;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 564216
    iget-object v0, p0, LX/3QL;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/3QL;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 564217
    iget-object v0, p0, LX/3QL;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Map;LX/6f4;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/6f4;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 564218
    const-string v0, "total_attachment_size"

    iget-wide v2, p1, LX/6f4;->a:J

    invoke-static {p0, v0, v2, v3}, LX/3QL;->a(Ljava/util/Map;Ljava/lang/String;J)V

    .line 564219
    const-string v0, "photo_attachment_count"

    iget v1, p1, LX/6f4;->b:I

    int-to-long v2, v1

    invoke-static {p0, v0, v2, v3}, LX/3QL;->a(Ljava/util/Map;Ljava/lang/String;J)V

    .line 564220
    const-string v0, "video_attachment_count"

    iget v1, p1, LX/6f4;->c:I

    int-to-long v2, v1

    invoke-static {p0, v0, v2, v3}, LX/3QL;->a(Ljava/util/Map;Ljava/lang/String;J)V

    .line 564221
    const-string v0, "audio_attachment_count"

    iget v1, p1, LX/6f4;->d:I

    int-to-long v2, v1

    invoke-static {p0, v0, v2, v3}, LX/3QL;->a(Ljava/util/Map;Ljava/lang/String;J)V

    .line 564222
    const-string v0, "sticker_attachment_count"

    iget v1, p1, LX/6f4;->e:I

    int-to-long v2, v1

    invoke-static {p0, v0, v2, v3}, LX/3QL;->a(Ljava/util/Map;Ljava/lang/String;J)V

    .line 564223
    const-string v0, "like_attachment_count"

    iget v1, p1, LX/6f4;->f:I

    int-to-long v2, v1

    invoke-static {p0, v0, v2, v3}, LX/3QL;->a(Ljava/util/Map;Ljava/lang/String;J)V

    .line 564224
    const-string v0, "share_attachment_count"

    iget v1, p1, LX/6f4;->g:I

    int-to-long v2, v1

    invoke-static {p0, v0, v2, v3}, LX/3QL;->a(Ljava/util/Map;Ljava/lang/String;J)V

    .line 564225
    iget-object v0, p1, LX/6f4;->k:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 564226
    const-string v0, "attachment_mime_type_list"

    iget-object v1, p1, LX/6f4;->k:LX/0Px;

    const/16 v6, 0x22

    .line 564227
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 564228
    const/4 v2, 0x0

    .line 564229
    :goto_0
    move-object v1, v2

    .line 564230
    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564231
    :cond_0
    if-eqz p2, :cond_3

    iget-object v0, p1, LX/6f4;->j:LX/3CE;

    invoke-virtual {v0}, LX/0Xt;->n()Z

    move-result v0

    if-nez v0, :cond_3

    .line 564232
    const-string v0, "type_source_map"

    iget-object v1, p1, LX/6f4;->j:LX/3CE;

    .line 564233
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 564234
    const-string v2, "["

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564235
    invoke-interface {v1}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2MK;

    .line 564236
    invoke-interface {v1, v2}, LX/0Xv;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/5zj;

    .line 564237
    const-string p1, "{\""

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564238
    invoke-virtual {v2}, LX/2MK;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564239
    const-string p1, "\":\""

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564240
    invoke-virtual {v3}, LX/5zj;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564241
    const-string v3, "\"}"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564242
    const-string v3, ","

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 564243
    :cond_2
    const-string v2, "]"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564244
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 564245
    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564246
    :cond_3
    return-void

    .line 564247
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 564248
    const-string v2, "["

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564249
    const/4 v2, 0x1

    .line 564250
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 564251
    if-nez v3, :cond_5

    .line 564252
    const-string v3, ","

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564253
    :cond_5
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 564254
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564255
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 564256
    const/4 v2, 0x0

    move v3, v2

    .line 564257
    goto :goto_2

    .line 564258
    :cond_6
    const-string v2, "]"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564259
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public static a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ")V"
        }
    .end annotation

    .prologue
    .line 564260
    if-nez p1, :cond_1

    .line 564261
    :cond_0
    :goto_0
    return-void

    .line 564262
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->GROUP:LX/5e9;

    if-ne v0, v1, :cond_2

    .line 564263
    const-string v0, "thread_fbid"

    iget-wide v2, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 564264
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v1, :cond_3

    .line 564265
    const-string v0, "user_fbid"

    iget-wide v2, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 564266
    :cond_3
    iget-object v0, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->SMS:LX/5e9;

    if-ne v0, v1, :cond_4

    .line 564267
    const-string v0, "sms_tid"

    iget-wide v2, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 564268
    :cond_4
    iget-object v0, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->PENDING_THREAD:LX/5e9;

    if-ne v0, v1, :cond_0

    .line 564269
    const-string v0, "pending_tid"

    iget-wide v2, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 564270
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 564271
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564272
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/3QL;
    .locals 9

    .prologue
    .line 564273
    new-instance v0, LX/3QL;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    const-class v3, LX/00H;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/00H;

    invoke-static {p0}, LX/2EL;->a(LX/0QB;)LX/2EL;

    move-result-object v4

    check-cast v4, LX/2EL;

    invoke-static {p0}, LX/29E;->b(LX/0QB;)LX/29E;

    move-result-object v5

    check-cast v5, LX/29E;

    invoke-static {p0}, LX/2bC;->a(LX/0QB;)LX/2bC;

    move-result-object v6

    check-cast v6, LX/2bC;

    const/16 v7, 0x14d7

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v8

    check-cast v8, LX/2Ly;

    invoke-direct/range {v0 .. v8}, LX/3QL;-><init>(LX/0SG;LX/0So;LX/00H;LX/2EL;LX/29E;LX/2bC;LX/0Or;LX/2Ly;)V

    .line 564274
    return-object v0
.end method

.method public static d(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 564275
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 564276
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 564277
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 564278
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/6iW;Lcom/facebook/messaging/service/model/MarkThreadFields;LX/FCY;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 564279
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v1, "mark_thread"

    const/16 v3, 0x8

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "mark_type"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p1}, LX/6iW;->getApiName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "state"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-boolean v5, p2, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "request_action_id"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-wide v6, p2, Lcom/facebook/messaging/service/model/MarkThreadFields;->c:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "channel"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    iget-object v5, p3, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v3}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564280
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;JLX/6f4;LX/FKG;LX/FCY;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 564281
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v1, "create_thread"

    const-string v2, "failed"

    const/16 v3, 0x10

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "offline_threading_id"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    iget-object v6, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string v6, "sent_timestamp_ms"

    aput-object v6, v3, v5

    const/4 v5, 0x3

    iget-wide v6, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x4

    const-string v6, "send_time_delta"

    aput-object v6, v3, v5

    const/4 v5, 0x5

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x6

    const-string v6, "error_type"

    aput-object v6, v3, v5

    const/4 v5, 0x7

    invoke-virtual {p5}, LX/FKG;->b()LX/6fP;

    move-result-object v6

    invoke-virtual {v6}, LX/6fP;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/16 v5, 0x8

    const-string v6, "error_message"

    aput-object v6, v3, v5

    const/16 v5, 0x9

    invoke-virtual {p5}, LX/FKG;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/16 v5, 0xa

    const-string v6, "total_attachment_size"

    aput-object v6, v3, v5

    const/16 v5, 0xb

    iget-wide v6, p4, LX/6f4;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/16 v5, 0xc

    const-string v6, "current_time"

    aput-object v6, v3, v5

    const/16 v5, 0xd

    invoke-static {p0}, LX/3QL;->a(LX/3QL;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/16 v5, 0xe

    const-string v6, "channel"

    aput-object v6, v3, v5

    const/16 v5, 0xf

    iget-object v6, p6, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v6, v3, v5

    invoke-static {v3}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564282
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;LX/6f4;LX/FCY;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 564187
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "offline_threading_id"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    const-string v3, "current_time"

    aput-object v3, v0, v1

    const/4 v1, 0x3

    invoke-static {p0}, LX/3QL;->a(LX/3QL;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    const-string v3, "channel"

    aput-object v3, v0, v1

    const/4 v1, 0x5

    iget-object v3, p3, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v3, v0, v1

    invoke-static {v0}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 564188
    invoke-static {v3, p2, v4}, LX/3QL;->a(Ljava/util/Map;LX/6f4;Z)V

    .line 564189
    iget-object v0, p0, LX/3QL;->c:LX/29E;

    invoke-virtual {v0, v3}, LX/29E;->a(Ljava/util/Map;)V

    .line 564190
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v1, "msg_create_thread_attempted"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564191
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;Lcom/facebook/messaging/send/trigger/NavigationTrigger;Ljava/lang/String;ZI)V
    .locals 7
    .param p3    # Lcom/facebook/messaging/send/trigger/NavigationTrigger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 564192
    const/16 v0, 0xc

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "offline_threading_id"

    aput-object v3, v1, v0

    const/4 v0, 0x1

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v3, v1, v0

    const/4 v0, 0x2

    const-string v3, "current_time"

    aput-object v3, v1, v0

    const/4 v0, 0x3

    invoke-static {p0}, LX/3QL;->a(LX/3QL;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    const/4 v0, 0x4

    const-string v3, "trigger"

    aput-object v3, v1, v0

    const/4 v3, 0x5

    if-nez p3, :cond_0

    move-object v0, v2

    :goto_0
    aput-object v0, v1, v3

    const/4 v0, 0x6

    const-string v3, "mqtt_state"

    aput-object v3, v1, v0

    const/4 v0, 0x7

    aput-object p4, v1, v0

    const/16 v0, 0x8

    const-string v3, "connect_sent"

    aput-object v3, v1, v0

    const/16 v0, 0x9

    invoke-static {p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    const/16 v0, 0xa

    const-string v3, "thread_q_Size"

    aput-object v3, v1, v0

    const/16 v0, 0xb

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    invoke-static {v1}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 564193
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v3, v0}, LX/3QL;->a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564194
    iget-object v0, p0, LX/3QL;->c:LX/29E;

    invoke-virtual {v0, v3}, LX/29E;->a(Ljava/util/Map;)V

    .line 564195
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v1, "msg_send_click"

    move-object v4, v2

    move-object v5, v2

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564196
    return-void

    .line 564197
    :cond_0
    invoke-virtual {p3}, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/Message;JLX/6f4;LX/FCY;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 564101
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "offline_threading_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "sent_timestamp_ms"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p2, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "send_time_delta"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "total_attachment_size"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p5, LX/6f4;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "current_time"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    invoke-static {p0}, LX/3QL;->a(LX/3QL;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "channel"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p6, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 564102
    invoke-static {v3, p1}, LX/3QL;->a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564103
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v1, "create_thread"

    const-string v2, "success"

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564104
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;J)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 564105
    iget-object v0, p0, LX/3QL;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564106
    :goto_0
    return-void

    .line 564107
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 564108
    const-string v1, "messaging_pub_ack"

    .line 564109
    invoke-static {v0}, LX/3QL;->a(Lcom/facebook/messaging/model/messages/Message;)LX/0P1;

    move-result-object v3

    const/16 v5, 0x12

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "offline_threading_id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "sent_timestamp_ms"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget-wide v8, v0, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "send_time_delta"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "current_time"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    invoke-static {p0}, LX/3QL;->a(LX/3QL;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x8

    const-string v7, "channel"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    sget-object v7, LX/FCY;->MQTT:LX/FCY;

    iget-object v7, v7, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, "retry_count"

    aput-object v7, v5, v6

    const/16 v6, 0xb

    iget v7, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->d:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xc

    const-string v7, "first_send_delta"

    aput-object v7, v5, v6

    const/16 v6, 0xd

    iget-wide v8, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->e:J

    invoke-static {p0, v8, v9}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0xe

    const-string v7, "first_message_first_send_delta"

    aput-object v7, v5, v6

    const/16 v6, 0xf

    iget-wide v8, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->f:J

    invoke-static {p0, v8, v9}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x10

    const-string v7, "message_type"

    aput-object v7, v5, v6

    const/16 v6, 0x11

    iget-object v7, p0, LX/3QL;->h:LX/2Ly;

    invoke-virtual {v7, v0}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v5}, LX/29E;->a(Ljava/util/Map;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 564110
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v3, v0}, LX/3QL;->a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564111
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v2, "success_puback"

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;LX/FKG;ZLjava/lang/String;)V
    .locals 8

    .prologue
    .line 564112
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 564113
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "messaging_send_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 564114
    invoke-static {v0}, LX/3QL;->a(Lcom/facebook/messaging/model/messages/Message;)LX/0P1;

    move-result-object v3

    const/16 v4, 0x1c

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "offline_threading_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "sent_timestamp_ms"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-wide v6, v0, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "send_time_delta"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "error_type"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    invoke-virtual {p5}, LX/FKG;->b()LX/6fP;

    move-result-object v6

    invoke-virtual {v6}, LX/6fP;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "error_message"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    invoke-virtual {p5}, LX/FKG;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "current_time"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    invoke-static {p0}, LX/3QL;->a(LX/3QL;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xc

    const-string v6, "network_connected"

    aput-object v6, v4, v5

    const/16 v5, 0xd

    invoke-static {p6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xe

    const-string v6, "channel"

    aput-object v6, v4, v5

    const/16 v5, 0xf

    sget-object v6, LX/FCY;->GRAPH:LX/FCY;

    iget-object v6, v6, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0x10

    const-string v6, "netcheck_state"

    aput-object v6, v4, v5

    const/16 v5, 0x11

    iget-object v6, p0, LX/3QL;->b:LX/2EL;

    .line 564115
    iget-object v2, v6, LX/2EL;->k:LX/2EQ;

    move-object v6, v2

    .line 564116
    invoke-virtual {v6}, LX/2EQ;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x12

    const-string v6, "last_netcheck_time"

    aput-object v6, v4, v5

    const/16 v5, 0x13

    iget-object v6, p0, LX/3QL;->b:LX/2EL;

    invoke-virtual {v6}, LX/2EL;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x14

    const-string v6, "retry_count"

    aput-object v6, v4, v5

    const/16 v5, 0x15

    iget v6, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x16

    const-string v6, "first_send_delta"

    aput-object v6, v4, v5

    const/16 v5, 0x17

    iget-wide v6, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->e:J

    invoke-static {p0, v6, v7}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x18

    const-string v6, "first_message_first_send_delta"

    aput-object v6, v4, v5

    const/16 v5, 0x19

    iget-wide v6, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->f:J

    invoke-static {p0, v6, v7}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x1a

    const-string v6, "message_type"

    aput-object v6, v4, v5

    const/16 v5, 0x1b

    iget-object v6, p0, LX/3QL;->h:LX/2Ly;

    invoke-virtual {v6, v0}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, LX/29E;->a(Ljava/util/Map;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 564117
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v3, v0}, LX/3QL;->a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564118
    const/4 v0, 0x0

    invoke-static {v3, p4, v0}, LX/3QL;->a(Ljava/util/Map;LX/6f4;Z)V

    .line 564119
    iget-object v0, p0, LX/3QL;->a:LX/00H;

    .line 564120
    iget-object v2, v0, LX/00H;->i:LX/01S;

    move-object v0, v2

    .line 564121
    sget-object v2, LX/01S;->PUBLIC:LX/01S;

    if-eq v0, v2, :cond_1

    .line 564122
    const-string v0, "stack_trace"

    invoke-static {p5}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564123
    :cond_0
    :goto_0
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v2, "failed"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564124
    return-void

    .line 564125
    :cond_1
    invoke-virtual {p5}, LX/FKG;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/NullPointerException;

    if-eqz v0, :cond_0

    .line 564126
    const-string v0, "stack_trace"

    invoke-virtual {p5}, LX/FKG;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-static {v2}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;Ljava/lang/String;IZLX/1Mb;LX/1Mb;ZIZ)V
    .locals 8
    .param p8    # LX/1Mb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/1Mb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 564127
    iget-object v2, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 564128
    const-string v1, "messaging_send_via_mqtt"

    .line 564129
    const/16 v0, 0x26

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "offline_threading_id"

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget-object v4, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v4, v3, v0

    const/4 v0, 0x2

    const-string v4, "sent_timestamp_ms"

    aput-object v4, v3, v0

    const/4 v0, 0x3

    iget-wide v4, v2, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x4

    const-string v4, "send_time_delta"

    aput-object v4, v3, v0

    const/4 v0, 0x5

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x6

    const-string v4, "error_message"

    aput-object v4, v3, v0

    const/4 v0, 0x7

    aput-object p5, v3, v0

    const/16 v0, 0x8

    const-string v4, "error_number"

    aput-object v4, v3, v0

    const/16 v0, 0x9

    invoke-static {p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0xa

    const-string v4, "initial_mqtt_push_state"

    aput-object v4, v3, v0

    const/16 v4, 0xb

    if-eqz p8, :cond_0

    invoke-virtual/range {p8 .. p8}, LX/1Mb;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    const/16 v0, 0xc

    const-string v4, "mqtt_push_state"

    aput-object v4, v3, v0

    const/16 v4, 0xd

    if-eqz p9, :cond_1

    invoke-virtual/range {p9 .. p9}, LX/1Mb;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v3, v4

    const/16 v0, 0xe

    const-string v4, "network_connected"

    aput-object v4, v3, v0

    const/16 v0, 0xf

    invoke-static/range {p10 .. p10}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x10

    const-string v4, "current_time"

    aput-object v4, v3, v0

    const/16 v0, 0x11

    invoke-static {p0}, LX/3QL;->a(LX/3QL;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x12

    const-string v4, "mqtt_back_to_back_attempt_number"

    aput-object v4, v3, v0

    const/16 v0, 0x13

    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x14

    const-string v4, "is_retriable"

    aput-object v4, v3, v0

    const/16 v4, 0x15

    if-nez p7, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    const/16 v0, 0x16

    const-string v4, "showed_optimistic_send"

    aput-object v4, v3, v0

    const/16 v0, 0x17

    invoke-static/range {p12 .. p12}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x18

    const-string v4, "channel"

    aput-object v4, v3, v0

    const/16 v0, 0x19

    sget-object v4, LX/FCY;->MQTT:LX/FCY;

    iget-object v4, v4, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v4, v3, v0

    const/16 v0, 0x1a

    const-string v4, "netcheck_state"

    aput-object v4, v3, v0

    const/16 v0, 0x1b

    iget-object v4, p0, LX/3QL;->b:LX/2EL;

    invoke-virtual {v4}, LX/2EL;->c()LX/2EQ;

    move-result-object v4

    invoke-virtual {v4}, LX/2EQ;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x1c

    const-string v4, "last_netcheck_time"

    aput-object v4, v3, v0

    const/16 v0, 0x1d

    iget-object v4, p0, LX/3QL;->b:LX/2EL;

    invoke-virtual {v4}, LX/2EL;->d()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x1e

    const-string v4, "retry_count"

    aput-object v4, v3, v0

    const/16 v0, 0x1f

    iget v4, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x20

    const-string v4, "first_send_delta"

    aput-object v4, v3, v0

    const/16 v0, 0x21

    iget-wide v4, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->e:J

    invoke-static {p0, v4, v5}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x22

    const-string v4, "first_message_first_send_delta"

    aput-object v4, v3, v0

    const/16 v0, 0x23

    iget-wide v4, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->f:J

    invoke-static {p0, v4, v5}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x24

    const-string v4, "message_type"

    aput-object v4, v3, v0

    const/16 v0, 0x25

    iget-object v4, p0, LX/3QL;->h:LX/2Ly;

    invoke-virtual {v4, v2}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v3}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 564130
    iget-object v0, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v3, v0}, LX/3QL;->a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564131
    const/4 v0, 0x0

    invoke-static {v3, p4, v0}, LX/3QL;->a(Ljava/util/Map;LX/6f4;Z)V

    .line 564132
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v2, "failed"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564133
    return-void

    .line 564134
    :cond_0
    const-string v0, "UNKNOWN"

    goto/16 :goto_0

    :cond_1
    const-string v0, "UNKNOWN"

    goto/16 :goto_1

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;Ljava/lang/String;IZLX/1Mb;LX/1Mb;ZLX/FCY;Z)V
    .locals 10
    .param p8    # LX/1Mb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/1Mb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 564135
    iget-object v2, p0, LX/3QL;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 564136
    :goto_0
    return-void

    .line 564137
    :cond_0
    iget-object v2, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 564138
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "messaging_send_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 564139
    invoke-static {v2}, LX/3QL;->a(Lcom/facebook/messaging/model/messages/Message;)LX/0P1;

    move-result-object v5

    const/16 v6, 0x1a

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "offline_threading_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "sent_timestamp_ms"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    iget-wide v8, v2, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "send_time_delta"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "current_time"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    invoke-static {p0}, LX/3QL;->a(LX/3QL;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string v8, "mqtt_back_to_back_attempt_number"

    aput-object v8, v6, v7

    const/16 v7, 0x9

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xa

    const-string v8, "showed_optimistic_send"

    aput-object v8, v6, v7

    const/16 v7, 0xb

    invoke-static/range {p7 .. p7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xc

    const-string v8, "channel"

    aput-object v8, v6, v7

    const/16 v7, 0xd

    move-object/from16 v0, p11

    iget-object v8, v0, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0xe

    const-string v8, "network_connected"

    aput-object v8, v6, v7

    const/16 v7, 0xf

    invoke-static/range {p10 .. p10}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x10

    const-string v8, "retry_count"

    aput-object v8, v6, v7

    const/16 v7, 0x11

    iget v8, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->d:I

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x12

    const-string v8, "first_send_delta"

    aput-object v8, v6, v7

    const/16 v7, 0x13

    iget-wide v8, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->e:J

    invoke-static {p0, v8, v9}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x14

    const-string v8, "first_message_first_send_delta"

    aput-object v8, v6, v7

    const/16 v7, 0x15

    iget-wide v8, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->f:J

    invoke-static {p0, v8, v9}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x16

    const-string v8, "success_from_delta"

    aput-object v8, v6, v7

    const/16 v7, 0x17

    invoke-static/range {p12 .. p12}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x18

    const-string v8, "message_type"

    aput-object v8, v6, v7

    const/16 v7, 0x19

    iget-object v8, p0, LX/3QL;->h:LX/2Ly;

    invoke-virtual {v8, v2}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, LX/29E;->a(Ljava/util/Map;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 564140
    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v5, v2}, LX/3QL;->a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564141
    const/4 v2, 0x0

    invoke-static {v5, p4, v2}, LX/3QL;->a(Ljava/util/Map;LX/6f4;Z)V

    .line 564142
    if-eqz p8, :cond_1

    .line 564143
    const-string v2, "initial_mqtt_push_state"

    invoke-virtual/range {p8 .. p8}, LX/1Mb;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564144
    :cond_1
    if-eqz p9, :cond_2

    .line 564145
    const-string v2, "mqtt_push_state"

    invoke-virtual/range {p9 .. p9}, LX/1Mb;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564146
    :cond_2
    iget-object v2, p0, LX/3QL;->f:LX/2bC;

    const-string v4, "success"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;Ljava/lang/String;ILX/1Mb;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 564147
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 564148
    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "offline_threading_id"

    aput-object v3, v1, v6

    const/4 v3, 0x1

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v0, v1, v3

    const/4 v0, 0x2

    const-string v3, "reason"

    aput-object v3, v1, v0

    const/4 v0, 0x3

    aput-object p3, v1, v0

    const/4 v0, 0x4

    const-string v3, "mqtt_back_to_back_attempt_number"

    aput-object v3, v1, v0

    const/4 v0, 0x5

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    const/4 v0, 0x6

    const-string v3, "initial_mqtt_push_state"

    aput-object v3, v1, v0

    const/4 v0, 0x7

    invoke-virtual {p5}, LX/1Mb;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    const/16 v0, 0x8

    const-string v3, "network_connected"

    aput-object v3, v1, v0

    const/16 v0, 0x9

    invoke-static {p6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    const/16 v0, 0xa

    const-string v3, "channel"

    aput-object v3, v1, v0

    const/16 v0, 0xb

    sget-object v3, LX/FCY;->MQTT:LX/FCY;

    iget-object v3, v3, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v3, v1, v0

    const/16 v0, 0xc

    const-string v3, "retry_count"

    aput-object v3, v1, v0

    const/16 v0, 0xd

    iget v3, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    const/16 v0, 0xe

    const-string v3, "first_send_delta"

    aput-object v3, v1, v0

    const/16 v0, 0xf

    iget-wide v4, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->e:J

    invoke-static {p0, v4, v5}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    const/16 v0, 0x10

    const-string v3, "first_message_first_send_delta"

    aput-object v3, v1, v0

    const/16 v0, 0x11

    iget-wide v4, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->f:J

    invoke-static {p0, v4, v5}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    invoke-static {v1}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 564149
    invoke-static {v3, p2, v6}, LX/3QL;->a(Ljava/util/Map;LX/6f4;Z)V

    .line 564150
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v1, "msg_cant_send_via_mqtt"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564151
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;ZLX/FCY;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 564152
    iget-object v0, p0, LX/3QL;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564153
    :goto_0
    return-void

    .line 564154
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 564155
    const/16 v1, 0x10

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "offline_threading_id"

    aput-object v4, v1, v3

    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v3, v1, v6

    const/4 v3, 0x2

    const-string v4, "current_time"

    aput-object v4, v1, v3

    const/4 v3, 0x3

    invoke-static {p0}, LX/3QL;->a(LX/3QL;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x4

    const-string v4, "network_connected"

    aput-object v4, v1, v3

    const/4 v3, 0x5

    invoke-static {p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x6

    const-string v4, "channel"

    aput-object v4, v1, v3

    const/4 v3, 0x7

    iget-object v4, p4, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v4, v1, v3

    const/16 v3, 0x8

    const-string v4, "retry_count"

    aput-object v4, v1, v3

    const/16 v3, 0x9

    iget v4, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/16 v3, 0xa

    const-string v4, "first_send_delta"

    aput-object v4, v1, v3

    const/16 v3, 0xb

    iget-wide v4, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->e:J

    invoke-static {p0, v4, v5}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/16 v3, 0xc

    const-string v4, "first_message_first_send_delta"

    aput-object v4, v1, v3

    const/16 v3, 0xd

    iget-wide v4, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->f:J

    invoke-static {p0, v4, v5}, LX/3QL;->a(LX/3QL;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/16 v3, 0xe

    const-string v4, "message_type"

    aput-object v4, v1, v3

    const/16 v3, 0xf

    iget-object v4, p0, LX/3QL;->h:LX/2Ly;

    invoke-virtual {v4, v0}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v1}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 564156
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v3, v0}, LX/3QL;->a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564157
    invoke-static {v3, p2, v6}, LX/3QL;->a(Ljava/util/Map;LX/6f4;Z)V

    .line 564158
    iget-object v0, p0, LX/3QL;->c:LX/29E;

    invoke-virtual {v0, v3}, LX/29E;->a(Ljava/util/Map;)V

    .line 564159
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v1, "msg_send_attempted"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;JLX/6f4;Ljava/lang/String;IZLX/FCY;)V
    .locals 10

    .prologue
    .line 564160
    iget-object v2, p0, LX/3QL;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 564161
    :goto_0
    return-void

    .line 564162
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 564163
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "messaging_send_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 564164
    invoke-static {v2}, LX/3QL;->a(Lcom/facebook/messaging/model/messages/Message;)LX/0P1;

    move-result-object v5

    const/16 v6, 0x10

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "offline_threading_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "sent_timestamp_ms"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    iget-wide v8, v2, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "send_time_delta"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "current_time"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    invoke-static {p0}, LX/3QL;->a(LX/3QL;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string v8, "mqtt_back_to_back_attempt_number"

    aput-object v8, v6, v7

    const/16 v7, 0x9

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xa

    const-string v8, "channel"

    aput-object v8, v6, v7

    const/16 v7, 0xb

    move-object/from16 v0, p8

    iget-object v8, v0, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/16 v7, 0xc

    const-string v8, "network_connected"

    aput-object v8, v6, v7

    const/16 v7, 0xd

    invoke-static/range {p7 .. p7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xe

    const-string v8, "message_type"

    aput-object v8, v6, v7

    const/16 v7, 0xf

    iget-object v8, p0, LX/3QL;->h:LX/2Ly;

    invoke-virtual {v8, v2}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, LX/29E;->a(Ljava/util/Map;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 564165
    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v5, v2}, LX/3QL;->a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564166
    const/4 v2, 0x0

    invoke-static {v5, p4, v2}, LX/3QL;->a(Ljava/util/Map;LX/6f4;Z)V

    .line 564167
    iget-object v2, p0, LX/3QL;->f:LX/2bC;

    const-string v4, "success"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;LX/6f4;ZLX/FCY;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 564168
    iget-object v0, p0, LX/3QL;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564169
    :goto_0
    return-void

    .line 564170
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 564171
    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "offline_threading_id"

    aput-object v4, v1, v3

    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v3, v1, v5

    const/4 v3, 0x2

    const-string v4, "current_time"

    aput-object v4, v1, v3

    const/4 v3, 0x3

    invoke-static {p0}, LX/3QL;->a(LX/3QL;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x4

    const-string v4, "network_connected"

    aput-object v4, v1, v3

    const/4 v3, 0x5

    invoke-static {p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x6

    const-string v4, "channel"

    aput-object v4, v1, v3

    const/4 v3, 0x7

    iget-object v4, p4, LX/FCY;->channelName:Ljava/lang/String;

    aput-object v4, v1, v3

    const/16 v3, 0x8

    const-string v4, "message_type"

    aput-object v4, v1, v3

    const/16 v3, 0x9

    iget-object v4, p0, LX/3QL;->h:LX/2Ly;

    invoke-virtual {v4, v0}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v1}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 564172
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v3, v0}, LX/3QL;->a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564173
    invoke-static {v3, p2, v5}, LX/3QL;->a(Ljava/util/Map;LX/6f4;Z)V

    .line 564174
    iget-object v0, p0, LX/3QL;->c:LX/29E;

    invoke-virtual {v0, v3}, LX/29E;->a(Ljava/util/Map;)V

    .line 564175
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v1, "msg_send_attempted"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p2    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 564176
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, LX/3QL;->a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 564177
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 7
    .param p2    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 564095
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {v0}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    .line 564096
    if-eqz p6, :cond_0

    .line 564097
    invoke-interface {v4, p6}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 564098
    :cond_0
    invoke-static {v4, p2}, LX/3QL;->a(Ljava/util/Map;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564099
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v5, "message_id"

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 564100
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 564178
    iget-object v0, p0, LX/3QL;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564179
    :goto_0
    return-void

    .line 564180
    :cond_0
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v1, "messaging_received_delay"

    const/16 v3, 0x8

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "earlier_source"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const/4 v4, 0x2

    const-string v5, "later_source"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p2, v3, v4

    const/4 v4, 0x4

    const-string v5, "delay_ms"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "message_id"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    aput-object p5, v3, v4

    invoke-static {v3}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 564184
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "offline_threading_id"

    aput-object v3, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, LX/3QL;->d(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x2

    const-string v3, "batch_size"

    aput-object v3, v0, v1

    const/4 v1, 0x3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v0}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 564185
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v1, "msg_send_batch_attempted"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564186
    return-void
.end method

.method public final a(Ljava/util/List;IILjava/lang/String;ILjava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;II",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 564181
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "offline_threading_id"

    aput-object v3, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, LX/3QL;->d(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x2

    const-string v3, "batch_result"

    aput-object v3, v0, v1

    const/4 v1, 0x3

    const-string v3, "success_mqtt"

    aput-object v3, v0, v1

    const/4 v1, 0x4

    const-string v3, "batch_result_size"

    aput-object v3, v0, v1

    const/4 v1, 0x5

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x6

    const-string v3, "batch_success_count"

    aput-object v3, v0, v1

    const/4 v1, 0x7

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x8

    const-string v3, "batch_success_list"

    aput-object v3, v0, v1

    const/16 v1, 0x9

    aput-object p4, v0, v1

    const/16 v1, 0xa

    const-string v3, "batch_failure_list"

    aput-object v3, v0, v1

    const/16 v1, 0xb

    aput-object p6, v0, v1

    const/16 v1, 0xc

    const-string v3, "batch_failure_count"

    aput-object v3, v0, v1

    const/16 v1, 0xd

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v0}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 564182
    iget-object v0, p0, LX/3QL;->f:LX/2bC;

    const-string v1, "msg_send_batch_responded"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564183
    return-void
.end method
