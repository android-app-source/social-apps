.class public LX/2RD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[Ljava/lang/String;

.field private static volatile e:LX/2RD;


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:Landroid/content/pm/PackageManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 409650
    const-class v0, LX/2RD;

    sput-object v0, LX/2RD;->a:Ljava/lang/Class;

    .line 409651
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.facebook.orca"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.facebook.katana"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.facebook.wakizashi"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "com.facebook.lite"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.facebook.pages.app"

    aput-object v2, v0, v1

    sput-object v0, LX/2RD;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409653
    iput-object p1, p0, LX/2RD;->c:Landroid/content/Context;

    .line 409654
    iput-object p2, p0, LX/2RD;->d:Landroid/content/pm/PackageManager;

    .line 409655
    return-void
.end method

.method public static a(LX/0QB;)LX/2RD;
    .locals 5

    .prologue
    .line 409656
    sget-object v0, LX/2RD;->e:LX/2RD;

    if-nez v0, :cond_1

    .line 409657
    const-class v1, LX/2RD;

    monitor-enter v1

    .line 409658
    :try_start_0
    sget-object v0, LX/2RD;->e:LX/2RD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 409659
    if-eqz v2, :cond_0

    .line 409660
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 409661
    new-instance p0, LX/2RD;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageManager;

    invoke-direct {p0, v3, v4}, LX/2RD;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;)V

    .line 409662
    move-object v0, p0

    .line 409663
    sput-object v0, LX/2RD;->e:LX/2RD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409664
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 409665
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 409666
    :cond_1
    sget-object v0, LX/2RD;->e:LX/2RD;

    return-object v0

    .line 409667
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 409668
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 409669
    sget-object v2, LX/2RD;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 409670
    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 409671
    const/4 v0, 0x1

    .line 409672
    :cond_0
    return v0

    .line 409673
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 5

    .prologue
    .line 409674
    iget-object v0, p0, LX/2RD;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2RD;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 409675
    const-class v0, Lcom/facebook/push/crossapp/PackageFullyRemovedBroadcastReceiver;

    .line 409676
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, LX/2RD;->c:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 409677
    iget-object v2, p0, LX/2RD;->d:Landroid/content/pm/PackageManager;

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 409678
    :cond_0
    return-void
.end method
