.class public LX/2PK;
.super LX/2PI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2PI",
        "<",
        "LX/2PD;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2PK;


# instance fields
.field public final c:LX/2PJ;

.field public final d:LX/0SF;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 406287
    const-class v0, LX/2PK;

    sput-object v0, LX/2PK;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2PJ;LX/0SF;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2PJ;",
            "LX/0SF;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406288
    const/16 v0, 0xd

    invoke-direct {p0, v0}, LX/2PI;-><init>(I)V

    .line 406289
    iput-object p1, p0, LX/2PK;->c:LX/2PJ;

    .line 406290
    iput-object p2, p0, LX/2PK;->d:LX/0SF;

    .line 406291
    iput-object p3, p0, LX/2PK;->e:LX/0Or;

    .line 406292
    return-void
.end method

.method public static a(LX/0QB;)LX/2PK;
    .locals 6

    .prologue
    .line 406293
    sget-object v0, LX/2PK;->f:LX/2PK;

    if-nez v0, :cond_1

    .line 406294
    const-class v1, LX/2PK;

    monitor-enter v1

    .line 406295
    :try_start_0
    sget-object v0, LX/2PK;->f:LX/2PK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406296
    if-eqz v2, :cond_0

    .line 406297
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406298
    new-instance v5, LX/2PK;

    invoke-static {v0}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v3

    check-cast v3, LX/2PJ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SF;

    const/16 p0, 0x15e8

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2PK;-><init>(LX/2PJ;LX/0SF;LX/0Or;)V

    .line 406299
    move-object v0, v5

    .line 406300
    sput-object v0, LX/2PK;->f:LX/2PK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406301
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406302
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406303
    :cond_1
    sget-object v0, LX/2PK;->f:LX/2PK;

    return-object v0

    .line 406304
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406305
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static c()V
    .locals 2

    .prologue
    .line 406306
    sget-object v0, LX/2PK;->b:Ljava/lang/Class;

    const-string v1, "Could not deserialise LookupResponsePayload"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 406307
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 406308
    return-void
.end method

.method public final a(LX/Dph;)V
    .locals 6

    .prologue
    .line 406309
    if-nez p1, :cond_1

    .line 406310
    invoke-static {}, LX/2PK;->c()V

    .line 406311
    :cond_0
    :goto_0
    return-void

    .line 406312
    :cond_1
    iget-object v0, p1, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    .line 406313
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PD;

    .line 406314
    iget-object v2, p1, LX/Dph;->nonce:[B

    invoke-interface {v0, v2}, LX/2PD;->a([B)V

    goto :goto_1

    .line 406315
    :cond_2
    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    if-eqz v0, :cond_3

    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/6kT;->a(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 406316
    :cond_3
    invoke-static {}, LX/2PK;->c()V

    goto :goto_0

    .line 406317
    :cond_4
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PD;

    .line 406318
    iget-object v2, p1, LX/Dph;->nonce:[B

    iget-object v3, p1, LX/Dph;->date_micros:Ljava/lang/Long;

    iget-object v4, p1, LX/Dph;->body:LX/Dpi;

    invoke-virtual {v4}, LX/Dpi;->d()LX/DpK;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v0, v2, v3, v4, v5}, LX/2PD;->a([BLjava/lang/Long;LX/DpK;Z)V

    goto :goto_2
.end method
