.class public LX/2tn;
.super LX/0ux;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0ux;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3CL;)V
    .locals 1

    .prologue
    .line 475253
    invoke-direct {p0}, LX/0ux;-><init>()V

    .line 475254
    iget-object v0, p1, LX/3CL;->a:Ljava/lang/String;

    iput-object v0, p0, LX/2tn;->a:Ljava/lang/String;

    .line 475255
    iget-object v0, p1, LX/3CL;->b:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/2tn;->b:LX/0Px;

    .line 475256
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 475258
    iget-object v0, p0, LX/2tn;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475259
    const-string v0, ""

    .line 475260
    :goto_0
    return-object v0

    .line 475261
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 475262
    const-string v0, "%1$s = CASE "

    iget-object v1, p0, LX/2tn;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475263
    iget-object v0, p0, LX/2tn;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/2tn;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ux;

    .line 475264
    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475265
    const-string v0, " "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475266
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 475267
    :cond_1
    const-string v0, "END"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475268
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 475269
    invoke-virtual {p0}, LX/2tn;->c()Ljava/lang/Iterable;

    move-result-object v0

    .line 475270
    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 475257
    iget-object v0, p0, LX/2tn;->b:LX/0Px;

    new-instance v1, LX/3Fq;

    invoke-direct {v1, p0}, LX/3Fq;-><init>(LX/2tn;)V

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0Ph;->f(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
