.class public LX/2Pb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Pb;


# instance fields
.field public a:LX/0Zb;

.field public b:LX/0oz;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0oz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406758
    iput-object p1, p0, LX/2Pb;->a:LX/0Zb;

    .line 406759
    iput-object p2, p0, LX/2Pb;->b:LX/0oz;

    .line 406760
    return-void
.end method

.method public static a(LX/0QB;)LX/2Pb;
    .locals 5

    .prologue
    .line 406761
    sget-object v0, LX/2Pb;->c:LX/2Pb;

    if-nez v0, :cond_1

    .line 406762
    const-class v1, LX/2Pb;

    monitor-enter v1

    .line 406763
    :try_start_0
    sget-object v0, LX/2Pb;->c:LX/2Pb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406764
    if-eqz v2, :cond_0

    .line 406765
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406766
    new-instance p0, LX/2Pb;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v4

    check-cast v4, LX/0oz;

    invoke-direct {p0, v3, v4}, LX/2Pb;-><init>(LX/0Zb;LX/0oz;)V

    .line 406767
    move-object v0, p0

    .line 406768
    sput-object v0, LX/2Pb;->c:LX/2Pb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406769
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406770
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406771
    :cond_1
    sget-object v0, LX/2Pb;->c:LX/2Pb;

    return-object v0

    .line 406772
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406773
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Pb;LX/8wV;LX/8wW;Lcom/facebook/adinterfaces/external/AdInterfacesEventData;Ljava/util/Map;LX/03V;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8wV;",
            "LX/8wW;",
            "Lcom/facebook/adinterfaces/external/AdInterfacesEventData;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 406774
    :try_start_0
    iget-object v0, p0, LX/2Pb;->a:LX/0Zb;

    invoke-virtual {p2}, LX/8wW;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 406775
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 406776
    invoke-virtual {p1}, LX/8wV;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 406777
    if-eqz p3, :cond_0

    .line 406778
    const-string v5, "ad_account_id"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->adAccountId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406779
    const-string v5, "ad_status"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->adStatus:Ljava/lang/Object;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 406780
    const-string v5, "audience_id"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->audienceOption:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406781
    const-string v5, "budget_type"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->budgetType:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406782
    const-string v5, "budget"

    iget-wide v7, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->budget:J

    invoke-virtual {v1, v5, v7, v8}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 406783
    const-string v5, "connection_quality_class"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->connectionQualityClass:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406784
    const-string v5, "currency"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->currency:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406785
    const-string v5, "duration"

    iget v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->duration:I

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 406786
    const-string v5, "end_time"

    iget-wide v7, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->endTime:J

    invoke-virtual {v1, v5, v7, v8}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 406787
    const-string v5, "flow"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->flow:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406788
    const-string v5, "flow_id"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->flowId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406789
    const-string v5, "lower_estimate"

    iget v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->lowerEstimate:I

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 406790
    const-string v5, "page_id"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->pageId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406791
    const-string v5, "placement"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->placement:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406792
    const-string v5, "saved_audience_id"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->savedAudienceId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406793
    const-string v5, "start_time"

    iget-wide v7, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->startTime:J

    invoke-virtual {v1, v5, v7, v8}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 406794
    const-string v5, "story_graphql_id"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->storyId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406795
    const-string v5, "targeting_spec"

    iget-object v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->targetingSpec:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406796
    const-string v5, "upper_estimate"

    iget v6, p3, Lcom/facebook/adinterfaces/external/AdInterfacesEventData;->upperEstimate:I

    invoke-virtual {v1, v5, v6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 406797
    :cond_0
    if-eqz p4, :cond_2

    .line 406798
    invoke-interface {p4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 406799
    invoke-interface {p4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 406800
    :catch_0
    move-exception v0

    .line 406801
    const-string v1, "adinterface_logging_error"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error logging event "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/8wW;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 406802
    iput v4, v1, LX/0VK;->e:I

    .line 406803
    move-object v1, v1

    .line 406804
    iput-object v0, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 406805
    move-object v0, v1

    .line 406806
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 406807
    invoke-virtual {p5, v0}, LX/03V;->a(LX/0VG;)V

    .line 406808
    :cond_1
    :goto_1
    return-void

    .line 406809
    :cond_2
    :try_start_1
    invoke-virtual {v1}, LX/0oG;->d()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/8wV;LX/8wW;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 406810
    :try_start_0
    iget-object v0, p0, LX/2Pb;->a:LX/0Zb;

    invoke-virtual {p2}, LX/8wW;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 406811
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 406812
    invoke-virtual {p1}, LX/8wV;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 406813
    const-string v1, "placement"

    invoke-virtual {v0, v1, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406814
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406815
    const-string v1, "connection_quality_class"

    iget-object v2, p0, LX/2Pb;->b:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    invoke-virtual {v2}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406816
    invoke-virtual {v0}, LX/0oG;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 406817
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final a(LX/8wV;LX/8wW;Ljava/util/Map;LX/03V;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8wV;",
            "LX/8wW;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 406818
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/2Pb;->a(LX/2Pb;LX/8wV;LX/8wW;Lcom/facebook/adinterfaces/external/AdInterfacesEventData;Ljava/util/Map;LX/03V;)V

    .line 406819
    return-void
.end method

.method public final a(LX/8wW;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 406820
    :try_start_0
    iget-object v0, p0, LX/2Pb;->a:LX/0Zb;

    invoke-virtual {p1}, LX/8wW;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 406821
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 406822
    sget-object v1, LX/8wV;->PROMOTE_CTA_MOBILE_MODULE:LX/8wV;

    invoke-virtual {v1}, LX/8wV;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 406823
    const-string v1, "placement"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406824
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406825
    const-string v1, "connection_quality_class"

    iget-object v2, p0, LX/2Pb;->b:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    invoke-virtual {v2}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406826
    const-string v1, "flow"

    const-string v2, "share_cta"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 406827
    invoke-virtual {v0}, LX/0oG;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 406828
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
