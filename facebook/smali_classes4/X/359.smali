.class public LX/359;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/35C;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/359",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/35C;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 496724
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 496725
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/359;->b:LX/0Zi;

    .line 496726
    iput-object p1, p0, LX/359;->a:LX/0Ot;

    .line 496727
    return-void
.end method

.method public static a(LX/0QB;)LX/359;
    .locals 4

    .prologue
    .line 496728
    const-class v1, LX/359;

    monitor-enter v1

    .line 496729
    :try_start_0
    sget-object v0, LX/359;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 496730
    sput-object v2, LX/359;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 496731
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496732
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 496733
    new-instance v3, LX/359;

    const/16 p0, 0x3ab

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/359;-><init>(LX/0Ot;)V

    .line 496734
    move-object v0, v3

    .line 496735
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 496736
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/359;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496737
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 496738
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 496739
    invoke-static {}, LX/1dS;->b()V

    .line 496740
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 9

    .prologue
    const/16 v0, 0x8

    const/16 v1, 0x1e

    const v2, 0x4ff83fb3    # 8.3298483E9f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 496753
    check-cast p6, LX/35A;

    .line 496754
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 496755
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 496756
    iget-object v0, p0, LX/359;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v4, p6, LX/35A;->e:LX/1dc;

    iget v5, p6, LX/35A;->f:I

    move-object v0, p1

    move v1, p3

    move v2, p4

    move-object v3, p5

    invoke-static/range {v0 .. v7}, LX/35C;->a(LX/1De;IILX/1no;LX/1dc;ILX/1np;LX/1np;)V

    .line 496757
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 496758
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p6, LX/35A;->g:Ljava/lang/Integer;

    .line 496759
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 496760
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 496761
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p6, LX/35A;->h:Ljava/lang/Integer;

    .line 496762
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 496763
    const/16 v0, 0x8

    const/16 v1, 0x1f

    const v2, 0x151e29b2

    invoke-static {v0, v1, v2, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 496741
    iget-object v0, p0, LX/359;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 496742
    new-instance v0, LX/35E;

    invoke-direct {v0}, LX/35E;-><init>()V

    move-object v0, v0

    .line 496743
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 5

    .prologue
    .line 496744
    check-cast p2, LX/35A;

    .line 496745
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 496746
    iget-object v0, p0, LX/359;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35C;

    iget-object v2, p2, LX/35A;->a:LX/1Pr;

    iget-object v3, p2, LX/35A;->b:Ljava/lang/String;

    iget-object v4, p2, LX/35A;->c:LX/0jW;

    .line 496747
    new-instance p0, LX/35D;

    iget-object p1, v0, LX/35C;->a:LX/216;

    invoke-direct {p0, v3, p1}, LX/35D;-><init>(Ljava/lang/String;LX/216;)V

    invoke-interface {v2, p0, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object p0

    .line 496748
    iput-object p0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 496749
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 496750
    check-cast v0, LX/0wd;

    iput-object v0, p2, LX/35A;->d:LX/0wd;

    .line 496751
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 496752
    return-void
.end method

.method public final c(LX/1De;)LX/35B;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/359",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 496687
    new-instance v1, LX/35A;

    invoke-direct {v1, p0}, LX/35A;-><init>(LX/359;)V

    .line 496688
    iget-object v2, p0, LX/359;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/35B;

    .line 496689
    if-nez v2, :cond_0

    .line 496690
    new-instance v2, LX/35B;

    invoke-direct {v2, p0}, LX/35B;-><init>(LX/359;)V

    .line 496691
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/35B;->a$redex0(LX/35B;LX/1De;IILX/35A;)V

    .line 496692
    move-object v1, v2

    .line 496693
    move-object v0, v1

    .line 496694
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 496723
    const/4 v0, 0x1

    return v0
.end method

.method public final c(LX/1X1;LX/1X1;)Z
    .locals 2

    .prologue
    .line 496713
    check-cast p1, LX/35A;

    .line 496714
    check-cast p2, LX/35A;

    .line 496715
    iget-object v0, p1, LX/35A;->e:LX/1dc;

    iget-object v1, p2, LX/35A;->e:LX/1dc;

    invoke-static {v0, v1}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v0

    .line 496716
    iget-object v1, p0, LX/359;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 496717
    iget-object v1, v0, LX/3lz;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 496718
    check-cast v1, LX/1dc;

    .line 496719
    iget-object p0, v0, LX/3lz;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 496720
    check-cast p0, LX/1dc;

    invoke-static {v1, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v1

    move v1, v1

    .line 496721
    invoke-static {v0}, LX/1cy;->a(LX/3lz;)V

    .line 496722
    return v1
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 7

    .prologue
    .line 496704
    check-cast p3, LX/35A;

    .line 496705
    iget-object v0, p0, LX/359;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v1, p2

    check-cast v1, LX/35E;

    iget-object v2, p3, LX/35A;->e:LX/1dc;

    iget v3, p3, LX/35A;->i:I

    iget-object v4, p3, LX/35A;->d:LX/0wd;

    iget-object v5, p3, LX/35A;->g:Ljava/lang/Integer;

    iget-object v6, p3, LX/35A;->h:Ljava/lang/Integer;

    move-object v0, p1

    const/4 p0, 0x0

    .line 496706
    iput v3, v1, LX/35E;->f:I

    .line 496707
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :goto_0
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result p0

    .line 496708
    :cond_0
    iput p1, v1, LX/35E;->g:I

    .line 496709
    iput p0, v1, LX/35E;->h:I

    .line 496710
    invoke-static {v0, v2}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p0, v4}, LX/35E;->a(Landroid/graphics/drawable/Drawable;LX/0wd;)V

    .line 496711
    return-void

    :cond_1
    move p1, p0

    .line 496712
    goto :goto_0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 496703
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 496698
    check-cast p3, LX/35A;

    .line 496699
    iget-object v0, p0, LX/359;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, LX/35E;

    iget-object v0, p3, LX/35A;->e:LX/1dc;

    const/4 p3, 0x0

    .line 496700
    invoke-virtual {p2}, LX/1ah;->a()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-static {p1, p0, v0}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 496701
    invoke-virtual {p2, p3, p3}, LX/35E;->a(Landroid/graphics/drawable/Drawable;LX/0wd;)V

    .line 496702
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 496697
    const/4 v0, 0x1

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 496696
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 496695
    const/16 v0, 0xf

    return v0
.end method
