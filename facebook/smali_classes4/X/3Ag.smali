.class public LX/3Ag;
.super Landroid/app/Dialog;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Ljava/lang/reflect/Field;

.field private d:Ljava/lang/reflect/Field;

.field private e:Ljava/lang/reflect/Field;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 525898
    new-instance v0, LX/31Z;

    invoke-direct {v0, p1}, LX/31Z;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 525899
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Ag;->f:Z

    .line 525900
    invoke-direct {p0}, LX/3Ag;->a()V

    .line 525901
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 525894
    new-instance v0, LX/31Z;

    invoke-direct {v0, p1}, LX/31Z;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 525895
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Ag;->f:Z

    .line 525896
    invoke-direct {p0}, LX/3Ag;->a()V

    .line 525897
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 525880
    const-class v0, LX/3Ag;

    invoke-static {v0, p0}, LX/3Ag;->a(Ljava/lang/Class;Landroid/app/Dialog;)V

    .line 525881
    iget-object v0, p0, LX/3Ag;->a:LX/0ad;

    sget-short v1, LX/3ie;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 525882
    if-eqz v0, :cond_0

    .line 525883
    const-class v0, Landroid/app/Dialog;

    .line 525884
    :try_start_0
    const-string v1, "mCancelMessage"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, p0, LX/3Ag;->c:Ljava/lang/reflect/Field;

    .line 525885
    const-string v1, "mDismissMessage"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, p0, LX/3Ag;->d:Ljava/lang/reflect/Field;

    .line 525886
    const-string v1, "mShowMessage"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, LX/3Ag;->e:Ljava/lang/reflect/Field;

    .line 525887
    iget-object v0, p0, LX/3Ag;->c:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 525888
    iget-object v0, p0, LX/3Ag;->d:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 525889
    iget-object v0, p0, LX/3Ag;->e:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 525890
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3Ag;->f:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 525891
    :cond_0
    :goto_0
    return-void

    .line 525892
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 525893
    iget-object v0, p0, LX/3Ag;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "FbDialog"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/app/Dialog;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Dialog;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/3Ag;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    const/16 p0, 0x259

    invoke-static {v2, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, LX/3Ag;->a:LX/0ad;

    iput-object v2, p1, LX/3Ag;->b:LX/0Ot;

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 3

    .prologue
    .line 525862
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 525863
    iget-boolean v0, p0, LX/3Ag;->f:Z

    if-eqz v0, :cond_2

    .line 525864
    :try_start_0
    iget-object v0, p0, LX/3Ag;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 525865
    if-eqz v0, :cond_0

    .line 525866
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    .line 525867
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3Ag;->setCancelMessage(Landroid/os/Message;)V

    .line 525868
    :cond_0
    iget-object v0, p0, LX/3Ag;->d:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 525869
    if-eqz v0, :cond_1

    .line 525870
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    .line 525871
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3Ag;->setDismissMessage(Landroid/os/Message;)V

    .line 525872
    :cond_1
    iget-object v0, p0, LX/3Ag;->e:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 525873
    if-eqz v0, :cond_2

    .line 525874
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    .line 525875
    iget-object v0, p0, LX/3Ag;->e:Ljava/lang/reflect/Field;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 525876
    :cond_2
    :goto_0
    return-void

    .line 525877
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 525878
    :goto_1
    iget-object v0, p0, LX/3Ag;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "FbDialog"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 525879
    :catch_1
    move-exception v0

    move-object v1, v0

    goto :goto_1
.end method

.method public show()V
    .locals 0

    .prologue
    .line 525859
    invoke-static {p0}, LX/4md;->a(Landroid/app/Dialog;)V

    .line 525860
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 525861
    return-void
.end method
