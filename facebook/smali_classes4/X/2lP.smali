.class public LX/2lP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/ComponentName;

.field private final b:LX/2lR;

.field private c:Lcom/facebook/content/SecureContextHelper;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/ComponentName;LX/2lR;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .param p1    # Landroid/content/ComponentName;
        .annotation runtime Lcom/facebook/katana/login/LogoutActivityComponent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 457897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457898
    iput-object p1, p0, LX/2lP;->a:Landroid/content/ComponentName;

    .line 457899
    iput-object p2, p0, LX/2lP;->b:LX/2lR;

    .line 457900
    iput-object p3, p0, LX/2lP;->c:Lcom/facebook/content/SecureContextHelper;

    .line 457901
    iput-object p4, p0, LX/2lP;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 457902
    return-void
.end method

.method public static a(LX/0QB;)LX/2lP;
    .locals 1

    .prologue
    .line 457896
    invoke-static {p0}, LX/2lP;->b(LX/0QB;)LX/2lP;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2lP;
    .locals 5

    .prologue
    .line 457891
    new-instance v4, LX/2lP;

    invoke-static {p0}, LX/2lQ;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 457892
    new-instance v2, LX/2lR;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v1}, LX/2lR;-><init>(LX/0ad;)V

    .line 457893
    move-object v1, v2

    .line 457894
    check-cast v1, LX/2lR;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2lP;-><init>(Landroid/content/ComponentName;LX/2lR;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 457895
    return-object v4
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 457878
    iget-object v0, p0, LX/2lP;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0hM;->j:LX/0Tn;

    iget-object v2, p0, LX/2lP;->b:LX/2lR;

    .line 457879
    iget-object v3, v2, LX/2lR;->a:LX/0ad;

    sget-short v4, LX/15r;->z:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    move v2, v3

    .line 457880
    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 457881
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, LX/2lP;->a:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 457882
    iget-object v1, p0, LX/2lP;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 457883
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 457884
    return-void
.end method

.method public final a(Landroid/app/Activity;Z)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 457885
    if-eqz p2, :cond_0

    .line 457886
    invoke-virtual {p0, p1}, LX/2lP;->a(Landroid/app/Activity;)V

    .line 457887
    :goto_0
    return-void

    .line 457888
    :cond_0
    new-instance v5, LX/FC0;

    invoke-direct {v5, p0, p1}, LX/FC0;-><init>(LX/2lP;Landroid/app/Activity;)V

    .line 457889
    const v0, 0x7f083206

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const v0, 0x7f083207

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f083206

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f083205

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x1

    move-object v0, p1

    move-object v8, v7

    invoke-static/range {v0 .. v9}, LX/2Tz;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)LX/2EJ;

    move-result-object v0

    .line 457890
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method
