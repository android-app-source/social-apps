.class public LX/2it;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2it;


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Uh;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 452181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452182
    iput-object p1, p0, LX/2it;->a:LX/0Uh;

    .line 452183
    iput-object p2, p0, LX/2it;->b:LX/0ad;

    .line 452184
    return-void
.end method

.method public static a(LX/0QB;)LX/2it;
    .locals 5

    .prologue
    .line 452167
    sget-object v0, LX/2it;->c:LX/2it;

    if-nez v0, :cond_1

    .line 452168
    const-class v1, LX/2it;

    monitor-enter v1

    .line 452169
    :try_start_0
    sget-object v0, LX/2it;->c:LX/2it;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 452170
    if-eqz v2, :cond_0

    .line 452171
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 452172
    new-instance p0, LX/2it;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/2it;-><init>(LX/0Uh;LX/0ad;)V

    .line 452173
    move-object v0, p0

    .line 452174
    sput-object v0, LX/2it;->c:LX/2it;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 452175
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 452176
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 452177
    :cond_1
    sget-object v0, LX/2it;->c:LX/2it;

    return-object v0

    .line 452178
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 452179
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    .line 452180
    iget-object v0, p0, LX/2it;->b:LX/0ad;

    sget-short v1, LX/15r;->H:S

    iget-object v2, p0, LX/2it;->a:LX/0Uh;

    const/16 v3, 0x64f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
