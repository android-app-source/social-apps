.class public LX/2Ia;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/2Ia;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/2Ib;

.field private final c:Landroid/content/Context;

.field public d:LX/0V8;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 391897
    const-class v0, LX/2Ia;

    sput-object v0, LX/2Ia;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2Ib;Landroid/content/Context;LX/0V8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391899
    iput-object p1, p0, LX/2Ia;->b:LX/2Ib;

    .line 391900
    iput-object p2, p0, LX/2Ia;->c:Landroid/content/Context;

    .line 391901
    iput-object p3, p0, LX/2Ia;->d:LX/0V8;

    .line 391902
    return-void
.end method

.method public static b(LX/0QB;)LX/2Ia;
    .locals 4

    .prologue
    .line 391903
    new-instance v3, LX/2Ia;

    invoke-static {p0}, LX/2Ib;->a(LX/0QB;)LX/2Ib;

    move-result-object v0

    check-cast v0, LX/2Ib;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v2

    check-cast v2, LX/0V8;

    invoke-direct {v3, v0, v1, v2}, LX/2Ia;-><init>(LX/2Ib;Landroid/content/Context;LX/0V8;)V

    .line 391904
    return-object v3
.end method

.method private b()Ljava/io/File;
    .locals 3

    .prologue
    .line 391905
    iget-object v0, p0, LX/2Ia;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 391906
    new-instance v1, Ljava/io/File;

    const-string v2, "assetdownload"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public static b(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 391907
    const-string v0, "%s%s.%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "asset_"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 391908
    iget-object v3, p0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    move-object v3, v3

    .line 391909
    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "dat"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/2Ia;)Ljava/io/File;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 391910
    iget-object v1, p0, LX/2Ia;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 391911
    if-nez v1, :cond_0

    .line 391912
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v2, "assetdownload"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Z)Ljava/io/File;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 391913
    new-instance v1, Ljava/io/File;

    invoke-direct {p0}, LX/2Ia;->b()Ljava/io/File;

    move-result-object v2

    invoke-static {p1}, LX/2Ia;->b(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 391914
    if-eqz p2, :cond_2

    .line 391915
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-nez v2, :cond_1

    .line 391916
    :cond_0
    :goto_0
    return-object v0

    .line 391917
    :cond_1
    iget-object v2, p0, LX/2Ia;->d:LX/0V8;

    sget-object v3, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v2, v3}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v2

    const-wide/32 v4, 0x6400000

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    :cond_2
    move-object v0, v1

    .line 391918
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391919
    invoke-static {p0}, LX/2Ia;->c(LX/2Ia;)Ljava/io/File;

    move-result-object v0

    .line 391920
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 391921
    invoke-direct {p0}, LX/2Ia;->b()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 391922
    if-eqz v0, :cond_0

    .line 391923
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 391924
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Z)Ljava/io/File;
    .locals 9

    .prologue
    .line 391925
    sget-object v0, LX/6Bm;->a:[I

    .line 391926
    iget-object v1, p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStorageConstraint:LX/6BV;

    move-object v1, v1

    .line 391927
    invoke-virtual {v1}, LX/6BV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 391928
    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    .line 391929
    :pswitch_0
    const/4 v0, 0x0

    .line 391930
    invoke-virtual {p1}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->g()Ljava/io/File;

    move-result-object v1

    .line 391931
    if-nez v1, :cond_4

    .line 391932
    sget-object v1, LX/2Ia;->a:Ljava/lang/Class;

    const-string v2, "configuration has StorageConstraint.MUST_BE_CUSTOM_LOCATION but no customLocation: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object p1, v3, p0

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391933
    :cond_1
    :goto_1
    move-object v0, v0

    .line 391934
    goto :goto_0

    .line 391935
    :pswitch_1
    invoke-direct {p0, p1, p2}, LX/2Ia;->c(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Z)Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 391936
    :pswitch_2
    const/4 v3, 0x0

    .line 391937
    invoke-static {p0}, LX/2Ia;->c(LX/2Ia;)Ljava/io/File;

    move-result-object v5

    .line 391938
    if-nez v5, :cond_6

    .line 391939
    :cond_2
    :goto_2
    move-object v1, v3

    .line 391940
    invoke-direct {p0, p1, p2}, LX/2Ia;->c(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Z)Ljava/io/File;

    move-result-object v0

    .line 391941
    if-eqz v1, :cond_0

    .line 391942
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move-object v0, v1

    .line 391943
    goto :goto_0

    .line 391944
    :cond_4
    if-eqz p2, :cond_5

    .line 391945
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_5
    move-object v0, v1

    .line 391946
    goto :goto_1

    .line 391947
    :cond_6
    new-instance v4, Ljava/io/File;

    invoke-static {p1}, LX/2Ia;->b(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 391948
    if-eqz p2, :cond_8

    .line 391949
    invoke-static {}, LX/2Ib;->b()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 391950
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {v4}, Ljava/io/File;->canWrite()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 391951
    :cond_7
    iget-object v5, p0, LX/2Ia;->d:LX/0V8;

    sget-object v6, LX/0VA;->EXTERNAL:LX/0VA;

    invoke-virtual {v5, v6}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v5

    const-wide/32 v7, 0x6400000

    cmp-long v5, v5, v7

    if-ltz v5, :cond_2

    move-object v3, v4

    .line 391952
    goto :goto_2

    .line 391953
    :cond_8
    invoke-static {}, LX/2Ib;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v3, v4

    .line 391954
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 391955
    invoke-virtual {p0, p1, v0}, LX/2Ia;->a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Z)Ljava/io/File;

    move-result-object v1

    .line 391956
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
