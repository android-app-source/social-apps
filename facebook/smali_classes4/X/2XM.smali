.class public LX/2XM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 419809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419810
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 419811
    check-cast p1, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;

    .line 419812
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "package_name"

    .line 419813
    iget-object v2, p1, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 419814
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    .line 419815
    iget-object v3, p1, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 419816
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 419817
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "reportAppDeletion"

    .line 419818
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 419819
    move-object v1, v1

    .line 419820
    const-string v2, "POST"

    .line 419821
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 419822
    move-object v1, v1

    .line 419823
    const-string v2, "method/user.reportAppDeletionCallback"

    .line 419824
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 419825
    move-object v1, v1

    .line 419826
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 419827
    move-object v0, v1

    .line 419828
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 419829
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 419830
    move-object v0, v0

    .line 419831
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 419832
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 419833
    const/4 v0, 0x0

    return-object v0
.end method
