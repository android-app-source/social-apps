.class public final LX/2pU;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2pX;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;)V
    .locals 0

    .prologue
    .line 468362
    iput-object p1, p0, LX/2pU;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2pX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 468363
    const-class v0, LX/2pX;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 468364
    check-cast p1, LX/2pX;

    const/4 v2, 0x0

    .line 468365
    iget-object v0, p0, LX/2pU;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v1, p1, LX/2pX;->a:LX/2pR;

    .line 468366
    iput-object v1, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->q:LX/2pR;

    .line 468367
    iget-object v0, p0, LX/2pU;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2pU;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 468368
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 468369
    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-eq v0, v1, :cond_1

    .line 468370
    iget-object v0, p0, LX/2pU;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-static {v0, v2}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->a$redex0(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;Z)V

    .line 468371
    :cond_0
    :goto_0
    return-void

    .line 468372
    :cond_1
    iget-object v0, p0, LX/2pU;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->a:LX/2pS;

    invoke-virtual {v0, v2}, LX/2pS;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 468373
    iget-object v0, p0, LX/2pU;->a:Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;->a$redex0(Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;Z)V

    goto :goto_0
.end method
