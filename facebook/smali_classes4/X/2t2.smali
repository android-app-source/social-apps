.class public LX/2t2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/2Dr;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0dC;

.field public final e:LX/0aG;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/concurrent/ExecutorService;

.field public final h:LX/2Dt;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 474511
    const-class v0, LX/2t2;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2t2;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2Dr;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dC;LX/0aG;LX/0Or;Ljava/util/concurrent/ExecutorService;LX/2Dt;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Dr;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/2Dt;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 474512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474513
    iput-object p1, p0, LX/2t2;->b:LX/2Dr;

    .line 474514
    iput-object p2, p0, LX/2t2;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 474515
    iput-object p3, p0, LX/2t2;->d:LX/0dC;

    .line 474516
    iput-object p4, p0, LX/2t2;->e:LX/0aG;

    .line 474517
    iput-object p5, p0, LX/2t2;->f:LX/0Or;

    .line 474518
    iput-object p6, p0, LX/2t2;->g:Ljava/util/concurrent/ExecutorService;

    .line 474519
    iput-object p7, p0, LX/2t2;->h:LX/2Dt;

    .line 474520
    return-void
.end method
