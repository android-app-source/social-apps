.class public final LX/36C;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source ""


# instance fields
.field public a:Landroid/graphics/Bitmap;

.field public b:Landroid/graphics/Paint;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:F

.field public j:Lcom/facebook/fbui/drawable/NetworkDrawable;

.field public final k:Landroid/graphics/Rect;

.field public l:Landroid/graphics/drawable/Drawable$ConstantState;


# direct methods
.method public constructor <init>(LX/36C;)V
    .locals 2

    .prologue
    .line 498237
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    .line 498238
    if-eqz p1, :cond_1

    .line 498239
    iget-object v0, p1, LX/36C;->a:Landroid/graphics/Bitmap;

    iput-object v0, p0, LX/36C;->a:Landroid/graphics/Bitmap;

    .line 498240
    iget-object v0, p1, LX/36C;->b:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p1, LX/36C;->b:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    :goto_0
    iput-object v0, p0, LX/36C;->b:Landroid/graphics/Paint;

    .line 498241
    iget v0, p1, LX/36C;->c:I

    iput v0, p0, LX/36C;->c:I

    .line 498242
    iget v0, p1, LX/36C;->d:I

    iput v0, p0, LX/36C;->d:I

    .line 498243
    iget v0, p1, LX/36C;->e:I

    iput v0, p0, LX/36C;->e:I

    .line 498244
    iget v0, p1, LX/36C;->f:I

    iput v0, p0, LX/36C;->f:I

    .line 498245
    iget v0, p1, LX/36C;->g:I

    iput v0, p0, LX/36C;->g:I

    .line 498246
    iget v0, p1, LX/36C;->h:I

    iput v0, p0, LX/36C;->h:I

    .line 498247
    iget-object v0, p1, LX/36C;->k:Landroid/graphics/Rect;

    iput-object v0, p0, LX/36C;->k:Landroid/graphics/Rect;

    .line 498248
    iget v0, p1, LX/36C;->i:F

    iput v0, p0, LX/36C;->i:F

    .line 498249
    iget-object v0, p1, LX/36C;->l:Landroid/graphics/drawable/Drawable$ConstantState;

    iput-object v0, p0, LX/36C;->l:Landroid/graphics/drawable/Drawable$ConstantState;

    .line 498250
    iget-object v0, p1, LX/36C;->j:Lcom/facebook/fbui/drawable/NetworkDrawable;

    iput-object v0, p0, LX/36C;->j:Lcom/facebook/fbui/drawable/NetworkDrawable;

    .line 498251
    :goto_1
    return-void

    .line 498252
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 498253
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/36C;->k:Landroid/graphics/Rect;

    goto :goto_1
.end method


# virtual methods
.method public final getChangingConfigurations()I
    .locals 1

    .prologue
    .line 498254
    const/4 v0, 0x0

    return v0
.end method

.method public final newDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 498255
    new-instance v0, LX/36A;

    invoke-direct {v0, p0}, LX/36A;-><init>(LX/36C;)V

    return-object v0
.end method
