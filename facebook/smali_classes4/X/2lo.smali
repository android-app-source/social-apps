.class public final LX/2lo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field public final synthetic d:LX/118;


# direct methods
.method public constructor <init>(LX/118;)V
    .locals 1

    .prologue
    .line 458976
    iput-object p1, p0, LX/2lo;->d:LX/118;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 458977
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2lo;->c:Z

    .line 458978
    invoke-virtual {p1}, LX/118;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/2lo;->a:I

    .line 458979
    const/4 v0, -0x1

    iput v0, p0, LX/2lo;->b:I

    .line 458980
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 458970
    iget-boolean v2, p0, LX/2lo;->c:Z

    if-nez v2, :cond_0

    .line 458971
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This container does not support retaining Map.Entry objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458972
    :cond_0
    instance-of v2, p1, Ljava/util/Map$Entry;

    if-nez v2, :cond_2

    .line 458973
    :cond_1
    :goto_0
    return v0

    .line 458974
    :cond_2
    check-cast p1, Ljava/util/Map$Entry;

    .line 458975
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LX/2lo;->d:LX/118;

    iget v4, p0, LX/2lo;->b:I

    invoke-virtual {v3, v4, v0}, LX/118;->a(II)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, LX/01M;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LX/2lo;->d:LX/118;

    iget v4, p0, LX/2lo;->b:I

    invoke-virtual {v3, v4, v1}, LX/118;->a(II)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, LX/01M;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public final getKey()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 458967
    iget-boolean v0, p0, LX/2lo;->c:Z

    if-nez v0, :cond_0

    .line 458968
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This container does not support retaining Map.Entry objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458969
    :cond_0
    iget-object v0, p0, LX/2lo;->d:LX/118;

    iget v1, p0, LX/2lo;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/118;->a(II)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 458964
    iget-boolean v0, p0, LX/2lo;->c:Z

    if-nez v0, :cond_0

    .line 458965
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This container does not support retaining Map.Entry objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458966
    :cond_0
    iget-object v0, p0, LX/2lo;->d:LX/118;

    iget v1, p0, LX/2lo;->b:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/118;->a(II)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 458963
    iget v0, p0, LX/2lo;->b:I

    iget v1, p0, LX/2lo;->a:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 458944
    iget-boolean v1, p0, LX/2lo;->c:Z

    if-nez v1, :cond_0

    .line 458945
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This container does not support retaining Map.Entry objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458946
    :cond_0
    iget-object v1, p0, LX/2lo;->d:LX/118;

    iget v2, p0, LX/2lo;->b:I

    invoke-virtual {v1, v2, v0}, LX/118;->a(II)Ljava/lang/Object;

    move-result-object v1

    .line 458947
    iget-object v2, p0, LX/2lo;->d:LX/118;

    iget v3, p0, LX/2lo;->b:I

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, LX/118;->a(II)Ljava/lang/Object;

    move-result-object v2

    .line 458948
    if-nez v1, :cond_1

    move v1, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    xor-int/2addr v0, v1

    return v0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 458960
    iget v0, p0, LX/2lo;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/2lo;->b:I

    .line 458961
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2lo;->c:Z

    .line 458962
    return-object p0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 458953
    iget-boolean v0, p0, LX/2lo;->c:Z

    if-nez v0, :cond_0

    .line 458954
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 458955
    :cond_0
    iget-object v0, p0, LX/2lo;->d:LX/118;

    iget v1, p0, LX/2lo;->b:I

    invoke-virtual {v0, v1}, LX/118;->a(I)V

    .line 458956
    iget v0, p0, LX/2lo;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/2lo;->b:I

    .line 458957
    iget v0, p0, LX/2lo;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/2lo;->a:I

    .line 458958
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2lo;->c:Z

    .line 458959
    return-void
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    .line 458950
    iget-boolean v0, p0, LX/2lo;->c:Z

    if-nez v0, :cond_0

    .line 458951
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This container does not support retaining Map.Entry objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458952
    :cond_0
    iget-object v0, p0, LX/2lo;->d:LX/118;

    iget v1, p0, LX/2lo;->b:I

    invoke-virtual {v0, v1, p1}, LX/118;->a(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 458949
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LX/2lo;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/2lo;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
