.class public final LX/2Ai;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2Aj;

.field public b:LX/2Ak;


# direct methods
.method private constructor <init>(LX/2Aj;)V
    .locals 1

    .prologue
    .line 377551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377552
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Ai;->b:LX/2Ak;

    .line 377553
    iput-object p1, p0, LX/2Ai;->a:LX/2Aj;

    .line 377554
    return-void
.end method

.method public static a(Ljava/util/HashMap;)LX/2Ai;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "LX/2Ak;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;>;)",
            "LX/2Ai;"
        }
    .end annotation

    .prologue
    .line 377550
    new-instance v0, LX/2Ai;

    new-instance v1, LX/2Aj;

    invoke-direct {v1, p0}, LX/2Aj;-><init>(Ljava/util/Map;)V

    invoke-direct {v0, v1}, LX/2Ai;-><init>(LX/2Aj;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/2Ai;
    .locals 2

    .prologue
    .line 377549
    new-instance v0, LX/2Ai;

    iget-object v1, p0, LX/2Ai;->a:LX/2Aj;

    invoke-direct {v0, v1}, LX/2Ai;-><init>(LX/2Aj;)V

    return-object v0
.end method

.method public final a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377545
    iget-object v0, p0, LX/2Ai;->b:LX/2Ak;

    if-nez v0, :cond_0

    .line 377546
    new-instance v0, LX/2Ak;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, LX/2Ak;-><init>(LX/0lJ;Z)V

    iput-object v0, p0, LX/2Ai;->b:LX/2Ak;

    .line 377547
    :goto_0
    iget-object v0, p0, LX/2Ai;->a:LX/2Aj;

    iget-object v1, p0, LX/2Ai;->b:LX/2Ak;

    invoke-virtual {v0, v1}, LX/2Aj;->a(LX/2Ak;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    return-object v0

    .line 377548
    :cond_0
    iget-object v0, p0, LX/2Ai;->b:LX/2Ak;

    invoke-virtual {v0, p1}, LX/2Ak;->a(LX/0lJ;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377541
    iget-object v0, p0, LX/2Ai;->b:LX/2Ak;

    if-nez v0, :cond_0

    .line 377542
    new-instance v0, LX/2Ak;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, LX/2Ak;-><init>(Ljava/lang/Class;Z)V

    iput-object v0, p0, LX/2Ai;->b:LX/2Ak;

    .line 377543
    :goto_0
    iget-object v0, p0, LX/2Ai;->a:LX/2Aj;

    iget-object v1, p0, LX/2Ai;->b:LX/2Ak;

    invoke-virtual {v0, v1}, LX/2Aj;->a(LX/2Ak;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    return-object v0

    .line 377544
    :cond_0
    iget-object v0, p0, LX/2Ai;->b:LX/2Ak;

    invoke-virtual {v0, p1}, LX/2Ak;->a(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public final b(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377533
    iget-object v0, p0, LX/2Ai;->b:LX/2Ak;

    if-nez v0, :cond_0

    .line 377534
    new-instance v0, LX/2Ak;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, LX/2Ak;-><init>(LX/0lJ;Z)V

    iput-object v0, p0, LX/2Ai;->b:LX/2Ak;

    .line 377535
    :goto_0
    iget-object v0, p0, LX/2Ai;->a:LX/2Aj;

    iget-object v1, p0, LX/2Ai;->b:LX/2Ak;

    invoke-virtual {v0, v1}, LX/2Aj;->a(LX/2Ak;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    return-object v0

    .line 377536
    :cond_0
    iget-object v0, p0, LX/2Ai;->b:LX/2Ak;

    invoke-virtual {v0, p1}, LX/2Ak;->b(LX/0lJ;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377537
    iget-object v0, p0, LX/2Ai;->b:LX/2Ak;

    if-nez v0, :cond_0

    .line 377538
    new-instance v0, LX/2Ak;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, LX/2Ak;-><init>(Ljava/lang/Class;Z)V

    iput-object v0, p0, LX/2Ai;->b:LX/2Ak;

    .line 377539
    :goto_0
    iget-object v0, p0, LX/2Ai;->a:LX/2Aj;

    iget-object v1, p0, LX/2Ai;->b:LX/2Ak;

    invoke-virtual {v0, v1}, LX/2Aj;->a(LX/2Ak;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    return-object v0

    .line 377540
    :cond_0
    iget-object v0, p0, LX/2Ai;->b:LX/2Ak;

    invoke-virtual {v0, p1}, LX/2Ak;->b(Ljava/lang/Class;)V

    goto :goto_0
.end method
