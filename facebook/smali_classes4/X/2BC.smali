.class public final LX/2BC;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/ref/SoftReference",
            "<",
            "LX/2BC;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final e:[C

.field private static final f:[B


# instance fields
.field public b:LX/15x;

.field public c:LX/2SG;

.field public final d:[C


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 378717
    invoke-static {}, LX/12H;->g()[C

    move-result-object v0

    sput-object v0, LX/2BC;->e:[C

    .line 378718
    invoke-static {}, LX/12H;->h()[B

    move-result-object v0

    sput-object v0, LX/2BC;->f:[B

    .line 378719
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, LX/2BC;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x30

    .line 378720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378721
    const/4 v0, 0x6

    new-array v0, v0, [C

    iput-object v0, p0, LX/2BC;->d:[C

    .line 378722
    iget-object v0, p0, LX/2BC;->d:[C

    const/4 v1, 0x0

    const/16 v2, 0x5c

    aput-char v2, v0, v1

    .line 378723
    iget-object v0, p0, LX/2BC;->d:[C

    const/4 v1, 0x2

    aput-char v3, v0, v1

    .line 378724
    iget-object v0, p0, LX/2BC;->d:[C

    const/4 v1, 0x3

    aput-char v3, v0, v1

    .line 378725
    return-void
.end method

.method private static a(II)I
    .locals 3

    .prologue
    const v2, 0xdc00

    .line 378726
    if-lt p1, v2, :cond_0

    const v0, 0xdfff

    if-le p1, v0, :cond_1

    .line 378727
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Broken surrogate pair: first char 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", second 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; illegal combination"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 378728
    :cond_1
    const/high16 v0, 0x10000

    const v1, 0xd800

    sub-int v1, p0, v1

    shl-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    sub-int v1, p1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method private static a(IILX/2SG;I)I
    .locals 3

    .prologue
    const/16 v1, 0x30

    .line 378729
    iput p3, p2, LX/2SG;->f:I

    .line 378730
    const/16 v0, 0x5c

    invoke-virtual {p2, v0}, LX/2SG;->a(I)V

    .line 378731
    if-gez p1, :cond_1

    .line 378732
    const/16 v0, 0x75

    invoke-virtual {p2, v0}, LX/2SG;->a(I)V

    .line 378733
    const/16 v0, 0xff

    if-le p0, v0, :cond_0

    .line 378734
    shr-int/lit8 v0, p0, 0x8

    .line 378735
    sget-object v1, LX/2BC;->f:[B

    shr-int/lit8 v2, v0, 0x4

    aget-byte v1, v1, v2

    invoke-virtual {p2, v1}, LX/2SG;->a(I)V

    .line 378736
    sget-object v1, LX/2BC;->f:[B

    and-int/lit8 v0, v0, 0xf

    aget-byte v0, v1, v0

    invoke-virtual {p2, v0}, LX/2SG;->a(I)V

    .line 378737
    and-int/lit16 p0, p0, 0xff

    .line 378738
    :goto_0
    sget-object v0, LX/2BC;->f:[B

    shr-int/lit8 v1, p0, 0x4

    aget-byte v0, v0, v1

    invoke-virtual {p2, v0}, LX/2SG;->a(I)V

    .line 378739
    sget-object v0, LX/2BC;->f:[B

    and-int/lit8 v1, p0, 0xf

    aget-byte v0, v0, v1

    invoke-virtual {p2, v0}, LX/2SG;->a(I)V

    .line 378740
    :goto_1
    iget v0, p2, LX/2SG;->f:I

    move v0, v0

    .line 378741
    return v0

    .line 378742
    :cond_0
    invoke-virtual {p2, v1}, LX/2SG;->a(I)V

    .line 378743
    invoke-virtual {p2, v1}, LX/2SG;->a(I)V

    goto :goto_0

    .line 378744
    :cond_1
    int-to-byte v0, p1

    invoke-virtual {p2, v0}, LX/2SG;->a(I)V

    goto :goto_1
.end method

.method public static a()LX/2BC;
    .locals 3

    .prologue
    .line 378745
    sget-object v0, LX/2BC;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 378746
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 378747
    :goto_0
    if-nez v0, :cond_0

    .line 378748
    new-instance v0, LX/2BC;

    invoke-direct {v0}, LX/2BC;-><init>()V

    .line 378749
    sget-object v1, LX/2BC;->a:Ljava/lang/ThreadLocal;

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 378750
    :cond_0
    return-object v0

    .line 378751
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2BC;

    goto :goto_0
.end method

.method private static a(I)V
    .locals 2

    .prologue
    .line 378752
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, LX/4pf;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)[C
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 378753
    iget-object v0, p0, LX/2BC;->b:LX/15x;

    .line 378754
    if-nez v0, :cond_0

    .line 378755
    new-instance v0, LX/15x;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/15x;-><init>(LX/12B;)V

    iput-object v0, p0, LX/2BC;->b:LX/15x;

    .line 378756
    :cond_0
    invoke-virtual {v0}, LX/15x;->l()[C

    move-result-object v3

    .line 378757
    sget-object v1, LX/12H;->f:[I

    move-object v6, v1

    .line 378758
    array-length v7, v6

    .line 378759
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    move v1, v5

    move v2, v5

    .line 378760
    :goto_0
    if-ge v2, v8, :cond_3

    .line 378761
    :cond_1
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v9

    .line 378762
    if-ge v9, v7, :cond_2

    aget v4, v6, v9

    if-nez v4, :cond_4

    .line 378763
    :cond_2
    array-length v4, v3

    if-lt v1, v4, :cond_8

    .line 378764
    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v3

    move v4, v5

    .line 378765
    :goto_1
    add-int/lit8 v1, v4, 0x1

    aput-char v9, v3, v4

    .line 378766
    add-int/lit8 v2, v2, 0x1

    if-lt v2, v8, :cond_1

    .line 378767
    :cond_3
    iput v1, v0, LX/15x;->j:I

    .line 378768
    invoke-virtual {v0}, LX/15x;->h()[C

    move-result-object v0

    return-object v0

    .line 378769
    :cond_4
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 378770
    aget v9, v6, v2

    .line 378771
    if-gez v9, :cond_6

    const/16 v2, 0x6

    .line 378772
    :goto_2
    add-int v9, v1, v2

    array-length v10, v3

    if-le v9, v10, :cond_7

    .line 378773
    array-length v9, v3

    sub-int/2addr v9, v1

    .line 378774
    if-lez v9, :cond_5

    .line 378775
    iget-object v10, p0, LX/2BC;->d:[C

    invoke-static {v10, v5, v3, v1, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 378776
    :cond_5
    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v3

    .line 378777
    sub-int v1, v2, v9

    .line 378778
    iget-object v2, p0, LX/2BC;->d:[C

    invoke-static {v2, v9, v3, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, v4

    .line 378779
    goto :goto_0

    .line 378780
    :cond_6
    const/16 v2, 0x2

    goto :goto_2

    .line 378781
    :cond_7
    iget-object v9, p0, LX/2BC;->d:[C

    invoke-static {v9, v5, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 378782
    add-int/2addr v1, v2

    move v2, v4

    .line 378783
    goto :goto_0

    :cond_8
    move v4, v1

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)[B
    .locals 11

    .prologue
    const/16 v9, 0x7f

    const/4 v5, 0x0

    .line 378784
    iget-object v0, p0, LX/2BC;->c:LX/2SG;

    .line 378785
    if-nez v0, :cond_0

    .line 378786
    new-instance v0, LX/2SG;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/2SG;-><init>(LX/12B;)V

    iput-object v0, p0, LX/2BC;->c:LX/2SG;

    .line 378787
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    .line 378788
    invoke-virtual {v0}, LX/2SG;->d()[B

    move-result-object v1

    move v2, v5

    move v3, v5

    .line 378789
    :goto_0
    if-ge v3, v7, :cond_2

    .line 378790
    sget-object v4, LX/12H;->f:[I

    move-object v6, v4

    .line 378791
    :cond_1
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 378792
    if-gt v8, v9, :cond_3

    aget v4, v6, v8

    if-nez v4, :cond_3

    .line 378793
    array-length v4, v1

    if-lt v2, v4, :cond_10

    .line 378794
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v1

    move v4, v5

    .line 378795
    :goto_1
    add-int/lit8 v2, v4, 0x1

    int-to-byte v8, v8

    aput-byte v8, v1, v4

    .line 378796
    add-int/lit8 v3, v3, 0x1

    if-lt v3, v7, :cond_1

    .line 378797
    :cond_2
    iget-object v0, p0, LX/2BC;->c:LX/2SG;

    invoke-virtual {v0, v2}, LX/2SG;->d(I)[B

    move-result-object v0

    return-object v0

    .line 378798
    :cond_3
    array-length v4, v1

    if-lt v2, v4, :cond_4

    .line 378799
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v1

    move v2, v5

    .line 378800
    :cond_4
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 378801
    if-gt v8, v9, :cond_5

    .line 378802
    aget v1, v6, v8

    .line 378803
    invoke-static {v8, v1, v0, v2}, LX/2BC;->a(IILX/2SG;I)I

    move-result v2

    .line 378804
    iget-object v1, v0, LX/2SG;->e:[B

    move-object v1, v1

    .line 378805
    move v3, v4

    .line 378806
    goto :goto_0

    .line 378807
    :cond_5
    const/16 v3, 0x7ff

    if-gt v8, v3, :cond_7

    .line 378808
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v6, v8, 0x6

    or-int/lit16 v6, v6, 0xc0

    int-to-byte v6, v6

    aput-byte v6, v1, v2

    .line 378809
    and-int/lit8 v2, v8, 0x3f

    or-int/lit16 v2, v2, 0x80

    move v10, v2

    move-object v2, v1

    move v1, v10

    .line 378810
    :goto_2
    array-length v6, v2

    if-lt v3, v6, :cond_6

    .line 378811
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v2

    move v3, v5

    .line 378812
    :cond_6
    add-int/lit8 v6, v3, 0x1

    int-to-byte v1, v1

    aput-byte v1, v2, v3

    move-object v1, v2

    move v3, v4

    move v2, v6

    .line 378813
    goto :goto_0

    .line 378814
    :cond_7
    const v3, 0xd800

    if-lt v8, v3, :cond_8

    const v3, 0xdfff

    if-le v8, v3, :cond_9

    .line 378815
    :cond_8
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v6, v8, 0xc

    or-int/lit16 v6, v6, 0xe0

    int-to-byte v6, v6

    aput-byte v6, v1, v2

    .line 378816
    array-length v2, v1

    if-lt v3, v2, :cond_f

    .line 378817
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v1

    move v2, v5

    .line 378818
    :goto_3
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v6, v8, 0x6

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v1, v2

    .line 378819
    and-int/lit8 v2, v8, 0x3f

    or-int/lit16 v2, v2, 0x80

    move v10, v2

    move-object v2, v1

    move v1, v10

    goto :goto_2

    .line 378820
    :cond_9
    const v3, 0xdbff

    if-le v8, v3, :cond_a

    .line 378821
    invoke-static {v8}, LX/2BC;->a(I)V

    .line 378822
    :cond_a
    if-lt v4, v7, :cond_b

    .line 378823
    invoke-static {v8}, LX/2BC;->a(I)V

    .line 378824
    :cond_b
    add-int/lit8 v6, v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v8, v3}, LX/2BC;->a(II)I

    move-result v4

    .line 378825
    const v3, 0x10ffff

    if-le v4, v3, :cond_c

    .line 378826
    invoke-static {v4}, LX/2BC;->a(I)V

    .line 378827
    :cond_c
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v8, v4, 0x12

    or-int/lit16 v8, v8, 0xf0

    int-to-byte v8, v8

    aput-byte v8, v1, v2

    .line 378828
    array-length v2, v1

    if-lt v3, v2, :cond_e

    .line 378829
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v1

    move v2, v5

    .line 378830
    :goto_4
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v8, v4, 0xc

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v1, v2

    .line 378831
    array-length v2, v1

    if-lt v3, v2, :cond_d

    .line 378832
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v1

    move v2, v5

    .line 378833
    :goto_5
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v8, v4, 0x6

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v1, v2

    .line 378834
    and-int/lit8 v2, v4, 0x3f

    or-int/lit16 v2, v2, 0x80

    move v4, v6

    move-object v10, v1

    move v1, v2

    move-object v2, v10

    goto/16 :goto_2

    :cond_d
    move v2, v3

    goto :goto_5

    :cond_e
    move v2, v3

    goto :goto_4

    :cond_f
    move v2, v3

    goto :goto_3

    :cond_10
    move v4, v2

    goto/16 :goto_1
.end method

.method public final c(Ljava/lang/String;)[B
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 378835
    iget-object v0, p0, LX/2BC;->c:LX/2SG;

    .line 378836
    if-nez v0, :cond_0

    .line 378837
    new-instance v0, LX/2SG;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/2SG;-><init>(LX/12B;)V

    iput-object v0, p0, LX/2BC;->c:LX/2SG;

    .line 378838
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    .line 378839
    invoke-virtual {v0}, LX/2SG;->d()[B

    move-result-object v3

    .line 378840
    array-length v2, v3

    move v1, v4

    move v6, v4

    .line 378841
    :goto_0
    if-ge v6, v9, :cond_f

    .line 378842
    add-int/lit8 v7, v6, 0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v10, v2

    move-object v2, v3

    move v3, v1

    move v1, v10

    .line 378843
    :goto_1
    const/16 v5, 0x7f

    if-gt v6, v5, :cond_2

    .line 378844
    if-lt v3, v1, :cond_1

    .line 378845
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v2

    .line 378846
    array-length v1, v2

    move v3, v4

    .line 378847
    :cond_1
    add-int/lit8 v5, v3, 0x1

    int-to-byte v6, v6

    aput-byte v6, v2, v3

    .line 378848
    if-ge v7, v9, :cond_c

    .line 378849
    add-int/lit8 v3, v7, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v7, v3

    move v3, v5

    goto :goto_1

    .line 378850
    :cond_2
    if-lt v3, v1, :cond_e

    .line 378851
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v2

    .line 378852
    array-length v1, v2

    move v5, v4

    .line 378853
    :goto_2
    const/16 v3, 0x800

    if-ge v6, v3, :cond_4

    .line 378854
    add-int/lit8 v3, v5, 0x1

    shr-int/lit8 v8, v6, 0x6

    or-int/lit16 v8, v8, 0xc0

    int-to-byte v8, v8

    aput-byte v8, v2, v5

    move v5, v6

    move v6, v7

    .line 378855
    :goto_3
    if-lt v3, v1, :cond_3

    .line 378856
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v2

    .line 378857
    array-length v1, v2

    move v3, v4

    .line 378858
    :cond_3
    add-int/lit8 v7, v3, 0x1

    and-int/lit8 v5, v5, 0x3f

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v2, v3

    move-object v3, v2

    move v2, v1

    move v1, v7

    .line 378859
    goto :goto_0

    .line 378860
    :cond_4
    const v3, 0xd800

    if-lt v6, v3, :cond_5

    const v3, 0xdfff

    if-le v6, v3, :cond_7

    .line 378861
    :cond_5
    add-int/lit8 v3, v5, 0x1

    shr-int/lit8 v8, v6, 0xc

    or-int/lit16 v8, v8, 0xe0

    int-to-byte v8, v8

    aput-byte v8, v2, v5

    .line 378862
    if-lt v3, v1, :cond_6

    .line 378863
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v2

    .line 378864
    array-length v1, v2

    move v3, v4

    .line 378865
    :cond_6
    add-int/lit8 v5, v3, 0x1

    shr-int/lit8 v8, v6, 0x6

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v2, v3

    move v3, v5

    move v5, v6

    move v6, v7

    goto :goto_3

    .line 378866
    :cond_7
    const v3, 0xdbff

    if-le v6, v3, :cond_8

    .line 378867
    invoke-static {v6}, LX/2BC;->a(I)V

    .line 378868
    :cond_8
    if-lt v7, v9, :cond_9

    .line 378869
    invoke-static {v6}, LX/2BC;->a(I)V

    .line 378870
    :cond_9
    add-int/lit8 v8, v7, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v6, v3}, LX/2BC;->a(II)I

    move-result v6

    .line 378871
    const v3, 0x10ffff

    if-le v6, v3, :cond_a

    .line 378872
    invoke-static {v6}, LX/2BC;->a(I)V

    .line 378873
    :cond_a
    add-int/lit8 v3, v5, 0x1

    shr-int/lit8 v7, v6, 0x12

    or-int/lit16 v7, v7, 0xf0

    int-to-byte v7, v7

    aput-byte v7, v2, v5

    .line 378874
    if-lt v3, v1, :cond_b

    .line 378875
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v2

    .line 378876
    array-length v1, v2

    move v3, v4

    .line 378877
    :cond_b
    add-int/lit8 v5, v3, 0x1

    shr-int/lit8 v7, v6, 0xc

    and-int/lit8 v7, v7, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v2, v3

    .line 378878
    if-lt v5, v1, :cond_d

    .line 378879
    invoke-virtual {v0}, LX/2SG;->e()[B

    move-result-object v2

    .line 378880
    array-length v1, v2

    move v3, v4

    .line 378881
    :goto_4
    add-int/lit8 v5, v3, 0x1

    shr-int/lit8 v7, v6, 0x6

    and-int/lit8 v7, v7, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v2, v3

    move v3, v5

    move v5, v6

    move v6, v8

    goto/16 :goto_3

    :cond_c
    move v0, v5

    .line 378882
    :goto_5
    iget-object v1, p0, LX/2BC;->c:LX/2SG;

    invoke-virtual {v1, v0}, LX/2SG;->d(I)[B

    move-result-object v0

    return-object v0

    :cond_d
    move v3, v5

    goto :goto_4

    :cond_e
    move v5, v3

    goto/16 :goto_2

    :cond_f
    move v0, v1

    goto :goto_5
.end method
