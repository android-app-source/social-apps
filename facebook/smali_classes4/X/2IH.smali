.class public LX/2IH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/2IH;


# instance fields
.field private final a:LX/0Uo;

.field public final b:LX/0ka;

.field private final c:LX/0Xl;

.field public final d:Landroid/os/Handler;

.field public final e:LX/2IK;

.field public final f:LX/30F;

.field private final g:LX/1sf;

.field public final h:Ljava/lang/Runnable;

.field public final i:LX/0qD;


# direct methods
.method public constructor <init>(LX/0Uo;LX/0ka;LX/0Xl;Landroid/os/Handler;LX/2IK;LX/30F;LX/1sf;)V
    .locals 1
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391549
    iput-object p1, p0, LX/2IH;->a:LX/0Uo;

    .line 391550
    iput-object p2, p0, LX/2IH;->b:LX/0ka;

    .line 391551
    iput-object p3, p0, LX/2IH;->c:LX/0Xl;

    .line 391552
    iput-object p4, p0, LX/2IH;->d:Landroid/os/Handler;

    .line 391553
    iput-object p5, p0, LX/2IH;->e:LX/2IK;

    .line 391554
    iput-object p6, p0, LX/2IH;->f:LX/30F;

    .line 391555
    iput-object p7, p0, LX/2IH;->g:LX/1sf;

    .line 391556
    new-instance v0, Lcom/facebook/selfupdate/remotepushtrigger/RemotePushTriggerWifiChangeMonitor$1;

    invoke-direct {v0, p0}, Lcom/facebook/selfupdate/remotepushtrigger/RemotePushTriggerWifiChangeMonitor$1;-><init>(LX/2IH;)V

    iput-object v0, p0, LX/2IH;->h:Ljava/lang/Runnable;

    .line 391557
    new-instance v0, LX/30G;

    invoke-direct {v0, p0}, LX/30G;-><init>(LX/2IH;)V

    iput-object v0, p0, LX/2IH;->i:LX/0qD;

    .line 391558
    return-void
.end method

.method public static a(LX/0QB;)LX/2IH;
    .locals 3

    .prologue
    .line 391559
    sget-object v0, LX/2IH;->j:LX/2IH;

    if-nez v0, :cond_1

    .line 391560
    const-class v1, LX/2IH;

    monitor-enter v1

    .line 391561
    :try_start_0
    sget-object v0, LX/2IH;->j:LX/2IH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391562
    if-eqz v2, :cond_0

    .line 391563
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2IH;->b(LX/0QB;)LX/2IH;

    move-result-object v0

    sput-object v0, LX/2IH;->j:LX/2IH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391564
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391565
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391566
    :cond_1
    sget-object v0, LX/2IH;->j:LX/2IH;

    return-object v0

    .line 391567
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391568
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()Z
    .locals 3

    .prologue
    .line 391569
    iget-object v0, p0, LX/2IH;->g:LX/1sf;

    invoke-virtual {v0}, LX/1sf;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2IH;->e:LX/2IK;

    .line 391570
    iget-object v1, v0, LX/2IK;->a:LX/0ad;

    sget-short v2, LX/2IM;->b:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 391571
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/2IH;
    .locals 14

    .prologue
    .line 391572
    new-instance v0, LX/2IH;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v1

    check-cast v1, LX/0Uo;

    invoke-static {p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v2

    check-cast v2, LX/0ka;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {p0}, LX/2IK;->b(LX/0QB;)LX/2IK;

    move-result-object v5

    check-cast v5, LX/2IK;

    .line 391573
    new-instance v8, LX/30F;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v9

    check-cast v9, LX/0Uo;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/2IK;->b(LX/0QB;)LX/2IK;

    move-result-object v12

    check-cast v12, LX/2IK;

    const/16 v13, 0xafd

    invoke-static {p0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v8 .. v13}, LX/30F;-><init>(LX/0Uo;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2IK;LX/0Ot;)V

    .line 391574
    move-object v6, v8

    .line 391575
    check-cast v6, LX/30F;

    invoke-static {p0}, LX/1sf;->b(LX/0QB;)LX/1sf;

    move-result-object v7

    check-cast v7, LX/1sf;

    invoke-direct/range {v0 .. v7}, LX/2IH;-><init>(LX/0Uo;LX/0ka;LX/0Xl;Landroid/os/Handler;LX/2IK;LX/30F;LX/1sf;)V

    .line 391576
    return-object v0
.end method

.method public static b(LX/2IH;)V
    .locals 2

    .prologue
    .line 391577
    invoke-direct {p0}, LX/2IH;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 391578
    :goto_0
    return-void

    .line 391579
    :cond_0
    iget-object v0, p0, LX/2IH;->b:LX/0ka;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ka;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 391580
    iget-object v0, p0, LX/2IH;->f:LX/30F;

    invoke-virtual {v0}, LX/30F;->a()V

    .line 391581
    :cond_1
    iget-object v0, p0, LX/2IH;->b:LX/0ka;

    iget-object v1, p0, LX/2IH;->i:LX/0qD;

    invoke-virtual {v0, v1}, LX/0ka;->a(LX/0qD;)V

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 391582
    invoke-direct {p0}, LX/2IH;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 391583
    :cond_0
    :goto_0
    return-void

    .line 391584
    :cond_1
    iget-object v0, p0, LX/2IH;->c:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v2, LX/K2n;

    invoke-direct {v2, p0}, LX/K2n;-><init>(LX/2IH;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v2, LX/K2m;

    invoke-direct {v2, p0}, LX/K2m;-><init>(LX/2IH;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 391585
    iget-object v0, p0, LX/2IH;->e:LX/2IK;

    invoke-virtual {v0}, LX/2IK;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/2IH;->a:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 391586
    :cond_2
    invoke-static {p0}, LX/2IH;->b(LX/2IH;)V

    goto :goto_0
.end method
