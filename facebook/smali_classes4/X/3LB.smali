.class public LX/3LB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3LC;

.field public final b:LX/3LD;

.field public final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(LX/3LC;LX/3LD;Landroid/os/Handler;)V
    .locals 0
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550090
    iput-object p1, p0, LX/3LB;->a:LX/3LC;

    .line 550091
    iput-object p2, p0, LX/3LB;->b:LX/3LD;

    .line 550092
    iput-object p3, p0, LX/3LB;->c:Landroid/os/Handler;

    .line 550093
    return-void
.end method

.method public static b(LX/0QB;)LX/3LB;
    .locals 5

    .prologue
    .line 550094
    new-instance v3, LX/3LB;

    .line 550095
    new-instance v1, LX/3LC;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/3LC;-><init>(LX/0Zb;)V

    .line 550096
    move-object v0, v1

    .line 550097
    check-cast v0, LX/3LC;

    .line 550098
    new-instance v4, LX/3LD;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-direct {v4, v1, v2}, LX/3LD;-><init>(LX/0Zb;LX/0ad;)V

    .line 550099
    move-object v1, v4

    .line 550100
    check-cast v1, LX/3LD;

    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    invoke-direct {v3, v0, v1, v2}, LX/3LB;-><init>(LX/3LC;LX/3LD;Landroid/os/Handler;)V

    .line 550101
    return-object v3
.end method

.method public static b(LX/0Px;)LX/FD4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/Ish;",
            ">;)",
            "LX/FD4;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 550102
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 550103
    :cond_0
    const/4 v0, 0x0

    .line 550104
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ish;

    .line 550105
    sget-object v1, LX/Isg;->BOTS:LX/Isg;

    .line 550106
    iget-object p0, v0, LX/Ish;->c:LX/Isg;

    move-object p0, p0

    .line 550107
    invoke-virtual {v1, p0}, LX/Isg;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/FD4;->BOTS:LX/FD4;

    :goto_1
    move-object v0, v1

    .line 550108
    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
