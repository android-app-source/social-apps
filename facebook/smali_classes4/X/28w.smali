.class public LX/28w;
.super LX/16B;
.source ""


# instance fields
.field private final a:LX/28x;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/28x;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374979
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 374980
    iput-object p1, p0, LX/28w;->a:LX/28x;

    .line 374981
    iput-object p2, p0, LX/28w;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 374982
    return-void
.end method


# virtual methods
.method public final g()V
    .locals 2

    .prologue
    .line 374983
    iget-object v0, p0, LX/28w;->a:LX/28x;

    const-string v1, "logout"

    invoke-virtual {v0, v1}, LX/28x;->b(Ljava/lang/String;)V

    .line 374984
    iget-object v0, p0, LX/28w;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0db;->ae:LX/0Tn;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    .line 374985
    return-void
.end method
