.class public final LX/267;
.super LX/268;
.source ""


# static fields
.field private static final serialVersionUID:J = -0x6cbb315ebf8435f0L


# direct methods
.method private constructor <init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0lJ;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 370696
    invoke-direct/range {p0 .. p5}, LX/268;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 370697
    return-void
.end method

.method public static a(Ljava/lang/Class;LX/0lJ;)LX/267;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0lJ;",
            ")",
            "LX/267;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 370695
    new-instance v0, LX/267;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, LX/267;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private i(Ljava/lang/Object;)LX/267;
    .locals 6

    .prologue
    .line 370694
    new-instance v0, LX/267;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/267;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private j(Ljava/lang/Object;)LX/267;
    .locals 6

    .prologue
    .line 370693
    new-instance v0, LX/267;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->a(Ljava/lang/Object;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v5}, LX/267;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private k(Ljava/lang/Object;)LX/267;
    .locals 6

    .prologue
    .line 370692
    new-instance v0, LX/267;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/267;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private l(Ljava/lang/Object;)LX/267;
    .locals 6

    .prologue
    .line 370691
    new-instance v0, LX/267;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->c(Ljava/lang/Object;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v5}, LX/267;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private y()LX/267;
    .locals 6

    .prologue
    .line 370689
    iget-boolean v0, p0, LX/0lJ;->_asStatic:Z

    if-eqz v0, :cond_0

    .line 370690
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/267;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v2}, LX/0lJ;->b()LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, LX/267;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 370688
    invoke-direct {p0, p1}, LX/267;->i(Ljava/lang/Object;)LX/267;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/0lJ;
    .locals 1

    .prologue
    .line 370687
    invoke-direct {p0}, LX/267;->y()LX/267;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 370670
    invoke-direct {p0, p1}, LX/267;->j(Ljava/lang/Object;)LX/267;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 370686
    invoke-direct {p0, p1}, LX/267;->k(Ljava/lang/Object;)LX/267;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Class;)LX/0lJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 370669
    new-instance v0, LX/267;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    move-object v1, p1

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, LX/267;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 370671
    invoke-direct {p0, p1}, LX/267;->l(Ljava/lang/Object;)LX/267;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/Class;)LX/0lJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 370672
    iget-object v0, p0, LX/268;->_elementType:LX/0lJ;

    .line 370673
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 370674
    if-ne p1, v0, :cond_0

    .line 370675
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/267;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v5}, LX/267;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final synthetic e(Ljava/lang/Object;)LX/268;
    .locals 1

    .prologue
    .line 370676
    invoke-direct {p0, p1}, LX/267;->i(Ljava/lang/Object;)LX/267;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/Class;)LX/0lJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 370677
    iget-object v0, p0, LX/268;->_elementType:LX/0lJ;

    .line 370678
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 370679
    if-ne p1, v0, :cond_0

    .line 370680
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/267;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->c(Ljava/lang/Class;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v5}, LX/267;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final synthetic f(Ljava/lang/Object;)LX/268;
    .locals 1

    .prologue
    .line 370681
    invoke-direct {p0, p1}, LX/267;->j(Ljava/lang/Object;)LX/267;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic g(Ljava/lang/Object;)LX/268;
    .locals 1

    .prologue
    .line 370682
    invoke-direct {p0, p1}, LX/267;->k(Ljava/lang/Object;)LX/267;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h(Ljava/lang/Object;)LX/268;
    .locals 1

    .prologue
    .line 370683
    invoke-direct {p0, p1}, LX/267;->l(Ljava/lang/Object;)LX/267;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 370684
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[collection type; class "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", contains "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic w()LX/268;
    .locals 1

    .prologue
    .line 370685
    invoke-direct {p0}, LX/267;->y()LX/267;

    move-result-object v0

    return-object v0
.end method
