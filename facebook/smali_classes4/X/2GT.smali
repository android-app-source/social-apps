.class public final LX/2GT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile D:LX/2GT;


# instance fields
.field public A:LX/0W3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public B:LX/0Uo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public C:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public a:Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public b:Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public c:Ljava/util/Map;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public e:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public f:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public g:Lcom/facebook/proxygen/NetworkStatusMonitor;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public h:Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private i:Z

.field private j:J

.field private k:J

.field public l:Ljava/lang/String;

.field public m:LX/Ehs;

.field public n:LX/Ehr;

.field private o:Ljava/util/Timer;

.field private p:LX/0Yb;

.field private q:LX/Eho;

.field private r:LX/1iq;

.field public s:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/0kb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:LX/0So;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public w:LX/18b;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:LX/2GV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public y:LX/14F;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public z:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/liger/LigerHttpClientProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 388690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388691
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2GT;->c:Ljava/util/Map;

    .line 388692
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 388693
    iput-object v0, p0, LX/2GT;->z:LX/0Ot;

    .line 388694
    return-void
.end method

.method public static a(LX/0QB;)LX/2GT;
    .locals 14

    .prologue
    .line 388675
    sget-object v0, LX/2GT;->D:LX/2GT;

    if-nez v0, :cond_1

    .line 388676
    const-class v1, LX/2GT;

    monitor-enter v1

    .line 388677
    :try_start_0
    sget-object v0, LX/2GT;->D:LX/2GT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 388678
    if-eqz v2, :cond_0

    .line 388679
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 388680
    new-instance v3, LX/2GT;

    invoke-direct {v3}, LX/2GT;-><init>()V

    .line 388681
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v0}, LX/18b;->a(LX/0QB;)LX/18b;

    move-result-object v8

    check-cast v8, LX/18b;

    invoke-static {v0}, LX/2GU;->a(LX/0QB;)LX/2GV;

    move-result-object v9

    check-cast v9, LX/2GV;

    invoke-static {v0}, LX/14F;->b(LX/0QB;)LX/14F;

    move-result-object v10

    check-cast v10, LX/14F;

    const/16 v11, 0xb66

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v12

    check-cast v12, LX/0W3;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v13

    check-cast v13, LX/0Uo;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p0

    check-cast p0, LX/03V;

    .line 388682
    iput-object v4, v3, LX/2GT;->s:LX/0Uh;

    iput-object v5, v3, LX/2GT;->t:LX/0Xl;

    iput-object v6, v3, LX/2GT;->u:LX/0kb;

    iput-object v7, v3, LX/2GT;->v:LX/0So;

    iput-object v8, v3, LX/2GT;->w:LX/18b;

    iput-object v9, v3, LX/2GT;->x:LX/2GV;

    iput-object v10, v3, LX/2GT;->y:LX/14F;

    iput-object v11, v3, LX/2GT;->z:LX/0Ot;

    iput-object v12, v3, LX/2GT;->A:LX/0W3;

    iput-object v13, v3, LX/2GT;->B:LX/0Uo;

    iput-object p0, v3, LX/2GT;->C:LX/03V;

    .line 388683
    move-object v0, v3

    .line 388684
    sput-object v0, LX/2GT;->D:LX/2GT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 388685
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 388686
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 388687
    :cond_1
    sget-object v0, LX/2GT;->D:LX/2GT;

    return-object v0

    .line 388688
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 388689
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(ZI)LX/Ehr;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 388668
    if-eqz p0, :cond_0

    .line 388669
    sparse-switch p1, :sswitch_data_0

    .line 388670
    sget-object v0, LX/Ehr;->OTHER:LX/Ehr;

    .line 388671
    :goto_0
    return-object v0

    .line 388672
    :sswitch_0
    sget-object v0, LX/Ehr;->WIFI:LX/Ehr;

    goto :goto_0

    .line 388673
    :sswitch_1
    sget-object v0, LX/Ehr;->CELLULAR:LX/Ehr;

    goto :goto_0

    .line 388674
    :cond_0
    sget-object v0, LX/Ehr;->NOCONN:LX/Ehr;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2GT;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0    # LX/2GT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 388667
    const-string v1, "{\"%s\":%d,\"%s\":%d,\"%s\":%d,\"%s\":%d%s%s}"

    const/16 v0, 0xa

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v3, "timeslice_width"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    iget v3, p0, LX/2GT;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "trace_data_poll_period"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    iget v3, p0, LX/2GT;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "max_trace_duration"

    aput-object v3, v2, v0

    const/4 v0, 0x5

    iget v3, p0, LX/2GT;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x6

    const-string v3, "trace_duration"

    aput-object v3, v2, v0

    const/4 v0, 0x7

    iget-wide v4, p0, LX/2GT;->j:J

    iget-wide v6, p0, LX/2GT;->k:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v3, 0x8

    if-eqz p1, :cond_0

    const-string v0, ","

    :goto_0
    aput-object v0, v2, v3

    const/16 v0, 0x9

    if-eqz p1, :cond_1

    :goto_1
    aput-object p1, v2, v0

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string p1, ""

    goto :goto_1
.end method

.method private declared-synchronized a(J)V
    .locals 9

    .prologue
    .line 388635
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2GT;->n:LX/Ehr;

    sget-object v1, LX/Ehr;->WIFI:LX/Ehr;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2GT;->n:LX/Ehr;

    sget-object v1, LX/Ehr;->NOCONN:LX/Ehr;

    if-ne v0, v1, :cond_3

    .line 388636
    :cond_0
    iget-object v1, p0, LX/2GT;->x:LX/2GV;

    const v2, 0xb50001

    iget-object v0, p0, LX/2GT;->n:LX/Ehr;

    invoke-virtual {v0}, LX/Ehr;->name()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x1

    move-wide v6, p1

    invoke-virtual/range {v1 .. v7}, LX/2GV;->a(ILjava/lang/String;JJ)V

    .line 388637
    :cond_1
    :goto_0
    iget-object v0, p0, LX/2GT;->m:LX/Ehs;

    if-eqz v0, :cond_2

    .line 388638
    iget-object v0, p0, LX/2GT;->C:LX/03V;

    const-string v1, "CloudSeederHistogramData"

    const-string v2, "SignalStrengthMonitor not cleared"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388639
    :cond_2
    monitor-exit p0

    return-void

    .line 388640
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/2GT;->n:LX/Ehr;

    sget-object v1, LX/Ehr;->CELLULAR:LX/Ehr;

    if-ne v0, v1, :cond_1

    .line 388641
    iget-object v0, p0, LX/2GT;->g:Lcom/facebook/proxygen/NetworkStatusMonitor;

    invoke-virtual {v0}, Lcom/facebook/proxygen/NetworkStatusMonitor;->stopInboundHistogramTracingNative()V

    .line 388642
    iget-object v0, p0, LX/2GT;->o:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 388643
    const/4 v0, 0x0

    iput-object v0, p0, LX/2GT;->o:Ljava/util/Timer;

    .line 388644
    invoke-direct {p0}, LX/2GT;->g()V

    .line 388645
    iget-object v0, p0, LX/2GT;->m:LX/Ehs;

    if-eqz v0, :cond_5

    .line 388646
    iget-object v0, p0, LX/2GT;->m:LX/Ehs;

    .line 388647
    iget v1, v0, LX/Ehs;->c:I

    invoke-virtual {v0, v1}, LX/440;->a(I)Landroid/util/Pair;

    move-result-object v1

    move-object v0, v1

    .line 388648
    const/4 p1, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 388649
    iget-object v1, p0, LX/2GT;->m:LX/Ehs;

    if-eqz v1, :cond_7

    iget-object v1, p0, LX/2GT;->m:LX/Ehs;

    .line 388650
    iget-object v2, v1, LX/Ehs;->b:Landroid/util/Pair;

    move-object v1, v2

    .line 388651
    :goto_1
    if-nez v1, :cond_8

    .line 388652
    const/4 v1, 0x0

    .line 388653
    :goto_2
    move-object v1, v1

    .line 388654
    if-eqz v1, :cond_4

    .line 388655
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/2GT;->n:LX/Ehr;

    invoke-virtual {v3}, LX/Ehr;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/2GT;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 388656
    iget-object v3, p0, LX/2GT;->c:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 388657
    iget-object v3, p0, LX/2GT;->c:Ljava/util/Map;

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388658
    :cond_4
    :goto_3
    iget-object v0, p0, LX/2GT;->m:LX/Ehs;

    invoke-virtual {v0}, LX/440;->close()V

    .line 388659
    const/4 v0, 0x0

    iput-object v0, p0, LX/2GT;->m:LX/Ehs;

    .line 388660
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, LX/2GT;->l:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388661
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 388662
    :cond_6
    iget-object v1, p0, LX/2GT;->C:LX/03V;

    const-string v3, "CloudSeederHistogramData"

    const-string v4, "Missing histogram %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 388663
    :cond_7
    const/4 v1, 0x0

    goto :goto_1

    .line 388664
    :cond_8
    if-eqz v0, :cond_9

    .line 388665
    const-string v2, "\"%s\":\"%s\",\"%s\":\"%s\",\"%s\":%d,\"%s\":\"%s\",\"%s\":%d"

    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "network_subtype"

    aput-object v4, v3, v5

    iget-object v4, p0, LX/2GT;->m:LX/Ehs;

    invoke-virtual {v4}, LX/Ehs;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, "initial_method"

    aput-object v4, v3, v7

    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v4, v3, v8

    const-string v4, "initial_dbm"

    aput-object v4, v3, p1

    const/4 v4, 0x5

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v1, v3, v4

    const/4 v1, 0x6

    const-string v4, "final_method"

    aput-object v4, v3, v1

    const/4 v1, 0x7

    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v4, v3, v1

    const/16 v1, 0x8

    const-string v4, "final_dbm"

    aput-object v4, v3, v1

    const/16 v1, 0x9

    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 388666
    :cond_9
    const-string v2, "\"%s\":\"%s\",\"%s\":\"%s\",\"%s\":%d"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "network_subtype"

    aput-object v4, v3, v5

    iget-object v4, p0, LX/2GT;->m:LX/Ehs;

    invoke-virtual {v4}, LX/Ehs;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, "initial_method"

    aput-object v4, v3, v7

    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v4, v3, v8

    const-string v4, "initial_dbm"

    aput-object v4, v3, p1

    const/4 v4, 0x5

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2
.end method

.method private declared-synchronized a(Landroid/net/NetworkInfo;)V
    .locals 12
    .param p1    # Landroid/net/NetworkInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 388611
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/2GT;->b(Landroid/net/NetworkInfo;)LX/Ehr;

    move-result-object v7

    .line 388612
    invoke-virtual {v7}, LX/Ehr;->name()Ljava/lang/String;

    .line 388613
    iput-object v7, p0, LX/2GT;->n:LX/Ehr;

    .line 388614
    sget-object v0, LX/Ehq;->a:[I

    invoke-virtual {v7}, LX/Ehr;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 388615
    const-string v0, "CloudSeederHistogramData"

    const-string v1, "Unexpected ConnectivityType: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v7, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388616
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 388617
    :pswitch_0
    :try_start_1
    invoke-virtual {v7}, LX/Ehr;->name()Ljava/lang/String;

    move-result-object v0

    .line 388618
    :goto_1
    iget-object v1, p0, LX/2GT;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 388619
    sget-object v1, LX/Ehr;->CELLULAR:LX/Ehr;

    if-ne v7, v1, :cond_3

    .line 388620
    iget-object v1, p0, LX/2GT;->x:LX/2GV;

    const v2, 0xb50001

    .line 388621
    invoke-virtual {v1, v2}, LX/2GV;->b(I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 388622
    iget-object v8, v1, LX/2GV;->a:LX/2GW;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-virtual {v8, v9, v0, v10, v11}, LX/2GW;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 388623
    :cond_1
    :goto_2
    iget-object v1, p0, LX/2GT;->c:Ljava/util/Map;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388624
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 388625
    :pswitch_1
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LX/2GT;->h:Z

    .line 388626
    invoke-direct {p0}, LX/2GT;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2GT;->l:Ljava/lang/String;

    .line 388627
    iget-boolean v0, p0, LX/2GT;->i:Z

    if-eqz v0, :cond_2

    .line 388628
    new-instance v0, LX/Ehs;

    iget-object v1, p0, LX/2GT;->w:LX/18b;

    invoke-direct {v0, v1, p1}, LX/Ehs;-><init>(LX/18b;Landroid/net/NetworkInfo;)V

    iput-object v0, p0, LX/2GT;->m:LX/Ehs;

    .line 388629
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, LX/Ehr;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2GT;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 388630
    iget-object v0, p0, LX/2GT;->g:Lcom/facebook/proxygen/NetworkStatusMonitor;

    iget v1, p0, LX/2GT;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/proxygen/NetworkStatusMonitor;->startInboundHistogramTracingNative(I)V

    .line 388631
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, LX/2GT;->o:Ljava/util/Timer;

    .line 388632
    iget-object v0, p0, LX/2GT;->o:Ljava/util/Timer;

    new-instance v1, Lcom/facebook/cloudseeder/CloudSeederHistogramData$5;

    invoke-direct {v1, p0}, Lcom/facebook/cloudseeder/CloudSeederHistogramData$5;-><init>(LX/2GT;)V

    iget v2, p0, LX/2GT;->e:I

    int-to-long v2, v2

    iget v4, p0, LX/2GT;->e:I

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    move-object v0, v6

    .line 388633
    goto :goto_1

    .line 388634
    :cond_3
    iget-object v1, p0, LX/2GT;->x:LX/2GV;

    const v2, 0xb50001

    invoke-virtual {v1, v2, v0}, LX/2GV;->b(ILjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized a([J)V
    .locals 10

    .prologue
    .line 388603
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/2GT;->n:LX/Ehr;

    invoke-virtual {v1}, LX/Ehr;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2GT;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 388604
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-wide v4, p1, v0

    .line 388605
    iget-object v3, p0, LX/2GT;->x:LX/2GV;

    const v6, 0xb50001

    .line 388606
    invoke-virtual {v3, v6}, LX/2GV;->b(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 388607
    iget-object v7, v3, LX/2GV;->a:LX/2GW;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v1, v4, v5}, LX/2GW;->b(Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388608
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 388609
    :cond_1
    monitor-exit p0

    return-void

    .line 388610
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(Landroid/net/NetworkInfo;)LX/Ehr;
    .locals 2

    .prologue
    .line 388600
    if-nez p0, :cond_0

    .line 388601
    sget-object v0, LX/Ehr;->NOCONN:LX/Ehr;

    .line 388602
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    invoke-static {v0, v1}, LX/2GT;->a(ZI)LX/Ehr;

    move-result-object v0

    goto :goto_0
.end method

.method private declared-synchronized d()V
    .locals 3

    .prologue
    .line 388695
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2GT;->p:LX/0Yb;

    if-nez v0, :cond_0

    .line 388696
    iget-object v0, p0, LX/2GT;->t:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v2, LX/Ehn;

    invoke-direct {v2, p0}, LX/Ehn;-><init>(LX/2GT;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2GT;->p:LX/0Yb;

    .line 388697
    :cond_0
    iget-object v0, p0, LX/2GT;->p:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 388698
    iget-object v0, p0, LX/2GT;->q:LX/Eho;

    if-nez v0, :cond_1

    .line 388699
    new-instance v0, LX/Eho;

    invoke-direct {v0, p0}, LX/Eho;-><init>(LX/2GT;)V

    iput-object v0, p0, LX/2GT;->q:LX/Eho;

    .line 388700
    :cond_1
    iget-object v0, p0, LX/2GT;->w:LX/18b;

    iget-object v1, p0, LX/2GT;->q:LX/Eho;

    invoke-virtual {v0, v1}, LX/18b;->a(LX/Eho;)V

    .line 388701
    iget-object v0, p0, LX/2GT;->r:LX/1iq;

    if-nez v0, :cond_2

    .line 388702
    new-instance v0, LX/Ehp;

    invoke-direct {v0, p0}, LX/Ehp;-><init>(LX/2GT;)V

    iput-object v0, p0, LX/2GT;->r:LX/1iq;

    .line 388703
    :cond_2
    iget-object v0, p0, LX/2GT;->x:LX/2GV;

    iget-object v1, p0, LX/2GT;->r:LX/1iq;

    invoke-virtual {v0, v1}, LX/2GV;->a(LX/1iq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388704
    monitor-exit p0

    return-void

    .line 388705
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 2

    .prologue
    .line 388491
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2GT;->p:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 388492
    iget-object v0, p0, LX/2GT;->w:LX/18b;

    iget-object v1, p0, LX/2GT;->q:LX/Eho;

    invoke-virtual {v0, v1}, LX/18b;->b(LX/Eho;)V

    .line 388493
    iget-object v0, p0, LX/2GT;->x:LX/2GV;

    iget-object v1, p0, LX/2GT;->r:LX/1iq;

    invoke-virtual {v0, v1}, LX/2GV;->b(LX/1iq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388494
    monitor-exit p0

    return-void

    .line 388495
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized f(LX/2GT;)V
    .locals 6

    .prologue
    .line 388496
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2GT;->n:LX/Ehr;

    invoke-virtual {v0}, LX/Ehr;->name()Ljava/lang/String;

    invoke-direct {p0}, LX/2GT;->h()LX/Ehr;

    move-result-object v0

    invoke-virtual {v0}, LX/Ehr;->name()Ljava/lang/String;

    .line 388497
    iget-boolean v0, p0, LX/2GT;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 388498
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 388499
    :cond_1
    :try_start_1
    invoke-direct {p0}, LX/2GT;->h()LX/Ehr;

    move-result-object v0

    .line 388500
    iget-object v1, p0, LX/2GT;->n:LX/Ehr;

    if-ne v0, v1, :cond_3

    .line 388501
    iget-object v0, p0, LX/2GT;->n:LX/Ehr;

    sget-object v1, LX/Ehr;->CELLULAR:LX/Ehr;

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, LX/2GT;->i()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/2GT;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget-object v0, p0, LX/2GT;->n:LX/Ehr;

    sget-object v1, LX/Ehr;->WIFI:LX/Ehr;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2GT;->n:LX/Ehr;

    sget-object v1, LX/Ehr;->NOCONN:LX/Ehr;

    if-eq v0, v1, :cond_0

    .line 388502
    :cond_3
    iget-object v0, p0, LX/2GT;->v:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 388503
    iget-wide v2, p0, LX/2GT;->k:J

    sub-long v2, v0, v2

    iget v4, p0, LX/2GT;->f:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    .line 388504
    invoke-virtual {p0}, LX/2GT;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388505
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 388506
    :cond_4
    :try_start_2
    iget-wide v2, p0, LX/2GT;->j:J

    .line 388507
    sub-long v2, v0, v2

    iget v4, p0, LX/2GT;->d:I

    int-to-long v4, v4

    div-long/2addr v2, v4

    .line 388508
    iput-wide v0, p0, LX/2GT;->j:J

    .line 388509
    invoke-direct {p0, v2, v3}, LX/2GT;->a(J)V

    .line 388510
    iget-object v0, p0, LX/2GT;->u:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2GT;->a(Landroid/net/NetworkInfo;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized g()V
    .locals 2

    .prologue
    .line 388511
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2GT;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 388512
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 388513
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2GT;->g:Lcom/facebook/proxygen/NetworkStatusMonitor;

    invoke-virtual {v0}, Lcom/facebook/proxygen/NetworkStatusMonitor;->getInboundHistogramTraceDataNative()[J

    move-result-object v0

    .line 388514
    if-eqz v0, :cond_0

    array-length v1, v0

    if-eqz v1, :cond_0

    .line 388515
    invoke-direct {p0, v0}, LX/2GT;->a([J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388516
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h()LX/Ehr;
    .locals 1

    .prologue
    .line 388517
    iget-object v0, p0, LX/2GT;->u:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 388518
    invoke-static {v0}, LX/2GT;->b(Landroid/net/NetworkInfo;)LX/Ehr;

    move-result-object v0

    return-object v0
.end method

.method private i()Ljava/lang/String;
    .locals 3

    .prologue
    .line 388519
    iget-object v0, p0, LX/2GT;->w:LX/18b;

    .line 388520
    invoke-static {v0}, LX/18b;->k(LX/18b;)V

    .line 388521
    iget v1, v0, LX/18b;->i:I

    move v0, v1

    .line 388522
    packed-switch v0, :pswitch_data_0

    .line 388523
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 388524
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CDMA_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2GT;->w:LX/18b;

    invoke-virtual {v1}, LX/18b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2GT;->w:LX/18b;

    .line 388525
    invoke-static {v1}, LX/18b;->k(LX/18b;)V

    .line 388526
    iget-object v2, v1, LX/18b;->e:Ljava/lang/String;

    move-object v1, v2

    .line 388527
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2GT;->w:LX/18b;

    .line 388528
    invoke-static {v1}, LX/18b;->k(LX/18b;)V

    .line 388529
    iget-object v2, v1, LX/18b;->f:Ljava/lang/String;

    move-object v1, v2

    .line 388530
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 388531
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GSM_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2GT;->w:LX/18b;

    invoke-virtual {v1}, LX/18b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2GT;->w:LX/18b;

    invoke-virtual {v1}, LX/18b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2GT;->w:LX/18b;

    .line 388532
    invoke-static {v1}, LX/18b;->k(LX/18b;)V

    .line 388533
    iget-object v2, v1, LX/18b;->g:Ljava/lang/String;

    move-object v1, v2

    .line 388534
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2GT;->w:LX/18b;

    .line 388535
    invoke-static {v1}, LX/18b;->k(LX/18b;)V

    .line 388536
    iget-object v2, v1, LX/18b;->h:Ljava/lang/String;

    move-object v1, v2

    .line 388537
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 388538
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2GT;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 388539
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 388540
    :cond_1
    :try_start_1
    invoke-direct {p0}, LX/2GT;->g()V

    .line 388541
    iget-object v0, p0, LX/2GT;->v:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/2GT;->k:J

    sub-long/2addr v0, v2

    iget v2, p0, LX/2GT;->f:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 388542
    invoke-virtual {p0}, LX/2GT;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388543
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 388544
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2GT;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 388545
    :goto_0
    monitor-exit p0

    return-void

    .line 388546
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2GT;->v:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 388547
    iget-wide v2, p0, LX/2GT;->j:J

    .line 388548
    sub-long v2, v0, v2

    iget v4, p0, LX/2GT;->d:I

    int-to-long v4, v4

    div-long/2addr v2, v4

    .line 388549
    iput-wide v0, p0, LX/2GT;->j:J

    .line 388550
    invoke-direct {p0}, LX/2GT;->e()V

    .line 388551
    invoke-direct {p0, v2, v3}, LX/2GT;->a(J)V

    .line 388552
    iget-boolean v0, p0, LX/2GT;->h:Z

    if-eqz v0, :cond_5

    .line 388553
    const v5, 0xb50001

    .line 388554
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/2GT;->a(LX/2GT;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 388555
    iget-object v0, p0, LX/2GT;->x:LX/2GV;

    .line 388556
    invoke-virtual {v0, v5}, LX/2GV;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 388557
    iget-object v1, v0, LX/2GV;->a:LX/2GW;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, LX/2GW;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 388558
    :cond_1
    iget-object v0, p0, LX/2GT;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 388559
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 388560
    if-eqz v1, :cond_3

    invoke-static {p0, v1}, LX/2GT;->a(LX/2GT;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 388561
    :goto_2
    iget-object v4, p0, LX/2GT;->x:LX/2GV;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 388562
    invoke-virtual {v4, v5}, LX/2GV;->b(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 388563
    iget-object v6, v4, LX/2GV;->a:LX/2GW;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v0, v1}, LX/2GW;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 388564
    :cond_2
    goto :goto_1

    :cond_3
    move-object v1, v2

    .line 388565
    goto :goto_2

    .line 388566
    :cond_4
    :goto_3
    iget-object v0, p0, LX/2GT;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 388567
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2GT;->h:Z

    .line 388568
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2GT;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388569
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 388570
    :cond_5
    :try_start_2
    iget-object v0, p0, LX/2GT;->x:LX/2GV;

    const v1, 0xb50001

    .line 388571
    invoke-virtual {v0, v1}, LX/2GV;->b(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 388572
    iget-object v2, v0, LX/2GV;->a:LX/2GW;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2GW;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 388573
    :cond_6
    goto :goto_3
.end method

.method public final declared-synchronized c()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 388574
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2GT;->x:LX/2GV;

    const v1, 0xb50001

    invoke-virtual {v0, v1}, LX/2GV;->b(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 388575
    if-nez v0, :cond_0

    .line 388576
    :goto_0
    monitor-exit p0

    return-void

    .line 388577
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2GT;->A:LX/0W3;

    sget-wide v2, LX/0X5;->iX:J

    const/16 v1, 0x64

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/2GT;->d:I

    .line 388578
    iget-object v0, p0, LX/2GT;->A:LX/0W3;

    sget-wide v2, LX/0X5;->iY:J

    const/16 v1, 0x3e8

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/2GT;->e:I

    .line 388579
    iget-object v0, p0, LX/2GT;->A:LX/0W3;

    sget-wide v2, LX/0X5;->iZ:J

    const v1, 0xdbba0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/2GT;->f:I

    .line 388580
    iget-object v0, p0, LX/2GT;->v:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/2GT;->j:J

    .line 388581
    iget-wide v0, p0, LX/2GT;->j:J

    iput-wide v0, p0, LX/2GT;->k:J

    .line 388582
    invoke-direct {p0}, LX/2GT;->d()V

    .line 388583
    iget-object v0, p0, LX/2GT;->u:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2GT;->a(Landroid/net/NetworkInfo;)V

    .line 388584
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2GT;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388585
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final init()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 388586
    iget-object v0, p0, LX/2GT;->s:LX/0Uh;

    const/16 v2, 0x37f

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2GT;->w:LX/18b;

    invoke-virtual {v0}, LX/18b;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/2GT;->a:Z

    .line 388587
    iget-boolean v0, p0, LX/2GT;->a:Z

    if-nez v0, :cond_2

    .line 388588
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 388589
    goto :goto_0

    .line 388590
    :cond_2
    iget-object v0, p0, LX/2GT;->s:LX/0Uh;

    const/16 v2, 0x380

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2GT;->i:Z

    .line 388591
    iget-object v0, p0, LX/2GT;->y:LX/14F;

    invoke-virtual {v0}, LX/14F;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 388592
    const-string v0, "CloudSeederHistogramData"

    const-string v2, "CloudSeederHistogramData disabled due to Liger load failure"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 388593
    iput-boolean v1, p0, LX/2GT;->a:Z

    goto :goto_1

    .line 388594
    :cond_3
    iget-object v0, p0, LX/2GT;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MP;

    .line 388595
    iget-object v1, v0, LX/1MP;->s:LX/1hL;

    move-object v0, v1

    .line 388596
    iput-object v0, p0, LX/2GT;->g:Lcom/facebook/proxygen/NetworkStatusMonitor;

    .line 388597
    iget-object v0, p0, LX/2GT;->t:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v2, LX/Ehm;

    invoke-direct {v2, p0}, LX/Ehm;-><init>(LX/2GT;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 388598
    iget-object v0, p0, LX/2GT;->B:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388599
    invoke-virtual {p0}, LX/2GT;->c()V

    goto :goto_1
.end method
