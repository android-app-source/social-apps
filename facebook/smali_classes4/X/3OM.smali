.class public final enum LX/3OM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3OM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3OM;

.field public static final enum NONE:LX/3OM;

.field public static final enum ON_FACEBOOK:LX/3OM;

.field public static final enum ON_MESSENGER:LX/3OM;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 560411
    new-instance v0, LX/3OM;

    const-string v1, "ON_MESSENGER"

    invoke-direct {v0, v1, v2}, LX/3OM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OM;->ON_MESSENGER:LX/3OM;

    .line 560412
    new-instance v0, LX/3OM;

    const-string v1, "ON_FACEBOOK"

    invoke-direct {v0, v1, v3}, LX/3OM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OM;->ON_FACEBOOK:LX/3OM;

    .line 560413
    new-instance v0, LX/3OM;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/3OM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OM;->NONE:LX/3OM;

    .line 560414
    const/4 v0, 0x3

    new-array v0, v0, [LX/3OM;

    sget-object v1, LX/3OM;->ON_MESSENGER:LX/3OM;

    aput-object v1, v0, v2

    sget-object v1, LX/3OM;->ON_FACEBOOK:LX/3OM;

    aput-object v1, v0, v3

    sget-object v1, LX/3OM;->NONE:LX/3OM;

    aput-object v1, v0, v4

    sput-object v0, LX/3OM;->$VALUES:[LX/3OM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560415
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3OM;
    .locals 1

    .prologue
    .line 560416
    const-class v0, LX/3OM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3OM;

    return-object v0
.end method

.method public static values()[LX/3OM;
    .locals 1

    .prologue
    .line 560417
    sget-object v0, LX/3OM;->$VALUES:[LX/3OM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3OM;

    return-object v0
.end method
