.class public LX/2RA;
.super LX/2QZ;
.source ""


# instance fields
.field private final b:LX/2Qt;


# direct methods
.method public constructor <init>(LX/2Qt;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409522
    const-string v0, "platform_delete_temp_files"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 409523
    iput-object p1, p0, LX/2RA;->b:LX/2Qt;

    .line 409524
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 409525
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 409526
    const-string v1, "platform_delete_temp_files_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 409527
    iget-object v1, p0, LX/2RA;->b:LX/2Qt;

    .line 409528
    iget-object p0, v1, LX/2Qt;->a:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p0

    .line 409529
    const-string p1, "platform"

    invoke-static {p0, p1}, LX/0en;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object p0

    .line 409530
    invoke-static {p0, v0}, LX/0en;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object p0

    .line 409531
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 409532
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 409533
    invoke-static {p0}, LX/2W9;->a(Ljava/io/File;)Z

    .line 409534
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 409535
    :cond_1
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 409536
    return-object v0
.end method
