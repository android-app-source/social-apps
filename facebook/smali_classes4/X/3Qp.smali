.class public LX/3Qp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Pw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile g:LX/3Qp;


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Xl;

.field private final e:LX/0WJ;

.field private final f:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 565966
    const-class v0, LX/3Qp;

    sput-object v0, LX/3Qp;->a:Ljava/lang/Class;

    .line 565967
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/notifications/constants/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->ZERO:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/3Qp;->b:LX/2QP;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Xl;LX/0WJ;LX/0Zb;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/push/annotations/IsZeroMobilePushEnabled;
        .end annotation
    .end param
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Xl;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565948
    iput-object p1, p0, LX/3Qp;->c:LX/0Or;

    .line 565949
    iput-object p2, p0, LX/3Qp;->d:LX/0Xl;

    .line 565950
    iput-object p3, p0, LX/3Qp;->e:LX/0WJ;

    .line 565951
    iput-object p4, p0, LX/3Qp;->f:LX/0Zb;

    .line 565952
    return-void
.end method

.method public static a(LX/0QB;)LX/3Qp;
    .locals 7

    .prologue
    .line 565953
    sget-object v0, LX/3Qp;->g:LX/3Qp;

    if-nez v0, :cond_1

    .line 565954
    const-class v1, LX/3Qp;

    monitor-enter v1

    .line 565955
    :try_start_0
    sget-object v0, LX/3Qp;->g:LX/3Qp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 565956
    if-eqz v2, :cond_0

    .line 565957
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 565958
    new-instance v6, LX/3Qp;

    const/16 v3, 0x389

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-direct {v6, p0, v3, v4, v5}, LX/3Qp;-><init>(LX/0Or;LX/0Xl;LX/0WJ;LX/0Zb;)V

    .line 565959
    move-object v0, v6

    .line 565960
    sput-object v0, LX/3Qp;->g:LX/3Qp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565961
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 565962
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565963
    :cond_1
    sget-object v0, LX/3Qp;->g:LX/3Qp;

    return-object v0

    .line 565964
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 565965
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 565938
    sget-object v0, LX/3Qp;->b:LX/2QP;

    return-object v0
.end method

.method public final a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 4

    .prologue
    .line 565939
    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, LX/3Qp;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 565940
    :cond_0
    :goto_0
    return-void

    .line 565941
    :cond_1
    iget-object v0, p0, LX/3Qp;->e:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565942
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "zero_push_to_refresh_token"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "zero_push"

    .line 565943
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 565944
    move-object v0, v0

    .line 565945
    iget-object v1, p0, LX/3Qp;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 565946
    iget-object v0, p0, LX/3Qp;->d:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "zero_token_request_reason"

    sget-object v3, LX/32P;->PUSH:LX/32P;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
