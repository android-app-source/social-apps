.class public LX/3Ji;
.super LX/3Jj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3Jj",
        "<",
        "LX/3Jc;",
        "Landroid/graphics/Matrix;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3JT;

.field public final b:[F
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;[[[FLX/3JT;[F)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3Jc;",
            ">;[[[F",
            "LX/3JT;",
            "[F)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 547831
    invoke-direct {p0, p1, p2}, LX/3Jj;-><init>(Ljava/util/List;[[[F)V

    .line 547832
    iput-object p3, p0, LX/3Ji;->a:LX/3JT;

    .line 547833
    if-eqz p4, :cond_1

    :goto_0
    iput-object p4, p0, LX/3Ji;->b:[F

    .line 547834
    sget-object v0, LX/3JT;->POSITION:LX/3JT;

    if-ne p3, v0, :cond_0

    .line 547835
    iget-object v1, p0, LX/3Ji;->b:[F

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jc;

    .line 547836
    iget-object p2, v0, LX/3Jc;->b:[F

    move-object v0, p2

    .line 547837
    aget v0, v0, v2

    aput v0, v1, v2

    .line 547838
    iget-object v1, p0, LX/3Ji;->b:[F

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Jc;

    .line 547839
    iget-object v2, v0, LX/3Jc;->b:[F

    move-object v0, v2

    .line 547840
    aget v0, v0, v3

    aput v0, v1, v3

    .line 547841
    :cond_0
    return-void

    .line 547842
    :cond_1
    const/4 v0, 0x2

    new-array p4, v0, [F

    goto :goto_0
.end method

.method public static a(LX/3Jf;)LX/3Ji;
    .locals 5

    .prologue
    .line 547843
    iget-object v0, p0, LX/3Jf;->b:LX/3JT;

    move-object v0, v0

    .line 547844
    invoke-virtual {v0}, LX/3JT;->isMatrixBased()Z

    move-result v0

    if-nez v0, :cond_0

    .line 547845
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot create a KeyFramedMatrixAnimation from a non matrix based KFAnimation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 547846
    :cond_0
    new-instance v0, LX/3Ji;

    .line 547847
    iget-object v1, p0, LX/3Jf;->c:Ljava/util/List;

    move-object v1, v1

    .line 547848
    iget-object v2, p0, LX/3Jf;->d:[[[F

    move-object v2, v2

    .line 547849
    iget-object v3, p0, LX/3Jf;->b:LX/3JT;

    move-object v3, v3

    .line 547850
    iget-object v4, p0, LX/3Jf;->e:[F

    move-object v4, v4

    .line 547851
    invoke-direct {v0, v1, v2, v3, v4}, LX/3Ji;-><init>(Ljava/util/List;[[[FLX/3JT;[F)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/3Jd;LX/3Jd;FLjava/lang/Object;)V
    .locals 8

    .prologue
    .line 547852
    check-cast p1, LX/3Jc;

    check-cast p2, LX/3Jc;

    check-cast p4, Landroid/graphics/Matrix;

    .line 547853
    sget-object v0, LX/9Un;->a:[I

    iget-object v1, p0, LX/3Ji;->a:LX/3JT;

    invoke-virtual {v1}, LX/3JT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 547854
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot apply matrix transformation to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/3Ji;->a:LX/3JT;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 547855
    :pswitch_0
    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 547856
    if-nez p2, :cond_3

    .line 547857
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 547858
    aget v2, v0, v3

    iget-object v0, p0, LX/3Ji;->b:[F

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3Ji;->b:[F

    aget v0, v0, v3

    :goto_0
    iget-object v3, p0, LX/3Ji;->b:[F

    if-eqz v3, :cond_0

    iget-object v1, p0, LX/3Ji;->b:[F

    aget v1, v1, v4

    :cond_0
    invoke-virtual {p4, v2, v0, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 547859
    :goto_1
    return-void

    .line 547860
    :pswitch_1
    const/high16 v7, 0x42c80000    # 100.0f

    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 547861
    if-nez p2, :cond_7

    .line 547862
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 547863
    aget v0, v0, v5

    div-float v2, v0, v7

    .line 547864
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 547865
    aget v0, v0, v6

    div-float v3, v0, v7

    iget-object v0, p0, LX/3Ji;->b:[F

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/3Ji;->b:[F

    aget v0, v0, v5

    :goto_2
    iget-object v4, p0, LX/3Ji;->b:[F

    if-eqz v4, :cond_1

    iget-object v1, p0, LX/3Ji;->b:[F

    aget v1, v1, v6

    :cond_1
    invoke-virtual {p4, v2, v3, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 547866
    :goto_3
    goto :goto_1

    .line 547867
    :pswitch_2
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 547868
    if-nez p2, :cond_a

    .line 547869
    :goto_4
    goto :goto_1

    .line 547870
    :pswitch_3
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 547871
    if-nez p2, :cond_b

    .line 547872
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 547873
    aget v0, v0, v2

    invoke-virtual {p4, v0, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 547874
    :goto_5
    goto :goto_1

    .line 547875
    :pswitch_4
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 547876
    if-nez p2, :cond_c

    .line 547877
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 547878
    aget v0, v0, v2

    invoke-virtual {p4, v3, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 547879
    :goto_6
    goto :goto_1

    :cond_2
    move v0, v1

    .line 547880
    goto :goto_0

    .line 547881
    :cond_3
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 547882
    aget v0, v0, v3

    .line 547883
    iget-object v2, p2, LX/3Jc;->b:[F

    move-object v2, v2

    .line 547884
    aget v2, v2, v3

    .line 547885
    invoke-static {v0, v2, p3}, LX/3Jj;->a(FFF)F

    move-result v2

    iget-object v0, p0, LX/3Ji;->b:[F

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/3Ji;->b:[F

    aget v0, v0, v3

    :goto_7
    iget-object v3, p0, LX/3Ji;->b:[F

    if-eqz v3, :cond_4

    iget-object v1, p0, LX/3Ji;->b:[F

    aget v1, v1, v4

    :cond_4
    invoke-virtual {p4, v2, v0, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_7

    :cond_6
    move v0, v1

    .line 547886
    goto :goto_2

    .line 547887
    :cond_7
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 547888
    aget v0, v0, v5

    .line 547889
    iget-object v2, p2, LX/3Jc;->b:[F

    move-object v2, v2

    .line 547890
    aget v2, v2, v5

    .line 547891
    iget-object v3, p1, LX/3Jc;->b:[F

    move-object v3, v3

    .line 547892
    aget v3, v3, v6

    .line 547893
    iget-object v4, p2, LX/3Jc;->b:[F

    move-object v4, v4

    .line 547894
    aget v4, v4, v6

    .line 547895
    invoke-static {v0, v2, p3}, LX/3Jj;->a(FFF)F

    move-result v0

    div-float v2, v0, v7

    invoke-static {v3, v4, p3}, LX/3Jj;->a(FFF)F

    move-result v0

    div-float v3, v0, v7

    iget-object v0, p0, LX/3Ji;->b:[F

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/3Ji;->b:[F

    aget v0, v0, v5

    :goto_8
    iget-object v4, p0, LX/3Ji;->b:[F

    if-eqz v4, :cond_8

    iget-object v1, p0, LX/3Ji;->b:[F

    aget v1, v1, v6

    :cond_8
    invoke-virtual {p4, v2, v3, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_8

    .line 547896
    :cond_a
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 547897
    aget v0, v0, v4

    .line 547898
    iget-object v1, p2, LX/3Jc;->b:[F

    move-object v1, v1

    .line 547899
    aget v1, v1, v4

    .line 547900
    iget-object v2, p1, LX/3Jc;->b:[F

    move-object v2, v2

    .line 547901
    aget v2, v2, v5

    .line 547902
    iget-object v3, p2, LX/3Jc;->b:[F

    move-object v3, v3

    .line 547903
    aget v3, v3, v5

    .line 547904
    invoke-static {v0, v1, p3}, LX/3Jj;->a(FFF)F

    move-result v0

    iget-object v1, p0, LX/3Ji;->b:[F

    aget v1, v1, v4

    sub-float/2addr v0, v1

    invoke-static {v2, v3, p3}, LX/3Jj;->a(FFF)F

    move-result v1

    iget-object v2, p0, LX/3Ji;->b:[F

    aget v2, v2, v5

    sub-float/2addr v1, v2

    invoke-virtual {p4, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_4

    .line 547905
    :cond_b
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 547906
    aget v0, v0, v2

    .line 547907
    iget-object v1, p2, LX/3Jc;->b:[F

    move-object v1, v1

    .line 547908
    aget v1, v1, v2

    .line 547909
    invoke-static {v0, v1, p3}, LX/3Jj;->a(FFF)F

    move-result v0

    invoke-virtual {p4, v0, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_5

    .line 547910
    :cond_c
    iget-object v0, p1, LX/3Jc;->b:[F

    move-object v0, v0

    .line 547911
    aget v0, v0, v2

    .line 547912
    iget-object v1, p2, LX/3Jc;->b:[F

    move-object v1, v1

    .line 547913
    aget v1, v1, v2

    .line 547914
    invoke-static {v0, v1, p3}, LX/3Jj;->a(FFF)F

    move-result v0

    invoke-virtual {p4, v3, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
