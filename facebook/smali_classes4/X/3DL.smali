.class public LX/3DL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0rS;

.field public b:I

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public g:Ljava/lang/String;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:Z

.field public k:I

.field public l:Ljava/lang/String;

.field public m:Z

.field public n:Ljava/lang/String;

.field public o:Z

.field public p:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v0, 0xa

    const/4 v1, 0x0

    .line 532115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 532116
    iput v0, p0, LX/3DL;->b:I

    .line 532117
    iput v0, p0, LX/3DL;->c:I

    .line 532118
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/3DL;->h:Ljava/util/List;

    .line 532119
    iput-boolean v1, p0, LX/3DL;->j:Z

    .line 532120
    iput-boolean v1, p0, LX/3DL;->m:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)LX/3DL;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/3DL;"
        }
    .end annotation

    .prologue
    .line 532121
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 532122
    iput-object p1, p0, LX/3DL;->h:Ljava/util/List;

    .line 532123
    return-object p0
.end method

.method public final q()Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;
    .locals 1

    .prologue
    .line 532124
    iget-object v0, p0, LX/3DL;->a:LX/0rS;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 532125
    new-instance v0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    invoke-direct {v0, p0}, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;-><init>(LX/3DL;)V

    return-object v0
.end method
