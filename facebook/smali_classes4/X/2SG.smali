.class public final LX/2SG;
.super Ljava/io/OutputStream;
.source ""


# static fields
.field private static final a:[B


# instance fields
.field private final b:LX/12B;

.field private final c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[B>;"
        }
    .end annotation
.end field

.field private d:I

.field public e:[B

.field public f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 412051
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, LX/2SG;->a:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 412050
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2SG;-><init>(LX/12B;)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 412049
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/2SG;-><init>(LX/12B;I)V

    return-void
.end method

.method public constructor <init>(LX/12B;)V
    .locals 1

    .prologue
    .line 412048
    const/16 v0, 0x1f4

    invoke-direct {p0, p1, v0}, LX/2SG;-><init>(LX/12B;I)V

    return-void
.end method

.method private constructor <init>(LX/12B;I)V
    .locals 1

    .prologue
    .line 412041
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 412042
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2SG;->c:Ljava/util/LinkedList;

    .line 412043
    iput-object p1, p0, LX/2SG;->b:LX/12B;

    .line 412044
    if-nez p1, :cond_0

    .line 412045
    new-array v0, p2, [B

    iput-object v0, p0, LX/2SG;->e:[B

    .line 412046
    :goto_0
    return-void

    .line 412047
    :cond_0
    sget-object v0, LX/12C;->WRITE_CONCAT_BUFFER:LX/12C;

    invoke-virtual {p1, v0}, LX/12B;->a(LX/12C;)[B

    move-result-object v0

    iput-object v0, p0, LX/2SG;->e:[B

    goto :goto_0
.end method

.method private h()V
    .locals 3

    .prologue
    const/high16 v0, 0x40000

    .line 412034
    iget v1, p0, LX/2SG;->d:I

    iget-object v2, p0, LX/2SG;->e:[B

    array-length v2, v2

    add-int/2addr v1, v2

    iput v1, p0, LX/2SG;->d:I

    .line 412035
    iget v1, p0, LX/2SG;->d:I

    shr-int/lit8 v1, v1, 0x1

    const/16 v2, 0x3e8

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 412036
    if-le v1, v0, :cond_0

    .line 412037
    :goto_0
    iget-object v1, p0, LX/2SG;->c:Ljava/util/LinkedList;

    iget-object v2, p0, LX/2SG;->e:[B

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 412038
    new-array v0, v0, [B

    iput-object v0, p0, LX/2SG;->e:[B

    .line 412039
    const/4 v0, 0x0

    iput v0, p0, LX/2SG;->f:I

    .line 412040
    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 412029
    iput v0, p0, LX/2SG;->d:I

    .line 412030
    iput v0, p0, LX/2SG;->f:I

    .line 412031
    iget-object v0, p0, LX/2SG;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 412032
    iget-object v0, p0, LX/2SG;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 412033
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 412006
    iget v0, p0, LX/2SG;->f:I

    iget-object v1, p0, LX/2SG;->e:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 412007
    invoke-direct {p0}, LX/2SG;->h()V

    .line 412008
    :cond_0
    iget-object v0, p0, LX/2SG;->e:[B

    iget v1, p0, LX/2SG;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2SG;->f:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 412009
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 412024
    invoke-virtual {p0}, LX/2SG;->a()V

    .line 412025
    iget-object v0, p0, LX/2SG;->b:LX/12B;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2SG;->e:[B

    if-eqz v0, :cond_0

    .line 412026
    iget-object v0, p0, LX/2SG;->b:LX/12B;

    sget-object v1, LX/12C;->WRITE_CONCAT_BUFFER:LX/12C;

    iget-object v2, p0, LX/2SG;->e:[B

    invoke-virtual {v0, v1, v2}, LX/12B;->a(LX/12C;[B)V

    .line 412027
    const/4 v0, 0x0

    iput-object v0, p0, LX/2SG;->e:[B

    .line 412028
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 412018
    iget v0, p0, LX/2SG;->f:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/2SG;->e:[B

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 412019
    iget-object v0, p0, LX/2SG;->e:[B

    iget v1, p0, LX/2SG;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2SG;->f:I

    shr-int/lit8 v2, p1, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 412020
    iget-object v0, p0, LX/2SG;->e:[B

    iget v1, p0, LX/2SG;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2SG;->f:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 412021
    :goto_0
    return-void

    .line 412022
    :cond_0
    shr-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, LX/2SG;->a(I)V

    .line 412023
    invoke-virtual {p0, p1}, LX/2SG;->a(I)V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 412010
    iget v0, p0, LX/2SG;->f:I

    add-int/lit8 v0, v0, 0x2

    iget-object v1, p0, LX/2SG;->e:[B

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 412011
    iget-object v0, p0, LX/2SG;->e:[B

    iget v1, p0, LX/2SG;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2SG;->f:I

    shr-int/lit8 v2, p1, 0x10

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 412012
    iget-object v0, p0, LX/2SG;->e:[B

    iget v1, p0, LX/2SG;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2SG;->f:I

    shr-int/lit8 v2, p1, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 412013
    iget-object v0, p0, LX/2SG;->e:[B

    iget v1, p0, LX/2SG;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2SG;->f:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 412014
    :goto_0
    return-void

    .line 412015
    :cond_0
    shr-int/lit8 v0, p1, 0x10

    invoke-virtual {p0, v0}, LX/2SG;->a(I)V

    .line 412016
    shr-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, LX/2SG;->a(I)V

    .line 412017
    invoke-virtual {p0, p1}, LX/2SG;->a(I)V

    goto :goto_0
.end method

.method public final c()[B
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 411967
    iget v0, p0, LX/2SG;->d:I

    iget v1, p0, LX/2SG;->f:I

    add-int v4, v0, v1

    .line 411968
    if-nez v4, :cond_0

    .line 411969
    sget-object v0, LX/2SG;->a:[B

    .line 411970
    :goto_0
    return-object v0

    .line 411971
    :cond_0
    new-array v3, v4, [B

    .line 411972
    iget-object v0, p0, LX/2SG;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 411973
    array-length v6, v0

    .line 411974
    invoke-static {v0, v2, v3, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 411975
    add-int v0, v1, v6

    move v1, v0

    .line 411976
    goto :goto_1

    .line 411977
    :cond_1
    iget-object v0, p0, LX/2SG;->e:[B

    iget v5, p0, LX/2SG;->f:I

    invoke-static {v0, v2, v3, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 411978
    iget v0, p0, LX/2SG;->f:I

    add-int/2addr v0, v1

    .line 411979
    if-eq v0, v4, :cond_2

    .line 411980
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Internal error: total len assumed to be "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", copied "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 411981
    :cond_2
    iget-object v0, p0, LX/2SG;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 411982
    invoke-virtual {p0}, LX/2SG;->a()V

    :cond_3
    move-object v0, v3

    .line 411983
    goto :goto_0
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 411984
    return-void
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 411985
    invoke-virtual {p0}, LX/2SG;->a()V

    .line 411986
    iget-object v0, p0, LX/2SG;->e:[B

    return-object v0
.end method

.method public final d(I)[B
    .locals 1

    .prologue
    .line 411987
    iput p1, p0, LX/2SG;->f:I

    .line 411988
    invoke-virtual {p0}, LX/2SG;->c()[B

    move-result-object v0

    return-object v0
.end method

.method public final e()[B
    .locals 1

    .prologue
    .line 411989
    invoke-direct {p0}, LX/2SG;->h()V

    .line 411990
    iget-object v0, p0, LX/2SG;->e:[B

    return-object v0
.end method

.method public final flush()V
    .locals 0

    .prologue
    .line 411991
    return-void
.end method

.method public final write(I)V
    .locals 0

    .prologue
    .line 411992
    invoke-virtual {p0, p1}, LX/2SG;->a(I)V

    .line 411993
    return-void
.end method

.method public final write([B)V
    .locals 2

    .prologue
    .line 411994
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/2SG;->write([BII)V

    .line 411995
    return-void
.end method

.method public final write([BII)V
    .locals 3

    .prologue
    .line 411996
    :goto_0
    iget-object v0, p0, LX/2SG;->e:[B

    array-length v0, v0

    iget v1, p0, LX/2SG;->f:I

    sub-int/2addr v0, v1

    .line 411997
    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 411998
    if-lez v0, :cond_0

    .line 411999
    iget-object v1, p0, LX/2SG;->e:[B

    iget v2, p0, LX/2SG;->f:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 412000
    add-int/2addr p2, v0

    .line 412001
    iget v1, p0, LX/2SG;->f:I

    add-int/2addr v1, v0

    iput v1, p0, LX/2SG;->f:I

    .line 412002
    sub-int/2addr p3, v0

    .line 412003
    :cond_0
    if-lez p3, :cond_1

    .line 412004
    invoke-direct {p0}, LX/2SG;->h()V

    goto :goto_0

    .line 412005
    :cond_1
    return-void
.end method
