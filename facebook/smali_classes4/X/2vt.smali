.class public LX/2vt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2vu;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2wX;Landroid/app/PendingIntent;)LX/2wg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Landroid/app/PendingIntent;",
            ")",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    new-instance v0, LX/3KS;

    invoke-direct {v0, p0, p1, p2}, LX/3KS;-><init>(LX/2vt;LX/2wX;Landroid/app/PendingIntent;)V

    invoke-virtual {p1, v0}, LX/2wX;->b(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2wX;Lcom/google/android/gms/location/LocationRequest;LX/2vi;)LX/2wg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Lcom/google/android/gms/location/LocationRequest;",
            "LX/2vi;",
            ")",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    new-instance v0, LX/2xK;

    invoke-direct {v0, p0, p1, p2, p3}, LX/2xK;-><init>(LX/2vt;LX/2wX;Lcom/google/android/gms/location/LocationRequest;LX/2vi;)V

    invoke-virtual {p1, v0}, LX/2wX;->b(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2wX;Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)LX/2wg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Lcom/google/android/gms/location/LocationRequest;",
            "Landroid/app/PendingIntent;",
            ")",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    new-instance v0, LX/7a3;

    invoke-direct {v0, p0, p1, p2, p3}, LX/7a3;-><init>(LX/2vt;LX/2wX;Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V

    invoke-virtual {p1, v0}, LX/2wX;->b(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2wX;)Landroid/location/Location;
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    const-string p0, "GoogleApiClient parameter is required."

    invoke-static {v0, p0}, LX/1ol;->b(ZLjava/lang/Object;)V

    sget-object v0, LX/2vm;->e:LX/2vn;

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2vo;)LX/2wJ;

    move-result-object v0

    check-cast v0, LX/2wF;

    if-eqz v0, :cond_1

    :goto_1
    const-string v2, "GoogleApiClient is not configured to use the LocationServices.API Api. Pass thisinto GoogleApiClient.Builder#addApi() to use this feature."

    invoke-static {v1, v2}, LX/1ol;->a(ZLjava/lang/Object;)V

    move-object v0, v0

    :try_start_0
    iget-object v1, v0, LX/2wF;->e:LX/2wV;

    invoke-virtual {v1}, LX/2wV;->a()Landroid/location/Location;

    move-result-object v1

    move-object v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_2

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
