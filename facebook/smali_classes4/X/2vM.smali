.class public LX/2vM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 478016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478017
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;)LX/0Rf;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 478018
    invoke-static {p0}, LX/2vM;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Set;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 478019
    const-string v0, "FeedUnitTagHelper.findTags"

    const v1, -0x6b6167c8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 478020
    if-eqz p0, :cond_0

    .line 478021
    :try_start_0
    new-instance v0, LX/31Q;

    invoke-direct {v0}, LX/31Q;-><init>()V

    .line 478022
    invoke-virtual {v0, p0}, LX/1jx;->b(LX/0jT;)LX/0jT;

    .line 478023
    iget-object v0, v0, LX/31Q;->a:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478024
    const v1, 0x7284a744

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 478025
    :cond_0
    :try_start_1
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478026
    const v1, -0x28b3a3b1

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x29475ec3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
