.class public LX/22l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/22l;


# instance fields
.field private final a:LX/03V;

.field private final b:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pn;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qe;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0SG;

.field private final f:LX/0pk;

.field private final g:LX/0pk;


# direct methods
.method public constructor <init>(LX/03V;LX/0pi;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;LX/0Ot;LX/0SG;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0pi;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0Ot",
            "<",
            "LX/0pn;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1qe;",
            ">;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 361253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361254
    iput-object p1, p0, LX/22l;->a:LX/03V;

    .line 361255
    iput-object p3, p0, LX/22l;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 361256
    iput-object p4, p0, LX/22l;->c:LX/0Ot;

    .line 361257
    iput-object p5, p0, LX/22l;->d:LX/0Ot;

    .line 361258
    iput-object p6, p0, LX/22l;->e:LX/0SG;

    .line 361259
    const-string v0, "feed_db_request"

    invoke-virtual {p2, v0}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v0

    iput-object v0, p0, LX/22l;->f:LX/0pk;

    .line 361260
    const-string v0, "feed_db_entries"

    invoke-virtual {p2, v0}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v0

    iput-object v0, p0, LX/22l;->g:LX/0pk;

    .line 361261
    return-void
.end method

.method public static a(LX/0QB;)LX/22l;
    .locals 10

    .prologue
    .line 361160
    sget-object v0, LX/22l;->h:LX/22l;

    if-nez v0, :cond_1

    .line 361161
    const-class v1, LX/22l;

    monitor-enter v1

    .line 361162
    :try_start_0
    sget-object v0, LX/22l;->h:LX/22l;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 361163
    if-eqz v2, :cond_0

    .line 361164
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 361165
    new-instance v3, LX/22l;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v5

    check-cast v5, LX/0pi;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v6

    check-cast v6, Lcom/facebook/performancelogger/PerformanceLogger;

    const/16 v7, 0xe8

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xed

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-direct/range {v3 .. v9}, LX/22l;-><init>(LX/03V;LX/0pi;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;LX/0Ot;LX/0SG;)V

    .line 361166
    move-object v0, v3

    .line 361167
    sput-object v0, LX/22l;->h:LX/22l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361168
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 361169
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 361170
    :cond_1
    sget-object v0, LX/22l;->h:LX/22l;

    return-object v0

    .line 361171
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 361172
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 6

    .prologue
    .line 361236
    const-string v0, "FeedFetchCoordinator.readFromDbCache"

    const v1, -0x30900927

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 361237
    :try_start_0
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 361238
    if-nez v0, :cond_0

    .line 361239
    iget-object v0, p0, LX/22l;->a:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LastNewerStoriesRequestFromDb_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 361240
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v2, v2

    .line 361241
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " time="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/22l;->e:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 361242
    :goto_0
    iget-object v0, p0, LX/22l;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pn;

    invoke-virtual {v0, p1}, LX/0pn;->a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    .line 361243
    if-eqz v0, :cond_1

    sget-object v1, LX/0ta;->NO_DATA:LX/0ta;

    .line 361244
    iget-object v2, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 361245
    if-eq v1, v2, :cond_1

    .line 361246
    iget-object v1, p0, LX/22l;->f:LX/0pk;

    invoke-virtual {v1}, LX/0pk;->b()V

    .line 361247
    iget-object v1, p0, LX/22l;->g:LX/0pk;

    invoke-virtual {v0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, LX/0pk;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361248
    const v1, 0x5f1ebf6f

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_1
    return-object v0

    .line 361249
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/22l;->a:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LastOlderStoriesRequestFromDb_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 361250
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v2, v2

    .line 361251
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " time="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/22l;->e:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 361252
    :catchall_0
    move-exception v0

    const v1, -0x62454437

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    const v0, -0x6c2cf3a5

    invoke-static {v0}, LX/02m;->a(I)V

    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedParams;LX/1qH;LX/232;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 361173
    const-string v0, "FeedFetchCoordinator.handleFetchNewsFeed"

    const v2, -0x5917efc2

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 361174
    :try_start_0
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v0

    .line 361175
    invoke-virtual {v0}, Lcom/facebook/api/feedtype/FeedType;->e()LX/0pL;

    move-result-object v0

    .line 361176
    sget-object v2, LX/0pL;->NO_CACHE:LX/0pL;

    if-eq v0, v2, :cond_0

    sget-object v2, LX/0pL;->MEMORY_ONLY_CACHE:LX/0pL;

    if-ne v0, v2, :cond_1

    .line 361177
    :cond_0
    invoke-interface {p3, p1, p2}, LX/232;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/1qH;)Lcom/facebook/api/feed/FetchFeedResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 361178
    const v1, 0x48893979

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 361179
    :cond_1
    :try_start_1
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 361180
    if-eqz v0, :cond_5

    .line 361181
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 361182
    const-string v2, "cold_start_cursor"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 361183
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 361184
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 361185
    invoke-virtual {v0, v2}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 361186
    sget-object v0, LX/00a;->a:LX/00a;

    move-object v0, v0

    .line 361187
    invoke-virtual {v0}, LX/00a;->i()Ljava/lang/String;

    move-result-object v0

    .line 361188
    :goto_1
    if-nez v0, :cond_2

    .line 361189
    iget-object v0, p0, LX/22l;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pn;

    .line 361190
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 361191
    invoke-virtual {v0, v2}, LX/0pn;->c(Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v0

    .line 361192
    :cond_2
    new-instance v2, LX/0rT;

    invoke-direct {v2}, LX/0rT;-><init>()V

    invoke-virtual {v2, p1}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v2

    .line 361193
    iput-object v0, v2, LX/0rT;->g:Ljava/lang/String;

    .line 361194
    move-object v0, v2

    .line 361195
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object p1

    .line 361196
    :cond_3
    :goto_2
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v0

    .line 361197
    sget-object v2, LX/0gf;->SKIP_TAIL_GAP:LX/0gf;

    if-eq v0, v2, :cond_d

    .line 361198
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 361199
    :cond_4
    iget-boolean v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->l:Z

    move v0, v0

    .line 361200
    if-nez v0, :cond_a

    .line 361201
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v0, v0

    .line 361202
    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne v0, v1, :cond_9

    .line 361203
    invoke-static {p1}, Lcom/facebook/api/feed/FetchFeedResult;->a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 361204
    const v1, 0x6ffb68b5

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 361205
    :cond_5
    :try_start_2
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v0, v0

    .line 361206
    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-eq v0, v2, :cond_3

    .line 361207
    iget-object v0, p0, LX/22l;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 361208
    iget-object v0, p0, LX/22l;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0052

    const-string v2, "NNFCold_DbCache"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 361209
    :cond_6
    :try_start_3
    invoke-direct {p0, p1}, LX/22l;->a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v1

    .line 361210
    :try_start_4
    iget-object v0, p0, LX/22l;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0001

    const-string v3, "NNFColdStart"

    invoke-interface {v0, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 361211
    if-eqz v1, :cond_7

    .line 361212
    iget-object v0, p0, LX/22l;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0052

    const-string v3, "NNFCold_DbCache"

    invoke-interface {v0, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 361213
    :catchall_0
    move-exception v0

    const v1, -0x427f23a1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 361214
    :cond_7
    :try_start_5
    iget-object v0, p0, LX/22l;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0052

    const-string v3, "NNFCold_DbCache"

    invoke-interface {v0, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    goto :goto_2

    .line 361215
    :catchall_1
    move-exception v0

    iget-object v1, p0, LX/22l;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0001

    const-string v3, "NNFColdStart"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 361216
    iget-object v1, p0, LX/22l;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xa0052

    const-string v3, "NNFCold_DbCache"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    :cond_8
    throw v0

    .line 361217
    :cond_9
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v0, v0

    .line 361218
    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-ne v0, v1, :cond_a

    .line 361219
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 361220
    if-nez v0, :cond_a

    .line 361221
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 361222
    if-nez v0, :cond_a

    .line 361223
    invoke-static {p1}, Lcom/facebook/api/feed/FetchFeedResult;->a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 361224
    const v1, 0x6a3b71a2

    invoke-static {v1}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 361225
    :cond_a
    :try_start_6
    invoke-interface {p3, p1, p2}, LX/232;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/1qH;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v1

    .line 361226
    if-eqz v1, :cond_d

    .line 361227
    iget-object v0, p0, LX/22l;->f:LX/0pk;

    invoke-virtual {v0}, LX/0pk;->c()V

    .line 361228
    iget-object v0, p0, LX/22l;->g:LX/0pk;

    invoke-virtual {v1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, LX/0pk;->b(J)V

    .line 361229
    invoke-virtual {v1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 361230
    iget-object v0, p0, LX/22l;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v1}, LX/0pn;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 361231
    iget-object v0, p0, LX/22l;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qe;

    invoke-virtual {v0, v1}, LX/1qe;->a(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-object v0, v1

    .line 361232
    :goto_3
    const v1, -0x5cc5888e

    invoke-static {v1}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 361233
    :cond_b
    :try_start_7
    iget-object v0, p0, LX/22l;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pn;

    invoke-virtual {v0, v1}, LX/0pn;->b(Lcom/facebook/api/feed/FetchFeedResult;)V

    move-object v0, v1

    goto :goto_3

    .line 361234
    :cond_c
    if-eqz v1, :cond_d

    .line 361235
    iget-object v0, p0, LX/22l;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pn;

    invoke-virtual {v0, v1}, LX/0pn;->b(Lcom/facebook/api/feed/FetchFeedResult;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_d
    move-object v0, v1

    goto :goto_3

    :cond_e
    move-object v0, v1

    goto/16 :goto_1
.end method
