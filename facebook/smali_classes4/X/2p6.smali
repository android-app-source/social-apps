.class public final enum LX/2p6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2p6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2p6;

.field public static final enum CENTER:LX/2p6;

.field public static final enum END:LX/2p6;

.field public static final enum START:LX/2p6;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 467631
    new-instance v0, LX/2p6;

    const-string v1, "START"

    invoke-direct {v0, v1, v2}, LX/2p6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2p6;->START:LX/2p6;

    .line 467632
    new-instance v0, LX/2p6;

    const-string v1, "END"

    invoke-direct {v0, v1, v3}, LX/2p6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2p6;->END:LX/2p6;

    .line 467633
    new-instance v0, LX/2p6;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v4}, LX/2p6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2p6;->CENTER:LX/2p6;

    .line 467634
    const/4 v0, 0x3

    new-array v0, v0, [LX/2p6;

    sget-object v1, LX/2p6;->START:LX/2p6;

    aput-object v1, v0, v2

    sget-object v1, LX/2p6;->END:LX/2p6;

    aput-object v1, v0, v3

    sget-object v1, LX/2p6;->CENTER:LX/2p6;

    aput-object v1, v0, v4

    sput-object v0, LX/2p6;->$VALUES:[LX/2p6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 467629
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2p6;
    .locals 1

    .prologue
    .line 467630
    const-class v0, LX/2p6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2p6;

    return-object v0
.end method

.method public static values()[LX/2p6;
    .locals 1

    .prologue
    .line 467628
    sget-object v0, LX/2p6;->$VALUES:[LX/2p6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2p6;

    return-object v0
.end method
