.class public final LX/2kY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1vq",
        "<TTEdge;TTUserInfo;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1vq",
            "<TTEdge;TTUserInfo;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 456336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456337
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/2kY;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 2
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<TTEdge;>;",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 456331
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 456332
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be run on UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456333
    :cond_0
    iget-object v0, p0, LX/2kY;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vq;

    .line 456334
    invoke-interface {v0, p1, p2, p3, p4}, LX/1vq;->a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V

    goto :goto_0

    .line 456335
    :cond_1
    return-void
.end method

.method public final a(LX/1vq;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vq",
            "<TTEdge;TTUserInfo;>;)V"
        }
    .end annotation

    .prologue
    .line 456327
    if-nez p1, :cond_0

    .line 456328
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 456329
    :cond_0
    iget-object v0, p0, LX/2kY;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 456330
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 2
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 456338
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 456339
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be run on UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456340
    :cond_0
    iget-object v0, p0, LX/2kY;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vq;

    .line 456341
    invoke-interface {v0, p1}, LX/1vq;->a(LX/2kM;)V

    goto :goto_0

    .line 456342
    :cond_1
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 2
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;)V"
        }
    .end annotation

    .prologue
    .line 456322
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 456323
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be run on UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456324
    :cond_0
    iget-object v0, p0, LX/2kY;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vq;

    .line 456325
    invoke-interface {v0, p1, p2, p3}, LX/1vq;->a(LX/2nj;LX/3DP;Ljava/lang/Object;)V

    goto :goto_0

    .line 456326
    :cond_1
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 2
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 456317
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 456318
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be run on UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456319
    :cond_0
    iget-object v0, p0, LX/2kY;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vq;

    .line 456320
    invoke-interface {v0, p1, p2, p3, p4}, LX/1vq;->a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 456321
    :cond_1
    return-void
.end method

.method public final b(LX/1vq;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vq",
            "<TTEdge;TTUserInfo;>;)V"
        }
    .end annotation

    .prologue
    .line 456308
    if-nez p1, :cond_0

    .line 456309
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 456310
    :cond_0
    iget-object v0, p0, LX/2kY;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 456311
    return-void
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 2
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;)V"
        }
    .end annotation

    .prologue
    .line 456312
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 456313
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be run on UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456314
    :cond_0
    iget-object v0, p0, LX/2kY;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vq;

    .line 456315
    invoke-interface {v0, p1, p2, p3}, LX/1vq;->b(LX/2nj;LX/3DP;Ljava/lang/Object;)V

    goto :goto_0

    .line 456316
    :cond_1
    return-void
.end method
