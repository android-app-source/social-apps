.class public LX/2SY;
.super LX/2SP;
.source ""

# interfaces
.implements LX/2SZ;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile r:LX/2SY;


# instance fields
.field private final a:LX/2Sa;

.field private final b:LX/2Se;

.field private final c:LX/2Sj;

.field private final d:LX/0ad;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Uh;

.field public final g:LX/2Sc;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EQ0;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/concurrent/ExecutorService;

.field private final k:LX/0SG;

.field public l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cw5;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private m:LX/Cw5;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private n:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public o:Z

.field private p:LX/2SR;

.field public q:I


# direct methods
.method public constructor <init>(LX/2Sa;LX/2Se;LX/2Sg;LX/2Sj;LX/0ad;LX/0Or;LX/0Uh;LX/2Sc;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0SG;)V
    .locals 1
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/search/abtest/gk/TrendingNewsEnabled;
        .end annotation
    .end param
    .param p9    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p11    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Sa;",
            "LX/2Se;",
            "LX/2Sg;",
            "LX/2Sj;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/2Sc;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EQ0;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 412609
    invoke-direct {p0, p3}, LX/2SP;-><init>(LX/2Sg;)V

    .line 412610
    const/4 v0, 0x0

    iput v0, p0, LX/2SY;->q:I

    .line 412611
    iput-object p1, p0, LX/2SY;->a:LX/2Sa;

    .line 412612
    iput-object p2, p0, LX/2SY;->b:LX/2Se;

    .line 412613
    iput-object p4, p0, LX/2SY;->c:LX/2Sj;

    .line 412614
    iput-object p5, p0, LX/2SY;->d:LX/0ad;

    .line 412615
    iput-object p6, p0, LX/2SY;->e:LX/0Or;

    .line 412616
    iput-object p7, p0, LX/2SY;->f:LX/0Uh;

    .line 412617
    iput-object p8, p0, LX/2SY;->g:LX/2Sc;

    .line 412618
    iput-object p9, p0, LX/2SY;->h:LX/0Ot;

    .line 412619
    iput-object p10, p0, LX/2SY;->i:LX/0Ot;

    .line 412620
    iput-object p11, p0, LX/2SY;->j:Ljava/util/concurrent/ExecutorService;

    .line 412621
    iput-object p12, p0, LX/2SY;->k:LX/0SG;

    .line 412622
    invoke-virtual {p0}, LX/2SY;->k()Z

    move-result v0

    iput-boolean v0, p0, LX/2SY;->o:Z

    .line 412623
    return-void
.end method

.method public static a(LX/0QB;)LX/2SY;
    .locals 3

    .prologue
    .line 412624
    sget-object v0, LX/2SY;->r:LX/2SY;

    if-nez v0, :cond_1

    .line 412625
    const-class v1, LX/2SY;

    monitor-enter v1

    .line 412626
    :try_start_0
    sget-object v0, LX/2SY;->r:LX/2SY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 412627
    if-eqz v2, :cond_0

    .line 412628
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2SY;->b(LX/0QB;)LX/2SY;

    move-result-object v0

    sput-object v0, LX/2SY;->r:LX/2SY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412629
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 412630
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 412631
    :cond_1
    sget-object v0, LX/2SY;->r:LX/2SY;

    return-object v0

    .line 412632
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 412633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/2SY;
    .locals 13

    .prologue
    .line 412634
    new-instance v0, LX/2SY;

    invoke-static {p0}, LX/2Sa;->a(LX/0QB;)LX/2Sa;

    move-result-object v1

    check-cast v1, LX/2Sa;

    invoke-static {p0}, LX/2Se;->a(LX/0QB;)LX/2Se;

    move-result-object v2

    check-cast v2, LX/2Se;

    invoke-static {p0}, LX/2Sg;->a(LX/0QB;)LX/2Sg;

    move-result-object v3

    check-cast v3, LX/2Sg;

    invoke-static {p0}, LX/2Sj;->a(LX/0QB;)LX/2Sj;

    move-result-object v4

    check-cast v4, LX/2Sj;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    const/16 v6, 0x156c

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {p0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v8

    check-cast v8, LX/2Sc;

    const/16 v9, 0x1646

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x34d3

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    invoke-direct/range {v0 .. v12}, LX/2SY;-><init>(LX/2Sa;LX/2Se;LX/2Sg;LX/2Sj;LX/0ad;LX/0Or;LX/0Uh;LX/2Sc;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0SG;)V

    .line 412635
    return-object v0
.end method

.method public static declared-synchronized b(LX/2SY;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 412636
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/Cw5;

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/2SY;->k:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, LX/Cw5;-><init>(LX/0Px;J)V

    iput-object v0, p0, LX/2SY;->m:LX/Cw5;

    .line 412637
    iget-object v0, p0, LX/2SY;->p:LX/2SR;

    if-eqz v0, :cond_0

    .line 412638
    iget-object v0, p0, LX/2SY;->p:LX/2SR;

    sget-object v1, LX/7BE;->READY:LX/7BE;

    invoke-interface {v0, v1}, LX/2SR;->a(LX/7BE;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412639
    :cond_0
    monitor-exit p0

    return-void

    .line 412640
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/2SY;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 412641
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2SY;->b:LX/2Se;

    const-string v1, "recent_search_cache_tag"

    iget-object v2, p0, LX/2SY;->m:LX/Cw5;

    invoke-virtual {v0, v1, p1, p2, v2}, LX/2Se;->a(Ljava/lang/String;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;Ljava/lang/String;LX/Cw5;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/2SY;->b(LX/2SY;LX/0Px;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412642
    monitor-exit p0

    return-void

    .line 412643
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V
    .locals 1

    .prologue
    .line 412644
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, p1, v0}, LX/2SY;->b(LX/2SY;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412645
    monitor-exit p0

    return-void

    .line 412646
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized m(LX/2SY;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 412647
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/2SY;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2SY;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 412648
    iget-object v1, p0, LX/2SY;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 412649
    const/4 v1, 0x0

    iput-object v1, p0, LX/2SY;->l:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412650
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 412651
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static n(LX/2SY;)V
    .locals 2

    .prologue
    .line 412601
    iget-object v0, p0, LX/2SY;->n:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 412602
    iget-object v0, p0, LX/2SY;->n:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 412603
    const/4 v0, 0x0

    iput-object v0, p0, LX/2SY;->n:Ljava/util/concurrent/ScheduledFuture;

    .line 412604
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/7BE;
    .locals 8

    .prologue
    .line 412652
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2SY;->m:LX/Cw5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2SY;->c:LX/2Sj;

    const-wide/16 v2, 0xe10

    iget-object v1, p0, LX/2SY;->m:LX/Cw5;

    .line 412653
    iget-wide v6, v1, LX/Cw5;->b:J

    move-wide v4, v6

    .line 412654
    invoke-virtual {v0, v2, v3, v4, v5}, LX/2Sj;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412655
    sget-object v0, LX/7BE;->READY:LX/7BE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412656
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, LX/7BE;->NOT_READY:LX/7BE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 412657
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/common/callercontext/CallerContext;LX/0zS;J)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0zS;",
            "J)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cw5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 412658
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2SY;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 412659
    iget-object v0, p0, LX/2SY;->l:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412660
    :goto_0
    monitor-exit p0

    return-object v0

    .line 412661
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2SY;->f:LX/0Uh;

    sget v1, LX/2SU;->e:I

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2SY;->f:LX/0Uh;

    sget v1, LX/2SU;->y:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 412662
    :cond_1
    const-string v2, "entity_only"

    .line 412663
    :goto_1
    invoke-virtual {p0}, LX/2SP;->kF_()V

    .line 412664
    iget-object v0, p0, LX/2SY;->a:LX/2Sa;

    const-string v1, "recent_search_cache_tag"

    const/16 v3, 0xf

    move-object v4, p1

    move-object v5, p2

    move-wide v6, p3

    invoke-virtual/range {v0 .. v7}, LX/2Sa;->a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;LX/0zS;J)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/2SY;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 412665
    iget-object v0, p0, LX/2SY;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/search/suggestions/nullstate/recent/RecentSearchesNullStateSupplier$1;

    invoke-direct {v1, p0}, Lcom/facebook/search/suggestions/nullstate/recent/RecentSearchesNullStateSupplier$1;-><init>(LX/2SY;)V

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2SY;->n:Ljava/util/concurrent/ScheduledFuture;

    .line 412666
    new-instance v0, LX/EQA;

    invoke-direct {v0, p0}, LX/EQA;-><init>(LX/2SY;)V

    .line 412667
    iget-object v1, p0, LX/2SY;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, LX/2SY;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 412668
    iget-object v0, p0, LX/2SY;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 412669
    :cond_2
    iget-object v0, p0, LX/2SY;->d:LX/0ad;

    sget-short v1, LX/100;->bO:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 412670
    const-string v2, "keyword_only"

    goto :goto_1

    .line 412671
    :cond_3
    const-string v2, "all"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 412672
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/2SR;LX/2Sp;)V
    .locals 0

    .prologue
    .line 412673
    iput-object p1, p0, LX/2SY;->p:LX/2SR;

    .line 412674
    return-void
.end method

.method public final a(LX/Cwb;)V
    .locals 1

    .prologue
    .line 412675
    sget-object v0, LX/Cwb;->RECENT:LX/Cwb;

    if-ne p1, v0, :cond_0

    .line 412676
    const/4 v0, 0x1

    .line 412677
    iput-boolean v0, p0, LX/2SY;->o:Z

    .line 412678
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 4
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 412605
    invoke-static {p2}, LX/2SP;->a(LX/EPu;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/0zS;->d:LX/0zS;

    :goto_0
    const-wide/32 v2, 0x3f480

    invoke-virtual {p0, p1, v0, v2, v3}, LX/2SY;->a(Lcom/facebook/common/callercontext/CallerContext;LX/0zS;J)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 412606
    return-void

    .line 412607
    :cond_0
    sget-object v0, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 0

    .prologue
    .line 412608
    return-void
.end method

.method public final a(Lcom/facebook/search/model/EntityTypeaheadUnit;)V
    .locals 1

    .prologue
    .line 412575
    invoke-static {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2SY;->b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    .line 412576
    return-void
.end method

.method public final a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V
    .locals 1

    .prologue
    .line 412565
    invoke-static {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2SY;->b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    .line 412566
    return-void
.end method

.method public final a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V
    .locals 1

    .prologue
    .line 412567
    invoke-static {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2SY;->b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    .line 412568
    return-void
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)V
    .locals 1

    .prologue
    .line 412569
    iget-object v0, p1, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-object v0, v0

    .line 412570
    invoke-static {v0}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    .line 412571
    invoke-direct {p0, v0}, LX/2SY;->b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    .line 412572
    return-void
.end method

.method public final a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)V
    .locals 1

    .prologue
    .line 412573
    invoke-static {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2SY;->b(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    .line 412574
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 412577
    const/4 v0, 0x1

    return v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 412578
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/2SY;->m:LX/Cw5;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412579
    monitor-exit p0

    return-void

    .line 412580
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 412581
    invoke-virtual {p0}, LX/2SY;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412582
    :goto_0
    return-void

    .line 412583
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2SY;->o:Z

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 412584
    const-string v0, "recent_searches_network"

    return-object v0
.end method

.method public final f()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 412585
    invoke-virtual {p0}, LX/2SP;->c()V

    .line 412586
    iget-object v0, p0, LX/2SY;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EQ0;

    const-string v1, "recent_search_cache_tag"

    invoke-virtual {v0, v1}, LX/EQ0;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 412587
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2SY;->m:LX/Cw5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2SY;->m:LX/Cw5;

    invoke-virtual {v0}, LX/Cw5;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 412588
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, LX/2SY;->q:I

    .line 412589
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412590
    :goto_0
    monitor-exit p0

    return-object v0

    .line 412591
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2SY;->m:LX/Cw5;

    .line 412592
    iget-object v1, v0, LX/Cw5;->a:LX/0Px;

    move-object v0, v1

    .line 412593
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    .line 412594
    iget-object v0, p0, LX/2SY;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0xf

    .line 412595
    :goto_1
    iget-boolean v2, p0, LX/2SY;->o:Z

    if-eqz v2, :cond_3

    move v0, v1

    :goto_2
    iput v0, p0, LX/2SY;->q:I

    .line 412596
    iget-object v0, p0, LX/2SY;->b:LX/2Se;

    sget-object v1, LX/Cwb;->RECENT:LX/Cwb;

    iget-object v2, p0, LX/2SY;->m:LX/Cw5;

    iget-boolean v3, p0, LX/2SY;->o:Z

    iget v4, p0, LX/2SY;->q:I

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2Se;->a(LX/Cwb;LX/Cw5;ZI)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 412597
    :cond_2
    const/4 v0, 0x3

    goto :goto_1

    .line 412598
    :cond_3
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_2

    .line 412599
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final k()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 412600
    iget-object v1, p0, LX/2SY;->d:LX/0ad;

    sget-short v2, LX/100;->l:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2SY;->f:LX/0Uh;

    sget v2, LX/2SU;->e:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method
