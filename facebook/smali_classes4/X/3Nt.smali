.class public final enum LX/3Nt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Nt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Nt;

.field public static final enum CAMERA_ROLL_ROW:LX/3Nt;

.field public static final enum CONTACT_ROW:LX/3Nt;

.field public static final enum FAVORITES_SECTION_HEADER:LX/3Nt;

.field public static final enum FRIENDS_WITH_NEW_POSTS:LX/3Nt;

.field public static final enum GROUP_ROW:LX/3Nt;

.field public static final enum INVITE_FRIENDS:LX/3Nt;

.field public static final enum LOADING_MORE:LX/3Nt;

.field public static final enum MESSAGE_SEARCH_RESULT_ROW:LX/3Nt;

.field public static final enum MESSAGE_SEARCH_ROW:LX/3Nt;

.field public static final enum MONTAGE_AUDIENCE_ROW:LX/3Nt;

.field public static final enum MONTAGE_ROW:LX/3Nt;

.field public static final enum NEARBY_FRIENDS:LX/3Nt;

.field public static final enum NEARBY_FRIENDS_MORE_ROW:LX/3Nt;

.field public static final enum PAYMENT_ELIGIBLE_FOOTER:LX/3Nt;

.field public static final enum PAYMENT_ROW:LX/3Nt;

.field public static final enum PHONE_CONTACT_ROW:LX/3Nt;

.field public static final enum PLATFORM_SEARCH_ROW:LX/3Nt;

.field public static final enum RTC_PROMOTION_ROW:LX/3Nt;

.field public static final enum SECTION_HEADER:LX/3Nt;

.field public static final enum SECTION_SPLITTER:LX/3Nt;

.field public static final enum SMS_BRIDGE_PROMOTION_ROW:LX/3Nt;

.field public static final enum TINCAN_ROW:LX/3Nt;

.field public static final enum VIEW_MORE_INLINE:LX/3Nt;

.field public static final enum VOICEMAIL_ROW:LX/3Nt;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 560032
    new-instance v0, LX/3Nt;

    const-string v1, "CONTACT_ROW"

    invoke-direct {v0, v1, v3}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->CONTACT_ROW:LX/3Nt;

    .line 560033
    new-instance v0, LX/3Nt;

    const-string v1, "SECTION_HEADER"

    invoke-direct {v0, v1, v4}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->SECTION_HEADER:LX/3Nt;

    .line 560034
    new-instance v0, LX/3Nt;

    const-string v1, "SECTION_SPLITTER"

    invoke-direct {v0, v1, v5}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->SECTION_SPLITTER:LX/3Nt;

    .line 560035
    new-instance v0, LX/3Nt;

    const-string v1, "FAVORITES_SECTION_HEADER"

    invoke-direct {v0, v1, v6}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->FAVORITES_SECTION_HEADER:LX/3Nt;

    .line 560036
    new-instance v0, LX/3Nt;

    const-string v1, "INVITE_FRIENDS"

    invoke-direct {v0, v1, v7}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->INVITE_FRIENDS:LX/3Nt;

    .line 560037
    new-instance v0, LX/3Nt;

    const-string v1, "GROUP_ROW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->GROUP_ROW:LX/3Nt;

    .line 560038
    new-instance v0, LX/3Nt;

    const-string v1, "NEARBY_FRIENDS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->NEARBY_FRIENDS:LX/3Nt;

    .line 560039
    new-instance v0, LX/3Nt;

    const-string v1, "NEARBY_FRIENDS_MORE_ROW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->NEARBY_FRIENDS_MORE_ROW:LX/3Nt;

    .line 560040
    new-instance v0, LX/3Nt;

    const-string v1, "FRIENDS_WITH_NEW_POSTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->FRIENDS_WITH_NEW_POSTS:LX/3Nt;

    .line 560041
    new-instance v0, LX/3Nt;

    const-string v1, "VIEW_MORE_INLINE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->VIEW_MORE_INLINE:LX/3Nt;

    .line 560042
    new-instance v0, LX/3Nt;

    const-string v1, "LOADING_MORE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->LOADING_MORE:LX/3Nt;

    .line 560043
    new-instance v0, LX/3Nt;

    const-string v1, "PHONE_CONTACT_ROW"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->PHONE_CONTACT_ROW:LX/3Nt;

    .line 560044
    new-instance v0, LX/3Nt;

    const-string v1, "PAYMENT_ROW"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->PAYMENT_ROW:LX/3Nt;

    .line 560045
    new-instance v0, LX/3Nt;

    const-string v1, "PAYMENT_ELIGIBLE_FOOTER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->PAYMENT_ELIGIBLE_FOOTER:LX/3Nt;

    .line 560046
    new-instance v0, LX/3Nt;

    const-string v1, "MONTAGE_ROW"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->MONTAGE_ROW:LX/3Nt;

    .line 560047
    new-instance v0, LX/3Nt;

    const-string v1, "CAMERA_ROLL_ROW"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->CAMERA_ROLL_ROW:LX/3Nt;

    .line 560048
    new-instance v0, LX/3Nt;

    const-string v1, "MONTAGE_AUDIENCE_ROW"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->MONTAGE_AUDIENCE_ROW:LX/3Nt;

    .line 560049
    new-instance v0, LX/3Nt;

    const-string v1, "TINCAN_ROW"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->TINCAN_ROW:LX/3Nt;

    .line 560050
    new-instance v0, LX/3Nt;

    const-string v1, "RTC_PROMOTION_ROW"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->RTC_PROMOTION_ROW:LX/3Nt;

    .line 560051
    new-instance v0, LX/3Nt;

    const-string v1, "VOICEMAIL_ROW"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->VOICEMAIL_ROW:LX/3Nt;

    .line 560052
    new-instance v0, LX/3Nt;

    const-string v1, "MESSAGE_SEARCH_ROW"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->MESSAGE_SEARCH_ROW:LX/3Nt;

    .line 560053
    new-instance v0, LX/3Nt;

    const-string v1, "MESSAGE_SEARCH_RESULT_ROW"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->MESSAGE_SEARCH_RESULT_ROW:LX/3Nt;

    .line 560054
    new-instance v0, LX/3Nt;

    const-string v1, "SMS_BRIDGE_PROMOTION_ROW"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->SMS_BRIDGE_PROMOTION_ROW:LX/3Nt;

    .line 560055
    new-instance v0, LX/3Nt;

    const-string v1, "PLATFORM_SEARCH_ROW"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/3Nt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Nt;->PLATFORM_SEARCH_ROW:LX/3Nt;

    .line 560056
    const/16 v0, 0x18

    new-array v0, v0, [LX/3Nt;

    sget-object v1, LX/3Nt;->CONTACT_ROW:LX/3Nt;

    aput-object v1, v0, v3

    sget-object v1, LX/3Nt;->SECTION_HEADER:LX/3Nt;

    aput-object v1, v0, v4

    sget-object v1, LX/3Nt;->SECTION_SPLITTER:LX/3Nt;

    aput-object v1, v0, v5

    sget-object v1, LX/3Nt;->FAVORITES_SECTION_HEADER:LX/3Nt;

    aput-object v1, v0, v6

    sget-object v1, LX/3Nt;->INVITE_FRIENDS:LX/3Nt;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3Nt;->GROUP_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3Nt;->NEARBY_FRIENDS:LX/3Nt;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3Nt;->NEARBY_FRIENDS_MORE_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3Nt;->FRIENDS_WITH_NEW_POSTS:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/3Nt;->VIEW_MORE_INLINE:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/3Nt;->LOADING_MORE:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/3Nt;->PHONE_CONTACT_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/3Nt;->PAYMENT_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/3Nt;->PAYMENT_ELIGIBLE_FOOTER:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/3Nt;->MONTAGE_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/3Nt;->CAMERA_ROLL_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/3Nt;->MONTAGE_AUDIENCE_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/3Nt;->TINCAN_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/3Nt;->RTC_PROMOTION_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/3Nt;->VOICEMAIL_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/3Nt;->MESSAGE_SEARCH_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/3Nt;->MESSAGE_SEARCH_RESULT_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/3Nt;->SMS_BRIDGE_PROMOTION_ROW:LX/3Nt;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/3Nt;->PLATFORM_SEARCH_ROW:LX/3Nt;

    aput-object v2, v0, v1

    sput-object v0, LX/3Nt;->$VALUES:[LX/3Nt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560057
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Nt;
    .locals 1

    .prologue
    .line 560031
    const-class v0, LX/3Nt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Nt;

    return-object v0
.end method

.method public static values()[LX/3Nt;
    .locals 1

    .prologue
    .line 560030
    sget-object v0, LX/3Nt;->$VALUES:[LX/3Nt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Nt;

    return-object v0
.end method
