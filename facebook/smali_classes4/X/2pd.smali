.class public final LX/2pd;
.super LX/2pe;
.source ""


# instance fields
.field public final synthetic a:LX/2pb;


# direct methods
.method public constructor <init>(LX/2pb;)V
    .locals 0

    .prologue
    .line 468810
    iput-object p1, p0, LX/2pd;->a:LX/2pb;

    invoke-direct {p0}, LX/2pe;-><init>()V

    .line 468811
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 468803
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->e:LX/2pg;

    .line 468804
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/2pg;->c:Z

    .line 468805
    invoke-static {v0}, LX/2pg;->e(LX/2pg;)V

    .line 468806
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->r()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468807
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/2ok;

    sget-object v2, LX/3np;->API_CONFIG:LX/3np;

    iget-object v2, v2, LX/3np;->value:Ljava/lang/String;

    iget-object v3, p0, LX/2pd;->a:LX/2pb;

    iget-object v3, v3, LX/2pb;->E:LX/2q7;

    invoke-interface {v3}, LX/2q7;->r()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468808
    :cond_0
    invoke-super {p0}, LX/2pe;->a()V

    .line 468809
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 468794
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-boolean v0, v0, LX/2pb;->M:Z

    if-eqz v0, :cond_0

    .line 468795
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    invoke-static {v0, v1}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    .line 468796
    :goto_0
    return-void

    .line 468797
    :cond_0
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_1

    .line 468798
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    invoke-static {v0, v1}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    .line 468799
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/2op;

    invoke-direct {v1, p1}, LX/2op;-><init>(I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468800
    :cond_1
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->e:LX/2pg;

    invoke-virtual {v0}, LX/2pg;->a()V

    .line 468801
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->P:LX/2pq;

    invoke-virtual {v0}, LX/2pq;->a()V

    .line 468802
    invoke-super {p0, p1}, LX/2pe;->a(I)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 468715
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    .line 468716
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/2pE;

    invoke-direct {v1, p1, p2}, LX/2pE;-><init>(II)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468717
    :cond_0
    invoke-super {p0, p1, p2}, LX/2pe;->a(II)V

    .line 468718
    return-void
.end method

.method public final a(LX/04g;)V
    .locals 2

    .prologue
    .line 468791
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    invoke-static {v0, v1}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    .line 468792
    invoke-super {p0, p1}, LX/2pe;->a(LX/04g;)V

    .line 468793
    return-void
.end method

.method public final a(LX/04g;Z)V
    .locals 2

    .prologue
    .line 468785
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_1

    .line 468786
    if-nez p2, :cond_0

    .line 468787
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    sget-object v1, LX/2qV;->ERROR:LX/2qV;

    invoke-static {v0, v1}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    .line 468788
    :cond_0
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/7Lt;

    invoke-direct {v1, p2}, LX/7Lt;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468789
    :cond_1
    invoke-super {p0, p1, p2}, LX/2pe;->a(LX/04g;Z)V

    .line 468790
    return-void
.end method

.method public final a(LX/2oi;)V
    .locals 2

    .prologue
    .line 468781
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    .line 468782
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/7MI;

    invoke-direct {v1, p1}, LX/7MI;-><init>(LX/2oi;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468783
    :cond_0
    invoke-super {p0, p1}, LX/2pe;->a(LX/2oi;)V

    .line 468784
    return-void
.end method

.method public final a(LX/2qD;)V
    .locals 4

    .prologue
    .line 468777
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    .line 468778
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/2ok;

    sget-object v2, LX/3np;->CURRENT_STATE:LX/3np;

    iget-object v2, v2, LX/3np;->value:Ljava/lang/String;

    iget-object v3, p1, LX/2qD;->value:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468779
    :cond_0
    invoke-super {p0, p1}, LX/2pe;->a(LX/2qD;)V

    .line 468780
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 468773
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    .line 468774
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/7MC;

    invoke-direct {v1, p1}, LX/7MC;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468775
    :cond_0
    invoke-super {p0, p1}, LX/2pe;->a(Landroid/graphics/Bitmap;)V

    .line 468776
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 468812
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    .line 468813
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/2ok;

    sget-object v2, LX/3np;->DASH_STREAM:LX/3np;

    iget-object v2, v2, LX/3np;->value:Ljava/lang/String;

    invoke-direct {v1, v2, p1}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468814
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/7Lz;

    invoke-direct {v1, p1}, LX/7Lz;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468815
    :cond_0
    invoke-super {p0, p1}, LX/2pe;->a(Ljava/lang/String;)V

    .line 468816
    return-void
.end method

.method public final a(Ljava/lang/String;LX/7Jj;)V
    .locals 4

    .prologue
    .line 468763
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    sget-object v1, LX/2qV;->ERROR:LX/2qV;

    invoke-static {v0, v1}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    .line 468764
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->E:LX/2q7;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->e()LX/2oi;

    move-result-object v0

    sget-object v1, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    if-ne v0, v1, :cond_1

    .line 468765
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->E:LX/2q7;

    sget-object v1, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/2q7;->a(LX/2oi;LX/04g;Ljava/lang/String;)V

    .line 468766
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->E:LX/2q7;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-interface {v0, v1}, LX/2q7;->a(LX/04g;)V

    .line 468767
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-boolean v0, v0, LX/2pb;->O:Z

    if-eqz v0, :cond_0

    .line 468768
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->m:LX/099;

    iget-object v1, p0, LX/2pd;->a:LX/2pb;

    iget-object v1, v1, LX/2pb;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/099;->b(Ljava/lang/String;)V

    .line 468769
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, LX/2pe;->a(Ljava/lang/String;LX/7Jj;)V

    .line 468770
    return-void

    .line 468771
    :cond_1
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    .line 468772
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/2or;

    invoke-direct {v1, p1, p2}, LX/2or;-><init>(Ljava/lang/String;LX/7Jj;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 468759
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    .line 468760
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/7MH;

    invoke-direct {v1, p1}, LX/7MH;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468761
    :cond_0
    invoke-super {p0, p1}, LX/2pe;->a(Ljava/util/List;)V

    .line 468762
    return-void
.end method

.method public final b(LX/04g;)V
    .locals 2

    .prologue
    .line 468753
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v1, p0, LX/2pd;->a:LX/2pb;

    iget-object v1, v1, LX/2pb;->E:LX/2q7;

    invoke-interface {v1}, LX/2q8;->b()I

    move-result v1

    invoke-static {v0, v1}, LX/2pb;->b(LX/2pb;I)V

    .line 468754
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->SEEKING:LX/2qV;

    if-eq v0, v1, :cond_0

    .line 468755
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    invoke-static {v0, v1, p1}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;LX/04g;)V

    .line 468756
    :cond_0
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->P:LX/2pq;

    invoke-virtual {v0}, LX/2pq;->a()V

    .line 468757
    invoke-super {p0, p1}, LX/2pe;->b(LX/04g;)V

    .line 468758
    return-void
.end method

.method public final b(LX/2qD;)V
    .locals 4

    .prologue
    .line 468749
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    .line 468750
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/2ok;

    sget-object v2, LX/3np;->TARGET_STATE:LX/3np;

    iget-object v2, v2, LX/3np;->value:Ljava/lang/String;

    iget-object v3, p1, LX/2qD;->value:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468751
    :cond_0
    invoke-super {p0, p1}, LX/2pe;->b(LX/2qD;)V

    .line 468752
    return-void
.end method

.method public final c(LX/04g;)V
    .locals 7

    .prologue
    .line 468719
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    .line 468720
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    invoke-static {v0, v1, p1}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;LX/04g;)V

    .line 468721
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/7Ls;

    invoke-direct {v1}, LX/7Ls;-><init>()V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468722
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->p()LX/7IE;

    move-result-object v0

    .line 468723
    if-eqz v0, :cond_0

    .line 468724
    iget-object v1, p0, LX/2pd;->a:LX/2pb;

    iget-object v1, v1, LX/2pb;->H:LX/2oj;

    new-instance v2, LX/2ok;

    sget-object v3, LX/3np;->VIDEO_MIME:LX/3np;

    iget-object v3, v3, LX/3np;->value:Ljava/lang/String;

    invoke-virtual {v0}, LX/7IE;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 468725
    iget-object v1, p0, LX/2pd;->a:LX/2pb;

    iget-object v1, v1, LX/2pb;->H:LX/2oj;

    new-instance v2, LX/2ok;

    sget-object v3, LX/3np;->STREAMING_FORMAT:LX/3np;

    iget-object v3, v3, LX/3np;->value:Ljava/lang/String;

    iget-object v4, v0, LX/7IE;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 468726
    iget-object v1, p0, LX/2pd;->a:LX/2pb;

    iget-object v1, v1, LX/2pb;->H:LX/2oj;

    new-instance v2, LX/2ok;

    sget-object v3, LX/3np;->STREAM_TYPE:LX/3np;

    iget-object v3, v3, LX/3np;->value:Ljava/lang/String;

    iget-object v4, v0, LX/7IE;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 468727
    iget-object v1, p0, LX/2pd;->a:LX/2pb;

    iget-object v1, v1, LX/2pb;->H:LX/2oj;

    new-instance v2, LX/2ok;

    sget-object v3, LX/3np;->DASH_STREAM:LX/3np;

    iget-object v3, v3, LX/3np;->value:Ljava/lang/String;

    .line 468728
    iget-object v4, v0, LX/7IE;->h:Ljava/lang/String;

    move-object v4, v4

    .line 468729
    invoke-direct {v2, v3, v4}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 468730
    iget-object v1, p0, LX/2pd;->a:LX/2pb;

    iget-object v1, v1, LX/2pb;->H:LX/2oj;

    new-instance v2, LX/2ok;

    sget-object v3, LX/3np;->AUDIO_CHANNEL_LAYOUT:LX/3np;

    iget-object v3, v3, LX/3np;->value:Ljava/lang/String;

    .line 468731
    iget-object v4, v0, LX/7IE;->i:LX/03z;

    move-object v4, v4

    .line 468732
    invoke-virtual {v4}, LX/03z;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 468733
    iget-object v1, v0, LX/7IE;->j:Ljava/lang/String;

    move-object v1, v1

    .line 468734
    if-eqz v1, :cond_0

    .line 468735
    iget-object v1, p0, LX/2pd;->a:LX/2pb;

    iget-object v1, v1, LX/2pb;->H:LX/2oj;

    new-instance v2, LX/2ok;

    sget-object v3, LX/3np;->ABR_CONFIG:LX/3np;

    iget-object v3, v3, LX/3np;->value:Ljava/lang/String;

    .line 468736
    iget-object v4, v0, LX/7IE;->j:Ljava/lang/String;

    move-object v0, v4

    .line 468737
    invoke-direct {v2, v3, v0}, LX/2ok;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 468738
    :cond_0
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->e:LX/2pg;

    .line 468739
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/2pg;->b:Z

    .line 468740
    invoke-static {v0}, LX/2pg;->e(LX/2pg;)V

    .line 468741
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->P:LX/2pq;

    .line 468742
    sget-object v5, LX/2pr;->TIMER_STARTED:LX/2pr;

    iput-object v5, v0, LX/2pq;->f:LX/2pr;

    .line 468743
    iget-object v5, v0, LX/2pq;->c:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v5

    iput-wide v5, v0, LX/2pq;->g:J

    .line 468744
    invoke-static {v0}, LX/2pq;->c(LX/2pq;)V

    .line 468745
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->c:LX/2ph;

    invoke-virtual {v0}, LX/2ph;->b()V

    .line 468746
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    const/4 v1, -0x1

    invoke-static {v0, v1}, LX/2pb;->b(LX/2pb;I)V

    .line 468747
    invoke-super {p0, p1}, LX/2pe;->c(LX/04g;)V

    .line 468748
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 468711
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 468712
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    invoke-static {v0, v1}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    .line 468713
    :cond_0
    invoke-super {p0}, LX/2pe;->e()V

    .line 468714
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 468707
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 468708
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    invoke-static {v0, v1}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    .line 468709
    :cond_0
    invoke-super {p0}, LX/2pe;->f()V

    .line 468710
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 468703
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    .line 468704
    iget-object v0, p0, LX/2pd;->a:LX/2pb;

    iget-object v0, v0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/2pY;

    invoke-direct {v1}, LX/2pY;-><init>()V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468705
    :cond_0
    invoke-super {p0}, LX/2pe;->g()V

    .line 468706
    return-void
.end method
