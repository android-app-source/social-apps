.class public final LX/2sK;
.super LX/0ur;
.source ""


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLMegaphone;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

.field public D:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLGreetingCard;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierFeaturesVectorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLSouvenirClassifierModelParamsMapsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTaggableActivity;",
            ">;"
        }
    .end annotation
.end field

.field public J:I

.field public K:I

.field public L:I

.field public M:I

.field public N:I

.field public O:Lcom/facebook/graphql/model/GraphQLMediaSet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLAudienceInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLCustomizedStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I

.field public o:Lcom/facebook/graphql/model/GraphQLEligibleClashUnitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:I

.field public q:I

.field public r:Lcom/facebook/graphql/model/GraphQLFriendingPossibilitiesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Z

.field public u:Z

.field public v:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Z

.field public x:Z

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 472514
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 472515
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    iput-object v0, p0, LX/2sK;->C:Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    .line 472516
    instance-of v0, p0, LX/2sK;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 472517
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLViewer;
    .locals 2

    .prologue
    .line 472518
    new-instance v0, Lcom/facebook/graphql/model/GraphQLViewer;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLViewer;-><init>(LX/2sK;)V

    .line 472519
    return-object v0
.end method
