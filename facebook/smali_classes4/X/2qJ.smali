.class public final LX/2qJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field public final synthetic a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;


# direct methods
.method public constructor <init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V
    .locals 0

    .prologue
    .line 470805
    iput-object p1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 470806
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->M(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_1

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xf

    if-eq v0, v1, :cond_1

    .line 470807
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    const-string v1, "Try handling message but no service connected"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470808
    :cond_0
    :goto_0
    return v2

    .line 470809
    :cond_1
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 470810
    :pswitch_1
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->N(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 470811
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    const-string v1, "buildRenderers attempted but no session created. Aborting."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v4}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 470812
    :catch_0
    move-exception v0

    .line 470813
    sget-object v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aX:Ljava/lang/String;

    const-string v4, "Service DeadObjectException"

    invoke-static {v1, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 470814
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    const-string v1, "Tried to process message"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->c(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 470815
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    invoke-interface {v1, v4, v0}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Landroid/net/Uri;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 470816
    :catch_1
    move-exception v0

    .line 470817
    :try_start_2
    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    const-string v4, "Handle error during buildRenderers"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470818
    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    sget-object v4, LX/7Jj;->PLAYERSERVICE_DEAD:LX/7Jj;

    const-wide/16 v6, 0x0

    invoke-static {v1, v4, v0, v6, v7}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a$redex0(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/7Jj;Ljava/lang/Throwable;J)V

    .line 470819
    throw v0
    :try_end_2
    .catch Landroid/os/DeadObjectException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 470820
    :catch_2
    move-exception v0

    .line 470821
    sget-object v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aX:Ljava/lang/String;

    const-string v3, "Service RemoteException"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 470822
    :pswitch_2
    :try_start_3
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->N(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 470823
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    const-string v1, "addListener attempted but no session created. Aborting."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v4}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 470824
    :cond_3
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bg:LX/041;

    if-nez v0, :cond_4

    .line 470825
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    new-instance v1, LX/7KP;

    iget-object v4, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-direct {v1, v4}, LX/7KP;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V

    .line 470826
    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bg:LX/041;

    .line 470827
    :cond_4
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v4, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v4, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bg:LX/041;

    invoke-interface {v0, v1, v4}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/042;)V

    goto/16 :goto_0

    .line 470828
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;
    :try_end_3
    .catch Landroid/os/DeadObjectException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    .line 470829
    :try_start_4
    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v4, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    invoke-interface {v4, v0}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayRequest;)Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    move-result-object v4

    .line 470830
    iput-object v4, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 470831
    if-eqz v0, :cond_0

    .line 470832
    :try_start_5
    invoke-virtual {v0}, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->close()V
    :try_end_5
    .catch Landroid/os/DeadObjectException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 470833
    :catch_3
    move-exception v1

    .line 470834
    :try_start_6
    iget-object v4, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    const-string v5, "Handle error during registerSession"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470835
    iget-object v4, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    sget-object v5, LX/7Jj;->PLAYERSERVICE_DEAD:LX/7Jj;

    const-wide/16 v6, 0x0

    invoke-static {v4, v5, v1, v6, v7}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a$redex0(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/7Jj;Ljava/lang/Throwable;J)V

    .line 470836
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 470837
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_5

    .line 470838
    :try_start_7
    invoke-virtual {v0}, Lcom/facebook/exoplayer/ipc/VideoPlayRequest;->close()V

    :cond_5
    throw v1

    .line 470839
    :pswitch_4
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget v4, p1, Landroid/os/Message;->arg1:I

    int-to-long v4, v4

    invoke-interface {v0, v1, v4, v5}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;J)V

    goto/16 :goto_0

    .line 470840
    :pswitch_5
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {v1, v4, v0}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)V

    goto/16 :goto_0

    .line 470841
    :pswitch_6
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-interface {v1, v0}, LX/04j;->c(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto/16 :goto_0

    .line 470842
    :pswitch_7
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v5, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->al:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-interface {v1, v4, v5, v0}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;F)V

    goto/16 :goto_0

    .line 470843
    :pswitch_8
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-interface {v0, v1}, LX/04j;->i(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto/16 :goto_0

    .line 470844
    :pswitch_9
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-interface {v0, v1}, LX/04j;->j(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;)V

    goto/16 :goto_0

    .line 470845
    :pswitch_a
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bg:LX/041;

    if-eqz v0, :cond_0

    .line 470846
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v4, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v4, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bg:LX/041;

    invoke-interface {v1, v0, v4}, LX/04j;->b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;LX/042;)V

    .line 470847
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    const/4 v1, 0x0

    .line 470848
    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bg:LX/041;

    .line 470849
    goto/16 :goto_0

    .line 470850
    :pswitch_b
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget v1, p1, Landroid/os/Message;->arg1:I

    if-ne v1, v2, :cond_6

    move v1, v2

    :goto_1
    invoke-interface {v4, v0, v1}, LX/04j;->b(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Z)V

    .line 470851
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v0, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 470852
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    const/4 v1, 0x0

    .line 470853
    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 470854
    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 470855
    goto :goto_1

    .line 470856
    :pswitch_c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/7KG;

    .line 470857
    if-eqz v0, :cond_0

    .line 470858
    invoke-virtual {v0}, LX/7KG;->a()V

    goto/16 :goto_0

    .line 470859
    :pswitch_d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/Surface;

    .line 470860
    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->af:Landroid/view/Surface;

    if-eq v1, v0, :cond_0

    .line 470861
    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-boolean v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->bz:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ae:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    if-eqz v1, :cond_7

    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ae:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v4, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v4, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    invoke-virtual {v1, v4}, Lcom/facebook/exoplayer/ipc/VideoPlayerSession;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 470862
    const-string v1, "ExoVideoPlayerClient.setSurfaceToNull"

    const v4, -0x2ee70362

    invoke-static {v1, v4}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_7
    .catch Landroid/os/DeadObjectException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_2

    .line 470863
    :try_start_8
    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v4, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v4, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ae:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v5, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v5, v5, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ak:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    const/4 v6, 0x0

    invoke-interface {v1, v4, v5, v6}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Landroid/view/Surface;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 470864
    const v1, -0x33619434    # -8.3058272E7f

    :try_start_9
    invoke-static {v1}, LX/02m;->a(I)V

    .line 470865
    :cond_7
    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v4, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v4, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v5, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v5, v5, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aj:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    invoke-interface {v1, v4, v5, v0}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Landroid/view/Surface;)V

    .line 470866
    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    .line 470867
    iput-object v0, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->af:Landroid/view/Surface;

    .line 470868
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 470869
    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ae:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    .line 470870
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->aj:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    .line 470871
    iput-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ak:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    .line 470872
    goto/16 :goto_0

    .line 470873
    :catchall_1
    move-exception v0

    const v1, -0xa01175f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 470874
    :pswitch_e
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v5, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->al:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;

    invoke-interface {v1, v4, v5, v0}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;)V

    goto/16 :goto_0

    .line 470875
    :pswitch_f
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v1, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ag:LX/04j;

    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v4, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->ad:Lcom/facebook/exoplayer/ipc/VideoPlayerSession;

    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    iget-object v5, v0, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->al:Lcom/facebook/exoplayer/ipc/MediaRenderer;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;

    invoke-interface {v1, v4, v5, v0}, LX/04j;->a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;)V

    goto/16 :goto_0

    .line 470876
    :pswitch_10
    iget-object v0, p0, LX/2qJ;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-static {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->Q(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;)V
    :try_end_9
    .catch Landroid/os/DeadObjectException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_d
        :pswitch_e
        :pswitch_8
        :pswitch_9
        :pswitch_f
        :pswitch_0
        :pswitch_c
        :pswitch_10
    .end packed-switch
.end method
