.class public LX/2gU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2gU;


# instance fields
.field private final a:LX/2Hu;


# direct methods
.method public constructor <init>(LX/2Hu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448402
    iput-object p1, p0, LX/2gU;->a:LX/2Hu;

    .line 448403
    return-void
.end method

.method public static a(LX/0QB;)LX/2gU;
    .locals 4

    .prologue
    .line 448404
    sget-object v0, LX/2gU;->b:LX/2gU;

    if-nez v0, :cond_1

    .line 448405
    const-class v1, LX/2gU;

    monitor-enter v1

    .line 448406
    :try_start_0
    sget-object v0, LX/2gU;->b:LX/2gU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 448407
    if-eqz v2, :cond_0

    .line 448408
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 448409
    new-instance p0, LX/2gU;

    invoke-static {v0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v3

    check-cast v3, LX/2Hu;

    invoke-direct {p0, v3}, LX/2gU;-><init>(LX/2Hu;)V

    .line 448410
    move-object v0, p0

    .line 448411
    sput-object v0, LX/2gU;->b:LX/2gU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448412
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 448413
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 448414
    :cond_1
    sget-object v0, LX/2gU;->b:LX/2gU;

    return-object v0

    .line 448415
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 448416
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;LX/2I2;LX/76H;)I
    .locals 2
    .param p4    # LX/76H;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 448417
    :try_start_0
    iget-object v0, p0, LX/2gU;->a:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 448418
    :try_start_1
    invoke-virtual {v1, p1, p2, p3, p4}, LX/2gV;->a(Ljava/lang/String;LX/0lF;LX/2I2;LX/76H;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 448419
    :try_start_2
    invoke-virtual {v1}, LX/2gV;->f()V

    .line 448420
    :goto_0
    return v0

    .line 448421
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 448422
    :catch_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[BLX/2I2;LX/76H;)I
    .locals 2
    .param p4    # LX/76H;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 448423
    :try_start_0
    iget-object v0, p0, LX/2gU;->a:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 448424
    :try_start_1
    invoke-virtual {v1, p1, p2, p3, p4}, LX/2gV;->a(Ljava/lang/String;[BLX/2I2;LX/76H;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 448425
    :try_start_2
    invoke-virtual {v1}, LX/2gV;->f()V

    .line 448426
    :goto_0
    return v0

    .line 448427
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 448428
    :catch_0
    const/4 v0, -0x1

    goto :goto_0
.end method
