.class public LX/2Pf;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Pf;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406934
    const-string v0, "offline_lwi_part"

    const/4 v1, 0x1

    new-instance v2, LX/2Pg;

    invoke-direct {v2}, LX/2Pg;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 406935
    return-void
.end method

.method public static a(LX/0QB;)LX/2Pf;
    .locals 3

    .prologue
    .line 406922
    sget-object v0, LX/2Pf;->a:LX/2Pf;

    if-nez v0, :cond_1

    .line 406923
    const-class v1, LX/2Pf;

    monitor-enter v1

    .line 406924
    :try_start_0
    sget-object v0, LX/2Pf;->a:LX/2Pf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406925
    if-eqz v2, :cond_0

    .line 406926
    :try_start_1
    new-instance v0, LX/2Pf;

    invoke-direct {v0}, LX/2Pf;-><init>()V

    .line 406927
    move-object v0, v0

    .line 406928
    sput-object v0, LX/2Pf;->a:LX/2Pf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406929
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406930
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406931
    :cond_1
    sget-object v0, LX/2Pf;->a:LX/2Pf;

    return-object v0

    .line 406932
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406933
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
