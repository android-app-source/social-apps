.class public final LX/25f;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0

    .prologue
    .line 369820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369821
    iput p1, p0, LX/25f;->a:I

    .line 369822
    iput p2, p0, LX/25f;->b:I

    .line 369823
    iput p3, p0, LX/25f;->c:I

    .line 369824
    iput p4, p0, LX/25f;->d:I

    .line 369825
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 369826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369827
    add-int/lit8 v0, p2, 0x4

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iput v0, p0, LX/25f;->b:I

    .line 369828
    iget v0, p0, LX/25f;->b:I

    if-nez v0, :cond_1

    .line 369829
    iput v1, p0, LX/25f;->a:I

    .line 369830
    iput v1, p0, LX/25f;->c:I

    .line 369831
    iput v1, p0, LX/25f;->d:I

    .line 369832
    :goto_0
    iget v0, p0, LX/25f;->a:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p0, LX/25f;->a:I

    iget v1, p0, LX/25f;->b:I

    add-int/2addr v0, v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    if-gt v0, v1, :cond_0

    iget v0, p0, LX/25f;->c:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 369833
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "DeltaIndex out of bound, limit=%d, start=%d, size=%d, pivot=%d"

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p0, LX/25f;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v4, p0, LX/25f;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v5, p0, LX/25f;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369834
    :cond_1
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    add-int/2addr v0, p2

    iput v0, p0, LX/25f;->a:I

    .line 369835
    add-int/lit8 v0, p2, 0x8

    add-int/lit8 v1, p2, 0x8

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/25f;->c:I

    .line 369836
    add-int/lit8 v0, p2, 0xc

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    iput v0, p0, LX/25f;->d:I

    goto :goto_0

    .line 369837
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 369838
    iget v0, p0, LX/25f;->b:I

    packed-switch v0, :pswitch_data_0

    .line 369839
    :pswitch_0
    const/16 v0, 0x8

    :goto_0
    return v0

    .line 369840
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 369841
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 369842
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 369843
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/186;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 369844
    iget v0, p0, LX/25f;->b:I

    if-nez v0, :cond_0

    .line 369845
    invoke-virtual {p1, v1}, LX/186;->a(I)V

    .line 369846
    invoke-virtual {p1, v1}, LX/186;->b(I)V

    .line 369847
    invoke-virtual {p1, v1}, LX/186;->a(I)V

    .line 369848
    invoke-virtual {p1, v1}, LX/186;->b(I)V

    .line 369849
    :goto_0
    return-void

    .line 369850
    :cond_0
    iget v0, p0, LX/25f;->d:I

    invoke-virtual {p1, v0}, LX/186;->a(I)V

    .line 369851
    iget v0, p0, LX/25f;->c:I

    invoke-virtual {p1, v0}, LX/186;->b(I)V

    .line 369852
    iget v0, p0, LX/25f;->b:I

    invoke-virtual {p1, v0}, LX/186;->a(I)V

    .line 369853
    iget v0, p0, LX/25f;->a:I

    invoke-virtual {p1, v0}, LX/186;->b(I)V

    goto :goto_0
.end method
