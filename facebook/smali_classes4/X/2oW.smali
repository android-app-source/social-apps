.class public LX/2oW;
.super Lcom/facebook/video/player/RichVideoPlayer;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 466455
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 466456
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 466453
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 466454
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 466442
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/player/RichVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 466443
    invoke-virtual {p0}, LX/2oW;->getDefaultPlayerOrigin()LX/04D;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 466444
    invoke-virtual {p0}, LX/2oW;->getDefaultPlayerType()LX/04G;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 466445
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 466446
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 466447
    invoke-virtual {p0, p1}, LX/2oW;->a(Landroid/content/Context;)LX/0Px;

    move-result-object v2

    .line 466448
    if-eqz v2, :cond_0

    .line 466449
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 466450
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 466451
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 466452
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/0Px",
            "<+",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 466441
    new-instance v0, LX/2pM;

    invoke-direct {v0, p1}, LX/2pM;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 466457
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 466458
    return-void
.end method

.method public getDefaultPlayerOrigin()LX/04D;
    .locals 1

    .prologue
    .line 466440
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    return-object v0
.end method

.method public getDefaultPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 466439
    sget-object v0, LX/04G;->OTHERS:LX/04G;

    return-object v0
.end method

.method public jj_()V
    .locals 1

    .prologue
    .line 466437
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 466438
    return-void
.end method
