.class public LX/3NH;
.super LX/3Ml;
.source ""


# instance fields
.field private final c:LX/3NF;

.field public final d:LX/2Og;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zr;LX/3NF;LX/2Og;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zr;",
            "LX/3NF;",
            "LX/2Og;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558924
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 558925
    iput-object p2, p0, LX/3NH;->c:LX/3NF;

    .line 558926
    iput-object p3, p0, LX/3NH;->d:LX/2Og;

    .line 558927
    iput-object p4, p0, LX/3NH;->e:LX/0Or;

    .line 558928
    return-void
.end method

.method public static a(LX/0QB;)LX/3NH;
    .locals 1

    .prologue
    .line 558923
    invoke-static {p0}, LX/3NH;->b(LX/0QB;)LX/3NH;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/0Px;)LX/0Px;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/search/model/PlatformSearchData;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/search/model/PlatformSearchData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 558929
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 558930
    :cond_0
    :goto_0
    return-object p1

    .line 558931
    :cond_1
    iget-object v0, p0, LX/3NH;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 558932
    if-eqz v0, :cond_0

    .line 558933
    iget-object v1, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v1

    .line 558934
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 558935
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 558936
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v11

    const/4 v8, 0x0

    move v9, v8

    :goto_1
    if-ge v9, v11, :cond_3

    invoke-virtual {p1, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/business/search/model/PlatformSearchData;

    .line 558937
    instance-of v12, v8, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;

    if-eqz v12, :cond_2

    .line 558938
    check-cast v8, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;

    iget-object v8, v8, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->e:Ljava/lang/String;

    .line 558939
    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v12

    .line 558940
    iget-object v13, p0, LX/3NH;->d:LX/2Og;

    invoke-virtual {v13, v12}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v12

    if-eqz v12, :cond_2

    .line 558941
    invoke-interface {v10, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 558942
    :cond_2
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_1

    .line 558943
    :cond_3
    move-object v3, v10

    .line 558944
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 558945
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 558946
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 558947
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 558948
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v7, :cond_6

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;

    .line 558949
    iget-boolean v1, v0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;->d:Z

    if-eqz v1, :cond_4

    .line 558950
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 558951
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 558952
    :cond_4
    instance-of v1, v0, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;

    if-eqz v1, :cond_5

    move-object v1, v0

    check-cast v1, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;

    iget-object v1, v1, Lcom/facebook/messaging/business/search/model/PlatformSearchUserData;->e:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 558953
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 558954
    :cond_5
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 558955
    :cond_6
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 558956
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 558957
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    goto/16 :goto_0
.end method

.method public static b(LX/0QB;)LX/3NH;
    .locals 5

    .prologue
    .line 558921
    new-instance v3, LX/3NH;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v0

    check-cast v0, LX/0Zr;

    invoke-static {p0}, LX/3NF;->b(LX/0QB;)LX/3NF;

    move-result-object v1

    check-cast v1, LX/3NF;

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v2

    check-cast v2, LX/2Og;

    const/16 v4, 0x12cb

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v3, v0, v1, v2, v4}, LX/3NH;-><init>(LX/0Zr;LX/3NF;LX/2Og;LX/0Or;)V

    .line 558922
    return-object v3
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 7
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 558899
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 558900
    :goto_0
    new-instance v1, LX/39y;

    invoke-direct {v1}, LX/39y;-><init>()V

    .line 558901
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 558902
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    .line 558903
    const/4 v0, -0x1

    iput v0, v1, LX/39y;->b:I

    move-object v0, v1

    .line 558904
    :goto_1
    return-object v0

    .line 558905
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 558906
    :cond_1
    :try_start_0
    iget-object v3, p0, LX/3NH;->c:LX/3NF;

    const/4 v4, 0x6

    invoke-virtual {v3, v0, v4}, LX/3NF;->a(Ljava/lang/String;I)LX/0Px;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 558907
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 558908
    invoke-direct {p0, v0}, LX/3NH;->b(LX/0Px;)LX/0Px;

    move-result-object v4

    .line 558909
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_2
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/search/model/PlatformSearchData;

    .line 558910
    iget-object v6, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v6, v0}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v0

    .line 558911
    if-eqz v0, :cond_2

    .line 558912
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 558913
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 558914
    :catch_0
    iput v2, v1, LX/39y;->b:I

    .line 558915
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 558916
    goto :goto_1

    .line 558917
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 558918
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    iput v2, v1, LX/39y;->b:I

    .line 558919
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 558920
    goto :goto_1
.end method
