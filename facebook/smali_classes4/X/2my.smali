.class public LX/2my;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2mz;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static o:LX/0Xm;


# instance fields
.field public final a:LX/2nF;

.field public final b:LX/2nH;

.field public c:LX/D7g;

.field public final d:LX/17Y;

.field public final e:LX/2n0;

.field public final f:LX/2n3;

.field public final g:LX/17V;

.field public final h:LX/0Zb;

.field public final i:LX/0hB;

.field private final j:LX/2nG;

.field public k:Landroid/content/Context;

.field public l:Lcom/facebook/browser/lite/BrowserLiteFragment;

.field private m:Landroid/view/ViewGroup;

.field public n:LX/D8S;


# direct methods
.method public constructor <init>(LX/17Y;LX/2n0;LX/2n3;LX/17V;LX/0Zb;LX/0hB;LX/2nF;LX/2nG;LX/2nH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 462395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462396
    iput-object p1, p0, LX/2my;->d:LX/17Y;

    .line 462397
    iput-object p2, p0, LX/2my;->e:LX/2n0;

    .line 462398
    iput-object p3, p0, LX/2my;->f:LX/2n3;

    .line 462399
    iput-object p4, p0, LX/2my;->g:LX/17V;

    .line 462400
    iput-object p5, p0, LX/2my;->h:LX/0Zb;

    .line 462401
    iput-object p6, p0, LX/2my;->i:LX/0hB;

    .line 462402
    iput-object p7, p0, LX/2my;->a:LX/2nF;

    .line 462403
    iput-object p8, p0, LX/2my;->j:LX/2nG;

    .line 462404
    iput-object p9, p0, LX/2my;->b:LX/2nH;

    .line 462405
    return-void
.end method

.method public static a(LX/0QB;)LX/2my;
    .locals 13

    .prologue
    .line 462384
    const-class v1, LX/2my;

    monitor-enter v1

    .line 462385
    :try_start_0
    sget-object v0, LX/2my;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 462386
    sput-object v2, LX/2my;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 462387
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462388
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 462389
    new-instance v3, LX/2my;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17Y;

    invoke-static {v0}, LX/2n0;->b(LX/0QB;)LX/2n0;

    move-result-object v5

    check-cast v5, LX/2n0;

    invoke-static {v0}, LX/2n3;->b(LX/0QB;)LX/2n3;

    move-result-object v6

    check-cast v6, LX/2n3;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v7

    check-cast v7, LX/17V;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v9

    check-cast v9, LX/0hB;

    invoke-static {v0}, LX/2nF;->a(LX/0QB;)LX/2nF;

    move-result-object v10

    check-cast v10, LX/2nF;

    invoke-static {v0}, LX/2nG;->a(LX/0QB;)LX/2nG;

    move-result-object v11

    check-cast v11, LX/2nG;

    invoke-static {v0}, LX/2nH;->b(LX/0QB;)LX/2nH;

    move-result-object v12

    check-cast v12, LX/2nH;

    invoke-direct/range {v3 .. v12}, LX/2my;-><init>(LX/17Y;LX/2n0;LX/2n3;LX/17V;LX/0Zb;LX/0hB;LX/2nF;LX/2nG;LX/2nH;)V

    .line 462390
    move-object v0, v3

    .line 462391
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 462392
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2my;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 462393
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 462394
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 462383
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 462382
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b12da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 462369
    iget-object v1, p0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-nez v1, :cond_1

    .line 462370
    :cond_0
    :goto_0
    iput-object v0, p0, LX/2my;->k:Landroid/content/Context;

    .line 462371
    iput-object v0, p0, LX/2my;->c:LX/D7g;

    .line 462372
    iput-object v0, p0, LX/2my;->m:Landroid/view/ViewGroup;

    .line 462373
    iput-object v0, p0, LX/2my;->n:LX/D8S;

    .line 462374
    iget-object v0, p0, LX/2my;->j:LX/2nG;

    .line 462375
    iget-object v1, v0, LX/2nG;->a:LX/0if;

    sget-object v2, LX/0ig;->aR:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 462376
    return-void

    .line 462377
    :cond_1
    iget-object v1, p0, LX/2my;->k:Landroid/content/Context;

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 462378
    if-eqz v1, :cond_0

    .line 462379
    iget-object v2, p0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/browser/lite/BrowserLiteFragment;->b(I)V

    .line 462380
    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 462381
    const/4 v1, 0x0

    iput-object v1, p0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    goto :goto_0
.end method

.method public a(ILandroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D8b;LX/D8V;ILX/D8S;)V
    .locals 1
    .param p9    # LX/D8S;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/D8b;",
            "Lcom/facebook/video/watchandmore/core/OnExitWatchAndMoreListener;",
            "I",
            "Lcom/facebook/video/watchandmore/core/WatchAndMoreContentAnimationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 462406
    iput-object p4, p0, LX/2my;->k:Landroid/content/Context;

    .line 462407
    iput-object p2, p0, LX/2my;->m:Landroid/view/ViewGroup;

    .line 462408
    iput-object p9, p0, LX/2my;->n:LX/D8S;

    .line 462409
    iget-object v0, p0, LX/2my;->k:Landroid/content/Context;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 462410
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 462411
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 462412
    iget-object v0, p0, LX/2my;->k:Landroid/content/Context;

    const-class p2, Landroid/app/Activity;

    invoke-static {v0, p2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 462413
    if-nez v0, :cond_2

    .line 462414
    :cond_0
    :goto_0
    iget-object p1, p0, LX/2my;->b:LX/2nH;

    .line 462415
    iget-object v0, p5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 462416
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/2nH;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object p1

    .line 462417
    if-nez p1, :cond_6

    .line 462418
    :cond_1
    :goto_1
    iget-object v0, p0, LX/2my;->j:LX/2nG;

    .line 462419
    iget-object p0, v0, LX/2nG;->a:LX/0if;

    sget-object p1, LX/0ig;->aR:LX/0ih;

    invoke-virtual {p0, p1}, LX/0if;->a(LX/0ih;)V

    .line 462420
    return-void

    .line 462421
    :cond_2
    iget-object p2, p0, LX/2my;->k:Landroid/content/Context;

    invoke-virtual {p0, p2, p5}, LX/2my;->c(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/content/Intent;

    move-result-object p3

    .line 462422
    if-eqz p3, :cond_0

    .line 462423
    iget-object p2, p0, LX/2my;->a:LX/2nF;

    invoke-virtual {p2}, LX/2nF;->a()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 462424
    iget-object p2, p0, LX/2my;->i:LX/0hB;

    invoke-virtual {p2}, LX/0hB;->d()I

    move-result p2

    iget-object p4, p0, LX/2my;->k:Landroid/content/Context;

    invoke-virtual {p0, p4, p5}, LX/2my;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result p4

    sub-int/2addr p2, p4

    iget-object p4, p0, LX/2my;->k:Landroid/content/Context;

    invoke-virtual {p0, p4}, LX/2my;->a(Landroid/content/Context;)I

    move-result p4

    sub-int/2addr p2, p4

    .line 462425
    const-string p4, "watch_and_browse_browser_height"

    invoke-virtual {p3, p4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 462426
    :cond_3
    const-string p2, "watch_and_browse_is_in_watch_and_browse"

    const/4 p4, 0x1

    invoke-virtual {p3, p2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462427
    const-string p2, "watch_and_browse_dummy_video_view_height"

    invoke-virtual {p3, p2, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 462428
    iget-object p2, p0, LX/2my;->e:LX/2n0;

    invoke-virtual {p2, p3}, LX/2n0;->a(Landroid/content/Intent;)V

    .line 462429
    invoke-static {p5}, LX/14w;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p2

    if-eqz p2, :cond_5

    const-string p2, "watch_browse_ads"

    .line 462430
    :goto_2
    const-string p4, "iab_click_source"

    invoke-virtual {p3, p4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 462431
    invoke-static {p5}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object p2

    .line 462432
    if-eqz p2, :cond_4

    .line 462433
    const-string p4, "tracking_codes"

    invoke-virtual {p2}, LX/162;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 462434
    :cond_4
    iget-object p2, p0, LX/2my;->f:LX/2n3;

    iget-object p4, p0, LX/2my;->k:Landroid/content/Context;

    invoke-virtual {p2, p3, p4}, LX/2n3;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 462435
    invoke-virtual {v0, p3}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    .line 462436
    new-instance p2, Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-direct {p2}, Lcom/facebook/browser/lite/BrowserLiteFragment;-><init>()V

    iput-object p2, p0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    .line 462437
    iget-object p2, p0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    new-instance p3, LX/D7f;

    invoke-direct {p3, p0, p7}, LX/D7f;-><init>(LX/2my;LX/D8V;)V

    .line 462438
    iput-object p3, p2, Lcom/facebook/browser/lite/BrowserLiteFragment;->k:LX/0C4;

    .line 462439
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object p2, p0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0, p1, p2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    .line 462440
    :cond_5
    const-string p2, "watch_browse"

    goto :goto_2

    .line 462441
    :cond_6
    invoke-static {p5}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object p2

    .line 462442
    iget-object p3, p0, LX/2my;->g:LX/17V;

    .line 462443
    iget-object v0, p5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 462444
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    const-string p4, "video"

    invoke-virtual {p3, p1, v0, p2, p4}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 462445
    if-eqz v0, :cond_1

    .line 462446
    const-string p1, "is_watch_and_browse"

    const/4 p2, 0x1

    invoke-virtual {v0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 462447
    iget-object p1, p0, LX/2my;->h:LX/0Zb;

    invoke-interface {p1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_1
.end method

.method public final a(Landroid/content/res/Configuration;I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 462360
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 462361
    iget-object v0, p0, LX/2my;->m:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 462362
    :cond_0
    :goto_0
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_3

    .line 462363
    :cond_1
    :goto_1
    return-void

    .line 462364
    :cond_2
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_0

    .line 462365
    iget-object v0, p0, LX/2my;->m:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 462366
    :cond_3
    iget-object v0, p0, LX/2my;->c:LX/D7g;

    if-eqz v0, :cond_1

    .line 462367
    iget-object v0, p0, LX/2my;->c:LX/D7g;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/D7g;->a(F)V

    .line 462368
    iget-object v0, p0, LX/2my;->c:LX/D7g;

    invoke-interface {v0, v2}, LX/D7g;->b(I)V

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 462357
    iget-object v0, p0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-nez v0, :cond_0

    .line 462358
    :goto_0
    return-void

    .line 462359
    :cond_0
    iget-object v0, p0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->g()V

    goto :goto_0
.end method

.method public b(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 462350
    iget-object v1, p0, LX/2my;->b:LX/2nH;

    .line 462351
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 462352
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2nH;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 462353
    if-nez v0, :cond_1

    .line 462354
    const/4 v0, 0x0

    .line 462355
    :goto_0
    move-object v0, v0

    .line 462356
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    iget-object v1, p0, LX/2my;->d:LX/17Y;

    invoke-interface {v1, p1, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public c()LX/D8g;
    .locals 1

    .prologue
    .line 462349
    sget-object v0, LX/D8g;->WATCH_AND_BROWSE:LX/D8g;

    return-object v0
.end method

.method public c(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 462341
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v1

    .line 462342
    :goto_0
    return-object v0

    .line 462343
    :cond_1
    iget-object v2, p0, LX/2my;->b:LX/2nH;

    .line 462344
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 462345
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2nH;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 462346
    if-nez v0, :cond_2

    move-object v0, v1

    .line 462347
    goto :goto_0

    .line 462348
    :cond_2
    iget-object v1, p0, LX/2my;->d:LX/17Y;

    invoke-interface {v1, p1, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 462340
    const/4 v0, 0x0

    return v0
.end method

.method public e()LX/D7g;
    .locals 1

    .prologue
    .line 462334
    iget-object v0, p0, LX/2my;->a:LX/2nF;

    invoke-virtual {v0}, LX/2nF;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 462335
    const/4 v0, 0x0

    .line 462336
    :goto_0
    return-object v0

    .line 462337
    :cond_0
    iget-object v0, p0, LX/2my;->c:LX/D7g;

    if-nez v0, :cond_1

    .line 462338
    new-instance v0, LX/D7h;

    invoke-direct {v0, p0}, LX/D7h;-><init>(LX/2my;)V

    iput-object v0, p0, LX/2my;->c:LX/D7g;

    .line 462339
    :cond_1
    iget-object v0, p0, LX/2my;->c:LX/D7g;

    goto :goto_0
.end method
