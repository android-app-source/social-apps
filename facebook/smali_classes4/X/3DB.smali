.class public LX/3DB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/3DB;


# instance fields
.field private final a:LX/19s;

.field private final b:LX/19j;

.field private final c:LX/1Lr;

.field public final d:LX/3DC;


# direct methods
.method public constructor <init>(LX/19s;LX/19j;LX/1Lr;LX/3DC;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 531850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 531851
    iput-object p1, p0, LX/3DB;->a:LX/19s;

    .line 531852
    iput-object p2, p0, LX/3DB;->b:LX/19j;

    .line 531853
    iput-object p3, p0, LX/3DB;->c:LX/1Lr;

    .line 531854
    iput-object p4, p0, LX/3DB;->d:LX/3DC;

    .line 531855
    return-void
.end method

.method public static a(LX/0QB;)LX/3DB;
    .locals 7

    .prologue
    .line 531889
    sget-object v0, LX/3DB;->e:LX/3DB;

    if-nez v0, :cond_1

    .line 531890
    const-class v1, LX/3DB;

    monitor-enter v1

    .line 531891
    :try_start_0
    sget-object v0, LX/3DB;->e:LX/3DB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 531892
    if-eqz v2, :cond_0

    .line 531893
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 531894
    new-instance p0, LX/3DB;

    invoke-static {v0}, LX/19i;->a(LX/0QB;)LX/19s;

    move-result-object v3

    check-cast v3, LX/19s;

    invoke-static {v0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v4

    check-cast v4, LX/19j;

    invoke-static {v0}, LX/1Lr;->a(LX/0QB;)LX/1Lr;

    move-result-object v5

    check-cast v5, LX/1Lr;

    invoke-static {v0}, LX/3DC;->b(LX/0QB;)LX/3DC;

    move-result-object v6

    check-cast v6, LX/3DC;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3DB;-><init>(LX/19s;LX/19j;LX/1Lr;LX/3DC;)V

    .line 531895
    move-object v0, p0

    .line 531896
    sput-object v0, LX/3DB;->e:LX/3DB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 531897
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 531898
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 531899
    :cond_1
    sget-object v0, LX/3DB;->e:LX/3DB;

    return-object v0

    .line 531900
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 531901
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/util/List;LX/379;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;",
            ">;",
            "LX/379;",
            ")V"
        }
    .end annotation

    .prologue
    .line 531902
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 531903
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, LX/3DB;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/util/List;LX/379;)V

    .line 531904
    :cond_0
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 531905
    invoke-direct {p0, v0, p2, p3}, LX/3DB;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/util/List;LX/379;)V

    .line 531906
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 531907
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 531908
    invoke-direct {p0, v0, p2, p3}, LX/3DB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/util/List;LX/379;)V

    goto :goto_1

    .line 531909
    :cond_2
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/util/List;LX/379;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;",
            ">;",
            "LX/379;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 531866
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 531867
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v4

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 531868
    invoke-direct {p0, v0, p2, p3}, LX/3DB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/util/List;LX/379;)V

    .line 531869
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 531870
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 531871
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4ed245b

    if-eq v1, v2, :cond_2

    .line 531872
    :cond_1
    :goto_1
    return-void

    .line 531873
    :cond_2
    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v6

    .line 531874
    if-eqz v6, :cond_1

    .line 531875
    const/4 v1, 0x0

    .line 531876
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 531877
    :goto_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v2, :cond_3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v2, :cond_3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_CINEMAGRAPH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v2, :cond_3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v2, :cond_3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v0, v2, :cond_6

    .line 531878
    :cond_3
    const/4 v0, 0x1

    .line 531879
    :goto_3
    move v0, v0

    .line 531880
    if-nez v0, :cond_4

    iget-object v0, p0, LX/3DB;->c:LX/1Lr;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/1Lr;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 531881
    :cond_4
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 531882
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideo;->aD()Ljava/lang/String;

    move-result-object v0

    .line 531883
    iget-object v1, p0, LX/3DB;->b:LX/19j;

    iget-boolean v1, v1, LX/19j;->q:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 531884
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 531885
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v2, ".mpd"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 531886
    new-instance v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p3}, LX/379;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideo;->aA()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 531887
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_2

    :cond_6
    move v0, v1

    .line 531888
    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    .line 531856
    iget-object v0, p0, LX/3DB;->c:LX/1Lr;

    .line 531857
    iget-object v1, v0, LX/1Lr;->g:LX/0ad;

    sget-short v2, LX/0ws;->el:S

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 531858
    if-eqz v0, :cond_1

    .line 531859
    iget-object v0, p0, LX/3DB;->d:LX/3DC;

    sget-object v1, LX/379;->NOTIFICATION:LX/379;

    invoke-virtual {v0, p1, v1}, LX/3DC;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/379;)Z

    .line 531860
    :cond_0
    return-void

    .line 531861
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 531862
    sget-object v1, LX/379;->NOTIFICATION:LX/379;

    invoke-direct {p0, p1, v0, v1}, LX/3DB;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/util/List;LX/379;)V

    .line 531863
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 531864
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    .line 531865
    iget-object v2, p0, LX/3DB;->a:LX/19s;

    invoke-virtual {v2, v0}, LX/19s;->a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)J

    goto :goto_0
.end method
