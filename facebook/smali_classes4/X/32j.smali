.class public LX/32j;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/32l;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/32m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 490929
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/32j;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/32m;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 490926
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 490927
    iput-object p1, p0, LX/32j;->b:LX/0Ot;

    .line 490928
    return-void
.end method

.method public static a(LX/0QB;)LX/32j;
    .locals 4

    .prologue
    .line 490915
    const-class v1, LX/32j;

    monitor-enter v1

    .line 490916
    :try_start_0
    sget-object v0, LX/32j;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 490917
    sput-object v2, LX/32j;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 490918
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490919
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 490920
    new-instance v3, LX/32j;

    const/16 p0, 0x15f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/32j;-><init>(LX/0Ot;)V

    .line 490921
    move-object v0, v3

    .line 490922
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 490923
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/32j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490924
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 490925
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 490913
    invoke-static {}, LX/1dS;->b()V

    .line 490914
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 490898
    iget-object v0, p0, LX/32j;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 490899
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 490900
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0a045d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 490901
    new-instance v2, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    const v3, 0x7f021af6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/16 p0, 0x3e8

    invoke-direct {v2, v3, p0}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 490902
    new-instance v3, LX/1Uo;

    invoke-direct {v3, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 490903
    iput-object v1, v3, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 490904
    move-object v1, v3

    .line 490905
    const v3, 0x7f020aa6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 490906
    iput-object v0, v1, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    .line 490907
    move-object v0, v1

    .line 490908
    iput-object v2, v0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 490909
    move-object v0, v0

    .line 490910
    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 490911
    new-instance v1, Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-direct {v1, p1, v0}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;LX/1af;)V

    move-object v0, v1

    .line 490912
    return-object v0
.end method

.method public final c(LX/1De;)LX/32l;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 490890
    new-instance v1, LX/32k;

    invoke-direct {v1, p0}, LX/32k;-><init>(LX/32j;)V

    .line 490891
    sget-object v2, LX/32j;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/32l;

    .line 490892
    if-nez v2, :cond_0

    .line 490893
    new-instance v2, LX/32l;

    invoke-direct {v2}, LX/32l;-><init>()V

    .line 490894
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/32l;->a$redex0(LX/32l;LX/1De;IILX/32k;)V

    .line 490895
    move-object v1, v2

    .line 490896
    move-object v0, v1

    .line 490897
    return-object v0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 490879
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 490885
    check-cast p3, LX/32k;

    .line 490886
    iget-object v0, p0, LX/32j;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/drawee/view/GenericDraweeView;

    iget-object v0, p3, LX/32k;->a:LX/1aZ;

    iget-object v1, p3, LX/32k;->b:Landroid/graphics/PointF;

    .line 490887
    invoke-virtual {p2}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object p0

    check-cast p0, LX/1af;

    invoke-virtual {p0, v1}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 490888
    invoke-virtual {p2, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 490889
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 490882
    iget-object v0, p0, LX/32j;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/drawee/view/GenericDraweeView;

    .line 490883
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 490884
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 490881
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 490880
    const/16 v0, 0xf

    return v0
.end method
