.class public final LX/2ro;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Lcom/facebook/ipc/composer/intent/SharePreview;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 472117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472118
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;)V
    .locals 1

    .prologue
    .line 472119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472120
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->dataFailuresFatal:Z

    iput-boolean v0, p0, LX/2ro;->c:Z

    .line 472121
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->insightsPlatformRef:Ljava/lang/String;

    iput-object v0, p0, LX/2ro;->d:Ljava/lang/String;

    .line 472122
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->hashtag:Ljava/lang/String;

    iput-object v0, p0, LX/2ro;->e:Ljava/lang/String;

    .line 472123
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionJsonForRobotext:Ljava/lang/String;

    iput-object v0, p0, LX/2ro;->a:Ljava/lang/String;

    .line 472124
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionType:Ljava/lang/String;

    iput-object v0, p0, LX/2ro;->b:Ljava/lang/String;

    .line 472125
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->nameForShareLink:Ljava/lang/String;

    iput-object v0, p0, LX/2ro;->f:Ljava/lang/String;

    .line 472126
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->captionForShareLink:Ljava/lang/String;

    iput-object v0, p0, LX/2ro;->g:Ljava/lang/String;

    .line 472127
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->pictureForShareLink:Ljava/lang/String;

    iput-object v0, p0, LX/2ro;->h:Ljava/lang/String;

    .line 472128
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->descriptionForShareLink:Ljava/lang/String;

    iput-object v0, p0, LX/2ro;->i:Ljava/lang/String;

    .line 472129
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->platformSharePreview:Lcom/facebook/ipc/composer/intent/SharePreview;

    iput-object v0, p0, LX/2ro;->j:Lcom/facebook/ipc/composer/intent/SharePreview;

    .line 472130
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;
    .locals 2

    .prologue
    .line 472131
    new-instance v0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;-><init>(LX/2ro;)V

    return-object v0
.end method
