.class public LX/2bo;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 439710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 439711
    const/4 v15, 0x0

    .line 439712
    const/4 v14, 0x0

    .line 439713
    const/4 v13, 0x0

    .line 439714
    const/4 v12, 0x0

    .line 439715
    const/4 v11, 0x0

    .line 439716
    const/4 v10, 0x0

    .line 439717
    const/4 v9, 0x0

    .line 439718
    const/4 v8, 0x0

    .line 439719
    const/4 v7, 0x0

    .line 439720
    const/4 v6, 0x0

    .line 439721
    const/4 v5, 0x0

    .line 439722
    const/4 v4, 0x0

    .line 439723
    const/4 v3, 0x0

    .line 439724
    const/4 v2, 0x0

    .line 439725
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 439726
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 439727
    const/4 v2, 0x0

    .line 439728
    :goto_0
    return v2

    .line 439729
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 439730
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_d

    .line 439731
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 439732
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 439733
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 439734
    const-string v17, "can_viewer_edit"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 439735
    const/4 v3, 0x1

    .line 439736
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto :goto_1

    .line 439737
    :cond_2
    const-string v17, "description"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 439738
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 439739
    :cond_3
    const-string v17, "education_info"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 439740
    invoke-static/range {p0 .. p1}, LX/2bS;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 439741
    :cond_4
    const-string v17, "extended_description"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 439742
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 439743
    :cond_5
    const-string v17, "icon"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 439744
    invoke-static/range {p0 .. p1}, LX/2bm;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 439745
    :cond_6
    const-string v17, "icon_image"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 439746
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 439747
    :cond_7
    const-string v17, "label"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 439748
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 439749
    :cond_8
    const-string v17, "legacy_graph_api_privacy_json"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 439750
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 439751
    :cond_9
    const-string v17, "privacy_options"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 439752
    invoke-static/range {p0 .. p1}, LX/2bW;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 439753
    :cond_a
    const-string v17, "selectedPrivacyOption"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 439754
    invoke-static/range {p0 .. p1}, LX/2bW;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 439755
    :cond_b
    const-string v17, "show_tag_expansion_options"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 439756
    const/4 v2, 0x1

    .line 439757
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v5

    goto/16 :goto_1

    .line 439758
    :cond_c
    const-string v17, "type"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 439759
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 439760
    :cond_d
    const/16 v16, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 439761
    if-eqz v3, :cond_e

    .line 439762
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->a(IZ)V

    .line 439763
    :cond_e
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 439764
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 439765
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 439766
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 439767
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 439768
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 439769
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 439770
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 439771
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 439772
    if-eqz v2, :cond_f

    .line 439773
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->a(IZ)V

    .line 439774
    :cond_f
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 439775
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 439776
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 439777
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 439778
    if-eqz v0, :cond_0

    .line 439779
    const-string v1, "can_viewer_edit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439780
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 439781
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 439782
    if-eqz v0, :cond_1

    .line 439783
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439784
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 439785
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 439786
    if-eqz v0, :cond_2

    .line 439787
    const-string v1, "education_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439788
    invoke-static {p0, v0, p2, p3}, LX/2bS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 439789
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 439790
    if-eqz v0, :cond_3

    .line 439791
    const-string v1, "extended_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439792
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 439793
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 439794
    if-eqz v0, :cond_4

    .line 439795
    const-string v1, "icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439796
    invoke-static {p0, v0, p2}, LX/2bm;->a(LX/15i;ILX/0nX;)V

    .line 439797
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 439798
    if-eqz v0, :cond_5

    .line 439799
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439800
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 439801
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 439802
    if-eqz v0, :cond_6

    .line 439803
    const-string v1, "label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439804
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 439805
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 439806
    if-eqz v0, :cond_7

    .line 439807
    const-string v1, "legacy_graph_api_privacy_json"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439808
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 439809
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 439810
    if-eqz v0, :cond_8

    .line 439811
    const-string v1, "privacy_options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439812
    invoke-static {p0, v0, p2, p3}, LX/2bW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 439813
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 439814
    if-eqz v0, :cond_9

    .line 439815
    const-string v1, "selectedPrivacyOption"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439816
    invoke-static {p0, v0, p2, p3}, LX/2bW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 439817
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 439818
    if-eqz v0, :cond_a

    .line 439819
    const-string v1, "show_tag_expansion_options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439820
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 439821
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 439822
    if-eqz v0, :cond_b

    .line 439823
    const-string v1, "type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439824
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 439825
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 439826
    return-void
.end method
