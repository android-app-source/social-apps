.class public abstract LX/37v;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/381;

.field public final c:LX/38C;

.field public d:LX/37o;

.field public e:LX/38A;

.field public f:Z

.field public g:LX/382;

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/381;)V
    .locals 3

    .prologue
    .line 502724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 502725
    new-instance v0, LX/38C;

    invoke-direct {v0, p0}, LX/38C;-><init>(LX/37v;)V

    iput-object v0, p0, LX/37v;->c:LX/38C;

    .line 502726
    if-nez p1, :cond_0

    .line 502727
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502728
    :cond_0
    iput-object p1, p0, LX/37v;->a:Landroid/content/Context;

    .line 502729
    if-nez p2, :cond_1

    .line 502730
    new-instance v0, LX/381;

    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, LX/381;-><init>(Landroid/content/ComponentName;)V

    iput-object v0, p0, LX/37v;->b:LX/381;

    .line 502731
    :goto_0
    return-void

    .line 502732
    :cond_1
    iput-object p2, p0, LX/37v;->b:LX/381;

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)LX/389;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 502733
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/37o;)V
    .locals 0
    .param p1    # LX/37o;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 502734
    invoke-static {}, LX/37i;->b()V

    .line 502735
    iput-object p1, p0, LX/37v;->d:LX/37o;

    .line 502736
    return-void
.end method

.method public final a(LX/382;)V
    .locals 2
    .param p1    # LX/382;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 502737
    invoke-static {}, LX/37i;->b()V

    .line 502738
    iget-object v0, p0, LX/37v;->g:LX/382;

    if-eq v0, p1, :cond_0

    .line 502739
    iput-object p1, p0, LX/37v;->g:LX/382;

    .line 502740
    iget-boolean v0, p0, LX/37v;->h:Z

    if-nez v0, :cond_0

    .line 502741
    iput-boolean v1, p0, LX/37v;->h:Z

    .line 502742
    iget-object v0, p0, LX/37v;->c:LX/38C;

    invoke-virtual {v0, v1}, LX/38C;->sendEmptyMessage(I)Z

    .line 502743
    :cond_0
    return-void
.end method

.method public final a(LX/38A;)V
    .locals 2

    .prologue
    .line 502744
    invoke-static {}, LX/37i;->b()V

    .line 502745
    iget-object v0, p0, LX/37v;->e:LX/38A;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, LX/37v;->e:LX/38A;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/37v;->e:LX/38A;

    invoke-virtual {v0, p1}, LX/38A;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502746
    :cond_0
    :goto_0
    return-void

    .line 502747
    :cond_1
    iput-object p1, p0, LX/37v;->e:LX/38A;

    .line 502748
    iget-boolean v0, p0, LX/37v;->f:Z

    if-nez v0, :cond_0

    .line 502749
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/37v;->f:Z

    .line 502750
    iget-object v0, p0, LX/37v;->c:LX/38C;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/38C;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public b(LX/38A;)V
    .locals 0
    .param p1    # LX/38A;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 502751
    return-void
.end method
