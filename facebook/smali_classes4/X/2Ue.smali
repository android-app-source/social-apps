.class public final enum LX/2Ue;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Ue;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Ue;

.field public static final enum ACCURATE:LX/2Ue;

.field public static final enum SKEWED:LX/2Ue;

.field public static final enum UNKNOWN:LX/2Ue;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 416440
    new-instance v0, LX/2Ue;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/2Ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ue;->UNKNOWN:LX/2Ue;

    .line 416441
    new-instance v0, LX/2Ue;

    const-string v1, "ACCURATE"

    invoke-direct {v0, v1, v3}, LX/2Ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ue;->ACCURATE:LX/2Ue;

    .line 416442
    new-instance v0, LX/2Ue;

    const-string v1, "SKEWED"

    invoke-direct {v0, v1, v4}, LX/2Ue;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ue;->SKEWED:LX/2Ue;

    .line 416443
    const/4 v0, 0x3

    new-array v0, v0, [LX/2Ue;

    sget-object v1, LX/2Ue;->UNKNOWN:LX/2Ue;

    aput-object v1, v0, v2

    sget-object v1, LX/2Ue;->ACCURATE:LX/2Ue;

    aput-object v1, v0, v3

    sget-object v1, LX/2Ue;->SKEWED:LX/2Ue;

    aput-object v1, v0, v4

    sput-object v0, LX/2Ue;->$VALUES:[LX/2Ue;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 416439
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Ue;
    .locals 1

    .prologue
    .line 416444
    const-class v0, LX/2Ue;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Ue;

    return-object v0
.end method

.method public static values()[LX/2Ue;
    .locals 1

    .prologue
    .line 416438
    sget-object v0, LX/2Ue;->$VALUES:[LX/2Ue;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Ue;

    return-object v0
.end method
