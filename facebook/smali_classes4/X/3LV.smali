.class public LX/3LV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0W9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Z

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 550651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550652
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 550653
    iput-object v0, p0, LX/3LV;->a:LX/0Ot;

    .line 550654
    return-void
.end method

.method private static a(LX/3LV;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Integer;)V
    .locals 5
    .param p5    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 550655
    iget-boolean v0, p0, LX/3LV;->d:Z

    if-nez v0, :cond_0

    .line 550656
    iget-object v0, p0, LX/3LV;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "MessengerSearchFunnelLoggerlogClickedWithoutStarting"

    const-string v2, "Logging clicked search result item without being in a session!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 550657
    :cond_0
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "total_search_attempts"

    iget v2, p0, LX/3LV;->e:I

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v0

    const-string v1, "section_name"

    invoke-virtual {v0, v1, p4}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    .line 550658
    if-eqz p1, :cond_1

    .line 550659
    const-string v0, "result_type"

    invoke-virtual {v1, v0, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v2, "result_index"

    invoke-virtual {v0, v2, p5}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v0

    const-string v2, "result_id"

    invoke-virtual {v0, v2, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 550660
    :cond_1
    if-eqz p6, :cond_2

    .line 550661
    const-string v0, "num_participants"

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    .line 550662
    :cond_2
    if-eqz p3, :cond_3

    const-string v0, "search_session_picked_item"

    .line 550663
    :goto_0
    iget-object v2, p0, LX/3LV;->b:LX/0if;

    sget-object v3, LX/0ig;->R:LX/0ih;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 550664
    const/4 v0, 0x0

    iput v0, p0, LX/3LV;->e:I

    .line 550665
    return-void

    .line 550666
    :cond_3
    const-string v0, "search_session_unpicked_item"

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;ZILjava/lang/String;Ljava/lang/Integer;)V
    .locals 7
    .param p5    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 550667
    iget-object v0, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v0}, LX/5e9;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move v3, p2

    move-object v4, p4

    move v5, p3

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/3LV;->a(LX/3LV;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Integer;)V

    .line 550668
    return-void
.end method

.method public final a(Lcom/facebook/user/model/User;ZILjava/lang/String;)V
    .locals 7

    .prologue
    .line 550669
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->A:Z

    move v0, v0

    .line 550670
    if-nez v0, :cond_0

    .line 550671
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->T:Z

    move v0, v0

    .line 550672
    if-eqz v0, :cond_1

    .line 550673
    :cond_0
    const-string v0, "bot"

    .line 550674
    :goto_0
    move-object v1, v0

    .line 550675
    iget-object v0, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v0

    .line 550676
    const/4 v6, 0x0

    move-object v0, p0

    move v3, p2

    move-object v4, p4

    move v5, p3

    invoke-static/range {v0 .. v6}, LX/3LV;->a(LX/3LV;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Integer;)V

    .line 550677
    return-void

    .line 550678
    :cond_1
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->t:Z

    move v0, v0

    .line 550679
    if-eqz v0, :cond_2

    .line 550680
    const-string v0, "commerce_page"

    goto :goto_0

    .line 550681
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 550682
    const-string v0, "sms"

    goto :goto_0

    .line 550683
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->ax()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 550684
    const-string v0, "phone_contact"

    goto :goto_0

    .line 550685
    :cond_4
    iget-object v0, p1, Lcom/facebook/user/model/User;->r:Ljava/lang/String;

    move-object v0, v0

    .line 550686
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 550687
    iget-object v0, p1, Lcom/facebook/user/model/User;->r:Ljava/lang/String;

    move-object v0, v0

    .line 550688
    goto :goto_0

    .line 550689
    :cond_5
    const-string v0, "user"

    goto :goto_0
.end method
