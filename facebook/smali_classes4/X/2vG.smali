.class public LX/2vG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2yv;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 477635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 477650
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;FF)V
    .locals 0

    .prologue
    .line 477649
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;I)V
    .locals 0

    .prologue
    .line 477647
    invoke-static {p1, p2}, LX/3qm;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 477648
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;IIII)V
    .locals 0

    .prologue
    .line 477646
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 477644
    invoke-static {p1, p2}, LX/3qm;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 477645
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
    .locals 0

    .prologue
    .line 477642
    invoke-static {p1, p2}, LX/3qm;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 477643
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 0

    .prologue
    .line 477641
    return-void
.end method

.method public b(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 477640
    const/4 v0, 0x0

    return v0
.end method

.method public c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 477636
    instance-of v0, p1, LX/3qq;

    if-nez v0, :cond_0

    .line 477637
    new-instance v0, LX/3qq;

    invoke-direct {v0, p1}, LX/3qq;-><init>(Landroid/graphics/drawable/Drawable;)V

    move-object p1, v0

    .line 477638
    :cond_0
    move-object v0, p1

    .line 477639
    return-object v0
.end method
