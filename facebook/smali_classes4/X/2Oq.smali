.class public LX/2Oq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Z

.field private static volatile m:LX/2Oq;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Landroid/os/Handler;

.field private final d:LX/2Or;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/2Os;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/3Fd;

.field private final j:Ljava/lang/Object;

.field public k:Landroid/database/ContentObserver;

.field private l:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 403307
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/2Oq;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;LX/2Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/2Os;LX/0Or;LX/3Fd;)V
    .locals 1
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Handler;",
            "LX/2Or;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/2Os;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Fd;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 403295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403296
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/2Oq;->j:Ljava/lang/Object;

    .line 403297
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2Oq;->l:LX/03R;

    .line 403298
    iput-object p1, p0, LX/2Oq;->b:Landroid/content/Context;

    .line 403299
    iput-object p2, p0, LX/2Oq;->c:Landroid/os/Handler;

    .line 403300
    iput-object p3, p0, LX/2Oq;->d:LX/2Or;

    .line 403301
    iput-object p4, p0, LX/2Oq;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 403302
    iput-object p5, p0, LX/2Oq;->f:LX/0Ot;

    .line 403303
    iput-object p6, p0, LX/2Oq;->g:LX/2Os;

    .line 403304
    iput-object p7, p0, LX/2Oq;->h:LX/0Or;

    .line 403305
    iput-object p8, p0, LX/2Oq;->i:LX/3Fd;

    .line 403306
    return-void
.end method

.method public static a(LX/0QB;)LX/2Oq;
    .locals 12

    .prologue
    .line 403282
    sget-object v0, LX/2Oq;->m:LX/2Oq;

    if-nez v0, :cond_1

    .line 403283
    const-class v1, LX/2Oq;

    monitor-enter v1

    .line 403284
    :try_start_0
    sget-object v0, LX/2Oq;->m:LX/2Oq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 403285
    if-eqz v2, :cond_0

    .line 403286
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 403287
    new-instance v3, LX/2Oq;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-static {v0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v6

    check-cast v6, LX/2Or;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v8, 0x1ce

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/2Os;->a(LX/0QB;)LX/2Os;

    move-result-object v9

    check-cast v9, LX/2Os;

    const/16 v10, 0x15e7

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v0}, LX/3Fd;->a(LX/0QB;)LX/3Fd;

    move-result-object v11

    check-cast v11, LX/3Fd;

    invoke-direct/range {v3 .. v11}, LX/2Oq;-><init>(Landroid/content/Context;Landroid/os/Handler;LX/2Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/2Os;LX/0Or;LX/3Fd;)V

    .line 403288
    move-object v0, v3

    .line 403289
    sput-object v0, LX/2Oq;->m:LX/2Oq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403290
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 403291
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 403292
    :cond_1
    sget-object v0, LX/2Oq;->m:LX/2Oq;

    return-object v0

    .line 403293
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 403294
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static g(LX/2Oq;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 403281
    sget-boolean v1, LX/2Oq;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Oq;->d:LX/2Or;

    invoke-virtual {v1}, LX/2Or;->d()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2Oq;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->c:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static h(LX/2Oq;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 403266
    iget-object v1, p0, LX/2Oq;->g:LX/2Os;

    invoke-virtual {v1}, LX/2Os;->c()Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 403267
    iget-object v1, p0, LX/2Oq;->g:LX/2Os;

    invoke-virtual {v1}, LX/2Os;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_3

    iget-object v1, p0, LX/2Oq;->d:LX/2Or;

    invoke-virtual {v1}, LX/2Or;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 403268
    iget-object v1, p0, LX/2Oq;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->ab:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 403269
    iget-object v1, p0, LX/2Oq;->g:LX/2Os;

    invoke-virtual {v1}, LX/2Os;->a()Ljava/util/List;

    move-result-object v1

    .line 403270
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 403271
    invoke-static {p0}, LX/2Oq;->g(LX/2Oq;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, LX/2Oq;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 403272
    :cond_0
    const-string v2, ""

    .line 403273
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;

    .line 403274
    iget-object v5, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 403275
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403276
    iget-object v1, v1, Lcom/facebook/messaging/accountswitch/model/MessengerAccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403277
    const-string v1, " "

    :goto_1
    move-object v2, v1

    .line 403278
    goto :goto_0

    .line 403279
    :cond_1
    iget-object v1, p0, LX/2Oq;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/3Kr;->ab:LX/0Tn;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 403280
    :cond_2
    :goto_2
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    move-object v1, v2

    goto :goto_1
.end method

.method public static i(LX/2Oq;)Z
    .locals 2

    .prologue
    .line 403308
    iget-object v1, p0, LX/2Oq;->i:LX/3Fd;

    iget-object v0, p0, LX/2Oq;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/3Fd;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 403256
    sget-boolean v0, LX/2Oq;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Oq;->k:Landroid/database/ContentObserver;

    if-eqz v0, :cond_1

    .line 403257
    :cond_0
    :goto_0
    return-void

    .line 403258
    :cond_1
    iget-object v1, p0, LX/2Oq;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 403259
    :try_start_0
    iget-object v0, p0, LX/2Oq;->k:Landroid/database/ContentObserver;

    if-eqz v0, :cond_2

    .line 403260
    monitor-exit v1

    goto :goto_0

    .line 403261
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 403262
    :cond_2
    :try_start_1
    new-instance v0, LX/3Ks;

    iget-object v2, p0, LX/2Oq;->c:Landroid/os/Handler;

    invoke-direct {v0, p0, v2}, LX/3Ks;-><init>(LX/2Oq;Landroid/os/Handler;)V

    move-object v0, v0

    .line 403263
    iput-object v0, p0, LX/2Oq;->k:Landroid/database/ContentObserver;

    .line 403264
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403265
    iget-object v0, p0, LX/2Oq;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/messaging/sms/abtest/SmsIntegrationState$2;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/sms/abtest/SmsIntegrationState$2;-><init>(LX/2Oq;)V

    const v2, 0x46542f5d

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 403248
    iget-object v2, p0, LX/2Oq;->d:LX/2Or;

    .line 403249
    iget-object v3, v2, LX/2Or;->a:LX/0Uh;

    const/16 v4, 0x1d5

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v2, v3

    .line 403250
    if-eqz v2, :cond_1

    .line 403251
    :cond_0
    :goto_0
    return v0

    .line 403252
    :cond_1
    invoke-static {p0}, LX/2Oq;->h(LX/2Oq;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 403253
    invoke-static {p0}, LX/2Oq;->i(LX/2Oq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 403254
    goto :goto_0

    .line 403255
    :cond_2
    invoke-static {p0}, LX/2Oq;->g(LX/2Oq;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, LX/2Oq;->b()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final b()Z
    .locals 6

    .prologue
    .line 403230
    invoke-direct {p0}, LX/2Oq;->k()V

    .line 403231
    iget-object v0, p0, LX/2Oq;->l:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBooleanObject()Ljava/lang/Boolean;

    move-result-object v0

    .line 403232
    if-eqz v0, :cond_0

    .line 403233
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 403234
    :goto_0
    return v0

    .line 403235
    :cond_0
    iget-object v1, p0, LX/2Oq;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 403236
    :try_start_0
    iget-object v0, p0, LX/2Oq;->l:LX/03R;

    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v0, v2, :cond_2

    .line 403237
    const/4 v0, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403238
    :try_start_1
    sget-boolean v2, LX/2Oq;->a:Z

    if-nez v2, :cond_4
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403239
    :cond_1
    :goto_1
    :try_start_2
    move v0, v0

    .line 403240
    if-eqz v0, :cond_3

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_2
    iput-object v0, p0, LX/2Oq;->l:LX/03R;

    .line 403241
    :cond_2
    iget-object v0, p0, LX/2Oq;->l:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 403242
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 403243
    :cond_3
    :try_start_3
    sget-object v0, LX/03R;->NO:LX/03R;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 403244
    :cond_4
    :try_start_4
    iget-object v2, p0, LX/2Oq;->b:Landroid/content/Context;

    invoke-static {v2}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 403245
    if-eqz v2, :cond_1

    iget-object v3, p0, LX/2Oq;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    .line 403246
    :catch_0
    move-exception v2

    .line 403247
    const-string v3, "SmsIntegrationState"

    const-string v4, "Exception in detecting sms default app"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final c()LX/3Kt;
    .locals 1

    .prologue
    .line 403224
    invoke-virtual {p0}, LX/2Oq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403225
    sget-object v0, LX/3Kt;->FULL:LX/3Kt;

    .line 403226
    :goto_0
    return-object v0

    .line 403227
    :cond_0
    invoke-static {p0}, LX/2Oq;->g(LX/2Oq;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 403228
    sget-object v0, LX/3Kt;->READONLY:LX/3Kt;

    goto :goto_0

    .line 403229
    :cond_1
    sget-object v0, LX/3Kt;->NONE:LX/3Kt;

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 403218
    iget-object v1, p0, LX/2Oq;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 403219
    :try_start_0
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/2Oq;->l:LX/03R;

    .line 403220
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403221
    iget-object v0, p0, LX/2Oq;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/messaging/sms/abtest/SmsIntegrationState$1;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/sms/abtest/SmsIntegrationState$1;-><init>(LX/2Oq;)V

    const v2, -0x2aad178c

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 403222
    return-void

    .line 403223
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
