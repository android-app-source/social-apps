.class public LX/2bd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 439330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 439331
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 439332
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 439333
    :goto_0
    return v1

    .line 439334
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_5

    .line 439335
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 439336
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 439337
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 439338
    const-string v10, "count"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 439339
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v2

    goto :goto_1

    .line 439340
    :cond_1
    const-string v10, "new_likers_count"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 439341
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 439342
    :cond_2
    const-string v10, "nodes"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 439343
    invoke-static {p0, p1}, LX/2bO;->b(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 439344
    :cond_3
    const-string v10, "global_likers_count"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 439345
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 439346
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 439347
    :cond_5
    const/4 v9, 0x4

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 439348
    if-eqz v4, :cond_6

    .line 439349
    invoke-virtual {p1, v1, v8, v1}, LX/186;->a(III)V

    .line 439350
    :cond_6
    if-eqz v3, :cond_7

    .line 439351
    invoke-virtual {p1, v2, v7, v1}, LX/186;->a(III)V

    .line 439352
    :cond_7
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 439353
    if-eqz v0, :cond_8

    .line 439354
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 439355
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 439356
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 439357
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 439358
    if-eqz v0, :cond_0

    .line 439359
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439360
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 439361
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 439362
    if-eqz v0, :cond_1

    .line 439363
    const-string v1, "new_likers_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439364
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 439365
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 439366
    if-eqz v0, :cond_2

    .line 439367
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439368
    invoke-static {p0, v0, p2, p3}, LX/2bO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 439369
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 439370
    if-eqz v0, :cond_3

    .line 439371
    const-string v1, "global_likers_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 439372
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 439373
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 439374
    return-void
.end method
