.class public LX/2X9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2XA;


# instance fields
.field private final a:LX/2VM;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ae;


# direct methods
.method public constructor <init>(LX/2VM;LX/0Or;LX/0ae;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2VM;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0ae;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 419523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419524
    iput-object p1, p0, LX/2X9;->a:LX/2VM;

    .line 419525
    iput-object p2, p0, LX/2X9;->b:LX/0Or;

    .line 419526
    iput-object p3, p0, LX/2X9;->c:LX/0ae;

    .line 419527
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 419528
    iget-object v0, p0, LX/2X9;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 419529
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 419530
    :goto_0
    iget-object v1, p0, LX/2X9;->c:LX/0ae;

    invoke-interface {v1, v0}, LX/0ae;->a(Ljava/lang/String;)V

    .line 419531
    return-void

    .line 419532
    :cond_0
    invoke-static {v0}, LX/03l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()LX/2ZE;
    .locals 2

    .prologue
    .line 419533
    iget-object v0, p0, LX/2X9;->a:LX/2VM;

    const/4 v1, 0x0

    .line 419534
    new-instance p0, LX/2ZD;

    invoke-direct {p0, v0, v1}, LX/2ZD;-><init>(LX/2VM;Z)V

    move-object v0, p0

    .line 419535
    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 419536
    iget-object v0, p0, LX/2X9;->c:LX/0ae;

    invoke-interface {v0}, LX/0ae;->b()V

    .line 419537
    return-void
.end method

.method public final dq_()Z
    .locals 1

    .prologue
    .line 419538
    iget-object v0, p0, LX/2X9;->c:LX/0ae;

    invoke-interface {v0}, LX/0ae;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 419539
    iget-object v0, p0, LX/2X9;->a:LX/2VM;

    invoke-virtual {v0}, LX/2VM;->a()V

    .line 419540
    const/4 v0, 0x1

    .line 419541
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()LX/2ZF;
    .locals 1

    .prologue
    .line 419542
    sget-object v0, LX/2ZF;->QE:LX/2ZF;

    return-object v0
.end method
