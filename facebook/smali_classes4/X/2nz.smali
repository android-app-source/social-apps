.class public final LX/2nz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0gM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/0jT;",
        ">",
        "Ljava/lang/Object;",
        "LX/0gM",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:LX/1se;

.field public b:LX/0gV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0gV",
            "<TT;>;"
        }
    .end annotation
.end field

.field public c:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<TT;>;"
        }
    .end annotation
.end field

.field public d:LX/2o0;

.field public e:LX/0lp;

.field public f:LX/0lF;

.field public g:Z


# direct methods
.method public constructor <init>(LX/1se;LX/0gV;LX/0TF;LX/2o0;LX/0lp;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1se;",
            "LX/0gV",
            "<TT;>;",
            "LX/0TF",
            "<TT;>;",
            "LX/2o0;",
            "LX/0lp;",
            ")V"
        }
    .end annotation

    .prologue
    .line 465494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465495
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2nz;->g:Z

    .line 465496
    iput-object p1, p0, LX/2nz;->a:LX/1se;

    .line 465497
    iput-object p2, p0, LX/2nz;->b:LX/0gV;

    .line 465498
    iput-object p3, p0, LX/2nz;->c:LX/0TF;

    .line 465499
    iput-object p4, p0, LX/2nz;->d:LX/2o0;

    .line 465500
    iput-object p5, p0, LX/2nz;->e:LX/0lp;

    .line 465501
    return-void
.end method


# virtual methods
.method public final c()LX/0gV;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0gV",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 465502
    iget-object v0, p0, LX/2nz;->b:LX/0gV;

    return-object v0
.end method

.method public final d()LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 465503
    iget-object v0, p0, LX/2nz;->c:LX/0TF;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465504
    const-string v0, "mqtt"

    return-object v0
.end method

.method public final declared-synchronized f()LX/0lF;
    .locals 5

    .prologue
    .line 465505
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2nz;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 465506
    const/4 v0, 0x0

    .line 465507
    :goto_0
    monitor-exit p0

    return-object v0

    .line 465508
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2nz;->f:LX/0lF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 465509
    :try_start_2
    iget-object v0, p0, LX/2nz;->b:LX/0gV;

    .line 465510
    iget-object v1, v0, LX/0gV;->f:Ljava/lang/String;

    move-object v0, v1

    .line 465511
    iget-object v1, p0, LX/2nz;->b:LX/0gV;

    .line 465512
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 465513
    invoke-virtual {v1}, LX/0w7;->e()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 465514
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    const-string v2, "input"

    .line 465515
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 465516
    iget-object v4, p0, LX/2nz;->e:LX/0lp;

    invoke-virtual {v4, v3}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v4

    .line 465517
    invoke-virtual {v4, v0}, LX/0nX;->a(Ljava/lang/Object;)V

    .line 465518
    invoke-virtual {v4}, LX/0nX;->flush()V

    .line 465519
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 465520
    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    move-object v0, v0

    .line 465521
    iput-object v0, p0, LX/2nz;->f:LX/0lF;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 465522
    :cond_1
    :goto_1
    :try_start_3
    iget-object v0, p0, LX/2nz;->f:LX/0lF;

    goto :goto_0

    .line 465523
    :catch_0
    move-exception v0

    .line 465524
    sget-object v1, LX/1fS;->a:Ljava/lang/Class;

    const-string v2, "Failed to build input query param node. Its value will remain null"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 465525
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2nz;->g:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 465526
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
