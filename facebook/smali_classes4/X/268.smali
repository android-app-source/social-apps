.class public LX/268;
.super LX/0lI;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x3fffd755256ef9c2L


# instance fields
.field public final _elementType:LX/0lJ;


# direct methods
.method public constructor <init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0lJ;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 370727
    invoke-virtual {p2}, LX/0lJ;->hashCode()I

    move-result v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, LX/0lI;-><init>(Ljava/lang/Class;ILjava/lang/Object;Ljava/lang/Object;Z)V

    .line 370728
    iput-object p2, p0, LX/268;->_elementType:LX/0lJ;

    .line 370729
    return-void
.end method


# virtual methods
.method public final a(I)LX/0lJ;
    .locals 1

    .prologue
    .line 370716
    if-nez p1, :cond_0

    iget-object v0, p0, LX/268;->_elementType:LX/0lJ;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic a(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 370730
    invoke-virtual {p0, p1}, LX/268;->e(Ljava/lang/Object;)LX/268;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()LX/0lJ;
    .locals 1

    .prologue
    .line 370731
    invoke-virtual {p0}, LX/268;->w()LX/268;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 370732
    invoke-virtual {p0, p1}, LX/268;->f(Ljava/lang/Object;)LX/268;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 370733
    if-nez p1, :cond_0

    const-string v0, "E"

    .line 370734
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic c(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 370735
    invoke-virtual {p0, p1}, LX/268;->g(Ljava/lang/Object;)LX/268;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/Class;)LX/0lJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 370736
    new-instance v0, LX/268;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/268;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public synthetic d(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 370737
    invoke-virtual {p0, p1}, LX/268;->h(Ljava/lang/Object;)LX/268;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/Class;)LX/0lJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 370738
    iget-object v0, p0, LX/268;->_elementType:LX/0lJ;

    .line 370739
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 370740
    if-ne p1, v0, :cond_0

    .line 370741
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/268;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v5}, LX/268;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public e(Ljava/lang/Object;)LX/268;
    .locals 6

    .prologue
    .line 370742
    new-instance v0, LX/268;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/268;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 370717
    if-ne p1, p0, :cond_1

    .line 370718
    :cond_0
    :goto_0
    return v0

    .line 370719
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 370720
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    .line 370721
    :cond_3
    check-cast p1, LX/268;

    .line 370722
    iget-object v2, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v3, p1, LX/0lJ;->_class:Ljava/lang/Class;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    iget-object v3, p1, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v2, v3}, LX/0lJ;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public f(Ljava/lang/Class;)LX/0lJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 370723
    iget-object v0, p0, LX/268;->_elementType:LX/0lJ;

    .line 370724
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 370725
    if-ne p1, v0, :cond_0

    .line 370726
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/268;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->c(Ljava/lang/Class;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v5}, LX/268;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public f(Ljava/lang/Object;)LX/268;
    .locals 6

    .prologue
    .line 370698
    new-instance v0, LX/268;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->a(Ljava/lang/Object;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v5}, LX/268;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public g(Ljava/lang/Object;)LX/268;
    .locals 6

    .prologue
    .line 370699
    new-instance v0, LX/268;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/268;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public h(Ljava/lang/Object;)LX/268;
    .locals 6

    .prologue
    .line 370700
    new-instance v0, LX/268;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->c(Ljava/lang/Object;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v5}, LX/268;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 370701
    const/4 v0, 0x1

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 370702
    const/4 v0, 0x1

    return v0
.end method

.method public final r()LX/0lJ;
    .locals 1

    .prologue
    .line 370703
    iget-object v0, p0, LX/268;->_elementType:LX/0lJ;

    return-object v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 370704
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 370705
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[collection-like type; class "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", contains "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2

    .prologue
    .line 370706
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 370707
    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370708
    iget-object v1, p0, LX/268;->_elementType:LX/0lJ;

    if-eqz v1, :cond_0

    .line 370709
    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 370710
    iget-object v1, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v1}, LX/0lK;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370711
    const/16 v1, 0x3e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 370712
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public w()LX/268;
    .locals 6

    .prologue
    .line 370713
    iget-boolean v0, p0, LX/0lJ;->_asStatic:Z

    if-eqz v0, :cond_0

    .line 370714
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/268;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/268;->_elementType:LX/0lJ;

    invoke-virtual {v2}, LX/0lJ;->b()LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, LX/268;-><init>(Ljava/lang/Class;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 370715
    const-class v0, Ljava/util/Collection;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method
