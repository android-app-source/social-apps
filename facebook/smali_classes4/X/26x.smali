.class public LX/26x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26y;


# instance fields
.field private final a:LX/0ag;

.field private final b:LX/0ej;

.field private final c:LX/26w;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(LX/0ag;LX/0ej;LX/26w;)V
    .locals 0

    .prologue
    .line 372619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372620
    iput-object p1, p0, LX/26x;->a:LX/0ag;

    .line 372621
    iput-object p2, p0, LX/26x;->b:LX/0ej;

    .line 372622
    iput-object p3, p0, LX/26x;->c:LX/26w;

    .line 372623
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 372624
    iput-object p1, p0, LX/26x;->d:Ljava/lang/String;

    .line 372625
    iput p2, p0, LX/26x;->e:I

    .line 372626
    iget-object v0, p0, LX/26x;->a:LX/0ag;

    invoke-virtual {v0, p1}, LX/0ag;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 372627
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/26x;->f:Z

    .line 372628
    :goto_0
    return-void

    .line 372629
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/26x;->f:Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;IIZ)V
    .locals 6

    .prologue
    .line 372630
    iget-object v0, p0, LX/26x;->a:LX/0ag;

    iget-object v1, p0, LX/26x;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/0ag;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 372631
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 372632
    iget-boolean v0, p0, LX/26x;->f:Z

    if-nez v0, :cond_0

    .line 372633
    iget v0, p0, LX/26x;->e:I

    add-int/lit8 v0, v0, 0x3

    .line 372634
    iget-object v1, p0, LX/26x;->c:LX/26w;

    sget-object v2, LX/0oc;->ASSIGNED:LX/0oc;

    .line 372635
    invoke-static {v1, v2}, LX/26w;->a(LX/26w;LX/0oc;)[I

    move-result-object v3

    .line 372636
    const/4 v4, -0x1

    aput v4, v3, v0

    .line 372637
    :cond_0
    :goto_0
    return-void

    .line 372638
    :cond_1
    iget-object v3, p0, LX/26x;->b:LX/0ej;

    iget-object v4, p0, LX/26x;->c:LX/26w;

    sget-object v5, LX/0oc;->OVERRIDE:LX/0oc;

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/2Wy;->a(IIILX/0ej;LX/26w;LX/0oc;)V

    .line 372639
    iget-object v3, p0, LX/26x;->b:LX/0ej;

    iget-object v4, p0, LX/26x;->c:LX/26w;

    sget-object v5, LX/0oc;->ASSIGNED:LX/0oc;

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/2Wy;->a(IIILX/0ej;LX/26w;LX/0oc;)V

    goto :goto_0
.end method
