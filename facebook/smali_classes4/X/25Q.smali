.class public final LX/25Q;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/25N;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/25O;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 369214
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "color"

    aput-object v2, v0, v1

    sput-object v0, LX/25Q;->b:[Ljava/lang/String;

    .line 369215
    sput v3, LX/25Q;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 369212
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 369213
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/25Q;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/25Q;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/25Q;LX/1De;IILX/25O;)V
    .locals 1

    .prologue
    .line 369208
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 369209
    iput-object p4, p0, LX/25Q;->a:LX/25O;

    .line 369210
    iget-object v0, p0, LX/25Q;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 369211
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 369216
    invoke-super {p0}, LX/1X5;->a()V

    .line 369217
    const/4 v0, 0x0

    iput-object v0, p0, LX/25Q;->a:LX/25O;

    .line 369218
    sget-object v0, LX/25N;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 369219
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/25N;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 369192
    iget-object v1, p0, LX/25Q;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/25Q;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/25Q;->c:I

    if-ge v1, v2, :cond_2

    .line 369193
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 369194
    :goto_0
    sget v2, LX/25Q;->c:I

    if-ge v0, v2, :cond_1

    .line 369195
    iget-object v2, p0, LX/25Q;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 369196
    sget-object v2, LX/25Q;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369197
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 369198
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369199
    :cond_2
    iget-object v0, p0, LX/25Q;->a:LX/25O;

    .line 369200
    invoke-virtual {p0}, LX/25Q;->a()V

    .line 369201
    return-object v0
.end method

.method public final h(I)LX/25Q;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 369205
    iget-object v0, p0, LX/25Q;->a:LX/25O;

    iput p1, v0, LX/25O;->a:I

    .line 369206
    iget-object v0, p0, LX/25Q;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 369207
    return-object p0
.end method

.method public final i(I)LX/25Q;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 369202
    iget-object v0, p0, LX/25Q;->a:LX/25O;

    invoke-virtual {p0, p1}, LX/1Dp;->d(I)I

    move-result v1

    iput v1, v0, LX/25O;->a:I

    .line 369203
    iget-object v0, p0, LX/25Q;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 369204
    return-object p0
.end method
