.class public LX/2ax;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 427832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 427833
    const/4 v14, 0x0

    .line 427834
    const/4 v11, 0x0

    .line 427835
    const-wide/16 v12, 0x0

    .line 427836
    const/4 v10, 0x0

    .line 427837
    const/4 v9, 0x0

    .line 427838
    const/4 v8, 0x0

    .line 427839
    const/4 v7, 0x0

    .line 427840
    const/4 v6, 0x0

    .line 427841
    const/4 v5, 0x0

    .line 427842
    const/4 v4, 0x0

    .line 427843
    const/4 v3, 0x0

    .line 427844
    const/4 v2, 0x0

    .line 427845
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 427846
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 427847
    const/4 v2, 0x0

    .line 427848
    :goto_0
    return v2

    .line 427849
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_9

    .line 427850
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 427851
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 427852
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 427853
    const-string v7, "height"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 427854
    const/4 v2, 0x1

    .line 427855
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v16, v6

    move v6, v2

    goto :goto_1

    .line 427856
    :cond_1
    const-string v7, "name"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 427857
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 427858
    :cond_2
    const-string v7, "scale"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 427859
    const/4 v2, 0x1

    .line 427860
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 427861
    :cond_3
    const-string v7, "uri"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 427862
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 427863
    :cond_4
    const-string v7, "width"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 427864
    const/4 v2, 0x1

    .line 427865
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v9, v2

    move v13, v7

    goto :goto_1

    .line 427866
    :cond_5
    const-string v7, "dimensionless_cache_key"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 427867
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 427868
    :cond_6
    const-string v7, "mime_type"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 427869
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 427870
    :cond_7
    const-string v7, "is_silhouette"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 427871
    const/4 v2, 0x1

    .line 427872
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v8, v2

    move v10, v7

    goto/16 :goto_1

    .line 427873
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 427874
    :cond_9
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 427875
    if-eqz v6, :cond_a

    .line 427876
    const/4 v2, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v6}, LX/186;->a(III)V

    .line 427877
    :cond_a
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 427878
    if-eqz v3, :cond_b

    .line 427879
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 427880
    :cond_b
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 427881
    if-eqz v9, :cond_c

    .line 427882
    const/4 v2, 0x4

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v3}, LX/186;->a(III)V

    .line 427883
    :cond_c
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 427884
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 427885
    if-eqz v8, :cond_d

    .line 427886
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->a(IZ)V

    .line 427887
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v15, v11

    move/from16 v16, v14

    move v11, v7

    move v14, v10

    move v10, v6

    move v6, v5

    move/from16 v18, v8

    move v8, v2

    move/from16 v19, v9

    move v9, v3

    move v3, v4

    move-wide v4, v12

    move/from16 v12, v18

    move/from16 v13, v19

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 427888
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 427889
    invoke-virtual {p0, p1, v3, v3}, LX/15i;->a(III)I

    move-result v0

    .line 427890
    if-eqz v0, :cond_0

    .line 427891
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 427892
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 427893
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 427894
    if-eqz v0, :cond_1

    .line 427895
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 427896
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 427897
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 427898
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 427899
    const-string v2, "scale"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 427900
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 427901
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 427902
    if-eqz v0, :cond_3

    .line 427903
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 427904
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 427905
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 427906
    if-eqz v0, :cond_4

    .line 427907
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 427908
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 427909
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 427910
    if-eqz v0, :cond_5

    .line 427911
    const-string v1, "dimensionless_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 427912
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 427913
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 427914
    if-eqz v0, :cond_6

    .line 427915
    const-string v1, "mime_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 427916
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 427917
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 427918
    if-eqz v0, :cond_7

    .line 427919
    const-string v1, "is_silhouette"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 427920
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 427921
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 427922
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 427923
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 427924
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 427925
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 427926
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 427927
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 427928
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 427929
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 427930
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 427931
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 427932
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v1

    .line 427933
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 427934
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method
