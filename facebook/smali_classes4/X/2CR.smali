.class public LX/2CR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0l6;
.implements LX/2JL;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:Landroid/app/Activity;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:LX/03V;

.field public final f:Landroid/content/res/Resources;

.field public final g:Ljava/lang/Class;

.field public h:LX/2A4;

.field public i:LX/2Cy;

.field private j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/2NO;

.field public n:I

.field public o:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 382891
    const-class v0, LX/2CR;

    sput-object v0, LX/2CR;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/app/Activity;Lcom/facebook/content/SecureContextHelper;LX/03V;Landroid/content/res/Resources;Ljava/lang/Class;LX/2Cy;LX/0Or;LX/0Or;LX/0Or;LX/2NO;LX/0Uh;)V
    .locals 1
    .param p6    # Ljava/lang/Class;
        .annotation build Lcom/facebook/katana/login/LoggedOutActivityClass;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/katana/annotations/IsLoginErrorRegEnabled;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/katana/annotations/IsLoginErrorRegEmailEnabled;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/katana/annotations/IsLoginErrorRegPhoneEnabled;
        .end annotation
    .end param
    .param p12    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Landroid/app/Activity;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/res/Resources;",
            "Ljava/lang/Class;",
            "LX/2Cy;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/2NO;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 382876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382877
    const/4 v0, 0x0

    iput v0, p0, LX/2CR;->n:I

    .line 382878
    iput-object p1, p0, LX/2CR;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 382879
    iput-object p2, p0, LX/2CR;->c:Landroid/app/Activity;

    .line 382880
    iput-object p3, p0, LX/2CR;->d:Lcom/facebook/content/SecureContextHelper;

    .line 382881
    iput-object p4, p0, LX/2CR;->e:LX/03V;

    .line 382882
    iput-object p5, p0, LX/2CR;->f:Landroid/content/res/Resources;

    .line 382883
    iput-object p6, p0, LX/2CR;->g:Ljava/lang/Class;

    .line 382884
    iput-object p7, p0, LX/2CR;->i:LX/2Cy;

    .line 382885
    iput-object p8, p0, LX/2CR;->j:LX/0Or;

    .line 382886
    iput-object p9, p0, LX/2CR;->k:LX/0Or;

    .line 382887
    iput-object p10, p0, LX/2CR;->l:LX/0Or;

    .line 382888
    iput-object p11, p0, LX/2CR;->m:LX/2NO;

    .line 382889
    iput-object p12, p0, LX/2CR;->o:LX/0Uh;

    .line 382890
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/28P;
    .locals 6

    .prologue
    .line 382763
    new-instance v0, LX/28P;

    invoke-direct {v0}, LX/28P;-><init>()V

    .line 382764
    invoke-static {p0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 382765
    :goto_0
    return-object v0

    .line 382766
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_e

    .line 382767
    :try_start_1
    const-string v1, "error_title"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/28P;->f:Ljava/lang/String;

    .line 382768
    const-string v1, "error_message"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/28P;->g:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 382769
    :goto_1
    :try_start_2
    const-string v1, "url"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/28P;->h:Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 382770
    :goto_2
    :try_start_3
    const-string v1, "machine_id"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/28P;->i:Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    .line 382771
    :goto_3
    :try_start_4
    const-string v1, "uid"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v0, LX/28P;->a:J
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    .line 382772
    :goto_4
    :try_start_5
    const-string v1, "auth_token"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/28P;->c:Ljava/lang/String;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    .line 382773
    :goto_5
    :try_start_6
    const-string v1, "conditional_uri"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/28P;->j:Ljava/lang/String;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_6

    .line 382774
    :goto_6
    :try_start_7
    const-string v1, "actual_identifier"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/28P;->k:Ljava/lang/String;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_7

    .line 382775
    :goto_7
    :try_start_8
    const-string v1, "finish_after_loading_url"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    iput-object v1, v0, LX/28P;->l:LX/03R;
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_8

    .line 382776
    :goto_8
    :try_start_9
    const-string v1, "start_internal_webview_from_url"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    iput-object v1, v0, LX/28P;->m:LX/03R;
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_9

    .line 382777
    :goto_9
    :try_start_a
    const-string v1, "positive_button_string"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/28P;->n:Ljava/lang/String;
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_a

    .line 382778
    :goto_a
    :try_start_b
    const-string v1, "negative_button_string"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/28P;->o:Ljava/lang/String;
    :try_end_b
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_b

    .line 382779
    :goto_b
    :try_start_c
    const-string v1, "error_subcode"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, LX/28P;->b:I
    :try_end_c
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_c} :catch_c

    .line 382780
    :goto_c
    :try_start_d
    const-string v1, "login_first_factor"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/28P;->d:Ljava/lang/String;
    :try_end_d
    .catch Lorg/json/JSONException; {:try_start_d .. :try_end_d} :catch_d

    .line 382781
    :goto_d
    :try_start_e
    const-string v1, "support_uri"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/28P;->e:Ljava/lang/String;
    :try_end_e
    .catch Lorg/json/JSONException; {:try_start_e .. :try_end_e} :catch_0

    goto/16 :goto_0

    .line 382782
    :catch_0
    move-exception v1

    .line 382783
    sget-object v2, LX/2CR;->a:Ljava/lang/Class;

    const-string v3, "JSON Exception"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 382784
    :catch_1
    move-exception v1

    .line 382785
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 382786
    :catch_2
    move-exception v1

    .line 382787
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 382788
    :catch_3
    move-exception v1

    .line 382789
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 382790
    :catch_4
    move-exception v1

    .line 382791
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4

    .line 382792
    :catch_5
    move-exception v1

    .line 382793
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    .line 382794
    :catch_6
    move-exception v1

    .line 382795
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_6

    .line 382796
    :catch_7
    move-exception v1

    .line 382797
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 382798
    :catch_8
    move-exception v1

    .line 382799
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 382800
    :catch_9
    move-exception v1

    .line 382801
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_9

    .line 382802
    :catch_a
    move-exception v1

    .line 382803
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_a

    .line 382804
    :catch_b
    move-exception v1

    .line 382805
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_b

    .line 382806
    :catch_c
    move-exception v1

    .line 382807
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_c

    .line 382808
    :catch_d
    move-exception v1

    .line 382809
    sget-object v3, LX/2CR;->a:Ljava/lang/Class;

    const-string v4, "JSON Exception"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_d

    .line 382810
    :catch_e
    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/2CR;
    .locals 14

    .prologue
    .line 382870
    new-instance v1, LX/2CR;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    .line 382871
    const-class v7, Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    move-object v7, v7

    .line 382872
    move-object v7, v7

    .line 382873
    check-cast v7, Ljava/lang/Class;

    invoke-static {p0}, LX/2Cy;->b(LX/0QB;)LX/2Cy;

    move-result-object v8

    check-cast v8, LX/2Cy;

    const/16 v9, 0x329

    invoke-static {p0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x328

    invoke-static {p0, v10}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x32a

    invoke-static {p0, v11}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {p0}, LX/2NO;->b(LX/0QB;)LX/2NO;

    move-result-object v12

    check-cast v12, LX/2NO;

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-direct/range {v1 .. v13}, LX/2CR;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/app/Activity;Lcom/facebook/content/SecureContextHelper;LX/03V;Landroid/content/res/Resources;Ljava/lang/Class;LX/2Cy;LX/0Or;LX/0Or;LX/0Or;LX/2NO;LX/0Uh;)V

    .line 382874
    move-object v0, v1

    .line 382875
    return-object v0
.end method

.method public static a(LX/2CR;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 382858
    new-instance v0, LX/FB3;

    invoke-direct {v0, p0, p3, p6, p7}, LX/FB3;-><init>(LX/2CR;Ljava/lang/String;ZZ)V

    .line 382859
    new-instance v6, LX/FB4;

    invoke-direct {v6, p0}, LX/FB4;-><init>(LX/2CR;)V

    .line 382860
    if-eqz p8, :cond_3

    move-object v4, v0

    :goto_0
    if-eqz p8, :cond_4

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v5, p5

    .line 382861
    iget-object p0, v0, LX/2CR;->h:LX/2A4;

    if-eqz p0, :cond_2

    iget-object p0, v0, LX/2CR;->h:LX/2A4;

    invoke-interface {p0}, LX/2A4;->c()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 382862
    new-instance p0, LX/0ju;

    iget-object p1, v0, LX/2CR;->c:Landroid/app/Activity;

    invoke-direct {p0, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object p0

    const p1, 0x1080027

    invoke-virtual {p0, p1}, LX/0ju;->c(I)LX/0ju;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object p0

    .line 382863
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    if-eqz v4, :cond_0

    .line 382864
    invoke-virtual {p0, v3, v4}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 382865
    :cond_0
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    if-eqz v6, :cond_1

    .line 382866
    invoke-virtual {p0, v5, v6}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 382867
    :cond_1
    invoke-virtual {p0}, LX/0ju;->b()LX/2EJ;

    .line 382868
    :cond_2
    return-void

    :cond_3
    move-object v4, v6

    .line 382869
    goto :goto_0

    :cond_4
    move-object v6, v0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 382854
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 382855
    invoke-virtual {p0, p1, p2}, LX/2CR;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 382856
    :cond_0
    iget-object v0, p0, LX/2CR;->e:LX/03V;

    const-string v1, "LOGIN_ERROR"

    invoke-virtual {v0, v1, p2, p3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 382857
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 382852
    iget-object v0, p0, LX/2CR;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1CA;->n:LX/0Tn;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 382853
    return-void
.end method

.method public final a(LX/28P;)V
    .locals 3

    .prologue
    .line 382848
    iget-object v0, p1, LX/28P;->i:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2CR;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->f:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382849
    iget-object v0, p0, LX/2CR;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/26p;->f:LX/0Tn;

    iget-object v2, p1, LX/28P;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 382850
    :cond_0
    const/16 v0, 0x196

    invoke-virtual {p0, v0, p1}, LX/2CR;->a(ILX/28P;)Z

    .line 382851
    return-void
.end method

.method public final a(LX/2Oo;)V
    .locals 4

    .prologue
    .line 382845
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 382846
    iget-object v0, p0, LX/2CR;->f:Landroid/content/res/Resources;

    const v1, 0x7f0800e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "[%d] %s"

    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, LX/2CR;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 382847
    :cond_0
    return-void
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 3

    .prologue
    .line 382842
    if-eqz p1, :cond_0

    .line 382843
    iget-object v0, p0, LX/2CR;->f:Landroid/content/res/Resources;

    const v1, 0x7f0800e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/2CR;->f:Landroid/content/res/Resources;

    const v2, 0x7f0800e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, LX/2CR;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 382844
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 382839
    iget-object v0, p0, LX/2CR;->h:LX/2A4;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2CR;->h:LX/2A4;

    invoke-interface {v0}, LX/2A4;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382840
    new-instance v0, LX/0ju;

    iget-object v1, p0, LX/2CR;->c:Landroid/app/Activity;

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x1080027

    invoke-virtual {v0, v1}, LX/0ju;->c(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080036

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 382841
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 382836
    if-eqz p1, :cond_0

    .line 382837
    iget-object v0, p0, LX/2CR;->f:Landroid/content/res/Resources;

    const v1, 0x7f0800e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "%s (%s: %s)"

    iget-object v2, p0, LX/2CR;->f:Landroid/content/res/Resources;

    const v3, 0x7f0800e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, LX/2CR;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 382838
    :cond_0
    return-void
.end method

.method public final a(ILX/28P;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 382811
    iget-object v1, p2, LX/28P;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p2, LX/28P;->g:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 382812
    iget-object v1, p2, LX/28P;->h:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 382813
    iget-object v4, p2, LX/28P;->n:Ljava/lang/String;

    .line 382814
    iget-object v5, p2, LX/28P;->o:Ljava/lang/String;

    .line 382815
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 382816
    iget-object v1, p0, LX/2CR;->f:Landroid/content/res/Resources;

    const v2, 0x7f080036

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 382817
    iget-object v1, p0, LX/2CR;->f:Landroid/content/res/Resources;

    const v2, 0x7f083205

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 382818
    :cond_0
    iget-object v1, p2, LX/28P;->f:Ljava/lang/String;

    new-instance v2, Landroid/text/SpannableStringBuilder;

    iget-object v3, p2, LX/28P;->g:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iget-object v3, p2, LX/28P;->h:Ljava/lang/String;

    iget-object v6, p2, LX/28P;->m:LX/03R;

    invoke-virtual {v6, v0}, LX/03R;->asBoolean(Z)Z

    move-result v6

    iget-object v0, p2, LX/28P;->l:LX/03R;

    invoke-virtual {v0, v8}, LX/03R;->asBoolean(Z)Z

    move-result v7

    move-object v0, p0

    invoke-static/range {v0 .. v8}, LX/2CR;->a(LX/2CR;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 382819
    :goto_0
    return v8

    .line 382820
    :cond_1
    iget-object v0, p2, LX/28P;->f:Ljava/lang/String;

    iget-object v1, p2, LX/28P;->g:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/2CR;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 382821
    :cond_2
    sparse-switch p1, :sswitch_data_0

    move v8, v0

    .line 382822
    goto :goto_0

    .line 382823
    :sswitch_0
    const v1, 0x7f0800d1

    .line 382824
    const v0, 0x7f0800d2

    .line 382825
    :goto_1
    iget-object v2, p0, LX/2CR;->f:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2CR;->f:Landroid/content/res/Resources;

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, LX/2CR;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 382826
    :sswitch_1
    const v1, 0x7f0800d7

    .line 382827
    const v0, 0x7f0800d8

    goto :goto_1

    .line 382828
    :sswitch_2
    const v1, 0x7f0800df

    .line 382829
    const v0, 0x7f0800e0

    goto :goto_1

    .line 382830
    :sswitch_3
    const v1, 0x7f0800fa

    .line 382831
    const v0, 0x7f0800fc

    goto :goto_1

    .line 382832
    :sswitch_4
    const v1, 0x7f0800e1

    .line 382833
    const v0, 0x7f0800e2

    goto :goto_1

    .line 382834
    :sswitch_5
    const v1, 0x7f0800e3

    .line 382835
    const v0, 0x7f0800e4

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_5
        0x190 -> :sswitch_0
        0x191 -> :sswitch_1
        0x195 -> :sswitch_2
        0x196 -> :sswitch_3
        0x197 -> :sswitch_4
    .end sparse-switch
.end method
