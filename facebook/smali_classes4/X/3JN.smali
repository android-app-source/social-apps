.class public final LX/3JN;
.super LX/3JO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3JO",
        "<",
        "LX/3Jl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 547675
    invoke-direct {p0}, LX/3JO;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/util/JsonReader;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 547676
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 547677
    new-instance v1, LX/3JP;

    invoke-direct {v1}, LX/3JP;-><init>()V

    .line 547678
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547679
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 547680
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result p0

    sparse-switch p0, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 547681
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 547682
    :sswitch_0
    const-string p0, "group_id"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string p0, "parent_group"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string p0, "animations"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    .line 547683
    :pswitch_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v0

    iput v0, v1, LX/3JP;->a:I

    goto :goto_0

    .line 547684
    :pswitch_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v0

    iput v0, v1, LX/3JP;->b:I

    goto :goto_0

    .line 547685
    :pswitch_2
    sget-object v0, LX/3JQ;->a:LX/3JO;

    invoke-virtual {v0, p1}, LX/3JO;->a(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v1, LX/3JP;->c:Ljava/util/List;

    goto :goto_0

    .line 547686
    :cond_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 547687
    new-instance v0, LX/3Jl;

    iget v2, v1, LX/3JP;->a:I

    iget p0, v1, LX/3JP;->b:I

    iget-object p1, v1, LX/3JP;->c:Ljava/util/List;

    invoke-direct {v0, v2, p0, p1}, LX/3Jl;-><init>(IILjava/util/List;)V

    move-object v0, v0

    .line 547688
    move-object v0, v0

    .line 547689
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x12b8556f -> :sswitch_2
        0x1e2e76db -> :sswitch_0
        0x5325baaa -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
