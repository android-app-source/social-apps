.class public LX/31w;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 488201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488202
    iput-object p1, p0, LX/31w;->a:LX/0ad;

    .line 488203
    return-void
.end method

.method public static a(LX/0QB;)LX/31w;
    .locals 1

    .prologue
    .line 488204
    invoke-static {p0}, LX/31w;->b(LX/0QB;)LX/31w;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/31w;
    .locals 2

    .prologue
    .line 488205
    new-instance v1, LX/31w;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v0}, LX/31w;-><init>(LX/0ad;)V

    .line 488206
    return-object v1
.end method


# virtual methods
.method public final a(LX/2rw;)Z
    .locals 3

    .prologue
    .line 488207
    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    if-eq p1, v0, :cond_3

    const/4 v0, 0x0

    .line 488208
    sget-object v1, LX/2rw;->EVENT:LX/2rw;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, LX/31w;->a:LX/0ad;

    sget-short v2, LX/1EB;->ah:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 488209
    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 488210
    sget-object v1, LX/2rw;->GROUP:LX/2rw;

    if-ne p1, v1, :cond_1

    iget-object v1, p0, LX/31w;->a:LX/0ad;

    sget-short v2, LX/1EB;->ai:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 488211
    if-nez v0, :cond_3

    .line 488212
    sget-object v0, LX/2rw;->USER:LX/2rw;

    if-ne p1, v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 488213
    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 488214
    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    if-ne p1, v1, :cond_2

    iget-object v1, p0, LX/31w;->a:LX/0ad;

    sget-short v2, LX/1EB;->aj:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    move v0, v0

    .line 488215
    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 488216
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method
