.class public LX/281;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/27v;

.field private final b:LX/27x;


# direct methods
.method public constructor <init>(LX/27v;LX/27x;)V
    .locals 0

    .prologue
    .line 373550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373551
    iput-object p1, p0, LX/281;->a:LX/27v;

    .line 373552
    iput-object p2, p0, LX/281;->b:LX/27x;

    .line 373553
    return-void
.end method


# virtual methods
.method public final a(LX/4h7;)V
    .locals 12

    .prologue
    .line 373554
    iget-object v0, p1, LX/4h7;->b:LX/282;

    move-object v0, v0

    .line 373555
    iget-object v0, v0, LX/282;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 373556
    sget-object v0, LX/4h9;->NULL:LX/4h9;

    .line 373557
    iput-object v0, p1, LX/31y;->a:LX/4h9;

    .line 373558
    :cond_0
    :goto_0
    return-void

    .line 373559
    :cond_1
    const/4 v0, 0x0

    .line 373560
    iget-object v1, p0, LX/281;->a:LX/27v;

    invoke-virtual {v1}, LX/27v;->b()LX/282;

    move-result-object v1

    .line 373561
    iget-object v2, p1, LX/4h7;->b:LX/282;

    move-object v2, v2

    .line 373562
    iget-wide v2, v2, LX/282;->b:J

    iget-wide v4, v1, LX/282;->b:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    .line 373563
    iget-object v0, p0, LX/281;->a:LX/27v;

    .line 373564
    iget-object v2, p1, LX/4h7;->b:LX/282;

    move-object v2, v2

    .line 373565
    invoke-virtual {v0, v2}, LX/27v;->a(LX/282;)V

    .line 373566
    sget-object v0, LX/4h9;->OLDER:LX/4h9;

    .line 373567
    iput-object v0, p1, LX/31y;->a:LX/4h9;

    .line 373568
    const/4 v0, 0x1

    .line 373569
    :goto_1
    if-eqz v0, :cond_0

    .line 373570
    iget-object v0, p0, LX/281;->b:LX/27x;

    .line 373571
    iget-object v2, p1, LX/4h7;->b:LX/282;

    move-object v2, v2

    .line 373572
    iget-object v3, p1, LX/31y;->b:Ljava/lang/String;

    move-object v3, v3

    .line 373573
    new-instance v7, LX/0dI;

    iget-object v6, v1, LX/282;->a:Ljava/lang/String;

    iget-wide v8, v1, LX/282;->b:J

    invoke-direct {v7, v6, v8, v9}, LX/0dI;-><init>(Ljava/lang/String;J)V

    .line 373574
    new-instance v8, LX/0dI;

    iget-object v6, v2, LX/282;->a:Ljava/lang/String;

    iget-wide v10, v2, LX/282;->b:J

    invoke-direct {v8, v6, v10, v11}, LX/0dI;-><init>(Ljava/lang/String;J)V

    .line 373575
    iget-object v6, v0, LX/27x;->a:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/49e;

    .line 373576
    sget-object v10, LX/49d;->GLOBAL_SYNC:LX/49d;

    invoke-interface {v6, v7, v8, v10, v3}, LX/49e;->a(LX/0dI;LX/0dI;LX/49d;Ljava/lang/String;)V

    goto :goto_2

    .line 373577
    :cond_2
    goto :goto_0

    .line 373578
    :cond_3
    iget-object v2, p1, LX/4h7;->b:LX/282;

    move-object v2, v2

    .line 373579
    iget-wide v2, v2, LX/282;->b:J

    iget-wide v4, v1, LX/282;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 373580
    iget-object v2, p1, LX/4h7;->b:LX/282;

    move-object v2, v2

    .line 373581
    iget-object v2, v2, LX/282;->a:Ljava/lang/String;

    iget-object v3, v1, LX/282;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 373582
    sget-object v2, LX/4h9;->SAME:LX/4h9;

    .line 373583
    iput-object v2, p1, LX/31y;->a:LX/4h9;

    .line 373584
    goto :goto_1

    .line 373585
    :cond_4
    sget-object v2, LX/4h9;->NEWER:LX/4h9;

    .line 373586
    iput-object v2, p1, LX/31y;->a:LX/4h9;

    .line 373587
    goto :goto_1
.end method
