.class public LX/2px;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/11i;

.field private final b:LX/2py;

.field private final c:LX/16V;

.field private final d:LX/1Lv;

.field public e:LX/16V;

.field public final f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:I

.field public j:I

.field public k:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/11i;LX/16V;Ljava/lang/String;LX/1Lv;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 469132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 469133
    new-instance v0, LX/2py;

    invoke-direct {v0, p0}, LX/2py;-><init>(LX/2px;)V

    iput-object v0, p0, LX/2px;->b:LX/2py;

    .line 469134
    const-string v0, "unset"

    iput-object v0, p0, LX/2px;->g:Ljava/lang/String;

    .line 469135
    iput v1, p0, LX/2px;->i:I

    .line 469136
    iput v1, p0, LX/2px;->j:I

    .line 469137
    iput-object p1, p0, LX/2px;->a:LX/11i;

    .line 469138
    iput-object p2, p0, LX/2px;->c:LX/16V;

    .line 469139
    iput-object p3, p0, LX/2px;->f:Ljava/lang/String;

    .line 469140
    iput-object p4, p0, LX/2px;->d:LX/1Lv;

    .line 469141
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 469142
    iget-object v0, p0, LX/2px;->b:LX/2py;

    invoke-virtual {v0}, LX/2py;->i()V

    .line 469143
    iget-object v0, p0, LX/2px;->c:LX/16V;

    const-class v1, LX/2qj;

    iget-object v2, p0, LX/2px;->b:LX/2py;

    invoke-virtual {v0, v1, v2}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V

    .line 469144
    return-void
.end method

.method public final a(LX/16V;)V
    .locals 2

    .prologue
    .line 469145
    iget-object v0, p0, LX/2px;->e:LX/16V;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot register twice"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 469146
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16V;

    iput-object v0, p0, LX/2px;->e:LX/16V;

    .line 469147
    const-class v0, LX/2qK;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469148
    const-class v0, LX/2qL;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469149
    const-class v0, LX/2qM;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469150
    const-class v0, LX/09P;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469151
    const-class v0, LX/09Q;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469152
    const-class v0, LX/09R;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469153
    const-class v0, LX/09S;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469154
    const-class v0, LX/2qN;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469155
    const-class v0, LX/2qO;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469156
    const-class v0, LX/2qP;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469157
    const-class v0, LX/2qQ;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469158
    const-class v0, LX/2qR;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469159
    const-class v0, LX/2qS;

    iget-object v1, p0, LX/2px;->b:LX/2py;

    invoke-virtual {p1, v0, v1}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469160
    return-void

    .line 469161
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 469162
    iput-object p1, p0, LX/2px;->h:Ljava/lang/String;

    .line 469163
    iget-object v0, p0, LX/2px;->b:LX/2py;

    const/4 v2, 0x1

    .line 469164
    iget-boolean v1, v0, LX/2py;->c:Z

    if-nez v1, :cond_1

    move v1, v2

    .line 469165
    :goto_0
    iput-boolean v2, v0, LX/2py;->c:Z

    .line 469166
    move v0, v1

    .line 469167
    if-eqz v0, :cond_0

    .line 469168
    iget-object v0, p0, LX/2px;->c:LX/16V;

    const-class v1, LX/2qj;

    iget-object v2, p0, LX/2px;->b:LX/2py;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 469169
    :cond_0
    return-void

    .line 469170
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
