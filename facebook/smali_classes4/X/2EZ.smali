.class public LX/2EZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Zb;

.field public final c:LX/0XZ;

.field public final d:Ljava/util/concurrent/ScheduledExecutorService;

.field private final e:LX/09u;

.field private final f:Lcom/facebook/analytics/reporters/AppStateReporter$ReportAppStateRunnable;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 385490
    const-class v0, LX/2EZ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2EZ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0XZ;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0XZ;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 385591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385592
    iput-object p1, p0, LX/2EZ;->b:LX/0Zb;

    .line 385593
    iput-object p2, p0, LX/2EZ;->c:LX/0XZ;

    .line 385594
    iput-object p3, p0, LX/2EZ;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 385595
    iput-object p4, p0, LX/2EZ;->g:LX/0Or;

    .line 385596
    :try_start_0
    new-instance v0, LX/09u;

    invoke-direct {v0}, LX/09u;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 385597
    :goto_0
    move-object v0, v0

    .line 385598
    iput-object v0, p0, LX/2EZ;->e:LX/09u;

    .line 385599
    new-instance v0, Lcom/facebook/analytics/reporters/AppStateReporter$ReportAppStateRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/reporters/AppStateReporter$ReportAppStateRunnable;-><init>(LX/2EZ;)V

    iput-object v0, p0, LX/2EZ;->f:Lcom/facebook/analytics/reporters/AppStateReporter$ReportAppStateRunnable;

    .line 385600
    return-void

    .line 385601
    :catch_0
    move-exception v0

    .line 385602
    sget-object v1, LX/2EZ;->a:Ljava/lang/String;

    const-string p1, "Error instantiating app state log parser"

    invoke-static {v1, p1, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 385603
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/io/File;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 385587
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 385588
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 385589
    if-gez v1, :cond_0

    :goto_0
    move-object v0, v0

    .line 385590
    return-object v0

    :cond_0
    const/4 p0, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/nio/channels/FileChannel;)Ljava/nio/channels/FileLock;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 385557
    const/4 v0, 0x0

    .line 385558
    :try_start_0
    invoke-virtual {p0}, Ljava/nio/channels/FileChannel;->tryLock()Ljava/nio/channels/FileLock;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 385559
    :cond_0
    :goto_0
    return-object v0

    .line 385560
    :catch_0
    move-exception v1

    .line 385561
    :try_start_1
    invoke-virtual {v1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    .line 385562
    if-nez v2, :cond_1

    .line 385563
    throw v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 385564
    :catch_1
    move-exception v1

    .line 385565
    sget-object v2, LX/2EZ;->a:Ljava/lang/String;

    const-string v3, "Error acquiring lock"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 385566
    :cond_1
    :try_start_2
    if-nez v2, :cond_2

    .line 385567
    const/4 v3, -0x1

    .line 385568
    :goto_1
    move v2, v3

    .line 385569
    const/16 v3, 0xb

    if-eq v2, v3, :cond_0

    .line 385570
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 385571
    :cond_2
    :try_start_3
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_3

    .line 385572
    instance-of v3, v2, Landroid/system/ErrnoException;

    if-nez v3, :cond_5

    .line 385573
    const/4 v3, -0x1

    .line 385574
    :goto_2
    move v3, v3

    .line 385575
    goto :goto_1

    .line 385576
    :cond_3
    const/4 v3, -0x1

    .line 385577
    invoke-static {}, LX/3nT;->a()V

    .line 385578
    sget-object v4, LX/3nT;->c:Ljava/lang/Class;

    if-nez v4, :cond_6

    .line 385579
    :cond_4
    :goto_3
    move v3, v3

    .line 385580
    goto :goto_1

    .line 385581
    :cond_5
    check-cast v2, Landroid/system/ErrnoException;

    .line 385582
    iget v3, v2, Landroid/system/ErrnoException;->errno:I

    goto :goto_2

    .line 385583
    :cond_6
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    sget-object v5, LX/3nT;->c:Ljava/lang/Class;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 385584
    :try_start_4
    sget-object v4, LX/3nT;->d:Ljava/lang/reflect/Field;

    invoke-virtual {v4, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result v3

    goto :goto_3

    .line 385585
    :catch_2
    move-exception v4

    .line 385586
    sget-object v5, LX/3nT;->a:Ljava/lang/String;

    const-string p0, "Error accessing errno field"

    invoke-static {v5, p0, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method

.method private static a(Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 6

    .prologue
    .line 385536
    const-string v0, "status"

    .line 385537
    iget-object v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->a:Ljava/lang/String;

    move-object v1, v1

    .line 385538
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 385539
    const-string v0, "checksum"

    .line 385540
    iget-object v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->b:Ljava/lang/String;

    move-object v1, v1

    .line 385541
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 385542
    const-string v0, "contents"

    .line 385543
    iget-object v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->c:Ljava/lang/String;

    move-object v1, v1

    .line 385544
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 385545
    const-string v0, "reportId"

    .line 385546
    iget-object v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->d:Ljava/lang/String;

    move-object v1, v1

    .line 385547
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 385548
    const-string v0, "reportTime"

    .line 385549
    iget-wide v4, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->e:J

    move-wide v2, v4

    .line 385550
    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 385551
    iget-object v0, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->f:[B

    move-object v0, v0

    .line 385552
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1u4;->a([BZ)Ljava/lang/String;

    move-result-object v0

    .line 385553
    iget-object v1, p0, Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;->b:Ljava/lang/String;

    move-object v1, v1

    .line 385554
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 385555
    new-instance v1, LX/09v;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Checksum does not match. Expected \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/09v;-><init>(Ljava/lang/String;)V

    throw v1

    .line 385556
    :cond_0
    return-void
.end method

.method private static b(Ljava/io/File;)J
    .locals 4

    .prologue
    .line 385535
    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public static c(LX/2EZ;)V
    .locals 15

    .prologue
    .line 385494
    invoke-static {}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->d()Ljava/io/File;

    move-result-object v4

    .line 385495
    invoke-virtual {v4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 385496
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 385497
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 385498
    if-nez v5, :cond_1

    .line 385499
    sget-object v1, LX/2EZ;->a:Ljava/lang/String;

    const-string v2, "No app state log files found in app state log directory: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 385500
    :cond_0
    return-void

    .line 385501
    :cond_1
    array-length v6, v5

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v7, v5, v3

    .line 385502
    invoke-virtual {v4, v7}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 385503
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 385504
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 385505
    :cond_2
    :try_start_0
    new-instance v8, Ljava/io/RandomAccessFile;

    const-string v0, "rw"

    invoke-direct {v8, v7, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 385506
    const/4 v2, 0x0

    .line 385507
    :try_start_1
    new-instance v9, Ljava/io/FileInputStream;

    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 385508
    const/4 v1, 0x0

    .line 385509
    :try_start_2
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-static {v0}, LX/2EZ;->a(Ljava/nio/channels/FileChannel;)Ljava/nio/channels/FileLock;

    move-result-object v0

    .line 385510
    if-eqz v0, :cond_4

    .line 385511
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 385512
    new-instance v10, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "fbandroid_cold_start"

    invoke-direct {v10, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 385513
    :try_start_3
    iget-object v0, p0, LX/2EZ;->e:LX/09u;

    invoke-static {v7}, LX/2EZ;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v7}, LX/2EZ;->b(Ljava/io/File;)J

    move-result-wide v12

    invoke-virtual {v0, v9, v11, v12, v13}, LX/09u;->a(Ljava/io/InputStream;Ljava/lang/String;J)Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;

    move-result-object v0

    .line 385514
    invoke-static {v0, v10}, LX/2EZ;->a(Lcom/facebook/analytics/appstatelogger/AppStateServiceReport;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    :try_end_3
    .catch LX/09v; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 385515
    :goto_2
    :try_start_4
    iget-object v0, p0, LX/2EZ;->b:LX/0Zb;

    invoke-interface {v0, v10}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 385516
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v0

    .line 385517
    if-eqz v0, :cond_3

    .line 385518
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 385519
    :goto_3
    :try_start_5
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :try_start_6
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 385520
    sget-object v1, LX/2EZ;->a:Ljava/lang/String;

    const-string v2, "Error reporting on app state log file: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v9

    invoke-static {v1, v0, v2, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 385521
    :catch_1
    move-exception v0

    .line 385522
    :try_start_7
    new-instance v11, Ljava/io/StringWriter;

    invoke-direct {v11}, Ljava/io/StringWriter;-><init>()V

    .line 385523
    new-instance v12, Ljava/io/PrintWriter;

    invoke-direct {v12, v11}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 385524
    invoke-virtual {v0, v12}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 385525
    invoke-virtual {v11}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v11

    move-object v0, v11

    .line 385526
    const-string v11, "logParseError"

    invoke-virtual {v10, v11, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_2

    .line 385527
    :catch_2
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 385528
    :catchall_0
    move-exception v1

    move-object v14, v1

    move-object v1, v0

    move-object v0, v14

    :goto_4
    if-eqz v1, :cond_5

    :try_start_9
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :goto_5
    :try_start_a
    throw v0
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 385529
    :catch_3
    move-exception v0

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 385530
    :catchall_1
    move-exception v1

    move-object v14, v1

    move-object v1, v0

    move-object v0, v14

    :goto_6
    if-eqz v1, :cond_6

    :try_start_c
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_5
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    :goto_7
    :try_start_d
    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0

    .line 385531
    :cond_3
    :try_start_e
    sget-object v0, LX/2EZ;->a:Ljava/lang/String;

    const-string v10, "Failed to delete app state log file path: %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v0, v10, v11}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 385532
    :catchall_2
    move-exception v0

    goto :goto_4

    .line 385533
    :cond_4
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto :goto_3

    .line 385534
    :catch_4
    move-exception v9

    :try_start_f
    invoke-static {v1, v9}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_6

    :cond_5
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    goto :goto_5

    :catch_5
    move-exception v2

    :try_start_10
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_7

    :cond_6
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    goto :goto_7
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 385491
    iget-object v0, p0, LX/2EZ;->e:LX/09u;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/facebook/analytics/appstatelogger/AppStateLogger;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 385492
    :cond_0
    :goto_0
    return-void

    .line 385493
    :cond_1
    iget-object v0, p0, LX/2EZ;->d:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/2EZ;->f:Lcom/facebook/analytics/reporters/AppStateReporter$ReportAppStateRunnable;

    const v2, -0x11bad39c

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
