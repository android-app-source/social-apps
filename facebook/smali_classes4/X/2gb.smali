.class public LX/2gb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1fT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2gb;


# instance fields
.field public final b:LX/2gc;

.field private final c:LX/0lC;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B9n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 448677
    const-class v0, LX/2gb;

    sput-object v0, LX/2gb;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2gc;LX/0lC;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2gc;",
            "LX/0lC;",
            "LX/0Or",
            "<",
            "LX/B9n;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448679
    iput-object p1, p0, LX/2gb;->b:LX/2gc;

    .line 448680
    iput-object p2, p0, LX/2gb;->c:LX/0lC;

    .line 448681
    iput-object p3, p0, LX/2gb;->d:LX/0Or;

    .line 448682
    return-void
.end method

.method public static a(LX/0QB;)LX/2gb;
    .locals 6

    .prologue
    .line 448683
    sget-object v0, LX/2gb;->e:LX/2gb;

    if-nez v0, :cond_1

    .line 448684
    const-class v1, LX/2gb;

    monitor-enter v1

    .line 448685
    :try_start_0
    sget-object v0, LX/2gb;->e:LX/2gb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 448686
    if-eqz v2, :cond_0

    .line 448687
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 448688
    new-instance v5, LX/2gb;

    invoke-static {v0}, LX/2gc;->b(LX/0QB;)LX/2gc;

    move-result-object v3

    check-cast v3, LX/2gc;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    const/16 p0, 0x2680

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2gb;-><init>(LX/2gc;LX/0lC;LX/0Or;)V

    .line 448689
    move-object v0, v5

    .line 448690
    sput-object v0, LX/2gb;->e:LX/2gb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 448691
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 448692
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 448693
    :cond_1
    sget-object v0, LX/2gb;->e:LX/2gb;

    return-object v0

    .line 448694
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 448695
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a([B)LX/6mS;
    .locals 5

    .prologue
    .line 448696
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    .line 448697
    new-instance v1, LX/1sr;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    const/4 v3, 0x0

    array-length v4, p0

    invoke-direct {v2, p0, v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-direct {v1, v2}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v1}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 448698
    :try_start_0
    invoke-static {v0}, LX/3ll;->b(LX/1su;)LX/3ll;

    .line 448699
    invoke-static {v0}, LX/6mS;->b(LX/1su;)LX/6mS;
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 448700
    return-object v0

    .line 448701
    :catch_0
    move-exception v0

    .line 448702
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 3

    .prologue
    .line 448703
    :try_start_0
    const-string v0, "/t_inbox"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 448704
    invoke-static {p2}, LX/2gb;->a([B)LX/6mS;

    move-result-object v0

    .line 448705
    iget-object v1, v0, LX/6mS;->unseen:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 448706
    iget-object v2, v0, LX/6mS;->unread:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 448707
    iget-object p1, v0, LX/6mS;->recentUnread:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 448708
    iget-object v0, p0, LX/2gb;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9n;

    invoke-virtual {v0, v2, v1, p1}, LX/B9n;->a(III)V

    .line 448709
    iget-object v0, p0, LX/2gb;->b:LX/2gc;

    .line 448710
    iget-object p0, v0, LX/2gc;->b:LX/2gd;

    invoke-virtual {p0}, LX/2gd;->a()Z

    move-result p0

    if-eqz p0, :cond_3

    .line 448711
    :goto_0
    iget-object p0, v0, LX/2gc;->c:LX/2PR;

    invoke-virtual {p0, p1}, LX/2PR;->a(I)V

    .line 448712
    :cond_0
    :goto_1
    return-void

    .line 448713
    :cond_1
    const-string v0, "/mobile_requests_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 448714
    iget-object v0, p0, LX/2gb;->c:LX/0lC;

    invoke-static {p2}, LX/0YN;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 448715
    const-string v1, "num_unseen"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->d(LX/0lF;)I

    move-result v1

    .line 448716
    iget-object v2, p0, LX/2gb;->b:LX/2gc;

    invoke-virtual {v2, v1}, LX/2gc;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 448717
    goto :goto_1

    .line 448718
    :catch_0
    move-exception v0

    .line 448719
    sget-object v1, LX/2gb;->a:Ljava/lang/Class;

    const-string v2, "Failed to read mqtt message"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 448720
    :cond_2
    :try_start_1
    const-string v0, "/friend_requests_seen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 448721
    iget-object v0, p0, LX/2gb;->b:LX/2gc;

    const/4 v1, 0x0

    .line 448722
    iget-object v2, v0, LX/2gc;->a:LX/0xB;

    sget-object p0, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v2, p0, v1}, LX/0xB;->a(LX/12j;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 448723
    goto :goto_1

    :cond_3
    move p1, v1

    .line 448724
    goto :goto_0
.end method
