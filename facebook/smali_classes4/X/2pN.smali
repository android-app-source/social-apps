.class public final LX/2pN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16X;


# instance fields
.field public final synthetic a:LX/2pM;


# direct methods
.method public constructor <init>(LX/2pM;)V
    .locals 0

    .prologue
    .line 468094
    iput-object p1, p0, LX/2pN;->a:LX/2pM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1ub;)V
    .locals 4

    .prologue
    .line 468095
    iget-object v0, p0, LX/2pN;->a:LX/2pM;

    iget-object v1, p1, LX/1ub;->b:Ljava/lang/String;

    iget-object v2, p1, LX/1ub;->a:LX/2fs;

    .line 468096
    iget-object v3, v0, LX/2pM;->v:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 468097
    iget-object v3, v2, LX/2fs;->c:LX/1A0;

    sget-object p0, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-eq v3, p0, :cond_2

    iget-object v3, v2, LX/2fs;->c:LX/1A0;

    sget-object p0, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    if-eq v3, p0, :cond_2

    const/4 v3, 0x1

    :goto_0
    iput-boolean v3, v0, LX/2pM;->w:Z

    .line 468098
    :cond_0
    iget-object v3, v0, LX/2pM;->v:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, LX/2fs;->c:LX/1A0;

    sget-object p0, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-eq v3, p0, :cond_1

    .line 468099
    iget-object v3, v0, LX/2pM;->o:Landroid/os/Handler;

    new-instance p0, Lcom/facebook/video/player/plugins/SinglePlayIconPlugin$1;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/video/player/plugins/SinglePlayIconPlugin$1;-><init>(LX/2pM;Ljava/lang/String;LX/2fs;)V

    const p1, 0x5728a7f6

    invoke-static {v3, p0, p1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 468100
    :cond_1
    return-void

    .line 468101
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method
