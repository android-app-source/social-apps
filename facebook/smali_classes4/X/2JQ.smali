.class public LX/2JQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile g:LX/2JQ;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0Uo;

.field public final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 392814
    const-string v0, "dash_graphql_cache"

    const-string v1, "bookmarks.db"

    const-string v2, "fb.db"

    const-string v3, "graphql"

    const-string v4, "non_cached_preferences_db"

    const-string v5, "notifications.db"

    const-string v6, "pages_db"

    const-string v7, "pages_db2"

    const-string v8, "threads_db"

    const-string v9, "uploadmanager.db"

    const-string v10, "users_db"

    const-string v11, "users_db2"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "zero_rating_db"

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2JQ;->d:LX/0Px;

    .line 392815
    const-string v0, ""

    const-string v1, "-journal"

    const-string v2, "-shm"

    const-string v3, "-wal"

    const-string v4, "-uid"

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2JQ;->e:LX/0Px;

    .line 392816
    const-string v0, "-corrupted"

    const-string v1, ".back"

    const-string v2, ".old"

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2JQ;->f:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Uo;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392818
    iput-object p1, p0, LX/2JQ;->a:Landroid/content/Context;

    .line 392819
    iput-object p2, p0, LX/2JQ;->b:LX/0Uo;

    .line 392820
    iput-object p3, p0, LX/2JQ;->c:LX/03V;

    .line 392821
    return-void
.end method

.method public static a(LX/0QB;)LX/2JQ;
    .locals 6

    .prologue
    .line 392822
    sget-object v0, LX/2JQ;->g:LX/2JQ;

    if-nez v0, :cond_1

    .line 392823
    const-class v1, LX/2JQ;

    monitor-enter v1

    .line 392824
    :try_start_0
    sget-object v0, LX/2JQ;->g:LX/2JQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 392825
    if-eqz v2, :cond_0

    .line 392826
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 392827
    new-instance p0, LX/2JQ;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v4

    check-cast v4, LX/0Uo;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/2JQ;-><init>(Landroid/content/Context;LX/0Uo;LX/03V;)V

    .line 392828
    move-object v0, p0

    .line 392829
    sput-object v0, LX/2JQ;->g:LX/2JQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392830
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 392831
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392832
    :cond_1
    sget-object v0, LX/2JQ;->g:LX/2JQ;

    return-object v0

    .line 392833
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 392834
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 13

    .prologue
    .line 392835
    iget-object v0, p0, LX/2JQ;->b:LX/0Uo;

    .line 392836
    iget-boolean v1, v0, LX/0Uo;->V:Z

    move v0, v1

    .line 392837
    if-eqz v0, :cond_1

    .line 392838
    sget-object v0, LX/2JQ;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    sget-object v0, LX/2JQ;->d:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 392839
    :try_start_0
    iget-object v1, p0, LX/2JQ;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 392840
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 392841
    :catch_0
    move-exception v1

    .line 392842
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "OldDatabasesCleaner: cannot delete old db: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 392843
    iget-object v4, p0, LX/2JQ;->c:LX/03V;

    const-string v5, "old_databases_cleaner"

    invoke-virtual {v4, v5, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 392844
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2JQ;->a:Landroid/content/Context;

    const-string v1, "db"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    .line 392845
    :goto_2
    move-object v0, v0

    .line 392846
    if-eqz v0, :cond_1

    .line 392847
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 392848
    if-nez v1, :cond_2

    .line 392849
    const-string v0, "OldDatabasesCleaner: Databases folder doesn\'t exist"

    .line 392850
    iget-object v1, p0, LX/2JQ;->c:LX/03V;

    const-string v2, "old_databases_cleaner"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 392851
    :cond_1
    :goto_3
    return-void

    .line 392852
    :cond_2
    const/4 v5, 0x0

    .line 392853
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v7

    .line 392854
    array-length v3, v1

    move v2, v5

    :goto_4
    if-ge v2, v3, :cond_3

    aget-object v4, v1, v2

    .line 392855
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 392856
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 392857
    :cond_3
    sget-object v2, LX/2JQ;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    move v4, v5

    :goto_5
    if-ge v4, v8, :cond_6

    sget-object v2, LX/2JQ;->d:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 392858
    sget-object v2, LX/2JQ;->e:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v9

    move v6, v5

    :goto_6
    if-ge v6, v9, :cond_5

    sget-object v2, LX/2JQ;->e:LX/0Px;

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 392859
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 392860
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v10, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 392861
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 392862
    :try_start_2
    invoke-virtual {v10}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 392863
    :cond_4
    :goto_7
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_6

    .line 392864
    :catch_1
    move-exception v2

    .line 392865
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "OldDatabasesCleaner: cannot delete old db file "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 392866
    iget-object v11, p0, LX/2JQ;->c:LX/03V;

    const-string v12, "old_databases_cleaner"

    invoke-virtual {v11, v12, v10, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_7

    .line 392867
    :cond_5
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_5

    .line 392868
    :cond_6
    const/4 v3, 0x0

    .line 392869
    array-length v5, v1

    move v4, v3

    :goto_8
    if-ge v4, v5, :cond_9

    aget-object v6, v1, v4

    .line 392870
    sget-object v0, LX/2JQ;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v7

    move v2, v3

    :goto_9
    if-ge v2, v7, :cond_8

    sget-object v0, LX/2JQ;->f:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 392871
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 392872
    :try_start_3
    invoke-virtual {v6}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    .line 392873
    :cond_7
    :goto_a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    .line 392874
    :catch_2
    move-exception v0

    .line 392875
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "OldDatabasesCleaner: cannot delete invalid db file "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 392876
    iget-object v9, p0, LX/2JQ;->c:LX/03V;

    const-string v10, "old_databases_cleaner"

    invoke-virtual {v9, v10, v8, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_a

    .line 392877
    :cond_8
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_8

    .line 392878
    :cond_9
    goto/16 :goto_3

    .line 392879
    :catch_3
    move-exception v0

    .line 392880
    const-string v1, "OldDatabasesCleaner: Could not find databases folder"

    .line 392881
    iget-object v2, p0, LX/2JQ;->c:LX/03V;

    const-string v3, "old_databases_cleaner"

    invoke-virtual {v2, v3, v1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 392882
    const/4 v0, 0x0

    goto/16 :goto_2
.end method
