.class public LX/3J2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3J2;


# instance fields
.field private a:LX/0tX;

.field private b:Ljava/util/concurrent/ExecutorService;

.field public c:LX/03V;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/03V;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 547427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547428
    iput-object p1, p0, LX/3J2;->a:LX/0tX;

    .line 547429
    iput-object p2, p0, LX/3J2;->b:Ljava/util/concurrent/ExecutorService;

    .line 547430
    iput-object p3, p0, LX/3J2;->c:LX/03V;

    .line 547431
    return-void
.end method

.method public static a(LX/0QB;)LX/3J2;
    .locals 6

    .prologue
    .line 547432
    sget-object v0, LX/3J2;->d:LX/3J2;

    if-nez v0, :cond_1

    .line 547433
    const-class v1, LX/3J2;

    monitor-enter v1

    .line 547434
    :try_start_0
    sget-object v0, LX/3J2;->d:LX/3J2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 547435
    if-eqz v2, :cond_0

    .line 547436
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 547437
    new-instance p0, LX/3J2;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/3J2;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/03V;)V

    .line 547438
    move-object v0, p0

    .line 547439
    sput-object v0, LX/3J2;->d:LX/3J2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 547440
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 547441
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 547442
    :cond_1
    sget-object v0, LX/3J2;->d:LX/3J2;

    return-object v0

    .line 547443
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 547444
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/Bu6;)V
    .locals 4

    .prologue
    .line 547445
    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547446
    invoke-interface {p3}, LX/Bu6;->a()V

    .line 547447
    :goto_0
    return-void

    .line 547448
    :cond_0
    new-instance v0, LX/BZa;

    invoke-direct {v0}, LX/BZa;-><init>()V

    move-object v0, v0

    .line 547449
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 547450
    const-string v1, "locales"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 547451
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 547452
    iget-object v1, p0, LX/3J2;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 547453
    new-instance v1, LX/Bvp;

    invoke-direct {v1, p0, p3}, LX/Bvp;-><init>(LX/3J2;LX/Bu6;)V

    iget-object v2, p0, LX/3J2;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
