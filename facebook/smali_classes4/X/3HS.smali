.class public LX/3HS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3HS;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3Gr;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

.field private final c:LX/3H4;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;LX/3H4;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543513
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3HS;->a:Ljava/util/List;

    .line 543514
    iput-object p1, p0, LX/3HS;->b:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    .line 543515
    iput-object p2, p0, LX/3HS;->c:LX/3H4;

    .line 543516
    return-void
.end method

.method public static a(LX/0QB;)LX/3HS;
    .locals 5

    .prologue
    .line 543499
    sget-object v0, LX/3HS;->d:LX/3HS;

    if-nez v0, :cond_1

    .line 543500
    const-class v1, LX/3HS;

    monitor-enter v1

    .line 543501
    :try_start_0
    sget-object v0, LX/3HS;->d:LX/3HS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 543502
    if-eqz v2, :cond_0

    .line 543503
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 543504
    new-instance p0, LX/3HS;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-static {v0}, LX/3H4;->a(LX/0QB;)LX/3H4;

    move-result-object v4

    check-cast v4, LX/3H4;

    invoke-direct {p0, v3, v4}, LX/3HS;-><init>(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;LX/3H4;)V

    .line 543505
    move-object v0, p0

    .line 543506
    sput-object v0, LX/3HS;->d:LX/3HS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 543507
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 543508
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 543509
    :cond_1
    sget-object v0, LX/3HS;->d:LX/3HS;

    return-object v0

    .line 543510
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 543511
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(LX/D6q;)V
    .locals 2

    .prologue
    .line 543517
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3HS;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Gr;

    .line 543518
    invoke-interface {v0, p1}, LX/3Gr;->a(LX/D6q;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 543519
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 543520
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/3Gr;)V
    .locals 1

    .prologue
    .line 543495
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/3HS;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 543496
    iget-object v0, p0, LX/3HS;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 543497
    :cond_0
    monitor-exit p0

    return-void

    .line 543498
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 543486
    iget-object v0, p0, LX/3HS;->b:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-virtual {v0, p1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g(Ljava/lang/String;)LX/2oN;

    move-result-object v0

    sget-object v1, LX/2oN;->VIDEO_AD:LX/2oN;

    if-ne v0, v1, :cond_1

    .line 543487
    sget-object v0, LX/D6q;->HIDE_AD:LX/D6q;

    invoke-direct {p0, v0}, LX/3HS;->a(LX/D6q;)V

    .line 543488
    iget-object v0, p0, LX/3HS;->b:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-virtual {v0, p1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v1

    .line 543489
    new-instance v0, LX/BSQ;

    invoke-direct {v0}, LX/BSQ;-><init>()V

    .line 543490
    iput-object p1, v0, LX/BSQ;->a:Ljava/lang/String;

    .line 543491
    if-eqz v1, :cond_0

    .line 543492
    invoke-virtual {v1}, LX/D6v;->j()LX/BSQ;

    move-result-object v0

    .line 543493
    :cond_0
    iget-object v1, p0, LX/3HS;->c:LX/3H4;

    sget-object v2, LX/BSX;->HIDE_AD:LX/BSX;

    invoke-virtual {v1, v2, v0}, LX/3H4;->a(LX/BSX;LX/BSQ;)V

    .line 543494
    :cond_1
    return-void
.end method

.method public final declared-synchronized b(LX/3Gr;)V
    .locals 1

    .prologue
    .line 543483
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3HS;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 543484
    monitor-exit p0

    return-void

    .line 543485
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
