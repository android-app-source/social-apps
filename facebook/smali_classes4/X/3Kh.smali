.class public final LX/3Kh;
.super LX/3EK;
.source ""


# instance fields
.field public final synthetic d:Lcom/google/android/gms/location/GeofencingRequest;

.field public final synthetic e:Landroid/app/PendingIntent;

.field public final synthetic f:LX/2vv;


# direct methods
.method public constructor <init>(LX/2vv;LX/2wX;Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;)V
    .locals 0

    iput-object p1, p0, LX/3Kh;->f:LX/2vv;

    iput-object p3, p0, LX/3Kh;->d:Lcom/google/android/gms/location/GeofencingRequest;

    iput-object p4, p0, LX/3Kh;->e:Landroid/app/PendingIntent;

    invoke-direct {p0, p2}, LX/3EK;-><init>(LX/2wX;)V

    return-void
.end method


# virtual methods
.method public final b(LX/2wK;)V
    .locals 4

    check-cast p1, LX/2wF;

    iget-object v0, p0, LX/3Kh;->d:Lcom/google/android/gms/location/GeofencingRequest;

    iget-object v1, p0, LX/3Kh;->e:Landroid/app/PendingIntent;

    invoke-virtual {p1}, LX/2wI;->k()V

    const-string v2, "geofencingRequest can\'t be null."

    invoke-static {v0, v2}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "PendingIntent must be specified."

    invoke-static {v1, v2}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "ResultHolder not provided."

    invoke-static {p0, v2}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, LX/7aE;

    invoke-direct {v3, p0}, LX/7aE;-><init>(LX/2wh;)V

    invoke-virtual {p1}, LX/2wI;->m()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, LX/2xF;

    invoke-interface {v2, v0, v1, v3}, LX/2xF;->a(Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;LX/7a5;)V

    return-void
.end method
