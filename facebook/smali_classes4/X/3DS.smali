.class public LX/3DS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2vL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2vL",
        "<",
        "LX/3DT;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/4KY;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/4KY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/4KY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 534787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 534788
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3DS;->b:Ljava/util/ArrayList;

    .line 534789
    iput-object p1, p0, LX/3DS;->a:LX/0Px;

    .line 534790
    return-void
.end method

.method public static a(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 534806
    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    invoke-virtual {v0}, LX/15i;->e()Ljava/nio/ByteBuffer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 534807
    const-string v0, "GraphQLObserverMemoryCacheModelStore"

    const-string v1, "Losing extras on reflatten!"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 534808
    :cond_0
    invoke-static {p0}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 534809
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 534810
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 534811
    const-string v2, "GraphQLObserverMemoryCacheModelStore.reflattenMutableFlatbuffer"

    invoke-virtual {v0, v2, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 534812
    invoke-static {v1}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    return-object v0
.end method

.method private static a(Ljava/util/Collection;Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 534800
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v0, v1

    .line 534801
    :goto_0
    return v0

    .line 534802
    :cond_1
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 534803
    invoke-interface {p1, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 534804
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 534805
    goto :goto_0
.end method

.method private static d(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 534794
    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v3

    .line 534795
    new-instance v0, LX/15i;

    invoke-virtual {v3}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v3}, LX/15i;->c()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v3}, LX/15i;->e()Ljava/nio/ByteBuffer;

    move-result-object v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 534796
    const-string v1, "GraphQLObserverMemoryCacheModelStore.reloadFromMutableFlatbuffer"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 534797
    invoke-virtual {v0}, LX/15i;->b()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 534798
    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    return-object v0

    .line 534799
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 534791
    check-cast p1, LX/3DT;

    .line 534792
    iget-object v0, p1, LX/3DT;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    move v0, v0

    .line 534793
    return v0
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 1

    .prologue
    .line 534741
    invoke-static {p2}, LX/3DS;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 534781
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 534782
    iget-object v0, p0, LX/3DS;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/3DS;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4KY;

    .line 534783
    iget-object v4, v0, LX/4KY;->a:Ljava/util/Collection;

    invoke-static {p1, v4}, LX/3DS;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 534784
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 534785
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 534786
    :cond_1
    new-instance v0, LX/3DT;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3DT;-><init>(LX/0Px;)V

    return-object v0
.end method

.method public final a([J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 534780
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 534779
    return-void
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 534776
    check-cast p1, LX/3DT;

    .line 534777
    iput p2, p1, LX/3DT;->b:I

    .line 534778
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param

    .prologue
    .line 534770
    check-cast p1, LX/3DT;

    .line 534771
    invoke-virtual {p1}, LX/3DT;->b()LX/4KY;

    move-result-object v0

    .line 534772
    const-string v1, "confirmed"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 534773
    iput-object p3, v0, LX/4KY;->c:Lcom/facebook/flatbuffers/MutableFlattenable;

    .line 534774
    :goto_0
    return-void

    .line 534775
    :cond_0
    iput-object p3, v0, LX/4KY;->d:Lcom/facebook/flatbuffers/MutableFlattenable;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/util/Set;)Z
    .locals 1

    .prologue
    .line 534768
    check-cast p1, LX/3DT;

    .line 534769
    invoke-virtual {p1}, LX/3DT;->b()LX/4KY;

    move-result-object v0

    iget-object v0, v0, LX/4KY;->a:Ljava/util/Collection;

    invoke-static {p2, v0}, LX/3DS;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 1

    .prologue
    .line 534813
    invoke-static {p2}, LX/3DS;->d(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 534767
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 534766
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param

    .prologue
    .line 534760
    check-cast p1, LX/3DT;

    .line 534761
    invoke-virtual {p1}, LX/3DT;->b()LX/4KY;

    move-result-object v0

    .line 534762
    const-string v1, "confirmed"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 534763
    iput-object p3, v0, LX/4KY;->c:Lcom/facebook/flatbuffers/MutableFlattenable;

    .line 534764
    :goto_0
    return-void

    .line 534765
    :cond_0
    iput-object p3, v0, LX/4KY;->d:Lcom/facebook/flatbuffers/MutableFlattenable;

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 534757
    check-cast p1, LX/3DT;

    .line 534758
    invoke-virtual {p1}, LX/3DT;->b()LX/4KY;

    move-result-object v0

    .line 534759
    iget-object v0, v0, LX/4KY;->c:Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 534756
    return-void
.end method

.method public final d(Ljava/lang/Object;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 1

    .prologue
    .line 534754
    check-cast p1, LX/3DT;

    .line 534755
    invoke-virtual {p1}, LX/3DT;->b()LX/4KY;

    move-result-object v0

    iget-object v0, v0, LX/4KY;->c:Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-static {v0}, LX/3DS;->d(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 534753
    const-string v0, "ObserverMemoryCache"

    return-object v0
.end method

.method public final e(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 534750
    check-cast p1, LX/3DT;

    .line 534751
    invoke-virtual {p1}, LX/3DT;->b()LX/4KY;

    move-result-object v0

    .line 534752
    iget-object v1, v0, LX/4KY;->c:Lcom/facebook/flatbuffers/MutableFlattenable;

    iget-object v0, v0, LX/4KY;->d:Lcom/facebook/flatbuffers/MutableFlattenable;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic f(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 534749
    return-void
.end method

.method public final g(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 534745
    check-cast p1, LX/3DT;

    .line 534746
    invoke-virtual {p1}, LX/3DT;->b()LX/4KY;

    move-result-object v0

    .line 534747
    iget-object v1, v0, LX/4KY;->c:Lcom/facebook/flatbuffers/MutableFlattenable;

    iput-object v1, v0, LX/4KY;->d:Lcom/facebook/flatbuffers/MutableFlattenable;

    .line 534748
    return-void
.end method

.method public final h(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 534742
    check-cast p1, LX/3DT;

    .line 534743
    iget-object v0, p0, LX/3DS;->b:Ljava/util/ArrayList;

    invoke-virtual {p1}, LX/3DT;->b()LX/4KY;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 534744
    return-void
.end method
