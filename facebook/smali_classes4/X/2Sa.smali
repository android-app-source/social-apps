.class public LX/2Sa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2Sa;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0tX;

.field public final c:LX/2Sb;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0tX;LX/2Sb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 412679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412680
    iput-object p1, p0, LX/2Sa;->a:Landroid/content/res/Resources;

    .line 412681
    iput-object p2, p0, LX/2Sa;->b:LX/0tX;

    .line 412682
    iput-object p3, p0, LX/2Sa;->c:LX/2Sb;

    .line 412683
    return-void
.end method

.method public static a(LX/0QB;)LX/2Sa;
    .locals 6

    .prologue
    .line 412684
    sget-object v0, LX/2Sa;->d:LX/2Sa;

    if-nez v0, :cond_1

    .line 412685
    const-class v1, LX/2Sa;

    monitor-enter v1

    .line 412686
    :try_start_0
    sget-object v0, LX/2Sa;->d:LX/2Sa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 412687
    if-eqz v2, :cond_0

    .line 412688
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 412689
    new-instance p0, LX/2Sa;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/2Sb;->a(LX/0QB;)LX/2Sb;

    move-result-object v5

    check-cast v5, LX/2Sb;

    invoke-direct {p0, v3, v4, v5}, LX/2Sa;-><init>(Landroid/content/res/Resources;LX/0tX;LX/2Sb;)V

    .line 412690
    move-object v0, p0

    .line 412691
    sput-object v0, LX/2Sa;->d:LX/2Sa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412692
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 412693
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 412694
    :cond_1
    sget-object v0, LX/2Sa;->d:LX/2Sa;

    return-object v0

    .line 412695
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 412696
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;LX/0zS;J)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/RecentSearchesFactoryValue;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0zS;",
            "J)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cw5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 412697
    new-instance v0, LX/9zg;

    invoke-direct {v0}, LX/9zg;-><init>()V

    const-string v1, "count"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "filter"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_picture_size"

    iget-object v2, p0, LX/2Sa;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b14f2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    .line 412698
    iget-object v1, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v1

    .line 412699
    new-instance v1, LX/9zg;

    invoke-direct {v1}, LX/9zg;-><init>()V

    move-object v1, v1

    .line 412700
    const/4 v2, 0x1

    .line 412701
    iput-boolean v2, v1, LX/0gW;->l:Z

    .line 412702
    iget-object v2, p0, LX/2Sa;->b:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    .line 412703
    iput-object v3, v1, LX/0zO;->d:Ljava/util/Set;

    .line 412704
    move-object v1, v1

    .line 412705
    invoke-virtual {v1, p6, p7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v1

    if-eqz p4, :cond_0

    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    invoke-virtual {v1, v0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 412706
    iput-object p4, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 412707
    move-object v0, v0

    .line 412708
    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 412709
    new-instance v1, LX/EQ8;

    invoke-direct {v1, p0, p2}, LX/EQ8;-><init>(LX/2Sa;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 412710
    :cond_0
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method
