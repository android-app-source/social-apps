.class public LX/23i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/23i;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/150;

.field public final c:LX/14x;

.field public final d:LX/23j;

.field public final e:LX/23k;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/23l;

.field private final h:Z

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/150;LX/14x;LX/23j;LX/23k;LX/0Or;LX/23l;Ljava/lang/Boolean;LX/0Or;LX/0Or;)V
    .locals 1
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/forcemessenger/annotations/IsUserInDiode;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/forcemessenger/annotations/IsInAccountSwitchingDiode;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/150;",
            "LX/14x;",
            "LX/23j;",
            "LX/23k;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/23l;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365053
    iput-object p1, p0, LX/23i;->a:LX/0Zb;

    .line 365054
    iput-object p2, p0, LX/23i;->b:LX/150;

    .line 365055
    iput-object p3, p0, LX/23i;->c:LX/14x;

    .line 365056
    iput-object p4, p0, LX/23i;->d:LX/23j;

    .line 365057
    iput-object p5, p0, LX/23i;->e:LX/23k;

    .line 365058
    iput-object p6, p0, LX/23i;->f:LX/0Or;

    .line 365059
    iput-object p7, p0, LX/23i;->g:LX/23l;

    .line 365060
    invoke-virtual {p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/23i;->h:Z

    .line 365061
    iput-object p9, p0, LX/23i;->i:LX/0Or;

    .line 365062
    iput-object p10, p0, LX/23i;->j:LX/0Or;

    .line 365063
    return-void
.end method

.method public static a(LX/0QB;)LX/23i;
    .locals 14

    .prologue
    .line 365036
    sget-object v0, LX/23i;->k:LX/23i;

    if-nez v0, :cond_1

    .line 365037
    const-class v1, LX/23i;

    monitor-enter v1

    .line 365038
    :try_start_0
    sget-object v0, LX/23i;->k:LX/23i;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 365039
    if-eqz v2, :cond_0

    .line 365040
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 365041
    new-instance v3, LX/23i;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/150;->a(LX/0QB;)LX/150;

    move-result-object v5

    check-cast v5, LX/150;

    invoke-static {v0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v6

    check-cast v6, LX/14x;

    invoke-static {v0}, LX/23j;->a(LX/0QB;)LX/23j;

    move-result-object v7

    check-cast v7, LX/23j;

    invoke-static {v0}, LX/23k;->a(LX/0QB;)LX/23k;

    move-result-object v8

    check-cast v8, LX/23k;

    const/16 v9, 0x15e7

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    .line 365042
    new-instance v12, LX/23l;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v10

    check-cast v10, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-direct {v12, v10, v11}, LX/23l;-><init>(Landroid/content/ContentResolver;LX/0SG;)V

    .line 365043
    move-object v10, v12

    .line 365044
    check-cast v10, LX/23l;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v11

    check-cast v11, Ljava/lang/Boolean;

    const/16 v12, 0x151c

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x151a

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, LX/23i;-><init>(LX/0Zb;LX/150;LX/14x;LX/23j;LX/23k;LX/0Or;LX/23l;Ljava/lang/Boolean;LX/0Or;LX/0Or;)V

    .line 365045
    move-object v0, v3

    .line 365046
    sput-object v0, LX/23i;->k:LX/23i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365047
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 365048
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 365049
    :cond_1
    sget-object v0, LX/23i;->k:LX/23i;

    return-object v0

    .line 365050
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 365051
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/23i;LX/03R;)LX/23m;
    .locals 1

    .prologue
    .line 365029
    invoke-virtual {p1}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 365030
    invoke-virtual {p0}, LX/23i;->f()Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object p1

    .line 365031
    :cond_0
    invoke-virtual {p1}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365032
    invoke-virtual {p0}, LX/23i;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/23m;->REDIRECT:LX/23m;

    .line 365033
    :goto_0
    return-object v0

    .line 365034
    :cond_1
    sget-object v0, LX/23m;->NOT_IN_EXPERIMENT:LX/23m;

    goto :goto_0

    .line 365035
    :cond_2
    sget-object v0, LX/23m;->INSTALL_NOW:LX/23m;

    goto :goto_0
.end method

.method public static a(LX/23i;Z)Z
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 364998
    if-eqz p1, :cond_1

    .line 364999
    :cond_0
    :goto_0
    return v0

    .line 365000
    :cond_1
    iget-object v2, p0, LX/23i;->g:LX/23l;

    const/high16 v3, 0x1400000

    const/4 v6, 0x0

    .line 365001
    iget-object v4, v2, LX/23l;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/32 v8, 0x36ee80

    sub-long/2addr v4, v8

    iget-wide v8, v2, LX/23l;->e:J

    cmp-long v4, v4, v8

    if-lez v4, :cond_2

    .line 365002
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v4

    .line 365003
    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 365004
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x12

    if-lt v4, v7, :cond_5

    .line 365005
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v8

    iput-wide v8, v2, LX/23l;->c:J

    .line 365006
    invoke-virtual {v5}, Landroid/os/StatFs;->getTotalBytes()J

    move-result-wide v4

    .line 365007
    :goto_1
    const-string v7, "sys_storage_threshold_percentage"

    const/16 v8, 0xa

    invoke-static {v2, v7, v8}, LX/23l;->a(LX/23l;Ljava/lang/String;I)I

    move-result v7

    .line 365008
    const-string v8, "sys_storage_threshold_max_bytes"

    const/high16 v9, 0x1f400000

    invoke-static {v2, v8, v9}, LX/23l;->a(LX/23l;Ljava/lang/String;I)I

    move-result v8

    .line 365009
    int-to-float v7, v7

    long-to-float v4, v4

    mul-float/2addr v4, v7

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    float-to-long v4, v4

    iput-wide v4, v2, LX/23l;->d:J

    .line 365010
    int-to-long v4, v8

    iget-wide v8, v2, LX/23l;->d:J

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iput-wide v4, v2, LX/23l;->d:J

    .line 365011
    iget-object v4, v2, LX/23l;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iput-wide v4, v2, LX/23l;->e:J

    .line 365012
    :cond_2
    iget-wide v4, v2, LX/23l;->c:J

    iget-wide v8, v2, LX/23l;->d:J

    cmp-long v4, v4, v8

    if-gez v4, :cond_6

    move v4, v6

    .line 365013
    :goto_2
    move v2, v4

    .line 365014
    move v1, v2

    .line 365015
    if-nez v1, :cond_4

    .line 365016
    iget-object v1, p0, LX/23i;->a:LX/0Zb;

    const-string v2, "diode_promotion"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 365017
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 365018
    const-string v2, "insufficient disk space"

    invoke-virtual {v1, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 365019
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 365020
    :cond_3
    goto/16 :goto_0

    .line 365021
    :cond_4
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 365022
    if-eqz v1, :cond_8

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "amazon"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 365023
    if-nez v1, :cond_0

    .line 365024
    invoke-virtual {p0}, LX/23i;->d()Z

    move-result v0

    goto/16 :goto_0

    .line 365025
    :cond_5
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v4

    int-to-long v8, v4

    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v4

    int-to-long v10, v4

    mul-long/2addr v8, v10

    iput-wide v8, v2, LX/23l;->c:J

    .line 365026
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockCount()I

    move-result v4

    int-to-long v8, v4

    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v4, v8

    goto/16 :goto_1

    .line 365027
    :cond_6
    iget-wide v4, v2, LX/23l;->c:J

    iget-wide v8, v2, LX/23l;->d:J

    sub-long/2addr v4, v8

    .line 365028
    int-to-long v8, v3

    cmp-long v4, v8, v4

    if-gez v4, :cond_7

    const/4 v4, 0x1

    goto :goto_2

    :cond_7
    move v4, v6

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public static h(LX/23i;)Z
    .locals 1

    .prologue
    .line 364988
    iget-object v0, p0, LX/23i;->e:LX/23k;

    .line 364989
    invoke-virtual {v0}, LX/23k;->b()Landroid/content/pm/PackageInfo;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 364990
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    .line 365064
    sget-object v0, LX/03R;->UNSET:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    const/4 v2, 0x0

    .line 365065
    const/4 v3, 0x1

    .line 365066
    sget-object v4, LX/03R;->YES:LX/03R;

    if-ne v0, v4, :cond_5

    .line 365067
    :cond_0
    :goto_0
    move v3, v3

    .line 365068
    invoke-static {p0, v3}, LX/23i;->a(LX/23i;Z)Z

    move-result v3

    if-nez v3, :cond_4

    .line 365069
    :cond_1
    :goto_1
    move v0, v2

    .line 365070
    if-nez v0, :cond_2

    invoke-virtual {p0}, LX/23i;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 365071
    iget-object v0, p0, LX/23i;->c:LX/14x;

    invoke-virtual {v0}, LX/14x;->b()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, LX/23i;->f()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 365072
    if-nez v0, :cond_2

    invoke-virtual {p0}, LX/23i;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 365073
    :goto_3
    return v0

    .line 365074
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 365075
    :cond_4
    invoke-static {p0, v0}, LX/23i;->a(LX/23i;LX/03R;)LX/23m;

    move-result-object v3

    .line 365076
    sget-object v4, LX/23m;->INSTALL_NOW:LX/23m;

    if-ne v3, v4, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    .line 365077
    :cond_5
    sget-object v4, LX/03R;->YES:LX/03R;

    if-eq v1, v4, :cond_0

    .line 365078
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {p0}, LX/23i;->f()Z

    move-result v4

    if-nez v4, :cond_0

    .line 365079
    :cond_6
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v4

    if-nez v4, :cond_7

    invoke-static {p0}, LX/23i;->h(LX/23i;)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_7
    const/4 v3, 0x0

    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(LX/03R;Z)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 364991
    invoke-static {p0, p1}, LX/23i;->a(LX/23i;LX/03R;)LX/23m;

    move-result-object v0

    sget-object v3, LX/23m;->REDIRECT:LX/23m;

    if-eq v0, v3, :cond_0

    move v0, v1

    .line 364992
    :goto_0
    return v0

    .line 364993
    :cond_0
    iget-object v0, p0, LX/23i;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 364994
    iget-object v3, p0, LX/23i;->b:LX/150;

    invoke-virtual {v3, v0}, LX/151;->a(Ljava/lang/String;)LX/152;

    move-result-object v0

    .line 364995
    iget-boolean v3, v0, LX/152;->a:Z

    if-eqz v3, :cond_1

    move v0, v2

    .line 364996
    goto :goto_0

    .line 364997
    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, v0, LX/152;->c:LX/03R;

    sget-object v3, LX/03R;->NO:LX/03R;

    if-ne v0, v3, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 364979
    iget-boolean v0, p0, LX/23i;->h:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 364980
    :goto_0
    return v0

    .line 364981
    :cond_0
    invoke-virtual {p0}, LX/23i;->f()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 364982
    goto :goto_0

    .line 364983
    :cond_1
    iget-object v0, p0, LX/23i;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 364984
    iget-object v2, p0, LX/23i;->b:LX/150;

    invoke-virtual {v2, v0}, LX/151;->a(Ljava/lang/String;)LX/152;

    move-result-object v0

    .line 364985
    iget-boolean v2, v0, LX/152;->a:Z

    if-nez v2, :cond_2

    iget-object v0, v0, LX/152;->c:LX/03R;

    sget-object v2, LX/03R;->NO:LX/03R;

    if-eq v0, v2, :cond_3

    :cond_2
    move v0, v1

    .line 364986
    goto :goto_0

    .line 364987
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 364968
    invoke-virtual {p0}, LX/23i;->f()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 364969
    :goto_0
    return v0

    .line 364970
    :cond_0
    iget-object v0, p0, LX/23i;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 364971
    iget-object v2, p0, LX/23i;->b:LX/150;

    invoke-virtual {v2, v0}, LX/151;->a(Ljava/lang/String;)LX/152;

    move-result-object v0

    .line 364972
    iget-boolean v2, v0, LX/152;->a:Z

    if-eqz v2, :cond_1

    move v0, v1

    .line 364973
    goto :goto_0

    .line 364974
    :cond_1
    iget-object v2, v0, LX/152;->c:LX/03R;

    sget-object v3, LX/03R;->NO:LX/03R;

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 364975
    goto :goto_0

    .line 364976
    :cond_2
    iget-boolean v0, v0, LX/152;->d:Z

    if-nez v0, :cond_3

    move v0, v1

    .line 364977
    goto :goto_0

    .line 364978
    :cond_3
    iget-object v0, p0, LX/23i;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 364967
    iget-object v0, p0, LX/23i;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 364966
    iget-object v0, p0, LX/23i;->c:LX/14x;

    invoke-virtual {v0}, LX/14x;->a()Z

    move-result v0

    return v0
.end method
