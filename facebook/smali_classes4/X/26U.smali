.class public final LX/26U;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0QJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QJ",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:LX/0QM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QM",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 372238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372239
    new-instance v0, LX/26V;

    invoke-direct {v0, p0}, LX/26V;-><init>(LX/26U;)V

    .line 372240
    new-instance v1, LX/26W;

    invoke-direct {v1, v0}, LX/26W;-><init>(LX/0QK;)V

    move-object v0, v1

    .line 372241
    iput-object v0, p0, LX/26U;->c:LX/0QM;

    .line 372242
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->g()LX/0QN;

    move-result-object v0

    iget-object v1, p0, LX/26U;->c:LX/0QM;

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0QM;)LX/0QJ;

    move-result-object v0

    iput-object v0, p0, LX/26U;->a:LX/0QJ;

    .line 372243
    iput-object p1, p0, LX/26U;->b:Ljava/util/Collection;

    .line 372244
    return-void
.end method
