.class public final enum LX/27W;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/27W;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/27W;

.field public static final enum SMARTLOCK_DELETE:LX/27W;

.field public static final enum SMARTLOCK_HINT_FAIL:LX/27W;

.field public static final enum SMARTLOCK_HINT_REQUEST:LX/27W;

.field public static final enum SMARTLOCK_HINT_SUCCESS:LX/27W;

.field public static final enum SMARTLOCK_REQUEST:LX/27W;

.field public static final enum SMARTLOCK_RESOLVE:LX/27W;

.field public static final enum SMARTLOCK_RESOLVE_FAIL:LX/27W;

.field public static final enum SMARTLOCK_RESOLVE_SUCCESS:LX/27W;

.field public static final enum SMARTLOCK_SUCCESS:LX/27W;


# instance fields
.field private final mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 372937
    new-instance v0, LX/27W;

    const-string v1, "SMARTLOCK_REQUEST"

    const-string v2, "smartlock_request"

    invoke-direct {v0, v1, v4, v2}, LX/27W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/27W;->SMARTLOCK_REQUEST:LX/27W;

    .line 372938
    new-instance v0, LX/27W;

    const-string v1, "SMARTLOCK_SUCCESS"

    const-string v2, "smartlock_success"

    invoke-direct {v0, v1, v5, v2}, LX/27W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/27W;->SMARTLOCK_SUCCESS:LX/27W;

    .line 372939
    new-instance v0, LX/27W;

    const-string v1, "SMARTLOCK_RESOLVE"

    const-string v2, "smartlock_resolve"

    invoke-direct {v0, v1, v6, v2}, LX/27W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/27W;->SMARTLOCK_RESOLVE:LX/27W;

    .line 372940
    new-instance v0, LX/27W;

    const-string v1, "SMARTLOCK_RESOLVE_SUCCESS"

    const-string v2, "smartlock_resolve_success"

    invoke-direct {v0, v1, v7, v2}, LX/27W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/27W;->SMARTLOCK_RESOLVE_SUCCESS:LX/27W;

    .line 372941
    new-instance v0, LX/27W;

    const-string v1, "SMARTLOCK_RESOLVE_FAIL"

    const-string v2, "smartlock_resolve_fail"

    invoke-direct {v0, v1, v8, v2}, LX/27W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/27W;->SMARTLOCK_RESOLVE_FAIL:LX/27W;

    .line 372942
    new-instance v0, LX/27W;

    const-string v1, "SMARTLOCK_DELETE"

    const/4 v2, 0x5

    const-string v3, "smartlock_delete"

    invoke-direct {v0, v1, v2, v3}, LX/27W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/27W;->SMARTLOCK_DELETE:LX/27W;

    .line 372943
    new-instance v0, LX/27W;

    const-string v1, "SMARTLOCK_HINT_REQUEST"

    const/4 v2, 0x6

    const-string v3, "smartlock_hint_request"

    invoke-direct {v0, v1, v2, v3}, LX/27W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/27W;->SMARTLOCK_HINT_REQUEST:LX/27W;

    .line 372944
    new-instance v0, LX/27W;

    const-string v1, "SMARTLOCK_HINT_SUCCESS"

    const/4 v2, 0x7

    const-string v3, "smartlock_hint_success"

    invoke-direct {v0, v1, v2, v3}, LX/27W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/27W;->SMARTLOCK_HINT_SUCCESS:LX/27W;

    .line 372945
    new-instance v0, LX/27W;

    const-string v1, "SMARTLOCK_HINT_FAIL"

    const/16 v2, 0x8

    const-string v3, "smartlock_hint_fail"

    invoke-direct {v0, v1, v2, v3}, LX/27W;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/27W;->SMARTLOCK_HINT_FAIL:LX/27W;

    .line 372946
    const/16 v0, 0x9

    new-array v0, v0, [LX/27W;

    sget-object v1, LX/27W;->SMARTLOCK_REQUEST:LX/27W;

    aput-object v1, v0, v4

    sget-object v1, LX/27W;->SMARTLOCK_SUCCESS:LX/27W;

    aput-object v1, v0, v5

    sget-object v1, LX/27W;->SMARTLOCK_RESOLVE:LX/27W;

    aput-object v1, v0, v6

    sget-object v1, LX/27W;->SMARTLOCK_RESOLVE_SUCCESS:LX/27W;

    aput-object v1, v0, v7

    sget-object v1, LX/27W;->SMARTLOCK_RESOLVE_FAIL:LX/27W;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/27W;->SMARTLOCK_DELETE:LX/27W;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/27W;->SMARTLOCK_HINT_REQUEST:LX/27W;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/27W;->SMARTLOCK_HINT_SUCCESS:LX/27W;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/27W;->SMARTLOCK_HINT_FAIL:LX/27W;

    aput-object v2, v0, v1

    sput-object v0, LX/27W;->$VALUES:[LX/27W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 372934
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 372935
    iput-object p3, p0, LX/27W;->mEventName:Ljava/lang/String;

    .line 372936
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/27W;
    .locals 1

    .prologue
    .line 372947
    const-class v0, LX/27W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/27W;

    return-object v0
.end method

.method public static values()[LX/27W;
    .locals 1

    .prologue
    .line 372933
    sget-object v0, LX/27W;->$VALUES:[LX/27W;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/27W;

    return-object v0
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 372932
    iget-object v0, p0, LX/27W;->mEventName:Ljava/lang/String;

    return-object v0
.end method
