.class public final LX/2Dw;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/service/AppSession;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/service/AppSession;)V
    .locals 0

    .prologue
    .line 384825
    iput-object p1, p0, LX/2Dw;->a:Lcom/facebook/katana/service/AppSession;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 384826
    iget-object v0, p0, LX/2Dw;->a:Lcom/facebook/katana/service/AppSession;

    iget-object v0, v0, Lcom/facebook/katana/service/AppSession;->r:LX/2A0;

    .line 384827
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.orca.login.AuthStateMachineMonitor.AUTH_FAILED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 384828
    if-eqz p1, :cond_0

    .line 384829
    const-string v2, "AUTH_FAILED_THROWABLE"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 384830
    :cond_0
    iget-object v2, v0, LX/2A0;->a:LX/0Xp;

    invoke-virtual {v2, v1}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 384831
    iget-object v0, p0, LX/2Dw;->a:Lcom/facebook/katana/service/AppSession;

    sget-object v1, LX/2A1;->STATUS_LOGGED_OUT:LX/2A1;

    invoke-static {v0, v1}, Lcom/facebook/katana/service/AppSession;->a$redex0(Lcom/facebook/katana/service/AppSession;LX/2A1;)V

    .line 384832
    iget-object v0, p0, LX/2Dw;->a:Lcom/facebook/katana/service/AppSession;

    iget-object v0, v0, Lcom/facebook/katana/service/AppSession;->a:LX/0wb;

    invoke-virtual {v0}, LX/0wb;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/278;

    .line 384833
    invoke-virtual {v0, p1}, LX/278;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 384834
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 384835
    iget-object v0, p0, LX/2Dw;->a:Lcom/facebook/katana/service/AppSession;

    invoke-virtual {v0}, Lcom/facebook/katana/service/AppSession;->e()V

    .line 384836
    return-void
.end method
