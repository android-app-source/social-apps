.class public LX/35E;
.super LX/1ah;
.source ""

# interfaces
.implements LX/1cu;


# static fields
.field public static final a:I

.field private static final c:Landroid/graphics/Rect;


# instance fields
.field public final d:LX/0xi;

.field public e:LX/0wd;

.field public f:I

.field public g:I

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 496896
    const-wide v0, 0x40bd4c0000000000L    # 7500.0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    sput v0, LX/35E;->a:I

    .line 496897
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/35E;->c:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 496934
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/1ah;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 496935
    const/16 v0, 0x11

    iput v0, p0, LX/35E;->f:I

    .line 496936
    new-instance v0, LX/35F;

    invoke-direct {v0, p0}, LX/35F;-><init>(LX/35E;)V

    iput-object v0, p0, LX/35E;->d:LX/0xi;

    .line 496937
    return-void
.end method

.method private a(ILandroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 496926
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, LX/35E;->g:I

    sub-int/2addr v0, v1

    .line 496927
    sget v1, LX/35E;->a:I

    sub-int/2addr v1, p1

    mul-int/2addr v1, v0

    sget v2, LX/35E;->a:I

    div-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 496928
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget v2, p0, LX/35E;->h:I

    sub-int/2addr v1, v2

    .line 496929
    sget v2, LX/35E;->a:I

    sub-int/2addr v2, p1

    mul-int/2addr v2, v1

    sget v3, LX/35E;->a:I

    div-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 496930
    if-lez v0, :cond_0

    if-lez v1, :cond_0

    .line 496931
    iget v2, p0, LX/35E;->f:I

    sget-object v3, LX/35E;->c:Landroid/graphics/Rect;

    invoke-static {v2, v0, v1, p2, v3}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 496932
    invoke-virtual {p0}, LX/35E;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, LX/35E;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget-object v2, LX/35E;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sget-object v3, LX/35E;->c:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sget-object v4, LX/35E;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 496933
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;LX/0wd;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0wd;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 496915
    iget-object v0, p0, LX/35E;->e:LX/0wd;

    if-eqz v0, :cond_0

    .line 496916
    iget-object v0, p0, LX/35E;->e:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->k()LX/0wd;

    .line 496917
    :cond_0
    iput-object p2, p0, LX/35E;->e:LX/0wd;

    .line 496918
    iget-object v0, p0, LX/35E;->e:LX/0wd;

    if-nez v0, :cond_2

    .line 496919
    :goto_0
    invoke-virtual {p0, p1}, LX/1ah;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 496920
    if-eqz p1, :cond_1

    .line 496921
    const/16 v0, 0x1388

    invoke-virtual {p0, v0}, LX/35E;->setLevel(I)Z

    .line 496922
    invoke-virtual {p0}, LX/35E;->getLevel()I

    move-result v0

    invoke-virtual {p0}, LX/35E;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/35E;->a(ILandroid/graphics/Rect;)V

    .line 496923
    :cond_1
    return-void

    .line 496924
    :cond_2
    iget-object v0, p0, LX/35E;->e:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->k()LX/0wd;

    .line 496925
    iget-object v0, p0, LX/35E;->e:LX/0wd;

    iget-object v1, p0, LX/35E;->d:LX/0xi;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 496913
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    .line 496914
    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    if-eq v1, v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/view/View;)Z
    .locals 4

    .prologue
    .line 496907
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 496908
    packed-switch v0, :pswitch_data_0

    .line 496909
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 496910
    :pswitch_1
    iget-object v0, p0, LX/35E;->e:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    .line 496911
    :pswitch_2
    iget-object v0, p0, LX/35E;->e:LX/0wd;

    const-wide v2, 0x3fe99999a0000000L    # 0.800000011920929

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    .line 496912
    :pswitch_3
    iget-object v0, p0, LX/35E;->e:LX/0wd;

    const-wide v2, 0x3ff6666660000000L    # 1.399999976158142

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 496938
    invoke-virtual {p0}, LX/35E;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 496939
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getLevel()I

    move-result v1

    if-eqz v1, :cond_0

    .line 496940
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 496941
    :cond_0
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 2

    .prologue
    .line 496906
    invoke-virtual {p0}, LX/35E;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 2

    .prologue
    .line 496905
    invoke-virtual {p0}, LX/35E;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public final isStateful()Z
    .locals 1

    .prologue
    .line 496904
    const/4 v0, 0x1

    return v0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 496902
    invoke-virtual {p0}, LX/35E;->getLevel()I

    move-result v0

    invoke-direct {p0, v0, p1}, LX/35E;->a(ILandroid/graphics/Rect;)V

    .line 496903
    return-void
.end method

.method public final onLevelChange(I)Z
    .locals 1

    .prologue
    .line 496898
    invoke-virtual {p0}, LX/35E;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 496899
    invoke-virtual {p0}, LX/35E;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/35E;->a(ILandroid/graphics/Rect;)V

    .line 496900
    invoke-virtual {p0}, LX/35E;->invalidateSelf()V

    .line 496901
    const/4 v0, 0x1

    return v0
.end method
