.class public LX/2AD;
.super LX/2AE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2AE",
        "<",
        "Ljava/lang/String;",
        "LX/2nq;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# direct methods
.method public constructor <init>(LX/0Sh;Ljava/lang/String;Ljava/lang/String;LX/0rb;LX/03V;)V
    .locals 7

    .prologue
    .line 376996
    new-instance v1, LX/2AH;

    invoke-direct {v1}, LX/2AH;-><init>()V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/2AE;-><init>(Ljava/util/Comparator;LX/0Sh;Ljava/lang/String;Ljava/lang/String;LX/0rb;LX/03V;)V

    .line 376997
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)LX/2nq;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 377031
    const-string v0, "Can\'t query cache with null cursor"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377032
    sget-object v0, LX/2A7;->f:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 377033
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, LX/2AG;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 377034
    :goto_0
    if-nez v0, :cond_1

    move-object v0, v1

    .line 377035
    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    .line 377036
    goto :goto_0

    .line 377037
    :cond_1
    sget-object v2, LX/2A7;->d:LX/0U1;

    invoke-virtual {v2, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 377038
    sget-object v3, LX/2A7;->e:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 377039
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v4, v2, v6, v7}, LX/BDX;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLStorySeenState;J)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 377040
    sget-object v3, LX/2A7;->g:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 377041
    invoke-static {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->a(LX/2nq;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v4

    invoke-static {v4}, LX/BBE;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)LX/BBE;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/BBE;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/BBE;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/BBE;->b(Ljava/lang/String;)LX/BBE;

    move-result-object v4

    invoke-virtual {v4}, LX/BBE;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v4

    move-object v2, v4

    .line 377042
    move-object v0, v2

    .line 377043
    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 377028
    check-cast p1, LX/2nq;

    .line 377029
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 377030
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 377021
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2AG;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377022
    if-nez v0, :cond_0

    .line 377023
    const/4 v0, 0x0

    .line 377024
    :goto_0
    monitor-exit p0

    return v0

    .line 377025
    :cond_0
    :try_start_1
    invoke-static {v0, p2}, LX/BDX;->a(LX/2nq;I)LX/2nq;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2AG;->b(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377026
    const/4 v0, 0x1

    goto :goto_0

    .line 377027
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z
    .locals 1

    .prologue
    .line 377044
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2AG;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377045
    if-nez v0, :cond_0

    .line 377046
    const/4 v0, 0x0

    .line 377047
    :goto_0
    monitor-exit p0

    return v0

    .line 377048
    :cond_0
    :try_start_1
    invoke-static {v0, p2}, LX/BDX;->a(LX/2nq;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)LX/2nq;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2AG;->b(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377049
    const/4 v0, 0x1

    goto :goto_0

    .line 377050
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Z
    .locals 1

    .prologue
    .line 377014
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2AG;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377015
    if-nez v0, :cond_0

    .line 377016
    const/4 v0, 0x0

    .line 377017
    :goto_0
    monitor-exit p0

    return v0

    .line 377018
    :cond_0
    :try_start_1
    invoke-static {v0, p2}, LX/BDX;->a(LX/2nq;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)LX/2nq;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2AG;->b(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377019
    const/4 v0, 0x1

    goto :goto_0

    .line 377020
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 377005
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2AG;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377006
    if-nez v0, :cond_0

    move v0, v1

    .line 377007
    :goto_0
    monitor-exit p0

    return v0

    .line 377008
    :cond_0
    :try_start_1
    invoke-static {v0, p2}, LX/BDX;->a(LX/2nq;Ljava/lang/String;)LX/2nq;

    move-result-object v0

    .line 377009
    if-eqz v0, :cond_1

    .line 377010
    invoke-virtual {p0, v0}, LX/2AG;->b(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377011
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 377012
    goto :goto_0

    .line 377013
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 376998
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2AG;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376999
    if-nez v0, :cond_0

    .line 377000
    const/4 v0, 0x0

    .line 377001
    :goto_0
    monitor-exit p0

    return v0

    .line 377002
    :cond_0
    :try_start_1
    invoke-static {v0, p2}, LX/BDX;->a(LX/2nq;Z)LX/2nq;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2AG;->b(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377003
    const/4 v0, 0x1

    goto :goto_0

    .line 377004
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
