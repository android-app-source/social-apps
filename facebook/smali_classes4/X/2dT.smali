.class public LX/2dT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 443108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z
    .locals 1
    .param p0    # Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 443109
    if-nez p0, :cond_0

    .line 443110
    const/4 v0, 0x1

    .line 443111
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z
    .locals 1
    .param p0    # Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 443112
    invoke-static {p0}, LX/2dT;->a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443113
    if-nez p0, :cond_1

    .line 443114
    const/4 v0, 0x0

    .line 443115
    :goto_0
    move v0, v0

    .line 443116
    if-eqz v0, :cond_0

    invoke-static {p0}, LX/2dT;->c(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->d()Z

    move-result v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)Z
    .locals 1
    .param p0    # Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 443117
    if-nez p0, :cond_0

    .line 443118
    const/4 v0, 0x0

    .line 443119
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;->c()Z

    move-result v0

    goto :goto_0
.end method
