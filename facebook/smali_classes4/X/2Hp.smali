.class public LX/2Hp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static final b:LX/0Tn;

.field private static volatile m:LX/2Hp;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:LX/0V2;

.field private final f:LX/2Hq;

.field private final g:LX/0VP;

.field public final h:Ljava/util/concurrent/ScheduledExecutorService;

.field public final i:LX/0V3;

.field public final j:Lcom/facebook/common/errorreporting/memory/MemoryDumpScheduler$HprofRunnable;

.field public final k:LX/0SG;

.field public final l:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 391109
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "hprof/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 391110
    sput-object v0, LX/2Hp;->b:LX/0Tn;

    const-string v1, "next/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Hp;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0V2;LX/2Hq;LX/0VP;LX/0V3;Ljava/util/concurrent/ScheduledExecutorService;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .param p7    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391112
    iput-object p6, p0, LX/2Hp;->i:LX/0V3;

    .line 391113
    iput-object p7, p0, LX/2Hp;->h:Ljava/util/concurrent/ScheduledExecutorService;

    .line 391114
    iput-object p8, p0, LX/2Hp;->k:LX/0SG;

    .line 391115
    iput-object p9, p0, LX/2Hp;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 391116
    new-instance v0, Lcom/facebook/common/errorreporting/memory/MemoryDumpScheduler$HprofRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/common/errorreporting/memory/MemoryDumpScheduler$HprofRunnable;-><init>(LX/2Hp;)V

    iput-object v0, p0, LX/2Hp;->j:Lcom/facebook/common/errorreporting/memory/MemoryDumpScheduler$HprofRunnable;

    .line 391117
    iput-object p1, p0, LX/2Hp;->c:Landroid/content/Context;

    .line 391118
    iput-object p2, p0, LX/2Hp;->d:Lcom/facebook/content/SecureContextHelper;

    .line 391119
    iput-object p3, p0, LX/2Hp;->e:LX/0V2;

    .line 391120
    iput-object p4, p0, LX/2Hp;->f:LX/2Hq;

    .line 391121
    iput-object p5, p0, LX/2Hp;->g:LX/0VP;

    .line 391122
    return-void
.end method

.method public static a(LX/0QB;)LX/2Hp;
    .locals 13

    .prologue
    .line 391123
    sget-object v0, LX/2Hp;->m:LX/2Hp;

    if-nez v0, :cond_1

    .line 391124
    const-class v1, LX/2Hp;

    monitor-enter v1

    .line 391125
    :try_start_0
    sget-object v0, LX/2Hp;->m:LX/2Hp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391126
    if-eqz v2, :cond_0

    .line 391127
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391128
    new-instance v3, LX/2Hp;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0V2;->a(LX/0QB;)LX/0V2;

    move-result-object v6

    check-cast v6, LX/0V2;

    .line 391129
    new-instance v8, LX/2Hq;

    invoke-static {v0}, LX/0VP;->a(LX/0QB;)LX/0VP;

    move-result-object v7

    check-cast v7, LX/0VP;

    invoke-direct {v8, v7}, LX/2Hq;-><init>(LX/0VP;)V

    .line 391130
    move-object v7, v8

    .line 391131
    check-cast v7, LX/2Hq;

    invoke-static {v0}, LX/0VP;->a(LX/0QB;)LX/0VP;

    move-result-object v8

    check-cast v8, LX/0VP;

    invoke-static {v0}, LX/0V3;->a(LX/0QB;)LX/0V3;

    move-result-object v9

    check-cast v9, LX/0V3;

    invoke-static {v0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v3 .. v12}, LX/2Hp;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0V2;LX/2Hq;LX/0VP;LX/0V3;Ljava/util/concurrent/ScheduledExecutorService;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 391132
    move-object v0, v3

    .line 391133
    sput-object v0, LX/2Hp;->m:LX/2Hp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391134
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391135
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391136
    :cond_1
    sget-object v0, LX/2Hp;->m:LX/2Hp;

    return-object v0

    .line 391137
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391138
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2Hp;J)V
    .locals 3

    .prologue
    .line 391139
    iget-object v0, p0, LX/2Hp;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 391140
    sget-object v1, LX/2Hp;->a:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 391141
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 391142
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 13

    .prologue
    .line 391143
    iget-object v0, p0, LX/2Hp;->e:LX/0V2;

    .line 391144
    iget-object v1, v0, LX/0V2;->a:LX/0Uh;

    const/16 v2, 0x9e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 391145
    if-eqz v0, :cond_0

    .line 391146
    const-wide/32 v7, 0x5265c00

    .line 391147
    iget-object v3, p0, LX/2Hp;->k:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v11

    .line 391148
    iget-object v3, p0, LX/2Hp;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/2Hp;->a:LX/0Tn;

    add-long v5, v11, v7

    invoke-interface {v3, v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v3

    .line 391149
    const-wide/32 v5, 0xea60

    sub-long/2addr v3, v11

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    .line 391150
    iget-object v3, p0, LX/2Hp;->h:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v4, p0, LX/2Hp;->j:Lcom/facebook/common/errorreporting/memory/MemoryDumpScheduler$HprofRunnable;

    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v3 .. v9}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 391151
    add-long v3, v11, v5

    invoke-static {p0, v3, v4}, LX/2Hp;->a$redex0(LX/2Hp;J)V

    .line 391152
    :cond_0
    iget-object v0, p0, LX/2Hp;->e:LX/0V2;

    invoke-virtual {v0}, LX/0V2;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 391153
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2Hp;->c:Landroid/content/Context;

    const-class v2, Lcom/facebook/common/errorreporting/memory/MemoryDumpUploadService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 391154
    iget-object v1, p0, LX/2Hp;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/2Hp;->c:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 391155
    :goto_0
    return-void

    .line 391156
    :cond_1
    iget-object v0, p0, LX/2Hp;->f:LX/2Hq;

    invoke-static {}, LX/0VP;->b()Ljava/lang/String;

    move-result-object v1

    .line 391157
    iget-object v2, v0, LX/2Hq;->b:LX/0VP;

    sget-object v3, LX/2Hr;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1, v3}, LX/0VP;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)[Ljava/io/File;

    move-result-object v3

    .line 391158
    if-nez v3, :cond_3

    .line 391159
    :cond_2
    goto :goto_0

    .line 391160
    :cond_3
    array-length v2, v3

    const/4 v4, 0x3

    if-le v2, v4, :cond_2

    .line 391161
    invoke-static {v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 391162
    const/4 v2, 0x0

    :goto_1
    array-length v4, v3

    add-int/lit8 v4, v4, -0x3

    if-ge v2, v4, :cond_2

    .line 391163
    aget-object v4, v3, v2

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 391164
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
