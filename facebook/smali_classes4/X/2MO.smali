.class public LX/2MO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2MO;


# instance fields
.field private final a:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "LX/6ed;",
            "LX/FGj;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mTranscodedMediaResources"
    .end annotation
.end field

.field private final b:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "LX/6ed;",
            "LX/FGj;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mTranscodedMediaResourcesPhaseTwo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/32 v2, 0x7b98a000

    .line 396610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396611
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/2MO;->a:LX/0QI;

    .line 396612
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/2MO;->b:LX/0QI;

    .line 396613
    return-void
.end method

.method public static a(LX/0QB;)LX/2MO;
    .locals 3

    .prologue
    .line 396583
    sget-object v0, LX/2MO;->c:LX/2MO;

    if-nez v0, :cond_1

    .line 396584
    const-class v1, LX/2MO;

    monitor-enter v1

    .line 396585
    :try_start_0
    sget-object v0, LX/2MO;->c:LX/2MO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 396586
    if-eqz v2, :cond_0

    .line 396587
    :try_start_1
    new-instance v0, LX/2MO;

    invoke-direct {v0}, LX/2MO;-><init>()V

    .line 396588
    move-object v0, v0

    .line 396589
    sput-object v0, LX/2MO;->c:LX/2MO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396590
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 396591
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 396592
    :cond_1
    sget-object v0, LX/2MO;->c:LX/2MO;

    return-object v0

    .line 396593
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 396594
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 396614
    iget-object v1, p0, LX/2MO;->a:LX/0QI;

    monitor-enter v1

    .line 396615
    :try_start_0
    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v2

    .line 396616
    iget-object v0, p0, LX/2MO;->a:LX/0QI;

    invoke-interface {v0, v2}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGj;

    .line 396617
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 396618
    :goto_0
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 396619
    :cond_0
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v3

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 396620
    iput-object v0, v3, LX/5zn;->g:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 396621
    move-object v0, v3

    .line 396622
    iget-object v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    .line 396623
    iput-object v3, v0, LX/5zn;->o:Ljava/lang/String;

    .line 396624
    move-object v0, v0

    .line 396625
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 396626
    new-instance v3, LX/FGj;

    const/4 v4, 0x1

    invoke-direct {v3, v0, v4}, LX/FGj;-><init>(Lcom/facebook/ui/media/attachments/MediaResource;Z)V

    .line 396627
    iget-object v4, p0, LX/2MO;->a:LX/0QI;

    invoke-interface {v4, v2, v3}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 396628
    :cond_1
    monitor-exit v1

    return-object v0

    .line 396629
    :cond_2
    iget-object v0, v0, LX/FGj;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    goto :goto_0

    .line 396630
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 5
    .param p2    # Lcom/facebook/ui/media/attachments/MediaResource;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 396595
    iget-object v1, p0, LX/2MO;->a:LX/0QI;

    monitor-enter v1

    .line 396596
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    .line 396597
    :goto_0
    :try_start_0
    iget-object v2, p0, LX/2MO;->a:LX/0QI;

    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v3

    new-instance v4, LX/FGj;

    invoke-direct {v4, p2, v0}, LX/FGj;-><init>(Lcom/facebook/ui/media/attachments/MediaResource;Z)V

    invoke-interface {v2, v3, v4}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 396598
    monitor-exit v1

    return-void

    .line 396599
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 396600
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Z)V
    .locals 3

    .prologue
    .line 396601
    if-eqz p2, :cond_0

    .line 396602
    iget-object v1, p0, LX/2MO;->b:LX/0QI;

    monitor-enter v1

    .line 396603
    :try_start_0
    iget-object v0, p0, LX/2MO;->b:LX/0QI;

    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 396604
    monitor-exit v1

    .line 396605
    :goto_0
    return-void

    .line 396606
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 396607
    :cond_0
    iget-object v1, p0, LX/2MO;->a:LX/0QI;

    monitor-enter v1

    .line 396608
    :try_start_1
    iget-object v0, p0, LX/2MO;->a:LX/0QI;

    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 396609
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method public final a(LX/6ed;)Z
    .locals 2

    .prologue
    .line 396571
    iget-object v1, p0, LX/2MO;->a:LX/0QI;

    monitor-enter v1

    .line 396572
    :try_start_0
    iget-object v0, p0, LX/2MO;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGj;

    .line 396573
    if-nez v0, :cond_0

    .line 396574
    const/4 v0, 0x1

    monitor-exit v1

    .line 396575
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, v0, LX/FGj;->b:Z

    monitor-exit v1

    goto :goto_0

    .line 396576
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 5

    .prologue
    .line 396554
    iget-object v1, p0, LX/2MO;->b:LX/0QI;

    monitor-enter v1

    .line 396555
    :try_start_0
    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v2

    .line 396556
    iget-object v0, p0, LX/2MO;->b:LX/0QI;

    invoke-interface {v0, v2}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGj;

    .line 396557
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 396558
    :goto_0
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 396559
    :cond_0
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v3

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 396560
    iput-object v0, v3, LX/5zn;->g:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 396561
    move-object v0, v3

    .line 396562
    iget-object v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    .line 396563
    iput-object v3, v0, LX/5zn;->o:Ljava/lang/String;

    .line 396564
    move-object v0, v0

    .line 396565
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 396566
    new-instance v3, LX/FGj;

    const/4 v4, 0x1

    invoke-direct {v3, v0, v4}, LX/FGj;-><init>(Lcom/facebook/ui/media/attachments/MediaResource;Z)V

    .line 396567
    iget-object v4, p0, LX/2MO;->b:LX/0QI;

    invoke-interface {v4, v2, v3}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 396568
    :cond_1
    monitor-exit v1

    return-object v0

    .line 396569
    :cond_2
    iget-object v0, v0, LX/FGj;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    goto :goto_0

    .line 396570
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 5
    .param p2    # Lcom/facebook/ui/media/attachments/MediaResource;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 396577
    iget-object v1, p0, LX/2MO;->b:LX/0QI;

    monitor-enter v1

    .line 396578
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    .line 396579
    :goto_0
    :try_start_0
    iget-object v2, p0, LX/2MO;->b:LX/0QI;

    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v3

    new-instance v4, LX/FGj;

    invoke-direct {v4, p2, v0}, LX/FGj;-><init>(Lcom/facebook/ui/media/attachments/MediaResource;Z)V

    invoke-interface {v2, v3, v4}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 396580
    monitor-exit v1

    return-void

    .line 396581
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 396582
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
