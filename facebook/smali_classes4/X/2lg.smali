.class public final LX/2lg;
.super LX/0k9;
.source ""

# interfaces
.implements LX/0nz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0k9",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "LX/0nz;"
    }
.end annotation


# instance fields
.field private a:Z

.field private final b:LX/2l5;

.field private final c:Z

.field private final d:LX/0nz;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2l5;ZLX/0nz;)V
    .locals 1

    .prologue
    .line 458222
    invoke-direct {p0, p1}, LX/0k9;-><init>(Landroid/content/Context;)V

    .line 458223
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2lg;->a:Z

    .line 458224
    iput-object p2, p0, LX/2lg;->b:LX/2l5;

    .line 458225
    iput-boolean p3, p0, LX/2lg;->c:Z

    .line 458226
    iput-object p4, p0, LX/2lg;->d:LX/0nz;

    .line 458227
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/bookmark/FetchBookmarksResult;Z)V
    .locals 1

    .prologue
    .line 458217
    iget-boolean v0, p0, LX/0k9;->p:Z

    move v0, v0

    .line 458218
    if-eqz v0, :cond_0

    .line 458219
    iget-object v0, p0, LX/2lg;->d:LX/0nz;

    invoke-interface {v0, p1, p2}, LX/0nz;->a(Lcom/facebook/bookmark/FetchBookmarksResult;Z)V

    .line 458220
    :goto_0
    return-void

    .line 458221
    :cond_0
    invoke-virtual {p0}, LX/0k9;->w()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/bookmark/model/Bookmark;)V
    .locals 1

    .prologue
    .line 458228
    iget-boolean v0, p0, LX/0k9;->p:Z

    move v0, v0

    .line 458229
    if-eqz v0, :cond_0

    .line 458230
    iget-object v0, p0, LX/2lg;->d:LX/0nz;

    invoke-interface {v0, p1}, LX/0nz;->a(Lcom/facebook/bookmark/model/Bookmark;)V

    .line 458231
    :goto_0
    return-void

    .line 458232
    :cond_0
    invoke-virtual {p0}, LX/0k9;->w()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/bookmark/model/BookmarksGroup;)V
    .locals 1

    .prologue
    .line 458212
    iget-boolean v0, p0, LX/0k9;->p:Z

    move v0, v0

    .line 458213
    if-eqz v0, :cond_0

    .line 458214
    iget-object v0, p0, LX/2lg;->d:LX/0nz;

    invoke-interface {v0, p1}, LX/0nz;->a(Lcom/facebook/bookmark/model/BookmarksGroup;)V

    .line 458215
    :goto_0
    return-void

    .line 458216
    :cond_0
    invoke-virtual {p0}, LX/0k9;->w()V

    goto :goto_0
.end method

.method public final g()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 458196
    invoke-super {p0}, LX/0k9;->g()V

    .line 458197
    iget-boolean v0, p0, LX/2lg;->a:Z

    if-nez v0, :cond_0

    .line 458198
    iget-object v0, p0, LX/2lg;->b:LX/2l5;

    invoke-interface {v0, p0}, LX/2l5;->a(LX/0nz;)V

    .line 458199
    iput-boolean v4, p0, LX/2lg;->a:Z

    .line 458200
    :cond_0
    invoke-virtual {p0}, LX/0k9;->t()Z

    move-result v0

    .line 458201
    iget-object v1, p0, LX/2lg;->b:LX/2l5;

    invoke-interface {v1}, LX/2l5;->e()Lcom/facebook/bookmark/FetchBookmarksResult;

    move-result-object v1

    .line 458202
    sget-object v2, LX/2lh;->a:[I

    .line 458203
    iget-object v3, v1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 458204
    invoke-virtual {v3}, LX/0ta;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 458205
    :cond_1
    :goto_0
    return-void

    .line 458206
    :pswitch_0
    iget-object v0, p0, LX/2lg;->b:LX/2l5;

    invoke-interface {v0}, LX/2l5;->c()Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 458207
    :pswitch_1
    iget-boolean v2, p0, LX/2lg;->c:Z

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 458208
    iget-object v0, p0, LX/2lg;->d:LX/0nz;

    invoke-interface {v0, v1, v4}, LX/0nz;->a(Lcom/facebook/bookmark/FetchBookmarksResult;Z)V

    .line 458209
    :cond_2
    iget-object v0, p0, LX/2lg;->b:LX/2l5;

    invoke-interface {v0}, LX/2l5;->a()V

    goto :goto_0

    .line 458210
    :pswitch_2
    if-eqz v0, :cond_1

    .line 458211
    iget-object v0, p0, LX/2lg;->d:LX/0nz;

    invoke-interface {v0, v1, v4}, LX/0nz;->a(Lcom/facebook/bookmark/FetchBookmarksResult;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 458192
    invoke-super {p0}, LX/0k9;->i()V

    .line 458193
    iget-object v0, p0, LX/2lg;->b:LX/2l5;

    invoke-interface {v0, p0}, LX/2l5;->b(LX/0nz;)V

    .line 458194
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2lg;->a:Z

    .line 458195
    return-void
.end method
