.class public LX/3Iz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3J0;


# instance fields
.field private final a:LX/3Iy;

.field private final b:LX/198;

.field private final c:LX/1Pq;

.field private final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/3Iy;LX/1Pq;LX/198;)V
    .locals 0
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/3Iy;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/3Iy;",
            "LX/1Pq;",
            "LX/198;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 547374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547375
    iput-object p2, p0, LX/3Iz;->a:LX/3Iy;

    .line 547376
    iput-object p4, p0, LX/3Iz;->b:LX/198;

    .line 547377
    iput-object p3, p0, LX/3Iz;->c:LX/1Pq;

    .line 547378
    iput-object p1, p0, LX/3Iz;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 547379
    return-void
.end method


# virtual methods
.method public final a(LX/7Jv;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 547380
    iget-object v0, p0, LX/3Iz;->a:LX/3Iy;

    invoke-interface {v0}, LX/3Iy;->a()V

    .line 547381
    iget-object v0, p0, LX/3Iz;->a:LX/3Iy;

    invoke-interface {v0}, LX/3Iy;->f()LX/2oO;

    move-result-object v0

    iget-boolean v1, p1, LX/7Jv;->b:Z

    iget-boolean v2, p1, LX/7Jv;->a:Z

    invoke-virtual {v0, v1, v2}, LX/2oO;->a(ZZ)V

    .line 547382
    iget-object v0, p0, LX/3Iz;->a:LX/3Iy;

    invoke-interface {v0}, LX/3Iy;->e()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 547383
    :goto_0
    return-void

    .line 547384
    :cond_0
    iget-object v0, p0, LX/3Iz;->a:LX/3Iy;

    invoke-interface {v0}, LX/3Iy;->g()LX/2oL;

    move-result-object v0

    iget v1, p1, LX/7Jv;->c:I

    invoke-virtual {v0, v1}, LX/2oL;->a(I)V

    .line 547385
    iget-boolean v0, p1, LX/7Jv;->b:Z

    if-nez v0, :cond_4

    iget-boolean v0, p1, LX/7Jv;->a:Z

    if-nez v0, :cond_4

    iget v0, p1, LX/7Jv;->c:I

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 547386
    if-eqz v0, :cond_3

    .line 547387
    iget-object v0, p0, LX/3Iz;->a:LX/3Iy;

    invoke-interface {v0}, LX/3Iy;->b()V

    .line 547388
    iget-object v0, p0, LX/3Iz;->a:LX/3Iy;

    iget-object v1, p0, LX/3Iz;->b:LX/198;

    iget-object v2, p1, LX/7Jv;->h:LX/04g;

    new-instance v3, LX/7K4;

    iget v4, p1, LX/7Jv;->c:I

    iget v5, p1, LX/7Jv;->d:I

    invoke-direct {v3, v4, v5}, LX/7K4;-><init>(II)V

    invoke-interface {v0, v1, v2, v3}, LX/3Iy;->a(LX/198;LX/04g;LX/7K4;)V

    .line 547389
    :cond_1
    :goto_2
    iget-boolean v0, p1, LX/7Jv;->b:Z

    if-eqz v0, :cond_2

    .line 547390
    iget-object v0, p0, LX/3Iz;->a:LX/3Iy;

    invoke-interface {v0}, LX/3Iy;->c()V

    .line 547391
    :cond_2
    iget-object v0, p0, LX/3Iz;->a:LX/3Iy;

    invoke-interface {v0}, LX/3Iy;->d()V

    goto :goto_0

    .line 547392
    :cond_3
    iget-boolean v0, p1, LX/7Jv;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3Iz;->a:LX/3Iy;

    invoke-interface {v0}, LX/3Iy;->h()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/2v7;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547393
    iget-object v0, p0, LX/3Iz;->a:LX/3Iy;

    invoke-interface {v0}, LX/3Iy;->g()LX/2oL;

    move-result-object v0

    .line 547394
    iput-boolean v3, v0, LX/2oL;->a:Z

    .line 547395
    iget-object v0, p0, LX/3Iz;->c:LX/1Pq;

    new-array v1, v3, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, LX/3Iz;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
