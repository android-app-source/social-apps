.class public LX/2V4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 417114
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "stickers/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 417115
    sput-object v0, LX/2V4;->a:LX/0Tn;

    const-string v1, "popup/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 417116
    sput-object v0, LX/2V4;->b:LX/0Tn;

    const-string v1, "tab_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->c:LX/0Tn;

    .line 417117
    sget-object v0, LX/2V4;->b:LX/0Tn;

    const-string v1, "page"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->d:LX/0Tn;

    .line 417118
    sget-object v0, LX/2V4;->b:LX/0Tn;

    const-string v1, "scroll_position"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->e:LX/0Tn;

    .line 417119
    sget-object v0, LX/2V4;->a:LX/0Tn;

    const-string v1, "store/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 417120
    sput-object v0, LX/2V4;->f:LX/0Tn;

    const-string v1, "last_store_visit_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->g:LX/0Tn;

    .line 417121
    sget-object v0, LX/2V4;->f:LX/0Tn;

    const-string v1, "unseen_pack_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->h:LX/0Tn;

    .line 417122
    sget-object v0, LX/2V4;->a:LX/0Tn;

    const-string v1, "has_downloaded_sticker_pack"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->i:LX/0Tn;

    .line 417123
    sget-object v0, LX/2V4;->a:LX/0Tn;

    const-string v1, "background_task/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 417124
    sput-object v0, LX/2V4;->j:LX/0Tn;

    const-string v1, "unused_file/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->k:LX/0Tn;

    .line 417125
    sget-object v0, LX/2V4;->j:LX/0Tn;

    const-string v1, "last_cleanup"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->l:LX/0Tn;

    .line 417126
    sget-object v0, LX/2V4;->j:LX/0Tn;

    const-string v1, "last_asset_flush_xconfig_ver"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->m:LX/0Tn;

    .line 417127
    sget-object v0, LX/2V4;->j:LX/0Tn;

    const-string v1, "last_asset_flush_check_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->n:LX/0Tn;

    .line 417128
    sget-object v0, LX/2V4;->j:LX/0Tn;

    const-string v1, "last_asset_flush_completion_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->o:LX/0Tn;

    .line 417129
    sget-object v0, LX/2V4;->j:LX/0Tn;

    const-string v1, "assets_download/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->p:LX/0Tn;

    .line 417130
    sget-object v0, LX/2V4;->b:LX/0Tn;

    const-string v1, "sticker_search_has_opened"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->q:LX/0Tn;

    .line 417131
    sget-object v0, LX/2V4;->a:LX/0Tn;

    const-string v1, "sticker_in_comments_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->r:LX/0Tn;

    .line 417132
    sget-object v0, LX/2V4;->a:LX/0Tn;

    const-string v1, "last_time_sticker_keyboard_opened"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2V4;->s:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 417133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
