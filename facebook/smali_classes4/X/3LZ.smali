.class public final enum LX/3LZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3LZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3LZ;

.field public static final enum ALL_CONTACTS:LX/3LZ;

.field public static final enum ALL_CONTACTS_WITH_CAP:LX/3LZ;

.field public static final enum ALL_FRIENDS_COEFFICIENT_SORTED:LX/3LZ;

.field public static final enum ALL_FRIENDS_NAME_SORTED:LX/3LZ;

.field public static final enum FAVORITE_FRIENDS:LX/3LZ;

.field public static final enum FRIENDS_ON_MESSENGER:LX/3LZ;

.field public static final enum NOT_ON_MESSENGER_FRIENDS:LX/3LZ;

.field public static final enum ONLINE_FRIENDS:LX/3LZ;

.field public static final enum ONLINE_FRIENDS_COEFFICIENT_SORTED:LX/3LZ;

.field public static final enum PAGES:LX/3LZ;

.field public static final enum PHAT_CONTACTS:LX/3LZ;

.field public static final enum PHONE_CONTACTS:LX/3LZ;

.field public static final enum PROMOTIONAL_CONTACTS:LX/3LZ;

.field public static final enum PSTN_CALL_LOG_FRIENDS:LX/3LZ;

.field public static final enum RECENT_CALLS:LX/3LZ;

.field public static final enum RTC_CALLLOGS:LX/3LZ;

.field public static final enum RTC_ONGOING_GROUP_CALLS:LX/3LZ;

.field public static final enum RTC_VOICEMAILS:LX/3LZ;

.field public static final enum SMS_INVITE_ALL_PHONE_CONTACTS:LX/3LZ;

.field public static final enum SMS_INVITE_MOBILE_CONTACTS:LX/3LZ;

.field public static final enum TOP_CONTACTS:LX/3LZ;

.field public static final enum TOP_FRIENDS:LX/3LZ;

.field public static final enum TOP_FRIENDS_ON_MESSENGER:LX/3LZ;

.field public static final enum TOP_PHONE_CONTACTS:LX/3LZ;

.field public static final enum TOP_PHONE_CONTACTS_NULL_STATE:LX/3LZ;

.field public static final enum TOP_PUSHABLE_FRIENDS:LX/3LZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 550778
    new-instance v0, LX/3LZ;

    const-string v1, "FAVORITE_FRIENDS"

    invoke-direct {v0, v1, v3}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->FAVORITE_FRIENDS:LX/3LZ;

    .line 550779
    new-instance v0, LX/3LZ;

    const-string v1, "TOP_FRIENDS"

    invoke-direct {v0, v1, v4}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->TOP_FRIENDS:LX/3LZ;

    .line 550780
    new-instance v0, LX/3LZ;

    const-string v1, "ONLINE_FRIENDS"

    invoke-direct {v0, v1, v5}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->ONLINE_FRIENDS:LX/3LZ;

    .line 550781
    new-instance v0, LX/3LZ;

    const-string v1, "ONLINE_FRIENDS_COEFFICIENT_SORTED"

    invoke-direct {v0, v1, v6}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->ONLINE_FRIENDS_COEFFICIENT_SORTED:LX/3LZ;

    .line 550782
    new-instance v0, LX/3LZ;

    const-string v1, "FRIENDS_ON_MESSENGER"

    invoke-direct {v0, v1, v7}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->FRIENDS_ON_MESSENGER:LX/3LZ;

    .line 550783
    new-instance v0, LX/3LZ;

    const-string v1, "TOP_FRIENDS_ON_MESSENGER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->TOP_FRIENDS_ON_MESSENGER:LX/3LZ;

    .line 550784
    new-instance v0, LX/3LZ;

    const-string v1, "NOT_ON_MESSENGER_FRIENDS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->NOT_ON_MESSENGER_FRIENDS:LX/3LZ;

    .line 550785
    new-instance v0, LX/3LZ;

    const-string v1, "PHAT_CONTACTS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->PHAT_CONTACTS:LX/3LZ;

    .line 550786
    new-instance v0, LX/3LZ;

    const-string v1, "TOP_CONTACTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->TOP_CONTACTS:LX/3LZ;

    .line 550787
    new-instance v0, LX/3LZ;

    const-string v1, "ALL_FRIENDS_COEFFICIENT_SORTED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->ALL_FRIENDS_COEFFICIENT_SORTED:LX/3LZ;

    .line 550788
    new-instance v0, LX/3LZ;

    const-string v1, "ALL_FRIENDS_NAME_SORTED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->ALL_FRIENDS_NAME_SORTED:LX/3LZ;

    .line 550789
    new-instance v0, LX/3LZ;

    const-string v1, "RECENT_CALLS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->RECENT_CALLS:LX/3LZ;

    .line 550790
    new-instance v0, LX/3LZ;

    const-string v1, "TOP_PUSHABLE_FRIENDS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->TOP_PUSHABLE_FRIENDS:LX/3LZ;

    .line 550791
    new-instance v0, LX/3LZ;

    const-string v1, "SMS_INVITE_ALL_PHONE_CONTACTS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->SMS_INVITE_ALL_PHONE_CONTACTS:LX/3LZ;

    .line 550792
    new-instance v0, LX/3LZ;

    const-string v1, "SMS_INVITE_MOBILE_CONTACTS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->SMS_INVITE_MOBILE_CONTACTS:LX/3LZ;

    .line 550793
    new-instance v0, LX/3LZ;

    const-string v1, "TOP_PHONE_CONTACTS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->TOP_PHONE_CONTACTS:LX/3LZ;

    .line 550794
    new-instance v0, LX/3LZ;

    const-string v1, "TOP_PHONE_CONTACTS_NULL_STATE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->TOP_PHONE_CONTACTS_NULL_STATE:LX/3LZ;

    .line 550795
    new-instance v0, LX/3LZ;

    const-string v1, "PHONE_CONTACTS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->PHONE_CONTACTS:LX/3LZ;

    .line 550796
    new-instance v0, LX/3LZ;

    const-string v1, "ALL_CONTACTS_WITH_CAP"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->ALL_CONTACTS_WITH_CAP:LX/3LZ;

    .line 550797
    new-instance v0, LX/3LZ;

    const-string v1, "ALL_CONTACTS"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->ALL_CONTACTS:LX/3LZ;

    .line 550798
    new-instance v0, LX/3LZ;

    const-string v1, "PROMOTIONAL_CONTACTS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->PROMOTIONAL_CONTACTS:LX/3LZ;

    .line 550799
    new-instance v0, LX/3LZ;

    const-string v1, "RTC_CALLLOGS"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->RTC_CALLLOGS:LX/3LZ;

    .line 550800
    new-instance v0, LX/3LZ;

    const-string v1, "RTC_ONGOING_GROUP_CALLS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->RTC_ONGOING_GROUP_CALLS:LX/3LZ;

    .line 550801
    new-instance v0, LX/3LZ;

    const-string v1, "RTC_VOICEMAILS"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->RTC_VOICEMAILS:LX/3LZ;

    .line 550802
    new-instance v0, LX/3LZ;

    const-string v1, "PAGES"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->PAGES:LX/3LZ;

    .line 550803
    new-instance v0, LX/3LZ;

    const-string v1, "PSTN_CALL_LOG_FRIENDS"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/3LZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3LZ;->PSTN_CALL_LOG_FRIENDS:LX/3LZ;

    .line 550804
    const/16 v0, 0x1a

    new-array v0, v0, [LX/3LZ;

    sget-object v1, LX/3LZ;->FAVORITE_FRIENDS:LX/3LZ;

    aput-object v1, v0, v3

    sget-object v1, LX/3LZ;->TOP_FRIENDS:LX/3LZ;

    aput-object v1, v0, v4

    sget-object v1, LX/3LZ;->ONLINE_FRIENDS:LX/3LZ;

    aput-object v1, v0, v5

    sget-object v1, LX/3LZ;->ONLINE_FRIENDS_COEFFICIENT_SORTED:LX/3LZ;

    aput-object v1, v0, v6

    sget-object v1, LX/3LZ;->FRIENDS_ON_MESSENGER:LX/3LZ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3LZ;->TOP_FRIENDS_ON_MESSENGER:LX/3LZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3LZ;->NOT_ON_MESSENGER_FRIENDS:LX/3LZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3LZ;->PHAT_CONTACTS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3LZ;->TOP_CONTACTS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/3LZ;->ALL_FRIENDS_COEFFICIENT_SORTED:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/3LZ;->ALL_FRIENDS_NAME_SORTED:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/3LZ;->RECENT_CALLS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/3LZ;->TOP_PUSHABLE_FRIENDS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/3LZ;->SMS_INVITE_ALL_PHONE_CONTACTS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/3LZ;->SMS_INVITE_MOBILE_CONTACTS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/3LZ;->TOP_PHONE_CONTACTS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/3LZ;->TOP_PHONE_CONTACTS_NULL_STATE:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/3LZ;->PHONE_CONTACTS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/3LZ;->ALL_CONTACTS_WITH_CAP:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/3LZ;->ALL_CONTACTS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/3LZ;->PROMOTIONAL_CONTACTS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/3LZ;->RTC_CALLLOGS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/3LZ;->RTC_ONGOING_GROUP_CALLS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/3LZ;->RTC_VOICEMAILS:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/3LZ;->PAGES:LX/3LZ;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/3LZ;->PSTN_CALL_LOG_FRIENDS:LX/3LZ;

    aput-object v2, v0, v1

    sput-object v0, LX/3LZ;->$VALUES:[LX/3LZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 550805
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3LZ;
    .locals 1

    .prologue
    .line 550777
    const-class v0, LX/3LZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3LZ;

    return-object v0
.end method

.method public static values()[LX/3LZ;
    .locals 1

    .prologue
    .line 550776
    sget-object v0, LX/3LZ;->$VALUES:[LX/3LZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3LZ;

    return-object v0
.end method
