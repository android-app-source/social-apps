.class public final LX/2Jk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Jg;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 393191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393192
    new-instance v0, LX/2Jg;

    invoke-direct {v0}, LX/2Jg;-><init>()V

    iput-object v0, p0, LX/2Jk;->a:LX/2Jg;

    .line 393193
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2Jk;->b:Ljava/util/Set;

    .line 393194
    return-void
.end method

.method private a(Ljava/lang/Enum;)LX/2Jk;
    .locals 3
    .param p1    # Ljava/lang/Enum;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<*>;)",
            "LX/2Jk;"
        }
    .end annotation

    .prologue
    .line 393185
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 393186
    iget-object v1, p0, LX/2Jk;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 393187
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "has been set up. More than one value is not allowed from the same state type"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 393188
    :cond_0
    iget-object v1, p0, LX/2Jk;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 393189
    iget-object v0, p0, LX/2Jk;->a:LX/2Jg;

    invoke-virtual {v0, p1}, LX/2Jg;->a(Ljava/lang/Enum;)V

    .line 393190
    return-object p0
.end method


# virtual methods
.method public final a(LX/2Im;)LX/2Jk;
    .locals 1
    .param p1    # LX/2Im;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 393184
    invoke-direct {p0, p1}, LX/2Jk;->a(Ljava/lang/Enum;)LX/2Jk;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2Jh;)LX/2Jk;
    .locals 1
    .param p1    # LX/2Jh;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 393183
    invoke-direct {p0, p1}, LX/2Jk;->a(Ljava/lang/Enum;)LX/2Jk;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2Ji;)LX/2Jk;
    .locals 1
    .param p1    # LX/2Ji;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 393182
    invoke-direct {p0, p1}, LX/2Jk;->a(Ljava/lang/Enum;)LX/2Jk;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2Jq;)LX/2Jk;
    .locals 1
    .param p1    # LX/2Jq;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 393181
    invoke-direct {p0, p1}, LX/2Jk;->a(Ljava/lang/Enum;)LX/2Jk;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/2Jl;
    .locals 3

    .prologue
    .line 393180
    new-instance v0, LX/2Jl;

    iget-object v1, p0, LX/2Jk;->a:LX/2Jg;

    invoke-direct {v0, v1}, LX/2Jl;-><init>(LX/2Jg;)V

    return-object v0
.end method
