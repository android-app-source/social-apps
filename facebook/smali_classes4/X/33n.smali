.class public LX/33n;
.super LX/2h2;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h2;",
        "LX/0e6",
        "<",
        "Lcom/facebook/zero/sdk/request/ZeroOptinParams;",
        "Lcom/facebook/zero/sdk/request/ZeroOptinResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 494209
    const-class v0, LX/33n;

    sput-object v0, LX/33n;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494192
    invoke-direct {p0}, LX/2h2;-><init>()V

    .line 494193
    iput-object p1, p0, LX/33n;->b:LX/0lC;

    .line 494194
    return-void
.end method

.method private static b(Lcom/facebook/zero/sdk/request/ZeroOptinParams;)Ljava/util/List;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/sdk/request/ZeroOptinParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 494204
    invoke-static {p0}, LX/2h2;->a(Lcom/facebook/zero/sdk/request/ZeroRequestBaseParams;)Ljava/util/List;

    move-result-object v0

    .line 494205
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "enc_phone"

    .line 494206
    iget-object v3, p0, Lcom/facebook/zero/sdk/request/ZeroOptinParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 494207
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494208
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 494201
    check-cast p1, Lcom/facebook/zero/sdk/request/ZeroOptinParams;

    .line 494202
    invoke-static {p1}, LX/33n;->b(Lcom/facebook/zero/sdk/request/ZeroOptinParams;)Ljava/util/List;

    move-result-object v4

    .line 494203
    new-instance v0, LX/14N;

    const-string v1, "zeroOptin"

    const-string v2, "GET"

    const-string v3, "method/mobile.zeroOptin"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 494195
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 494196
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 494197
    iget-object v1, p0, LX/33n;->b:LX/0lC;

    invoke-virtual {v0}, LX/0lF;->c()LX/15w;

    move-result-object v0

    iget-object v2, p0, LX/33n;->b:LX/0lC;

    .line 494198
    iget-object v3, v2, LX/0lC;->_typeFactory:LX/0li;

    move-object v2, v3

    .line 494199
    const-class v3, Lcom/facebook/zero/sdk/request/ZeroOptinResult;

    invoke-virtual {v2, v3}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(LX/15w;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/ZeroOptinResult;

    .line 494200
    return-object v0
.end method
