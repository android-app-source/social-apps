.class public LX/28n;
.super LX/16B;
.source ""


# instance fields
.field private final a:LX/284;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1fJ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0v1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/284;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/284;",
            "LX/0Ot",
            "<",
            "LX/1fJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0v1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374775
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 374776
    iput-object p1, p0, LX/28n;->a:LX/284;

    .line 374777
    iput-object p2, p0, LX/28n;->b:LX/0Ot;

    .line 374778
    iput-object p3, p0, LX/28n;->c:LX/0Ot;

    .line 374779
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 1

    .prologue
    .line 374780
    iget-object v0, p0, LX/28n;->a:LX/284;

    invoke-virtual {v0}, LX/284;->d()V

    .line 374781
    iget-object v0, p0, LX/28n;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fJ;

    invoke-virtual {v0}, LX/1fJ;->a()V

    .line 374782
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 374783
    iget-object v0, p0, LX/28n;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fJ;

    invoke-virtual {v0}, LX/1fJ;->b()V

    .line 374784
    iget-object v0, p0, LX/28n;->a:LX/284;

    invoke-virtual {v0}, LX/284;->c()V

    .line 374785
    iget-object v0, p0, LX/28n;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->h()V

    .line 374786
    return-void
.end method
