.class public final enum LX/2sk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2sk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2sk;

.field public static final enum LOCATION_UNAVAILABLE:LX/2sk;

.field public static final enum PERMISSION_DENIED:LX/2sk;

.field public static final enum TIMEOUT:LX/2sk;

.field public static final enum UNKNOWN_ERROR:LX/2sk;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 473878
    new-instance v0, LX/2sk;

    const-string v1, "LOCATION_UNAVAILABLE"

    invoke-direct {v0, v1, v2}, LX/2sk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2sk;->LOCATION_UNAVAILABLE:LX/2sk;

    .line 473879
    new-instance v0, LX/2sk;

    const-string v1, "TIMEOUT"

    invoke-direct {v0, v1, v3}, LX/2sk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2sk;->TIMEOUT:LX/2sk;

    .line 473880
    new-instance v0, LX/2sk;

    const-string v1, "UNKNOWN_ERROR"

    invoke-direct {v0, v1, v4}, LX/2sk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2sk;->UNKNOWN_ERROR:LX/2sk;

    .line 473881
    new-instance v0, LX/2sk;

    const-string v1, "PERMISSION_DENIED"

    invoke-direct {v0, v1, v5}, LX/2sk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2sk;->PERMISSION_DENIED:LX/2sk;

    .line 473882
    const/4 v0, 0x4

    new-array v0, v0, [LX/2sk;

    sget-object v1, LX/2sk;->LOCATION_UNAVAILABLE:LX/2sk;

    aput-object v1, v0, v2

    sget-object v1, LX/2sk;->TIMEOUT:LX/2sk;

    aput-object v1, v0, v3

    sget-object v1, LX/2sk;->UNKNOWN_ERROR:LX/2sk;

    aput-object v1, v0, v4

    sget-object v1, LX/2sk;->PERMISSION_DENIED:LX/2sk;

    aput-object v1, v0, v5

    sput-object v0, LX/2sk;->$VALUES:[LX/2sk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 473883
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2sk;
    .locals 1

    .prologue
    .line 473884
    const-class v0, LX/2sk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2sk;

    return-object v0
.end method

.method public static values()[LX/2sk;
    .locals 1

    .prologue
    .line 473885
    sget-object v0, LX/2sk;->$VALUES:[LX/2sk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2sk;

    return-object v0
.end method
