.class public final LX/3PD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 561891
    const-wide/16 v8, 0x0

    .line 561892
    const-wide/16 v6, 0x0

    .line 561893
    const-wide/16 v4, 0x0

    .line 561894
    const/4 v2, 0x0

    .line 561895
    const/4 v1, 0x0

    .line 561896
    const/4 v0, 0x0

    .line 561897
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v10, :cond_8

    .line 561898
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 561899
    const/4 v0, 0x0

    .line 561900
    :goto_0
    return v0

    .line 561901
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_4

    .line 561902
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 561903
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 561904
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 561905
    const-string v4, "accuracy_meters"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 561906
    const/4 v0, 0x1

    .line 561907
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 561908
    :cond_1
    const-string v4, "latitude"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 561909
    const/4 v0, 0x1

    .line 561910
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v7, v0

    move-wide v10, v4

    goto :goto_1

    .line 561911
    :cond_2
    const-string v4, "longitude"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 561912
    const/4 v0, 0x1

    .line 561913
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto :goto_1

    .line 561914
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 561915
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 561916
    if-eqz v1, :cond_5

    .line 561917
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 561918
    :cond_5
    if-eqz v7, :cond_6

    .line 561919
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 561920
    :cond_6
    if-eqz v6, :cond_7

    .line 561921
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 561922
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_8
    move-wide v10, v6

    move v7, v1

    move v6, v0

    move v1, v2

    move-wide v2, v8

    move-wide v8, v4

    goto :goto_1
.end method
