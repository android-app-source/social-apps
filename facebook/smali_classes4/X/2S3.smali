.class public LX/2S3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/String;

.field private static volatile k:LX/2S3;


# instance fields
.field public final a:I

.field private final c:LX/0SG;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/ECY;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/03V;

.field private i:Z

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 410559
    const-class v0, LX/2S3;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2S3;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0Or;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/03V;)V
    .locals 1
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/ECY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410551
    const/4 v0, 0x1

    iput v0, p0, LX/2S3;->a:I

    .line 410552
    iput-object p1, p0, LX/2S3;->c:LX/0SG;

    .line 410553
    iput-object p2, p0, LX/2S3;->e:LX/0Or;

    .line 410554
    iput-object p3, p0, LX/2S3;->f:LX/0Ot;

    .line 410555
    iput-object p4, p0, LX/2S3;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 410556
    iput-object p5, p0, LX/2S3;->g:LX/0Ot;

    .line 410557
    iput-object p6, p0, LX/2S3;->h:LX/03V;

    .line 410558
    return-void
.end method

.method public static a(LX/0QB;)LX/2S3;
    .locals 10

    .prologue
    .line 410537
    sget-object v0, LX/2S3;->k:LX/2S3;

    if-nez v0, :cond_1

    .line 410538
    const-class v1, LX/2S3;

    monitor-enter v1

    .line 410539
    :try_start_0
    sget-object v0, LX/2S3;->k:LX/2S3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 410540
    if-eqz v2, :cond_0

    .line 410541
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 410542
    new-instance v3, LX/2S3;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    const/16 v5, 0x324e

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2ba

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v8, 0x1430

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-direct/range {v3 .. v9}, LX/2S3;-><init>(LX/0SG;LX/0Or;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/03V;)V

    .line 410543
    move-object v0, v3

    .line 410544
    sput-object v0, LX/2S3;->k:LX/2S3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410545
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 410546
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 410547
    :cond_1
    sget-object v0, LX/2S3;->k:LX/2S3;

    return-object v0

    .line 410548
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 410549
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/rtc/campon/RtcCamperModel;J)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 410527
    if-nez p0, :cond_1

    .line 410528
    :cond_0
    :goto_0
    return v0

    .line 410529
    :cond_1
    iget v1, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mCampType:I

    .line 410530
    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    :cond_2
    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 410531
    if-eqz v1, :cond_0

    .line 410532
    iget-wide v2, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 410533
    iget-wide v2, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mStartTimeMs:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mWaitTimeMs:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 410534
    iget-wide v2, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mStartTimeMs:J

    iget-wide v4, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mWaitTimeMs:J

    add-long/2addr v2, v4

    cmp-long v1, v2, p1

    if-ltz v1, :cond_0

    .line 410535
    iget-wide v2, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mWaitTimeMs:J

    add-long/2addr v2, p1

    iget-wide v4, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mStartTimeMs:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 410536
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static a$redex0(LX/2S3;Lcom/facebook/rtc/campon/RtcCamperModel;)Z
    .locals 13

    .prologue
    const-wide/16 v8, 0x7530

    const/4 v5, 0x0

    .line 410522
    iget v0, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mCampType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 410523
    iget-object v0, p0, LX/2S3;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECY;

    iget-wide v1, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerId:J

    iget-object v3, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerFirstName:Ljava/lang/String;

    iget-object v4, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerName:Ljava/lang/String;

    iget-wide v6, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mStartTimeMs:J

    iget-wide v10, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mWaitTimeMs:J

    iget-object v12, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mCampTrigger:Ljava/lang/String;

    invoke-virtual/range {v0 .. v12}, LX/ECY;->a(JLjava/lang/String;Ljava/lang/String;ZJJJLjava/lang/String;)Z

    move-result v5

    .line 410524
    :cond_0
    :goto_0
    return v5

    .line 410525
    :cond_1
    iget v0, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mCampType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 410526
    iget-object v0, p0, LX/2S3;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECY;

    iget-wide v1, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerId:J

    iget-object v3, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerFirstName:Ljava/lang/String;

    iget-object v4, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerName:Ljava/lang/String;

    const/4 v5, 0x1

    iget-wide v6, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mStartTimeMs:J

    iget-wide v10, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mWaitTimeMs:J

    iget-object v12, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mCampTrigger:Ljava/lang/String;

    invoke-virtual/range {v0 .. v12}, LX/ECY;->a(JLjava/lang/String;Ljava/lang/String;ZJJJLjava/lang/String;)Z

    move-result v5

    goto :goto_0
.end method

.method private static b(LX/2S3;Lcom/facebook/rtc/campon/RtcCamperModel;)V
    .locals 4

    .prologue
    .line 410481
    sget-object v0, LX/2S4;->O:LX/0Tn;

    iget-wide v2, p1, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 410482
    :try_start_0
    iget-object v1, p0, LX/2S3;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    iget-object v1, p0, LX/2S3;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-virtual {v1, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 410483
    :goto_0
    return-void

    .line 410484
    :catch_0
    move-exception v0

    .line 410485
    iget-object v1, p0, LX/2S3;->h:LX/03V;

    const-string v2, "RtcCamperStore"

    const-string v3, "Error encountered in writing the RtcCamperModel from FbSharedPreferences"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IJLjava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 12

    .prologue
    .line 410520
    new-instance v0, Lcom/facebook/rtc/campon/RtcCamperModel;

    move v1, p1

    move-wide v2, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lcom/facebook/rtc/campon/RtcCamperModel;-><init>(IJLjava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    invoke-static {p0, v0}, LX/2S3;->b(LX/2S3;Lcom/facebook/rtc/campon/RtcCamperModel;)V

    .line 410521
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 410515
    sget-object v0, LX/2S4;->O:LX/0Tn;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 410516
    iget-object v1, p0, LX/2S3;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 410517
    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 410518
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 410519
    return-void
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 410514
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2S3;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized init()V
    .locals 12

    .prologue
    .line 410486
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, LX/2S3;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 410487
    :goto_0
    monitor-exit p0

    return-void

    .line 410488
    :cond_0
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, LX/2S3;->i:Z

    .line 410489
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 410490
    iget-object v2, p0, LX/2S3;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v8

    .line 410491
    const/4 v5, 0x0

    .line 410492
    iget-object v2, p0, LX/2S3;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2S4;->O:LX/0Tn;

    invoke-interface {v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/Map$Entry;

    move-object v3, v0

    .line 410493
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410494
    :try_start_2
    iget-object v4, p0, LX/2S3;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0lC;

    const-class v10, Lcom/facebook/rtc/campon/RtcCamperModel;

    invoke-virtual {v4, v2, v10}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/rtc/campon/RtcCamperModel;

    .line 410495
    invoke-static {v2, v8, v9}, LX/2S3;->a(Lcom/facebook/rtc/campon/RtcCamperModel;J)Z

    move-result v4

    if-nez v4, :cond_1

    .line 410496
    if-nez v5, :cond_6

    .line 410497
    iget-object v2, p0, LX/2S3;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 410498
    :goto_2
    :try_start_3
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Tn;

    invoke-interface {v4, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v2, v4

    :goto_3
    move-object v5, v2

    .line 410499
    goto :goto_1

    .line 410500
    :cond_1
    :try_start_4
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 410501
    goto :goto_1

    .line 410502
    :catch_0
    move-exception v2

    .line 410503
    :goto_4
    :try_start_5
    iget-object v5, p0, LX/2S3;->h:LX/03V;

    const-string v10, "RtcCamperStore"

    const-string v11, "Error encountered in reading the RtcCamperModel from FbSharedPreferences"

    invoke-virtual {v5, v10, v11, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 410504
    if-nez v4, :cond_2

    .line 410505
    iget-object v2, p0, LX/2S3;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    .line 410506
    :cond_2
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Tn;

    invoke-interface {v4, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-object v2, v4

    goto :goto_3

    .line 410507
    :cond_3
    if-eqz v5, :cond_4

    .line 410508
    invoke-interface {v5}, LX/0hN;->commit()V

    .line 410509
    :cond_4
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 410510
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/2S3;->j:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 410511
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 410512
    :cond_5
    :try_start_6
    iget-object v2, p0, LX/2S3;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/facebook/rtc/campon/RtcCamperStore$1;

    invoke-direct {v3, p0, v6}, Lcom/facebook/rtc/campon/RtcCamperStore$1;-><init>(LX/2S3;Ljava/util/List;)V

    const v4, 0x1976c46e

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 410513
    :catch_1
    move-exception v2

    move-object v4, v5

    goto :goto_4

    :cond_6
    move-object v4, v5

    goto :goto_2
.end method
