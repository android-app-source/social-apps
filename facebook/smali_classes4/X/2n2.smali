.class public LX/2n2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2n2;


# instance fields
.field public final a:LX/0aG;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0aG;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 462492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462493
    iput-object p1, p0, LX/2n2;->a:LX/0aG;

    .line 462494
    iput-object p2, p0, LX/2n2;->b:LX/0Or;

    .line 462495
    return-void
.end method

.method public static a(LX/0QB;)LX/2n2;
    .locals 5

    .prologue
    .line 462496
    sget-object v0, LX/2n2;->c:LX/2n2;

    if-nez v0, :cond_1

    .line 462497
    const-class v1, LX/2n2;

    monitor-enter v1

    .line 462498
    :try_start_0
    sget-object v0, LX/2n2;->c:LX/2n2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 462499
    if-eqz v2, :cond_0

    .line 462500
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 462501
    new-instance v4, LX/2n2;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    const/16 p0, 0x15e7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/2n2;-><init>(LX/0aG;LX/0Or;)V

    .line 462502
    move-object v0, v4

    .line 462503
    sput-object v0, LX/2n2;->c:LX/2n2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 462504
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 462505
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 462506
    :cond_1
    sget-object v0, LX/2n2;->c:LX/2n2;

    return-object v0

    .line 462507
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 462508
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/net/Uri$Builder;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 462509
    invoke-virtual {p1, p2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 462510
    if-eqz v0, :cond_0

    .line 462511
    invoke-virtual {p0, p2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 462512
    :cond_0
    return-void
.end method
