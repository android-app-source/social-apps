.class public final LX/3HX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/157;


# instance fields
.field public final synthetic a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V
    .locals 0

    .prologue
    .line 543577
    iput-object p1, p0, LX/3HX;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 543583
    iget-object v0, p0, LX/3HX;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/3HX;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    .line 543584
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e:LX/3H1;

    invoke-virtual {v2, v1}, LX/3H1;->a(Ljava/lang/String;)V

    .line 543585
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->l:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 543586
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->n:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 543587
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->m:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 543588
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 543589
    iget-object v0, p0, LX/3HX;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->p:LX/14v;

    iget-object v1, p0, LX/3HX;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    iget-object v2, p0, LX/3HX;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v2, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->x:LX/157;

    invoke-virtual {v0, v1, v2}, LX/14v;->a(Ljava/lang/String;LX/157;)V

    .line 543590
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 543581
    invoke-direct {p0}, LX/3HX;->a()V

    .line 543582
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V
    .locals 1
    .param p3    # Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 543578
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne p2, v0, :cond_0

    .line 543579
    invoke-direct {p0}, LX/3HX;->a()V

    .line 543580
    :cond_0
    return-void
.end method
