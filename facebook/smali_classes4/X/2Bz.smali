.class public LX/2Bz;
.super LX/16B;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile i:LX/2Bz;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/12x;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2Gb;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Uh;

.field private final g:LX/0SG;

.field public final h:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 381898
    const-class v0, LX/2Bz;

    sput-object v0, LX/2Bz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/12x;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Uh;LX/0SG;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/alarm/FbAlarmManager;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2Gb;",
            ">;>;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 381889
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 381890
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/2Bz;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 381891
    iput-object p1, p0, LX/2Bz;->b:Landroid/content/Context;

    .line 381892
    iput-object p2, p0, LX/2Bz;->c:LX/12x;

    .line 381893
    iput-object p3, p0, LX/2Bz;->d:Ljava/util/concurrent/ExecutorService;

    .line 381894
    iput-object p4, p0, LX/2Bz;->e:LX/0Ot;

    .line 381895
    iput-object p5, p0, LX/2Bz;->f:LX/0Uh;

    .line 381896
    iput-object p6, p0, LX/2Bz;->g:LX/0SG;

    .line 381897
    return-void
.end method

.method public static a(LX/0QB;)LX/2Bz;
    .locals 10

    .prologue
    .line 381873
    sget-object v0, LX/2Bz;->i:LX/2Bz;

    if-nez v0, :cond_1

    .line 381874
    const-class v1, LX/2Bz;

    monitor-enter v1

    .line 381875
    :try_start_0
    sget-object v0, LX/2Bz;->i:LX/2Bz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 381876
    if-eqz v2, :cond_0

    .line 381877
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 381878
    new-instance v3, LX/2Bz;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v5

    check-cast v5, LX/12x;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    .line 381879
    new-instance v7, LX/2C0;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    invoke-direct {v7, v8}, LX/2C0;-><init>(LX/0QB;)V

    move-object v7, v7

    .line 381880
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    invoke-static {v7, v8}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v7

    move-object v7, v7

    .line 381881
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-direct/range {v3 .. v9}, LX/2Bz;-><init>(Landroid/content/Context;LX/12x;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Uh;LX/0SG;)V

    .line 381882
    move-object v0, v3

    .line 381883
    sput-object v0, LX/2Bz;->i:LX/2Bz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 381884
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 381885
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 381886
    :cond_1
    sget-object v0, LX/2Bz;->i:LX/2Bz;

    return-object v0

    .line 381887
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 381888
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 381864
    const-string v0, "PushInitializer.onLogin"

    const v1, 0x72ef8b95

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 381865
    :try_start_0
    iget-object v0, p0, LX/2Bz;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gb;

    .line 381866
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, -0x76fd2673

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381867
    :try_start_1
    invoke-interface {v0}, LX/2Gb;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 381868
    const v0, 0x6def3d65

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 381869
    :catchall_0
    move-exception v0

    const v1, 0x21019d1c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 381870
    :catchall_1
    move-exception v0

    const v1, -0x2750e2cd

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 381871
    :cond_0
    const v0, -0x32c06338

    invoke-static {v0}, LX/02m;->a(I)V

    .line 381872
    return-void
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 381849
    iget-object v0, p0, LX/2Bz;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v6, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 381850
    if-eqz p1, :cond_1

    .line 381851
    iget-object v0, p0, LX/2Bz;->c:LX/12x;

    invoke-direct {p0}, LX/2Bz;->l()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 381852
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2Bz;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/push/PushInitializer$LocalBroadcastReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 381853
    const-string v1, "com.facebook.messaging.push.ACTION_ALARM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 381854
    invoke-direct {p0}, LX/2Bz;->l()Landroid/app/PendingIntent;

    move-result-object v2

    .line 381855
    if-eqz p1, :cond_2

    .line 381856
    const-wide/32 v0, 0xdbba0

    .line 381857
    :goto_0
    iget-object v3, p0, LX/2Bz;->g:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 381858
    :try_start_0
    iget-object v3, p0, LX/2Bz;->c:LX/12x;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0, v1, v2}, LX/12x;->b(IJLandroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 381859
    :cond_1
    :goto_1
    return-void

    .line 381860
    :cond_2
    const-wide/32 v0, 0x5265c00

    goto :goto_0

    .line 381861
    :catch_0
    move-exception v0

    .line 381862
    sget-object v1, LX/2Bz;->a:Ljava/lang/Class;

    const-string v2, "Exception while setting an alarm"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381863
    iget-object v0, p0, LX/2Bz;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_1
.end method

.method public static b$redex0(LX/2Bz;)V
    .locals 4

    .prologue
    .line 381840
    const-string v0, "PushInitializer.ensureSupported"

    const v1, -0x70ce58ef

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 381841
    :try_start_0
    iget-object v0, p0, LX/2Bz;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gb;

    .line 381842
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x549bda80

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381843
    :try_start_1
    invoke-interface {v0}, LX/2Gb;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 381844
    const v0, -0x71a7f131

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 381845
    :catchall_0
    move-exception v0

    const v1, 0x5a078ec

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 381846
    :catchall_1
    move-exception v0

    const v1, -0x75116ebd

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 381847
    :cond_0
    const v0, 0x35664a10

    invoke-static {v0}, LX/02m;->a(I)V

    .line 381848
    return-void
.end method

.method public static k(LX/2Bz;)V
    .locals 4

    .prologue
    .line 381899
    const-string v0, "PushInitializer.ensureRegistered"

    const v1, 0x4dd404f7    # 4.44636896E8f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 381900
    :try_start_0
    iget-object v0, p0, LX/2Bz;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gb;

    .line 381901
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, -0x1b90c963

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381902
    :try_start_1
    invoke-interface {v0}, LX/2Gb;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 381903
    const v0, -0x193204b4

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 381904
    :catchall_0
    move-exception v0

    const v1, 0x6545a57a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 381905
    :catchall_1
    move-exception v0

    const v1, 0x7b808318

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 381906
    :cond_0
    const v0, 0x17da0e66

    invoke-static {v0}, LX/02m;->a(I)V

    .line 381907
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2Bz;->a(Z)V

    .line 381908
    return-void
.end method

.method private l()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 381837
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2Bz;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/push/PushInitializer$LocalBroadcastReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 381838
    const-string v1, "com.facebook.messaging.push.ACTION_ALARM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 381839
    iget-object v1, p0, LX/2Bz;->b:Landroid/content/Context;

    const/4 v2, -0x1

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/2Ge;)LX/2Gb;
    .locals 4

    .prologue
    .line 381827
    const-string v0, "PushInitializer.getPushManager"

    const v1, 0x63248cbf

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 381828
    :try_start_0
    iget-object v0, p0, LX/2Bz;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gb;

    .line 381829
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x5a648a93

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381830
    :try_start_1
    invoke-interface {v0}, LX/2Gb;->a()LX/2Ge;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/2Ge;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-eqz v2, :cond_0

    .line 381831
    const v1, 0x6e6b7def

    :try_start_2
    invoke-static {v1}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 381832
    const v1, 0x1cb9c133

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 381833
    :cond_0
    const v0, 0x3ae03781

    :try_start_3
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 381834
    :catchall_0
    move-exception v0

    const v1, 0x77dde8d8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 381835
    :catchall_1
    move-exception v0

    const v1, -0x66c376a7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 381836
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported ServiceType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/facebook/auth/component/AuthenticationResult;)V
    .locals 2
    .param p1    # Lcom/facebook/auth/component/AuthenticationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 381822
    const-string v0, "PushInitializer.authComplete"

    const v1, 0x7fafddeb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 381823
    :try_start_0
    invoke-direct {p0}, LX/2Bz;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381824
    const v0, 0x3a0a082f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 381825
    return-void

    .line 381826
    :catchall_0
    move-exception v0

    const v1, 0x6f514491

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 381806
    const-string v0, "PushInitializer.unregisterPushToken"

    const v1, 0x1aaf2fb8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 381807
    :try_start_0
    iget-object v0, p0, LX/2Bz;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gb;

    .line 381808
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x5aa83846

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381809
    :try_start_1
    invoke-interface {v0, p1}, LX/2Gb;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 381810
    const v0, -0x159a932

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 381811
    :catchall_0
    move-exception v0

    const v1, 0x5f6e812b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 381812
    :catchall_1
    move-exception v0

    const v1, -0x609d9377

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 381813
    :cond_0
    const v0, 0xc280d8e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 381814
    iget-object v0, p0, LX/2Bz;->f:LX/0Uh;

    const/16 v1, 0x257

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 381815
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/2Bz;->a(Z)V

    .line 381816
    :cond_1
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 381820
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/16B;->a(Ljava/lang/String;)V

    .line 381821
    return-void
.end method

.method public final init()V
    .locals 0

    .prologue
    .line 381817
    invoke-static {p0}, LX/2Bz;->b$redex0(LX/2Bz;)V

    .line 381818
    invoke-static {p0}, LX/2Bz;->k(LX/2Bz;)V

    .line 381819
    return-void
.end method
