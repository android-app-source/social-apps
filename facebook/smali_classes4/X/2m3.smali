.class public LX/2m3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# instance fields
.field public final a:LX/2lw;

.field private final b:LX/2l7;

.field private c:Ljava/lang/String;

.field private final d:LX/0lC;


# direct methods
.method public constructor <init>(LX/2lw;LX/2l7;LX/0lC;)V
    .locals 1

    .prologue
    .line 459280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459281
    const/4 v0, 0x0

    iput-object v0, p0, LX/2m3;->c:Ljava/lang/String;

    .line 459282
    iput-object p1, p0, LX/2m3;->a:LX/2lw;

    .line 459283
    iput-object p2, p0, LX/2m3;->b:LX/2l7;

    .line 459284
    iput-object p3, p0, LX/2m3;->d:LX/0lC;

    .line 459285
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9

    .prologue
    .line 459286
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 459287
    iget-boolean v1, v0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v1, v1

    .line 459288
    if-eqz v1, :cond_4

    .line 459289
    iget-object v1, v0, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v1

    .line 459290
    iget-object v1, p0, LX/2m3;->b:LX/2l7;

    invoke-virtual {v1}, LX/2l7;->a()J

    move-result-wide v2

    .line 459291
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 459292
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-gtz v1, :cond_0

    iget-object v1, p0, LX/2m3;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 459293
    :cond_0
    iput-object v0, p0, LX/2m3;->c:Ljava/lang/String;

    .line 459294
    iget-object v0, p0, LX/2m3;->d:LX/0lC;

    iget-object v1, p0, LX/2m3;->c:Ljava/lang/String;

    new-instance v2, LX/EhN;

    invoke-direct {v2, p0}, LX/EhN;-><init>(LX/2m3;)V

    invoke-virtual {v0, v1, v2}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 459295
    sget-boolean v1, LX/007;->j:Z

    move v1, v1

    .line 459296
    if-eqz v1, :cond_3

    .line 459297
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/model/BookmarksGroup;

    .line 459298
    invoke-virtual {v1}, Lcom/facebook/bookmark/model/BookmarksGroup;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/model/Bookmark;

    .line 459299
    iget-object v6, v1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    const-string v7, "fb://"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 459300
    iget-object v6, v1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    const-string v7, "fb://"

    sget-object v8, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    goto :goto_0

    .line 459301
    :cond_3
    iget-object v1, p0, LX/2m3;->b:LX/2l7;

    invoke-virtual {v1, v0, v4, v5}, LX/2l7;->a(Ljava/util/List;J)V

    .line 459302
    new-instance v1, Lcom/facebook/bookmark/FetchBookmarksResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-direct {v1, v2, v4, v5, v0}, Lcom/facebook/bookmark/FetchBookmarksResult;-><init>(LX/0ta;JLX/0Px;)V

    move-object v0, v1

    .line 459303
    :goto_1
    new-instance v1, Landroid/content/Intent;

    sget-object v2, LX/0Ow;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 459304
    const-string v2, "vnd.android.cursor.item/vnd.facebook.katana.bookmark"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 459305
    const-string v2, "bookmark_groups"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 459306
    iget-object v2, p0, LX/2m3;->a:LX/2lw;

    invoke-virtual {v2, v1}, LX/2lw;->a(Landroid/content/Intent;)V

    .line 459307
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 459308
    :cond_4
    return-object v0

    .line 459309
    :cond_5
    new-instance v0, Lcom/facebook/bookmark/FetchBookmarksResult;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v5, v2}, Lcom/facebook/bookmark/FetchBookmarksResult;-><init>(LX/0ta;JLX/0Px;)V

    goto :goto_1
.end method
