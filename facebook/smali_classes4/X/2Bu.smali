.class public LX/2Bu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2XA;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/28W;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;)V
    .locals 0
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2XA;",
            ">;>;",
            "LX/0Ot",
            "<",
            "LX/28W;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 381632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381633
    iput-object p1, p0, LX/2Bu;->a:LX/0Ot;

    .line 381634
    iput-object p2, p0, LX/2Bu;->b:LX/0Ot;

    .line 381635
    iput-object p3, p0, LX/2Bu;->c:LX/0Ot;

    .line 381636
    iput-object p4, p0, LX/2Bu;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 381637
    iput-object p5, p0, LX/2Bu;->e:LX/0Ot;

    .line 381638
    return-void
.end method

.method public static a$redex0(LX/2Bu;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/ArrayList",
            "<",
            "LX/2ZE;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const v4, 0x230011

    .line 381639
    iget-object v0, p0, LX/2Bu;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 381640
    iget-object v0, p0, LX/2Bu;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 381641
    new-instance v1, LX/14U;

    invoke-direct {v1}, LX/14U;-><init>()V

    .line 381642
    sget-object v0, LX/14V;->BOOTSTRAP:LX/14V;

    invoke-virtual {v1, v0}, LX/14U;->a(LX/14V;)V

    .line 381643
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 381644
    iput-object v0, v1, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 381645
    iget-object v0, p0, LX/2Bu;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/28W;

    .line 381646
    :try_start_0
    const-string v2, "fetchPersistentComponents"

    invoke-virtual {v0, v2, p1, p2, v1}, LX/28W;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/List;LX/14U;)V

    .line 381647
    iget-object v0, p0, LX/2Bu;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x230011

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 381648
    :goto_0
    return-void

    .line 381649
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 381650
    iget-object v0, p0, LX/2Bu;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "PersistentComponentManager"

    const-string v3, "BatchComponentRunner fetchPersistentComponents failure"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 381651
    iget-object v0, p0, LX/2Bu;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v1, 0x57

    invoke-interface {v0, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    const v7, 0x230010

    .line 381652
    iget-object v0, p0, LX/2Bu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 381653
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 381654
    :cond_0
    :goto_0
    return-void

    .line 381655
    :cond_1
    iget-object v2, p0, LX/2Bu;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 381656
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v3, v1

    move-object v2, v1

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2XA;

    .line 381657
    invoke-interface {v1}, LX/2XA;->a()V

    .line 381658
    invoke-interface {v1}, LX/2XA;->dq_()Z

    move-result v5

    if-nez v5, :cond_2

    .line 381659
    iget-object v5, p0, LX/2Bu;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v7, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 381660
    invoke-interface {v1}, LX/2XA;->c()LX/2ZE;

    move-result-object v5

    .line 381661
    if-eqz v5, :cond_2

    .line 381662
    invoke-interface {v1}, LX/2XA;->e()LX/2ZF;

    move-result-object v1

    sget-object v6, LX/2ZF;->QE:LX/2ZF;

    if-ne v1, v6, :cond_3

    .line 381663
    if-nez v3, :cond_a

    .line 381664
    new-instance v1, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 381665
    :goto_2
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v3, v1

    goto :goto_1

    .line 381666
    :cond_3
    if-nez v2, :cond_9

    .line 381667
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 381668
    :goto_3
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    goto :goto_1

    .line 381669
    :cond_4
    iget-object v0, p0, LX/2Bu;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, v7, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 381670
    if-eqz v2, :cond_7

    if-eqz v3, :cond_7

    .line 381671
    new-instance v9, Ljava/util/ArrayList;

    const/4 v8, 0x2

    invoke-direct {v9, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 381672
    new-instance v8, LX/2aV;

    invoke-direct {v8, p0, p1, v3}, LX/2aV;-><init>(LX/2Bu;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/ArrayList;)V

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381673
    new-instance v8, LX/2aW;

    invoke-direct {v8, p0, p1, v2}, LX/2aW;-><init>(LX/2Bu;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/ArrayList;)V

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381674
    :try_start_0
    iget-object v8, p0, LX/2Bu;->c:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    const-wide/32 v10, 0x88b8

    sget-object v12, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v8, v9, v10, v11, v12}, Ljava/util/concurrent/ExecutorService;->invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;

    move-result-object v8

    .line 381675
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_5
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Future;

    .line 381676
    invoke-interface {v8}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v10

    if-nez v10, :cond_5

    .line 381677
    const v10, 0x884a311

    invoke-static {v8, v10}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_4

    .line 381678
    :catch_0
    move-exception v8

    move-object v9, v8

    .line 381679
    iget-object v8, p0, LX/2Bu;->e:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/03V;

    const-string v10, "PersistentComponentManager"

    const-string v11, "PersistentComponents execution interruption"

    invoke-virtual {v8, v10, v11, v9}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 381680
    :cond_6
    :goto_5
    goto/16 :goto_0

    .line 381681
    :cond_7
    if-eqz v2, :cond_8

    .line 381682
    sget-object v0, LX/2ZF;->OTHER:LX/2ZF;

    invoke-virtual {v0}, LX/2ZF;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v2, v0}, LX/2Bu;->a$redex0(LX/2Bu;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 381683
    :cond_8
    if-eqz v3, :cond_0

    .line 381684
    sget-object v0, LX/2ZF;->QE:LX/2ZF;

    invoke-virtual {v0}, LX/2ZF;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v3, v0}, LX/2Bu;->a$redex0(LX/2Bu;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    move-object v1, v2

    goto/16 :goto_3

    :cond_a
    move-object v1, v3

    goto/16 :goto_2

    .line 381685
    :catch_1
    move-exception v8

    move-object v9, v8

    .line 381686
    iget-object v8, p0, LX/2Bu;->e:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/03V;

    const-string v10, "PersistentComponentManager"

    const-string v11, "PersistentComponents execution failure"

    invoke-virtual {v8, v10, v11, v9}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5
.end method
