.class public LX/3H6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/3H6;


# instance fields
.field public final a:LX/3H7;

.field public final b:LX/1Ck;

.field public final c:LX/189;

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/3H7;LX/1Ck;LX/189;Ljava/lang/String;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/video/commercialbreak/annotations/CommercialBreakDebugVideoAdStoryId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 542727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 542728
    iput-object p1, p0, LX/3H6;->a:LX/3H7;

    .line 542729
    iput-object p2, p0, LX/3H6;->b:LX/1Ck;

    .line 542730
    iput-object p3, p0, LX/3H6;->c:LX/189;

    .line 542731
    iput-object p4, p0, LX/3H6;->e:Ljava/lang/String;

    .line 542732
    invoke-static {p0}, LX/3H6;->d(LX/3H6;)V

    .line 542733
    return-void
.end method

.method public static a(LX/0QB;)LX/3H6;
    .locals 7

    .prologue
    .line 542734
    sget-object v0, LX/3H6;->f:LX/3H6;

    if-nez v0, :cond_1

    .line 542735
    const-class v1, LX/3H6;

    monitor-enter v1

    .line 542736
    :try_start_0
    sget-object v0, LX/3H6;->f:LX/3H6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 542737
    if-eqz v2, :cond_0

    .line 542738
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 542739
    new-instance p0, LX/3H6;

    invoke-static {v0}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v3

    check-cast v3, LX/3H7;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v5

    check-cast v5, LX/189;

    .line 542740
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v6}, LX/0eG;->l(Lcom/facebook/prefs/shared/FbSharedPreferences;)Ljava/lang/String;

    move-result-object v6

    move-object v6, v6

    .line 542741
    check-cast v6, Ljava/lang/String;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3H6;-><init>(LX/3H7;LX/1Ck;LX/189;Ljava/lang/String;)V

    .line 542742
    move-object v0, p0

    .line 542743
    sput-object v0, LX/3H6;->f:LX/3H6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 542744
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 542745
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 542746
    :cond_1
    sget-object v0, LX/3H6;->f:LX/3H6;

    return-object v0

    .line 542747
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 542748
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(LX/3H6;)V
    .locals 6

    .prologue
    .line 542749
    iget-object v0, p0, LX/3H6;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3H6;->e:Ljava/lang/String;

    const-string v1, "-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 542750
    iget-object v0, p0, LX/3H6;->b:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetch_story_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/3H6;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/3H6;->a:LX/3H7;

    iget-object v3, p0, LX/3H6;->e:Ljava/lang/String;

    sget-object v4, LX/5Go;->GRAPHQL_VIDEO_CREATION_STORY:LX/5Go;

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual {v2, v3, v4, v5}, LX/3H7;->a(Ljava/lang/String;LX/5Go;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/D6l;

    invoke-direct {v3, p0}, LX/D6l;-><init>(LX/3H6;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 542751
    :cond_0
    return-void
.end method
