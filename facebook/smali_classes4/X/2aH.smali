.class public LX/2aH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/335;


# instance fields
.field public final a:LX/0yP;

.field public final b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

.field public final c:LX/1pA;

.field public final d:LX/2Zp;

.field public final e:LX/2cH;

.field public final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final g:LX/2cI;

.field public final h:LX/2r3;

.field public final i:Z


# direct methods
.method public constructor <init>(LX/0yP;Lcom/facebook/zero/service/ZeroHeaderRequestManager;LX/1pA;LX/2Zp;LX/2cH;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2cI;LX/2r3;Ljava/lang/Boolean;)V
    .locals 1
    .param p9    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 424571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424572
    iput-object p1, p0, LX/2aH;->a:LX/0yP;

    .line 424573
    iput-object p2, p0, LX/2aH;->b:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    .line 424574
    iput-object p3, p0, LX/2aH;->c:LX/1pA;

    .line 424575
    iput-object p4, p0, LX/2aH;->d:LX/2Zp;

    .line 424576
    iput-object p5, p0, LX/2aH;->e:LX/2cH;

    .line 424577
    iput-object p6, p0, LX/2aH;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 424578
    invoke-virtual {p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/2aH;->i:Z

    .line 424579
    iput-object p7, p0, LX/2aH;->g:LX/2cI;

    .line 424580
    iput-object p8, p0, LX/2aH;->h:LX/2r3;

    .line 424581
    return-void
.end method

.method public static b(LX/0QB;)LX/2aH;
    .locals 12

    .prologue
    .line 424562
    new-instance v0, LX/2aH;

    invoke-static {p0}, LX/0yO;->b(LX/0QB;)LX/0yO;

    move-result-object v1

    check-cast v1, LX/0yP;

    invoke-static {p0}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a(LX/0QB;)Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    move-result-object v2

    check-cast v2, Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    invoke-static {p0}, LX/1pA;->b(LX/0QB;)LX/1pA;

    move-result-object v3

    check-cast v3, LX/1pA;

    invoke-static {p0}, LX/2Zp;->b(LX/0QB;)LX/2Zp;

    move-result-object v4

    check-cast v4, LX/2Zp;

    invoke-static {p0}, LX/2cH;->b(LX/0QB;)LX/2cH;

    move-result-object v5

    check-cast v5, LX/2cH;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 424563
    new-instance v8, LX/2cI;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-direct {v8, v7}, LX/2cI;-><init>(LX/0ad;)V

    .line 424564
    move-object v7, v8

    .line 424565
    check-cast v7, LX/2cI;

    .line 424566
    new-instance v10, LX/2r3;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, LX/0TD;

    const/16 v11, 0x1c6

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct {v10, v8, v9, v11}, LX/2r3;-><init>(LX/0tX;LX/0TD;LX/0Ot;)V

    .line 424567
    move-object v8, v10

    .line 424568
    check-cast v8, LX/2r3;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-direct/range {v0 .. v9}, LX/2aH;-><init>(LX/0yP;Lcom/facebook/zero/service/ZeroHeaderRequestManager;LX/1pA;LX/2Zp;LX/2cH;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2cI;LX/2r3;Ljava/lang/Boolean;)V

    .line 424569
    return-object v0
.end method


# virtual methods
.method public final a()LX/2ZE;
    .locals 2

    .prologue
    .line 424570
    new-instance v0, LX/2r4;

    invoke-direct {v0, p0}, LX/2r4;-><init>(LX/2aH;)V

    return-object v0
.end method
