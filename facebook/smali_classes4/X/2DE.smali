.class public LX/2DE;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:J

.field public static final b:J

.field public static final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 383847
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/2DE;->a:J

    .line 383848
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2d

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/2DE;->b:J

    .line 383849
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/2DE;->c:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 383850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383851
    return-void
.end method

.method public static b(LX/0Uq;)V
    .locals 2

    .prologue
    .line 383852
    const-string v0, "waitForInitialization"

    const v1, -0x5db0cc5c

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 383853
    :try_start_0
    invoke-virtual {p0}, LX/0Uq;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383854
    const v0, 0x6a7a8c2c

    invoke-static {v0}, LX/03q;->a(I)V

    .line 383855
    return-void

    .line 383856
    :catchall_0
    move-exception v0

    const v1, 0x4f3e82ed

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method
