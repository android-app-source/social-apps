.class public LX/2Us;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/2Us;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/2Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNW;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6jL;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 416773
    const-string v0, "SMS_SPAM_NUMBER_BACKGROUND_FETCH"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 416774
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416775
    iput-object v0, p0, LX/2Us;->d:LX/0Ot;

    .line 416776
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416777
    iput-object v0, p0, LX/2Us;->e:LX/0Ot;

    .line 416778
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 416779
    iput-object v0, p0, LX/2Us;->f:LX/0Ot;

    .line 416780
    return-void
.end method

.method public static a(LX/0QB;)LX/2Us;
    .locals 9

    .prologue
    .line 416781
    sget-object v0, LX/2Us;->g:LX/2Us;

    if-nez v0, :cond_1

    .line 416782
    const-class v1, LX/2Us;

    monitor-enter v1

    .line 416783
    :try_start_0
    sget-object v0, LX/2Us;->g:LX/2Us;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416784
    if-eqz v2, :cond_0

    .line 416785
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416786
    new-instance v3, LX/2Us;

    invoke-direct {v3}, LX/2Us;-><init>()V

    .line 416787
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v5

    check-cast v5, LX/2Or;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    const/16 v7, 0x29b4

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2987

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 p0, 0x140f

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 416788
    iput-object v4, v3, LX/2Us;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v5, v3, LX/2Us;->b:LX/2Or;

    iput-object v6, v3, LX/2Us;->c:LX/0SG;

    iput-object v7, v3, LX/2Us;->d:LX/0Ot;

    iput-object v8, v3, LX/2Us;->e:LX/0Ot;

    iput-object p0, v3, LX/2Us;->f:LX/0Ot;

    .line 416789
    move-object v0, v3

    .line 416790
    sput-object v0, LX/2Us;->g:LX/2Us;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416791
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416792
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416793
    :cond_1
    sget-object v0, LX/2Us;->g:LX/2Us;

    return-object v0

    .line 416794
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416795
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final f()J
    .locals 4

    .prologue
    .line 416796
    iget-object v0, p0, LX/2Us;->b:LX/2Or;

    invoke-virtual {v0}, LX/2Or;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 416797
    const-wide/16 v0, -0x1

    .line 416798
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/2Us;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Kr;->W:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416799
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 416800
    iget-object v1, p0, LX/2Us;->b:LX/2Or;

    invoke-virtual {v1}, LX/2Or;->g()Z

    move-result v1

    if-nez v1, :cond_1

    .line 416801
    :cond_0
    :goto_0
    return v0

    .line 416802
    :cond_1
    iget-object v1, p0, LX/2Us;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->W:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 416803
    iget-object v1, p0, LX/2Us;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-gtz v1, :cond_2

    iget-object v1, p0, LX/2Us;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416804
    new-instance v1, LX/FNM;

    invoke-direct {v1, p0}, LX/FNM;-><init>(LX/2Us;)V

    .line 416805
    iget-object v0, p0, LX/2Us;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
