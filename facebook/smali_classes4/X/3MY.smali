.class public final enum LX/3MY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3MY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3MY;

.field public static final enum BIG:LX/3MY;

.field public static final enum HUGE:LX/3MY;

.field public static final enum SMALL:LX/3MY;


# instance fields
.field public final dp:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 555072
    new-instance v0, LX/3MY;

    const-string v1, "SMALL"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v3, v2}, LX/3MY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3MY;->SMALL:LX/3MY;

    .line 555073
    new-instance v0, LX/3MY;

    const-string v1, "BIG"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v4, v2}, LX/3MY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3MY;->BIG:LX/3MY;

    .line 555074
    new-instance v0, LX/3MY;

    const-string v1, "HUGE"

    const/16 v2, 0x140

    invoke-direct {v0, v1, v5, v2}, LX/3MY;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/3MY;->HUGE:LX/3MY;

    .line 555075
    const/4 v0, 0x3

    new-array v0, v0, [LX/3MY;

    sget-object v1, LX/3MY;->SMALL:LX/3MY;

    aput-object v1, v0, v3

    sget-object v1, LX/3MY;->BIG:LX/3MY;

    aput-object v1, v0, v4

    sget-object v1, LX/3MY;->HUGE:LX/3MY;

    aput-object v1, v0, v5

    sput-object v0, LX/3MY;->$VALUES:[LX/3MY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 555076
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 555077
    iput p3, p0, LX/3MY;->dp:I

    .line 555078
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3MY;
    .locals 1

    .prologue
    .line 555079
    const-class v0, LX/3MY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3MY;

    return-object v0
.end method

.method public static values()[LX/3MY;
    .locals 1

    .prologue
    .line 555080
    sget-object v0, LX/3MY;->$VALUES:[LX/3MY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3MY;

    return-object v0
.end method
