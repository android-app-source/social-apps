.class public LX/2lS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/2lS;


# instance fields
.field public final a:LX/11i;

.field private final b:LX/0id;

.field private c:Z

.field private d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LX/2lT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/11i;LX/0id;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 457978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457979
    iput-object p1, p0, LX/2lS;->a:LX/11i;

    .line 457980
    iput-object p2, p0, LX/2lS;->b:LX/0id;

    .line 457981
    const-class v0, LX/2lT;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LX/2lS;->h:Ljava/util/EnumSet;

    .line 457982
    return-void
.end method

.method public static a(LX/0QB;)LX/2lS;
    .locals 5

    .prologue
    .line 457965
    sget-object v0, LX/2lS;->i:LX/2lS;

    if-nez v0, :cond_1

    .line 457966
    const-class v1, LX/2lS;

    monitor-enter v1

    .line 457967
    :try_start_0
    sget-object v0, LX/2lS;->i:LX/2lS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 457968
    if-eqz v2, :cond_0

    .line 457969
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 457970
    new-instance p0, LX/2lS;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v4

    check-cast v4, LX/0id;

    invoke-direct {p0, v3, v4}, LX/2lS;-><init>(LX/11i;LX/0id;)V

    .line 457971
    move-object v0, p0

    .line 457972
    sput-object v0, LX/2lS;->i:LX/2lS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457973
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 457974
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 457975
    :cond_1
    sget-object v0, LX/2lS;->i:LX/2lS;

    return-object v0

    .line 457976
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 457977
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2lS;LX/2lT;)V
    .locals 3

    .prologue
    .line 457983
    iget-object v0, p0, LX/2lS;->h:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457984
    :cond_0
    :goto_0
    return-void

    .line 457985
    :cond_1
    iget-object v0, p0, LX/2lS;->a:LX/11i;

    sget-object v1, LX/2lT;->GROUPS_FEED_TTI_SEQUENCE:LX/2lU;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 457986
    if-eqz v0, :cond_0

    .line 457987
    invoke-virtual {p1}, LX/2lT;->getName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x6f5b321

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 457988
    iget-object v0, p0, LX/2lS;->h:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(LX/2lS;LX/2lT;LX/0P1;)V
    .locals 4
    .param p1    # LX/2lT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2lT;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 458001
    iget-object v0, p0, LX/2lS;->h:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 458002
    :cond_0
    :goto_0
    return-void

    .line 458003
    :cond_1
    iget-object v0, p0, LX/2lS;->a:LX/11i;

    sget-object v1, LX/2lT;->GROUPS_FEED_TTI_SEQUENCE:LX/2lU;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 458004
    if-eqz v0, :cond_0

    .line 458005
    if-nez p2, :cond_2

    .line 458006
    invoke-virtual {p1}, LX/2lT;->getName()Ljava/lang/String;

    move-result-object v1

    const v2, -0x6e4599cf

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 458007
    :goto_1
    iget-object v0, p0, LX/2lS;->h:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 458008
    :cond_2
    invoke-virtual {p1}, LX/2lT;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const v3, -0x413b9028    # -0.3836658f

    invoke-static {v0, v1, v2, p2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    goto :goto_1
.end method

.method private b(LX/2lT;)V
    .locals 3

    .prologue
    .line 457989
    iget-object v0, p0, LX/2lS;->h:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 457990
    :cond_0
    :goto_0
    return-void

    .line 457991
    :cond_1
    iget-object v0, p0, LX/2lS;->a:LX/11i;

    sget-object v1, LX/2lT;->GROUPS_FEED_TTI_SEQUENCE:LX/2lU;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 457992
    if-eqz v0, :cond_0

    .line 457993
    invoke-virtual {p1}, LX/2lT;->getName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x2e31f945

    invoke-static {v0, v1, v2}, LX/096;->d(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 457994
    iget-object v0, p0, LX/2lS;->h:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static c(LX/2lS;LX/2lT;)V
    .locals 3

    .prologue
    .line 457995
    iget-object v0, p0, LX/2lS;->h:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 457996
    :cond_0
    :goto_0
    return-void

    .line 457997
    :cond_1
    iget-object v0, p0, LX/2lS;->a:LX/11i;

    sget-object v1, LX/2lT;->GROUPS_FEED_TTI_SEQUENCE:LX/2lU;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 457998
    if-eqz v0, :cond_0

    .line 457999
    invoke-virtual {p1}, LX/2lT;->getName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x6afbcd68

    invoke-static {v0, v1, v2}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 458000
    iget-object v0, p0, LX/2lS;->h:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 457937
    iput-boolean v0, p0, LX/2lS;->c:Z

    .line 457938
    iput-boolean v0, p0, LX/2lS;->d:Z

    .line 457939
    iput-boolean v0, p0, LX/2lS;->e:Z

    .line 457940
    iput-boolean v0, p0, LX/2lS;->f:Z

    .line 457941
    iput-boolean v0, p0, LX/2lS;->g:Z

    .line 457942
    sget-object v0, LX/2lT;->CACHED_HEADER:LX/2lT;

    invoke-direct {p0, v0}, LX/2lS;->b(LX/2lT;)V

    .line 457943
    sget-object v0, LX/2lT;->CACHED_STORIES:LX/2lT;

    invoke-direct {p0, v0}, LX/2lS;->b(LX/2lT;)V

    .line 457944
    sget-object v0, LX/2lT;->FRESH_HEADER:LX/2lT;

    invoke-direct {p0, v0}, LX/2lS;->b(LX/2lT;)V

    .line 457945
    sget-object v0, LX/2lT;->FRESH_STORIES:LX/2lT;

    invoke-direct {p0, v0}, LX/2lS;->b(LX/2lT;)V

    .line 457946
    sget-object v0, LX/2lT;->INITIAL_STORIES:LX/2lT;

    invoke-direct {p0, v0}, LX/2lS;->b(LX/2lT;)V

    .line 457947
    sget-object v0, LX/2lT;->GROUPS_FEED_TTI_SEQUENCE:LX/2lU;

    .line 457948
    iget-object v1, p0, LX/2lS;->a:LX/11i;

    invoke-interface {v1, v0}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 457949
    if-eqz v1, :cond_0

    .line 457950
    iget-object v1, p0, LX/2lS;->a:LX/11i;

    invoke-interface {v1, v0}, LX/11i;->d(LX/0Pq;)V

    .line 457951
    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 457952
    iput-boolean v0, p0, LX/2lS;->c:Z

    .line 457953
    iput-boolean v0, p0, LX/2lS;->d:Z

    .line 457954
    iput-boolean v0, p0, LX/2lS;->e:Z

    .line 457955
    iput-boolean v0, p0, LX/2lS;->f:Z

    .line 457956
    iput-boolean v0, p0, LX/2lS;->g:Z

    .line 457957
    iget-object v1, p0, LX/2lS;->a:LX/11i;

    sget-object v2, LX/2lT;->GROUPS_FEED_TTI_SEQUENCE:LX/2lU;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, p1, p2}, LX/11i;->a(LX/0Pq;LX/0P1;J)LX/11o;

    .line 457958
    sget-object v1, LX/2lT;->INITIAL_STORIES:LX/2lT;

    const/4 v6, 0x0

    .line 457959
    iget-object v4, p0, LX/2lS;->h:Ljava/util/EnumSet;

    invoke-virtual {v4, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 457960
    :cond_0
    :goto_0
    return-void

    .line 457961
    :cond_1
    iget-object v4, p0, LX/2lS;->a:LX/11i;

    sget-object v5, LX/2lT;->GROUPS_FEED_TTI_SEQUENCE:LX/2lU;

    invoke-interface {v4, v5}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v4

    .line 457962
    if-eqz v4, :cond_0

    .line 457963
    invoke-virtual {v1}, LX/2lT;->getName()Ljava/lang/String;

    move-result-object v5

    const v10, 0x4b47dda9    # 1.3098409E7f

    move-object v7, v6

    move-wide v8, p1

    invoke-static/range {v4 .. v10}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 457964
    iget-object v4, p0, LX/2lS;->h:Ljava/util/EnumSet;

    invoke-virtual {v4, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 457909
    sget-object v0, LX/2lT;->CACHED_HEADER:LX/2lT;

    invoke-static {p0, v0}, LX/2lS;->a(LX/2lS;LX/2lT;)V

    .line 457910
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 457911
    sget-object v0, LX/2lT;->FRESH_HEADER:LX/2lT;

    invoke-static {p0, v0}, LX/2lS;->a(LX/2lS;LX/2lT;)V

    .line 457912
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 457913
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2lS;->c:Z

    .line 457914
    sget-object v0, LX/2lT;->CACHED_HEADER:LX/2lT;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/2lS;->a(LX/2lS;LX/2lT;LX/0P1;)V

    .line 457915
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 457916
    sget-object v0, LX/2lT;->CACHED_HEADER:LX/2lT;

    invoke-direct {p0, v0}, LX/2lS;->b(LX/2lT;)V

    .line 457917
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 457918
    sget-object v0, LX/2lT;->FRESH_HEADER:LX/2lT;

    invoke-direct {p0, v0}, LX/2lS;->b(LX/2lT;)V

    .line 457919
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 457920
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2lS;->d:Z

    .line 457921
    sget-object v0, LX/2lT;->FRESH_HEADER:LX/2lT;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/2lS;->a(LX/2lS;LX/2lT;LX/0P1;)V

    .line 457922
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 457934
    sget-object v0, LX/2lT;->CACHED_HEADER:LX/2lT;

    invoke-static {p0, v0}, LX/2lS;->c(LX/2lS;LX/2lT;)V

    .line 457935
    sget-object v0, LX/2lT;->FRESH_HEADER:LX/2lT;

    invoke-static {p0, v0}, LX/2lS;->c(LX/2lS;LX/2lT;)V

    .line 457936
    return-void
.end method

.method public final m()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 457923
    iget-boolean v0, p0, LX/2lS;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/2lS;->d:Z

    if-eqz v0, :cond_4

    :cond_0
    iget-boolean v0, p0, LX/2lS;->e:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, LX/2lS;->f:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 457924
    :goto_0
    iget-boolean v1, p0, LX/2lS;->g:Z

    if-eqz v1, :cond_1

    .line 457925
    sget-object v1, LX/2lT;->INITIAL_STORIES:LX/2lT;

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, LX/2lS;->a(LX/2lS;LX/2lT;LX/0P1;)V

    .line 457926
    :cond_1
    if-eqz v0, :cond_3

    .line 457927
    sget-object v1, LX/2lT;->GROUPS_FEED_TTI_SEQUENCE:LX/2lU;

    .line 457928
    iget-object v2, p0, LX/2lS;->a:LX/11i;

    invoke-interface {v2, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v2

    .line 457929
    if-eqz v2, :cond_2

    .line 457930
    iget-object v2, p0, LX/2lS;->a:LX/11i;

    invoke-interface {v2, v1}, LX/11i;->b(LX/0Pq;)V

    .line 457931
    :cond_2
    iget-object v1, p0, LX/2lS;->b:LX/0id;

    const-string v2, "LoadGroupsFeed"

    invoke-virtual {v1, v2}, LX/0id;->b(Ljava/lang/String;)V

    .line 457932
    :cond_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 457933
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
