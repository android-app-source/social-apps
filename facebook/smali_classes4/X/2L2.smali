.class public LX/2L2;
.super LX/1qS;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2L2;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/2L3;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394645
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "offline_mode_db"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 394646
    return-void
.end method

.method public static a(LX/0QB;)LX/2L2;
    .locals 7

    .prologue
    .line 394647
    sget-object v0, LX/2L2;->a:LX/2L2;

    if-nez v0, :cond_1

    .line 394648
    const-class v1, LX/2L2;

    monitor-enter v1

    .line 394649
    :try_start_0
    sget-object v0, LX/2L2;->a:LX/2L2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394650
    if-eqz v2, :cond_0

    .line 394651
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 394652
    new-instance p0, LX/2L2;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v5

    check-cast v5, LX/1qU;

    invoke-static {v0}, LX/2L3;->a(LX/0QB;)LX/2L3;

    move-result-object v6

    check-cast v6, LX/2L3;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2L2;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/2L3;)V

    .line 394653
    move-object v0, p0

    .line 394654
    sput-object v0, LX/2L2;->a:LX/2L2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394655
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394656
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394657
    :cond_1
    sget-object v0, LX/2L2;->a:LX/2L2;

    return-object v0

    .line 394658
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394659
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 0

    .prologue
    .line 394660
    invoke-virtual {p0}, LX/0Tr;->f()V

    .line 394661
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 394662
    const v0, 0x32000

    return v0
.end method
