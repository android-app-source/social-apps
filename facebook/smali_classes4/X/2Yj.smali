.class public LX/2Yj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/aldrin/status/AldrinUserStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 421264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421265
    iput-object p1, p0, LX/2Yj;->a:LX/0SG;

    .line 421266
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 2

    .prologue
    .line 421291
    new-instance v0, LX/14O;

    invoke-direct {v0}, LX/14O;-><init>()V

    const-string v1, "getAldrinLoggedOutUser"

    .line 421292
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 421293
    move-object v0, v0

    .line 421294
    const-string v1, "GET"

    .line 421295
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 421296
    move-object v0, v0

    .line 421297
    const-string v1, "aldrin_logged_out_status"

    .line 421298
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 421299
    move-object v0, v0

    .line 421300
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 421301
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 421302
    move-object v0, v0

    .line 421303
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 421304
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 421305
    move-object v0, v0

    .line 421306
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 421267
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 421268
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    .line 421269
    new-instance v1, LX/2Yl;

    invoke-direct {v1}, LX/2Yl;-><init>()V

    const-string v2, "current_region"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 421270
    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    move-result-object v3

    .line 421271
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    if-eq v3, p1, :cond_0

    .line 421272
    :goto_0
    move-object v2, v3

    .line 421273
    iput-object v2, v1, LX/2Yl;->b:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 421274
    move-object v1, v1

    .line 421275
    const-string v2, "tos_terms_url"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 421276
    iput-object v2, v1, LX/2Yl;->f:Ljava/lang/String;

    .line 421277
    move-object v1, v1

    .line 421278
    const-string v2, "tos_privacy_url"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 421279
    iput-object v2, v1, LX/2Yl;->g:Ljava/lang/String;

    .line 421280
    move-object v1, v1

    .line 421281
    const-string v2, "tos_cookies_url"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 421282
    iput-object v2, v1, LX/2Yl;->h:Ljava/lang/String;

    .line 421283
    move-object v1, v1

    .line 421284
    const-string v2, "tos_version"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 421285
    iput-object v0, v1, LX/2Yl;->i:Ljava/lang/String;

    .line 421286
    move-object v0, v1

    .line 421287
    iget-object v1, p0, LX/2Yj;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 421288
    iput-wide v2, v0, LX/2Yl;->j:J

    .line 421289
    move-object v0, v0

    .line 421290
    invoke-virtual {v0}, LX/2Yl;->j()Lcom/facebook/aldrin/status/AldrinUserStatus;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    goto :goto_0
.end method
