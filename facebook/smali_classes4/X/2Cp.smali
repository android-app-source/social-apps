.class public LX/2Cp;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:LX/2Cp;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "UploadServiceProcessUtil.class"
    .end annotation
.end field


# instance fields
.field private final c:Landroid/content/Context;

.field private volatile d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 383250
    const-class v0, Lcom/facebook/analytics2/logger/AlarmBasedUploadService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Cp;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 383251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383252
    iput-object p1, p0, LX/2Cp;->c:Landroid/content/Context;

    .line 383253
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LX/2Cp;
    .locals 3

    .prologue
    .line 383254
    const-class v1, LX/2Cp;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/2Cp;->b:LX/2Cp;

    if-nez v0, :cond_0

    .line 383255
    new-instance v0, LX/2Cp;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/2Cp;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/2Cp;->b:LX/2Cp;

    .line 383256
    :cond_0
    sget-object v0, LX/2Cp;->b:LX/2Cp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 383257
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(LX/2Cp;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 383258
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Cp;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 383259
    sget-object v0, LX/2Cp;->a:Ljava/lang/String;

    invoke-static {p0, v0}, LX/2Cp;->b(LX/2Cp;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2Cp;->d:Ljava/lang/String;

    .line 383260
    :cond_0
    iget-object v0, p0, LX/2Cp;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 383261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(LX/2Cp;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 383262
    :try_start_0
    iget-object v0, p0, LX/2Cp;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, LX/2Cp;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 383263
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    if-eqz v0, :cond_1

    move v0, v1

    .line 383264
    :goto_0
    iget-object v3, v2, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 383265
    iget-object v3, v2, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    aget-object v3, v3, v0

    .line 383266
    iget-object v4, v3, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 383267
    iget-object v0, v3, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    return-object v0

    .line 383268
    :catch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Package "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/2Cp;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot be found!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383269
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 383270
    :cond_1
    const-string v0, "UploadServiceProcessUtil"

    const-string v3, "Unable to find the UploadService! Services registered: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    invoke-static {v2}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 383271
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to find the UploadService!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
