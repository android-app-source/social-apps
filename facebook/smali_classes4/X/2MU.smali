.class public LX/2MU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2MV;


# instance fields
.field private final a:LX/2MY;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/61e;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/61m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2MY;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2MY;",
            "LX/0Or",
            "<",
            "LX/61e;",
            ">;",
            "LX/0Or",
            "<",
            "LX/61m;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396826
    iput-object p1, p0, LX/2MU;->a:LX/2MY;

    .line 396827
    iput-object p2, p0, LX/2MU;->b:LX/0Or;

    .line 396828
    iput-object p3, p0, LX/2MU;->c:LX/0Or;

    .line 396829
    return-void
.end method

.method public static a(LX/0QB;)LX/2MU;
    .locals 1

    .prologue
    .line 396830
    invoke-static {p0}, LX/2MU;->b(LX/0QB;)LX/2MU;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2MU;
    .locals 4

    .prologue
    .line 396831
    new-instance v1, LX/2MU;

    invoke-static {p0}, LX/2MW;->a(LX/0QB;)LX/2MY;

    move-result-object v0

    check-cast v0, LX/2MY;

    const/16 v2, 0x386e

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x3872

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, LX/2MU;-><init>(LX/2MY;LX/0Or;LX/0Or;)V

    .line 396832
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)LX/60x;
    .locals 3

    .prologue
    .line 396833
    iget-object v0, p0, LX/2MU;->a:LX/2MY;

    .line 396834
    invoke-virtual {v0}, LX/03m;->T_()Z

    move-result v1

    move v0, v1

    .line 396835
    if-eqz v0, :cond_0

    .line 396836
    iget-object v0, p0, LX/2MU;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61m;

    invoke-virtual {v0, p1}, LX/61m;->a(Landroid/net/Uri;)LX/60x;

    move-result-object v1

    .line 396837
    :try_start_0
    iget-object v0, p0, LX/2MU;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61e;

    invoke-virtual {v0, p1}, LX/61e;->a(Landroid/net/Uri;)LX/60x;

    move-result-object v0

    .line 396838
    iget-object v2, v0, LX/60x;->i:Ljava/lang/String;

    iput-object v2, v1, LX/60x;->i:Ljava/lang/String;

    .line 396839
    iget-object v0, v0, LX/60x;->j:Ljava/lang/String;

    iput-object v0, v1, LX/60x;->j:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v0, v1

    .line 396840
    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2MU;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61e;

    invoke-virtual {v0, p1}, LX/61e;->a(Landroid/net/Uri;)LX/60x;

    move-result-object v0

    goto :goto_1

    :catch_0
    goto :goto_0
.end method
