.class public final LX/2yc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 482314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 482273
    if-nez p1, :cond_0

    .line 482274
    :goto_0
    return v0

    .line 482275
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 482276
    if-nez v1, :cond_1

    .line 482277
    :goto_1
    move v1, v4

    .line 482278
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 482279
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 482280
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 482281
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 482282
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 482283
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 482284
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    .line 482285
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 482286
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 482287
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    const/4 v3, 0x0

    .line 482288
    if-nez v2, :cond_4

    .line 482289
    :goto_2
    move v8, v3

    .line 482290
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->t()LX/0Px;

    move-result-object v9

    .line 482291
    if-eqz v9, :cond_3

    .line 482292
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    new-array v10, v2, [I

    move v3, v4

    .line 482293
    :goto_3
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 482294
    invoke-virtual {v9, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    invoke-static {p0, v2}, LX/2yc;->a(LX/186;Lcom/facebook/graphql/model/GraphQLRedirectionInfo;)I

    move-result v2

    aput v2, v10, v3

    .line 482295
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 482296
    :cond_2
    invoke-virtual {p0, v10, v11}, LX/186;->a([IZ)I

    move-result v2

    .line 482297
    :goto_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 482298
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 482299
    const/4 v10, 0x7

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 482300
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 482301
    invoke-virtual {p0, v11, v6}, LX/186;->b(II)V

    .line 482302
    const/4 v4, 0x2

    invoke-virtual {p0, v4, v7}, LX/186;->b(II)V

    .line 482303
    const/4 v4, 0x3

    invoke-virtual {p0, v4, v8}, LX/186;->b(II)V

    .line 482304
    const/4 v4, 0x4

    invoke-virtual {p0, v4, v2}, LX/186;->b(II)V

    .line 482305
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 482306
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 482307
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 482308
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_3
    move v2, v4

    goto :goto_4

    .line 482309
    :cond_4
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 482310
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 482311
    invoke-virtual {p0, v3, v8}, LX/186;->b(II)V

    .line 482312
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 482313
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLNode;)I
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 482145
    if-nez p1, :cond_0

    .line 482146
    :goto_0
    return v2

    .line 482147
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 482148
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->co()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    const/4 v1, 0x0

    .line 482149
    if-nez v0, :cond_3

    .line 482150
    :goto_1
    move v4, v1

    .line 482151
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 482152
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->hG()LX/0Px;

    move-result-object v6

    .line 482153
    if-eqz v6, :cond_2

    .line 482154
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    new-array v7, v0, [I

    move v1, v2

    .line 482155
    :goto_2
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 482156
    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;

    invoke-static {p0, v0}, LX/2yc;->a(LX/186;Lcom/facebook/graphql/model/GraphQLRedirectionInfo;)I

    move-result v0

    aput v0, v7, v1

    .line 482157
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 482158
    :cond_1
    invoke-virtual {p0, v7, v10}, LX/186;->a([IZ)I

    move-result v0

    .line 482159
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->hV()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    const/4 v6, 0x0

    .line 482160
    if-nez v1, :cond_4

    .line 482161
    :goto_4
    move v1, v6

    .line 482162
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->hW()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    const/4 v7, 0x0

    .line 482163
    if-nez v6, :cond_5

    .line 482164
    :goto_5
    move v6, v7

    .line 482165
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 482166
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->lh()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v8

    const/4 v9, 0x0

    .line 482167
    if-nez v8, :cond_6

    .line 482168
    :goto_6
    move v8, v9

    .line 482169
    const/16 v9, 0x8

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 482170
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 482171
    invoke-virtual {p0, v10, v4}, LX/186;->b(II)V

    .line 482172
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 482173
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 482174
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 482175
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 482176
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 482177
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 482178
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 482179
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 482180
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 482181
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 482182
    invoke-virtual {p0, v1, v4}, LX/186;->b(II)V

    .line 482183
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 482184
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 482185
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 482186
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 482187
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 482188
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 482189
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_4

    .line 482190
    :cond_5
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 482191
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 482192
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 482193
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 482194
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto/16 :goto_5

    .line 482195
    :cond_6
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 482196
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 482197
    invoke-virtual {p0, v9, v11}, LX/186;->b(II)V

    .line 482198
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 482199
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto/16 :goto_6
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLRedirectionInfo;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 482266
    if-nez p1, :cond_0

    .line 482267
    :goto_0
    return v0

    .line 482268
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLRedirectionInfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 482269
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 482270
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 482271
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 482272
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/171;)LX/1yA;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 482239
    if-nez p0, :cond_1

    .line 482240
    :cond_0
    :goto_0
    return-object v2

    .line 482241
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 482242
    const/4 v1, 0x0

    .line 482243
    if-nez p0, :cond_3

    .line 482244
    :goto_1
    move v1, v1

    .line 482245
    if-eqz v1, :cond_0

    .line 482246
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 482247
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 482248
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 482249
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 482250
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 482251
    const-string v1, "LinkExtractorConverter.getGetEntityFbLinkGraphQL"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 482252
    :cond_2
    new-instance v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetEntityFbLinkGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 482253
    :cond_3
    invoke-interface {p0}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 482254
    invoke-interface {p0}, LX/171;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 482255
    invoke-interface {p0}, LX/171;->v_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 482256
    invoke-interface {p0}, LX/171;->w_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 482257
    invoke-interface {p0}, LX/171;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 482258
    const/4 v8, 0x7

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 482259
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 482260
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 482261
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 482262
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 482263
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 482264
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 482265
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/3Ab;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 482212
    if-nez p0, :cond_1

    .line 482213
    :cond_0
    :goto_0
    return-object v2

    .line 482214
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 482215
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 482216
    if-nez p0, :cond_3

    .line 482217
    :goto_1
    move v1, v4

    .line 482218
    if-eqz v1, :cond_0

    .line 482219
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 482220
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 482221
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 482222
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 482223
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 482224
    const-string v1, "LinkExtractorConverter.getLinkableTextWithEntities"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 482225
    :cond_2
    new-instance v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-direct {v2, v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 482226
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v5

    .line 482227
    if-eqz v5, :cond_5

    .line 482228
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 482229
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 482230
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    invoke-static {v0, v1}, LX/2yc;->a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I

    move-result v1

    aput v1, v6, v3

    .line 482231
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 482232
    :cond_4
    invoke-virtual {v0, v6, v7}, LX/186;->a([IZ)I

    move-result v1

    .line 482233
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 482234
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 482235
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 482236
    invoke-virtual {v0, v7, v3}, LX/186;->b(II)V

    .line 482237
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 482238
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto :goto_1

    :cond_5
    move v1, v4

    goto :goto_3
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 482200
    if-nez p0, :cond_1

    .line 482201
    :cond_0
    :goto_0
    return-object v2

    .line 482202
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 482203
    invoke-static {v0, p0}, LX/2yc;->a(LX/186;Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v1

    .line 482204
    if-eqz v1, :cond_0

    .line 482205
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 482206
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 482207
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 482208
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 482209
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 482210
    const-string v1, "LinkExtractorConverter.getGetFeedStoryAttachmentFbLinkGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 482211
    :cond_2
    new-instance v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0
.end method
