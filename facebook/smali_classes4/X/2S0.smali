.class public LX/2S0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field private static volatile k:LX/2S0;


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Landroid/app/DownloadManager;

.field public final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2zI;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private j:LX/0Yd;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 410408
    const-class v0, LX/2S0;

    sput-object v0, LX/2S0;->a:Ljava/lang/Class;

    .line 410409
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "simplified_download_manager/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 410410
    sput-object v0, LX/2S0;->b:LX/0Tn;

    const-string v1, "extra/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S0;->c:LX/0Tn;

    .line 410411
    sget-object v0, LX/2S0;->b:LX/0Tn;

    const-string v1, "id/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2S0;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/DownloadManager;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410413
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2S0;->h:Ljava/util/Map;

    .line 410414
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/2S0;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 410415
    iput-object p1, p0, LX/2S0;->e:Landroid/content/Context;

    .line 410416
    iput-object p2, p0, LX/2S0;->f:Landroid/app/DownloadManager;

    .line 410417
    iput-object p3, p0, LX/2S0;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 410418
    return-void
.end method

.method public static a(LX/0QB;)LX/2S0;
    .locals 6

    .prologue
    .line 410419
    sget-object v0, LX/2S0;->k:LX/2S0;

    if-nez v0, :cond_1

    .line 410420
    const-class v1, LX/2S0;

    monitor-enter v1

    .line 410421
    :try_start_0
    sget-object v0, LX/2S0;->k:LX/2S0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 410422
    if-eqz v2, :cond_0

    .line 410423
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 410424
    new-instance p0, LX/2S0;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2S1;->b(LX/0QB;)Landroid/app/DownloadManager;

    move-result-object v4

    check-cast v4, Landroid/app/DownloadManager;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5}, LX/2S0;-><init>(Landroid/content/Context;Landroid/app/DownloadManager;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 410425
    move-object v0, p0

    .line 410426
    sput-object v0, LX/2S0;->k:LX/2S0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410427
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 410428
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 410429
    :cond_1
    sget-object v0, LX/2S0;->k:LX/2S0;

    return-object v0

    .line 410430
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 410431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2S0;J)V
    .locals 5

    .prologue
    .line 410432
    iget-object v0, p0, LX/2S0;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S0;->d:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    .line 410433
    invoke-interface {v0}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 410434
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 410435
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sget-object v1, LX/2S0;->d:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v0

    .line 410436
    :goto_0
    move-object v0, v0

    .line 410437
    if-nez v0, :cond_2

    .line 410438
    :cond_1
    :goto_1
    return-void

    .line 410439
    :cond_2
    invoke-static {p0, p1, p2}, LX/2S0;->b(LX/2S0;J)I

    move-result v1

    .line 410440
    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 410441
    :try_start_0
    invoke-static {p0, p1, p2, v0, v1}, LX/2S0;->a(LX/2S0;JLjava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410442
    invoke-static {p0, v0}, LX/2S0;->a(LX/2S0;Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-static {p0, v0}, LX/2S0;->a(LX/2S0;Ljava/lang/String;)V

    throw v1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/2S0;JLjava/lang/String;I)V
    .locals 3

    .prologue
    .line 410443
    if-nez p3, :cond_3

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    .line 410444
    if-eqz v0, :cond_0

    if-nez p4, :cond_1

    .line 410445
    :cond_0
    :goto_1
    return-void

    .line 410446
    :cond_1
    const/16 v0, 0x8

    if-eq p4, v0, :cond_2

    .line 410447
    :try_start_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DownloadManager failed with status code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410448
    :catch_0
    goto :goto_1

    .line 410449
    :cond_2
    iget-object v0, p0, LX/2S0;->f:Landroid/app/DownloadManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/DownloadManager;->openDownloadedFile(J)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 410450
    new-instance v1, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v1, v0}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 410451
    const/4 v1, 0x0

    .line 410452
    if-nez p3, :cond_4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 410453
    :goto_2
    goto :goto_1

    :cond_3
    iget-object v0, p0, LX/2S0;->h:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zI;

    goto :goto_0

    :cond_4
    iget-object v2, p0, LX/2S0;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/2S0;->c:LX/0Tn;

    invoke-virtual {v0, p3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v2, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_2
.end method

.method private static a(LX/2S0;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 410454
    iget-object v0, p0, LX/2S0;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v0, LX/2S0;->d:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v0, LX/2S0;->c:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 410455
    return-void
.end method

.method private static b(LX/2S0;J)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 410456
    new-instance v1, Landroid/app/DownloadManager$Query;

    invoke-direct {v1}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 410457
    const/4 v2, 0x1

    new-array v2, v2, [J

    aput-wide p1, v2, v0

    invoke-virtual {v1, v2}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 410458
    iget-object v2, p0, LX/2S0;->f:Landroid/app/DownloadManager;

    invoke-virtual {v2, v1}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v1

    .line 410459
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 410460
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return v0

    .line 410461
    :cond_0
    :try_start_1
    const-string v0, "status"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 410462
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final declared-synchronized init()V
    .locals 4

    .prologue
    .line 410463
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/2S2;

    invoke-direct {v0, p0}, LX/2S2;-><init>(LX/2S0;)V

    .line 410464
    new-instance v1, LX/0Yd;

    const-string v2, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-static {v2, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, LX/2S0;->j:LX/0Yd;

    .line 410465
    iget-object v0, p0, LX/2S0;->e:Landroid/content/Context;

    iget-object v1, p0, LX/2S0;->j:LX/0Yd;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 410466
    iget-object v0, p0, LX/2S0;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410467
    monitor-exit p0

    return-void

    .line 410468
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
