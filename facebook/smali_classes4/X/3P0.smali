.class public final enum LX/3P0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3P0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3P0;

.field public static final enum ABBREVIATED:LX/3P0;

.field public static final enum SHORT:LX/3P0;

.field public static final enum VERBOSE:LX/3P0;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 561022
    new-instance v0, LX/3P0;

    const-string v1, "VERBOSE"

    invoke-direct {v0, v1, v2}, LX/3P0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3P0;->VERBOSE:LX/3P0;

    .line 561023
    new-instance v0, LX/3P0;

    const-string v1, "ABBREVIATED"

    invoke-direct {v0, v1, v3}, LX/3P0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3P0;->ABBREVIATED:LX/3P0;

    .line 561024
    new-instance v0, LX/3P0;

    const-string v1, "SHORT"

    invoke-direct {v0, v1, v4}, LX/3P0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3P0;->SHORT:LX/3P0;

    .line 561025
    const/4 v0, 0x3

    new-array v0, v0, [LX/3P0;

    sget-object v1, LX/3P0;->VERBOSE:LX/3P0;

    aput-object v1, v0, v2

    sget-object v1, LX/3P0;->ABBREVIATED:LX/3P0;

    aput-object v1, v0, v3

    sget-object v1, LX/3P0;->SHORT:LX/3P0;

    aput-object v1, v0, v4

    sput-object v0, LX/3P0;->$VALUES:[LX/3P0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 561026
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3P0;
    .locals 1

    .prologue
    .line 561027
    const-class v0, LX/3P0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3P0;

    return-object v0
.end method

.method public static values()[LX/3P0;
    .locals 1

    .prologue
    .line 561028
    sget-object v0, LX/3P0;->$VALUES:[LX/3P0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3P0;

    return-object v0
.end method
