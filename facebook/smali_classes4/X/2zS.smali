.class public LX/2zS;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2zS;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 6
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 483425
    invoke-direct {p0}, LX/398;-><init>()V

    .line 483426
    const-string v0, "faceweb/f?href={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mobile_page"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "target_fragment"

    aput-object v2, v1, v4

    sget-object v2, LX/0cQ;->FACEWEB_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v1}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, LX/398;->a(Ljava/lang/String;LX/0Or;Landroid/os/Bundle;)V

    .line 483427
    const-string v0, "facewebmodal/f?href={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mobile_page"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "faceweb_modal"

    aput-object v2, v1, v4

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v1}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, LX/398;->a(Ljava/lang/String;LX/0Or;Landroid/os/Bundle;)V

    .line 483428
    return-void
.end method

.method public static a(LX/0QB;)LX/2zS;
    .locals 4

    .prologue
    .line 483429
    sget-object v0, LX/2zS;->a:LX/2zS;

    if-nez v0, :cond_1

    .line 483430
    const-class v1, LX/2zS;

    monitor-enter v1

    .line 483431
    :try_start_0
    sget-object v0, LX/2zS;->a:LX/2zS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 483432
    if-eqz v2, :cond_0

    .line 483433
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 483434
    new-instance v3, LX/2zS;

    const/16 p0, 0xc

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2zS;-><init>(LX/0Or;)V

    .line 483435
    move-object v0, v3

    .line 483436
    sput-object v0, LX/2zS;->a:LX/2zS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483437
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 483438
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 483439
    :cond_1
    sget-object v0, LX/2zS;->a:LX/2zS;

    return-object v0

    .line 483440
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 483441
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
