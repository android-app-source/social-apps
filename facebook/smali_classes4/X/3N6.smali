.class public LX/3N6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/3N7;

.field private final b:LX/0ad;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;LX/3N7;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/3N7;",
            "LX/0Or",
            "<",
            "LX/2Oi;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558210
    iput-object p1, p0, LX/3N6;->b:LX/0ad;

    .line 558211
    iput-object p2, p0, LX/3N6;->a:LX/3N7;

    .line 558212
    iput-object p3, p0, LX/3N6;->c:LX/0Or;

    .line 558213
    return-void
.end method

.method public static a(LX/0QB;)LX/3N6;
    .locals 6

    .prologue
    .line 558214
    const-class v1, LX/3N6;

    monitor-enter v1

    .line 558215
    :try_start_0
    sget-object v0, LX/3N6;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 558216
    sput-object v2, LX/3N6;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 558217
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558218
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 558219
    new-instance v5, LX/3N6;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    .line 558220
    new-instance v4, LX/3N7;

    const/16 p0, 0x12c7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, p0}, LX/3N7;-><init>(LX/0Or;)V

    .line 558221
    move-object v4, v4

    .line 558222
    check-cast v4, LX/3N7;

    const/16 p0, 0x12c7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/3N6;-><init>(LX/0ad;LX/3N7;LX/0Or;)V

    .line 558223
    move-object v0, v5

    .line 558224
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 558225
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3N6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 558226
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 558227
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static final a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Landroid/net/Uri;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 558228
    invoke-static {p0}, LX/3N6;->b(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 558229
    :cond_0
    :goto_0
    return-object v0

    .line 558230
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    .line 558231
    :goto_1
    if-eqz v1, :cond_0

    .line 558232
    invoke-interface {v1}, LX/5Ua;->w()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommercePromotionsModel$PromotionItemsModel;

    move-result-object v1

    .line 558233
    if-eqz v1, :cond_0

    .line 558234
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommercePromotionsModel$PromotionItemsModel;->a()LX/0Px;

    move-result-object v1

    .line 558235
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 558236
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5UY;

    invoke-interface {v0}, LX/5UY;->cD_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 558237
    goto :goto_1
.end method

.method public static final a(Ljava/lang/String;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 558238
    invoke-static {p1}, LX/3N6;->b(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 558239
    :cond_0
    :goto_0
    return-object v0

    .line 558240
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    .line 558241
    :goto_1
    if-eqz v1, :cond_0

    .line 558242
    invoke-interface {v1}, LX/5Ua;->d()LX/0Px;

    move-result-object v1

    .line 558243
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 558244
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    invoke-static {v0}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v0

    .line 558245
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 558246
    new-instance v1, LX/4gf;

    invoke-direct {v1, v0}, LX/4gf;-><init>(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)V

    .line 558247
    iput-object p0, v1, LX/4gf;->f:Ljava/lang/String;

    .line 558248
    move-object v0, v1

    .line 558249
    invoke-virtual {v0}, LX/4gf;->n()Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 558250
    goto :goto_1
.end method

.method private static b(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Z
    .locals 1

    .prologue
    .line 558251
    if-eqz p0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RETAIL_PROMOTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {p0, v0}, LX/6lp;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
