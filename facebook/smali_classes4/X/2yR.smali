.class public LX/2yR;
.super LX/2y5;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ":",
        "LX/1Po;",
        ">",
        "LX/2y5",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/03V;

.field public final b:LX/20p;

.field private final c:LX/0ad;

.field private final d:LX/2yS;

.field private final e:LX/1Nt;

.field private final f:LX/2yT;


# direct methods
.method public constructor <init>(LX/20p;LX/03V;LX/2yS;LX/0ad;LX/2yT;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481933
    invoke-direct {p0}, LX/2y5;-><init>()V

    .line 481934
    iput-object p1, p0, LX/2yR;->b:LX/20p;

    .line 481935
    iput-object p2, p0, LX/2yR;->a:LX/03V;

    .line 481936
    iput-object p3, p0, LX/2yR;->d:LX/2yS;

    .line 481937
    iput-object p4, p0, LX/2yR;->c:LX/0ad;

    .line 481938
    iput-object p5, p0, LX/2yR;->f:LX/2yT;

    .line 481939
    new-instance v0, Lcom/facebook/attachments/angora/actionbutton/ShareActionButton$ShareActionButtonPartDefinition;

    invoke-direct {v0, p0}, Lcom/facebook/attachments/angora/actionbutton/ShareActionButton$ShareActionButtonPartDefinition;-><init>(LX/2yR;)V

    iput-object v0, p0, LX/2yR;->e:LX/1Nt;

    .line 481940
    return-void
.end method

.method public static a(LX/0QB;)LX/2yR;
    .locals 9

    .prologue
    .line 481922
    const-class v1, LX/2yR;

    monitor-enter v1

    .line 481923
    :try_start_0
    sget-object v0, LX/2yR;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481924
    sput-object v2, LX/2yR;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481925
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481926
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 481927
    new-instance v3, LX/2yR;

    invoke-static {v0}, LX/20p;->a(LX/0QB;)LX/20p;

    move-result-object v4

    check-cast v4, LX/20p;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/2yS;->a(LX/0QB;)LX/2yS;

    move-result-object v6

    check-cast v6, LX/2yS;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    const-class v8, LX/2yT;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/2yT;

    invoke-direct/range {v3 .. v8}, LX/2yR;-><init>(LX/20p;LX/03V;LX/2yS;LX/0ad;LX/2yT;)V

    .line 481928
    move-object v0, v3

    .line 481929
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481930
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2yR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481931
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481932
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 481920
    invoke-static {p0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 481921
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_0

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 481905
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 481906
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 481907
    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Nt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "LX/35p;",
            ">()",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation

    .prologue
    .line 481919
    iget-object v0, p0, LX/2yR;->e:LX/1Nt;

    return-object v0
.end method

.method public final a(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/AE0;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/AE0;"
        }
    .end annotation

    .prologue
    .line 481908
    invoke-static {p3}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 481909
    if-nez v0, :cond_0

    .line 481910
    iget-object v0, p0, LX/2yR;->a:LX/03V;

    const-class v1, LX/2yR;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "attachment.getParentStory() is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 481911
    const/4 v0, 0x0

    .line 481912
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/2yR;->f:LX/2yT;

    const/4 v2, 0x2

    iget-object v3, p0, LX/2yR;->d:LX/2yS;

    invoke-virtual {v1, p4, v2, v3}, LX/2yT;->a(ZILX/2yS;)LX/AE0;

    move-result-object v1

    const v2, 0x7f080fd9

    invoke-virtual {p1, v2}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 481913
    iput-object v2, v1, LX/AE0;->f:Ljava/lang/CharSequence;

    .line 481914
    move-object v1, v1

    .line 481915
    new-instance v2, LX/AEp;

    invoke-static {v0}, LX/2yR;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    check-cast p2, LX/1Po;

    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-direct {v2, p0, v0, v3}, LX/AEp;-><init>(LX/2yR;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;)V

    .line 481916
    iput-object v2, v1, LX/AE0;->o:Landroid/view/View$OnClickListener;

    .line 481917
    move-object v0, v1

    .line 481918
    goto :goto_0
.end method
