.class public final enum LX/29X;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/29X;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/29X;

.field public static final enum DONE:LX/29X;

.field public static final enum FAILED:LX/29X;

.field public static final enum NOT_READY:LX/29X;

.field public static final enum READY:LX/29X;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 376353
    new-instance v0, LX/29X;

    const-string v1, "READY"

    invoke-direct {v0, v1, v2}, LX/29X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29X;->READY:LX/29X;

    .line 376354
    new-instance v0, LX/29X;

    const-string v1, "NOT_READY"

    invoke-direct {v0, v1, v3}, LX/29X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29X;->NOT_READY:LX/29X;

    .line 376355
    new-instance v0, LX/29X;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, LX/29X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29X;->DONE:LX/29X;

    .line 376356
    new-instance v0, LX/29X;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/29X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/29X;->FAILED:LX/29X;

    .line 376357
    const/4 v0, 0x4

    new-array v0, v0, [LX/29X;

    sget-object v1, LX/29X;->READY:LX/29X;

    aput-object v1, v0, v2

    sget-object v1, LX/29X;->NOT_READY:LX/29X;

    aput-object v1, v0, v3

    sget-object v1, LX/29X;->DONE:LX/29X;

    aput-object v1, v0, v4

    sget-object v1, LX/29X;->FAILED:LX/29X;

    aput-object v1, v0, v5

    sput-object v0, LX/29X;->$VALUES:[LX/29X;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 376358
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/29X;
    .locals 1

    .prologue
    .line 376359
    const-class v0, LX/29X;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/29X;

    return-object v0
.end method

.method public static values()[LX/29X;
    .locals 1

    .prologue
    .line 376360
    sget-object v0, LX/29X;->$VALUES:[LX/29X;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/29X;

    return-object v0
.end method
