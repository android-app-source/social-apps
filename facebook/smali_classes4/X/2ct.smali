.class public LX/2ct;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2cu;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/2ct;


# instance fields
.field private final a:LX/0Uo;

.field public final b:LX/0SG;

.field private final c:LX/2cv;

.field private final d:LX/2cw;

.field private final e:LX/0bT;

.field private final f:LX/2cy;

.field private g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLInterfaces$LocationTriggerWithReactionUnits;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uo;LX/0SG;LX/2cv;LX/2cw;LX/0bT;LX/2cy;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 442291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442292
    const/4 v0, 0x0

    iput-object v0, p0, LX/2ct;->g:LX/0am;

    .line 442293
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2ct;->h:LX/0am;

    .line 442294
    iput-object p1, p0, LX/2ct;->a:LX/0Uo;

    .line 442295
    iput-object p2, p0, LX/2ct;->b:LX/0SG;

    .line 442296
    iput-object p3, p0, LX/2ct;->c:LX/2cv;

    .line 442297
    iput-object p4, p0, LX/2ct;->d:LX/2cw;

    .line 442298
    iput-object p5, p0, LX/2ct;->e:LX/0bT;

    .line 442299
    iput-object p6, p0, LX/2ct;->f:LX/2cy;

    .line 442300
    return-void
.end method

.method private static declared-synchronized a(LX/2ct;Lcom/facebook/placetips/bootstrap/PresenceDescription;LX/2cx;)LX/0bX;
    .locals 3
    .param p0    # LX/2ct;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 442380
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2ct;->g:LX/0am;

    .line 442381
    invoke-direct {p0, p1}, LX/2ct;->a(Lcom/facebook/placetips/bootstrap/PresenceDescription;)V

    .line 442382
    new-instance v1, LX/0bX;

    iget-object v2, p0, LX/2ct;->g:LX/0am;

    invoke-direct {v1, v0, v2}, LX/0bX;-><init>(LX/0am;LX/0am;)V

    .line 442383
    iget-object v0, p0, LX/2ct;->e:LX/0bT;

    invoke-virtual {v0, v1}, LX/0bT;->a(LX/0bY;)V

    .line 442384
    iget-object v0, p0, LX/2ct;->d:LX/2cw;

    invoke-virtual {v0, v1, p2}, LX/2cw;->a(LX/0bZ;LX/2cx;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 442385
    monitor-exit p0

    return-object v1

    .line 442386
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/2ct;LX/CeG;Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/0bb;
    .locals 10
    .param p0    # LX/2ct;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x0

    .line 442374
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2ct;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442375
    iget-object v0, p0, LX/2ct;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    iget-object v0, p0, LX/2ct;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    if-nez p1, :cond_1

    move-object v4, v9

    :goto_0
    if-nez p1, :cond_2

    move-object v5, v9

    :goto_1
    if-nez p1, :cond_3

    move-object v6, v9

    :goto_2
    if-nez p1, :cond_4

    move-object v7, v9

    :goto_3
    if-nez p1, :cond_5

    :goto_4
    move-object v8, p2

    invoke-virtual/range {v1 .. v9}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->a(JLcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;LX/9qT;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/placetips/bootstrap/PresenceSource;LX/Cdj;)Lcom/facebook/placetips/bootstrap/PresenceDescription;

    move-result-object v0

    .line 442376
    invoke-direct {p0, v0}, LX/2ct;->a(Lcom/facebook/placetips/bootstrap/PresenceDescription;)V

    .line 442377
    :cond_0
    invoke-static {p0, p2}, LX/2ct;->b(LX/2ct;Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/0bb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 442378
    :cond_1
    :try_start_1
    iget-object v4, p1, LX/CeG;->f:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    goto :goto_0

    :cond_2
    iget-object v5, p1, LX/CeG;->m:LX/9qT;

    goto :goto_1

    :cond_3
    iget-object v6, p1, LX/CeG;->n:Ljava/lang/String;

    goto :goto_2

    :cond_4
    iget-object v7, p1, LX/CeG;->o:Ljava/lang/String;

    goto :goto_3

    :cond_5
    iget-object v9, p1, LX/CeG;->p:LX/Cdj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 442379
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/0QB;)LX/2ct;
    .locals 10

    .prologue
    .line 442361
    sget-object v0, LX/2ct;->i:LX/2ct;

    if-nez v0, :cond_1

    .line 442362
    const-class v1, LX/2ct;

    monitor-enter v1

    .line 442363
    :try_start_0
    sget-object v0, LX/2ct;->i:LX/2ct;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 442364
    if-eqz v2, :cond_0

    .line 442365
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 442366
    new-instance v3, LX/2ct;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v4

    check-cast v4, LX/0Uo;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/2cv;->b(LX/0QB;)LX/2cv;

    move-result-object v6

    check-cast v6, LX/2cv;

    invoke-static {v0}, LX/2cw;->a(LX/0QB;)LX/2cw;

    move-result-object v7

    check-cast v7, LX/2cw;

    invoke-static {v0}, LX/0bT;->a(LX/0QB;)LX/0bT;

    move-result-object v8

    check-cast v8, LX/0bT;

    invoke-static {v0}, LX/2cy;->b(LX/0QB;)LX/2cy;

    move-result-object v9

    check-cast v9, LX/2cy;

    invoke-direct/range {v3 .. v9}, LX/2ct;-><init>(LX/0Uo;LX/0SG;LX/2cv;LX/2cw;LX/0bT;LX/2cy;)V

    .line 442367
    move-object v0, v3

    .line 442368
    sput-object v0, LX/2ct;->i:LX/2ct;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 442369
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 442370
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 442371
    :cond_1
    sget-object v0, LX/2ct;->i:LX/2ct;

    return-object v0

    .line 442372
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 442373
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2ct;Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;)V
    .locals 1
    .param p0    # LX/2ct;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 442358
    if-nez p1, :cond_0

    iget-object v0, p0, LX/2ct;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 442359
    :cond_0
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2ct;->h:LX/0am;

    .line 442360
    :cond_1
    return-void
.end method

.method private declared-synchronized a(Lcom/facebook/placetips/bootstrap/PresenceDescription;)V
    .locals 1
    .param p1    # Lcom/facebook/placetips/bootstrap/PresenceDescription;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 442351
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2ct;->g:LX/0am;

    .line 442352
    iget-object v0, p0, LX/2ct;->f:LX/2cy;

    invoke-virtual {v0}, LX/2cy;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 442353
    :goto_0
    monitor-exit p0

    return-void

    .line 442354
    :cond_0
    if-eqz p1, :cond_1

    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object v0

    iget-boolean v0, v0, LX/2cx;->isPersistent:Z

    if-nez v0, :cond_2

    .line 442355
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2ct;->b(Lcom/facebook/placetips/bootstrap/PresenceDescription;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 442356
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 442357
    :cond_2
    :try_start_2
    invoke-direct {p0, p1}, LX/2ct;->b(Lcom/facebook/placetips/bootstrap/PresenceDescription;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized a$redex0(LX/2ct;LX/CeG;)LX/0bZ;
    .locals 5

    .prologue
    .line 442335
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, LX/CeG;->q:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;

    invoke-static {p0, v0}, LX/2ct;->a(LX/2ct;Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;)V

    .line 442336
    iget-object v0, p0, LX/2ct;->a:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2ct;->f:LX/2cy;

    invoke-virtual {v0}, LX/2cy;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 442337
    sget-object v0, LX/2cx;->FORCE_OFF:LX/2cx;

    invoke-virtual {p0, v0}, LX/2ct;->a(LX/2cx;)LX/0bZ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 442338
    :goto_0
    monitor-exit p0

    return-object v0

    .line 442339
    :cond_0
    :try_start_1
    iget-object v0, p1, LX/CeG;->b:Lcom/facebook/placetips/bootstrap/PresenceSource;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceSource;->a()LX/2cx;

    move-result-object v1

    .line 442340
    iget-object v0, p0, LX/2ct;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p1, LX/CeG;->c:Ljava/lang/String;

    iget-object v0, p0, LX/2ct;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 442341
    iget-object v0, p1, LX/CeG;->b:Lcom/facebook/placetips/bootstrap/PresenceSource;

    invoke-static {p0, p1, v0}, LX/2ct;->a(LX/2ct;LX/CeG;Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/0bb;

    move-result-object v0

    goto :goto_0

    .line 442342
    :cond_1
    invoke-virtual {p1}, LX/CeG;->b()Lcom/facebook/placetips/bootstrap/PresenceDescription;

    move-result-object v2

    .line 442343
    iget-object v0, p0, LX/2ct;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_2

    .line 442344
    invoke-static {p0, v2, v1}, LX/2ct;->a(LX/2ct;Lcom/facebook/placetips/bootstrap/PresenceDescription;LX/2cx;)LX/0bX;

    move-result-object v0

    goto :goto_0

    .line 442345
    :cond_2
    iget-object v0, p0, LX/2ct;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    .line 442346
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object v3

    .line 442347
    invoke-virtual {v1, v3}, LX/2cx;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iget v4, v1, LX/2cx;->foundPriority:I

    iget v3, v3, LX/2cx;->foundPriority:I

    if-lt v4, v3, :cond_4

    .line 442348
    :cond_3
    invoke-static {p0, v2, v1}, LX/2ct;->a(LX/2ct;Lcom/facebook/placetips/bootstrap/PresenceDescription;LX/2cx;)LX/0bX;

    move-result-object v0

    goto :goto_0

    .line 442349
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->k()Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v0

    invoke-static {p0, v0}, LX/2ct;->b(LX/2ct;Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/0bb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 442350
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/2ct;Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/0bb;
    .locals 3

    .prologue
    .line 442330
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/0bb;

    iget-object v1, p0, LX/2ct;->g:LX/0am;

    invoke-direct {v0, v1}, LX/0bb;-><init>(LX/0am;)V

    .line 442331
    iget-object v1, p0, LX/2ct;->e:LX/0bT;

    invoke-virtual {v1, v0}, LX/0bT;->a(LX/0bY;)V

    .line 442332
    iget-object v1, p0, LX/2ct;->d:LX/2cw;

    invoke-virtual {p1}, Lcom/facebook/placetips/bootstrap/PresenceSource;->a()LX/2cx;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/2cw;->a(LX/0bZ;LX/2cx;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 442333
    monitor-exit p0

    return-object v0

    .line 442334
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lcom/facebook/placetips/bootstrap/PresenceDescription;)V
    .locals 2
    .param p1    # Lcom/facebook/placetips/bootstrap/PresenceDescription;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 442387
    iget-object v0, p0, LX/2ct;->c:LX/2cv;

    invoke-virtual {v0}, LX/2cv;->a()LX/3C9;

    move-result-object v0

    sget-object v1, LX/CeL;->a:LX/2d5;

    invoke-interface {v0, v1, p1}, LX/3C9;->a(LX/2d5;Ljava/lang/Object;)LX/3C9;

    move-result-object v0

    invoke-interface {v0}, LX/3C9;->a()V

    .line 442388
    return-void
.end method

.method private static declared-synchronized c(LX/2ct;)V
    .locals 2

    .prologue
    .line 442325
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2ct;->g:LX/0am;

    if-nez v0, :cond_0

    .line 442326
    iget-object v0, p0, LX/2ct;->f:LX/2cy;

    invoke-virtual {v0}, LX/2cy;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2ct;->c:LX/2cv;

    sget-object v1, LX/CeL;->a:LX/2d5;

    invoke-virtual {v0, v1}, LX/2cv;->a(LX/2d5;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/2ct;->g:LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 442327
    :cond_0
    monitor-exit p0

    return-void

    .line 442328
    :cond_1
    :try_start_1
    invoke-static {}, LX/0am;->absent()LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 442329
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442323
    invoke-static {p0}, LX/2ct;->c(LX/2ct;)V

    .line 442324
    iget-object v0, p0, LX/2ct;->g:LX/0am;

    return-object v0
.end method

.method public final declared-synchronized a(LX/2cx;)LX/0bZ;
    .locals 2

    .prologue
    .line 442312
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2ct;->c(LX/2ct;)V

    .line 442313
    sget-object v0, LX/2cx;->FORCE_OFF:LX/2cx;

    if-ne p1, v0, :cond_0

    .line 442314
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/2ct;->h:LX/0am;

    .line 442315
    :cond_0
    iget-object v0, p0, LX/2ct;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 442316
    const/4 v0, 0x0

    invoke-static {p1}, Lcom/facebook/placetips/bootstrap/PresenceSource;->a(LX/2cx;)Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/2ct;->a(LX/2ct;LX/CeG;Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/0bb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 442317
    :goto_0
    monitor-exit p0

    return-object v0

    .line 442318
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2ct;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object v0

    .line 442319
    invoke-virtual {p1, v0}, LX/2cx;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p1, LX/2cx;->nothingFoundPriority:I

    iget v0, v0, LX/2cx;->foundPriority:I

    if-le v1, v0, :cond_3

    .line 442320
    :cond_2
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, LX/2ct;->a(LX/2ct;Lcom/facebook/placetips/bootstrap/PresenceDescription;LX/2cx;)LX/0bX;

    move-result-object v0

    goto :goto_0

    .line 442321
    :cond_3
    iget-object v0, p0, LX/2ct;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->k()Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v0

    invoke-static {p0, v0}, LX/2ct;->b(LX/2ct;Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/0bb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 442322
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2cx;Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;)LX/0bZ;
    .locals 1
    .param p2    # Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 442308
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2ct;->c(LX/2ct;)V

    .line 442309
    invoke-static {p0, p2}, LX/2ct;->a(LX/2ct;Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel;)V

    .line 442310
    invoke-virtual {p0, p1}, LX/2ct;->a(LX/2cx;)LX/0bZ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 442311
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/CeG;
    .locals 2

    .prologue
    .line 442306
    invoke-static {p0}, LX/2ct;->c(LX/2ct;)V

    .line 442307
    new-instance v0, LX/CeG;

    invoke-direct {v0, p0, p1}, LX/CeG;-><init>(LX/2ct;Lcom/facebook/placetips/bootstrap/PresenceSource;)V

    return-object v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 442302
    if-eqz p1, :cond_0

    .line 442303
    sget-object v0, LX/2cx;->FORCE_OFF:LX/2cx;

    invoke-virtual {p0, v0}, LX/2ct;->a(LX/2cx;)LX/0bZ;

    .line 442304
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2ct;->b(Lcom/facebook/placetips/bootstrap/PresenceDescription;)V

    .line 442305
    return-void
.end method

.method public final b()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLInterfaces$LocationTriggerWithReactionUnits;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442301
    iget-object v0, p0, LX/2ct;->h:LX/0am;

    return-object v0
.end method
