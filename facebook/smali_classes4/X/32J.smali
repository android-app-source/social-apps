.class public LX/32J;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/32J",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 489670
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 489671
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/32J;->b:LX/0Zi;

    .line 489672
    iput-object p1, p0, LX/32J;->a:LX/0Ot;

    .line 489673
    return-void
.end method

.method public static a(LX/0QB;)LX/32J;
    .locals 4

    .prologue
    .line 489741
    const-class v1, LX/32J;

    monitor-enter v1

    .line 489742
    :try_start_0
    sget-object v0, LX/32J;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 489743
    sput-object v2, LX/32J;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 489744
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489745
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 489746
    new-instance v3, LX/32J;

    const/16 p0, 0x1d35

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/32J;-><init>(LX/0Ot;)V

    .line 489747
    move-object v0, v3

    .line 489748
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 489749
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/32J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 489750
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 489751
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 10

    .prologue
    .line 489752
    check-cast p2, LX/Bsc;

    .line 489753
    iget-object v0, p0, LX/32J;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;

    iget-object v1, p2, LX/Bsc;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 489754
    iget-object v2, v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->g:LX/BsZ;

    .line 489755
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 489756
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 489757
    new-instance v4, LX/4E9;

    invoke-direct {v4}, LX/4E9;-><init>()V

    .line 489758
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    .line 489759
    const-string v6, "story_id"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 489760
    new-instance v5, LX/802;

    invoke-direct {v5}, LX/802;-><init>()V

    move-object v5, v5

    .line 489761
    const-string v6, "input"

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 489762
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 489763
    iget-object v5, v2, LX/BsZ;->d:LX/1Ck;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "task_key_disassociate_post_with_fundraiser_upsell:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v2, LX/BsZ;->c:LX/0tX;

    invoke-virtual {v6, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v6, LX/BsY;

    invoke-direct {v6, v2}, LX/BsY;-><init>(LX/BsZ;)V

    invoke-virtual {v5, v3, v4, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 489764
    iget-object v2, v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->e:LX/189;

    .line 489765
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 489766
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v5}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v5

    iget-object v6, v2, LX/189;->i:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v7

    .line 489767
    iput-wide v7, v5, LX/23u;->G:J

    .line 489768
    move-object v5, v5

    .line 489769
    const/4 v6, 0x0

    .line 489770
    iput-object v6, v5, LX/23u;->ay:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 489771
    move-object v5, v5

    .line 489772
    invoke-virtual {v5}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 489773
    invoke-virtual {v2, v5, v1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    move-object v2, v5

    .line 489774
    invoke-static {v2}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    .line 489775
    iget-object v3, v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->d:LX/0bH;

    new-instance v4, LX/1Ne;

    invoke-direct {v4, v2}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    .line 489776
    return-void
.end method

.method public static d(LX/1De;)V
    .locals 3

    .prologue
    .line 489710
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 489711
    if-nez v0, :cond_0

    .line 489712
    :goto_0
    return-void

    .line 489713
    :cond_0
    check-cast v0, LX/Bsc;

    .line 489714
    new-instance v1, LX/Bsb;

    iget-object v2, v0, LX/Bsc;->d:LX/32J;

    invoke-direct {v1, v2}, LX/Bsb;-><init>(LX/32J;)V

    move-object v0, v1

    .line 489715
    invoke-virtual {p0, v0}, LX/1De;->a(LX/48B;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 489716
    check-cast p2, LX/Bsc;

    .line 489717
    iget-object v0, p0, LX/32J;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;

    iget-boolean v1, p2, LX/Bsc;->a:Z

    iget-object v2, p2, LX/Bsc;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 489718
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 489719
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    move-object v5, v3

    .line 489720
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 489721
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    move-object v6, v3

    .line 489722
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 489723
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->p()LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    move-object v7, v3

    .line 489724
    if-eqz v1, :cond_0

    .line 489725
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 489726
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bu()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 489727
    move-object v4, v3

    .line 489728
    :goto_0
    iget-object v3, v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->h:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const v8, 0x7f0a00a6

    invoke-virtual {v3, v8}, LX/2xv;->j(I)LX/2xv;

    move-result-object v3

    const v8, 0x7f02081c

    invoke-virtual {v3, v8}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v8

    .line 489729
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 p0, 0x0

    invoke-interface {v3, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 p0, 0x1

    invoke-interface {v3, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 p0, 0x6

    const p2, 0x7f0b10e9

    invoke-interface {v3, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v3, p0}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const p0, 0x7f0b0050

    invoke-virtual {v5, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const p0, 0x7f0a00ab

    invoke-virtual {v5, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 p0, 0x3

    const p2, 0x7f0b10e8

    invoke-interface {v5, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004e

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00a8

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x3

    const p0, 0x7f0b10e9

    invoke-interface {v5, v6, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    .line 489730
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x2

    invoke-interface {v3, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x1

    invoke-interface {v3, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    iget-object v3, v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-virtual {v3, v7}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v3

    sget-object v7, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v3, v7}, LX/1up;->c(F)LX/1up;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v7, 0x7f0b10e7

    invoke-interface {v3, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5, v8}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    .line 489731
    const v6, -0x66420aa0

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 489732
    invoke-interface {v5, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 489733
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    iget-object v3, v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->f:LX/2g9;

    invoke-virtual {v3, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object v3

    const/16 v4, 0x101

    invoke-virtual {v3, v4}, LX/2gA;->h(I)LX/2gA;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    if-eqz v1, :cond_1

    const/4 v3, 0x0

    :goto_1
    invoke-interface {v4, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 489734
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 489735
    return-object v0

    .line 489736
    :cond_0
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 489737
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 489738
    move-object v4, v3

    goto/16 :goto_0

    .line 489739
    :cond_1
    const v3, -0x15dd141

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-static {p1, v3, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 489740
    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 489688
    invoke-static {}, LX/1dS;->b()V

    .line 489689
    iget v0, p1, LX/1dQ;->b:I

    .line 489690
    sparse-switch v0, :sswitch_data_0

    .line 489691
    :goto_0
    return-object v3

    .line 489692
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 489693
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/32J;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 489694
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 489695
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/1De;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 489696
    check-cast v2, LX/Bsc;

    .line 489697
    iget-object v4, p0, LX/32J;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;

    iget-object v5, v2, LX/Bsc;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 489698
    iget-object v6, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 489699
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v6

    move-object v6, v6

    .line 489700
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 489701
    iget-object v7, v4, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->g:LX/BsZ;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bs()Lcom/facebook/graphql/model/GraphQLFundraiserCharity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFundraiserCharity;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v5, v6}, LX/BsZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 489702
    invoke-static {v0}, LX/32J;->d(LX/1De;)V

    .line 489703
    :goto_1
    goto :goto_0

    .line 489704
    :cond_0
    new-instance v7, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec$1;

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v7, v4, p1, v5, v0}, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec$1;-><init>(Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;Landroid/os/Handler;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1De;)V

    .line 489705
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 489706
    const-string p2, "result_receiver"

    invoke-virtual {p1, p2, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 489707
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 489708
    iget-object v7, v4, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->c:LX/17W;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    sget-object p0, LX/0ax;->hf:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bv()Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFundraiserCreatePromo;->j()Ljava/lang/String;

    move-result-object v6

    const-string v2, "fundraiser_for_story_upsell"

    invoke-static {p0, v6, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, p2, v6, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_1

    .line 489709
    :cond_1
    iget-object v6, v4, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->c:LX/17W;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    sget-object p2, LX/0ax;->hb:Ljava/lang/String;

    const-string p0, "fundraiser_for_story_upsell"

    invoke-static {p2, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v6, v7, p2, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x66420aa0 -> :sswitch_0
        -0x15dd141 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 489684
    check-cast p1, LX/Bsc;

    .line 489685
    check-cast p2, LX/Bsc;

    .line 489686
    iget-boolean v0, p1, LX/Bsc;->a:Z

    iput-boolean v0, p2, LX/Bsc;->a:Z

    .line 489687
    return-void
.end method

.method public final d(LX/1De;LX/1X1;)V
    .locals 2

    .prologue
    .line 489675
    check-cast p2, LX/Bsc;

    .line 489676
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 489677
    iget-object v0, p0, LX/32J;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 489678
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 489679
    iput-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 489680
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 489681
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/Bsc;->a:Z

    .line 489682
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 489683
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 489674
    const/4 v0, 0x1

    return v0
.end method
