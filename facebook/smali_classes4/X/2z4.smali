.class public final LX/2z4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ed;


# direct methods
.method public constructor <init>(LX/0ed;)V
    .locals 0

    .prologue
    .line 482962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482963
    iput-object p1, p0, LX/2z4;->a:LX/0ed;

    .line 482964
    return-void
.end method

.method public static d(LX/2z4;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 482965
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 482966
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "app_locale"

    invoke-static {p0}, LX/2z4;->e(LX/2z4;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 482967
    iget-object v0, p0, LX/2z4;->a:LX/0ed;

    .line 482968
    iget-object v2, v0, LX/0ed;->c:LX/0WV;

    move-object v0, v2

    .line 482969
    invoke-virtual {v0}, LX/0WV;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 482970
    iget-object v0, p0, LX/2z4;->a:LX/0ed;

    invoke-virtual {v0}, LX/0ed;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 482971
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "release_number"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 482972
    :goto_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "release_package"

    iget-object v3, p0, LX/2z4;->a:LX/0ed;

    .line 482973
    iget-object v4, v3, LX/0ed;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 482974
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 482975
    invoke-virtual {p0}, LX/2z4;->a()LX/0eh;

    move-result-object v2

    .line 482976
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "file_format"

    invoke-virtual {v2}, LX/0eh;->getServerValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 482977
    invoke-virtual {p0}, LX/2z4;->b()Z

    move-result v3

    .line 482978
    if-eqz v3, :cond_0

    .line 482979
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "content_checksum"

    iget-object v0, p0, LX/2z4;->a:LX/0ed;

    invoke-virtual {v0}, LX/0ed;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v4, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 482980
    :cond_0
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v0

    .line 482981
    const-string v4, "download_url"

    invoke-virtual {v0, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 482982
    const-string v4, "download_checksum"

    invoke-virtual {v0, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 482983
    sget-object v4, LX/0eh;->LANGPACK:LX/0eh;

    if-ne v2, v4, :cond_1

    .line 482984
    const-string v2, "content_checksum"

    invoke-virtual {v0, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 482985
    :cond_1
    const-string v2, "release_number"

    invoke-virtual {v0, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 482986
    if-eqz v3, :cond_2

    .line 482987
    const-string v2, "delta"

    invoke-virtual {v0, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 482988
    :cond_2
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "fields"

    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 482989
    return-object v1

    .line 482990
    :cond_3
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "string_resources_hash"

    iget-object v3, p0, LX/2z4;->a:LX/0ed;

    .line 482991
    iget-object v4, v3, LX/0ed;->f:Ljava/lang/String;

    move-object v3, v4

    .line 482992
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public static e(LX/2z4;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 482993
    iget-object v0, p0, LX/2z4;->a:LX/0ed;

    invoke-virtual {v0}, LX/0ed;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/0eh;
    .locals 1

    .prologue
    .line 482994
    iget-object v0, p0, LX/2z4;->a:LX/0ed;

    .line 482995
    iget-object p0, v0, LX/0ed;->e:LX/0eh;

    move-object v0, p0

    .line 482996
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 482997
    invoke-virtual {p0}, LX/2z4;->a()LX/0eh;

    move-result-object v0

    sget-object v1, LX/0eh;->LANGPACK:LX/0eh;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/2z4;->a:LX/0ed;

    invoke-virtual {v0}, LX/0ed;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/util/Map;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 482998
    invoke-static {p0}, LX/2z4;->d(LX/2z4;)Ljava/util/List;

    move-result-object v0

    .line 482999
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 483000
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 483001
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "request_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 483002
    :cond_0
    return-object v1
.end method
