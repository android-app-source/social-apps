.class public final LX/28X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0cN;


# instance fields
.field public final synthetic a:LX/0Uo;


# direct methods
.method public constructor <init>(LX/0Uo;)V
    .locals 0

    .prologue
    .line 374218
    iput-object p1, p0, LX/28X;->a:LX/0Uo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0cK;)V
    .locals 3

    .prologue
    .line 374219
    iget-object v0, p0, LX/28X;->a:LX/0Uo;

    iget-object v0, v0, LX/0Uo;->X:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

    .line 374220
    if-nez v0, :cond_0

    .line 374221
    :goto_0
    return-void

    .line 374222
    :cond_0
    iget-boolean v1, v0, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->a:Z

    if-eqz v1, :cond_1

    .line 374223
    iget-object v1, p0, LX/28X;->a:LX/0Uo;

    invoke-static {v1}, LX/0Uo;->P(LX/0Uo;)V

    .line 374224
    :cond_1
    iget-boolean v1, v0, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;->b:Z

    if-eqz v1, :cond_2

    .line 374225
    iget-object v1, p0, LX/28X;->a:LX/0Uo;

    invoke-static {v1}, LX/0Uo;->N(LX/0Uo;)V

    .line 374226
    :cond_2
    iget-object v1, p0, LX/28X;->a:LX/0Uo;

    monitor-enter v1

    .line 374227
    :try_start_0
    iget-object v2, p0, LX/28X;->a:LX/0Uo;

    iget-object v2, v2, LX/0Uo;->X:LX/01J;

    invoke-virtual {v2, v0}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 374228
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Peer process "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, LX/0cK;->c:LX/00G;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " disconnected from "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/28X;->a:LX/0Uo;

    invoke-static {v2}, LX/0Uo;->v(LX/0Uo;)LX/0cJ;

    move-result-object v2

    invoke-interface {v2}, LX/0cJ;->a()LX/0cK;

    move-result-object v2

    iget-object v2, v2, LX/0cK;->c:LX/00G;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 374229
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/0cK;LX/1gR;)V
    .locals 3

    .prologue
    .line 374230
    iget-object v1, p0, LX/28X;->a:LX/0Uo;

    monitor-enter v1

    .line 374231
    :try_start_0
    iget-object v0, p0, LX/28X;->a:LX/0Uo;

    iget-object v0, v0, LX/0Uo;->X:LX/01J;

    new-instance v2, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;

    invoke-direct {v2}, Lcom/facebook/common/appstate/AppStateManager$AppStateInfo;-><init>()V

    invoke-virtual {v0, p1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374232
    sget-object v0, LX/1gR;->Incoming:LX/1gR;

    if-ne p2, v0, :cond_0

    .line 374233
    iget-object v0, p0, LX/28X;->a:LX/0Uo;

    invoke-static {v0}, LX/0Uo;->v(LX/0Uo;)LX/0cJ;

    move-result-object v0

    iget-object v2, p0, LX/28X;->a:LX/0Uo;

    invoke-static {v2}, LX/0Uo;->x(LX/0Uo;)Landroid/os/Message;

    move-result-object v2

    invoke-interface {v0, p1, v2}, LX/0cJ;->a(LX/0cK;Landroid/os/Message;)V

    .line 374234
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Peer process "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, LX/0cK;->c:LX/00G;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " connected to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/28X;->a:LX/0Uo;

    invoke-static {v2}, LX/0Uo;->v(LX/0Uo;)LX/0cJ;

    move-result-object v2

    invoke-interface {v2}, LX/0cJ;->a()LX/0cK;

    move-result-object v2

    iget-object v2, v2, LX/0cK;->c:LX/00G;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 374235
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
