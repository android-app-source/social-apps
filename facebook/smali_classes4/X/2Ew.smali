.class public final LX/2Ew;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1gW;

.field public b:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "Lcom/facebook/analytics/HoneyAnalyticsEvent;",
            ">;"
        }
    .end annotation
.end field

.field public c:J

.field public final synthetic d:LX/2CZ;


# direct methods
.method public constructor <init>(LX/2CZ;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 386127
    iput-object p1, p0, LX/2Ew;->d:LX/2CZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386128
    iput-object v0, p0, LX/2Ew;->a:LX/1gW;

    .line 386129
    iput-object v0, p0, LX/2Ew;->b:LX/0Pz;

    .line 386130
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/2Ew;->c:J

    .line 386131
    return-void
.end method

.method public static b$redex0(LX/2Ew;)V
    .locals 1

    .prologue
    .line 386132
    iget-object v0, p0, LX/2Ew;->a:LX/1gW;

    if-eqz v0, :cond_0

    .line 386133
    iget-object v0, p0, LX/2Ew;->a:LX/1gW;

    invoke-interface {v0}, LX/1gW;->b()Z

    .line 386134
    :cond_0
    return-void
.end method

.method public static c$redex0(LX/2Ew;)J
    .locals 4

    .prologue
    .line 386135
    iget-wide v0, p0, LX/2Ew;->c:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-wide v0, p0, LX/2Ew;->c:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 386136
    iget-wide v0, p0, LX/2Ew;->c:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/2Ew;->c:J

    .line 386137
    return-void
.end method

.method public final a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    .locals 1

    .prologue
    .line 386138
    iget-object v0, p0, LX/2Ew;->b:LX/0Pz;

    if-nez v0, :cond_0

    .line 386139
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iput-object v0, p0, LX/2Ew;->b:LX/0Pz;

    .line 386140
    :cond_0
    iget-object v0, p0, LX/2Ew;->b:LX/0Pz;

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 386141
    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 386142
    iget-object v0, p0, LX/2Ew;->a:LX/1gW;

    if-nez v0, :cond_0

    .line 386143
    iget-object v0, p0, LX/2Ew;->d:LX/2CZ;

    invoke-virtual {v0}, LX/2CZ;->b()LX/0WS;

    move-result-object v0

    invoke-virtual {v0}, LX/0WS;->b()LX/1gW;

    move-result-object v0

    iput-object v0, p0, LX/2Ew;->a:LX/1gW;

    .line 386144
    :cond_0
    iget-object v0, p0, LX/2Ew;->a:LX/1gW;

    invoke-interface {v0, p1, p2, p3}, LX/1gW;->a(Ljava/lang/String;J)LX/1gW;

    .line 386145
    return-void
.end method
