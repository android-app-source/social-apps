.class public LX/23o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/1V7;

.field private final b:LX/0gw;

.field private final c:LX/23p;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1V7;LX/0gw;LX/23p;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1V7;",
            "LX/0gw;",
            "LX/23p;",
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365310
    iput-object p3, p0, LX/23o;->c:LX/23p;

    .line 365311
    iput-object p1, p0, LX/23o;->a:LX/1V7;

    .line 365312
    iput-object p2, p0, LX/23o;->b:LX/0gw;

    .line 365313
    iput-object p4, p0, LX/23o;->d:LX/0Ot;

    .line 365314
    iput-object p5, p0, LX/23o;->e:LX/0Ot;

    .line 365315
    return-void
.end method

.method public static a(LX/0QB;)LX/23o;
    .locals 9

    .prologue
    .line 365298
    const-class v1, LX/23o;

    monitor-enter v1

    .line 365299
    :try_start_0
    sget-object v0, LX/23o;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 365300
    sput-object v2, LX/23o;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 365301
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365302
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 365303
    new-instance v3, LX/23o;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-static {v0}, LX/0gw;->a(LX/0QB;)LX/0gw;

    move-result-object v5

    check-cast v5, LX/0gw;

    invoke-static {v0}, LX/23p;->b(LX/0QB;)LX/23p;

    move-result-object v6

    check-cast v6, LX/23p;

    const/16 v7, 0x132a

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x132e

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/23o;-><init>(LX/1V7;LX/0gw;LX/23p;LX/0Ot;LX/0Ot;)V

    .line 365304
    move-object v0, v3

    .line 365305
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 365306
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/23o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365307
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 365308
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static e(LX/23o;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 365297
    iget-object v0, p0, LX/23o;->c:LX/23p;

    invoke-virtual {v0, p1}, LX/23p;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/1X6;"
        }
    .end annotation

    .prologue
    .line 365258
    iget-object v0, p0, LX/23o;->b:LX/0gw;

    invoke-virtual {v0}, LX/0gw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365259
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 365260
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 365261
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 365262
    invoke-static {v0}, LX/2v7;->i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365263
    new-instance v0, LX/1X6;

    iget-object v2, p0, LX/23o;->a:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->k()LX/1Ua;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 365264
    :goto_0
    move-object v0, v0

    .line 365265
    :goto_1
    return-object v0

    .line 365266
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 365267
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 365268
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 365269
    invoke-static {v0}, LX/2v7;->i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, LX/2v7;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 365270
    :cond_1
    new-instance v0, LX/1X6;

    iget-object v2, p0, LX/23o;->a:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->k()LX/1Ua;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 365271
    :goto_2
    move-object v0, v0

    .line 365272
    goto :goto_1

    .line 365273
    :cond_2
    invoke-static {p0, p1}, LX/23o;->e(LX/23o;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365274
    new-instance v0, LX/1X6;

    sget-object v2, LX/1Ua;->b:LX/1Ua;

    invoke-direct {v0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    goto :goto_0

    .line 365275
    :cond_3
    new-instance v0, LX/1X6;

    sget-object v2, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    goto :goto_0

    .line 365276
    :cond_4
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 365277
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 365278
    iget-object v0, p0, LX/23o;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->y()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 365279
    iget-object v0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 365280
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 365281
    iget-object v0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 365282
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    .line 365283
    :goto_3
    move v0, v0

    .line 365284
    if-eqz v0, :cond_5

    .line 365285
    new-instance v0, LX/1X6;

    iget-object v2, p0, LX/23o;->a:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->l()LX/1Ua;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    goto :goto_2

    .line 365286
    :cond_5
    invoke-static {p0, p1}, LX/23o;->e(LX/23o;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 365287
    new-instance v0, LX/1X6;

    sget-object v2, LX/1Ua;->f:LX/1Ua;

    invoke-direct {v0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    goto :goto_2

    .line 365288
    :cond_6
    new-instance v0, LX/1X6;

    sget-object v2, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    goto :goto_2

    :cond_7
    move v0, v3

    .line 365289
    goto :goto_3

    .line 365290
    :cond_8
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 365291
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    .line 365292
    iget-object v0, p0, LX/23o;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    if-eqz v5, :cond_9

    invoke-static {v4}, LX/14w;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/23o;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19w;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/19w;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {v5}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 365293
    iget-object v0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 365294
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 365295
    iget-object v0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 365296
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v2

    goto :goto_3

    :cond_9
    move v0, v3

    goto :goto_3
.end method
