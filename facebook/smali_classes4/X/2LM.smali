.class public LX/2LM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2LM;


# instance fields
.field public final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2LP;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0TD;


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/0TD;)V
    .locals 1
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/2LP;",
            ">;",
            "LX/0TD;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394862
    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/2LM;->a:LX/0Rf;

    .line 394863
    iput-object p2, p0, LX/2LM;->b:LX/0TD;

    .line 394864
    return-void
.end method

.method public static a(LX/0QB;)LX/2LM;
    .locals 6

    .prologue
    .line 394865
    sget-object v0, LX/2LM;->c:LX/2LM;

    if-nez v0, :cond_1

    .line 394866
    const-class v1, LX/2LM;

    monitor-enter v1

    .line 394867
    :try_start_0
    sget-object v0, LX/2LM;->c:LX/2LM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394868
    if-eqz v2, :cond_0

    .line 394869
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 394870
    new-instance v4, LX/2LM;

    .line 394871
    new-instance v3, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/2LN;

    invoke-direct {p0, v0}, LX/2LN;-><init>(LX/0QB;)V

    invoke-direct {v3, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v5, v3

    .line 394872
    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-direct {v4, v5, v3}, LX/2LM;-><init>(Ljava/util/Set;LX/0TD;)V

    .line 394873
    move-object v0, v4

    .line 394874
    sput-object v0, LX/2LM;->c:LX/2LM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394875
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394876
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394877
    :cond_1
    sget-object v0, LX/2LM;->c:LX/2LM;

    return-object v0

    .line 394878
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394879
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .prologue
    .line 394880
    new-instance v0, LX/2Lb;

    invoke-direct {v0, p0, p1}, LX/2Lb;-><init>(LX/2LM;I)V

    .line 394881
    move-object v0, v0

    .line 394882
    iget-object v1, p0, LX/2LM;->b:LX/0TD;

    invoke-interface {v1, v0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
