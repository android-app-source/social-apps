.class public final LX/2AV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:LX/2Aa;

.field public i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 377354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377355
    sget-object v0, LX/2Aa;->API_EC_DOMAIN:LX/2Aa;

    iput-object v0, p0, LX/2AV;->h:LX/2Aa;

    return-void
.end method


# virtual methods
.method public final a(LX/2Aa;)LX/2AV;
    .locals 2

    .prologue
    .line 377356
    if-nez p1, :cond_0

    .line 377357
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "errorDomain cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 377358
    :cond_0
    iput-object p1, p0, LX/2AV;->h:LX/2Aa;

    .line 377359
    return-object p0
.end method

.method public final a()Lcom/facebook/http/protocol/ApiErrorResult;
    .locals 10

    .prologue
    .line 377353
    new-instance v0, Lcom/facebook/http/protocol/ApiErrorResult;

    iget v1, p0, LX/2AV;->a:I

    iget v2, p0, LX/2AV;->b:I

    iget-object v3, p0, LX/2AV;->c:Ljava/lang/String;

    iget-object v4, p0, LX/2AV;->d:Ljava/lang/String;

    iget-object v5, p0, LX/2AV;->e:Ljava/lang/String;

    iget-object v6, p0, LX/2AV;->f:Ljava/lang/String;

    iget-object v7, p0, LX/2AV;->h:LX/2Aa;

    iget-object v8, p0, LX/2AV;->g:Ljava/lang/String;

    iget-boolean v9, p0, LX/2AV;->i:Z

    invoke-direct/range {v0 .. v9}, Lcom/facebook/http/protocol/ApiErrorResult;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2Aa;Ljava/lang/String;Z)V

    return-object v0
.end method
