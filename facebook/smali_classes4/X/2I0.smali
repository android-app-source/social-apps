.class public LX/2I0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2AX;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2I0;


# instance fields
.field private final a:Z

.field private final b:LX/1wV;


# direct methods
.method public constructor <init>(LX/0Uh;LX/1wV;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391394
    const/16 v0, 0x5a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2I0;->a:Z

    .line 391395
    iput-object p2, p0, LX/2I0;->b:LX/1wV;

    .line 391396
    return-void
.end method

.method public static a(LX/0QB;)LX/2I0;
    .locals 5

    .prologue
    .line 391370
    sget-object v0, LX/2I0;->c:LX/2I0;

    if-nez v0, :cond_1

    .line 391371
    const-class v1, LX/2I0;

    monitor-enter v1

    .line 391372
    :try_start_0
    sget-object v0, LX/2I0;->c:LX/2I0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391373
    if-eqz v2, :cond_0

    .line 391374
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391375
    new-instance p0, LX/2I0;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/1wV;->b(LX/0QB;)LX/1wV;

    move-result-object v4

    check-cast v4, LX/1wV;

    invoke-direct {p0, v3, v4}, LX/2I0;-><init>(LX/0Uh;LX/1wV;)V

    .line 391376
    move-object v0, p0

    .line 391377
    sput-object v0, LX/2I0;->c:LX/2I0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391378
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391379
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391380
    :cond_1
    sget-object v0, LX/2I0;->c:LX/2I0;

    return-object v0

    .line 391381
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391382
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()LX/0P1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 391383
    iget-boolean v0, p0, LX/2I0;->a:Z

    if-eqz v0, :cond_0

    sget-object v1, LX/2C3;->APP_USE:LX/2C3;

    .line 391384
    :goto_0
    iget-object v0, p0, LX/2I0;->b:LX/1wV;

    .line 391385
    iget-object v2, v0, LX/1wV;->a:LX/0ad;

    sget-short v3, LX/15r;->h:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 391386
    if-eqz v0, :cond_1

    .line 391387
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 391388
    :goto_1
    return-object v0

    .line 391389
    :cond_0
    sget-object v1, LX/2C3;->ALWAYS:LX/2C3;

    goto :goto_0

    .line 391390
    :cond_1
    iget-object v0, p0, LX/2I0;->b:LX/1wV;

    invoke-virtual {v0}, LX/1wV;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 391391
    new-instance v0, LX/1se;

    const-string v2, "/notifications_sync"

    invoke-direct {v0, v2, v6}, LX/1se;-><init>(Ljava/lang/String;I)V

    new-instance v2, LX/1se;

    const-string v3, "/notifications_seen"

    invoke-direct {v2, v3, v6}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v3, LX/2C3;->APP_USE:LX/2C3;

    new-instance v4, LX/1se;

    const-string v5, "/notifications_read"

    invoke-direct {v4, v5, v6}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v5, LX/2C3;->APP_USE:LX/2C3;

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    goto :goto_1

    .line 391392
    :cond_2
    new-instance v0, LX/1se;

    const-string v2, "/notifications_sync"

    invoke-direct {v0, v2, v6}, LX/1se;-><init>(Ljava/lang/String;I)V

    new-instance v2, LX/1se;

    const-string v3, "/notifications_seen"

    invoke-direct {v2, v3, v6}, LX/1se;-><init>(Ljava/lang/String;I)V

    new-instance v4, LX/1se;

    const-string v3, "/notifications_read"

    invoke-direct {v4, v3, v6}, LX/1se;-><init>(Ljava/lang/String;I)V

    move-object v3, v1

    move-object v5, v1

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    goto :goto_1
.end method
