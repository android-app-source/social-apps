.class public final LX/311;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/312;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 486633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486634
    array-length v0, p2

    invoke-static {v0}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/311;->a:Ljava/util/List;

    move v0, v1

    .line 486635
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_1

    .line 486636
    const-string v2, "%%%d$s"

    add-int/lit8 v3, v0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 486637
    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 486638
    :goto_1
    if-ltz v2, :cond_0

    .line 486639
    new-instance v4, LX/312;

    aget-object v5, p2, v0

    invoke-direct {v4, p0, v2, v3, v5}, LX/312;-><init>(LX/311;ILjava/lang/String;Ljava/lang/String;)V

    .line 486640
    iput v0, v4, LX/312;->c:I

    .line 486641
    iget-object v5, p0, LX/311;->a:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486642
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    goto :goto_1

    .line 486643
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 486644
    :cond_1
    iget-object v0, p0, LX/311;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 486645
    iget-object v0, p0, LX/311;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/312;

    .line 486646
    iget v3, v0, LX/312;->b:I

    move v3, v3

    .line 486647
    add-int/2addr v3, v1

    .line 486648
    iput v3, v0, LX/312;->b:I

    .line 486649
    iget v3, v0, LX/312;->e:I

    move v3, v3

    .line 486650
    iget v4, v0, LX/312;->d:I

    move v0, v4

    .line 486651
    sub-int v0, v3, v0

    add-int/2addr v1, v0

    .line 486652
    goto :goto_2

    .line 486653
    :cond_2
    return-void
.end method
