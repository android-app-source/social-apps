.class public LX/2P6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2P6;


# instance fields
.field private final a:LX/2P7;

.field private final b:LX/2N8;


# direct methods
.method public constructor <init>(LX/2P7;LX/2N8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 405517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 405518
    iput-object p1, p0, LX/2P6;->a:LX/2P7;

    .line 405519
    iput-object p2, p0, LX/2P6;->b:LX/2N8;

    .line 405520
    return-void
.end method

.method public static a(LX/0QB;)LX/2P6;
    .locals 5

    .prologue
    .line 405521
    sget-object v0, LX/2P6;->c:LX/2P6;

    if-nez v0, :cond_1

    .line 405522
    const-class v1, LX/2P6;

    monitor-enter v1

    .line 405523
    :try_start_0
    sget-object v0, LX/2P6;->c:LX/2P6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 405524
    if-eqz v2, :cond_0

    .line 405525
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 405526
    new-instance p0, LX/2P6;

    invoke-static {v0}, LX/2P7;->b(LX/0QB;)LX/2P7;

    move-result-object v3

    check-cast v3, LX/2P7;

    invoke-static {v0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v4

    check-cast v4, LX/2N8;

    invoke-direct {p0, v3, v4}, LX/2P6;-><init>(LX/2P7;LX/2N8;)V

    .line 405527
    move-object v0, p0

    .line 405528
    sput-object v0, LX/2P6;->c:LX/2P6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405529
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 405530
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 405531
    :cond_1
    sget-object v0, LX/2P6;->c:LX/2P6;

    return-object v0

    .line 405532
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 405533
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/database/Cursor;)Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .locals 8

    .prologue
    .line 405534
    sget-object v0, LX/2P5;->b:LX/0U1;

    invoke-virtual {v0, p0}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 405535
    sget-object v1, LX/2P2;->d:LX/0U1;

    invoke-virtual {v1, p0}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 405536
    sget-object v2, LX/2P5;->o:LX/0U1;

    invoke-virtual {v2, p0}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v2

    .line 405537
    sget-object v4, LX/2P5;->n:LX/0U1;

    invoke-virtual {v4, p0}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v4

    .line 405538
    new-instance v6, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-direct {v6, v0, v1}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 405539
    new-instance v0, LX/6fz;

    invoke-direct {v0}, LX/6fz;-><init>()V

    .line 405540
    iput-object v6, v0, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 405541
    move-object v0, v0

    .line 405542
    iput-wide v2, v0, LX/6fz;->e:J

    .line 405543
    move-object v0, v0

    .line 405544
    iput-wide v4, v0, LX/6fz;->b:J

    .line 405545
    move-object v0, v0

    .line 405546
    invoke-virtual {v0}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2P6;Landroid/database/Cursor;Ljava/lang/String;)Lcom/facebook/user/model/User;
    .locals 7

    .prologue
    .line 405547
    sget-object v0, LX/2P2;->b:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 405548
    sget-object v1, LX/2P2;->c:LX/0U1;

    invoke-virtual {v1, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 405549
    sget-object v2, LX/2P2;->d:LX/0U1;

    invoke-virtual {v2, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 405550
    sget-object v3, LX/2P2;->e:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 405551
    new-instance v4, Lcom/facebook/user/model/Name;

    invoke-direct {v4, v0, v1, v2}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 405552
    new-instance v5, LX/0XI;

    invoke-direct {v5}, LX/0XI;-><init>()V

    .line 405553
    const/4 v6, 0x1

    .line 405554
    iput-boolean v6, v5, LX/0XI;->z:Z

    .line 405555
    sget-object v6, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v5, v6, p2}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 405556
    iput-object v0, v5, LX/0XI;->i:Ljava/lang/String;

    .line 405557
    iput-object v1, v5, LX/0XI;->j:Ljava/lang/String;

    .line 405558
    iput-object v2, v5, LX/0XI;->h:Ljava/lang/String;

    .line 405559
    iput-object v4, v5, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 405560
    if-eqz v3, :cond_0

    .line 405561
    iget-object v0, p0, LX/2P6;->b:LX/2N8;

    invoke-virtual {v0, v3}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 405562
    invoke-static {v0}, LX/2P7;->a(LX/0lF;)Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    .line 405563
    iput-object v0, v5, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 405564
    :cond_0
    invoke-virtual {v5}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/database/Cursor;)Lcom/facebook/user/model/User;
    .locals 1

    .prologue
    .line 405565
    sget-object v0, LX/2P5;->b:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/2P6;->a(LX/2P6;Landroid/database/Cursor;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0
.end method
