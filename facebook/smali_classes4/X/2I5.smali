.class public LX/2I5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2AX;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2I5;


# instance fields
.field private final a:LX/2AZ;


# direct methods
.method public constructor <init>(LX/2AZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391442
    iput-object p1, p0, LX/2I5;->a:LX/2AZ;

    .line 391443
    return-void
.end method

.method public static a(LX/0QB;)LX/2I5;
    .locals 4

    .prologue
    .line 391444
    sget-object v0, LX/2I5;->b:LX/2I5;

    if-nez v0, :cond_1

    .line 391445
    const-class v1, LX/2I5;

    monitor-enter v1

    .line 391446
    :try_start_0
    sget-object v0, LX/2I5;->b:LX/2I5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391447
    if-eqz v2, :cond_0

    .line 391448
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 391449
    new-instance p0, LX/2I5;

    invoke-static {v0}, LX/2AZ;->a(LX/0QB;)LX/2AZ;

    move-result-object v3

    check-cast v3, LX/2AZ;

    invoke-direct {p0, v3}, LX/2I5;-><init>(LX/2AZ;)V

    .line 391450
    move-object v0, p0

    .line 391451
    sput-object v0, LX/2I5;->b:LX/2I5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391452
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391453
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391454
    :cond_1
    sget-object v0, LX/2I5;->b:LX/2I5;

    return-object v0

    .line 391455
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391456
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391457
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 391458
    iget-object v0, p0, LX/2I5;->a:LX/2AZ;

    .line 391459
    iget-object v2, v0, LX/2AZ;->a:Ljava/util/Set;

    invoke-static {v2}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v2

    move-object v0, v2

    .line 391460
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ol;

    .line 391461
    iget-boolean v3, v0, LX/3Ol;->c:Z

    move v3, v3

    .line 391462
    if-eqz v3, :cond_0

    .line 391463
    iget-object v3, v0, LX/3Ol;->a:LX/1se;

    move-object v3, v3

    .line 391464
    iget-object p0, v0, LX/3Ol;->b:LX/2C3;

    move-object v0, p0

    .line 391465
    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 391466
    :cond_1
    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method
