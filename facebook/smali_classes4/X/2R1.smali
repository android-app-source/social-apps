.class public LX/2R1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2R1;


# instance fields
.field private final a:LX/2CH;


# direct methods
.method public constructor <init>(LX/2CH;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409398
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CH;

    iput-object v0, p0, LX/2R1;->a:LX/2CH;

    .line 409399
    return-void
.end method

.method public static a(LX/0QB;)LX/2R1;
    .locals 4

    .prologue
    .line 409400
    sget-object v0, LX/2R1;->b:LX/2R1;

    if-nez v0, :cond_1

    .line 409401
    const-class v1, LX/2R1;

    monitor-enter v1

    .line 409402
    :try_start_0
    sget-object v0, LX/2R1;->b:LX/2R1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 409403
    if-eqz v2, :cond_0

    .line 409404
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 409405
    new-instance p0, LX/2R1;

    invoke-static {v0}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v3

    check-cast v3, LX/2CH;

    invoke-direct {p0, v3}, LX/2R1;-><init>(LX/2CH;)V

    .line 409406
    move-object v0, p0

    .line 409407
    sput-object v0, LX/2R1;->b:LX/2R1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409408
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 409409
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 409410
    :cond_1
    sget-object v0, LX/2R1;->b:LX/2R1;

    return-object v0

    .line 409411
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 409412
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 1

    .prologue
    .line 409413
    iget-object v0, p0, LX/2R1;->a:LX/2CH;

    invoke-virtual {v0}, LX/2CH;->c()V

    .line 409414
    return-void
.end method
