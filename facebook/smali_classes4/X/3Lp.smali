.class public LX/3Lp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static volatile h:LX/3Lp;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Hv;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/3Lq;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/3Lq;

.field public g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 552198
    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Lp;->b:Ljava/lang/String;

    .line 552199
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Lp;->c:Ljava/lang/String;

    .line 552200
    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Lp;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7Hv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 552189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 552190
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/3Lp;->e:Ljava/util/HashMap;

    .line 552191
    new-instance v0, LX/3Lq;

    invoke-direct {v0, p0}, LX/3Lq;-><init>(LX/3Lp;)V

    iput-object v0, p0, LX/3Lp;->f:LX/3Lq;

    .line 552192
    iput-object p1, p0, LX/3Lp;->a:LX/0Ot;

    .line 552193
    const/4 v0, 0x0

    .line 552194
    if-nez v0, :cond_0

    .line 552195
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, LX/3Lp;->g:Ljava/lang/String;

    .line 552196
    :goto_0
    return-void

    .line 552197
    :cond_0
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, LX/3Lp;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(LX/3Lp;I)I
    .locals 2

    .prologue
    .line 552186
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    sget-object v0, LX/3Lp;->c:Ljava/lang/String;

    iget-object v1, p0, LX/3Lp;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/3Lp;->d:Ljava/lang/String;

    iget-object v1, p0, LX/3Lp;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552187
    const/4 p1, 0x3

    .line 552188
    :cond_0
    return p1
.end method

.method public static a(LX/0QB;)LX/3Lp;
    .locals 4

    .prologue
    .line 552173
    sget-object v0, LX/3Lp;->h:LX/3Lp;

    if-nez v0, :cond_1

    .line 552174
    const-class v1, LX/3Lp;

    monitor-enter v1

    .line 552175
    :try_start_0
    sget-object v0, LX/3Lp;->h:LX/3Lp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 552176
    if-eqz v2, :cond_0

    .line 552177
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 552178
    new-instance v3, LX/3Lp;

    const/16 p0, 0x3786

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3Lp;-><init>(LX/0Ot;)V

    .line 552179
    move-object v0, v3

    .line 552180
    sput-object v0, LX/3Lp;->h:LX/3Lp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 552181
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 552182
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 552183
    :cond_1
    sget-object v0, LX/3Lp;->h:LX/3Lp;

    return-object v0

    .line 552184
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 552185
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized b(LX/3Lp;Ljava/lang/Integer;)LX/3Lq;
    .locals 3

    .prologue
    .line 552166
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Lp;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Lq;

    .line 552167
    if-nez v0, :cond_0

    .line 552168
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 552169
    new-instance v0, LX/7Hs;

    invoke-direct {v0, p0}, LX/7Hs;-><init>(LX/3Lp;)V

    .line 552170
    iget-object v1, p0, LX/3Lp;->e:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552171
    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, LX/3Lp;->f:LX/3Lq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-object v0

    .line 552172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
