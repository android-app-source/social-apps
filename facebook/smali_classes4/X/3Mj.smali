.class public LX/3Mj;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/3Mi;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 555192
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method private a()LX/3Mi;
    .locals 17

    .prologue
    .line 555191
    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SF;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/3Mk;->b(LX/0QB;)LX/3Mk;

    move-result-object v4

    check-cast v4, LX/3Mk;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->a(LX/0QB;)Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;

    invoke-static/range {p0 .. p0}, LX/3N9;->a(LX/0QB;)LX/3N9;

    move-result-object v6

    check-cast v6, LX/3N9;

    invoke-static/range {p0 .. p0}, LX/3NA;->a(LX/0QB;)LX/3NA;

    move-result-object v7

    check-cast v7, LX/3NA;

    invoke-static/range {p0 .. p0}, LX/3NE;->a(LX/0QB;)LX/3NE;

    move-result-object v8

    check-cast v8, LX/3NE;

    invoke-static/range {p0 .. p0}, LX/3NH;->a(LX/0QB;)LX/3NH;

    move-result-object v9

    check-cast v9, LX/3NH;

    invoke-static/range {p0 .. p0}, LX/3NI;->a(LX/0QB;)LX/3NI;

    move-result-object v10

    check-cast v10, LX/3NI;

    invoke-static/range {p0 .. p0}, LX/3NJ;->a(LX/0QB;)LX/3NJ;

    move-result-object v11

    check-cast v11, LX/3NJ;

    invoke-static/range {p0 .. p0}, LX/3NL;->a(LX/0QB;)LX/3NL;

    move-result-object v12

    check-cast v12, LX/3NL;

    invoke-static/range {p0 .. p0}, LX/3Km;->a(LX/0QB;)LX/3Km;

    move-result-object v13

    check-cast v13, LX/3Km;

    invoke-static/range {p0 .. p0}, LX/3NM;->a(LX/0QB;)LX/3NM;

    move-result-object v14

    check-cast v14, LX/3NM;

    invoke-static/range {p0 .. p0}, LX/3Lw;->a(LX/0QB;)LX/3Lw;

    move-result-object v15

    check-cast v15, LX/3Lw;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v16

    check-cast v16, LX/0ad;

    invoke-static/range {v0 .. v16}, LX/3LE;->a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;LX/3Mk;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/3N9;LX/3NA;LX/3NE;LX/3NH;LX/3NI;LX/3NJ;LX/3NL;LX/3Km;LX/3NM;LX/3Lw;LX/0ad;)LX/3Mi;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3Mi;
    .locals 17

    .prologue
    .line 555190
    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SF;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/3Mk;->b(LX/0QB;)LX/3Mk;

    move-result-object v4

    check-cast v4, LX/3Mk;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;->a(LX/0QB;)Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;

    invoke-static/range {p0 .. p0}, LX/3N9;->a(LX/0QB;)LX/3N9;

    move-result-object v6

    check-cast v6, LX/3N9;

    invoke-static/range {p0 .. p0}, LX/3NA;->a(LX/0QB;)LX/3NA;

    move-result-object v7

    check-cast v7, LX/3NA;

    invoke-static/range {p0 .. p0}, LX/3NE;->a(LX/0QB;)LX/3NE;

    move-result-object v8

    check-cast v8, LX/3NE;

    invoke-static/range {p0 .. p0}, LX/3NH;->a(LX/0QB;)LX/3NH;

    move-result-object v9

    check-cast v9, LX/3NH;

    invoke-static/range {p0 .. p0}, LX/3NI;->a(LX/0QB;)LX/3NI;

    move-result-object v10

    check-cast v10, LX/3NI;

    invoke-static/range {p0 .. p0}, LX/3NJ;->a(LX/0QB;)LX/3NJ;

    move-result-object v11

    check-cast v11, LX/3NJ;

    invoke-static/range {p0 .. p0}, LX/3NL;->a(LX/0QB;)LX/3NL;

    move-result-object v12

    check-cast v12, LX/3NL;

    invoke-static/range {p0 .. p0}, LX/3Km;->a(LX/0QB;)LX/3Km;

    move-result-object v13

    check-cast v13, LX/3Km;

    invoke-static/range {p0 .. p0}, LX/3NM;->a(LX/0QB;)LX/3NM;

    move-result-object v14

    check-cast v14, LX/3NM;

    invoke-static/range {p0 .. p0}, LX/3Lw;->a(LX/0QB;)LX/3Lw;

    move-result-object v15

    check-cast v15, LX/3Lw;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v16

    check-cast v16, LX/0ad;

    invoke-static/range {v0 .. v16}, LX/3LE;->a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;LX/3Mk;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/3N9;LX/3NA;LX/3NE;LX/3NH;LX/3NI;LX/3NJ;LX/3NL;LX/3Km;LX/3NM;LX/3Lw;LX/0ad;)LX/3Mi;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 555189
    invoke-direct {p0}, LX/3Mj;->a()LX/3Mi;

    move-result-object v0

    return-object v0
.end method
