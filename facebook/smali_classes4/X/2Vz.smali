.class public LX/2Vz;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1Ha;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1Ha;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 418495
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Ha;
    .locals 7

    .prologue
    .line 418496
    sget-object v0, LX/2Vz;->a:LX/1Ha;

    if-nez v0, :cond_1

    .line 418497
    const-class v1, LX/2Vz;

    monitor-enter v1

    .line 418498
    :try_start_0
    sget-object v0, LX/2Vz;->a:LX/1Ha;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 418499
    if-eqz v2, :cond_0

    .line 418500
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 418501
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1GP;->a(LX/0QB;)LX/1GP;

    move-result-object v4

    check-cast v4, LX/1GQ;

    invoke-static {v0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v5

    check-cast v5, LX/0pi;

    invoke-static {v0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v6

    check-cast v6, LX/0pq;

    invoke-static {v0}, LX/1Fs;->a(LX/0QB;)LX/1Fs;

    move-result-object p0

    check-cast p0, LX/1Ft;

    invoke-static {v3, v4, v5, v6, p0}, LX/2W0;->a(Landroid/content/Context;LX/1GQ;LX/0pi;LX/0pq;LX/1Ft;)LX/1Ha;

    move-result-object v3

    move-object v0, v3

    .line 418502
    sput-object v0, LX/2Vz;->a:LX/1Ha;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418503
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 418504
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 418505
    :cond_1
    sget-object v0, LX/2Vz;->a:LX/1Ha;

    return-object v0

    .line 418506
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 418507
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 418508
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1GP;->a(LX/0QB;)LX/1GP;

    move-result-object v1

    check-cast v1, LX/1GQ;

    invoke-static {p0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v2

    check-cast v2, LX/0pi;

    invoke-static {p0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v3

    check-cast v3, LX/0pq;

    invoke-static {p0}, LX/1Fs;->a(LX/0QB;)LX/1Fs;

    move-result-object v4

    check-cast v4, LX/1Ft;

    invoke-static {v0, v1, v2, v3, v4}, LX/2W0;->a(Landroid/content/Context;LX/1GQ;LX/0pi;LX/0pq;LX/1Ft;)LX/1Ha;

    move-result-object v0

    return-object v0
.end method
