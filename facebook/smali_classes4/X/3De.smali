.class public final LX/3De;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Da;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Da",
        "<TKey;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Lo;

.field private final b:LX/2WG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TKey;"
        }
    .end annotation
.end field

.field private final c:LX/3Da;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Da",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/3DY;


# direct methods
.method public constructor <init>(LX/1Lo;LX/2WG;LX/3Da;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;",
            "LX/3Da;",
            ")V"
        }
    .end annotation

    .prologue
    .line 535288
    iput-object p1, p0, LX/3De;->a:LX/1Lo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 535289
    iput-object p2, p0, LX/3De;->b:LX/2WG;

    .line 535290
    iput-object p3, p0, LX/3De;->c:LX/3Da;

    .line 535291
    new-instance v0, LX/3Df;

    invoke-direct {v0, p0, p1}, LX/3Df;-><init>(LX/3De;LX/1Lo;)V

    iput-object v0, p0, LX/3De;->d:LX/3DY;

    .line 535292
    return-void
.end method


# virtual methods
.method public final a(J)Ljava/io/OutputStream;
    .locals 3

    .prologue
    .line 535302
    iget-object v0, p0, LX/3De;->a:LX/1Lo;

    invoke-static {v0}, LX/1Lo;->c(LX/1Lo;)V

    .line 535303
    new-instance v0, LX/45i;

    iget-object v1, p0, LX/3De;->c:LX/3Da;

    invoke-interface {v1, p1, p2}, LX/3Da;->a(J)Ljava/io/OutputStream;

    move-result-object v1

    iget-object v2, p0, LX/3De;->d:LX/3DY;

    invoke-direct {v0, v1, v2}, LX/45i;-><init>(Ljava/io/OutputStream;LX/3DY;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 535299
    iget-object v1, p0, LX/3De;->b:LX/2WG;

    .line 535300
    invoke-static {v1}, LX/1Lo;->c(LX/2WG;)Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    .line 535301
    return-object v0
.end method

.method public final b()LX/3Dd;
    .locals 1

    .prologue
    .line 535298
    iget-object v0, p0, LX/3De;->c:LX/3Da;

    invoke-interface {v0}, LX/3Da;->b()LX/3Dd;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 535297
    iget-object v0, p0, LX/3De;->c:LX/3Da;

    invoke-interface {v0, p1, p2}, LX/3Da;->b(J)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 535296
    iget-object v0, p0, LX/3De;->c:LX/3Da;

    invoke-interface {v0}, LX/3Da;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 535295
    iget-object v0, p0, LX/3De;->c:LX/3Da;

    invoke-interface {v0}, LX/3Da;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2WF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535294
    iget-object v0, p0, LX/3De;->c:LX/3Da;

    invoke-interface {v0}, LX/3Da;->g()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 535293
    iget-object v0, p0, LX/3De;->b:LX/2WG;

    return-object v0
.end method
