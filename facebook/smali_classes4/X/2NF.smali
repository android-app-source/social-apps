.class public LX/2NF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2NF;


# instance fields
.field private final a:LX/2N8;


# direct methods
.method public constructor <init>(LX/2N8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 398910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398911
    iput-object p1, p0, LX/2NF;->a:LX/2N8;

    .line 398912
    return-void
.end method

.method public static a(LX/0QB;)LX/2NF;
    .locals 4

    .prologue
    .line 398913
    sget-object v0, LX/2NF;->b:LX/2NF;

    if-nez v0, :cond_1

    .line 398914
    const-class v1, LX/2NF;

    monitor-enter v1

    .line 398915
    :try_start_0
    sget-object v0, LX/2NF;->b:LX/2NF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 398916
    if-eqz v2, :cond_0

    .line 398917
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 398918
    new-instance p0, LX/2NF;

    invoke-static {v0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v3

    check-cast v3, LX/2N8;

    invoke-direct {p0, v3}, LX/2NF;-><init>(LX/2N8;)V

    .line 398919
    move-object v0, p0

    .line 398920
    sput-object v0, LX/2NF;->b:LX/2NF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398921
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 398922
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 398923
    :cond_1
    sget-object v0, LX/2NF;->b:LX/2NF;

    return-object v0

    .line 398924
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 398925
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 398926
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 398927
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 398928
    :cond_1
    iget-object v1, p0, LX/2NF;->a:LX/2N8;

    invoke-virtual {v1, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 398929
    const-string v2, "type"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 398930
    const-string v3, "sticker_id"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 398931
    const-string v4, "thumbnail_uri"

    invoke-virtual {v1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 398932
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 398933
    invoke-virtual {v2}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/6fx;->valueOf(Ljava/lang/String;)LX/6fx;

    move-result-object v2

    .line 398934
    sget-object v4, LX/6cl;->a:[I

    invoke-virtual {v2}, LX/6fx;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    .line 398935
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 398936
    :pswitch_1
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 398937
    invoke-virtual {v3}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    .line 398938
    invoke-static {v1}, LX/4m9;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 398939
    invoke-static {v1}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto :goto_0

    .line 398940
    :pswitch_2
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 398941
    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->b(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto :goto_0

    .line 398942
    :pswitch_3
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 398943
    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto :goto_0

    .line 398944
    :pswitch_4
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 398945
    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->c(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto :goto_0

    .line 398946
    :pswitch_5
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 398947
    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->d(Landroid/net/Uri;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
