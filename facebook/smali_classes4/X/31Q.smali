.class public final LX/31Q;
.super LX/1jx;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 487142
    invoke-direct {p0}, LX/1jx;-><init>()V

    .line 487143
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/31Q;->a:Ljava/util/Set;

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 487144
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 487145
    iget-object v0, p0, LX/31Q;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 487146
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)Z
    .locals 1

    .prologue
    .line 487147
    instance-of v0, p1, LX/16i;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 487148
    check-cast v0, LX/16i;

    invoke-interface {v0}, LX/16i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/31Q;->a(Ljava/lang/String;)V

    .line 487149
    :cond_0
    instance-of v0, p1, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 487150
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/31Q;->a(Ljava/lang/String;)V

    .line 487151
    :cond_1
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    .line 487152
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/31Q;->a(Ljava/lang/String;)V

    .line 487153
    :cond_2
    const/4 v0, 0x1

    return v0
.end method
