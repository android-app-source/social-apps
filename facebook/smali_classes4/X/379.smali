.class public final enum LX/379;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/379;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/379;

.field public static final enum CHANNEL:LX/379;

.field public static final enum COMMERCIAL_BREAK:LX/379;

.field public static final enum FEED:LX/379;

.field public static final enum INSTANT_ARTICLE:LX/379;

.field public static final enum MISC:LX/379;

.field public static final enum NOTIFICATION:LX/379;

.field public static final enum TIMELINE:LX/379;

.field public static final enum UNKNOWN:LX/379;

.field public static final enum VIDEO_HOME:LX/379;

.field public static final enum VIDEO_HOME_OCCLUSION:LX/379;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 500824
    new-instance v0, LX/379;

    const-string v1, "COMMERCIAL_BREAK"

    invoke-direct {v0, v1, v3}, LX/379;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/379;->COMMERCIAL_BREAK:LX/379;

    .line 500825
    new-instance v0, LX/379;

    const-string v1, "INSTANT_ARTICLE"

    invoke-direct {v0, v1, v4}, LX/379;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/379;->INSTANT_ARTICLE:LX/379;

    .line 500826
    new-instance v0, LX/379;

    const-string v1, "CHANNEL"

    invoke-direct {v0, v1, v5}, LX/379;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/379;->CHANNEL:LX/379;

    .line 500827
    new-instance v0, LX/379;

    const-string v1, "VIDEO_HOME"

    invoke-direct {v0, v1, v6}, LX/379;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/379;->VIDEO_HOME:LX/379;

    .line 500828
    new-instance v0, LX/379;

    const-string v1, "VIDEO_HOME_OCCLUSION"

    invoke-direct {v0, v1, v7}, LX/379;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/379;->VIDEO_HOME_OCCLUSION:LX/379;

    .line 500829
    new-instance v0, LX/379;

    const-string v1, "TIMELINE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/379;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/379;->TIMELINE:LX/379;

    .line 500830
    new-instance v0, LX/379;

    const-string v1, "MISC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/379;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/379;->MISC:LX/379;

    .line 500831
    new-instance v0, LX/379;

    const-string v1, "FEED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/379;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/379;->FEED:LX/379;

    .line 500832
    new-instance v0, LX/379;

    const-string v1, "NOTIFICATION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/379;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/379;->NOTIFICATION:LX/379;

    .line 500833
    new-instance v0, LX/379;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/379;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/379;->UNKNOWN:LX/379;

    .line 500834
    const/16 v0, 0xa

    new-array v0, v0, [LX/379;

    sget-object v1, LX/379;->COMMERCIAL_BREAK:LX/379;

    aput-object v1, v0, v3

    sget-object v1, LX/379;->INSTANT_ARTICLE:LX/379;

    aput-object v1, v0, v4

    sget-object v1, LX/379;->CHANNEL:LX/379;

    aput-object v1, v0, v5

    sget-object v1, LX/379;->VIDEO_HOME:LX/379;

    aput-object v1, v0, v6

    sget-object v1, LX/379;->VIDEO_HOME_OCCLUSION:LX/379;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/379;->TIMELINE:LX/379;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/379;->MISC:LX/379;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/379;->FEED:LX/379;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/379;->NOTIFICATION:LX/379;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/379;->UNKNOWN:LX/379;

    aput-object v2, v0, v1

    sput-object v0, LX/379;->$VALUES:[LX/379;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 500835
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/379;
    .locals 1

    .prologue
    .line 500836
    const-class v0, LX/379;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/379;

    return-object v0
.end method

.method public static values()[LX/379;
    .locals 1

    .prologue
    .line 500837
    sget-object v0, LX/379;->$VALUES:[LX/379;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/379;

    return-object v0
.end method
