.class public LX/2ac;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Ljava/lang/Boolean;",
        "Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/00H;

.field private final d:LX/2Oj;


# direct methods
.method public constructor <init>(LX/0SG;LX/00H;LX/0sO;LX/2Oj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 424682
    invoke-direct {p0, p3}, LX/0ro;-><init>(LX/0sO;)V

    .line 424683
    iput-object p1, p0, LX/2ac;->b:LX/0SG;

    .line 424684
    iput-object p2, p0, LX/2ac;->c:LX/00H;

    .line 424685
    iput-object p4, p0, LX/2ac;->d:LX/2Oj;

    .line 424686
    return-void
.end method

.method public static a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;
    .locals 3
    .param p0    # Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 424776
    if-eqz p0, :cond_0

    .line 424777
    new-instance v0, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    .line 424778
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2ac;
    .locals 5

    .prologue
    .line 424693
    new-instance v4, LX/2ac;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    const-class v1, LX/00H;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/00H;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v2

    check-cast v2, LX/0sO;

    invoke-static {p0}, LX/2Oj;->a(LX/0QB;)LX/2Oj;

    move-result-object v3

    check-cast v3, LX/2Oj;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2ac;-><init>(LX/0SG;LX/00H;LX/0sO;LX/2Oj;)V

    .line 424694
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 424695
    const-class v0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;

    .line 424696
    new-instance v4, LX/0XI;

    invoke-direct {v4}, LX/0XI;-><init>()V

    .line 424697
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v1

    const/4 v3, 0x0

    .line 424698
    if-nez v1, :cond_4

    .line 424699
    :cond_0
    :goto_0
    move-object v1, v3

    .line 424700
    iput-object v1, v4, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 424701
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->A()Ljava/lang/String;

    move-result-object v1

    .line 424702
    iput-object v1, v4, LX/0XI;->l:Ljava/lang/String;

    .line 424703
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v1

    .line 424704
    sget-object v2, LX/32X;->a:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGender;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 424705
    sget-object v2, LX/0XJ;->UNKNOWN:LX/0XJ;

    :goto_1
    move-object v1, v2

    .line 424706
    iput-object v1, v4, LX/0XI;->m:LX/0XJ;

    .line 424707
    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 424708
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->k()LX/0Px;

    move-result-object v1

    .line 424709
    new-instance v2, LX/32Y;

    invoke-direct {v2}, LX/32Y;-><init>()V

    invoke-static {v1, v2}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v2

    move-object v1, v2

    .line 424710
    iput-object v1, v4, LX/0XI;->c:Ljava/util/List;

    .line 424711
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->j()LX/0Px;

    move-result-object v1

    .line 424712
    new-instance v2, LX/2qp;

    invoke-direct {v2}, LX/2qp;-><init>()V

    invoke-static {v1, v2}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v2

    .line 424713
    new-instance v3, LX/334;

    invoke-direct {v3}, LX/334;-><init>()V

    invoke-static {v2, v3}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    move-object v1, v2

    .line 424714
    iput-object v1, v4, LX/0XI;->d:Ljava/util/List;

    .line 424715
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    .line 424716
    new-instance v2, Lcom/facebook/user/model/PicSquare;

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-static {v3}, LX/2ac;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-static {v5}, LX/2ac;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-static {v6}, LX/2ac;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v6

    invoke-direct {v2, v3, v5, v6}, Lcom/facebook/user/model/PicSquare;-><init>(Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;)V

    move-object v1, v2

    .line 424717
    iput-object v1, v4, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 424718
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/03R;->YES:LX/03R;

    .line 424719
    :goto_2
    iput-object v1, v4, LX/0XI;->u:LX/03R;

    .line 424720
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->s()Z

    move-result v1

    .line 424721
    iput-boolean v1, v4, LX/0XI;->w:Z

    .line 424722
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->r()Z

    move-result v1

    .line 424723
    iput-boolean v1, v4, LX/0XI;->M:Z

    .line 424724
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->p()Z

    move-result v1

    .line 424725
    iput-boolean v1, v4, LX/0XI;->N:Z

    .line 424726
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->v()Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 424727
    iput-object v1, v4, LX/0XI;->O:LX/03R;

    .line 424728
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->j()Z

    move-result v1

    .line 424729
    iput-boolean v1, v4, LX/0XI;->v:Z

    .line 424730
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->n()Z

    move-result v1

    .line 424731
    iput-boolean v1, v4, LX/0XI;->S:Z

    .line 424732
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->o()Z

    move-result v1

    .line 424733
    iput-boolean v1, v4, LX/0XI;->af:Z

    .line 424734
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->t()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$MessengerOnlyDeactivatedMatchedUserModel;

    move-result-object v1

    .line 424735
    if-nez v1, :cond_9

    .line 424736
    const/4 v2, 0x0

    .line 424737
    :goto_3
    move-object v1, v2

    .line 424738
    iput-object v1, v4, LX/0XI;->aj:Lcom/facebook/user/model/User;

    .line 424739
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->u()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->a()Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 424740
    :goto_4
    iput-wide v2, v4, LX/0XI;->X:J

    .line 424741
    invoke-virtual {v0}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLModels$GetLoggedInUserQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;

    move-result-object v0

    .line 424742
    if-eqz v0, :cond_1

    .line 424743
    sget-object v1, LX/32X;->b:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAudienceMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 424744
    :cond_1
    const/4 v1, 0x0

    :goto_5
    move-object v0, v1

    .line 424745
    iput-object v0, v4, LX/0XI;->ah:LX/0XN;

    .line 424746
    new-instance v0, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v4}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v2

    iget-object v3, p0, LX/2ac;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/facebook/auth/protocol/GetLoggedInUserGraphQLResult;-><init>(LX/0ta;Lcom/facebook/user/model/User;J)V

    return-object v0

    .line 424747
    :cond_2
    sget-object v1, LX/03R;->NO:LX/03R;

    goto/16 :goto_2

    .line 424748
    :cond_3
    const-wide/16 v2, 0x0

    goto :goto_4

    .line 424749
    :cond_4
    invoke-interface {v1}, LX/1k2;->m_()Ljava/lang/String;

    move-result-object v6

    .line 424750
    invoke-interface {v1}, LX/1k2;->a()LX/0Px;

    move-result-object v2

    .line 424751
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 424752
    if-eqz v6, :cond_0

    new-instance v3, Lcom/facebook/user/model/Name;

    invoke-direct {v3, v6}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 424753
    :cond_5
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v5, v3

    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2qo;

    .line 424754
    invoke-interface {v2}, LX/2qo;->n_()I

    move-result p1

    .line 424755
    invoke-interface {v2}, LX/2qo;->a()I

    move-result p2

    .line 424756
    invoke-interface {v2}, LX/2qo;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object p3

    .line 424757
    const/4 v2, 0x0

    invoke-virtual {v6, v2, p1}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v2

    .line 424758
    invoke-virtual {v6, v2, p2}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result p1

    .line 424759
    invoke-virtual {v6, v2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 424760
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->FIRST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-static {p3, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    move-object v5, v2

    .line 424761
    goto :goto_6

    .line 424762
    :cond_6
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->LAST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-static {p3, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    :goto_7
    move-object v3, v2

    .line 424763
    goto :goto_6

    .line 424764
    :cond_7
    new-instance v2, Lcom/facebook/user/model/Name;

    invoke-direct {v2, v5, v3, v6}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v2

    goto/16 :goto_0

    :cond_8
    move-object v2, v3

    goto :goto_7

    .line 424765
    :pswitch_0
    sget-object v2, LX/0XJ;->FEMALE:LX/0XJ;

    goto/16 :goto_1

    .line 424766
    :pswitch_1
    sget-object v2, LX/0XJ;->MALE:LX/0XJ;

    goto/16 :goto_1

    :cond_9
    new-instance v2, LX/0XI;

    invoke-direct {v2}, LX/0XI;-><init>()V

    sget-object v3, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$MessengerOnlyDeactivatedMatchedUserModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$MessengerOnlyDeactivatedMatchedUserModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 424767
    iput-object v3, v2, LX/0XI;->h:Ljava/lang/String;

    .line 424768
    move-object v2, v2

    .line 424769
    new-instance v3, Lcom/facebook/user/model/PicSquare;

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$MessengerOnlyDeactivatedMatchedUserModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-static {v5}, LX/2ac;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$MessengerOnlyDeactivatedMatchedUserModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-static {v6}, LX/2ac;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/auth/protocol/LoggedInUserQueryFragmentModels$LoggedInUserQueryFragmentModel$MessengerOnlyDeactivatedMatchedUserModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    invoke-static {v7}, LX/2ac;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v7

    invoke-direct {v3, v5, v6, v7}, Lcom/facebook/user/model/PicSquare;-><init>(Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;)V

    move-object v3, v3

    .line 424770
    iput-object v3, v2, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 424771
    move-object v2, v2

    .line 424772
    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v2

    goto/16 :goto_3

    .line 424773
    :pswitch_2
    sget-object v1, LX/0XN;->AUTO:LX/0XN;

    goto/16 :goto_5

    .line 424774
    :pswitch_3
    sget-object v1, LX/0XN;->MANUAL:LX/0XN;

    goto/16 :goto_5

    .line 424775
    :pswitch_4
    sget-object v1, LX/0XN;->UNSET:LX/0XN;

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 424692
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 424687
    check-cast p1, Ljava/lang/Boolean;

    .line 424688
    new-instance v0, LX/3A8;

    invoke-direct {v0}, LX/3A8;-><init>()V

    move-object v0, v0

    .line 424689
    const-string v1, "square_profile_pic_size_small"

    invoke-static {}, LX/0wB;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "square_profile_pic_size_big"

    invoke-static {}, LX/0wB;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "square_profile_pic_size_huge"

    invoke-static {}, LX/0wB;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "is_for_messenger"

    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    iget-object v3, p0, LX/2ac;->c:LX/00H;

    .line 424690
    iget-object p0, v3, LX/00H;->j:LX/01T;

    move-object v3, p0

    .line 424691
    invoke-virtual {v2, v3}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "is_for_delta_forced_fetch"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
