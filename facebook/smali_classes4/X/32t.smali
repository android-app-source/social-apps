.class public final LX/32t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Iterable",
        "<",
        "LX/32s;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final _buckets:[LX/32u;

.field private final _hashMask:I

.field private _nextBucketIndex:I

.field public final _size:I


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/32s;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 491424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491425
    const/4 v0, 0x0

    iput v0, p0, LX/32t;->_nextBucketIndex:I

    .line 491426
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    iput v0, p0, LX/32t;->_size:I

    .line 491427
    iget v0, p0, LX/32t;->_size:I

    invoke-static {v0}, LX/32t;->a(I)I

    move-result v0

    .line 491428
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, LX/32t;->_hashMask:I

    .line 491429
    new-array v1, v0, [LX/32u;

    .line 491430
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    .line 491431
    iget-object v3, v0, LX/32s;->_propName:Ljava/lang/String;

    move-object v3, v3

    .line 491432
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    iget v5, p0, LX/32t;->_hashMask:I

    and-int/2addr v4, v5

    .line 491433
    new-instance v5, LX/32u;

    aget-object v6, v1, v4

    iget v7, p0, LX/32t;->_nextBucketIndex:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, LX/32t;->_nextBucketIndex:I

    invoke-direct {v5, v6, v3, v0, v7}, LX/32u;-><init>(LX/32u;Ljava/lang/String;LX/32s;I)V

    aput-object v5, v1, v4

    goto :goto_0

    .line 491434
    :cond_0
    iput-object v1, p0, LX/32t;->_buckets:[LX/32u;

    .line 491435
    return-void
.end method

.method private constructor <init>([LX/32u;II)V
    .locals 1

    .prologue
    .line 491417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491418
    const/4 v0, 0x0

    iput v0, p0, LX/32t;->_nextBucketIndex:I

    .line 491419
    iput-object p1, p0, LX/32t;->_buckets:[LX/32u;

    .line 491420
    iput p2, p0, LX/32t;->_size:I

    .line 491421
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/32t;->_hashMask:I

    .line 491422
    iput p3, p0, LX/32t;->_nextBucketIndex:I

    .line 491423
    return-void
.end method

.method private static final a(I)I
    .locals 2

    .prologue
    .line 491411
    const/16 v0, 0x20

    if-gt p0, v0, :cond_0

    add-int v0, p0, p0

    .line 491412
    :goto_0
    const/4 v1, 0x2

    .line 491413
    :goto_1
    if-ge v1, v0, :cond_1

    .line 491414
    add-int/2addr v1, v1

    goto :goto_1

    .line 491415
    :cond_0
    shr-int/lit8 v0, p0, 0x2

    add-int/2addr v0, p0

    goto :goto_0

    .line 491416
    :cond_1
    return v1
.end method

.method private a(Ljava/lang/String;I)LX/32s;
    .locals 2

    .prologue
    .line 491404
    iget-object v0, p0, LX/32t;->_buckets:[LX/32u;

    aget-object v0, v0, p2

    .line 491405
    :goto_0
    if-eqz v0, :cond_1

    .line 491406
    iget-object v1, v0, LX/32u;->key:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 491407
    iget-object v0, v0, LX/32u;->value:LX/32s;

    .line 491408
    :goto_1
    return-object v0

    .line 491409
    :cond_0
    iget-object v0, v0, LX/32u;->next:LX/32u;

    goto :goto_0

    .line 491410
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/32s;
    .locals 3

    .prologue
    .line 491393
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget v1, p0, LX/32t;->_hashMask:I

    and-int/2addr v1, v0

    .line 491394
    iget-object v0, p0, LX/32t;->_buckets:[LX/32u;

    aget-object v0, v0, v1

    .line 491395
    if-nez v0, :cond_0

    .line 491396
    const/4 v0, 0x0

    .line 491397
    :goto_0
    return-object v0

    .line 491398
    :cond_0
    iget-object v2, v0, LX/32u;->key:Ljava/lang/String;

    if-ne v2, p1, :cond_1

    .line 491399
    iget-object v0, v0, LX/32u;->value:LX/32s;

    goto :goto_0

    .line 491400
    :cond_1
    iget-object v0, v0, LX/32u;->next:LX/32u;

    if-eqz v0, :cond_2

    .line 491401
    iget-object v2, v0, LX/32u;->key:Ljava/lang/String;

    if-ne v2, p1, :cond_1

    .line 491402
    iget-object v0, v0, LX/32u;->value:LX/32s;

    goto :goto_0

    .line 491403
    :cond_2
    invoke-direct {p0, p1, v1}, LX/32t;->a(Ljava/lang/String;I)LX/32s;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()LX/32t;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 491387
    iget-object v4, p0, LX/32t;->_buckets:[LX/32u;

    array-length v5, v4

    move v3, v0

    move v1, v0

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v0, v4, v3

    .line 491388
    :goto_1
    if-eqz v0, :cond_0

    .line 491389
    iget-object v6, v0, LX/32u;->value:LX/32s;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v6, v1}, LX/32s;->a(I)V

    .line 491390
    iget-object v0, v0, LX/32u;->next:LX/32u;

    move v1, v2

    goto :goto_1

    .line 491391
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 491392
    :cond_1
    return-object p0
.end method

.method public final a(LX/32s;)LX/32t;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 491436
    iget-object v0, p0, LX/32t;->_buckets:[LX/32u;

    array-length v1, v0

    .line 491437
    new-array v2, v1, [LX/32u;

    .line 491438
    iget-object v0, p0, LX/32t;->_buckets:[LX/32u;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 491439
    iget-object v0, p1, LX/32s;->_propName:Ljava/lang/String;

    move-object v0, v0

    .line 491440
    iget-object v3, p1, LX/32s;->_propName:Ljava/lang/String;

    move-object v3, v3

    .line 491441
    invoke-virtual {p0, v3}, LX/32t;->a(Ljava/lang/String;)LX/32s;

    move-result-object v3

    .line 491442
    if-nez v3, :cond_0

    .line 491443
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    iget v3, p0, LX/32t;->_hashMask:I

    and-int/2addr v1, v3

    .line 491444
    new-instance v3, LX/32u;

    aget-object v4, v2, v1

    iget v5, p0, LX/32t;->_nextBucketIndex:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/32t;->_nextBucketIndex:I

    invoke-direct {v3, v4, v0, p1, v5}, LX/32u;-><init>(LX/32u;Ljava/lang/String;LX/32s;I)V

    aput-object v3, v2, v1

    .line 491445
    new-instance v0, LX/32t;

    iget v1, p0, LX/32t;->_size:I

    add-int/lit8 v1, v1, 0x1

    iget v3, p0, LX/32t;->_nextBucketIndex:I

    invoke-direct {v0, v2, v1, v3}, LX/32t;-><init>([LX/32u;II)V

    .line 491446
    :goto_0
    return-object v0

    .line 491447
    :cond_0
    new-instance v0, LX/32t;

    iget v3, p0, LX/32t;->_nextBucketIndex:I

    invoke-direct {v0, v2, v1, v3}, LX/32t;-><init>([LX/32u;II)V

    .line 491448
    invoke-virtual {v0, p1}, LX/32t;->b(LX/32s;)V

    goto :goto_0
.end method

.method public final a(LX/4ro;)LX/32t;
    .locals 5

    .prologue
    .line 491371
    if-eqz p1, :cond_0

    sget-object v0, LX/4ro;->a:LX/4ro;

    if-ne p1, v0, :cond_1

    .line 491372
    :cond_0
    :goto_0
    return-object p0

    .line 491373
    :cond_1
    invoke-virtual {p0}, LX/32t;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 491374
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 491375
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 491376
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    .line 491377
    iget-object v3, v0, LX/32s;->_propName:Ljava/lang/String;

    move-object v3, v3

    .line 491378
    invoke-virtual {p1, v3}, LX/4ro;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 491379
    invoke-virtual {v0, v3}, LX/32s;->a(Ljava/lang/String;)LX/32s;

    move-result-object v0

    .line 491380
    invoke-virtual {v0}, LX/32s;->l()Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v3

    .line 491381
    if-eqz v3, :cond_2

    .line 491382
    invoke-virtual {v3, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->unwrappingDeserializer(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v4

    .line 491383
    if-eq v4, v3, :cond_2

    .line 491384
    invoke-virtual {v0, v4}, LX/32s;->b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;

    move-result-object v0

    .line 491385
    :cond_2
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 491386
    :cond_3
    new-instance p0, LX/32t;

    invoke-direct {p0, v2}, LX/32t;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public final b(LX/32s;)V
    .locals 9

    .prologue
    .line 491358
    iget-object v0, p1, LX/32s;->_propName:Ljava/lang/String;

    move-object v4, v0

    .line 491359
    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, LX/32t;->_buckets:[LX/32u;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    and-int v5, v0, v1

    .line 491360
    const/4 v2, 0x0

    .line 491361
    const/4 v1, -0x1

    .line 491362
    iget-object v0, p0, LX/32t;->_buckets:[LX/32u;

    aget-object v0, v0, v5

    move-object v3, v0

    move v0, v1

    move-object v1, v2

    :goto_0
    if-eqz v3, :cond_1

    .line 491363
    if-gez v0, :cond_0

    iget-object v2, v3, LX/32u;->key:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 491364
    iget v0, v3, LX/32u;->index:I

    .line 491365
    :goto_1
    iget-object v2, v3, LX/32u;->next:LX/32u;

    move-object v3, v2

    goto :goto_0

    .line 491366
    :cond_0
    new-instance v2, LX/32u;

    iget-object v6, v3, LX/32u;->key:Ljava/lang/String;

    iget-object v7, v3, LX/32u;->value:LX/32s;

    iget v8, v3, LX/32u;->index:I

    invoke-direct {v2, v1, v6, v7, v8}, LX/32u;-><init>(LX/32u;Ljava/lang/String;LX/32s;I)V

    move-object v1, v2

    goto :goto_1

    .line 491367
    :cond_1
    if-gez v0, :cond_2

    .line 491368
    new-instance v0, Ljava/util/NoSuchElementException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No entry \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' found, can\'t replace"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 491369
    :cond_2
    iget-object v2, p0, LX/32t;->_buckets:[LX/32u;

    new-instance v3, LX/32u;

    invoke-direct {v3, v1, v4, p1, v0}, LX/32u;-><init>(LX/32u;Ljava/lang/String;LX/32s;I)V

    aput-object v3, v2, v5

    .line 491370
    return-void
.end method

.method public final b()[LX/32s;
    .locals 7

    .prologue
    .line 491350
    iget v0, p0, LX/32t;->_nextBucketIndex:I

    .line 491351
    new-array v2, v0, [LX/32s;

    .line 491352
    iget-object v3, p0, LX/32t;->_buckets:[LX/32u;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 491353
    :goto_1
    if-eqz v0, :cond_0

    .line 491354
    iget v5, v0, LX/32u;->index:I

    iget-object v6, v0, LX/32u;->value:LX/32s;

    aput-object v6, v2, v5

    .line 491355
    iget-object v0, v0, LX/32u;->next:LX/32u;

    goto :goto_1

    .line 491356
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 491357
    :cond_1
    return-object v2
.end method

.method public final c(LX/32s;)V
    .locals 9

    .prologue
    .line 491337
    iget-object v0, p1, LX/32s;->_propName:Ljava/lang/String;

    move-object v4, v0

    .line 491338
    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, LX/32t;->_buckets:[LX/32u;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    and-int v5, v0, v1

    .line 491339
    const/4 v2, 0x0

    .line 491340
    const/4 v1, 0x0

    .line 491341
    iget-object v0, p0, LX/32t;->_buckets:[LX/32u;

    aget-object v0, v0, v5

    move-object v3, v0

    move v0, v1

    move-object v1, v2

    :goto_0
    if-eqz v3, :cond_1

    .line 491342
    if-nez v0, :cond_0

    iget-object v2, v3, LX/32u;->key:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 491343
    const/4 v0, 0x1

    .line 491344
    :goto_1
    iget-object v2, v3, LX/32u;->next:LX/32u;

    move-object v3, v2

    goto :goto_0

    .line 491345
    :cond_0
    new-instance v2, LX/32u;

    iget-object v6, v3, LX/32u;->key:Ljava/lang/String;

    iget-object v7, v3, LX/32u;->value:LX/32s;

    iget v8, v3, LX/32u;->index:I

    invoke-direct {v2, v1, v6, v7, v8}, LX/32u;-><init>(LX/32u;Ljava/lang/String;LX/32s;I)V

    move-object v1, v2

    goto :goto_1

    .line 491346
    :cond_1
    if-nez v0, :cond_2

    .line 491347
    new-instance v0, Ljava/util/NoSuchElementException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No entry \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' found, can\'t remove"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 491348
    :cond_2
    iget-object v0, p0, LX/32t;->_buckets:[LX/32u;

    aput-object v1, v0, v5

    .line 491349
    return-void
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/32s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 491336
    new-instance v0, LX/321;

    iget-object v1, p0, LX/32t;->_buckets:[LX/32u;

    invoke-direct {v0, v1}, LX/321;-><init>([LX/32u;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 491322
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 491323
    const-string v1, "Properties=["

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 491324
    invoke-virtual {p0}, LX/32t;->b()[LX/32s;

    move-result-object v4

    array-length v5, v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 491325
    if-eqz v6, :cond_2

    .line 491326
    add-int/lit8 v0, v1, 0x1

    if-lez v1, :cond_0

    .line 491327
    const-string v1, ", "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 491328
    :cond_0
    iget-object v1, v6, LX/32s;->_propName:Ljava/lang/String;

    move-object v1, v1

    .line 491329
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 491330
    const/16 v1, 0x28

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 491331
    invoke-virtual {v6}, LX/32s;->a()LX/0lJ;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 491332
    const/16 v1, 0x29

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 491333
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 491334
    :cond_1
    const/16 v0, 0x5d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 491335
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
