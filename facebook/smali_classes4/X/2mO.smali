.class public LX/2mO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CBU;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CBW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 460265
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/2mO;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CBW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 460266
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 460267
    iput-object p1, p0, LX/2mO;->b:LX/0Ot;

    .line 460268
    return-void
.end method

.method public static a(LX/0QB;)LX/2mO;
    .locals 4

    .prologue
    .line 460269
    const-class v1, LX/2mO;

    monitor-enter v1

    .line 460270
    :try_start_0
    sget-object v0, LX/2mO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 460271
    sput-object v2, LX/2mO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460272
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460273
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 460274
    new-instance v3, LX/2mO;

    const/16 p0, 0x213a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2mO;-><init>(LX/0Ot;)V

    .line 460275
    move-object v0, v3

    .line 460276
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 460277
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2mO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460278
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 460279
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 460280
    check-cast p2, LX/CBV;

    .line 460281
    iget-object v0, p0, LX/2mO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/CBV;->a:Landroid/text/Layout$Alignment;

    iget-object v1, p2, LX/CBV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 460282
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 460283
    check-cast v2, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    invoke-static {v2}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    .line 460284
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 460285
    const v3, 0x7f0e0a34

    invoke-static {p1, p0, v3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b1063

    invoke-virtual {v2, v3}, LX/1ne;->s(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->d(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->c(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->b()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 460286
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 460287
    invoke-static {}, LX/1dS;->b()V

    .line 460288
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/CBU;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 460289
    new-instance v1, LX/CBV;

    invoke-direct {v1, p0}, LX/CBV;-><init>(LX/2mO;)V

    .line 460290
    sget-object v2, LX/2mO;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CBU;

    .line 460291
    if-nez v2, :cond_0

    .line 460292
    new-instance v2, LX/CBU;

    invoke-direct {v2}, LX/CBU;-><init>()V

    .line 460293
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/CBU;->a$redex0(LX/CBU;LX/1De;IILX/CBV;)V

    .line 460294
    move-object v1, v2

    .line 460295
    move-object v0, v1

    .line 460296
    return-object v0
.end method
