.class public LX/3PE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HG",
        "<",
        "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3PF;

.field private final b:LX/3PG;

.field private final c:LX/3PH;

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(LX/3PF;LX/3PG;LX/3PH;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 561923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 561924
    iput-object p1, p0, LX/3PE;->a:LX/3PF;

    .line 561925
    iput-object p2, p0, LX/3PE;->b:LX/3PG;

    .line 561926
    iput-object p3, p0, LX/3PE;->c:LX/3PH;

    .line 561927
    iput-object p4, p0, LX/3PE;->d:LX/0Uh;

    .line 561928
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/3Bq;
    .locals 6

    .prologue
    .line 561929
    check-cast p1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    .line 561930
    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 561931
    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    .line 561932
    iget-object v0, p0, LX/3PE;->c:LX/3PH;

    invoke-virtual {v0}, LX/3PH;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 561933
    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 561934
    :cond_0
    const/4 v0, 0x0

    .line 561935
    :goto_0
    return-object v0

    .line 561936
    :cond_1
    new-instance v0, LX/4VL;

    iget-object v3, p0, LX/3PE;->d:LX/0Uh;

    const/4 v4, 0x1

    new-array v4, v4, [LX/4VT;

    const/4 v5, 0x0

    invoke-static {v2, v1}, LX/3PG;->a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)LX/69y;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-direct {v0, v3, v4}, LX/4VL;-><init>(LX/0Uh;[LX/4VT;)V

    goto :goto_0

    .line 561937
    :cond_2
    iget-object v0, p0, LX/3PE;->a:LX/3PF;

    invoke-virtual {v0, v2, v1}, LX/3PF;->a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)LX/69x;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 561938
    const-class v0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 561939
    new-instance v0, LX/69p;

    invoke-direct {v0, p0}, LX/69p;-><init>(LX/3PE;)V

    return-object v0
.end method
