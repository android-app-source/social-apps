.class public LX/3CQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3CQ;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 530162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530163
    iput-object p1, p0, LX/3CQ;->a:Landroid/content/Context;

    .line 530164
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, LX/3CQ;->b:Landroid/text/TextPaint;

    .line 530165
    return-void
.end method

.method public static a(LX/0QB;)LX/3CQ;
    .locals 4

    .prologue
    .line 530166
    sget-object v0, LX/3CQ;->c:LX/3CQ;

    if-nez v0, :cond_1

    .line 530167
    const-class v1, LX/3CQ;

    monitor-enter v1

    .line 530168
    :try_start_0
    sget-object v0, LX/3CQ;->c:LX/3CQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 530169
    if-eqz v2, :cond_0

    .line 530170
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 530171
    new-instance p0, LX/3CQ;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/3CQ;-><init>(Landroid/content/Context;)V

    .line 530172
    move-object v0, p0

    .line 530173
    sput-object v0, LX/3CQ;->c:LX/3CQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530174
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 530175
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 530176
    :cond_1
    sget-object v0, LX/3CQ;->c:LX/3CQ;

    return-object v0

    .line 530177
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 530178
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
