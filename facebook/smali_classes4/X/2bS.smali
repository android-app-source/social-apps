.class public LX/2bS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 436606
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 436607
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 436608
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 436609
    :goto_0
    return v1

    .line 436610
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 436611
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 436612
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 436613
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 436614
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 436615
    const-string v7, "fullindex_education_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 436616
    invoke-static {p0, p1}, LX/2bU;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 436617
    :cond_2
    const-string v7, "group_mall_ads_education_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 436618
    invoke-static {p0, p1}, LX/4OF;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 436619
    :cond_3
    const-string v7, "post_edit_upsell_privacy"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 436620
    invoke-static {p0, p1}, LX/39L;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 436621
    :cond_4
    const-string v7, "reshare_education_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 436622
    invoke-static {p0, p1}, LX/2bi;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 436623
    :cond_5
    const-string v7, "tag_expansion_education"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 436624
    invoke-static {p0, p1}, LX/2bT;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 436625
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 436626
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 436627
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 436628
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 436629
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 436630
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 436631
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 436632
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 436633
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 436634
    if-eqz v0, :cond_0

    .line 436635
    const-string v1, "fullindex_education_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 436636
    invoke-static {p0, v0, p2}, LX/2bU;->a(LX/15i;ILX/0nX;)V

    .line 436637
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 436638
    if-eqz v0, :cond_1

    .line 436639
    const-string v1, "group_mall_ads_education_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 436640
    invoke-static {p0, v0, p2}, LX/4OF;->a(LX/15i;ILX/0nX;)V

    .line 436641
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 436642
    if-eqz v0, :cond_2

    .line 436643
    const-string v1, "post_edit_upsell_privacy"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 436644
    invoke-static {p0, v0, p2, p3}, LX/39L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 436645
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 436646
    if-eqz v0, :cond_3

    .line 436647
    const-string v1, "reshare_education_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 436648
    invoke-static {p0, v0, p2}, LX/2bi;->a(LX/15i;ILX/0nX;)V

    .line 436649
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 436650
    if-eqz v0, :cond_4

    .line 436651
    const-string v1, "tag_expansion_education"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 436652
    invoke-static {p0, v0, p2}, LX/2bT;->a(LX/15i;ILX/0nX;)V

    .line 436653
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 436654
    return-void
.end method
