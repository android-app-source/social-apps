.class public LX/2Mj;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/graphics/RectF;


# instance fields
.field private final b:LX/2Md;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 397158
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, LX/2Mj;->a:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(LX/2Md;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/videocodec/policy/VideoResizingPolicy;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 397154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397155
    iput-object p1, p0, LX/2Mj;->b:LX/2Md;

    .line 397156
    iput-object p2, p0, LX/2Mj;->c:LX/0Or;

    .line 397157
    return-void
.end method

.method public static a(LX/2Mj;LX/60x;)I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 397140
    iget-object v0, p0, LX/2Mj;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 397141
    if-eqz v7, :cond_0

    .line 397142
    iget-object v0, p0, LX/2Mj;->b:LX/2Md;

    const/4 v2, 0x0

    sget-object v3, LX/2Mj;->a:Landroid/graphics/RectF;

    sget-object v4, LX/7Sv;->NONE:LX/7Sv;

    move-object v1, p1

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, LX/2Md;->a(LX/60x;ILandroid/graphics/RectF;LX/7Sv;LX/7Sy;Ljava/util/List;)LX/7Sx;

    move-result-object v5

    .line 397143
    :cond_0
    invoke-static {p1, v5, v7}, LX/2Mj;->a(LX/60x;LX/7Sx;Z)I

    move-result v0

    return v0
.end method

.method private static a(LX/60x;LX/7Sx;Z)I
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 397144
    if-eqz p2, :cond_1

    .line 397145
    iget v0, p1, LX/7Sx;->j:I

    .line 397146
    :cond_0
    :goto_0
    return v0

    .line 397147
    :cond_1
    iget v0, p0, LX/60x;->e:I

    .line 397148
    if-gez v0, :cond_0

    .line 397149
    iget-wide v0, p0, LX/60x;->f:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-wide v0, p0, LX/60x;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 397150
    iget-wide v0, p0, LX/60x;->f:J

    const-wide/16 v2, 0x8

    mul-long/2addr v0, v2

    iget-wide v2, p0, LX/60x;->a:J

    div-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0

    .line 397151
    :cond_2
    iget v0, p0, LX/60x;->b:I

    if-lez v0, :cond_3

    iget v0, p0, LX/60x;->c:I

    if-lez v0, :cond_3

    .line 397152
    iget v0, p0, LX/60x;->b:I

    iget v1, p0, LX/60x;->c:I

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x6

    goto :goto_0

    .line 397153
    :cond_3
    const v0, 0x384000

    goto :goto_0
.end method

.method private static a(LX/60x;IIIII)LX/7Sw;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 397132
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    move p1, v6

    .line 397133
    :cond_0
    const/4 v0, -0x2

    if-ne p2, v0, :cond_1

    .line 397134
    iget-wide v0, p0, LX/60x;->a:J

    long-to-int p2, v0

    .line 397135
    :cond_1
    sub-int v4, p2, p1

    .line 397136
    iget v0, p0, LX/60x;->g:I

    if-lez v0, :cond_2

    .line 397137
    iget v6, p0, LX/60x;->g:I

    .line 397138
    :cond_2
    add-int v0, v6, p5

    div-int/lit8 v0, v0, 0x8

    div-int/lit16 v1, v4, 0x3e8

    mul-int v3, v0, v1

    .line 397139
    new-instance v0, LX/7Sw;

    move v1, p3

    move v2, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, LX/7Sw;-><init>(IIIIII)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/60x;II)LX/7Sw;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 397112
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 397113
    iget-object v0, p0, LX/2Mj;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397114
    iget-object v0, p0, LX/2Mj;->b:LX/2Md;

    const/4 v2, 0x0

    sget-object v3, LX/2Mj;->a:Landroid/graphics/RectF;

    sget-object v4, LX/7Sv;->NONE:LX/7Sv;

    move-object v1, p1

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, LX/2Md;->a(LX/60x;ILandroid/graphics/RectF;LX/7Sv;LX/7Sy;Ljava/util/List;)LX/7Sx;

    move-result-object v0

    .line 397115
    iget v3, v0, LX/7Sx;->d:I

    .line 397116
    iget v4, v0, LX/7Sx;->e:I

    .line 397117
    :goto_0
    invoke-static {p0, p1}, LX/2Mj;->a(LX/2Mj;LX/60x;)I

    move-result v5

    move-object v0, p1

    move v1, p2

    move v2, p3

    .line 397118
    invoke-static/range {v0 .. v5}, LX/2Mj;->a(LX/60x;IIIII)LX/7Sw;

    move-result-object v0

    return-object v0

    .line 397119
    :cond_0
    iget v3, p1, LX/60x;->b:I

    .line 397120
    iget v4, p1, LX/60x;->c:I

    goto :goto_0
.end method

.method public final a(LX/60x;LX/2Md;II)LX/7Sw;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 397121
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 397122
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 397123
    iget-object v0, p0, LX/2Mj;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 397124
    if-eqz v7, :cond_0

    .line 397125
    const/4 v2, 0x0

    sget-object v3, LX/2Mj;->a:Landroid/graphics/RectF;

    sget-object v4, LX/7Sv;->NONE:LX/7Sv;

    move-object v0, p2

    move-object v1, p1

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, LX/2Md;->a(LX/60x;ILandroid/graphics/RectF;LX/7Sv;LX/7Sy;Ljava/util/List;)LX/7Sx;

    move-result-object v5

    .line 397126
    iget v3, v5, LX/7Sx;->d:I

    .line 397127
    iget v4, v5, LX/7Sx;->e:I

    .line 397128
    :goto_0
    invoke-static {p1, v5, v7}, LX/2Mj;->a(LX/60x;LX/7Sx;Z)I

    move-result v5

    move-object v0, p1

    move v1, p3

    move v2, p4

    .line 397129
    invoke-static/range {v0 .. v5}, LX/2Mj;->a(LX/60x;IIIII)LX/7Sw;

    move-result-object v0

    return-object v0

    .line 397130
    :cond_0
    iget v3, p1, LX/60x;->b:I

    .line 397131
    iget v4, p1, LX/60x;->c:I

    goto :goto_0
.end method
