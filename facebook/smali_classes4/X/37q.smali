.class public abstract LX/37q;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/content/Context;",
            "LX/37q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 502332
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, LX/37q;->a:Ljava/util/WeakHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 502333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 502334
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/37q;
    .locals 3

    .prologue
    .line 502335
    sget-object v1, LX/37q;->a:Ljava/util/WeakHashMap;

    monitor-enter v1

    .line 502336
    :try_start_0
    sget-object v0, LX/37q;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/37q;

    .line 502337
    if-nez v0, :cond_0

    .line 502338
    new-instance v0, LX/38B;

    invoke-direct {v0, p0}, LX/38B;-><init>(Landroid/content/Context;)V

    .line 502339
    sget-object v2, LX/37q;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p0, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 502340
    :cond_0
    monitor-exit v1

    return-object v0

    .line 502341
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
