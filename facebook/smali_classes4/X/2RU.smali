.class public final enum LX/2RU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2RU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2RU;

.field public static final ALL_TYPES:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2RU;",
            ">;"
        }
    .end annotation
.end field

.field public static final FACEBOOK_FRIENDS_TYPES:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2RU;",
            ">;"
        }
    .end annotation
.end field

.field public static final MESSAGABLE_TYPES:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2RU;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PAGE:LX/2RU;

.field public static final PAGES_TYPES:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2RU;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum UNMATCHED:LX/2RU;

.field public static final enum USER:LX/2RU;


# instance fields
.field private final mGraphQlParamValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 409881
    new-instance v0, LX/2RU;

    const-string v1, "USER"

    const-string v2, "user"

    invoke-direct {v0, v1, v3, v2}, LX/2RU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2RU;->USER:LX/2RU;

    .line 409882
    new-instance v0, LX/2RU;

    const-string v1, "UNMATCHED"

    const-string v2, "unmatched"

    invoke-direct {v0, v1, v4, v2}, LX/2RU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2RU;->UNMATCHED:LX/2RU;

    .line 409883
    new-instance v0, LX/2RU;

    const-string v1, "PAGE"

    const-string v2, "page"

    invoke-direct {v0, v1, v5, v2}, LX/2RU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2RU;->PAGE:LX/2RU;

    .line 409884
    const/4 v0, 0x3

    new-array v0, v0, [LX/2RU;

    sget-object v1, LX/2RU;->USER:LX/2RU;

    aput-object v1, v0, v3

    sget-object v1, LX/2RU;->UNMATCHED:LX/2RU;

    aput-object v1, v0, v4

    sget-object v1, LX/2RU;->PAGE:LX/2RU;

    aput-object v1, v0, v5

    sput-object v0, LX/2RU;->$VALUES:[LX/2RU;

    .line 409885
    sget-object v0, LX/2RU;->USER:LX/2RU;

    sget-object v1, LX/2RU;->UNMATCHED:LX/2RU;

    sget-object v2, LX/2RU;->PAGE:LX/2RU;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2RU;->ALL_TYPES:LX/0Px;

    .line 409886
    sget-object v0, LX/2RU;->USER:LX/2RU;

    sget-object v1, LX/2RU;->UNMATCHED:LX/2RU;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 409887
    sget-object v0, LX/2RU;->USER:LX/2RU;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2RU;->FACEBOOK_FRIENDS_TYPES:LX/0Px;

    .line 409888
    sget-object v0, LX/2RU;->PAGE:LX/2RU;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2RU;->PAGES_TYPES:LX/0Px;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 409865
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 409866
    iput-object p3, p0, LX/2RU;->mGraphQlParamValue:Ljava/lang/String;

    .line 409867
    return-void
.end method

.method public static fromDbValue(I)LX/2RU;
    .locals 1

    .prologue
    .line 409876
    packed-switch p0, :pswitch_data_0

    .line 409877
    :pswitch_0
    sget-object v0, LX/2RU;->UNMATCHED:LX/2RU;

    .line 409878
    :goto_0
    return-object v0

    .line 409879
    :pswitch_1
    sget-object v0, LX/2RU;->USER:LX/2RU;

    goto :goto_0

    .line 409880
    :pswitch_2
    sget-object v0, LX/2RU;->PAGE:LX/2RU;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/2RU;
    .locals 1

    .prologue
    .line 409875
    const-class v0, LX/2RU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2RU;

    return-object v0
.end method

.method public static values()[LX/2RU;
    .locals 1

    .prologue
    .line 409874
    sget-object v0, LX/2RU;->$VALUES:[LX/2RU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2RU;

    return-object v0
.end method


# virtual methods
.method public final getDbValue()I
    .locals 2

    .prologue
    .line 409869
    sget-object v0, LX/2Rc;->a:[I

    invoke-virtual {p0}, LX/2RU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 409870
    :pswitch_0
    const/4 v0, 0x2

    .line 409871
    :goto_0
    return v0

    .line 409872
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 409873
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final getGraphQlParamValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 409868
    iget-object v0, p0, LX/2RU;->mGraphQlParamValue:Ljava/lang/String;

    return-object v0
.end method
