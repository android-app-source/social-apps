.class public LX/2fO;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/2fT;


# instance fields
.field private final b:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 446520
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 446521
    new-instance v0, LX/2fP;

    invoke-direct {v0}, LX/2fP;-><init>()V

    sput-object v0, LX/2fO;->a:LX/2fT;

    .line 446522
    :goto_0
    return-void

    .line 446523
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_1

    .line 446524
    new-instance v0, LX/2fQ;

    invoke-direct {v0}, LX/2fQ;-><init>()V

    sput-object v0, LX/2fO;->a:LX/2fT;

    goto :goto_0

    .line 446525
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    .line 446526
    new-instance v0, LX/2fR;

    invoke-direct {v0}, LX/2fR;-><init>()V

    sput-object v0, LX/2fO;->a:LX/2fT;

    goto :goto_0

    .line 446527
    :cond_2
    new-instance v0, LX/2fS;

    invoke-direct {v0}, LX/2fS;-><init>()V

    sput-object v0, LX/2fO;->a:LX/2fT;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 446517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 446518
    iput-object p1, p0, LX/2fO;->b:Ljava/lang/Object;

    .line 446519
    return-void
.end method

.method public static a()LX/2fO;
    .locals 2

    .prologue
    .line 446516
    new-instance v0, LX/2fO;

    sget-object v1, LX/2fO;->a:LX/2fT;

    invoke-interface {v1}, LX/2fT;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2fO;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 446514
    sget-object v0, LX/2fO;->a:LX/2fT;

    iget-object v1, p0, LX/2fO;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/2fT;->b(Ljava/lang/Object;I)V

    .line 446515
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 446485
    sget-object v0, LX/2fO;->a:LX/2fT;

    iget-object v1, p0, LX/2fO;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, LX/2fT;->a(Ljava/lang/Object;Landroid/view/View;I)V

    .line 446486
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 446512
    sget-object v0, LX/2fO;->a:LX/2fT;

    iget-object v1, p0, LX/2fO;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/2fT;->a(Ljava/lang/Object;Z)V

    .line 446513
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 446510
    sget-object v0, LX/2fO;->a:LX/2fT;

    iget-object v1, p0, LX/2fO;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/2fT;->a(Ljava/lang/Object;I)V

    .line 446511
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 446508
    sget-object v0, LX/2fO;->a:LX/2fT;

    iget-object v1, p0, LX/2fO;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/2fT;->e(Ljava/lang/Object;I)V

    .line 446509
    return-void
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 446506
    sget-object v0, LX/2fO;->a:LX/2fT;

    iget-object v1, p0, LX/2fO;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/2fT;->c(Ljava/lang/Object;I)V

    .line 446507
    return-void
.end method

.method public final e(I)V
    .locals 2

    .prologue
    .line 446504
    sget-object v0, LX/2fO;->a:LX/2fT;

    iget-object v1, p0, LX/2fO;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/2fT;->d(Ljava/lang/Object;I)V

    .line 446505
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 446492
    if-ne p0, p1, :cond_1

    .line 446493
    :cond_0
    :goto_0
    return v0

    .line 446494
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 446495
    goto :goto_0

    .line 446496
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 446497
    goto :goto_0

    .line 446498
    :cond_3
    check-cast p1, LX/2fO;

    .line 446499
    iget-object v2, p0, LX/2fO;->b:Ljava/lang/Object;

    if-nez v2, :cond_4

    .line 446500
    iget-object v2, p1, LX/2fO;->b:Ljava/lang/Object;

    if-eqz v2, :cond_0

    move v0, v1

    .line 446501
    goto :goto_0

    .line 446502
    :cond_4
    iget-object v2, p0, LX/2fO;->b:Ljava/lang/Object;

    iget-object v3, p1, LX/2fO;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 446503
    goto :goto_0
.end method

.method public final f(I)V
    .locals 2

    .prologue
    .line 446490
    sget-object v0, LX/2fO;->a:LX/2fT;

    iget-object v1, p0, LX/2fO;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/2fT;->f(Ljava/lang/Object;I)V

    .line 446491
    return-void
.end method

.method public final g(I)V
    .locals 2

    .prologue
    .line 446488
    sget-object v0, LX/2fO;->a:LX/2fT;

    iget-object v1, p0, LX/2fO;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/2fT;->g(Ljava/lang/Object;I)V

    .line 446489
    return-void
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 446487
    iget-object v0, p0, LX/2fO;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2fO;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
