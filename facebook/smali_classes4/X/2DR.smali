.class public LX/2DR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2gK;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0iA;

.field public final c:Landroid/app/Activity;

.field private final d:Landroid/accounts/AccountManager;

.field public e:Landroid/telephony/TelephonyManager;

.field public f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public g:Ljava/lang/String;


# direct methods
.method private constructor <init>(LX/0iA;Landroid/app/Activity;Landroid/accounts/AccountManager;Landroid/telephony/TelephonyManager;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 384202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384203
    iput-object p1, p0, LX/2DR;->b:LX/0iA;

    .line 384204
    iput-object p2, p0, LX/2DR;->c:Landroid/app/Activity;

    .line 384205
    iput-object p3, p0, LX/2DR;->d:Landroid/accounts/AccountManager;

    .line 384206
    iput-object p4, p0, LX/2DR;->e:Landroid/telephony/TelephonyManager;

    .line 384207
    iput-object p5, p0, LX/2DR;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 384208
    return-void
.end method

.method public static a(LX/0QB;)LX/2DR;
    .locals 1

    .prologue
    .line 384201
    invoke-static {p0}, LX/2DR;->b(LX/0QB;)LX/2DR;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2DR;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 384160
    iget-object v0, p0, LX/2DR;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "activity_launcher"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 384161
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 384162
    const-string v1, "activity_launcher"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 384163
    :cond_0
    return-object p1
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 384191
    packed-switch p0, :pswitch_data_0

    .line 384192
    const-string v0, "unset"

    :goto_0
    return-object v0

    .line 384193
    :pswitch_0
    const-string v0, "tweak_prefill"

    goto :goto_0

    .line 384194
    :pswitch_1
    const-string v0, "fast_redirect"

    goto :goto_0

    .line 384195
    :pswitch_2
    const-string v0, "medium_redirect"

    goto :goto_0

    .line 384196
    :pswitch_3
    const-string v0, "slow_redirect"

    goto :goto_0

    .line 384197
    :pswitch_4
    const-string v0, "tweak_prefill_and_fast_redirect"

    goto :goto_0

    .line 384198
    :pswitch_5
    const-string v0, "tweak_prefill_and_medium_redirect"

    goto :goto_0

    .line 384199
    :pswitch_6
    const-string v0, "tweak_prefill_and_slow_redirect"

    goto :goto_0

    .line 384200
    :pswitch_7
    const-string v0, "control"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/2DR;
    .locals 6

    .prologue
    .line 384187
    new-instance v0, LX/2DR;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-static {p0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-static {p0}, LX/2sm;->b(LX/0QB;)Landroid/accounts/AccountManager;

    move-result-object v3

    check-cast v3, Landroid/accounts/AccountManager;

    invoke-static {p0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v0 .. v5}, LX/2DR;-><init>(LX/0iA;Landroid/app/Activity;Landroid/accounts/AccountManager;Landroid/telephony/TelephonyManager;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 384188
    const/16 v1, 0x1c4

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    .line 384189
    iput-object v1, v0, LX/2DR;->a:LX/0Or;

    .line 384190
    return-object v0
.end method


# virtual methods
.method public final b()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 384180
    const/4 v1, 0x0

    .line 384181
    iget-object v0, p0, LX/2DR;->b:LX/0iA;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/13G;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13G;

    .line 384182
    if-eqz v0, :cond_0

    .line 384183
    invoke-interface {v0}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/2DR;->g:Ljava/lang/String;

    .line 384184
    iget-object v1, p0, LX/2DR;->c:Landroid/app/Activity;

    invoke-interface {v0, v1}, LX/13G;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 384185
    iget-object v2, p0, LX/2DR;->b:LX/0iA;

    invoke-virtual {v2}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v2

    invoke-interface {v0}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 384186
    :goto_0
    invoke-static {p0, v0}, LX/2DR;->a(LX/2DR;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final d()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 384177
    iget-object v0, p0, LX/2DR;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gK;

    .line 384178
    invoke-virtual {v0}, LX/2gK;->a()Landroid/content/Intent;

    move-result-object v0

    .line 384179
    invoke-static {p0, v0}, LX/2DR;->a(LX/2DR;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 384174
    iget-object v1, p0, LX/2DR;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "add_account"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2DR;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const/high16 v2, 0x100000

    and-int/2addr v1, v2

    if-nez v1, :cond_0

    .line 384175
    const/4 v0, 0x1

    .line 384176
    :cond_0
    return v0
.end method

.method public final h()J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 384171
    :try_start_0
    iget-object v2, p0, LX/2DR;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1CA;->o:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 384172
    :goto_0
    return-wide v0

    .line 384173
    :catch_0
    goto :goto_0
.end method

.method public final j()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 384164
    invoke-static {}, LX/0RA;->d()Ljava/util/TreeSet;

    move-result-object v1

    .line 384165
    iget-object v0, p0, LX/2DR;->d:Landroid/accounts/AccountManager;

    if-eqz v0, :cond_1

    .line 384166
    iget-object v0, p0, LX/2DR;->d:Landroid/accounts/AccountManager;

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 384167
    sget-object v5, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 384168
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 384169
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 384170
    :cond_1
    return-object v1
.end method
