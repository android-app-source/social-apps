.class public LX/2He;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2He;


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390771
    iput-object p1, p0, LX/2He;->a:LX/0Uh;

    .line 390772
    return-void
.end method

.method public static a(LX/0QB;)LX/2He;
    .locals 4

    .prologue
    .line 390773
    sget-object v0, LX/2He;->b:LX/2He;

    if-nez v0, :cond_1

    .line 390774
    const-class v1, LX/2He;

    monitor-enter v1

    .line 390775
    :try_start_0
    sget-object v0, LX/2He;->b:LX/2He;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390776
    if-eqz v2, :cond_0

    .line 390777
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390778
    new-instance p0, LX/2He;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/2He;-><init>(LX/0Uh;)V

    .line 390779
    move-object v0, p0

    .line 390780
    sput-object v0, LX/2He;->b:LX/2He;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390781
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390782
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390783
    :cond_1
    sget-object v0, LX/2He;->b:LX/2He;

    return-object v0

    .line 390784
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-com.facebook.analytics.logger.HoneyClientEvent._Constructor"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 390786
    iget-object v1, p0, LX/2He;->a:LX/0Uh;

    const/16 v2, 0x49d

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-eq v1, v2, :cond_0

    .line 390787
    :goto_0
    return-object v0

    .line 390788
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Pistol fire crash check"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 390789
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, LX/009;->handleException(Ljava/lang/Throwable;Ljava/util/Map;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;

    .line 390790
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "fbandroid_pistol_fire_crash"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
