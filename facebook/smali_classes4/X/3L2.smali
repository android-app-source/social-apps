.class public LX/3L2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3L3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3L3",
        "<",
        "LX/FON;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1zC;

.field private final b:Landroid/content/res/Resources;

.field private final c:LX/3L0;

.field private final d:LX/3L4;

.field private final e:LX/3GL;

.field private final f:LX/0Uh;


# direct methods
.method public constructor <init>(LX/1zC;Landroid/content/res/Resources;LX/3L0;LX/3L4;LX/3GL;LX/0Uh;)V
    .locals 0

    .prologue
    .line 549879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549880
    iput-object p1, p0, LX/3L2;->a:LX/1zC;

    .line 549881
    iput-object p2, p0, LX/3L2;->b:Landroid/content/res/Resources;

    .line 549882
    iput-object p3, p0, LX/3L2;->c:LX/3L0;

    .line 549883
    iput-object p4, p0, LX/3L2;->d:LX/3L4;

    .line 549884
    iput-object p5, p0, LX/3L2;->e:LX/3GL;

    .line 549885
    iput-object p6, p0, LX/3L2;->f:LX/0Uh;

    .line 549886
    return-void
.end method

.method private static a(Landroid/text/TextPaint;Ljava/lang/String;I)LX/FOT;
    .locals 2

    .prologue
    .line 549805
    invoke-virtual {p0, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 549806
    int-to-float v1, p2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 549807
    new-instance v0, LX/FOT;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/FOT;-><init>(Landroid/text/TextPaint;Ljava/util/List;)V

    .line 549808
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/List;Ljava/util/List;III)LX/FOT;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/text/TextPaint;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;III)",
            "LX/FOT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 549809
    invoke-direct {p0, p2}, LX/3L2;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 549810
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextPaint;

    .line 549811
    invoke-static {v0, v1, p3}, LX/3L2;->a(Landroid/text/TextPaint;Ljava/lang/String;I)LX/FOT;

    move-result-object v0

    .line 549812
    if-eqz v0, :cond_0

    .line 549813
    :goto_0
    return-object v0

    .line 549814
    :cond_1
    if-gez p5, :cond_2

    .line 549815
    const p5, 0x7fffffff

    .line 549816
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/text/TextPaint;

    .line 549817
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    .line 549818
    if-ge v0, v3, :cond_3

    .line 549819
    new-instance v0, LX/FOT;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v5, v1}, LX/FOT;-><init>(Landroid/text/TextPaint;Ljava/util/List;)V

    goto :goto_0

    .line 549820
    :cond_3
    const/4 v0, 0x3

    new-array v6, v0, [F

    .line 549821
    const/4 v0, 0x0

    const/16 v1, 0x8

    invoke-direct {p0, v1}, LX/3L2;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    aput v1, v6, v0

    .line 549822
    const/4 v0, 0x1

    const/16 v1, 0x58

    invoke-direct {p0, v1}, LX/3L2;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    aput v1, v6, v0

    .line 549823
    const/16 v0, 0x378

    invoke-direct {p0, v0}, LX/3L2;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    aput v0, v6, v3

    .line 549824
    iget-object v0, p0, LX/3L2;->c:LX/3L0;

    int-to-float v2, p3

    int-to-float v4, p5

    move-object v1, p2

    move v3, p4

    invoke-virtual/range {v0 .. v6}, LX/3L0;->a(Ljava/util/List;FIFLandroid/text/TextPaint;[F)LX/FOS;

    move-result-object v1

    .line 549825
    iget-object v0, v1, LX/FOS;->a:Ljava/util/List;

    move-object v2, v0

    .line 549826
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 549827
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 549828
    iget v3, v1, LX/FOS;->b:I

    move v3, v3

    .line 549829
    if-lez v3, :cond_4

    .line 549830
    add-int/lit8 v3, v0, -0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 549831
    iget v4, v1, LX/FOS;->b:I

    move v1, v4

    .line 549832
    invoke-direct {p0, v1}, LX/3L2;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 549833
    :cond_4
    new-instance v0, LX/FOT;

    invoke-direct {v0, v5, v2}, LX/FOT;-><init>(Landroid/text/TextPaint;Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method private a(LX/FON;Ljava/util/List;ILandroid/text/Layout$Alignment;II)Landroid/text/Layout;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FON;",
            "Ljava/util/List",
            "<",
            "Landroid/text/TextPaint;",
            ">;I",
            "Landroid/text/Layout$Alignment;",
            "II)",
            "Landroid/text/Layout;"
        }
    .end annotation

    .prologue
    .line 549834
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextPaint;

    .line 549835
    if-nez p1, :cond_0

    .line 549836
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 549837
    invoke-direct {p0, v1, v0, p3, p4}, LX/3L2;->a(Ljava/util/List;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    .line 549838
    :goto_0
    return-object v0

    .line 549839
    :cond_0
    invoke-direct {p0, p1}, LX/3L2;->a(LX/FON;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 549840
    iget-object v1, p1, LX/FON;->b:Ljava/lang/String;

    move-object v1, v1

    .line 549841
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 549842
    invoke-direct {p0, v1, v0, p3, p4}, LX/3L2;->a(Ljava/util/List;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    goto :goto_0

    .line 549843
    :cond_1
    iget-object v0, p1, LX/FON;->c:LX/0Px;

    move-object v2, v0

    .line 549844
    move-object v0, p0

    move-object v1, p2

    move v3, p3

    move v4, p5

    move v5, p6

    .line 549845
    invoke-direct/range {v0 .. v5}, LX/3L2;->a(Ljava/util/List;Ljava/util/List;III)LX/FOT;

    move-result-object v0

    .line 549846
    iget-object v1, v0, LX/FOT;->b:Ljava/util/List;

    iget-object v0, v0, LX/FOT;->a:Landroid/text/TextPaint;

    invoke-direct {p0, v1, v0, p3, p4}, LX/3L2;->a(Ljava/util/List;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/util/List;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/text/TextPaint;",
            "I",
            "Landroid/text/Layout$Alignment;",
            ")",
            "Landroid/text/StaticLayout;"
        }
    .end annotation

    .prologue
    .line 549847
    if-nez p1, :cond_0

    .line 549848
    const/4 v0, 0x0

    .line 549849
    :goto_0
    return-object v0

    .line 549850
    :cond_0
    const/4 v0, 0x1

    .line 549851
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 549852
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 549853
    if-nez v1, :cond_1

    .line 549854
    const-string v1, "\n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549855
    :cond_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549856
    const/4 v0, 0x0

    move v1, v0

    .line 549857
    goto :goto_1

    .line 549858
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 549859
    iget-object v0, p0, LX/3L2;->a:LX/1zC;

    invoke-virtual {p2}, Landroid/text/TextPaint;->getTextSize()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 549860
    new-instance v0, Landroid/text/StaticLayout;

    const/4 v2, 0x0

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v3

    const/16 v5, 0x4000

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v9, 0x1

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v4, p2

    move-object/from16 v6, p4

    move v11, p3

    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    goto :goto_0
.end method

.method private a(I)Ljava/lang/String;
    .locals 6

    .prologue
    .line 549861
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/3L2;->e:LX/3GL;

    invoke-virtual {v1}, LX/3GL;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3L2;->b:Landroid/content/res/Resources;

    const v2, 0x7f080247

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 549862
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 549863
    invoke-direct {p0, p1}, LX/3L2;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 549864
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3L2;->b:Landroid/content/res/Resources;

    const v1, 0x7f080246

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/FON;)Z
    .locals 2

    .prologue
    .line 549865
    iget-object v0, p0, LX/3L2;->d:LX/3L4;

    sget-object v1, LX/3L4;->USE_THREAD_NAME_IF_AVAILABLE:LX/3L4;

    if-ne v0, v1, :cond_0

    .line 549866
    iget-boolean v0, p1, LX/FON;->a:Z

    move v0, v0

    .line 549867
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 549868
    iget-object v0, p0, LX/3L2;->f:LX/0Uh;

    const/16 v1, 0x172

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 549869
    iget-object v0, p0, LX/3L2;->e:LX/3GL;

    invoke-virtual {v0, p1}, LX/3GL;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 549870
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3L2;->e:LX/3GL;

    invoke-virtual {v0, p1}, LX/3GL;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/util/List;ILandroid/text/Layout$Alignment;II)Landroid/text/Layout;
    .locals 7

    .prologue
    .line 549871
    move-object v1, p1

    check-cast v1, LX/FON;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, LX/3L2;->a(LX/FON;Ljava/util/List;ILandroid/text/Layout$Alignment;II)Landroid/text/Layout;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/FON;I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 549872
    invoke-direct {p0, p1}, LX/3L2;->a(LX/FON;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 549873
    iget-object v0, p1, LX/FON;->b:Ljava/lang/String;

    move-object v0, v0

    .line 549874
    :goto_0
    return-object v0

    .line 549875
    :cond_0
    iget-object v0, p1, LX/FON;->c:LX/0Px;

    move-object v0, v0

    .line 549876
    const/4 v1, -0x1

    if-eq p2, v1, :cond_1

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-gt v1, p2, :cond_2

    .line 549877
    :cond_1
    invoke-direct {p0, v0}, LX/3L2;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 549878
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2, p2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v2}, LX/3L2;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    sub-int/2addr v0, p2

    invoke-direct {p0, v0}, LX/3L2;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
