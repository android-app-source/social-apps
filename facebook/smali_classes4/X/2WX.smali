.class public final LX/2WX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:LX/2DI;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILX/2DI;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 419005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419006
    iput p1, p0, LX/2WX;->a:I

    .line 419007
    iput-object p2, p0, LX/2WX;->b:LX/2DI;

    .line 419008
    iput-object p3, p0, LX/2WX;->c:Ljava/lang/String;

    .line 419009
    iput-object p4, p0, LX/2WX;->d:Ljava/lang/String;

    .line 419010
    return-void
.end method

.method public static a(ILX/2DI;Ljava/lang/String;Ljava/lang/String;)LX/2WX;
    .locals 2

    .prologue
    .line 419011
    if-nez p2, :cond_0

    .line 419012
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startServiceHackAction cannot be null for logout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419013
    :cond_0
    if-nez p3, :cond_1

    .line 419014
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "userIdToDeleteOnExit cannot be null for logout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419015
    :cond_1
    new-instance v0, LX/2WX;

    invoke-direct {v0, p0, p1, p2, p3}, LX/2WX;-><init>(ILX/2DI;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
