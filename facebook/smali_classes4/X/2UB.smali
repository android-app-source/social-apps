.class public LX/2UB;
.super LX/1Eg;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/regex/Pattern;

.field public static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:LX/0Uo;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:LX/0SG;

.field public final g:LX/2U8;

.field public final h:LX/2UC;

.field private final i:LX/1Ml;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Eiz;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2U9;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Uh;

.field private final o:LX/0W3;

.field private p:I

.field public final q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 415429
    const-class v0, LX/2UB;

    sput-object v0, LX/2UB;->a:Ljava/lang/Class;

    .line 415430
    const-string v0, "(^|\\D)(\\d{4,10})($|\\D)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/2UB;->b:Ljava/util/regex/Pattern;

    .line 415431
    const-string v0, "32665"

    const-string v1, "FACEBOOK"

    const-string v2, "1006"

    const-string v3, "575756"

    const-string v4, "57575601"

    const-string v5, "57575751"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "2123"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "3404"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "561619"

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/2UB;->c:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/0Uo;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/2U8;LX/1Ml;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2UC;LX/0Uh;LX/0W3;)V
    .locals 1
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Uo;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            "LX/2U8;",
            "LX/1Ml;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Eiz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2U9;",
            ">;",
            "LX/2UC;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415432
    const-string v0, "READ_SMS_FOR_CONFIRMATION_CODE"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 415433
    const/4 v0, 0x0

    iput v0, p0, LX/2UB;->p:I

    .line 415434
    iput-object p1, p0, LX/2UB;->d:LX/0Uo;

    .line 415435
    iput-object p2, p0, LX/2UB;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 415436
    iput-object p3, p0, LX/2UB;->f:LX/0SG;

    .line 415437
    iput-object p4, p0, LX/2UB;->g:LX/2U8;

    .line 415438
    iput-object p5, p0, LX/2UB;->i:LX/1Ml;

    .line 415439
    iput-object p6, p0, LX/2UB;->j:LX/0Ot;

    .line 415440
    iput-object p7, p0, LX/2UB;->k:LX/0Ot;

    .line 415441
    iput-object p8, p0, LX/2UB;->l:LX/0Ot;

    .line 415442
    iput-object p9, p0, LX/2UB;->m:LX/0Ot;

    .line 415443
    iput-object p10, p0, LX/2UB;->h:LX/2UC;

    .line 415444
    iput-object p11, p0, LX/2UB;->n:LX/0Uh;

    .line 415445
    iput-object p12, p0, LX/2UB;->o:LX/0W3;

    .line 415446
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2UB;->q:Ljava/util/HashMap;

    .line 415447
    return-void
.end method

.method public static a(LX/2UB;Lcom/facebook/growth/model/Contactpoint;LX/EiE;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 415448
    iget v0, p0, LX/2UB;->p:I

    if-lez v0, :cond_0

    iget-object v0, p2, LX/EiE;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, LX/2UB;->p:I

    if-eq v0, v1, :cond_0

    .line 415449
    :goto_0
    return v2

    .line 415450
    :cond_0
    new-instance v3, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;

    iget-object v0, p2, LX/EiE;->a:Ljava/lang/String;

    sget-object v1, LX/Ej0;->ANDROID_AUTO_SMS_API:LX/Ej0;

    const-string v4, "auto_confirmation"

    invoke-direct {v3, p1, v0, v1, v4}, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;-><init>(Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;LX/Ej0;Ljava/lang/String;)V

    .line 415451
    const/4 v4, 0x0

    .line 415452
    :try_start_0
    iget-object v0, p0, LX/2UB;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/2UB;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v0, v1, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_1
    move v2, v0

    .line 415453
    goto :goto_0

    .line 415454
    :catch_0
    move-exception v0

    .line 415455
    :try_start_1
    invoke-static {v0}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v0

    .line 415456
    iget-object v1, p0, LX/2UB;->g:LX/2U8;

    invoke-virtual {v1, v0}, LX/2U8;->a(Lcom/facebook/fbservice/service/ServiceException;)LX/3rL;

    move-result-object v1

    .line 415457
    invoke-static {v1, v0}, LX/2U8;->a(LX/3rL;Lcom/facebook/fbservice/service/ServiceException;)LX/3rL;

    move-result-object v0

    .line 415458
    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415459
    if-eqz v0, :cond_4

    .line 415460
    iget-object v1, p0, LX/2UB;->q:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 415461
    iget-object v1, p0, LX/2UB;->q:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415462
    :cond_1
    iget-object v3, p0, LX/2UB;->q:Ljava/util/HashMap;

    iget-object v1, p0, LX/2UB;->q:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v2

    goto :goto_1

    .line 415463
    :catchall_0
    move-exception v0

    move-object v1, v0

    if-eqz v4, :cond_3

    .line 415464
    iget-object v0, p0, LX/2UB;->q:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 415465
    iget-object v0, p0, LX/2UB;->q:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415466
    :cond_2
    iget-object v2, p0, LX/2UB;->q:Ljava/util/HashMap;

    iget-object v0, p0, LX/2UB;->q:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    throw v1

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/2UB;
    .locals 13

    .prologue
    .line 415467
    new-instance v0, LX/2UB;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v1

    check-cast v1, LX/0Uo;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {p0}, LX/2U8;->b(LX/0QB;)LX/2U8;

    move-result-object v4

    check-cast v4, LX/2U8;

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v5

    check-cast v5, LX/1Ml;

    const/16 v6, 0x1ce

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xb83

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x19f4

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3e8

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p0}, LX/2UC;->b(LX/0QB;)LX/2UC;

    move-result-object v10

    check-cast v10, LX/2UC;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v12

    check-cast v12, LX/0W3;

    invoke-direct/range {v0 .. v12}, LX/2UB;-><init>(LX/0Uo;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/2U8;LX/1Ml;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2UC;LX/0Uh;LX/0W3;)V

    .line 415468
    return-object v0
.end method

.method public static l(LX/2UB;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/growth/model/Contactpoint;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415469
    iget-object v0, p0, LX/2UB;->g:LX/2U8;

    sget-object v1, LX/2UD;->REGULAR_SMS_CONFIRMATION:LX/2UD;

    invoke-virtual {v0, v1}, LX/2U8;->a(LX/2UD;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final f()J
    .locals 6

    .prologue
    .line 415470
    invoke-virtual {p0}, LX/2UB;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 415471
    const-wide/16 v0, -0x1

    .line 415472
    :goto_0
    return-wide v0

    .line 415473
    :cond_0
    iget-object v0, p0, LX/2UB;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3df;->a:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 415474
    iget-object v0, p0, LX/2UB;->d:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x14

    .line 415475
    :goto_1
    int-to-long v0, v0

    const-wide/32 v4, 0xea60

    mul-long/2addr v0, v4

    add-long/2addr v0, v2

    goto :goto_0

    .line 415476
    :cond_1
    const/4 v0, 0x5

    goto :goto_1
.end method

.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415477
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 415478
    invoke-static {p0}, LX/2UB;->l(LX/2UB;)Ljava/util/Map;

    move-result-object v0

    .line 415479
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 415480
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 415481
    iget-object v1, p0, LX/2UB;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x240c8400

    cmp-long v1, v6, v8

    if-gtz v1, :cond_0

    .line 415482
    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    .line 415483
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 415484
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 415485
    iget-object v1, p0, LX/2UB;->g:LX/2U8;

    sget-object v5, LX/2UD;->REGULAR_SMS_CONFIRMATION:LX/2UD;

    new-array v0, v3, [Lcom/facebook/growth/model/Contactpoint;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v1, v5, v0}, LX/2U8;->a(LX/2UD;[Lcom/facebook/growth/model/Contactpoint;)Z

    .line 415486
    :cond_2
    return v2
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 415487
    iget-object v0, p0, LX/2UB;->i:LX/1Ml;

    const-string v1, "android.permission.READ_SMS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v1

    .line 415488
    iget-object v0, p0, LX/2UB;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U9;

    invoke-virtual {v0, v1}, LX/2U9;->a(Z)V

    .line 415489
    if-eqz v1, :cond_1

    .line 415490
    iget-object v0, p0, LX/2UB;->n:LX/0Uh;

    const/16 v1, 0x384

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415491
    iget-object v0, p0, LX/2UB;->o:LX/0W3;

    sget-wide v2, LX/0X5;->hU:J

    invoke-interface {v0, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/2UB;->p:I

    .line 415492
    :cond_0
    invoke-static {p0}, LX/2UB;->l(LX/2UB;)Ljava/util/Map;

    move-result-object v6

    .line 415493
    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 415494
    :cond_1
    :goto_0
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 415495
    :cond_2
    iget-object v5, p0, LX/2UB;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/3df;->a:LX/0Tn;

    const-wide/16 v9, 0x0

    invoke-interface {v5, v7, v9, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v7

    .line 415496
    iget-object v5, p0, LX/2UB;->h:LX/2UC;

    const-wide/32 v9, 0xf731400

    invoke-virtual {v5, v7, v8, v9, v10}, LX/2UC;->a(JJ)Ljava/util/List;

    move-result-object v7

    .line 415497
    iget-object v5, p0, LX/2UB;->m:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2U9;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v5, v8}, LX/2U9;->a(I)V

    .line 415498
    sget-object v8, LX/2UB;->c:Ljava/util/Set;

    sget-object v9, LX/2UB;->b:Ljava/util/regex/Pattern;

    const/4 v10, 0x2

    invoke-static {v7, v8, v9, v10}, LX/2UC;->a(Ljava/util/List;Ljava/util/Set;Ljava/util/regex/Pattern;I)Ljava/util/List;

    move-result-object v5

    .line 415499
    iget-object v7, p0, LX/2UB;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    sget-object v8, LX/3df;->a:LX/0Tn;

    iget-object v9, p0, LX/2UB;->f:LX/0SG;

    invoke-interface {v9}, LX/0SG;->a()J

    move-result-wide v9

    invoke-interface {v7, v8, v9, v10}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v7

    invoke-interface {v7}, LX/0hN;->commit()V

    .line 415500
    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    .line 415501
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-interface {v6}, Ljava/util/Set;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 415502
    :cond_3
    :goto_1
    goto :goto_0

    .line 415503
    :cond_4
    iget-object v7, p0, LX/2UB;->m:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/2U9;

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v8

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v7, v8, v9}, LX/2U9;->a(II)V

    .line 415504
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/growth/model/Contactpoint;

    .line 415505
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EiE;

    .line 415506
    invoke-static {p0, v7, v8}, LX/2UB;->a(LX/2UB;Lcom/facebook/growth/model/Contactpoint;LX/EiE;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 415507
    iget-object v9, p0, LX/2UB;->m:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/2U9;

    iget-object v8, v8, LX/EiE;->b:LX/EiF;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, LX/2U9;->a(Ljava/lang/String;)V

    .line 415508
    iget-object v9, p0, LX/2UB;->g:LX/2U8;

    sget-object v10, LX/2UD;->REGULAR_SMS_CONFIRMATION:LX/2UD;

    const/4 v8, 0x0

    new-array v8, v8, [Lcom/facebook/growth/model/Contactpoint;

    invoke-interface {v6, v8}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v9, v10, v8}, LX/2U8;->a(LX/2UD;[Lcom/facebook/growth/model/Contactpoint;)Z

    .line 415509
    new-instance v8, Landroid/content/Intent;

    const-string v9, "action_background_contactpoint_confirmed"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v9, "extra_background_confirmed_contactpoint"

    invoke-virtual {v8, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v8

    .line 415510
    iget-object v7, p0, LX/2UB;->j:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-interface {v7, v8}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto :goto_1

    .line 415511
    :cond_7
    iget-object v7, p0, LX/2UB;->m:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/2U9;

    iget-object v8, p0, LX/2UB;->q:Ljava/util/HashMap;

    invoke-virtual {v7, v8}, LX/2U9;->a(Ljava/util/HashMap;)V

    .line 415512
    iget-object v7, p0, LX/2UB;->q:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    goto/16 :goto_1
.end method
