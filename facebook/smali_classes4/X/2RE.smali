.class public final LX/2RE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/2RE;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/net/Uri;

.field public final c:LX/2RK;

.field public final d:LX/2RM;

.field public final e:LX/2RN;

.field public final f:LX/2RO;

.field public final g:LX/2RP;

.field private final h:LX/2RJ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2RJ;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409684
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".contacts"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2RE;->a:Ljava/lang/String;

    .line 409685
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2RE;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/2RE;->b:Landroid/net/Uri;

    .line 409686
    new-instance v0, LX/2RK;

    invoke-direct {v0, p0}, LX/2RK;-><init>(LX/2RE;)V

    iput-object v0, p0, LX/2RE;->c:LX/2RK;

    .line 409687
    new-instance v0, LX/2RM;

    invoke-direct {v0, p0}, LX/2RM;-><init>(LX/2RE;)V

    iput-object v0, p0, LX/2RE;->d:LX/2RM;

    .line 409688
    new-instance v0, LX/2RN;

    invoke-direct {v0, p0}, LX/2RN;-><init>(LX/2RE;)V

    iput-object v0, p0, LX/2RE;->e:LX/2RN;

    .line 409689
    new-instance v0, LX/2RO;

    invoke-direct {v0, p0}, LX/2RO;-><init>(LX/2RE;)V

    iput-object v0, p0, LX/2RE;->f:LX/2RO;

    .line 409690
    new-instance v0, LX/2RP;

    invoke-direct {v0, p0}, LX/2RP;-><init>(LX/2RE;)V

    iput-object v0, p0, LX/2RE;->g:LX/2RP;

    .line 409691
    iput-object p2, p0, LX/2RE;->h:LX/2RJ;

    .line 409692
    return-void
.end method

.method public static a(LX/0QB;)LX/2RE;
    .locals 5

    .prologue
    .line 409693
    sget-object v0, LX/2RE;->i:LX/2RE;

    if-nez v0, :cond_1

    .line 409694
    const-class v1, LX/2RE;

    monitor-enter v1

    .line 409695
    :try_start_0
    sget-object v0, LX/2RE;->i:LX/2RE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 409696
    if-eqz v2, :cond_0

    .line 409697
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 409698
    new-instance p0, LX/2RE;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2RJ;->b(LX/0QB;)LX/2RJ;

    move-result-object v4

    check-cast v4, LX/2RJ;

    invoke-direct {p0, v3, v4}, LX/2RE;-><init>(Landroid/content/Context;LX/2RJ;)V

    .line 409699
    move-object v0, p0

    .line 409700
    sput-object v0, LX/2RE;->i:LX/2RE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 409701
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 409702
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 409703
    :cond_1
    sget-object v0, LX/2RE;->i:LX/2RE;

    return-object v0

    .line 409704
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 409705
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0PH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 409679
    sget-object v0, LX/0PH;->NAME:LX/0PH;

    sget-object v1, LX/0PH;->PHONE_E164:LX/0PH;

    sget-object v2, LX/0PH;->PHONE_NATIONAL:LX/0PH;

    sget-object v3, LX/0PH;->PHONE_LOCAL:LX/0PH;

    invoke-static {v0, v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    .line 409680
    iget-object v1, p0, LX/2RE;->h:LX/2RJ;

    invoke-virtual {v1}, LX/2RJ;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 409681
    sget-object v1, LX/0PH;->USERNAME:LX/0PH;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 409682
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
