.class public LX/3PS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3PT;


# direct methods
.method public constructor <init>(LX/3PT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562048
    iput-object p1, p0, LX/3PS;->a:LX/3PT;

    .line 562049
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/4VT;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562050
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;

    const/4 v1, 0x0

    .line 562051
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel$TargetModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel$TargetModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 562052
    :cond_0
    const/4 v0, 0x0

    .line 562053
    :goto_0
    return-object v0

    .line 562054
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel$TargetModel;

    move-result-object v0

    .line 562055
    iget-object v1, p0, LX/3PS;->a:LX/3PT;

    invoke-static {v0, v1}, LX/6AF;->a(Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$CommentFieldsLightweightRecCreationModel$AttachmentsModel$TargetModel;LX/3PT;)LX/6A5;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 562056
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/LightweightPlaceListMutationsModels$PlaceListLightweightCreateFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562057
    const/4 v0, 0x0

    return-object v0
.end method
