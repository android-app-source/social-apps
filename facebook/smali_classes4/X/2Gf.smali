.class public final LX/2Gf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/2Gj;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/2Gj;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 389056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389057
    iput-object p1, p0, LX/2Gf;->a:LX/0QB;

    .line 389058
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 389041
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2Gf;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 389043
    packed-switch p2, :pswitch_data_0

    .line 389044
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 389045
    :pswitch_0
    invoke-static {p1}, LX/2Gi;->a(LX/0QB;)LX/2Gi;

    move-result-object v0

    .line 389046
    :goto_0
    return-object v0

    .line 389047
    :pswitch_1
    invoke-static {p1}, LX/2Gk;->a(LX/0QB;)LX/2Gk;

    move-result-object v0

    goto :goto_0

    .line 389048
    :pswitch_2
    invoke-static {p1}, LX/2HH;->a(LX/0QB;)LX/2HH;

    move-result-object v0

    goto :goto_0

    .line 389049
    :pswitch_3
    new-instance v4, LX/2HT;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageManager;

    invoke-static {p1}, LX/1sb;->b(LX/0QB;)LX/1sd;

    move-result-object v2

    check-cast v2, LX/1sd;

    .line 389050
    new-instance p2, LX/2HV;

    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p1}, LX/1sb;->b(LX/0QB;)LX/1sd;

    move-result-object v5

    check-cast v5, LX/1sd;

    invoke-static {p1}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object p0

    check-cast p0, LX/0WJ;

    invoke-direct {p2, v3, v5, p0}, LX/2HV;-><init>(Landroid/content/Context;LX/1sd;LX/0WJ;)V

    .line 389051
    move-object v3, p2

    .line 389052
    check-cast v3, LX/2HV;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2HT;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/1sd;LX/2HV;)V

    .line 389053
    move-object v0, v4

    .line 389054
    goto :goto_0

    .line 389055
    :pswitch_4
    invoke-static {p1}, LX/2HZ;->a(LX/0QB;)LX/2HZ;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 389042
    const/4 v0, 0x5

    return v0
.end method
