.class public final LX/23M;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private b:LX/35y;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 364012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 364013
    iput-object p1, p0, LX/23M;->a:Landroid/content/Context;

    .line 364014
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 364015
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/23M;->b:LX/35y;

    if-nez v0, :cond_0

    .line 364016
    new-instance v0, LX/35y;

    iget-object v1, p0, LX/23M;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/35y;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/23M;->b:LX/35y;

    .line 364017
    :cond_0
    iget-object v0, p0, LX/23M;->b:LX/35y;

    invoke-virtual {v0}, LX/35y;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 364018
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
