.class public LX/3Ge;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field public l:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 541554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 541555
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Ge;->l:Z

    return-void
.end method

.method private static c(LX/2pa;)LX/3J8;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 541556
    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-nez v0, :cond_0

    .line 541557
    sget-object v0, LX/3J8;->UNKNOWN_VIDEO:LX/3J8;

    .line 541558
    :goto_0
    return-object v0

    .line 541559
    :cond_0
    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->x:Lcom/facebook/spherical/model/SphericalVideoParams;

    if-eqz v0, :cond_3

    .line 541560
    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541561
    sget-object v0, LX/3J8;->LIVE_360_VIDEO:LX/3J8;

    goto :goto_0

    .line 541562
    :cond_1
    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 541563
    sget-object v0, LX/3J8;->PREVIOUSLY_LIVE_360_VIDEO:LX/3J8;

    goto :goto_0

    .line 541564
    :cond_2
    sget-object v0, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    goto :goto_0

    .line 541565
    :cond_3
    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v0, :cond_4

    .line 541566
    sget-object v0, LX/3J8;->LIVE_VIDEO:LX/3J8;

    goto :goto_0

    .line 541567
    :cond_4
    invoke-static {p0}, LX/393;->m(LX/2pa;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 541568
    sget-object v0, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    goto :goto_0

    .line 541569
    :cond_5
    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->j:Z

    if-eqz v0, :cond_6

    .line 541570
    sget-object v0, LX/3J8;->ANIMATED_GIF_VIDEO:LX/3J8;

    goto :goto_0

    .line 541571
    :cond_6
    iget-object v0, p0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->k:Z

    if-eqz v0, :cond_7

    .line 541572
    sget-object v0, LX/3J8;->PURPLE_RAIN_VIDEO:LX/3J8;

    goto :goto_0

    .line 541573
    :cond_7
    sget-object v0, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    goto :goto_0
.end method

.method private l()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541574
    iget-object v0, p0, LX/3Ge;->b:LX/0Px;

    if-nez v0, :cond_0

    .line 541575
    invoke-virtual {p0}, LX/3Ge;->c()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3Ge;->b:LX/0Px;

    .line 541576
    :cond_0
    iget-object v0, p0, LX/3Ge;->b:LX/0Px;

    return-object v0
.end method

.method private static m(LX/3Ge;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541577
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    if-nez v0, :cond_0

    .line 541578
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-direct {p0}, LX/3Ge;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {p0}, LX/3Ge;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3Ge;->c:LX/0Px;

    .line 541579
    :cond_0
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    return-object v0
.end method

.method private static n(LX/3Ge;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541580
    iget-object v0, p0, LX/3Ge;->d:LX/0Px;

    if-nez v0, :cond_0

    .line 541581
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-direct {p0}, LX/3Ge;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {p0}, LX/3Ge;->f()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3Ge;->d:LX/0Px;

    .line 541582
    :cond_0
    iget-object v0, p0, LX/3Ge;->d:LX/0Px;

    return-object v0
.end method

.method private static o(LX/3Ge;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541583
    iget-object v0, p0, LX/3Ge;->f:LX/0Px;

    if-nez v0, :cond_0

    .line 541584
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-direct {p0}, LX/3Ge;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {p0}, LX/3Ge;->g()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3Ge;->f:LX/0Px;

    .line 541585
    :cond_0
    iget-object v0, p0, LX/3Ge;->f:LX/0Px;

    return-object v0
.end method

.method private static p(LX/3Ge;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541636
    iget-object v0, p0, LX/3Ge;->g:LX/0Px;

    if-nez v0, :cond_0

    .line 541637
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-virtual {p0}, LX/3Ge;->h()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3Ge;->g:LX/0Px;

    .line 541638
    :cond_0
    iget-object v0, p0, LX/3Ge;->g:LX/0Px;

    return-object v0
.end method

.method private static q(LX/3Ge;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541586
    iget-object v0, p0, LX/3Ge;->i:LX/0Px;

    if-nez v0, :cond_0

    .line 541587
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-direct {p0}, LX/3Ge;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {p0}, LX/3Ge;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3Ge;->i:LX/0Px;

    .line 541588
    :cond_0
    iget-object v0, p0, LX/3Ge;->i:LX/0Px;

    return-object v0
.end method

.method private static r(LX/3Ge;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541589
    iget-object v0, p0, LX/3Ge;->j:LX/0Px;

    if-nez v0, :cond_0

    .line 541590
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-direct {p0}, LX/3Ge;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {p0}, LX/3Ge;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3Ge;->j:LX/0Px;

    .line 541591
    :cond_0
    iget-object v0, p0, LX/3Ge;->j:LX/0Px;

    return-object v0
.end method

.method private static s(LX/3Ge;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541592
    iget-object v0, p0, LX/3Ge;->h:LX/0Px;

    if-nez v0, :cond_0

    .line 541593
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-direct {p0}, LX/3Ge;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {p0}, LX/3Ge;->i()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3Ge;->h:LX/0Px;

    .line 541594
    :cond_0
    iget-object v0, p0, LX/3Ge;->h:LX/0Px;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 541595
    const-class v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 541596
    sget-object v0, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    .line 541597
    :goto_0
    return-object v0

    .line 541598
    :cond_0
    const-class v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 541599
    sget-object v0, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    goto :goto_0

    .line 541600
    :cond_1
    sget-object v0, LX/3J8;->UNKNOWN_VIDEO:LX/3J8;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;
    .locals 3
    .param p3    # LX/7Lf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 541601
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541602
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541603
    invoke-static {p2}, LX/3Ge;->c(LX/2pa;)LX/3J8;

    move-result-object v0

    .line 541604
    iget-boolean v1, p0, LX/3Ge;->l:Z

    if-nez v1, :cond_1

    invoke-virtual {p0, p1, v0}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/3J8;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 541605
    :cond_0
    :goto_0
    return-object p1

    .line 541606
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 541607
    if-eqz v1, :cond_0

    .line 541608
    iget-object v2, p0, LX/3Ge;->a:LX/0Px;

    if-nez v2, :cond_2

    .line 541609
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 541610
    iput-object v2, p0, LX/3Ge;->a:LX/0Px;

    .line 541611
    :cond_2
    iget-object v2, p0, LX/3Ge;->a:LX/0Px;

    invoke-virtual {p1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(Ljava/util/List;)Ljava/util/List;

    .line 541612
    iget-boolean v2, p0, LX/3Ge;->k:Z

    if-eqz v2, :cond_3

    .line 541613
    new-instance v2, LX/7MZ;

    invoke-direct {v2, v1}, LX/7MZ;-><init>(Landroid/content/Context;)V

    .line 541614
    invoke-static {p1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 541615
    :cond_3
    sget-object v1, LX/3JA;->a:[I

    invoke-virtual {v0}, LX/3J8;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 541616
    :goto_1
    invoke-virtual {p1, p3}, Lcom/facebook/video/player/RichVideoPlayer;->setPluginEnvironment(LX/7Lf;)V

    goto :goto_0

    .line 541617
    :pswitch_0
    iget-object v0, p0, LX/3Ge;->g:LX/0Px;

    if-eqz v0, :cond_4

    .line 541618
    iget-object v0, p0, LX/3Ge;->g:LX/0Px;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541619
    :cond_4
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541620
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541621
    :pswitch_1
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541622
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541623
    :pswitch_2
    iget-object v0, p0, LX/3Ge;->d:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541624
    iget-object v0, p0, LX/3Ge;->d:LX/0Px;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541625
    :pswitch_3
    iget-object v0, p0, LX/3Ge;->e:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541626
    iget-object v0, p0, LX/3Ge;->e:LX/0Px;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541627
    :pswitch_4
    iget-object v0, p0, LX/3Ge;->f:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541628
    iget-object v0, p0, LX/3Ge;->f:LX/0Px;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541629
    :pswitch_5
    iget-object v0, p0, LX/3Ge;->h:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541630
    iget-object v0, p0, LX/3Ge;->h:LX/0Px;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541631
    :pswitch_6
    iget-object v0, p0, LX/3Ge;->i:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541632
    iget-object v0, p0, LX/3Ge;->i:LX/0Px;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541633
    :pswitch_7
    iget-object v0, p0, LX/3Ge;->j:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541634
    iget-object v0, p0, LX/3Ge;->j:LX/0Px;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541635
    :pswitch_8
    const/4 p1, 0x0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_5
        :pswitch_8
    .end packed-switch
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 541525
    invoke-virtual {p0}, LX/3Ge;->b()LX/0Px;

    .line 541526
    invoke-static {p0}, LX/3Ge;->o(LX/3Ge;)LX/0Px;

    .line 541527
    invoke-static {p0}, LX/3Ge;->n(LX/3Ge;)LX/0Px;

    .line 541528
    invoke-static {p0}, LX/3Ge;->m(LX/3Ge;)LX/0Px;

    .line 541529
    invoke-static {p0}, LX/3Ge;->s(LX/3Ge;)LX/0Px;

    .line 541530
    invoke-static {p0}, LX/3Ge;->p(LX/3Ge;)LX/0Px;

    .line 541531
    invoke-static {p0}, LX/3Ge;->q(LX/3Ge;)LX/0Px;

    .line 541532
    invoke-static {p0}, LX/3Ge;->r(LX/3Ge;)LX/0Px;

    .line 541533
    return-void
.end method

.method public final a(LX/2pa;)V
    .locals 2

    .prologue
    .line 541534
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541535
    invoke-static {p1}, LX/3Ge;->c(LX/2pa;)LX/3J8;

    move-result-object v0

    .line 541536
    sget-object v1, LX/3JA;->a:[I

    invoke-virtual {v0}, LX/3J8;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 541537
    :goto_0
    return-void

    .line 541538
    :pswitch_0
    invoke-static {p0}, LX/3Ge;->m(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541539
    invoke-static {p0}, LX/3Ge;->m(LX/3Ge;)LX/0Px;

    goto :goto_0

    .line 541540
    :pswitch_1
    invoke-static {p0}, LX/3Ge;->n(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541541
    invoke-static {p0}, LX/3Ge;->n(LX/3Ge;)LX/0Px;

    goto :goto_0

    .line 541542
    :pswitch_2
    invoke-virtual {p0}, LX/3Ge;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541543
    invoke-virtual {p0}, LX/3Ge;->b()LX/0Px;

    goto :goto_0

    .line 541544
    :pswitch_3
    invoke-static {p0}, LX/3Ge;->o(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541545
    invoke-static {p0}, LX/3Ge;->o(LX/3Ge;)LX/0Px;

    .line 541546
    :pswitch_4
    invoke-static {p0}, LX/3Ge;->q(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541547
    invoke-static {p0}, LX/3Ge;->q(LX/3Ge;)LX/0Px;

    goto :goto_0

    .line 541548
    :pswitch_5
    invoke-static {p0}, LX/3Ge;->r(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541549
    invoke-static {p0}, LX/3Ge;->r(LX/3Ge;)LX/0Px;

    goto :goto_0

    .line 541550
    :pswitch_6
    invoke-static {p0}, LX/3Ge;->p(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541551
    invoke-static {p0}, LX/3Ge;->p(LX/3Ge;)LX/0Px;

    goto :goto_0

    .line 541552
    :pswitch_7
    invoke-static {p0}, LX/3Ge;->s(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541553
    invoke-static {p0}, LX/3Ge;->s(LX/3Ge;)LX/0Px;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public a(Lcom/facebook/video/player/RichVideoPlayer;LX/3J8;)Z
    .locals 1

    .prologue
    .line 541440
    invoke-virtual {p0, p1}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;

    move-result-object v0

    .line 541441
    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541442
    iget-object v0, p0, LX/3Ge;->e:LX/0Px;

    if-nez v0, :cond_0

    .line 541443
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-direct {p0}, LX/3Ge;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {p0}, LX/3Ge;->d()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3Ge;->e:LX/0Px;

    .line 541444
    :cond_0
    iget-object v0, p0, LX/3Ge;->e:LX/0Px;

    return-object v0
.end method

.method public final b(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;
    .locals 3
    .param p3    # LX/7Lf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 541445
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541446
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541447
    invoke-static {p2}, LX/3Ge;->c(LX/2pa;)LX/3J8;

    move-result-object v0

    .line 541448
    invoke-virtual {p0, p1, v0}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/3J8;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 541449
    :cond_0
    :goto_0
    return-object p1

    .line 541450
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 541451
    if-eqz v1, :cond_0

    .line 541452
    iget-object v2, p0, LX/3Ge;->a:LX/0Px;

    if-nez v2, :cond_2

    .line 541453
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, p0, LX/3Ge;->a:LX/0Px;

    .line 541454
    :cond_2
    iget-object v2, p0, LX/3Ge;->a:LX/0Px;

    invoke-virtual {p1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(Ljava/util/List;)Ljava/util/List;

    .line 541455
    iget-boolean v2, p0, LX/3Ge;->k:Z

    if-eqz v2, :cond_3

    .line 541456
    new-instance v2, LX/7MZ;

    invoke-direct {v2, v1}, LX/7MZ;-><init>(Landroid/content/Context;)V

    .line 541457
    invoke-static {p1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 541458
    :cond_3
    sget-object v1, LX/3JA;->a:[I

    invoke-virtual {v0}, LX/3J8;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 541459
    :goto_1
    invoke-virtual {p1, p3}, Lcom/facebook/video/player/RichVideoPlayer;->setPluginEnvironment(LX/7Lf;)V

    goto :goto_0

    .line 541460
    :pswitch_0
    invoke-static {p0}, LX/3Ge;->m(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541461
    invoke-static {p0}, LX/3Ge;->m(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541462
    :pswitch_1
    invoke-static {p0}, LX/3Ge;->n(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541463
    invoke-static {p0}, LX/3Ge;->n(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541464
    :pswitch_2
    invoke-virtual {p0}, LX/3Ge;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541465
    invoke-virtual {p0}, LX/3Ge;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541466
    :pswitch_3
    invoke-static {p0}, LX/3Ge;->o(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541467
    invoke-static {p0}, LX/3Ge;->o(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541468
    :pswitch_4
    invoke-static {p0}, LX/3Ge;->p(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541469
    invoke-static {p0}, LX/3Ge;->p(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541470
    :pswitch_5
    invoke-static {p0}, LX/3Ge;->s(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541471
    invoke-static {p0}, LX/3Ge;->s(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541472
    :pswitch_6
    invoke-static {p0}, LX/3Ge;->q(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541473
    invoke-static {p0}, LX/3Ge;->q(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541474
    :pswitch_7
    invoke-static {p0}, LX/3Ge;->r(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 541475
    invoke-static {p0}, LX/3Ge;->r(LX/3Ge;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    goto :goto_1

    .line 541476
    :pswitch_8
    const/4 p1, 0x0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_8
    .end packed-switch
.end method

.method public final b(LX/2pa;)V
    .locals 6

    .prologue
    .line 541477
    invoke-static {p1}, LX/3Ge;->c(LX/2pa;)LX/3J8;

    move-result-object v1

    .line 541478
    const/4 v0, 0x0

    .line 541479
    sget-object v2, LX/3JA;->a:[I

    invoke-virtual {v1}, LX/3J8;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    move-object v3, v0

    .line 541480
    :goto_0
    if-eqz v3, :cond_2

    .line 541481
    const/4 v0, 0x0

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    .line 541482
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 541483
    iget-boolean v1, v0, LX/2oy;->c:Z

    move v1, v1

    .line 541484
    if-eqz v1, :cond_0

    .line 541485
    invoke-virtual {v0}, LX/2oy;->im_()V

    .line 541486
    invoke-virtual {v0}, LX/2oy;->dJ_()V

    .line 541487
    invoke-virtual {v0}, LX/2oy;->b()Landroid/view/ViewGroup;

    move-result-object v1

    .line 541488
    instance-of v5, v1, Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v5, :cond_0

    .line 541489
    check-cast v1, Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2oy;)Z

    .line 541490
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 541491
    :pswitch_0
    iget-object v0, p0, LX/3Ge;->g:LX/0Px;

    if-eqz v0, :cond_1

    .line 541492
    iget-object v0, p0, LX/3Ge;->g:LX/0Px;

    move-object v3, v0

    goto :goto_0

    .line 541493
    :cond_1
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    move-object v3, v0

    .line 541494
    goto :goto_0

    .line 541495
    :pswitch_1
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    move-object v3, v0

    .line 541496
    goto :goto_0

    .line 541497
    :pswitch_2
    iget-object v0, p0, LX/3Ge;->d:LX/0Px;

    move-object v3, v0

    .line 541498
    goto :goto_0

    .line 541499
    :pswitch_3
    iget-object v0, p0, LX/3Ge;->e:LX/0Px;

    move-object v3, v0

    .line 541500
    goto :goto_0

    .line 541501
    :pswitch_4
    iget-object v0, p0, LX/3Ge;->f:LX/0Px;

    move-object v3, v0

    .line 541502
    goto :goto_0

    .line 541503
    :pswitch_5
    iget-object v0, p0, LX/3Ge;->h:LX/0Px;

    move-object v3, v0

    .line 541504
    goto :goto_0

    .line 541505
    :pswitch_6
    iget-object v0, p0, LX/3Ge;->i:LX/0Px;

    move-object v3, v0

    .line 541506
    goto :goto_0

    .line 541507
    :pswitch_7
    iget-object v0, p0, LX/3Ge;->j:LX/0Px;

    move-object v3, v0

    goto :goto_0

    .line 541508
    :cond_2
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541509
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 541510
    return-object v0
.end method

.method public d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541511
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 541512
    return-object v0
.end method

.method public e()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541513
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 541514
    return-object v0
.end method

.method public f()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541515
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 541516
    return-object v0
.end method

.method public g()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541517
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 541518
    return-object v0
.end method

.method public h()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541438
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 541439
    return-object v0
.end method

.method public i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541519
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 541520
    return-object v0
.end method

.method public j()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541521
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 541522
    return-object v0
.end method

.method public k()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541523
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 541524
    return-object v0
.end method
