.class public LX/2cE;
.super LX/0aB;
.source ""

# interfaces
.implements LX/2cF;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Pk;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Uh;

.field private final d:LX/0a8;

.field private final e:LX/2cG;

.field public f:Lcom/facebook/omnistore/Collection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/omnistore/CollectionName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 440483
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2cE;->h:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/0Or;LX/0Or;LX/0Uh;LX/0a8;LX/2cG;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2Pk;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/gk/store/GatekeeperListeners;",
            "LX/2cG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 440484
    invoke-direct {p0}, LX/0aB;-><init>()V

    .line 440485
    iput-object p1, p0, LX/2cE;->a:LX/0Or;

    .line 440486
    iput-object p2, p0, LX/2cE;->b:LX/0Or;

    .line 440487
    iput-object p3, p0, LX/2cE;->c:LX/0Uh;

    .line 440488
    iput-object p4, p0, LX/2cE;->d:LX/0a8;

    .line 440489
    iput-object p5, p0, LX/2cE;->e:LX/2cG;

    .line 440490
    return-void
.end method

.method public static a(LX/0QB;)LX/2cE;
    .locals 7

    .prologue
    .line 440491
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 440492
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 440493
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 440494
    if-nez v1, :cond_0

    .line 440495
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 440496
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 440497
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 440498
    sget-object v1, LX/2cE;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 440499
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 440500
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 440501
    :cond_1
    if-nez v1, :cond_4

    .line 440502
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 440503
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 440504
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/2cE;->b(LX/0QB;)LX/2cE;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 440505
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 440506
    if-nez v1, :cond_2

    .line 440507
    sget-object v0, LX/2cE;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cE;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 440508
    :goto_1
    if-eqz v0, :cond_3

    .line 440509
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 440510
    :goto_3
    check-cast v0, LX/2cE;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 440511
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 440512
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 440513
    :catchall_1
    move-exception v0

    .line 440514
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 440515
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 440516
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 440517
    :cond_2
    :try_start_8
    sget-object v0, LX/2cE;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cE;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/2cE;
    .locals 9

    .prologue
    .line 440518
    new-instance v0, LX/2cE;

    const/16 v1, 0xea6

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x15e8

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {p0}, LX/0Zn;->a(LX/0QB;)LX/0a8;

    move-result-object v4

    check-cast v4, LX/0a8;

    .line 440519
    new-instance v8, LX/2cG;

    invoke-static {p0}, LX/18A;->b(LX/0QB;)LX/18A;

    move-result-object v5

    check-cast v5, LX/18A;

    invoke-static {p0}, LX/1fP;->b(LX/0QB;)LX/1fP;

    move-result-object v6

    check-cast v6, LX/1fP;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-direct {v8, v5, v6, v7}, LX/2cG;-><init>(LX/18A;LX/1fP;LX/0Uh;)V

    .line 440520
    move-object v5, v8

    .line 440521
    check-cast v5, LX/2cG;

    invoke-direct/range {v0 .. v5}, LX/2cE;-><init>(LX/0Or;LX/0Or;LX/0Uh;LX/0a8;LX/2cG;)V

    .line 440522
    return-object v0
.end method


# virtual methods
.method public final a(LX/0Uh;I)V
    .locals 1

    .prologue
    .line 440523
    iget-object v0, p0, LX/2cE;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Pk;

    invoke-virtual {v0, p0}, LX/2Pk;->checkComponentSubscription(LX/2cF;)V

    .line 440524
    return-void
.end method

.method public final indexObject(Ljava/lang/String;Ljava/lang/String;Ljava/nio/ByteBuffer;)Lcom/facebook/omnistore/IndexedFields;
    .locals 1

    .prologue
    .line 440525
    new-instance v0, Lcom/facebook/omnistore/IndexedFields;

    invoke-direct {v0}, Lcom/facebook/omnistore/IndexedFields;-><init>()V

    return-object v0
.end method

.method public final onCollectionAvailable(Lcom/facebook/omnistore/Collection;)V
    .locals 0

    .prologue
    .line 440526
    iput-object p1, p0, LX/2cE;->f:Lcom/facebook/omnistore/Collection;

    .line 440527
    return-void
.end method

.method public final onCollectionInvalidated()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 440528
    iput-object v0, p0, LX/2cE;->f:Lcom/facebook/omnistore/Collection;

    .line 440529
    iput-object v0, p0, LX/2cE;->g:Lcom/facebook/omnistore/CollectionName;

    .line 440530
    return-void
.end method

.method public final onDeltasReceived(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/omnistore/Delta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 440531
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 440532
    iget-object v0, p0, LX/2cE;->e:LX/2cG;

    .line 440533
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 440534
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/omnistore/Delta;

    .line 440535
    iget-object v3, v0, LX/2cG;->b:LX/1fP;

    .line 440536
    if-eqz v1, :cond_1

    iget-object v4, v3, LX/1fP;->b:Ljava/util/Random;

    const/16 v5, 0x32

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    if-eqz v4, :cond_8

    .line 440537
    :cond_1
    :goto_1
    sget-object v3, Lcom/facebook/omnistore/Delta$Status;->PERSISTED_REMOTE:Lcom/facebook/omnistore/Delta$Status;

    .line 440538
    iget-object v4, v1, Lcom/facebook/omnistore/Delta;->mStatus:Lcom/facebook/omnistore/Delta$Status;

    move-object v4, v4

    .line 440539
    if-ne v3, v4, :cond_0

    .line 440540
    iget-object v3, v0, LX/2cG;->c:LX/0Uh;

    const/16 v4, 0x594

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-nez v3, :cond_3

    .line 440541
    iget-object v1, v0, LX/2cG;->b:LX/1fP;

    const-string v2, ""

    const-string v3, ""

    const-string v4, "User is not in the gk"

    invoke-virtual {v1, v2, v3, v4}, LX/1fP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 440542
    :cond_2
    :goto_2
    return-void

    .line 440543
    :cond_3
    iget-object v3, v1, Lcom/facebook/omnistore/Delta;->mPrimaryKey:Ljava/lang/String;

    move-object v3, v3

    .line 440544
    iget-object v4, v1, Lcom/facebook/omnistore/Delta;->mBlob:Ljava/nio/ByteBuffer;

    move-object v1, v4

    .line 440545
    new-instance v4, LX/55p;

    invoke-direct {v4}, LX/55p;-><init>()V

    .line 440546
    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v5

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result p1

    add-int/2addr v5, p1

    .line 440547
    iput v5, v4, LX/55p;->a:I

    iput-object v1, v4, LX/55p;->b:Ljava/nio/ByteBuffer;

    move-object v5, v4

    .line 440548
    move-object v4, v5

    .line 440549
    move-object v1, v4

    .line 440550
    if-nez v1, :cond_4

    .line 440551
    iget-object v1, v0, LX/2cG;->b:LX/1fP;

    const-string v2, ""

    const-string v3, ""

    const-string v4, "FeedUnit is null"

    invoke-virtual {v1, v2, v3, v4}, LX/1fP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 440552
    :cond_4
    const/4 v4, 0x6

    invoke-virtual {v1, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_9

    iget-object v5, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p1, v1, LX/0eW;->a:I

    add-int/2addr v4, p1

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_3
    move v4, v4

    .line 440553
    if-nez v4, :cond_6

    .line 440554
    iget-object v2, v0, LX/2cG;->a:LX/18A;

    invoke-virtual {v2, v3}, LX/18A;->a(Ljava/lang/String;)V

    .line 440555
    iget-object v2, v0, LX/2cG;->b:LX/1fP;

    .line 440556
    const/4 v4, 0x4

    invoke-virtual {v1, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_a

    iget v5, v1, LX/0eW;->a:I

    add-int/2addr v4, v5

    invoke-virtual {v1, v4}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v4

    :goto_4
    move-object v1, v4

    .line 440557
    iget-object v4, v2, LX/1fP;->a:LX/0Zb;

    const-string v5, "feed_omnistore_invalidation"

    const/4 v0, 0x0

    invoke-interface {v4, v5, v0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 440558
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 440559
    const-string v5, "status"

    const-string v0, "success"

    invoke-virtual {v4, v5, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 440560
    const-string v5, "dedup_key"

    invoke-virtual {v4, v5, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 440561
    const-string v5, "graphql_id"

    invoke-virtual {v4, v5, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 440562
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 440563
    :cond_5
    goto :goto_2

    .line 440564
    :cond_6
    invoke-virtual {v1}, LX/55p;->c()I

    invoke-virtual {v1}, LX/55p;->d()I

    .line 440565
    iget-object v4, v0, LX/2cG;->a:LX/18A;

    invoke-virtual {v1}, LX/55p;->c()I

    move-result v5

    invoke-virtual {v1}, LX/55p;->d()I

    move-result v1

    .line 440566
    iget-object v6, v4, LX/18A;->a:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1fI;

    .line 440567
    new-instance p1, LX/69T;

    iget-object p0, v6, LX/1fI;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1fM;

    new-instance v4, LX/69a;

    invoke-direct {v4, v3, v5, v1}, LX/69a;-><init>(Ljava/lang/String;II)V

    invoke-direct {p1, p0, v4}, LX/69T;-><init>(LX/1fM;LX/1fd;)V

    .line 440568
    iget-object p0, v6, LX/1fI;->b:LX/1fJ;

    invoke-virtual {p0, p1}, LX/1fJ;->a(LX/1fL;)V

    .line 440569
    goto :goto_5

    .line 440570
    :cond_7
    goto/16 :goto_0

    .line 440571
    :cond_8
    iget-object v4, v3, LX/1fP;->a:LX/0Zb;

    const-string v5, "feed_omnistore_delta_mod_50"

    const/4 p1, 0x0

    invoke-interface {v4, v5, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 440572
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 440573
    const-string v5, "dedup_key"

    .line 440574
    iget-object p1, v1, Lcom/facebook/omnistore/Delta;->mPrimaryKey:Ljava/lang/String;

    move-object p1, p1

    .line 440575
    invoke-virtual {v4, v5, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 440576
    const-string v5, "sort_key"

    .line 440577
    iget-object p1, v1, Lcom/facebook/omnistore/Delta;->mSortKey:Ljava/lang/String;

    move-object p1, p1

    .line 440578
    invoke-virtual {v4, v5, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 440579
    const-string v5, "status"

    .line 440580
    iget-object p1, v1, Lcom/facebook/omnistore/Delta;->mStatus:Lcom/facebook/omnistore/Delta$Status;

    move-object p1, p1

    .line 440581
    invoke-virtual {v4, v5, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 440582
    const-string v5, "reason"

    .line 440583
    iget-object p1, v1, Lcom/facebook/omnistore/Delta;->mCollectionName:Lcom/facebook/omnistore/CollectionName;

    move-object p1, p1

    .line 440584
    invoke-virtual {v4, v5, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 440585
    const-string v5, "type"

    .line 440586
    iget-object p1, v1, Lcom/facebook/omnistore/Delta;->mType:Lcom/facebook/omnistore/Delta$Type;

    move-object p1, p1

    .line 440587
    invoke-virtual {v4, v5, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 440588
    invoke-virtual {v4}, LX/0oG;->d()V

    goto/16 :goto_1

    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_4
.end method

.method public final provideSubscriptionInfo(Lcom/facebook/omnistore/Omnistore;)LX/2cJ;
    .locals 3

    .prologue
    .line 440589
    iget-object v0, p0, LX/2cE;->d:LX/0a8;

    const/16 v1, 0x594

    invoke-virtual {v0, p0, v1}, LX/0a8;->a(LX/0aB;I)V

    .line 440590
    iget-object v0, p0, LX/2cE;->c:LX/0Uh;

    const/16 v1, 0x594

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 440591
    sget-object v0, LX/2cJ;->IGNORED_INFO:LX/2cJ;

    move-object v0, v0

    .line 440592
    :goto_0
    return-object v0

    .line 440593
    :cond_0
    const-string v0, "fb4a_feed_updates"

    invoke-virtual {p1, v0}, Lcom/facebook/omnistore/Omnistore;->createCollectionNameBuilder(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v1

    iget-object v0, p0, LX/2cE;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/omnistore/CollectionName$Builder;->addSegment(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/omnistore/CollectionName$Builder;->addDeviceId()Lcom/facebook/omnistore/CollectionName$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/omnistore/CollectionName$Builder;->build()Lcom/facebook/omnistore/CollectionName;

    move-result-object v0

    .line 440594
    iput-object v0, p0, LX/2cE;->g:Lcom/facebook/omnistore/CollectionName;

    .line 440595
    new-instance v1, LX/2cX;

    invoke-direct {v1}, LX/2cX;-><init>()V

    const-string v2, ""

    .line 440596
    iput-object v2, v1, LX/2cX;->mCollectionParams:Ljava/lang/String;

    .line 440597
    move-object v1, v1

    .line 440598
    const-string v2, "namespace com.facebook.api.feedcache.omnistore.flatbuffer;\n\ntable FeedUnit {\n  graphqlid:string;\n  valid:int;\n  likes:int;\n  comments:int;\n}\n\nroot_type FeedUnit;\n"

    .line 440599
    iput-object v2, v1, LX/2cX;->mIdl:Ljava/lang/String;

    .line 440600
    move-object v1, v1

    .line 440601
    invoke-virtual {v1}, LX/2cX;->build()LX/2cY;

    move-result-object v1

    move-object v1, v1

    .line 440602
    invoke-static {v0, v1}, LX/2cJ;->forOpenSubscription(Lcom/facebook/omnistore/CollectionName;LX/2cY;)LX/2cJ;

    move-result-object v0

    goto :goto_0
.end method
