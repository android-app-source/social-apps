.class public LX/33u;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile q:LX/33u;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/fbreact/instance/FbReactInstanceHolderSpec;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Sh;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/0Uo;

.field private final f:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0ad;

.field public final j:LX/1LT;

.field public final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/fbreact/instance/FbReactInstanceHolder$ClearListener;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/33w;

.field private m:LX/340;

.field public n:LX/33y;

.field public o:Ljava/util/Timer;

.field public p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Sh;Lcom/facebook/content/SecureContextHelper;LX/0Uo;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1LT;LX/0ad;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/fbreact/instance/FbReactInstanceHolderSpec;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Uo;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/1LT;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494371
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/33u;->k:Ljava/util/Set;

    .line 494372
    new-instance v0, LX/33v;

    invoke-direct {v0, p0}, LX/33v;-><init>(LX/33u;)V

    iput-object v0, p0, LX/33u;->l:LX/33w;

    .line 494373
    iput-object p1, p0, LX/33u;->a:Landroid/content/Context;

    .line 494374
    iput-object p2, p0, LX/33u;->b:LX/0Ot;

    .line 494375
    iput-object p3, p0, LX/33u;->c:LX/0Sh;

    .line 494376
    iput-object p4, p0, LX/33u;->d:Lcom/facebook/content/SecureContextHelper;

    .line 494377
    iput-object p5, p0, LX/33u;->e:LX/0Uo;

    .line 494378
    iput-object p6, p0, LX/33u;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 494379
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/33u;->g:Ljava/util/Set;

    .line 494380
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/33u;->h:Ljava/util/Set;

    .line 494381
    iput-object p7, p0, LX/33u;->j:LX/1LT;

    .line 494382
    iput-object p8, p0, LX/33u;->i:LX/0ad;

    .line 494383
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, LX/33u;->o:Ljava/util/Timer;

    .line 494384
    iget-object v0, p0, LX/33u;->j:LX/1LT;

    new-instance p1, LX/33x;

    invoke-direct {p1, p0}, LX/33x;-><init>(LX/33u;)V

    .line 494385
    iput-object p1, v0, LX/1LT;->c:LX/0fl;

    .line 494386
    return-void
.end method

.method public static a(LX/0QB;)LX/33u;
    .locals 12

    .prologue
    .line 494391
    sget-object v0, LX/33u;->q:LX/33u;

    if-nez v0, :cond_1

    .line 494392
    const-class v1, LX/33u;

    monitor-enter v1

    .line 494393
    :try_start_0
    sget-object v0, LX/33u;->q:LX/33u;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 494394
    if-eqz v2, :cond_0

    .line 494395
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 494396
    new-instance v3, LX/33u;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x53b

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v8

    check-cast v8, LX/0Uo;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v9

    check-cast v9, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/1LT;->b(LX/0QB;)LX/1LT;

    move-result-object v10

    check-cast v10, LX/1LT;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v3 .. v11}, LX/33u;-><init>(Landroid/content/Context;LX/0Ot;LX/0Sh;Lcom/facebook/content/SecureContextHelper;LX/0Uo;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1LT;LX/0ad;)V

    .line 494397
    move-object v0, v3

    .line 494398
    sput-object v0, LX/33u;->q:LX/33u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 494399
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 494400
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 494401
    :cond_1
    sget-object v0, LX/33u;->q:LX/33u;

    return-object v0

    .line 494402
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 494403
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static g$redex0(LX/33u;)V
    .locals 1

    .prologue
    .line 494387
    iget-object v0, p0, LX/33u;->o:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 494388
    iget-object v0, p0, LX/33u;->o:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 494389
    const/4 v0, 0x0

    iput-object v0, p0, LX/33u;->o:Ljava/util/Timer;

    .line 494390
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 494404
    iget-object v0, p0, LX/33u;->n:LX/33y;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 494281
    iget-object v0, p0, LX/33u;->c:LX/0Sh;

    new-instance v1, Lcom/facebook/fbreact/instance/FbReactInstanceHolder$2;

    invoke-direct {v1, p0}, Lcom/facebook/fbreact/instance/FbReactInstanceHolder$2;-><init>(LX/33u;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 494282
    return-void
.end method

.method public final b(LX/33z;)V
    .locals 1

    .prologue
    .line 494283
    iget-object v0, p0, LX/33u;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 494284
    return-void
.end method

.method public final c()LX/33y;
    .locals 10

    .prologue
    const v2, 0x77000f

    const v6, 0x77000e

    const v5, 0x77000d

    const/4 v4, 0x2

    .line 494285
    iget-object v0, p0, LX/33u;->n:LX/33y;

    if-nez v0, :cond_5

    .line 494286
    iget-object v0, p0, LX/33u;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 494287
    iget-object v0, p0, LX/33u;->m:LX/340;

    if-nez v0, :cond_0

    .line 494288
    iget-object v0, p0, LX/33u;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 494289
    iget-object v0, p0, LX/33u;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/340;

    iput-object v0, p0, LX/33u;->m:LX/340;

    .line 494290
    const-string v0, "Your app must implement it\'s own FbReactInstanceHolderSpec."

    iget-object v1, p0, LX/33u;->m:LX/340;

    invoke-static {v0, v1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 494291
    iget-object v0, p0, LX/33u;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 494292
    :cond_0
    iget-object v0, p0, LX/33u;->i:LX/0ad;

    sget-short v1, LX/0ds;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 494293
    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/33u;->p:Z

    if-eqz v0, :cond_6

    .line 494294
    :cond_1
    :goto_0
    sget-boolean v1, LX/0AN;->a:Z

    if-nez v1, :cond_2

    sget-boolean v1, LX/0AN;->b:Z

    if-eqz v1, :cond_7

    :cond_2
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 494295
    if-eqz v1, :cond_3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v0, v2, :cond_3

    .line 494296
    iget-object v0, p0, LX/33u;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 494297
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.settings.action.MANAGE_OVERLAY_PERMISSION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 494298
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 494299
    iget-object v2, p0, LX/33u;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/33u;->a:Landroid/content/Context;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 494300
    iget-object v0, p0, LX/33u;->a:Landroid/content/Context;

    const-string v2, "Overlay permissions needs to be granted in order for react native apps to run in dev mode."

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 494301
    :cond_3
    iget-object v0, p0, LX/33u;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 494302
    invoke-static {}, LX/33y;->m()LX/2so;

    move-result-object v2

    iget-object v0, p0, LX/33u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    .line 494303
    iput-object v0, v2, LX/2so;->f:Landroid/app/Application;

    .line 494304
    move-object v0, v2

    .line 494305
    const-string v3, "RKJSModules/Apps/Fb4a/Fb4aBundle"

    move-object v2, v3

    .line 494306
    iput-object v2, v0, LX/2so;->d:Ljava/lang/String;

    .line 494307
    move-object v0, v0

    .line 494308
    sget-object v3, LX/341;->BEFORE_RESUME:LX/341;

    move-object v2, v3

    .line 494309
    iput-object v2, v0, LX/2so;->h:LX/341;

    .line 494310
    move-object v0, v0

    .line 494311
    iput-boolean v1, v0, LX/2so;->g:Z

    .line 494312
    move-object v0, v0

    .line 494313
    iget-object v1, p0, LX/33u;->m:LX/340;

    .line 494314
    iget-object v2, v1, LX/340;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0o1;

    move-object v1, v2

    .line 494315
    iput-object v1, v0, LX/2so;->j:LX/0o1;

    .line 494316
    move-object v0, v0

    .line 494317
    iget-object v1, p0, LX/33u;->m:LX/340;

    .line 494318
    iget-object v2, v1, LX/340;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/342;

    move-object v1, v2

    .line 494319
    iput-object v1, v0, LX/2so;->k:LX/342;

    .line 494320
    move-object v0, v0

    .line 494321
    iget-object v1, p0, LX/33u;->m:LX/340;

    .line 494322
    iget-object v2, v1, LX/340;->f:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-short v7, LX/0ds;->f:S

    const/4 v8, 0x0

    invoke-interface {v2, v3, v7, v8}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 494323
    new-instance v2, LX/343;

    invoke-direct {v2}, LX/343;-><init>()V

    .line 494324
    :goto_2
    move-object v1, v2

    .line 494325
    iput-object v1, v0, LX/2so;->i:LX/344;

    .line 494326
    move-object v0, v0

    .line 494327
    iget-object v1, p0, LX/33u;->m:LX/340;

    .line 494328
    iget-object v2, v1, LX/340;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/345;

    move-object v1, v2

    .line 494329
    iput-object v1, v0, LX/2so;->n:LX/345;

    .line 494330
    move-object v0, v0

    .line 494331
    iget-object v1, p0, LX/33u;->m:LX/340;

    iget-object v2, p0, LX/33u;->a:Landroid/content/Context;

    .line 494332
    :try_start_0
    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    const-string v7, ""

    invoke-virtual {v3, v7}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const-string v7, "otaE2ETest.jsbundle"

    invoke-interface {v3, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 494333
    const-string v3, "otaE2ETest.jsbundle"

    invoke-static {v2, v3}, LX/346;->a(Landroid/content/Context;Ljava/lang/String;)LX/346;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 494334
    :goto_3
    move-object v1, v3

    .line 494335
    invoke-virtual {v0, v1}, LX/2so;->a(LX/346;)LX/2so;

    move-result-object v0

    iget-object v1, p0, LX/33u;->m:LX/340;

    .line 494336
    iget-object v2, v1, LX/340;->f:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-short v7, LX/347;->G:S

    const/4 v8, 0x0

    invoke-interface {v2, v3, v7, v8}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    move v1, v2

    .line 494337
    iput-boolean v1, v0, LX/2so;->o:Z

    .line 494338
    move-object v0, v0

    .line 494339
    const/4 v2, 0x0

    move v1, v2

    .line 494340
    iput-boolean v1, v0, LX/2so;->p:Z

    .line 494341
    move-object v1, v0

    .line 494342
    iget-object v0, p0, LX/33u;->m:LX/340;

    .line 494343
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 494344
    iget-object v3, v0, LX/340;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494345
    iget-object v3, v0, LX/340;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494346
    move-object v0, v2

    .line 494347
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/348;

    .line 494348
    invoke-virtual {v1, v0}, LX/2so;->a(LX/348;)LX/2so;

    goto :goto_4

    .line 494349
    :cond_4
    invoke-virtual {v1}, LX/2so;->a()LX/33y;

    move-result-object v0

    iput-object v0, p0, LX/33u;->n:LX/33y;

    .line 494350
    iget-object v0, p0, LX/33u;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 494351
    iget-object v0, p0, LX/33u;->n:LX/33y;

    invoke-virtual {v0}, LX/33y;->b()LX/349;

    move-result-object v0

    iget-object v1, p0, LX/33u;->l:LX/33w;

    invoke-virtual {v0, v1}, LX/349;->a(LX/33w;)V

    .line 494352
    iget-object v0, p0, LX/33u;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v5, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 494353
    :cond_5
    iget-object v0, p0, LX/33u;->n:LX/33y;

    return-object v0

    .line 494354
    :cond_6
    iget-object v0, p0, LX/33u;->j:LX/1LT;

    invoke-virtual {v0}, LX/1LT;->a()V

    .line 494355
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/33u;->p:Z

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 494356
    :catch_0
    :cond_9
    iget-object v3, v1, LX/340;->g:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/34A;

    .line 494357
    invoke-static {v3}, LX/34B;->b(LX/34B;)LX/2sp;

    move-result-object v7

    .line 494358
    if-nez v7, :cond_b

    .line 494359
    const/4 v7, 0x0

    .line 494360
    :goto_5
    move-object v3, v7

    .line 494361
    iget-object v7, v1, LX/340;->f:LX/0ad;

    sget-short v8, LX/0ds;->a:S

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v7

    move v7, v7

    .line 494362
    if-nez v7, :cond_a

    if-eqz v3, :cond_a

    .line 494363
    invoke-static {v3}, LX/346;->a(Ljava/lang/String;)LX/346;

    move-result-object v3

    goto/16 :goto_3

    .line 494364
    :cond_a
    const-string v3, "assets://Fb4aBundle.js"

    invoke-static {v2, v3}, LX/346;->a(Landroid/content/Context;Ljava/lang/String;)LX/346;

    move-result-object v3

    goto/16 :goto_3

    :cond_b
    const-string v8, "main.jsbundle"

    .line 494365
    new-instance v9, Ljava/io/File;

    iget-object v3, v7, LX/2sp;->b:Ljava/io/File;

    invoke-direct {v9, v3, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 494366
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 494367
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    .line 494368
    :goto_6
    move-object v7, v9

    .line 494369
    goto :goto_5

    :cond_c
    const/4 v9, 0x0

    goto :goto_6
.end method
