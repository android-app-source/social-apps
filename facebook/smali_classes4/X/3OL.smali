.class public final enum LX/3OL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3OL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3OL;

.field public static final enum INTERACTED:LX/3OL;

.field public static final enum NOT_AVAILABLE:LX/3OL;

.field public static final enum NOT_SENT:LX/3OL;

.field public static final enum RECEIVED:LX/3OL;

.field public static final enum SENT_UNDOABLE:LX/3OL;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 560402
    new-instance v0, LX/3OL;

    const-string v1, "INTERACTED"

    invoke-direct {v0, v1, v2}, LX/3OL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OL;->INTERACTED:LX/3OL;

    .line 560403
    new-instance v0, LX/3OL;

    const-string v1, "NOT_AVAILABLE"

    invoke-direct {v0, v1, v3}, LX/3OL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    .line 560404
    new-instance v0, LX/3OL;

    const-string v1, "NOT_SENT"

    invoke-direct {v0, v1, v4}, LX/3OL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OL;->NOT_SENT:LX/3OL;

    .line 560405
    new-instance v0, LX/3OL;

    const-string v1, "RECEIVED"

    invoke-direct {v0, v1, v5}, LX/3OL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OL;->RECEIVED:LX/3OL;

    .line 560406
    new-instance v0, LX/3OL;

    const-string v1, "SENT_UNDOABLE"

    invoke-direct {v0, v1, v6}, LX/3OL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OL;->SENT_UNDOABLE:LX/3OL;

    .line 560407
    const/4 v0, 0x5

    new-array v0, v0, [LX/3OL;

    sget-object v1, LX/3OL;->INTERACTED:LX/3OL;

    aput-object v1, v0, v2

    sget-object v1, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    aput-object v1, v0, v3

    sget-object v1, LX/3OL;->NOT_SENT:LX/3OL;

    aput-object v1, v0, v4

    sget-object v1, LX/3OL;->RECEIVED:LX/3OL;

    aput-object v1, v0, v5

    sget-object v1, LX/3OL;->SENT_UNDOABLE:LX/3OL;

    aput-object v1, v0, v6

    sput-object v0, LX/3OL;->$VALUES:[LX/3OL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560408
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3OL;
    .locals 1

    .prologue
    .line 560409
    const-class v0, LX/3OL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3OL;

    return-object v0
.end method

.method public static values()[LX/3OL;
    .locals 1

    .prologue
    .line 560410
    sget-object v0, LX/3OL;->$VALUES:[LX/3OL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3OL;

    return-object v0
.end method
