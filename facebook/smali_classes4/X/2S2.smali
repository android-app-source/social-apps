.class public final LX/2S2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/2S0;


# direct methods
.method public constructor <init>(LX/2S0;)V
    .locals 0

    .prologue
    .line 410472
    iput-object p1, p0, LX/2S2;->a:LX/2S0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x64d958a9

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 410473
    iget-object v2, p0, LX/2S2;->a:LX/2S0;

    monitor-enter v2

    .line 410474
    :try_start_0
    iget-object v0, p0, LX/2S2;->a:LX/2S0;

    .line 410475
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 410476
    const-string v4, "extra_download_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 410477
    invoke-static {v0, v3, v4}, LX/2S0;->a(LX/2S0;J)V

    .line 410478
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410479
    const v0, -0x243fbdd

    invoke-static {v0, v1}, LX/02F;->e(II)V

    return-void

    .line 410480
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const v2, -0x43487c83

    invoke-static {v2, v1}, LX/02F;->e(II)V

    throw v0
.end method
