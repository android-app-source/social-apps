.class public LX/2VN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;",
        "Lcom/facebook/abtest/qe/protocol/sync/user/SyncMultiQuickExperimentUserInfoResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/0Tn;


# instance fields
.field private final b:LX/0Uh;

.field private final c:LX/0WJ;

.field private final d:LX/2VO;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private f:Ljava/lang/String;

.field private g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 417632
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "qe_last_fetch_ids_hash"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2VN;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/0WJ;LX/2VO;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 417633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417634
    iput-object p1, p0, LX/2VN;->b:LX/0Uh;

    .line 417635
    iput-object p2, p0, LX/2VN;->c:LX/0WJ;

    .line 417636
    iput-object p3, p0, LX/2VN;->d:LX/2VO;

    .line 417637
    iput-object p4, p0, LX/2VN;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 417638
    return-void
.end method

.method public static a(LX/0QB;)LX/2VN;
    .locals 5

    .prologue
    .line 417539
    new-instance v4, LX/2VN;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v1

    check-cast v1, LX/0WJ;

    .line 417540
    new-instance v2, LX/2VO;

    invoke-direct {v2}, LX/2VO;-><init>()V

    .line 417541
    move-object v2, v2

    .line 417542
    move-object v2, v2

    .line 417543
    check-cast v2, LX/2VO;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2VN;-><init>(LX/0Uh;LX/0WJ;LX/2VO;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 417544
    move-object v0, v4

    .line 417545
    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 417628
    iget-object v0, p0, LX/2VN;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 417629
    sget-object v1, LX/2VN;->a:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 417630
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 417631
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 12

    .prologue
    .line 417566
    check-cast p1, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;

    const/4 v1, 0x0

    .line 417567
    const-string v0, ""

    iput-object v0, p0, LX/2VN;->f:Ljava/lang/String;

    .line 417568
    iget-boolean v0, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->c:Z

    move v0, v0

    .line 417569
    iput-boolean v0, p0, LX/2VN;->g:Z

    .line 417570
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 417571
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v4, "json"

    invoke-direct {v0, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417572
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "member"

    .line 417573
    iget-object v4, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->b:Ljava/lang/String;

    move-object v4, v4

    .line 417574
    invoke-direct {v0, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417575
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 417576
    new-instance v5, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v0}, LX/162;-><init>(LX/0mC;)V

    .line 417577
    iget-object v0, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->a:LX/0Px;

    move-object v6, v0

    .line 417578
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v2, v1

    .line 417579
    :goto_0
    if-ge v2, v7, :cond_0

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;

    .line 417580
    iget-object v8, v0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->a:Ljava/lang/String;

    move-object v8, v8

    .line 417581
    invoke-static {v8}, LX/2VI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 417582
    iget-object v8, v0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->b:Ljava/lang/String;

    move-object v0, v8

    .line 417583
    invoke-virtual {v5, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 417584
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 417585
    :cond_0
    iget-object v0, p0, LX/2VN;->b:LX/0Uh;

    const/16 v2, 0x5d5

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/2VN;->g:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/2VN;->c:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/2VN;->c:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->d()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 417586
    :goto_1
    if-eqz v0, :cond_5

    .line 417587
    iget-object v0, p0, LX/2VN;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2VN;->a:LX/0Tn;

    const-string v6, ""

    invoke-interface {v0, v2, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 417588
    invoke-virtual {v4}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    .line 417589
    :try_start_0
    const-string v6, "SHA-1"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v6

    .line 417590
    const-string v7, "UTF-8"

    invoke-virtual {v2, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    .line 417591
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 417592
    array-length v9, v7

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v9, :cond_1

    aget-byte v10, v7, v6

    .line 417593
    const-string v11, "%02X"

    invoke-static {v10}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v10

    invoke-static {v11, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417594
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 417595
    :cond_1
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 417596
    :goto_3
    move-object v2, v6

    .line 417597
    iput-object v2, p0, LX/2VN;->f:Ljava/lang/String;

    .line 417598
    iget-object v2, p0, LX/2VN;->f:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 417599
    iget-object v1, p0, LX/2VN;->f:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 417600
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "quick_experiment_ids_hash"

    iget-object v6, p0, LX/2VN;->f:Ljava/lang/String;

    invoke-direct {v1, v2, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417601
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "suppress_http_code"

    const-string v6, ""

    invoke-direct {v1, v2, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417602
    :goto_4
    if-nez v0, :cond_2

    .line 417603
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "quick_experiment_ids"

    invoke-virtual {v4}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417604
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "quick_experiment_hashes"

    invoke-virtual {v5}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417605
    iget-boolean v0, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->c:Z

    move v0, v0

    .line 417606
    if-eqz v0, :cond_4

    .line 417607
    const-string v0, "/sessionless_test_experiment_members"

    move-object v0, v0

    .line 417608
    :goto_5
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "syncUserQE"

    .line 417609
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 417610
    move-object v1, v1

    .line 417611
    const-string v2, "GET"

    .line 417612
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 417613
    move-object v1, v1

    .line 417614
    iput-object v0, v1, LX/14O;->d:Ljava/lang/String;

    .line 417615
    move-object v0, v1

    .line 417616
    iput-object v3, v0, LX/14O;->g:Ljava/util/List;

    .line 417617
    move-object v0, v0

    .line 417618
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 417619
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 417620
    move-object v0, v0

    .line 417621
    sget-object v1, LX/14Q;->FALLBACK_REQUIRED:LX/14Q;

    .line 417622
    iput-object v1, v0, LX/14O;->v:LX/14Q;

    .line 417623
    move-object v0, v0

    .line 417624
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    .line 417625
    goto/16 :goto_1

    .line 417626
    :cond_4
    const-string v0, "/test_experiment_members"

    move-object v0, v0

    .line 417627
    goto :goto_5

    :cond_5
    move v0, v1

    goto :goto_4

    :catch_0
    const-string v6, ""

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 417546
    check-cast p1, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;

    .line 417547
    :try_start_0
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 417548
    iget-boolean v1, p0, LX/2VN;->g:Z

    if-nez v1, :cond_0

    .line 417549
    iget-object v1, p0, LX/2VN;->f:Ljava/lang/String;

    invoke-direct {p0, v1}, LX/2VN;->b(Ljava/lang/String;)V

    .line 417550
    :cond_0
    if-nez v0, :cond_2

    .line 417551
    const/4 v1, 0x0

    .line 417552
    :goto_0
    move-object v0, v1

    .line 417553
    return-object v0

    .line 417554
    :catch_0
    move-exception v0

    .line 417555
    iget-boolean v1, p0, LX/2VN;->g:Z

    if-nez v1, :cond_1

    .line 417556
    const-string v1, ""

    invoke-direct {p0, v1}, LX/2VN;->b(Ljava/lang/String;)V

    .line 417557
    :cond_1
    throw v0

    .line 417558
    :cond_2
    new-instance v3, LX/2WN;

    invoke-direct {v3}, LX/2WN;-><init>()V

    .line 417559
    iget-object v1, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->a:LX/0Px;

    move-object p0, v1

    .line 417560
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 417561
    invoke-virtual {v0, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object p2

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;

    invoke-static {p2, v1}, LX/2VO;->a(LX/0lF;Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;)Lcom/facebook/abtest/qe/protocol/sync/user/SyncQuickExperimentUserInfoResult;

    move-result-object v1

    .line 417562
    iget-object p2, v3, LX/2WN;->a:Ljava/util/ArrayList;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417563
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 417564
    :cond_3
    new-instance v1, Lcom/facebook/abtest/qe/protocol/sync/user/SyncMultiQuickExperimentUserInfoResult;

    invoke-direct {v1, v3}, Lcom/facebook/abtest/qe/protocol/sync/user/SyncMultiQuickExperimentUserInfoResult;-><init>(LX/2WN;)V

    move-object v1, v1

    .line 417565
    goto :goto_0
.end method
