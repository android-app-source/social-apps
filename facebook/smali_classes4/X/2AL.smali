.class public final enum LX/2AL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2AL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2AL;

.field public static final enum MQTT_CONNECTED_WAITING_FOR_PRESENCE:LX/2AL;

.field public static final enum MQTT_DISCONNECTED:LX/2AL;

.field public static final enum PRESENCE_MAP_RECEIVED:LX/2AL;

.field public static final enum TP_DISABLED:LX/2AL;

.field public static final enum TP_FULL_LIST_RECEIVED:LX/2AL;

.field public static final enum TP_WAITING_FOR_FULL_LIST:LX/2AL;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 377270
    new-instance v0, LX/2AL;

    const-string v1, "MQTT_DISCONNECTED"

    invoke-direct {v0, v1, v3}, LX/2AL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2AL;->MQTT_DISCONNECTED:LX/2AL;

    .line 377271
    new-instance v0, LX/2AL;

    const-string v1, "MQTT_CONNECTED_WAITING_FOR_PRESENCE"

    invoke-direct {v0, v1, v4}, LX/2AL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2AL;->MQTT_CONNECTED_WAITING_FOR_PRESENCE:LX/2AL;

    .line 377272
    new-instance v0, LX/2AL;

    const-string v1, "PRESENCE_MAP_RECEIVED"

    invoke-direct {v0, v1, v5}, LX/2AL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2AL;->PRESENCE_MAP_RECEIVED:LX/2AL;

    .line 377273
    new-instance v0, LX/2AL;

    const-string v1, "TP_DISABLED"

    invoke-direct {v0, v1, v6}, LX/2AL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2AL;->TP_DISABLED:LX/2AL;

    .line 377274
    new-instance v0, LX/2AL;

    const-string v1, "TP_WAITING_FOR_FULL_LIST"

    invoke-direct {v0, v1, v7}, LX/2AL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2AL;->TP_WAITING_FOR_FULL_LIST:LX/2AL;

    .line 377275
    new-instance v0, LX/2AL;

    const-string v1, "TP_FULL_LIST_RECEIVED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/2AL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2AL;->TP_FULL_LIST_RECEIVED:LX/2AL;

    .line 377276
    const/4 v0, 0x6

    new-array v0, v0, [LX/2AL;

    sget-object v1, LX/2AL;->MQTT_DISCONNECTED:LX/2AL;

    aput-object v1, v0, v3

    sget-object v1, LX/2AL;->MQTT_CONNECTED_WAITING_FOR_PRESENCE:LX/2AL;

    aput-object v1, v0, v4

    sget-object v1, LX/2AL;->PRESENCE_MAP_RECEIVED:LX/2AL;

    aput-object v1, v0, v5

    sget-object v1, LX/2AL;->TP_DISABLED:LX/2AL;

    aput-object v1, v0, v6

    sget-object v1, LX/2AL;->TP_WAITING_FOR_FULL_LIST:LX/2AL;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2AL;->TP_FULL_LIST_RECEIVED:LX/2AL;

    aput-object v2, v0, v1

    sput-object v0, LX/2AL;->$VALUES:[LX/2AL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 377277
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2AL;
    .locals 1

    .prologue
    .line 377278
    const-class v0, LX/2AL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2AL;

    return-object v0
.end method

.method public static values()[LX/2AL;
    .locals 1

    .prologue
    .line 377279
    sget-object v0, LX/2AL;->$VALUES:[LX/2AL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2AL;

    return-object v0
.end method
