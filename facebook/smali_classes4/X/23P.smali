.class public LX/23P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/method/TransformationMethod;


# instance fields
.field private a:Z

.field private final b:Ljava/util/Locale;


# direct methods
.method public constructor <init>(LX/0W9;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 364157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 364158
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/23P;->a:Z

    .line 364159
    invoke-virtual {p1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, LX/23P;->b:Ljava/util/Locale;

    .line 364160
    invoke-virtual {p1}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 364161
    if-nez v0, :cond_0

    .line 364162
    :goto_0
    move v0, v1

    .line 364163
    iput-boolean v0, p0, LX/23P;->a:Z

    .line 364164
    return-void

    .line 364165
    :cond_0
    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move v1, v2

    .line 364166
    goto :goto_0

    .line 364167
    :sswitch_0
    const-string p1, "af_ZA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    move v3, v1

    goto :goto_1

    :sswitch_1
    const-string p1, "cs_CZ"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    move v3, v2

    goto :goto_1

    :sswitch_2
    const-string p1, "da_DK"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v3, 0x2

    goto :goto_1

    :sswitch_3
    const-string p1, "de_DE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v3, 0x3

    goto :goto_1

    :sswitch_4
    const-string p1, "el_GR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v3, 0x4

    goto :goto_1

    :sswitch_5
    const-string p1, "en_US"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v3, 0x5

    goto :goto_1

    :sswitch_6
    const-string p1, "en_GB"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v3, 0x6

    goto :goto_1

    :sswitch_7
    const-string p1, "es_ES"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v3, 0x7

    goto :goto_1

    :sswitch_8
    const-string p1, "es_LA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0x8

    goto :goto_1

    :sswitch_9
    const-string p1, "fr_FR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0x9

    goto :goto_1

    :sswitch_a
    const-string p1, "id_ID"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0xa

    goto :goto_1

    :sswitch_b
    const-string p1, "it_IT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0xb

    goto/16 :goto_1

    :sswitch_c
    const-string p1, "nb_NO"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0xc

    goto/16 :goto_1

    :sswitch_d
    const-string p1, "pt_BR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0xd

    goto/16 :goto_1

    :sswitch_e
    const-string p1, "pt_PT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0xe

    goto/16 :goto_1

    :sswitch_f
    const-string p1, "ru_RU"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0xf

    goto/16 :goto_1

    :sswitch_10
    const-string p1, "sk_SK"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0x10

    goto/16 :goto_1

    :sswitch_11
    const-string p1, "sv_SE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0x11

    goto/16 :goto_1

    :sswitch_12
    const-string p1, "tl_PH"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0x12

    goto/16 :goto_1

    :sswitch_13
    const-string p1, "tr_TR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0x13

    goto/16 :goto_1

    :sswitch_14
    const-string p1, "vi_VN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v3, 0x14

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x586b581 -> :sswitch_0
        0x5a8caa6 -> :sswitch_1
        0x5aeb389 -> :sswitch_2
        0x5b084ff -> :sswitch_3
        0x5c1cb83 -> :sswitch_4
        0x5c2b431 -> :sswitch_6
        0x5c2b5f4 -> :sswitch_5
        0x5c4f9df -> :sswitch_7
        0x5c4faa6 -> :sswitch_8
        0x5d29d1f -> :sswitch_9
        0x5f686bf -> :sswitch_a
        0x5fdccbf -> :sswitch_b
        0x63c142c -> :sswitch_c
        0x660706b -> :sswitch_d
        0x660721f -> :sswitch_e
        0x67d15bf -> :sswitch_f
        0x686a19f -> :sswitch_10
        0x68ba1ae -> :sswitch_11
        0x6952d1f -> :sswitch_12
        0x697e7df -> :sswitch_13
        0x6afffc4 -> :sswitch_14
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/23P;
    .locals 1

    .prologue
    .line 364168
    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/23P;
    .locals 2

    .prologue
    .line 364169
    new-instance v1, LX/23P;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-direct {v1, v0}, LX/23P;-><init>(LX/0W9;)V

    .line 364170
    return-object v1
.end method


# virtual methods
.method public final getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 364171
    iget-boolean v0, p0, LX/23P;->a:Z

    if-nez v0, :cond_0

    .line 364172
    :goto_0
    return-object p1

    .line 364173
    :cond_0
    if-nez p1, :cond_1

    .line 364174
    const/4 p1, 0x0

    goto :goto_0

    .line 364175
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/23P;->b:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 364176
    instance-of v1, p1, Landroid/text/Spanned;

    if-nez v1, :cond_2

    move-object p1, v0

    .line 364177
    goto :goto_0

    .line 364178
    :cond_2
    check-cast p1, Landroid/text/Spanned;

    .line 364179
    invoke-interface {p1}, Landroid/text/Spanned;->length()I

    move-result v1

    const-class v3, Ljava/lang/Object;

    invoke-interface {p1, v2, v1, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    .line 364180
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 364181
    array-length v4, v3

    move v0, v2

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v2, v3, v0

    .line 364182
    invoke-interface {p1, v2}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    invoke-interface {p1, v2}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {p1, v2}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v1, v2, v5, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 364183
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move-object p1, v1

    .line 364184
    goto :goto_0
.end method

.method public final onFocusChanged(Landroid/view/View;Ljava/lang/CharSequence;ZILandroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 364185
    return-void
.end method
