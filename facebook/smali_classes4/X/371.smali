.class public final LX/371;
.super LX/36y;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/36y",
        "<",
        "Lcom/facebook/video/server/prefetcher/PrefetchEvents$PrefetchRangeEndEvent$Handler;",
        ">;"
    }
.end annotation


# instance fields
.field public final b:J

.field public final c:Z


# direct methods
.method public constructor <init>(IJZ)V
    .locals 0

    .prologue
    .line 500576
    invoke-direct {p0, p1}, LX/36y;-><init>(I)V

    .line 500577
    iput-wide p2, p0, LX/371;->b:J

    .line 500578
    iput-boolean p4, p0, LX/371;->c:Z

    .line 500579
    return-void
.end method


# virtual methods
.method public final a(LX/16Y;)V
    .locals 6

    .prologue
    .line 500580
    check-cast p1, LX/36w;

    .line 500581
    invoke-static {p1, p0}, LX/36w;->a(LX/36w;LX/36y;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500582
    iget-wide v0, p1, LX/36w;->d:J

    iget-wide v2, p0, LX/371;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p1, LX/36w;->d:J

    .line 500583
    iget-object v0, p1, LX/36w;->c:LX/11o;

    const-string v1, "PrefetchRange"

    const/4 v2, 0x0

    iget-wide v4, p0, LX/371;->b:J

    invoke-static {v4, v5}, LX/36w;->a(J)LX/0P1;

    move-result-object v3

    const v4, 0x173de140

    invoke-static {v0, v1, v2, v3, v4}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 500584
    :cond_0
    return-void
.end method
