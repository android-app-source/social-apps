.class public LX/3Fx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 540221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540222
    return-void
.end method

.method public static a(LX/0QB;)LX/3Fx;
    .locals 1

    .prologue
    .line 540218
    new-instance v0, LX/3Fx;

    invoke-direct {v0}, LX/3Fx;-><init>()V

    .line 540219
    move-object v0, v0

    .line 540220
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 540223
    invoke-static {p1}, LX/2s8;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540224
    const v0, 0x7f0e091c

    .line 540225
    :goto_0
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-direct {v1, p0, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v1

    .line 540226
    :cond_0
    const-string v0, "ANDROID_EVENT_DISCOVER_EVENT_LIST"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 540227
    if-eqz v0, :cond_2

    .line 540228
    const v0, 0x7f0e091d

    goto :goto_0

    .line 540229
    :cond_2
    const-string v0, "ANDROID_EVENT_PERMALINK"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ANDROID_EVENT_PERMALINK_PRIVATE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_3
    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 540230
    if-eqz v0, :cond_4

    .line 540231
    const v0, 0x7f0e091e

    goto :goto_0

    .line 540232
    :cond_4
    invoke-static {p1}, LX/2s8;->o(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 540233
    const v0, 0x7f0e0949

    goto :goto_0

    .line 540234
    :cond_5
    invoke-static {p1}, LX/2s8;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 540235
    const v0, 0x7f0e0a41

    goto :goto_0

    .line 540236
    :cond_6
    const v0, 0x7f0e091b

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    :cond_8
    const/4 v0, 0x0

    goto :goto_2
.end method
