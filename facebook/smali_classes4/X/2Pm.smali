.class public LX/2Pm;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/omnistore/MqttProtocolProvider;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile sInstance__com_facebook_omnistore_MqttProtocolProvider__INJECTED_BY_TemplateInjector:Lcom/facebook/omnistore/MqttProtocolProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 407201
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static getInstance__com_facebook_omnistore_MqttProtocolProvider__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/omnistore/MqttProtocolProvider;
    .locals 3

    .prologue
    .line 407202
    sget-object v0, LX/2Pm;->sInstance__com_facebook_omnistore_MqttProtocolProvider__INJECTED_BY_TemplateInjector:Lcom/facebook/omnistore/MqttProtocolProvider;

    if-nez v0, :cond_1

    .line 407203
    const-class v1, LX/2Pm;

    monitor-enter v1

    .line 407204
    :try_start_0
    sget-object v0, LX/2Pm;->sInstance__com_facebook_omnistore_MqttProtocolProvider__INJECTED_BY_TemplateInjector:Lcom/facebook/omnistore/MqttProtocolProvider;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 407205
    if-eqz v2, :cond_0

    .line 407206
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 407207
    invoke-static {v0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->getInstance__com_facebook_omnistore_mqtt_OmnistoreMqttJniHandler__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    move-result-object p0

    check-cast p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    invoke-static {p0}, LX/2Po;->provideMqttProtocolProvider(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;)Lcom/facebook/omnistore/MqttProtocolProvider;

    move-result-object p0

    move-object v0, p0

    .line 407208
    sput-object v0, LX/2Pm;->sInstance__com_facebook_omnistore_MqttProtocolProvider__INJECTED_BY_TemplateInjector:Lcom/facebook/omnistore/MqttProtocolProvider;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 407209
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 407210
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 407211
    :cond_1
    sget-object v0, LX/2Pm;->sInstance__com_facebook_omnistore_MqttProtocolProvider__INJECTED_BY_TemplateInjector:Lcom/facebook/omnistore/MqttProtocolProvider;

    return-object v0

    .line 407212
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 407213
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 407214
    invoke-static {p0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->getInstance__com_facebook_omnistore_mqtt_OmnistoreMqttJniHandler__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    move-result-object v0

    check-cast v0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    invoke-static {v0}, LX/2Po;->provideMqttProtocolProvider(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;)Lcom/facebook/omnistore/MqttProtocolProvider;

    move-result-object v0

    move-object v0, v0

    .line 407215
    return-object v0
.end method
