.class public final enum LX/3Oh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Oh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Oh;

.field public static final enum EMPTY_CONSTRAINT:LX/3Oh;

.field public static final enum EXCEPTION:LX/3Oh;

.field public static final enum OK:LX/3Oh;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 560708
    new-instance v0, LX/3Oh;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2}, LX/3Oh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oh;->OK:LX/3Oh;

    .line 560709
    new-instance v0, LX/3Oh;

    const-string v1, "EMPTY_CONSTRAINT"

    invoke-direct {v0, v1, v3}, LX/3Oh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oh;->EMPTY_CONSTRAINT:LX/3Oh;

    .line 560710
    new-instance v0, LX/3Oh;

    const-string v1, "EXCEPTION"

    invoke-direct {v0, v1, v4}, LX/3Oh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oh;->EXCEPTION:LX/3Oh;

    .line 560711
    const/4 v0, 0x3

    new-array v0, v0, [LX/3Oh;

    sget-object v1, LX/3Oh;->OK:LX/3Oh;

    aput-object v1, v0, v2

    sget-object v1, LX/3Oh;->EMPTY_CONSTRAINT:LX/3Oh;

    aput-object v1, v0, v3

    sget-object v1, LX/3Oh;->EXCEPTION:LX/3Oh;

    aput-object v1, v0, v4

    sput-object v0, LX/3Oh;->$VALUES:[LX/3Oh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560713
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Oh;
    .locals 1

    .prologue
    .line 560714
    const-class v0, LX/3Oh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Oh;

    return-object v0
.end method

.method public static values()[LX/3Oh;
    .locals 1

    .prologue
    .line 560712
    sget-object v0, LX/3Oh;->$VALUES:[LX/3Oh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Oh;

    return-object v0
.end method
