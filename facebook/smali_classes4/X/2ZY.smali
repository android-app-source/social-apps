.class public LX/2ZY;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static b:LX/2Zg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2Zg",
            "<",
            "Ljava/lang/Object;",
            "LX/0cG",
            "<",
            "Lcom/facebook/katana/urimap/api/UriHandler;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 422559
    new-instance v0, LX/2ZZ;

    invoke-direct {v0}, LX/2ZZ;-><init>()V

    sput-object v0, LX/2ZY;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 422560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422561
    return-void
.end method

.method public static b(Landroid/content/Context;)LX/2Zg;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/2Zg",
            "<",
            "Ljava/lang/Object;",
            "LX/0cG",
            "<",
            "Lcom/facebook/katana/urimap/api/UriHandler;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 422562
    sget-object v0, LX/2ZY;->b:LX/2Zg;

    if-nez v0, :cond_0

    .line 422563
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 422564
    invoke-static {v0}, LX/2ZU;->b(LX/0QB;)LX/2ZX;

    .line 422565
    new-instance v5, LX/2Za;

    invoke-static {v0}, LX/2ZQ;->b(LX/0QB;)LX/2ZQ;

    move-result-object v1

    check-cast v1, LX/2ZQ;

    invoke-static {v0}, LX/2ZU;->b(LX/0QB;)LX/2ZX;

    move-result-object v2

    check-cast v2, LX/2ZX;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v3

    check-cast v3, LX/11H;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-direct {v5, v1, v2, v3, v4}, LX/2Za;-><init>(LX/2ZQ;LX/2ZX;LX/11H;LX/0TD;)V

    .line 422566
    move-object v0, v5

    .line 422567
    check-cast v0, LX/2Za;

    .line 422568
    new-instance v1, LX/2Zg;

    sget-object v2, LX/2Zh;->SINGLE_REQUEST:LX/2Zh;

    invoke-direct {v1, v0, v2, p0}, LX/2Zg;-><init>(LX/2Zb;LX/2Zh;Landroid/content/Context;)V

    sput-object v1, LX/2ZY;->b:LX/2Zg;

    .line 422569
    :cond_0
    sget-object v0, LX/2ZY;->b:LX/2Zg;

    return-object v0
.end method
