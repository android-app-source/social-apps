.class public final LX/38T;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/38T;


# instance fields
.field public final b:Landroid/os/Bundle;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 521032
    new-instance v0, LX/38T;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/38T;-><init>(Landroid/os/Bundle;Ljava/util/List;)V

    sput-object v0, LX/38T;->a:LX/38T;

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 521028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521029
    iput-object p1, p0, LX/38T;->b:Landroid/os/Bundle;

    .line 521030
    iput-object p2, p0, LX/38T;->c:Ljava/util/List;

    .line 521031
    return-void
.end method

.method public static e(LX/38T;)V
    .locals 2

    .prologue
    .line 521023
    iget-object v0, p0, LX/38T;->c:Ljava/util/List;

    if-nez v0, :cond_1

    .line 521024
    iget-object v0, p0, LX/38T;->b:Landroid/os/Bundle;

    const-string v1, "controlCategories"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/38T;->c:Ljava/util/List;

    .line 521025
    iget-object v0, p0, LX/38T;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/38T;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 521026
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/38T;->c:Ljava/util/List;

    .line 521027
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 521033
    invoke-static {p0}, LX/38T;->e(LX/38T;)V

    .line 521034
    iget-object v0, p0, LX/38T;->c:Ljava/util/List;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 521017
    instance-of v0, p1, LX/38T;

    if-eqz v0, :cond_0

    .line 521018
    check-cast p1, LX/38T;

    .line 521019
    invoke-static {p0}, LX/38T;->e(LX/38T;)V

    .line 521020
    invoke-static {p1}, LX/38T;->e(LX/38T;)V

    .line 521021
    iget-object v0, p0, LX/38T;->c:Ljava/util/List;

    iget-object v1, p1, LX/38T;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 521022
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 521015
    invoke-static {p0}, LX/38T;->e(LX/38T;)V

    .line 521016
    iget-object v0, p0, LX/38T;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 521010
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 521011
    const-string v1, "MediaRouteSelector{ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 521012
    const-string v1, "controlCategories="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/38T;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 521013
    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 521014
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
