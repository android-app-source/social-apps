.class public LX/3Im;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/1m0;

.field private final c:LX/19m;

.field public final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/graphql/model/GraphQLVideo;

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field private final i:Z

.field private final j:I

.field private final k:Z

.field private l:Landroid/net/Uri;

.field private m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 547177
    const-class v0, LX/3Im;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Im;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1m0;LX/0Or;)V
    .locals 4
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLVideo;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "LX/1m0;",
            "LX/0Or",
            "<",
            "LX/19m;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 547178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547179
    iput-boolean v3, p0, LX/3Im;->m:Z

    .line 547180
    iput-object p3, p0, LX/3Im;->b:LX/1m0;

    .line 547181
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19m;

    iput-object v0, p0, LX/3Im;->c:LX/19m;

    .line 547182
    iput-object p1, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 547183
    iput-object p2, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 547184
    iget-object v0, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 547185
    :goto_0
    iget-object v1, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, LX/3Im;->f:Z

    .line 547186
    iget-object v1, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->af()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/3Im;->c:LX/19m;

    .line 547187
    iget-boolean p1, v1, LX/19m;->d:Z

    move v1, p1

    .line 547188
    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    iput-boolean v1, p0, LX/3Im;->g:Z

    .line 547189
    iget-object v1, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->af()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    iput-boolean v1, p0, LX/3Im;->h:Z

    .line 547190
    iget-object v1, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->af()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v1, p0, LX/3Im;->g:Z

    if-nez v1, :cond_4

    iget-object v1, p0, LX/3Im;->c:LX/19m;

    .line 547191
    iget-boolean p1, v1, LX/19m;->i:Z

    move v1, p1

    .line 547192
    if-eqz v1, :cond_4

    iget-boolean v1, p0, LX/3Im;->h:Z

    if-nez v1, :cond_4

    :goto_4
    iput-boolean v2, p0, LX/3Im;->k:Z

    .line 547193
    invoke-static {v0}, LX/1VF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    iput-boolean v1, p0, LX/3Im;->i:Z

    .line 547194
    if-nez v0, :cond_5

    const/4 v0, -0x1

    :goto_5
    iput v0, p0, LX/3Im;->j:I

    .line 547195
    return-void

    .line 547196
    :cond_0
    iget-object v0, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v1, v3

    .line 547197
    goto :goto_1

    :cond_2
    move v1, v3

    .line 547198
    goto :goto_2

    :cond_3
    move v1, v3

    .line 547199
    goto :goto_3

    :cond_4
    move v2, v3

    .line 547200
    goto :goto_4

    .line 547201
    :cond_5
    invoke-static {v0}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/0x1;->f(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v0

    goto :goto_5
.end method

.method private static a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 547170
    invoke-static {p0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 547171
    if-eqz v0, :cond_0

    .line 547172
    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 547173
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 547174
    iget-boolean v0, p0, LX/3Im;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/3Im;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->aP()Ljava/lang/String;

    move-result-object v0

    .line 547175
    :goto_0
    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 547176
    :cond_0
    iget-object v0, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->aw()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c()Landroid/net/Uri;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 547156
    iget-boolean v0, p0, LX/3Im;->m:Z

    if-nez v0, :cond_1

    .line 547157
    iget-object v0, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 547158
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 547159
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 547160
    iget-object v0, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 547161
    :cond_0
    :goto_0
    move-object v0, v0

    .line 547162
    iput-object v0, p0, LX/3Im;->l:Landroid/net/Uri;

    .line 547163
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3Im;->m:Z

    .line 547164
    :cond_1
    iget-object v0, p0, LX/3Im;->l:Landroid/net/Uri;

    return-object v0

    .line 547165
    :cond_2
    iget-object v0, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->aY()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 547166
    if-nez v0, :cond_0

    .line 547167
    iget-boolean v0, p0, LX/3Im;->g:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/3Im;->h:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->aQ()Ljava/lang/String;

    move-result-object v0

    .line 547168
    :goto_1
    iget-object v1, p0, LX/3Im;->b:LX/1m0;

    iget-object v2, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, LX/3Im;->f:Z

    invoke-virtual {v1, v0, v2, v3}, LX/1m0;->a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 547169
    :cond_3
    iget-object v0, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 547149
    iget-object v0, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_1

    .line 547150
    iget-object v0, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 547151
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 547152
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    .line 547153
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547154
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GIF:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 547155
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/video/engine/VideoPlayerParams;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 547148
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1, v1}, LX/3Im;->a(ZZZ)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZZ)Lcom/facebook/video/engine/VideoPlayerParams;
    .locals 1

    .prologue
    .line 547147
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/3Im;->a(ZZZ)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZZZ)Lcom/facebook/video/engine/VideoPlayerParams;
    .locals 15

    .prologue
    .line 547102
    invoke-direct {p0}, LX/3Im;->b()Landroid/net/Uri;

    move-result-object v7

    .line 547103
    iget-object v2, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->aD()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 547104
    iget-object v2, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->aA()Ljava/lang/String;

    move-result-object v4

    .line 547105
    const/4 v3, 0x0

    .line 547106
    const/4 v2, 0x0

    .line 547107
    const/4 v5, 0x0

    .line 547108
    iget-boolean v8, p0, LX/3Im;->g:Z

    if-eqz v8, :cond_3

    iget-boolean v8, p0, LX/3Im;->h:Z

    if-nez v8, :cond_3

    .line 547109
    if-eqz v4, :cond_0

    .line 547110
    iget-object v2, p0, LX/3Im;->c:LX/19m;

    invoke-virtual {v2}, LX/19m;->g()LX/0Px;

    move-result-object v2

    .line 547111
    iget-object v3, p0, LX/3Im;->c:LX/19m;

    invoke-virtual {v3}, LX/19m;->j()LX/0Px;

    move-result-object v8

    .line 547112
    new-instance v9, LX/7QV;

    invoke-direct {v9, v4}, LX/7QV;-><init>(Ljava/lang/String;)V

    .line 547113
    invoke-virtual {v9, v2}, LX/7QV;->a(Ljava/util/List;)LX/19o;

    move-result-object v3

    .line 547114
    invoke-virtual {v9, v8}, LX/7QV;->b(Ljava/util/List;)LX/03z;

    move-result-object v2

    .line 547115
    invoke-virtual {v9}, LX/7QV;->toString()Ljava/lang/String;

    move-result-object v4

    .line 547116
    iget-object v8, p0, LX/3Im;->c:LX/19m;

    invoke-virtual {v8}, LX/19m;->c()Z

    move-result p3

    .line 547117
    :cond_0
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    .line 547118
    if-eqz v3, :cond_1

    if-nez v2, :cond_c

    .line 547119
    :cond_1
    const/4 v5, 0x1

    move v14, v5

    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    move v2, v14

    .line 547120
    :goto_0
    iget-boolean v8, p0, LX/3Im;->h:Z

    if-eqz v8, :cond_4

    iget-boolean v8, p0, LX/3Im;->g:Z

    if-eqz v8, :cond_4

    .line 547121
    iget-object v2, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->aF()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/19o;->fromString(Ljava/lang/String;)LX/19o;

    move-result-object v2

    .line 547122
    sget-object v4, LX/19o;->UNKNOWN:LX/19o;

    invoke-virtual {v2, v4}, LX/19o;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v2, LX/19o;->EQUIRECTANGULAR:LX/19o;

    :cond_2
    move-object v4, v2

    move-object v2, v6

    .line 547123
    :goto_1
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v6

    sget-object v8, LX/097;->FROM_STREAM:LX/097;

    invoke-virtual {v6, v8}, LX/2oE;->a(LX/097;)LX/2oE;

    move-result-object v6

    invoke-direct {p0}, LX/3Im;->c()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/2oE;->a(Landroid/net/Uri;)LX/2oE;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/2oE;->b(Landroid/net/Uri;)LX/2oE;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/2oE;->c(Landroid/net/Uri;)LX/2oE;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/2oE;->a(Ljava/lang/String;)LX/2oE;

    move-result-object v2

    invoke-virtual {v2}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v6

    .line 547124
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 547125
    iget-object v2, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->z()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->z()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 547126
    iget-object v2, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->z()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v2, 0x0

    move v5, v2

    :goto_2
    if-ge v5, v9, :cond_5

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;

    .line 547127
    new-instance v10, LX/7DB;

    invoke-direct {v10}, LX/7DB;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->j()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, LX/7DB;->a(J)LX/7DB;

    move-result-object v10

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->a()I

    move-result v11

    invoke-virtual {v10, v11}, LX/7DB;->a(I)LX/7DB;

    move-result-object v10

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->k()I

    move-result v2

    invoke-virtual {v10, v2}, LX/7DB;->b(I)LX/7DB;

    move-result-object v2

    invoke-virtual {v2}, LX/7DB;->a()Lcom/facebook/spherical/model/KeyframeParams;

    move-result-object v2

    .line 547128
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547129
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 547130
    :cond_3
    iget-boolean v8, p0, LX/3Im;->k:Z

    if-eqz v8, :cond_b

    .line 547131
    const/4 v5, 0x1

    move v14, v5

    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    move v2, v14

    goto/16 :goto_0

    .line 547132
    :cond_4
    if-eqz v2, :cond_a

    .line 547133
    const-string v5, ""

    .line 547134
    const/4 v4, 0x0

    .line 547135
    sget-object v2, LX/19o;->CUBEMAP:LX/19o;

    .line 547136
    sget-object v3, LX/03z;->STEREO:LX/03z;

    move-object v14, v2

    move-object v2, v4

    move-object v4, v14

    goto/16 :goto_1

    .line 547137
    :cond_5
    iget-boolean v2, p0, LX/3Im;->g:Z

    if-nez v2, :cond_8

    const/4 v2, 0x0

    .line 547138
    :goto_3
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v3

    invoke-direct {p0}, LX/3Im;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2oH;->a(Ljava/lang/String;)LX/2oH;

    move-result-object v3

    iget-object v4, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->ay()I

    move-result v4

    invoke-virtual {v3, v4}, LX/2oH;->a(I)LX/2oH;

    move-result-object v3

    iget-boolean v4, p0, LX/3Im;->f:Z

    invoke-virtual {v3, v4}, LX/2oH;->a(Z)LX/2oH;

    move-result-object v4

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->ab()Z

    move-result v3

    if-eqz v3, :cond_9

    if-eqz p1, :cond_9

    const/4 v3, 0x1

    :goto_4
    invoke-virtual {v4, v3}, LX/2oH;->c(Z)LX/2oH;

    move-result-object v3

    iget-object v4, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->q()I

    move-result v4

    iget-object v5, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLVideo;->D()I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/2oH;->a(II)LX/2oH;

    move-result-object v3

    iget-object v4, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->m()I

    move-result v4

    invoke-virtual {v3, v4}, LX/2oH;->b(I)LX/2oH;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/2oH;->a(Lcom/facebook/spherical/model/SphericalVideoParams;)LX/2oH;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/2oH;->g(Z)LX/2oH;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/2oH;->b(Z)LX/2oH;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->r()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2oH;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)LX/2oH;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, LX/2oH;->e(Z)LX/2oH;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->am()I

    move-result v3

    invoke-virtual {v2, v3}, LX/2oH;->d(I)LX/2oH;

    move-result-object v2

    iget-boolean v3, p0, LX/3Im;->i:Z

    invoke-virtual {v2, v3}, LX/2oH;->i(Z)LX/2oH;

    move-result-object v2

    iget v3, p0, LX/3Im;->j:I

    invoke-virtual {v2, v3}, LX/2oH;->e(I)LX/2oH;

    move-result-object v2

    iget-boolean v3, p0, LX/3Im;->k:Z

    invoke-virtual {v2, v3}, LX/2oH;->k(Z)LX/2oH;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, LX/2oH;->j(Z)LX/2oH;

    move-result-object v3

    .line 547139
    iget-object v2, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_7

    .line 547140
    iget-object v2, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/3Im;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    .line 547141
    invoke-virtual {v3, v2}, LX/2oH;->a(LX/162;)LX/2oH;

    .line 547142
    iget-object v2, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v2}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, LX/3Im;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v2}, LX/1VO;->l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 547143
    :cond_6
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, LX/2oH;->h(Z)LX/2oH;

    .line 547144
    :cond_7
    invoke-virtual {v3}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v2

    return-object v2

    .line 547145
    :cond_8
    new-instance v2, LX/7DI;

    invoke-direct {v2}, LX/7DI;-><init>()V

    invoke-virtual {v2, v4}, LX/7DI;->a(LX/19o;)LX/7DI;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/7DI;->a(LX/03z;)LX/7DI;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->S()I

    move-result v3

    invoke-virtual {v2, v3}, LX/7DI;->a(I)LX/7DI;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->T()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {v2, v3}, LX/7DI;->b(I)LX/7DI;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->U()I

    move-result v3

    invoke-virtual {v2, v3}, LX/7DI;->c(I)LX/7DI;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->aR()I

    move-result v3

    invoke-virtual {v2, v3}, LX/7DI;->d(I)LX/7DI;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->aO()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, LX/7DI;->a(D)LX/7DI;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->aN()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, LX/7DI;->b(D)LX/7DI;

    move-result-object v2

    new-instance v3, LX/7D9;

    invoke-direct {v3}, LX/7D9;-><init>()V

    invoke-virtual {v3, v7}, LX/7D9;->a(Ljava/util/List;)LX/7D9;

    move-result-object v3

    invoke-virtual {v3}, LX/7D9;->a()Lcom/facebook/spherical/model/GuidedTourParams;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/7DI;->a(Lcom/facebook/spherical/model/GuidedTourParams;)LX/7DI;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->bk()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/7DI;->a(Z)LX/7DI;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->bm()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, LX/7DI;->c(D)LX/7DI;

    move-result-object v2

    iget-object v3, p0, LX/3Im;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->bl()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, LX/7DI;->d(D)LX/7DI;

    move-result-object v2

    invoke-virtual {v2}, LX/7DI;->a()Lcom/facebook/spherical/model/SphericalVideoParams;

    move-result-object v2

    goto/16 :goto_3

    .line 547146
    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_4

    :cond_a
    move-object v2, v6

    goto/16 :goto_1

    :cond_b
    move v14, v5

    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    move v2, v14

    goto/16 :goto_0

    :cond_c
    move v14, v5

    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    move v2, v14

    goto/16 :goto_0
.end method
