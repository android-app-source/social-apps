.class public final LX/2xy;
.super LX/1yS;
.source ""


# instance fields
.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/ufiservices/util/LinkifyUtil$ExternalClickableSpanCallback;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/1Uf;

.field private final e:LX/17U;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1Uf;Ljava/lang/String;LX/17U;Landroid/content/Context;LX/0lF;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 479881
    iput-object p1, p0, LX/2xy;->d:LX/1Uf;

    .line 479882
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, LX/1yS;-><init>(LX/1Uf;Ljava/lang/String;Landroid/content/Context;LX/0lF;ILX/1yD;Ljava/lang/String;)V

    .line 479883
    iput-object p3, p0, LX/2xy;->e:LX/17U;

    .line 479884
    iput-object v6, p0, LX/2xy;->f:Ljava/lang/String;

    .line 479885
    iput-object v6, p0, LX/2xy;->g:Ljava/lang/String;

    .line 479886
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;LX/0lF;ZLX/1vY;)V
    .locals 6

    .prologue
    .line 479887
    iget-object v0, p0, LX/2xy;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/2xy;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nS;

    .line 479888
    :goto_0
    if-eqz v0, :cond_0

    .line 479889
    iget-object v1, v0, LX/1nS;->a:LX/1nA;

    iget-object v1, v1, LX/1nA;->i:LX/1Ay;

    .line 479890
    iget-object v2, p0, LX/1yS;->e:LX/0lF;

    move-object v2, v2

    .line 479891
    iget-object v3, p0, LX/1yS;->c:Ljava/lang/String;

    move-object v3, v3

    .line 479892
    invoke-virtual {v1, v2, v3}, LX/1Ay;->a(LX/0lF;Ljava/lang/String;)V

    .line 479893
    iget-object v1, p0, LX/2xy;->f:Ljava/lang/String;

    move-object v1, v1

    .line 479894
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 479895
    iget-object v1, v0, LX/1nS;->a:LX/1nA;

    iget-object v1, v1, LX/1nA;->j:LX/0bH;

    new-instance v2, LX/1ZS;

    .line 479896
    iget-object v3, p0, LX/2xy;->f:Ljava/lang/String;

    move-object v3, v3

    .line 479897
    iget-object v4, p0, LX/2xy;->g:Ljava/lang/String;

    move-object v4, v4

    .line 479898
    invoke-direct {v2, v3, v4}, LX/1ZS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 479899
    :cond_0
    invoke-static {p2}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/2xy;->d:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v1

    .line 479900
    iput-object p2, v1, LX/47H;->a:Ljava/lang/String;

    .line 479901
    move-object v1, v1

    .line 479902
    invoke-virtual {v1}, LX/47H;->a()LX/47I;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;LX/47I;)Landroid/content/Intent;

    move-result-object v1

    .line 479903
    :goto_1
    if-eqz v1, :cond_2

    .line 479904
    if-eqz p3, :cond_1

    invoke-virtual {p3}, LX/0lF;->e()I

    move-result v0

    if-lez v0, :cond_1

    .line 479905
    const-string v0, "tracking_codes"

    invoke-virtual {p3}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 479906
    :cond_1
    if-eqz p4, :cond_5

    .line 479907
    const-string v0, "iab_click_source"

    const-string v2, "feed_link_ads"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 479908
    const-string v0, "browser_metrics_join_key"

    iget-object v2, p0, LX/2xy;->d:LX/1Uf;

    iget-object v2, v2, LX/1Uf;->H:LX/1Um;

    invoke-virtual {v2}, LX/1Um;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 479909
    :goto_2
    iget-object v0, p0, LX/2xy;->e:LX/17U;

    move-object v2, p1

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/17U;->a(Landroid/content/Intent;Landroid/content/Context;LX/0lF;ZLX/1vY;)V

    .line 479910
    :cond_2
    return-void

    .line 479911
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 479912
    :cond_4
    iget-object v0, p0, LX/2xy;->d:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Qo;

    invoke-virtual {v0, p2}, LX/5Qo;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_1

    .line 479913
    :cond_5
    const-string v0, "iab_click_source"

    const-string v2, "feed_link"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2
.end method
