.class public LX/2S7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements Lcom/facebook/webrtc/IWebrtcLoggingInterface;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile N:LX/2S7;

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field public F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:J

.field private K:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final M:LX/2S9;

.field public a:Ljava/lang/String;

.field private final c:Landroid/content/Context;

.field private final d:LX/0Zb;

.field public final e:LX/1MZ;

.field public final f:LX/0ka;

.field private final g:LX/0kb;

.field public final h:Landroid/telephony/TelephonyManager;

.field private final i:LX/0SG;

.field private final j:LX/0TL;

.field public final k:LX/1fX;

.field public final l:Ljava/util/Random;

.field private final m:LX/1sj;

.field private final n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0lC;

.field private final q:LX/0u7;

.field private final r:Landroid/media/AudioManager;

.field private final s:LX/0ad;

.field public t:Ljava/io/File;

.field public u:I

.field public v:Ljava/lang/String;

.field public w:J

.field public x:J

.field public y:J

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 410930
    const-class v0, LX/2S7;

    sput-object v0, LX/2S7;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Zb;LX/1MZ;LX/0ka;LX/0kb;Landroid/telephony/TelephonyManager;LX/0SG;LX/0TL;Ljava/util/Random;LX/1fX;LX/1sj;LX/0Or;LX/0lC;LX/0ad;LX/0u7;Landroid/media/AudioManager;LX/2S9;)V
    .locals 2
    .param p9    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .param p10    # LX/1fX;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/rtc/logging/annotations/MobileTracerEmployees;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Zb;",
            "LX/1MZ;",
            "LX/0ka;",
            "LX/0kb;",
            "Landroid/telephony/TelephonyManager;",
            "LX/0SG;",
            "LX/0TL;",
            "Ljava/util/Random;",
            "LX/1fX;",
            "LX/1sj;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0lC;",
            "LX/0ad;",
            "LX/0u7;",
            "Landroid/media/AudioManager;",
            "LX/2S9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410932
    const/4 v1, -0x1

    iput v1, p0, LX/2S7;->B:I

    .line 410933
    const/4 v1, -0x1

    iput v1, p0, LX/2S7;->C:I

    .line 410934
    const/4 v1, 0x0

    iput v1, p0, LX/2S7;->D:I

    .line 410935
    const/4 v1, 0x0

    iput v1, p0, LX/2S7;->E:I

    .line 410936
    const/4 v1, -0x1

    iput v1, p0, LX/2S7;->F:I

    .line 410937
    const/4 v1, 0x0

    iput-object v1, p0, LX/2S7;->K:Ljava/util/HashMap;

    .line 410938
    const/4 v1, 0x0

    iput-object v1, p0, LX/2S7;->L:Ljava/util/HashMap;

    .line 410939
    iput-object p1, p0, LX/2S7;->c:Landroid/content/Context;

    .line 410940
    iput-object p2, p0, LX/2S7;->d:LX/0Zb;

    .line 410941
    iput-object p3, p0, LX/2S7;->e:LX/1MZ;

    .line 410942
    iput-object p4, p0, LX/2S7;->f:LX/0ka;

    .line 410943
    iput-object p5, p0, LX/2S7;->g:LX/0kb;

    .line 410944
    iput-object p6, p0, LX/2S7;->h:Landroid/telephony/TelephonyManager;

    .line 410945
    iput-object p7, p0, LX/2S7;->i:LX/0SG;

    .line 410946
    iput-object p8, p0, LX/2S7;->j:LX/0TL;

    .line 410947
    iput-object p9, p0, LX/2S7;->l:Ljava/util/Random;

    .line 410948
    iput-object p10, p0, LX/2S7;->k:LX/1fX;

    .line 410949
    iput-object p11, p0, LX/2S7;->m:LX/1sj;

    .line 410950
    iput-object p12, p0, LX/2S7;->o:LX/0Or;

    .line 410951
    iput-object p13, p0, LX/2S7;->p:LX/0lC;

    .line 410952
    move-object/from16 v0, p14

    iput-object v0, p0, LX/2S7;->s:LX/0ad;

    .line 410953
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, LX/2S7;->n:Ljava/util/Set;

    .line 410954
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2S7;->q:LX/0u7;

    .line 410955
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2S7;->r:Landroid/media/AudioManager;

    .line 410956
    move-object/from16 v0, p17

    iput-object v0, p0, LX/2S7;->M:LX/2S9;

    .line 410957
    return-void
.end method

.method public static a(LX/0QB;)LX/2S7;
    .locals 3

    .prologue
    .line 410958
    sget-object v0, LX/2S7;->N:LX/2S7;

    if-nez v0, :cond_1

    .line 410959
    const-class v1, LX/2S7;

    monitor-enter v1

    .line 410960
    :try_start_0
    sget-object v0, LX/2S7;->N:LX/2S7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 410961
    if-eqz v2, :cond_0

    .line 410962
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2S7;->b(LX/0QB;)LX/2S7;

    move-result-object v0

    sput-object v0, LX/2S7;->N:LX/2S7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410963
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 410964
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 410965
    :cond_1
    sget-object v0, LX/2S7;->N:LX/2S7;

    return-object v0

    .line 410966
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 410967
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 410968
    iget-object v0, p0, LX/2S7;->k:LX/1fX;

    new-instance v1, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$3;-><init>(LX/2S7;J)V

    const v2, 0x681f2887

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 410969
    return-void
.end method

.method private a(JLjava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 410970
    invoke-static {p0, p3}, LX/2S7;->a$redex0(LX/2S7;Ljava/util/HashMap;)V

    .line 410971
    invoke-direct {p0, p1, p2}, LX/2S7;->a(J)V

    .line 410972
    return-void
.end method

.method public static a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 410973
    const-string v0, "webrtc"

    .line 410974
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 410975
    iget-object v0, p0, LX/2S7;->d:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 410976
    return-void
.end method

.method private static a(LX/2gQ;JJ)V
    .locals 3

    .prologue
    .line 410977
    const-string v0, "msg_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p0, v0, v1}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410978
    const-string v0, "call_id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p0, v0, v1}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410979
    return-void
.end method

.method private static a(LX/2gQ;JJJ)V
    .locals 2

    .prologue
    .line 410980
    const-string v0, "ack_msg_id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p0, v0, v1}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410981
    invoke-static {p0, p1, p2, p5, p6}, LX/2S7;->a(LX/2gQ;JJ)V

    .line 410982
    return-void
.end method

.method private a(LX/2gQ;JJJLjava/lang/String;)V
    .locals 8

    .prologue
    .line 410983
    const-string v0, "ack_msg_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p6

    move-object/from16 v6, p8

    .line 410984
    invoke-static/range {v1 .. v6}, LX/2S7;->a(LX/2gQ;JJLjava/lang/String;)V

    .line 410985
    return-void
.end method

.method private static a(LX/2gQ;JJLjava/lang/String;)V
    .locals 2

    .prologue
    .line 410986
    const-string v0, "error_code"

    invoke-interface {p0, v0, p5}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410987
    const-string v0, "success"

    const-string v1, "false"

    invoke-interface {p0, v0, v1}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410988
    invoke-static {p0, p1, p2, p3, p4}, LX/2S7;->a(LX/2gQ;JJ)V

    .line 410989
    return-void
.end method

.method private a(LX/2gQ;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 410990
    :try_start_0
    iget-object v0, p0, LX/2S7;->p:LX/0lC;

    invoke-virtual {v0, p2}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 410991
    iget-object v1, p0, LX/2S7;->p:LX/0lC;

    const-class v2, Ljava/util/Map;

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 410992
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 410993
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 410994
    :catch_0
    :goto_1
    const/4 v0, 0x0

    :goto_2
    return v0

    .line 410995
    :cond_0
    const/4 v0, 0x1

    goto :goto_2

    .line 410996
    :catch_1
    goto :goto_1

    :catch_2
    goto :goto_1
.end method

.method public static a$redex0(LX/2S7;Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 410997
    if-nez p1, :cond_0

    .line 410998
    :goto_0
    return-void

    .line 410999
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "info"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 411000
    const-string v1, "tag"

    const-string v2, "endcallstats"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411001
    invoke-virtual {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411002
    invoke-direct {p0, v0}, LX/2S7;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 411003
    iget v1, p0, LX/2S7;->F:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 411004
    const-string v1, "cell_lvl"

    iget v2, p0, LX/2S7;->F:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411005
    :cond_1
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 411006
    invoke-static {}, LX/01m;->a()I

    move-result v1

    .line 411007
    const/4 v2, 0x3

    if-le v1, v2, :cond_2

    .line 411008
    :goto_1
    goto :goto_0

    .line 411009
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    goto :goto_1
.end method

.method private static b(LX/0QB;)LX/2S7;
    .locals 19

    .prologue
    .line 411010
    new-instance v1, LX/2S7;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v4

    check-cast v4, LX/1MZ;

    invoke-static/range {p0 .. p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v5

    check-cast v5, LX/0ka;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/0e7;->a(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0TK;->a(LX/0QB;)LX/0TL;

    move-result-object v9

    check-cast v9, LX/0TL;

    invoke-static/range {p0 .. p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v10

    check-cast v10, Ljava/util/Random;

    invoke-static/range {p0 .. p0}, LX/1fV;->a(LX/0QB;)LX/1fW;

    move-result-object v11

    check-cast v11, LX/1fX;

    invoke-static/range {p0 .. p0}, LX/1sj;->a(LX/0QB;)LX/1sj;

    move-result-object v12

    check-cast v12, LX/1sj;

    const/16 v13, 0x1562

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v14

    check-cast v14, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v15

    check-cast v15, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0u7;->a(LX/0QB;)LX/0u7;

    move-result-object v16

    check-cast v16, LX/0u7;

    invoke-static/range {p0 .. p0}, LX/19T;->a(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v17

    check-cast v17, Landroid/media/AudioManager;

    invoke-static/range {p0 .. p0}, LX/2S9;->a(LX/0QB;)LX/2S9;

    move-result-object v18

    check-cast v18, LX/2S9;

    invoke-direct/range {v1 .. v18}, LX/2S7;-><init>(Landroid/content/Context;LX/0Zb;LX/1MZ;LX/0ka;LX/0kb;Landroid/telephony/TelephonyManager;LX/0SG;LX/0TL;Ljava/util/Random;LX/1fX;LX/1sj;LX/0Or;LX/0lC;LX/0ad;LX/0u7;Landroid/media/AudioManager;LX/2S9;)V

    .line 411011
    return-object v1
.end method

.method public static b(LX/2S7;J)Ljava/io/File;
    .locals 5

    .prologue
    .line 411012
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/2S7;->t:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".callsum"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 6

    .prologue
    .line 411025
    const-string v0, "connectivity"

    invoke-static {p0}, LX/2S7;->m(LX/2S7;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411026
    const-string v0, "net_sid"

    iget-object v1, p0, LX/2S7;->g:LX/0kb;

    .line 411027
    iget-wide v4, v1, LX/0kb;->x:J

    move-wide v2, v4

    .line 411028
    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411029
    iget-object v0, p0, LX/2S7;->g:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->e()LX/0am;

    move-result-object v0

    .line 411030
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 411031
    const-string v1, "net_duration"

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411032
    :cond_0
    iget-object v0, p0, LX/2S7;->g:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->r()I

    move-result v0

    .line 411033
    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 411034
    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    .line 411035
    const-string v1, "rssi100"

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411036
    :cond_1
    iget-object v0, p0, LX/2S7;->f:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->c()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 411037
    const-string v0, ""

    .line 411038
    if-eqz v1, :cond_2

    .line 411039
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo$State;->name()Ljava/lang/String;

    move-result-object v0

    .line 411040
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_2

    .line 411041
    const-string v1, "is_connected"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411042
    :cond_2
    const-string v1, "net_state"

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411043
    iget-object v0, p0, LX/2S7;->h:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_3

    .line 411044
    const-string v0, "network_type"

    iget-object v1, p0, LX/2S7;->h:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    invoke-static {v1}, LX/0km;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411045
    const-string v0, "phone_type"

    iget-object v1, p0, LX/2S7;->h:Landroid/telephony/TelephonyManager;

    invoke-static {v1}, LX/0km;->a(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411046
    :cond_3
    const-string v0, "mqtt"

    iget-object v1, p0, LX/2S7;->e:LX/1MZ;

    invoke-virtual {v1}, LX/1MZ;->d()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411047
    const-string v0, "wifi"

    iget-object v1, p0, LX/2S7;->f:LX/0ka;

    invoke-virtual {v1}, LX/0ka;->b()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411048
    return-void
.end method

.method private static j(LX/2S7;)I
    .locals 2

    .prologue
    .line 411013
    iget-object v0, p0, LX/2S7;->r:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    and-int/lit8 v0, v0, 0x7

    .line 411014
    iget-object v1, p0, LX/2S7;->r:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 411015
    or-int/lit8 v0, v0, 0x8

    .line 411016
    :cond_0
    iget-object v1, p0, LX/2S7;->r:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 411017
    or-int/lit8 v0, v0, 0x10

    .line 411018
    :cond_1
    iget-object v1, p0, LX/2S7;->r:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 411019
    or-int/lit8 v0, v0, 0x20

    .line 411020
    :cond_2
    iget-object v1, p0, LX/2S7;->r:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 411021
    or-int/lit8 v0, v0, 0x40

    .line 411022
    :cond_3
    iget-object v1, p0, LX/2S7;->r:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 411023
    or-int/lit16 v0, v0, 0x80

    .line 411024
    :cond_4
    return v0
.end method

.method public static m(LX/2S7;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 411125
    iget-object v0, p0, LX/2S7;->f:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->c()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 411126
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v1

    if-nez v1, :cond_1

    .line 411127
    :cond_0
    const-string v0, "none"

    .line 411128
    :goto_0
    return-object v0

    .line 411129
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-nez v1, :cond_2

    .line 411130
    const-string v0, "cell"

    goto :goto_0

    .line 411131
    :cond_2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const-string v1, "mobile2"

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 411132
    const-string v0, "cell"

    goto :goto_0

    .line 411133
    :cond_3
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 411105
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 411106
    const/4 v0, 0x0

    .line 411107
    :cond_0
    :goto_0
    return-object v0

    .line 411108
    :cond_1
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 411109
    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411110
    const-string v1, "call_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411111
    iget v1, p0, LX/2S7;->z:I

    if-ltz v1, :cond_2

    .line 411112
    const-string v1, "battery_start"

    iget v2, p0, LX/2S7;->z:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411113
    :cond_2
    iget-object v1, p0, LX/2S7;->q:LX/0u7;

    invoke-virtual {v1}, LX/0u7;->a()F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 411114
    if-ltz v1, :cond_3

    .line 411115
    const-string v2, "battery_end"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411116
    :cond_3
    const-string v1, "screen_res"

    iget-object v2, p0, LX/2S7;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411117
    const-string v1, "hw_au_md_cfg"

    iget v2, p0, LX/2S7;->A:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411118
    iget v1, p0, LX/2S7;->B:I

    if-eq v1, v3, :cond_4

    .line 411119
    const-string v1, "start_au_manager"

    iget v2, p0, LX/2S7;->B:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411120
    :cond_4
    iget v1, p0, LX/2S7;->C:I

    if-eq v1, v3, :cond_5

    .line 411121
    const-string v1, "end_au_manager"

    iget v2, p0, LX/2S7;->C:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411122
    :cond_5
    const-string v1, "num_au_manager_changed"

    iget v2, p0, LX/2S7;->E:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411123
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 411124
    const-string v1, "cpu_cores"

    iget-object v2, p0, LX/2S7;->j:LX/0TL;

    invoke-virtual {v2}, LX/0TL;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 411103
    invoke-static {p0}, LX/2S7;->j(LX/2S7;)I

    move-result v0

    iput v0, p0, LX/2S7;->B:I

    .line 411104
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 411101
    iput p1, p0, LX/2S7;->A:I

    .line 411102
    return-void
.end method

.method public final a(III)V
    .locals 0

    .prologue
    .line 411097
    iput p1, p0, LX/2S7;->G:I

    .line 411098
    iput p2, p0, LX/2S7;->H:I

    .line 411099
    iput p3, p0, LX/2S7;->I:I

    .line 411100
    return-void
.end method

.method public final a(IZZJ)V
    .locals 4

    .prologue
    .line 411088
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "survey"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 411089
    const-string v1, "rating5"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411090
    const-string v1, "speaker_on"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411091
    const-string v1, "microphone_mute"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411092
    const-string v1, "call_id"

    invoke-virtual {v0, v1, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411093
    const-string v1, "android_sdk"

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411094
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 411095
    iget-object v0, p0, LX/2S7;->M:LX/2S9;

    const-string v1, "Survey: call_id[%d] rating[%d]"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2S9;->a(Ljava/lang/String;)V

    .line 411096
    return-void
.end method

.method public final a(JJZLX/79H;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 411134
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "rtc_expression"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 411135
    const-string v2, "rtc_expression_action"

    if-eqz p5, :cond_0

    const-string v0, "enabled"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411136
    const-string v0, "peer_id"

    invoke-virtual {v1, v0, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411137
    const-string v0, "call_id"

    invoke-virtual {v1, v0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411138
    const-string v0, "rtc_expression_type"

    invoke-virtual {p6}, LX/79H;->ordinal()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411139
    const-string v0, "rtc_expression_duration"

    invoke-virtual {v1, v0, p9, p10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411140
    const-string v0, "rtc_expression_name"

    invoke-virtual {v1, v0, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411141
    const-string v0, "rtc_expression_id"

    invoke-virtual {v1, v0, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411142
    invoke-static {p0, v1}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 411143
    return-void

    .line 411144
    :cond_0
    const-string v0, "disabled"

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 411083
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "client_error"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 411084
    const-string v1, "call_id"

    iget-wide v2, p0, LX/2S7;->w:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411085
    const-string v1, "content"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411086
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 411087
    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 411080
    iput-object p1, p0, LX/2S7;->v:Ljava/lang/String;

    .line 411081
    iput-wide p2, p0, LX/2S7;->x:J

    .line 411082
    return-void
.end method

.method public final a(Ljava/lang/String;JJJLjava/lang/String;)V
    .locals 2

    .prologue
    .line 411072
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "dropped_message"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 411073
    const-string v1, "type"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411074
    const-string v1, "msg_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411075
    const-string v1, "call_id"

    invoke-virtual {v0, v1, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411076
    const-string v1, "from"

    invoke-virtual {v0, v1, p6, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411077
    const-string v1, "source"

    invoke-virtual {v0, v1, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 411078
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 411079
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 411065
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2S7;->K:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 411066
    iget-object v0, p0, LX/2S7;->L:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 411067
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2S7;->L:Ljava/util/HashMap;

    .line 411068
    :cond_0
    iget-object v0, p0, LX/2S7;->L:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 411069
    :goto_0
    monitor-exit p0

    return-void

    .line 411070
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2S7;->K:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 411071
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 11

    .prologue
    .line 411052
    const/4 v10, 0x0

    .line 411053
    new-instance v5, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 411054
    const-string v4, "core_metrics"

    invoke-virtual {v5, v4}, LX/0m9;->j(Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "ver"

    const/16 v7, 0x14

    invoke-virtual {v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v6

    const-string v7, "call_type"

    if-eqz p3, :cond_1

    const-string v4, "direct_video"

    :goto_0
    invoke-virtual {v6, v7, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "caller"

    const/4 v7, 0x1

    invoke-virtual {v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v4

    const-string v6, "peer_id"

    invoke-virtual {v4, v6, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    .line 411055
    const-string v6, "end"

    invoke-virtual {v4, v6}, LX/0m9;->j(Ljava/lang/String;)LX/0m9;

    move-result-object v6

    const-string v7, "conn_type"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    const-string v7, "ctd"

    invoke-virtual {v6, v7, v10}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v6

    const-string v7, "remote_ended"

    invoke-virtual {v6, v7, v10}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v6

    const-string v7, "end_call_reason_string"

    const-string v8, "AbortedCall"

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v6

    const-string v7, "end_call_subreason_string"

    invoke-virtual {v6, v7, p4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 411056
    const-string v6, "signaling"

    invoke-virtual {v4, v6}, LX/0m9;->j(Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "start_time"

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v4, v6, v8, v9}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    move-result-object v4

    const-string v6, "trigger"

    invoke-virtual {v4, v6, p2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    .line 411057
    const-string v6, "time_from_start"

    invoke-virtual {v4, v6}, LX/0m9;->j(Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string v6, "started"

    invoke-virtual {v4, v6, v10}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v4

    const-string v6, "ended"

    invoke-virtual {v4, v6, v10}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 411058
    invoke-virtual {v5}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 411059
    :cond_0
    iget-object v4, p0, LX/2S7;->l:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-long v4, v4

    .line 411060
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-eqz v6, :cond_0

    .line 411061
    move-wide v2, v4

    .line 411062
    invoke-virtual {p0, v2, v3, v0}, LX/2S7;->a(JLjava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p0, v0}, LX/2S7;->a$redex0(LX/2S7;Ljava/util/HashMap;)V

    .line 411063
    return-void

    .line 411064
    :cond_1
    const-string v4, "voip"

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 411049
    if-eqz p2, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, p1, v0}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 411050
    return-void

    .line 411051
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 410920
    invoke-static {p0}, LX/2S7;->j(LX/2S7;)I

    move-result v0

    iput v0, p0, LX/2S7;->C:I

    .line 410921
    return-void
.end method

.method public final declared-synchronized b(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 410922
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, LX/2S7;->J:J

    .line 410923
    invoke-virtual {p0, p1, p2, p3}, LX/2S7;->a(JLjava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2S7;->K:Ljava/util/HashMap;

    .line 410924
    iget-object v0, p0, LX/2S7;->L:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 410925
    iget-object v0, p0, LX/2S7;->L:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 410926
    iget-object v2, p0, LX/2S7;->K:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 410927
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 410928
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, LX/2S7;->L:Ljava/util/HashMap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410929
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final b(Ljava/lang/String;J)Z
    .locals 2

    .prologue
    .line 410828
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 410829
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 410830
    const/4 v0, 0x1

    .line 410831
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 410824
    if-eqz p2, :cond_0

    .line 410825
    invoke-virtual {p0, p1, p2}, LX/2S7;->a(Ljava/lang/String;Z)V

    .line 410826
    const/4 v0, 0x1

    .line 410827
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final discardCall(J)V
    .locals 3

    .prologue
    .line 410821
    invoke-direct {p0, p1, p2}, LX/2S7;->a(J)V

    .line 410822
    iget-object v0, p0, LX/2S7;->n:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 410823
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 410815
    iget-object v0, p0, LX/2S7;->K:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 410816
    :goto_0
    return-void

    .line 410817
    :cond_0
    iget-wide v0, p0, LX/2S7;->J:J

    iget-object v2, p0, LX/2S7;->K:Ljava/util/HashMap;

    invoke-direct {p0, v0, v1, v2}, LX/2S7;->a(JLjava/util/HashMap;)V

    .line 410818
    const/4 v0, 0x0

    iput-object v0, p0, LX/2S7;->K:Ljava/util/HashMap;

    .line 410819
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2S7;->J:J

    .line 410820
    invoke-virtual {p0}, LX/2S7;->h()V

    goto :goto_0
.end method

.method public final h()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const-wide/16 v2, 0x0

    .line 410688
    iget-object v0, p0, LX/2S7;->K:Ljava/util/HashMap;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2S7;->L:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 410689
    :cond_0
    iget-object v0, p0, LX/2S7;->K:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    const-string v7, "end_call_summary"

    .line 410690
    :goto_0
    const-string v6, "logging_not_reset"

    move-object v1, p0

    move-wide v4, v2

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    .line 410691
    iput-object v8, p0, LX/2S7;->K:Ljava/util/HashMap;

    .line 410692
    iput-object v8, p0, LX/2S7;->L:Ljava/util/HashMap;

    .line 410693
    iput-wide v2, p0, LX/2S7;->J:J

    .line 410694
    :cond_1
    return-void

    .line 410695
    :cond_2
    const-string v7, "temp_call_summary"

    goto :goto_0
.end method

.method public final init()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 410811
    iget-object v0, p0, LX/2S7;->c:Landroid/content/Context;

    const-string v1, "call_stats"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/2S7;->t:Ljava/io/File;

    .line 410812
    iput v2, p0, LX/2S7;->E:I

    .line 410813
    iget-object v0, p0, LX/2S7;->k:LX/1fX;

    new-instance v1, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;-><init>(LX/2S7;)V

    const v2, -0x59ba74b1

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 410814
    return-void
.end method

.method public final logCallAction(JJLjava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    .line 410776
    iget-object v0, p0, LX/2S7;->s:LX/0ad;

    sget-short v1, LX/79F;->a:S

    invoke-interface {v0, v1, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 410777
    if-eqz v0, :cond_0

    .line 410778
    :goto_0
    return-void

    .line 410779
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "call_action"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410780
    const-string v1, "call_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410781
    const-string v1, "peer_id"

    invoke-virtual {v0, v1, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410782
    const-string v1, "call_action"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410783
    const-string v1, "content"

    invoke-virtual {v0, v1, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410784
    invoke-direct {p0, v0}, LX/2S7;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410785
    const-string v1, "start_call"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 410786
    iget-object v1, p0, LX/2S7;->M:LX/2S9;

    const-string v2, "Call action: call_id[%d] peer_id[%d] call_action[%s] details[%s] trigger[%s]"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p5, v3, v4

    const/4 v4, 0x3

    aput-object p6, v3, v4

    const/4 v4, 0x4

    iget-object v5, p0, LX/2S7;->v:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410787
    const-string v1, "trigger"

    iget-object v2, p0, LX/2S7;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410788
    const/4 v1, 0x0

    iput-object v1, p0, LX/2S7;->v:Ljava/lang/String;

    .line 410789
    iget-wide v2, p0, LX/2S7;->x:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_2

    .line 410790
    iget-object v1, p0, LX/2S7;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 410791
    const-string v1, "callable_freshness"

    iget-wide v4, p0, LX/2S7;->x:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410792
    :goto_1
    iget-wide v2, p0, LX/2S7;->x:J

    iput-wide v2, p0, LX/2S7;->y:J

    .line 410793
    iput-wide v6, p0, LX/2S7;->x:J

    .line 410794
    iput-wide p1, p0, LX/2S7;->w:J

    .line 410795
    :cond_1
    :goto_2
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0

    .line 410796
    :cond_2
    const-string v1, "callable_freshness"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_1

    .line 410797
    :cond_3
    const-string v1, "end_call"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 410798
    iget-object v1, p0, LX/2S7;->M:LX/2S9;

    const-string v2, "Call action: call_id[%d] peer_id[%d] call_action[%s] details[%s]"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v2, v3, v4, p5, p6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410799
    iput-wide v6, p0, LX/2S7;->w:J

    goto :goto_2

    .line 410800
    :cond_4
    const-string v1, "set_video_on"

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 410801
    iget-object v1, p0, LX/2S7;->M:LX/2S9;

    const-string v2, "Call action: call_id[%d] peer_id[%d] call_action[%s] details[%s]"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v2, v3, v4, p5, p6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410802
    const-string v1, "num_cam"

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410803
    const-string v1, "id"

    iget v2, p0, LX/2S7;->G:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410804
    const-string v1, "width"

    iget v2, p0, LX/2S7;->H:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410805
    const-string v1, "height"

    iget v2, p0, LX/2S7;->I:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410806
    iget v1, p0, LX/2S7;->G:I

    if-ltz v1, :cond_1

    .line 410807
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 410808
    iget v2, p0, LX/2S7;->G:I

    invoke-static {v2, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 410809
    const-string v2, "facing"

    iget v3, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410810
    const-string v2, "orient"

    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_2
.end method

.method public final logConnectionStatus(ZLjava/lang/String;J)V
    .locals 5

    .prologue
    .line 410768
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "connection_status"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410769
    const-string v1, "is_connected"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410770
    const-string v1, "call_id"

    invoke-virtual {v0, v1, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410771
    const-string v1, "content"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410772
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410773
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 410774
    iget-object v0, p0, LX/2S7;->M:LX/2S9;

    const-string v1, "Connection status: call_id[%d] is_connected[%b] details[%s]"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v2, v3, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410775
    return-void
.end method

.method public final logEndCallSummary(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 410763
    invoke-virtual {p0, p1, p2, p3}, LX/2S7;->a(JLjava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    .line 410764
    invoke-direct {p0, p1, p2, v0}, LX/2S7;->a(JLjava/util/HashMap;)V

    .line 410765
    iget-object v0, p0, LX/2S7;->n:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 410766
    iget-object v0, p0, LX/2S7;->M:LX/2S9;

    const-string v1, "EndCallSummary call_id[%d] %s"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410767
    return-void
.end method

.method public final logFbTraceReplyReceivedFailure(Ljava/lang/String;JJLjava/lang/String;)V
    .locals 8

    .prologue
    .line 410755
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410756
    :goto_0
    return-void

    .line 410757
    :cond_0
    iget-object v0, p0, LX/2S7;->m:LX/1sj;

    invoke-virtual {v0, p1}, LX/1sj;->b(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v7

    .line 410758
    invoke-static {v7}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    .line 410759
    invoke-static/range {v1 .. v6}, LX/2S7;->a(LX/2gQ;JJLjava/lang/String;)V

    .line 410760
    const-string v0, "op"

    const-string v2, "engine_to_app_send"

    invoke-interface {v1, v0, v2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410761
    const-string v0, "service"

    const-string v2, "sender_webrtc_application_layer"

    invoke-interface {v1, v0, v2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410762
    iget-object v0, p0, LX/2S7;->m:LX/1sj;

    sget-object v2, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    invoke-virtual {v0, v7, v2, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    goto :goto_0
.end method

.method public final logFbTraceReplyReceivedSuccess(Ljava/lang/String;JJ)V
    .locals 4

    .prologue
    .line 410746
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410747
    :goto_0
    return-void

    .line 410748
    :cond_0
    iget-object v0, p0, LX/2S7;->m:LX/1sj;

    invoke-virtual {v0, p1}, LX/1sj;->b(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v0

    .line 410749
    invoke-static {v0}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    .line 410750
    invoke-static {v1, p2, p3, p4, p5}, LX/2S7;->a(LX/2gQ;JJ)V

    .line 410751
    const-string v2, "success"

    const-string v3, "true"

    invoke-interface {v1, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410752
    const-string v2, "op"

    const-string v3, "engine_to_app_send"

    invoke-interface {v1, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410753
    const-string v2, "service"

    const-string v3, "sender_webrtc_application_layer"

    invoke-interface {v1, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410754
    iget-object v2, p0, LX/2S7;->m:LX/1sj;

    sget-object v3, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    invoke-virtual {v2, v0, v3, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    goto :goto_0
.end method

.method public final logFbTraceReplySentFailure(Ljava/lang/String;JJJJLjava/lang/String;)V
    .locals 10

    .prologue
    .line 410736
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410737
    :goto_0
    return-void

    .line 410738
    :cond_0
    iget-object v0, p0, LX/2S7;->n:Ljava/util/Set;

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 410739
    iget-object v0, p0, LX/2S7;->m:LX/1sj;

    invoke-virtual {v0, p1}, LX/1sj;->b(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v9

    .line 410740
    invoke-static {v9}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    move-object v0, p0

    move-wide v2, p4

    move-wide v4, p2

    move-wide/from16 v6, p6

    move-object/from16 v8, p10

    .line 410741
    invoke-direct/range {v0 .. v8}, LX/2S7;->a(LX/2gQ;JJJLjava/lang/String;)V

    .line 410742
    const-string v0, "sender_id"

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410743
    const-string v0, "op"

    const-string v2, "app_to_engine_receive"

    invoke-interface {v1, v0, v2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410744
    const-string v0, "service"

    const-string v2, "receiver_webrtc_engine_layer"

    invoke-interface {v1, v0, v2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410745
    iget-object v0, p0, LX/2S7;->m:LX/1sj;

    sget-object v2, LX/2gR;->RESPONSE_SEND:LX/2gR;

    invoke-virtual {v0, v9, v2, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    goto :goto_0
.end method

.method public final logFbTraceReplySentSuccess(Ljava/lang/String;Ljava/lang/String;JJJJ)V
    .locals 9

    .prologue
    .line 410724
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410725
    :goto_0
    return-void

    .line 410726
    :cond_0
    iget-object v0, p0, LX/2S7;->n:Ljava/util/Set;

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 410727
    iget-object v0, p0, LX/2S7;->m:LX/1sj;

    invoke-virtual {v0, p1}, LX/1sj;->b(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v8

    .line 410728
    invoke-static {v8}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    .line 410729
    const-string v0, "msg_type"

    invoke-interface {v1, v0, p2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-wide v2, p5

    move-wide v4, p3

    move-wide/from16 v6, p7

    .line 410730
    invoke-static/range {v1 .. v7}, LX/2S7;->a(LX/2gQ;JJJ)V

    .line 410731
    const-string v0, "sender_id"

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410732
    const-string v0, "success"

    const-string v2, "true"

    invoke-interface {v1, v0, v2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410733
    const-string v0, "op"

    const-string v2, "app_to_engine_receive"

    invoke-interface {v1, v0, v2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410734
    const-string v0, "service"

    const-string v2, "receiver_webrtc_engine_layer"

    invoke-interface {v1, v0, v2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410735
    iget-object v0, p0, LX/2S7;->m:LX/1sj;

    sget-object v2, LX/2gR;->RESPONSE_SEND:LX/2gR;

    invoke-virtual {v0, v8, v2, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    goto :goto_0
.end method

.method public final logFbTraceRequestReceived(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 410717
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410718
    :goto_0
    return-void

    .line 410719
    :cond_0
    iget-object v0, p0, LX/2S7;->m:LX/1sj;

    invoke-virtual {v0, p1}, LX/1sj;->b(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v0

    .line 410720
    invoke-static {v0}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    .line 410721
    const-string v2, "op"

    const-string v3, "app_to_engine_receive"

    invoke-interface {v1, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410722
    const-string v2, "service"

    const-string v3, "receiver_webrtc_engine_layer"

    invoke-interface {v1, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410723
    iget-object v2, p0, LX/2S7;->m:LX/1sj;

    sget-object v3, LX/2gR;->REQUEST_RECEIVE:LX/2gR;

    invoke-virtual {v2, v0, v3, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    goto :goto_0
.end method

.method public final logFbTraceRequestSent(Ljava/lang/String;JJJJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    .line 410696
    iget-object v2, p0, LX/2S7;->n:Ljava/util/Set;

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/2S7;->o:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 410697
    :cond_0
    invoke-static {}, LX/1sj;->b()Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v2

    move-object v10, v2

    .line 410698
    :goto_0
    sget-object v2, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    if-ne v10, v2, :cond_2

    .line 410699
    const-string v2, ""

    .line 410700
    :goto_1
    return-object v2

    .line 410701
    :cond_1
    iget-object v2, p0, LX/2S7;->m:LX/1sj;

    const-string v3, "voip_sampling_rate"

    invoke-virtual {v2, v3}, LX/1sj;->a(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v2

    move-object v10, v2

    goto :goto_0

    .line 410702
    :cond_2
    iget-object v2, p0, LX/2S7;->n:Ljava/util/Set;

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 410703
    invoke-static {v10}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v3

    move-wide/from16 v4, p4

    move-wide v6, p2

    move-wide/from16 v8, p6

    .line 410704
    invoke-static/range {v3 .. v9}, LX/2S7;->a(LX/2gQ;JJJ)V

    .line 410705
    const-string v2, "msg_type"

    invoke-interface {v3, v2, p1}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410706
    const-string v2, "recipient_id"

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v2, v4}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410707
    const-string v2, "op"

    const-string v4, "engine_to_app_send"

    invoke-interface {v3, v2, v4}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410708
    const-string v2, "service"

    const-string v4, "sender_webrtc_application_layer"

    invoke-interface {v3, v2, v4}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410709
    const-string v2, "destination"

    move-object/from16 v0, p10

    invoke-interface {v3, v2, v0}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410710
    const-string v2, "retry_count"

    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v2, v4}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410711
    const-string v2, "trigger"

    move-object/from16 v0, p12

    invoke-interface {v3, v2, v0}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410712
    invoke-virtual/range {p13 .. p13}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 410713
    move-object/from16 v0, p13

    invoke-direct {p0, v3, v0}, LX/2S7;->a(LX/2gQ;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 410714
    const-string v2, "callability_raw"

    move-object/from16 v0, p13

    invoke-interface {v3, v2, v0}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410715
    :cond_3
    iget-object v2, p0, LX/2S7;->m:LX/1sj;

    sget-object v4, LX/2gR;->REQUEST_SEND:LX/2gR;

    invoke-virtual {v2, v10, v4, v3}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 410716
    invoke-virtual {v10}, Lcom/facebook/fbtrace/FbTraceNode;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final logIceConnectionState(JI)V
    .locals 5

    .prologue
    .line 410832
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "ice_connection_state"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410833
    const-string v1, "call_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410834
    const-string v1, "state"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410835
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410836
    iget-object v0, p0, LX/2S7;->M:LX/2S9;

    const-string v1, "Ice connection state: call_id[%d] state[%d]"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410837
    return-void
.end method

.method public final logInitialBatteryLevel()V
    .locals 2

    .prologue
    .line 410838
    iget-object v0, p0, LX/2S7;->q:LX/0u7;

    invoke-virtual {v0}, LX/0u7;->a()F

    move-result v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, LX/2S7;->z:I

    .line 410839
    return-void
.end method

.method public final logInternalError(Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 410840
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "internal_error"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410841
    const-string v1, "error"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410842
    const-string v1, "call_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410843
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410844
    sget-object v0, LX/2S7;->b:Ljava/lang/Class;

    const-string v1, "internal error: callid=%d: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 410845
    iget-object v0, p0, LX/2S7;->M:LX/2S9;

    const-string v1, "InternalError call_id[%d], %s"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410846
    return-void
.end method

.method public final logInternalInfo(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 410847
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "info"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410848
    const-string v1, "tag"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410849
    const-string v1, "content"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410850
    const-string v1, "call_id"

    invoke-virtual {v0, v1, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410851
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410852
    iget-object v0, p0, LX/2S7;->M:LX/2S9;

    const-string v1, "call_id[%d] tag[%s]: %s"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2, p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410853
    return-void
.end method

.method public final logReceivedMessage(Ljava/lang/String;JJJJLjava/lang/String;)V
    .locals 8

    .prologue
    .line 410854
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "received_message"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410855
    const-string v3, "type"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410856
    const-string v3, "msg_id"

    invoke-virtual {v2, v3, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410857
    const-wide/16 v4, 0x0

    cmp-long v3, p4, v4

    if-eqz v3, :cond_0

    .line 410858
    const-string v3, "ack_msg_id"

    invoke-virtual {v2, v3, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410859
    :cond_0
    const-string v3, "call_id"

    invoke-virtual {v2, v3, p6, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410860
    const-string v3, "from"

    move-wide/from16 v0, p8

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410861
    const-string v3, "content"

    move-object/from16 v0, p10

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410862
    invoke-static {p0, v2}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410863
    iget-object v2, p0, LX/2S7;->M:LX/2S9;

    const-string v3, "Message received: call_id[%d] msg_id[%d] type[%s] peer_id[%d] contents[%s]"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object p1, v4, v5

    const/4 v5, 0x3

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    aput-object p10, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410864
    return-void
.end method

.method public final logScreenResolution()V
    .locals 3

    .prologue
    .line 410865
    iget-object v0, p0, LX/2S7;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 410866
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 410867
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 410868
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2S7;->a:Ljava/lang/String;

    .line 410869
    return-void
.end method

.method public final logSentMessage(Ljava/lang/String;JJJJLjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 410870
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "sent_message"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410871
    const-string v3, "type"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410872
    const-string v3, "msg_id"

    invoke-virtual {v2, v3, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410873
    const-wide/16 v4, 0x0

    cmp-long v3, p4, v4

    if-eqz v3, :cond_0

    .line 410874
    const-string v3, "ack_msg_id"

    invoke-virtual {v2, v3, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410875
    :cond_0
    const-string v3, "call_id"

    invoke-virtual {v2, v3, p6, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410876
    const-string v3, "to"

    move-wide/from16 v0, p8

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410877
    const-string v3, "content"

    move-object/from16 v0, p11

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410878
    const-string v3, "destination"

    move-object/from16 v0, p10

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410879
    invoke-static {p0, v2}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410880
    iget-object v2, p0, LX/2S7;->M:LX/2S9;

    const-string v3, "Message sent: call_id[%d] msg_id[%d] type[%s] peer_id[%d] destination[%s] contents[%s]"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object p1, v4, v5

    const/4 v5, 0x3

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    aput-object p10, v4, v5

    const/4 v5, 0x5

    aput-object p11, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410881
    return-void
.end method

.method public final logSentMessageFailure(JILjava/lang/String;Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 410882
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "send_failed"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410883
    const-string v1, "msg_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410884
    const-string v1, "error"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410885
    const-string v1, "error_code"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410886
    const-string v1, "error_domain"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410887
    const-string v1, "call_id"

    invoke-virtual {v0, v1, p6, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410888
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410889
    iget-object v0, p0, LX/2S7;->M:LX/2S9;

    const-string v1, "Message failure: call_id[%d] msg_id[%d] domain[%s] code[%d] err_desc[%s]"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object p5, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410890
    return-void
.end method

.method public final logSentMessageSuccess(JJ)V
    .locals 5

    .prologue
    .line 410891
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "send_succeeded"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410892
    const-string v1, "msg_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410893
    const-string v1, "call_id"

    invoke-virtual {v0, v1, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 410894
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410895
    iget-object v0, p0, LX/2S7;->M:LX/2S9;

    const-string v1, "Messege success: call_id[%d] msg_id[%d]"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2S9;->a(Ljava/lang/String;)V

    .line 410896
    return-void
.end method

.method public final pauseLogUpload()V
    .locals 3

    .prologue
    .line 410897
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "control_event"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410898
    const-string v1, "pause_upload"

    const-string v2, "90000"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 410899
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410900
    return-void
.end method

.method public final periodicLogging(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 410901
    invoke-virtual {p0}, LX/2S7;->refreshLogUploadPause()V

    .line 410902
    iget-object v0, p0, LX/2S7;->k:LX/1fX;

    new-instance v1, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$2;-><init>(LX/2S7;JLjava/lang/String;)V

    const v2, -0x176ef7f6

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 410903
    invoke-static {p0}, LX/2S7;->j(LX/2S7;)I

    move-result v0

    .line 410904
    iget v1, p0, LX/2S7;->D:I

    if-eq v0, v1, :cond_0

    .line 410905
    iget v1, p0, LX/2S7;->E:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/2S7;->E:I

    .line 410906
    iput v0, p0, LX/2S7;->D:I

    .line 410907
    :cond_0
    return-void
.end method

.method public final refreshLogUploadPause()V
    .locals 3

    .prologue
    .line 410908
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "control_event"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410909
    const-string v1, "pause_upload"

    const-string v2, "30000"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 410910
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410911
    return-void
.end method

.method public final resumeLogUpload()V
    .locals 3

    .prologue
    .line 410912
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "control_event"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 410913
    const-string v1, "unpause_upload"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 410914
    invoke-static {p0, v0}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 410915
    return-void
.end method

.method public final setLastCallId(J)V
    .locals 1

    .prologue
    .line 410916
    iput-wide p1, p0, LX/2S7;->w:J

    .line 410917
    return-void
.end method

.method public final setUploadLogLevel(I)V
    .locals 0

    .prologue
    .line 410918
    iput p1, p0, LX/2S7;->u:I

    .line 410919
    return-void
.end method
