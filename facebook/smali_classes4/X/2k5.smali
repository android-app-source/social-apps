.class public interface abstract LX/2k5;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 455325
    new-instance v0, LX/0U1;

    const-string v1, "_id"

    const-string v2, "INTEGER PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k5;->a:LX/0U1;

    .line 455326
    new-instance v0, LX/0U1;

    const-string v1, "confirmed_model"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k5;->b:LX/0U1;

    .line 455327
    new-instance v0, LX/0U1;

    const-string v1, "optimistic_model"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k5;->c:LX/0U1;

    .line 455328
    new-instance v0, LX/0U1;

    const-string v1, "class"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k5;->d:LX/0U1;

    .line 455329
    new-instance v0, LX/0U1;

    const-string v1, "model_type"

    const-string v2, "BLOB NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k5;->e:LX/0U1;

    .line 455330
    new-instance v0, LX/0U1;

    const-string v1, "session_id"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k5;->f:LX/0U1;

    .line 455331
    new-instance v0, LX/0U1;

    const-string v1, "user_id"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k5;->g:LX/0U1;

    .line 455332
    new-instance v0, LX/0U1;

    const-string v1, "flags"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k5;->h:LX/0U1;

    .line 455333
    new-instance v0, LX/0U1;

    const-string v1, "version"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k5;->i:LX/0U1;

    .line 455334
    new-instance v0, LX/0U1;

    const-string v1, "sort_key"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k5;->j:LX/0U1;

    .line 455335
    new-instance v0, LX/0U1;

    const-string v1, "tags"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2k5;->k:LX/0U1;

    return-void
.end method
