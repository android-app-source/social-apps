.class public final enum LX/3OZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3OZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3OZ;

.field public static final enum CHAT_CONTEXTS:LX/3OZ;

.field public static final enum LOCATION:LX/3OZ;

.field public static final enum NEARBY_FRIENDS:LX/3OZ;

.field public static final enum NEARBY_FRIENDS_SECTION:LX/3OZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 560595
    new-instance v0, LX/3OZ;

    const-string v1, "LOCATION"

    invoke-direct {v0, v1, v2}, LX/3OZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OZ;->LOCATION:LX/3OZ;

    .line 560596
    new-instance v0, LX/3OZ;

    const-string v1, "CHAT_CONTEXTS"

    invoke-direct {v0, v1, v3}, LX/3OZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OZ;->CHAT_CONTEXTS:LX/3OZ;

    .line 560597
    new-instance v0, LX/3OZ;

    const-string v1, "NEARBY_FRIENDS"

    invoke-direct {v0, v1, v4}, LX/3OZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OZ;->NEARBY_FRIENDS:LX/3OZ;

    .line 560598
    new-instance v0, LX/3OZ;

    const-string v1, "NEARBY_FRIENDS_SECTION"

    invoke-direct {v0, v1, v5}, LX/3OZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OZ;->NEARBY_FRIENDS_SECTION:LX/3OZ;

    .line 560599
    const/4 v0, 0x4

    new-array v0, v0, [LX/3OZ;

    sget-object v1, LX/3OZ;->LOCATION:LX/3OZ;

    aput-object v1, v0, v2

    sget-object v1, LX/3OZ;->CHAT_CONTEXTS:LX/3OZ;

    aput-object v1, v0, v3

    sget-object v1, LX/3OZ;->NEARBY_FRIENDS:LX/3OZ;

    aput-object v1, v0, v4

    sget-object v1, LX/3OZ;->NEARBY_FRIENDS_SECTION:LX/3OZ;

    aput-object v1, v0, v5

    sput-object v0, LX/3OZ;->$VALUES:[LX/3OZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560600
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3OZ;
    .locals 1

    .prologue
    .line 560601
    const-class v0, LX/3OZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3OZ;

    return-object v0
.end method

.method public static values()[LX/3OZ;
    .locals 1

    .prologue
    .line 560602
    sget-object v0, LX/3OZ;->$VALUES:[LX/3OZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3OZ;

    return-object v0
.end method
