.class public LX/2Yl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

.field public b:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

.field public c:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

.field private d:Z

.field private e:Z

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:J

.field public k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 421312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421313
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    iput-object v0, p0, LX/2Yl;->a:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 421314
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    iput-object v0, p0, LX/2Yl;->b:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    .line 421315
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;->NOOP:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    iput-object v0, p0, LX/2Yl;->c:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    .line 421316
    iput-boolean v1, p0, LX/2Yl;->d:Z

    .line 421317
    iput-boolean v1, p0, LX/2Yl;->e:Z

    .line 421318
    iput-object v2, p0, LX/2Yl;->f:Ljava/lang/String;

    .line 421319
    iput-object v2, p0, LX/2Yl;->g:Ljava/lang/String;

    .line 421320
    iput-object v2, p0, LX/2Yl;->h:Ljava/lang/String;

    .line 421321
    iput-object v2, p0, LX/2Yl;->i:Ljava/lang/String;

    .line 421322
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2Yl;->j:J

    .line 421323
    iput-object v2, p0, LX/2Yl;->k:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final j()Lcom/facebook/aldrin/status/AldrinUserStatus;
    .locals 1

    .prologue
    .line 421324
    new-instance v0, Lcom/facebook/aldrin/status/AldrinUserStatus;

    invoke-direct {v0, p0}, Lcom/facebook/aldrin/status/AldrinUserStatus;-><init>(LX/2Yl;)V

    return-object v0
.end method
