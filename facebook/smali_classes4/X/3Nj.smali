.class public LX/3Nj;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/animatablelistview/AnimatingItemView;

.field public b:Z

.field public c:I

.field public d:LX/3Np;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 559783
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Nj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 559784
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 559785
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3Nj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 559786
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    .line 559787
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 559788
    sget-object v0, LX/03r;->SlidingOutSuggestionView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 559789
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v2

    .line 559790
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 559791
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v3

    .line 559792
    const/16 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 559793
    iget v4, p0, LX/3Nj;->c:I

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, LX/3Nj;->c:I

    .line 559794
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 559795
    :cond_1
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 559796
    return-void
.end method

.method public static f(LX/3Nj;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 559797
    iget-boolean v0, p0, LX/3Nj;->b:Z

    if-eqz v0, :cond_0

    .line 559798
    :goto_0
    return-void

    .line 559799
    :cond_0
    iget-object v0, p0, LX/3Nj;->a:Lcom/facebook/widget/animatablelistview/AnimatingItemView;

    invoke-virtual {v0}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 559800
    :goto_1
    sget-object v2, LX/3Nq;->a:[I

    iget-object v3, p0, LX/3Nj;->d:LX/3Np;

    invoke-virtual {v3}, LX/3Np;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 559801
    :cond_1
    :goto_2
    :pswitch_0
    sget-object v0, LX/3Np;->WHATEVER:LX/3Np;

    iput-object v0, p0, LX/3Nj;->d:LX/3Np;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 559802
    goto :goto_1

    .line 559803
    :pswitch_1
    iget-object v0, p0, LX/3Nj;->a:Lcom/facebook/widget/animatablelistview/AnimatingItemView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->setVisibility(I)V

    goto :goto_2

    .line 559804
    :pswitch_2
    iget-object v0, p0, LX/3Nj;->a:Lcom/facebook/widget/animatablelistview/AnimatingItemView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->setVisibility(I)V

    goto :goto_2

    .line 559805
    :pswitch_3
    if-eqz v0, :cond_1

    .line 559806
    const/4 v9, 0x1

    .line 559807
    iget v4, p0, LX/3Nj;->c:I

    if-ne v4, v9, :cond_3

    const/high16 v4, 0x3f800000    # 1.0f

    .line 559808
    :goto_3
    new-instance v5, LX/JEV;

    invoke-direct {v5}, LX/JEV;-><init>()V

    .line 559809
    iget-object v6, p0, LX/3Nj;->a:Lcom/facebook/widget/animatablelistview/AnimatingItemView;

    invoke-virtual {v6, v5}, Lcom/facebook/widget/animatablelistview/AnimatingItemView;->setItemInfo(LX/JEV;)V

    .line 559810
    const-string v6, "animationOffset"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    aput v4, v7, v8

    const/4 v4, 0x0

    aput v4, v7, v9

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 559811
    const-wide/16 v6, 0x12c

    invoke-virtual {v4, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 559812
    new-instance v6, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v6}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v4, v6}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 559813
    new-instance v6, LX/Idx;

    invoke-direct {v6, p0, v5}, LX/Idx;-><init>(LX/3Nj;LX/JEV;)V

    invoke-virtual {v4, v6}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 559814
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 559815
    iput-boolean v9, p0, LX/3Nj;->b:Z

    .line 559816
    goto :goto_2

    .line 559817
    :cond_3
    const/high16 v4, -0x40800000    # -1.0f

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getAnimateOutDirection()I
    .locals 1

    .prologue
    .line 559818
    iget v0, p0, LX/3Nj;->c:I

    return v0
.end method

.method public setAnimateOutDirection(I)V
    .locals 0

    .prologue
    .line 559819
    iput p1, p0, LX/3Nj;->c:I

    .line 559820
    return-void
.end method
