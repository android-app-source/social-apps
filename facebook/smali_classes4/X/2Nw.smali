.class public LX/2Nw;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final e:LX/1Hb;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/1Hn;

.field private final d:Ljava/security/SecureRandom;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 400248
    const-class v0, LX/2Nw;

    sput-object v0, LX/2Nw;->a:Ljava/lang/Class;

    .line 400249
    const-string v0, ""

    invoke-static {v0}, LX/1Hb;->a(Ljava/lang/String;)LX/1Hb;

    move-result-object v0

    sput-object v0, LX/2Nw;->e:LX/1Hb;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1Hn;LX/1Hr;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 400191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400192
    iput-object p1, p0, LX/2Nw;->b:Landroid/content/Context;

    .line 400193
    iput-object p2, p0, LX/2Nw;->c:LX/1Hn;

    .line 400194
    invoke-static {}, LX/1Hr;->a()Ljava/security/SecureRandom;

    move-result-object v0

    iput-object v0, p0, LX/2Nw;->d:Ljava/security/SecureRandom;

    .line 400195
    return-void
.end method

.method private static a(LX/2Nw;Ljava/lang/String;)LX/1Hz;
    .locals 4

    .prologue
    .line 400246
    new-instance v0, LX/6bh;

    sget-object v1, LX/1Hy;->KEY_256:LX/1Hy;

    iget-object v2, p0, LX/2Nw;->d:Ljava/security/SecureRandom;

    const/4 v3, 0x0

    invoke-static {p1, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/6bh;-><init>(LX/1Hy;Ljava/security/SecureRandom;[B)V

    .line 400247
    iget-object v1, p0, LX/2Nw;->c:LX/1Hn;

    invoke-virtual {v1, v0}, LX/1Ho;->c(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/2Nw;
    .locals 1

    .prologue
    .line 400245
    invoke-static {p0}, LX/2Nw;->b(LX/0QB;)LX/2Nw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 400241
    const/high16 v0, 0x20000

    new-array v0, v0, [B

    .line 400242
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 400243
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 400244
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/2Nw;
    .locals 4

    .prologue
    .line 400239
    new-instance v3, LX/2Nw;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1Hn;->a(LX/0QB;)LX/1Hn;

    move-result-object v1

    check-cast v1, LX/1Hn;

    invoke-static {p0}, LX/1Hr;->a(LX/0QB;)LX/1Hr;

    move-result-object v2

    check-cast v2, LX/1Hr;

    invoke-direct {v3, v0, v1, v2}, LX/2Nw;-><init>(Landroid/content/Context;LX/1Hn;LX/1Hr;)V

    .line 400240
    return-object v3
.end method

.method private static b(Landroid/net/Uri;)Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 400238
    new-instance v0, Ljava/io/FileInputStream;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 400218
    invoke-static {p0, p2}, LX/2Nw;->a(LX/2Nw;Ljava/lang/String;)LX/1Hz;

    move-result-object v1

    .line 400219
    invoke-virtual {v1}, LX/1Hz;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 400220
    :goto_0
    return-object v0

    .line 400221
    :cond_0
    iget-object v7, p0, LX/2Nw;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    .line 400222
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "attachement"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 400223
    sget-object v10, LX/0SF;->a:LX/0SF;

    move-object v10, v10

    .line 400224
    invoke-virtual {v10}, LX/0SF;->a()J

    move-result-wide v11

    invoke-virtual {v9, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v7, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v2, v8

    .line 400225
    invoke-static {p1}, LX/2Nw;->b(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v3

    .line 400226
    :try_start_0
    new-instance v4, Ljava/io/BufferedOutputStream;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v4, v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 400227
    :try_start_1
    sget-object v5, LX/2Nw;->e:LX/1Hb;

    .line 400228
    const/4 v6, 0x0

    invoke-virtual {v1, v4, v5, v6}, LX/1Hz;->a(Ljava/io/OutputStream;LX/1Hb;[B)Ljava/io/OutputStream;

    move-result-object v6

    move-object v5, v6
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 400229
    :try_start_2
    invoke-static {v3, v5}, LX/2Nw;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 400230
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    move-result-object v1

    .line 400231
    if-eqz v5, :cond_1

    :try_start_3
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :cond_1
    if-eqz v4, :cond_2

    :try_start_4
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_3
    move-object v0, v1

    goto :goto_0

    .line 400232
    :catch_0
    move-exception v1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 400233
    :catchall_0
    move-exception v2

    move-object v6, v2

    move-object v2, v1

    move-object v1, v6

    :goto_1
    if-eqz v5, :cond_4

    if-eqz v2, :cond_7

    :try_start_6
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :cond_4
    :goto_2
    :try_start_7
    throw v1
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 400234
    :catch_1
    move-exception v1

    :try_start_8
    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 400235
    :catchall_1
    move-exception v2

    move-object v6, v2

    move-object v2, v1

    move-object v1, v6

    :goto_3
    if-eqz v4, :cond_5

    if-eqz v2, :cond_8

    :try_start_9
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    :cond_5
    :goto_4
    :try_start_a
    throw v1
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 400236
    :catch_2
    move-exception v0

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 400237
    :catchall_2
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_5
    if-eqz v3, :cond_6

    if-eqz v1, :cond_9

    :try_start_c
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_5

    :cond_6
    :goto_6
    throw v0

    :catch_3
    move-exception v5

    :try_start_d
    invoke-static {v2, v5}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :catchall_3
    move-exception v1

    move-object v2, v0

    goto :goto_3

    :cond_7
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_2

    :catch_4
    move-exception v4

    :try_start_e
    invoke-static {v2, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :catchall_4
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_5

    :cond_8
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    goto :goto_4

    :catch_5
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_9
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_6

    :catchall_5
    move-exception v1

    move-object v2, v0

    goto :goto_1
.end method

.method public final b(Landroid/net/Uri;Ljava/lang/String;)[B
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 400196
    invoke-static {p0, p2}, LX/2Nw;->a(LX/2Nw;Ljava/lang/String;)LX/1Hz;

    move-result-object v1

    .line 400197
    invoke-virtual {v1}, LX/1Hz;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 400198
    sget-object v1, LX/2Nw;->a:Ljava/lang/Class;

    const-string v2, "Crypto is not available"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 400199
    :goto_0
    return-object v0

    .line 400200
    :cond_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    .line 400201
    new-instance v7, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 400202
    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v9

    .line 400203
    const-wide/32 v7, 0x7fffffff

    cmp-long v7, v9, v7

    if-gtz v7, :cond_8

    const/4 v7, 0x1

    :goto_1
    invoke-static {v7}, LX/0PB;->checkState(Z)V

    .line 400204
    long-to-int v7, v9

    move v2, v7

    .line 400205
    invoke-direct {v3, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 400206
    :try_start_0
    invoke-static {p1}, LX/2Nw;->b(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    move-result-object v4

    .line 400207
    :try_start_1
    sget-object v2, LX/2Nw;->e:LX/1Hb;

    invoke-virtual {v1, v4, v2}, LX/1Hz;->a(Ljava/io/InputStream;LX/1Hb;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v5

    .line 400208
    :try_start_2
    invoke-static {v5, v3}, LX/2Nw;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 400209
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    move-result-object v1

    .line 400210
    if-eqz v5, :cond_1

    :try_start_3
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :cond_1
    if-eqz v4, :cond_2

    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    :cond_2
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V

    move-object v0, v1

    goto :goto_0

    .line 400211
    :catch_0
    move-exception v1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 400212
    :catchall_0
    move-exception v2

    move-object v6, v2

    move-object v2, v1

    move-object v1, v6

    :goto_2
    if-eqz v5, :cond_3

    if-eqz v2, :cond_5

    :try_start_6
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :cond_3
    :goto_3
    :try_start_7
    throw v1
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 400213
    :catch_1
    move-exception v1

    :try_start_8
    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 400214
    :catchall_1
    move-exception v2

    move-object v6, v2

    move-object v2, v1

    move-object v1, v6

    :goto_4
    if-eqz v4, :cond_4

    if-eqz v2, :cond_6

    :try_start_9
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    :cond_4
    :goto_5
    :try_start_a
    throw v1
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 400215
    :catch_2
    move-exception v0

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 400216
    :catchall_2
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_6
    if-eqz v1, :cond_7

    :try_start_c
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_5

    :goto_7
    throw v0

    :catch_3
    move-exception v5

    :try_start_d
    invoke-static {v2, v5}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :catchall_3
    move-exception v1

    move-object v2, v0

    goto :goto_4

    :cond_5
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_3

    :catch_4
    move-exception v4

    :try_start_e
    invoke-static {v2, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :catchall_4
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_6

    :cond_6
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    goto :goto_5

    :catch_5
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_7

    :cond_7
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V

    goto :goto_7

    :catchall_5
    move-exception v1

    move-object v2, v0

    goto :goto_2

    .line 400217
    :cond_8
    const/4 v7, 0x0

    goto :goto_1
.end method
