.class public LX/2qc;
.super LX/2ol;
.source ""


# instance fields
.field public final a:LX/04g;

.field public final b:I


# direct methods
.method public constructor <init>(ILX/04g;)V
    .locals 0

    .prologue
    .line 471359
    invoke-direct {p0}, LX/2ol;-><init>()V

    .line 471360
    iput p1, p0, LX/2qc;->b:I

    .line 471361
    iput-object p2, p0, LX/2qc;->a:LX/04g;

    .line 471362
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 471363
    const-string v0, "%s: %s, time: %d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LX/2qc;->a:LX/04g;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, LX/2qc;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
