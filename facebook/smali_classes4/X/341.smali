.class public final enum LX/341;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/341;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/341;

.field public static final enum BEFORE_CREATE:LX/341;

.field public static final enum BEFORE_RESUME:LX/341;

.field public static final enum RESUMED:LX/341;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 494432
    new-instance v0, LX/341;

    const-string v1, "BEFORE_CREATE"

    invoke-direct {v0, v1, v2}, LX/341;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/341;->BEFORE_CREATE:LX/341;

    .line 494433
    new-instance v0, LX/341;

    const-string v1, "BEFORE_RESUME"

    invoke-direct {v0, v1, v3}, LX/341;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/341;->BEFORE_RESUME:LX/341;

    .line 494434
    new-instance v0, LX/341;

    const-string v1, "RESUMED"

    invoke-direct {v0, v1, v4}, LX/341;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/341;->RESUMED:LX/341;

    .line 494435
    const/4 v0, 0x3

    new-array v0, v0, [LX/341;

    sget-object v1, LX/341;->BEFORE_CREATE:LX/341;

    aput-object v1, v0, v2

    sget-object v1, LX/341;->BEFORE_RESUME:LX/341;

    aput-object v1, v0, v3

    sget-object v1, LX/341;->RESUMED:LX/341;

    aput-object v1, v0, v4

    sput-object v0, LX/341;->$VALUES:[LX/341;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 494436
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/341;
    .locals 1

    .prologue
    .line 494437
    const-class v0, LX/341;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/341;

    return-object v0
.end method

.method public static values()[LX/341;
    .locals 1

    .prologue
    .line 494438
    sget-object v0, LX/341;->$VALUES:[LX/341;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/341;

    return-object v0
.end method
