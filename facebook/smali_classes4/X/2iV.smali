.class public final LX/2iV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2iW;


# instance fields
.field public final synthetic a:LX/2iK;


# direct methods
.method public constructor <init>(LX/2iK;)V
    .locals 0

    .prologue
    .line 451776
    iput-object p1, p0, LX/2iV;->a:LX/2iK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/friends/model/FriendRequest;LX/2na;)V
    .locals 7

    .prologue
    .line 451777
    iget-object v0, p0, LX/2iV;->a:LX/2iK;

    iget-object v0, v0, LX/2iK;->e:LX/2iU;

    .line 451778
    iget-object v1, v0, LX/2iU;->d:LX/2hs;

    invoke-virtual {v1}, LX/2hs;->d()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 451779
    invoke-static {v0}, LX/2iU;->a(LX/2iU;)V

    .line 451780
    invoke-virtual {p1}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v3

    .line 451781
    iget-object v1, v0, LX/2iU;->a:LX/2dj;

    iget-object v2, v0, LX/2iU;->f:LX/2h7;

    iget-object v2, v2, LX/2h7;->friendRequestResponseRef:LX/2hA;

    invoke-virtual {v1, v3, v4, p2, v2}, LX/2dj;->a(JLX/2na;LX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 451782
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 451783
    sget-object v5, LX/2na;->REJECT:LX/2na;

    if-ne p2, v5, :cond_0

    .line 451784
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 451785
    :cond_0
    const/4 v5, 0x1

    invoke-static {v0, v3, v4, v1, v5}, LX/2iU;->a$redex0(LX/2iU;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 451786
    iget-object v1, v0, LX/2iU;->h:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "RESPOND_TO_FRIEND_REQUEST_TASK"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/EyY;

    invoke-direct {v4, v0, p1}, LX/EyY;-><init>(LX/2iU;Lcom/facebook/friends/model/FriendRequest;)V

    invoke-virtual {v1, v3, v2, v4}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 451787
    return-void
.end method
