.class public final LX/36P;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/1Uf;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "LX/36L;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0lF;

.field private final d:I


# direct methods
.method public constructor <init>(LX/1Uf;LX/0Px;LX/0lF;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/36L;",
            ">;",
            "LX/0lF;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 498814
    iput-object p1, p0, LX/36P;->a:LX/1Uf;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 498815
    iput-object p2, p0, LX/36P;->b:Ljava/util/List;

    .line 498816
    iput-object p3, p0, LX/36P;->c:LX/0lF;

    .line 498817
    iput p4, p0, LX/36P;->d:I

    .line 498818
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 498771
    iget-object v0, p0, LX/36P;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/36P;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 498772
    :cond_0
    :goto_0
    return-void

    .line 498773
    :cond_1
    iget-object v0, p0, LX/36P;->a:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/36P;->c:LX/0lF;

    const-string v1, "native_newsfeed"

    .line 498774
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v2

    if-nez v2, :cond_5

    .line 498775
    :cond_2
    const/4 v2, 0x0

    .line 498776
    :goto_1
    move-object v0, v2

    .line 498777
    iget-object v1, p0, LX/36P;->a:LX/1Uf;

    iget-object v1, v1, LX/1Uf;->l:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 498778
    const/4 v1, 0x0

    .line 498779
    iget-object v0, p0, LX/36P;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/36L;

    invoke-interface {v0}, LX/36L;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 498780
    if-eqz v0, :cond_3

    .line 498781
    iget-object v0, p0, LX/36P;->a:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8qT;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/36P;->b:Ljava/util/List;

    .line 498782
    iget-object v3, v0, LX/8qT;->a:LX/1nJ;

    .line 498783
    iget-boolean p0, v3, LX/1nJ;->a:Z

    move v3, p0

    .line 498784
    if-eqz v3, :cond_7

    .line 498785
    :goto_3
    goto :goto_0

    .line 498786
    :cond_3
    iget-object v0, p0, LX/36P;->a:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8qT;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 498787
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 498788
    iget-object v2, p0, LX/36P;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/36L;

    .line 498789
    invoke-interface {v2}, LX/36L;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 498790
    :cond_4
    move-object v2, v3

    .line 498791
    invoke-virtual {v0, v1, v2}, LX/8qT;->a(Landroid/content/Context;Ljava/util/List;)V

    goto :goto_0

    .line 498792
    :cond_5
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "open_people_list"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tracking"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 498793
    iput-object v1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 498794
    move-object v2, v2

    .line 498795
    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2

    .line 498796
    :cond_7
    new-instance v3, LX/8qQ;

    invoke-direct {v3}, LX/8qQ;-><init>()V

    new-instance p0, LX/8qS;

    invoke-direct {p0, v0}, LX/8qS;-><init>(LX/8qT;)V

    invoke-static {v2, p0}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p0

    .line 498797
    iput-object p0, v3, LX/8qQ;->b:Ljava/util/List;

    .line 498798
    move-object v3, v3

    .line 498799
    sget-object p0, LX/89l;->PROFILES:LX/89l;

    .line 498800
    iput-object p0, v3, LX/8qQ;->d:LX/89l;

    .line 498801
    move-object v3, v3

    .line 498802
    const/4 p0, 0x1

    .line 498803
    iput-boolean p0, v3, LX/8qQ;->g:Z

    .line 498804
    move-object v3, v3

    .line 498805
    const/4 p0, 0x0

    .line 498806
    iput-boolean p0, v3, LX/8qQ;->f:Z

    .line 498807
    move-object v3, v3

    .line 498808
    invoke-virtual {v3}, LX/8qQ;->a()Lcom/facebook/ufiservices/flyout/ProfileListParams;

    move-result-object v3

    .line 498809
    invoke-static {v0, v3, v1}, LX/8qT;->a(LX/8qT;Lcom/facebook/ufiservices/flyout/ProfileListParams;Landroid/content/Context;)V

    goto :goto_3
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 498810
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 498811
    iget-object v0, p0, LX/36P;->a:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, LX/36P;->d:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 498812
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 498813
    return-void
.end method
