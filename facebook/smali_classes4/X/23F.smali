.class public final synthetic LX/23F;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 363529
    invoke-static {}, LX/18G;->values()[LX/18G;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/23F;->b:[I

    :try_start_0
    sget-object v0, LX/23F;->b:[I

    sget-object v1, LX/18G;->SUCCESS:LX/18G;

    invoke-virtual {v1}, LX/18G;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v0, LX/23F;->b:[I

    sget-object v1, LX/18G;->CANCELLATION:LX/18G;

    invoke-virtual {v1}, LX/18G;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_1
    :try_start_2
    sget-object v0, LX/23F;->b:[I

    sget-object v1, LX/18G;->EMPTY:LX/18G;

    invoke-virtual {v1}, LX/18G;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    :try_start_3
    sget-object v0, LX/23F;->b:[I

    sget-object v1, LX/18G;->SERVICE_EXCEPTION:LX/18G;

    invoke-virtual {v1}, LX/18G;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    .line 363530
    :goto_3
    invoke-static {}, LX/23E;->values()[LX/23E;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/23F;->a:[I

    :try_start_4
    sget-object v0, LX/23F;->a:[I

    sget-object v1, LX/23E;->AFTER:LX/23E;

    invoke-virtual {v1}, LX/23E;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v0, LX/23F;->a:[I

    sget-object v1, LX/23E;->BEFORE:LX/23E;

    invoke-virtual {v1}, LX/23E;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    return-void

    :catch_0
    goto :goto_5

    :catch_1
    goto :goto_4

    :catch_2
    goto :goto_3

    :catch_3
    goto :goto_2

    :catch_4
    goto :goto_1

    :catch_5
    goto :goto_0
.end method
