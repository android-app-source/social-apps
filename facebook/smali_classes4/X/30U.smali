.class public LX/30U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6Bb;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0W3;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0W3;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/assetdownload/IsInAssetDownloadMainGatekeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6Bb;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 484521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484522
    iput-object p1, p0, LX/30U;->b:LX/0Or;

    .line 484523
    iput-object p2, p0, LX/30U;->c:LX/0Or;

    .line 484524
    iput-object p3, p0, LX/30U;->d:LX/0W3;

    .line 484525
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 484531
    iget-object v0, p0, LX/30U;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 484530
    sget-object v0, LX/2Jm;->INTERVAL:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<+",
            "LX/2Jw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 484529
    iget-object v0, p0, LX/30U;->b:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 2

    .prologue
    .line 484527
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jh;->BACKGROUND:LX/2Jh;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jh;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Ji;->NOT_LOW:LX/2Ji;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Ji;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jq;->LOGGED_IN:LX/2Jq;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jq;)LX/2Jk;

    move-result-object v0

    .line 484528
    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 6

    .prologue
    .line 484526
    iget-object v0, p0, LX/30U;->d:LX/0W3;

    sget-wide v2, LX/0X5;->m:J

    const-wide/32 v4, 0x1499700

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method
