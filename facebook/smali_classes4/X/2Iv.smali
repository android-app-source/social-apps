.class public LX/2Iv;
.super LX/1qS;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Iv;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/2Iw;LX/2JD;LX/30i;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392391
    invoke-static {p4, p5, p6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "contacts_db2"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 392392
    return-void
.end method

.method public static a(LX/0QB;)LX/2Iv;
    .locals 10

    .prologue
    .line 392375
    sget-object v0, LX/2Iv;->a:LX/2Iv;

    if-nez v0, :cond_1

    .line 392376
    const-class v1, LX/2Iv;

    monitor-enter v1

    .line 392377
    :try_start_0
    sget-object v0, LX/2Iv;->a:LX/2Iv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 392378
    if-eqz v2, :cond_0

    .line 392379
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 392380
    new-instance v3, LX/2Iv;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v5

    check-cast v5, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v6

    check-cast v6, LX/1qU;

    invoke-static {v0}, LX/2Iw;->a(LX/0QB;)LX/2Iw;

    move-result-object v7

    check-cast v7, LX/2Iw;

    invoke-static {v0}, LX/2JD;->a(LX/0QB;)LX/2JD;

    move-result-object v8

    check-cast v8, LX/2JD;

    invoke-static {v0}, LX/30i;->a(LX/0QB;)LX/30i;

    move-result-object v9

    check-cast v9, LX/30i;

    invoke-direct/range {v3 .. v9}, LX/2Iv;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/2Iw;LX/2JD;LX/30i;)V

    .line 392381
    move-object v0, v3

    .line 392382
    sput-object v0, LX/2Iv;->a:LX/2Iv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392383
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 392384
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392385
    :cond_1
    sget-object v0, LX/2Iv;->a:LX/2Iv;

    return-object v0

    .line 392386
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 392387
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final d()I
    .locals 1

    .prologue
    .line 392390
    const v0, 0x19000

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 392388
    invoke-super {p0}, LX/1qS;->h()V

    .line 392389
    return-void
.end method
