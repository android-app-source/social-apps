.class public LX/2JF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9An;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Or;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/9An;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392727
    iput-object p1, p0, LX/2JF;->b:LX/0Or;

    .line 392728
    iput-object p2, p0, LX/2JF;->c:LX/0ad;

    .line 392729
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 392730
    iget-object v0, p0, LX/2JF;->c:LX/0ad;

    sget-short v1, LX/1sg;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 392731
    sget-object v0, LX/2Jm;->INTERVAL:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<+",
            "LX/2Jw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392732
    iget-object v0, p0, LX/2JF;->b:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 2

    .prologue
    .line 392733
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jh;->BACKGROUND:LX/2Jh;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jh;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Ji;->NOT_LOW:LX/2Ji;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Ji;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jq;->LOGGED_IN:LX/2Jq;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jq;)LX/2Jk;

    move-result-object v0

    .line 392734
    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 4

    .prologue
    .line 392735
    iget-object v0, p0, LX/2JF;->c:LX/0ad;

    sget v1, LX/1sg;->c:I

    const/16 v2, 0x18

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x36ee80

    mul-long/2addr v0, v2

    return-wide v0
.end method
