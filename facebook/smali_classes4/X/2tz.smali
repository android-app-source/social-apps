.class public LX/2tz;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BOi;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/2tz",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BOi;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 475424
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 475425
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/2tz;->b:LX/0Zi;

    .line 475426
    iput-object p1, p0, LX/2tz;->a:LX/0Ot;

    .line 475427
    return-void
.end method

.method public static a(LX/0QB;)LX/2tz;
    .locals 4

    .prologue
    .line 475457
    const-class v1, LX/2tz;

    monitor-enter v1

    .line 475458
    :try_start_0
    sget-object v0, LX/2tz;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 475459
    sput-object v2, LX/2tz;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 475460
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475461
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 475462
    new-instance v3, LX/2tz;

    const/16 p0, 0x35aa

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2tz;-><init>(LX/0Ot;)V

    .line 475463
    move-object v0, v3

    .line 475464
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 475465
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2tz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 475466
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 475467
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 475438
    check-cast p2, LX/2u4;

    .line 475439
    iget-object v0, p0, LX/2tz;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BOi;

    iget-object v1, p2, LX/2u4;->a:LX/1Ri;

    .line 475440
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    .line 475441
    iget-object v3, v0, LX/BOi;->b:LX/5JV;

    invoke-virtual {v3, p1}, LX/5JV;->c(LX/1De;)LX/5JT;

    move-result-object v3

    const v4, 0x7f020874

    invoke-virtual {v3, v4}, LX/5JT;->h(I)LX/5JT;

    move-result-object v3

    iget-object v4, v0, LX/BOi;->a:Landroid/content/res/Resources;

    const v5, 0x7f0a00d5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 475442
    iget-object v5, v3, LX/5JT;->a:LX/5JU;

    iput v4, v5, LX/5JU;->b:I

    .line 475443
    iget-object v5, v3, LX/5JT;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 475444
    move-object v3, v3

    .line 475445
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1bb4

    invoke-interface {v3, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1bb4

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v4

    iget-object v5, v0, LX/BOi;->a:Landroid/content/res/Resources;

    const p0, 0x7f0218ad

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dc;)LX/1Di;

    move-result-object v3

    const/16 v4, 0x8

    const v5, 0x7f0b1bb5

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x4

    const v5, 0x7f0b1bb6

    invoke-interface {v3, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    const v5, 0x7f0b0060

    invoke-interface {v3, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x5

    const v5, 0x7f0b1bb7

    invoke-interface {v3, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 475446
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 475447
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x7

    const v5, 0x7f0b0060

    invoke-interface {v3, v4, v5}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    move-object v3, v3

    .line 475448
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    iget-object v5, v0, LX/BOi;->a:Landroid/content/res/Resources;

    const p0, 0x7f08289b

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0050

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00a8

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    sget-object v5, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p0, LX/0xr;->MEDIUM:LX/0xr;

    const/4 p2, 0x0

    invoke-static {p1, v5, p0, p2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    move-object v4, v4

    .line 475449
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    .line 475450
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    iget-object v5, v0, LX/BOi;->a:Landroid/content/res/Resources;

    const p0, 0x7f08289c

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00a4

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    sget-object v5, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p0, LX/0xr;->REGULAR:LX/0xr;

    const/4 p2, 0x0

    invoke-static {p1, v5, p0, p2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x1

    const p0, 0x7f0b1bb8

    invoke-interface {v4, v5, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 475451
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 475452
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    iget-object v5, v0, LX/BOi;->a:Landroid/content/res/Resources;

    const p0, 0x7f08289d

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a008d

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    sget-object v5, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p0, LX/0xr;->BOLD:LX/0xr;

    const/4 p2, 0x0

    invoke-static {p1, v5, p0, p2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x1

    const p0, 0x7f0b1bb9

    invoke-interface {v4, v5, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 475453
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, LX/BOi;->c:LX/BMm;

    invoke-virtual {v3, p1}, LX/BMm;->c(LX/1De;)LX/BMk;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/BMk;->a(LX/1Ri;)LX/BMk;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    .line 475454
    const v3, 0x11f25374

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 475455
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 475456
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 475428
    invoke-static {}, LX/1dS;->b()V

    .line 475429
    iget v0, p1, LX/1dQ;->b:I

    .line 475430
    packed-switch v0, :pswitch_data_0

    .line 475431
    :goto_0
    return-object v2

    .line 475432
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 475433
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 475434
    check-cast v1, LX/2u4;

    .line 475435
    iget-object v3, p0, LX/2tz;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BOi;

    iget-object v4, v1, LX/2u4;->a:LX/1Ri;

    .line 475436
    iget-object p1, v3, LX/BOi;->d:LX/BMP;

    iget-object p2, v4, LX/1Ri;->a:LX/1RN;

    iget-object p0, v3, LX/BOi;->e:LX/B5l;

    iget-object v1, v4, LX/1Ri;->a:LX/1RN;

    invoke-virtual {p0, v1}, LX/B5l;->a(LX/1RN;)LX/B5n;

    move-result-object p0

    invoke-virtual {p0}, LX/B5n;->a()LX/B5p;

    move-result-object p0

    invoke-virtual {p1, v0, p2, p0}, LX/B5s;->a(Landroid/view/View;LX/1RN;LX/B5o;)V

    .line 475437
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x11f25374
        :pswitch_0
    .end packed-switch
.end method
