.class public final enum LX/22w;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/22w;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/22w;

.field public static final enum FINISHED_WITH_DATA:LX/22w;

.field public static final enum FINISHED_WITH_DATA_AT_LEAST_ONCE:LX/22w;

.field public static final enum FINISHED_WITH_NO_DATA:LX/22w;

.field public static final enum NOT_SCHEDULED:LX/22w;

.field public static final enum SCHEDULED:LX/22w;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 361374
    new-instance v0, LX/22w;

    const-string v1, "NOT_SCHEDULED"

    invoke-direct {v0, v1, v2}, LX/22w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/22w;->NOT_SCHEDULED:LX/22w;

    .line 361375
    new-instance v0, LX/22w;

    const-string v1, "SCHEDULED"

    invoke-direct {v0, v1, v3}, LX/22w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/22w;->SCHEDULED:LX/22w;

    .line 361376
    new-instance v0, LX/22w;

    const-string v1, "FINISHED_WITH_DATA"

    invoke-direct {v0, v1, v4}, LX/22w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/22w;->FINISHED_WITH_DATA:LX/22w;

    .line 361377
    new-instance v0, LX/22w;

    const-string v1, "FINISHED_WITH_DATA_AT_LEAST_ONCE"

    invoke-direct {v0, v1, v5}, LX/22w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/22w;->FINISHED_WITH_DATA_AT_LEAST_ONCE:LX/22w;

    .line 361378
    new-instance v0, LX/22w;

    const-string v1, "FINISHED_WITH_NO_DATA"

    invoke-direct {v0, v1, v6}, LX/22w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/22w;->FINISHED_WITH_NO_DATA:LX/22w;

    .line 361379
    const/4 v0, 0x5

    new-array v0, v0, [LX/22w;

    sget-object v1, LX/22w;->NOT_SCHEDULED:LX/22w;

    aput-object v1, v0, v2

    sget-object v1, LX/22w;->SCHEDULED:LX/22w;

    aput-object v1, v0, v3

    sget-object v1, LX/22w;->FINISHED_WITH_DATA:LX/22w;

    aput-object v1, v0, v4

    sget-object v1, LX/22w;->FINISHED_WITH_DATA_AT_LEAST_ONCE:LX/22w;

    aput-object v1, v0, v5

    sget-object v1, LX/22w;->FINISHED_WITH_NO_DATA:LX/22w;

    aput-object v1, v0, v6

    sput-object v0, LX/22w;->$VALUES:[LX/22w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 361380
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/22w;
    .locals 1

    .prologue
    .line 361381
    const-class v0, LX/22w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/22w;

    return-object v0
.end method

.method public static values()[LX/22w;
    .locals 1

    .prologue
    .line 361382
    sget-object v0, LX/22w;->$VALUES:[LX/22w;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/22w;

    return-object v0
.end method


# virtual methods
.method public final canSchedule()Z
    .locals 1

    .prologue
    .line 361383
    sget-object v0, LX/22w;->NOT_SCHEDULED:LX/22w;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/22w;->FINISHED_WITH_DATA:LX/22w;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final finishNetworkTailFetch()Z
    .locals 1

    .prologue
    .line 361384
    sget-object v0, LX/22w;->FINISHED_WITH_DATA:LX/22w;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final finishedWithDataOnce()Z
    .locals 1

    .prologue
    .line 361385
    sget-object v0, LX/22w;->FINISHED_WITH_DATA:LX/22w;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/22w;->FINISHED_WITH_DATA_AT_LEAST_ONCE:LX/22w;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
