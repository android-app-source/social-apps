.class public LX/2sO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/2y5;

.field public static final b:Ljava/lang/String;

.field private static h:LX/0Xm;


# instance fields
.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0Ot",
            "<+",
            "LX/2y5;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:[I

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AEB;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2yR;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/36W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 472529
    new-instance v0, LX/2y4;

    invoke-direct {v0}, LX/2y4;-><init>()V

    sput-object v0, LX/2sO;->a:LX/2y5;

    .line 472530
    const-class v0, LX/2sO;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2sO;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2yR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AEA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AEd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3i1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AEB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AEY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AEM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AEg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AEI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/36W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AEW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 472562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472563
    iput-object p1, p0, LX/2sO;->f:LX/0Ot;

    .line 472564
    iput-object p10, p0, LX/2sO;->g:LX/0Ot;

    .line 472565
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    .line 472566
    iget-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    const v1, 0x4fb5732f

    invoke-virtual {v0, v1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472567
    iget-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    const v1, -0x22a42d2a    # -9.8999738E17f

    invoke-virtual {v0, v1, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472568
    iget-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    const v1, -0x1e53800c

    invoke-virtual {v0, v1, p4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472569
    iget-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    const v1, 0x46a1c4a4

    invoke-virtual {v0, v1, p6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472570
    iget-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    const v1, -0x3625f733

    invoke-virtual {v0, v1, p7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472571
    iget-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    const v1, 0x54643306

    invoke-virtual {v0, v1, p8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472572
    iget-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    const v1, -0x12bcfc94

    invoke-virtual {v0, v1, p9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472573
    iget-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    const v1, -0x4f3adba7

    invoke-virtual {v0, v1, p11}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472574
    iget-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    invoke-static {v0}, LX/2y6;->a(Landroid/util/SparseArray;)[I

    move-result-object v0

    iput-object v0, p0, LX/2sO;->d:[I

    .line 472575
    iput-object p5, p0, LX/2sO;->e:LX/0Ot;

    .line 472576
    return-void
.end method

.method public static a(LX/0QB;)LX/2sO;
    .locals 15

    .prologue
    .line 472551
    const-class v1, LX/2sO;

    monitor-enter v1

    .line 472552
    :try_start_0
    sget-object v0, LX/2sO;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 472553
    sput-object v2, LX/2sO;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 472554
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472555
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 472556
    new-instance v3, LX/2sO;

    const/16 v4, 0x155

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1792

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x179a

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x151

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1793

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1799

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1796

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x179b

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x1795

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x152

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x1798

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, LX/2sO;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 472557
    move-object v0, v3

    .line 472558
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 472559
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2sO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 472560
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 472561
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2y5;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/2y5;"
        }
    .end annotation

    .prologue
    const v3, -0x1e53800c

    .line 472533
    iget-object v0, p0, LX/2sO;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/2yR;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472534
    iget-object v0, p0, LX/2sO;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2y5;

    .line 472535
    :goto_0
    return-object v0

    .line 472536
    :cond_0
    iget-object v0, p0, LX/2sO;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 472537
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 472538
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MESSENGER_GROUP_JOINABLE_LINK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 472539
    if-eqz v0, :cond_1

    .line 472540
    iget-object v0, p0, LX/2sO;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2y5;

    goto :goto_0

    .line 472541
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 472542
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v1, p0, LX/2sO;->d:[I

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;[I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 472543
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-nez v0, :cond_3

    .line 472544
    :cond_2
    sget-object v0, LX/2sO;->a:LX/2y5;

    goto :goto_0

    .line 472545
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-ne v0, v3, :cond_4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 472546
    sget-object v0, LX/2sO;->a:LX/2y5;

    goto :goto_0

    .line 472547
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-ne v0, v3, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    if-eq v0, v2, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP_WITH_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    if-ne v0, v2, :cond_6

    .line 472548
    :cond_5
    iget-object v0, p0, LX/2sO;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2y5;

    goto :goto_0

    .line 472549
    :cond_6
    iget-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 472550
    iget-object v0, p0, LX/2sO;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2y5;

    goto/16 :goto_0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 472531
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 472532
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v1, p0, LX/2sO;->d:[I

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;[I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2sO;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/2yR;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
