.class public LX/2a1;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0Tf;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0Tf;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 423219
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0Tf;
    .locals 4

    .prologue
    .line 423206
    sget-object v0, LX/2a1;->a:LX/0Tf;

    if-nez v0, :cond_1

    .line 423207
    const-class v1, LX/2a1;

    monitor-enter v1

    .line 423208
    :try_start_0
    sget-object v0, LX/2a1;->a:LX/0Tf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 423209
    if-eqz v2, :cond_0

    .line 423210
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 423211
    const-class v3, LX/28q;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/28q;

    const/16 p0, 0x1a1

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v3, p0}, LX/0Su;->a(LX/28q;LX/0Or;)LX/0Tf;

    move-result-object v3

    move-object v0, v3

    .line 423212
    sput-object v0, LX/2a1;->a:LX/0Tf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423213
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 423214
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 423215
    :cond_1
    sget-object v0, LX/2a1;->a:LX/0Tf;

    return-object v0

    .line 423216
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 423217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 423218
    const-class v0, LX/28q;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/28q;

    const/16 v1, 0x1a1

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Su;->a(LX/28q;LX/0Or;)LX/0Tf;

    move-result-object v0

    return-object v0
.end method
