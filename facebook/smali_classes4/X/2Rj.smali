.class public final LX/2Rj;
.super LX/2Rg;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/contacts/data/FbContactsContentProvider;


# direct methods
.method public constructor <init>(Lcom/facebook/contacts/data/FbContactsContentProvider;)V
    .locals 0

    .prologue
    .line 410012
    iput-object p1, p0, LX/2Rj;->a:Lcom/facebook/contacts/data/FbContactsContentProvider;

    invoke-direct {p0}, LX/2Rg;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/contacts/data/FbContactsContentProvider;B)V
    .locals 0

    .prologue
    .line 410013
    invoke-direct {p0, p1}, LX/2Rj;-><init>(Lcom/facebook/contacts/data/FbContactsContentProvider;)V

    return-void
.end method

.method private a(LX/6Lq;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 410014
    iget-object v0, p1, LX/6Lq;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410015
    const-string v0, "contacts"

    .line 410016
    :goto_0
    return-object v0

    .line 410017
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 410018
    iget-object v0, p1, LX/6Lq;->b:LX/0Rf;

    sget-object v4, LX/0PH;->NAME:LX/0PH;

    invoke-virtual {v0, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 410019
    iget-object v0, p0, LX/2Rj;->a:Lcom/facebook/contacts/data/FbContactsContentProvider;

    iget-object v0, v0, Lcom/facebook/contacts/data/FbContactsContentProvider;->i:LX/2Re;

    iget-object v4, p1, LX/6Lq;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/2Re;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 410020
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 410021
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v2, :cond_2

    .line 410022
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 410023
    iget-object v6, p0, LX/2Rj;->a:Lcom/facebook/contacts/data/FbContactsContentProvider;

    iget-object v6, v6, Lcom/facebook/contacts/data/FbContactsContentProvider;->g:LX/2Rd;

    invoke-virtual {v6, v0}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 410024
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 410025
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 410026
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_a

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v5, 0x4

    if-gt v0, v5, :cond_a

    .line 410027
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 410028
    const-string v0, "SELECT t0.contact_internal_id FROM "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 410029
    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_4

    .line 410030
    if-lez v0, :cond_3

    .line 410031
    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410032
    :cond_3
    const-string v6, "contacts_indexed_data t%d"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410033
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 410034
    :cond_4
    const-string v0, " WHERE "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 410035
    :goto_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_6

    .line 410036
    if-lez v0, :cond_5

    .line 410037
    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410038
    :cond_5
    const-string v6, "(t%d.type = \'name\' AND "

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410039
    const-string v6, "t%d.indexed_data GLOB \'%s*\')"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410040
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    move v0, v2

    .line 410041
    :goto_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_7

    .line 410042
    const-string v6, " AND (t0.contact_internal_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410043
    const-string v6, "t%d.contact_internal_id)"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410044
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 410045
    :cond_7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410046
    :cond_8
    :goto_5
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 410047
    iget-object v0, p1, LX/6Lq;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0PH;

    .line 410048
    sget-object v4, Lcom/facebook/contacts/data/FbContactsContentProvider;->e:LX/0P1;

    invoke-virtual {v4, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 410049
    if-eqz v0, :cond_9

    .line 410050
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 410051
    :cond_a
    iget-object v0, p0, LX/2Rj;->a:Lcom/facebook/contacts/data/FbContactsContentProvider;

    iget-object v0, v0, Lcom/facebook/contacts/data/FbContactsContentProvider;->g:LX/2Rd;

    iget-object v1, p1, LX/6Lq;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 410052
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 410053
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SELECT contact_internal_id FROM contacts_indexed_data WHERE type = \'name\' AND indexed_data GLOB \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*\' "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 410054
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 410055
    :cond_b
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 410056
    iget-object v0, p1, LX/6Lq;->a:Ljava/lang/String;

    invoke-static {v0}, LX/6My;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 410057
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_c

    .line 410058
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "SELECT contact_internal_id FROM contacts_indexed_data WHERE type IN "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/2bQ;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND indexed_data GLOB \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*\' "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 410059
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410060
    :cond_c
    const-string v0, " UNION ALL "

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 410061
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SELECT * FROM contacts"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " WHERE internal_id IN ( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 410062
    iget-object v0, p0, LX/2Rj;->a:Lcom/facebook/contacts/data/FbContactsContentProvider;

    iget-object v0, v0, Lcom/facebook/contacts/data/FbContactsContentProvider;->h:LX/2RE;

    iget-object v0, v0, LX/2RE;->f:LX/2RO;

    invoke-virtual {v0, p1}, LX/2RO;->a(Landroid/net/Uri;)LX/6Lq;

    move-result-object v0

    .line 410063
    invoke-direct {p0, v0}, LX/2Rj;->a(LX/6Lq;)Ljava/lang/String;

    move-result-object v1

    .line 410064
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 410065
    iget-object v2, p0, LX/2Rj;->a:Lcom/facebook/contacts/data/FbContactsContentProvider;

    invoke-static {v2, p5}, Lcom/facebook/contacts/data/FbContactsContentProvider;->a(Lcom/facebook/contacts/data/FbContactsContentProvider;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 410066
    iget-object v3, p0, LX/2Rj;->a:Lcom/facebook/contacts/data/FbContactsContentProvider;

    invoke-static {v3, p3, v2}, Lcom/facebook/contacts/data/FbContactsContentProvider;->a(Lcom/facebook/contacts/data/FbContactsContentProvider;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 410067
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ")"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2, v3, v2}, Lcom/facebook/contacts/data/FbContactsContentProvider;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 410068
    sget-object v1, Lcom/facebook/contacts/data/FbContactsContentProvider;->b:LX/0P1;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 410069
    const-string v1, "FbContactsContentProvider.doSearchQuery"

    const v2, -0x6f8ae643

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 410070
    :try_start_0
    iget-object v1, p0, LX/2Rj;->a:Lcom/facebook/contacts/data/FbContactsContentProvider;

    iget-object v1, v1, Lcom/facebook/contacts/data/FbContactsContentProvider;->f:LX/2Iv;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move-object v4, p4

    move-object v7, p5

    move-object v8, p6

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 410071
    const v1, 0x2262024e

    invoke-static {v1}, LX/02m;->b(I)J

    .line 410072
    return-object v0

    .line 410073
    :catchall_0
    move-exception v0

    const v1, -0x4ffa457a

    invoke-static {v1}, LX/02m;->b(I)J

    .line 410074
    throw v0
.end method
