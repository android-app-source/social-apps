.class public final LX/2AQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/2CH;


# direct methods
.method public constructor <init>(LX/2CH;)V
    .locals 0

    .prologue
    .line 377317
    iput-object p1, p0, LX/2AQ;->a:LX/2CH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x6ea8fbab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 377318
    iget-object v1, p0, LX/2AQ;->a:LX/2CH;

    const/4 v5, 0x0

    .line 377319
    const-string v2, "extra_on_messenger_map"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 377320
    const-string v2, "extra_on_messenger_map"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/contacts/protocol/push/ContactsMessengerUserMap;

    .line 377321
    iget-object v4, v2, Lcom/facebook/contacts/protocol/push/ContactsMessengerUserMap;->a:LX/0P1;

    invoke-virtual {v4}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v4

    move-object v4, v4

    .line 377322
    invoke-virtual {v4}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move v4, v5

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/UserKey;

    .line 377323
    invoke-static {v1, v4}, LX/2CH;->g(LX/2CH;Lcom/facebook/user/model/UserKey;)LX/3hU;

    move-result-object p0

    .line 377324
    iget-object p3, v2, Lcom/facebook/contacts/protocol/push/ContactsMessengerUserMap;->a:LX/0P1;

    invoke-virtual {p3, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    move p3, p3

    .line 377325
    invoke-static {p3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object p3

    iput-object p3, p0, LX/3hU;->c:LX/03R;

    .line 377326
    const/4 p0, 0x1

    .line 377327
    invoke-static {v1, v4}, LX/2CH;->h(LX/2CH;Lcom/facebook/user/model/UserKey;)V

    move v4, p0

    .line 377328
    goto :goto_0

    .line 377329
    :cond_0
    if-eqz v4, :cond_1

    .line 377330
    invoke-static {v1, v5}, LX/2CH;->b(LX/2CH;Z)V

    .line 377331
    :cond_1
    const/16 v1, 0x27

    const v2, 0x42863e02

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
