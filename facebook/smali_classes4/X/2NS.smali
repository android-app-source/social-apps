.class public final LX/2NS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 0

    .prologue
    .line 399836
    iput-object p1, p0, LX/2NS;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x46aa93dd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 399837
    iget-object v1, p0, LX/2NS;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-static {v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->L(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V

    .line 399838
    iget-object v1, p0, LX/2NS;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aB:Lcom/facebook/katana/service/AppSession;

    invoke-virtual {v1}, Lcom/facebook/katana/service/AppSession;->a()V

    .line 399839
    iget-object v1, p0, LX/2NS;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aB:Lcom/facebook/katana/service/AppSession;

    iget-object v2, p0, LX/2NS;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aC:LX/2NU;

    invoke-virtual {v1, v2}, Lcom/facebook/katana/service/AppSession;->a(LX/278;)V

    .line 399840
    iget-object v1, p0, LX/2NS;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v1, v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aB:Lcom/facebook/katana/service/AppSession;

    iget-object v2, p0, LX/2NS;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v2, v2, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aM:Ljava/lang/String;

    iget-object v3, p0, LX/2NS;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v3, v3, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->aN:Ljava/lang/String;

    const/4 v6, 0x0

    .line 399841
    iget-object v5, v1, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    move-object v5, v5

    .line 399842
    sget-object v7, LX/2A1;->STATUS_LOGGED_OUT:LX/2A1;

    if-ne v5, v7, :cond_0

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 399843
    iget-object v5, v1, Lcom/facebook/katana/service/AppSession;->q:Landroid/content/Context;

    invoke-static {v5}, Lcom/facebook/katana/service/AppSession;->e(Landroid/content/Context;)V

    .line 399844
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 399845
    const-string v7, "sso_auth_token"

    invoke-virtual {v5, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 399846
    const-string v7, "sso_username"

    invoke-virtual {v5, v7, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 399847
    iget-object v7, v1, Lcom/facebook/katana/service/AppSession;->o:LX/0aG;

    const-string p0, "sso"

    const p1, -0x6185f966

    invoke-static {v7, p0, v5, p1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    move-result-object v5

    .line 399848
    new-instance v7, LX/2Dw;

    invoke-direct {v7, v1}, LX/2Dw;-><init>(Lcom/facebook/katana/service/AppSession;)V

    iput-object v7, v1, Lcom/facebook/katana/service/AppSession;->f:LX/0Ve;

    .line 399849
    iget-object v6, v1, Lcom/facebook/katana/service/AppSession;->f:LX/0Ve;

    iget-object v7, v1, Lcom/facebook/katana/service/AppSession;->p:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 399850
    sget-object v5, LX/2A1;->STATUS_LOGGING_IN:LX/2A1;

    invoke-static {v1, v5}, Lcom/facebook/katana/service/AppSession;->a$redex0(Lcom/facebook/katana/service/AppSession;LX/2A1;)V

    .line 399851
    const v1, 0x660dbd23

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move v5, v6

    .line 399852
    goto :goto_0
.end method
