.class public LX/3OJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Z

.field public B:Ljava/lang/String;

.field public C:Z

.field public D:Z

.field public E:Z

.field public F:Z

.field public G:Z

.field public final H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public L:LX/EFb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:LX/DAc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Z

.field public a:Lcom/facebook/user/model/User;

.field public b:LX/3ON;

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;

.field public k:LX/3OL;

.field public l:LX/DHx;

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:J

.field public o:Z

.field public p:LX/6Lx;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/3OM;

.field public r:LX/3OI;

.field public s:LX/DAd;

.field public t:Z

.field public u:LX/DAe;

.field public v:LX/Ddk;

.field public w:LX/Jke;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/DAf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 560385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560386
    sget-object v0, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    iput-object v0, p0, LX/3OJ;->k:LX/3OL;

    .line 560387
    sget-object v0, LX/3OM;->NONE:LX/3OM;

    iput-object v0, p0, LX/3OJ;->q:LX/3OM;

    .line 560388
    sget-object v0, LX/3OH;->UNKNOWN:LX/3OH;

    iput-object v0, p0, LX/3OJ;->r:LX/3OI;

    .line 560389
    iput-boolean v2, p0, LX/3OJ;->y:Z

    .line 560390
    iput-boolean v1, p0, LX/3OJ;->C:Z

    .line 560391
    iput-boolean v1, p0, LX/3OJ;->E:Z

    .line 560392
    iput-boolean v2, p0, LX/3OJ;->G:Z

    .line 560393
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/3OJ;->H:Ljava/util/List;

    .line 560394
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/3OJ;->I:Ljava/util/List;

    .line 560395
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/3OJ;->J:Ljava/util/List;

    .line 560396
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/3OJ;->K:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()LX/3OO;
    .locals 1

    .prologue
    .line 560384
    new-instance v0, LX/3OO;

    invoke-direct {v0, p0}, LX/3OO;-><init>(LX/3OJ;)V

    return-object v0
.end method
