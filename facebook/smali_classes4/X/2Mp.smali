.class public LX/2Mp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2MK;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/2Mp;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 397583
    sget-object v0, LX/2MK;->VIDEO:LX/2MK;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/2Mp;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/media/upload/IsSkipVideoUploadingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397585
    iput-object p1, p0, LX/2Mp;->a:LX/0Or;

    .line 397586
    return-void
.end method

.method public static a(LX/0QB;)LX/2Mp;
    .locals 4

    .prologue
    .line 397587
    sget-object v0, LX/2Mp;->c:LX/2Mp;

    if-nez v0, :cond_1

    .line 397588
    const-class v1, LX/2Mp;

    monitor-enter v1

    .line 397589
    :try_start_0
    sget-object v0, LX/2Mp;->c:LX/2Mp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 397590
    if-eqz v2, :cond_0

    .line 397591
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 397592
    new-instance v3, LX/2Mp;

    const/16 p0, 0x1524

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2Mp;-><init>(LX/0Or;)V

    .line 397593
    move-object v0, v3

    .line 397594
    sput-object v0, LX/2Mp;->c:LX/2Mp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397595
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 397596
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397597
    :cond_1
    sget-object v0, LX/2Mp;->c:LX/2Mp;

    return-object v0

    .line 397598
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 397599
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 397600
    sget-object v1, LX/2Mp;->b:LX/0Rf;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->y:Z

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v1}, LX/5zj;->isQuickCamSource()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    if-nez v1, :cond_0

    invoke-static {p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 397601
    if-eqz v1, :cond_2

    .line 397602
    :cond_1
    :goto_1
    return v0

    .line 397603
    :cond_2
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-ne v1, v2, :cond_1

    .line 397604
    iget-object v0, p0, LX/2Mp;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
