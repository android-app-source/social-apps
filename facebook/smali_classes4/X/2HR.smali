.class public LX/2HR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Go;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/2H7;

.field private final c:LX/2Gs;

.field public final d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

.field public final e:LX/2H0;

.field public final f:LX/2H4;

.field private final g:LX/2HS;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 390379
    const-class v0, LX/2HR;

    sput-object v0, LX/2HR;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2Gs;Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Gq;LX/2Gr;LX/2Gp;LX/2HS;LX/0Or;Ljava/util/concurrent/ExecutorService;)V
    .locals 3
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Gs;",
            "Lcom/facebook/push/registration/FacebookPushServerRegistrar;",
            "LX/2Gq;",
            "LX/2Gr;",
            "LX/2Gp;",
            "LX/2HS;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390381
    iput-object p1, p0, LX/2HR;->c:LX/2Gs;

    .line 390382
    iput-object p2, p0, LX/2HR;->d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    .line 390383
    iput-object p6, p0, LX/2HR;->g:LX/2HS;

    .line 390384
    iput-object p7, p0, LX/2HR;->h:LX/0Or;

    .line 390385
    iput-object p8, p0, LX/2HR;->i:Ljava/util/concurrent/ExecutorService;

    .line 390386
    sget-object v0, LX/2Ge;->FBNS:LX/2Ge;

    invoke-virtual {p3, v0}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v0

    iput-object v0, p0, LX/2HR;->e:LX/2H0;

    .line 390387
    sget-object v0, LX/2Ge;->FBNS:LX/2Ge;

    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    invoke-virtual {p4, v1}, LX/2Gr;->a(LX/2Ge;)LX/2H3;

    move-result-object v1

    iget-object v2, p0, LX/2HR;->e:LX/2H0;

    invoke-virtual {p5, v0, v1, v2}, LX/2Gp;->a(LX/2Ge;LX/2H3;LX/2H0;)LX/2H4;

    move-result-object v0

    iput-object v0, p0, LX/2HR;->f:LX/2H4;

    .line 390388
    new-instance v0, LX/2HW;

    invoke-direct {v0, p0}, LX/2HW;-><init>(LX/2HR;)V

    iput-object v0, p0, LX/2HR;->a:LX/2H7;

    .line 390389
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 390390
    iget-object v0, p0, LX/2HR;->e:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390391
    :goto_0
    return-void

    .line 390392
    :cond_0
    iget-object v0, p0, LX/2HR;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/push/fbns/FbnsRegistrar$2;

    invoke-direct {v1, p0}, Lcom/facebook/push/fbns/FbnsRegistrar$2;-><init>(LX/2HR;)V

    const v2, -0x54829fdb

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 390393
    const/4 v0, 0x0

    .line 390394
    iget-object v1, p0, LX/2HR;->f:LX/2H4;

    sget-object v2, LX/Cem;->ATTEMPT:LX/Cem;

    invoke-virtual {v2}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/2H4;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 390395
    iget-object v1, p0, LX/2HR;->d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v2, LX/2Ge;->FBNS:LX/2Ge;

    invoke-virtual {v1, v2, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/String;)Z

    move-result v1

    .line 390396
    if-eqz v1, :cond_0

    .line 390397
    iget-object v1, p0, LX/2HR;->e:LX/2H0;

    invoke-virtual {v1}, LX/2H0;->h()V

    .line 390398
    iget-object v1, p0, LX/2HR;->f:LX/2H4;

    sget-object v2, LX/Cem;->SUCCESS:LX/Cem;

    invoke-virtual {v2}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/2H4;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 390399
    :goto_0
    return-void

    .line 390400
    :cond_0
    iget-object v1, p0, LX/2HR;->f:LX/2H4;

    sget-object v2, LX/Cem;->FAILED:LX/Cem;

    invoke-virtual {v2}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/2H4;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 390401
    iget-object v0, p0, LX/2HR;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390402
    :goto_0
    return-void

    .line 390403
    :cond_0
    iget-object v0, p0, LX/2HR;->e:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v0

    .line 390404
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 390405
    sget-object v0, LX/2H9;->NONE:LX/2H9;

    .line 390406
    :goto_1
    move-object v0, v0

    .line 390407
    iget-object v1, p0, LX/2HR;->c:LX/2Gs;

    sget-object v2, LX/2Ge;->FBNS:LX/2Ge;

    invoke-virtual {v2}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/2H9;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/2HR;->e:LX/2H0;

    invoke-virtual {v4}, LX/2H0;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/2Gs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390408
    sget-object v1, LX/2XH;->a:[I

    invoke-virtual {v0}, LX/2H9;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 390409
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, LX/2H9;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 390410
    :pswitch_0
    if-eqz p1, :cond_1

    .line 390411
    iget-object v0, p0, LX/2HR;->d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    iget-object v2, p0, LX/2HR;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_0

    .line 390412
    :cond_1
    iget-object v0, p0, LX/2HR;->d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    iget-object v2, p0, LX/2HR;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->b(LX/2Ge;LX/2H7;)V

    goto :goto_0

    .line 390413
    :pswitch_1
    invoke-virtual {p0}, LX/2HR;->b()V

    goto :goto_0

    .line 390414
    :cond_2
    iget-object v0, p0, LX/2HR;->e:LX/2H0;

    invoke-virtual {v0}, LX/2H0;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 390415
    sget-object v0, LX/2H9;->UPGRADED:LX/2H9;

    goto :goto_1

    .line 390416
    :cond_3
    sget-object v0, LX/2H9;->CURRENT:LX/2H9;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 390417
    iget-object v0, p0, LX/2HR;->f:LX/2H4;

    sget-object v1, LX/2gP;->ATTEMPT:LX/2gP;

    invoke-virtual {v1}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 390418
    iget-object v0, p0, LX/2HR;->g:LX/2HS;

    .line 390419
    iget-object v1, v0, LX/2HS;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, v0, LX/2HS;->f:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 390420
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 390421
    :goto_0
    move-object v0, v1

    .line 390422
    iget-object v1, p0, LX/2HR;->e:LX/2H0;

    invoke-virtual {v1, v0}, LX/2H0;->a(Ljava/lang/String;)V

    .line 390423
    iget-object v0, p0, LX/2HR;->f:LX/2H4;

    sget-object v1, LX/2gP;->SUCCESS:LX/2gP;

    invoke-virtual {v1}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 390424
    iget-object v0, p0, LX/2HR;->d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    iget-object v2, p0, LX/2HR;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    .line 390425
    return-void

    .line 390426
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "fbns"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, LX/2HS;->e:LX/0dC;

    invoke-virtual {v4}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, LX/2HS;->d:LX/00H;

    invoke-virtual {v4}, LX/00H;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/03l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 390427
    iget-object v3, v0, LX/2HS;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    iget-object v4, v0, LX/2HS;->f:LX/0Tn;

    invoke-interface {v3, v4, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public final c()LX/2H7;
    .locals 1

    .prologue
    .line 390428
    iget-object v0, p0, LX/2HR;->a:LX/2H7;

    return-object v0
.end method
