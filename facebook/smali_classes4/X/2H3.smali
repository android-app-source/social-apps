.class public abstract LX/2H3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Tn;

.field public final b:LX/0Tn;

.field public final c:LX/0Tn;

.field public final d:LX/0Tn;

.field public final e:LX/0Tn;

.field public final f:LX/0Tn;

.field public final g:LX/0Tn;

.field public final h:LX/0Tn;

.field public final i:LX/0Tn;

.field public final j:LX/0Tn;

.field public final k:LX/0Tn;

.field public final l:LX/0Tn;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 389744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389745
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "token"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->a:LX/0Tn;

    .line 389746
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "token_owner"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->b:LX/0Tn;

    .line 389747
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "last_register_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->c:LX/0Tn;

    .line 389748
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "last_change_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->d:LX/0Tn;

    .line 389749
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "backoff_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->e:LX/0Tn;

    .line 389750
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "server_backoff_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->f:LX/0Tn;

    .line 389751
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "last_push_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->g:LX/0Tn;

    .line 389752
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "fb_server_registered"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->h:LX/0Tn;

    .line 389753
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "fb_server_last_register_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->i:LX/0Tn;

    .line 389754
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "fb_server_build"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->j:LX/0Tn;

    .line 389755
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "fb_server_device_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->k:LX/0Tn;

    .line 389756
    invoke-virtual {p0}, LX/2H3;->a()LX/0Tn;

    move-result-object v0

    const-string v1, "is_c2dm"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2H3;->l:LX/0Tn;

    return-void
.end method


# virtual methods
.method public abstract a()LX/0Tn;
.end method
