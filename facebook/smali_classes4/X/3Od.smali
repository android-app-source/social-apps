.class public final LX/3Od;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3NY;


# instance fields
.field public final synthetic a:LX/3NW;

.field public final synthetic b:LX/3NS;


# direct methods
.method public constructor <init>(LX/3NS;LX/3NW;)V
    .locals 0

    .prologue
    .line 560621
    iput-object p1, p0, LX/3Od;->b:LX/3NS;

    iput-object p2, p0, LX/3Od;->a:LX/3NW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 560622
    return-void
.end method

.method public final a(LX/3Mr;)V
    .locals 5

    .prologue
    .line 560623
    iget-object v0, p0, LX/3Od;->a:LX/3NW;

    .line 560624
    iput-object p1, v0, LX/3NW;->f:LX/3Mr;

    .line 560625
    sget-object v0, LX/3Mr;->FINISHED:LX/3Mr;

    if-ne p1, v0, :cond_0

    .line 560626
    iget-object v0, p0, LX/3Od;->a:LX/3NW;

    iget-object v1, p0, LX/3Od;->b:LX/3NS;

    iget-object v1, v1, LX/3NS;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 560627
    iput-wide v2, v0, LX/3NW;->i:J

    .line 560628
    iget-object v0, p0, LX/3Od;->b:LX/3NS;

    invoke-static {v0}, LX/3NS;->d$redex0(LX/3NS;)V

    .line 560629
    :cond_0
    iget-object v0, p0, LX/3Od;->b:LX/3NS;

    .line 560630
    sget-object v2, LX/3Mr;->FINISHED:LX/3Mr;

    .line 560631
    iget-object v1, v0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v4, :cond_3

    iget-object v1, v0, LX/3NS;->b:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3NW;

    .line 560632
    iget-boolean p0, v1, LX/3NW;->g:Z

    move p0, p0

    .line 560633
    if-nez p0, :cond_2

    sget-object p0, LX/3Mr;->FILTERING:LX/3Mr;

    .line 560634
    iget-object p1, v1, LX/3NW;->f:LX/3Mr;

    move-object v1, p1

    .line 560635
    if-ne p0, v1, :cond_2

    .line 560636
    sget-object v1, LX/3Mr;->FILTERING:LX/3Mr;

    .line 560637
    :goto_1
    iget-object v2, v0, LX/3NS;->h:LX/3Mr;

    if-eq v1, v2, :cond_1

    .line 560638
    iput-object v1, v0, LX/3NS;->h:LX/3Mr;

    .line 560639
    iget-object v1, v0, LX/3NS;->g:LX/3NY;

    if-eqz v1, :cond_1

    .line 560640
    iget-object v1, v0, LX/3NS;->g:LX/3NY;

    iget-object v2, v0, LX/3NS;->h:LX/3Mr;

    invoke-interface {v1, v2}, LX/3NY;->a(LX/3Mr;)V

    .line 560641
    :cond_1
    return-void

    .line 560642
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_3
    move-object v1, v2

    goto :goto_1
.end method
