.class public LX/26Q;
.super LX/1cr;
.source ""


# instance fields
.field private final b:Landroid/graphics/Rect;

.field public final c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/collage/ui/CollageAttachmentView;)V
    .locals 1

    .prologue
    .line 372137
    invoke-direct {p0, p1}, LX/1cr;-><init>(Landroid/view/View;)V

    .line 372138
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/26Q;->b:Landroid/graphics/Rect;

    .line 372139
    iput-object p1, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 372140
    return-void
.end method

.method public static a(Landroid/content/res/Resources;IIILjava/lang/String;)Ljava/lang/CharSequence;
    .locals 6
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 372128
    add-int/lit8 v0, p3, 0x1

    .line 372129
    if-le p2, v3, :cond_1

    .line 372130
    if-ne v0, p1, :cond_0

    .line 372131
    const v0, 0x7f081a3e

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 372132
    :goto_0
    return-object v0

    .line 372133
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 372134
    :cond_1
    if-eqz p4, :cond_2

    .line 372135
    const v1, 0x7f081a3f

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    aput-object p4, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 372136
    :cond_2
    const v1, 0x7f081a3d

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(I)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 372157
    iget-object v0, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 372158
    iget-object v1, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->r:[Ljava/lang/String;

    move-object v0, v1

    .line 372159
    iget-object v1, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getVisibleAttachmentsCount()I

    move-result v2

    iget-object v3, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 372160
    iget p0, v3, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    move v3, p0

    .line 372161
    if-eqz v0, :cond_0

    aget-object v0, v0, p1

    :goto_0
    invoke-static {v1, v2, v3, p1, v0}, LX/26Q;->a(Landroid/content/res/Resources;IIILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(FF)I
    .locals 6

    .prologue
    .line 372151
    const/high16 v0, -0x80000000

    .line 372152
    const/4 v1, 0x0

    iget-object v2, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getVisibleAttachmentsCount()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_1

    .line 372153
    iget-object v3, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v3, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->e(I)Landroid/graphics/Rect;

    move-result-object v3

    .line 372154
    float-to-int v4, p1

    float-to-int v5, p2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 372155
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 372156
    :cond_1
    return v0
.end method

.method public final a(ILX/3sp;)V
    .locals 8

    .prologue
    .line 372162
    iget-object v0, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getVisibleAttachmentsCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 372163
    :goto_0
    return-void

    .line 372164
    :cond_0
    invoke-direct {p0, p1}, LX/26Q;->b(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 372165
    iget-object v1, p0, LX/26Q;->b:Landroid/graphics/Rect;

    .line 372166
    iget-object v2, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v2, p1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c(I)Landroid/graphics/Rect;

    move-result-object v3

    .line 372167
    new-instance v2, Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v5}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingLeft()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v3, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v6}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingTop()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, v3, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v7}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingLeft()I

    move-result v7

    add-int/2addr v6, v7

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v7, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v7}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingTop()I

    move-result v7

    add-int/2addr v3, v7

    invoke-direct {v2, v4, v5, v6, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 372168
    if-eqz v1, :cond_1

    .line 372169
    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 372170
    :goto_1
    iget-object v1, p0, LX/26Q;->b:Landroid/graphics/Rect;

    invoke-virtual {p2, v1}, LX/3sp;->b(Landroid/graphics/Rect;)V

    .line 372171
    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 372172
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 372173
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/3sp;->f(Z)V

    .line 372174
    const-class v0, Landroid/widget/Button;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    goto :goto_1
.end method

.method public final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 372148
    invoke-direct {p0, p1}, LX/26Q;->b(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 372149
    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 372150
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 372142
    const/4 v0, 0x0

    iget-object v1, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getVisibleAttachmentsCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 372143
    iget-object v2, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v2, v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c(I)Landroid/graphics/Rect;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 372144
    iget-object v2, p0, LX/26Q;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v2, v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->f(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 372145
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372146
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 372147
    :cond_1
    return-void
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 372141
    const/4 v0, 0x0

    return v0
.end method
