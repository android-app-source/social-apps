.class public LX/3BU;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:LX/31d;

.field public static e:Ljava/lang/String;

.field public static f:J

.field public static final g:Ljava/util/concurrent/Semaphore;

.field public static final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/68H;",
            ">;>;"
        }
    .end annotation
.end field

.field public static volatile i:Ljava/lang/String;

.field private static final j:LX/3BV;

.field private static final k:LX/3BV;

.field public static volatile l:LX/3BV;

.field public static m:Landroid/content/Context;

.field private static n:Landroid/content/BroadcastReceiver;

.field public static final o:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

.field public static final p:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const v4, 0x7fffffff

    const/4 v3, 0x0

    .line 528576
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v0, LX/3BU;->g:Ljava/util/concurrent/Semaphore;

    .line 528577
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, LX/3BU;->h:Ljava/util/List;

    .line 528578
    const-string v0, "https://graph.facebook.com/v2.2/maps_configs?fields=base_url,static_base_url,osm_config,url_override_config&pretty=0&access_token="

    sput-object v0, LX/3BU;->i:Ljava/lang/String;

    .line 528579
    new-instance v0, LX/3BV;

    const-string v1, "https://www.facebook.com/maps/tile/?"

    const-string v2, "https://www.facebook.com/maps/static/?"

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, LX/3BV;-><init>(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Rect;I[Ljava/lang/String;[[LX/31i;)V

    sput-object v0, LX/3BU;->j:LX/3BV;

    .line 528580
    new-instance v0, LX/3BV;

    const-string v1, "https://maps.instagram.com/maps/tile/?"

    const-string v2, "https://maps.instagram.com/maps/static/?"

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, LX/3BV;-><init>(Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Rect;I[Ljava/lang/String;[[LX/31i;)V

    sput-object v0, LX/3BU;->k:LX/3BV;

    .line 528581
    sget-object v0, LX/3BU;->j:LX/3BV;

    sput-object v0, LX/3BU;->l:LX/3BV;

    .line 528582
    invoke-static {}, LX/3BU;->m()V

    .line 528583
    new-instance v0, Lcom/facebook/android/maps/internal/MapConfig$1;

    invoke-direct {v0}, Lcom/facebook/android/maps/internal/MapConfig$1;-><init>()V

    sput-object v0, LX/3BU;->o:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 528584
    new-instance v0, Lcom/facebook/android/maps/internal/MapConfig$2;

    invoke-direct {v0}, Lcom/facebook/android/maps/internal/MapConfig$2;-><init>()V

    sput-object v0, LX/3BU;->p:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 528599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528600
    return-void
.end method

.method public static a(LX/69C;I)I
    .locals 7

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x0

    .line 528585
    const/4 v2, 0x5

    if-ne p1, v2, :cond_1

    move v0, v1

    .line 528586
    :cond_0
    :goto_0
    return v0

    .line 528587
    :cond_1
    sget-object v2, LX/3BU;->l:LX/3BV;

    .line 528588
    iget-object v3, v2, LX/3BV;->d:[Landroid/graphics/Rect;

    if-eqz v3, :cond_0

    .line 528589
    iget v3, p0, LX/69C;->e:I

    iget v4, v2, LX/3BV;->e:I

    if-lt v3, v4, :cond_3

    .line 528590
    iget v3, p0, LX/69C;->e:I

    iget v4, v2, LX/3BV;->e:I

    sub-int/2addr v3, v4

    .line 528591
    iget v4, p0, LX/69C;->f:I

    shr-int/2addr v4, v3

    .line 528592
    iget v5, p0, LX/69C;->g:I

    shr-int v3, v5, v3

    .line 528593
    :goto_1
    iget-object v5, v2, LX/3BV;->d:[Landroid/graphics/Rect;

    array-length v5, v5

    if-ge v0, v5, :cond_3

    .line 528594
    iget-object v5, v2, LX/3BV;->d:[Landroid/graphics/Rect;

    aget-object v5, v5, v0

    .line 528595
    iget v6, v5, Landroid/graphics/Rect;->left:I

    if-gt v6, v4, :cond_2

    iget v6, v5, Landroid/graphics/Rect;->right:I

    if-gt v4, v6, :cond_2

    iget v6, v5, Landroid/graphics/Rect;->top:I

    if-gt v6, v3, :cond_2

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    if-gt v3, v5, :cond_2

    move v0, v1

    .line 528596
    goto :goto_0

    .line 528597
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 528598
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(III)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 528601
    invoke-static {}, LX/3BU;->a()V

    .line 528602
    sget-object v3, LX/3BU;->l:LX/3BV;

    .line 528603
    iget-object v0, v3, LX/3BV;->f:[Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 528604
    const/4 v0, 0x1

    shl-int v4, v0, p2

    .line 528605
    iget-object v0, v3, LX/3BV;->f:[Ljava/lang/String;

    array-length v5, v0

    move v2, v1

    .line 528606
    :goto_0
    if-ge v2, v5, :cond_2

    .line 528607
    iget-object v0, v3, LX/3BV;->g:[[LX/31i;

    aget-object v0, v0, v2

    array-length v6, v0

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_1

    .line 528608
    iget-object v7, v3, LX/3BV;->g:[[LX/31i;

    aget-object v7, v7, v2

    aget-object v7, v7, v0

    .line 528609
    iget-wide v8, v7, LX/31i;->c:D

    int-to-double v10, v4

    mul-double/2addr v8, v10

    double-to-int v8, v8

    if-gt v8, p0, :cond_0

    iget-wide v8, v7, LX/31i;->d:D

    int-to-double v10, v4

    mul-double/2addr v8, v10

    double-to-int v8, v8

    if-gt p0, v8, :cond_0

    iget-wide v8, v7, LX/31i;->a:D

    int-to-double v10, v4

    mul-double/2addr v8, v10

    double-to-int v8, v8

    if-gt v8, p1, :cond_0

    iget-wide v8, v7, LX/31i;->b:D

    int-to-double v10, v4

    mul-double/2addr v8, v10

    double-to-int v7, v8

    if-gt p1, v7, :cond_0

    .line 528610
    iget-object v0, v3, LX/3BV;->f:[Ljava/lang/String;

    aget-object v0, v0, v2

    .line 528611
    :goto_2
    return-object v0

    .line 528612
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 528613
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 528614
    :cond_2
    iget-object v0, v3, LX/3BV;->a:Ljava/lang/String;

    goto :goto_2
.end method

.method public static a()V
    .locals 4

    .prologue
    .line 528549
    sget-object v0, LX/3BU;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, LX/3BU;->m:Landroid/content/Context;

    if-eqz v0, :cond_1

    sget-object v0, LX/3BU;->g:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 528550
    sget-wide v0, LX/3BU;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sget-wide v2, LX/3BU;->f:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 528551
    :goto_0
    if-eqz v0, :cond_3

    .line 528552
    sget-object v0, LX/3BU;->p:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    invoke-static {v0}, LX/31l;->a(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    .line 528553
    :cond_1
    :goto_1
    return-void

    .line 528554
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 528555
    :cond_3
    sget-object v0, LX/3BU;->g:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;LX/31d;)V
    .locals 4

    .prologue
    .line 528556
    sput-object p2, LX/3BU;->d:LX/31d;

    .line 528557
    sput-object p1, LX/3BU;->e:Ljava/lang/String;

    .line 528558
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, LX/3BU;->m:Landroid/content/Context;

    .line 528559
    const-string v0, "com.instagram.android"

    sget-object v1, LX/3BU;->m:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528560
    const-string v0, "https://logger.instagram.com/graph/server.php?_fb_url=v2.2/maps_configs&fields=base_url,static_base_url,osm_config,url_override_config&pretty=0&access_token="

    sput-object v0, LX/3BU;->i:Ljava/lang/String;

    .line 528561
    sget-object v0, LX/3BU;->k:LX/3BV;

    sput-object v0, LX/3BU;->l:LX/3BV;

    .line 528562
    :cond_0
    sget-object v0, LX/3BU;->n:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_1

    .line 528563
    new-instance v0, LX/31e;

    invoke-direct {v0}, LX/31e;-><init>()V

    sput-object v0, LX/3BU;->n:Landroid/content/BroadcastReceiver;

    .line 528564
    sget-object v0, LX/3BU;->m:Landroid/content/Context;

    sget-object v1, LX/3BU;->n:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 528565
    :cond_1
    return-void
.end method

.method public static m()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 528566
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 528567
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 528568
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 528569
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v3, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3BU;->a:Ljava/lang/String;

    .line 528570
    :goto_1
    sget-object v0, LX/3BU;->a:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3BU;->b:Ljava/lang/String;

    .line 528571
    :try_start_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3BU;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 528572
    :goto_2
    return-void

    .line 528573
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 528574
    :cond_1
    const-string v0, "en"

    sput-object v0, LX/3BU;->a:Ljava/lang/String;

    goto :goto_1

    .line 528575
    :catch_0
    const-string v0, "eng"

    sput-object v0, LX/3BU;->c:Ljava/lang/String;

    goto :goto_2
.end method
