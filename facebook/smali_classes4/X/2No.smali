.class public LX/2No;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2N8;


# direct methods
.method public constructor <init>(LX/2N8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 400027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400028
    iput-object p1, p0, LX/2No;->a:LX/2N8;

    .line 400029
    return-void
.end method

.method public static a(LX/0QB;)LX/2No;
    .locals 1

    .prologue
    .line 400037
    invoke-static {p0}, LX/2No;->b(LX/0QB;)LX/2No;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0P1;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 400032
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 400033
    if-eqz p0, :cond_0

    .line 400034
    invoke-virtual {p0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 400035
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 400036
    :cond_0
    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2No;
    .locals 2

    .prologue
    .line 400030
    new-instance v1, LX/2No;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v0

    check-cast v0, LX/2N8;

    invoke-direct {v1, v0}, LX/2No;-><init>(LX/2N8;)V

    .line 400031
    return-object v1
.end method
