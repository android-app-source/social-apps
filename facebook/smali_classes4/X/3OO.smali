.class public LX/3OO;
.super LX/3OP;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final A:LX/Jke;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final B:LX/DAf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Z

.field public D:Z

.field public E:Z

.field public F:Ljava/lang/String;

.field public G:Ljava/lang/String;

.field public H:Z

.field private I:Z

.field public J:Z

.field public K:Z

.field public L:Ljava/lang/String;

.field public M:Z

.field public N:LX/EFb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:J

.field public P:Z

.field public Q:Z

.field public R:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:LX/DAc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private T:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private V:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public X:I

.field public final a:Lcom/facebook/user/model/User;

.field public final b:LX/3ON;

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field private final f:Z

.field public g:Z

.field public final h:Z

.field private final i:LX/3OM;

.field public final j:Z

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/lang/String;

.field public final m:LX/3OL;

.field public final n:LX/DHx;

.field private final o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:J

.field public final q:LX/6Lx;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:LX/3OI;

.field private s:Z

.field public t:Z

.field public u:Z

.field public final v:Z

.field public w:Z

.field public final x:LX/DAd;

.field public final y:LX/DAe;

.field public final z:LX/Ddk;


# direct methods
.method public constructor <init>(LX/3OJ;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 560455
    invoke-direct {p0}, LX/3OP;-><init>()V

    .line 560456
    iput-boolean v2, p0, LX/3OO;->t:Z

    .line 560457
    iput-boolean v2, p0, LX/3OO;->C:Z

    .line 560458
    iput-boolean v2, p0, LX/3OO;->D:Z

    .line 560459
    iput-boolean v2, p0, LX/3OO;->E:Z

    .line 560460
    iput-boolean v2, p0, LX/3OO;->Q:Z

    .line 560461
    iput v1, p0, LX/3OO;->X:I

    .line 560462
    iget-object v0, p1, LX/3OJ;->a:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 560463
    iput-object v0, p0, LX/3OO;->a:Lcom/facebook/user/model/User;

    .line 560464
    iget-object v0, p1, LX/3OJ;->b:LX/3ON;

    move-object v0, v0

    .line 560465
    iput-object v0, p0, LX/3OO;->b:LX/3ON;

    .line 560466
    iget-boolean v0, p1, LX/3OJ;->d:Z

    move v0, v0

    .line 560467
    iput-boolean v0, p0, LX/3OO;->c:Z

    .line 560468
    iget-boolean v0, p1, LX/3OJ;->c:Z

    move v0, v0

    .line 560469
    iput-boolean v0, p0, LX/3OO;->d:Z

    .line 560470
    iget-boolean v0, p1, LX/3OJ;->e:Z

    move v0, v0

    .line 560471
    iput-boolean v0, p0, LX/3OO;->e:Z

    .line 560472
    iget-boolean v0, p1, LX/3OJ;->f:Z

    move v0, v0

    .line 560473
    iput-boolean v0, p0, LX/3OO;->f:Z

    .line 560474
    iget-boolean v0, p0, LX/3OO;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/3OO;->f:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/3OO;->g:Z

    .line 560475
    iget-boolean v0, p1, LX/3OJ;->g:Z

    move v0, v0

    .line 560476
    iput-boolean v0, p0, LX/3OO;->h:Z

    .line 560477
    iget-object v0, p1, LX/3OJ;->q:LX/3OM;

    move-object v0, v0

    .line 560478
    iput-object v0, p0, LX/3OO;->i:LX/3OM;

    .line 560479
    iget-boolean v0, p1, LX/3OJ;->h:Z

    move v0, v0

    .line 560480
    iput-boolean v0, p0, LX/3OO;->j:Z

    .line 560481
    iget-object v0, p1, LX/3OJ;->i:Ljava/lang/String;

    move-object v0, v0

    .line 560482
    iput-object v0, p0, LX/3OO;->k:Ljava/lang/String;

    .line 560483
    iget-object v0, p1, LX/3OJ;->j:Ljava/lang/String;

    move-object v0, v0

    .line 560484
    iput-object v0, p0, LX/3OO;->l:Ljava/lang/String;

    .line 560485
    iget-object v0, p1, LX/3OJ;->k:LX/3OL;

    move-object v0, v0

    .line 560486
    iput-object v0, p0, LX/3OO;->m:LX/3OL;

    .line 560487
    iget-object v0, p1, LX/3OJ;->l:LX/DHx;

    move-object v0, v0

    .line 560488
    iput-object v0, p0, LX/3OO;->n:LX/DHx;

    .line 560489
    iget-object v0, p1, LX/3OJ;->m:Ljava/lang/String;

    move-object v0, v0

    .line 560490
    iput-object v0, p0, LX/3OO;->o:Ljava/lang/String;

    .line 560491
    iget-wide v6, p1, LX/3OJ;->n:J

    move-wide v4, v6

    .line 560492
    iput-wide v4, p0, LX/3OO;->p:J

    .line 560493
    iget-object v0, p1, LX/3OJ;->p:LX/6Lx;

    move-object v0, v0

    .line 560494
    iput-object v0, p0, LX/3OO;->q:LX/6Lx;

    .line 560495
    iget-object v0, p1, LX/3OJ;->r:LX/3OI;

    move-object v0, v0

    .line 560496
    iput-object v0, p0, LX/3OO;->r:LX/3OI;

    .line 560497
    iget-boolean v0, p1, LX/3OJ;->o:Z

    move v0, v0

    .line 560498
    iput-boolean v0, p0, LX/3OO;->s:Z

    .line 560499
    iget-boolean v0, p1, LX/3OJ;->O:Z

    move v0, v0

    .line 560500
    iput-boolean v0, p0, LX/3OO;->u:Z

    .line 560501
    iget-boolean v0, p1, LX/3OJ;->t:Z

    move v0, v0

    .line 560502
    iput-boolean v0, p0, LX/3OO;->v:Z

    .line 560503
    iget-object v0, p1, LX/3OJ;->s:LX/DAd;

    move-object v0, v0

    .line 560504
    iput-object v0, p0, LX/3OO;->x:LX/DAd;

    .line 560505
    iget-object v0, p1, LX/3OJ;->u:LX/DAe;

    move-object v0, v0

    .line 560506
    iput-object v0, p0, LX/3OO;->y:LX/DAe;

    .line 560507
    iget-object v0, p1, LX/3OJ;->v:LX/Ddk;

    move-object v0, v0

    .line 560508
    iput-object v0, p0, LX/3OO;->z:LX/Ddk;

    .line 560509
    iget-object v0, p1, LX/3OJ;->w:LX/Jke;

    move-object v0, v0

    .line 560510
    iput-object v0, p0, LX/3OO;->A:LX/Jke;

    .line 560511
    iget-object v0, p1, LX/3OJ;->x:LX/DAf;

    move-object v0, v0

    .line 560512
    iput-object v0, p0, LX/3OO;->B:LX/DAf;

    .line 560513
    iget-boolean v0, p1, LX/3OJ;->y:Z

    move v0, v0

    .line 560514
    iput-boolean v0, p0, LX/3OO;->w:Z

    .line 560515
    iget-object v0, p1, LX/3OJ;->B:Ljava/lang/String;

    move-object v0, v0

    .line 560516
    iput-object v0, p0, LX/3OO;->F:Ljava/lang/String;

    .line 560517
    iget-boolean v0, p1, LX/3OJ;->z:Z

    move v0, v0

    .line 560518
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3OO;->F:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, LX/3OO;->C:Z

    .line 560519
    iget-boolean v0, p1, LX/3OJ;->A:Z

    move v0, v0

    .line 560520
    iput-boolean v0, p0, LX/3OO;->E:Z

    .line 560521
    iget-boolean v0, p1, LX/3OJ;->D:Z

    move v0, v0

    .line 560522
    iput-boolean v0, p0, LX/3OO;->H:Z

    .line 560523
    iget-boolean v0, p1, LX/3OJ;->E:Z

    move v0, v0

    .line 560524
    iput-boolean v0, p0, LX/3OO;->I:Z

    .line 560525
    iget-boolean v0, p1, LX/3OJ;->F:Z

    move v0, v0

    .line 560526
    iput-boolean v0, p0, LX/3OO;->J:Z

    .line 560527
    iget-object v0, p1, LX/3OJ;->L:LX/EFb;

    move-object v0, v0

    .line 560528
    iput-object v0, p0, LX/3OO;->N:LX/EFb;

    .line 560529
    iget-object v0, p1, LX/3OJ;->H:Ljava/util/List;

    move-object v0, v0

    .line 560530
    iput-object v0, p0, LX/3OO;->T:Ljava/util/List;

    .line 560531
    iget-object v0, p1, LX/3OJ;->I:Ljava/util/List;

    move-object v0, v0

    .line 560532
    iput-object v0, p0, LX/3OO;->U:Ljava/util/List;

    .line 560533
    iget-object v0, p1, LX/3OJ;->J:Ljava/util/List;

    move-object v0, v0

    .line 560534
    iput-object v0, p0, LX/3OO;->V:Ljava/util/List;

    .line 560535
    iget-object v0, p1, LX/3OJ;->K:Ljava/util/List;

    move-object v0, v0

    .line 560536
    iput-object v0, p0, LX/3OO;->W:Ljava/util/List;

    .line 560537
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3OO;->O:J

    .line 560538
    iget-boolean v0, p1, LX/3OJ;->G:Z

    move v0, v0

    .line 560539
    iput-boolean v0, p0, LX/3OO;->P:Z

    .line 560540
    iget-boolean v0, p1, LX/3OJ;->C:Z

    move v0, v0

    .line 560541
    iput-boolean v0, p0, LX/3OO;->Q:Z

    .line 560542
    iget-object v0, p1, LX/3OJ;->M:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 560543
    iput-object v0, p0, LX/3OO;->R:Lcom/facebook/user/model/User;

    .line 560544
    iget-object v0, p1, LX/3OJ;->N:LX/DAc;

    move-object v0, v0

    .line 560545
    iput-object v0, p0, LX/3OO;->S:LX/DAc;

    .line 560546
    return-void

    :cond_0
    move v0, v2

    .line 560547
    goto/16 :goto_0

    :cond_1
    move v1, v2

    .line 560548
    goto :goto_1
.end method


# virtual methods
.method public final O()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 560430
    iget-object v0, p0, LX/3OO;->T:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final P()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 560431
    iget-object v0, p0, LX/3OO;->U:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final Q()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 560432
    iget-object v0, p0, LX/3OO;->V:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final S()Z
    .locals 1

    .prologue
    .line 560433
    iget-object v0, p0, LX/3OO;->L:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/3L9;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "ARG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3L9",
            "<TT;TARG;>;TARG;)TT;"
        }
    .end annotation

    .prologue
    .line 560434
    invoke-interface {p1, p0, p2}, LX/3L9;->a(LX/3OO;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 560435
    iput-boolean p1, p0, LX/3OO;->s:Z

    .line 560436
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 560437
    iget-boolean v0, p0, LX/3OO;->s:Z

    return v0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 560438
    iput-boolean p1, p0, LX/3OO;->w:Z

    .line 560439
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 560440
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 560441
    :cond_0
    const/4 v0, 0x0

    .line 560442
    :goto_0
    return v0

    .line 560443
    :cond_1
    check-cast p1, LX/3OO;

    .line 560444
    iget-object v0, p1, LX/3OO;->a:Lcom/facebook/user/model/User;

    .line 560445
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 560446
    iget-object v1, p0, LX/3OO;->a:Lcom/facebook/user/model/User;

    .line 560447
    iget-object p0, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, p0

    .line 560448
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 560449
    iget-object v0, p0, LX/3OO;->a:Lcom/facebook/user/model/User;

    .line 560450
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 560451
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final r()J
    .locals 2

    .prologue
    .line 560452
    iget-wide v0, p0, LX/3OO;->O:J

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 560453
    iget-object v0, p0, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v0, v0

    .line 560454
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
