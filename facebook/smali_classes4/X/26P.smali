.class public final enum LX/26P;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/26P;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/26P;

.field public static final enum AddFriend:LX/26P;

.field public static final enum Album:LX/26P;

.field public static final enum AvatarList:LX/26P;

.field public static final enum CollageOptimisticPost:LX/26P;

.field public static final enum MusicPreviewCover:LX/26P;

.field public static final enum Photo:LX/26P;

.field public static final enum Share:LX/26P;

.field public static final enum ShareLargeImage:LX/26P;

.field public static final enum Video:LX/26P;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 372114
    new-instance v0, LX/26P;

    const-string v1, "AddFriend"

    invoke-direct {v0, v1, v3}, LX/26P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/26P;->AddFriend:LX/26P;

    .line 372115
    new-instance v0, LX/26P;

    const-string v1, "Album"

    invoke-direct {v0, v1, v4}, LX/26P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/26P;->Album:LX/26P;

    .line 372116
    new-instance v0, LX/26P;

    const-string v1, "AvatarList"

    invoke-direct {v0, v1, v5}, LX/26P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/26P;->AvatarList:LX/26P;

    .line 372117
    new-instance v0, LX/26P;

    const-string v1, "Photo"

    invoke-direct {v0, v1, v6}, LX/26P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/26P;->Photo:LX/26P;

    .line 372118
    new-instance v0, LX/26P;

    const-string v1, "Share"

    invoke-direct {v0, v1, v7}, LX/26P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/26P;->Share:LX/26P;

    .line 372119
    new-instance v0, LX/26P;

    const-string v1, "Video"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/26P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/26P;->Video:LX/26P;

    .line 372120
    new-instance v0, LX/26P;

    const-string v1, "ShareLargeImage"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/26P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/26P;->ShareLargeImage:LX/26P;

    .line 372121
    new-instance v0, LX/26P;

    const-string v1, "MusicPreviewCover"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/26P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/26P;->MusicPreviewCover:LX/26P;

    .line 372122
    new-instance v0, LX/26P;

    const-string v1, "CollageOptimisticPost"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/26P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/26P;->CollageOptimisticPost:LX/26P;

    .line 372123
    const/16 v0, 0x9

    new-array v0, v0, [LX/26P;

    sget-object v1, LX/26P;->AddFriend:LX/26P;

    aput-object v1, v0, v3

    sget-object v1, LX/26P;->Album:LX/26P;

    aput-object v1, v0, v4

    sget-object v1, LX/26P;->AvatarList:LX/26P;

    aput-object v1, v0, v5

    sget-object v1, LX/26P;->Photo:LX/26P;

    aput-object v1, v0, v6

    sget-object v1, LX/26P;->Share:LX/26P;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/26P;->Video:LX/26P;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/26P;->ShareLargeImage:LX/26P;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/26P;->MusicPreviewCover:LX/26P;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/26P;->CollageOptimisticPost:LX/26P;

    aput-object v2, v0, v1

    sput-object v0, LX/26P;->$VALUES:[LX/26P;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 372124
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/26P;
    .locals 1

    .prologue
    .line 372125
    const-class v0, LX/26P;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/26P;

    return-object v0
.end method

.method public static values()[LX/26P;
    .locals 1

    .prologue
    .line 372126
    sget-object v0, LX/26P;->$VALUES:[LX/26P;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/26P;

    return-object v0
.end method
