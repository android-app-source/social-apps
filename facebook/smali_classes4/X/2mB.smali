.class public LX/2mB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# instance fields
.field private final a:LX/2l7;

.field private final b:I


# direct methods
.method public constructor <init>(LX/2l7;I)V
    .locals 2

    .prologue
    .line 459340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459341
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2l7;

    iput-object v0, p0, LX/2mB;->a:LX/2l7;

    .line 459342
    if-gtz p2, :cond_0

    .line 459343
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ExpireTime should be positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 459344
    :cond_0
    iput p2, p0, LX/2mB;->b:I

    .line 459345
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 459346
    iget-object v0, p0, LX/2mB;->a:LX/2l7;

    invoke-virtual {v0}, LX/2l7;->b()J

    move-result-wide v2

    .line 459347
    iget v0, p0, LX/2mB;->b:I

    int-to-long v0, v0

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    .line 459348
    :try_start_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 459349
    iget-boolean v1, v0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v1, v1

    .line 459350
    if-eqz v1, :cond_0

    .line 459351
    iget-object v1, v0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 459352
    const-string v4, "bookmarks_expire_time"

    iget v5, p0, LX/2mB;->b:I

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 459353
    :goto_0
    return-object v0

    .line 459354
    :catch_0
    move-exception v0

    .line 459355
    const-class v1, LX/2mB;

    const-string v4, "Fail to fetch bookmarks from server while data in db was expired."

    invoke-static {v1, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 459356
    :cond_0
    sget-object v0, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    .line 459357
    :goto_1
    iget-object v1, p0, LX/2mB;->a:LX/2l7;

    .line 459358
    invoke-static {v1}, LX/2l7;->d(LX/2l7;)LX/0Xu;

    move-result-object v4

    invoke-static {v1, v4}, LX/2l7;->a(LX/2l7;LX/0Xu;)Ljava/util/List;

    move-result-object v4

    move-object v1, v4

    .line 459359
    new-instance v4, Lcom/facebook/bookmark/FetchBookmarksResult;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v4, v0, v2, v3, v1}, Lcom/facebook/bookmark/FetchBookmarksResult;-><init>(LX/0ta;JLX/0Px;)V

    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 459360
    iget-object v1, v0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 459361
    const-string v2, "bookmarks_expire_time"

    iget v3, p0, LX/2mB;->b:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 459362
    :cond_1
    sget-object v0, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    goto :goto_1
.end method
