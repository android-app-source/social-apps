.class public LX/31O;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;

.field public c:Ljava/lang/String;

.field public d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/7ER;


# direct methods
.method public constructor <init>(Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;LX/7ER;)V
    .locals 1

    .prologue
    .line 486985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486986
    iput-object p1, p0, LX/31O;->b:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;

    .line 486987
    iput-object p2, p0, LX/31O;->f:LX/7ER;

    .line 486988
    const/4 v0, -0x1

    iput v0, p0, LX/31O;->a:I

    .line 486989
    iget-object v0, p0, LX/31O;->b:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 486990
    iget-object v0, p0, LX/31O;->b:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/31O;->c:Ljava/lang/String;

    .line 486991
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 p1, 0x0

    const-string p2, "linear"

    aput-object p2, v0, p1

    const/4 p1, 0x1

    const-string p2, "linear_bucket"

    aput-object p2, v0, p1

    const/4 p1, 0x2

    const-string p2, "control_node"

    aput-object p2, v0, p1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object p1, p0, LX/31O;->c:Ljava/lang/String;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 486992
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/31O;->d:Ljava/util/HashMap;

    .line 486993
    return-void
.end method

.method public static a(LX/31O;Ljava/lang/String;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/7EQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 486872
    iget-object v0, p0, LX/31O;->f:LX/7ER;

    .line 486873
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 486874
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 486875
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    invoke-static {p0}, LX/0PB;->checkArgument(Z)V

    .line 486876
    iget-object p0, v0, LX/7ER;->a:Ljava/util/HashMap;

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486877
    return-void

    .line 486878
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static d(LX/31O;LX/7Fc;)I
    .locals 6

    .prologue
    .line 486942
    if-nez p1, :cond_0

    .line 486943
    iget v0, p0, LX/31O;->a:I

    add-int/lit8 v0, v0, 0x1

    .line 486944
    :goto_0
    return v0

    .line 486945
    :cond_0
    invoke-interface {p1}, LX/7Fb;->dF_()Ljava/lang/String;

    move-result-object v0

    .line 486946
    const-string v1, "direct"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 486947
    invoke-interface {p1}, LX/7Fb;->e()I

    move-result v0

    goto :goto_0

    .line 486948
    :cond_1
    const-string v1, "random"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 486949
    invoke-interface {p1}, LX/7Fb;->j()LX/0Px;

    move-result-object v0

    .line 486950
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-nez v1, :cond_7

    .line 486951
    const/4 v0, -0x1

    .line 486952
    :goto_1
    move v0, v0

    .line 486953
    goto :goto_0

    .line 486954
    :cond_2
    const-string v1, "branch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 486955
    const/4 v5, 0x0

    .line 486956
    invoke-interface {p1}, LX/7Fb;->d()Ljava/lang/String;

    move-result-object v0

    .line 486957
    iget-object v1, p0, LX/31O;->f:LX/7ER;

    .line 486958
    iget-object v2, v1, LX/7ER;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    move-object v1, v2

    .line 486959
    if-eqz v1, :cond_3

    iget-object v2, p0, LX/31O;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_8

    .line 486960
    :cond_3
    invoke-interface {p1}, LX/7Fb;->c()I

    move-result v0

    .line 486961
    :goto_2
    move v0, v0

    .line 486962
    goto :goto_0

    .line 486963
    :cond_4
    const-string v1, "qe"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 486964
    invoke-interface {p1}, LX/7Fb;->dG_()I

    move-result v0

    goto :goto_0

    .line 486965
    :cond_5
    const-string v1, "composite"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 486966
    check-cast p1, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;

    .line 486967
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->a()LX/7Fc;

    move-result-object v0

    .line 486968
    invoke-static {p0, v0}, LX/31O;->d(LX/31O;LX/7Fc;)I

    move-result v0

    .line 486969
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lt v0, v1, :cond_b

    .line 486970
    const/4 v0, -0x1

    .line 486971
    :goto_3
    move v0, v0

    .line 486972
    goto :goto_0

    .line 486973
    :cond_6
    iget v0, p0, LX/31O;->a:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 486974
    :cond_7
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 486975
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p0

    invoke-virtual {v1, p0}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 486976
    :cond_8
    sget-object v2, LX/7EP;->a:[I

    iget-object v3, p0, LX/31O;->d:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 486977
    :cond_9
    invoke-interface {p1}, LX/7Fb;->c()I

    move-result v0

    goto :goto_2

    .line 486978
    :pswitch_0
    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7EQ;

    .line 486979
    iget v1, v0, LX/7EQ;->a:I

    move v0, v1

    .line 486980
    invoke-interface {p1}, LX/7Fc;->k()LX/2uF;

    move-result-object v1

    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 486981
    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, LX/15i;->j(II)I

    move-result v4

    if-ne v4, v0, :cond_a

    .line 486982
    invoke-virtual {v3, v2, v5}, LX/15i;->j(II)I

    move-result v0

    goto/16 :goto_2

    .line 486983
    :cond_b
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Fc;

    .line 486984
    invoke-static {p0, v0}, LX/31O;->d(LX/31O;LX/7Fc;)I

    move-result v0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static g(LX/31O;)I
    .locals 2

    .prologue
    .line 486937
    iget-object v0, p0, LX/31O;->c:Ljava/lang/String;

    const-string v1, "control_node"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486938
    const/4 v0, -0x1

    .line 486939
    :goto_0
    return v0

    .line 486940
    :cond_0
    iget-object v0, p0, LX/31O;->b:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;->k()LX/0Px;

    move-result-object v0

    .line 486941
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 486902
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/31O;->e:Ljava/util/List;

    .line 486903
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 486904
    iget-object v0, p0, LX/31O;->b:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;->k()LX/0Px;

    move-result-object v0

    .line 486905
    iget-object v3, p0, LX/31O;->c:Ljava/lang/String;

    const-string v4, "linear"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, LX/31O;->c:Ljava/lang/String;

    const-string v4, "linear_bucket"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 486906
    :cond_0
    iget v3, p0, LX/31O;->a:I

    add-int/lit8 v3, v3, 0x1

    .line 486907
    :goto_0
    move v3, v3

    .line 486908
    iput v3, p0, LX/31O;->a:I

    .line 486909
    iget-object v3, p0, LX/31O;->c:Ljava/lang/String;

    const-string v4, "control_node"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget v3, p0, LX/31O;->a:I

    if-ltz v3, :cond_1

    iget v3, p0, LX/31O;->a:I

    invoke-static {p0}, LX/31O;->g(LX/31O;)I

    move-result v4

    if-lt v3, v4, :cond_2

    :cond_1
    move-object v0, v1

    .line 486910
    :goto_1
    return-object v0

    .line 486911
    :cond_2
    iget-object v3, p0, LX/31O;->f:LX/7ER;

    iget v4, p0, LX/31O;->a:I

    .line 486912
    iget-object v5, v3, LX/7ER;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486913
    iget v3, p0, LX/31O;->a:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowPageFragmentModel;

    .line 486914
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowPageFragmentModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_2
    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowPageFragmentModel$BucketsModel;

    .line 486915
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowPageFragmentModel$BucketsModel;->a()LX/0Px;

    move-result-object v6

    .line 486916
    new-instance v7, LX/7EN;

    invoke-direct {v7, p0}, LX/7EN;-><init>(LX/31O;)V

    invoke-static {v6, v7}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v6

    invoke-static {v6}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v6

    .line 486917
    new-instance v7, Ljava/util/Random;

    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    .line 486918
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v7

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;

    move-object v0, v6

    .line 486919
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486920
    iget-object v6, p0, LX/31O;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486921
    iget-object v6, p0, LX/31O;->e:Ljava/util/List;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486922
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 486923
    :cond_3
    iget-object v0, p0, LX/31O;->f:LX/7ER;

    iget-object v3, p0, LX/31O;->e:Ljava/util/List;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    .line 486924
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 486925
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_8

    const/4 v4, 0x1

    :goto_3
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 486926
    iget-object v4, v0, LX/7ER;->b:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486927
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v1

    goto/16 :goto_1

    :cond_4
    move-object v0, v2

    goto/16 :goto_1

    .line 486928
    :cond_5
    iget-object v3, p0, LX/31O;->c:Ljava/lang/String;

    const-string v4, "control_node"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 486929
    iget v3, p0, LX/31O;->a:I

    if-gez v3, :cond_6

    .line 486930
    iget-object v3, p0, LX/31O;->b:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;->j()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;

    move-result-object v3

    invoke-static {p0, v3}, LX/31O;->d(LX/31O;LX/7Fc;)I

    move-result v3

    goto/16 :goto_0

    .line 486931
    :cond_6
    iget-object v3, p0, LX/31O;->b:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;->k()LX/0Px;

    move-result-object v3

    .line 486932
    iget v4, p0, LX/31O;->a:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowPageFragmentModel;

    .line 486933
    invoke-virtual {v3}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowPageFragmentModel;->j()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;

    move-result-object v3

    .line 486934
    invoke-static {p0, v3}, LX/31O;->d(LX/31O;LX/7Fc;)I

    move-result v3

    goto/16 :goto_0

    .line 486935
    :cond_7
    const/4 v3, -0x1

    goto/16 :goto_0

    .line 486936
    :cond_8
    const/4 v4, 0x0

    goto :goto_3
.end method

.method public final b()I
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 486879
    iget-object v0, p0, LX/31O;->c:Ljava/lang/String;

    const-string v1, "control_node"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v3

    .line 486880
    :cond_0
    :goto_0
    return v1

    .line 486881
    :cond_1
    iget-object v0, p0, LX/31O;->b:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;->k()LX/0Px;

    move-result-object v0

    .line 486882
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowPageFragmentModel;

    .line 486883
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowPageFragmentModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v2

    :goto_1
    if-ge v4, v7, :cond_2

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowPageFragmentModel$BucketsModel;

    .line 486884
    const/4 v9, 0x0

    .line 486885
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowPageFragmentModel$BucketsModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v10, v9

    :goto_2
    if-ge v10, v12, :cond_6

    invoke-virtual {v11, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;

    .line 486886
    invoke-virtual {v8}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v8

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    if-eq v8, p0, :cond_5

    move v8, v9

    .line 486887
    :goto_3
    move v8, v8

    .line 486888
    if-nez v8, :cond_4

    .line 486889
    const/4 v9, 0x0

    .line 486890
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowPageFragmentModel$BucketsModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v10, v9

    :goto_4
    if-ge v10, v12, :cond_8

    invoke-virtual {v11, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;

    .line 486891
    invoke-virtual {v8}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v8

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    if-ne v8, p0, :cond_7

    .line 486892
    const/4 v8, 0x1

    .line 486893
    :goto_5
    move v0, v8

    .line 486894
    if-eqz v0, :cond_3

    move v1, v3

    .line 486895
    goto :goto_0

    .line 486896
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 486897
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 486898
    :cond_5
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    goto :goto_2

    .line 486899
    :cond_6
    const/4 v8, 0x1

    goto :goto_3

    .line 486900
    :cond_7
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    goto :goto_4

    :cond_8
    move v8, v9

    .line 486901
    goto :goto_5
.end method
