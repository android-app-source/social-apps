.class public LX/3NF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Sh;

.field private final b:LX/3Km;

.field public final c:LX/0tX;

.field public final d:LX/3MW;

.field private final e:LX/2Oi;

.field public final f:LX/3NG;

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FEb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sh;LX/3Km;LX/0tX;LX/3MW;LX/2Oi;LX/3NG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558684
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 558685
    iput-object v0, p0, LX/3NF;->g:LX/0Ot;

    .line 558686
    iput-object p1, p0, LX/3NF;->a:LX/0Sh;

    .line 558687
    iput-object p2, p0, LX/3NF;->b:LX/3Km;

    .line 558688
    iput-object p3, p0, LX/3NF;->c:LX/0tX;

    .line 558689
    iput-object p4, p0, LX/3NF;->d:LX/3MW;

    .line 558690
    iput-object p5, p0, LX/3NF;->e:LX/2Oi;

    .line 558691
    iput-object p6, p0, LX/3NF;->f:LX/3NG;

    .line 558692
    return-void
.end method

.method public static b(LX/0QB;)LX/3NF;
    .locals 11

    .prologue
    .line 558788
    new-instance v0, LX/3NF;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/3Km;->b(LX/0QB;)LX/3Km;

    move-result-object v2

    check-cast v2, LX/3Km;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/3MW;->b(LX/0QB;)LX/3MW;

    move-result-object v4

    check-cast v4, LX/3MW;

    invoke-static {p0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v5

    check-cast v5, LX/2Oi;

    .line 558789
    new-instance v10, LX/3NG;

    invoke-static {p0}, LX/3Km;->b(LX/0QB;)LX/3Km;

    move-result-object v6

    check-cast v6, LX/3Km;

    invoke-static {p0}, LX/3MV;->a(LX/0QB;)LX/3MV;

    move-result-object v7

    check-cast v7, LX/3MV;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    const-class v9, Landroid/content/Context;

    invoke-interface {p0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-direct {v10, v6, v7, v8, v9}, LX/3NG;-><init>(LX/3Km;LX/3MV;LX/0ad;Landroid/content/Context;)V

    .line 558790
    move-object v6, v10

    .line 558791
    check-cast v6, LX/3NG;

    invoke-direct/range {v0 .. v6}, LX/3NF;-><init>(LX/0Sh;LX/3Km;LX/0tX;LX/3MW;LX/2Oi;LX/3NG;)V

    .line 558792
    const/16 v1, 0x2784

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 558793
    iput-object v1, v0, LX/3NF;->g:LX/0Ot;

    .line 558794
    return-object v0
.end method

.method public static c(LX/3NF;Ljava/lang/String;I)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 558764
    iget-object v0, p0, LX/3NF;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 558765
    new-instance v0, LX/5Z0;

    invoke-direct {v0}, LX/5Z0;-><init>()V

    move-object v1, v0

    .line 558766
    const-string v0, "results_limit"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "name_search_string"

    invoke-virtual {v0, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "include_full_user_info"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "include_games"

    iget-object v0, p0, LX/3NF;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FEb;

    const/4 v4, 0x0

    .line 558767
    iget-object v5, v0, LX/FEb;->b:LX/0Uh;

    const/16 v6, 0x15c

    invoke-virtual {v5, v6, v4}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 558768
    sget-object v5, LX/FEa;->ALLOWED:LX/FEa;

    .line 558769
    iget-object v6, v0, LX/FEb;->b:LX/0Uh;

    const/16 p1, 0x157

    const/4 p2, 0x0

    invoke-virtual {v6, p1, p2}, LX/0Uh;->a(IZ)Z

    move-result v6

    move v6, v6

    .line 558770
    if-nez v6, :cond_4

    .line 558771
    sget-object v6, LX/FEa;->NOT_IN_GK:LX/FEa;

    .line 558772
    :goto_0
    move-object v6, v6

    .line 558773
    invoke-virtual {v5, v6}, LX/FEa;->compareTo(Ljava/lang/Enum;)I

    move-result v5

    if-nez v5, :cond_3

    const/4 v5, 0x1

    :goto_1
    move v5, v5

    .line 558774
    if-eqz v5, :cond_0

    const/4 v4, 0x1

    :cond_0
    move v0, v4

    .line 558775
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 558776
    iget-object v0, p0, LX/3NF;->d:LX/3MW;

    invoke-virtual {v0, v1}, LX/3MW;->a(LX/0gW;)V

    .line 558777
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 558778
    iget-object v1, p0, LX/3NF;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    const v1, -0x46480abe

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 558779
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 558780
    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel;

    .line 558781
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel;

    move-result-object v1

    if-nez v1, :cond_2

    .line 558782
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 558783
    :goto_2
    return-object v0

    :cond_2
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v0

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    .line 558784
    :cond_4
    iget-object v6, v0, LX/FEb;->c:LX/0s6;

    invoke-static {v6}, LX/36d;->a(LX/0s6;)Z

    move-result v6

    move v6, v6

    .line 558785
    if-nez v6, :cond_5

    .line 558786
    sget-object v6, LX/FEa;->NOT_SUPPORTED:LX/FEa;

    goto :goto_0

    .line 558787
    :cond_5
    sget-object v6, LX/FEa;->ALLOWED:LX/FEa;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/search/model/PlatformSearchData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 558710
    invoke-static {p0, p1, p2}, LX/3NF;->c(LX/3NF;Ljava/lang/String;I)LX/0Px;

    move-result-object v2

    .line 558711
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 558712
    if-eqz v2, :cond_3

    .line 558713
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel$EdgesModel;

    .line 558714
    iget-object v5, p0, LX/3NF;->f:LX/3NG;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel$EdgesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel$EdgesModel;->j()Ljava/lang/String;

    move-result-object v0

    const/4 v7, 0x0

    .line 558715
    const-string v8, "top"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, v5, LX/3NG;->a:LX/3Km;

    .line 558716
    iget-object v9, v8, LX/3Km;->a:LX/0Uh;

    const/16 v10, 0x1a8

    const/4 v0, 0x0

    invoke-virtual {v9, v10, v0}, LX/0Uh;->a(IZ)Z

    move-result v9

    move v8, v9

    .line 558717
    if-eqz v8, :cond_0

    iget-object v8, v5, LX/3NG;->d:LX/0ad;

    sget-short v9, LX/FDF;->c:S

    invoke-interface {v8, v9, v7}, LX/0ad;->a(SZ)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v7, 0x1

    .line 558718
    :cond_0
    invoke-virtual {v6}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessNameSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;->q()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 558719
    const/4 v0, 0x3

    const/4 p2, 0x2

    const/4 v8, 0x0

    .line 558720
    if-eqz v6, :cond_1

    invoke-interface {v6}, LX/5ZF;->o()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-interface {v6}, LX/5ZF;->o()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;->a()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_5

    .line 558721
    :cond_1
    :goto_1
    move-object v7, v8

    .line 558722
    :goto_2
    move-object v0, v7

    .line 558723
    if-eqz v0, :cond_2

    .line 558724
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 558725
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 558726
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_4
    invoke-static {v5, v6, v7}, LX/3NG;->a(LX/3NG;LX/5ZG;Z)Lcom/facebook/messaging/business/search/model/PlatformSearchData;

    move-result-object v7

    goto :goto_2

    .line 558727
    :cond_5
    new-instance v9, LX/4gf;

    invoke-direct {v9}, LX/4gf;-><init>()V

    invoke-interface {v6}, LX/5ZF;->o()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;->a()Ljava/lang/String;

    move-result-object v10

    .line 558728
    iput-object v10, v9, LX/4gf;->a:Ljava/lang/String;

    .line 558729
    move-object v9, v9

    .line 558730
    iget-object v10, v5, LX/3NG;->c:Landroid/content/Context;

    const v11, 0x7f083239

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 558731
    iput-object v10, v9, LX/4gf;->b:Ljava/lang/String;

    .line 558732
    move-object v9, v9

    .line 558733
    sget-object v10, LX/4ge;->OPEN_NATIVE:LX/4ge;

    .line 558734
    iput-object v10, v9, LX/4gf;->e:LX/4ge;

    .line 558735
    move-object v9, v9

    .line 558736
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "fb-messenger://instant_games/play?game_id="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v6}, LX/5ZF;->o()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/4gf;->d(Ljava/lang/String;)LX/4gf;

    move-result-object v9

    invoke-virtual {v9}, LX/4gf;->n()Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v9

    .line 558737
    new-instance v10, LX/Dd5;

    invoke-direct {v10}, LX/Dd5;-><init>()V

    .line 558738
    iput-object v9, v10, LX/Dd5;->a:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 558739
    sget-object v9, LX/Dd2;->GAMES:LX/Dd2;

    .line 558740
    iput-object v9, v10, LX/Dd3;->d:LX/Dd2;

    .line 558741
    new-instance v9, Lcom/facebook/user/model/Name;

    invoke-interface {v6}, LX/5ZF;->d()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v11}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 558742
    iput-object v9, v10, LX/Dd3;->a:Lcom/facebook/user/model/Name;

    .line 558743
    iput-boolean v7, v10, LX/Dd3;->c:Z

    .line 558744
    invoke-interface {v6}, LX/5ZF;->p()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$SquareLogoModel;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 558745
    new-instance v9, Lcom/facebook/user/model/PicSquare;

    new-instance v11, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-interface {v6}, LX/5ZF;->p()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$SquareLogoModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$SquareLogoModel;->a()I

    move-result v12

    invoke-interface {v6}, LX/5ZF;->p()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$SquareLogoModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$SquareLogoModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v11, v12, p1}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-direct {v9, v11, v8, v8}, Lcom/facebook/user/model/PicSquare;-><init>(Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;)V

    .line 558746
    iput-object v9, v10, LX/Dd3;->b:Lcom/facebook/user/model/PicSquare;

    .line 558747
    :cond_6
    sget-object v8, LX/0Q7;->a:LX/0Px;

    move-object v8, v8

    .line 558748
    invoke-interface {v6}, LX/5ZF;->o()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel;

    move-result-object v9

    if-eqz v9, :cond_b

    invoke-interface {v6}, LX/5ZF;->o()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel;->a()LX/0Px;

    move-result-object v9

    invoke-static {v9}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 558749
    invoke-interface {v6}, LX/5ZF;->o()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel;->a()LX/0Px;

    move-result-object v8

    move-object v9, v8

    .line 558750
    :goto_3
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v8

    if-lez v8, :cond_7

    .line 558751
    const/4 v8, 0x0

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel$TextLinesModel;

    invoke-virtual {v8}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel$TextLinesModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 558752
    iput-object v8, v10, LX/Dd5;->b:Ljava/lang/String;

    .line 558753
    :cond_7
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v8

    if-lt v8, p2, :cond_8

    .line 558754
    const/4 v8, 0x1

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel$TextLinesModel;

    invoke-virtual {v8}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel$TextLinesModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 558755
    iput-object v8, v10, LX/Dd5;->c:Ljava/lang/String;

    .line 558756
    :cond_8
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v8

    if-lt v8, v0, :cond_9

    .line 558757
    invoke-virtual {v9, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel$TextLinesModel;

    invoke-virtual {v8}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel$TextLinesModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 558758
    iput-object v8, v10, LX/Dd5;->d:Ljava/lang/String;

    .line 558759
    :cond_9
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v8

    const/4 v11, 0x4

    if-lt v8, v11, :cond_a

    .line 558760
    invoke-virtual {v9, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel$TextLinesModel;

    invoke-virtual {v8}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$InstantGameSearchDetailsQueryFragmentModel$InstantGameInfoModel$ListItemModel$TextLinesModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 558761
    iput-object v8, v10, LX/Dd5;->e:Ljava/lang/String;

    .line 558762
    :cond_a
    new-instance v8, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;

    invoke-direct {v8, v10}, Lcom/facebook/messaging/business/search/model/PlatformSearchGameData;-><init>(LX/Dd5;)V

    move-object v8, v8

    .line 558763
    goto/16 :goto_1

    :cond_b
    move-object v9, v8

    goto :goto_3
.end method

.method public final a()Lcom/facebook/user/model/User;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 558707
    iget-object v0, p0, LX/3NF;->b:LX/3Km;

    invoke-virtual {v0}, LX/3Km;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 558708
    const/4 v0, 0x0

    .line 558709
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/3NF;->b()Lcom/facebook/user/model/User;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/user/model/User;
    .locals 6

    .prologue
    .line 558693
    iget-object v0, p0, LX/3NF;->e:LX/2Oi;

    const-string v1, "881263441913087"

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 558694
    if-nez v0, :cond_0

    .line 558695
    const-string v0, "881263441913087"

    .line 558696
    iget-object v2, p0, LX/3NF;->a:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 558697
    new-instance v2, LX/5Z1;

    invoke-direct {v2}, LX/5Z1;-><init>()V

    move-object v2, v2

    .line 558698
    const-string v3, "include_full_user_info"

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 558699
    iget-object v3, p0, LX/3NF;->d:LX/3MW;

    invoke-virtual {v3, v2}, LX/3MW;->a(LX/0gW;)V

    .line 558700
    const-string v3, "business_page_id"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 558701
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->a:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    const-wide/16 v4, 0x258

    invoke-virtual {v2, v4, v5}, LX/0zO;->a(J)LX/0zO;

    move-result-object v2

    .line 558702
    iget-object v3, p0, LX/3NF;->c:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    const v3, -0x3dce0f06

    invoke-static {v2, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 558703
    iget-object v3, v2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v3

    .line 558704
    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BusinessPageQueryFragmentModel;

    .line 558705
    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_0
    move-object v0, v2

    .line 558706
    :cond_0
    return-object v0

    :cond_1
    invoke-static {v2}, LX/3NG;->a(LX/5ZD;)Lcom/facebook/user/model/User;

    move-result-object v2

    goto :goto_0
.end method
