.class public final LX/3Gw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3GF;


# instance fields
.field public final synthetic a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V
    .locals 0

    .prologue
    .line 542473
    iput-object p1, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 542437
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 542438
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->i()I

    move-result v0

    .line 542439
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/2oN;LX/2oN;LX/7M6;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 542445
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_1

    .line 542446
    :cond_0
    :goto_0
    return-void

    .line 542447
    :cond_1
    sget-object v0, LX/D7A;->c:[I

    invoke-virtual {p2}, LX/2oN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 542448
    :cond_2
    :goto_1
    sget-object v0, LX/D7A;->c:[I

    invoke-virtual {p1}, LX/2oN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 542449
    :cond_3
    :goto_2
    :pswitch_0
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2ow;

    invoke-direct {v1, p1, p2, p3}, LX/2ow;-><init>(LX/2oN;LX/2oN;LX/7M6;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0

    .line 542450
    :pswitch_1
    sget-object v0, LX/2oN;->NONE:LX/2oN;

    if-eq p1, v0, :cond_2

    .line 542451
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_COMMERCIAL_BREAK:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 542452
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x1

    .line 542453
    iput-boolean v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->R:Z

    .line 542454
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 542455
    iput-boolean v3, v0, LX/2pb;->M:Z

    .line 542456
    goto :goto_1

    .line 542457
    :pswitch_2
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->z:LX/2pa;

    sget-object v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->r:LX/0Px;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2pa;LX/0Px;Z)V

    goto :goto_1

    .line 542458
    :pswitch_3
    sget-object v0, LX/2oN;->NONE:LX/2oN;

    if-eq p2, v0, :cond_3

    .line 542459
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_COMMERCIAL_BREAK:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 542460
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 542461
    iput-boolean v3, v0, Lcom/facebook/video/player/RichVideoPlayer;->R:Z

    .line 542462
    goto :goto_2

    .line 542463
    :pswitch_4
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v1, p3, LX/7M6;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 542464
    if-nez v1, :cond_5

    .line 542465
    :cond_4
    :goto_3
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->C:LX/2pa;

    sget-object v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->r:LX/0Px;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2pa;LX/0Px;Z)V

    .line 542466
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v1, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 542467
    iput-wide v2, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->I:J

    .line 542468
    goto :goto_2

    .line 542469
    :cond_5
    iget-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->b:LX/3HR;

    invoke-virtual {v2, v1}, LX/3HR;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v2

    .line 542470
    if-eqz v2, :cond_4

    .line 542471
    iget-object v4, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1, v4, v2}, LX/3HR;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/video/engine/VideoPlayerParams;)LX/2pa;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->C:LX/2pa;

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 542472
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 542442
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    const/4 v1, 0x0

    .line 542443
    iput-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    .line 542444
    return-void
.end method

.method public final d()LX/BSW;
    .locals 1

    .prologue
    .line 542441
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->getLiveStreamingFormat(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)LX/BSW;

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 542440
    iget-object v0, p0, LX/3Gw;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->G:I

    return v0
.end method
