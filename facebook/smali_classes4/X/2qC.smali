.class public LX/2qC;
.super Landroid/util/LruCache;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "LX/7Of;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/09G;

.field private final c:LX/0lC;


# direct methods
.method public constructor <init>(ILX/09G;LX/0lC;)V
    .locals 1

    .prologue
    .line 470718
    invoke-direct {p0, p1}, Landroid/util/LruCache;-><init>(I)V

    .line 470719
    const-class v0, LX/2qC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2qC;->a:Ljava/lang/String;

    .line 470720
    iput-object p2, p0, LX/2qC;->b:LX/09G;

    .line 470721
    iput-object p3, p0, LX/2qC;->c:LX/0lC;

    .line 470722
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 470723
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 470724
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 470725
    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, LX/2qC;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Of;

    .line 470726
    if-nez v0, :cond_0

    .line 470727
    new-instance v0, LX/7Of;

    iget-object v1, p0, LX/2qC;->b:LX/09G;

    iget-object v2, p0, LX/2qC;->c:LX/0lC;

    invoke-direct {v0, p1, v1, v2}, LX/7Of;-><init>(Ljava/lang/String;LX/09G;LX/0lC;)V

    .line 470728
    iget-object v1, v0, LX/7Of;->d:LX/09G;

    iget-object v2, v0, LX/7Of;->c:Ljava/lang/String;

    new-instance v3, LX/7Od;

    invoke-direct {v3, v0}, LX/7Od;-><init>(LX/7Of;)V

    invoke-virtual {v1, v2, v3}, LX/09G;->a(Ljava/lang/String;LX/0TF;)V

    .line 470729
    invoke-super {p0, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 470730
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)LX/7Oe;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 470731
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Of;

    .line 470732
    if-eqz v0, :cond_0

    .line 470733
    iget-object p1, v0, LX/7Of;->e:LX/7Oe;

    move-object v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470734
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 470735
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 470736
    check-cast p3, LX/7Of;

    .line 470737
    if-eqz p3, :cond_0

    .line 470738
    iget-object p0, p3, LX/7Of;->d:LX/09G;

    iget-object p1, p3, LX/7Of;->c:Ljava/lang/String;

    invoke-virtual {p0, p1}, LX/09G;->a(Ljava/lang/String;)V

    .line 470739
    :cond_0
    return-void
.end method
