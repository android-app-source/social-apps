.class public final LX/2Kt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/2Kt;


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/0Sh;

.field private final c:LX/2Ku;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ezw;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ezv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/0Sh;LX/2Ku;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/2Ku;",
            "LX/0Ot",
            "<",
            "LX/Ezv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ezw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 393996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393997
    iput-object p1, p0, LX/2Kt;->a:LX/0Uh;

    .line 393998
    iput-object p2, p0, LX/2Kt;->b:LX/0Sh;

    .line 393999
    iput-object p3, p0, LX/2Kt;->c:LX/2Ku;

    .line 394000
    iput-object p5, p0, LX/2Kt;->d:LX/0Ot;

    .line 394001
    iput-object p4, p0, LX/2Kt;->e:LX/0Ot;

    .line 394002
    return-void
.end method

.method public static a(LX/0QB;)LX/2Kt;
    .locals 9

    .prologue
    .line 394003
    sget-object v0, LX/2Kt;->f:LX/2Kt;

    if-nez v0, :cond_1

    .line 394004
    const-class v1, LX/2Kt;

    monitor-enter v1

    .line 394005
    :try_start_0
    sget-object v0, LX/2Kt;->f:LX/2Kt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394006
    if-eqz v2, :cond_0

    .line 394007
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 394008
    new-instance v3, LX/2Kt;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v0}, LX/2Ku;->a(LX/0QB;)LX/2Ku;

    move-result-object v6

    check-cast v6, LX/2Ku;

    const/16 v7, 0x2272

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2273

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/2Kt;-><init>(LX/0Uh;LX/0Sh;LX/2Ku;LX/0Ot;LX/0Ot;)V

    .line 394009
    move-object v0, v3

    .line 394010
    sput-object v0, LX/2Kt;->f:LX/2Kt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394011
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394012
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394013
    :cond_1
    sget-object v0, LX/2Kt;->f:LX/2Kt;

    return-object v0

    .line 394014
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394015
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 394016
    iget-object v0, p0, LX/2Kt;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 394017
    iget-object v0, p0, LX/2Kt;->a:LX/0Uh;

    const/16 v1, 0x39a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 394018
    :goto_0
    return-void

    .line 394019
    :cond_0
    iget-object v1, p0, LX/2Kt;->c:LX/2Ku;

    iget-object v0, p0, LX/2Kt;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b2;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 394020
    iget-object v1, p0, LX/2Kt;->c:LX/2Ku;

    iget-object v0, p0, LX/2Kt;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b2;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0
.end method
