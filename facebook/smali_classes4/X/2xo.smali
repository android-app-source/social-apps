.class public LX/2xo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:J

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:J

.field public h:Ljava/lang/Object;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/Boolean;

.field public m:Landroid/net/Uri;

.field public n:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(J)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, -0x1

    .line 479310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 479311
    iput-wide v0, p0, LX/2xo;->b:J

    .line 479312
    iput-wide v0, p0, LX/2xo;->c:J

    .line 479313
    iput-wide v0, p0, LX/2xo;->d:J

    .line 479314
    iput-wide v0, p0, LX/2xo;->e:J

    .line 479315
    iput-wide v0, p0, LX/2xo;->f:J

    .line 479316
    iput-wide v0, p0, LX/2xo;->g:J

    .line 479317
    iput-object v2, p0, LX/2xo;->j:Ljava/lang/Boolean;

    .line 479318
    iput-object v2, p0, LX/2xo;->k:Ljava/lang/Boolean;

    .line 479319
    iput-object v2, p0, LX/2xo;->l:Ljava/lang/Boolean;

    .line 479320
    iput-object v2, p0, LX/2xo;->m:Landroid/net/Uri;

    .line 479321
    iput-object v2, p0, LX/2xo;->n:Landroid/util/Pair;

    .line 479322
    iput-wide p1, p0, LX/2xo;->a:J

    .line 479323
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 12

    .prologue
    const/4 v0, 0x1

    const-wide/16 v6, -0x1

    .line 479324
    invoke-virtual {p0}, LX/2xo;->i()Z

    move-result v1

    if-nez v1, :cond_1

    .line 479325
    const/4 v0, 0x0

    .line 479326
    :cond_0
    :goto_0
    return v0

    .line 479327
    :cond_1
    const-wide/16 v8, -0x1

    .line 479328
    iget-wide v10, p0, LX/2xo;->b:J

    cmp-long v10, v10, v8

    if-eqz v10, :cond_2

    iget-wide v10, p0, LX/2xo;->c:J

    cmp-long v10, v10, v8

    if-nez v10, :cond_6

    .line 479329
    :cond_2
    :goto_1
    move-wide v2, v8

    .line 479330
    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    iget-wide v4, p0, LX/2xo;->a:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 479331
    const/4 v0, 0x3

    goto :goto_0

    .line 479332
    :cond_3
    iget-wide v2, p0, LX/2xo;->f:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    iget-wide v2, p0, LX/2xo;->f:J

    iget-wide v4, p0, LX/2xo;->c:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 479333
    :cond_4
    iget-wide v2, p0, LX/2xo;->e:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_5

    iget-wide v2, p0, LX/2xo;->e:J

    iget-wide v4, p0, LX/2xo;->c:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 479334
    :cond_5
    const/4 v0, 0x2

    goto :goto_0

    :cond_6
    iget-wide v8, p0, LX/2xo;->c:J

    iget-wide v10, p0, LX/2xo;->b:J

    sub-long/2addr v8, v10

    goto :goto_1
.end method

.method public final i()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, -0x1

    .line 479335
    iget-wide v2, p0, LX/2xo;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p0, LX/2xo;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 479336
    :cond_0
    :goto_0
    return v0

    .line 479337
    :cond_1
    iget-wide v2, p0, LX/2xo;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 479338
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 479339
    invoke-static {p0}, LX/15f;->a(Ljava/lang/Object;)LX/15g;

    move-result-object v1

    const-string v2, "bitmapSize"

    iget-object v0, p0, LX/2xo;->n:Landroid/util/Pair;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "callerContext"

    iget-object v2, p0, LX/2xo;->h:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "failureMessage"

    iget-object v2, p0, LX/2xo;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "isBitmapCacheHit"

    iget-object v2, p0, LX/2xo;->j:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "isMemoryCacheHit"

    iget-object v2, p0, LX/2xo;->k:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "isDiskCacheHit"

    iget-object v2, p0, LX/2xo;->l:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "isReadyToLog"

    invoke-virtual {p0}, LX/2xo;->i()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Z)LX/15g;

    move-result-object v0

    const-string v1, "timeEnterViewport"

    iget-wide v2, p0, LX/2xo;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/15g;->a(Ljava/lang/String;J)LX/15g;

    move-result-object v0

    const-string v1, "timeSubmit"

    iget-wide v2, p0, LX/2xo;->d:J

    invoke-virtual {v0, v1, v2, v3}, LX/15g;->a(Ljava/lang/String;J)LX/15g;

    move-result-object v0

    const-string v1, "timeIntermediateSet"

    iget-wide v2, p0, LX/2xo;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/15g;->a(Ljava/lang/String;J)LX/15g;

    move-result-object v0

    const-string v1, "timeSuccess"

    iget-wide v2, p0, LX/2xo;->f:J

    invoke-virtual {v0, v1, v2, v3}, LX/15g;->a(Ljava/lang/String;J)LX/15g;

    move-result-object v0

    const-string v1, "timeFailure"

    iget-wide v2, p0, LX/2xo;->g:J

    invoke-virtual {v0, v1, v2, v3}, LX/15g;->a(Ljava/lang/String;J)LX/15g;

    move-result-object v0

    const-string v1, "timeExitViewport"

    iget-wide v2, p0, LX/2xo;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/15g;->a(Ljava/lang/String;J)LX/15g;

    move-result-object v0

    const-string v1, "uri"

    iget-object v2, p0, LX/2xo;->m:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "verdict"

    .line 479340
    invoke-virtual {p0}, LX/2xo;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 479341
    const-string v2, "UNKNOWN"

    :goto_1
    move-object v2, v2

    .line 479342
    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "vpvdMs"

    iget-wide v2, p0, LX/2xo;->a:J

    invoke-virtual {v0, v1, v2, v3}, LX/15g;->a(Ljava/lang/String;J)LX/15g;

    move-result-object v0

    invoke-virtual {v0}, LX/15g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/2xo;->n:Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, LX/2xo;->n:Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 479343
    :pswitch_0
    const-string v2, "IGNORED"

    goto :goto_1

    .line 479344
    :pswitch_1
    const-string v2, "RENDERED_IN_TIME"

    goto :goto_1

    .line 479345
    :pswitch_2
    const-string v2, "NOT_RENDERED_IN_TIME"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
