.class public final enum LX/38a;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/38a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/38a;

.field public static final enum CONNECTED:LX/38a;

.field public static final enum CONNECTING:LX/38a;

.field public static final enum DISCONNECTED:LX/38a;

.field public static final enum SELECTING:LX/38a;

.field public static final enum SUSPENDED:LX/38a;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 521112
    new-instance v0, LX/38a;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v2}, LX/38a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/38a;->DISCONNECTED:LX/38a;

    .line 521113
    new-instance v0, LX/38a;

    const-string v1, "SELECTING"

    invoke-direct {v0, v1, v3}, LX/38a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/38a;->SELECTING:LX/38a;

    .line 521114
    new-instance v0, LX/38a;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v4}, LX/38a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/38a;->CONNECTING:LX/38a;

    .line 521115
    new-instance v0, LX/38a;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v5}, LX/38a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/38a;->CONNECTED:LX/38a;

    .line 521116
    new-instance v0, LX/38a;

    const-string v1, "SUSPENDED"

    invoke-direct {v0, v1, v6}, LX/38a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/38a;->SUSPENDED:LX/38a;

    .line 521117
    const/4 v0, 0x5

    new-array v0, v0, [LX/38a;

    sget-object v1, LX/38a;->DISCONNECTED:LX/38a;

    aput-object v1, v0, v2

    sget-object v1, LX/38a;->SELECTING:LX/38a;

    aput-object v1, v0, v3

    sget-object v1, LX/38a;->CONNECTING:LX/38a;

    aput-object v1, v0, v4

    sget-object v1, LX/38a;->CONNECTED:LX/38a;

    aput-object v1, v0, v5

    sget-object v1, LX/38a;->SUSPENDED:LX/38a;

    aput-object v1, v0, v6

    sput-object v0, LX/38a;->$VALUES:[LX/38a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 521118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/38a;
    .locals 1

    .prologue
    .line 521119
    const-class v0, LX/38a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/38a;

    return-object v0
.end method

.method public static values()[LX/38a;
    .locals 1

    .prologue
    .line 521120
    sget-object v0, LX/38a;->$VALUES:[LX/38a;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/38a;

    return-object v0
.end method
