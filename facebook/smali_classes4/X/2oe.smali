.class public final LX/2oe;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2os;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/RichVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 0

    .prologue
    .line 467205
    iput-object p1, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/RichVideoPlayer;B)V
    .locals 0

    .prologue
    .line 467206
    invoke-direct {p0, p1}, LX/2oe;-><init>(Lcom/facebook/video/player/RichVideoPlayer;)V

    return-void
.end method

.method private a(LX/2os;)V
    .locals 2

    .prologue
    .line 467207
    iget-boolean v0, p1, LX/2os;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/RichVideoPlayer;->B:Z

    if-nez v0, :cond_1

    .line 467208
    iget-object v0, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x1

    .line 467209
    iput-boolean v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->B:Z

    .line 467210
    iget-object v0, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    .line 467211
    iget-object p1, v1, LX/2pb;->y:LX/2qV;

    move-object v1, p1

    .line 467212
    invoke-virtual {v1}, LX/2qV;->isPlayingState()Z

    move-result v1

    .line 467213
    iput-boolean v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->A:Z

    .line 467214
    iget-object v0, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/RichVideoPlayer;->A:Z

    if-eqz v0, :cond_0

    .line 467215
    iget-object v0, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 467216
    :cond_0
    :goto_0
    return-void

    .line 467217
    :cond_1
    iget-boolean v0, p1, LX/2os;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/RichVideoPlayer;->B:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/RichVideoPlayer;->P:Z

    if-nez v0, :cond_0

    .line 467218
    iget-object v0, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    .line 467219
    iput-boolean v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->B:Z

    .line 467220
    iget-object v0, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-boolean v0, v0, Lcom/facebook/video/player/RichVideoPlayer;->A:Z

    if-eqz v0, :cond_0

    .line 467221
    iget-object v0, p0, LX/2oe;->a:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2os;",
            ">;"
        }
    .end annotation

    .prologue
    .line 467222
    const-class v0, LX/2os;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 467223
    check-cast p1, LX/2os;

    invoke-direct {p0, p1}, LX/2oe;->a(LX/2os;)V

    return-void
.end method
