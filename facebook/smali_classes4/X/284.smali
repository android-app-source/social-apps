.class public LX/284;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0po;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/Context;

.field public final c:LX/0ad;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field public final e:Ljava/io/File;

.field public final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Or;Landroid/content/Context;LX/0ad;LX/0pq;Ljava/util/concurrent/ExecutorService;Ljava/io/File;Ljava/lang/String;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p6    # Ljava/io/File;
        .annotation runtime Lcom/facebook/api/feedcache/db/storage/FeedCacheStorageDirectory;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/api/feed/annotation/FeedDatabaseName;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Context;",
            "LX/0ad;",
            "LX/0pq;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 373668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373669
    iput-object p1, p0, LX/284;->a:LX/0Or;

    .line 373670
    iput-object p2, p0, LX/284;->b:Landroid/content/Context;

    .line 373671
    iput-object p3, p0, LX/284;->c:LX/0ad;

    .line 373672
    iput-object p5, p0, LX/284;->d:Ljava/util/concurrent/ExecutorService;

    .line 373673
    iput-object p6, p0, LX/284;->e:Ljava/io/File;

    .line 373674
    iput-object p7, p0, LX/284;->f:Ljava/lang/String;

    .line 373675
    invoke-virtual {p4, p0}, LX/0pq;->a(LX/0po;)V

    .line 373676
    return-void
.end method

.method public static a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 373677
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/net/URI;Ljava/net/URI;Ljava/io/File;Z)V
    .locals 5

    .prologue
    .line 373678
    invoke-virtual {p2}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/net/URI;->relativize(Ljava/net/URI;)Ljava/net/URI;

    move-result-object v0

    .line 373679
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1, v0}, Ljava/net/URI;->resolve(Ljava/net/URI;)Ljava/net/URI;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 373680
    const/4 p1, 0x1

    const/4 p0, 0x0

    .line 373681
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 373682
    sget-object v0, LX/GVU;->a:Ljava/lang/String;

    const-string v2, "Source \'%s\' does not exist"

    new-array v3, p1, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, p0

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373683
    :cond_0
    if-nez p3, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 373684
    sget-object v0, LX/GVU;->a:Ljava/lang/String;

    const-string v2, "Destination \'%s\' already exists"

    new-array v3, p1, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, p0

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373685
    :cond_1
    :goto_0
    return-void

    .line 373686
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_3

    .line 373687
    sget-object v0, LX/GVU;->a:Ljava/lang/String;

    const-string v2, "Destination \'%s\' directory cannot be created"

    new-array v3, p1, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, p0

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 373688
    :cond_3
    invoke-virtual {p2, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 373689
    sget-object v0, LX/GVU;->a:Ljava/lang/String;

    const-string v2, "moveFile failure: \'%s\' to \'%s\'"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, p0

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, p1

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/284;
    .locals 8

    .prologue
    .line 373690
    new-instance v0, LX/284;

    const/16 v1, 0x15e8

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {p0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v4

    check-cast v4, LX/0pq;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0qL;->b(LX/0QB;)Ljava/io/File;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    invoke-static {p0}, LX/0v5;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, LX/284;-><init>(LX/0Or;Landroid/content/Context;LX/0ad;LX/0pq;Ljava/util/concurrent/ExecutorService;Ljava/io/File;Ljava/lang/String;)V

    .line 373691
    return-object v0
.end method

.method private static b(LX/284;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 373660
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 373661
    :cond_0
    :goto_0
    return-object v0

    .line 373662
    :cond_1
    invoke-static {p0}, LX/284;->e(LX/284;)Ljava/io/File;

    move-result-object v1

    .line 373663
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 373664
    invoke-static {v1, p1}, LX/284;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 373665
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 373666
    goto :goto_0
.end method

.method public static e(LX/284;)Ljava/io/File;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 373667
    iget-object v0, p0, LX/284;->b:Landroid/content/Context;

    const-string v1, "bootstrap"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private static f(LX/284;)Ljava/net/URI;
    .locals 2

    .prologue
    .line 373659
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/284;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final U_()V
    .locals 0

    .prologue
    .line 373657
    invoke-virtual {p0}, LX/284;->b()V

    .line 373658
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 373652
    const-string v0, "BootstrapCache.purgeRecentlyUsedFiles"

    const v1, 0x55b2f822

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 373653
    :try_start_0
    iget-object v0, p0, LX/284;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, p1}, LX/284;->b(LX/284;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1}, LX/GVU;->a(Ljava/util/concurrent/ExecutorService;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373654
    const v0, 0xab3c079

    invoke-static {v0}, LX/02m;->a(I)V

    .line 373655
    return-void

    .line 373656
    :catchall_0
    move-exception v0

    const v1, -0x51584185

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 373647
    const-string v0, "BootstrapCache.trimToNothing"

    const v1, 0x4bc9d3c5    # 2.6453898E7f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 373648
    :try_start_0
    iget-object v0, p0, LX/284;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/284;->e(LX/284;)Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1}, LX/GVU;->a(Ljava/util/concurrent/ExecutorService;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373649
    const v0, 0x2ecfe311

    invoke-static {v0}, LX/02m;->a(I)V

    .line 373650
    return-void

    .line 373651
    :catchall_0
    move-exception v0

    const v1, 0x4596eff3    # 4829.9937f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 373624
    const-string v0, "BootstrapCache.persistRecentlyUsedFiles"

    const v1, 0x517b09a2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 373625
    :try_start_0
    const/4 v1, 0x0

    .line 373626
    iget-object v0, p0, LX/284;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 373627
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 373628
    :cond_0
    :goto_0
    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373629
    if-nez v0, :cond_1

    .line 373630
    const v0, -0x51a001a9

    invoke-static {v0}, LX/02m;->a(I)V

    .line 373631
    :goto_1
    return-void

    .line 373632
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v0

    .line 373633
    invoke-static {p0}, LX/284;->f(LX/284;)Ljava/net/URI;

    move-result-object v1

    .line 373634
    const/4 v4, 0x1

    .line 373635
    iget-object v2, p0, LX/284;->e:Ljava/io/File;

    invoke-static {v1, v0, v2, v4}, LX/284;->a(Ljava/net/URI;Ljava/net/URI;Ljava/io/File;Z)V

    .line 373636
    iget-object v2, p0, LX/284;->b:Landroid/content/Context;

    iget-object v3, p0, LX/284;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-static {v1, v0, v2, v4}, LX/284;->a(Ljava/net/URI;Ljava/net/URI;Ljava/io/File;Z)V

    .line 373637
    iget-object v2, p0, LX/284;->c:LX/0ad;

    sget-short v3, LX/0wf;->ae:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373638
    :goto_2
    const v0, 0x7af7c989

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_1

    :catchall_0
    move-exception v0

    const v1, 0x7bcfc006

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 373639
    :cond_2
    :try_start_2
    invoke-static {p0}, LX/284;->e(LX/284;)Ljava/io/File;

    move-result-object v2

    .line 373640
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    move-result v3

    if-nez v3, :cond_3

    move-object v0, v1

    .line 373641
    goto :goto_0

    .line 373642
    :cond_3
    invoke-static {v2, v0}, LX/284;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 373643
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 373644
    goto :goto_0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 373645
    :cond_4
    iget-object v2, p0, LX/284;->b:Landroid/content/Context;

    const-string v3, "timeline_db"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 373646
    const/4 v3, 0x1

    invoke-static {v1, v0, v2, v3}, LX/284;->a(Ljava/net/URI;Ljava/net/URI;Ljava/io/File;Z)V

    goto :goto_2
.end method

.method public final d()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 373604
    const-string v0, "BootstrapCache.restoreRecentlyUsedFiles"

    const v2, -0x5f3ce1d4

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 373605
    :try_start_0
    iget-object v0, p0, LX/284;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, LX/284;->b(LX/284;Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 373606
    if-nez v0, :cond_0

    .line 373607
    const v0, 0x4cc44888

    invoke-static {v0}, LX/02m;->a(I)V

    .line 373608
    :goto_0
    return-void

    .line 373609
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v3

    .line 373610
    invoke-static {p0}, LX/284;->f(LX/284;)Ljava/net/URI;

    move-result-object v4

    .line 373611
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 373612
    if-nez v5, :cond_1

    .line 373613
    const v0, 0x75784662

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 373614
    :cond_1
    :try_start_2
    array-length v6, v5

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_4

    aget-object v0, v5, v2

    .line 373615
    if-eqz v0, :cond_3

    .line 373616
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 373617
    if-eqz v7, :cond_3

    .line 373618
    array-length v8, v7

    move v0, v1

    :goto_2
    if-ge v0, v8, :cond_3

    aget-object v9, v7, v0

    .line 373619
    if-eqz v9, :cond_2

    .line 373620
    const/4 v10, 0x0

    invoke-static {v3, v4, v9, v10}, LX/284;->a(Ljava/net/URI;Ljava/net/URI;Ljava/io/File;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 373621
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 373622
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 373623
    :cond_4
    const v0, -0x575d0f6d

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x5190c971

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
