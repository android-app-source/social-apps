.class public final LX/3Aj;
.super LX/0gS;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 526036
    invoke-direct {p0}, LX/0gS;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Double;)LX/3Aj;
    .locals 1

    .prologue
    .line 526026
    const-string v0, "latitude"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 526027
    return-object p0
.end method

.method public final b(Ljava/lang/Double;)LX/3Aj;
    .locals 1

    .prologue
    .line 526034
    const-string v0, "longitude"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 526035
    return-object p0
.end method

.method public final c(Ljava/lang/Double;)LX/3Aj;
    .locals 1

    .prologue
    .line 526032
    const-string v0, "accuracy"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 526033
    return-object p0
.end method

.method public final d(Ljava/lang/Double;)LX/3Aj;
    .locals 1

    .prologue
    .line 526030
    const-string v0, "speed"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 526031
    return-object p0
.end method

.method public final e(Ljava/lang/Double;)LX/3Aj;
    .locals 1

    .prologue
    .line 526028
    const-string v0, "stale_time"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Double;)V

    .line 526029
    return-object p0
.end method
