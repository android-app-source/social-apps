.class public LX/28b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/28b;


# instance fields
.field public final a:LX/0WJ;

.field public final b:LX/28W;

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/335;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2XA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0WJ;LX/28W;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/28W;",
            "Ljava/util/Set",
            "<",
            "LX/335;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/2XA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374308
    iput-object p1, p0, LX/28b;->a:LX/0WJ;

    .line 374309
    iput-object p2, p0, LX/28b;->b:LX/28W;

    .line 374310
    iput-object p3, p0, LX/28b;->c:Ljava/util/Set;

    .line 374311
    iput-object p4, p0, LX/28b;->d:Ljava/util/Set;

    .line 374312
    return-void
.end method

.method public static a(LX/0QB;)LX/28b;
    .locals 9

    .prologue
    .line 374313
    sget-object v0, LX/28b;->e:LX/28b;

    if-nez v0, :cond_1

    .line 374314
    const-class v1, LX/28b;

    monitor-enter v1

    .line 374315
    :try_start_0
    sget-object v0, LX/28b;->e:LX/28b;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 374316
    if-eqz v2, :cond_0

    .line 374317
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 374318
    new-instance v5, LX/28b;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-static {v0}, LX/28W;->a(LX/0QB;)LX/28W;

    move-result-object v4

    check-cast v4, LX/28W;

    invoke-static {v0}, LX/28V;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v6

    .line 374319
    new-instance v7, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    new-instance p0, LX/2Xj;

    invoke-direct {p0, v0}, LX/2Xj;-><init>(LX/0QB;)V

    invoke-direct {v7, v8, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v7, v7

    .line 374320
    invoke-direct {v5, v3, v4, v6, v7}, LX/28b;-><init>(LX/0WJ;LX/28W;Ljava/util/Set;Ljava/util/Set;)V

    .line 374321
    move-object v0, v5

    .line 374322
    sput-object v0, LX/28b;->e:LX/28b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374323
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 374324
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 374325
    :cond_1
    sget-object v0, LX/28b;->e:LX/28b;

    return-object v0

    .line 374326
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 374327
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
