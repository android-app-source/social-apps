.class public final LX/37L;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/34s;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/34s;


# direct methods
.method public constructor <init>(LX/34s;)V
    .locals 1

    .prologue
    .line 500963
    iput-object p1, p0, LX/37L;->b:LX/34s;

    .line 500964
    move-object v0, p1

    .line 500965
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 500966
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 500967
    const-string v0, "GroupCommerceItemAttachmentTitleComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 500968
    if-ne p0, p1, :cond_1

    .line 500969
    :cond_0
    :goto_0
    return v0

    .line 500970
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 500971
    goto :goto_0

    .line 500972
    :cond_3
    check-cast p1, LX/37L;

    .line 500973
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 500974
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 500975
    if-eq v2, v3, :cond_0

    .line 500976
    iget-object v2, p0, LX/37L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/37L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/37L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 500977
    goto :goto_0

    .line 500978
    :cond_4
    iget-object v2, p1, LX/37L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
