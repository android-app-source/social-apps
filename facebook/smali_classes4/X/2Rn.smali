.class public LX/2Rn;
.super LX/2QY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2QY",
        "<",
        "Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/platform/server/handler/ParcelableString;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0Or;LX/2Ro;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/2Ro;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410146
    const-string v0, "platform_get_canonical_profile_ids"

    invoke-direct {p0, v0, p1, p2}, LX/2QY;-><init>(Ljava/lang/String;LX/0Or;LX/0e6;)V

    .line 410147
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 410148
    check-cast p1, Ljava/util/HashMap;

    .line 410149
    invoke-static {p1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/HashMap;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 410150
    const-string v0, "app_scoped_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/server/protocol/GetCanonicalProfileIdsMethod$Params;

    return-object v0
.end method
