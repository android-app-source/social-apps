.class public LX/33Q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 493051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/text/Editable;)Landroid/text/Editable;
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 493052
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    move-result v0

    .line 493053
    if-eqz v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    if-eq v0, v1, :cond_1

    .line 493054
    :cond_0
    invoke-interface {p0, v1}, Landroid/text/Editable;->append(C)Landroid/text/Editable;

    .line 493055
    :cond_1
    return-object p0
.end method

.method public static a(Ljava/lang/CharSequence;II)Ljava/lang/CharSequence;
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 493056
    instance-of v0, p0, Landroid/text/Spanned;

    if-nez v0, :cond_0

    .line 493057
    invoke-interface {p0, p1, p2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 493058
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    .line 493059
    check-cast v0, Landroid/text/Spanned;

    .line 493060
    const-class v1, Landroid/text/style/ParagraphStyle;

    invoke-interface {v0, p1, p2, v1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/ParagraphStyle;

    .line 493061
    array-length v6, v1

    .line 493062
    if-nez v6, :cond_1

    .line 493063
    invoke-interface {p0, p1, p2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 493064
    :cond_1
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move v5, v2

    move v4, v2

    .line 493065
    :goto_1
    if-nez v2, :cond_4

    if-nez v4, :cond_4

    if-ge v5, v6, :cond_4

    .line 493066
    aget-object v8, v1, v5

    invoke-interface {v0, v8}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v8

    .line 493067
    aget-object v9, v1, v5

    invoke-interface {v0, v9}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v9

    .line 493068
    if-nez v4, :cond_2

    if-ge v8, p1, :cond_2

    if-ge p1, v9, :cond_2

    .line 493069
    add-int/lit8 v4, p1, 0x1

    const-string v10, "\n"

    invoke-interface {v7, p1, v4, v10}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move v4, v3

    .line 493070
    :cond_2
    if-nez v2, :cond_3

    if-ge v8, p2, :cond_3

    if-ge p2, v9, :cond_3

    .line 493071
    add-int/lit8 v2, p2, -0x1

    const-string v8, "\n"

    invoke-interface {v7, v2, p2, v8}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move v2, v3

    .line 493072
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 493073
    :cond_4
    invoke-interface {v7, p1, p2}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method
