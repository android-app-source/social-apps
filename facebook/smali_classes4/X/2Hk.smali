.class public LX/2Hk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile u:LX/2Hk;


# instance fields
.field public final a:Ljava/util/concurrent/ScheduledExecutorService;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2C2;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0SG;

.field public final d:LX/0Uo;

.field public final e:LX/2Hm;

.field private final f:LX/0Xl;

.field private final g:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
    .end annotation
.end field

.field public final h:LX/0pu;

.field private final i:LX/0ad;

.field public final j:LX/03V;

.field private final k:LX/0Uh;

.field private l:LX/2Ho;

.field private m:LX/2Ho;

.field private n:Z

.field private o:Ljava/util/concurrent/ScheduledFuture;

.field private p:Ljava/util/concurrent/ScheduledFuture;

.field public q:Ljava/util/concurrent/ScheduledFuture;

.field private final r:Ljava/lang/Runnable;

.field private final s:Ljava/lang/Runnable;

.field public final t:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0SG;Ljava/util/Set;LX/0Uo;LX/2Hm;LX/0Xl;Landroid/os/Handler;LX/0pu;LX/0ad;LX/03V;LX/0Uh;)V
    .locals 3
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/push/mqtt/annotations/ForMqttThreadWakeup;
        .end annotation
    .end param
    .param p6    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p7    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0SG;",
            "Ljava/util/Set",
            "<",
            "LX/2C2;",
            ">;",
            "LX/0Uo;",
            "LX/2Hm;",
            "LX/0Xl;",
            "Landroid/os/Handler;",
            "LX/0pu;",
            "LX/0ad;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391043
    sget-object v0, LX/2Ho;->STOPPED:LX/2Ho;

    iput-object v0, p0, LX/2Hk;->l:LX/2Ho;

    .line 391044
    sget-object v0, LX/2Ho;->STOPPED:LX/2Ho;

    iput-object v0, p0, LX/2Hk;->m:LX/2Ho;

    .line 391045
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Hk;->n:Z

    .line 391046
    new-instance v0, Lcom/facebook/push/mqtt/service/MqttClientStateManager$1;

    const-string v1, "MqttClientStateManager"

    const-string v2, "appStopped"

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/push/mqtt/service/MqttClientStateManager$1;-><init>(LX/2Hk;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/2Hk;->r:Ljava/lang/Runnable;

    .line 391047
    new-instance v0, Lcom/facebook/push/mqtt/service/MqttClientStateManager$2;

    const-string v1, "MqttClientStateManager"

    const-string v2, "deviceStopped"

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/push/mqtt/service/MqttClientStateManager$2;-><init>(LX/2Hk;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/2Hk;->s:Ljava/lang/Runnable;

    .line 391048
    new-instance v0, Lcom/facebook/push/mqtt/service/MqttClientStateManager$3;

    const-string v1, "MqttClientStateManager"

    const-string v2, "appStateCheck"

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/push/mqtt/service/MqttClientStateManager$3;-><init>(LX/2Hk;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/2Hk;->t:Ljava/lang/Runnable;

    .line 391049
    iput-object p1, p0, LX/2Hk;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 391050
    iput-object p2, p0, LX/2Hk;->c:LX/0SG;

    .line 391051
    iput-object p3, p0, LX/2Hk;->b:Ljava/util/Set;

    .line 391052
    iput-object p4, p0, LX/2Hk;->d:LX/0Uo;

    .line 391053
    iput-object p5, p0, LX/2Hk;->e:LX/2Hm;

    .line 391054
    iput-object p6, p0, LX/2Hk;->f:LX/0Xl;

    .line 391055
    iput-object p7, p0, LX/2Hk;->g:Landroid/os/Handler;

    .line 391056
    iput-object p8, p0, LX/2Hk;->h:LX/0pu;

    .line 391057
    iput-object p9, p0, LX/2Hk;->i:LX/0ad;

    .line 391058
    iput-object p10, p0, LX/2Hk;->j:LX/03V;

    .line 391059
    iput-object p11, p0, LX/2Hk;->k:LX/0Uh;

    .line 391060
    return-void
.end method

.method public static a(LX/0QB;)LX/2Hk;
    .locals 15

    .prologue
    .line 390953
    sget-object v0, LX/2Hk;->u:LX/2Hk;

    if-nez v0, :cond_1

    .line 390954
    const-class v1, LX/2Hk;

    monitor-enter v1

    .line 390955
    :try_start_0
    sget-object v0, LX/2Hk;->u:LX/2Hk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390956
    if-eqz v2, :cond_0

    .line 390957
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390958
    new-instance v3, LX/2Hk;

    invoke-static {v0}, LX/29C;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    .line 390959
    new-instance v6, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance v8, LX/2Hl;

    invoke-direct {v8, v0}, LX/2Hl;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, v8}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    .line 390960
    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v7

    check-cast v7, LX/0Uo;

    invoke-static {v0}, LX/2Hm;->a(LX/0QB;)LX/2Hm;

    move-result-object v8

    check-cast v8, LX/2Hm;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v9

    check-cast v9, LX/0Xl;

    invoke-static {v0}, LX/1rR;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v10

    check-cast v10, Landroid/os/Handler;

    invoke-static {v0}, LX/0pu;->a(LX/0QB;)LX/0pu;

    move-result-object v11

    check-cast v11, LX/0pu;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v14

    check-cast v14, LX/0Uh;

    invoke-direct/range {v3 .. v14}, LX/2Hk;-><init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0SG;Ljava/util/Set;LX/0Uo;LX/2Hm;LX/0Xl;Landroid/os/Handler;LX/0pu;LX/0ad;LX/03V;LX/0Uh;)V

    .line 390961
    move-object v0, v3

    .line 390962
    sput-object v0, LX/2Hk;->u:LX/2Hk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390963
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390964
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390965
    :cond_1
    sget-object v0, LX/2Hk;->u:LX/2Hk;

    return-object v0

    .line 390966
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390967
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2Hk;Ljava/lang/String;)V
    .locals 14

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 390980
    iget-object v1, p0, LX/2Hk;->l:LX/2Ho;

    .line 390981
    iget-object v3, p0, LX/2Hk;->m:LX/2Ho;

    .line 390982
    iget-object v8, p0, LX/2Hk;->d:LX/0Uo;

    invoke-virtual {v8}, LX/0Uo;->o()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 390983
    sget-object v8, LX/2Ho;->ACTIVE:LX/2Ho;

    .line 390984
    :goto_0
    move-object v4, v8

    .line 390985
    iput-object v4, p0, LX/2Hk;->l:LX/2Ho;

    .line 390986
    iget-object v8, p0, LX/2Hk;->c:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    iget-object v10, p0, LX/2Hk;->e:LX/2Hm;

    .line 390987
    iget-wide v12, v10, LX/2Hm;->g:J

    move-wide v10, v12

    .line 390988
    sub-long/2addr v8, v10

    .line 390989
    iget-object v10, p0, LX/2Hk;->e:LX/2Hm;

    .line 390990
    iget-object v11, v10, LX/2Hm;->e:LX/0pu;

    invoke-virtual {v11}, LX/0pu;->a()Z

    move-result v11

    move v10, v11

    .line 390991
    if-eqz v10, :cond_11

    .line 390992
    sget-object v8, LX/2Ho;->ACTIVE:LX/2Ho;

    .line 390993
    :goto_1
    move-object v4, v8

    .line 390994
    iput-object v4, p0, LX/2Hk;->m:LX/2Ho;

    .line 390995
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, LX/2Hk;->l:LX/2Ho;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LX/2Hk;->m:LX/2Ho;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LX/2Hk;->o:Ljava/util/concurrent/ScheduledFuture;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 390996
    const-string v4, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, LX/2Hk;->l:LX/2Ho;

    sget-object v5, LX/2Ho;->ACTIVE:LX/2Ho;

    if-eq v4, v5, :cond_0

    .line 390997
    sget-object v4, LX/2Ho;->ACTIVE:LX/2Ho;

    iput-object v4, p0, LX/2Hk;->l:LX/2Ho;

    .line 390998
    :cond_0
    iget-object v4, p0, LX/2Hk;->l:LX/2Ho;

    sget-object v5, LX/2Ho;->ACTIVE:LX/2Ho;

    if-ne v4, v5, :cond_1

    .line 390999
    sget-object v4, LX/2Ho;->ACTIVE:LX/2Ho;

    iput-object v4, p0, LX/2Hk;->m:LX/2Ho;

    .line 391000
    :cond_1
    iget-object v4, p0, LX/2Hk;->m:LX/2Ho;

    sget-object v5, LX/2Ho;->STOPPED:LX/2Ho;

    if-ne v4, v5, :cond_2

    .line 391001
    sget-object v4, LX/2Ho;->STOPPED:LX/2Ho;

    iput-object v4, p0, LX/2Hk;->l:LX/2Ho;

    .line 391002
    :cond_2
    iget-object v4, p0, LX/2Hk;->l:LX/2Ho;

    if-eq v4, v1, :cond_4

    move v1, v0

    .line 391003
    :goto_2
    iget-object v4, p0, LX/2Hk;->m:LX/2Ho;

    if-eq v4, v3, :cond_5

    move v3, v0

    .line 391004
    :goto_3
    if-nez v1, :cond_7

    if-nez v3, :cond_7

    .line 391005
    iget-boolean v3, p0, LX/2Hk;->n:Z

    if-eqz v3, :cond_6

    .line 391006
    :cond_3
    return-void

    :cond_4
    move v1, v2

    .line 391007
    goto :goto_2

    :cond_5
    move v3, v2

    .line 391008
    goto :goto_3

    :cond_6
    move v3, v0

    .line 391009
    :cond_7
    iput-boolean v0, p0, LX/2Hk;->n:Z

    .line 391010
    iget-object v0, p0, LX/2Hk;->m:LX/2Ho;

    sget-object v4, LX/2Ho;->ACTIVE:LX/2Ho;

    if-ne v0, v4, :cond_9

    if-eqz v3, :cond_9

    .line 391011
    iget-object v0, p0, LX/2Hk;->p:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_8

    .line 391012
    iget-object v0, p0, LX/2Hk;->p:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 391013
    iput-object v7, p0, LX/2Hk;->p:Ljava/util/concurrent/ScheduledFuture;

    .line 391014
    :cond_8
    iget-object v0, p0, LX/2Hk;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2C2;

    .line 391015
    invoke-interface {v0}, LX/2C2;->onDeviceActive()V

    goto :goto_4

    .line 391016
    :cond_9
    iget-object v0, p0, LX/2Hk;->l:LX/2Ho;

    sget-object v4, LX/2Ho;->ACTIVE:LX/2Ho;

    if-ne v0, v4, :cond_b

    if-eqz v1, :cond_b

    .line 391017
    iget-object v0, p0, LX/2Hk;->o:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_a

    .line 391018
    iget-object v0, p0, LX/2Hk;->o:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 391019
    iput-object v7, p0, LX/2Hk;->o:Ljava/util/concurrent/ScheduledFuture;

    .line 391020
    :cond_a
    iget-object v0, p0, LX/2Hk;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2C2;

    .line 391021
    invoke-interface {v0}, LX/2C2;->onAppActive()V

    goto :goto_5

    .line 391022
    :cond_b
    iget-object v0, p0, LX/2Hk;->l:LX/2Ho;

    sget-object v2, LX/2Ho;->PAUSED:LX/2Ho;

    if-ne v0, v2, :cond_c

    if-eqz v1, :cond_c

    .line 391023
    iget-object v0, p0, LX/2Hk;->o:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_c

    .line 391024
    iget-object v0, p0, LX/2Hk;->a:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, p0, LX/2Hk;->r:Ljava/lang/Runnable;

    invoke-static {p0}, LX/2Hk;->e(LX/2Hk;)J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v4, v5, v6}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2Hk;->o:Ljava/util/concurrent/ScheduledFuture;

    .line 391025
    :cond_c
    iget-object v0, p0, LX/2Hk;->m:LX/2Ho;

    sget-object v2, LX/2Ho;->PAUSED:LX/2Ho;

    if-ne v0, v2, :cond_d

    if-eqz v3, :cond_d

    .line 391026
    iget-object v0, p0, LX/2Hk;->p:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_d

    .line 391027
    iget-object v0, p0, LX/2Hk;->a:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, p0, LX/2Hk;->s:Ljava/lang/Runnable;

    invoke-static {p0}, LX/2Hk;->e(LX/2Hk;)J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v4, v5, v6}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/2Hk;->p:Ljava/util/concurrent/ScheduledFuture;

    .line 391028
    :cond_d
    iget-object v0, p0, LX/2Hk;->l:LX/2Ho;

    sget-object v2, LX/2Ho;->STOPPED:LX/2Ho;

    if-ne v0, v2, :cond_e

    if-eqz v1, :cond_e

    .line 391029
    iput-object v7, p0, LX/2Hk;->o:Ljava/util/concurrent/ScheduledFuture;

    .line 391030
    iget-object v0, p0, LX/2Hk;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2C2;

    .line 391031
    invoke-interface {v0}, LX/2C2;->onAppStopped()V

    goto :goto_6

    .line 391032
    :cond_e
    iget-object v0, p0, LX/2Hk;->m:LX/2Ho;

    sget-object v1, LX/2Ho;->STOPPED:LX/2Ho;

    if-ne v0, v1, :cond_3

    if-eqz v3, :cond_3

    .line 391033
    iput-object v7, p0, LX/2Hk;->p:Ljava/util/concurrent/ScheduledFuture;

    .line 391034
    iget-object v0, p0, LX/2Hk;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2C2;

    .line 391035
    invoke-interface {v0}, LX/2C2;->onDeviceStopped()V

    goto :goto_7

    .line 391036
    :cond_f
    iget-object v8, p0, LX/2Hk;->d:LX/0Uo;

    invoke-virtual {v8}, LX/0Uo;->q()J

    move-result-wide v8

    invoke-static {p0}, LX/2Hk;->e(LX/2Hk;)J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-gez v8, :cond_10

    .line 391037
    sget-object v8, LX/2Ho;->PAUSED:LX/2Ho;

    goto/16 :goto_0

    .line 391038
    :cond_10
    sget-object v8, LX/2Ho;->STOPPED:LX/2Ho;

    goto/16 :goto_0

    .line 391039
    :cond_11
    invoke-static {p0}, LX/2Hk;->e(LX/2Hk;)J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-gez v8, :cond_12

    .line 391040
    sget-object v8, LX/2Ho;->PAUSED:LX/2Ho;

    goto/16 :goto_1

    .line 391041
    :cond_12
    sget-object v8, LX/2Ho;->STOPPED:LX/2Ho;

    goto/16 :goto_1
.end method

.method public static e(LX/2Hk;)J
    .locals 6

    .prologue
    .line 390979
    iget-object v0, p0, LX/2Hk;->i:LX/0ad;

    sget-wide v2, LX/2IF;->a:J

    const-wide/16 v4, 0x78

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final declared-synchronized init()V
    .locals 5

    .prologue
    .line 390968
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Hk;->f:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    .line 390969
    const-string v0, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    sget-object v2, LX/2Hm;->a:Ljava/lang/String;

    const-string v3, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    sget-object v4, LX/2Hm;->b:Ljava/lang/String;

    invoke-static {v0, v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    .line 390970
    new-instance v2, LX/2zk;

    invoke-direct {v2, p0}, LX/2zk;-><init>(LX/2Hk;)V

    .line 390971
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 390972
    invoke-interface {v1, v0, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 390973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 390974
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2Hk;->g:Landroid/os/Handler;

    invoke-interface {v1, v0}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 390975
    const-string v0, "init"

    invoke-static {p0, v0}, LX/2Hk;->a$redex0(LX/2Hk;Ljava/lang/String;)V

    .line 390976
    iget-object v0, p0, LX/2Hk;->k:LX/0Uh;

    const/16 v1, 0xed

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 390977
    iget-object v0, p0, LX/2Hk;->h:LX/0pu;

    new-instance v1, LX/76G;

    invoke-direct {v1, p0}, LX/76G;-><init>(LX/2Hk;)V

    iget-object v2, p0, LX/2Hk;->g:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, LX/0pu;->a(LX/0q0;Landroid/os/Handler;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390978
    :cond_1
    monitor-exit p0

    return-void
.end method
