.class public LX/2Cx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/2Cx;


# instance fields
.field public final a:LX/0yD;

.field public final b:LX/0Zb;

.field public final c:LX/0So;

.field public d:J

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(LX/0yD;LX/0Zb;LX/0So;)V
    .locals 2
    .param p3    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v0, -0x1

    .line 383536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383537
    iput-wide v0, p0, LX/2Cx;->d:J

    .line 383538
    iput-wide v0, p0, LX/2Cx;->e:J

    .line 383539
    iput-wide v0, p0, LX/2Cx;->f:J

    .line 383540
    iput-object p1, p0, LX/2Cx;->a:LX/0yD;

    .line 383541
    iput-object p2, p0, LX/2Cx;->b:LX/0Zb;

    .line 383542
    iput-object p3, p0, LX/2Cx;->c:LX/0So;

    .line 383543
    return-void
.end method

.method public static a(LX/0QB;)LX/2Cx;
    .locals 6

    .prologue
    .line 383523
    sget-object v0, LX/2Cx;->g:LX/2Cx;

    if-nez v0, :cond_1

    .line 383524
    const-class v1, LX/2Cx;

    monitor-enter v1

    .line 383525
    :try_start_0
    sget-object v0, LX/2Cx;->g:LX/2Cx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 383526
    if-eqz v2, :cond_0

    .line 383527
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 383528
    new-instance p0, LX/2Cx;

    invoke-static {v0}, LX/0yD;->a(LX/0QB;)LX/0yD;

    move-result-object v3

    check-cast v3, LX/0yD;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {p0, v3, v4, v5}, LX/2Cx;-><init>(LX/0yD;LX/0Zb;LX/0So;)V

    .line 383529
    move-object v0, p0

    .line 383530
    sput-object v0, LX/2Cx;->g:LX/2Cx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 383531
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 383532
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 383533
    :cond_1
    sget-object v0, LX/2Cx;->g:LX/2Cx;

    return-object v0

    .line 383534
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 383535
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Cx;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 383516
    iget-wide v0, p0, LX/2Cx;->d:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 383517
    const-string v0, "last_location_update_age_ms"

    iget-object v1, p0, LX/2Cx;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/2Cx;->d:J

    sub-long/2addr v2, v4

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 383518
    :cond_0
    iget-wide v0, p0, LX/2Cx;->e:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    .line 383519
    const-string v0, "last_write_attempt_age_ms"

    iget-object v1, p0, LX/2Cx;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/2Cx;->e:J

    sub-long/2addr v2, v4

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 383520
    :cond_1
    iget-wide v0, p0, LX/2Cx;->f:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    .line 383521
    const-string v0, "last_write_success_age_ms"

    iget-object v1, p0, LX/2Cx;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/2Cx;->f:J

    sub-long/2addr v2, v4

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 383522
    :cond_2
    return-object p1
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 383512
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "background_location"

    .line 383513
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 383514
    move-object v0, v0

    .line 383515
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;)V
    .locals 11

    .prologue
    .line 383481
    iget-object v0, p0, LX/2Cx;->b:LX/0Zb;

    const-string v1, "background_location_server_write_start"

    invoke-static {v1}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {p0, v1}, LX/2Cx;->a(LX/2Cx;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "locationPackages"

    iget-object v3, p1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->a:LX/0Px;

    .line 383482
    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-virtual {v4}, LX/0mC;->b()LX/162;

    move-result-object v5

    .line 383483
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/location/LocationSignalDataPackage;

    .line 383484
    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-virtual {v7}, LX/0mC;->c()LX/0m9;

    move-result-object v8

    .line 383485
    iget-object v7, v4, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    if-eqz v7, :cond_0

    .line 383486
    const-string v7, "age_ms"

    iget-object v9, p0, LX/2Cx;->a:LX/0yD;

    iget-object v10, v4, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v9, v10}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v9

    invoke-virtual {v8, v7, v9, v10}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 383487
    const-string v9, "accuracy_meters"

    iget-object v7, v4, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v7}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v8, v9, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Float;)LX/0m9;

    .line 383488
    :cond_0
    move-object v4, v8

    .line 383489
    invoke-virtual {v5, v4}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 383490
    :cond_1
    move-object v3, v5

    .line 383491
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "trace_ids"

    iget-object v3, p1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->b:LX/0Px;

    .line 383492
    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-virtual {v4}, LX/0mC;->b()LX/162;

    move-result-object v5

    .line 383493
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 383494
    invoke-virtual {v5, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_1

    .line 383495
    :cond_2
    move-object v3, v5

    .line 383496
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383497
    iget-object v0, p0, LX/2Cx;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/2Cx;->e:J

    .line 383498
    return-void
.end method

.method public final a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 383507
    const-string v0, "background_location_server_write_failure"

    invoke-static {v0}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "failure_reason"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "task_tag"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "uploader_name"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 383508
    if-eqz p1, :cond_0

    .line 383509
    const-string v1, "succeeded"

    iget-boolean v2, p1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->a:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "is_best_device"

    iget-boolean v3, p1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->c:Z

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "is_location_history_enabled"

    iget-boolean v3, p1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->b:Z

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 383510
    :cond_0
    iget-object v1, p0, LX/2Cx;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383511
    return-void
.end method

.method public final a(Lcom/facebook/location/ImmutableLocation;JJ)V
    .locals 4

    .prologue
    .line 383505
    iget-object v0, p0, LX/2Cx;->b:LX/0Zb;

    const-string v1, "background_location_inconsistent_ages_between_clocks"

    invoke-static {v1}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "wall_clock_timestamp_ms"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "since_boot_clock_timestamp_ns"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->h()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "wall_clock_now_ms"

    invoke-virtual {v1, v2, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "since_boot_clock_now_ms"

    invoke-virtual {v1, v2, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383506
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 383503
    iget-object v0, p0, LX/2Cx;->b:LX/0Zb;

    const-string v1, "background_location_loading_location_from_storage_fail"

    invoke-static {v1}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {p0, v1}, LX/2Cx;->a(LX/2Cx;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "exception"

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383504
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 383499
    const-string v0, "background_location_server_write_success"

    invoke-static {v0}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "task_tag"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "uploader_name"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 383500
    iget-object v1, p0, LX/2Cx;->b:LX/0Zb;

    invoke-static {p0, v0}, LX/2Cx;->a(LX/2Cx;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383501
    iget-object v0, p0, LX/2Cx;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/2Cx;->f:J

    .line 383502
    return-void
.end method
