.class public LX/3Qn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile s:LX/3Qn;


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/2Sc;

.field private final e:LX/3Qo;

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Bi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final h:LX/3Qe;

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final j:LX/0SG;

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Bc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final l:LX/0Sy;

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final n:LX/0WJ;

.field public o:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final p:LX/0kb;

.field public q:LX/3Ql;

.field private r:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7C3;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Or;LX/2Sc;LX/3Qo;LX/3Qe;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0Sy;LX/0WJ;LX/0kb;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/search/bootstrap/abtest/UsePeriodicPull;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/search/bootstrap/abtest/BootstrapNoRefresh;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Sc;",
            "LX/3Qo;",
            "LX/3Qe;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            "LX/0WJ;",
            "LX/0kb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565904
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 565905
    iput-object v0, p0, LX/3Qn;->f:LX/0Ot;

    .line 565906
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 565907
    iput-object v0, p0, LX/3Qn;->g:LX/0Ot;

    .line 565908
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 565909
    iput-object v0, p0, LX/3Qn;->k:LX/0Ot;

    .line 565910
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 565911
    iput-object v0, p0, LX/3Qn;->m:LX/0Ot;

    .line 565912
    iput-object p1, p0, LX/3Qn;->a:Ljava/util/concurrent/ExecutorService;

    .line 565913
    iput-object p2, p0, LX/3Qn;->b:Ljava/util/concurrent/ExecutorService;

    .line 565914
    iput-object p4, p0, LX/3Qn;->c:LX/0Or;

    .line 565915
    iput-object p5, p0, LX/3Qn;->d:LX/2Sc;

    .line 565916
    iput-object p6, p0, LX/3Qn;->e:LX/3Qo;

    .line 565917
    iput-object p7, p0, LX/3Qn;->h:LX/3Qe;

    .line 565918
    iput-object p8, p0, LX/3Qn;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 565919
    iput-object p9, p0, LX/3Qn;->j:LX/0SG;

    .line 565920
    iput-object p10, p0, LX/3Qn;->l:LX/0Sy;

    .line 565921
    iput-object p11, p0, LX/3Qn;->n:LX/0WJ;

    .line 565922
    sget-object v0, LX/3Ql;->NO_ERROR:LX/3Ql;

    iput-object v0, p0, LX/3Qn;->q:LX/3Ql;

    .line 565923
    iget-object v0, p0, LX/3Qn;->h:LX/3Qe;

    invoke-direct {p0}, LX/3Qn;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/3Qe;->a(Z)V

    .line 565924
    iput-object p12, p0, LX/3Qn;->p:LX/0kb;

    .line 565925
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 565901
    const-string v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 565902
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3c

    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public static a(LX/0QB;)LX/3Qn;
    .locals 3

    .prologue
    .line 565891
    sget-object v0, LX/3Qn;->s:LX/3Qn;

    if-nez v0, :cond_1

    .line 565892
    const-class v1, LX/3Qn;

    monitor-enter v1

    .line 565893
    :try_start_0
    sget-object v0, LX/3Qn;->s:LX/3Qn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 565894
    if-eqz v2, :cond_0

    .line 565895
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/3Qn;->b(LX/0QB;)LX/3Qn;

    move-result-object v0

    sput-object v0, LX/3Qn;->s:LX/3Qn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565896
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 565897
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565898
    :cond_1
    sget-object v0, LX/3Qn;->s:LX/3Qn;

    return-object v0

    .line 565899
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 565900
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(J)Z
    .locals 13

    .prologue
    const-wide/32 v10, 0x5265c00

    const/4 v1, 0x0

    .line 565882
    iget-object v0, p0, LX/3Qn;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 565883
    sub-long v4, v2, p1

    iget-object v0, p0, LX/3Qn;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v6, LX/100;->m:I

    const/16 v7, 0x18

    invoke-interface {v0, v6, v7}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v6, v0

    const-wide/32 v8, 0x36ee80

    mul-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-gez v0, :cond_0

    move v0, v1

    .line 565884
    :goto_0
    return v0

    .line 565885
    :cond_0
    iget-object v0, p0, LX/3Qn;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v4, LX/100;->n:S

    invoke-interface {v0, v4, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 565886
    const-string v0, "wifi"

    iget-object v4, p0, LX/3Qn;->p:LX/0kb;

    invoke-virtual {v4}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sub-long v4, v2, p1

    cmp-long v0, v4, v10

    if-gez v0, :cond_1

    move v0, v1

    .line 565887
    goto :goto_0

    .line 565888
    :cond_1
    invoke-static {p0, v2, v3}, LX/3Qn;->b(LX/3Qn;J)Z

    move-result v0

    if-eqz v0, :cond_2

    sub-long/2addr v2, p1

    cmp-long v0, v2, v10

    if-gez v0, :cond_2

    move v0, v1

    .line 565889
    goto :goto_0

    .line 565890
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/3Qn;
    .locals 13

    .prologue
    .line 565873
    new-instance v0, LX/3Qn;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    const/16 v3, 0x364

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x156d

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v5

    check-cast v5, LX/2Sc;

    .line 565874
    new-instance v6, LX/3Qo;

    invoke-direct {v6}, LX/3Qo;-><init>()V

    .line 565875
    const/16 v7, 0x32b6

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xafd

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x113f

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x32ac

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xac0

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    .line 565876
    iput-object v7, v6, LX/3Qo;->a:LX/0Ot;

    iput-object v8, v6, LX/3Qo;->b:LX/0Ot;

    iput-object v9, v6, LX/3Qo;->c:LX/0Ot;

    iput-object v10, v6, LX/3Qo;->d:LX/0Ot;

    iput-object v11, v6, LX/3Qo;->e:LX/0Ot;

    .line 565877
    move-object v6, v6

    .line 565878
    check-cast v6, LX/3Qo;

    invoke-static {p0}, LX/3Qe;->a(LX/0QB;)LX/3Qe;

    move-result-object v7

    check-cast v7, LX/3Qe;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static {p0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v10

    check-cast v10, LX/0Sy;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v11

    check-cast v11, LX/0WJ;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v12

    check-cast v12, LX/0kb;

    invoke-direct/range {v0 .. v12}, LX/3Qn;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Or;LX/2Sc;LX/3Qo;LX/3Qe;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0Sy;LX/0WJ;LX/0kb;)V

    .line 565879
    const/16 v1, 0x32bb

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x32bc

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x32b9

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x1032

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    .line 565880
    iput-object v1, v0, LX/3Qn;->f:LX/0Ot;

    iput-object v2, v0, LX/3Qn;->g:LX/0Ot;

    iput-object v3, v0, LX/3Qn;->k:LX/0Ot;

    iput-object v4, v0, LX/3Qn;->m:LX/0Ot;

    iput-object v5, v0, LX/3Qn;->o:LX/0Uh;

    .line 565881
    return-object v0
.end method

.method private static b(LX/3Qn;J)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 565856
    iget-object v0, p0, LX/3Qn;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v3, LX/100;->p:C

    const-string v4, "00:00"

    invoke-interface {v0, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 565857
    iget-object v0, p0, LX/3Qn;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v4, LX/100;->o:C

    const-string v5, "00:00"

    invoke-interface {v0, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 565858
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 565859
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v6, "HH:mm"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v5, v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 565860
    const-string v6, "PST"

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 565861
    invoke-virtual {v5, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 565862
    invoke-static {v3}, LX/3Qn;->a(Ljava/lang/String;)I

    move-result v3

    .line 565863
    invoke-static {v0}, LX/3Qn;->a(Ljava/lang/String;)I

    move-result v0

    .line 565864
    invoke-static {v4}, LX/3Qn;->a(Ljava/lang/String;)I

    move-result v4

    .line 565865
    if-ne v3, v0, :cond_0

    move v0, v1

    .line 565866
    :goto_0
    return v0

    .line 565867
    :cond_0
    if-ge v3, v0, :cond_1

    .line 565868
    if-le v4, v3, :cond_3

    if-ge v4, v0, :cond_3

    move v0, v2

    .line 565869
    goto :goto_0

    .line 565870
    :cond_1
    if-gt v4, v3, :cond_2

    if-ge v4, v0, :cond_3

    :cond_2
    move v0, v2

    .line 565871
    goto :goto_0

    :cond_3
    move v0, v1

    .line 565872
    goto :goto_0
.end method

.method public static declared-synchronized c(LX/3Qn;)V
    .locals 1

    .prologue
    .line 565805
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/3Qn;->r:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565806
    monitor-exit p0

    return-void

    .line 565807
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized c(LX/3Qn;J)V
    .locals 5

    .prologue
    .line 565836
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3Qn;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Qn;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 565837
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 565838
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_2

    .line 565839
    :try_start_1
    iget-object v0, p0, LX/3Qn;->h:LX/3Qe;

    const-wide/16 v2, -0x1

    const-string v1, "time_to_load_bootstrap_keywords"

    invoke-virtual {v0, v2, v3, v1}, LX/3Qe;->a(JLjava/lang/String;)V

    .line 565840
    :goto_1
    iget-object v0, p0, LX/3Qn;->e:LX/3Qo;

    .line 565841
    new-instance v1, LX/8c4;

    invoke-direct {v1}, LX/8c4;-><init>()V

    move-object v1, v1

    .line 565842
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v2

    .line 565843
    iget-object v1, v0, LX/3Qo;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7B0;

    sget-object v3, LX/7Az;->BOOTSTRAP:LX/7Az;

    invoke-virtual {v1, v3, v2}, LX/7B0;->a(LX/7Az;LX/0zO;)V

    .line 565844
    iget-object v1, v2, LX/0zO;->m:LX/0gW;

    move-object v3, v1

    .line 565845
    const-string v4, "enable_keywords"

    iget-object v1, v0, LX/3Qo;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Uh;

    sget p1, LX/2SU;->t:I

    const/4 p2, 0x0

    invoke-virtual {v1, p1, p2}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 565846
    iget-object v1, v0, LX/3Qo;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-virtual {v1, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 565847
    new-instance v2, LX/8cX;

    invoke-direct {v2, v0}, LX/8cX;-><init>(LX/3Qo;)V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 565848
    iput-object v0, p0, LX/3Qn;->r:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565849
    :try_start_2
    new-instance v0, LX/8cb;

    invoke-direct {v0, p0}, LX/8cb;-><init>(LX/3Qn;)V

    .line 565850
    iget-object v1, p0, LX/3Qn;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, LX/3Qn;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 565851
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 565852
    :cond_2
    :try_start_3
    iget-object v0, p0, LX/3Qn;->h:LX/3Qe;

    iget-object v1, p0, LX/3Qn;->j:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, p1

    const-string v1, "time_to_load_bootstrap_keywords"

    invoke-virtual {v0, v2, v3, v1}, LX/3Qe;->a(JLjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 565853
    :catch_0
    move-exception v0

    .line 565854
    :try_start_4
    sget-object v1, LX/3Ql;->FETCH_DB_BOOTSTRAP_KEYWORD_PRE_FAIL:LX/3Ql;

    iput-object v1, p0, LX/3Qn;->q:LX/3Ql;

    .line 565855
    iget-object v1, p0, LX/3Qn;->d:LX/2Sc;

    sget-object v2, LX/3Ql;->FETCH_DB_BOOTSTRAP_KEYWORD_PRE_FAIL:LX/3Ql;

    invoke-virtual {v1, v2, v0}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method private d()Z
    .locals 4

    .prologue
    .line 565831
    invoke-static {p0}, LX/3Qn;->e(LX/3Qn;)Ljava/lang/String;

    move-result-object v0

    .line 565832
    if-eqz v0, :cond_1

    .line 565833
    iget-object v1, p0, LX/3Qn;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Qm;->d:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 565834
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 565835
    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/3Qn;->h()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/3Qn;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 565824
    iget-object v0, p0, LX/3Qn;->n:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 565825
    const/4 v0, 0x0

    .line 565826
    if-eqz v1, :cond_0

    .line 565827
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->m()Lcom/facebook/user/model/UserFbidIdentifier;

    move-result-object v1

    .line 565828
    if-eqz v1, :cond_0

    .line 565829
    invoke-virtual {v1}, Lcom/facebook/user/model/UserFbidIdentifier;->a()Ljava/lang/String;

    move-result-object v0

    .line 565830
    :cond_0
    return-object v0
.end method

.method private h()J
    .locals 4

    .prologue
    .line 565823
    iget-object v0, p0, LX/3Qn;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Qm;->c:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static i(LX/3Qn;)Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 565816
    iget-object v0, p0, LX/3Qn;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Bc;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 565817
    const v0, -0x2503beed

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 565818
    :try_start_0
    iget-object v0, p0, LX/3Qn;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Bi;

    sget-object v2, LX/7Bg;->a:LX/7Bh;

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, LX/2Iu;->a(LX/0To;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 565819
    const v2, -0x6e99adfa

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 565820
    if-eq v0, v4, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    .line 565821
    :catchall_0
    move-exception v0

    const v2, 0x4e77665f

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 565822
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 565808
    iget-object v0, p0, LX/3Qn;->o:LX/0Uh;

    sget v1, LX/2SU;->t:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 565809
    :goto_0
    return-void

    .line 565810
    :cond_0
    invoke-direct {p0}, LX/3Qn;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 565811
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, LX/3Qn;->c(LX/3Qn;J)V

    goto :goto_0

    .line 565812
    :cond_1
    invoke-direct {p0}, LX/3Qn;->h()J

    move-result-wide v0

    .line 565813
    invoke-direct {p0, v0, v1}, LX/3Qn;->a(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 565814
    invoke-static {p0, v0, v1}, LX/3Qn;->c(LX/3Qn;J)V

    goto :goto_0

    .line 565815
    :cond_2
    iget-object v0, p0, LX/3Qn;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/search/bootstrap/sync/BootstrapKeywordsLoader$1;

    invoke-direct {v1, p0}, Lcom/facebook/search/bootstrap/sync/BootstrapKeywordsLoader$1;-><init>(LX/3Qn;)V

    const v2, 0x20b934e

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
