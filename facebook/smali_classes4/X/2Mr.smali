.class public LX/2Mr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Mr;


# instance fields
.field private volatile a:Z

.field public volatile b:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397720
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Mr;->a:Z

    .line 397721
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2Mr;->b:Z

    .line 397722
    return-void
.end method

.method public static a(LX/0QB;)LX/2Mr;
    .locals 3

    .prologue
    .line 397723
    sget-object v0, LX/2Mr;->c:LX/2Mr;

    if-nez v0, :cond_1

    .line 397724
    const-class v1, LX/2Mr;

    monitor-enter v1

    .line 397725
    :try_start_0
    sget-object v0, LX/2Mr;->c:LX/2Mr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 397726
    if-eqz v2, :cond_0

    .line 397727
    :try_start_1
    new-instance v0, LX/2Mr;

    invoke-direct {v0}, LX/2Mr;-><init>()V

    .line 397728
    move-object v0, v0

    .line 397729
    sput-object v0, LX/2Mr;->c:LX/2Mr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397730
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 397731
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397732
    :cond_1
    sget-object v0, LX/2Mr;->c:LX/2Mr;

    return-object v0

    .line 397733
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 397734
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
