.class public final LX/2MQ;
.super LX/0R0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0R0",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public a:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public b:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/2MP;


# direct methods
.method public constructor <init>(LX/2MP;)V
    .locals 0

    .prologue
    .line 396669
    iput-object p1, p0, LX/2MQ;->c:LX/2MP;

    invoke-direct {p0}, LX/0R0;-><init>()V

    .line 396670
    iput-object p0, p0, LX/2MQ;->a:LX/0R1;

    .line 396671
    iput-object p0, p0, LX/2MQ;->b:LX/0R1;

    return-void
.end method


# virtual methods
.method public final getNextInWriteQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 396679
    iget-object v0, p0, LX/2MQ;->a:LX/0R1;

    return-object v0
.end method

.method public final getPreviousInWriteQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 396678
    iget-object v0, p0, LX/2MQ;->b:LX/0R1;

    return-object v0
.end method

.method public final getWriteTime()J
    .locals 2

    .prologue
    .line 396677
    const-wide v0, 0x7fffffffffffffffL

    return-wide v0
.end method

.method public final setNextInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 396675
    iput-object p1, p0, LX/2MQ;->a:LX/0R1;

    .line 396676
    return-void
.end method

.method public final setPreviousInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 396673
    iput-object p1, p0, LX/2MQ;->b:LX/0R1;

    .line 396674
    return-void
.end method

.method public final setWriteTime(J)V
    .locals 0

    .prologue
    .line 396672
    return-void
.end method
