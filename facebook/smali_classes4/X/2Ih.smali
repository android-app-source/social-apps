.class public LX/2Ih;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Ih;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/composer/shareintent/IsUsingAlphabeticalShareAlias;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392080
    iput-object p1, p0, LX/2Ih;->a:Landroid/content/Context;

    .line 392081
    iput-object p2, p0, LX/2Ih;->b:LX/0Or;

    .line 392082
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ih;
    .locals 5

    .prologue
    .line 392083
    sget-object v0, LX/2Ih;->c:LX/2Ih;

    if-nez v0, :cond_1

    .line 392084
    const-class v1, LX/2Ih;

    monitor-enter v1

    .line 392085
    :try_start_0
    sget-object v0, LX/2Ih;->c:LX/2Ih;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 392086
    if-eqz v2, :cond_0

    .line 392087
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 392088
    new-instance v4, LX/2Ih;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 p0, 0x1470

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/2Ih;-><init>(Landroid/content/Context;LX/0Or;)V

    .line 392089
    move-object v0, v4

    .line 392090
    sput-object v0, LX/2Ih;->c:LX/2Ih;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392091
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 392092
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392093
    :cond_1
    sget-object v0, LX/2Ih;->c:LX/2Ih;

    return-object v0

    .line 392094
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 392095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2Ih;)V
    .locals 7

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 392096
    iget-object v0, p0, LX/2Ih;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 392097
    new-instance v4, Landroid/content/ComponentName;

    iget-object v0, p0, LX/2Ih;->a:Landroid/content/Context;

    const-string v5, "com.facebook.composer.shareintent.ImplicitShareIntentHandlerDefaultAlias"

    invoke-direct {v4, v0, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 392098
    new-instance v5, Landroid/content/ComponentName;

    iget-object v0, p0, LX/2Ih;->a:Landroid/content/Context;

    const-string v6, "com.facebook.composer.shareintent.ImplicitShareIntentHandlerAlphabeticalAlias"

    invoke-direct {v5, v0, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 392099
    iget-object v0, p0, LX/2Ih;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v6, v4, v0, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 392100
    iget-object v0, p0, LX/2Ih;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-eqz v3, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v5, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 392101
    return-void

    :cond_1
    move v0, v2

    .line 392102
    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 0

    .prologue
    .line 392103
    invoke-static {p0}, LX/2Ih;->a$redex0(LX/2Ih;)V

    .line 392104
    return-void
.end method
