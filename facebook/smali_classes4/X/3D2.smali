.class public LX/3D2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Enum;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/3D2;


# instance fields
.field public final b:LX/33W;

.field private final c:LX/33Y;

.field private final d:LX/1rw;

.field private final e:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 531410
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    .line 531411
    sput-object v0, LX/3D2;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const v2, 0x7f0208cf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531412
    sget-object v0, LX/3D2;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSUB:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const v2, 0x7f020a1d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531413
    sget-object v0, LX/3D2;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SHOW_MORE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const v2, 0x7f020908

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531414
    sget-object v0, LX/3D2;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->OPEN_ACTION_SHEET:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const v2, 0x7f020908

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531415
    sget-object v0, LX/3D2;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const v2, 0x7f020781

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531416
    sget-object v0, LX/3D2;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const v2, 0x7f020818

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531417
    return-void
.end method

.method public constructor <init>(LX/33W;LX/33Y;LX/1rw;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 531404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 531405
    iput-object p1, p0, LX/3D2;->b:LX/33W;

    .line 531406
    iput-object p2, p0, LX/3D2;->c:LX/33Y;

    .line 531407
    iput-object p3, p0, LX/3D2;->d:LX/1rw;

    .line 531408
    iput-object p4, p0, LX/3D2;->e:LX/0ad;

    .line 531409
    return-void
.end method

.method public static a(LX/0QB;)LX/3D2;
    .locals 7

    .prologue
    .line 531391
    sget-object v0, LX/3D2;->f:LX/3D2;

    if-nez v0, :cond_1

    .line 531392
    const-class v1, LX/3D2;

    monitor-enter v1

    .line 531393
    :try_start_0
    sget-object v0, LX/3D2;->f:LX/3D2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 531394
    if-eqz v2, :cond_0

    .line 531395
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 531396
    new-instance p0, LX/3D2;

    invoke-static {v0}, LX/33W;->a(LX/0QB;)LX/33W;

    move-result-object v3

    check-cast v3, LX/33W;

    invoke-static {v0}, LX/33Y;->b(LX/0QB;)LX/33Y;

    move-result-object v4

    check-cast v4, LX/33Y;

    invoke-static {v0}, LX/1rw;->b(LX/0QB;)LX/1rw;

    move-result-object v5

    check-cast v5, LX/1rw;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3D2;-><init>(LX/33W;LX/33Y;LX/1rw;LX/0ad;)V

    .line 531397
    move-object v0, p0

    .line 531398
    sput-object v0, LX/3D2;->f:LX/3D2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 531399
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 531400
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 531401
    :cond_1
    sget-object v0, LX/3D2;->f:LX/3D2;

    return-object v0

    .line 531402
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 531403
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/3D2;Landroid/content/Context;Landroid/view/View;LX/2nq;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;LX/3Ai;LX/3D3;Ljava/lang/String;I)V
    .locals 9
    .param p0    # LX/3D2;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 531386
    invoke-virtual {p4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, LX/3D2;->a:Ljava/util/Map;

    invoke-virtual {p4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531387
    sget-object v0, LX/3D2;->a:Ljava/util/Map;

    invoke-virtual {p4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 531388
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p5, v0}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 531389
    :cond_0
    new-instance v0, LX/Dq5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, LX/Dq5;-><init>(LX/3D2;Landroid/content/Context;Landroid/view/View;LX/2nq;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;LX/3D3;Ljava/lang/String;I)V

    invoke-virtual {p5, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 531390
    return-void
.end method

.method private static a(LX/3D2;Landroid/view/View;LX/2nq;Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;LX/5OG;LX/3D3;Ljava/lang/String;I)V
    .locals 9

    .prologue
    .line 531418
    invoke-static {p0, p3, p4}, LX/3D2;->a(LX/3D2;Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 531419
    :goto_0
    return-void

    .line 531420
    :cond_0
    invoke-virtual {p4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->b()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v5

    .line 531421
    invoke-virtual {p5}, LX/5OG;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v4

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-static/range {v0 .. v8}, LX/3D2;->a(LX/3D2;Landroid/content/Context;Landroid/view/View;LX/2nq;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;LX/3Ai;LX/3D3;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private static a(LX/3D2;Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 531370
    invoke-virtual {p2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->b()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->b()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;->e()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->b()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 531371
    :cond_0
    :goto_0
    return v0

    .line 531372
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v1

    .line 531373
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->SAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSAVE_ITEM:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    if-ne v1, v2, :cond_3

    .line 531374
    :cond_2
    iget-object v1, p0, LX/3D2;->e:LX/0ad;

    sget-short v2, LX/34q;->c:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 531375
    :cond_3
    iget-object v0, p0, LX/3D2;->d:LX/1rw;

    invoke-virtual {p2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->b()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$GenericInlineActionNotifOptionRowDisplayFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    move-result-object v1

    const/4 v4, 0x0

    .line 531376
    invoke-static {v0}, LX/1rw;->d(LX/1rw;)LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p0

    move v5, v4

    :goto_1
    if-ge v5, p0, :cond_5

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3T2;

    .line 531377
    invoke-virtual {v2}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v3

    const-string p2, "option_set_display_style"

    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 531378
    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 531379
    invoke-virtual {v2}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v2

    const-string v3, "option_display_styles"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 531380
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 531381
    const/4 v2, 0x1

    .line 531382
    :goto_2
    move v0, v2

    .line 531383
    goto :goto_0

    .line 531384
    :cond_4
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    :cond_5
    move v2, v4

    .line 531385
    goto :goto_2
.end method

.method private a(LX/BAx;)Z
    .locals 6

    .prologue
    .line 531359
    invoke-interface {p1}, LX/BAx;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/BAx;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->b()LX/BCQ;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/BAx;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->b()LX/BCQ;

    move-result-object v0

    invoke-interface {v0}, LX/BCQ;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    move-result-object v0

    if-nez v0, :cond_1

    .line 531360
    :cond_0
    const/4 v0, 0x0

    .line 531361
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/3D2;->d:LX/1rw;

    invoke-interface {p1}, LX/BAx;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->b()LX/BCQ;

    move-result-object v1

    invoke-interface {v1}, LX/BCQ;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    move-result-object v1

    const/4 v3, 0x0

    .line 531362
    invoke-static {v0}, LX/1rw;->d(LX/1rw;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result p0

    move v4, v3

    :goto_1
    if-ge v4, p0, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3T2;

    .line 531363
    invoke-virtual {v2}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v2

    const-string p1, "option_set_display_style"

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 531364
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 531365
    const/4 v2, 0x1

    .line 531366
    :goto_2
    move v0, v2

    .line 531367
    goto :goto_0

    .line 531368
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_3
    move v2, v3

    .line 531369
    goto :goto_2
.end method

.method private static b(LX/3D2;LX/5OM;LX/2nq;Landroid/view/View;LX/3D3;Ljava/lang/String;I)V
    .locals 18

    .prologue
    .line 531339
    invoke-interface/range {p2 .. p2}, LX/2nq;->n()LX/BAy;

    move-result-object v2

    invoke-interface {v2}, LX/BAy;->a()LX/0Px;

    move-result-object v14

    invoke-virtual {v14}, LX/0Px;->size()I

    move-result v15

    const/4 v2, 0x0

    move v13, v2

    :goto_0
    if-ge v13, v15, :cond_1

    invoke-virtual {v14, v13}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, LX/BAx;

    .line 531340
    invoke-interface {v11}, LX/BAx;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;->a()LX/0Px;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, LX/0Px;->size()I

    move-result v17

    const/4 v2, 0x0

    move v12, v2

    :goto_1
    move/from16 v0, v17

    if-ge v12, v0, :cond_0

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    .line 531341
    invoke-interface {v11}, LX/BAx;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->b()LX/BCQ;

    move-result-object v2

    invoke-interface {v2}, LX/BCQ;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, LX/5OM;->c()LX/5OG;

    move-result-object v7

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p2

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move/from16 v10, p6

    invoke-static/range {v2 .. v10}, LX/3D2;->a(LX/3D2;Landroid/view/View;LX/2nq;Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;LX/5OG;LX/3D3;Ljava/lang/String;I)V

    .line 531342
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto :goto_1

    .line 531343
    :cond_0
    add-int/lit8 v2, v13, 0x1

    move v13, v2

    goto :goto_0

    .line 531344
    :cond_1
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 531345
    return-void
.end method


# virtual methods
.method public final a(LX/2nq;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 531352
    invoke-interface {p1}, LX/2nq;->n()LX/BAy;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/2nq;->n()LX/BAy;

    move-result-object v0

    invoke-interface {v0}, LX/BAy;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 531353
    :goto_0
    return v0

    .line 531354
    :cond_1
    invoke-interface {p1}, LX/2nq;->n()LX/BAy;

    move-result-object v0

    invoke-interface {v0}, LX/BAy;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BAx;

    .line 531355
    invoke-interface {v0}, LX/BAx;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, LX/BAx;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, LX/BAx;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-direct {p0, v0}, LX/3D2;->a(LX/BAx;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 531356
    goto :goto_0

    .line 531357
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 531358
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(LX/5OM;LX/2nq;Landroid/view/View;LX/3D3;Ljava/lang/String;I)Z
    .locals 6

    .prologue
    .line 531346
    invoke-virtual {p0, p2}, LX/3D2;->a(LX/2nq;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 531347
    const/4 v0, 0x0

    .line 531348
    :goto_0
    return v0

    .line 531349
    :cond_0
    invoke-static/range {p0 .. p6}, LX/3D2;->b(LX/3D2;LX/5OM;LX/2nq;Landroid/view/View;LX/3D3;Ljava/lang/String;I)V

    .line 531350
    iget-object v0, p0, LX/3D2;->c:LX/33Y;

    const-string v1, "inline_actions_launched"

    const/4 v2, 0x0

    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    move-object v3, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, LX/33Y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;I)V

    .line 531351
    const/4 v0, 0x1

    goto :goto_0
.end method
