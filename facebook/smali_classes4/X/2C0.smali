.class public final LX/2C0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/2Gb;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/2Gb;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 381919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381920
    iput-object p1, p0, LX/2C0;->a:LX/0QB;

    .line 381921
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 381918
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2C0;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 381910
    packed-switch p2, :pswitch_data_0

    .line 381911
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 381912
    :pswitch_0
    invoke-static {p1}, LX/2Ga;->a(LX/0QB;)LX/2Ga;

    move-result-object v0

    .line 381913
    :goto_0
    return-object v0

    .line 381914
    :pswitch_1
    invoke-static {p1}, LX/2H8;->a(LX/0QB;)LX/2H8;

    move-result-object v0

    goto :goto_0

    .line 381915
    :pswitch_2
    invoke-static {p1}, LX/2HK;->a(LX/0QB;)LX/2HK;

    move-result-object v0

    goto :goto_0

    .line 381916
    :pswitch_3
    invoke-static {p1}, LX/2HM;->a(LX/0QB;)LX/2HM;

    move-result-object v0

    goto :goto_0

    .line 381917
    :pswitch_4
    invoke-static {p1}, LX/2HN;->a(LX/0QB;)LX/2HN;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 381909
    const/4 v0, 0x5

    return v0
.end method
