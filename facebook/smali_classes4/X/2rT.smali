.class public final LX/2rT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3It;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;)V
    .locals 0

    .prologue
    .line 471948
    iput-object p1, p0, LX/2rT;->a:Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 471949
    return-void
.end method

.method public final a(LX/2op;)V
    .locals 7

    .prologue
    .line 471950
    iget-object v0, p0, LX/2rT;->a:Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->f:LX/Avq;

    if-eqz v0, :cond_0

    .line 471951
    iget-object v0, p0, LX/2rT;->a:Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->f:LX/Avq;

    .line 471952
    iget-object v1, v0, LX/Avq;->b:LX/0iJ;

    iget-object v1, v1, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    .line 471953
    iget v2, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move v1, v2

    .line 471954
    iget v2, v0, LX/Avq;->a:I

    if-eq v1, v2, :cond_1

    .line 471955
    :cond_0
    :goto_0
    return-void

    .line 471956
    :cond_1
    iget-object v2, v0, LX/Avq;->b:LX/0iJ;

    .line 471957
    iget-object v3, v2, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    const-wide/high16 v5, 0x4010000000000000L    # 4.0

    invoke-virtual {v3, v5, v6}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollFactor(D)V

    .line 471958
    const/4 v3, 0x1

    iput-boolean v3, v2, LX/0iJ;->y:Z

    .line 471959
    iget-object v2, v0, LX/Avq;->b:LX/0iJ;

    iget-object v2, v2, LX/0iJ;->o:Lcom/facebook/widget/verticalviewpager/VerticalViewPager;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public final a(LX/2oq;)V
    .locals 2

    .prologue
    .line 471960
    iget-object v0, p0, LX/2rT;->a:Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 471961
    iget-object v0, p0, LX/2rT;->a:Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 471962
    iget-object v0, p0, LX/2rT;->a:Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->b:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 471963
    :cond_0
    return-void
.end method

.method public final a(LX/2or;)V
    .locals 0

    .prologue
    .line 471964
    return-void
.end method
