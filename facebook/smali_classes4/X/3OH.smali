.class public final enum LX/3OH;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/3OI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3OH;",
        ">;",
        "LX/3OI;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3OH;

.field public static final enum ACTIVE_FRIENDS:LX/3OH;

.field public static final enum AGGREGATE_CALL_DETAILS:LX/3OH;

.field public static final enum ALPHABETIC_SECTION:LX/3OH;

.field public static final enum AUTO_COMPLETE:LX/3OH;

.field public static final enum CALL_LOGS:LX/3OH;

.field public static final enum CONTACTS_UPLOADED_DIALOG:LX/3OH;

.field public static final enum FAVORITES:LX/3OH;

.field public static final enum NEARBY:LX/3OH;

.field public static final enum NEARBY_FRIENDS:LX/3OH;

.field public static final enum NEW_CONTACTS:LX/3OH;

.field public static final enum NULL_STATE_BOTS:LX/3OH;

.field public static final enum NULL_STATE_BYMM:LX/3OH;

.field public static final enum NULL_STATE_RECENT_SEARCHES:LX/3OH;

.field public static final enum NULL_STATE_TOP_PEOPLE:LX/3OH;

.field public static final enum OMNIPICKER_SUGGESTIONS:LX/3OH;

.field public static final enum ONLINE:LX/3OH;

.field public static final enum ON_MESSENGER:LX/3OH;

.field public static final enum PROMOTION:LX/3OH;

.field public static final enum SEARCH_RESULT:LX/3OH;

.field public static final enum SELF_PROFILE:LX/3OH;

.field public static final enum SMS_BRIDGE_JOIN_GROUPS_NUX:LX/3OH;

.field public static final enum SUGGESTIONS:LX/3OH;

.field public static final enum TOP_FRIENDS:LX/3OH;

.field public static final enum UNKNOWN:LX/3OH;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 560356
    new-instance v0, LX/3OH;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->UNKNOWN:LX/3OH;

    .line 560357
    new-instance v0, LX/3OH;

    const-string v1, "SELF_PROFILE"

    invoke-direct {v0, v1, v4}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->SELF_PROFILE:LX/3OH;

    .line 560358
    new-instance v0, LX/3OH;

    const-string v1, "FAVORITES"

    invoke-direct {v0, v1, v5}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->FAVORITES:LX/3OH;

    .line 560359
    new-instance v0, LX/3OH;

    const-string v1, "TOP_FRIENDS"

    invoke-direct {v0, v1, v6}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->TOP_FRIENDS:LX/3OH;

    .line 560360
    new-instance v0, LX/3OH;

    const-string v1, "ACTIVE_FRIENDS"

    invoke-direct {v0, v1, v7}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->ACTIVE_FRIENDS:LX/3OH;

    .line 560361
    new-instance v0, LX/3OH;

    const-string v1, "SEARCH_RESULT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->SEARCH_RESULT:LX/3OH;

    .line 560362
    new-instance v0, LX/3OH;

    const-string v1, "AUTO_COMPLETE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->AUTO_COMPLETE:LX/3OH;

    .line 560363
    new-instance v0, LX/3OH;

    const-string v1, "SUGGESTIONS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->SUGGESTIONS:LX/3OH;

    .line 560364
    new-instance v0, LX/3OH;

    const-string v1, "NEARBY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->NEARBY:LX/3OH;

    .line 560365
    new-instance v0, LX/3OH;

    const-string v1, "NEARBY_FRIENDS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->NEARBY_FRIENDS:LX/3OH;

    .line 560366
    new-instance v0, LX/3OH;

    const-string v1, "ON_MESSENGER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->ON_MESSENGER:LX/3OH;

    .line 560367
    new-instance v0, LX/3OH;

    const-string v1, "ONLINE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->ONLINE:LX/3OH;

    .line 560368
    new-instance v0, LX/3OH;

    const-string v1, "ALPHABETIC_SECTION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->ALPHABETIC_SECTION:LX/3OH;

    .line 560369
    new-instance v0, LX/3OH;

    const-string v1, "CONTACTS_UPLOADED_DIALOG"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->CONTACTS_UPLOADED_DIALOG:LX/3OH;

    .line 560370
    new-instance v0, LX/3OH;

    const-string v1, "PROMOTION"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->PROMOTION:LX/3OH;

    .line 560371
    new-instance v0, LX/3OH;

    const-string v1, "CALL_LOGS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->CALL_LOGS:LX/3OH;

    .line 560372
    new-instance v0, LX/3OH;

    const-string v1, "AGGREGATE_CALL_DETAILS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->AGGREGATE_CALL_DETAILS:LX/3OH;

    .line 560373
    new-instance v0, LX/3OH;

    const-string v1, "OMNIPICKER_SUGGESTIONS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->OMNIPICKER_SUGGESTIONS:LX/3OH;

    .line 560374
    new-instance v0, LX/3OH;

    const-string v1, "NULL_STATE_TOP_PEOPLE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->NULL_STATE_TOP_PEOPLE:LX/3OH;

    .line 560375
    new-instance v0, LX/3OH;

    const-string v1, "NULL_STATE_BYMM"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->NULL_STATE_BYMM:LX/3OH;

    .line 560376
    new-instance v0, LX/3OH;

    const-string v1, "NULL_STATE_BOTS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->NULL_STATE_BOTS:LX/3OH;

    .line 560377
    new-instance v0, LX/3OH;

    const-string v1, "NULL_STATE_RECENT_SEARCHES"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->NULL_STATE_RECENT_SEARCHES:LX/3OH;

    .line 560378
    new-instance v0, LX/3OH;

    const-string v1, "SMS_BRIDGE_JOIN_GROUPS_NUX"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->SMS_BRIDGE_JOIN_GROUPS_NUX:LX/3OH;

    .line 560379
    new-instance v0, LX/3OH;

    const-string v1, "NEW_CONTACTS"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/3OH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3OH;->NEW_CONTACTS:LX/3OH;

    .line 560380
    const/16 v0, 0x18

    new-array v0, v0, [LX/3OH;

    sget-object v1, LX/3OH;->UNKNOWN:LX/3OH;

    aput-object v1, v0, v3

    sget-object v1, LX/3OH;->SELF_PROFILE:LX/3OH;

    aput-object v1, v0, v4

    sget-object v1, LX/3OH;->FAVORITES:LX/3OH;

    aput-object v1, v0, v5

    sget-object v1, LX/3OH;->TOP_FRIENDS:LX/3OH;

    aput-object v1, v0, v6

    sget-object v1, LX/3OH;->ACTIVE_FRIENDS:LX/3OH;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3OH;->SEARCH_RESULT:LX/3OH;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3OH;->AUTO_COMPLETE:LX/3OH;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3OH;->SUGGESTIONS:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3OH;->NEARBY:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/3OH;->NEARBY_FRIENDS:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/3OH;->ON_MESSENGER:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/3OH;->ONLINE:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/3OH;->ALPHABETIC_SECTION:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/3OH;->CONTACTS_UPLOADED_DIALOG:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/3OH;->PROMOTION:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/3OH;->CALL_LOGS:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/3OH;->AGGREGATE_CALL_DETAILS:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/3OH;->OMNIPICKER_SUGGESTIONS:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/3OH;->NULL_STATE_TOP_PEOPLE:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/3OH;->NULL_STATE_BYMM:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/3OH;->NULL_STATE_BOTS:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/3OH;->NULL_STATE_RECENT_SEARCHES:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/3OH;->SMS_BRIDGE_JOIN_GROUPS_NUX:LX/3OH;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/3OH;->NEW_CONTACTS:LX/3OH;

    aput-object v2, v0, v1

    sput-object v0, LX/3OH;->$VALUES:[LX/3OH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560381
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3OH;
    .locals 1

    .prologue
    .line 560382
    const-class v0, LX/3OH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3OH;

    return-object v0
.end method

.method public static values()[LX/3OH;
    .locals 1

    .prologue
    .line 560383
    sget-object v0, LX/3OH;->$VALUES:[LX/3OH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3OH;

    return-object v0
.end method
