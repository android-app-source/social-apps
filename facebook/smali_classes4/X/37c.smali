.class public final enum LX/37c;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/37c;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/37c;

.field public static final enum CONNECTED:LX/37c;

.field public static final enum CONNECTING:LX/37c;

.field public static final enum DISCONNECTED:LX/37c;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 501702
    new-instance v0, LX/37c;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v2}, LX/37c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/37c;->DISCONNECTED:LX/37c;

    .line 501703
    new-instance v0, LX/37c;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, LX/37c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/37c;->CONNECTING:LX/37c;

    .line 501704
    new-instance v0, LX/37c;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v4}, LX/37c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/37c;->CONNECTED:LX/37c;

    .line 501705
    const/4 v0, 0x3

    new-array v0, v0, [LX/37c;

    sget-object v1, LX/37c;->DISCONNECTED:LX/37c;

    aput-object v1, v0, v2

    sget-object v1, LX/37c;->CONNECTING:LX/37c;

    aput-object v1, v0, v3

    sget-object v1, LX/37c;->CONNECTED:LX/37c;

    aput-object v1, v0, v4

    sput-object v0, LX/37c;->$VALUES:[LX/37c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 501706
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/37c;
    .locals 1

    .prologue
    .line 501707
    const-class v0, LX/37c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/37c;

    return-object v0
.end method

.method public static values()[LX/37c;
    .locals 1

    .prologue
    .line 501708
    sget-object v0, LX/37c;->$VALUES:[LX/37c;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/37c;

    return-object v0
.end method
