.class public LX/24M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aT;",
        ">",
        "Ljava/lang/Object;",
        "LX/24J",
        "<",
        "LX/24P;",
        "TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1Rg;

.field public final b:LX/24L;


# direct methods
.method public constructor <init>(LX/1Rg;LX/24L;)V
    .locals 0

    .prologue
    .line 367099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367100
    iput-object p1, p0, LX/24M;->a:LX/1Rg;

    .line 367101
    iput-object p2, p0, LX/24M;->b:LX/24L;

    .line 367102
    return-void
.end method

.method public static a(LX/24M;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Z)LX/8sj;
    .locals 10

    .prologue
    const v5, 0x3f4ccccd    # 0.8f

    const/4 v8, 0x0

    const-wide/16 v2, 0x96

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 367103
    iget-object v0, p0, LX/24M;->a:LX/1Rg;

    if-eqz p2, :cond_0

    move v4, v7

    :goto_0
    if-eqz p2, :cond_1

    :goto_1
    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Ljava/lang/Object;JFFLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v9

    .line 367104
    iget-object v0, p0, LX/24M;->a:LX/1Rg;

    new-instance v1, LX/Ajz;

    invoke-direct {v1, p1}, LX/Ajz;-><init>(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    if-eqz p2, :cond_2

    move v4, v7

    :goto_2
    if-eqz p2, :cond_3

    move v5, v8

    :goto_3
    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Ljava/lang/Object;JFFLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    .line 367105
    iget-object v1, p0, LX/24M;->a:LX/1Rg;

    invoke-static {v9, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0, v2, v3, v6}, LX/1Rg;->a(LX/0Px;JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    return-object v0

    :cond_0
    move v4, v5

    .line 367106
    goto :goto_0

    :cond_1
    move v5, v7

    goto :goto_1

    :cond_2
    move v4, v8

    .line 367107
    goto :goto_2

    :cond_3
    move v5, v7

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)Ljava/lang/Runnable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 367108
    new-instance v0, Lcom/facebook/feed/inlinecomposer/multirow/animations/PromptIconAnimationBuilder$1;

    invoke-direct {v0, p0, p3}, Lcom/facebook/feed/inlinecomposer/multirow/animations/PromptIconAnimationBuilder$1;-><init>(LX/24M;Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)V
    .locals 5

    .prologue
    .line 367109
    check-cast p2, LX/24P;

    check-cast p3, LX/24P;

    .line 367110
    if-ne p2, p3, :cond_1

    .line 367111
    :cond_0
    :goto_0
    return-void

    .line 367112
    :cond_1
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    if-ne p2, v0, :cond_2

    move-object v0, p4

    .line 367113
    check-cast v0, LX/1aT;

    invoke-interface {v0}, LX/1aT;->getIconView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    .line 367114
    iget-object v1, p0, LX/24M;->a:LX/1Rg;

    const-wide/16 v3, 0xc8

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v4, v2}, LX/1Rg;->a(JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367115
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/24M;->a(LX/24M;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Z)LX/8sj;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367116
    :cond_2
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    if-ne p3, v0, :cond_0

    .line 367117
    check-cast p4, LX/1aT;

    invoke-interface {p4}, LX/1aT;->getIconView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    .line 367118
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/24M;->a(LX/24M;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Z)LX/8sj;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367119
    goto :goto_0
.end method
