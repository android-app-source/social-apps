.class public LX/2uq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:J

.field public static r:LX/03R;

.field private static volatile u:LX/2uq;


# instance fields
.field private b:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/30m;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/2Oq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Or;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMo;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3RA;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Uq;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private q:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 477025
    const-wide/16 v0, 0x7

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    mul-long/2addr v0, v2

    sput-wide v0, LX/2uq;->a:J

    .line 477026
    sget-object v0, LX/03R;->UNSET:LX/03R;

    sput-object v0, LX/2uq;->r:LX/03R;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 477027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 477028
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 477029
    iput-object v0, p0, LX/2uq;->c:LX/0Ot;

    .line 477030
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 477031
    iput-object v0, p0, LX/2uq;->d:LX/0Ot;

    .line 477032
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 477033
    iput-object v0, p0, LX/2uq;->g:LX/0Ot;

    .line 477034
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 477035
    iput-object v0, p0, LX/2uq;->h:LX/0Ot;

    .line 477036
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 477037
    iput-object v0, p0, LX/2uq;->i:LX/0Ot;

    .line 477038
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 477039
    iput-object v0, p0, LX/2uq;->j:LX/0Ot;

    .line 477040
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 477041
    iput-object v0, p0, LX/2uq;->k:LX/0Ot;

    .line 477042
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 477043
    iput-object v0, p0, LX/2uq;->l:LX/0Ot;

    .line 477044
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 477045
    iput-object v0, p0, LX/2uq;->m:LX/0Ot;

    .line 477046
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 477047
    iput-object v0, p0, LX/2uq;->n:LX/0Ot;

    .line 477048
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/2uq;->s:Ljava/util/List;

    .line 477049
    new-instance v0, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$1;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/sms/defaultapp/SmsDefaultAppManager$1;-><init>(LX/2uq;)V

    iput-object v0, p0, LX/2uq;->t:Ljava/lang/Runnable;

    .line 477050
    return-void
.end method

.method public static a(LX/0QB;)LX/2uq;
    .locals 3

    .prologue
    .line 477015
    sget-object v0, LX/2uq;->u:LX/2uq;

    if-nez v0, :cond_1

    .line 477016
    const-class v1, LX/2uq;

    monitor-enter v1

    .line 477017
    :try_start_0
    sget-object v0, LX/2uq;->u:LX/2uq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 477018
    if-eqz v2, :cond_0

    .line 477019
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2uq;->b(LX/0QB;)LX/2uq;

    move-result-object v0

    sput-object v0, LX/2uq;->u:LX/2uq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477020
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 477021
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 477022
    :cond_1
    sget-object v0, LX/2uq;->u:LX/2uq;

    return-object v0

    .line 477023
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 477024
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2uq;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/2Oq;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Xl;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2uq;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/30m;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/2Oq;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/2Or;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3RA;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Uq;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 477014
    iput-object p1, p0, LX/2uq;->b:Landroid/content/Context;

    iput-object p2, p0, LX/2uq;->c:LX/0Ot;

    iput-object p3, p0, LX/2uq;->d:LX/0Ot;

    iput-object p4, p0, LX/2uq;->e:LX/2Oq;

    iput-object p5, p0, LX/2uq;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p6, p0, LX/2uq;->g:LX/0Ot;

    iput-object p7, p0, LX/2uq;->h:LX/0Ot;

    iput-object p8, p0, LX/2uq;->i:LX/0Ot;

    iput-object p9, p0, LX/2uq;->j:LX/0Ot;

    iput-object p10, p0, LX/2uq;->k:LX/0Ot;

    iput-object p11, p0, LX/2uq;->l:LX/0Ot;

    iput-object p12, p0, LX/2uq;->m:LX/0Ot;

    iput-object p13, p0, LX/2uq;->n:LX/0Ot;

    iput-object p14, p0, LX/2uq;->o:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/2uq;->p:LX/0Xl;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/2uq;->q:LX/0Or;

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 477051
    :try_start_0
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/facebook/messaging/sms/defaultapp/ComposeSmsActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 477052
    new-instance v3, Landroid/content/ComponentName;

    const-class v4, LX/FMf;

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 477053
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 477054
    invoke-virtual {v4, v2}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v2

    .line 477055
    invoke-virtual {v4, v3}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 477056
    if-ne v2, v0, :cond_0

    if-ne v3, v0, :cond_0

    .line 477057
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 477058
    goto :goto_0

    .line 477059
    :catch_0
    move v0, v1

    goto :goto_0
.end method

.method public static a$redex0(LX/2uq;Landroid/content/Intent;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 477010
    :try_start_0
    iget-object v0, p0, LX/2uq;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p1, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 477011
    :goto_0
    return-void

    .line 477012
    :catch_0
    move-exception v0

    .line 477013
    const-string v1, "SmsDefaultAppManager"

    const-string v2, "Unable to start system setting to turn off SMS integration"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/2uq;
    .locals 19

    .prologue
    .line 477007
    new-instance v2, LX/2uq;

    invoke-direct {v2}, LX/2uq;-><init>()V

    .line 477008
    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0xda0

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v6

    check-cast v6, LX/2Oq;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v8, 0xd9d

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x271

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x23d

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2e3

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x298f

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xcf4

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x29a0

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xd9f

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v16

    check-cast v16, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v17

    check-cast v17, LX/0Xl;

    const/16 v18, 0x15e7

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    invoke-static/range {v2 .. v18}, LX/2uq;->a(LX/2uq;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/2Oq;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Xl;LX/0Or;)V

    .line 477009
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 476986
    iget-object v0, p0, LX/2uq;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Or;

    .line 476987
    iget-object v1, v0, LX/2Or;->a:LX/0Uh;

    const/16 v2, 0x62e

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 476988
    sput-boolean v0, LX/EdD;->a:Z

    .line 476989
    return-void
.end method

.method public final a(Ljava/lang/Object;Landroid/content/Context;Z)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 476990
    :try_start_0
    new-instance v3, Landroid/content/ComponentName;

    const-class v0, Lcom/facebook/messaging/sms/defaultapp/ComposeSmsActivity;

    invoke-direct {v3, p2, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 476991
    new-instance v4, Landroid/content/ComponentName;

    const-class v0, LX/FMf;

    invoke-direct {v4, p2, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 476992
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 476993
    iget-object v0, p0, LX/2uq;->e:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->c()LX/3Kt;

    move-result-object v6

    .line 476994
    invoke-static {p2}, LX/2uq;->a(Landroid/content/Context;)Z

    move-result v0

    if-eq v0, p3, :cond_0

    .line 476995
    if-eqz p3, :cond_3

    move v0, v1

    :goto_0
    const/4 v7, 0x1

    invoke-virtual {v5, v3, v0, v7}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 476996
    iget-object v0, p0, LX/2uq;->e:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->f()V

    .line 476997
    :cond_0
    invoke-virtual {v5, v4}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 476998
    const/4 v0, 0x1

    const/4 v3, 0x1

    invoke-virtual {v5, v4, v0, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 476999
    :cond_1
    iget-object v0, p0, LX/2uq;->e:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->c()LX/3Kt;

    move-result-object v3

    .line 477000
    if-eq v6, v3, :cond_2

    .line 477001
    iget-object v0, p0, LX/2uq;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30m;

    invoke-virtual {v0, p1, v6, v3}, LX/30m;->a(Ljava/lang/Object;LX/3Kt;LX/3Kt;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    move v0, v1

    .line 477002
    :goto_1
    return v0

    .line 477003
    :cond_3
    const/4 v0, 0x2

    goto :goto_0

    .line 477004
    :catch_0
    move-exception v0

    .line 477005
    const-string v1, "SmsDefaultAppManager"

    const-string v3, "Failed to enable SMS components"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 477006
    goto :goto_1
.end method
