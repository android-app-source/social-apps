.class public LX/38p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/38a;


# direct methods
.method public constructor <init>(LX/38a;)V
    .locals 0

    .prologue
    .line 521418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521419
    iput-object p1, p0, LX/38p;->a:LX/38a;

    .line 521420
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 521421
    iget-object v0, p0, LX/38p;->a:LX/38a;

    sget-object v1, LX/38a;->CONNECTED:LX/38a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 521417
    iget-object v0, p0, LX/38p;->a:LX/38a;

    sget-object v1, LX/38a;->DISCONNECTED:LX/38a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 521416
    iget-object v0, p0, LX/38p;->a:LX/38a;

    sget-object v1, LX/38a;->SUSPENDED:LX/38a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 521415
    iget-object v0, p0, LX/38p;->a:LX/38a;

    invoke-virtual {v0}, LX/38a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
