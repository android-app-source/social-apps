.class public LX/2G4;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/2G5;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/2G5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 387783
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2G5;
    .locals 5

    .prologue
    .line 387784
    sget-object v0, LX/2G4;->a:LX/2G5;

    if-nez v0, :cond_1

    .line 387785
    const-class v1, LX/2G4;

    monitor-enter v1

    .line 387786
    :try_start_0
    sget-object v0, LX/2G4;->a:LX/2G5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387787
    if-eqz v2, :cond_0

    .line 387788
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 387789
    invoke-static {v0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v3

    check-cast v3, LX/0VT;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Wt;->a(LX/0QB;)LX/0Wt;

    move-result-object p0

    check-cast p0, LX/0Wt;

    invoke-static {v3, v4, p0}, LX/0Wu;->a(LX/0VT;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Wt;)LX/2G5;

    move-result-object v3

    move-object v0, v3

    .line 387790
    sput-object v0, LX/2G4;->a:LX/2G5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387791
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387792
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387793
    :cond_1
    sget-object v0, LX/2G4;->a:LX/2G5;

    return-object v0

    .line 387794
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387795
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 387796
    invoke-static {p0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v0

    check-cast v0, LX/0VT;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Wt;->a(LX/0QB;)LX/0Wt;

    move-result-object v2

    check-cast v2, LX/0Wt;

    invoke-static {v0, v1, v2}, LX/0Wu;->a(LX/0VT;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Wt;)LX/2G5;

    move-result-object v0

    return-object v0
.end method
