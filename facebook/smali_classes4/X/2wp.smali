.class public LX/2wp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2wq;


# instance fields
.field private final a:LX/2wm;


# direct methods
.method public constructor <init>(LX/2wm;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/2wp;->a:LX/2wm;

    return-void
.end method


# virtual methods
.method public final a(LX/2we;)LX/2we;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LX/2wK;",
            "R::",
            "LX/2NW;",
            "T:",
            "LX/2we",
            "<TR;TA;>;>(TT;)TT;"
        }
    .end annotation

    iget-object v0, p0, LX/2wp;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->g:LX/2wW;

    iget-object v0, v0, LX/2wW;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-object p1
.end method

.method public final a()V
    .locals 3

    iget-object v0, p0, LX/2wp;->a:LX/2wm;

    iget-object v1, v0, LX/2wm;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2wJ;

    invoke-interface {v1}, LX/2wJ;->f()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, LX/2wp;->a:LX/2wm;

    iget-object v0, v0, LX/2wm;->g:LX/2wW;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    iput-object v1, v0, LX/2wW;->d:Ljava/util/Set;

    return-void
.end method

.method public final a(I)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;LX/2vs;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "LX/2vs",
            "<*>;I)V"
        }
    .end annotation

    return-void
.end method

.method public final b(LX/2we;)LX/2we;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LX/2wK;",
            "T:",
            "LX/2we",
            "<+",
            "LX/2NW;",
            "TA;>;>(TT;)TT;"
        }
    .end annotation

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GoogleApiClient is not connected yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, LX/2wp;->a:LX/2wm;

    invoke-virtual {v0}, LX/2wm;->h()V

    return-void
.end method
