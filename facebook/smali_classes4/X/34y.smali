.class public LX/34y;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DDl;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/34y;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DDo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 496310
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/34y;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DDo;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 496307
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 496308
    iput-object p1, p0, LX/34y;->b:LX/0Ot;

    .line 496309
    return-void
.end method

.method public static a(LX/0QB;)LX/34y;
    .locals 4

    .prologue
    .line 496311
    sget-object v0, LX/34y;->c:LX/34y;

    if-nez v0, :cond_1

    .line 496312
    const-class v1, LX/34y;

    monitor-enter v1

    .line 496313
    :try_start_0
    sget-object v0, LX/34y;->c:LX/34y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 496314
    if-eqz v2, :cond_0

    .line 496315
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 496316
    new-instance v3, LX/34y;

    const/16 p0, 0x1f93

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/34y;-><init>(LX/0Ot;)V

    .line 496317
    move-object v0, v3

    .line 496318
    sput-object v0, LX/34y;->c:LX/34y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496319
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 496320
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 496321
    :cond_1
    sget-object v0, LX/34y;->c:LX/34y;

    return-object v0

    .line 496322
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 496323
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 496306
    const v0, 0x175c8433

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 496301
    iget-object v0, p0, LX/34y;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DDo;

    const/16 p2, 0x20

    const/4 p0, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 496302
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    .line 496303
    const v2, 0x175c8433

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 496304
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b0062

    invoke-interface {v2, v3, v4}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    iget-object v4, v0, LX/DDo;->a:LX/1vg;

    invoke-virtual {v4, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v4

    const v5, 0x7f0a00d5

    invoke-virtual {v4, v5}, LX/2xv;->j(I)LX/2xv;

    move-result-object v4

    const v5, 0x7f02089a

    invoke-virtual {v4, v5}, LX/2xv;->h(I)LX/2xv;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f021758

    invoke-interface {v3, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->o(I)LX/1Di;

    move-result-object v3

    const/16 v4, 0x8

    invoke-interface {v3, v4, p0}, LX/1Di;->e(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v7, p0}, LX/1Di;->k(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->g(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a008a

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f08251e

    invoke-virtual {v3, v4}, LX/1ne;->h(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0062

    invoke-interface {v3, v6, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 496305
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 496290
    invoke-static {}, LX/1dS;->b()V

    .line 496291
    iget v0, p1, LX/1dQ;->b:I

    .line 496292
    packed-switch v0, :pswitch_data_0

    .line 496293
    :goto_0
    return-object v2

    .line 496294
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 496295
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 496296
    check-cast v1, LX/DDm;

    .line 496297
    iget-object p1, p0, LX/34y;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/DDm;->a:Landroid/view/View$OnClickListener;

    .line 496298
    if-eqz p1, :cond_0

    .line 496299
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 496300
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x175c8433
        :pswitch_0
    .end packed-switch
.end method
