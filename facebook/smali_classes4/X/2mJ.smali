.class public LX/2mJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/03V;

.field private final d:LX/17Y;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/17Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 459795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459796
    iput-object p1, p0, LX/2mJ;->a:Landroid/content/Context;

    .line 459797
    iput-object p2, p0, LX/2mJ;->b:Lcom/facebook/content/SecureContextHelper;

    .line 459798
    iput-object p3, p0, LX/2mJ;->c:LX/03V;

    .line 459799
    iput-object p4, p0, LX/2mJ;->d:LX/17Y;

    .line 459800
    return-void
.end method

.method public static a(LX/0QB;)LX/2mJ;
    .locals 7

    .prologue
    .line 459801
    const-class v1, LX/2mJ;

    monitor-enter v1

    .line 459802
    :try_start_0
    sget-object v0, LX/2mJ;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 459803
    sput-object v2, LX/2mJ;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 459804
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459805
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 459806
    new-instance p0, LX/2mJ;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v6

    check-cast v6, LX/17Y;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2mJ;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/17Y;)V

    .line 459807
    move-object v0, p0

    .line 459808
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 459809
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2mJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459810
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 459811
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 459812
    iget-object v0, p0, LX/2mJ;->d:LX/17Y;

    iget-object v1, p0, LX/2mJ;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 459813
    if-nez v0, :cond_0

    .line 459814
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 459815
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 459816
    :cond_0
    :try_start_0
    invoke-static {p1}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const-string v1, "force_external_browser"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 459817
    :cond_2
    iget-object v1, p0, LX/2mJ;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/2mJ;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 459818
    :goto_0
    return-void

    .line 459819
    :cond_3
    invoke-static {p1}, LX/32x;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 459820
    iget-object v1, p0, LX/2mJ;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/2mJ;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 459821
    :catch_0
    move-exception v0

    .line 459822
    :goto_1
    iget-object v1, p0, LX/2mJ;->c:LX/03V;

    const-string v2, "QuickPromotion_action"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 459823
    :cond_4
    :try_start_1
    iget-object v1, p0, LX/2mJ;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/2mJ;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 459824
    :catch_1
    move-exception v0

    goto :goto_1
.end method
