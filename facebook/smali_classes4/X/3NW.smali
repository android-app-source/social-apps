.class public final LX/3NW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/3Mi;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Z

.field public e:LX/3Og;

.field public f:LX/3Mr;

.field public g:Z

.field public h:LX/3Og;

.field public i:J


# direct methods
.method public constructor <init>(LX/3NU;)V
    .locals 2

    .prologue
    .line 559395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 559396
    sget-object v0, LX/3Mr;->FINISHED:LX/3Mr;

    iput-object v0, p0, LX/3NW;->f:LX/3Mr;

    .line 559397
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3NW;->g:Z

    .line 559398
    const/4 v0, 0x0

    iput-object v0, p0, LX/3NW;->h:LX/3Og;

    .line 559399
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/3NW;->i:J

    .line 559400
    iget-object v0, p1, LX/3NU;->a:LX/3Mi;

    move-object v0, v0

    .line 559401
    iput-object v0, p0, LX/3NW;->b:LX/3Mi;

    .line 559402
    iget-object v0, p1, LX/3NU;->b:Ljava/lang/String;

    move-object v0, v0

    .line 559403
    iput-object v0, p0, LX/3NW;->c:Ljava/lang/String;

    .line 559404
    iget-boolean v0, p1, LX/3NU;->c:Z

    move v0, v0

    .line 559405
    iput-boolean v0, p0, LX/3NW;->d:Z

    .line 559406
    iget-object v0, p0, LX/3NW;->b:LX/3Mi;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3NW;->a:Ljava/lang/String;

    .line 559407
    return-void
.end method


# virtual methods
.method public final c()Z
    .locals 1

    .prologue
    .line 559394
    iget-boolean v0, p0, LX/3NW;->d:Z

    return v0
.end method

.method public final d()LX/3Og;
    .locals 1

    .prologue
    .line 559392
    iget-boolean v0, p0, LX/3NW;->g:Z

    move v0, v0

    .line 559393
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3NW;->h:LX/3Og;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3NW;->e:LX/3Og;

    goto :goto_0
.end method

.method public final e()LX/3Mr;
    .locals 1

    .prologue
    .line 559391
    iget-object v0, p0, LX/3NW;->f:LX/3Mr;

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 559390
    iget-wide v0, p0, LX/3NW;->i:J

    return-wide v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 559387
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3NW;->g:Z

    .line 559388
    iget-object v0, p0, LX/3NW;->e:LX/3Og;

    iput-object v0, p0, LX/3NW;->h:LX/3Og;

    .line 559389
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 559386
    iget-boolean v0, p0, LX/3NW;->g:Z

    return v0
.end method
