.class public LX/3MX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3MX;


# instance fields
.field private final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/3MY;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 555054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555055
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, LX/3MY;->SMALL:LX/3MY;

    sget-object v2, LX/3MY;->SMALL:LX/3MY;

    iget v2, v2, LX/3MY;->dp:I

    int-to-float v2, v2

    invoke-static {p1, v2}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/3MY;->BIG:LX/3MY;

    sget-object v2, LX/3MY;->BIG:LX/3MY;

    iget v2, v2, LX/3MY;->dp:I

    int-to-float v2, v2

    invoke-static {p1, v2}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/3MY;->HUGE:LX/3MY;

    sget-object v2, LX/3MY;->HUGE:LX/3MY;

    iget v2, v2, LX/3MY;->dp:I

    int-to-float v2, v2

    invoke-static {p1, v2}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/3MX;->a:LX/0P1;

    .line 555056
    return-void
.end method

.method public static a(LX/0QB;)LX/3MX;
    .locals 4

    .prologue
    .line 555057
    sget-object v0, LX/3MX;->b:LX/3MX;

    if-nez v0, :cond_1

    .line 555058
    const-class v1, LX/3MX;

    monitor-enter v1

    .line 555059
    :try_start_0
    sget-object v0, LX/3MX;->b:LX/3MX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 555060
    if-eqz v2, :cond_0

    .line 555061
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 555062
    new-instance p0, LX/3MX;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/3MX;-><init>(Landroid/content/res/Resources;)V

    .line 555063
    move-object v0, p0

    .line 555064
    sput-object v0, LX/3MX;->b:LX/3MX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 555065
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 555066
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 555067
    :cond_1
    sget-object v0, LX/3MX;->b:LX/3MX;

    return-object v0

    .line 555068
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 555069
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/3MY;)I
    .locals 1

    .prologue
    .line 555070
    iget-object v0, p0, LX/3MX;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final b(LX/3MY;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 555071
    invoke-virtual {p0, p1}, LX/3MX;->a(LX/3MY;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
