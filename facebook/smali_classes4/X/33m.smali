.class public LX/33m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/zero/server/SendZeroHeaderRequestParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 494152
    const-class v0, LX/33m;

    sput-object v0, LX/33m;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494154
    return-void
.end method

.method private static b(Lcom/facebook/zero/server/SendZeroHeaderRequestParams;)Ljava/util/List;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/server/SendZeroHeaderRequestParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 494155
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 494156
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "redirect_app"

    .line 494157
    iget-object v3, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 494158
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494159
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "n"

    .line 494160
    iget-object v3, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 494161
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494162
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "zc"

    .line 494163
    iget-object v3, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 494164
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494165
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "em"

    .line 494166
    iget-object v3, p0, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 494167
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494168
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 494169
    check-cast p1, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;

    .line 494170
    invoke-static {p1}, LX/33m;->b(Lcom/facebook/zero/server/SendZeroHeaderRequestParams;)Ljava/util/List;

    move-result-object v0

    .line 494171
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "sendZeroHeaderRequest"

    .line 494172
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 494173
    move-object v1, v1

    .line 494174
    const-string v2, "GET"

    .line 494175
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 494176
    move-object v1, v1

    .line 494177
    const-string v2, "hr/r"

    .line 494178
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 494179
    move-object v1, v1

    .line 494180
    const/4 v2, 0x1

    .line 494181
    iput-boolean v2, v1, LX/14O;->q:Z

    .line 494182
    move-object v1, v1

    .line 494183
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 494184
    move-object v0, v1

    .line 494185
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 494186
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 494187
    move-object v0, v0

    .line 494188
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 494189
    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    .line 494190
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 494191
    const/4 v0, 0x0

    return-object v0
.end method
