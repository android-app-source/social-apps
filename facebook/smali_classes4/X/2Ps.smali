.class public final LX/2Ps;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/omnistore/module/OmnistoreStoredProcedureComponent;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/omnistore/module/OmnistoreStoredProcedureComponent;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final mInjector:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 407280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407281
    iput-object p1, p0, LX/2Ps;->mInjector:LX/0QB;

    .line 407282
    return-void
.end method

.method public static getLazySet(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/omnistore/module/OmnistoreStoredProcedureComponent;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 407283
    new-instance v0, LX/2Ps;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Ps;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 407284
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public static getSet(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/omnistore/module/OmnistoreStoredProcedureComponent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407285
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/2Ps;

    invoke-direct {v2, p0}, LX/2Ps;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 407286
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2Ps;->mInjector:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v0, v0

    .line 407287
    return-object v0
.end method

.method public final provide(LX/0QC;I)LX/2PI;
    .locals 2

    .prologue
    .line 407288
    packed-switch p2, :pswitch_data_0

    .line 407289
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407290
    :pswitch_0
    invoke-static {p1}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(LX/0QB;)Lcom/facebook/messaging/tincan/outbound/Sender;

    move-result-object v0

    .line 407291
    :goto_0
    return-object v0

    .line 407292
    :pswitch_1
    invoke-static {p1}, LX/2PL;->a(LX/0QB;)LX/2PL;

    move-result-object v0

    goto :goto_0

    .line 407293
    :pswitch_2
    invoke-static {p1}, LX/2cD;->a(LX/0QB;)LX/2cD;

    move-result-object v0

    goto :goto_0

    .line 407294
    :pswitch_3
    invoke-static {p1}, LX/2PO;->a(LX/0QB;)LX/2PO;

    move-result-object v0

    goto :goto_0

    .line 407295
    :pswitch_4
    invoke-static {p1}, LX/2PQ;->a(LX/0QB;)LX/2PQ;

    move-result-object v0

    goto :goto_0

    .line 407296
    :pswitch_5
    invoke-static {p1}, LX/2PK;->a(LX/0QB;)LX/2PK;

    move-result-object v0

    goto :goto_0

    .line 407297
    :pswitch_6
    invoke-static {p1}, LX/2PH;->a(LX/0QB;)LX/2PH;

    move-result-object v0

    goto :goto_0

    .line 407298
    :pswitch_7
    invoke-static {p1}, LX/2PP;->a(LX/0QB;)LX/2PP;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final bridge synthetic provide(LX/0QC;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 407299
    invoke-virtual {p0, p1, p2}, LX/2Ps;->provide(LX/0QC;I)LX/2PI;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 407300
    const/16 v0, 0x8

    return v0
.end method
