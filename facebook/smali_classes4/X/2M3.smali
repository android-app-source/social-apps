.class public LX/2M3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2M3;


# instance fields
.field private final a:LX/2MA;


# direct methods
.method public constructor <init>(LX/2MA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396159
    iput-object p1, p0, LX/2M3;->a:LX/2MA;

    .line 396160
    return-void
.end method

.method public static a(LX/0QB;)LX/2M3;
    .locals 4

    .prologue
    .line 396161
    sget-object v0, LX/2M3;->b:LX/2M3;

    if-nez v0, :cond_1

    .line 396162
    const-class v1, LX/2M3;

    monitor-enter v1

    .line 396163
    :try_start_0
    sget-object v0, LX/2M3;->b:LX/2M3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 396164
    if-eqz v2, :cond_0

    .line 396165
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 396166
    new-instance p0, LX/2M3;

    invoke-static {v0}, LX/2M4;->a(LX/0QB;)LX/2MA;

    move-result-object v3

    check-cast v3, LX/2MA;

    invoke-direct {p0, v3}, LX/2M3;-><init>(LX/2MA;)V

    .line 396167
    move-object v0, p0

    .line 396168
    sput-object v0, LX/2M3;->b:LX/2M3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396169
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 396170
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 396171
    :cond_1
    sget-object v0, LX/2M3;->b:LX/2M3;

    return-object v0

    .line 396172
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 396173
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 1

    .prologue
    .line 396174
    iget-object v0, p0, LX/2M3;->a:LX/2MA;

    invoke-interface {v0}, LX/2MA;->f()V

    .line 396175
    return-void
.end method
