.class public final LX/35g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/VisibleForTesting;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/1gH;

.field private c:J

.field private d:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/io/File;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 497379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497380
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 497381
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/35g;->a:Ljava/lang/String;

    .line 497382
    invoke-static {p2}, LX/1gH;->a(Ljava/io/File;)LX/1gH;

    move-result-object v0

    iput-object v0, p0, LX/35g;->b:LX/1gH;

    .line 497383
    iput-wide v2, p0, LX/35g;->c:J

    .line 497384
    iput-wide v2, p0, LX/35g;->d:J

    .line 497385
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 497386
    iget-object v0, p0, LX/35g;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 497387
    iget-wide v0, p0, LX/35g;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 497388
    iget-object v0, p0, LX/35g;->b:LX/1gH;

    .line 497389
    iget-object v1, v0, LX/1gH;->a:Ljava/io/File;

    move-object v0, v1

    .line 497390
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, LX/35g;->d:J

    .line 497391
    :cond_0
    iget-wide v0, p0, LX/35g;->d:J

    return-wide v0
.end method

.method public final d()J
    .locals 4

    .prologue
    .line 497392
    iget-wide v0, p0, LX/35g;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 497393
    iget-object v0, p0, LX/35g;->b:LX/1gH;

    invoke-virtual {v0}, LX/1gH;->c()J

    move-result-wide v0

    iput-wide v0, p0, LX/35g;->c:J

    .line 497394
    :cond_0
    iget-wide v0, p0, LX/35g;->c:J

    return-wide v0
.end method
