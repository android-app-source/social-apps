.class public LX/3Cl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/3Cl;


# instance fields
.field public final a:LX/03V;

.field public final b:LX/3Cm;

.field public final c:LX/3Cs;

.field public final d:LX/33W;

.field public final e:LX/2jO;

.field private f:Z


# direct methods
.method public constructor <init>(LX/3Cm;LX/03V;LX/3Cs;LX/33W;LX/2jO;LX/3Ct;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 530492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530493
    iput-object p1, p0, LX/3Cl;->b:LX/3Cm;

    .line 530494
    iput-object p2, p0, LX/3Cl;->a:LX/03V;

    .line 530495
    iput-object p3, p0, LX/3Cl;->c:LX/3Cs;

    .line 530496
    iput-object p4, p0, LX/3Cl;->d:LX/33W;

    .line 530497
    iput-object p5, p0, LX/3Cl;->e:LX/2jO;

    .line 530498
    iget-object v0, p6, LX/3Ct;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 530499
    iget-object v0, p6, LX/3Ct;->a:LX/0ad;

    sget-short p1, LX/15r;->e:S

    const/4 p2, 0x0

    invoke-interface {v0, p1, p2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p6, LX/3Ct;->b:Ljava/lang/Boolean;

    .line 530500
    :cond_0
    iget-object v0, p6, LX/3Ct;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 530501
    iput-boolean v0, p0, LX/3Cl;->f:Z

    .line 530502
    return-void
.end method

.method public static a(LX/0QB;)LX/3Cl;
    .locals 10

    .prologue
    .line 530479
    sget-object v0, LX/3Cl;->g:LX/3Cl;

    if-nez v0, :cond_1

    .line 530480
    const-class v1, LX/3Cl;

    monitor-enter v1

    .line 530481
    :try_start_0
    sget-object v0, LX/3Cl;->g:LX/3Cl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 530482
    if-eqz v2, :cond_0

    .line 530483
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 530484
    new-instance v3, LX/3Cl;

    invoke-static {v0}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v4

    check-cast v4, LX/3Cm;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/3Cs;->a(LX/0QB;)LX/3Cs;

    move-result-object v6

    check-cast v6, LX/3Cs;

    invoke-static {v0}, LX/33W;->a(LX/0QB;)LX/33W;

    move-result-object v7

    check-cast v7, LX/33W;

    invoke-static {v0}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v8

    check-cast v8, LX/2jO;

    invoke-static {v0}, LX/3Ct;->a(LX/0QB;)LX/3Ct;

    move-result-object v9

    check-cast v9, LX/3Ct;

    invoke-direct/range {v3 .. v9}, LX/3Cl;-><init>(LX/3Cm;LX/03V;LX/3Cs;LX/33W;LX/2jO;LX/3Ct;)V

    .line 530485
    move-object v0, v3

    .line 530486
    sput-object v0, LX/3Cl;->g:LX/3Cl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530487
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 530488
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 530489
    :cond_1
    sget-object v0, LX/3Cl;->g:LX/3Cl;

    return-object v0

    .line 530490
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 530491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View$OnClickListener;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 530410
    new-instance v0, LX/3DF;

    invoke-direct {v0, p0, p1}, LX/3DF;-><init>(LX/3Cl;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private static a(LX/2nq;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 530474
    invoke-interface {p0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 530475
    :cond_1
    :goto_0
    return-object v0

    .line 530476
    :cond_2
    invoke-interface {p0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    .line 530477
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->c()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->e()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_3
    move-object v0, v1

    .line 530478
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 530467
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 530468
    const/4 v1, 0x2

    new-array v1, v1, [I

    const v2, 0x7f010204

    aput v2, v1, v4

    const v2, 0x7f0101fd

    aput v2, v1, v5

    .line 530469
    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 530470
    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 530471
    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 530472
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 530473
    mul-int/lit8 v0, v2, 0x2

    add-int/2addr v0, v1

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 530465
    new-instance v0, LX/3Cv;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v0, p0}, LX/3Cv;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 530466
    return-object v0
.end method

.method public final a(Landroid/view/View;LX/DqC;LX/2nq;I)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    .line 530444
    invoke-interface {p3}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    move-object v2, p1

    .line 530445
    check-cast v2, LX/3Cv;

    .line 530446
    if-nez p2, :cond_0

    .line 530447
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 530448
    :goto_0
    return-void

    .line 530449
    :cond_0
    iget-object v0, v2, LX/3Cv;->b:Lcom/facebook/notifications/widget/CaspianNotificationsView;

    invoke-virtual {v0}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 530450
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v1, :cond_1

    .line 530451
    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    .line 530452
    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, LX/3qk;->a(II)I

    move-result v0

    const/high16 v1, -0x1000000

    or-int/2addr v0, v1

    .line 530453
    iget-object v1, v2, LX/3Cv;->d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    invoke-virtual {v1, v0}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->setColor(I)V

    .line 530454
    iget-object v1, v2, LX/3Cv;->c:Lcom/facebook/notifications/widget/PostFeedbackView;

    invoke-virtual {v1, v0}, Lcom/facebook/notifications/widget/PostFeedbackView;->setBackgroundColor(I)V

    .line 530455
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 530456
    new-instance v0, LX/Ds8;

    move-object v1, p0

    move-object v3, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/Ds8;-><init>(LX/3Cl;LX/3Cv;LX/DqC;Lcom/facebook/graphql/model/GraphQLStory;I)V

    .line 530457
    const v1, 0x7f08116d

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 530458
    iget-object v3, v2, LX/3Cv;->c:Lcom/facebook/notifications/widget/PostFeedbackView;

    move-object v2, v3

    .line 530459
    iget-object v3, p2, LX/DqC;->e:Ljava/lang/String;

    move-object v3, v3

    .line 530460
    new-instance v4, Landroid/text/SpannableString;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 530461
    new-instance v3, LX/Ds9;

    invoke-direct {v3, p0, v0, p1}, LX/Ds9;-><init>(LX/3Cl;LX/Ds8;Landroid/view/View;)V

    .line 530462
    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/16 v5, 0x21

    invoke-virtual {v4, v3, v0, v1, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 530463
    invoke-virtual {v2, v4}, Lcom/facebook/notifications/widget/PostFeedbackView;->setText(Landroid/text/SpannableString;)V

    .line 530464
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a009c

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v2, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2nq;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 14

    .prologue
    .line 530411
    invoke-interface/range {p5 .. p5}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 530412
    if-nez v5, :cond_0

    .line 530413
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 530414
    iget-object v2, p0, LX/3Cl;->a:LX/03V;

    const-string v3, "notification"

    const-string v4, "Can\'t bind null FeedStory"

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 530415
    :goto_0
    return-void

    .line 530416
    :cond_0
    instance-of v2, p1, LX/3Cv;

    if-eqz v2, :cond_2

    move-object v2, p1

    .line 530417
    check-cast v2, LX/3Cv;

    .line 530418
    invoke-virtual {v2}, LX/3Cv;->a()V

    .line 530419
    invoke-virtual {v2}, LX/3Cv;->getNotificationView()Lcom/facebook/notifications/widget/CaspianNotificationsView;

    move-result-object v2

    .line 530420
    :goto_1
    const v3, 0x7f0d08e0

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Landroid/widget/TextView;

    .line 530421
    invoke-interface/range {p5 .. p5}, LX/2nq;->j()LX/0Px;

    move-result-object v6

    .line 530422
    const v3, 0x7f0d08e2

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 530423
    invoke-static {v4}, LX/0vv;->n(Landroid/view/View;)I

    move-result v7

    .line 530424
    invoke-static {v4}, LX/0vv;->o(Landroid/view/View;)I

    move-result v8

    .line 530425
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v9

    .line 530426
    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v10

    .line 530427
    if-eqz v6, :cond_3

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->SEE_FIRST_NOTIFICATIONS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    invoke-virtual {v6, v11}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-boolean v6, p0, LX/3Cl;->f:Z

    if-eqz v6, :cond_3

    .line 530428
    invoke-virtual {v4}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f0b0a78

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    invoke-static {v4, v7, v9, v6, v10}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 530429
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 530430
    :goto_2
    const/4 v13, 0x0

    .line 530431
    const/4 v12, 0x0

    .line 530432
    if-eqz p7, :cond_1

    .line 530433
    invoke-static/range {p5 .. p5}, LX/3Cl;->a(LX/2nq;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    move-result-object v6

    .line 530434
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->b()Z

    move-result v3

    if-nez v3, :cond_1

    .line 530435
    invoke-interface/range {p5 .. p5}, LX/2nq;->k()Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Landroid/text/SpannableString;

    invoke-virtual {v6}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->e()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 530436
    :goto_3
    move-object/from16 v0, p7

    invoke-direct {p0, v0}, LX/3Cl;->a(Landroid/view/View$OnClickListener;)Landroid/text/style/ClickableSpan;

    move-result-object v12

    move-object v13, v3

    .line 530437
    :cond_1
    move-object/from16 v0, p6

    invoke-direct {p0, v0}, LX/3Cl;->a(Landroid/view/View$OnClickListener;)Landroid/text/style/ClickableSpan;

    move-result-object v11

    .line 530438
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v3

    iget-object v6, p0, LX/3Cl;->b:LX/3Cm;

    sget-object v7, LX/3DG;->JEWEL:LX/3DG;

    invoke-virtual {v6, v5, v7, v4}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;Landroid/widget/TextView;)Landroid/text/Spannable;

    move-result-object v7

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v8

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p2

    invoke-virtual/range {v2 .. v13}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/text/Spannable;JLjava/lang/String;Landroid/text/style/ClickableSpan;Landroid/text/style/ClickableSpan;Landroid/text/SpannableString;)V

    .line 530439
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_2
    move-object v2, p1

    .line 530440
    check-cast v2, Lcom/facebook/notifications/widget/CaspianNotificationsView;

    goto/16 :goto_1

    .line 530441
    :cond_3
    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 530442
    invoke-static {v4, v7, v9, v8, v10}, LX/0vv;->b(Landroid/view/View;IIII)V

    goto :goto_2

    .line 530443
    :cond_4
    new-instance v3, Landroid/text/SpannableString;

    invoke-virtual {v6}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->c()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
