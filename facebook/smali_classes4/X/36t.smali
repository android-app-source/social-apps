.class public final enum LX/36t;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/36t;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/36t;

.field public static final enum DASH:LX/36t;

.field public static final enum PROGRESSIVE:LX/36t;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 500528
    new-instance v0, LX/36t;

    const-string v1, "PROGRESSIVE"

    invoke-direct {v0, v1, v2}, LX/36t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/36t;->PROGRESSIVE:LX/36t;

    .line 500529
    new-instance v0, LX/36t;

    const-string v1, "DASH"

    invoke-direct {v0, v1, v3}, LX/36t;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/36t;->DASH:LX/36t;

    .line 500530
    const/4 v0, 0x2

    new-array v0, v0, [LX/36t;

    sget-object v1, LX/36t;->PROGRESSIVE:LX/36t;

    aput-object v1, v0, v2

    sget-object v1, LX/36t;->DASH:LX/36t;

    aput-object v1, v0, v3

    sput-object v0, LX/36t;->$VALUES:[LX/36t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 500531
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/36t;
    .locals 1

    .prologue
    .line 500532
    const-class v0, LX/36t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/36t;

    return-object v0
.end method

.method public static values()[LX/36t;
    .locals 1

    .prologue
    .line 500533
    sget-object v0, LX/36t;->$VALUES:[LX/36t;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/36t;

    return-object v0
.end method
