.class public final LX/28y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2aX;


# instance fields
.field public final synthetic a:LX/28x;


# direct methods
.method public constructor <init>(LX/28x;)V
    .locals 0

    .prologue
    .line 375192
    iput-object p1, p0, LX/28y;->a:LX/28x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Z)V
    .locals 4

    .prologue
    .line 375193
    if-nez p2, :cond_0

    .line 375194
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 375195
    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 375196
    if-eqz v0, :cond_0

    .line 375197
    iget-object v1, p0, LX/28y;->a:LX/28x;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "peerstate:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/28x;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    .line 375198
    :cond_0
    return-void
.end method
