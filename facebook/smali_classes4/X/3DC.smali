.class public LX/3DC;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/1Lg;

.field public final c:LX/1mB;

.field public final d:LX/19s;

.field public final e:LX/19j;

.field private final f:LX/1Bv;

.field private final g:LX/1Lr;

.field private final h:LX/1b4;

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1M7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 531910
    const-class v0, LX/3DC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3DC;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(LX/0Or;LX/1mB;LX/19s;LX/19j;LX/1Bv;LX/1Lr;LX/1b4;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Lg;",
            ">;",
            "LX/1mB;",
            "LX/19s;",
            "LX/19j;",
            "LX/1Bv;",
            "LX/1Lr;",
            "LX/1b4;",
            ")V"
        }
    .end annotation

    .prologue
    .line 531911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 531912
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lg;

    iput-object v0, p0, LX/3DC;->b:LX/1Lg;

    .line 531913
    iput-object p2, p0, LX/3DC;->c:LX/1mB;

    .line 531914
    iput-object p3, p0, LX/3DC;->d:LX/19s;

    .line 531915
    iput-object p4, p0, LX/3DC;->e:LX/19j;

    .line 531916
    iput-object p5, p0, LX/3DC;->f:LX/1Bv;

    .line 531917
    iput-object p6, p0, LX/3DC;->g:LX/1Lr;

    .line 531918
    iput-object p7, p0, LX/3DC;->h:LX/1b4;

    .line 531919
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/3DC;->i:Ljava/util/Map;

    .line 531920
    return-void
.end method

.method public static a(LX/0QB;)LX/3DC;
    .locals 1

    .prologue
    .line 531921
    invoke-static {p0}, LX/3DC;->b(LX/0QB;)LX/3DC;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3DC;
    .locals 8

    .prologue
    .line 531922
    new-instance v0, LX/3DC;

    const/16 v1, 0x135c

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/1lz;->a(LX/0QB;)LX/1mB;

    move-result-object v2

    check-cast v2, LX/1mB;

    invoke-static {p0}, LX/19i;->a(LX/0QB;)LX/19s;

    move-result-object v3

    check-cast v3, LX/19s;

    invoke-static {p0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v4

    check-cast v4, LX/19j;

    invoke-static {p0}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v5

    check-cast v5, LX/1Bv;

    invoke-static {p0}, LX/1Lr;->a(LX/0QB;)LX/1Lr;

    move-result-object v6

    check-cast v6, LX/1Lr;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v7

    check-cast v7, LX/1b4;

    invoke-direct/range {v0 .. v7}, LX/3DC;-><init>(LX/0Or;LX/1mB;LX/19s;LX/19j;LX/1Bv;LX/1Lr;LX/1b4;)V

    .line 531923
    return-object v0
.end method

.method private c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 531924
    invoke-static {p1}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 531925
    :goto_0
    return v0

    .line 531926
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 531927
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 531928
    :goto_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_CINEMAGRAPH:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSPIRATION_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v0, v3, :cond_6

    :cond_1
    const/4 v3, 0x1

    :goto_2
    move v0, v3

    .line 531929
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3DC;->f:LX/1Bv;

    invoke-virtual {v0}, LX/1Bv;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 531930
    :goto_3
    if-nez v0, :cond_4

    iget-object v0, p0, LX/3DC;->g:LX/1Lr;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v3

    invoke-virtual {v0, v3}, LX/1Lr;->a(Z)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 531931
    goto :goto_0

    .line 531932
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_1

    :cond_3
    move v0, v1

    .line 531933
    goto :goto_3

    .line 531934
    :cond_4
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 531935
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 531936
    iget-object v4, p0, LX/3DC;->e:LX/19j;

    iget-boolean v4, v4, LX/19j;->q:Z

    if-eqz v4, :cond_9

    .line 531937
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aW()Ljava/lang/String;

    move-result-object v4

    .line 531938
    if-eqz v4, :cond_9

    .line 531939
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 531940
    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v6, ".mpd"

    invoke-virtual {v4, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    move-object v5, v7

    .line 531941
    :goto_4
    if-nez v5, :cond_8

    .line 531942
    :goto_5
    move v0, v8

    .line 531943
    goto :goto_0

    .line 531944
    :cond_5
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 531945
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 531946
    if-nez v6, :cond_a

    .line 531947
    :goto_6
    move v0, v4

    .line 531948
    goto/16 :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_2

    .line 531949
    :cond_7
    invoke-virtual {p2}, LX/379;->name()Ljava/lang/String;

    goto :goto_4

    .line 531950
    :cond_8
    new-instance v4, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, LX/379;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aT()Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v4 .. v10}, Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 531951
    iget-object v5, p0, LX/3DC;->d:LX/19s;

    invoke-virtual {v5, v4}, LX/19s;->a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)J

    .line 531952
    const/4 v8, 0x1

    goto :goto_5

    :cond_9
    move-object v5, v7

    goto :goto_4

    .line 531953
    :cond_a
    new-instance v7, LX/36s;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v6, v8}, LX/36s;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 531954
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->r()I

    move-result v6

    int-to-long v8, v6

    .line 531955
    iput-wide v8, v7, LX/36s;->f:J

    .line 531956
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->n()I

    move-result v6

    int-to-long v8, v6

    .line 531957
    iput-wide v8, v7, LX/36s;->e:J

    .line 531958
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aR()I

    move-result v6

    int-to-long v8, v6

    .line 531959
    iput-wide v8, v7, LX/36s;->g:J

    .line 531960
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v6

    .line 531961
    iput-boolean v6, v7, LX/36s;->i:Z

    .line 531962
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aT()Ljava/lang/String;

    move-result-object v6

    .line 531963
    iget-object v8, p0, LX/3DC;->i:Ljava/util/Map;

    invoke-virtual {p2}, LX/379;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1M7;

    .line 531964
    if-nez v8, :cond_b

    .line 531965
    iget-object v8, p0, LX/3DC;->b:LX/1Lg;

    .line 531966
    sget-object v9, LX/D7b;->a:[I

    invoke-virtual {p2}, LX/379;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 531967
    sget-object v9, LX/1Li;->MISC:LX/1Li;

    :goto_7
    move-object v9, v9

    .line 531968
    invoke-virtual {v8, v9}, LX/1Lg;->a(LX/1Li;)LX/1M7;

    move-result-object v8

    .line 531969
    iget-object v9, p0, LX/3DC;->i:Ljava/util/Map;

    invoke-virtual {p2}, LX/379;->name()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531970
    :cond_b
    move-object v8, v8

    .line 531971
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_c

    .line 531972
    iget-object v9, p0, LX/3DC;->c:LX/1mB;

    invoke-virtual {v9, v6, v8, v7}, LX/1mB;->a(Ljava/lang/String;LX/1M7;LX/36s;)V

    .line 531973
    invoke-virtual {p2}, LX/379;->name()Ljava/lang/String;

    .line 531974
    :cond_c
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_d

    iget-object v6, p0, LX/3DC;->c:LX/1mB;

    invoke-virtual {v6}, LX/1mB;->a()Z

    move-result v6

    if-nez v6, :cond_e

    .line 531975
    :cond_d
    new-array v6, v5, [LX/36s;

    aput-object v7, v6, v4

    invoke-interface {v8, v6}, LX/1M7;->b([LX/36s;)V

    .line 531976
    invoke-virtual {p2}, LX/379;->name()Ljava/lang/String;

    :cond_e
    move v4, v5

    .line 531977
    goto/16 :goto_6

    .line 531978
    :pswitch_0
    sget-object v9, LX/1Li;->COMMERCIAL_BREAK:LX/1Li;

    goto :goto_7

    .line 531979
    :pswitch_1
    sget-object v9, LX/1Li;->INSTANT_ARTICLE:LX/1Li;

    goto :goto_7

    .line 531980
    :pswitch_2
    sget-object v9, LX/1Li;->CHANNEL:LX/1Li;

    goto :goto_7

    .line 531981
    :pswitch_3
    sget-object v9, LX/1Li;->VIDEO_HOME:LX/1Li;

    goto :goto_7

    .line 531982
    :pswitch_4
    sget-object v9, LX/1Li;->VIDEO_HOME:LX/1Li;

    goto :goto_7

    .line 531983
    :pswitch_5
    sget-object v9, LX/1Li;->TIMELINE:LX/1Li;

    goto :goto_7

    .line 531984
    :pswitch_6
    sget-object v9, LX/1Li;->MISC:LX/1Li;

    goto :goto_7

    .line 531985
    :pswitch_7
    sget-object v9, LX/1Li;->NEWSFEED:LX/1Li;

    goto :goto_7

    .line 531986
    :pswitch_8
    sget-object v9, LX/1Li;->MISC:LX/1Li;

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/379;)Z
    .locals 5
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 531987
    if-nez p1, :cond_0

    .line 531988
    :goto_0
    return v2

    .line 531989
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 531990
    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 531991
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 531992
    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 531993
    if-eqz v0, :cond_1

    .line 531994
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531995
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 531996
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 531997
    invoke-virtual {p0, v0, p2}, LX/3DC;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z

    move-result v0

    or-int/2addr v1, v0

    .line 531998
    goto :goto_1

    :cond_2
    move v2, v1

    .line 531999
    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 532000
    invoke-virtual {p0, p1, p2}, LX/3DC;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z
    .locals 7
    .param p1    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 532001
    if-nez p1, :cond_0

    .line 532002
    :goto_0
    return v2

    .line 532003
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 532004
    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 532005
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 532006
    invoke-interface {v4, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 532007
    if-eqz v0, :cond_1

    .line 532008
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 532009
    invoke-direct {p0, v0, p2}, LX/3DC;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 532010
    iget-object v3, p0, LX/3DC;->e:LX/19j;

    iget-boolean v3, v3, LX/19j;->ah:Z

    if-nez v3, :cond_2

    iget-object v3, p0, LX/3DC;->h:LX/1b4;

    .line 532011
    iget-object v5, v3, LX/1b4;->a:LX/0ad;

    sget-short v6, LX/1v6;->O:S

    const/4 p1, 0x1

    invoke-interface {v5, v6, p1}, LX/0ad;->a(SZ)Z

    move-result v5

    move v3, v5

    .line 532012
    if-eqz v3, :cond_1

    .line 532013
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 532014
    if-eqz v0, :cond_3

    .line 532015
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aV()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/3DC;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/379;)Z

    move-result v0

    or-int/2addr v1, v0

    .line 532016
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_4
    move v2, v1

    .line 532017
    goto :goto_0
.end method
