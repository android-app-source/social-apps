.class public final enum LX/24W;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/24W;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/24W;

.field public static final enum CW_0:LX/24W;

.field public static final enum CW_180:LX/24W;

.field public static final enum CW_270:LX/24W;

.field public static final enum CW_90:LX/24W;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 367529
    new-instance v0, LX/24W;

    const-string v1, "CW_0"

    invoke-direct {v0, v1, v2}, LX/24W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/24W;->CW_0:LX/24W;

    .line 367530
    new-instance v0, LX/24W;

    const-string v1, "CW_90"

    invoke-direct {v0, v1, v3}, LX/24W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/24W;->CW_90:LX/24W;

    .line 367531
    new-instance v0, LX/24W;

    const-string v1, "CW_180"

    invoke-direct {v0, v1, v4}, LX/24W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/24W;->CW_180:LX/24W;

    .line 367532
    new-instance v0, LX/24W;

    const-string v1, "CW_270"

    invoke-direct {v0, v1, v5}, LX/24W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/24W;->CW_270:LX/24W;

    .line 367533
    const/4 v0, 0x4

    new-array v0, v0, [LX/24W;

    sget-object v1, LX/24W;->CW_0:LX/24W;

    aput-object v1, v0, v2

    sget-object v1, LX/24W;->CW_90:LX/24W;

    aput-object v1, v0, v3

    sget-object v1, LX/24W;->CW_180:LX/24W;

    aput-object v1, v0, v4

    sget-object v1, LX/24W;->CW_270:LX/24W;

    aput-object v1, v0, v5

    sput-object v0, LX/24W;->$VALUES:[LX/24W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 367534
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/24W;
    .locals 1

    .prologue
    .line 367535
    const-class v0, LX/24W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/24W;

    return-object v0
.end method

.method public static values()[LX/24W;
    .locals 1

    .prologue
    .line 367536
    sget-object v0, LX/24W;->$VALUES:[LX/24W;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/24W;

    return-object v0
.end method
