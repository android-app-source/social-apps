.class public final LX/31h;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/680;

.field private final b:[F

.field private final c:LX/31i;


# direct methods
.method public constructor <init>(LX/680;)V
    .locals 1

    .prologue
    .line 487830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487831
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/31h;->b:[F

    .line 487832
    new-instance v0, LX/31i;

    invoke-direct {v0}, LX/31i;-><init>()V

    iput-object v0, p0, LX/31h;->c:LX/31i;

    .line 487833
    iput-object p1, p0, LX/31h;->a:LX/680;

    .line 487834
    return-void
.end method

.method public static a(D)D
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 487829
    const-wide v0, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double v4, p0, v6

    sub-double/2addr v2, v4

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    mul-double/2addr v2, v6

    const-wide v4, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(DD)D
    .locals 2

    .prologue
    .line 487828
    const-wide v0, 0x41584db040000000L    # 6371009.0

    div-double v0, p2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    add-double/2addr v0, p0

    return-wide v0
.end method

.method public static a(DDD)D
    .locals 4

    .prologue
    .line 487827
    const-wide v0, 0x41584db040000000L    # 6371009.0

    div-double v0, p4, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    invoke-static {p2, p3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    add-double/2addr v0, p0

    return-wide v0
.end method

.method public static b(D)F
    .locals 8

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 487825
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, p0

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    .line 487826
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double v4, v6, v0

    sub-double v0, v6, v0

    div-double v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v4, 0x402921fb54442d18L    # 12.566370614359172

    div-double/2addr v0, v4

    sub-double v0, v2, v0

    double-to-float v0, v0

    return v0
.end method

.method public static c(D)D
    .locals 4

    .prologue
    .line 487824
    const-wide v0, 0x4076800000000000L    # 360.0

    mul-double/2addr v0, p0

    const-wide v2, 0x4066800000000000L    # 180.0

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method public static d(D)F
    .locals 2

    .prologue
    .line 487823
    const-wide v0, 0x4066800000000000L    # 180.0

    add-double/2addr v0, p0

    double-to-float v0, v0

    const/high16 v1, 0x43b40000    # 360.0f

    div-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(F)D
    .locals 4

    .prologue
    .line 487749
    iget-object v0, p0, LX/31h;->a:LX/680;

    .line 487750
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 487751
    iget-wide v2, v0, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v1, v2

    iget v0, v0, Lcom/facebook/android/maps/MapView;->h:F

    mul-float/2addr v0, v1

    div-float v0, p1, v0

    float-to-double v0, v0

    return-wide v0
.end method

.method public final a()LX/69F;
    .locals 6

    .prologue
    .line 487816
    iget-object v0, p0, LX/31h;->a:LX/680;

    .line 487817
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 487818
    iget-object v1, p0, LX/31h;->a:LX/680;

    iget v1, v1, LX/680;->c:I

    int-to-float v1, v1

    iget v2, v0, Lcom/facebook/android/maps/MapView;->d:I

    iget-object v3, p0, LX/31h;->a:LX/680;

    iget v3, v3, LX/680;->f:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p0, v1, v2}, LX/31h;->a(FF)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v1

    .line 487819
    iget v2, v0, Lcom/facebook/android/maps/MapView;->c:I

    iget-object v3, p0, LX/31h;->a:LX/680;

    iget v3, v3, LX/680;->e:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, v0, Lcom/facebook/android/maps/MapView;->d:I

    iget-object v4, p0, LX/31h;->a:LX/680;

    iget v4, v4, LX/680;->f:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {p0, v2, v3}, LX/31h;->a(FF)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v2

    .line 487820
    iget-object v3, p0, LX/31h;->a:LX/680;

    iget v3, v3, LX/680;->c:I

    int-to-float v3, v3

    iget-object v4, p0, LX/31h;->a:LX/680;

    iget v4, v4, LX/680;->d:I

    int-to-float v4, v4

    invoke-virtual {p0, v3, v4}, LX/31h;->a(FF)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v3

    .line 487821
    iget v0, v0, Lcom/facebook/android/maps/MapView;->c:I

    iget-object v4, p0, LX/31h;->a:LX/680;

    iget v4, v4, LX/680;->e:I

    sub-int/2addr v0, v4

    int-to-float v0, v0

    iget-object v4, p0, LX/31h;->a:LX/680;

    iget v4, v4, LX/680;->d:I

    int-to-float v4, v4

    invoke-virtual {p0, v0, v4}, LX/31h;->a(FF)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v4

    .line 487822
    new-instance v0, LX/69F;

    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v5

    invoke-virtual {v5}, LX/696;->a()LX/697;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/69F;-><init>(Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;LX/697;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/android/maps/model/LatLng;)Landroid/graphics/Point;
    .locals 14

    .prologue
    .line 487813
    iget-wide v2, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-wide v4, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-object v6, p0, LX/31h;->b:[F

    move-object v1, p0

    .line 487814
    invoke-static {v4, v5}, LX/31h;->d(D)F

    move-result v7

    float-to-double v9, v7

    invoke-static {v2, v3}, LX/31h;->b(D)F

    move-result v7

    float-to-double v11, v7

    move-object v8, v1

    move-object v13, v6

    invoke-virtual/range {v8 .. v13}, LX/31h;->b(DD[F)V

    .line 487815
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, LX/31h;->b:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    float-to-int v1, v1

    iget-object v2, p0, LX/31h;->b:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public final a(FF)Lcom/facebook/android/maps/model/LatLng;
    .locals 6

    .prologue
    .line 487811
    iget-object v0, p0, LX/31h;->b:[F

    invoke-virtual {p0, p1, p2, v0}, LX/31h;->a(FF[F)V

    .line 487812
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    iget-object v1, p0, LX/31h;->b:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, LX/31h;->a(D)D

    move-result-wide v2

    iget-object v1, p0, LX/31h;->b:[F

    const/4 v4, 0x0

    aget v1, v1, v4

    float-to-double v4, v1

    invoke-static {v4, v5}, LX/31h;->c(D)D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    return-object v0
.end method

.method public final a(Landroid/graphics/Point;)Lcom/facebook/android/maps/model/LatLng;
    .locals 2

    .prologue
    .line 487810
    iget v0, p1, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, LX/31h;->a(FF)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public final a(DD[F)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 487796
    iget-object v0, p0, LX/31h;->a:LX/680;

    .line 487797
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v2, v1

    .line 487798
    iget-wide v0, v2, Lcom/facebook/android/maps/MapView;->m:D

    .line 487799
    iget-object v3, p0, LX/31h;->c:LX/31i;

    invoke-virtual {p0, v3}, LX/31h;->a(LX/31i;)V

    .line 487800
    iget-object v3, p0, LX/31h;->c:LX/31i;

    iget-wide v4, v3, LX/31i;->c:D

    cmpg-double v3, v4, v0

    if-gtz v3, :cond_0

    iget-object v3, p0, LX/31h;->c:LX/31i;

    iget-wide v4, v3, LX/31i;->d:D

    cmpg-double v3, v0, v4

    if-lez v3, :cond_1

    .line 487801
    :cond_0
    iget-object v3, p0, LX/31h;->c:LX/31i;

    iget-wide v4, v3, LX/31i;->c:D

    sub-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    int-to-double v4, v3

    add-double/2addr v0, v4

    .line 487802
    :cond_1
    sub-double v0, p1, v0

    double-to-float v0, v0

    .line 487803
    iget-wide v4, v2, Lcom/facebook/android/maps/MapView;->n:D

    sub-double v4, p3, v4

    double-to-float v1, v4

    .line 487804
    iget-wide v4, v2, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v3, v4

    mul-float/2addr v0, v3

    aput v0, p5, v6

    .line 487805
    iget-wide v4, v2, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v0, v4

    mul-float/2addr v0, v1

    aput v0, p5, v7

    .line 487806
    iget-object v0, v2, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0, p5}, Landroid/graphics/Matrix;->mapVectors([F)V

    .line 487807
    aget v0, p5, v6

    iget v1, v2, Lcom/facebook/android/maps/MapView;->e:F

    add-float/2addr v0, v1

    aput v0, p5, v6

    .line 487808
    aget v0, p5, v7

    iget v1, v2, Lcom/facebook/android/maps/MapView;->f:F

    add-float/2addr v0, v1

    aput v0, p5, v7

    .line 487809
    return-void
.end method

.method public final a(FF[F)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 487784
    iget-object v0, p0, LX/31h;->a:LX/680;

    .line 487785
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 487786
    iget v1, v0, Lcom/facebook/android/maps/MapView;->e:F

    sub-float v1, p1, v1

    aput v1, p3, v6

    .line 487787
    iget v1, v0, Lcom/facebook/android/maps/MapView;->f:F

    sub-float v1, p2, v1

    aput v1, p3, v8

    .line 487788
    iget-object v1, v0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    invoke-virtual {v1, p3}, Landroid/graphics/Matrix;->mapVectors([F)V

    .line 487789
    iget-wide v2, v0, Lcom/facebook/android/maps/MapView;->m:D

    aget v1, p3, v6

    iget-wide v4, v0, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v4, v4

    div-float/2addr v1, v4

    float-to-double v4, v1

    add-double/2addr v2, v4

    double-to-float v1, v2

    aput v1, p3, v6

    .line 487790
    iget-wide v2, v0, Lcom/facebook/android/maps/MapView;->n:D

    aget v1, p3, v8

    iget-wide v4, v0, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v0, v4

    div-float v0, v1, v0

    float-to-double v0, v0

    add-double/2addr v0, v2

    double-to-float v0, v0

    aput v0, p3, v8

    .line 487791
    aget v0, p3, v6

    cmpl-float v0, v0, v7

    if-lez v0, :cond_1

    .line 487792
    aget v0, p3, v6

    sub-float/2addr v0, v7

    aput v0, p3, v6

    .line 487793
    :cond_0
    :goto_0
    return-void

    .line 487794
    :cond_1
    aget v0, p3, v6

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 487795
    aget v0, p3, v6

    add-float/2addr v0, v7

    aput v0, p3, v6

    goto :goto_0
.end method

.method public final a(LX/31i;)V
    .locals 6

    .prologue
    .line 487773
    iget-object v0, p0, LX/31h;->a:LX/680;

    .line 487774
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 487775
    iget-wide v2, v0, Lcom/facebook/android/maps/MapView;->n:D

    iget-wide v4, v0, Lcom/facebook/android/maps/MapView;->p:D

    sub-double/2addr v2, v4

    iput-wide v2, p1, LX/31i;->a:D

    .line 487776
    iget-wide v2, v0, Lcom/facebook/android/maps/MapView;->n:D

    iget-wide v4, v0, Lcom/facebook/android/maps/MapView;->p:D

    add-double/2addr v2, v4

    iput-wide v2, p1, LX/31i;->b:D

    .line 487777
    iget-wide v2, v0, Lcom/facebook/android/maps/MapView;->m:D

    iget-wide v4, v0, Lcom/facebook/android/maps/MapView;->o:D

    sub-double/2addr v2, v4

    iput-wide v2, p1, LX/31i;->c:D

    .line 487778
    iget-wide v2, v0, Lcom/facebook/android/maps/MapView;->m:D

    iget-wide v0, v0, Lcom/facebook/android/maps/MapView;->o:D

    add-double/2addr v0, v2

    iput-wide v0, p1, LX/31i;->d:D

    .line 487779
    iget-wide v0, p1, LX/31i;->c:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 487780
    iget-wide v0, p1, LX/31i;->c:D

    neg-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 487781
    iget-wide v2, p1, LX/31i;->c:D

    int-to-double v4, v0

    add-double/2addr v2, v4

    iput-wide v2, p1, LX/31i;->c:D

    .line 487782
    iget-wide v2, p1, LX/31i;->d:D

    int-to-double v0, v0

    add-double/2addr v0, v2

    iput-wide v0, p1, LX/31i;->d:D

    .line 487783
    :cond_0
    return-void
.end method

.method public final b()F
    .locals 2

    .prologue
    .line 487767
    iget-object v0, p0, LX/31h;->a:LX/680;

    .line 487768
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 487769
    iget v0, v0, Lcom/facebook/android/maps/MapView;->j:F

    .line 487770
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 487771
    const/high16 v1, 0x43b40000    # 360.0f

    add-float/2addr v0, v1

    .line 487772
    :cond_0
    return v0
.end method

.method public final b(DD[F)V
    .locals 11

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 487757
    iget-object v0, p0, LX/31h;->c:LX/31i;

    invoke-virtual {p0, v0}, LX/31h;->a(LX/31i;)V

    .line 487758
    iget-object v0, p0, LX/31h;->c:LX/31i;

    iget-wide v0, v0, LX/31i;->c:D

    cmpg-double v0, p1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, LX/31h;->c:LX/31i;

    iget-wide v0, v0, LX/31i;->d:D

    cmpl-double v0, p1, v0

    if-lez v0, :cond_2

    .line 487759
    :cond_0
    iget-object v0, p0, LX/31h;->c:LX/31i;

    iget-wide v0, v0, LX/31i;->c:D

    sub-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    int-to-double v0, v0

    add-double v2, p1, v0

    .line 487760
    iget-object v0, p0, LX/31h;->c:LX/31i;

    iget-wide v0, v0, LX/31i;->d:D

    cmpl-double v0, v2, v0

    if-lez v0, :cond_1

    .line 487761
    iget-object v0, p0, LX/31h;->c:LX/31i;

    iget-wide v0, v0, LX/31i;->d:D

    sub-double v0, v2, v0

    .line 487762
    iget-object v4, p0, LX/31h;->c:LX/31i;

    iget-wide v4, v4, LX/31i;->c:D

    sub-double v6, v2, v8

    sub-double/2addr v4, v6

    .line 487763
    cmpg-double v0, v4, v0

    if-gez v0, :cond_1

    .line 487764
    sub-double/2addr v2, v8

    :cond_1
    :goto_0
    move-object v1, p0

    move-wide v4, p3

    move-object/from16 v6, p5

    .line 487765
    invoke-virtual/range {v1 .. v6}, LX/31h;->a(DD[F)V

    .line 487766
    return-void

    :cond_2
    move-wide v2, p1

    goto :goto_0
.end method

.method public final e(D)D
    .locals 5

    .prologue
    .line 487752
    iget-object v0, p0, LX/31h;->a:LX/680;

    .line 487753
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 487754
    const-wide/16 v2, 0x0

    cmpg-double v1, p1, v2

    if-gez v1, :cond_0

    .line 487755
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr p1, v2

    .line 487756
    :cond_0
    iget-wide v2, v0, Lcom/facebook/android/maps/MapView;->r:J

    long-to-double v2, v2

    mul-double/2addr v2, p1

    iget v0, v0, Lcom/facebook/android/maps/MapView;->h:F

    float-to-double v0, v0

    mul-double/2addr v0, v2

    return-wide v0
.end method
