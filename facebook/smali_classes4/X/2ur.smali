.class public final enum LX/2ur;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2ur;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2ur;

.field public static final enum AKAMAI_CDN_CACHE_EDGE_HIT:LX/2ur;

.field public static final enum AKAMAI_CDN_CACHE_MIDGRESS_HIT:LX/2ur;

.field public static final enum AKAMAI_CDN_CACHE_MISS:LX/2ur;

.field public static final enum FB_CDN_CACHE_HIT:LX/2ur;

.field public static final enum FB_CDN_CACHE_MISS:LX/2ur;

.field public static final enum NOT_IN_GK:LX/2ur;

.field public static final enum NO_HEADER:LX/2ur;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 477063
    new-instance v0, LX/2ur;

    const-string v1, "FB_CDN_CACHE_HIT"

    invoke-direct {v0, v1, v3}, LX/2ur;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ur;->FB_CDN_CACHE_HIT:LX/2ur;

    .line 477064
    new-instance v0, LX/2ur;

    const-string v1, "FB_CDN_CACHE_MISS"

    invoke-direct {v0, v1, v4}, LX/2ur;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ur;->FB_CDN_CACHE_MISS:LX/2ur;

    .line 477065
    new-instance v0, LX/2ur;

    const-string v1, "AKAMAI_CDN_CACHE_EDGE_HIT"

    invoke-direct {v0, v1, v5}, LX/2ur;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ur;->AKAMAI_CDN_CACHE_EDGE_HIT:LX/2ur;

    .line 477066
    new-instance v0, LX/2ur;

    const-string v1, "AKAMAI_CDN_CACHE_MIDGRESS_HIT"

    invoke-direct {v0, v1, v6}, LX/2ur;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ur;->AKAMAI_CDN_CACHE_MIDGRESS_HIT:LX/2ur;

    .line 477067
    new-instance v0, LX/2ur;

    const-string v1, "AKAMAI_CDN_CACHE_MISS"

    invoke-direct {v0, v1, v7}, LX/2ur;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ur;->AKAMAI_CDN_CACHE_MISS:LX/2ur;

    .line 477068
    new-instance v0, LX/2ur;

    const-string v1, "NO_HEADER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/2ur;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ur;->NO_HEADER:LX/2ur;

    .line 477069
    new-instance v0, LX/2ur;

    const-string v1, "NOT_IN_GK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/2ur;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ur;->NOT_IN_GK:LX/2ur;

    .line 477070
    const/4 v0, 0x7

    new-array v0, v0, [LX/2ur;

    sget-object v1, LX/2ur;->FB_CDN_CACHE_HIT:LX/2ur;

    aput-object v1, v0, v3

    sget-object v1, LX/2ur;->FB_CDN_CACHE_MISS:LX/2ur;

    aput-object v1, v0, v4

    sget-object v1, LX/2ur;->AKAMAI_CDN_CACHE_EDGE_HIT:LX/2ur;

    aput-object v1, v0, v5

    sget-object v1, LX/2ur;->AKAMAI_CDN_CACHE_MIDGRESS_HIT:LX/2ur;

    aput-object v1, v0, v6

    sget-object v1, LX/2ur;->AKAMAI_CDN_CACHE_MISS:LX/2ur;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2ur;->NO_HEADER:LX/2ur;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2ur;->NOT_IN_GK:LX/2ur;

    aput-object v2, v0, v1

    sput-object v0, LX/2ur;->$VALUES:[LX/2ur;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 477062
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2ur;
    .locals 1

    .prologue
    .line 477061
    const-class v0, LX/2ur;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2ur;

    return-object v0
.end method

.method public static values()[LX/2ur;
    .locals 1

    .prologue
    .line 477060
    sget-object v0, LX/2ur;->$VALUES:[LX/2ur;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2ur;

    return-object v0
.end method
