.class public LX/2t1;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/2t2;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2t1;


# instance fields
.field private final a:LX/0Wd;

.field private b:Lcom/facebook/account/recovery/service/AccountRecoveryActivationsReceiverRegistration$ActivationRunnable;


# direct methods
.method public constructor <init>(LX/0Wd;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0Ot",
            "<",
            "LX/2t2;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 474491
    invoke-direct {p0, p2}, LX/0aT;-><init>(LX/0Ot;)V

    .line 474492
    iput-object p1, p0, LX/2t1;->a:LX/0Wd;

    .line 474493
    return-void
.end method

.method public static a(LX/0QB;)LX/2t1;
    .locals 5

    .prologue
    .line 474494
    sget-object v0, LX/2t1;->c:LX/2t1;

    if-nez v0, :cond_1

    .line 474495
    const-class v1, LX/2t1;

    monitor-enter v1

    .line 474496
    :try_start_0
    sget-object v0, LX/2t1;->c:LX/2t1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 474497
    if-eqz v2, :cond_0

    .line 474498
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 474499
    new-instance v4, LX/2t1;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v3

    check-cast v3, LX/0Wd;

    const/16 p0, 0x65

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/2t1;-><init>(LX/0Wd;LX/0Ot;)V

    .line 474500
    move-object v0, v4

    .line 474501
    sput-object v0, LX/2t1;->c:LX/2t1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 474502
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 474503
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 474504
    :cond_1
    sget-object v0, LX/2t1;->c:LX/2t1;

    return-object v0

    .line 474505
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 474506
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 474507
    check-cast p3, LX/2t2;

    .line 474508
    new-instance v0, Lcom/facebook/account/recovery/service/AccountRecoveryActivationsReceiverRegistration$ActivationRunnable;

    invoke-direct {v0, p3}, Lcom/facebook/account/recovery/service/AccountRecoveryActivationsReceiverRegistration$ActivationRunnable;-><init>(LX/2t2;)V

    iput-object v0, p0, LX/2t1;->b:Lcom/facebook/account/recovery/service/AccountRecoveryActivationsReceiverRegistration$ActivationRunnable;

    .line 474509
    iget-object v0, p0, LX/2t1;->a:LX/0Wd;

    iget-object v1, p0, LX/2t1;->b:Lcom/facebook/account/recovery/service/AccountRecoveryActivationsReceiverRegistration$ActivationRunnable;

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 474510
    return-void
.end method
