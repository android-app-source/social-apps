.class public LX/3Ao;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 526290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 35

    .prologue
    .line 526168
    const/16 v31, 0x0

    .line 526169
    const/16 v30, 0x0

    .line 526170
    const/16 v29, 0x0

    .line 526171
    const/16 v28, 0x0

    .line 526172
    const/16 v27, 0x0

    .line 526173
    const/16 v26, 0x0

    .line 526174
    const/16 v25, 0x0

    .line 526175
    const/16 v24, 0x0

    .line 526176
    const/16 v23, 0x0

    .line 526177
    const/16 v22, 0x0

    .line 526178
    const/16 v21, 0x0

    .line 526179
    const/16 v20, 0x0

    .line 526180
    const/16 v19, 0x0

    .line 526181
    const/16 v18, 0x0

    .line 526182
    const/16 v17, 0x0

    .line 526183
    const/16 v16, 0x0

    .line 526184
    const/4 v15, 0x0

    .line 526185
    const/4 v14, 0x0

    .line 526186
    const/4 v13, 0x0

    .line 526187
    const/4 v12, 0x0

    .line 526188
    const/4 v11, 0x0

    .line 526189
    const/4 v10, 0x0

    .line 526190
    const/4 v9, 0x0

    .line 526191
    const/4 v8, 0x0

    .line 526192
    const/4 v7, 0x0

    .line 526193
    const/4 v6, 0x0

    .line 526194
    const/4 v5, 0x0

    .line 526195
    const/4 v4, 0x0

    .line 526196
    const/4 v3, 0x0

    .line 526197
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_1

    .line 526198
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 526199
    const/4 v3, 0x0

    .line 526200
    :goto_0
    return v3

    .line 526201
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 526202
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_19

    .line 526203
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v32

    .line 526204
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 526205
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v33

    sget-object v34, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-eq v0, v1, :cond_1

    if-eqz v32, :cond_1

    .line 526206
    const-string v33, "all_icons"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_2

    .line 526207
    invoke-static/range {p0 .. p1}, LX/4Td;->a(LX/15w;LX/186;)I

    move-result v31

    goto :goto_1

    .line 526208
    :cond_2
    const-string v33, "glyph"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_3

    .line 526209
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v30

    goto :goto_1

    .line 526210
    :cond_3
    const-string v33, "iconImageLarge"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_4

    .line 526211
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v29

    goto :goto_1

    .line 526212
    :cond_4
    const-string v33, "id"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_5

    .line 526213
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto :goto_1

    .line 526214
    :cond_5
    const-string v33, "is_linking_verb"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_6

    .line 526215
    const/4 v7, 0x1

    .line 526216
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto :goto_1

    .line 526217
    :cond_6
    const-string v33, "legacy_api_id"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_7

    .line 526218
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    goto :goto_1

    .line 526219
    :cond_7
    const-string v33, "prefetch_priority"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_8

    .line 526220
    const/4 v6, 0x1

    .line 526221
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v25

    goto/16 :goto_1

    .line 526222
    :cond_8
    const-string v33, "present_participle"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_9

    .line 526223
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto/16 :goto_1

    .line 526224
    :cond_9
    const-string v33, "previewTemplateAtPlace"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_a

    .line 526225
    invoke-static/range {p0 .. p1}, LX/3Ap;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 526226
    :cond_a
    const-string v33, "previewTemplateNoTags"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_b

    .line 526227
    invoke-static/range {p0 .. p1}, LX/3Ap;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 526228
    :cond_b
    const-string v33, "previewTemplateWithMultipleUsers"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_c

    .line 526229
    invoke-static/range {p0 .. p1}, LX/3Ap;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 526230
    :cond_c
    const-string v33, "previewTemplateWithMultipleUsersAtPlace"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_d

    .line 526231
    invoke-static/range {p0 .. p1}, LX/3Ap;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 526232
    :cond_d
    const-string v33, "previewTemplateWithPeople"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_e

    .line 526233
    invoke-static/range {p0 .. p1}, LX/3Ap;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 526234
    :cond_e
    const-string v33, "previewTemplateWithPeopleAtPlace"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_f

    .line 526235
    invoke-static/range {p0 .. p1}, LX/3Ap;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 526236
    :cond_f
    const-string v33, "previewTemplateWithPerson"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_10

    .line 526237
    invoke-static/range {p0 .. p1}, LX/3Ap;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 526238
    :cond_10
    const-string v33, "previewTemplateWithPersonAtPlace"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_11

    .line 526239
    invoke-static/range {p0 .. p1}, LX/3Ap;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 526240
    :cond_11
    const-string v33, "previewTemplateWithUser"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_12

    .line 526241
    invoke-static/range {p0 .. p1}, LX/3Ap;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 526242
    :cond_12
    const-string v33, "previewTemplateWithUserAtPlace"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_13

    .line 526243
    invoke-static/range {p0 .. p1}, LX/3Ap;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 526244
    :cond_13
    const-string v33, "preview_template"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_14

    .line 526245
    invoke-static/range {p0 .. p1}, LX/3Ap;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 526246
    :cond_14
    const-string v33, "prompt"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_15

    .line 526247
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 526248
    :cond_15
    const-string v33, "supports_audio_suggestions"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_16

    .line 526249
    const/4 v5, 0x1

    .line 526250
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 526251
    :cond_16
    const-string v33, "supports_freeform"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_17

    .line 526252
    const/4 v4, 0x1

    .line 526253
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 526254
    :cond_17
    const-string v33, "supports_offline_posting"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_18

    .line 526255
    const/4 v3, 0x1

    .line 526256
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 526257
    :cond_18
    const-string v33, "url"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_0

    .line 526258
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 526259
    :cond_19
    const/16 v32, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 526260
    const/16 v32, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 526261
    const/16 v31, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 526262
    const/16 v30, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 526263
    const/16 v29, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 526264
    if-eqz v7, :cond_1a

    .line 526265
    const/4 v7, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 526266
    :cond_1a
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 526267
    if-eqz v6, :cond_1b

    .line 526268
    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v6, v1, v7}, LX/186;->a(III)V

    .line 526269
    :cond_1b
    const/16 v6, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 526270
    const/16 v6, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 526271
    const/16 v6, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 526272
    const/16 v6, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 526273
    const/16 v6, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 526274
    const/16 v6, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 526275
    const/16 v6, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 526276
    const/16 v6, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 526277
    const/16 v6, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 526278
    const/16 v6, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->b(II)V

    .line 526279
    const/16 v6, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->b(II)V

    .line 526280
    const/16 v6, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->b(II)V

    .line 526281
    const/16 v6, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 526282
    if-eqz v5, :cond_1c

    .line 526283
    const/16 v5, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->a(IZ)V

    .line 526284
    :cond_1c
    if-eqz v4, :cond_1d

    .line 526285
    const/16 v4, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 526286
    :cond_1d
    if-eqz v3, :cond_1e

    .line 526287
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 526288
    :cond_1e
    const/16 v3, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 526289
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 526063
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 526064
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 526065
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 526066
    invoke-static {p0, p1}, LX/3Ao;->a(LX/15w;LX/186;)I

    move-result v1

    .line 526067
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 526068
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 526069
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 526070
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526071
    if-eqz v0, :cond_0

    .line 526072
    const-string v1, "all_icons"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526073
    invoke-static {p0, v0, p2, p3}, LX/4Td;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526074
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526075
    if-eqz v0, :cond_1

    .line 526076
    const-string v1, "glyph"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526077
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 526078
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526079
    if-eqz v0, :cond_2

    .line 526080
    const-string v1, "iconImageLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526081
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 526082
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 526083
    if-eqz v0, :cond_3

    .line 526084
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526085
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 526086
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 526087
    if-eqz v0, :cond_4

    .line 526088
    const-string v1, "is_linking_verb"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526089
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 526090
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 526091
    if-eqz v0, :cond_5

    .line 526092
    const-string v1, "legacy_api_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526093
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 526094
    :cond_5
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 526095
    if-eqz v0, :cond_6

    .line 526096
    const-string v1, "prefetch_priority"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526097
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 526098
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 526099
    if-eqz v0, :cond_7

    .line 526100
    const-string v1, "present_participle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526101
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 526102
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526103
    if-eqz v0, :cond_8

    .line 526104
    const-string v1, "previewTemplateAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526105
    invoke-static {p0, v0, p2, p3}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526106
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526107
    if-eqz v0, :cond_9

    .line 526108
    const-string v1, "previewTemplateNoTags"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526109
    invoke-static {p0, v0, p2, p3}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526110
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526111
    if-eqz v0, :cond_a

    .line 526112
    const-string v1, "previewTemplateWithMultipleUsers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526113
    invoke-static {p0, v0, p2, p3}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526114
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526115
    if-eqz v0, :cond_b

    .line 526116
    const-string v1, "previewTemplateWithMultipleUsersAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526117
    invoke-static {p0, v0, p2, p3}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526118
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526119
    if-eqz v0, :cond_c

    .line 526120
    const-string v1, "previewTemplateWithPeople"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526121
    invoke-static {p0, v0, p2, p3}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526122
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526123
    if-eqz v0, :cond_d

    .line 526124
    const-string v1, "previewTemplateWithPeopleAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526125
    invoke-static {p0, v0, p2, p3}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526126
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526127
    if-eqz v0, :cond_e

    .line 526128
    const-string v1, "previewTemplateWithPerson"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526129
    invoke-static {p0, v0, p2, p3}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526130
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526131
    if-eqz v0, :cond_f

    .line 526132
    const-string v1, "previewTemplateWithPersonAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526133
    invoke-static {p0, v0, p2, p3}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526134
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526135
    if-eqz v0, :cond_10

    .line 526136
    const-string v1, "previewTemplateWithUser"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526137
    invoke-static {p0, v0, p2, p3}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526138
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526139
    if-eqz v0, :cond_11

    .line 526140
    const-string v1, "previewTemplateWithUserAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526141
    invoke-static {p0, v0, p2, p3}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526142
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 526143
    if-eqz v0, :cond_12

    .line 526144
    const-string v1, "preview_template"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526145
    invoke-static {p0, v0, p2, p3}, LX/3Ap;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 526146
    :cond_12
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 526147
    if-eqz v0, :cond_13

    .line 526148
    const-string v1, "prompt"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526149
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 526150
    :cond_13
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 526151
    if-eqz v0, :cond_14

    .line 526152
    const-string v1, "supports_audio_suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526153
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 526154
    :cond_14
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 526155
    if-eqz v0, :cond_15

    .line 526156
    const-string v1, "supports_freeform"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526157
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 526158
    :cond_15
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 526159
    if-eqz v0, :cond_16

    .line 526160
    const-string v1, "supports_offline_posting"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526161
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 526162
    :cond_16
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 526163
    if-eqz v0, :cond_17

    .line 526164
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 526165
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 526166
    :cond_17
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 526167
    return-void
.end method
