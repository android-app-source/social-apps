.class public LX/31U;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/31U;

.field public static final B:LX/31U;

.field public static final C:LX/31U;

.field public static final D:LX/31U;

.field private static E:LX/3BR;

.field private static final F:Ljava/util/Random;

.field public static final a:LX/31U;

.field public static final b:LX/31U;

.field public static final c:LX/31U;

.field public static final d:LX/31U;

.field public static final e:LX/31U;

.field public static final f:LX/31U;

.field public static final g:LX/31U;

.field public static final h:LX/31U;

.field public static final i:LX/31U;

.field public static final j:LX/31U;

.field public static final k:[LX/31U;

.field public static final l:LX/31U;

.field public static final m:LX/31U;

.field public static final n:LX/31U;

.field public static final o:LX/31U;

.field public static final p:LX/31U;

.field public static final q:LX/31U;

.field public static final r:LX/31U;

.field public static final s:LX/31U;

.field public static final t:LX/31U;

.field public static final u:LX/31U;

.field public static final v:LX/31U;

.field public static final w:LX/31U;

.field public static final x:LX/31U;

.field public static final y:LX/31U;

.field public static final z:LX/31U;


# instance fields
.field private final G:Z

.field private final H:Ljava/lang/String;

.field private I:I

.field private J:[J

.field private K:J

.field private L:J

.field private M:J


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 487527
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_draw_time_ns"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->a:LX/31U;

    .line 487528
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_layout_time_ns"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->b:LX/31U;

    .line 487529
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_touch_event_time_ns"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->c:LX/31U;

    .line 487530
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_tile_download_time_ns"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->d:LX/31U;

    .line 487531
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_tile_download_size"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->e:LX/31U;

    .line 487532
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_tree_compaction_time"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->f:LX/31U;

    .line 487533
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_marker_draw_time"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->g:LX/31U;

    .line 487534
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_marker_touch_detection_time"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->h:LX/31U;

    .line 487535
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_info_window_draw_time"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->i:LX/31U;

    .line 487536
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_tile_overlay_draw_time_ns"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->j:LX/31U;

    .line 487537
    const/16 v0, 0x8

    new-array v0, v0, [LX/31U;

    sget-object v1, LX/31U;->a:LX/31U;

    aput-object v1, v0, v3

    sget-object v1, LX/31U;->b:LX/31U;

    aput-object v1, v0, v4

    const/4 v1, 0x2

    sget-object v2, LX/31U;->d:LX/31U;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/31U;->e:LX/31U;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/31U;->f:LX/31U;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/31U;->g:LX/31U;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/31U;->h:LX/31U;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/31U;->i:LX/31U;

    aput-object v2, v0, v1

    sput-object v0, LX/31U;->k:[LX/31U;

    .line 487538
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_bitmap_reuse_error"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->l:LX/31U;

    .line 487539
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_disk_cache_null_key_error"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->m:LX/31U;

    .line 487540
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_disk_cache_write_error"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->n:LX/31U;

    .line 487541
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_disk_cache_read_error"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->o:LX/31U;

    .line 487542
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_disk_cache_init_error"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->p:LX/31U;

    .line 487543
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_config_fetch_error"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->q:LX/31U;

    .line 487544
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_tile_download_error"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->r:LX/31U;

    .line 487545
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_empty_cluster_error"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->s:LX/31U;

    .line 487546
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_static_map_image_download_error"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->t:LX/31U;

    .line 487547
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_suppressed_drain_to_byte_array_error"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->u:LX/31U;

    .line 487548
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_runtime_permission_error"

    invoke-direct {v0, v1, v4}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->v:LX/31U;

    .line 487549
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_static_map_report_button_clicked"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->w:LX/31U;

    .line 487550
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_default_reporter_dialog_clicked"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->x:LX/31U;

    .line 487551
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_static_map_view_impression"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->y:LX/31U;

    .line 487552
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_dynamic_map_view_impression"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->z:LX/31U;

    .line 487553
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_dynamic_map_warm_tti_ns"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->A:LX/31U;

    .line 487554
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_dynamic_map_cold_tti_ns"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->B:LX/31U;

    .line 487555
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_static_map_render_time_ns"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->C:LX/31U;

    .line 487556
    new-instance v0, LX/31U;

    const-string v1, "oxygen_map_dynamic_map_stats_per_impression"

    invoke-direct {v0, v1, v3}, LX/31U;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LX/31U;->D:LX/31U;

    .line 487557
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, LX/31U;->F:Ljava/util/Random;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 487519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487520
    const/4 v0, 0x0

    iput v0, p0, LX/31U;->I:I

    .line 487521
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/31U;->K:J

    .line 487522
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/31U;->L:J

    .line 487523
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/31U;->M:J

    .line 487524
    iput-object p1, p0, LX/31U;->H:Ljava/lang/String;

    .line 487525
    iput-boolean p2, p0, LX/31U;->G:Z

    .line 487526
    return-void
.end method

.method public static a()J
    .locals 2

    .prologue
    .line 487518
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public static declared-synchronized a(LX/3BR;)V
    .locals 2

    .prologue
    .line 487514
    const-class v1, LX/31U;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/31U;->E:LX/3BR;

    if-eq v0, p0, :cond_0

    .line 487515
    sput-object p0, LX/31U;->E:LX/3BR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 487516
    :cond_0
    monitor-exit v1

    return-void

    .line 487517
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 487507
    iget-boolean v0, p0, LX/31U;->G:Z

    if-eqz v0, :cond_0

    .line 487508
    monitor-enter p0

    .line 487509
    :try_start_0
    invoke-direct {p0, p1, p2}, LX/31U;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 487510
    monitor-exit p0

    .line 487511
    :goto_0
    return-void

    .line 487512
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 487513
    :cond_0
    invoke-direct {p0, p1, p2}, LX/31U;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a([JI)[J
    .locals 14

    .prologue
    const/4 v1, 0x0

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    const/4 v10, 0x5

    .line 487497
    new-array v0, v10, [J

    .line 487498
    if-gtz p1, :cond_1

    .line 487499
    :cond_0
    return-object v0

    .line 487500
    :cond_1
    invoke-static {p0, p1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v2

    .line 487501
    invoke-static {v2}, Ljava/util/Arrays;->sort([J)V

    .line 487502
    add-int/lit8 v3, p1, -0x1

    .line 487503
    new-array v4, v10, [I

    shr-int/lit8 v5, v3, 0x2

    aput v5, v4, v1

    const/4 v5, 0x1

    shr-int/lit8 v6, v3, 0x1

    aput v6, v4, v5

    const/4 v5, 0x2

    shr-int/lit8 v6, v3, 0x2

    sub-int v6, v3, v6

    aput v6, v4, v5

    const/4 v5, 0x3

    int-to-double v6, v3

    const-wide v8, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v6, v8

    add-double/2addr v6, v12

    double-to-int v6, v6

    aput v6, v4, v5

    const/4 v5, 0x4

    int-to-double v6, v3

    const-wide v8, 0x3fefae147ae147aeL    # 0.99

    mul-double/2addr v6, v8

    add-double/2addr v6, v12

    double-to-int v3, v6

    aput v3, v4, v5

    .line 487504
    :goto_0
    if-ge v1, v10, :cond_0

    .line 487505
    aget v3, v4, v1

    aget-wide v6, v2, v3

    aput-wide v6, v0, v1

    .line 487506
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private b(J)V
    .locals 3

    .prologue
    const/16 v2, 0x100

    .line 487437
    iget-object v0, p0, LX/31U;->J:[J

    if-nez v0, :cond_0

    .line 487438
    new-array v0, v2, [J

    iput-object v0, p0, LX/31U;->J:[J

    .line 487439
    :cond_0
    iget-wide v0, p0, LX/31U;->K:J

    cmp-long v0, v0, p1

    if-lez v0, :cond_3

    iget-wide v0, p0, LX/31U;->K:J

    :goto_0
    iput-wide v0, p0, LX/31U;->K:J

    .line 487440
    iget-wide v0, p0, LX/31U;->L:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_4

    iget-wide v0, p0, LX/31U;->L:J

    :goto_1
    iput-wide v0, p0, LX/31U;->L:J

    .line 487441
    iget-wide v0, p0, LX/31U;->M:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/31U;->M:J

    .line 487442
    iget v0, p0, LX/31U;->I:I

    if-ge v0, v2, :cond_5

    .line 487443
    iget-object v0, p0, LX/31U;->J:[J

    iget v1, p0, LX/31U;->I:I

    aput-wide p1, v0, v1

    .line 487444
    :cond_1
    :goto_2
    iget v0, p0, LX/31U;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/31U;->I:I

    .line 487445
    iget v0, p0, LX/31U;->I:I

    if-ne v0, v2, :cond_2

    .line 487446
    invoke-static {p0}, LX/31U;->e(LX/31U;)V

    .line 487447
    :cond_2
    return-void

    :cond_3
    move-wide v0, p1

    .line 487448
    goto :goto_0

    :cond_4
    move-wide v0, p1

    .line 487449
    goto :goto_1

    .line 487450
    :cond_5
    sget-object v0, LX/31U;->F:Ljava/util/Random;

    iget v1, p0, LX/31U;->I:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 487451
    if-ge v0, v2, :cond_1

    .line 487452
    iget-object v1, p0, LX/31U;->J:[J

    aput-wide p1, v1, v0

    goto :goto_2
.end method

.method private b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 487491
    sget-object v0, LX/31U;->E:LX/3BR;

    if-eqz v0, :cond_0

    .line 487492
    sget-object v0, LX/31U;->E:LX/3BR;

    iget-object v1, p0, LX/31U;->H:Ljava/lang/String;

    .line 487493
    if-nez p2, :cond_1

    .line 487494
    iget-object p0, v0, LX/3BR;->c:LX/03V;

    invoke-virtual {p0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 487495
    :cond_0
    :goto_0
    return-void

    .line 487496
    :cond_1
    iget-object p0, v0, LX/3BR;->c:LX/03V;

    invoke-virtual {p0, v1, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static e(LX/31U;)V
    .locals 1

    .prologue
    .line 487484
    iget-boolean v0, p0, LX/31U;->G:Z

    if-eqz v0, :cond_0

    .line 487485
    monitor-enter p0

    .line 487486
    :try_start_0
    invoke-direct {p0}, LX/31U;->f()V

    .line 487487
    monitor-exit p0

    .line 487488
    :goto_0
    return-void

    .line 487489
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 487490
    :cond_0
    invoke-direct {p0}, LX/31U;->f()V

    goto :goto_0
.end method

.method private f()V
    .locals 10

    .prologue
    const/16 v0, 0x100

    .line 487474
    sget-object v1, LX/31U;->E:LX/3BR;

    if-eqz v1, :cond_0

    .line 487475
    iget v1, p0, LX/31U;->I:I

    if-ge v1, v0, :cond_1

    iget v0, p0, LX/31U;->I:I

    move v2, v0

    .line 487476
    :goto_0
    if-lez v2, :cond_0

    .line 487477
    sget-object v0, LX/31U;->E:LX/3BR;

    iget-object v1, p0, LX/31U;->H:Ljava/lang/String;

    iget-object v3, p0, LX/31U;->J:[J

    invoke-static {v3, v2}, LX/31U;->a([JI)[J

    move-result-object v2

    iget v3, p0, LX/31U;->I:I

    iget-wide v4, p0, LX/31U;->K:J

    iget-wide v6, p0, LX/31U;->L:J

    iget-wide v8, p0, LX/31U;->M:J

    invoke-virtual/range {v0 .. v9}, LX/3BR;->a(Ljava/lang/String;[JIJJJ)V

    .line 487478
    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/31U;->K:J

    .line 487479
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/31U;->L:J

    .line 487480
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/31U;->M:J

    .line 487481
    const/4 v0, 0x0

    iput v0, p0, LX/31U;->I:I

    .line 487482
    return-void

    :cond_1
    move v2, v0

    .line 487483
    goto :goto_0
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 487467
    iget-boolean v0, p0, LX/31U;->G:Z

    if-eqz v0, :cond_0

    .line 487468
    monitor-enter p0

    .line 487469
    :try_start_0
    invoke-direct {p0, p1, p2}, LX/31U;->b(J)V

    .line 487470
    monitor-exit p0

    .line 487471
    :goto_0
    return-void

    .line 487472
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 487473
    :cond_0
    invoke-direct {p0, p1, p2}, LX/31U;->b(J)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 487465
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/31U;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 487466
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 487463
    const-string v0, ""

    invoke-direct {p0, v0, p1}, LX/31U;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 487464
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 487456
    sget-object v0, LX/31U;->E:LX/3BR;

    if-eqz v0, :cond_0

    .line 487457
    sget-object v0, LX/31U;->E:LX/3BR;

    iget-object v1, p0, LX/31U;->H:Ljava/lang/String;

    .line 487458
    iget-object v2, v0, LX/3BR;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Zb;

    const/4 p0, 0x0

    invoke-interface {v2, v1, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 487459
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 487460
    const-string p0, "oxygen_map"

    invoke-virtual {v2, p0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p0

    invoke-virtual {p0, p1}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 487461
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 487462
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 487453
    sget-object v0, LX/31U;->E:LX/3BR;

    if-eqz v0, :cond_0

    sget-object v0, LX/31U;->E:LX/3BR;

    iget-object v1, p0, LX/31U;->H:Ljava/lang/String;

    .line 487454
    iget-object p0, v0, LX/3BR;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0Zm;

    invoke-virtual {p0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result p0

    move v0, p0

    .line 487455
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
