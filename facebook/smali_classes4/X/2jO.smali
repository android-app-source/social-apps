.class public LX/2jO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/2jO;


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field public final b:LX/03V;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/1rx;

.field public final e:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field public final f:Ljava/lang/Runnable;

.field public final g:Landroid/os/Handler;

.field public h:Ljava/lang/Runnable;

.field public i:LX/DqC;

.field public j:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1rx;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 452838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452839
    new-instance v0, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$1;

    invoke-direct {v0, p0}, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$1;-><init>(LX/2jO;)V

    iput-object v0, p0, LX/2jO;->f:Ljava/lang/Runnable;

    .line 452840
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/2jO;->g:Landroid/os/Handler;

    .line 452841
    iput-object p1, p0, LX/2jO;->a:Ljava/util/concurrent/ExecutorService;

    .line 452842
    iput-object p2, p0, LX/2jO;->b:LX/03V;

    .line 452843
    iput-object p3, p0, LX/2jO;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 452844
    iput-object p4, p0, LX/2jO;->e:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 452845
    iput-object p5, p0, LX/2jO;->d:LX/1rx;

    .line 452846
    return-void
.end method

.method public static a(LX/0QB;)LX/2jO;
    .locals 9

    .prologue
    .line 452847
    sget-object v0, LX/2jO;->k:LX/2jO;

    if-nez v0, :cond_1

    .line 452848
    const-class v1, LX/2jO;

    monitor-enter v1

    .line 452849
    :try_start_0
    sget-object v0, LX/2jO;->k:LX/2jO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 452850
    if-eqz v2, :cond_0

    .line 452851
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 452852
    new-instance v3, LX/2jO;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v7

    check-cast v7, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static {v0}, LX/1rx;->b(LX/0QB;)LX/1rx;

    move-result-object v8

    check-cast v8, LX/1rx;

    invoke-direct/range {v3 .. v8}, LX/2jO;-><init>(Ljava/util/concurrent/ExecutorService;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1rx;)V

    .line 452853
    move-object v0, v3

    .line 452854
    sput-object v0, LX/2jO;->k:LX/2jO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 452855
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 452856
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 452857
    :cond_1
    sget-object v0, LX/2jO;->k:LX/2jO;

    return-object v0

    .line 452858
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 452859
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/3D3;
    .locals 1

    .prologue
    .line 452860
    new-instance v0, LX/3D3;

    invoke-direct {v0, p0, p1, p2}, LX/3D3;-><init>(LX/2jO;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 452861
    const/4 v0, 0x0

    iput-object v0, p0, LX/2jO;->i:LX/DqC;

    .line 452862
    return-void
.end method

.method public final a(LX/DqC;)V
    .locals 3
    .param p1    # LX/DqC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 452863
    if-nez p1, :cond_1

    iget-object v0, p0, LX/2jO;->i:LX/DqC;

    if-nez v0, :cond_1

    .line 452864
    :cond_0
    :goto_0
    return-void

    .line 452865
    :cond_1
    iget-object v0, p0, LX/2jO;->i:LX/DqC;

    if-eqz v0, :cond_2

    .line 452866
    iget-object v0, p0, LX/2jO;->j:LX/2kW;

    if-eqz v0, :cond_3

    .line 452867
    iget-object v0, p0, LX/2jO;->i:LX/DqC;

    .line 452868
    iget-object v1, v0, LX/DqC;->b:Ljava/lang/String;

    move-object v0, v1

    .line 452869
    iget-object v1, p0, LX/2jO;->j:LX/2kW;

    new-instance v2, LX/DqB;

    invoke-direct {v2, p0, v0}, LX/DqB;-><init>(LX/2jO;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v0}, LX/2kW;->a(LX/0Rl;Ljava/lang/String;)V

    .line 452870
    iput-object p1, p0, LX/2jO;->i:LX/DqC;

    .line 452871
    :goto_1
    iget-object v0, p0, LX/2jO;->h:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2jO;->j:LX/2kW;

    if-eqz v0, :cond_0

    .line 452872
    iget-object v0, p0, LX/2jO;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 452873
    :cond_2
    iput-object p1, p0, LX/2jO;->i:LX/DqC;

    .line 452874
    iget-object v0, p0, LX/2jO;->h:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 452875
    iget-object v0, p0, LX/2jO;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 452876
    :cond_3
    iget-object v0, p0, LX/2jO;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;-><init>(LX/2jO;LX/DqC;)V

    const v2, -0x22f20ce8

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_1
.end method
