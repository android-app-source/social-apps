.class public final LX/2nn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/controller/connectioncontroller/store/diskstore/ConnectionCache",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2ng;

.field public volatile b:Z

.field private c:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "SimpleConnectionCache.this"
    .end annotation
.end field

.field private d:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "SimpleConnectionCache.this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2ng;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 465079
    iput-object p1, p0, LX/2nn;->a:LX/2ng;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 465080
    iput-boolean v0, p0, LX/2nn;->b:Z

    .line 465081
    iput-boolean v0, p0, LX/2nn;->c:Z

    .line 465082
    iput-boolean v0, p0, LX/2nn;->d:Z

    return-void
.end method


# virtual methods
.method public final a(I)Ljava/util/ArrayList;
    .locals 5
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 465083
    iget-object v2, p0, LX/2nn;->a:LX/2ng;

    iget-object v2, v2, LX/2ng;->b:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 465084
    monitor-enter p0

    .line 465085
    :try_start_0
    iget-object v2, p0, LX/2nn;->a:LX/2ng;

    iget-object v2, v2, LX/2ng;->d:LX/2nf;

    invoke-interface {v2}, LX/2nf;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, LX/2nn;->c:Z

    if-eqz v2, :cond_1

    .line 465086
    :cond_0
    monitor-exit p0

    move-object v0, v1

    .line 465087
    :goto_0
    return-object v0

    .line 465088
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/2nn;->c:Z

    .line 465089
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 465090
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, LX/2nn;->a:LX/2ng;

    iget-object v3, v3, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 465091
    iget-object v3, p0, LX/2nn;->a:LX/2ng;

    iget-object v3, v3, LX/2ng;->d:LX/2nf;

    iget-object v4, p0, LX/2nn;->a:LX/2ng;

    iget-object v4, v4, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-interface {v3, v4}, LX/2nf;->moveToPosition(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 465092
    :cond_2
    iget-object v3, p0, LX/2nn;->a:LX/2ng;

    iget-object v3, v3, LX/2ng;->d:LX/2nf;

    invoke-interface {v3}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    .line 465093
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465094
    add-int/lit8 v0, v0, 0x1

    .line 465095
    iget-object v3, p0, LX/2nn;->a:LX/2ng;

    iget-object v3, v3, LX/2ng;->d:LX/2nf;

    invoke-interface {v3}, LX/2nf;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    if-lt v0, p1, :cond_2

    .line 465096
    :cond_3
    invoke-virtual {p0}, LX/2nn;->e()V

    .line 465097
    :cond_4
    monitor-enter p0

    .line 465098
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, LX/2nn;->c:Z

    .line 465099
    iget-boolean v0, p0, LX/2nn;->d:Z

    if-eqz v0, :cond_5

    .line 465100
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2nn;->d:Z

    .line 465101
    iget-object v0, p0, LX/2nn;->a:LX/2ng;

    iget-object v0, v0, LX/2ng;->d:LX/2nf;

    invoke-interface {v0}, LX/2nf;->close()V

    .line 465102
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v1

    goto :goto_0

    .line 465103
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 465104
    :cond_5
    :try_start_3
    monitor-exit p0

    move-object v0, v2

    .line 465105
    goto :goto_0

    .line 465106
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 465107
    monitor-enter p0

    .line 465108
    :try_start_0
    iget-boolean v0, p0, LX/2nn;->c:Z

    if-eqz v0, :cond_0

    .line 465109
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2nn;->d:Z

    .line 465110
    :goto_0
    monitor-exit p0

    return-void

    .line 465111
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2nn;->d:Z

    .line 465112
    iget-object v0, p0, LX/2nn;->a:LX/2ng;

    iget-object v0, v0, LX/2ng;->d:LX/2nf;

    invoke-interface {v0}, LX/2nf;->close()V

    goto :goto_0

    .line 465113
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 465114
    monitor-enter p0

    .line 465115
    :try_start_0
    iget-boolean v0, p0, LX/2nn;->d:Z

    monitor-exit p0

    return v0

    .line 465116
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 465117
    iget-object v0, p0, LX/2nn;->a:LX/2ng;

    iget-object v0, v0, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 465118
    :cond_0
    :goto_0
    return-void

    .line 465119
    :cond_1
    iget-object v0, p0, LX/2nn;->a:LX/2ng;

    iget-object v0, v0, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/16i;

    if-eqz v0, :cond_0

    .line 465120
    new-instance v3, Ljava/util/HashMap;

    iget-object v0, p0, LX/2nn;->a:LX/2ng;

    iget-object v0, v0, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(I)V

    move v1, v2

    .line 465121
    :goto_1
    iget-object v0, p0, LX/2nn;->a:LX/2ng;

    iget-object v0, v0, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 465122
    iget-object v0, p0, LX/2nn;->a:LX/2ng;

    iget-object v0, v0, LX/2ng;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 465123
    instance-of v4, v0, LX/16i;

    if-eqz v4, :cond_3

    .line 465124
    check-cast v0, LX/16i;

    invoke-interface {v0}, LX/16i;->a()Ljava/lang/String;

    move-result-object v0

    .line 465125
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 465126
    const-string v4, "BatchCursorConnectionState"

    const-string v5, "Item at row %d has the same key %s as item at row %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v2

    const/4 v1, 0x1

    aput-object v0, v6, v1

    const/4 v1, 0x2

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v4, v5, v6}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 465127
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465128
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
