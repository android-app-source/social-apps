.class public final LX/2FL;
.super Landroid/content/BroadcastReceiver;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VA",
        "L:Ljava/lang/Object;",
        ">",
        "Landroid/content/BroadcastReceiver;"
    }
.end annotation


# instance fields
.field private final a:LX/0py;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0py",
            "<TVA",
            "L;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0pw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0pw",
            "<TVA",
            "L;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/os/Looper;


# direct methods
.method public constructor <init>(LX/0py;LX/0pw;Landroid/os/Looper;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0py",
            "<TVA",
            "L;",
            ">;",
            "LX/0pw",
            "<TVA",
            "L;",
            ">;",
            "Landroid/os/Looper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 386827
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 386828
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0py;

    iput-object v0, p0, LX/2FL;->a:LX/0py;

    .line 386829
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pw;

    iput-object v0, p0, LX/2FL;->b:LX/0pw;

    .line 386830
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, LX/2FL;->c:Landroid/os/Looper;

    .line 386831
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x15ef76dd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 386832
    iget-object v1, p0, LX/2FL;->b:LX/0pw;

    iget-object v2, p0, LX/2FL;->c:Landroid/os/Looper;

    invoke-virtual {v1, v2}, LX/0pw;->a(Landroid/os/Looper;)Ljava/util/Collection;

    move-result-object v1

    .line 386833
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 386834
    :cond_0
    const/16 v1, 0x27

    const v2, -0x239323de

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 386835
    :goto_0
    return-void

    .line 386836
    :cond_1
    iget-object v2, p0, LX/2FL;->a:LX/0py;

    invoke-interface {v2, v1, p2}, LX/0py;->a(Ljava/util/Collection;Landroid/content/Intent;)V

    .line 386837
    const v1, 0x6b84ca2b

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method
