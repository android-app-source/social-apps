.class public final LX/2E9;
.super LX/2EA;
.source ""


# instance fields
.field public i:J

.field public j:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0}, LX/2EA;-><init>()V

    iput-wide v0, p0, LX/2E9;->i:J

    iput-wide v0, p0, LX/2E9;->j:J

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2E9;->e:Z

    return-void
.end method


# virtual methods
.method public final a(JJ)LX/2E9;
    .locals 1

    iput-wide p1, p0, LX/2E9;->i:J

    iput-wide p3, p0, LX/2E9;->j:J

    return-object p0
.end method

.method public final a(Ljava/lang/Class;)LX/2E9;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/2fD;",
            ">;)",
            "LX/2E9;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2E9;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final a()V
    .locals 4

    const-wide/16 v2, -0x1

    invoke-super {p0}, LX/2EA;->a()V

    iget-wide v0, p0, LX/2E9;->i:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/2E9;->j:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must specify an execution window using setExecutionWindow."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-wide v0, p0, LX/2E9;->i:J

    iget-wide v2, p0, LX/2E9;->j:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Window start must be shorter than window end."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method

.method public final synthetic b(I)LX/2EA;
    .locals 1

    iput p1, p0, LX/2EA;->a:I

    move-object v0, p0

    return-object v0
.end method

.method public final synthetic b(Landroid/os/Bundle;)LX/2EA;
    .locals 1

    iput-object p1, p0, LX/2EA;->h:Landroid/os/Bundle;

    move-object v0, p0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Class;)LX/2EA;
    .locals 1

    invoke-virtual {p0, p1}, LX/2E9;->a(Ljava/lang/Class;)LX/2E9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/String;)LX/2EA;
    .locals 1

    iput-object p1, p0, LX/2EA;->c:Ljava/lang/String;

    move-object v0, p0

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/gcm/OneoffTask;
    .locals 2

    invoke-virtual {p0}, LX/2E9;->a()V

    new-instance v0, Lcom/google/android/gms/gcm/OneoffTask;

    invoke-direct {v0, p0}, Lcom/google/android/gms/gcm/OneoffTask;-><init>(LX/2E9;)V

    return-object v0
.end method

.method public final synthetic c()Lcom/google/android/gms/gcm/Task;
    .locals 1

    invoke-virtual {p0}, LX/2E9;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Z)LX/2EA;
    .locals 1

    iput-boolean p1, p0, LX/2EA;->d:Z

    move-object v0, p0

    return-object v0
.end method

.method public final synthetic e(Z)LX/2EA;
    .locals 1

    iput-boolean p1, p0, LX/2EA;->f:Z

    move-object v0, p0

    return-object v0
.end method
