.class public LX/2FT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/2FU;

.field private final c:LX/0tX;

.field private final d:Landroid/content/Context;

.field public final e:LX/2FW;

.field public final f:LX/0SG;

.field private final g:LX/2FX;

.field private h:LX/0Yd;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 386914
    const-class v0, LX/2FT;

    sput-object v0, LX/2FT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2FU;LX/0tX;Landroid/content/Context;LX/2FW;LX/0SG;LX/2FX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 386959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386960
    iput-object p1, p0, LX/2FT;->b:LX/2FU;

    .line 386961
    iput-object p2, p0, LX/2FT;->c:LX/0tX;

    .line 386962
    iput-object p3, p0, LX/2FT;->d:Landroid/content/Context;

    .line 386963
    iput-object p4, p0, LX/2FT;->e:LX/2FW;

    .line 386964
    iput-object p5, p0, LX/2FT;->f:LX/0SG;

    .line 386965
    iput-object p6, p0, LX/2FT;->g:LX/2FX;

    .line 386966
    return-void
.end method

.method private static a(LX/2FT;)Z
    .locals 1

    .prologue
    .line 386958
    iget-object v0, p0, LX/2FT;->h:LX/0Yd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/2FT;LX/GRu;)V
    .locals 4

    .prologue
    .line 386949
    new-instance v0, LX/4Cr;

    invoke-direct {v0}, LX/4Cr;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p1, LX/GRu;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 386950
    const-string v2, "request_ids"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 386951
    move-object v0, v0

    .line 386952
    new-instance v1, LX/GS0;

    invoke-direct {v1}, LX/GS0;-><init>()V

    move-object v1, v1

    .line 386953
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/GS0;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 386954
    iget-object v1, p0, LX/2FT;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 386955
    iget-object v0, p1, LX/GRu;->b:Ljava/lang/String;

    invoke-static {p0, v0}, LX/2FT;->a$redex0(LX/2FT;Ljava/lang/String;)V

    .line 386956
    iget-object v0, p0, LX/2FT;->g:LX/2FX;

    new-instance v1, LX/GRd;

    iget-object v2, p1, LX/GRu;->b:Ljava/lang/String;

    invoke-direct {v1, v2}, LX/GRd;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 386957
    return-void
.end method

.method public static a$redex0(LX/2FT;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 386944
    iget-object v0, p0, LX/2FT;->e:LX/2FW;

    .line 386945
    iget-object v1, v0, LX/2FW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p0

    .line 386946
    sget-object v1, LX/2FY;->b:LX/0Tn;

    invoke-virtual {v1, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-interface {p0, v1}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    .line 386947
    invoke-interface {p0}, LX/0hN;->commit()V

    .line 386948
    return-void
.end method

.method public static b(LX/0QB;)LX/2FT;
    .locals 7

    .prologue
    .line 386939
    new-instance v0, LX/2FT;

    invoke-static {p0}, LX/2FU;->a(LX/0QB;)LX/2FU;

    move-result-object v1

    check-cast v1, LX/2FU;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 386940
    new-instance v5, LX/2FW;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v5, v4}, LX/2FW;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 386941
    move-object v4, v5

    .line 386942
    check-cast v4, LX/2FW;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {p0}, LX/2FX;->a(LX/0QB;)LX/2FX;

    move-result-object v6

    check-cast v6, LX/2FX;

    invoke-direct/range {v0 .. v6}, LX/2FT;-><init>(LX/2FU;LX/0tX;Landroid/content/Context;LX/2FW;LX/0SG;LX/2FX;)V

    .line 386943
    return-object v0
.end method

.method private static b(LX/2FT;)V
    .locals 3

    .prologue
    .line 386933
    new-instance v0, LX/0Yd;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    new-instance v2, LX/GRt;

    invoke-direct {v2, p0}, LX/GRt;-><init>(LX/2FT;)V

    invoke-direct {v0, v1, v2}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    iput-object v0, p0, LX/2FT;->h:LX/0Yd;

    .line 386934
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 386935
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 386936
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 386937
    iget-object v1, p0, LX/2FT;->d:Landroid/content/Context;

    iget-object v2, p0, LX/2FT;->h:LX/0Yd;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 386938
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 386924
    iget-object v0, p0, LX/2FT;->e:LX/2FW;

    new-instance v1, LX/GRu;

    iget-object v2, p0, LX/2FT;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-direct {v1, p1, p2, v2, v3}, LX/GRu;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 386925
    iget-object v4, v0, LX/2FW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    .line 386926
    sget-object v4, LX/2FY;->b:LX/0Tn;

    iget-object v5, v1, LX/GRu;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v4

    check-cast v4, LX/0Tn;

    const-string v5, "/"

    invoke-virtual {v4, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v4

    check-cast v4, LX/0Tn;

    .line 386927
    const-string v5, "invite_id"

    invoke-virtual {v4, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v5

    check-cast v5, LX/0Tn;

    iget-object v7, v1, LX/GRu;->a:Ljava/lang/String;

    invoke-interface {v6, v5, v7}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 386928
    const-string v5, "timestamp"

    invoke-virtual {v4, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v4

    check-cast v4, LX/0Tn;

    iget-wide v8, v1, LX/GRu;->c:J

    invoke-interface {v6, v4, v8, v9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 386929
    invoke-interface {v6}, LX/0hN;->commit()V

    .line 386930
    invoke-static {p0}, LX/2FT;->a(LX/2FT;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 386931
    invoke-static {p0}, LX/2FT;->b(LX/2FT;)V

    .line 386932
    :cond_0
    return-void
.end method

.method public final init()V
    .locals 6

    .prologue
    .line 386915
    iget-object v0, p0, LX/2FT;->e:LX/2FW;

    invoke-virtual {v0}, LX/2FW;->a()Ljava/util/Set;

    move-result-object v0

    .line 386916
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GRu;

    .line 386917
    iget-object v2, p0, LX/2FT;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, v0, LX/GRu;->c:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 386918
    iget-object v0, v0, LX/GRu;->b:Ljava/lang/String;

    invoke-static {p0, v0}, LX/2FT;->a$redex0(LX/2FT;Ljava/lang/String;)V

    goto :goto_0

    .line 386919
    :cond_1
    iget-object v2, p0, LX/2FT;->b:LX/2FU;

    iget-object v3, v0, LX/GRu;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2FU;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 386920
    invoke-static {p0, v0}, LX/2FT;->a$redex0(LX/2FT;LX/GRu;)V

    goto :goto_0

    .line 386921
    :cond_2
    iget-object v0, p0, LX/2FT;->e:LX/2FW;

    invoke-virtual {v0}, LX/2FW;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, LX/2FT;->a(LX/2FT;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 386922
    invoke-static {p0}, LX/2FT;->b(LX/2FT;)V

    .line 386923
    :cond_3
    return-void
.end method
