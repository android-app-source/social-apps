.class public LX/2ks;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/3D9;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 456588
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 456589
    return-void
.end method


# virtual methods
.method public final a(LX/2nq;Ljava/lang/Object;)LX/3D9;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ":",
            "LX/1Pq;",
            ":",
            "LX/2kk;",
            ":",
            "LX/2kl;",
            ">(",
            "LX/2nq;",
            "TE;)",
            "LX/3D9",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 456590
    new-instance v1, LX/3D9;

    move-object/from16 v3, p2

    check-cast v3, LX/1Pq;

    invoke-static/range {p0 .. p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static/range {p0 .. p0}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v6

    check-cast v6, LX/3Cm;

    invoke-static/range {p0 .. p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v7

    check-cast v7, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static/range {p0 .. p0}, LX/3DA;->a(LX/0QB;)LX/3DA;

    move-result-object v8

    check-cast v8, LX/3DA;

    invoke-static/range {p0 .. p0}, LX/1rn;->a(LX/0QB;)LX/1rn;

    move-result-object v9

    check-cast v9, LX/1rn;

    invoke-static/range {p0 .. p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v10

    check-cast v10, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v11

    check-cast v11, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v12

    check-cast v12, LX/1rU;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-static/range {p0 .. p0}, LX/1rp;->a(LX/0QB;)LX/1rp;

    move-result-object v14

    check-cast v14, LX/1rp;

    invoke-static/range {p0 .. p0}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v15

    check-cast v15, LX/2c4;

    invoke-static/range {p0 .. p0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v16

    check-cast v16, LX/19j;

    const-class v2, LX/1Ns;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/1Ns;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v18

    check-cast v18, LX/0W3;

    invoke-static/range {p0 .. p0}, LX/3DB;->a(LX/0QB;)LX/3DB;

    move-result-object v19

    check-cast v19, LX/3DB;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v19}, LX/3D9;-><init>(LX/2nq;LX/1Pq;LX/0TD;LX/17W;LX/3Cm;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/3DA;LX/1rn;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Sh;LX/1rU;LX/03V;LX/1rp;LX/2c4;LX/19j;LX/1Ns;LX/0W3;LX/3DB;)V

    .line 456591
    return-object v1
.end method
