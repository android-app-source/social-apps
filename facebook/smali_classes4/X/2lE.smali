.class public LX/2lE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2lE;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 457402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457403
    iput-object p1, p0, LX/2lE;->a:LX/0Zb;

    .line 457404
    return-void
.end method

.method public static a(LX/0QB;)LX/2lE;
    .locals 4

    .prologue
    .line 457405
    sget-object v0, LX/2lE;->b:LX/2lE;

    if-nez v0, :cond_1

    .line 457406
    const-class v1, LX/2lE;

    monitor-enter v1

    .line 457407
    :try_start_0
    sget-object v0, LX/2lE;->b:LX/2lE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 457408
    if-eqz v2, :cond_0

    .line 457409
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 457410
    new-instance p0, LX/2lE;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/2lE;-><init>(LX/0Zb;)V

    .line 457411
    move-object v0, p0

    .line 457412
    sput-object v0, LX/2lE;->b:LX/2lE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457413
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 457414
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 457415
    :cond_1
    sget-object v0, LX/2lE;->b:LX/2lE;

    return-object v0

    .line 457416
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 457417
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/3zK;)V
    .locals 3

    .prologue
    .line 457418
    iget-object v0, p0, LX/2lE;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v2, p1, LX/3zK;->name:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 457419
    return-void
.end method
