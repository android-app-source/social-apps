.class public final LX/3Hx;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/3JC;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/31H;


# direct methods
.method public constructor <init>(LX/31H;)V
    .locals 0

    .prologue
    .line 545268
    iput-object p1, p0, LX/3Hx;->a:LX/31H;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/3JC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 545267
    const-class v0, LX/3JC;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 545260
    check-cast p1, LX/3JC;

    .line 545261
    iget-object v0, p0, LX/3Hx;->a:LX/31H;

    iget-object v0, v0, LX/31H;->t:LX/D6v;

    if-nez v0, :cond_0

    .line 545262
    :goto_0
    return-void

    .line 545263
    :cond_0
    sget-object v0, LX/D7E;->a:[I

    iget-object v1, p1, LX/3JC;->a:LX/2qV;

    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 545264
    :pswitch_0
    iget-object v0, p0, LX/3Hx;->a:LX/31H;

    iget-object v0, v0, LX/31H;->t:LX/D6v;

    sget-object v1, LX/BSR;->ERROR:LX/BSR;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/BSR;)V

    .line 545265
    iget-object v0, p0, LX/3Hx;->a:LX/31H;

    iget-object v0, v0, LX/31H;->d:LX/03V;

    sget-object v1, LX/31H;->e:Ljava/lang/String;

    const-string v2, "Commercial break RVP playback error in VOD"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 545266
    :pswitch_1
    iget-object v0, p0, LX/3Hx;->a:LX/31H;

    iget-object v0, v0, LX/31H;->t:LX/D6v;

    sget-object v1, LX/BSR;->PLAYBACK_FINISHED:LX/BSR;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/BSR;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
