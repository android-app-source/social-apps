.class public LX/2J9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/GbP;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0fW;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0fW;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/GbP;",
            ">;",
            "LX/0fW;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392551
    iput-object p1, p0, LX/2J9;->b:LX/0Or;

    .line 392552
    iput-object p2, p0, LX/2J9;->c:LX/0fW;

    .line 392553
    iput-object p3, p0, LX/2J9;->d:LX/0Or;

    .line 392554
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 392555
    iget-object v0, p0, LX/2J9;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 392556
    const/4 v0, 0x0

    .line 392557
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/2J9;->c:LX/0fW;

    iget-object v0, p0, LX/2J9;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 392558
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 392559
    invoke-virtual {v1, v0}, LX/0fW;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 392560
    sget-object v0, LX/2Jm;->INTERVAL:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<+",
            "LX/GbP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392561
    iget-object v0, p0, LX/2J9;->b:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 2

    .prologue
    .line 392562
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jq;->LOGGED_IN:LX/2Jq;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jq;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jh;->BACKGROUND:LX/2Jh;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jh;)LX/2Jk;

    move-result-object v0

    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 392563
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method
