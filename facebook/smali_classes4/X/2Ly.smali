.class public LX/2Ly;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2Ly;


# instance fields
.field public final a:LX/2Lz;

.field private final b:LX/2M0;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Mv;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            "LX/6eh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Lz;LX/2M0;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Lz;",
            "LX/2M0;",
            "LX/0Ot",
            "<",
            "LX/2Mv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395830
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/2Ly;->d:Ljava/util/Map;

    .line 395831
    iput-object p1, p0, LX/2Ly;->a:LX/2Lz;

    .line 395832
    iput-object p2, p0, LX/2Ly;->b:LX/2M0;

    .line 395833
    iput-object p3, p0, LX/2Ly;->c:LX/0Ot;

    .line 395834
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ly;
    .locals 6

    .prologue
    .line 395835
    sget-object v0, LX/2Ly;->e:LX/2Ly;

    if-nez v0, :cond_1

    .line 395836
    const-class v1, LX/2Ly;

    monitor-enter v1

    .line 395837
    :try_start_0
    sget-object v0, LX/2Ly;->e:LX/2Ly;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 395838
    if-eqz v2, :cond_0

    .line 395839
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 395840
    new-instance v5, LX/2Ly;

    invoke-static {v0}, LX/2Lz;->a(LX/0QB;)LX/2Lz;

    move-result-object v3

    check-cast v3, LX/2Lz;

    invoke-static {v0}, LX/2M0;->a(LX/0QB;)LX/2M0;

    move-result-object v4

    check-cast v4, LX/2M0;

    const/16 p0, 0xd6d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/2Ly;-><init>(LX/2Lz;LX/2M0;LX/0Ot;)V

    .line 395841
    move-object v0, v5

    .line 395842
    sput-object v0, LX/2Ly;->e:LX/2Ly;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395843
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 395844
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 395845
    :cond_1
    sget-object v0, LX/2Ly;->e:LX/2Ly;

    return-object v0

    .line 395846
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 395847
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;
    .locals 6

    .prologue
    .line 395848
    sget-object v0, LX/6ei;->a:[I

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    invoke-virtual {v1}, LX/2uW;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 395849
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected message type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 395850
    :pswitch_0
    sget-object v0, LX/6eh;->GROUP_MEMBERSHIP_CHANGE:LX/6eh;

    .line 395851
    :goto_0
    return-object v0

    .line 395852
    :pswitch_1
    sget-object v0, LX/6eh;->GROUP_NAME_CHANGE:LX/6eh;

    goto :goto_0

    .line 395853
    :pswitch_2
    sget-object v0, LX/6eh;->GROUP_IMAGE_CHANGE:LX/6eh;

    goto :goto_0

    .line 395854
    :pswitch_3
    sget-object v0, LX/6eh;->VIDEO_CALL:LX/6eh;

    goto :goto_0

    .line 395855
    :pswitch_4
    sget-object v0, LX/6eh;->VOIP_CALL:LX/6eh;

    goto :goto_0

    .line 395856
    :pswitch_5
    sget-object v0, LX/6eh;->CALL_LOG:LX/6eh;

    goto :goto_0

    .line 395857
    :pswitch_6
    sget-object v0, LX/6eh;->TELEPHONE_COMMUNICATION_LOG:LX/6eh;

    goto :goto_0

    .line 395858
    :pswitch_7
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 395859
    sget-object v0, LX/6eh;->STICKER:LX/6eh;

    .line 395860
    :goto_1
    move-object v0, v0

    .line 395861
    goto :goto_0

    .line 395862
    :pswitch_8
    sget-object v0, LX/6eh;->GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:LX/6eh;

    goto :goto_0

    .line 395863
    :pswitch_9
    sget-object v0, LX/6eh;->ADMIN:LX/6eh;

    goto :goto_0

    .line 395864
    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 395865
    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_c

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 395866
    invoke-static {v0}, LX/2M0;->b(Lcom/facebook/messaging/model/attachment/Attachment;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    .line 395867
    :goto_3
    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-eqz v3, :cond_9

    if-nez v0, :cond_9

    .line 395868
    :cond_1
    :goto_4
    move v0, v1

    .line 395869
    if-eqz v0, :cond_2

    .line 395870
    sget-object v0, LX/6eh;->PHOTOS:LX/6eh;

    goto :goto_1

    .line 395871
    :cond_2
    iget-object v0, p0, LX/2Ly;->a:LX/2Lz;

    invoke-virtual {v0, p1}, LX/2Lz;->b(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 395872
    sget-object v0, LX/6eh;->VIDEO_CLIP:LX/6eh;

    goto :goto_1

    .line 395873
    :cond_3
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 395874
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_d

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    invoke-static {v0}, LX/2M0;->a(Lcom/facebook/messaging/model/attachment/Attachment;)Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    .line 395875
    :goto_5
    move v0, v0

    .line 395876
    if-eqz v0, :cond_4

    .line 395877
    sget-object v0, LX/6eh;->AUDIO_CLIP:LX/6eh;

    goto :goto_1

    .line 395878
    :cond_4
    iget-object v0, p0, LX/2Ly;->a:LX/2Lz;

    invoke-virtual {v0, p1}, LX/2Lz;->e(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 395879
    sget-object v0, LX/6eh;->PAYMENT:LX/6eh;

    goto :goto_1

    .line 395880
    :cond_5
    iget-object v0, p0, LX/2Ly;->a:LX/2Lz;

    invoke-virtual {v0, p1}, LX/2Lz;->f(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 395881
    sget-object v0, LX/6eh;->MOMENTS_INVITE:LX/6eh;

    goto :goto_1

    .line 395882
    :cond_6
    invoke-static {p1}, LX/2Lz;->g(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 395883
    sget-object v0, LX/6eh;->COMMERCE:LX/6eh;

    goto :goto_1

    .line 395884
    :cond_7
    sget-object v0, LX/6eh;->NORMAL:LX/6eh;

    goto :goto_1

    .line 395885
    :cond_8
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 395886
    :cond_9
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    move v1, v2

    .line 395887
    goto :goto_4

    .line 395888
    :cond_a
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_6
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 395889
    invoke-static {v0}, LX/5zt;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-nez v0, :cond_b

    move v1, v2

    .line 395890
    goto :goto_4

    .line 395891
    :cond_b
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    :cond_c
    move v0, v2

    goto/16 :goto_3

    .line 395892
    :cond_d
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_e

    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->AUDIO:LX/2MK;

    if-ne v0, v3, :cond_e

    move v0, v1

    .line 395893
    goto :goto_5

    :cond_e
    move v0, v2

    .line 395894
    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;
    .locals 3

    .prologue
    .line 395895
    iget-object v1, p0, LX/2Ly;->d:Ljava/util/Map;

    monitor-enter v1

    .line 395896
    :try_start_0
    iget-object v0, p0, LX/2Ly;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6eh;

    .line 395897
    if-nez v0, :cond_0

    .line 395898
    invoke-direct {p0, p1}, LX/2Ly;->d(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v0

    .line 395899
    iget-object v2, p0, LX/2Ly;->d:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395900
    :cond_0
    monitor-exit v1

    return-object v0

    .line 395901
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 395902
    const/4 v1, 0x0

    .line 395903
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 395904
    const-string v0, "h"

    .line 395905
    :goto_0
    move-object v1, v0

    .line 395906
    iget-object v0, p0, LX/2Ly;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Mv;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 395907
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "m"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 395908
    :goto_1
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 395909
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "p"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 395910
    :cond_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 395911
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v0, :cond_3

    .line 395912
    const-string v0, "h"

    goto :goto_0

    .line 395913
    :cond_3
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 395914
    const-string v0, "227878347358915"

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "369239263222822"

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "369239343222814"

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "369239383222810"

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 395915
    :cond_4
    const-string v0, "l"

    goto :goto_0

    .line 395916
    :cond_5
    const-string v0, "s"

    goto :goto_0

    .line 395917
    :cond_6
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 395918
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 395919
    invoke-static {v0}, LX/2M0;->a(Lcom/facebook/messaging/model/attachment/Attachment;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 395920
    const-string v0, "a"

    goto/16 :goto_0

    .line 395921
    :cond_7
    iget-object v1, v0, Lcom/facebook/messaging/model/attachment/Attachment;->d:Ljava/lang/String;

    .line 395922
    if-eqz v1, :cond_11

    const-string v2, "video/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 395923
    if-eqz v1, :cond_8

    .line 395924
    const-string v0, "v"

    goto/16 :goto_0

    .line 395925
    :cond_8
    invoke-static {v0}, LX/2M0;->c(Lcom/facebook/messaging/model/attachment/Attachment;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 395926
    const-string v0, "g"

    goto/16 :goto_0

    .line 395927
    :cond_9
    invoke-static {v0}, LX/2M0;->b(Lcom/facebook/messaging/model/attachment/Attachment;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 395928
    const-string v0, "i"

    goto/16 :goto_0

    .line 395929
    :cond_a
    const-string v0, "f"

    goto/16 :goto_0

    .line 395930
    :cond_b
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    if-eqz v0, :cond_10

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 395931
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 395932
    iget-object v1, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->AUDIO:LX/2MK;

    if-ne v1, v2, :cond_c

    .line 395933
    const-string v0, "a"

    goto/16 :goto_0

    .line 395934
    :cond_c
    iget-object v1, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-ne v1, v2, :cond_d

    .line 395935
    const-string v0, "v"

    goto/16 :goto_0

    .line 395936
    :cond_d
    iget-object v1, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->PHOTO:LX/2MK;

    if-ne v1, v2, :cond_f

    .line 395937
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 395938
    const-string v0, "g"

    goto/16 :goto_0

    .line 395939
    :cond_e
    const-string v0, "i"

    goto/16 :goto_0

    .line 395940
    :cond_f
    const-string v0, "f"

    goto/16 :goto_0

    .line 395941
    :cond_10
    const-string v0, "t"

    goto/16 :goto_0

    :cond_11
    const/4 v1, 0x0

    goto :goto_2
.end method
