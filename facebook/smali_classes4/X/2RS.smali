.class public final enum LX/2RS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2RS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2RS;

.field public static final enum COMMUNICATION_RANK:LX/2RS;

.field public static final enum ID:LX/2RS;

.field public static final enum NAME:LX/2RS;

.field public static final enum NO_SORT_ORDER:LX/2RS;

.field public static final enum PHAT_RANK:LX/2RS;

.field public static final enum WITH_TAGGING_RANK:LX/2RS;


# instance fields
.field public final mLegacyIndexColumnName:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mOmnistoreIndexColumnName:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 409850
    new-instance v0, LX/2RS;

    const-string v1, "NO_SORT_ORDER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v2}, LX/2RS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2RS;->NO_SORT_ORDER:LX/2RS;

    .line 409851
    new-instance v0, LX/2RS;

    const-string v1, "NAME"

    const-string v2, "sort_name_key"

    invoke-direct {v0, v1, v5, v2}, LX/2RS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2RS;->NAME:LX/2RS;

    .line 409852
    new-instance v0, LX/2RS;

    const-string v1, "COMMUNICATION_RANK"

    const-string v2, "communication_rank"

    invoke-direct {v0, v1, v6, v2}, LX/2RS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    .line 409853
    new-instance v0, LX/2RS;

    const-string v1, "WITH_TAGGING_RANK"

    const-string v2, "with_tagging_rank"

    invoke-direct {v0, v1, v7, v2}, LX/2RS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2RS;->WITH_TAGGING_RANK:LX/2RS;

    .line 409854
    new-instance v0, LX/2RS;

    const-string v1, "PHAT_RANK"

    const-string v2, "communication_rank"

    const-string v3, "phat_rank"

    invoke-direct {v0, v1, v8, v2, v3}, LX/2RS;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2RS;->PHAT_RANK:LX/2RS;

    .line 409855
    new-instance v0, LX/2RS;

    const-string v1, "ID"

    const/4 v2, 0x5

    const-string v3, "_id"

    invoke-direct {v0, v1, v2, v3}, LX/2RS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2RS;->ID:LX/2RS;

    .line 409856
    const/4 v0, 0x6

    new-array v0, v0, [LX/2RS;

    sget-object v1, LX/2RS;->NO_SORT_ORDER:LX/2RS;

    aput-object v1, v0, v4

    sget-object v1, LX/2RS;->NAME:LX/2RS;

    aput-object v1, v0, v5

    sget-object v1, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    aput-object v1, v0, v6

    sget-object v1, LX/2RS;->WITH_TAGGING_RANK:LX/2RS;

    aput-object v1, v0, v7

    sget-object v1, LX/2RS;->PHAT_RANK:LX/2RS;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2RS;->ID:LX/2RS;

    aput-object v2, v0, v1

    sput-object v0, LX/2RS;->$VALUES:[LX/2RS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 409846
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 409847
    iput-object p3, p0, LX/2RS;->mLegacyIndexColumnName:Ljava/lang/String;

    .line 409848
    iput-object p3, p0, LX/2RS;->mOmnistoreIndexColumnName:Ljava/lang/String;

    .line 409849
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 409840
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 409841
    iput-object p3, p0, LX/2RS;->mLegacyIndexColumnName:Ljava/lang/String;

    .line 409842
    iput-object p4, p0, LX/2RS;->mOmnistoreIndexColumnName:Ljava/lang/String;

    .line 409843
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2RS;
    .locals 1

    .prologue
    .line 409845
    const-class v0, LX/2RS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2RS;

    return-object v0
.end method

.method public static values()[LX/2RS;
    .locals 1

    .prologue
    .line 409844
    sget-object v0, LX/2RS;->$VALUES:[LX/2RS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2RS;

    return-object v0
.end method
