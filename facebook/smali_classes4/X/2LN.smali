.class public final LX/2LN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/2LP;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/2LP;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 394883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394884
    iput-object p1, p0, LX/2LN;->a:LX/0QB;

    .line 394885
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 394886
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2LN;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 394887
    packed-switch p2, :pswitch_data_0

    .line 394888
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 394889
    :pswitch_0
    new-instance p2, LX/2LO;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p1}, LX/2LQ;->a(LX/0QB;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    invoke-direct {p2, v0, v1, p0}, LX/2LO;-><init>(Landroid/content/Context;LX/03V;Ljava/lang/String;)V

    .line 394890
    move-object v0, p2

    .line 394891
    :goto_0
    return-object v0

    .line 394892
    :pswitch_1
    new-instance v2, LX/2LR;

    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p1}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageManager;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p1}, LX/2LS;->b(LX/0QB;)LX/2LS;

    move-result-object v6

    check-cast v6, LX/2LS;

    invoke-static {p1}, LX/2LQ;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, LX/2LR;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/03V;LX/2LS;Ljava/lang/String;)V

    .line 394893
    move-object v0, v2

    .line 394894
    goto :goto_0

    .line 394895
    :pswitch_2
    new-instance v4, LX/2LU;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p1}, LX/2LS;->b(LX/0QB;)LX/2LS;

    move-result-object v2

    check-cast v2, LX/2LS;

    invoke-static {p1}, LX/2LQ;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2LU;-><init>(Landroid/content/Context;LX/03V;LX/2LS;Ljava/lang/String;)V

    .line 394896
    move-object v0, v4

    .line 394897
    goto :goto_0

    .line 394898
    :pswitch_3
    new-instance v4, LX/2LV;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p1}, LX/2LS;->b(LX/0QB;)LX/2LS;

    move-result-object v2

    check-cast v2, LX/2LS;

    invoke-static {p1}, LX/2LQ;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2LV;-><init>(Landroid/content/Context;LX/03V;LX/2LS;Ljava/lang/String;)V

    .line 394899
    move-object v0, v4

    .line 394900
    goto :goto_0

    .line 394901
    :pswitch_4
    new-instance v4, LX/2LW;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p1}, LX/2LS;->b(LX/0QB;)LX/2LS;

    move-result-object v2

    check-cast v2, LX/2LS;

    invoke-static {p1}, LX/2LQ;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2LW;-><init>(Landroid/content/Context;LX/03V;LX/2LS;Ljava/lang/String;)V

    .line 394902
    move-object v0, v4

    .line 394903
    goto/16 :goto_0

    .line 394904
    :pswitch_5
    new-instance v3, LX/2LX;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p1}, LX/2LQ;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v0, v1, v2}, LX/2LX;-><init>(Landroid/content/Context;LX/03V;Ljava/lang/String;)V

    .line 394905
    move-object v0, v3

    .line 394906
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 394907
    const/4 v0, 0x6

    return v0
.end method
