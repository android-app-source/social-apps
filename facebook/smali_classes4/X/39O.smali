.class public abstract LX/39O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/39P;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 522910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 522911
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 522889
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LX/15i;II)Z
    .locals 1

    .prologue
    .line 522909
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LX/39P;)Z
    .locals 5

    .prologue
    .line 522899
    const/4 v0, 0x0

    .line 522900
    invoke-interface {p1}, LX/39P;->b()LX/2sN;

    move-result-object v1

    .line 522901
    :cond_0
    :goto_0
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 522902
    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    .line 522903
    iget-object v3, v2, LX/1vs;->a:LX/15i;

    .line 522904
    iget v4, v2, LX/1vs;->b:I

    .line 522905
    iget v2, v2, LX/1vs;->c:I

    .line 522906
    invoke-virtual {p0, v3, v4, v2}, LX/39O;->a(LX/15i;II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 522907
    const/4 v0, 0x1

    goto :goto_0

    .line 522908
    :cond_1
    return v0
.end method

.method public abstract b()LX/2sN;
.end method

.method public abstract c()I
.end method

.method public d()LX/3Sd;
    .locals 5

    .prologue
    .line 522890
    new-instance v0, LX/4AD;

    invoke-virtual {p0}, LX/39O;->c()I

    move-result v1

    invoke-direct {v0, v1}, LX/4AD;-><init>(I)V

    .line 522891
    invoke-virtual {p0}, LX/39O;->b()LX/2sN;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 522892
    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    .line 522893
    iget-object v3, v2, LX/1vs;->a:LX/15i;

    .line 522894
    iget v4, v2, LX/1vs;->b:I

    .line 522895
    iget v2, v2, LX/1vs;->c:I

    .line 522896
    invoke-virtual {v0, v3, v4, v2}, LX/39O;->a(LX/15i;II)Z

    goto :goto_0

    .line 522897
    :cond_0
    move-object v0, v0

    .line 522898
    invoke-virtual {v0}, LX/39O;->d()LX/3Sd;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 522870
    invoke-virtual {p0}, LX/39O;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522871
    const-string v0, "[]"

    .line 522872
    :goto_0
    return-object v0

    .line 522873
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, LX/39O;->c()I

    move-result v1

    mul-int/lit8 v1, v1, 0x10

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 522874
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 522875
    invoke-virtual {p0}, LX/39O;->b()LX/2sN;

    move-result-object v1

    .line 522876
    :cond_1
    :goto_1
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 522877
    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    .line 522878
    iget-object v3, v2, LX/1vs;->a:LX/15i;

    .line 522879
    iget v4, v2, LX/1vs;->b:I

    .line 522880
    iget v2, v2, LX/1vs;->c:I

    .line 522881
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    .line 522882
    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522883
    invoke-static {v3, v4, v2}, LX/1vu;->a(LX/15i;II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 522884
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 522885
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 522886
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 522887
    :cond_2
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 522888
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
