.class public LX/2G6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/facebook/analytics/jscmetrics/JscMetrics;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/analytics/jscmetrics/JscMetrics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/analytics/jscmetrics/JscMetrics;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387824
    iput-object p1, p0, LX/2G6;->a:Ljava/util/Set;

    .line 387825
    return-void
.end method

.method public static b(LX/0QB;)LX/2G6;
    .locals 4

    .prologue
    .line 387826
    new-instance v0, LX/2G6;

    .line 387827
    new-instance v1, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/2G7;

    invoke-direct {v3, p0}, LX/2G7;-><init>(LX/0QB;)V

    invoke-direct {v1, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v1, v1

    .line 387828
    invoke-direct {v0, v1}, LX/2G6;-><init>(Ljava/util/Set;)V

    .line 387829
    return-object v0
.end method


# virtual methods
.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/facebook/analytics/jscmetrics/JscMetrics;",
            ">;"
        }
    .end annotation

    .prologue
    .line 387830
    iget-object v0, p0, LX/2G6;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
