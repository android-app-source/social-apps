.class public LX/3Bd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 528772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;)LX/1y5;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 528766
    if-nez p0, :cond_0

    .line 528767
    :goto_0
    return-object v1

    .line 528768
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 528769
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 528770
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-static {v2, v3, v0, v4, v1}, LX/3Bd;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)LX/1y5;

    move-result-object v1

    goto :goto_0

    .line 528771
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->f()I

    move-result v0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 528773
    if-nez p0, :cond_0

    .line 528774
    :goto_0
    return-object v0

    .line 528775
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->f()I

    move-result v1

    .line 528776
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->aE()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-static {v2, v3, v1, v4, v0}, LX/3Bd;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)LX/1y5;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPlace;)LX/1y5;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 528760
    if-nez p0, :cond_0

    .line 528761
    :goto_0
    return-object v1

    .line 528762
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 528763
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 528764
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->Y()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-static {v2, v3, v0, v4, v1}, LX/3Bd;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)LX/1y5;

    move-result-object v1

    goto :goto_0

    .line 528765
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->f()I

    move-result v0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/1y5;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 528754
    if-nez p0, :cond_0

    .line 528755
    :goto_0
    return-object v1

    .line 528756
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 528757
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 528758
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->M()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-static {v2, v3, v0, v4, v1}, LX/3Bd;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)LX/1y5;

    move-result-object v1

    goto :goto_0

    .line 528759
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->f()I

    move-result v0

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)LX/1y5;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 528735
    const/4 v0, 0x0

    .line 528736
    if-eqz p4, :cond_0

    .line 528737
    new-instance v0, LX/3Be;

    invoke-direct {v0}, LX/3Be;-><init>()V

    .line 528738
    iput-object p4, v0, LX/3Be;->a:Ljava/lang/String;

    .line 528739
    move-object v0, v0

    .line 528740
    invoke-virtual {v0}, LX/3Be;->a()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    move-result-object v0

    .line 528741
    :cond_0
    new-instance v1, LX/3Bg;

    invoke-direct {v1}, LX/3Bg;-><init>()V

    .line 528742
    iput-object p1, v1, LX/3Bg;->b:Ljava/lang/String;

    .line 528743
    move-object v1, v1

    .line 528744
    iput-object p0, v1, LX/3Bg;->c:Ljava/lang/String;

    .line 528745
    move-object v1, v1

    .line 528746
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v2, p2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 528747
    iput-object v2, v1, LX/3Bg;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 528748
    move-object v1, v1

    .line 528749
    iput-object p3, v1, LX/3Bg;->e:Ljava/lang/String;

    .line 528750
    move-object v1, v1

    .line 528751
    iput-object v0, v1, LX/3Bg;->d:Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel$ProfilePictureModel;

    .line 528752
    move-object v0, v1

    .line 528753
    invoke-virtual {v0}, LX/3Bg;->a()Lcom/facebook/linkify/LinkifyTargetGraphQLModels$LinkifyTargetGraphQLModel;

    move-result-object v0

    return-object v0
.end method
