.class public LX/2X8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/device_id/ShareDeviceId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 419522
    const-class v0, LX/2X8;

    sput-object v0, LX/2X8;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 419521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 419518
    invoke-static {p1}, LX/1mU;->a(Landroid/content/Context;)V

    .line 419519
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/2X8;

    invoke-static {v0}, LX/38I;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v1, p0, LX/2X8;->a:Ljava/lang/Boolean;

    iput-object v0, p0, LX/2X8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 419520
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x2

    const/4 v8, -0x1

    const-wide v6, 0x7fffffffffffffffL

    const/16 v0, 0x26

    const v2, 0x6261ca94

    invoke-static {v3, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 419494
    invoke-direct {p0, p1}, LX/2X8;->a(Landroid/content/Context;)V

    .line 419495
    iget-object v0, p0, LX/2X8;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 419496
    const/16 v0, 0x27

    const v1, 0x3026eacf

    invoke-static {v3, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 419497
    :goto_0
    return-void

    .line 419498
    :cond_0
    invoke-interface {p3}, LX/0Yf;->getResultCode()I

    move-result v0

    if-ne v0, v8, :cond_4

    .line 419499
    invoke-interface {p3}, LX/0Yf;->getResultData()Ljava/lang/String;

    move-result-object v3

    .line 419500
    const/4 v0, 0x1

    invoke-interface {p3, v0}, LX/0Yf;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 419501
    const-string v4, "device_id_generated_timestamp_ms"

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 419502
    new-instance v0, LX/0dI;

    invoke-direct {v0, v3, v4, v5}, LX/0dI;-><init>(Ljava/lang/String;J)V

    .line 419503
    :goto_1
    iget-object v3, p0, LX/2X8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0dH;->a:LX/0Tn;

    invoke-interface {v3, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 419504
    iget-object v3, p0, LX/2X8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0dH;->b:LX/0Tn;

    invoke-interface {v3, v4, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 419505
    if-eqz v1, :cond_1

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 419506
    :cond_1
    const v0, 0x25bcde70

    invoke-static {v0, v2}, LX/02F;->e(II)V

    goto :goto_0

    .line 419507
    :cond_2
    if-eqz v0, :cond_3

    .line 419508
    iget-wide v9, v0, LX/0dI;->b:J

    move-wide v6, v9

    .line 419509
    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    .line 419510
    const v0, 0x295b2789

    invoke-static {v0, v2}, LX/02F;->e(II)V

    goto :goto_0

    .line 419511
    :cond_3
    invoke-interface {p3, v8}, LX/0Yf;->setResultCode(I)V

    .line 419512
    invoke-interface {p3, v1}, LX/0Yf;->setResultData(Ljava/lang/String;)V

    .line 419513
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 419514
    const-string v3, "device_id_generated_timestamp_ms"

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 419515
    invoke-interface {p3, v0}, LX/0Yf;->setResultExtras(Landroid/os/Bundle;)V

    .line 419516
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "device id found: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 419517
    const v0, 0x168c2662

    invoke-static {v0, v2}, LX/02F;->e(II)V

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
