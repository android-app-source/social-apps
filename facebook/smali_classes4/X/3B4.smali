.class public final enum LX/3B4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3B4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3B4;

.field public static final enum ADM:LX/3B4;

.field public static final enum C2DM:LX/3B4;

.field public static final enum FBNS:LX/3B4;

.field public static final enum FBNS_LITE:LX/3B4;

.field public static final enum INTEGRATION_TEST:LX/3B4;

.field public static final enum MMS:LX/3B4;

.field public static final enum MQTT:LX/3B4;

.field public static final enum MQTT_PUSH:LX/3B4;

.field public static final enum NNA:LX/3B4;

.field public static final enum SMS_DEFAULT_APP:LX/3B4;

.field public static final enum SMS_READONLY_MODE:LX/3B4;

.field public static final enum TINCAN:LX/3B4;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 527373
    new-instance v0, LX/3B4;

    const-string v1, "ADM"

    invoke-direct {v0, v1, v3}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->ADM:LX/3B4;

    .line 527374
    new-instance v0, LX/3B4;

    const-string v1, "C2DM"

    invoke-direct {v0, v1, v4}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->C2DM:LX/3B4;

    .line 527375
    new-instance v0, LX/3B4;

    const-string v1, "NNA"

    invoke-direct {v0, v1, v5}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->NNA:LX/3B4;

    .line 527376
    new-instance v0, LX/3B4;

    const-string v1, "MQTT"

    invoke-direct {v0, v1, v6}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->MQTT:LX/3B4;

    .line 527377
    new-instance v0, LX/3B4;

    const-string v1, "MQTT_PUSH"

    invoke-direct {v0, v1, v7}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->MQTT_PUSH:LX/3B4;

    .line 527378
    new-instance v0, LX/3B4;

    const-string v1, "FBNS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->FBNS:LX/3B4;

    .line 527379
    new-instance v0, LX/3B4;

    const-string v1, "MMS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->MMS:LX/3B4;

    .line 527380
    new-instance v0, LX/3B4;

    const-string v1, "FBNS_LITE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->FBNS_LITE:LX/3B4;

    .line 527381
    new-instance v0, LX/3B4;

    const-string v1, "INTEGRATION_TEST"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->INTEGRATION_TEST:LX/3B4;

    .line 527382
    new-instance v0, LX/3B4;

    const-string v1, "SMS_DEFAULT_APP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->SMS_DEFAULT_APP:LX/3B4;

    .line 527383
    new-instance v0, LX/3B4;

    const-string v1, "SMS_READONLY_MODE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->SMS_READONLY_MODE:LX/3B4;

    .line 527384
    new-instance v0, LX/3B4;

    const-string v1, "TINCAN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/3B4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3B4;->TINCAN:LX/3B4;

    .line 527385
    const/16 v0, 0xc

    new-array v0, v0, [LX/3B4;

    sget-object v1, LX/3B4;->ADM:LX/3B4;

    aput-object v1, v0, v3

    sget-object v1, LX/3B4;->C2DM:LX/3B4;

    aput-object v1, v0, v4

    sget-object v1, LX/3B4;->NNA:LX/3B4;

    aput-object v1, v0, v5

    sget-object v1, LX/3B4;->MQTT:LX/3B4;

    aput-object v1, v0, v6

    sget-object v1, LX/3B4;->MQTT_PUSH:LX/3B4;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3B4;->FBNS:LX/3B4;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3B4;->MMS:LX/3B4;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3B4;->FBNS_LITE:LX/3B4;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3B4;->INTEGRATION_TEST:LX/3B4;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/3B4;->SMS_DEFAULT_APP:LX/3B4;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/3B4;->SMS_READONLY_MODE:LX/3B4;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/3B4;->TINCAN:LX/3B4;

    aput-object v2, v0, v1

    sput-object v0, LX/3B4;->$VALUES:[LX/3B4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 527386
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static isPushNotification(LX/3B4;)Z
    .locals 1

    .prologue
    .line 527387
    sget-object v0, LX/3B4;->ADM:LX/3B4;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/3B4;->C2DM:LX/3B4;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/3B4;->NNA:LX/3B4;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/3B4;->MQTT_PUSH:LX/3B4;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/3B4;->FBNS:LX/3B4;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/3B4;->FBNS_LITE:LX/3B4;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPushNotification(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 527388
    if-nez p0, :cond_0

    .line 527389
    :goto_0
    return v0

    .line 527390
    :cond_0
    :try_start_0
    invoke-static {p0}, LX/3B4;->valueOf(Ljava/lang/String;)LX/3B4;

    move-result-object v1

    invoke-static {v1}, LX/3B4;->isPushNotification(LX/3B4;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 527391
    :catch_0
    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/3B4;
    .locals 1

    .prologue
    .line 527392
    const-class v0, LX/3B4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3B4;

    return-object v0
.end method

.method public static values()[LX/3B4;
    .locals 1

    .prologue
    .line 527393
    sget-object v0, LX/3B4;->$VALUES:[LX/3B4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3B4;

    return-object v0
.end method
