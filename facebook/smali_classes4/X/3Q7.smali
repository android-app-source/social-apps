.class public LX/3Q7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile o:LX/3Q7;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/01T;

.field public final e:Landroid/content/pm/PackageManager;

.field private final f:LX/0Zb;

.field private final g:LX/3Q0;

.field private final h:LX/3Ax;

.field private final i:Landroid/app/KeyguardManager;

.field private final j:LX/3Q8;

.field public final k:Ljava/util/concurrent/ExecutorService;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DqG;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 563135
    const-class v0, LX/3Q7;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Q7;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/01T;Landroid/content/pm/PackageManager;LX/0Zb;LX/3Q0;LX/3Ax;Landroid/app/KeyguardManager;LX/3Q8;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Or;LX/0Or;)V
    .locals 0
    .param p10    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/notifications/annotations/Fb4aLockscreenNotifications;
        .end annotation
    .end param
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/notifications/annotations/Fb4aLockscreenNotifPassthrough;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/01T;",
            "Landroid/content/pm/PackageManager;",
            "LX/0Zb;",
            "Lcom/facebook/notifications/lockscreen/util/PushNotificationIntentHelper;",
            "LX/3Ax;",
            "Landroid/app/KeyguardManager;",
            "LX/3Q8;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/DqG;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 563149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563150
    iput-object p1, p0, LX/3Q7;->b:Landroid/content/Context;

    .line 563151
    iput-object p2, p0, LX/3Q7;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 563152
    iput-object p3, p0, LX/3Q7;->d:LX/01T;

    .line 563153
    iput-object p4, p0, LX/3Q7;->e:Landroid/content/pm/PackageManager;

    .line 563154
    iput-object p5, p0, LX/3Q7;->f:LX/0Zb;

    .line 563155
    iput-object p6, p0, LX/3Q7;->g:LX/3Q0;

    .line 563156
    iput-object p7, p0, LX/3Q7;->h:LX/3Ax;

    .line 563157
    iput-object p8, p0, LX/3Q7;->i:Landroid/app/KeyguardManager;

    .line 563158
    iput-object p9, p0, LX/3Q7;->j:LX/3Q8;

    .line 563159
    iput-object p10, p0, LX/3Q7;->k:Ljava/util/concurrent/ExecutorService;

    .line 563160
    iput-object p11, p0, LX/3Q7;->l:LX/0Ot;

    .line 563161
    iput-object p12, p0, LX/3Q7;->m:LX/0Or;

    .line 563162
    iput-object p13, p0, LX/3Q7;->n:LX/0Or;

    .line 563163
    return-void
.end method

.method public static a(LX/0QB;)LX/3Q7;
    .locals 3

    .prologue
    .line 563139
    sget-object v0, LX/3Q7;->o:LX/3Q7;

    if-nez v0, :cond_1

    .line 563140
    const-class v1, LX/3Q7;

    monitor-enter v1

    .line 563141
    :try_start_0
    sget-object v0, LX/3Q7;->o:LX/3Q7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 563142
    if-eqz v2, :cond_0

    .line 563143
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/3Q7;->b(LX/0QB;)LX/3Q7;

    move-result-object v0

    sput-object v0, LX/3Q7;->o:LX/3Q7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 563144
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 563145
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 563146
    :cond_1
    sget-object v0, LX/3Q7;->o:LX/3Q7;

    return-object v0

    .line 563147
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 563148
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 563138
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[show on lockscreen]="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "hipri [turn screen on]="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [default timeout]=-1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [default priority]=100"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/3Q7;
    .locals 14

    .prologue
    .line 563136
    new-instance v0, LX/3Q7;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageManager;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {p0}, LX/3Q0;->b(LX/0QB;)LX/3Q0;

    move-result-object v6

    check-cast v6, LX/3Q0;

    invoke-static {p0}, LX/3Ax;->a(LX/0QB;)LX/3Ax;

    move-result-object v7

    check-cast v7, LX/3Ax;

    invoke-static {p0}, LX/1sh;->b(LX/0QB;)Landroid/app/KeyguardManager;

    move-result-object v8

    check-cast v8, Landroid/app/KeyguardManager;

    invoke-static {p0}, LX/3Q8;->a(LX/0QB;)LX/3Q8;

    move-result-object v9

    check-cast v9, LX/3Q8;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    const/16 v11, 0x2ac9

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x33c

    invoke-static {p0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x33b

    invoke-static {p0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, LX/3Q7;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/01T;Landroid/content/pm/PackageManager;LX/0Zb;LX/3Q0;LX/3Ax;Landroid/app/KeyguardManager;LX/3Q8;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Or;LX/0Or;)V

    .line 563137
    return-object v0
.end method

.method private b(ZLjava/lang/String;)Z
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 563058
    iget-object v0, p0, LX/3Q7;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    move v0, v0

    .line 563059
    if-eqz v0, :cond_5

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 563060
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v0, v3, :cond_0

    .line 563061
    iget-object v0, p0, LX/3Q7;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/03R;->asBoolean(Z)Z

    move-result v0

    move v0, v0

    .line 563062
    if-eqz v0, :cond_6

    :cond_0
    move v0, v2

    .line 563063
    :goto_0
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 563064
    const-string v3, "sdk"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "sdk_version"

    aput-object v5, v4, v1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    const/4 v1, 0x2

    const-string v2, "ndid"

    aput-object v2, v4, v1

    const/4 v1, 0x3

    aput-object p2, v4, v1

    invoke-virtual {p0, v3, v4}, LX/3Q7;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 563065
    :cond_1
    move v0, v0

    .line 563066
    if-eqz v0, :cond_5

    invoke-static {p0}, LX/3Q7;->l(LX/3Q7;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 563067
    iget-object v0, p0, LX/3Q7;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    .line 563068
    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    .line 563069
    const-string v1, "shared_preferences_not_initialized"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ndid"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {p0, v1, v2}, LX/3Q7;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 563070
    :cond_2
    move v0, v0

    .line 563071
    if-eqz v0, :cond_5

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 563072
    :try_start_0
    iget-object v2, p0, LX/3Q7;->e:Landroid/content/pm/PackageManager;

    const-string v3, "com.motorola.aon"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 563073
    iget-boolean v2, v2, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v2, :cond_7

    .line 563074
    if-eqz p1, :cond_3

    .line 563075
    const-string v2, "moto_active_display_on"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "ndid"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {p0, v2, v3}, LX/3Q7;->a(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 563076
    :cond_3
    :goto_1
    move v0, v0

    .line 563077
    if-nez v0, :cond_5

    .line 563078
    const-string v0, "hipri"

    const-string v1, "none"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 563079
    if-nez v0, :cond_4

    if-eqz p1, :cond_4

    .line 563080
    const-string v1, "experiment group"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "experiment_params"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, LX/3Q7;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "ndid"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    invoke-virtual {p0, v1, v2}, LX/3Q7;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 563081
    :cond_4
    move v0, v0

    .line 563082
    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    move v0, v1

    .line 563083
    goto/16 :goto_0

    :catch_0
    :cond_7
    :goto_4
    move v0, v1

    goto :goto_1

    .line 563084
    :catch_1
    goto :goto_4

    :cond_8
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private static l(LX/3Q7;)Z
    .locals 2

    .prologue
    .line 563134
    iget-object v0, p0, LX/3Q7;->d:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Lcom/facebook/notifications/model/SystemTrayNotification;J)V
    .locals 9

    .prologue
    .line 563164
    iget-object v0, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->c:Ljava/lang/String;

    move-object v0, v0

    .line 563165
    if-nez v0, :cond_2

    const-string v0, "notag"

    .line 563166
    :goto_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 563167
    sget-object v2, LX/3B0;->a:LX/0U1;

    .line 563168
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 563169
    iget-wide v7, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->e:J

    move-wide v4, v7

    .line 563170
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 563171
    sget-object v2, LX/3B0;->b:LX/0U1;

    .line 563172
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 563173
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 563174
    sget-object v0, LX/3B0;->d:LX/0U1;

    .line 563175
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 563176
    iget-object v2, p2, Lcom/facebook/notifications/model/SystemTrayNotification;->mMessage:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 563177
    sget-object v0, LX/3B0;->e:LX/0U1;

    .line 563178
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 563179
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 563180
    sget-object v0, LX/3B0;->f:LX/0U1;

    .line 563181
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 563182
    invoke-virtual {p2}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/constants/NotificationType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 563183
    invoke-virtual {p2}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563184
    sget-object v0, LX/3B0;->c:LX/0U1;

    .line 563185
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 563186
    invoke-virtual {p2}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 563187
    :cond_0
    iget-object v0, p0, LX/3Q7;->g:LX/3Q0;

    invoke-virtual {v0, p2}, LX/3Q0;->b(Lcom/facebook/notifications/model/SystemTrayNotification;)Ljava/lang/String;

    move-result-object v0

    .line 563188
    if-eqz v0, :cond_1

    .line 563189
    sget-object v2, LX/3B0;->g:LX/0U1;

    .line 563190
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 563191
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 563192
    :cond_1
    sget-object v0, LX/3B0;->h:LX/0U1;

    .line 563193
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 563194
    invoke-virtual {p2}, Lcom/facebook/notifications/model/SystemTrayNotification;->i()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 563195
    iget-object v0, p0, LX/3Q7;->h:LX/3Ax;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 563196
    :try_start_0
    const-string v2, "push_notifications"

    const/4 v3, 0x0

    const/4 v4, 0x5

    const v5, -0x3a3836d3

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v0, -0x1b896dde

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0

    .line 563197
    :goto_1
    return-void

    .line 563198
    :cond_2
    iget-object v0, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->c:Ljava/lang/String;

    move-object v0, v0

    .line 563199
    goto/16 :goto_0

    .line 563200
    :catch_0
    move-exception v0

    .line 563201
    sget-object v1, LX/3Q7;->a:Ljava/lang/String;

    const-string v2, "lockscreen database was full"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 563121
    array-length v1, p2

    .line 563122
    and-int/lit8 v0, v1, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 563123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Number of parameters should be even"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 563124
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "lockscreen_notification_dropped"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "notifications"

    .line 563125
    iput-object v2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 563126
    move-object v0, v0

    .line 563127
    const-string v2, "reason"

    invoke-virtual {v0, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 563128
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 563129
    aget-object v3, p2, v0

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    add-int/lit8 v3, v0, 0x1

    aget-object v3, p2, v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 563130
    aget-object v3, p2, v0

    add-int/lit8 v4, v0, 0x1

    aget-object v4, p2, v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 563131
    :cond_1
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 563132
    :cond_2
    iget-object v0, p0, LX/3Q7;->f:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 563133
    return-void
.end method

.method public final a(Lcom/facebook/notifications/model/SystemTrayNotification;)Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 563107
    invoke-static {p0}, LX/3Q7;->l(LX/3Q7;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 563108
    :goto_0
    return v1

    .line 563109
    :cond_0
    const/16 v0, 0x64

    move v3, v0

    .line 563110
    const-string v0, "tp"

    invoke-virtual {p1, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d(Ljava/lang/String;)LX/0am;

    move-result-object v0

    .line 563111
    const/16 v4, 0x3e8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 563112
    const-string v0, "af"

    invoke-virtual {p1, v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d(Ljava/lang/String;)LX/0am;

    move-result-object v0

    .line 563113
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 563114
    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->i()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v2, v0}, LX/3Q7;->a(ZLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 563115
    const/4 v0, 0x1

    move v0, v0

    .line 563116
    if-eqz v0, :cond_1

    if-gt v4, v3, :cond_4

    .line 563117
    :cond_1
    invoke-virtual {p0}, LX/3Q7;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    if-gt v4, v3, :cond_3

    if-ne v5, v2, :cond_3

    :cond_2
    move v0, v2

    :goto_1
    move v1, v0

    .line 563118
    goto :goto_0

    .line 563119
    :cond_3
    const-string v3, "notif not from friend for nux"

    new-array v4, v6, [Ljava/lang/String;

    const-string v0, "ndid"

    aput-object v0, v4, v1

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->i()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v2

    invoke-virtual {p0, v3, v4}, LX/3Q7;->a(Ljava/lang/String;[Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    .line 563120
    :cond_4
    const-string v3, "notif type not hipri"

    new-array v4, v6, [Ljava/lang/String;

    const-string v0, "ndid"

    aput-object v0, v4, v1

    invoke-virtual {p1}, Lcom/facebook/notifications/model/SystemTrayNotification;->i()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v2

    invoke-virtual {p0, v3, v4}, LX/3Q7;->a(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final a(Z)Z
    .locals 1

    .prologue
    .line 563106
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Q7;->b(ZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(ZLjava/lang/String;)Z
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 563100
    invoke-direct {p0, p1, p2}, LX/3Q7;->b(ZLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v5, 0x1

    .line 563101
    iget-object v0, p0, LX/3Q7;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->l:LX/0Tn;

    invoke-interface {v0, v1, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 563102
    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 563103
    const-string v1, "user turned off master setting"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ndid"

    aput-object v4, v2, v3

    aput-object p2, v2, v5

    invoke-virtual {p0, v1, v2}, LX/3Q7;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 563104
    :cond_0
    move v0, v0

    .line 563105
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 563099
    iget-object v0, p0, LX/3Q7;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->G:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 563098
    iget-object v0, p0, LX/3Q7;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->E:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 563085
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3Q7;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3Q7;->i:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3Q7;->j:LX/3Q8;

    const/4 v1, 0x1

    .line 563086
    sget-object v2, LX/3Q8;->a:Landroid/content/ComponentName;

    invoke-static {v0, v2}, LX/3Q8;->a(LX/3Q8;Landroid/content/ComponentName;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "com.google.android.apps.maps"

    const-string v3, ".NavigationService"

    invoke-static {v0, v2, v3}, LX/3Q8;->a(LX/3Q8;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 563087
    :cond_0
    :goto_0
    move v0, v1

    .line 563088
    if-nez v0, :cond_1

    .line 563089
    :try_start_0
    iget-object v0, p0, LX/3Q7;->b:Landroid/content/Context;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 563090
    :try_start_1
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/3Q7;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.facebook.notifications.lockscreenservice.LockScreenService"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 563091
    :goto_1
    :try_start_2
    const-string v2, "com.facebook.notifications.lockscreen.ACTION_LAUNCH_LOCKSCREEN_NOTIFICATIONS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 563092
    move-object v1, v1

    .line 563093
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    .line 563094
    :cond_1
    :goto_2
    return-void

    :catch_0
    goto :goto_2

    .line 563095
    :cond_2
    sget-object v2, LX/3Q8;->b:Landroid/content/ComponentName;

    invoke-static {v0, v2}, LX/3Q8;->a(LX/3Q8;Landroid/content/ComponentName;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "com.google.android.apps.maps"

    const-string v3, ".NavigationService"

    invoke-static {v0, v2, v3}, LX/3Q8;->a(LX/3Q8;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 563096
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 563097
    :catch_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    goto :goto_1
.end method
