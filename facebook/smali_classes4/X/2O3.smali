.class public LX/2O3;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/compactdisk/TrashCollector;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/compactdisk/TrashCollector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 400412
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/compactdisk/TrashCollector;
    .locals 6

    .prologue
    .line 400413
    sget-object v0, LX/2O3;->a:Lcom/facebook/compactdisk/TrashCollector;

    if-nez v0, :cond_1

    .line 400414
    const-class v1, LX/2O3;

    monitor-enter v1

    .line 400415
    :try_start_0
    sget-object v0, LX/2O3;->a:Lcom/facebook/compactdisk/TrashCollector;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 400416
    if-eqz v2, :cond_0

    .line 400417
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 400418
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2FA;->a(LX/0QB;)Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object v4

    check-cast v4, Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-static {v0}, LX/2FE;->a(LX/0QB;)Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    move-result-object v5

    check-cast v5, Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    invoke-static {v0}, LX/30E;->a(LX/0QB;)Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    move-result-object p0

    check-cast p0, Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    invoke-static {v3, v4, v5, p0}, LX/2FB;->a(Landroid/content/Context;Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Lcom/facebook/compactdisk/TaskQueueFactoryHolder;)Lcom/facebook/compactdisk/TrashCollector;

    move-result-object v3

    move-object v0, v3

    .line 400419
    sput-object v0, LX/2O3;->a:Lcom/facebook/compactdisk/TrashCollector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400420
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 400421
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 400422
    :cond_1
    sget-object v0, LX/2O3;->a:Lcom/facebook/compactdisk/TrashCollector;

    return-object v0

    .line 400423
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 400424
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 400425
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/2FA;->a(LX/0QB;)Lcom/facebook/compactdisk/FileUtilsHolder;

    move-result-object v1

    check-cast v1, Lcom/facebook/compactdisk/FileUtilsHolder;

    invoke-static {p0}, LX/2FE;->a(LX/0QB;)Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    move-result-object v2

    check-cast v2, Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;

    invoke-static {p0}, LX/30E;->a(LX/0QB;)Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    move-result-object v3

    check-cast v3, Lcom/facebook/compactdisk/TaskQueueFactoryHolder;

    invoke-static {v0, v1, v2, v3}, LX/2FB;->a(Landroid/content/Context;Lcom/facebook/compactdisk/FileUtilsHolder;Lcom/facebook/compactdisk/AnalyticsEventReporterHolder;Lcom/facebook/compactdisk/TaskQueueFactoryHolder;)Lcom/facebook/compactdisk/TrashCollector;

    move-result-object v0

    return-object v0
.end method
