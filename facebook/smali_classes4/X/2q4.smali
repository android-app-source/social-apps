.class public final LX/2q4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16Y;
.implements LX/2q1;


# instance fields
.field public final synthetic a:LX/2q3;


# direct methods
.method public constructor <init>(LX/2q3;)V
    .locals 0

    .prologue
    .line 469317
    iput-object p1, p0, LX/2q4;->a:LX/2q3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2qL;)V
    .locals 2

    .prologue
    .line 469318
    iget-object v0, p0, LX/2q4;->a:LX/2q3;

    iget v1, p1, LX/2qL;->a:I

    .line 469319
    iput v1, v0, LX/2q3;->i:I

    .line 469320
    return-void
.end method

.method public final a(LX/2qT;)V
    .locals 7

    .prologue
    .line 469307
    iget v0, p1, LX/2qT;->a:I

    .line 469308
    iget-object v1, p0, LX/2q4;->a:LX/2q3;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 469309
    iget-object v2, v1, LX/2q3;->g:LX/37C;

    if-eqz v2, :cond_0

    iget v2, v1, LX/2q3;->h:I

    if-gtz v2, :cond_1

    .line 469310
    :cond_0
    sget-object v5, LX/2q3;->a:Ljava/lang/String;

    const-string v6, "BytesViewedLogger not properly configured: has key: %s, has bitrate: %d"

    const/4 v2, 0x2

    new-array p1, v2, [Ljava/lang/Object;

    iget-object v2, v1, LX/2q3;->g:LX/37C;

    if-eqz v2, :cond_3

    move v2, v3

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, p1, v4

    iget v2, v1, LX/2q3;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, v3

    invoke-static {v5, v6, p1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469311
    :cond_1
    iget-object v1, p0, LX/2q4;->a:LX/2q3;

    iget v1, v1, LX/2q3;->i:I

    if-ltz v1, :cond_2

    iget-object v1, p0, LX/2q4;->a:LX/2q3;

    iget v1, v1, LX/2q3;->i:I

    if-ge v1, v0, :cond_2

    .line 469312
    iget-object v1, p0, LX/2q4;->a:LX/2q3;

    iget-object v2, p0, LX/2q4;->a:LX/2q3;

    iget v2, v2, LX/2q3;->i:I

    int-to-long v2, v2

    int-to-long v4, v0

    invoke-static {v1, v2, v3, v4, v5}, LX/2q3;->a$redex0(LX/2q3;JJ)V

    .line 469313
    iget-object v0, p0, LX/2q4;->a:LX/2q3;

    const/4 v1, -0x1

    .line 469314
    iput v1, v0, LX/2q3;->i:I

    .line 469315
    :cond_2
    return-void

    :cond_3
    move v2, v4

    .line 469316
    goto :goto_0
.end method
