.class public LX/3BG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3BG;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 527719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 527720
    iput-object p1, p0, LX/3BG;->a:Landroid/content/res/Resources;

    .line 527721
    return-void
.end method

.method public static a(LX/0QB;)LX/3BG;
    .locals 4

    .prologue
    .line 527722
    sget-object v0, LX/3BG;->b:LX/3BG;

    if-nez v0, :cond_1

    .line 527723
    const-class v1, LX/3BG;

    monitor-enter v1

    .line 527724
    :try_start_0
    sget-object v0, LX/3BG;->b:LX/3BG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 527725
    if-eqz v2, :cond_0

    .line 527726
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 527727
    new-instance p0, LX/3BG;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/3BG;-><init>(Landroid/content/res/Resources;)V

    .line 527728
    move-object v0, p0

    .line 527729
    sput-object v0, LX/3BG;->b:LX/3BG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527730
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 527731
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 527732
    :cond_1
    sget-object v0, LX/3BG;->b:LX/3BG;

    return-object v0

    .line 527733
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 527734
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ID)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 527735
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double v2, p2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v0, v2, v4

    if-gtz v0, :cond_0

    move v0, v1

    .line 527736
    :goto_0
    iget-object v2, p0, LX/3BG;->a:Landroid/content/res/Resources;

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {v2, p1, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 527737
    :cond_0
    const/4 v0, 0x5

    goto :goto_0
.end method
