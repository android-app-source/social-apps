.class public abstract LX/2DD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:LX/2u6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/2Db;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2DD",
            "<TT;>.Batch;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/2DJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:I

.field public final e:I

.field private final f:LX/0mm;

.field private final g:LX/0Zh;

.field public final h:[C

.field public final i:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(IILX/0mm;LX/0Zh;)V
    .locals 3

    .prologue
    .line 383837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383838
    const/16 v0, 0x400

    new-array v0, v0, [C

    iput-object v0, p0, LX/2DD;->h:[C

    .line 383839
    const/16 v0, 0x800

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/2DD;->i:Ljava/nio/ByteBuffer;

    .line 383840
    if-le p1, p2, :cond_0

    .line 383841
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383842
    :cond_0
    iput p1, p0, LX/2DD;->d:I

    .line 383843
    iput p2, p0, LX/2DD;->e:I

    .line 383844
    iput-object p3, p0, LX/2DD;->f:LX/0mm;

    .line 383845
    iput-object p4, p0, LX/2DD;->g:LX/0Zh;

    .line 383846
    return-void
.end method

.method private static e(LX/2DD;)LX/2Db;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2DD",
            "<TT;>.Batch;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 383819
    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    iget-object v0, v0, LX/2Db;->c:LX/2DZ;

    invoke-virtual {v0, p0}, LX/2DZ;->d(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 383820
    :cond_0
    invoke-virtual {p0}, LX/2DD;->d()V

    .line 383821
    iget-object v0, p0, LX/2DD;->a:LX/2u6;

    .line 383822
    iget-object v1, v0, LX/2u6;->b:Ljava/lang/String;

    move-object v0, v1

    .line 383823
    invoke-virtual {p0, v0}, LX/2DD;->a(Ljava/lang/String;)LX/2Db;

    move-result-object v0

    iput-object v0, p0, LX/2DD;->b:LX/2Db;

    .line 383824
    :try_start_0
    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    iget-object v0, v0, LX/2Db;->e:LX/2De;

    iget-object v1, p0, LX/2DD;->f:LX/0mm;

    .line 383825
    invoke-static {v0}, LX/2De;->c(LX/2De;)V

    .line 383826
    invoke-static {v0}, LX/2De;->a(LX/2De;)V

    .line 383827
    iget-object v2, v0, LX/2De;->a:Ljava/io/Writer;

    invoke-virtual {v1, v2}, LX/0mm;->a(Ljava/io/Writer;)V

    .line 383828
    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    iget-object v0, v0, LX/2Db;->e:LX/2De;

    iget-object v1, p0, LX/2DD;->c:LX/2DJ;

    .line 383829
    invoke-static {v0}, LX/2De;->c(LX/2De;)V

    .line 383830
    invoke-static {v0}, LX/2De;->a(LX/2De;)V

    .line 383831
    iget-object v2, v0, LX/2De;->a:Ljava/io/Writer;

    invoke-virtual {v1, v2}, LX/2DJ;->a(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 383832
    :cond_1
    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    return-object v0

    .line 383833
    :catch_0
    move-exception v0

    .line 383834
    iget-object v1, p0, LX/2DD;->b:LX/2Db;

    iget-object v1, v1, LX/2Db;->c:LX/2DZ;

    invoke-virtual {v1, p0}, LX/2DZ;->f(Ljava/lang/Object;)V

    .line 383835
    invoke-virtual {p0}, LX/2DD;->d()V

    .line 383836
    throw v0
.end method

.method private static f(LX/2DD;)V
    .locals 2

    .prologue
    .line 383816
    iget-object v0, p0, LX/2DD;->c:LX/2DJ;

    if-nez v0, :cond_0

    .line 383817
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mBatchSessionMetadataHelper is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383818
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 383810
    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    if-eqz v0, :cond_0

    .line 383811
    :try_start_0
    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    .line 383812
    iget-object v1, v0, LX/2Db;->d:Ljava/io/Writer;

    invoke-virtual {v1}, Ljava/io/Writer;->close()V

    .line 383813
    iget-object v1, v0, LX/2Db;->c:LX/2DZ;

    invoke-virtual {v1}, LX/2DZ;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 383814
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/2DD;->b:LX/2Db;

    .line 383815
    :cond_0
    return-void

    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;)LX/2Db;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/2DD",
            "<TT;>.Batch;"
        }
    .end annotation
.end method

.method public abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final a(LX/0nA;)V
    .locals 4

    .prologue
    .line 383794
    invoke-static {p0}, LX/2DD;->f(LX/2DD;)V

    .line 383795
    invoke-static {p0}, LX/2DD;->e(LX/2DD;)LX/2Db;

    move-result-object v1

    .line 383796
    :try_start_0
    iget-object v0, v1, LX/2Db;->e:LX/2De;

    .line 383797
    invoke-static {v0}, LX/2De;->c(LX/2De;)V

    .line 383798
    iget-boolean v2, v0, LX/2De;->b:Z

    if-nez v2, :cond_0

    .line 383799
    invoke-static {v0}, LX/2De;->c(LX/2De;)V

    .line 383800
    invoke-static {v0}, LX/2De;->a(LX/2De;)V

    .line 383801
    iget-object v2, v0, LX/2De;->a:Ljava/io/Writer;

    const-string v3, "\"data\":["

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 383802
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/2De;->b:Z

    .line 383803
    :goto_0
    iget-object v2, v0, LX/2De;->a:Ljava/io/Writer;

    invoke-virtual {p1, v2}, LX/0nA;->a(Ljava/io/Writer;)V

    .line 383804
    iget-object v0, v1, LX/2Db;->d:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    .line 383805
    iget v0, v1, LX/2Db;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, LX/2Db;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383806
    iget-object v0, v1, LX/2Db;->c:LX/2DZ;

    invoke-virtual {v0, p0}, LX/2DZ;->f(Ljava/lang/Object;)V

    .line 383807
    return-void

    .line 383808
    :catchall_0
    move-exception v0

    iget-object v1, v1, LX/2Db;->c:LX/2DZ;

    invoke-virtual {v1, p0}, LX/2DZ;->f(Ljava/lang/Object;)V

    throw v0

    .line 383809
    :cond_0
    iget-object v2, v0, LX/2De;->a:Ljava/io/Writer;

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(I)V

    goto :goto_0
.end method

.method public final a(LX/2u6;)V
    .locals 3

    .prologue
    .line 383790
    iput-object p1, p0, LX/2DD;->a:LX/2u6;

    .line 383791
    new-instance v0, LX/2DJ;

    iget-object v1, p0, LX/2DD;->g:LX/0Zh;

    iget-object v2, p0, LX/2DD;->a:LX/2u6;

    invoke-direct {v0, v1, v2}, LX/2DJ;-><init>(LX/0Zh;LX/2u6;)V

    iput-object v0, p0, LX/2DD;->c:LX/2DJ;

    .line 383792
    invoke-direct {p0}, LX/2DD;->g()V

    .line 383793
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 383786
    iget-object v0, p0, LX/2DD;->b:LX/2Db;

    if-eqz v0, :cond_0

    .line 383787
    invoke-static {p0}, LX/2DD;->f(LX/2DD;)V

    .line 383788
    invoke-direct {p0}, LX/2DD;->g()V

    .line 383789
    :cond_0
    return-void
.end method
