.class public LX/3D9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ":",
        "LX/2kl;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final c:LX/17W;

.field private final d:LX/0TD;

.field private final e:LX/2nq;

.field private final f:LX/3Cm;

.field private final g:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field private final h:LX/3DA;

.field public final i:LX/1rn;

.field public final j:Lcom/facebook/performancelogger/PerformanceLogger;

.field public final k:LX/0Sh;

.field private final l:LX/1rU;

.field private final m:LX/03V;

.field private final n:LX/1rp;

.field private final o:LX/2c4;

.field private final p:LX/19j;

.field private final q:LX/1Ns;

.field private final r:LX/3DB;

.field private final s:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 531579
    const-class v0, LX/3D9;

    sput-object v0, LX/3D9;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2nq;LX/1Pq;LX/0TD;LX/17W;LX/3Cm;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/3DA;LX/1rn;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Sh;LX/1rU;LX/03V;LX/1rp;LX/2c4;LX/19j;LX/1Ns;LX/0W3;LX/3DB;)V
    .locals 1
    .param p1    # LX/2nq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nq;",
            "TE;",
            "LX/0TD;",
            "LX/17W;",
            "LX/3Cm;",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            "Lcom/facebook/notifications/util/NotificationStoryLauncher;",
            "LX/1rn;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/1rU;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1rp;",
            "LX/2c4;",
            "LX/19j;",
            "LX/1Ns;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/3DB;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 531580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 531581
    iput-object p2, p0, LX/3D9;->b:LX/1Pq;

    .line 531582
    iput-object p4, p0, LX/3D9;->c:LX/17W;

    .line 531583
    iput-object p3, p0, LX/3D9;->d:LX/0TD;

    .line 531584
    iput-object p1, p0, LX/3D9;->e:LX/2nq;

    .line 531585
    iput-object p5, p0, LX/3D9;->f:LX/3Cm;

    .line 531586
    iput-object p6, p0, LX/3D9;->g:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 531587
    iput-object p7, p0, LX/3D9;->h:LX/3DA;

    .line 531588
    iput-object p8, p0, LX/3D9;->i:LX/1rn;

    .line 531589
    iput-object p9, p0, LX/3D9;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 531590
    iput-object p10, p0, LX/3D9;->k:LX/0Sh;

    .line 531591
    iput-object p11, p0, LX/3D9;->l:LX/1rU;

    .line 531592
    iput-object p12, p0, LX/3D9;->m:LX/03V;

    .line 531593
    iput-object p13, p0, LX/3D9;->n:LX/1rp;

    .line 531594
    iput-object p14, p0, LX/3D9;->o:LX/2c4;

    .line 531595
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3D9;->p:LX/19j;

    .line 531596
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3D9;->q:LX/1Ns;

    .line 531597
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3D9;->s:LX/0W3;

    .line 531598
    move-object/from16 v0, p18

    iput-object v0, p0, LX/3D9;->r:LX/3DB;

    .line 531599
    return-void
.end method

.method private static a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 531600
    const v0, 0x7f0d2e68

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 531601
    if-eqz v1, :cond_1

    .line 531602
    instance-of v0, v1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 531603
    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0, v2}, LX/8t0;->a(Landroid/view/ViewGroup;I)V

    .line 531604
    :cond_0
    instance-of v0, v1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz v0, :cond_1

    .line 531605
    check-cast v1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-static {v1, v2}, LX/8t0;->a(Lcom/facebook/fbui/widget/layout/ImageBlockLayout;I)V

    .line 531606
    :cond_1
    return-void
.end method

.method public static b(LX/3D9;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const v4, 0xa0030

    .line 531607
    if-nez p2, :cond_1

    .line 531608
    iget-object v0, p0, LX/3D9;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_PermalinkNotificationLoad"

    invoke-interface {v0, v4, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 531609
    iget-object v0, p0, LX/3D9;->m:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/3D9;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_null_notif_story"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Null notif story in adapter"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 531610
    :cond_0
    :goto_0
    return-void

    .line 531611
    :cond_1
    iget-object v0, p0, LX/3D9;->p:LX/19j;

    iget-boolean v0, v0, LX/19j;->am:Z

    if-eqz v0, :cond_2

    .line 531612
    iget-object v0, p0, LX/3D9;->r:LX/3DB;

    invoke-virtual {v0, p2}, LX/3DB;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 531613
    :cond_2
    iget-object v0, p0, LX/3D9;->b:LX/1Pq;

    check-cast v0, LX/2kl;

    invoke-interface {v0}, LX/2kl;->md_()LX/2jc;

    move-result-object v0

    invoke-interface {v0, p2, v1, v1}, LX/2jc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V

    .line 531614
    invoke-static {p2}, LX/3Cm;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 531615
    if-eqz v0, :cond_3

    iget-object v1, p0, LX/3D9;->c:LX/17W;

    invoke-virtual {v1, p1, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 531616
    :goto_1
    if-nez v0, :cond_0

    .line 531617
    iget-object v0, p0, LX/3D9;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_PermalinkNotificationLoad"

    invoke-interface {v0, v4, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 531618
    iget-object v0, p0, LX/3D9;->m:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/3D9;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_launch_failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not launch notification story "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 531619
    :cond_3
    iget-object v1, p0, LX/3D9;->h:LX/3DA;

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    iget-object v0, p0, LX/3D9;->b:LX/1Pq;

    check-cast v0, LX/2kk;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, LX/2kk;->e_(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, p1, v2, v0}, LX/3DA;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;I)Z

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const v9, 0xa0030

    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x9c4f0f

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 531620
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 531621
    iget-object v0, p0, LX/3D9;->e:LX/2nq;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 531622
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    .line 531623
    const-string v0, "_"

    invoke-virtual {v4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 531624
    array-length v5, v0

    if-ne v5, v8, :cond_0

    .line 531625
    const/4 v5, 0x0

    aget-object v0, v0, v5

    const-string v5, "photonotification"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3D9;->s:LX/0W3;

    sget-wide v6, LX/0X5;->eA:J

    invoke-interface {v0, v6, v7}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531626
    iget-object v0, p0, LX/3D9;->b:LX/1Pq;

    check-cast v0, LX/2kk;

    iget-object v1, p0, LX/3D9;->e:LX/2nq;

    invoke-interface {v0, v1}, LX/2kk;->a(LX/2nq;)V

    .line 531627
    iget-object v0, p0, LX/3D9;->o:LX/2c4;

    invoke-virtual {v0, v4}, LX/2c4;->a(Ljava/lang/String;)V

    .line 531628
    iget-object v0, p0, LX/3D9;->q:LX/1Ns;

    new-instance v1, LX/1Qg;

    invoke-direct {v1}, LX/1Qg;-><init>()V

    new-instance v4, LX/H4q;

    invoke-direct {v4, p0}, LX/H4q;-><init>(LX/3D9;)V

    invoke-virtual {v0, v1, v4}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v1

    .line 531629
    const-class v0, Landroid/app/Activity;

    invoke-static {v3, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const-string v3, "photonotification"

    invoke-virtual {v1, v10, v0, v3, v10}, LX/1aw;->a(Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)V

    .line 531630
    invoke-static {p1}, LX/3D9;->a(Landroid/view/View;)V

    .line 531631
    iget-object v0, p0, LX/3D9;->b:LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 531632
    const v0, -0x12d9cc0d

    invoke-static {v8, v8, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 531633
    :goto_0
    return-void

    .line 531634
    :cond_0
    iget-object v0, p0, LX/3D9;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v4, "NNF_PermalinkNotificationLoad"

    invoke-interface {v0, v9, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 531635
    iget-object v0, p0, LX/3D9;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v4, "NNF_PermalinkNotificationLoad"

    const-string v5, "NotificationsFeedClickListener"

    invoke-interface {v0, v9, v4, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->e(ILjava/lang/String;Ljava/lang/String;)V

    .line 531636
    iget-object v4, p0, LX/3D9;->j:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v5, "NNF_PermalinkNotificationLoad"

    iget-object v0, p0, LX/3D9;->l:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "connection_controller"

    :goto_1
    invoke-interface {v4, v9, v5, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->e(ILjava/lang/String;Ljava/lang/String;)V

    .line 531637
    iget-object v0, p0, LX/3D9;->b:LX/1Pq;

    check-cast v0, LX/2kk;

    iget-object v4, p0, LX/3D9;->e:LX/2nq;

    invoke-interface {v0, v4}, LX/2kk;->a(LX/2nq;)V

    .line 531638
    iget-object v0, p0, LX/3D9;->o:LX/2c4;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/2c4;->a(Ljava/lang/String;)V

    .line 531639
    iget-object v0, p0, LX/3D9;->l:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 531640
    :goto_2
    if-nez v0, :cond_4

    .line 531641
    iget-object v0, p0, LX/3D9;->i:LX/1rn;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, LX/1rn;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 531642
    iget-object v4, p0, LX/3D9;->k:LX/0Sh;

    new-instance v5, LX/H4r;

    invoke-direct {v5, p0, v3}, LX/H4r;-><init>(LX/3D9;Landroid/content/Context;)V

    invoke-virtual {v4, v0, v5}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 531643
    :goto_3
    iget-object v0, p0, LX/3D9;->e:LX/2nq;

    invoke-interface {v0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3D9;->e:LX/2nq;

    invoke-interface {v0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 531644
    iget-object v0, p0, LX/3D9;->b:LX/1Pq;

    check-cast v0, LX/2kk;

    iget-object v1, p0, LX/3D9;->e:LX/2nq;

    iget-object v3, p0, LX/3D9;->e:LX/2nq;

    invoke-static {v3}, LX/BE6;->a(LX/2nq;)Z

    move-result v3

    invoke-interface {v0, v1, v3}, LX/2kk;->a(LX/2nq;Z)Z

    .line 531645
    :cond_1
    invoke-static {p1}, LX/3D9;->a(Landroid/view/View;)V

    .line 531646
    iget-object v0, p0, LX/3D9;->b:LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 531647
    const v0, 0x1a9cc18e

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 531648
    :cond_2
    const-string v0, "not_connection_controller"

    goto :goto_1

    .line 531649
    :cond_3
    iget-object v0, p0, LX/3D9;->g:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_2

    .line 531650
    :cond_4
    invoke-static {p0, v3, v0}, LX/3D9;->b(LX/3D9;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_3
.end method
