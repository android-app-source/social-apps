.class public LX/2l8;
.super LX/2l9;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static m:LX/0Xm;


# instance fields
.field private final a:LX/2l5;

.field private final b:LX/0kX;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17X;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0ad;

.field private final f:LX/0Uh;

.field private final g:LX/2lA;

.field private final h:LX/2l3;

.field private final i:LX/17d;

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Landroid/content/ComponentName;

.field private l:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/2l5;LX/0kX;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0ad;LX/0Uh;LX/2lA;LX/2l3;LX/17d;)V
    .locals 1
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/ReactFragmentActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/2l5;",
            "LX/0kX;",
            "LX/0Ot",
            "<",
            "LX/17X;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/2lA;",
            "LX/2l3;",
            "LX/17d;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 457302
    invoke-direct {p0, p1}, LX/2l9;-><init>(Lcom/facebook/content/SecureContextHelper;)V

    .line 457303
    iput-object p3, p0, LX/2l8;->b:LX/0kX;

    .line 457304
    iput-object p2, p0, LX/2l8;->a:LX/2l5;

    .line 457305
    iput-object p5, p0, LX/2l8;->c:LX/0Or;

    .line 457306
    iput-object p4, p0, LX/2l8;->d:LX/0Ot;

    .line 457307
    iput-object p8, p0, LX/2l8;->e:LX/0ad;

    .line 457308
    invoke-interface {p6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    iput-object v0, p0, LX/2l8;->k:Landroid/content/ComponentName;

    .line 457309
    invoke-interface {p7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    iput-object v0, p0, LX/2l8;->l:Landroid/content/ComponentName;

    .line 457310
    iput-object p9, p0, LX/2l8;->f:LX/0Uh;

    .line 457311
    iput-object p10, p0, LX/2l8;->g:LX/2lA;

    .line 457312
    iput-object p11, p0, LX/2l8;->h:LX/2l3;

    .line 457313
    iput-object p12, p0, LX/2l8;->i:LX/17d;

    .line 457314
    return-void
.end method

.method public static a(LX/0QB;)LX/2l8;
    .locals 3

    .prologue
    .line 457294
    const-class v1, LX/2l8;

    monitor-enter v1

    .line 457295
    :try_start_0
    sget-object v0, LX/2l8;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 457296
    sput-object v2, LX/2l8;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 457297
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457298
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/2l8;->b(LX/0QB;)LX/2l8;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 457299
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2l8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457300
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 457301
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/2l8;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 457284
    new-instance v0, Lcom/facebook/api/feedtype/FeedType;

    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->b:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {v0, p1, v1}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    .line 457285
    invoke-direct {p0, v0}, LX/2l8;->a(Lcom/facebook/api/feedtype/FeedType;)Landroid/content/Intent;

    move-result-object v0

    .line 457286
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 457287
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 457288
    const-string v1, "feed_type_name"

    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->b:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 457289
    iget-object p0, v2, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v2, p0

    .line 457290
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457291
    const-string v1, "friend_list_feed_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457292
    const-string v1, "friend_list_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457293
    return-object v0
.end method

.method private a(Lcom/facebook/api/feedtype/FeedType;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 457280
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, LX/2l8;->k:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 457281
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->NATIVE_NEWS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 457282
    const-string v1, "feed_type"

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457283
    return-object v0
.end method

.method private a(Lcom/facebook/bookmark/model/Bookmark;LX/EhY;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 457270
    if-eqz p1, :cond_0

    const-string v0, "friend_list"

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457271
    iget-wide v0, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    invoke-static {p0, v0, v1}, LX/2l8;->a(LX/2l8;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 457272
    :goto_0
    return-object v0

    .line 457273
    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, LX/0ax;->ae:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    invoke-static {v1}, LX/2l8;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 457274
    iget-object v0, p0, LX/2l8;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17X;

    iget-object v1, p2, LX/EhY;->a:Landroid/app/Activity;

    invoke-virtual {p2}, LX/EhY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 457275
    :cond_1
    if-eqz p1, :cond_2

    sget-object v0, LX/0ax;->bY:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    invoke-static {v1}, LX/2l8;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 457276
    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, LX/2l8;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 457277
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p2, LX/EhY;->a:Landroid/app/Activity;

    const-class v2, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 457278
    const-string v1, "tab_to_show"

    const-string v2, "sync"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 457279
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/bookmark/model/Bookmark;)Z
    .locals 4

    .prologue
    .line 457315
    if-eqz p0, :cond_1

    const-wide v0, 0x11b0dc443L

    iget-wide v2, p0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const-wide v0, 0x229cf4f51d6aaL

    iget-wide v2, p0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/2l8;
    .locals 13

    .prologue
    .line 457266
    new-instance v0, LX/2l8;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, Lcom/facebook/bookmark/client/BookmarkClient;->a(LX/0QB;)Lcom/facebook/bookmark/client/BookmarkClient;

    move-result-object v2

    check-cast v2, LX/2l5;

    invoke-static {p0}, LX/0kT;->a(LX/0QB;)LX/0kX;

    move-result-object v3

    check-cast v3, LX/0kX;

    const/16 v4, 0xc49

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2fd

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0xc

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0xd

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {p0}, LX/2lA;->a(LX/0QB;)LX/2lA;

    move-result-object v10

    check-cast v10, LX/2lA;

    invoke-static {p0}, LX/2l3;->b(LX/0QB;)LX/2l3;

    move-result-object v11

    check-cast v11, LX/2l3;

    invoke-static {p0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v12

    check-cast v12, LX/17d;

    invoke-direct/range {v0 .. v12}, LX/2l8;-><init>(Lcom/facebook/content/SecureContextHelper;LX/2l5;LX/0kX;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0ad;LX/0Uh;LX/2lA;LX/2l3;LX/17d;)V

    .line 457267
    const/16 v1, 0x97

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 457268
    iput-object v1, v0, LX/2l8;->j:LX/0Ot;

    .line 457269
    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 457258
    if-eqz p0, :cond_1

    .line 457259
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 457260
    if-eqz v0, :cond_1

    .line 457261
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 457262
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 457263
    :goto_0
    return-object v0

    .line 457264
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 457265
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/EhY;Landroid/content/Intent;)V
    .locals 4
    .param p2    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 457244
    invoke-super {p0, p1, p2}, LX/2l9;->a(LX/EhY;Landroid/content/Intent;)V

    .line 457245
    const-string v0, "local_module"

    .line 457246
    if-nez p2, :cond_4

    .line 457247
    const-string v0, "error"

    .line 457248
    :cond_0
    :goto_0
    iget-object v1, p0, LX/2l8;->h:LX/2l3;

    iget-object v2, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    invoke-virtual {v1, v2}, LX/2l3;->a(Lcom/facebook/bookmark/model/Bookmark;)V

    .line 457249
    iget-object v1, p0, LX/2l8;->h:LX/2l3;

    invoke-virtual {v1, p1, v0}, LX/2l3;->a(LX/EhY;Ljava/lang/String;)V

    .line 457250
    if-eqz p2, :cond_1

    iget-object v0, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    if-eqz v0, :cond_1

    const-string v0, "group"

    iget-object v1, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget-object v1, v1, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457251
    const-string v0, "group_feed_title"

    iget-object v1, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    invoke-virtual {v1}, Lcom/facebook/bookmark/model/Bookmark;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457252
    :cond_1
    if-eqz p2, :cond_3

    iget-object v0, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    if-eqz v0, :cond_3

    iget-object v0, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    invoke-virtual {v0}, Lcom/facebook/bookmark/model/Bookmark;->b()I

    move-result v0

    if-lez v0, :cond_3

    const-string v0, "group"

    iget-object v1, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget-object v1, v1, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "friend_list"

    iget-object v1, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget-object v1, v1, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-wide v0, 0x21531ffed86f8L

    iget-object v2, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v2, v2, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    const-wide v0, 0x8bb78869L

    iget-object v2, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v2, v2, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 457253
    :cond_2
    iget-object v0, p0, LX/2l8;->a:LX/2l5;

    iget-object v1, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v2, v1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/2l5;->a(JI)V

    .line 457254
    :cond_3
    return-void

    .line 457255
    :cond_4
    const-string v1, "application_link_type"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 457256
    const-string v0, "application_link_type"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 457257
    const-string v1, "application_link_type"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final b(LX/EhY;)Landroid/content/Intent;
    .locals 14

    .prologue
    const-wide v12, 0xc63f291b142bL

    const/4 v10, 0x0

    .line 457205
    iget-object v9, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    .line 457206
    invoke-static {v9}, LX/2l8;->a(Lcom/facebook/bookmark/model/Bookmark;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457207
    const-wide v0, 0x229cf4f51d6aaL

    iget-wide v2, v9, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->a:Lcom/facebook/api/feedtype/FeedType;

    :goto_0
    invoke-direct {p0, v0}, LX/2l8;->a(Lcom/facebook/api/feedtype/FeedType;)Landroid/content/Intent;

    move-result-object v10

    .line 457208
    :goto_1
    return-object v10

    .line 457209
    :cond_0
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    goto :goto_0

    .line 457210
    :cond_1
    iget-boolean v0, p1, LX/EhY;->d:Z

    if-eqz v0, :cond_2

    .line 457211
    invoke-direct {p0, v9, p1}, LX/2l8;->a(Lcom/facebook/bookmark/model/Bookmark;LX/EhY;)Landroid/content/Intent;

    move-result-object v10

    goto :goto_1

    .line 457212
    :cond_2
    invoke-virtual {p1}, LX/EhY;->b()Ljava/lang/String;

    move-result-object v11

    .line 457213
    if-eqz v9, :cond_3

    iget-wide v0, v9, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v12, v0

    if-nez v0, :cond_3

    .line 457214
    iget-object v0, p1, LX/EhY;->e:Landroid/os/Parcelable;

    move-object v0, v0

    .line 457215
    instance-of v0, v0, Landroid/net/Uri;

    .line 457216
    if-eqz v0, :cond_3

    .line 457217
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 457218
    iget-object v0, p1, LX/EhY;->e:Landroid/os/Parcelable;

    move-object v0, v0

    .line 457219
    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v10

    goto :goto_1

    .line 457220
    :cond_3
    if-eqz v9, :cond_4

    .line 457221
    iget-wide v0, v9, Lcom/facebook/bookmark/model/Bookmark;->id:J

    iget-object v2, p1, LX/EhY;->a:Landroid/app/Activity;

    iget-object v3, p0, LX/2l8;->e:LX/0ad;

    iget-object v4, p0, LX/2l8;->l:Landroid/content/ComponentName;

    iget-object v5, p0, LX/2l8;->k:Landroid/content/ComponentName;

    iget-object v6, p0, LX/2l8;->f:LX/0Uh;

    iget-object v7, p0, LX/2l8;->d:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/17X;

    iget-object v8, p0, LX/2l8;->g:LX/2lA;

    invoke-static/range {v0 .. v8}, LX/FBl;->a(JLandroid/app/Activity;LX/0ad;Landroid/content/ComponentName;Landroid/content/ComponentName;LX/0Uh;LX/17X;LX/2lA;)Landroid/content/Intent;

    move-result-object v1

    .line 457222
    if-eqz v1, :cond_4

    move-object v10, v1

    .line 457223
    goto :goto_1

    .line 457224
    :cond_4
    if-eqz v9, :cond_5

    const-string v0, "friend_list"

    iget-object v1, v9, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 457225
    iget-wide v0, v9, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v9, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    invoke-static {p0, v0, v1}, LX/2l8;->a(LX/2l8;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    goto :goto_1

    .line 457226
    :cond_5
    if-nez v11, :cond_7

    .line 457227
    iget-object v0, p1, LX/EhY;->a:Landroid/app/Activity;

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 457228
    const-string v2, "bookmarks"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "The bookmark with id: "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    if-nez v1, :cond_6

    move-object v1, v10

    :goto_2
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " does not have url"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    iget-object v1, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v4, v1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_2

    .line 457229
    :cond_7
    instance-of v0, v9, LX/EhI;

    if-eqz v0, :cond_c

    move-object v0, v9

    .line 457230
    check-cast v0, LX/EhI;

    invoke-interface {v0}, LX/EhI;->a()Landroid/content/Intent;

    move-result-object v0

    .line 457231
    :goto_3
    if-nez v0, :cond_a

    .line 457232
    iget-object v0, p0, LX/2l8;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17X;

    iget-object v1, p1, LX/EhY;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1, v11}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 457233
    if-nez v0, :cond_b

    iget-object v1, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v2, v1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v1, v2, v12

    if-nez v1, :cond_b

    .line 457234
    const-string v1, "https://m.facebook.com/messages/"

    .line 457235
    iget-object v0, p0, LX/2l8;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17X;

    iget-object v2, p1, LX/EhY;->a:Landroid/app/Activity;

    invoke-virtual {v0, v2, v1}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 457236
    :goto_4
    if-nez v0, :cond_a

    .line 457237
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object v1, v0

    .line 457238
    :goto_5
    iget-object v0, p1, LX/EhY;->a:Landroid/app/Activity;

    iget-object v2, p0, LX/2l8;->j:LX/0Ot;

    invoke-static {v0, v1, v2}, LX/17d;->a(Landroid/content/Context;Landroid/content/Intent;LX/0Ot;)V

    .line 457239
    iget-object v0, p1, LX/EhY;->a:Landroid/app/Activity;

    if-eqz v0, :cond_8

    iget-object v0, p1, LX/EhY;->a:Landroid/app/Activity;

    instance-of v0, v0, LX/2A2;

    if-eqz v0, :cond_8

    .line 457240
    iget-object v0, p1, LX/EhY;->a:Landroid/app/Activity;

    check-cast v0, LX/2A2;

    invoke-interface {v0}, LX/2A2;->b()LX/0kX;

    move-result-object v0

    invoke-virtual {v0}, LX/0kX;->e()V

    .line 457241
    :cond_8
    if-eqz v9, :cond_9

    iget-wide v2, v9, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v4, 0xa025af54ad8aL

    cmp-long v0, v2, v4

    if-nez v0, :cond_9

    .line 457242
    const-string v0, "nearby_places_entry"

    sget-object v2, LX/CQB;->BOOKMARK:LX/CQB;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_9
    move-object v10, v1

    .line 457243
    goto/16 :goto_1

    :cond_a
    move-object v1, v0

    goto :goto_5

    :cond_b
    move-object v1, v11

    goto :goto_4

    :cond_c
    move-object v0, v10

    goto :goto_3
.end method

.method public final c(LX/EhY;)Z
    .locals 2

    .prologue
    .line 457197
    invoke-virtual {p1}, LX/EhY;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/0ax;->ar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457198
    iget-object v0, p1, LX/EhY;->a:Landroid/app/Activity;

    invoke-static {v0}, LX/14o;->a(Landroid/content/Context;)LX/0gh;

    move-result-object v0

    .line 457199
    const-string v1, "via_sidebar"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 457200
    iget-object v0, p0, LX/2l8;->h:LX/2l3;

    const-string v1, "local_module"

    invoke-virtual {v0, p1, v1}, LX/2l3;->a(LX/EhY;Ljava/lang/String;)V

    .line 457201
    iget-object v0, p0, LX/2l8;->h:LX/2l3;

    iget-object v1, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    invoke-virtual {v0, v1}, LX/2l3;->a(Lcom/facebook/bookmark/model/Bookmark;)V

    .line 457202
    iget-object v0, p0, LX/2l8;->b:LX/0kX;

    invoke-virtual {v0}, LX/0kX;->f()V

    .line 457203
    const/4 v0, 0x1

    .line 457204
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/2l9;->c(LX/EhY;)Z

    move-result v0

    goto :goto_0
.end method
