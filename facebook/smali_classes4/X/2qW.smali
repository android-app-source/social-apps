.class public abstract LX/2qW;
.super Landroid/view/TextureView;
.source ""


# instance fields
.field public final a:Landroid/os/Handler;

.field public b:Z

.field public c:LX/7Cy;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 471182
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/2qW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 471183
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 471184
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/2qW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 471185
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 471186
    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 471187
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/2qW;->a:Landroid/os/Handler;

    .line 471188
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2qW;->b:Z

    .line 471189
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/view/TextureView$SurfaceTextureListener;)LX/7Cy;
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 471190
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    .line 471191
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    invoke-virtual {v0}, LX/7Cy;->a()V

    .line 471192
    :cond_0
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 471193
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471194
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471195
    iget-boolean v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->M:Z

    if-nez v1, :cond_1

    .line 471196
    :cond_0
    :goto_0
    return-void

    .line 471197
    :cond_1
    iget v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    const/high16 p0, 0x40000000    # 2.0f

    sub-float/2addr p0, p1

    mul-float/2addr v1, p0

    .line 471198
    iget p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->o:F

    invoke-static {v1, p0}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 471199
    iget p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->p:F

    invoke-static {v1, p0}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 471200
    move v1, v1

    .line 471201
    const/4 p0, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/facebook/spherical/GlMediaRenderThread;->a(FZ)V

    .line 471202
    goto :goto_0
.end method

.method public final a(FF)V
    .locals 2

    .prologue
    .line 471203
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v0, :cond_1

    .line 471204
    :cond_0
    invoke-virtual {p0}, LX/2qW;->getRenderThreadParams()LX/7Cz;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, LX/7Cz;->j:Ljava/lang/Float;

    .line 471205
    invoke-virtual {p0}, LX/2qW;->getRenderThreadParams()LX/7Cz;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, LX/7Cz;->i:Ljava/lang/Float;

    .line 471206
    :goto_0
    return-void

    .line 471207
    :cond_1
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/spherical/GlMediaRenderThread;->a(FF)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 471261
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471262
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471263
    iget-object p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    instance-of p0, p0, LX/7Cu;

    if-eqz p0, :cond_0

    .line 471264
    iget-object p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    check-cast p0, LX/7Cu;

    .line 471265
    iput-boolean p1, p0, LX/7Cu;->v:Z

    .line 471266
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 471208
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471209
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-virtual {v0}, Lcom/facebook/spherical/GlMediaRenderThread;->c()V

    .line 471210
    :cond_0
    return-void
.end method

.method public final c(FF)V
    .locals 6

    .prologue
    .line 471211
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471212
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471213
    sget-object v1, Lcom/facebook/spherical/GlMediaRenderThread;->C:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 471214
    neg-float v2, p1

    iget v3, v0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    iget v4, v0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    invoke-static {v2, v3, v4}, LX/7Cq;->a(FIF)F

    move-result v2

    .line 471215
    neg-float v3, p2

    iget v4, v0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    iget v5, v0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    invoke-static {v3, v4, v5}, LX/7Cq;->a(FIF)F

    move-result v3

    .line 471216
    iget v4, v0, Lcom/facebook/spherical/GlMediaRenderThread;->z:I

    int-to-float v4, v4

    add-float/2addr v4, v2

    float-to-int v4, v4

    iput v4, v0, Lcom/facebook/spherical/GlMediaRenderThread;->z:I

    .line 471217
    iget v4, v0, Lcom/facebook/spherical/GlMediaRenderThread;->A:I

    int-to-float v4, v4

    add-float/2addr v4, v3

    float-to-int v4, v4

    iput v4, v0, Lcom/facebook/spherical/GlMediaRenderThread;->A:I

    .line 471218
    iget-object v4, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v4, v4, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 471219
    iget-object v4, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    invoke-virtual {v4, v2, v3}, LX/7Ce;->b(FF)V

    .line 471220
    iget-object v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v2, v2, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 471221
    iget-object v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget-object v2, v2, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 471222
    iget-object v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iput-wide v3, v2, LX/3IO;->g:J

    .line 471223
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget-object v1, v1, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 471224
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 471225
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471226
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    invoke-static {v0}, LX/7Cy;->c(LX/7Cy;)V

    .line 471227
    :cond_0
    return-void
.end method

.method public final d(FF)V
    .locals 10

    .prologue
    .line 471228
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471229
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471230
    iget v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    iget v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    invoke-static {p1, v1, v2}, LX/7Cq;->a(FIF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 471231
    iget v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    iget v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    invoke-static {p2, v1, v2}, LX/7Cq;->a(FIF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 471232
    iget v2, v0, Lcom/facebook/spherical/GlMediaRenderThread;->z:I

    .line 471233
    iget v3, v0, Lcom/facebook/spherical/GlMediaRenderThread;->A:I

    .line 471234
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->H:Landroid/widget/Scroller;

    add-int/lit16 v6, v2, -0x2d0

    add-int/lit16 v7, v2, 0x2d0

    add-int/lit16 v8, v3, -0x2d0

    add-int/lit16 v9, v3, 0x2d0

    invoke-virtual/range {v1 .. v9}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 471235
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 471236
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471237
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    .line 471238
    iget-object v1, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v1, :cond_0

    .line 471239
    iget-object v1, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471240
    iget-object p0, v1, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    if-eqz p0, :cond_0

    .line 471241
    iget-object p0, v1, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 471242
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 471243
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_1

    .line 471244
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471245
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    instance-of v1, v1, LX/7Cu;

    if-eqz v1, :cond_0

    .line 471246
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    check-cast v1, LX/7Cu;

    const/4 p0, 0x0

    .line 471247
    iget-object v2, v1, LX/7Cu;->y:LX/0wd;

    invoke-virtual {v2}, LX/0wd;->k()LX/0wd;

    .line 471248
    iget-object v2, v1, LX/7Cu;->z:LX/0wd;

    invoke-virtual {v2}, LX/0wd;->k()LX/0wd;

    .line 471249
    iput-boolean p0, v1, LX/7Cu;->A:Z

    .line 471250
    iput-boolean p0, v1, LX/7Cu;->B:Z

    .line 471251
    :cond_0
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->H:Landroid/widget/Scroller;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 471252
    :cond_1
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 471253
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471254
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471255
    iget-boolean p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->M:Z

    if-nez p0, :cond_1

    .line 471256
    :cond_0
    :goto_0
    return-void

    .line 471257
    :cond_1
    iget-object p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    instance-of p0, p0, LX/7Cu;

    if-eqz p0, :cond_0

    .line 471258
    iget-object p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    check-cast p0, LX/7Cu;

    .line 471259
    invoke-virtual {p0}, LX/7Cu;->e()V

    .line 471260
    goto :goto_0
.end method

.method public getFov()F
    .locals 1

    .prologue
    .line 471172
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v0, :cond_1

    .line 471173
    :cond_0
    const/high16 v0, 0x428c0000    # 70.0f

    .line 471174
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471175
    iget p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    move v0, p0

    .line 471176
    goto :goto_0
.end method

.method public getPitch()F
    .locals 1

    .prologue
    .line 471177
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v0, :cond_1

    .line 471178
    :cond_0
    const/4 v0, 0x0

    .line 471179
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471180
    iget-object p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget p0, p0, LX/3IO;->d:F

    move v0, p0

    .line 471181
    goto :goto_0
.end method

.method public getRenderMethod()LX/7DC;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 471083
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471084
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471085
    iget-object p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    invoke-interface {p0}, LX/7D0;->d()LX/7DC;

    move-result-object p0

    move-object v0, p0

    .line 471086
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract getRenderThreadParams()LX/7Cz;
.end method

.method public getRoll()F
    .locals 1

    .prologue
    .line 471087
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v0, :cond_1

    .line 471088
    :cond_0
    const/4 v0, 0x0

    .line 471089
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471090
    iget-object p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget p0, p0, LX/3IO;->f:F

    move v0, p0

    .line 471091
    goto :goto_0
.end method

.method public getRotationMatrix()[F
    .locals 9

    .prologue
    .line 471092
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v0, :cond_1

    .line 471093
    :cond_0
    const/16 v0, 0x10

    new-array v0, v0, [F

    .line 471094
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 471095
    const/16 v1, 0x10

    new-array v1, v1, [F

    .line 471096
    iget-object v3, v0, Lcom/facebook/spherical/GlMediaRenderThread;->K:[F

    iget v4, v0, Lcom/facebook/spherical/GlMediaRenderThread;->m:F

    neg-float v5, v4

    const/high16 v7, 0x3f800000    # 1.0f

    move v4, v2

    move v8, v6

    invoke-static/range {v1 .. v8}, Landroid/opengl/Matrix;->rotateM([FI[FIFFFF)V

    .line 471097
    move-object v0, v1

    .line 471098
    goto :goto_0
.end method

.method public getYaw()F
    .locals 1

    .prologue
    .line 471099
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v0, :cond_1

    .line 471100
    :cond_0
    const/4 v0, 0x0

    .line 471101
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471102
    iget-object p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget p0, p0, LX/3IO;->e:F

    move v0, p0

    .line 471103
    goto :goto_0
.end method

.method public setGuideActive(Z)V
    .locals 1

    .prologue
    .line 471104
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471105
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471106
    iget-object p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object p0, p0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 471107
    iget-object p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    .line 471108
    iput-boolean p1, p0, LX/7Ce;->r:Z

    .line 471109
    iget-object p0, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object p0, p0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 471110
    :cond_0
    return-void
.end method

.method public setMaxVerticalFOV(F)V
    .locals 2

    .prologue
    .line 471111
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v0, :cond_1

    .line 471112
    :cond_0
    invoke-virtual {p0}, LX/2qW;->getRenderThreadParams()LX/7Cz;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, LX/7Cz;->h:Ljava/lang/Float;

    .line 471113
    :goto_0
    return-void

    .line 471114
    :cond_1
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-virtual {v0, p1}, Lcom/facebook/spherical/GlMediaRenderThread;->a(F)V

    goto :goto_0
.end method

.method public setPreferredVerticalFOV(F)V
    .locals 2

    .prologue
    .line 471115
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v0, :cond_1

    .line 471116
    :cond_0
    invoke-virtual {p0}, LX/2qW;->getRenderThreadParams()LX/7Cz;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, LX/7Cz;->g:Ljava/lang/Float;

    .line 471117
    :goto_0
    return-void

    .line 471118
    :cond_1
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/facebook/spherical/GlMediaRenderThread;->a(FZ)V

    goto :goto_0
.end method

.method public setPreferredVerticalFOVOnZoom(F)V
    .locals 2

    .prologue
    .line 471119
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471120
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/spherical/GlMediaRenderThread;->a(FZ)V

    .line 471121
    :cond_0
    return-void
.end method

.method public setProjectionType(LX/19o;)V
    .locals 1

    .prologue
    .line 471122
    invoke-virtual {p0}, LX/2qW;->getRenderThreadParams()LX/7Cz;

    move-result-object v0

    iput-object p1, v0, LX/7Cz;->b:LX/19o;

    .line 471123
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471124
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    invoke-virtual {v0, p1}, Lcom/facebook/spherical/GlMediaRenderThread;->a(LX/19o;)V

    .line 471125
    :cond_0
    return-void
.end method

.method public setRenderAxisRotation(LX/7Cm;)V
    .locals 2

    .prologue
    .line 471126
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz v0, :cond_0

    .line 471127
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471128
    iput-object p1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->r:LX/7Cm;

    .line 471129
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    if-nez v1, :cond_1

    .line 471130
    :cond_0
    :goto_0
    return-void

    .line 471131
    :cond_1
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->r:LX/7Cm;

    sget-object p0, LX/7Cm;->RENDER_AXIS_ROTATE_90_LEFT:LX/7Cm;

    if-ne v1, p0, :cond_2

    .line 471132
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    const/4 p0, 0x1

    .line 471133
    iput p0, v1, LX/7Ce;->s:I

    .line 471134
    goto :goto_0

    .line 471135
    :cond_2
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->r:LX/7Cm;

    sget-object p0, LX/7Cm;->RENDER_AXIS_ROTATE_90_RIGHT:LX/7Cm;

    if-ne v1, p0, :cond_3

    .line 471136
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    const/4 p0, 0x3

    .line 471137
    iput p0, v1, LX/7Ce;->s:I

    .line 471138
    goto :goto_0

    .line 471139
    :cond_3
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->r:LX/7Cm;

    sget-object p0, LX/7Cm;->RENDER_AXIS_ROTATE_0_PORTRAIT:LX/7Cm;

    if-ne v1, p0, :cond_4

    .line 471140
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    const/4 p0, 0x0

    .line 471141
    iput p0, v1, LX/7Ce;->s:I

    .line 471142
    goto :goto_0

    .line 471143
    :cond_4
    iget-object v1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    const/4 p0, -0x1

    .line 471144
    iput p0, v1, LX/7Ce;->s:I

    .line 471145
    goto :goto_0
.end method

.method public setRendererBounds(LX/7DG;)V
    .locals 1

    .prologue
    .line 471146
    invoke-virtual {p0}, LX/2qW;->getRenderThreadParams()LX/7Cz;

    move-result-object v0

    iput-object p1, v0, LX/7Cz;->a:LX/7DG;

    .line 471147
    return-void
.end method

.method public setShouldUseFullScreenControl(Z)V
    .locals 0

    .prologue
    .line 471148
    iput-boolean p1, p0, LX/2qW;->b:Z

    .line 471149
    return-void
.end method

.method public setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 471150
    if-eqz p1, :cond_1

    .line 471151
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 471152
    invoke-virtual {p0, p1}, LX/2qW;->a(Landroid/view/TextureView$SurfaceTextureListener;)LX/7Cy;

    move-result-object v0

    iput-object v0, p0, LX/2qW;->c:LX/7Cy;

    .line 471153
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    invoke-super {p0, v0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 471154
    :goto_1
    return-void

    .line 471155
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 471156
    :cond_1
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_2

    .line 471157
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    invoke-static {v0}, LX/7Cy;->c(LX/7Cy;)V

    .line 471158
    iput-object v1, p0, LX/2qW;->c:LX/7Cy;

    .line 471159
    :cond_2
    invoke-super {p0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    goto :goto_1
.end method

.method public setTiltInteractionEnabled(Z)V
    .locals 1

    .prologue
    .line 471160
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v0, :cond_1

    .line 471161
    :cond_0
    invoke-virtual {p0}, LX/2qW;->getRenderThreadParams()LX/7Cz;

    move-result-object v0

    iput-boolean p1, v0, LX/7Cz;->f:Z

    .line 471162
    :goto_0
    return-void

    .line 471163
    :cond_1
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471164
    iput-boolean p1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->q:Z

    .line 471165
    goto :goto_0
.end method

.method public setZoomInteractionEnabled(Z)V
    .locals 1

    .prologue
    .line 471166
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-nez v0, :cond_1

    .line 471167
    :cond_0
    invoke-virtual {p0}, LX/2qW;->getRenderThreadParams()LX/7Cz;

    move-result-object v0

    iput-boolean p1, v0, LX/7Cz;->e:Z

    .line 471168
    :goto_0
    return-void

    .line 471169
    :cond_1
    iget-object v0, p0, LX/2qW;->c:LX/7Cy;

    iget-object v0, v0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 471170
    iput-boolean p1, v0, Lcom/facebook/spherical/GlMediaRenderThread;->M:Z

    .line 471171
    goto :goto_0
.end method
