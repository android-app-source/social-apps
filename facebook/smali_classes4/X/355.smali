.class public LX/355;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field public final a:LX/1Vm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Vm",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B35;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B4O;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1nG;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/356;

.field public final g:LX/0ad;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Nq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Vm;LX/0Or;LX/0Or;LX/1nG;LX/0Or;LX/356;LX/0ad;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Vm;",
            "LX/0Or",
            "<",
            "LX/B35;",
            ">;",
            "LX/0Or",
            "<",
            "LX/B4O;",
            ">;",
            "LX/1nG;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/356;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Nq;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 496589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496590
    iput-object p1, p0, LX/355;->a:LX/1Vm;

    .line 496591
    iput-object p2, p0, LX/355;->b:LX/0Or;

    .line 496592
    iput-object p3, p0, LX/355;->c:LX/0Or;

    .line 496593
    iput-object p4, p0, LX/355;->d:LX/1nG;

    .line 496594
    iput-object p5, p0, LX/355;->e:LX/0Or;

    .line 496595
    iput-object p6, p0, LX/355;->f:LX/356;

    .line 496596
    iput-object p7, p0, LX/355;->g:LX/0ad;

    .line 496597
    iput-object p8, p0, LX/355;->h:LX/0Ot;

    .line 496598
    iput-object p9, p0, LX/355;->i:LX/0Ot;

    .line 496599
    return-void
.end method

.method public static a(LX/0QB;)LX/355;
    .locals 13

    .prologue
    .line 496600
    const-class v1, LX/355;

    monitor-enter v1

    .line 496601
    :try_start_0
    sget-object v0, LX/355;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 496602
    sput-object v2, LX/355;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 496603
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496604
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 496605
    new-instance v3, LX/355;

    invoke-static {v0}, LX/1Vm;->a(LX/0QB;)LX/1Vm;

    move-result-object v4

    check-cast v4, LX/1Vm;

    const/16 v5, 0x24de

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x24ec

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v7

    check-cast v7, LX/1nG;

    const/16 v8, 0xbc6

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/356;->a(LX/0QB;)LX/356;

    move-result-object v9

    check-cast v9, LX/356;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    const/16 v11, 0x3be

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xbdd

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, LX/355;-><init>(LX/1Vm;LX/0Or;LX/0Or;LX/1nG;LX/0Or;LX/356;LX/0ad;LX/0Ot;LX/0Ot;)V

    .line 496606
    move-object v0, v3

    .line 496607
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 496608
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/355;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 496609
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 496610
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
