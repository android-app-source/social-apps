.class public LX/2Oj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field private static volatile e:LX/2Oj;


# instance fields
.field public final a:LX/0W9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 403024
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Oj;->b:Ljava/lang/String;

    .line 403025
    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Oj;->c:Ljava/lang/String;

    .line 403026
    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Oj;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 403027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403028
    iput-object p1, p0, LX/2Oj;->a:LX/0W9;

    .line 403029
    return-void
.end method

.method public static a(LX/0QB;)LX/2Oj;
    .locals 4

    .prologue
    .line 403030
    sget-object v0, LX/2Oj;->e:LX/2Oj;

    if-nez v0, :cond_1

    .line 403031
    const-class v1, LX/2Oj;

    monitor-enter v1

    .line 403032
    :try_start_0
    sget-object v0, LX/2Oj;->e:LX/2Oj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 403033
    if-eqz v2, :cond_0

    .line 403034
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 403035
    new-instance p0, LX/2Oj;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-direct {p0, v3}, LX/2Oj;-><init>(LX/0W9;)V

    .line 403036
    move-object v0, p0

    .line 403037
    sput-object v0, LX/2Oj;->e:LX/2Oj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403038
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 403039
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 403040
    :cond_1
    sget-object v0, LX/2Oj;->e:LX/2Oj;

    return-object v0

    .line 403041
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 403042
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/user/model/Name;Lcom/facebook/user/model/Name;)Ljava/lang/String;
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 403043
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 403044
    const-string v0, ""

    .line 403045
    if-eqz p1, :cond_0

    .line 403046
    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    .line 403047
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 403048
    invoke-virtual {p0}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v0

    .line 403049
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/User;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 403050
    if-nez p1, :cond_1

    .line 403051
    const/4 v0, 0x0

    .line 403052
    :cond_0
    :goto_0
    return-object v0

    .line 403053
    :cond_1
    iget-object v0, p0, LX/2Oj;->a:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 403054
    sget-object v1, LX/2Oj;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, LX/2Oj;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, LX/2Oj;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 403055
    if-eqz v0, :cond_3

    .line 403056
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v0

    .line 403057
    if-nez v0, :cond_0

    .line 403058
    :cond_3
    iget-object v0, p1, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 403059
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 403060
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v1

    .line 403061
    :goto_2
    move-object v0, v1

    .line 403062
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 403063
    :goto_3
    move-object v0, v0

    .line 403064
    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 403065
    :cond_5
    iget-object v0, p1, Lcom/facebook/user/model/User;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 403066
    if-eqz v0, :cond_6

    .line 403067
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->s()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 403068
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 403069
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->e()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 403070
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 403071
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->h()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 403072
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 403073
    :cond_9
    const/4 v1, 0x0

    goto :goto_2

    :cond_a
    const/4 v0, 0x0

    goto :goto_4
.end method
