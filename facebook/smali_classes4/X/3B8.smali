.class public LX/3B8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3B9;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 527436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewParent;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 527433
    instance-of v0, p1, LX/0vZ;

    if-eqz v0, :cond_0

    .line 527434
    check-cast p1, LX/0vZ;

    invoke-interface {p1, p2}, LX/0vZ;->onStopNestedScroll(Landroid/view/View;)V

    .line 527435
    :cond_0
    return-void
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;IIII)V
    .locals 6

    .prologue
    .line 527430
    instance-of v0, p1, LX/0vZ;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 527431
    check-cast v0, LX/0vZ;

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-interface/range {v0 .. v5}, LX/0vZ;->onNestedScroll(Landroid/view/View;IIII)V

    .line 527432
    :cond_0
    return-void
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;II[I)V
    .locals 1

    .prologue
    .line 527427
    instance-of v0, p1, LX/0vZ;

    if-eqz v0, :cond_0

    .line 527428
    check-cast p1, LX/0vZ;

    invoke-interface {p1, p2, p3, p4, p5}, LX/0vZ;->onNestedPreScroll(Landroid/view/View;II[I)V

    .line 527429
    :cond_0
    return-void
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;FF)Z
    .locals 1

    .prologue
    .line 527437
    instance-of v0, p1, LX/0vZ;

    if-eqz v0, :cond_0

    .line 527438
    check-cast p1, LX/0vZ;

    invoke-interface {p1, p2, p3, p4}, LX/0vZ;->onNestedPreFling(Landroid/view/View;FF)Z

    move-result v0

    .line 527439
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z
    .locals 1

    .prologue
    .line 527424
    instance-of v0, p1, LX/0vZ;

    if-eqz v0, :cond_0

    .line 527425
    check-cast p1, LX/0vZ;

    invoke-interface {p1, p2, p3, p4, p5}, LX/0vZ;->onNestedFling(Landroid/view/View;FFZ)Z

    move-result v0

    .line 527426
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 527421
    instance-of v0, p1, LX/0vZ;

    if-eqz v0, :cond_0

    .line 527422
    check-cast p1, LX/0vZ;

    invoke-interface {p1, p2, p3, p4}, LX/0vZ;->onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z

    move-result v0

    .line 527423
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .prologue
    .line 527415
    if-nez p2, :cond_0

    .line 527416
    const/4 v0, 0x0

    .line 527417
    :goto_0
    return v0

    .line 527418
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 527419
    invoke-virtual {v0, p3}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 527420
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 527412
    instance-of v0, p1, LX/0vZ;

    if-eqz v0, :cond_0

    .line 527413
    check-cast p1, LX/0vZ;

    invoke-interface {p1, p2, p3, p4}, LX/0vZ;->onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V

    .line 527414
    :cond_0
    return-void
.end method
