.class public final LX/2p4;
.super Landroid/widget/RelativeLayout$LayoutParams;
.source ""


# instance fields
.field public a:LX/2p5;

.field public b:I

.field public c:LX/2p6;

.field public d:I

.field public e:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 467601
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 467602
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 467603
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 467604
    sget-object v0, LX/03r;->AnchorLayout_Layout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 467605
    invoke-static {}, LX/2p5;->values()[LX/2p5;

    move-result-object v1

    const/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, LX/2p4;->a:LX/2p5;

    .line 467606
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, LX/2p4;->b:I

    .line 467607
    invoke-static {}, LX/2p6;->values()[LX/2p6;

    move-result-object v1

    const/16 v2, 0x2

    sget-object v3, LX/2p6;->CENTER:LX/2p6;

    invoke-virtual {v3}, LX/2p6;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, LX/2p4;->c:LX/2p6;

    .line 467608
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 467609
    return-void
.end method
