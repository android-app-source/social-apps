.class public abstract LX/34o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/34p;


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/03V;

.field public final d:LX/16H;

.field public final e:LX/0iA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 495743
    const-class v0, LX/34o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/34o;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Rf;LX/16H;LX/0iA;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/16H;",
            "LX/0iA;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 495736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495737
    iput-object p1, p0, LX/34o;->c:LX/03V;

    .line 495738
    iput-object p2, p0, LX/34o;->b:LX/0Rf;

    .line 495739
    iput-object p3, p0, LX/34o;->d:LX/16H;

    .line 495740
    iput-object p4, p0, LX/34o;->e:LX/0iA;

    .line 495741
    iput-object p5, p0, LX/34o;->a:Ljava/lang/String;

    .line 495742
    return-void
.end method

.method public static a(LX/34o;LX/0am;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 495728
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 495729
    iget-object v0, p0, LX/34o;->c:LX/03V;

    sget-object v2, LX/34o;->f:Ljava/lang/String;

    const-string v3, "The caret nux tooltip is shown on a non-story feed unit."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 495730
    :goto_0
    return v0

    .line 495731
    :cond_0
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 495732
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 495733
    :cond_1
    iget-object v0, p0, LX/34o;->c:LX/03V;

    sget-object v2, LX/34o;->f:Ljava/lang/String;

    const-string v3, "The caret nux tooltip is shown on a feed unit without enough save info."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 495734
    goto :goto_0

    .line 495735
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 0

    .prologue
    .line 495698
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 495727
    iget-object v0, p0, LX/34o;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 7

    .prologue
    .line 495716
    iget-object v0, p0, LX/34o;->e:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "2862"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 495717
    invoke-virtual {p0, p1}, LX/34o;->d(Lcom/facebook/graphql/model/FeedUnit;)LX/0am;

    move-result-object v0

    .line 495718
    invoke-static {p0, v0}, LX/34o;->a(LX/34o;LX/0am;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 495719
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 495720
    iget-object v0, p0, LX/34o;->d:LX/16H;

    const-string v1, "native_newsfeed"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "native_story"

    const-string v4, "caret_nux"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->k()Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    move-result-object v5

    .line 495721
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "saved_caret_nux_imp"

    invoke-direct {v6, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 495722
    iput-object v1, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 495723
    move-object v6, v6

    .line 495724
    const-string p0, "story_id"

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "surface"

    invoke-virtual {v6, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "mechanism"

    invoke-virtual {v6, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "nux_type"

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 495725
    iget-object p0, v0, LX/16H;->a:LX/0Zb;

    invoke-interface {p0, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 495726
    :cond_0
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 8

    .prologue
    .line 495709
    invoke-virtual {p0, p1}, LX/34o;->d(Lcom/facebook/graphql/model/FeedUnit;)LX/0am;

    move-result-object v0

    .line 495710
    invoke-static {p0, v0}, LX/34o;->a(LX/34o;LX/0am;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 495711
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 495712
    iget-object v0, p0, LX/34o;->d:LX/16H;

    const-string v1, "native_newsfeed"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "native_story"

    const-string v4, "caret_nux"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->k()Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    move-result-object v5

    const/4 p1, 0x0

    .line 495713
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v6

    const-string v7, "action_name"

    const-string p0, "saved_caret_nux_clicked"

    invoke-virtual {v6, v7, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v6

    const-string v7, "story_id"

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v6

    const-string v7, "surface"

    invoke-virtual {v6, v7, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v6

    const-string v7, "mechanism"

    invoke-virtual {v6, v7, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v6

    const-string v7, "nux_type"

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v6

    const-string v7, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v6

    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;

    move-result-object v6

    .line 495714
    iget-object v7, v0, LX/16H;->b:LX/0gh;

    invoke-virtual {v7, v1, p1, p1, v6}, LX/0gh;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 495715
    :cond_0
    return-void
.end method

.method public final d(Lcom/facebook/graphql/model/FeedUnit;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 495700
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_0

    .line 495701
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 495702
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    .line 495703
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 495704
    iget-object v1, p0, LX/34o;->b:LX/0Rf;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 495705
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 495706
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/34o;->b:LX/0Rf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 495707
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 495708
    :cond_2
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final jy_()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 495699
    const/4 v0, 0x0

    return-object v0
.end method
