.class public LX/34b;
.super LX/34c;
.source ""


# instance fields
.field public c:Landroid/content/Context;

.field public d:Z

.field public e:LX/6WD;

.field public f:Ljava/lang/String;

.field private g:Landroid/view/View;

.field public h:F

.field public i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 495338
    invoke-direct {p0, p1}, LX/34c;-><init>(Landroid/content/Context;)V

    .line 495339
    iput-boolean v1, p0, LX/34b;->d:Z

    .line 495340
    sget-object v0, LX/6WD;->NONE:LX/6WD;

    iput-object v0, p0, LX/34b;->e:LX/6WD;

    .line 495341
    iput-boolean v1, p0, LX/34b;->i:Z

    .line 495342
    iput-object p1, p0, LX/34b;->c:Landroid/content/Context;

    .line 495343
    return-void
.end method

.method public static a(LX/34b;LX/6W9;Landroid/view/MenuItem;)V
    .locals 3

    .prologue
    .line 495371
    invoke-interface {p2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 495372
    iget-object v0, p1, LX/6W9;->l:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-interface {p2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 495373
    :cond_0
    iget-boolean v0, p0, LX/34b;->i:Z

    if-nez v0, :cond_1

    .line 495374
    iget-object v0, p1, LX/6W9;->l:Lcom/facebook/fbui/glyph/GlyphView;

    .line 495375
    iget-object v1, p0, LX/34c;->c:Landroid/content/Context;

    move-object v1, v1

    .line 495376
    const v2, 0x7f0a00a9

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 495377
    :cond_1
    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 495378
    iget-object v0, p1, LX/6W9;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-interface {p2}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 495379
    :cond_2
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v1, LX/6W8;

    invoke-direct {v1, p0, p2}, LX/6W8;-><init>(LX/34b;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 495380
    return-void
.end method

.method private j()I
    .locals 1

    .prologue
    .line 495381
    invoke-virtual {p0}, LX/34b;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 495406
    iget-object v0, p0, LX/34b;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 495407
    packed-switch p2, :pswitch_data_0

    .line 495408
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view type for creating view holder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 495409
    :pswitch_0
    const v1, 0x7f030664

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 495410
    new-instance v0, LX/6WB;

    invoke-direct {v0, v1}, LX/6WB;-><init>(Landroid/view/View;)V

    .line 495411
    :goto_0
    return-object v0

    .line 495412
    :pswitch_1
    const v1, 0x7f030663

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 495413
    new-instance v0, LX/6W9;

    invoke-direct {v0, v1}, LX/6W9;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 495414
    :pswitch_2
    const v1, 0x7f030665

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 495415
    new-instance v0, LX/6WE;

    invoke-direct {v0, v1}, LX/6WE;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 495416
    :pswitch_3
    new-instance v0, LX/6WA;

    iget-object v1, p0, LX/34b;->g:Landroid/view/View;

    invoke-direct {v0, v1}, LX/6WA;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 495417
    :pswitch_4
    new-instance v1, Landroid/view/View;

    iget-object v0, p0, LX/34b;->c:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 495418
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, LX/34b;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b112c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 495419
    new-instance v0, LX/6WC;

    invoke-direct {v0, v1}, LX/6WC;-><init>(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 495382
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 495383
    packed-switch v0, :pswitch_data_0

    .line 495384
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view type for binding view holder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 495385
    :pswitch_0
    invoke-direct {p0}, LX/34b;->j()I

    move-result v0

    sub-int v0, p2, v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, LX/34c;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 495386
    check-cast p1, LX/6WB;

    .line 495387
    const/16 p2, 0x8

    .line 495388
    invoke-static {p0, p1, v0}, LX/34b;->a(LX/34b;LX/6W9;Landroid/view/MenuItem;)V

    .line 495389
    instance-of v1, v0, LX/3Ai;

    if-eqz v1, :cond_1

    .line 495390
    check-cast v0, LX/3Ai;

    .line 495391
    iget-object v1, v0, LX/3Ai;->d:Ljava/lang/CharSequence;

    move-object v1, v1

    .line 495392
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 495393
    iget-object v1, p1, LX/6WB;->n:Lcom/facebook/resources/ui/FbTextView;

    const/4 p2, 0x0

    invoke-virtual {v1, p2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 495394
    iget-object v1, p1, LX/6WB;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 495395
    iget-object p2, v0, LX/3Ai;->d:Ljava/lang/CharSequence;

    move-object p2, p2

    .line 495396
    invoke-virtual {v1, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 495397
    :goto_0
    :pswitch_1
    return-void

    .line 495398
    :pswitch_2
    invoke-direct {p0}, LX/34b;->j()I

    move-result v0

    sub-int v0, p2, v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, LX/34c;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 495399
    check-cast p1, LX/6W9;

    .line 495400
    invoke-static {p0, p1, v0}, LX/34b;->a(LX/34b;LX/6W9;Landroid/view/MenuItem;)V

    goto :goto_0

    .line 495401
    :pswitch_3
    check-cast p1, LX/6WE;

    .line 495402
    iget-object v0, p1, LX/6WE;->l:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/34b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 495403
    goto :goto_0

    .line 495404
    :cond_0
    iget-object v1, p1, LX/6WB;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 495405
    :cond_1
    iget-object v1, p1, LX/6WB;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/View;F)V
    .locals 4

    .prologue
    .line 495358
    iget-object v0, p0, LX/34b;->e:LX/6WD;

    sget-object v1, LX/6WD;->BASIC:LX/6WD;

    if-ne v0, v1, :cond_0

    .line 495359
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Bottom-sheet has basic title"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 495360
    :cond_0
    sget-object v0, LX/6WD;->CUSTOM:LX/6WD;

    iput-object v0, p0, LX/34b;->e:LX/6WD;

    .line 495361
    iput p2, p0, LX/34b;->h:F

    .line 495362
    iput-object p1, p0, LX/34b;->g:Landroid/view/View;

    .line 495363
    iget-object v0, p0, LX/34b;->g:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, LX/34b;->h:F

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 495364
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 495365
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 495366
    iget-object v0, p0, LX/34b;->e:LX/6WD;

    sget-object v1, LX/6WD;->CUSTOM:LX/6WD;

    if-ne v0, v1, :cond_0

    .line 495367
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Bottom-sheet has custom title"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 495368
    :cond_0
    sget-object v0, LX/6WD;->BASIC:LX/6WD;

    iput-object v0, p0, LX/34b;->e:LX/6WD;

    .line 495369
    iput-object p1, p0, LX/34b;->f:Ljava/lang/String;

    .line 495370
    :cond_1
    return-void
.end method

.method public final d(Landroid/view/MenuItem;)I
    .locals 1

    .prologue
    .line 495356
    invoke-virtual {p0, p1}, LX/34c;->b(Landroid/view/MenuItem;)I

    move-result v0

    .line 495357
    if-ltz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 495354
    iget-object v0, p0, LX/34b;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/34b;->a(Ljava/lang/String;)V

    .line 495355
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 495346
    invoke-direct {p0}, LX/34b;->j()I

    move-result v0

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 495347
    :cond_0
    const/4 v0, 0x4

    .line 495348
    :goto_0
    return v0

    .line 495349
    :cond_1
    invoke-virtual {p0}, LX/34b;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    .line 495350
    iget-object v0, p0, LX/34b;->e:LX/6WD;

    sget-object v1, LX/6WD;->CUSTOM:LX/6WD;

    if-ne v0, v1, :cond_2

    .line 495351
    const/4 v0, 0x3

    goto :goto_0

    .line 495352
    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    .line 495353
    :cond_3
    iget-boolean v0, p0, LX/34b;->d:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 495345
    iget-object v0, p0, LX/34b;->e:LX/6WD;

    sget-object v1, LX/6WD;->NONE:LX/6WD;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 495344
    invoke-super {p0}, LX/34c;->ij_()I

    move-result v1

    invoke-virtual {p0}, LX/34b;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
