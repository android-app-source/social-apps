.class public LX/2tA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Z

.field public final synthetic c:LX/AlU;


# direct methods
.method public constructor <init>(LX/AlU;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 474771
    iput-object p1, p0, LX/2tA;->c:LX/AlU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474772
    iput-object p2, p0, LX/2tA;->a:Landroid/view/View;

    .line 474773
    iput-boolean p3, p0, LX/2tA;->b:Z

    .line 474774
    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 474783
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 474780
    iget-boolean v0, p0, LX/2tA;->b:Z

    if-nez v0, :cond_0

    .line 474781
    iget-object v0, p0, LX/2tA;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 474782
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 474779
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 474775
    iget-boolean v0, p0, LX/2tA;->b:Z

    if-eqz v0, :cond_0

    .line 474776
    iget-object v0, p0, LX/2tA;->a:Landroid/view/View;

    iget-object v1, p0, LX/2tA;->c:LX/AlU;

    iget-object v1, v1, LX/AlU;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 474777
    iget-object v0, p0, LX/2tA;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 474778
    :cond_0
    return-void
.end method
