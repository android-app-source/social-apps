.class public LX/26K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26L;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/26L",
        "<",
        "LX/26M;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1VL;

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1VL;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 3
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1VL;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 372047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372048
    iput-object p2, p0, LX/26K;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 372049
    iput-object p1, p0, LX/26K;->b:LX/1VL;

    .line 372050
    iget-object v0, p0, LX/26K;->b:LX/1VL;

    invoke-virtual {v0, p2}, LX/1VL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;

    move-result-object v0

    .line 372051
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p1

    .line 372052
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 372053
    new-instance p2, LX/26M;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p2, v1}, LX/26M;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {p1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 372054
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 372055
    :cond_0
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 372056
    iput-object v0, p0, LX/26K;->c:LX/0Px;

    .line 372057
    return-void
.end method


# virtual methods
.method public final a(LX/26N;)I
    .locals 1

    .prologue
    .line 372022
    check-cast p1, LX/26M;

    .line 372023
    iget-object v0, p1, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 372024
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 372025
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VL;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372046
    iget-object v0, p0, LX/26K;->c:LX/0Px;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 372044
    const/4 v0, 0x0

    move v0, v0

    .line 372045
    return v0
.end method

.method public final b(LX/26N;)I
    .locals 1

    .prologue
    .line 372037
    check-cast p1, LX/26M;

    .line 372038
    iget-object v0, p1, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 372039
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 372040
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 372041
    invoke-static {v0}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object p0

    .line 372042
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->y()I

    move-result p0

    :goto_0
    move v0, p0

    .line 372043
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 372034
    iget-object v1, p0, LX/26K;->b:LX/1VL;

    iget-object v0, p0, LX/26K;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 372035
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 372036
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1, v0}, LX/1VL;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    return v0
.end method

.method public final c(LX/26N;)I
    .locals 1

    .prologue
    .line 372030
    check-cast p1, LX/26M;

    .line 372031
    iget-object v0, p1, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 372032
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 372033
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VL;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    return v0
.end method

.method public final d(LX/26N;)I
    .locals 1

    .prologue
    .line 372026
    check-cast p1, LX/26M;

    .line 372027
    iget-object v0, p1, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 372028
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 372029
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VL;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    return v0
.end method
