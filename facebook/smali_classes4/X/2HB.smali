.class public final LX/2HB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Landroid/app/Notification;

.field public B:Landroid/app/Notification;

.field public C:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public a:Landroid/content/Context;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Landroid/app/PendingIntent;

.field public e:Landroid/app/PendingIntent;

.field public f:Landroid/widget/RemoteViews;

.field public g:Landroid/graphics/Bitmap;

.field public h:Ljava/lang/CharSequence;

.field public i:I

.field public j:I

.field public k:Z

.field public l:Z

.field public m:LX/3pc;

.field public n:Ljava/lang/CharSequence;

.field public o:I

.field public p:I

.field public q:Z

.field public r:Ljava/lang/String;

.field public s:Z

.field public t:Ljava/lang/String;

.field public u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3pb;",
            ">;"
        }
    .end annotation
.end field

.field public v:Z

.field public w:Ljava/lang/String;

.field public x:Landroid/os/Bundle;

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 390090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390091
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2HB;->k:Z

    .line 390092
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2HB;->u:Ljava/util/ArrayList;

    .line 390093
    iput-boolean v4, p0, LX/2HB;->v:Z

    .line 390094
    iput v4, p0, LX/2HB;->y:I

    .line 390095
    iput v4, p0, LX/2HB;->z:I

    .line 390096
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    .line 390097
    iput-object p1, p0, LX/2HB;->a:Landroid/content/Context;

    .line 390098
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 390099
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 390100
    iput v4, p0, LX/2HB;->j:I

    .line 390101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2HB;->C:Ljava/util/ArrayList;

    .line 390102
    return-void
.end method

.method public static a(LX/2HB;IZ)V
    .locals 3

    .prologue
    .line 390103
    if-eqz p2, :cond_0

    .line 390104
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 390105
    :goto_0
    return-void

    .line 390106
    :cond_0
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_0
.end method

.method public static f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 390107
    if-nez p0, :cond_1

    .line 390108
    :cond_0
    :goto_0
    return-object p0

    .line 390109
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 390110
    const/4 v0, 0x0

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/2HB;
    .locals 1

    .prologue
    .line 390086
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 390087
    return-object p0
.end method

.method public final a(III)LX/2HB;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 390111
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->ledARGB:I

    .line 390112
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iput p2, v0, Landroid/app/Notification;->ledOnMS:I

    .line 390113
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iput p3, v0, Landroid/app/Notification;->ledOffMS:I

    .line 390114
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iget v0, v0, Landroid/app/Notification;->ledOnMS:I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iget v0, v0, Landroid/app/Notification;->ledOffMS:I

    if-eqz v0, :cond_0

    move v0, v1

    .line 390115
    :goto_0
    iget-object v3, p0, LX/2HB;->B:Landroid/app/Notification;

    iget-object v4, p0, LX/2HB;->B:Landroid/app/Notification;

    iget v4, v4, Landroid/app/Notification;->flags:I

    and-int/lit8 v4, v4, -0x2

    if-eqz v0, :cond_1

    :goto_1
    or-int v0, v4, v1

    iput v0, v3, Landroid/app/Notification;->flags:I

    .line 390116
    return-object p0

    :cond_0
    move v0, v2

    .line 390117
    goto :goto_0

    :cond_1
    move v1, v2

    .line 390118
    goto :goto_1
.end method

.method public final a(IIZ)LX/2HB;
    .locals 0

    .prologue
    .line 390119
    iput p1, p0, LX/2HB;->o:I

    .line 390120
    iput p2, p0, LX/2HB;->p:I

    .line 390121
    iput-boolean p3, p0, LX/2HB;->q:Z

    .line 390122
    return-object p0
.end method

.method public final a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;
    .locals 2

    .prologue
    .line 390123
    iget-object v0, p0, LX/2HB;->u:Ljava/util/ArrayList;

    new-instance v1, LX/3pb;

    invoke-direct {v1, p1, p2, p3}, LX/3pb;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390124
    return-object p0
.end method

.method public final a(J)LX/2HB;
    .locals 1

    .prologue
    .line 390125
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iput-wide p1, v0, Landroid/app/Notification;->when:J

    .line 390126
    return-object p0
.end method

.method public final a(LX/3pb;)LX/2HB;
    .locals 1

    .prologue
    .line 390127
    iget-object v0, p0, LX/2HB;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390128
    return-object p0
.end method

.method public final a(LX/3pc;)LX/2HB;
    .locals 1

    .prologue
    .line 390129
    iget-object v0, p0, LX/2HB;->m:LX/3pc;

    if-eq v0, p1, :cond_0

    .line 390130
    iput-object p1, p0, LX/2HB;->m:LX/3pc;

    .line 390131
    iget-object v0, p0, LX/2HB;->m:LX/3pc;

    if-eqz v0, :cond_0

    .line 390132
    iget-object v0, p0, LX/2HB;->m:LX/3pc;

    invoke-virtual {v0, p0}, LX/3pc;->a(LX/2HB;)V

    .line 390133
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/app/PendingIntent;)LX/2HB;
    .locals 0

    .prologue
    .line 390134
    iput-object p1, p0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 390135
    return-object p0
.end method

.method public final a(Landroid/graphics/Bitmap;)LX/2HB;
    .locals 0

    .prologue
    .line 390136
    iput-object p1, p0, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 390137
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/2HB;
    .locals 2

    .prologue
    .line 390138
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 390139
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 390140
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/2HB;
    .locals 1

    .prologue
    .line 390088
    invoke-static {p1}, LX/2HB;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, LX/2HB;->b:Ljava/lang/CharSequence;

    .line 390089
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/2HB;
    .locals 0

    .prologue
    .line 390056
    iput-object p1, p0, LX/2HB;->w:Ljava/lang/String;

    .line 390057
    return-object p0
.end method

.method public final a(Z)LX/2HB;
    .locals 1

    .prologue
    .line 390059
    const/4 v0, 0x2

    invoke-static {p0, v0, p1}, LX/2HB;->a(LX/2HB;IZ)V

    .line 390060
    return-object p0
.end method

.method public final a([J)LX/2HB;
    .locals 1

    .prologue
    .line 390061
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->vibrate:[J

    .line 390062
    return-object p0
.end method

.method public final a()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 390063
    iget-object v0, p0, LX/2HB;->x:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 390064
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/2HB;->x:Landroid/os/Bundle;

    .line 390065
    :cond_0
    iget-object v0, p0, LX/2HB;->x:Landroid/os/Bundle;

    return-object v0
.end method

.method public final b(I)LX/2HB;
    .locals 0

    .prologue
    .line 390066
    iput p1, p0, LX/2HB;->i:I

    .line 390067
    return-object p0
.end method

.method public final b(Landroid/app/PendingIntent;)LX/2HB;
    .locals 1

    .prologue
    .line 390068
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 390069
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)LX/2HB;
    .locals 1

    .prologue
    .line 390070
    invoke-static {p1}, LX/2HB;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, LX/2HB;->c:Ljava/lang/CharSequence;

    .line 390071
    return-object p0
.end method

.method public final b(Z)LX/2HB;
    .locals 1

    .prologue
    .line 390072
    const/16 v0, 0x8

    invoke-static {p0, v0, p1}, LX/2HB;->a(LX/2HB;IZ)V

    .line 390073
    return-object p0
.end method

.method public final c(I)LX/2HB;
    .locals 2

    .prologue
    .line 390074
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->defaults:I

    .line 390075
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    .line 390076
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 390077
    :cond_0
    return-object p0
.end method

.method public final c(Z)LX/2HB;
    .locals 1

    .prologue
    .line 390078
    const/16 v0, 0x10

    invoke-static {p0, v0, p1}, LX/2HB;->a(LX/2HB;IZ)V

    .line 390079
    return-object p0
.end method

.method public final c()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 390058
    sget-object v0, LX/3px;->a:LX/3pn;

    invoke-interface {v0, p0}, LX/3pn;->a(LX/2HB;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)LX/2HB;
    .locals 0

    .prologue
    .line 390080
    iput p1, p0, LX/2HB;->j:I

    .line 390081
    return-object p0
.end method

.method public final d(Ljava/lang/CharSequence;)LX/2HB;
    .locals 1

    .prologue
    .line 390082
    invoke-static {p1}, LX/2HB;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, LX/2HB;->h:Ljava/lang/CharSequence;

    .line 390083
    return-object p0
.end method

.method public final e(Ljava/lang/CharSequence;)LX/2HB;
    .locals 2

    .prologue
    .line 390084
    iget-object v0, p0, LX/2HB;->B:Landroid/app/Notification;

    invoke-static {p1}, LX/2HB;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 390085
    return-object p0
.end method
