.class public LX/324;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/analytics/logger/HoneyClientEvent;


# direct methods
.method private constructor <init>(LX/0ge;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 489340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 489341
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v1, p1, LX/0ge;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "composer"

    .line 489342
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 489343
    move-object v0, v0

    .line 489344
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 489345
    move-object v0, v0

    .line 489346
    iput-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489347
    return-void
.end method

.method public static a(LX/0ge;Ljava/lang/String;)LX/324;
    .locals 1

    .prologue
    .line 489348
    new-instance v0, LX/324;

    invoke-direct {v0, p0, p1}, LX/324;-><init>(LX/0ge;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(I)LX/324;
    .locals 2

    .prologue
    .line 489349
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "retry_count"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489350
    return-object p0
.end method

.method public final a(J)LX/324;
    .locals 3

    .prologue
    .line 489351
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "target_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489352
    return-object p0
.end method

.method public final a(LX/0P1;)LX/324;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)",
            "LX/324;"
        }
    .end annotation

    .prologue
    .line 489353
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 489354
    invoke-virtual {p1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 489355
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz v1, :cond_0

    .line 489356
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 489357
    :cond_1
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "payload_count"

    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489358
    return-object p0
.end method

.method public final a(LX/1M1;)LX/324;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1M1",
            "<",
            "LX/7mH;",
            ">;)",
            "LX/324;"
        }
    .end annotation

    .prologue
    .line 489359
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v1

    .line 489360
    invoke-interface {p1}, LX/1M1;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mH;

    .line 489361
    invoke-virtual {v0}, LX/7mH;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0mC;->a(Ljava/lang/String;)LX/0mD;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 489362
    :cond_0
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "remaining_tasks"

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489363
    return-object p0
.end method

.method public final a(LX/2rt;)LX/324;
    .locals 3

    .prologue
    .line 489364
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "composer_type"

    iget-object v2, p1, LX/2rt;->analyticsName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489365
    return-object p0
.end method

.method public final a(LX/2rw;)LX/324;
    .locals 2

    .prologue
    .line 489366
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "target_type"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489367
    return-object p0
.end method

.method public final a(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)LX/324;
    .locals 3

    .prologue
    .line 489368
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "composer_entry_picker"

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getEntryPicker()LX/5RI;

    move-result-object v2

    invoke-virtual {v2}, LX/5RI;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "composer_source_surface"

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v2

    invoke-virtual {v2}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "composer_entry_point_name"

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getEntryPointName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489369
    return-object p0
.end method

.method public final a(Z)LX/324;
    .locals 2

    .prologue
    .line 489393
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "is_privacy_blocked"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489394
    return-object p0
.end method

.method public final a()Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1

    .prologue
    .line 489370
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    return-object v0
.end method

.method public final b(I)LX/324;
    .locals 2

    .prologue
    .line 489371
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "remaining_character_count"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489372
    return-object p0
.end method

.method public final b(J)LX/324;
    .locals 3

    .prologue
    .line 489373
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "publish_target"

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489374
    return-object p0
.end method

.method public final b(LX/0Px;Z)LX/324;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "LX/324;"
        }
    .end annotation

    .prologue
    .line 489375
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v2

    .line 489376
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 489377
    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 489378
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 489379
    :cond_0
    if-eqz p2, :cond_1

    const-string v0, "video_items_removed"

    .line 489380
    :goto_1
    iget-object v1, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489381
    return-object p0

    .line 489382
    :cond_1
    const-string v0, "photo_items_removed"

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)LX/324;
    .locals 2

    .prologue
    .line 489383
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "publish_method"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489384
    return-object p0
.end method

.method public final b(Z)LX/324;
    .locals 2

    .prologue
    .line 489385
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "is_media_attached"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489386
    return-object p0
.end method

.method public final c(I)LX/324;
    .locals 2

    .prologue
    .line 489387
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "rating"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489388
    return-object p0
.end method

.method public final c(J)LX/324;
    .locals 3

    .prologue
    .line 489389
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "loaded_time"

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489390
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/324;
    .locals 2

    .prologue
    .line 489338
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "params"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489339
    return-object p0
.end method

.method public final c(Z)LX/324;
    .locals 2

    .prologue
    .line 489391
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "auto_tag_timeout_has_passed"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489392
    return-object p0
.end method

.method public final d(I)LX/324;
    .locals 2

    .prologue
    .line 489298
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "total_sticker_count"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489299
    return-object p0
.end method

.method public final d(J)LX/324;
    .locals 3

    .prologue
    .line 489302
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "ttf_ms"

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489303
    return-object p0
.end method

.method public final d(Z)LX/324;
    .locals 2

    .prologue
    .line 489304
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "has_user_edited_content"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489305
    return-object p0
.end method

.method public final e(I)LX/324;
    .locals 2

    .prologue
    .line 489306
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "char_count"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489307
    return-object p0
.end method

.method public final e(Z)LX/324;
    .locals 2

    .prologue
    .line 489300
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "has_submittable_content"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489301
    return-object p0
.end method

.method public final f(Z)LX/324;
    .locals 2

    .prologue
    .line 489308
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "can_submit"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489309
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/324;
    .locals 2

    .prologue
    .line 489310
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "reachability_status"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489311
    return-object p0
.end method

.method public final g(Z)LX/324;
    .locals 2

    .prologue
    .line 489312
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "face_found"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489313
    return-object p0
.end method

.method public final h(Z)LX/324;
    .locals 2

    .prologue
    .line 489314
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "face_detect_finished"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489315
    return-object p0
.end method

.method public final i(Z)LX/324;
    .locals 2

    .prologue
    .line 489316
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "is_sticker_post"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489317
    return-object p0
.end method

.method public final j(Ljava/lang/String;)LX/324;
    .locals 2

    .prologue
    .line 489318
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "payload_type"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489319
    return-object p0
.end method

.method public final l(Ljava/lang/String;)LX/324;
    .locals 2

    .prologue
    .line 489320
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "viewer_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489321
    return-object p0
.end method

.method public final l(Z)LX/324;
    .locals 2

    .prologue
    .line 489322
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "is_edit_composer"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489323
    return-object p0
.end method

.method public final m(Z)LX/324;
    .locals 2

    .prologue
    .line 489324
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "is_saving_draft"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489325
    return-object p0
.end method

.method public final n(Ljava/lang/String;)LX/324;
    .locals 2

    .prologue
    .line 489326
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "payload_attachment_format"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489327
    return-object p0
.end method

.method public final n(Z)LX/324;
    .locals 2

    .prologue
    .line 489328
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "is_publish_from_composer"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489329
    return-object p0
.end method

.method public final o(Ljava/lang/String;)LX/324;
    .locals 2

    .prologue
    .line 489330
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "payload_attachment_source"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489331
    return-object p0
.end method

.method public final r(Ljava/lang/String;)LX/324;
    .locals 2

    .prologue
    .line 489332
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "attached_share_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489333
    return-object p0
.end method

.method public final s(Ljava/lang/String;)LX/324;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 489334
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "edited_story_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489335
    return-object p0
.end method

.method public final u(Ljava/lang/String;)LX/324;
    .locals 2

    .prologue
    .line 489336
    iget-object v0, p0, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "souvenir_unique_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 489337
    return-object p0
.end method
