.class public LX/34X;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/3AO;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/net/Uri;

.field public final c:LX/3AO;

.field public final d:Lcom/facebook/common/callercontext/CallerContext;

.field public final e:Lcom/facebook/http/interfaces/RequestPriority;

.field public final f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/1uy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1uy",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    .line 495253
    const-string v0, "https"

    sget-object v1, LX/3AO;->HTTPS:LX/3AO;

    const-string v2, "http"

    sget-object v3, LX/3AO;->HTTP:LX/3AO;

    const-string v4, "content"

    sget-object v5, LX/3AO;->CONTENT:LX/3AO;

    const-string v6, "file"

    sget-object v7, LX/3AO;->FILE:LX/3AO;

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/34X;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LX/1uy",
            "<TT;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 495254
    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->DEFAULT_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    .line 495255
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v5, v0

    .line 495256
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;LX/0P1;)V

    .line 495257
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;LX/0P1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LX/1uy",
            "<TT;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 495258
    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->DEFAULT_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;LX/0P1;)V

    .line 495259
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LX/1uy",
            "<TT;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 495260
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v5, v0

    .line 495261
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;LX/0P1;)V

    .line 495262
    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LX/1uy",
            "<TT;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 495263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495264
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, LX/34X;->b:Landroid/net/Uri;

    .line 495265
    sget-object v0, LX/34X;->a:LX/0P1;

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AO;

    .line 495266
    if-eqz v0, :cond_0

    :goto_0
    iput-object v0, p0, LX/34X;->c:LX/3AO;

    .line 495267
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1uy;

    iput-object v0, p0, LX/34X;->g:LX/1uy;

    .line 495268
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, LX/34X;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 495269
    iput-object p4, p0, LX/34X;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 495270
    iput-object p5, p0, LX/34X;->f:LX/0P1;

    .line 495271
    return-void

    .line 495272
    :cond_0
    sget-object v0, LX/3AO;->UNSUPPORTED:LX/3AO;

    goto :goto_0
.end method


# virtual methods
.method public a()Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3

    .prologue
    .line 495273
    :try_start_0
    iget-object v0, p0, LX/34X;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 495274
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    return-object v1

    .line 495275
    :catch_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/34X;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
