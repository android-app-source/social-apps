.class public LX/38o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final d:LX/04G;

.field private static final e:LX/04H;

.field private static volatile f:LX/38o;


# instance fields
.field private final b:LX/1C2;

.field private final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 521342
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    iget-object v0, v0, LX/04g;->value:Ljava/lang/String;

    sput-object v0, LX/38o;->a:Ljava/lang/String;

    .line 521343
    sget-object v0, LX/04G;->TV_PLAYER:LX/04G;

    sput-object v0, LX/38o;->d:LX/04G;

    .line 521344
    sget-object v0, LX/04H;->NO_INFO:LX/04H;

    sput-object v0, LX/38o;->e:LX/04H;

    return-void
.end method

.method public constructor <init>(LX/1C2;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521346
    iput-object p1, p0, LX/38o;->b:LX/1C2;

    .line 521347
    iput-object p2, p0, LX/38o;->c:LX/03V;

    .line 521348
    return-void
.end method

.method public static a(LX/0QB;)LX/38o;
    .locals 5

    .prologue
    .line 521349
    sget-object v0, LX/38o;->f:LX/38o;

    if-nez v0, :cond_1

    .line 521350
    const-class v1, LX/38o;

    monitor-enter v1

    .line 521351
    :try_start_0
    sget-object v0, LX/38o;->f:LX/38o;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 521352
    if-eqz v2, :cond_0

    .line 521353
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 521354
    new-instance p0, LX/38o;

    invoke-static {v0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v3

    check-cast v3, LX/1C2;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/38o;-><init>(LX/1C2;LX/03V;)V

    .line 521355
    move-object v0, p0

    .line 521356
    sput-object v0, LX/38o;->f:LX/38o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521357
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 521358
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 521359
    :cond_1
    sget-object v0, LX/38o;->f:LX/38o;

    return-object v0

    .line 521360
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 521361
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/38o;LX/7JN;I)V
    .locals 17

    .prologue
    .line 521362
    if-eqz p1, :cond_0

    .line 521363
    move-object/from16 v0, p0

    iget-object v1, v0, LX/38o;->b:LX/1C2;

    invoke-virtual/range {p1 .. p1}, LX/7JN;->q()LX/162;

    move-result-object v2

    sget-object v3, LX/38o;->d:LX/04G;

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, LX/38o;->a:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual/range {p1 .. p1}, LX/7JN;->m()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, LX/7JN;->r()LX/04D;

    move-result-object v10

    const/4 v11, 0x0

    sget-object v12, LX/38o;->a:Ljava/lang/String;

    const/4 v13, 0x0

    sget-object v14, LX/38o;->e:LX/04H;

    const/4 v15, 0x0

    move/from16 v7, p2

    move-object/from16 v16, p1

    invoke-virtual/range {v1 .. v16}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/04H;LX/09M;LX/098;)LX/1C2;

    .line 521364
    :goto_0
    return-void

    .line 521365
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/38o;->c:LX/03V;

    const-string v2, "CHROMECAST_LOGGING_ERROR"

    const-string v3, "logVideoStartOrUnPausedEvent() : VideoCastParams is null"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method

.method public static c(LX/38o;LX/7JN;)V
    .locals 15

    .prologue
    .line 521366
    if-eqz p1, :cond_0

    .line 521367
    iget-object v0, p0, LX/38o;->b:LX/1C2;

    invoke-virtual/range {p1 .. p1}, LX/7JN;->q()LX/162;

    move-result-object v1

    sget-object v2, LX/38o;->d:LX/04G;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, LX/7JN;->i()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, LX/7JN;->s()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, LX/7JN;->m()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, LX/7JN;->r()LX/04D;

    move-result-object v9

    sget-object v10, LX/38o;->a:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x0

    sget-object v13, LX/097;->FROM_STREAM:LX/097;

    iget-object v14, v13, LX/097;->value:Ljava/lang/String;

    move-object/from16 v13, p1

    invoke-virtual/range {v0 .. v14}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;IIILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 521368
    :goto_0
    return-void

    .line 521369
    :cond_0
    iget-object v0, p0, LX/38o;->c:LX/03V;

    const-string v1, "CHROMECAST_LOGGING_ERROR"

    const-string v2, "logVideoCompleteEvent() : VideoCastParams is null"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method

.method public static c(LX/38o;LX/7JN;I)V
    .locals 17

    .prologue
    .line 521370
    if-eqz p1, :cond_0

    .line 521371
    move-object/from16 v0, p0

    iget-object v1, v0, LX/38o;->b:LX/1C2;

    invoke-virtual/range {p1 .. p1}, LX/7JN;->q()LX/162;

    move-result-object v2

    sget-object v3, LX/38o;->d:LX/04G;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, LX/38o;->a:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, LX/7JN;->s()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, LX/7JN;->m()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, LX/7JN;->r()LX/04D;

    move-result-object v11

    sget-object v12, LX/38o;->a:Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x0

    sget-object v8, LX/097;->FROM_STREAM:LX/097;

    iget-object v0, v8, LX/097;->value:Ljava/lang/String;

    move-object/from16 v16, v0

    move/from16 v8, p2

    move-object/from16 v15, p1

    invoke-virtual/range {v1 .. v16}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;LX/0AR;ILjava/lang/String;IILjava/lang/String;LX/04D;Ljava/lang/String;Ljava/lang/String;LX/09M;LX/098;Ljava/lang/String;)LX/1C2;

    .line 521372
    :goto_0
    return-void

    .line 521373
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/38o;->c:LX/03V;

    const-string v2, "CHROMECAST_LOGGING_ERROR"

    const-string v3, "logVideoPausedEvent() : VideoCastParams is null"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7JN;)V
    .locals 13

    .prologue
    .line 521374
    if-eqz p1, :cond_0

    .line 521375
    iget-object v0, p0, LX/38o;->b:LX/1C2;

    .line 521376
    iget-object v1, p1, LX/7JN;->d:LX/162;

    move-object v1, v1

    .line 521377
    sget-object v2, LX/38o;->d:LX/04G;

    .line 521378
    iget-object v3, p1, LX/7JN;->a:Ljava/lang/String;

    move-object v3, v3

    .line 521379
    iget-object v4, p1, LX/7JN;->k:LX/04D;

    move-object v4, v4

    .line 521380
    invoke-virtual {p1}, LX/7JN;->c()Z

    move-result v5

    .line 521381
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v6, LX/0JC;->CHROMECAST_CAST_CONNECTED:LX/0JC;

    iget-object v6, v6, LX/0JC;->value:Ljava/lang/String;

    invoke-direct {v7, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    move-object v6, v0

    move-object v8, v3

    move-object v9, v1

    move v10, v5

    move-object v11, v4

    move-object v12, v2

    .line 521382
    invoke-static/range {v6 .. v12}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    .line 521383
    :goto_0
    return-void

    .line 521384
    :cond_0
    iget-object v0, p0, LX/38o;->c:LX/03V;

    const-string v1, "CHROMECAST_LOGGING_ERROR"

    const-string v2, "logCastAppConnected() : VideoCastParams is null when trying to connect"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method

.method public final a(LX/7JN;I)V
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 521385
    if-eqz p1, :cond_0

    .line 521386
    iget-object v0, p0, LX/38o;->b:LX/1C2;

    .line 521387
    iget-object v1, p1, LX/7JN;->d:LX/162;

    move-object v1, v1

    .line 521388
    sget-object v2, LX/38o;->d:LX/04G;

    sget-object v3, LX/04G;->INLINE_PLAYER:LX/04G;

    .line 521389
    iget-object v4, p1, LX/7JN;->a:Ljava/lang/String;

    move-object v4, v4

    .line 521390
    iget-object v5, p1, LX/7JN;->k:LX/04D;

    move-object v5, v5

    .line 521391
    sget-object v6, LX/38o;->a:Ljava/lang/String;

    .line 521392
    iget v7, p1, LX/7JN;->p:I

    move v8, v7

    .line 521393
    move v7, p2

    move-object v9, p1

    move-object v11, v10

    move-object v12, v10

    invoke-virtual/range {v0 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 521394
    :goto_0
    return-void

    .line 521395
    :cond_0
    iget-object v0, p0, LX/38o;->c:LX/03V;

    const-string v1, "CHROMECAST_LOGGING_ERROR"

    const-string v2, "logEnterChromecastMode() : VideoCastParams is null"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/7JN;ZI)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 521396
    if-eqz p2, :cond_1

    .line 521397
    if-eqz p3, :cond_0

    .line 521398
    invoke-static {p0, p2, p4}, LX/38o;->c(LX/38o;LX/7JN;I)V

    .line 521399
    :cond_0
    iget-object v0, p0, LX/38o;->b:LX/1C2;

    sget-object v1, LX/38o;->d:LX/04G;

    .line 521400
    iget-object v2, p2, LX/7JN;->d:LX/162;

    move-object v3, v2

    .line 521401
    iget-object v2, p2, LX/7JN;->a:Ljava/lang/String;

    move-object v4, v2

    .line 521402
    iget-object v2, p2, LX/7JN;->k:LX/04D;

    move-object v5, v2

    .line 521403
    invoke-virtual {p2}, LX/7JN;->c()Z

    move-result v6

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, LX/1C2;->a(LX/04G;Ljava/lang/String;LX/0lF;Ljava/lang/String;LX/04D;Z)LX/1C2;

    .line 521404
    :goto_0
    return-void

    .line 521405
    :cond_1
    iget-object v0, p0, LX/38o;->b:LX/1C2;

    sget-object v1, LX/38o;->d:LX/04G;

    const/4 v6, 0x0

    move-object v2, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, LX/1C2;->a(LX/04G;Ljava/lang/String;LX/0lF;Ljava/lang/String;LX/04D;Z)LX/1C2;

    goto :goto_0
.end method

.method public final b(LX/7JN;)V
    .locals 11

    .prologue
    .line 521406
    if-eqz p1, :cond_0

    .line 521407
    iget-object v0, p0, LX/38o;->b:LX/1C2;

    .line 521408
    iget-object v1, p1, LX/7JN;->k:LX/04D;

    move-object v1, v1

    .line 521409
    sget-object v2, LX/38o;->d:LX/04G;

    .line 521410
    iget-object v3, p1, LX/7JN;->a:Ljava/lang/String;

    move-object v3, v3

    .line 521411
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/0JC;->CHROMECAST_CAST_RECONNECTED:LX/0JC;

    iget-object v4, v4, LX/0JC;->value:Ljava/lang/String;

    invoke-direct {v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 521412
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v4, v0

    move-object v6, v3

    move-object v9, v1

    move-object v10, v2

    invoke-static/range {v4 .. v10}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    .line 521413
    :goto_0
    return-void

    .line 521414
    :cond_0
    iget-object v0, p0, LX/38o;->c:LX/03V;

    const-string v1, "CHROMECAST_LOGGING_ERROR"

    const-string v2, "logCastAppReconnected() : VideoCastParams is null when trying to reconnect"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method
