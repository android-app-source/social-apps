.class public LX/2IA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2AX;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2IA;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391486
    return-void
.end method

.method public static a(LX/0QB;)LX/2IA;
    .locals 3

    .prologue
    .line 391487
    sget-object v0, LX/2IA;->a:LX/2IA;

    if-nez v0, :cond_1

    .line 391488
    const-class v1, LX/2IA;

    monitor-enter v1

    .line 391489
    :try_start_0
    sget-object v0, LX/2IA;->a:LX/2IA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391490
    if-eqz v2, :cond_0

    .line 391491
    :try_start_1
    new-instance v0, LX/2IA;

    invoke-direct {v0}, LX/2IA;-><init>()V

    .line 391492
    move-object v0, v0

    .line 391493
    sput-object v0, LX/2IA;->a:LX/2IA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391494
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391495
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391496
    :cond_1
    sget-object v0, LX/2IA;->a:LX/2IA;

    return-object v0

    .line 391497
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391498
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391499
    new-instance v0, LX/1se;

    const-string v1, "/t_push"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v1, LX/2C3;->ALWAYS:LX/2C3;

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    return-object v0
.end method
