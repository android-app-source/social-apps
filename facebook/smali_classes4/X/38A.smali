.class public final LX/38A;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/os/Bundle;

.field public b:LX/38T;


# direct methods
.method public constructor <init>(LX/38T;Z)V
    .locals 3

    .prologue
    .line 503005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 503006
    if-nez p1, :cond_0

    .line 503007
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503008
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/38A;->a:Landroid/os/Bundle;

    .line 503009
    iput-object p1, p0, LX/38A;->b:LX/38T;

    .line 503010
    iget-object v0, p0, LX/38A;->a:Landroid/os/Bundle;

    const-string v1, "selector"

    .line 503011
    iget-object v2, p1, LX/38T;->b:Landroid/os/Bundle;

    move-object v2, v2

    .line 503012
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 503013
    iget-object v0, p0, LX/38A;->a:Landroid/os/Bundle;

    const-string v1, "activeScan"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 503014
    return-void
.end method

.method public static d(LX/38A;)V
    .locals 3

    .prologue
    .line 502998
    iget-object v0, p0, LX/38A;->b:LX/38T;

    if-nez v0, :cond_0

    .line 502999
    iget-object v0, p0, LX/38A;->a:Landroid/os/Bundle;

    const-string v1, "selector"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const/4 v2, 0x0

    .line 503000
    if-eqz v0, :cond_1

    new-instance v1, LX/38T;

    invoke-direct {v1, v0, v2}, LX/38T;-><init>(Landroid/os/Bundle;Ljava/util/List;)V

    :goto_0
    move-object v0, v1

    .line 503001
    iput-object v0, p0, LX/38A;->b:LX/38T;

    .line 503002
    iget-object v0, p0, LX/38A;->b:LX/38T;

    if-nez v0, :cond_0

    .line 503003
    sget-object v0, LX/38T;->a:LX/38T;

    iput-object v0, p0, LX/38A;->b:LX/38T;

    .line 503004
    :cond_0
    return-void

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/38T;
    .locals 1

    .prologue
    .line 502996
    invoke-static {p0}, LX/38A;->d(LX/38A;)V

    .line 502997
    iget-object v0, p0, LX/38A;->b:LX/38T;

    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 502976
    iget-object v0, p0, LX/38A;->a:Landroid/os/Bundle;

    const-string v1, "activeScan"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 502992
    instance-of v1, p1, LX/38A;

    if-eqz v1, :cond_0

    .line 502993
    check-cast p1, LX/38A;

    .line 502994
    invoke-virtual {p0}, LX/38A;->a()LX/38T;

    move-result-object v1

    invoke-virtual {p1}, LX/38A;->a()LX/38T;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/38T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/38A;->b()Z

    move-result v1

    invoke-virtual {p1}, LX/38A;->b()Z

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 502995
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 502991
    invoke-virtual {p0}, LX/38A;->a()LX/38T;

    move-result-object v0

    invoke-virtual {v0}, LX/38T;->hashCode()I

    move-result v1

    invoke-virtual {p0}, LX/38A;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    xor-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 502977
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 502978
    const-string v1, "DiscoveryRequest{ selector="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/38A;->a()LX/38T;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502979
    const-string v1, ", activeScan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/38A;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 502980
    const-string v1, ", isValid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 502981
    invoke-static {p0}, LX/38A;->d(LX/38A;)V

    .line 502982
    iget-object v2, p0, LX/38A;->b:LX/38T;

    .line 502983
    invoke-static {v2}, LX/38T;->e(LX/38T;)V

    .line 502984
    iget-object v3, v2, LX/38T;->c:Ljava/util/List;

    const/4 p0, 0x0

    invoke-interface {v3, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 502985
    const/4 v3, 0x0

    .line 502986
    :goto_0
    move v2, v3

    .line 502987
    move v2, v2

    .line 502988
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 502989
    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 502990
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v3, 0x1

    goto :goto_0
.end method
