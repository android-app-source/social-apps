.class public LX/34G;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2Ly;

.field private final c:LX/2OS;

.field public final d:LX/0lB;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5fv;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/content/res/Resources;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oi;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oj;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6lh;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dnv;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/34H;

.field private final p:LX/2Mk;

.field private final q:LX/2N9;


# direct methods
.method public constructor <init>(LX/0Or;LX/2Ly;LX/2OS;LX/0lB;LX/0Ot;Landroid/content/res/Resources;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/34H;LX/2Mk;LX/2N9;)V
    .locals 1
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsShowPlatformAttributionEnabled;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/customthreads/annotations/CanViewThreadCustomization;
        .end annotation
    .end param
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/customthreads/annotations/CanViewCustomNicknames;
        .end annotation
    .end param
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/2Ly;",
            "LX/2OS;",
            "LX/0lB;",
            "LX/0Ot",
            "<",
            "LX/5fv;",
            ">;",
            "Landroid/content/res/Resources;",
            "LX/0Or",
            "<",
            "LX/2Oi;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Oj;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6lh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dnv;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/34H;",
            "LX/2Mk;",
            "LX/2N9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494751
    iput-object p1, p0, LX/34G;->a:LX/0Or;

    .line 494752
    iput-object p2, p0, LX/34G;->b:LX/2Ly;

    .line 494753
    iput-object p3, p0, LX/34G;->c:LX/2OS;

    .line 494754
    iput-object p4, p0, LX/34G;->d:LX/0lB;

    .line 494755
    iput-object p5, p0, LX/34G;->e:LX/0Ot;

    .line 494756
    iput-object p6, p0, LX/34G;->f:Landroid/content/res/Resources;

    .line 494757
    iput-object p7, p0, LX/34G;->g:LX/0Or;

    .line 494758
    iput-object p8, p0, LX/34G;->h:LX/0Or;

    .line 494759
    iput-object p9, p0, LX/34G;->i:LX/0Or;

    .line 494760
    iput-object p10, p0, LX/34G;->j:LX/0Ot;

    .line 494761
    iput-object p11, p0, LX/34G;->k:LX/0Ot;

    .line 494762
    iput-object p12, p0, LX/34G;->l:LX/0Or;

    .line 494763
    iput-object p13, p0, LX/34G;->m:LX/0Or;

    .line 494764
    iput-object p14, p0, LX/34G;->n:LX/0Or;

    .line 494765
    move-object/from16 v0, p15

    iput-object v0, p0, LX/34G;->o:LX/34H;

    .line 494766
    move-object/from16 v0, p16

    iput-object v0, p0, LX/34G;->p:LX/2Mk;

    .line 494767
    move-object/from16 v0, p17

    iput-object v0, p0, LX/34G;->q:LX/2N9;

    .line 494768
    return-void
.end method

.method public static a(LX/0QB;)LX/34G;
    .locals 1

    .prologue
    .line 494867
    invoke-static {p0}, LX/34G;->b(LX/0QB;)LX/34G;

    move-result-object v0

    return-object v0
.end method

.method private a()Lcom/facebook/user/model/UserKey;
    .locals 1

    .prologue
    .line 494864
    iget-object v0, p0, LX/34G;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 494865
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, p0

    .line 494866
    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;Z)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 494769
    iget-object v0, p0, LX/34G;->o:LX/34H;

    invoke-virtual {v0, p1}, LX/34H;->a(Lcom/facebook/messaging/model/messages/Message;)LX/CMk;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 494770
    invoke-virtual {p0, p1, p2}, LX/34G;->d(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    .line 494771
    :goto_0
    return-object v0

    .line 494772
    :cond_0
    invoke-static {p1}, LX/34G;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494773
    invoke-virtual {p0, p1, p2}, LX/34G;->e(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494774
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-direct {p0}, LX/34G;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    .line 494775
    const-string v3, ""

    .line 494776
    if-nez v5, :cond_2

    .line 494777
    invoke-static {p0, p1, p2}, LX/34G;->f(LX/34G;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v3

    .line 494778
    :cond_2
    iget-object v0, p0, LX/34G;->b:LX/2Ly;

    invoke-virtual {v0, p1}, LX/2Ly;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v0

    .line 494779
    sget-object v2, LX/6eh;->STICKER:LX/6eh;

    if-ne v0, v2, :cond_5

    .line 494780
    if-eqz p3, :cond_3

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v0}, LX/4m9;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 494781
    iget-object v0, p0, LX/34G;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1b

    if-eqz p2, :cond_1b

    .line 494782
    iget-object v0, p2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    move-object v0, v0

    .line 494783
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1b

    .line 494784
    :goto_1
    move-object v0, v0

    .line 494785
    goto :goto_0

    .line 494786
    :cond_3
    if-eqz v5, :cond_4

    .line 494787
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v1, 0x7f08034c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494788
    :cond_4
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v2, 0x7f08034d

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v7

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494789
    :cond_5
    sget-object v2, LX/6eh;->AUDIO_CLIP:LX/6eh;

    if-ne v0, v2, :cond_7

    .line 494790
    if-eqz v5, :cond_6

    .line 494791
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v1, 0x7f080354

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494792
    :cond_6
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v2, 0x7f080355

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v7

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494793
    :cond_7
    sget-object v2, LX/6eh;->VIDEO_CLIP:LX/6eh;

    if-ne v0, v2, :cond_9

    .line 494794
    if-eqz v5, :cond_8

    .line 494795
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v1, 0x7f080352

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494796
    :cond_8
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v2, 0x7f080353

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v7

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494797
    :cond_9
    invoke-static {p1}, LX/2OS;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 494798
    const/4 v4, 0x0

    .line 494799
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1c

    move v2, v4

    .line 494800
    :cond_a
    move v0, v2

    .line 494801
    invoke-static {p1}, LX/2OS;->b(Lcom/facebook/messaging/model/messages/Message;)I

    move-result v2

    .line 494802
    if-ge v0, v2, :cond_c

    .line 494803
    if-eqz v5, :cond_b

    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v3, 0x7f0f001b

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v7

    invoke-virtual {v0, v3, v2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v4, 0x7f0f001d

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v3, v5, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    invoke-virtual {v0, v4, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494804
    :cond_c
    if-eqz v5, :cond_d

    iget-object v2, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v3, 0x7f0f001c

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v7

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_d
    iget-object v2, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v4, 0x7f0f001a

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v3, v5, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    invoke-virtual {v2, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494805
    :cond_e
    sget-object v2, LX/6eh;->PAYMENT:LX/6eh;

    if-ne v0, v2, :cond_12

    .line 494806
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v6

    .line 494807
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    if-eqz v0, :cond_f

    .line 494808
    iget-object v0, p0, LX/34G;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5fv;

    new-instance v2, Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 494809
    iget-object v7, v4, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->e:Ljava/lang/String;

    move-object v4, v7

    .line 494810
    iget-object v7, p1, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 494811
    iget v8, v7, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->d:I

    move v7, v8

    .line 494812
    int-to-long v8, v7

    invoke-direct {v2, v4, v8, v9}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    sget-object v4, LX/5fu;->NO_EMPTY_DECIMALS:LX/5fu;

    invoke-virtual {v0, v2, v4}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v2

    .line 494813
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 494814
    iget-wide v10, v0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->c:J

    move-wide v8, v10

    .line 494815
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    .line 494816
    invoke-direct/range {v0 .. v6}, LX/34G;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494817
    :cond_f
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    if-eqz v0, :cond_10

    .line 494818
    iget-object v0, p0, LX/34G;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5fv;

    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    .line 494819
    iget-object v4, v2, Lcom/facebook/messaging/model/payment/PaymentRequestData;->e:Ljava/lang/String;

    move-object v2, v4

    .line 494820
    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    .line 494821
    iget v8, v4, Lcom/facebook/messaging/model/payment/PaymentRequestData;->d:I

    move v4, v8

    .line 494822
    int-to-long v8, v4

    invoke-direct {v1, v2, v8, v9}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    sget-object v2, LX/5fu;->NO_EMPTY_DECIMALS:LX/5fu;

    invoke-virtual {v0, v1, v2}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v2

    .line 494823
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    .line 494824
    iget-wide v10, v0, Lcom/facebook/messaging/model/payment/PaymentRequestData;->c:J

    move-wide v0, v10

    .line 494825
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move v1, v7

    .line 494826
    invoke-direct/range {v0 .. v6}, LX/34G;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494827
    :cond_10
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v0, :cond_11

    .line 494828
    iget-object v0, p0, LX/34G;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5fv;

    new-instance v2, Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v4, v4, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    iget-object v4, v4, Lcom/facebook/messaging/model/payment/SentPayment;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 494829
    iget-object v7, v4, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v4, v7

    .line 494830
    iget-object v7, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v7, v7, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    iget-object v7, v7, Lcom/facebook/messaging/model/payment/SentPayment;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v7}, Lcom/facebook/payments/currency/CurrencyAmount;->c()I

    move-result v7

    int-to-long v8, v7

    invoke-direct {v2, v4, v8, v9}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    sget-object v4, LX/5fu;->NO_EMPTY_DECIMALS:LX/5fu;

    invoke-virtual {v0, v2, v4}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v2

    .line 494831
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    iget-object v4, v0, Lcom/facebook/messaging/model/payment/SentPayment;->c:Ljava/lang/String;

    move-object v0, p0

    .line 494832
    invoke-direct/range {v0 .. v6}, LX/34G;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494833
    :cond_11
    const-string v0, ""

    goto/16 :goto_0

    .line 494834
    :cond_12
    sget-object v2, LX/6eh;->CALL_LOG:LX/6eh;

    if-ne v0, v2, :cond_13

    .line 494835
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v0}, LX/6lg;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6lg;

    move-result-object v1

    .line 494836
    iget-object v0, p0, LX/34G;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dnv;

    invoke-virtual {v0, v1}, LX/Dnv;->a(LX/6lg;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494837
    :cond_13
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    if-eqz v0, :cond_15

    .line 494838
    if-eqz v5, :cond_14

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v0}, LX/6lg;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6lg;

    move-result-object v0

    move-object v1, v0

    .line 494839
    :goto_2
    iget-object v0, p0, LX/34G;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6lh;

    invoke-virtual {v0, v1}, LX/6lh;->a(LX/6lg;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494840
    :cond_14
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v3, v0}, LX/6lg;->a(Ljava/lang/String;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6lg;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 494841
    :cond_15
    const-string v0, "sms"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/mms/MmsData;->a()Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 494842
    if-eqz v0, :cond_16

    .line 494843
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v1, 0x7f0808f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494844
    :cond_16
    const-string v0, "sms"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    const/4 v2, 0x0

    .line 494845
    iget-object v4, v0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-lez v4, :cond_17

    iget-object v4, v0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 494846
    const-string v4, "text/x-vCard"

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    .line 494847
    :cond_17
    move v0, v2

    .line 494848
    if-eqz v0, :cond_19

    .line 494849
    if-eqz v5, :cond_18

    .line 494850
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v1, 0x7f080350

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494851
    :cond_18
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v2, 0x7f080351

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v7

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494852
    :cond_19
    if-eqz v5, :cond_1a

    .line 494853
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v1, 0x7f08035f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494854
    :cond_1a
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v2, 0x7f080360

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v7

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_1b
    sget-object v0, LX/1zU;->b:Ljava/lang/String;

    goto/16 :goto_1

    .line 494855
    :cond_1c
    iget-object v9, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result p2

    move v8, v4

    move v2, v4

    :goto_4
    if-ge v8, p2, :cond_1d

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 494856
    invoke-static {v0}, LX/2M0;->c(Lcom/facebook/messaging/model/attachment/Attachment;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 494857
    add-int/lit8 v0, v2, 0x1

    .line 494858
    :goto_5
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v2, v0

    goto :goto_4

    .line 494859
    :cond_1d
    if-gtz v2, :cond_a

    .line 494860
    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/Message;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    :goto_6
    if-ge v4, v9, :cond_a

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 494861
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->d()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 494862
    add-int/lit8 v0, v2, 0x1

    .line 494863
    :goto_7
    add-int/lit8 v4, v4, 0x1

    move v2, v0

    goto :goto_6

    :cond_1e
    move v0, v2

    goto :goto_7

    :cond_1f
    move v0, v2

    goto :goto_5

    :cond_20
    const/4 v0, 0x0

    goto/16 :goto_3
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 494734
    invoke-static {p4}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    .line 494735
    iget-object v0, p0, LX/34G;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oi;

    invoke-virtual {v0, v1}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 494736
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v1, v0

    .line 494737
    if-eqz p5, :cond_3

    .line 494738
    if-eqz p6, :cond_1

    .line 494739
    iget-object v2, p0, LX/34G;->f:Landroid/content/res/Resources;

    if-eqz p1, :cond_0

    const v0, 0x7f080358

    :goto_1
    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v4

    aput-object p2, v3, v5

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 494740
    :goto_2
    return-object v0

    .line 494741
    :cond_0
    const v0, 0x7f080359

    goto :goto_1

    .line 494742
    :cond_1
    iget-object v1, p0, LX/34G;->f:Landroid/content/res/Resources;

    if-eqz p1, :cond_2

    const v0, 0x7f080356

    :goto_3
    new-array v2, v5, [Ljava/lang/Object;

    aput-object p2, v2, v4

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    const v0, 0x7f080357

    goto :goto_3

    .line 494743
    :cond_3
    iget-object v0, p0, LX/34G;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 494744
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 494745
    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 494746
    if-eqz p1, :cond_4

    .line 494747
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v1, 0x7f08035a

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p3, v2, v4

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 494748
    :cond_4
    iget-object v1, p0, LX/34G;->f:Landroid/content/res/Resources;

    if-eqz p6, :cond_5

    const v0, 0x7f08035c

    :goto_4
    new-array v2, v6, [Ljava/lang/Object;

    aput-object p3, v2, v4

    aput-object p2, v2, v5

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    const v0, 0x7f08035b

    goto :goto_4

    .line 494749
    :cond_6
    iget-object v2, p0, LX/34G;->f:Landroid/content/res/Resources;

    if-eqz p1, :cond_7

    const v0, 0x7f08035d

    :goto_5
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v4

    aput-object v1, v3, v5

    aput-object p2, v3, v6

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_7
    const v0, 0x7f08035e

    goto :goto_5

    :cond_8
    const-string v0, ""

    goto/16 :goto_0
.end method

.method public static b(LX/0QB;)LX/34G;
    .locals 19

    .prologue
    .line 494732
    new-instance v1, LX/34G;

    const/16 v2, 0x19e

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static/range {p0 .. p0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v3

    check-cast v3, LX/2Ly;

    invoke-static/range {p0 .. p0}, LX/2OS;->a(LX/0QB;)LX/2OS;

    move-result-object v4

    check-cast v4, LX/2OS;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lB;

    const/16 v6, 0x2d0e

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    const/16 v8, 0x12c7

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x12cf

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x14f1

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x2a46

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2914

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x1510

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0x150f

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const/16 v15, 0x12cc

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/34H;->a(LX/0QB;)LX/34H;

    move-result-object v16

    check-cast v16, LX/34H;

    invoke-static/range {p0 .. p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v17

    check-cast v17, LX/2Mk;

    invoke-static/range {p0 .. p0}, LX/2N9;->a(LX/0QB;)LX/2N9;

    move-result-object v18

    check-cast v18, LX/2N9;

    invoke-direct/range {v1 .. v18}, LX/34G;-><init>(LX/0Or;LX/2Ly;LX/2OS;LX/0lB;LX/0Ot;Landroid/content/res/Resources;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/34H;LX/2Mk;LX/2N9;)V

    .line 494733
    return-object v1
.end method

.method public static final c(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 3

    .prologue
    .line 494729
    const-string v0, "photo_type"

    const-string v1, "drawing"

    .line 494730
    invoke-static {p0}, LX/2Mk;->q(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 494731
    return v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static f(LX/34G;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 494868
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 494869
    iget-object v1, p0, LX/34G;->m:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz p2, :cond_4

    iget-object v1, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    if-eqz v1, :cond_4

    .line 494870
    iget-object v1, p2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/34G;->d:LX/0lB;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/messaging/model/threads/NicknamesMap;->a(Ljava/lang/String;LX/0lC;)Ljava/lang/String;

    move-result-object v1

    .line 494871
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 494872
    :goto_0
    move-object v1, v1

    .line 494873
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    if-eqz v2, :cond_0

    .line 494874
    iget-object v1, p0, LX/34G;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oi;

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v1, v2}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v2

    .line 494875
    iget-object v1, p0, LX/34G;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oj;

    invoke-virtual {v1, v2}, LX/2Oj;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v1

    .line 494876
    :cond_0
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    :cond_1
    move-object v0, v1

    .line 494877
    if-nez v0, :cond_2

    .line 494878
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    .line 494879
    :cond_2
    if-nez v0, :cond_3

    .line 494880
    iget-object v1, p0, LX/34G;->q:LX/2N9;

    const-string v2, "MessageSnippetHelper.getSenderNameForMessage"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/2N9;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 494881
    :cond_3
    return-object v0

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;
    .locals 8
    .param p2    # Lcom/facebook/messaging/model/threads/ThreadCustomization;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 494711
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 494712
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iget-object v0, v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 494713
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-direct {p0}, LX/34G;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v3

    invoke-static {v0, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 494714
    iget-object v3, p0, LX/34G;->b:LX/2Ly;

    invoke-virtual {v3, p1}, LX/2Ly;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v3

    .line 494715
    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iget-object v4, v4, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->c:Ljava/lang/String;

    .line 494716
    invoke-static {p0, p1, p2}, LX/34G;->f(LX/34G;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v5

    .line 494717
    sget-object v6, LX/6eh;->AUDIO_CLIP:LX/6eh;

    if-ne v3, v6, :cond_2

    .line 494718
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v3, 0x7f080346

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 494719
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 494720
    goto :goto_0

    .line 494721
    :cond_1
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v3, 0x7f080347

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v5, v6, v2

    aput-object v4, v6, v1

    invoke-virtual {v0, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 494722
    :cond_2
    sget-object v6, LX/6eh;->VIDEO_CLIP:LX/6eh;

    if-ne v3, v6, :cond_4

    .line 494723
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v3, 0x7f080348

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v3, 0x7f080349

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v5, v6, v2

    aput-object v4, v6, v1

    invoke-virtual {v0, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 494724
    :cond_4
    invoke-static {p1}, LX/2OS;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 494725
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v3, 0x7f08034a

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v3, 0x7f08034b

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v5, v6, v2

    aput-object v4, v6, v1

    invoke-virtual {v0, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 494726
    :cond_6
    invoke-static {p1}, LX/2OS;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 494727
    if-eqz v0, :cond_7

    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v3, 0x7f080344

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_7
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v3, 0x7f080345

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v5, v6, v2

    aput-object v4, v6, v1

    invoke-virtual {v0, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 494728
    :cond_8
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {p0, p1, v0}, LX/34G;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;LX/Di4;Z)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 494690
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494691
    invoke-virtual {p0, p1}, LX/34G;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v0

    .line 494692
    if-eqz v0, :cond_1

    .line 494693
    :cond_0
    :goto_0
    return-object v0

    .line 494694
    :cond_1
    iget-object v0, p0, LX/34G;->o:LX/34H;

    invoke-virtual {v0, p1}, LX/34H;->a(Lcom/facebook/messaging/model/messages/Message;)LX/CMk;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 494695
    invoke-virtual {p0, p1, p2}, LX/34G;->d(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494696
    :cond_2
    invoke-static {p1}, LX/34G;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 494697
    invoke-virtual {p0, p1, p2}, LX/34G;->e(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494698
    :cond_3
    invoke-virtual {p0, p1}, LX/34G;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 494699
    invoke-virtual {p0, p1, p2}, LX/34G;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494700
    :cond_4
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 494701
    if-eqz p4, :cond_5

    .line 494702
    iget-object v3, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v4, 0x7f082e79

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p0, p1, p2}, LX/34G;->f(LX/34G;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    aput-object v0, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494703
    :cond_5
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 494704
    invoke-virtual {p0, p1, p2}, LX/34G;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494705
    :cond_6
    iget-object v3, p0, LX/34G;->b:LX/2Ly;

    invoke-virtual {v3, p1}, LX/2Ly;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v3

    sget-object v4, LX/6eh;->STICKER:LX/6eh;

    if-eq v3, v4, :cond_0

    .line 494706
    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v4, LX/5e9;->GROUP:LX/5e9;

    if-ne v3, v4, :cond_8

    .line 494707
    :goto_1
    if-nez v1, :cond_7

    sget-object v1, LX/Di4;->ALWAYS:LX/Di4;

    if-ne p3, v1, :cond_0

    .line 494708
    :cond_7
    invoke-static {p0, p1, p2}, LX/34G;->f(LX/34G;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v1

    .line 494709
    const-string v2, "%s: %s"

    invoke-static {v2, v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_8
    move v1, v2

    .line 494710
    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 494689
    iget-object v0, p0, LX/34G;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iget-object v0, v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 494676
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 494677
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->ADMIN:LX/2uW;

    if-ne v0, v1, :cond_0

    .line 494678
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 494679
    :goto_0
    return-object v0

    .line 494680
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_2

    .line 494681
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-direct {p0}, LX/34G;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 494682
    if-eqz v0, :cond_1

    .line 494683
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v1, 0x7f0806d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494684
    :cond_1
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-static {p0, p1, v0}, LX/34G;->f(LX/34G;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    .line 494685
    iget-object v1, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v2, 0x7f0806d2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494686
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 494687
    sget-object v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {p0, p1, v0}, LX/34G;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 494688
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 494663
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/34G;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 494675
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/34G;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;
    .locals 5
    .param p2    # Lcom/facebook/messaging/model/threads/ThreadCustomization;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 494668
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-direct {p0}, LX/34G;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 494669
    const-string v0, ""

    .line 494670
    if-nez v1, :cond_0

    .line 494671
    invoke-static {p0, p1, p2}, LX/34G;->f(LX/34G;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    .line 494672
    :cond_0
    if-eqz v1, :cond_1

    .line 494673
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v1, 0x7f0805b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 494674
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v2, 0x7f0805b8

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;
    .locals 5
    .param p2    # Lcom/facebook/messaging/model/threads/ThreadCustomization;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 494664
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-direct {p0}, LX/34G;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 494665
    if-eqz v0, :cond_0

    .line 494666
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v1, 0x7f082e77

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 494667
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/34G;->f:Landroid/content/res/Resources;

    const v1, 0x7f082e78

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0, p1, p2}, LX/34G;->f(LX/34G;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
