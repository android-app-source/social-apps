.class public LX/3Py;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public b:LX/3Pz;

.field private c:LX/2Gc;

.field public d:LX/2Gq;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3Pz;LX/2Gc;LX/2Gq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562592
    iput-object p1, p0, LX/3Py;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 562593
    iput-object p2, p0, LX/3Py;->b:LX/3Pz;

    .line 562594
    iput-object p3, p0, LX/3Py;->c:LX/2Gc;

    .line 562595
    iput-object p4, p0, LX/3Py;->d:LX/2Gq;

    .line 562596
    return-void
.end method

.method public static a(LX/0QB;)LX/3Py;
    .locals 1

    .prologue
    .line 562597
    invoke-static {p0}, LX/3Py;->b(LX/0QB;)LX/3Py;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3Py;
    .locals 5

    .prologue
    .line 562598
    new-instance v4, LX/3Py;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/3Pz;->a(LX/0QB;)LX/3Pz;

    move-result-object v1

    check-cast v1, LX/3Pz;

    invoke-static {p0}, LX/2Gc;->a(LX/0QB;)LX/2Gc;

    move-result-object v2

    check-cast v2, LX/2Gc;

    invoke-static {p0}, LX/2Gq;->a(LX/0QB;)LX/2Gq;

    move-result-object v3

    check-cast v3, LX/2Gq;

    invoke-direct {v4, v0, v1, v2, v3}, LX/3Py;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3Pz;LX/2Gc;LX/2Gq;)V

    .line 562599
    return-object v4
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 562600
    iget-object v0, p0, LX/3Py;->c:LX/2Gc;

    new-instance v1, LX/FC1;

    invoke-direct {v1, p0, p1}, LX/FC1;-><init>(LX/3Py;Z)V

    .line 562601
    invoke-virtual {v0}, LX/2Gc;->a()Ljava/util/Set;

    move-result-object p0

    .line 562602
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/2Ge;

    .line 562603
    invoke-virtual {v1, p0}, LX/FC1;->a(LX/2Ge;)V

    goto :goto_0

    .line 562604
    :cond_0
    return-void
.end method
