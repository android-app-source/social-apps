.class public final enum LX/32M;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/32M;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/32M;

.field public static final enum CLEAR:LX/32M;

.field public static final enum DETECT:LX/32M;

.field public static final enum REMOVE:LX/32M;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 489862
    new-instance v0, LX/32M;

    const-string v1, "DETECT"

    invoke-direct {v0, v1, v2}, LX/32M;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32M;->DETECT:LX/32M;

    new-instance v0, LX/32M;

    const-string v1, "CLEAR"

    invoke-direct {v0, v1, v3}, LX/32M;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32M;->CLEAR:LX/32M;

    new-instance v0, LX/32M;

    const-string v1, "REMOVE"

    invoke-direct {v0, v1, v4}, LX/32M;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/32M;->REMOVE:LX/32M;

    .line 489863
    const/4 v0, 0x3

    new-array v0, v0, [LX/32M;

    sget-object v1, LX/32M;->DETECT:LX/32M;

    aput-object v1, v0, v2

    sget-object v1, LX/32M;->CLEAR:LX/32M;

    aput-object v1, v0, v3

    sget-object v1, LX/32M;->REMOVE:LX/32M;

    aput-object v1, v0, v4

    sput-object v0, LX/32M;->$VALUES:[LX/32M;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 489865
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/32M;
    .locals 1

    .prologue
    .line 489866
    const-class v0, LX/32M;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/32M;

    return-object v0
.end method

.method public static values()[LX/32M;
    .locals 1

    .prologue
    .line 489864
    sget-object v0, LX/32M;->$VALUES:[LX/32M;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/32M;

    return-object v0
.end method
