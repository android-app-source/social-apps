.class public LX/2vg;
.super Lcom/facebook/location/BaseFbLocationManager;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1wc;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field public final d:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final e:LX/03V;

.field private final f:Landroid/os/Handler;

.field public g:LX/2vk;

.field public h:LX/2wX;

.field public final i:LX/2vh;

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 478460
    const-class v0, LX/2vg;

    sput-object v0, LX/2vg;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0y3;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/1wc;LX/0Or;Landroid/os/Handler;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Zb;LX/03V;LX/0y2;LX/0Uo;)V
    .locals 10
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForLightweightTaskHandlerThread;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0y3;",
            "LX/0SG;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/1wc;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Landroid/os/Handler;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0Zb;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0y2;",
            "LX/0Uo;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 478452
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p10

    move-object/from16 v9, p11

    invoke-direct/range {v1 .. v9}, Lcom/facebook/location/BaseFbLocationManager;-><init>(LX/0y3;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Zb;LX/0y2;LX/0Uo;)V

    .line 478453
    new-instance v1, LX/2vh;

    invoke-direct {v1, p0}, LX/2vh;-><init>(LX/2vg;)V

    iput-object v1, p0, LX/2vg;->i:LX/2vh;

    .line 478454
    move-object/from16 v0, p6

    iput-object v0, p0, LX/2vg;->f:Landroid/os/Handler;

    .line 478455
    iput-object p4, p0, LX/2vg;->b:LX/1wc;

    .line 478456
    iput-object p3, p0, LX/2vg;->c:Ljava/util/concurrent/ExecutorService;

    .line 478457
    move-object/from16 v0, p7

    iput-object v0, p0, LX/2vg;->d:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 478458
    move-object/from16 v0, p9

    iput-object v0, p0, LX/2vg;->e:LX/03V;

    .line 478459
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/2vg;Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 4

    .prologue
    .line 478437
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2vg;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 478438
    :goto_0
    monitor-exit p0

    return-void

    .line 478439
    :cond_0
    :try_start_1
    sget-object v0, LX/2wk;->CLIENT_CONNECT:LX/2wk;

    .line 478440
    iget-object v1, p0, LX/2vg;->d:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v2, v0, LX/2wk;->perfMarkerId:I

    iget-object v3, v0, LX/2wk;->perfLoggerName:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 478441
    iget-object v0, p0, LX/2vg;->h:LX/2wX;

    iget-object v1, p0, LX/2vg;->i:LX/2vh;

    invoke-virtual {v0, v1}, LX/2wX;->a(LX/1qf;)V

    .line 478442
    const/4 v0, 0x0

    iput-object v0, p0, LX/2vg;->h:LX/2wX;

    .line 478443
    invoke-static {p0}, LX/2vg;->g$redex0(LX/2vg;)V

    .line 478444
    sget-object v0, LX/2vg;->a:Ljava/lang/Class;

    const-string v1, "Client connection failed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 478445
    iget v0, p1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v0, v0

    .line 478446
    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    iget v0, p1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v0, v0

    .line 478447
    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 478448
    if-nez v0, :cond_2

    .line 478449
    iget-object v0, p0, LX/2vg;->e:LX/03V;

    const-string v1, "location_manager_google_play"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Client connection failed from resolvable error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 478450
    :cond_2
    new-instance v0, LX/2Ce;

    sget-object v1, LX/2sk;->UNKNOWN_ERROR:LX/2sk;

    invoke-direct {v0, v1}, LX/2Ce;-><init>(LX/2sk;)V

    invoke-virtual {p0, v0}, Lcom/facebook/location/BaseFbLocationManager;->a(LX/2Ce;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 478451
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static declared-synchronized e$redex0(LX/2vg;)V
    .locals 6

    .prologue
    .line 478421
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2vg;->j:Z

    if-nez v0, :cond_0

    .line 478422
    invoke-static {p0}, LX/2vg;->g$redex0(LX/2vg;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478423
    :goto_0
    monitor-exit p0

    return-void

    .line 478424
    :cond_0
    :try_start_1
    sget-object v0, LX/2wk;->CLIENT_CONNECT:LX/2wk;

    .line 478425
    iget-object v1, p0, LX/2vg;->d:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v2, v0, LX/2wk;->perfMarkerId:I

    iget-object v3, v0, LX/2wk;->perfLoggerName:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 478426
    sget-object v0, LX/2vm;->b:LX/2vu;

    iget-object v1, p0, LX/2vg;->h:LX/2wX;

    invoke-interface {v0, v1}, LX/2vu;->a(LX/2wX;)Landroid/location/Location;

    move-result-object v0

    .line 478427
    invoke-static {v0}, Lcom/facebook/location/ImmutableLocation;->c(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v1

    .line 478428
    const/4 v0, 0x0

    .line 478429
    if-eqz v1, :cond_1

    .line 478430
    invoke-virtual {p0, v1}, Lcom/facebook/location/BaseFbLocationManager;->a(Lcom/facebook/location/ImmutableLocation;)V

    .line 478431
    invoke-virtual {p0, v1}, Lcom/facebook/location/BaseFbLocationManager;->b(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v2

    const-wide/32 v4, 0xdbba0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    .line 478432
    const/4 v0, 0x1

    .line 478433
    :cond_1
    if-nez v0, :cond_2

    .line 478434
    invoke-virtual {p0}, Lcom/facebook/location/BaseFbLocationManager;->d()V

    .line 478435
    :cond_2
    iget-object v0, p0, LX/2vg;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/location/GooglePlayFbLocationManager$2;

    invoke-direct {v1, p0}, Lcom/facebook/location/GooglePlayFbLocationManager$2;-><init>(LX/2vg;)V

    const v2, -0x711ea841

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 478436
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized f$redex0(LX/2vg;)V
    .locals 2

    .prologue
    .line 478392
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2vg;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 478393
    :goto_0
    monitor-exit p0

    return-void

    .line 478394
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2vg;->h:LX/2wX;

    iget-object v1, p0, LX/2vg;->i:LX/2vh;

    invoke-virtual {v0, v1}, LX/2wX;->a(LX/1qf;)V

    .line 478395
    const/4 v0, 0x0

    iput-object v0, p0, LX/2vg;->h:LX/2wX;

    .line 478396
    invoke-static {p0}, LX/2vg;->g$redex0(LX/2vg;)V

    .line 478397
    sget-object v0, LX/2vg;->a:Ljava/lang/Class;

    const-string v1, "Client disconnected unexpectedly"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 478398
    new-instance v0, LX/2Ce;

    sget-object v1, LX/2sk;->UNKNOWN_ERROR:LX/2sk;

    invoke-direct {v0, v1}, LX/2Ce;-><init>(LX/2sk;)V

    invoke-virtual {p0, v0}, Lcom/facebook/location/BaseFbLocationManager;->a(LX/2Ce;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 478399
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static g$redex0(LX/2vg;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 478412
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2vg;->j:Z

    .line 478413
    iput-object v2, p0, LX/2vg;->g:LX/2vk;

    .line 478414
    iget-object v0, p0, LX/2vg;->h:LX/2wX;

    if-eqz v0, :cond_0

    .line 478415
    sget-object v0, LX/2wk;->CLIENT_CONNECT:LX/2wk;

    .line 478416
    iget-object v1, p0, LX/2vg;->d:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v3, v0, LX/2wk;->perfMarkerId:I

    iget-object v4, v0, LX/2wk;->perfLoggerName:Ljava/lang/String;

    invoke-interface {v1, v3, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 478417
    iget-object v0, p0, LX/2vg;->h:LX/2wX;

    iget-object v1, p0, LX/2vg;->i:LX/2vh;

    invoke-virtual {v0, v1}, LX/2wX;->a(LX/1qf;)V

    .line 478418
    iget-object v0, p0, LX/2vg;->h:LX/2wX;

    invoke-virtual {v0}, LX/2wX;->g()V

    .line 478419
    iput-object v2, p0, LX/2vg;->h:LX/2wX;

    .line 478420
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/1wZ;
    .locals 1

    .prologue
    .line 478411
    sget-object v0, LX/1wZ;->GOOGLE_PLAY:LX/1wZ;

    return-object v0
.end method

.method public final declared-synchronized a(LX/2vk;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 478404
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/2vg;->j:Z

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 478405
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2vg;->j:Z

    .line 478406
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2vk;

    iput-object v0, p0, LX/2vg;->g:LX/2vk;

    .line 478407
    iget-object v0, p0, LX/2vg;->b:LX/1wc;

    iget-object v1, p0, LX/2vg;->i:LX/2vh;

    iget-object v2, p0, LX/2vg;->i:LX/2vh;

    sget-object v3, LX/2vm;->a:LX/2vs;

    iget-object v4, p0, LX/2vg;->f:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1wc;->a(LX/1qf;LX/1qg;LX/2vs;Landroid/os/Handler;)LX/2wX;

    move-result-object v0

    iput-object v0, p0, LX/2vg;->h:LX/2wX;

    .line 478408
    iget-object v0, p0, LX/2vg;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/location/GooglePlayFbLocationManager$1;

    invoke-direct {v1, p0}, Lcom/facebook/location/GooglePlayFbLocationManager$1;-><init>(LX/2vg;)V

    const v2, 0x70166372

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 478409
    monitor-exit p0

    return-void

    .line 478410
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 478400
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2vg;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 478401
    :goto_0
    monitor-exit p0

    return-void

    .line 478402
    :cond_0
    :try_start_1
    invoke-static {p0}, LX/2vg;->g$redex0(LX/2vg;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 478403
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
