.class public LX/2ee;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/24a;
.implements LX/2ef;
.implements LX/2eg;


# static fields
.field public static final b:LX/1Cz;


# instance fields
.field private final a:Lcom/facebook/feed/rows/views/ContentTextView;

.field public final c:Landroid/widget/ImageView;

.field private final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 445430
    new-instance v0, LX/2eh;

    invoke-direct {v0}, LX/2eh;-><init>()V

    sput-object v0, LX/2ee;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 445428
    const v0, 0x7f031495

    invoke-direct {p0, p1, v0}, LX/2ee;-><init>(Landroid/content/Context;I)V

    .line 445429
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 445403
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 445404
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 445405
    const v0, 0x7f0d2ec4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/views/ContentTextView;

    iput-object v0, p0, LX/2ee;->a:Lcom/facebook/feed/rows/views/ContentTextView;

    .line 445406
    const v0, 0x7f0d0bde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/2ee;->c:Landroid/widget/ImageView;

    .line 445407
    invoke-virtual {p0}, LX/2ee;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/2ee;->d:I

    .line 445408
    return-void
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 445426
    iget-object v0, p0, LX/2ee;->c:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 445427
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;LX/2ej;)V
    .locals 3

    .prologue
    .line 445422
    iget-object v0, p0, LX/2ee;->a:Lcom/facebook/feed/rows/views/ContentTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/rows/views/ContentTextView;->setText(Ljava/lang/CharSequence;)V

    .line 445423
    iget-object v1, p0, LX/2ee;->a:Lcom/facebook/feed/rows/views/ContentTextView;

    const v2, 0x7f0d0081

    sget-object v0, LX/2ej;->SPONSORED:LX/2ej;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/feed/rows/views/ContentTextView;->setTag(ILjava/lang/Object;)V

    .line 445424
    return-void

    .line 445425
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 445420
    iget-object v0, p0, LX/2ee;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 445421
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMenuButtonActive(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 445413
    iget-object v2, p0, LX/2ee;->c:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 445414
    iget-object v0, p0, LX/2ee;->a:Lcom/facebook/feed/rows/views/ContentTextView;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/views/ContentTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 445415
    if-eqz p1, :cond_1

    :goto_1
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 445416
    iget-object v1, p0, LX/2ee;->a:Lcom/facebook/feed/rows/views/ContentTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/views/ContentTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 445417
    return-void

    .line 445418
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 445419
    :cond_1
    iget v1, p0, LX/2ee;->d:I

    goto :goto_1
.end method

.method public setStyle(LX/2ei;)V
    .locals 3

    .prologue
    .line 445409
    iget-object v0, p0, LX/2ee;->a:Lcom/facebook/feed/rows/views/ContentTextView;

    invoke-virtual {p0}, LX/2ee;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/2ei;->getColor(Landroid/content/res/Resources;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/views/ContentTextView;->setTextColor(I)V

    .line 445410
    iget-object v0, p0, LX/2ee;->a:Lcom/facebook/feed/rows/views/ContentTextView;

    const/4 v1, 0x0

    invoke-virtual {p1}, LX/2ei;->getFontStyle()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/feed/rows/views/ContentTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 445411
    iget-object v0, p0, LX/2ee;->a:Lcom/facebook/feed/rows/views/ContentTextView;

    invoke-virtual {p0}, LX/2ee;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/2ei;->getFontSize(Landroid/content/res/Resources;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/views/ContentTextView;->setTextSize(F)V

    .line 445412
    return-void
.end method
