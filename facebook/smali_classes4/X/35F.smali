.class public final LX/35F;
.super LX/0xh;
.source ""


# instance fields
.field private final a:LX/35E;


# direct methods
.method public constructor <init>(LX/35E;)V
    .locals 0

    .prologue
    .line 496942
    invoke-direct {p0}, LX/0xh;-><init>()V

    .line 496943
    iput-object p1, p0, LX/35F;->a:LX/35E;

    .line 496944
    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 8

    .prologue
    .line 496945
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 496946
    const v1, 0x3fa66666    # 1.3f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    .line 496947
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 496948
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, LX/35F;->a:LX/35E;

    invoke-virtual {v2}, LX/35E;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, LX/35F;->a:LX/35E;

    invoke-virtual {v3}, LX/35E;->getIntrinsicHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 496949
    iget-wide v6, p1, LX/0wd;->i:D

    move-wide v2, v6

    .line 496950
    float-to-double v4, v0

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    float-to-double v4, v1

    cmpl-double v1, v2, v4

    if-lez v1, :cond_1

    .line 496951
    iget-object v1, p0, LX/35F;->a:LX/35E;

    const v2, 0x459c4000    # 5000.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-virtual {v1, v0}, LX/35E;->setLevel(I)Z

    .line 496952
    :goto_0
    return-void

    .line 496953
    :cond_1
    iget-object v0, p0, LX/35F;->a:LX/35E;

    const-wide v2, 0x40b3880000000000L    # 5000.0

    .line 496954
    iget-wide v6, p1, LX/0wd;->i:D

    move-wide v4, v6

    .line 496955
    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, LX/35E;->setLevel(I)Z

    goto :goto_0
.end method
