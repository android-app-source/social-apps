.class public LX/299;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/299;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2AX;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/mqtt/service/MqttTopicList;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2AX;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 375712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375713
    iput-object p1, p0, LX/299;->a:LX/0Ot;

    .line 375714
    return-void
.end method

.method public static a(LX/0QB;)LX/299;
    .locals 5

    .prologue
    .line 375715
    sget-object v0, LX/299;->b:LX/299;

    if-nez v0, :cond_1

    .line 375716
    const-class v1, LX/299;

    monitor-enter v1

    .line 375717
    :try_start_0
    sget-object v0, LX/299;->b:LX/299;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 375718
    if-eqz v2, :cond_0

    .line 375719
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 375720
    new-instance v3, LX/299;

    .line 375721
    new-instance v4, LX/2C5;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v4, p0}, LX/2C5;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 375722
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v4, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v4

    move-object v4, v4

    .line 375723
    invoke-direct {v3, v4}, LX/299;-><init>(LX/0Ot;)V

    .line 375724
    move-object v0, v3

    .line 375725
    sput-object v0, LX/299;->b:LX/299;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375726
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 375727
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 375728
    :cond_1
    sget-object v0, LX/299;->b:LX/299;

    return-object v0

    .line 375729
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 375730
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
