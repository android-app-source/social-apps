.class public abstract LX/2fo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2fp;


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:LX/03V;

.field private final c:LX/0bH;

.field public final d:Landroid/view/View$OnClickListener;

.field private final e:LX/2fk;

.field private final f:LX/1Sa;

.field private final g:LX/1Sj;

.field private final h:LX/2fh;

.field private final i:LX/14w;

.field private final j:Landroid/content/Context;

.field private final k:LX/1Sl;

.field private final l:LX/0tX;

.field private final m:Ljava/util/concurrent/ExecutorService;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/2fm;

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field private r:Lcom/facebook/graphql/model/GraphQLNode;

.field private s:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/2fk;LX/14w;Landroid/content/Context;LX/1Sl;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/2fm;Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;LX/0Ot;)V
    .locals 3
    .param p15    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p17    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Sa;",
            "LX/1Sj;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2fh;",
            "LX/0bH;",
            "LX/2fk;",
            "LX/14w;",
            "Landroid/content/Context;",
            "LX/1Sl;",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;",
            "LX/2fm;",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 447580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447581
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/2fo;->a:Ljava/lang/String;

    .line 447582
    iput-object p1, p0, LX/2fo;->f:LX/1Sa;

    .line 447583
    iput-object p2, p0, LX/2fo;->g:LX/1Sj;

    .line 447584
    iput-object p3, p0, LX/2fo;->b:LX/03V;

    .line 447585
    iput-object p4, p0, LX/2fo;->h:LX/2fh;

    .line 447586
    iput-object p5, p0, LX/2fo;->c:LX/0bH;

    .line 447587
    iput-object p6, p0, LX/2fo;->e:LX/2fk;

    .line 447588
    iput-object p7, p0, LX/2fo;->i:LX/14w;

    .line 447589
    iput-object p8, p0, LX/2fo;->j:Landroid/content/Context;

    .line 447590
    iput-object p9, p0, LX/2fo;->k:LX/1Sl;

    .line 447591
    iput-object p10, p0, LX/2fo;->l:LX/0tX;

    .line 447592
    iput-object p11, p0, LX/2fo;->m:Ljava/util/concurrent/ExecutorService;

    .line 447593
    iput-object p12, p0, LX/2fo;->n:LX/0Ot;

    .line 447594
    move-object/from16 v0, p13

    iput-object v0, p0, LX/2fo;->o:LX/2fm;

    .line 447595
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2fo;->q:Ljava/lang/String;

    .line 447596
    move-object/from16 v0, p17

    iput-object v0, p0, LX/2fo;->v:Landroid/view/View$OnClickListener;

    .line 447597
    move-object/from16 v0, p14

    iput-object v0, p0, LX/2fo;->r:Lcom/facebook/graphql/model/GraphQLNode;

    .line 447598
    move-object/from16 v0, p16

    iput-object v0, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447599
    move-object/from16 v0, p18

    iput-object v0, p0, LX/2fo;->p:LX/0Ot;

    .line 447600
    new-instance v1, LX/2fq;

    invoke-direct {v1, p0}, LX/2fq;-><init>(LX/2fo;)V

    iput-object v1, p0, LX/2fo;->d:Landroid/view/View$OnClickListener;

    .line 447601
    iget-object v1, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 447602
    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 447603
    iget-object v1, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/182;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iput-object v1, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447604
    iget-object v2, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447605
    invoke-virtual {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/2fo;->t:Ljava/lang/String;

    .line 447606
    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/2fo;->u:Ljava/lang/String;

    .line 447607
    :goto_0
    return-void

    .line 447608
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, LX/2fo;->t:Ljava/lang/String;

    .line 447609
    const/4 v1, 0x0

    iput-object v1, p0, LX/2fo;->u:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/FeedUnit;)LX/0Px;
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447574
    if-nez p1, :cond_0

    .line 447575
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 447576
    :goto_0
    return-object v0

    .line 447577
    :cond_0
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 447578
    iget-object v0, p0, LX/2fo;->i:LX/14w;

    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, p1}, LX/14w;->h(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 447579
    :cond_1
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 447569
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    if-eqz v0, :cond_2

    .line 447570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 447571
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 447572
    goto :goto_0

    :cond_1
    move v1, v2

    .line 447573
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static a$redex0(LX/2fo;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 447562
    invoke-virtual {p0}, LX/2fo;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 447563
    :cond_0
    :goto_0
    return-void

    .line 447564
    :cond_1
    invoke-virtual {p0}, LX/2fo;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 447565
    invoke-direct {p0, p1}, LX/2fo;->b(Landroid/view/View;)V

    .line 447566
    :goto_1
    iget-object v0, p0, LX/2fo;->v:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 447567
    iget-object v0, p0, LX/2fo;->v:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 447568
    :cond_2
    invoke-direct {p0}, LX/2fo;->d()V

    goto :goto_1
.end method

.method private b(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 447519
    iget-object v0, p0, LX/2fo;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79m;

    invoke-virtual {v0, p1}, LX/79m;->a(Landroid/view/View;)V

    .line 447520
    iget-object v0, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447521
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 447522
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 447523
    iget-object v1, p0, LX/2fo;->f:LX/1Sa;

    iget-object v2, p0, LX/2fo;->j:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/1Sa;->a(Landroid/content/Context;)V

    .line 447524
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 447525
    iget-object v0, p0, LX/2fo;->g:LX/1Sj;

    iget-object v1, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    const-string v2, "toggle_button"

    iget-object v3, p0, LX/2fo;->q:Ljava/lang/String;

    new-instance v4, LX/AEk;

    invoke-direct {v4, p0}, LX/AEk;-><init>(LX/2fo;)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1Sj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;LX/79i;)V

    .line 447526
    invoke-virtual {p0, v5}, LX/2fo;->a(Z)V

    .line 447527
    :goto_0
    return-void

    .line 447528
    :cond_0
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    if-eqz v1, :cond_3

    .line 447529
    iget-object v1, p0, LX/2fo;->k:LX/1Sl;

    invoke-virtual {v1}, LX/1Sl;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 447530
    invoke-direct {p0, v5}, LX/2fo;->c(Z)V

    goto :goto_0

    .line 447531
    :cond_1
    invoke-direct {p0, v5}, LX/2fo;->b(Z)V

    .line 447532
    invoke-virtual {p0, v5}, LX/2fo;->a(Z)V

    .line 447533
    iget-object v1, p0, LX/2fo;->r:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v1

    .line 447534
    new-instance v2, LX/5vM;

    invoke-direct {v2}, LX/5vM;-><init>()V

    invoke-interface {v1}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v3

    .line 447535
    iput-object v3, v2, LX/5vM;->a:Ljava/lang/String;

    .line 447536
    move-object v2, v2

    .line 447537
    iget-object v3, p0, LX/2fo;->r:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    .line 447538
    iput-object v3, v2, LX/5vM;->b:Ljava/lang/String;

    .line 447539
    move-object v2, v2

    .line 447540
    invoke-interface {v1}, LX/1oP;->d()LX/1oQ;

    move-result-object v3

    invoke-interface {v3}, LX/1oQ;->c()Ljava/lang/String;

    move-result-object v3

    .line 447541
    iput-object v3, v2, LX/5vM;->g:Ljava/lang/String;

    .line 447542
    move-object v2, v2

    .line 447543
    invoke-direct {p0, v0}, LX/2fo;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/0Px;

    move-result-object v3

    .line 447544
    iput-object v3, v2, LX/5vM;->h:LX/0Px;

    .line 447545
    move-object v2, v2

    .line 447546
    iget-object v3, p0, LX/2fo;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/5vM;->c(Ljava/lang/String;)LX/5vM;

    move-result-object v2

    const-string v3, "toggle_button"

    invoke-virtual {v2, v3}, LX/5vM;->d(Ljava/lang/String;)LX/5vM;

    move-result-object v2

    .line 447547
    iput-boolean v5, v2, LX/5vM;->j:Z

    .line 447548
    move-object v2, v2

    .line 447549
    iget-object v3, p0, LX/2fo;->t:Ljava/lang/String;

    .line 447550
    iput-object v3, v2, LX/5vM;->l:Ljava/lang/String;

    .line 447551
    move-object v2, v2

    .line 447552
    iget-object v3, p0, LX/2fo;->u:Ljava/lang/String;

    .line 447553
    iput-object v3, v2, LX/5vM;->k:Ljava/lang/String;

    .line 447554
    move-object v2, v2

    .line 447555
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 447556
    :goto_1
    iput-object v0, v2, LX/5vM;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 447557
    move-object v0, v2

    .line 447558
    invoke-virtual {v0}, LX/5vM;->a()Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    move-result-object v0

    .line 447559
    iget-object v2, p0, LX/2fo;->h:LX/2fh;

    invoke-virtual {v2, p0, v1, v0}, LX/2fh;->a(LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 447560
    :cond_2
    invoke-interface {v0}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    goto :goto_1

    .line 447561
    :cond_3
    iget-object v0, p0, LX/2fo;->b:LX/03V;

    iget-object v1, p0, LX/2fo;->a:Ljava/lang/String;

    const-string v2, "Saving something that isn\'t a Story or SavedCollectionFeedUnit. Item was not saved."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 447514
    iget-object v1, p0, LX/2fo;->e:LX/2fk;

    iget-object v0, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447515
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 447516
    check-cast v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    iget-object v2, p0, LX/2fo;->r:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/2fk;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;LX/0P1;)Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    move-result-object v0

    .line 447517
    iget-object v1, p0, LX/2fo;->c:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 447518
    return-void
.end method

.method private c(Z)V
    .locals 8

    .prologue
    .line 447610
    iget-object v7, p0, LX/2fo;->l:LX/0tX;

    iget-object v0, p0, LX/2fo;->o:LX/2fm;

    iget-object v1, p0, LX/2fo;->r:Lcom/facebook/graphql/model/GraphQLNode;

    iget-object v2, p0, LX/2fo;->t:Ljava/lang/String;

    iget-object v3, p0, LX/2fo;->q:Ljava/lang/String;

    const-string v4, "toggle_button"

    iget-object v5, p0, LX/2fo;->u:Ljava/lang/String;

    move v6, p1

    invoke-virtual/range {v0 .. v6}, LX/2fm;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/AEN;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/0tX;->a(LX/4V2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 447611
    new-instance v1, LX/AEl;

    invoke-direct {v1, p0}, LX/AEl;-><init>(LX/2fo;)V

    iget-object v2, p0, LX/2fo;->m:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 447612
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 447475
    iget-object v0, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447476
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 447477
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 447478
    iget-object v1, p0, LX/2fo;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/79m;

    invoke-virtual {v1}, LX/79m;->a()V

    .line 447479
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 447480
    iget-object v0, p0, LX/2fo;->g:LX/1Sj;

    iget-object v1, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    const-string v2, "toggle_button"

    iget-object v3, p0, LX/2fo;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/1Sj;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 447481
    invoke-virtual {p0, v4}, LX/2fo;->a(Z)V

    .line 447482
    :goto_0
    return-void

    .line 447483
    :cond_0
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    if-eqz v1, :cond_3

    .line 447484
    iget-object v1, p0, LX/2fo;->k:LX/1Sl;

    invoke-virtual {v1}, LX/1Sl;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 447485
    invoke-direct {p0, v4}, LX/2fo;->c(Z)V

    goto :goto_0

    .line 447486
    :cond_1
    invoke-direct {p0, v4}, LX/2fo;->b(Z)V

    .line 447487
    invoke-virtual {p0, v4}, LX/2fo;->a(Z)V

    .line 447488
    iget-object v1, p0, LX/2fo;->r:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v1

    .line 447489
    new-instance v2, LX/5vM;

    invoke-direct {v2}, LX/5vM;-><init>()V

    invoke-interface {v1}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v3

    .line 447490
    iput-object v3, v2, LX/5vM;->a:Ljava/lang/String;

    .line 447491
    move-object v2, v2

    .line 447492
    iget-object v3, p0, LX/2fo;->r:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    .line 447493
    iput-object v3, v2, LX/5vM;->b:Ljava/lang/String;

    .line 447494
    move-object v2, v2

    .line 447495
    invoke-direct {p0, v0}, LX/2fo;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/0Px;

    move-result-object v3

    .line 447496
    iput-object v3, v2, LX/5vM;->h:LX/0Px;

    .line 447497
    move-object v2, v2

    .line 447498
    const/4 v3, 0x1

    .line 447499
    iput-boolean v3, v2, LX/5vM;->j:Z

    .line 447500
    move-object v2, v2

    .line 447501
    const-string v3, "toggle_button"

    invoke-virtual {v2, v3}, LX/5vM;->d(Ljava/lang/String;)LX/5vM;

    move-result-object v2

    iget-object v3, p0, LX/2fo;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/5vM;->c(Ljava/lang/String;)LX/5vM;

    move-result-object v2

    iget-object v3, p0, LX/2fo;->t:Ljava/lang/String;

    .line 447502
    iput-object v3, v2, LX/5vM;->l:Ljava/lang/String;

    .line 447503
    move-object v2, v2

    .line 447504
    iget-object v3, p0, LX/2fo;->u:Ljava/lang/String;

    .line 447505
    iput-object v3, v2, LX/5vM;->k:Ljava/lang/String;

    .line 447506
    move-object v2, v2

    .line 447507
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 447508
    :goto_1
    iput-object v0, v2, LX/5vM;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 447509
    move-object v0, v2

    .line 447510
    invoke-virtual {v0}, LX/5vM;->a()Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    move-result-object v0

    .line 447511
    iget-object v2, p0, LX/2fo;->h:LX/2fh;

    invoke-virtual {v2, p0, v1, v0}, LX/2fh;->b(LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 447512
    :cond_2
    invoke-interface {v0}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    goto :goto_1

    .line 447513
    :cond_3
    iget-object v0, p0, LX/2fo;->b:LX/03V;

    iget-object v1, p0, LX/2fo;->a:Ljava/lang/String;

    const-string v2, "Unsaving something that isn\'t a Story or SavedCollectionFeedUnit. Item not unsaved."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 447474
    return-void
.end method

.method public final a(LX/1oP;LX/5vL;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 447460
    invoke-virtual {p0}, LX/2fo;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 447461
    :cond_0
    :goto_0
    return-void

    .line 447462
    :cond_1
    iget-object v0, p0, LX/2fo;->r:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v0

    if-eq p1, v0, :cond_2

    .line 447463
    iget-object v0, p0, LX/2fo;->b:LX/03V;

    iget-object v1, p0, LX/2fo;->a:Ljava/lang/String;

    const-string v2, "Save Button has been rebinded to a different attachment."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 447464
    :cond_2
    iget-object v0, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447465
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 447466
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 447467
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    if-eqz v0, :cond_0

    .line 447468
    sget-object v0, LX/AEm;->a:[I

    invoke-virtual {p2}, LX/5vL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 447469
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 447470
    :pswitch_0
    invoke-direct {p0, v2}, LX/2fo;->b(Z)V

    .line 447471
    invoke-virtual {p0, v2}, LX/2fo;->a(Z)V

    goto :goto_0

    .line 447472
    :pswitch_1
    invoke-direct {p0, v3}, LX/2fo;->b(Z)V

    .line 447473
    invoke-virtual {p0, v3}, LX/2fo;->a(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public abstract a(Z)V
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 447451
    iget-object v0, p0, LX/2fo;->r:Lcom/facebook/graphql/model/GraphQLNode;

    if-nez v0, :cond_0

    .line 447452
    iget-object v0, p0, LX/2fo;->b:LX/03V;

    iget-object v2, p0, LX/2fo;->a:Ljava/lang/String;

    const-string v3, "Save Button is binded without a target object."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 447453
    :goto_0
    return v0

    .line 447454
    :cond_0
    iget-object v2, p0, LX/2fo;->r:Lcom/facebook/graphql/model/GraphQLNode;

    iget-object v0, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447455
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 447456
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v2, v0}, LX/2fo;->a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 447457
    iget-object v0, p0, LX/2fo;->b:LX/03V;

    iget-object v2, p0, LX/2fo;->a:Ljava/lang/String;

    const-string v3, "SaveActionLink does not have enough information for save."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 447458
    goto :goto_0

    .line 447459
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 447438
    iget-object v0, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447439
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 447440
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447441
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 447442
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447443
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 447444
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 447445
    iget-object v0, p0, LX/2fo;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 447446
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 447447
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 447448
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 447449
    goto :goto_0

    .line 447450
    :cond_1
    iget-object v0, p0, LX/2fo;->r:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method
