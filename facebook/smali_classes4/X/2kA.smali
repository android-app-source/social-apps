.class public final LX/2kA;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 455364
    sget-object v0, LX/2kB;->a:LX/0U1;

    sget-object v1, LX/2kB;->b:LX/0U1;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/2kA;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    .line 455368
    const-string v0, "tags"

    sget-object v1, LX/2kA;->a:LX/0Px;

    new-instance v2, LX/0su;

    sget-object v3, LX/2kB;->a:LX/0U1;

    sget-object v4, LX/2kB;->b:LX/0U1;

    invoke-static {v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0su;-><init>(LX/0Px;)V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 455369
    return-void
.end method


# virtual methods
.method public final d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 455365
    invoke-super {p0, p1}, LX/0Tz;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 455366
    const-string v0, "PRAGMA foreign_keys = ON"

    const v1, -0x6f73bf92

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x52577cd3

    invoke-static {v0}, LX/03h;->a(I)V

    .line 455367
    return-void
.end method
