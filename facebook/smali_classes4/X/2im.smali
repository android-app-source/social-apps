.class public final LX/2im;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friends/model/FriendRequest;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:J

.field public final synthetic d:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;ZZJ)V
    .locals 0

    .prologue
    .line 452048
    iput-object p1, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iput-boolean p2, p0, LX/2im;->a:Z

    iput-boolean p3, p0, LX/2im;->b:Z

    iput-wide p4, p0, LX/2im;->c:J

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 452036
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->M:LX/2he;

    iget-object v1, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    .line 452037
    sget-object v3, LX/2he;->a:LX/0P1;

    invoke-virtual {v3, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Yj;

    .line 452038
    if-eqz v3, :cond_0

    .line 452039
    iget-object v4, v0, LX/2he;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v4, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 452040
    :cond_0
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    .line 452041
    invoke-static {v0, p1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Ljava/lang/Throwable;)V

    .line 452042
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_1

    .line 452043
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 452044
    :cond_1
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->E(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    .line 452045
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v1}, LX/2iK;->isEmpty()Z

    move-result v1

    .line 452046
    invoke-static {v0, v1, v2}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;ZZ)V

    .line 452047
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 451987
    check-cast p1, Ljava/util/List;

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 451988
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    .line 451989
    iput-boolean v1, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aP:Z

    .line 451990
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    .line 451991
    iput-boolean v1, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aN:Z

    .line 451992
    iget-boolean v0, p0, LX/2im;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/2im;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451993
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->M:LX/2he;

    iget-object v3, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v3, v3, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aC:LX/2h7;

    iget-object v4, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v4, v4, Lcom/facebook/friending/jewel/FriendRequestsFragment;->r:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, p0, LX/2im;->c:J

    sub-long/2addr v4, v6

    invoke-virtual {v0, v3, v4, v5}, LX/2he;->a(LX/2h7;J)V

    .line 451994
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 451995
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    .line 451996
    if-eqz v0, :cond_1

    .line 451997
    iget-object v5, v0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v5, v5

    .line 451998
    sget-object v6, LX/2lu;->NEEDS_RESPONSE:LX/2lu;

    if-ne v5, v6, :cond_1

    .line 451999
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 452000
    :cond_2
    iget-object v4, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    .line 452001
    iget-boolean v5, v0, Lcom/facebook/friends/model/FriendRequest;->h:Z

    move v0, v5

    .line 452002
    if-nez v0, :cond_8

    move v0, v1

    :goto_1
    invoke-static {v4, v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->g$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    .line 452003
    iget-boolean v0, p0, LX/2im;->b:Z

    if-eqz v0, :cond_3

    .line 452004
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 452005
    iget-object v4, v0, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 452006
    iget-object v4, v0, LX/2iK;->j:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 452007
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    iget-object v4, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-boolean v4, v4, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aH:Z

    .line 452008
    iput-boolean v4, v0, LX/2iK;->s:Z

    .line 452009
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 452010
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->e()V

    .line 452011
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 452012
    iget-object v4, v0, LX/2iK;->p:LX/2ib;

    invoke-virtual {v4}, LX/2ib;->c()V

    .line 452013
    :cond_3
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 452014
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 452015
    :cond_4
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 452016
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friends/model/FriendRequest;

    .line 452017
    if-eqz v4, :cond_5

    .line 452018
    iget-object v6, v0, LX/2iK;->j:Ljava/util/Map;

    .line 452019
    iget-object v7, v4, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    move-object v7, v7

    .line 452020
    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 452021
    iget-object v6, v0, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 452022
    iget-object v6, v0, LX/2iK;->j:Ljava/util/Map;

    .line 452023
    iget-object v7, v4, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    move-object v7, v7

    .line 452024
    invoke-interface {v6, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 452025
    :cond_6
    invoke-virtual {v0}, LX/2iK;->m()V

    .line 452026
    const v4, -0x77e9acab

    invoke-static {v0, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 452027
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_7

    .line 452028
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 452029
    :cond_7
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v2, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v2, v2, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v2}, LX/2iK;->isEmpty()Z

    move-result v2

    .line 452030
    invoke-static {v0, v2, v1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;ZZ)V

    .line 452031
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 452032
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->r(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    .line 452033
    :goto_3
    return-void

    :cond_8
    move v0, v2

    .line 452034
    goto/16 :goto_1

    .line 452035
    :cond_9
    iget-object v0, p0, LX/2im;->d:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto :goto_3
.end method
