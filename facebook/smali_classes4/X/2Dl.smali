.class public LX/2Dl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Dl;


# instance fields
.field private volatile a:Ljava/lang/String;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384651
    iput-object p1, p0, LX/2Dl;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 384652
    return-void
.end method

.method public static a(LX/0QB;)LX/2Dl;
    .locals 4

    .prologue
    .line 384628
    sget-object v0, LX/2Dl;->c:LX/2Dl;

    if-nez v0, :cond_1

    .line 384629
    const-class v1, LX/2Dl;

    monitor-enter v1

    .line 384630
    :try_start_0
    sget-object v0, LX/2Dl;->c:LX/2Dl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 384631
    if-eqz v2, :cond_0

    .line 384632
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 384633
    new-instance p0, LX/2Dl;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/2Dl;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 384634
    move-object v0, p0

    .line 384635
    sput-object v0, LX/2Dl;->c:LX/2Dl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384636
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 384637
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 384638
    :cond_1
    sget-object v0, LX/2Dl;->c:LX/2Dl;

    return-object v0

    .line 384639
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 384640
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 384641
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Dl;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 384642
    iget-object v0, p0, LX/2Dl;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/7fx;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 384643
    if-nez v0, :cond_0

    .line 384644
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 384645
    iget-object v1, p0, LX/2Dl;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/7fx;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 384646
    :cond_0
    move-object v0, v0

    .line 384647
    iput-object v0, p0, LX/2Dl;->a:Ljava/lang/String;

    .line 384648
    :cond_1
    iget-object v0, p0, LX/2Dl;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 384649
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
