.class public final LX/25a;
.super LX/1P5;
.source ""


# direct methods
.method public constructor <init>(LX/1OR;)V
    .locals 1

    .prologue
    .line 369785
    invoke-direct {p0, p1}, LX/1P5;-><init>(LX/1OR;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 369797
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 369798
    invoke-static {p1}, LX/1OR;->i(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v0, v1, v0

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 369795
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0, p1}, LX/1OR;->g(I)V

    .line 369796
    return-void
.end method

.method public final b(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 369793
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 369794
    invoke-static {p1}, LX/1OR;->k(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 369792
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->y()I

    move-result v0

    return v0
.end method

.method public final c(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 369799
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 369800
    invoke-static {p1}, LX/1OR;->g(Landroid/view/View;)I

    move-result v1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 369791
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->w()I

    move-result v0

    iget-object v1, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v1}, LX/1OR;->A()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final d(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 369789
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 369790
    invoke-static {p1}, LX/1OR;->h(Landroid/view/View;)I

    move-result v1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 369788
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->w()I

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 369787
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->w()I

    move-result v0

    iget-object v1, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v1}, LX/1OR;->y()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v1}, LX/1OR;->A()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 369786
    iget-object v0, p0, LX/1P5;->a:LX/1OR;

    invoke-virtual {v0}, LX/1OR;->A()I

    move-result v0

    return v0
.end method
