.class public LX/2NA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2NB;

.field private final b:LX/2N8;


# direct methods
.method public constructor <init>(LX/2NB;LX/2N8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 398638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398639
    iput-object p1, p0, LX/2NA;->a:LX/2NB;

    .line 398640
    iput-object p2, p0, LX/2NA;->b:LX/2N8;

    .line 398641
    return-void
.end method

.method public static a(LX/0QB;)LX/2NA;
    .locals 1

    .prologue
    .line 398637
    invoke-static {p0}, LX/2NA;->b(LX/0QB;)LX/2NA;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2NA;
    .locals 3

    .prologue
    .line 398599
    new-instance v2, LX/2NA;

    invoke-static {p0}, LX/2NB;->b(LX/0QB;)LX/2NB;

    move-result-object v0

    check-cast v0, LX/2NB;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v1

    check-cast v1, LX/2N8;

    invoke-direct {v2, v0, v1}, LX/2NA;-><init>(LX/2NB;LX/2N8;)V

    .line 398600
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/MessageDraft;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 398623
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398624
    :goto_0
    return-object v1

    .line 398625
    :cond_0
    iget-object v0, p0, LX/2NA;->b:LX/2N8;

    invoke-virtual {v0, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 398626
    const-string v0, "text"

    invoke-virtual {v2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v3

    .line 398627
    const/4 v0, 0x0

    .line 398628
    const-string v4, "cursorPosition"

    invoke-virtual {v2, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 398629
    const-string v0, "cursorPosition"

    invoke-virtual {v2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->w()I

    move-result v0

    .line 398630
    :cond_1
    const-string v4, "offlineMessageId"

    invoke-virtual {v2, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 398631
    const-string v1, "offlineMessageId"

    invoke-virtual {v2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    .line 398632
    :cond_2
    const-string v4, "attachmentData"

    invoke-virtual {v2, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 398633
    const-string v4, "attachmentData"

    invoke-virtual {v2, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v2

    .line 398634
    iget-object v4, p0, LX/2NA;->a:LX/2NB;

    invoke-virtual {v4, v2}, LX/2NB;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 398635
    new-instance v2, Lcom/facebook/messaging/model/messages/MessageDraft;

    invoke-direct {v2, v3, v0, v4, v1}, Lcom/facebook/messaging/model/messages/MessageDraft;-><init>(Ljava/lang/String;ILjava/util/List;Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_0

    .line 398636
    :cond_3
    new-instance v2, Lcom/facebook/messaging/model/messages/MessageDraft;

    invoke-direct {v2, v3, v0, v1}, Lcom/facebook/messaging/model/messages/MessageDraft;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/MessageDraft;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/facebook/messaging/model/messages/MessageDraft;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 398601
    if-nez p1, :cond_0

    .line 398602
    const/4 v0, 0x0

    .line 398603
    :goto_0
    return-object v0

    .line 398604
    :cond_0
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 398605
    const-string v1, "text"

    .line 398606
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/MessageDraft;->a:Ljava/lang/String;

    move-object v2, v2

    .line 398607
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398608
    const-string v1, "cursorPosition"

    .line 398609
    iget v2, p1, Lcom/facebook/messaging/model/messages/MessageDraft;->b:I

    move v2, v2

    .line 398610
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 398611
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/MessageDraft;->c:Ljava/util/List;

    move-object v1, v1

    .line 398612
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 398613
    iget-object v1, p0, LX/2NA;->a:LX/2NB;

    .line 398614
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/MessageDraft;->c:Ljava/util/List;

    move-object v2, v2

    .line 398615
    invoke-virtual {v1, v2}, LX/2NB;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 398616
    const-string v2, "attachmentData"

    invoke-virtual {v0, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398617
    :cond_1
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/MessageDraft;->d:Ljava/lang/String;

    move-object v1, v1

    .line 398618
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 398619
    const-string v1, "offlineMessageId"

    .line 398620
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/MessageDraft;->d:Ljava/lang/String;

    move-object v2, v2

    .line 398621
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398622
    :cond_2
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
