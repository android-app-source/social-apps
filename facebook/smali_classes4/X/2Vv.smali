.class public LX/2Vv;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 418240
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "orca_accounts/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 418241
    sput-object v0, LX/2Vv;->a:LX/0Tn;

    const-string v1, "override_gating"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Vv;->b:LX/0Tn;

    .line 418242
    sget-object v0, LX/2Vv;->a:LX/0Tn;

    const-string v1, "override_unseen_gating"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Vv;->c:LX/0Tn;

    .line 418243
    sget-object v0, LX/2Vv;->a:LX/0Tn;

    const-string v1, "accountswich_visited"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Vv;->d:LX/0Tn;

    .line 418244
    sget-object v0, LX/2Vv;->a:LX/0Tn;

    const-string v1, "saved_account/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Vv;->e:LX/0Tn;

    .line 418245
    sget-object v0, LX/2Vv;->a:LX/0Tn;

    const-string v1, "unseen_for_tab"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Vv;->f:LX/0Tn;

    .line 418246
    sget-object v0, LX/2Vv;->a:LX/0Tn;

    const-string v1, "unseen_threads/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Vv;->g:LX/0Tn;

    .line 418247
    sget-object v0, LX/2Vv;->a:LX/0Tn;

    const-string v1, "short_nux_needed"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Vv;->h:LX/0Tn;

    .line 418248
    sget-object v0, LX/2Vv;->a:LX/0Tn;

    const-string v1, "get_dbl_nonce"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Vv;->i:LX/0Tn;

    .line 418249
    sget-object v0, LX/2Vv;->a:LX/0Tn;

    const-string v1, "entering_source"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Vv;->j:LX/0Tn;

    .line 418250
    sget-object v0, LX/2Vv;->a:LX/0Tn;

    const-string v1, "unseen_last_fetch_time_attempt_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Vv;->k:LX/0Tn;

    .line 418251
    sget-object v0, LX/2Vv;->a:LX/0Tn;

    const-string v1, "unseen_last_fetch_time_success_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Vv;->l:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 418252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 418253
    sget-object v0, LX/2Vv;->e:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 418254
    sget-object v0, LX/2Vv;->g:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
