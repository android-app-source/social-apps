.class public LX/3Ne;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/widget/listview/BetterListView;

.field public c:LX/3O4;

.field public d:LX/3Nv;

.field public e:Lcom/facebook/widget/listview/EmptyListViewItem;

.field private f:LX/3LH;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 559682
    const-class v0, LX/3Ne;

    sput-object v0, LX/3Ne;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 559673
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 559674
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 559675
    const v0, 0x7f0d2057

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    .line 559676
    const v0, 0x7f0d2058

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, LX/3Ne;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 559677
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setDividerHeight(I)V

    .line 559678
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setBroadcastInteractionChanges(Z)V

    .line 559679
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    new-instance p1, LX/3Nr;

    invoke-direct {p1, p0}, LX/3Nr;-><init>(LX/3Ne;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 559680
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    new-instance p1, LX/3Ns;

    invoke-direct {p1, p0}, LX/3Ns;-><init>(LX/3Ne;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 559681
    return-void
.end method

.method public static b$redex0(LX/3Ne;I)V
    .locals 2

    .prologue
    .line 559669
    iget-object v0, p0, LX/3Ne;->f:LX/3LH;

    invoke-virtual {v0, p1}, LX/3LH;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OQ;

    .line 559670
    iget-object v1, p0, LX/3Ne;->c:LX/3O4;

    if-eqz v1, :cond_0

    .line 559671
    iget-object v1, p0, LX/3Ne;->c:LX/3O4;

    invoke-interface {v1, v0, p1}, LX/3O4;->a(LX/3OQ;I)V

    .line 559672
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 559667
    sget-object v0, LX/3Nx;->LOADING:LX/3Nx;

    invoke-virtual {p0, v0}, LX/3Ne;->a(LX/3Nx;)V

    .line 559668
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3OQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 559662
    iget-object v0, p0, LX/3Ne;->f:LX/3LH;

    invoke-virtual {v0, p1}, LX/3LH;->a(LX/0Px;)V

    .line 559663
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559664
    sget-object v0, LX/3Nx;->NO_RESULTS:LX/3Nx;

    invoke-virtual {p0, v0}, LX/3Ne;->a(LX/3Nx;)V

    .line 559665
    :goto_0
    return-void

    .line 559666
    :cond_0
    invoke-virtual {p0}, LX/3Ne;->b()V

    goto :goto_0
.end method

.method public final a(LX/3Nx;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 559633
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 559634
    sget-object v0, LX/3Ny;->a:[I

    invoke-virtual {p1}, LX/3Nx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 559635
    :goto_0
    iget-object v0, p0, LX/3Ne;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 559636
    return-void

    .line 559637
    :pswitch_0
    iget-object v0, p0, LX/3Ne;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    const v1, 0x7f0801ad

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(I)V

    .line 559638
    iget-object v0, p0, LX/3Ne;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    goto :goto_0

    .line 559639
    :pswitch_1
    iget-object v0, p0, LX/3Ne;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    const v1, 0x7f0801ac

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(I)V

    .line 559640
    iget-object v0, p0, LX/3Ne;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 559658
    iget-object v0, p0, LX/3Ne;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 559659
    iget-object v0, p0, LX/3Ne;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 559660
    iget-object v0, p0, LX/3Ne;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    const v1, 0x7f0801ac

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(I)V

    .line 559661
    return-void
.end method

.method public getAdapter()LX/3LH;
    .locals 1

    .prologue
    .line 559657
    iget-object v0, p0, LX/3Ne;->f:LX/3LH;

    return-object v0
.end method

.method public getListChildCount()I
    .locals 1

    .prologue
    .line 559654
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_0

    .line 559655
    const/4 v0, 0x0

    .line 559656
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v0

    goto :goto_0
.end method

.method public setAdapter(LX/3LH;)V
    .locals 1

    .prologue
    .line 559651
    iput-object p1, p0, LX/3Ne;->f:LX/3LH;

    .line 559652
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 559653
    return-void
.end method

.method public setFastScrollEnabled(Z)V
    .locals 1

    .prologue
    .line 559648
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setFastScrollEnabled(Z)V

    .line 559649
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setFastScrollAlwaysVisible(Z)V

    .line 559650
    return-void
.end method

.method public setListOnDrawListener(LX/0fu;)V
    .locals 1

    .prologue
    .line 559645
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_0

    .line 559646
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnDrawListenerTo(LX/0fu;)V

    .line 559647
    :cond_0
    return-void
.end method

.method public setOnRowClickedListener(LX/3O4;)V
    .locals 0

    .prologue
    .line 559643
    iput-object p1, p0, LX/3Ne;->c:LX/3O4;

    .line 559644
    return-void
.end method

.method public setStickyHeaderEnabled(Z)V
    .locals 1

    .prologue
    .line 559641
    iget-object v0, p0, LX/3Ne;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 559642
    return-void
.end method
