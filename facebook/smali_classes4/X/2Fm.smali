.class public final LX/2Fm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/2Fp;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/2Fp;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 387289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387290
    iput-object p1, p0, LX/2Fm;->a:LX/0QB;

    .line 387291
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 387292
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2Fm;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 387293
    packed-switch p2, :pswitch_data_0

    .line 387294
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387295
    :pswitch_0
    invoke-static {p1}, LX/2Fo;->b(LX/0QB;)LX/2Fo;

    move-result-object v0

    .line 387296
    :goto_0
    return-object v0

    .line 387297
    :pswitch_1
    new-instance p0, LX/2Ft;

    invoke-direct {p0}, LX/2Ft;-><init>()V

    .line 387298
    invoke-static {p1}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v0

    check-cast v0, LX/0Xl;

    invoke-static {p1}, LX/2Fu;->b(LX/0QB;)LX/2Fu;

    move-result-object v1

    check-cast v1, LX/2Fu;

    .line 387299
    iput-object v0, p0, LX/2Ft;->b:LX/0Xl;

    iput-object v1, p0, LX/2Ft;->d:LX/2Fu;

    .line 387300
    move-object v0, p0

    .line 387301
    goto :goto_0

    .line 387302
    :pswitch_2
    new-instance v1, LX/2zg;

    invoke-direct {v1}, LX/2zg;-><init>()V

    .line 387303
    const-class v0, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p1, v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 p0, 0x19

    invoke-static {p1, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 387304
    iput-object v0, v1, LX/2zg;->b:Landroid/content/Context;

    iput-object p0, v1, LX/2zg;->c:LX/0Or;

    .line 387305
    move-object v0, v1

    .line 387306
    goto :goto_0

    .line 387307
    :pswitch_3
    new-instance v1, LX/2Fx;

    invoke-direct {v1}, LX/2Fx;-><init>()V

    .line 387308
    const-class v0, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p1, v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 p0, 0x19

    invoke-static {p1, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 387309
    iput-object v0, v1, LX/2Fx;->b:Landroid/content/Context;

    iput-object p0, v1, LX/2Fx;->c:LX/0Or;

    .line 387310
    move-object v0, v1

    .line 387311
    goto :goto_0

    .line 387312
    :pswitch_4
    invoke-static {p1}, LX/2Hg;->b(LX/0QB;)LX/2Hg;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 387313
    const/4 v0, 0x5

    return v0
.end method
