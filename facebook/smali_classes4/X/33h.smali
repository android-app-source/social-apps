.class public LX/33h;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0lC;

.field private final b:LX/0lp;


# direct methods
.method public constructor <init>(LX/0lC;LX/0lp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494093
    iput-object p1, p0, LX/33h;->a:LX/0lC;

    .line 494094
    iput-object p2, p0, LX/33h;->b:LX/0lp;

    .line 494095
    return-void
.end method

.method public static b(LX/0QB;)LX/33h;
    .locals 3

    .prologue
    .line 494096
    new-instance v2, LX/33h;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v1

    check-cast v1, LX/0lp;

    invoke-direct {v2, v0, v1}, LX/33h;-><init>(LX/0lC;LX/0lp;)V

    .line 494097
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/zero/sdk/request/ZeroIndicatorData;
    .locals 3

    .prologue
    .line 494098
    const/4 v1, 0x0

    .line 494099
    :try_start_0
    iget-object v0, p0, LX/33h;->b:LX/0lp;

    invoke-virtual {v0, p1}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v1

    .line 494100
    invoke-virtual {v1}, LX/15w;->c()LX/15z;

    .line 494101
    iget-object v0, p0, LX/33h;->a:LX/0lC;

    const-class v2, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;

    invoke-virtual {v0, v1, v2}, LX/0lD;->a(LX/15w;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 494102
    invoke-static {v1}, LX/1pX;->a(Ljava/io/Closeable;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1pX;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public final a(Lcom/facebook/zero/sdk/request/ZeroIndicatorData;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 494103
    if-nez p1, :cond_0

    .line 494104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 494105
    :cond_0
    iget-object v0, p0, LX/33h;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
