.class public LX/2SS;
.super LX/2SP;
.source ""

# interfaces
.implements LX/2ST;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile n:LX/2SS;


# instance fields
.field private final a:LX/0Uh;

.field private final b:Landroid/content/Context;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fap;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fb7;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13B;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sf;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cwt;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z

.field private m:LX/2SR;


# direct methods
.method public constructor <init>(LX/0Uh;Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 412366
    invoke-direct {p0}, LX/2SP;-><init>()V

    .line 412367
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 412368
    iput-object v0, p0, LX/2SS;->c:LX/0Ot;

    .line 412369
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 412370
    iput-object v0, p0, LX/2SS;->d:LX/0Ot;

    .line 412371
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 412372
    iput-object v0, p0, LX/2SS;->e:LX/0Ot;

    .line 412373
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 412374
    iput-object v0, p0, LX/2SS;->f:LX/0Ot;

    .line 412375
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 412376
    iput-object v0, p0, LX/2SS;->g:LX/0Ot;

    .line 412377
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 412378
    iput-object v0, p0, LX/2SS;->h:LX/0Ot;

    .line 412379
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 412380
    iput-object v0, p0, LX/2SS;->i:LX/0Ot;

    .line 412381
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 412382
    iput-object v0, p0, LX/2SS;->j:LX/0Ot;

    .line 412383
    iput-object p1, p0, LX/2SS;->a:LX/0Uh;

    .line 412384
    iput-object p2, p0, LX/2SS;->b:Landroid/content/Context;

    .line 412385
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2SS;->l:Z

    .line 412386
    return-void
.end method

.method public static a(LX/0QB;)LX/2SS;
    .locals 11

    .prologue
    .line 412351
    sget-object v0, LX/2SS;->n:LX/2SS;

    if-nez v0, :cond_1

    .line 412352
    const-class v1, LX/2SS;

    monitor-enter v1

    .line 412353
    :try_start_0
    sget-object v0, LX/2SS;->n:LX/2SS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 412354
    if-eqz v2, :cond_0

    .line 412355
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 412356
    new-instance v3, LX/2SS;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {v3, v4, v5}, LX/2SS;-><init>(LX/0Uh;Landroid/content/Context;)V

    .line 412357
    const/16 v4, 0x32f8

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x32fc

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1141

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1143

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x32e4

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x113f

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2e3

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 p0, 0xf9a

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 412358
    iput-object v4, v3, LX/2SS;->c:LX/0Ot;

    iput-object v5, v3, LX/2SS;->d:LX/0Ot;

    iput-object v6, v3, LX/2SS;->e:LX/0Ot;

    iput-object v7, v3, LX/2SS;->f:LX/0Ot;

    iput-object v8, v3, LX/2SS;->g:LX/0Ot;

    iput-object v9, v3, LX/2SS;->h:LX/0Ot;

    iput-object v10, v3, LX/2SS;->i:LX/0Ot;

    iput-object p0, v3, LX/2SS;->j:LX/0Ot;

    .line 412359
    move-object v0, v3

    .line 412360
    sput-object v0, LX/2SS;->n:LX/2SS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412361
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 412362
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 412363
    :cond_1
    sget-object v0, LX/2SS;->n:LX/2SS;

    return-object v0

    .line 412364
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 412365
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/7BE;
    .locals 1

    .prologue
    .line 412350
    iget-object v0, p0, LX/2SS;->k:LX/0Px;

    if-nez v0, :cond_0

    sget-object v0, LX/7BE;->NOT_READY:LX/7BE;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/7BE;->READY:LX/7BE;

    goto :goto_0
.end method

.method public final a(LX/2SR;LX/2Sp;)V
    .locals 0

    .prologue
    .line 412348
    iput-object p1, p0, LX/2SS;->m:LX/2SR;

    .line 412349
    return-void
.end method

.method public final a(LX/7C4;)V
    .locals 1

    .prologue
    .line 412280
    iget-object v0, p0, LX/2SS;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sc;

    invoke-virtual {v0, p1}, LX/2Sc;->a(LX/7C4;)V

    .line 412281
    return-void
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 9
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 412338
    iget-object v0, p0, LX/2SS;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/7CP;->h:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    iget-object v0, p0, LX/2SS;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_2

    .line 412339
    iget-object v0, p0, LX/2SS;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fap;

    .line 412340
    iput-object p0, v0, LX/Fap;->d:LX/2ST;

    .line 412341
    iget-object v0, p0, LX/2SS;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fap;

    .line 412342
    iget-object v4, v0, LX/Fap;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v4, :cond_0

    iget-object v4, v0, LX/Fap;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v4}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 412343
    :cond_0
    invoke-static {}, LX/A0Y;->b()LX/A0W;

    move-result-object v5

    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object v6, LX/0zS;->c:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    const-wide/16 v7, 0x0

    invoke-virtual {v5, v7, v8}, LX/0zO;->a(J)LX/0zO;

    move-result-object v5

    move-object v4, v5

    .line 412344
    invoke-static {v0, v4}, LX/Fap;->a(LX/Fap;LX/0zO;)V

    .line 412345
    :cond_1
    :goto_0
    return-void

    .line 412346
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 412347
    iput-object v0, p0, LX/2SS;->k:LX/0Px;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;)V
    .locals 14

    .prologue
    .line 412289
    iget-object v0, p0, LX/2SS;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/7CP;->h:LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2SS;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/32 v4, 0x5265c00

    add-long/2addr v0, v4

    :goto_0
    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 412290
    iget-object v0, p0, LX/2SS;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fb7;

    invoke-virtual {v0, p1}, LX/Fb7;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 412291
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 412292
    iput-object v0, p0, LX/2SS;->k:LX/0Px;

    .line 412293
    iget-object v0, p0, LX/2SS;->m:LX/2SR;

    if-eqz v0, :cond_0

    .line 412294
    iget-object v0, p0, LX/2SS;->m:LX/2SR;

    sget-object v1, LX/7BE;->READY:LX/7BE;

    invoke-interface {v0, v1}, LX/2SR;->a(LX/7BE;)V

    .line 412295
    :cond_0
    :goto_1
    return-void

    .line 412296
    :cond_1
    iget-object v0, p0, LX/2SS;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->j()I

    move-result v4

    int-to-long v4, v4

    const-wide/32 v6, 0x36ee80

    mul-long/2addr v4, v6

    add-long/2addr v0, v4

    goto :goto_0

    .line 412297
    :cond_2
    iget-object v0, p0, LX/2SS;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwt;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v1

    const/16 p1, 0x8

    const/4 v13, 0x0

    .line 412298
    if-nez v1, :cond_5

    .line 412299
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 412300
    :goto_2
    move-object v0, v2

    .line 412301
    if-eqz v0, :cond_3

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 412302
    :cond_3
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 412303
    iput-object v0, p0, LX/2SS;->k:LX/0Px;

    .line 412304
    :goto_3
    iget-object v0, p0, LX/2SS;->m:LX/2SR;

    if-eqz v0, :cond_0

    .line 412305
    iget-object v0, p0, LX/2SS;->m:LX/2SR;

    sget-object v1, LX/7BE;->READY:LX/7BE;

    invoke-interface {v0, v1}, LX/2SR;->a(LX/7BE;)V

    goto :goto_1

    .line 412306
    :cond_4
    iget-object v1, p0, LX/2SS;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    new-instance v1, LX/Cwa;

    invoke-direct {v1}, LX/Cwa;-><init>()V

    sget-object v2, LX/Cwb;->NS_SEARCH_SPOTLIGHT:LX/Cwb;

    .line 412307
    iput-object v2, v1, LX/Cwa;->a:LX/Cwb;

    .line 412308
    move-object v1, v1

    .line 412309
    iget-object v2, p0, LX/2SS;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0822e2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 412310
    iput-object v2, v1, LX/Cwa;->c:Ljava/lang/String;

    .line 412311
    move-object v1, v1

    .line 412312
    new-instance v2, Lcom/facebook/search/model/SearchSpotlightCollectionUnit;

    invoke-direct {v2, v0}, Lcom/facebook/search/model/SearchSpotlightCollectionUnit;-><init>(LX/0Px;)V

    new-instance v0, Lcom/facebook/search/model/GapTypeaheadUnit;

    sget-object v3, LX/Cw9;->GRAY:LX/Cw9;

    invoke-direct {v0, v3}, Lcom/facebook/search/model/GapTypeaheadUnit;-><init>(LX/Cw9;)V

    invoke-static {v2, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 412313
    iput-object v0, v1, LX/Cwa;->b:LX/0Px;

    .line 412314
    move-object v0, v1

    .line 412315
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2Sf;->b(Ljava/util/List;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/2SS;->k:LX/0Px;

    .line 412316
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2SS;->l:Z

    goto :goto_3

    .line 412317
    :cond_5
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 412318
    invoke-virtual {v1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->j()LX/2uF;

    move-result-object v2

    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v5

    :cond_6
    :goto_4
    invoke-interface {v5}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v5}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v6, v2, LX/1vs;->a:LX/15i;

    iget v7, v2, LX/1vs;->b:I

    .line 412319
    const/4 v3, 0x0

    .line 412320
    sget-object v8, LX/Cws;->a:[I

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    invoke-virtual {v6, v7, p1, v2, v9}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->ordinal()I

    move-result v2

    aget v2, v8, v2

    packed-switch v2, :pswitch_data_0

    .line 412321
    iget-object v2, v0, LX/Cwt;->a:LX/2Sc;

    new-instance v8, LX/7C4;

    sget-object v9, LX/3Ql;->BAD_SEARCH_SPOTLIGHT_SUGGESTION:LX/3Ql;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Unsupported Search Spotlight template: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v11, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    invoke-virtual {v6, v7, p1, v11, v12}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v6

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v8, v9, v6}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    invoke-virtual {v2, v8}, LX/2Sc;->a(LX/7C4;)V

    move-object v2, v3

    .line 412322
    :goto_5
    if-eqz v2, :cond_6

    .line 412323
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 412324
    :pswitch_0
    new-instance v2, LX/CwX;

    invoke-direct {v2}, LX/CwX;-><init>()V

    invoke-virtual {v6, v7, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 412325
    iput-object v3, v2, LX/CwX;->a:Ljava/lang/String;

    .line 412326
    move-object v2, v2

    .line 412327
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "#"

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    .line 412328
    iput v3, v2, LX/CwX;->b:I

    .line 412329
    move-object v2, v2

    .line 412330
    new-instance v3, Lcom/facebook/search/model/SearchSpotlightCardUnit;

    invoke-direct {v3, v2}, Lcom/facebook/search/model/SearchSpotlightCardUnit;-><init>(LX/CwX;)V

    move-object v2, v3

    .line 412331
    goto :goto_5

    .line 412332
    :pswitch_1
    new-instance v2, LX/CwY;

    invoke-direct {v2}, LX/CwY;-><init>()V

    invoke-virtual {v6, v7, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 412333
    iput-object v3, v2, LX/CwY;->a:Ljava/lang/String;

    .line 412334
    move-object v2, v2

    .line 412335
    new-instance v3, Lcom/facebook/search/model/SearchSpotlightIntroUnit;

    invoke-direct {v3, v2}, Lcom/facebook/search/model/SearchSpotlightIntroUnit;-><init>(LX/CwY;)V

    move-object v2, v3

    .line 412336
    goto :goto_5

    .line 412337
    :cond_7
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 412288
    iget-object v0, p0, LX/2SS;->a:LX/0Uh;

    sget v1, LX/2SU;->E:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 412286
    const/4 v0, 0x0

    iput-object v0, p0, LX/2SS;->k:LX/0Px;

    .line 412287
    return-void
.end method

.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 412282
    iget-boolean v0, p0, LX/2SS;->l:Z

    if-eqz v0, :cond_0

    .line 412283
    iget-object v0, p0, LX/2SS;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13B;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/13B;->b(I)V

    .line 412284
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2SS;->l:Z

    .line 412285
    :cond_0
    iget-object v0, p0, LX/2SS;->k:LX/0Px;

    return-object v0
.end method
