.class public LX/3A3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile n:LX/3A3;


# instance fields
.field private final e:LX/0TD;

.field private final f:LX/11H;

.field private final g:LX/3A4;

.field private final h:LX/0Sh;

.field private final i:LX/2Hu;

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/2Hv;

.field private final l:LX/3A6;

.field private final m:LX/3A5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 524673
    const-class v0, LX/3A3;

    sput-object v0, LX/3A3;->a:Ljava/lang/Class;

    .line 524674
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/3A3;->b:Ljava/util/Map;

    .line 524675
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/3A3;->c:Ljava/util/List;

    .line 524676
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, LX/3A3;->d:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/0TD;LX/11H;LX/3A4;LX/0Sh;LX/2Hu;LX/0Or;LX/2Hv;LX/3A6;LX/3A5;)V
    .locals 0
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/rtcpresence/annotations/IsRtcPresenceOverMqtt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/3A4;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClientManager;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Hv;",
            "LX/3A6;",
            "LX/3A5;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 524677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524678
    iput-object p1, p0, LX/3A3;->e:LX/0TD;

    .line 524679
    iput-object p2, p0, LX/3A3;->f:LX/11H;

    .line 524680
    iput-object p3, p0, LX/3A3;->g:LX/3A4;

    .line 524681
    iput-object p4, p0, LX/3A3;->h:LX/0Sh;

    .line 524682
    iput-object p5, p0, LX/3A3;->i:LX/2Hu;

    .line 524683
    iput-object p6, p0, LX/3A3;->j:LX/0Or;

    .line 524684
    iput-object p7, p0, LX/3A3;->k:LX/2Hv;

    .line 524685
    iput-object p8, p0, LX/3A3;->l:LX/3A6;

    .line 524686
    iput-object p9, p0, LX/3A3;->m:LX/3A5;

    .line 524687
    return-void
.end method

.method public static a(LX/0QB;)LX/3A3;
    .locals 13

    .prologue
    .line 524688
    sget-object v0, LX/3A3;->n:LX/3A3;

    if-nez v0, :cond_1

    .line 524689
    const-class v1, LX/3A3;

    monitor-enter v1

    .line 524690
    :try_start_0
    sget-object v0, LX/3A3;->n:LX/3A3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 524691
    if-eqz v2, :cond_0

    .line 524692
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 524693
    new-instance v3, LX/3A3;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v5

    check-cast v5, LX/11H;

    .line 524694
    new-instance v7, LX/3A4;

    invoke-static {v0}, LX/3A5;->b(LX/0QB;)LX/3A5;

    move-result-object v6

    check-cast v6, LX/3A5;

    invoke-direct {v7, v6}, LX/3A4;-><init>(LX/3A5;)V

    .line 524695
    move-object v6, v7

    .line 524696
    check-cast v6, LX/3A4;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v8

    check-cast v8, LX/2Hu;

    const/16 v9, 0x1565

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/2Hv;->a(LX/0QB;)LX/2Hv;

    move-result-object v10

    check-cast v10, LX/2Hv;

    invoke-static {v0}, LX/3A6;->a(LX/0QB;)LX/3A6;

    move-result-object v11

    check-cast v11, LX/3A6;

    invoke-static {v0}, LX/3A5;->b(LX/0QB;)LX/3A5;

    move-result-object v12

    check-cast v12, LX/3A5;

    invoke-direct/range {v3 .. v12}, LX/3A3;-><init>(LX/0TD;LX/11H;LX/3A4;LX/0Sh;LX/2Hu;LX/0Or;LX/2Hv;LX/3A6;LX/3A5;)V

    .line 524697
    move-object v0, v3

    .line 524698
    sput-object v0, LX/3A3;->n:LX/3A3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 524699
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 524700
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 524701
    :cond_1
    sget-object v0, LX/3A3;->n:LX/3A3;

    return-object v0

    .line 524702
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 524703
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
