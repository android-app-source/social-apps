.class public LX/2s9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2s9;


# instance fields
.field public final a:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/net/wifi/WifiManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 472410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472411
    iput-object p1, p0, LX/2s9;->a:Landroid/net/wifi/WifiManager;

    .line 472412
    return-void
.end method

.method public static a(LX/0QB;)LX/2s9;
    .locals 4

    .prologue
    .line 472397
    sget-object v0, LX/2s9;->b:LX/2s9;

    if-nez v0, :cond_1

    .line 472398
    const-class v1, LX/2s9;

    monitor-enter v1

    .line 472399
    :try_start_0
    sget-object v0, LX/2s9;->b:LX/2s9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 472400
    if-eqz v2, :cond_0

    .line 472401
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 472402
    new-instance p0, LX/2s9;

    invoke-static {v0}, LX/0kt;->b(LX/0QB;)Landroid/net/wifi/WifiManager;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    invoke-direct {p0, v3}, LX/2s9;-><init>(Landroid/net/wifi/WifiManager;)V

    .line 472403
    move-object v0, p0

    .line 472404
    sput-object v0, LX/2s9;->b:LX/2s9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 472405
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 472406
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 472407
    :cond_1
    sget-object v0, LX/2s9;->b:LX/2s9;

    return-object v0

    .line 472408
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 472409
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/2s9;)LX/0lF;
    .locals 4

    .prologue
    .line 472368
    iget-object v0, p0, LX/2s9;->a:Landroid/net/wifi/WifiManager;

    if-nez v0, :cond_1

    .line 472369
    const/4 v0, 0x0

    .line 472370
    :cond_0
    :goto_0
    return-object v0

    .line 472371
    :cond_1
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 472372
    iget-object v1, p0, LX/2s9;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    .line 472373
    iget-object v2, p0, LX/2s9;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v2

    .line 472374
    const-string v3, "enabled"

    invoke-virtual {v0, v3, v1}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 472375
    const-string v1, "state"

    .line 472376
    packed-switch v2, :pswitch_data_0

    .line 472377
    const-string v3, "unknown"

    :goto_1
    move-object v2, v3

    .line 472378
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 472379
    iget-object v1, p0, LX/2s9;->a:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 472380
    const/4 v2, 0x0

    .line 472381
    if-nez v1, :cond_3

    .line 472382
    :cond_2
    :goto_2
    move-object v1, v2

    .line 472383
    if-eqz v1, :cond_0

    .line 472384
    const-string v2, "current_connection"

    invoke-virtual {v0, v2, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0

    .line 472385
    :pswitch_0
    const-string v3, "disabled"

    goto :goto_1

    .line 472386
    :pswitch_1
    const-string v3, "disabling"

    goto :goto_1

    .line 472387
    :pswitch_2
    const-string v3, "enabled"

    goto :goto_1

    .line 472388
    :pswitch_3
    const-string v3, "enabling"

    goto :goto_1

    .line 472389
    :cond_3
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v3

    .line 472390
    if-eqz v3, :cond_2

    .line 472391
    new-instance v2, LX/0m9;

    sget-object p0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, p0}, LX/0m9;-><init>(LX/0mC;)V

    .line 472392
    const-string p0, "mac_address"

    invoke-virtual {v2, p0, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 472393
    const-string v3, "is_hidden_ssid"

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getHiddenSSID()Z

    move-result p0

    invoke-virtual {v2, v3, p0}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 472394
    const-string v3, "link_speed_mbps"

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result p0

    invoke-virtual {v2, v3, p0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 472395
    const-string v3, "signal_strength"

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result p0

    invoke-virtual {v2, v3, p0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 472396
    const-string v3, "ssid"

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
