.class public final LX/34m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1S9;


# direct methods
.method public constructor <init>(LX/1S9;)V
    .locals 0

    .prologue
    .line 495656
    iput-object p1, p0, LX/34m;->a:LX/1S9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 495657
    iget-object v0, p0, LX/34m;->a:LX/1S9;

    iget-object v0, v0, LX/1S9;->d:LX/0if;

    sget-object v1, LX/0ig;->av:LX/0ih;

    const-string v2, "fetch_question_failed"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 495658
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 495659
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 495660
    if-eqz p1, :cond_0

    .line 495661
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 495662
    if-eqz v0, :cond_0

    .line 495663
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 495664
    check-cast v0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;->a()Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 495665
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 495666
    check-cast v0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;->a()Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 495667
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 495668
    check-cast v0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;->a()Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 495669
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 495670
    check-cast v0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;->a()Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 495671
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 495672
    check-cast v0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;->a()Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel$UnitModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 495673
    :cond_0
    iget-object v0, p0, LX/34m;->a:LX/1S9;

    iget-object v0, v0, LX/1S9;->d:LX/0if;

    sget-object v1, LX/0ig;->av:LX/0ih;

    const-string v2, "fetch_question_successful_no_result"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 495674
    :cond_1
    return-void

    .line 495675
    :cond_2
    iget-object v0, p0, LX/34m;->a:LX/1S9;

    iget-object v0, v0, LX/1S9;->d:LX/0if;

    sget-object v1, LX/0ig;->av:LX/0ih;

    const-string v2, "fetch_question_successful"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 495676
    iget-object v1, p0, LX/34m;->a:LX/1S9;

    new-instance v2, LX/2tX;

    iget-object v3, p0, LX/34m;->a:LX/1S9;

    .line 495677
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 495678
    check-cast v0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;

    sget-object v4, LX/328;->UNANSWERED:LX/328;

    invoke-direct {v2, v3, v0, v4}, LX/2tX;-><init>(LX/1S9;Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuQueryModels$CrowdsourcingTofuQueryModel;LX/328;)V

    .line 495679
    iput-object v2, v1, LX/1S9;->k:LX/2tX;

    .line 495680
    iget-object v0, p0, LX/34m;->a:LX/1S9;

    iget-object v0, v0, LX/1S9;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1SA;

    .line 495681
    invoke-virtual {v0}, LX/1SA;->a()V

    goto :goto_0
.end method
