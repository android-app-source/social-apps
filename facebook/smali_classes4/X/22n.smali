.class public final LX/22n;
.super LX/0PO;
.source ""


# instance fields
.field public final synthetic this$0:LX/0PO;


# direct methods
.method public constructor <init>(LX/0PO;LX/0PO;)V
    .locals 1

    .prologue
    .line 361264
    iput-object p1, p0, LX/22n;->this$0:LX/0PO;

    invoke-direct {p0, p2}, LX/0PO;-><init>(LX/0PO;)V

    return-void
.end method


# virtual methods
.method public appendTo(Ljava/lang/Appendable;Ljava/util/Iterator;)Ljava/lang/Appendable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Ljava/lang/Appendable;",
            ">(TA;",
            "Ljava/util/Iterator",
            "<*>;)TA;"
        }
    .end annotation

    .prologue
    .line 361265
    const-string v0, "appendable"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361266
    const-string v0, "parts"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361267
    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 361268
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 361269
    if-eqz v0, :cond_0

    .line 361270
    iget-object v1, p0, LX/22n;->this$0:LX/0PO;

    invoke-virtual {v1, v0}, LX/0PO;->toString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 361271
    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 361272
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 361273
    if-eqz v0, :cond_1

    .line 361274
    iget-object v1, p0, LX/22n;->this$0:LX/0PO;

    iget-object v1, v1, LX/0PO;->separator:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 361275
    iget-object v1, p0, LX/22n;->this$0:LX/0PO;

    invoke-virtual {v1, v0}, LX/0PO;->toString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    goto :goto_0

    .line 361276
    :cond_2
    return-object p1
.end method

.method public useForNull(Ljava/lang/String;)LX/0PO;
    .locals 2

    .prologue
    .line 361277
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "already specified skipNulls"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public withKeyValueSeparator(Ljava/lang/String;)LX/0PQ;
    .locals 2

    .prologue
    .line 361278
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t use .skipNulls() with maps"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
