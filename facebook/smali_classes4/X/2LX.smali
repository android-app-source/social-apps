.class public LX/2LX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2LP;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/03V;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/03V;Ljava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/launcherbadges/AppLaunchClass;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395194
    iput-object p1, p0, LX/2LX;->a:Landroid/content/Context;

    .line 395195
    iput-object p2, p0, LX/2LX;->b:LX/03V;

    .line 395196
    iput-object p3, p0, LX/2LX;->d:Ljava/lang/String;

    .line 395197
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2LX;->c:Ljava/lang/String;

    .line 395198
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2LX;->e:Z

    .line 395199
    return-void
.end method


# virtual methods
.method public final a(I)LX/03R;
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 395177
    iget-boolean v0, p0, LX/2LX;->e:Z

    if-ne v0, v1, :cond_0

    .line 395178
    sget-object v0, LX/03R;->NO:LX/03R;

    .line 395179
    :goto_0
    return-object v0

    .line 395180
    :cond_0
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.sonyericsson.home.action.UPDATE_BADGE"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 395181
    const/16 v0, 0x10

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 395182
    const-string v0, "com.sonyericsson.home.intent.extra.badge.ACTIVITY_NAME"

    iget-object v3, p0, LX/2LX;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 395183
    const-string v3, "com.sonyericsson.home.intent.extra.badge.SHOW_MESSAGE"

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 395184
    const-string v0, "com.sonyericsson.home.intent.extra.badge.MESSAGE"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 395185
    const-string v0, "com.sonyericsson.home.intent.extra.badge.PACKAGE_NAME"

    iget-object v3, p0, LX/2LX;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 395186
    iget-object v0, p0, LX/2LX;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 395187
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0

    .line 395188
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 395189
    :catch_0
    move-exception v0

    .line 395190
    iget-object v2, p0, LX/2LX;->b:LX/03V;

    const-class v3, LX/2LX;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "unexpected exception"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 395191
    iput-boolean v1, p0, LX/2LX;->e:Z

    .line 395192
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method
