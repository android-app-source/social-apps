.class public final LX/2K0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/2K2;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/2K2;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 393338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393339
    iput-object p1, p0, LX/2K0;->a:LX/0QB;

    .line 393340
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 393341
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2K0;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 393342
    packed-switch p2, :pswitch_data_0

    .line 393343
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 393344
    :pswitch_0
    new-instance p0, LX/2K1;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, Lcom/facebook/gk/store/GatekeeperWriter;

    invoke-direct {p0, v0, v1}, LX/2K1;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/gk/store/GatekeeperWriter;)V

    .line 393345
    move-object v0, p0

    .line 393346
    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 393347
    const/4 v0, 0x1

    return v0
.end method
