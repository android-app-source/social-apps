.class public final LX/2oY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 467139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 467140
    check-cast p1, Landroid/view/View;

    check-cast p2, Landroid/view/View;

    .line 467141
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/2p4;

    .line 467142
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/2p4;

    .line 467143
    iget v2, v0, LX/2p4;->b:I

    iget v3, v1, LX/2p4;->b:I

    sub-int/2addr v2, v3

    .line 467144
    if-eqz v2, :cond_0

    move v0, v2

    .line 467145
    :goto_0
    return v0

    .line 467146
    :cond_0
    iget-object v2, v0, LX/2p4;->a:LX/2p5;

    iget-object v3, v1, LX/2p4;->a:LX/2p5;

    invoke-virtual {v2, v3}, LX/2p5;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    .line 467147
    if-eqz v2, :cond_1

    move v0, v2

    .line 467148
    goto :goto_0

    .line 467149
    :cond_1
    iget-object v2, v0, LX/2p4;->c:LX/2p6;

    iget-object v3, v1, LX/2p4;->c:LX/2p6;

    invoke-virtual {v2, v3}, LX/2p6;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    .line 467150
    if-eqz v2, :cond_2

    move v0, v2

    .line 467151
    goto :goto_0

    .line 467152
    :cond_2
    iget v0, v0, LX/2p4;->d:I

    iget v1, v1, LX/2p4;->d:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method
