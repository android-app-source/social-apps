.class public final LX/373;
.super LX/36y;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/36y",
        "<",
        "Lcom/facebook/video/server/prefetcher/PrefetchEvents$RetrieveMetadataEndEvent$Handler;",
        ">;"
    }
.end annotation


# instance fields
.field public final b:J

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2WF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LX/2WF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 500591
    invoke-direct {p0, p1}, LX/36y;-><init>(I)V

    .line 500592
    int-to-long v0, p2

    iput-wide v0, p0, LX/373;->b:J

    .line 500593
    iput-object p3, p0, LX/373;->c:Ljava/util/List;

    .line 500594
    return-void
.end method


# virtual methods
.method public final a(LX/16Y;)V
    .locals 3

    .prologue
    .line 500595
    check-cast p1, LX/36w;

    .line 500596
    invoke-static {p1, p0}, LX/36w;->a(LX/36w;LX/36y;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500597
    iget-object v0, p1, LX/36w;->c:LX/11o;

    const-string v1, "RetrieveMetadata"

    const v2, 0x661d2ba5

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 500598
    :cond_0
    return-void
.end method
