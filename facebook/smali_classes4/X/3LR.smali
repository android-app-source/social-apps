.class public LX/3LR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/base/fragment/FbFragment;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 550531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/3LR;)LX/0gc;
    .locals 2

    .prologue
    .line 550532
    invoke-static {p0}, LX/3LR;->c(LX/3LR;)Landroid/app/Activity;

    move-result-object v0

    .line 550533
    instance-of v1, v0, LX/0fD;

    if-eqz v1, :cond_0

    .line 550534
    check-cast v0, LX/0fD;

    invoke-interface {v0}, LX/0fD;->e()LX/0fK;

    move-result-object v0

    .line 550535
    instance-of v1, v0, LX/0fL;

    if-eqz v1, :cond_0

    .line 550536
    check-cast v0, LX/0fL;

    .line 550537
    iget-object v1, v0, LX/0fL;->j:LX/0gc;

    move-object v0, v1

    .line 550538
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3LR;->a:Landroid/support/v4/app/Fragment;

    .line 550539
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v1

    .line 550540
    goto :goto_0
.end method

.method public static c(LX/3LR;)Landroid/app/Activity;
    .locals 2

    .prologue
    .line 550541
    iget-object v0, p0, LX/3LR;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 550542
    instance-of v1, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v1, :cond_0

    .line 550543
    check-cast v0, Landroid/app/Activity;

    .line 550544
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/3LR;)Z
    .locals 2

    .prologue
    .line 550545
    iget-object v0, p0, LX/3LR;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 550546
    if-nez v0, :cond_0

    .line 550547
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Divebar is not hosted anywhere"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 550548
    :cond_0
    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/3LR;)Z
    .locals 1

    .prologue
    .line 550549
    invoke-static {p0}, LX/3LR;->a(LX/3LR;)LX/0gc;

    move-result-object v0

    .line 550550
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/base/fragment/FbFragment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 550551
    invoke-static {p0}, LX/3LR;->a(LX/3LR;)LX/0gc;

    move-result-object v0

    .line 550552
    if-nez v0, :cond_0

    .line 550553
    const/4 v0, 0x0

    .line 550554
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    goto :goto_0
.end method
