.class public final LX/3K4;
.super LX/3Jv;
.source ""


# direct methods
.method public constructor <init>(LX/3Jx;[F)V
    .locals 0

    .prologue
    .line 548336
    invoke-direct {p0, p1, p2}, LX/3Jv;-><init>(LX/3Jx;[F)V

    .line 548337
    return-void
.end method


# virtual methods
.method public final a(LX/9Ua;)V
    .locals 2

    .prologue
    .line 548338
    iget-object v0, p0, LX/3Jv;->a:LX/3Jx;

    iget-object v1, p0, LX/3Jv;->b:[F

    invoke-virtual {p0, p1, v0, v1}, LX/3K4;->a(LX/9Ua;LX/3Jx;[F)V

    .line 548339
    return-void
.end method

.method public final a(LX/9Ua;LX/3Jx;[F)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 548326
    sget-object v0, LX/3Jy;->b:[I

    invoke-virtual {p2}, LX/3Jx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 548327
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "No such argument format %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548328
    :pswitch_0
    aget v0, p3, v4

    aget v1, p3, v3

    aget v2, p3, v2

    aget v3, p3, v5

    .line 548329
    iget-object v4, p1, LX/9Ua;->a:Landroid/graphics/Path;

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Path;->rQuadTo(FFFF)V

    .line 548330
    invoke-static {p1, v2, v3}, LX/9Ua;->f(LX/9Ua;FF)V

    .line 548331
    :goto_0
    return-void

    .line 548332
    :pswitch_1
    aget v0, p3, v4

    aget v1, p3, v3

    aget v2, p3, v2

    aget v3, p3, v5

    .line 548333
    iget-object v4, p1, LX/9Ua;->a:Landroid/graphics/Path;

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 548334
    invoke-static {p1, v2, v3}, LX/9Ua;->e(LX/9Ua;FF)V

    .line 548335
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
