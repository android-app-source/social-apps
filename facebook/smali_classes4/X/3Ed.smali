.class public final LX/3Ed;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3Ed;


# instance fields
.field private a:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 537895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537896
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/3Ed;->a:Ljava/util/Random;

    .line 537897
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ed;
    .locals 3

    .prologue
    .line 537898
    sget-object v0, LX/3Ed;->b:LX/3Ed;

    if-nez v0, :cond_1

    .line 537899
    const-class v1, LX/3Ed;

    monitor-enter v1

    .line 537900
    :try_start_0
    sget-object v0, LX/3Ed;->b:LX/3Ed;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 537901
    if-eqz v2, :cond_0

    .line 537902
    :try_start_1
    new-instance v0, LX/3Ed;

    invoke-direct {v0}, LX/3Ed;-><init>()V

    .line 537903
    move-object v0, v0

    .line 537904
    sput-object v0, LX/3Ed;->b:LX/3Ed;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 537905
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 537906
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 537907
    :cond_1
    sget-object v0, LX/3Ed;->b:LX/3Ed;

    return-object v0

    .line 537908
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 537909
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 537910
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 537911
    iget-object v2, p0, LX/3Ed;->a:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    .line 537912
    const/16 v3, 0x16

    shl-long/2addr v0, v3

    const v3, 0x3fffff

    and-int/2addr v2, v3

    int-to-long v2, v2

    or-long/2addr v0, v2

    const-wide v2, 0x7fffffffffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method
