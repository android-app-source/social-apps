.class public LX/2nI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2mz;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0Uh;

.field private final c:LX/17Q;

.field public d:Landroid/view/View;

.field private e:I

.field private f:Landroid/content/Context;

.field private g:Landroid/content/Intent;

.field public h:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/D8V;

.field private j:I

.field private k:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0Uh;LX/17Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463780
    iput-object p1, p0, LX/2nI;->a:LX/0Zb;

    .line 463781
    iput-object p2, p0, LX/2nI;->b:LX/0Uh;

    .line 463782
    iput-object p3, p0, LX/2nI;->c:LX/17Q;

    .line 463783
    return-void
.end method

.method public static a(LX/0QB;)LX/2nI;
    .locals 6

    .prologue
    .line 463768
    const-class v1, LX/2nI;

    monitor-enter v1

    .line 463769
    :try_start_0
    sget-object v0, LX/2nI;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 463770
    sput-object v2, LX/2nI;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 463771
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463772
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 463773
    new-instance p0, LX/2nI;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    invoke-direct {p0, v3, v4, v5}, LX/2nI;-><init>(LX/0Zb;LX/0Uh;LX/17Q;)V

    .line 463774
    move-object v0, p0

    .line 463775
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 463776
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2nI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463777
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 463778
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private c(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 463750
    iget-object v0, p0, LX/2nI;->g:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 463751
    iget-object v0, p0, LX/2nI;->g:Landroid/content/Intent;

    .line 463752
    :goto_0
    return-object v0

    .line 463753
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_2

    .line 463754
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 463755
    :cond_2
    const/4 v1, 0x0

    .line 463756
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 463757
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 463758
    if-nez v2, :cond_3

    move-object v0, v1

    .line 463759
    :goto_1
    move-object v0, v0

    .line 463760
    iput-object v0, p0, LX/2nI;->g:Landroid/content/Intent;

    .line 463761
    iget-object v0, p0, LX/2nI;->g:Landroid/content/Intent;

    goto :goto_0

    .line 463762
    :cond_3
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 463763
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, LX/2yB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/47G;

    move-result-object v2

    .line 463764
    if-nez v2, :cond_4

    move-object v0, v1

    .line 463765
    goto :goto_1

    .line 463766
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 463767
    invoke-static {v2}, LX/6Q1;->a(LX/47G;)Lcom/facebook/directinstall/intent/DirectInstallAppData;

    move-result-object v1

    invoke-static {v0, v1}, LX/6Qv;->a(Landroid/content/Intent;Lcom/facebook/directinstall/intent/DirectInstallAppData;)V

    goto :goto_1
.end method

.method private f()V
    .locals 5

    .prologue
    .line 463682
    iget-object v0, p0, LX/2nI;->f:Landroid/content/Context;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 463683
    iget-object v0, p0, LX/2nI;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 463684
    iget-object v0, p0, LX/2nI;->i:LX/D8V;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 463685
    iget-object v0, p0, LX/2nI;->f:Landroid/content/Context;

    const-class v1, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 463686
    if-nez v0, :cond_1

    .line 463687
    :cond_0
    :goto_0
    return-void

    .line 463688
    :cond_1
    iget-object v1, p0, LX/2nI;->f:Landroid/content/Context;

    iget-object v2, p0, LX/2nI;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, v1, v2}, LX/2nI;->c(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/content/Intent;

    move-result-object v1

    .line 463689
    if-eqz v1, :cond_0

    .line 463690
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 463691
    const-string v3, "app_data"

    const-string v4, "app_data"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 463692
    const-string v3, "analytics"

    const-string v4, "analytics"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 463693
    const-string v1, "IS_WATCH_AND_DIRECT_INSTALL"

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 463694
    const-string v1, "WATCH_AND_DIRECT_INSTALL_DUMMY_VIDEO_VIEW_HEIGHT"

    iget v3, p0, LX/2nI;->j:I

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 463695
    new-instance v1, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-direct {v1}, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;-><init>()V

    iput-object v1, p0, LX/2nI;->k:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    .line 463696
    iget-object v1, p0, LX/2nI;->k:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 463697
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget v1, p0, LX/2nI;->e:I

    iget-object v2, p0, LX/2nI;->k:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 463744
    iget-object v0, p0, LX/2nI;->k:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    if-nez v0, :cond_1

    .line 463745
    :cond_0
    :goto_0
    return-void

    .line 463746
    :cond_1
    iget-object v0, p0, LX/2nI;->f:Landroid/content/Context;

    const-class v1, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 463747
    if-eqz v0, :cond_0

    .line 463748
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, LX/2nI;->k:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 463749
    const/4 v0, 0x0

    iput-object v0, p0, LX/2nI;->k:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 463743
    const/4 v0, 0x0

    return v0
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 463738
    invoke-direct {p0}, LX/2nI;->g()V

    .line 463739
    iput-object v0, p0, LX/2nI;->f:Landroid/content/Context;

    .line 463740
    iput-object v0, p0, LX/2nI;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 463741
    iput-object v0, p0, LX/2nI;->i:LX/D8V;

    .line 463742
    return-void
.end method

.method public final a(ILandroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D8b;LX/D8V;ILX/D8S;)V
    .locals 0
    .param p9    # LX/D8S;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/D8b;",
            "Lcom/facebook/video/watchandmore/core/OnExitWatchAndMoreListener;",
            "I",
            "Lcom/facebook/video/watchandmore/core/WatchAndMoreContentAnimationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 463714
    iput-object p2, p0, LX/2nI;->d:Landroid/view/View;

    .line 463715
    iput p1, p0, LX/2nI;->e:I

    .line 463716
    iput-object p4, p0, LX/2nI;->f:Landroid/content/Context;

    .line 463717
    iput-object p7, p0, LX/2nI;->i:LX/D8V;

    .line 463718
    iput-object p5, p0, LX/2nI;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 463719
    iput p8, p0, LX/2nI;->j:I

    .line 463720
    invoke-direct {p0}, LX/2nI;->f()V

    .line 463721
    iget-object p1, p0, LX/2nI;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 463722
    iget-object p2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p2

    .line 463723
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object p2

    .line 463724
    if-nez p2, :cond_0

    .line 463725
    :goto_0
    return-void

    .line 463726
    :cond_0
    iget-object p1, p0, LX/2nI;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 463727
    iget-object p3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p3

    .line 463728
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p1, p3}, LX/2yB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/47G;

    move-result-object p1

    .line 463729
    iget-object p2, p0, LX/2nI;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p2

    .line 463730
    iget-object p3, p0, LX/2nI;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p3}, LX/182;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object p3

    .line 463731
    iget-object p1, p1, LX/47G;->l:Ljava/lang/String;

    invoke-static {p1, p2, p3}, LX/17Q;->a(Ljava/lang/String;ZLX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    .line 463732
    iget-object p2, p0, LX/2nI;->d:Landroid/view/View;

    invoke-static {p2}, LX/1vZ;->a(Landroid/view/View;)LX/1vY;

    move-result-object p2

    .line 463733
    if-eqz p2, :cond_1

    .line 463734
    invoke-static {p2}, LX/1vZ;->b(LX/1vY;)LX/162;

    move-result-object p2

    .line 463735
    if-eqz p2, :cond_1

    .line 463736
    const-string p3, "tn"

    invoke-virtual {p1, p3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 463737
    :cond_1
    iget-object p2, p0, LX/2nI;->a:LX/0Zb;

    invoke-interface {p2, p1}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/res/Configuration;I)V
    .locals 2

    .prologue
    .line 463709
    iput p2, p0, LX/2nI;->j:I

    .line 463710
    invoke-direct {p0}, LX/2nI;->g()V

    .line 463711
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 463712
    invoke-direct {p0}, LX/2nI;->f()V

    .line 463713
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 463702
    iget-object v0, p0, LX/2nI;->k:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    if-nez v0, :cond_0

    .line 463703
    :goto_0
    return-void

    .line 463704
    :cond_0
    iget-object v0, p0, LX/2nI;->k:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    const/4 p0, 0x0

    .line 463705
    iget-object v1, v0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 463706
    iget-object v1, v0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    invoke-virtual {v1, p0, p0, p0, p0}, Landroid/view/View;->setPadding(IIII)V

    .line 463707
    iget-object v1, v0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->v:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 463708
    :cond_1
    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 463701
    iget-object v1, p0, LX/2nI;->b:LX/0Uh;

    const/16 v2, 0x588

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2}, LX/2nI;->c(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final c()LX/D8g;
    .locals 1

    .prologue
    .line 463700
    sget-object v0, LX/D8g;->WATCH_AND_DIRECT_INSTALL:LX/D8g;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 463699
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/D7g;
    .locals 1

    .prologue
    .line 463698
    const/4 v0, 0x0

    return-object v0
.end method
