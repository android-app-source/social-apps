.class public LX/2So;
.super LX/2SP;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0Up;
.implements LX/2Sp;
.implements LX/2Sq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2SP;",
        "LX/0c5;",
        "LX/0Up;",
        "LX/2Sp;",
        "LX/2Sq",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/search/model/TypeaheadUnit;

.field private static volatile g:LX/2So;


# instance fields
.field private final b:LX/0ad;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/2SP;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2SR;

.field public f:LX/2SR;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 413395
    new-instance v0, Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;

    sget-object v1, LX/103;->VIDEO:LX/103;

    invoke-direct {v0, v1}, Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;-><init>(LX/103;)V

    sput-object v0, LX/2So;->a:Lcom/facebook/search/model/TypeaheadUnit;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0Or;LX/2Sr;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2Sr;",
            ")V"
        }
    .end annotation

    .prologue
    .line 413389
    invoke-direct {p0}, LX/2SP;-><init>()V

    .line 413390
    new-instance v0, LX/2Ss;

    invoke-direct {v0, p0}, LX/2Ss;-><init>(LX/2So;)V

    iput-object v0, p0, LX/2So;->e:LX/2SR;

    .line 413391
    iput-object p1, p0, LX/2So;->b:LX/0ad;

    .line 413392
    iput-object p2, p0, LX/2So;->c:LX/0Or;

    .line 413393
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/2So;->d:LX/0Px;

    .line 413394
    return-void
.end method

.method public static a(LX/0QB;)LX/2So;
    .locals 6

    .prologue
    .line 413376
    sget-object v0, LX/2So;->g:LX/2So;

    if-nez v0, :cond_1

    .line 413377
    const-class v1, LX/2So;

    monitor-enter v1

    .line 413378
    :try_start_0
    sget-object v0, LX/2So;->g:LX/2So;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413379
    if-eqz v2, :cond_0

    .line 413380
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413381
    new-instance v5, LX/2So;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2Sr;->a(LX/0QB;)LX/2Sr;

    move-result-object v4

    check-cast v4, LX/2Sr;

    invoke-direct {v5, v3, p0, v4}, LX/2So;-><init>(LX/0ad;LX/0Or;LX/2Sr;)V

    .line 413382
    move-object v0, v5

    .line 413383
    sput-object v0, LX/2So;->g:LX/2So;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413384
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413385
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413386
    :cond_1
    sget-object v0, LX/2So;->g:LX/2So;

    return-object v0

    .line 413387
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413388
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/7BE;
    .locals 1

    .prologue
    .line 413375
    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-static {v0}, LX/2SP;->a(LX/0Px;)LX/7BE;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2SR;LX/2Sp;)V
    .locals 4

    .prologue
    .line 413369
    iput-object p1, p0, LX/2So;->f:LX/2SR;

    .line 413370
    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 413371
    if-eqz p1, :cond_0

    iget-object v1, p0, LX/2So;->e:LX/2SR;

    :goto_1
    invoke-virtual {v0, v1, p2}, LX/2SP;->a(LX/2SR;LX/2Sp;)V

    .line 413372
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 413373
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 413374
    :cond_1
    return-void
.end method

.method public final a(LX/7HZ;)V
    .locals 0

    .prologue
    .line 413368
    return-void
.end method

.method public final a(LX/7Hi;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 413367
    return-void
.end method

.method public final a(LX/Cwb;)V
    .locals 3

    .prologue
    .line 413363
    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 413364
    invoke-virtual {v0, p1}, LX/2SP;->a(LX/Cwb;)V

    .line 413365
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 413366
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 5
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 413358
    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 413359
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, LX/EPu;->MEMORY:LX/EPu;

    if-ne p2, v3, :cond_0

    sget-object v3, LX/7BE;->READY:LX/7BE;

    invoke-virtual {v0}, LX/2SP;->a()LX/7BE;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/7BE;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 413360
    :cond_0
    invoke-virtual {v0, p1, p2}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 413361
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 413362
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 3

    .prologue
    .line 413326
    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 413327
    invoke-virtual {v0, p1}, LX/2SP;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 413328
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 413329
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 413357
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 413353
    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 413354
    invoke-virtual {v0}, LX/2SP;->c()V

    .line 413355
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 413356
    :cond_0
    return-void
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 413351
    invoke-virtual {p0}, LX/2SP;->c()V

    .line 413352
    return-void
.end method

.method public final get()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 413334
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 413335
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 413336
    iget-object v0, p0, LX/2So;->b:LX/0ad;

    sget-short v1, LX/100;->cf:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 413337
    new-instance v0, Lcom/facebook/search/model/GapTypeaheadUnit;

    invoke-direct {v0}, Lcom/facebook/search/model/GapTypeaheadUnit;-><init>()V

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 413338
    :cond_0
    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/2Sr;

    if-eqz v0, :cond_1

    .line 413339
    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sr;

    invoke-virtual {v0}, LX/2Sr;->i()V

    .line 413340
    :cond_1
    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    move v1, v2

    :goto_0
    if-ge v4, v6, :cond_5

    iget-object v0, p0, LX/2So;->d:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 413341
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v7

    if-eqz v7, :cond_2

    sget-object v7, LX/7BE;->NOT_READY:LX/7BE;

    invoke-virtual {v0}, LX/2SP;->a()LX/7BE;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/7BE;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 413342
    :cond_2
    invoke-virtual {v0}, LX/2SP;->b()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 413343
    invoke-virtual {v0}, LX/2SP;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 413344
    if-eqz v1, :cond_4

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 413345
    :goto_1
    invoke-virtual {v5, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 413346
    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_4
    move v1, v3

    .line 413347
    goto :goto_1

    .line 413348
    :cond_5
    if-eqz v1, :cond_6

    .line 413349
    sget-object v0, LX/2So;->a:Lcom/facebook/search/model/TypeaheadUnit;

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 413350
    :cond_6
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final init()V
    .locals 2

    .prologue
    .line 413330
    invoke-virtual {p0}, LX/2SP;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 413331
    :cond_0
    :goto_0
    return-void

    .line 413332
    :cond_1
    iget-object v0, p0, LX/2So;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 413333
    const/4 v0, 0x0

    sget-object v1, LX/EPu;->MEMORY:LX/EPu;

    invoke-virtual {p0, v0, v1}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    goto :goto_0
.end method
