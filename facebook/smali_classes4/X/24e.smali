.class public interface abstract LX/24e;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a(II)V
.end method

.method public abstract a(Ljava/lang/String;I)V
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract getPhotoAttachmentView()Landroid/view/View;
.end method

.method public abstract getUnderlyingDraweeView()Lcom/facebook/drawee/view/GenericDraweeView;
.end method

.method public abstract setActualImageFocusPoint(Landroid/graphics/PointF;)V
.end method

.method public abstract setController(LX/1aZ;)V
.end method

.method public abstract setOnBadgeClickListener(LX/24m;)V
    .param p1    # LX/24m;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract setOnPhotoClickListener(LX/24m;)V
    .param p1    # LX/24m;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract setPairedVideoUri(Ljava/lang/String;)V
.end method
