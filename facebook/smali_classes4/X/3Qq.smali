.class public LX/3Qq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Pw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile n:LX/3Qq;


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Xl;

.field private final e:LX/0WJ;

.field private final f:LX/0Zb;

.field private final g:LX/3QD;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BAa;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

.field public final j:LX/2c4;

.field public final k:LX/17Y;

.field public final l:Landroid/content/Context;

.field private final m:LX/2bC;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 565968
    const-class v0, LX/3Qq;

    sput-object v0, LX/3Qq;->a:Ljava/lang/Class;

    .line 565969
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/notifications/constants/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->MOBILE_ZERO_FREE_FACEBOOK_LAUNCH:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/3Qq;->b:LX/2QP;

    return-void
.end method

.method public constructor <init>(LX/2bC;LX/0Or;LX/0Xl;LX/0WJ;LX/0Zb;LX/3QD;LX/0Or;Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;LX/2c4;Landroid/content/Context;LX/17Y;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/push/annotations/IsZeroMobilePushEnabled;
        .end annotation
    .end param
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2bC;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Xl;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0Zb;",
            "LX/3QD;",
            "LX/0Or",
            "<",
            "LX/BAa;",
            ">;",
            "Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;",
            "LX/2c4;",
            "Landroid/content/Context;",
            "LX/17Y;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565971
    iput-object p1, p0, LX/3Qq;->m:LX/2bC;

    .line 565972
    iput-object p2, p0, LX/3Qq;->c:LX/0Or;

    .line 565973
    iput-object p3, p0, LX/3Qq;->d:LX/0Xl;

    .line 565974
    iput-object p4, p0, LX/3Qq;->e:LX/0WJ;

    .line 565975
    iput-object p5, p0, LX/3Qq;->f:LX/0Zb;

    .line 565976
    iput-object p6, p0, LX/3Qq;->g:LX/3QD;

    .line 565977
    iput-object p7, p0, LX/3Qq;->h:LX/0Or;

    .line 565978
    iput-object p8, p0, LX/3Qq;->i:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    .line 565979
    iput-object p9, p0, LX/3Qq;->j:LX/2c4;

    .line 565980
    iput-object p10, p0, LX/3Qq;->l:Landroid/content/Context;

    .line 565981
    iput-object p11, p0, LX/3Qq;->k:LX/17Y;

    .line 565982
    return-void
.end method

.method public static a(LX/0QB;)LX/3Qq;
    .locals 15

    .prologue
    .line 565983
    sget-object v0, LX/3Qq;->n:LX/3Qq;

    if-nez v0, :cond_1

    .line 565984
    const-class v1, LX/3Qq;

    monitor-enter v1

    .line 565985
    :try_start_0
    sget-object v0, LX/3Qq;->n:LX/3Qq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 565986
    if-eqz v2, :cond_0

    .line 565987
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 565988
    new-instance v3, LX/3Qq;

    invoke-static {v0}, LX/2bC;->a(LX/0QB;)LX/2bC;

    move-result-object v4

    check-cast v4, LX/2bC;

    const/16 v5, 0x389

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v7

    check-cast v7, LX/0WJ;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-static {v0}, LX/3QD;->a(LX/0QB;)LX/3QD;

    move-result-object v9

    check-cast v9, LX/3QD;

    const/16 v10, 0x2acf

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v0}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a(LX/0QB;)Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    move-result-object v11

    check-cast v11, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    invoke-static {v0}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v12

    check-cast v12, LX/2c4;

    const-class v13, Landroid/content/Context;

    invoke-interface {v0, v13}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/Context;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v14

    check-cast v14, LX/17Y;

    invoke-direct/range {v3 .. v14}, LX/3Qq;-><init>(LX/2bC;LX/0Or;LX/0Xl;LX/0WJ;LX/0Zb;LX/3QD;LX/0Or;Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;LX/2c4;Landroid/content/Context;LX/17Y;)V

    .line 565989
    move-object v0, v3

    .line 565990
    sput-object v0, LX/3Qq;->n:LX/3Qq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565991
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 565992
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565993
    :cond_1
    sget-object v0, LX/3Qq;->n:LX/3Qq;

    return-object v0

    .line 565994
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 565995
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 565996
    sget-object v0, LX/3Qq;->b:LX/2QP;

    return-object v0
.end method

.method public final a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 11

    .prologue
    .line 565997
    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, LX/3Qq;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 565998
    :cond_0
    :goto_0
    return-void

    .line 565999
    :cond_1
    iget-object v0, p0, LX/3Qq;->e:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566000
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "zero_push_on_free_facebook_launch"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "zero_push"

    .line 566001
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 566002
    move-object v0, v0

    .line 566003
    iget-object v1, p0, LX/3Qq;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 566004
    :try_start_0
    iget-object v0, p0, LX/3Qq;->g:LX/3QD;

    invoke-virtual {v0, p1}, LX/3QD;->a(LX/0lF;)Lcom/facebook/notifications/model/SystemTrayNotification;

    move-result-object v0

    iget-object v1, p2, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/model/SystemTrayNotification;->a(Ljava/lang/String;)Lcom/facebook/notifications/model/SystemTrayNotification;

    move-result-object v0

    iget-object v1, p2, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v1}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/model/SystemTrayNotification;->b(Ljava/lang/String;)Lcom/facebook/notifications/model/SystemTrayNotification;

    move-result-object v0

    iget-wide v2, p2, Lcom/facebook/push/PushProperty;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/facebook/notifications/model/SystemTrayNotification;->a(J)Lcom/facebook/notifications/model/SystemTrayNotification;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 566005
    iget-object v1, p0, LX/3Qq;->d:LX/0Xl;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "zero_token_request_reason"

    sget-object v4, LX/32P;->FREE_FACEBOOK_LAUNCH_PUSH:LX/32P;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 566006
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v6

    .line 566007
    const v8, 0x7f0218e4

    .line 566008
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->c()Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v10

    .line 566009
    iget-object v5, p0, LX/3Qq;->h:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/BAa;

    const-string v7, "lnb"

    invoke-virtual {v0, v7}, Lcom/facebook/notifications/model/SystemTrayNotification;->c(Ljava/lang/String;)LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v5, v7}, LX/BAa;->a(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v7

    const-string v5, "lnt"

    invoke-virtual {v0, v5}, Lcom/facebook/notifications/model/SystemTrayNotification;->c(Ljava/lang/String;)LX/0am;

    move-result-object v5

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v7, v5}, LX/BAa;->d(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v5

    invoke-virtual {v5, v8}, LX/BAa;->a(I)LX/BAa;

    move-result-object v7

    const-string v5, "lnt"

    invoke-virtual {v0, v5}, Lcom/facebook/notifications/model/SystemTrayNotification;->c(Ljava/lang/String;)LX/0am;

    move-result-object v5

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v7, v5}, LX/BAa;->c(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v5

    invoke-static {v0}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a(Lcom/facebook/notifications/model/SystemTrayNotification;)J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, LX/BAa;->a(J)LX/BAa;

    move-result-object v5

    .line 566010
    iput-object v0, v5, LX/BAa;->c:Lcom/facebook/notifications/model/SystemTrayNotification;

    .line 566011
    move-object v7, v5

    .line 566012
    iget-object v5, p0, LX/3Qq;->k:LX/17Y;

    iget-object v8, p0, LX/3Qq;->l:Landroid/content/Context;

    sget-object v9, LX/0ax;->cL:Ljava/lang/String;

    invoke-interface {v5, v8, v9}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    .line 566013
    iget-object v5, p0, LX/3Qq;->j:LX/2c4;

    sget-object v9, LX/8D4;->ACTIVITY:LX/8D4;

    invoke-virtual/range {v5 .. v10}, LX/2c4;->a(Lcom/facebook/notifications/constants/NotificationType;LX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 566014
    goto/16 :goto_0

    .line 566015
    :catch_0
    move-exception v0

    .line 566016
    sget-object v1, LX/3Qq;->a:Ljava/lang/Class;

    const-string v2, "IOException"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 566017
    iget-object v1, p0, LX/3Qq;->m:LX/2bC;

    iget-object v2, p2, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method
