.class public LX/2mp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2mp;


# instance fields
.field public final a:LX/0ka;

.field private final b:LX/1Yk;

.field public c:LX/0wq;


# direct methods
.method public constructor <init>(LX/0ka;LX/1Yk;LX/0wq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 462096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462097
    iput-object p1, p0, LX/2mp;->a:LX/0ka;

    .line 462098
    iput-object p2, p0, LX/2mp;->b:LX/1Yk;

    .line 462099
    iput-object p3, p0, LX/2mp;->c:LX/0wq;

    .line 462100
    return-void
.end method

.method public static a(LX/0QB;)LX/2mp;
    .locals 6

    .prologue
    .line 462101
    sget-object v0, LX/2mp;->d:LX/2mp;

    if-nez v0, :cond_1

    .line 462102
    const-class v1, LX/2mp;

    monitor-enter v1

    .line 462103
    :try_start_0
    sget-object v0, LX/2mp;->d:LX/2mp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 462104
    if-eqz v2, :cond_0

    .line 462105
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 462106
    new-instance p0, LX/2mp;

    invoke-static {v0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v3

    check-cast v3, LX/0ka;

    invoke-static {v0}, LX/1Yk;->a(LX/0QB;)LX/1Yk;

    move-result-object v4

    check-cast v4, LX/1Yk;

    invoke-static {v0}, LX/0wq;->b(LX/0QB;)LX/0wq;

    move-result-object v5

    check-cast v5, LX/0wq;

    invoke-direct {p0, v3, v4, v5}, LX/2mp;-><init>(LX/0ka;LX/1Yk;LX/0wq;)V

    .line 462107
    move-object v0, p0

    .line 462108
    sput-object v0, LX/2mp;->d:LX/2mp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 462109
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 462110
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 462111
    :cond_1
    sget-object v0, LX/2mp;->d:LX/2mp;

    return-object v0

    .line 462112
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 462113
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
