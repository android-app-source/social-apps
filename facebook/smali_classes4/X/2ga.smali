.class public LX/2ga;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1fT;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/0lC;

.field public final d:LX/0Xl;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 448671
    const-class v0, LX/2ga;

    sput-object v0, LX/2ga;->b:Ljava/lang/Class;

    .line 448672
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/2ga;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ACTION_RECEIVE_FRIENDS_LOCATION"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2ga;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0lC;LX/0Xl;)V
    .locals 0
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 448673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448674
    iput-object p1, p0, LX/2ga;->c:LX/0lC;

    .line 448675
    iput-object p2, p0, LX/2ga;->d:LX/0Xl;

    .line 448676
    return-void
.end method

.method private static b(LX/0lF;)Lcom/facebook/location/ImmutableLocation;
    .locals 8

    .prologue
    .line 448661
    const-string v0, "lat"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->f(LX/0lF;)F

    move-result v0

    .line 448662
    const-string v1, "lon"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->f(LX/0lF;)F

    move-result v1

    .line 448663
    const-string v2, "acc"

    invoke-virtual {p0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->f(LX/0lF;)F

    move-result v2

    .line 448664
    const-string v3, "ts"

    invoke-virtual {p0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->c(LX/0lF;)J

    move-result-wide v4

    .line 448665
    float-to-double v6, v0

    float-to-double v0, v1

    invoke-static {v6, v7, v0, v1}, Lcom/facebook/location/ImmutableLocation;->a(DD)LX/0z7;

    move-result-object v0

    .line 448666
    const/4 v1, 0x0

    cmpl-float v1, v2, v1

    if-ltz v1, :cond_0

    .line 448667
    invoke-virtual {v0, v2}, LX/0z7;->b(F)LX/0z7;

    .line 448668
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v1, v4, v2

    if-lez v1, :cond_1

    .line 448669
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0z7;->c(J)LX/0z7;

    .line 448670
    :cond_1
    invoke-virtual {v0}, LX/0z7;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 4

    .prologue
    .line 448643
    :try_start_0
    const-string v0, "/friends_locations"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 448644
    iget-object v0, p0, LX/2ga;->c:LX/0lC;

    invoke-static {p2}, LX/0YN;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 448645
    const-string v1, "nearby_context"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 448646
    const-string v2, "meta_context"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 448647
    invoke-static {v0}, LX/2ga;->b(LX/0lF;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v3

    .line 448648
    const-string p1, "id"

    invoke-virtual {v0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p1

    invoke-static {p1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object p1

    move-object v0, p1

    .line 448649
    new-instance p1, Landroid/content/Intent;

    sget-object p2, LX/2ga;->a:Ljava/lang/String;

    invoke-direct {p1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 448650
    if-eqz v0, :cond_0

    .line 448651
    const-string p2, "user_id"

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 448652
    :cond_0
    if-eqz v3, :cond_1

    .line 448653
    const-string p2, "user_location"

    invoke-virtual {p1, p2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 448654
    :cond_1
    if-eqz v1, :cond_2

    .line 448655
    const-string p2, "user_nearby_context"

    invoke-virtual {p1, p2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 448656
    :cond_2
    if-eqz v2, :cond_3

    .line 448657
    const-string p2, "user_meta_context"

    invoke-virtual {p1, p2, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 448658
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    invoke-virtual {p2}, Landroid/os/Bundle;->size()I

    move-result p2

    if-lez p2, :cond_4

    .line 448659
    iget-object p2, p0, LX/2ga;->d:LX/0Xl;

    invoke-interface {p2, p1}, LX/0Xl;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 448660
    :cond_4
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
