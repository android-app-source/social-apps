.class public final LX/27V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 0

    .prologue
    .line 372920
    iput-object p1, p0, LX/27V;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 372921
    iget-object v0, p0, LX/27V;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    new-instance v1, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$26$1;

    invoke-direct {v1, p0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$26$1;-><init>(LX/27V;)V

    invoke-virtual {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 372922
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 372923
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, LX/2Oo;

    if-eqz v0, :cond_0

    .line 372924
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 372925
    invoke-virtual {v0}, LX/2Oo;->b()Ljava/lang/String;

    move-result-object v1

    .line 372926
    invoke-virtual {v0}, LX/2Oo;->c()Ljava/lang/String;

    move-result-object v0

    .line 372927
    :goto_0
    iget-object v2, p0, LX/27V;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    new-instance v3, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$26$2;

    invoke-direct {v3, p0, v1, v0}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity$26$2;-><init>(LX/27V;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 372928
    return-void

    .line 372929
    :cond_0
    const-string v1, ""

    .line 372930
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 372931
    invoke-direct {p0}, LX/27V;->a()V

    return-void
.end method
