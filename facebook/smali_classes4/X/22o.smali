.class public LX/22o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/22o;


# instance fields
.field public final a:LX/0kb;

.field public final b:LX/0qX;

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/0kb;LX/0ad;LX/0qX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 361283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361284
    iput-object p1, p0, LX/22o;->a:LX/0kb;

    .line 361285
    iput-object p2, p0, LX/22o;->c:LX/0ad;

    .line 361286
    iput-object p3, p0, LX/22o;->b:LX/0qX;

    .line 361287
    return-void
.end method

.method public static a(LX/0QB;)LX/22o;
    .locals 6

    .prologue
    .line 361288
    sget-object v0, LX/22o;->d:LX/22o;

    if-nez v0, :cond_1

    .line 361289
    const-class v1, LX/22o;

    monitor-enter v1

    .line 361290
    :try_start_0
    sget-object v0, LX/22o;->d:LX/22o;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 361291
    if-eqz v2, :cond_0

    .line 361292
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 361293
    new-instance p0, LX/22o;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v5

    check-cast v5, LX/0qX;

    invoke-direct {p0, v3, v4, v5}, LX/22o;-><init>(LX/0kb;LX/0ad;LX/0qX;)V

    .line 361294
    move-object v0, p0

    .line 361295
    sput-object v0, LX/22o;->d:LX/22o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361296
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 361297
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 361298
    :cond_1
    sget-object v0, LX/22o;->d:LX/22o;

    return-object v0

    .line 361299
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 361300
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 361282
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public final d()Z
    .locals 4

    .prologue
    .line 361279
    iget-object v0, p0, LX/22o;->b:LX/0qX;

    const/4 v1, 0x0

    .line 361280
    sget-wide v2, LX/0X5;->cm:J

    invoke-static {v0, v2, v3, v1}, LX/0qX;->a(LX/0qX;JZ)Z

    move-result v2

    move v0, v2

    .line 361281
    return v0
.end method
