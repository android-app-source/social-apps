.class public LX/3NA;
.super LX/3Ml;
.source ""


# instance fields
.field private final c:LX/2Og;

.field private final d:LX/3NB;

.field private final e:LX/3NC;

.field private final f:LX/3ND;

.field private final g:LX/3Mn;

.field private final h:LX/2Or;

.field private final i:LX/2Oq;

.field public j:I

.field public k:I

.field public l:Z

.field public m:Z

.field public n:Z


# direct methods
.method public constructor <init>(LX/0Zr;LX/3NB;LX/3NC;LX/2Og;LX/3ND;LX/3Mn;LX/2Or;LX/2Oq;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 558401
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 558402
    iput v0, p0, LX/3NA;->j:I

    .line 558403
    iput v0, p0, LX/3NA;->k:I

    .line 558404
    iput-object p4, p0, LX/3NA;->c:LX/2Og;

    .line 558405
    iput-object p2, p0, LX/3NA;->d:LX/3NB;

    .line 558406
    iput-object p3, p0, LX/3NA;->e:LX/3NC;

    .line 558407
    iput-object p5, p0, LX/3NA;->f:LX/3ND;

    .line 558408
    iput-object p6, p0, LX/3NA;->g:LX/3Mn;

    .line 558409
    iput-object p7, p0, LX/3NA;->h:LX/2Or;

    .line 558410
    iput-object p8, p0, LX/3NA;->i:LX/2Oq;

    .line 558411
    return-void
.end method

.method public static a(LX/0QB;)LX/3NA;
    .locals 1

    .prologue
    .line 558412
    invoke-static {p0}, LX/3NA;->b(LX/0QB;)LX/3NA;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3NA;
    .locals 9

    .prologue
    .line 558413
    new-instance v0, LX/3NA;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v1

    check-cast v1, LX/0Zr;

    invoke-static {p0}, LX/3NB;->b(LX/0QB;)LX/3NB;

    move-result-object v2

    check-cast v2, LX/3NB;

    invoke-static {p0}, LX/3NC;->b(LX/0QB;)LX/3NC;

    move-result-object v3

    check-cast v3, LX/3NC;

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v4

    check-cast v4, LX/2Og;

    invoke-static {p0}, LX/3ND;->b(LX/0QB;)LX/3ND;

    move-result-object v5

    check-cast v5, LX/3ND;

    invoke-static {p0}, LX/3Mn;->b(LX/0QB;)LX/3Mn;

    move-result-object v6

    check-cast v6, LX/3Mn;

    invoke-static {p0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v7

    check-cast v7, LX/2Or;

    invoke-static {p0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v8

    check-cast v8, LX/2Oq;

    invoke-direct/range {v0 .. v8}, LX/3NA;-><init>(LX/0Zr;LX/3NB;LX/3NC;LX/2Og;LX/3ND;LX/3Mn;LX/2Or;LX/2Oq;)V

    .line 558414
    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 11
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x6

    const/4 v6, -0x1

    .line 558415
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "starting filtering, constraint="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 558416
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 558417
    :goto_0
    new-instance v5, LX/39y;

    invoke-direct {v5}, LX/39y;-><init>()V

    .line 558418
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 558419
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v5, LX/39y;->a:Ljava/lang/Object;

    .line 558420
    iput v6, v5, LX/39y;->b:I

    move-object v0, v5

    .line 558421
    :goto_1
    return-object v0

    .line 558422
    :cond_0
    const-string v0, ""

    move-object v1, v0

    goto :goto_0

    .line 558423
    :cond_1
    iget v0, p0, LX/3NA;->j:I

    if-eq v0, v6, :cond_2

    iget v0, p0, LX/3NA;->j:I

    .line 558424
    :goto_2
    iget v3, p0, LX/3NA;->k:I

    if-eq v3, v6, :cond_3

    iget v3, p0, LX/3NA;->k:I

    .line 558425
    :goto_3
    add-int v6, v0, v3

    .line 558426
    new-instance v0, LX/6is;

    invoke-direct {v0}, LX/6is;-><init>()V

    move-object v0, v0

    .line 558427
    iput-object v1, v0, LX/6is;->b:Ljava/lang/String;

    .line 558428
    move-object v0, v0

    .line 558429
    iput v6, v0, LX/6is;->a:I

    .line 558430
    move-object v0, v0

    .line 558431
    const/4 v3, 0x1

    .line 558432
    iput-boolean v3, v0, LX/6is;->c:Z

    .line 558433
    move-object v0, v0

    .line 558434
    new-instance v3, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;

    invoke-direct {v3, v0}, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;-><init>(LX/6is;)V

    move-object v0, v3

    .line 558435
    :try_start_0
    iget-object v3, p0, LX/3NA;->f:LX/3ND;

    invoke-virtual {v3}, LX/3ND;->c()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 558436
    iget-object v3, p0, LX/3NA;->d:LX/3NB;

    invoke-virtual {v3, v0}, LX/3NB;->a(Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;)Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v3, v0

    .line 558437
    :goto_4
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v7

    .line 558438
    iget-boolean v0, p0, LX/3NA;->l:Z

    if-eqz v0, :cond_5

    .line 558439
    iget-object v0, p0, LX/3NA;->c:LX/2Og;

    invoke-virtual {v0}, LX/2Og;->a()LX/0Px;

    move-result-object v0

    .line 558440
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 558441
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_2
    move v0, v2

    .line 558442
    goto :goto_2

    :cond_3
    move v3, v2

    .line 558443
    goto :goto_3

    .line 558444
    :cond_4
    :try_start_1
    iget-object v3, p0, LX/3NA;->e:LX/3NC;

    invoke-virtual {v3, v0}, LX/3NC;->a(Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;)Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    move-object v3, v0

    .line 558445
    goto :goto_4

    .line 558446
    :catch_0
    move-exception v0

    .line 558447
    const-string v1, "ContactPickerServerGroupFilter"

    const-string v2, "exception with filtering groups"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 558448
    iput v4, v5, LX/39y;->b:I

    .line 558449
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v5, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v5

    .line 558450
    goto :goto_1

    .line 558451
    :cond_5
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 558452
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 558453
    iget-object v9, p0, LX/3NA;->h:LX/2Or;

    invoke-virtual {v9}, LX/2Or;->n()Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, LX/3NA;->i:LX/2Oq;

    invoke-virtual {v9}, LX/2Oq;->a()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 558454
    iget-object v0, p0, LX/3NA;->g:LX/3Mn;

    invoke-virtual {v0, v1, v6}, LX/3Mn;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 558455
    :cond_6
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    .line 558456
    iget-object v1, v3, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->a:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-object v1, v1

    .line 558457
    iget-object v2, v1, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v1, v2

    .line 558458
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 558459
    new-instance v1, LX/6g7;

    invoke-direct {v1}, LX/6g7;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 558460
    const/16 v2, 0x8

    .line 558461
    :goto_6
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 558462
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v4

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 558463
    if-ge v1, v2, :cond_9

    .line 558464
    const/4 v9, 0x1

    .line 558465
    iget-object v4, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v4, v4, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v6, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-eq v4, v6, :cond_7

    iget-object v4, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v7, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 558466
    :cond_7
    const/4 v4, 0x0

    .line 558467
    :goto_8
    move v0, v4

    .line 558468
    if-eqz v0, :cond_a

    .line 558469
    add-int/lit8 v0, v1, 0x1

    :goto_9
    move v1, v0

    .line 558470
    goto :goto_7

    .line 558471
    :cond_8
    iget-object v0, v3, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->a:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-object v0, v0

    .line 558472
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v0, v1

    .line 558473
    goto :goto_6

    .line 558474
    :cond_9
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 558475
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    iput v1, v5, LX/39y;->b:I

    .line 558476
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    iput-object v0, v5, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v5

    .line 558477
    goto/16 :goto_1

    :cond_a
    move v0, v1

    goto :goto_9

    .line 558478
    :cond_b
    iget-object v4, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v4, v0}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v6

    .line 558479
    instance-of v4, v6, LX/DAU;

    if-eqz v4, :cond_e

    iget-boolean v4, p0, LX/3NA;->m:Z

    if-nez v4, :cond_c

    iget-boolean v4, p0, LX/3NA;->n:Z

    if-eqz v4, :cond_e

    :cond_c
    move-object v4, v6

    .line 558480
    check-cast v4, LX/DAU;

    .line 558481
    iget-boolean v10, p0, LX/3NA;->m:Z

    if-eqz v10, :cond_d

    .line 558482
    iput-boolean v9, v4, LX/DAU;->j:Z

    .line 558483
    const-string v10, "multiway_call_search"

    .line 558484
    iput-object v10, v4, LX/DAU;->n:Ljava/lang/String;

    .line 558485
    :cond_d
    iget-boolean v10, p0, LX/3NA;->n:Z

    if-eqz v10, :cond_e

    .line 558486
    iput-boolean v9, v4, LX/DAU;->k:Z

    .line 558487
    const-string v10, "multiway_call_search_video"

    .line 558488
    iput-object v10, v4, LX/DAU;->o:Ljava/lang/String;

    .line 558489
    :cond_e
    invoke-virtual {v8, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v4, v9

    .line 558490
    goto :goto_8
.end method
