.class public LX/2ML;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2ML;


# instance fields
.field private final b:LX/0Sh;

.field public final c:LX/2MM;

.field public final d:LX/0W3;

.field private final e:LX/0QJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QJ",
            "<",
            "LX/6ed;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 396460
    const-class v0, LX/2ML;

    sput-object v0, LX/2ML;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/2MM;LX/0W3;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396455
    iput-object p1, p0, LX/2ML;->b:LX/0Sh;

    .line 396456
    iput-object p2, p0, LX/2ML;->c:LX/2MM;

    .line 396457
    iput-object p3, p0, LX/2ML;->d:LX/0W3;

    .line 396458
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/32 v2, 0x7b98a000

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->b(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    new-instance v1, LX/2MN;

    invoke-direct {v1, p0}, LX/2MN;-><init>(LX/2ML;)V

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0QM;)LX/0QJ;

    move-result-object v0

    iput-object v0, p0, LX/2ML;->e:LX/0QJ;

    .line 396459
    return-void
.end method

.method public static a(LX/0QB;)LX/2ML;
    .locals 6

    .prologue
    .line 396441
    sget-object v0, LX/2ML;->f:LX/2ML;

    if-nez v0, :cond_1

    .line 396442
    const-class v1, LX/2ML;

    monitor-enter v1

    .line 396443
    :try_start_0
    sget-object v0, LX/2ML;->f:LX/2ML;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 396444
    if-eqz v2, :cond_0

    .line 396445
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 396446
    new-instance p0, LX/2ML;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v4

    check-cast v4, LX/2MM;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-direct {p0, v3, v4, v5}, LX/2ML;-><init>(LX/0Sh;LX/2MM;LX/0W3;)V

    .line 396447
    move-object v0, p0

    .line 396448
    sput-object v0, LX/2ML;->f:LX/2ML;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396449
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 396450
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 396451
    :cond_1
    sget-object v0, LX/2ML;->f:LX/2ML;

    return-object v0

    .line 396452
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 396453
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/6ed;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 396435
    iget-object v0, p0, LX/2ML;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 396436
    :try_start_0
    iget-object v0, p0, LX/2ML;->e:LX/0QJ;

    invoke-interface {v0, p1}, LX/0QJ;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 396437
    :goto_0
    return-object v0

    .line 396438
    :catch_0
    move-exception v0

    .line 396439
    sget-object v1, LX/2ML;->a:Ljava/lang/Class;

    const-string v2, "Couldn\'t get sha256 hash for media resource: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, LX/6ed;->a:Landroid/net/Uri;

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 396440
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 396434
    iget-object v0, p0, LX/2ML;->e:LX/0QJ;

    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 396432
    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2ML;->a(LX/6ed;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 396433
    invoke-static {p1}, LX/6ed;->b(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2ML;->a(LX/6ed;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
