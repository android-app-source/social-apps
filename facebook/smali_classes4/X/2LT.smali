.class public LX/2LT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2LT;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395067
    iput-object p1, p0, LX/2LT;->a:Landroid/content/Context;

    .line 395068
    return-void
.end method

.method public static a(LX/0QB;)LX/2LT;
    .locals 4

    .prologue
    .line 395069
    sget-object v0, LX/2LT;->b:LX/2LT;

    if-nez v0, :cond_1

    .line 395070
    const-class v1, LX/2LT;

    monitor-enter v1

    .line 395071
    :try_start_0
    sget-object v0, LX/2LT;->b:LX/2LT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 395072
    if-eqz v2, :cond_0

    .line 395073
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 395074
    new-instance p0, LX/2LT;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/2LT;-><init>(Landroid/content/Context;)V

    .line 395075
    move-object v0, p0

    .line 395076
    sput-object v0, LX/2LT;->b:LX/2LT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395077
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 395078
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 395079
    :cond_1
    sget-object v0, LX/2LT;->b:LX/2LT;

    return-object v0

    .line 395080
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 395081
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
