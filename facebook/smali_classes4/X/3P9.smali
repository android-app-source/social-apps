.class public final LX/3P9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel;",
        ">;",
        "LX/3OS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3LL;


# direct methods
.method public constructor <init>(LX/3LL;)V
    .locals 0

    .prologue
    .line 561134
    iput-object p1, p0, LX/3P9;->a:LX/3LL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 561135
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 561136
    if-nez p1, :cond_0

    move-object v0, v1

    .line 561137
    :goto_0
    return-object v0

    .line 561138
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 561139
    check-cast v0, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/divebar/FetchDivebarNearbyFriendsStatusGraphQLModels$DivebarNearbyFriendsStatusQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 561140
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/15i;->h(II)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v0, v1

    .line 561141
    goto :goto_0

    .line 561142
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 561143
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 561144
    new-instance v0, LX/3OS;

    sget-object v1, LX/3OT;->NUX:LX/3OT;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/3OS;-><init>(LX/3OT;LX/0Px;)V

    move-object v0, v0

    .line 561145
    goto :goto_0

    .line 561146
    :cond_2
    new-instance v0, LX/3OS;

    sget-object v1, LX/3OT;->UPSELL:LX/3OT;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/3OS;-><init>(LX/3OT;LX/0Px;)V

    move-object v0, v0

    .line 561147
    goto :goto_0
.end method
