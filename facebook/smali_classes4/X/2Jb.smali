.class public LX/2Jb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/30V;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8cc;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field private final d:Z

.field private final e:J


# direct methods
.method public constructor <init>(LX/0ad;LX/0Or;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/8cc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 393076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393077
    iput-object p2, p0, LX/2Jb;->b:LX/0Or;

    .line 393078
    sget-short v0, LX/100;->aY:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2Jb;->c:Z

    .line 393079
    sget-short v0, LX/100;->aZ:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2Jb;->d:Z

    .line 393080
    sget v0, LX/100;->aV:I

    const/16 v1, 0x18

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x36ee80

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/2Jb;->e:J

    .line 393081
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 393082
    iget-boolean v0, p0, LX/2Jb;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/2Jb;->d:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/2Jm;
    .locals 1

    .prologue
    .line 393083
    sget-object v0, LX/2Jm;->STATE_CHANGE:LX/2Jm;

    return-object v0
.end method

.method public final c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<+",
            "LX/2Jw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 393084
    iget-object v0, p0, LX/2Jb;->b:LX/0Or;

    return-object v0
.end method

.method public final d()LX/2Jl;
    .locals 2

    .prologue
    .line 393085
    new-instance v0, LX/2Jk;

    invoke-direct {v0}, LX/2Jk;-><init>()V

    sget-object v1, LX/2Jh;->BACKGROUND:LX/2Jh;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jh;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Im;->CONNECTED:LX/2Im;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Im;)LX/2Jk;

    move-result-object v0

    sget-object v1, LX/2Jq;->LOGGED_IN:LX/2Jq;

    invoke-virtual {v0, v1}, LX/2Jk;->a(LX/2Jq;)LX/2Jk;

    move-result-object v0

    .line 393086
    invoke-virtual {v0}, LX/2Jk;->a()LX/2Jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 393087
    iget-wide v0, p0, LX/2Jb;->e:J

    return-wide v0
.end method
