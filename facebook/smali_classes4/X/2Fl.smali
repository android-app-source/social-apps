.class public LX/2Fl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/2Cw;

.field private final c:LX/0W3;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2Fp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 387288
    const-class v0, LX/2Fl;

    sput-object v0, LX/2Fl;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2Cw;LX/0W3;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Cw;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "Ljava/util/Set",
            "<",
            "LX/2Fp;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387284
    iput-object p1, p0, LX/2Fl;->b:LX/2Cw;

    .line 387285
    iput-object p2, p0, LX/2Fl;->c:LX/0W3;

    .line 387286
    iput-object p3, p0, LX/2Fl;->d:Ljava/util/Set;

    .line 387287
    return-void
.end method

.method private declared-synchronized a()V
    .locals 6

    .prologue
    .line 387272
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Fl;->b:LX/2Cw;

    invoke-virtual {v0}, LX/2Cw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 387273
    iget-object v0, p0, LX/2Fl;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Fp;

    .line 387274
    invoke-interface {v0}, LX/2Fp;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 387275
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 387276
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2Fl;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Fp;

    .line 387277
    iget-object v2, p0, LX/2Fl;->c:LX/0W3;

    invoke-interface {v0}, LX/2Fp;->a()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, LX/0W4;->a(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 387278
    invoke-interface {v0}, LX/2Fp;->b()V

    goto :goto_1

    .line 387279
    :cond_1
    invoke-interface {v0}, LX/2Fp;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 387280
    :cond_2
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final init()V
    .locals 0

    .prologue
    .line 387281
    invoke-direct {p0}, LX/2Fl;->a()V

    .line 387282
    return-void
.end method
