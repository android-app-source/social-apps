.class public LX/2OJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/16I;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3R7;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0WJ;

.field private final f:LX/26j;

.field public g:LX/0Yb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 400712
    const-class v0, LX/2OJ;

    sput-object v0, LX/2OJ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/16I;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0WJ;LX/26j;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/16I;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "LX/3R7;",
            ">;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/26j;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 400713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400714
    const/4 v0, 0x0

    iput-object v0, p0, LX/2OJ;->g:LX/0Yb;

    .line 400715
    iput-object p1, p0, LX/2OJ;->b:LX/16I;

    .line 400716
    iput-object p2, p0, LX/2OJ;->c:Ljava/util/concurrent/ExecutorService;

    .line 400717
    iput-object p3, p0, LX/2OJ;->d:LX/0Or;

    .line 400718
    iput-object p4, p0, LX/2OJ;->e:LX/0WJ;

    .line 400719
    iput-object p5, p0, LX/2OJ;->f:LX/26j;

    .line 400720
    return-void
.end method

.method public static c(LX/2OJ;)V
    .locals 1

    .prologue
    .line 400721
    iget-object v0, p0, LX/2OJ;->e:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 400722
    :goto_0
    return-void

    .line 400723
    :cond_0
    invoke-static {p0}, LX/2OJ;->d(LX/2OJ;)V

    goto :goto_0
.end method

.method public static d(LX/2OJ;)V
    .locals 3

    .prologue
    .line 400724
    iget-object v0, p0, LX/2OJ;->g:LX/0Yb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2OJ;->g:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400725
    iget-object v0, p0, LX/2OJ;->g:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 400726
    const/4 v0, 0x0

    iput-object v0, p0, LX/2OJ;->g:LX/0Yb;

    .line 400727
    :cond_0
    iget-object v0, p0, LX/2OJ;->f:LX/26j;

    invoke-virtual {v0}, LX/26j;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 400728
    :goto_0
    return-void

    .line 400729
    :cond_1
    iget-object v0, p0, LX/2OJ;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryColdStartTrigger$2;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryColdStartTrigger$2;-><init>(LX/2OJ;)V

    const v2, -0x4c021990

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 400730
    iget-object v0, p0, LX/2OJ;->b:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400731
    invoke-static {p0}, LX/2OJ;->c(LX/2OJ;)V

    .line 400732
    :goto_0
    return-void

    .line 400733
    :cond_0
    iget-object v0, p0, LX/2OJ;->b:LX/16I;

    sget-object v1, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v2, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryColdStartTrigger$1;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryColdStartTrigger$1;-><init>(LX/2OJ;)V

    invoke-virtual {v0, v1, v2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2OJ;->g:LX/0Yb;

    goto :goto_0
.end method
