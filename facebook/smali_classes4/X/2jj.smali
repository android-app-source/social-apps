.class public LX/2jj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        "TModel:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/1rs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1rs",
            "<TTEdge;TTUserInfo;TTModel;>;"
        }
    .end annotation
.end field

.field private final c:LX/2jk;

.field private final d:LX/2jl;

.field private final e:LX/2jm;

.field private final f:LX/2jn;

.field private final g:LX/2jo;

.field private final h:LX/2jp;

.field public i:LX/2jq;

.field public j:J

.field public k:Z

.field public l:LX/2js;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2js",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field public m:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<TTEdge;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/1s3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionControllerPreprocessor",
            "<TTEdge;TTUserInfo;TTModel;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:I

.field private p:LX/5Mb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5Mb",
            "<TTEdge;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/1rs;LX/2jk;LX/2jl;LX/2jm;LX/2jn;LX/2jo;LX/2jp;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1rs;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/1rs",
            "<TTEdge;TTUserInfo;TTModel;>;",
            "LX/2jk;",
            "LX/2jl;",
            "LX/2jm;",
            "LX/2jn;",
            "LX/2jo;",
            "LX/2jp;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 454210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454211
    sget-object v0, LX/2jq;->USE_CACHE_IF_AVAILABLE:LX/2jq;

    iput-object v0, p0, LX/2jj;->i:LX/2jq;

    .line 454212
    sget-object v0, LX/2jr;->a:LX/2jr;

    iput-object v0, p0, LX/2jj;->l:LX/2js;

    .line 454213
    const/4 v0, 0x0

    iput-object v0, p0, LX/2jj;->p:LX/5Mb;

    .line 454214
    iput-object p4, p0, LX/2jj;->d:LX/2jl;

    .line 454215
    iput-object p5, p0, LX/2jj;->e:LX/2jm;

    .line 454216
    iput-object p6, p0, LX/2jj;->f:LX/2jn;

    .line 454217
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/2jj;->a:Ljava/lang/String;

    .line 454218
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1rs;

    iput-object v0, p0, LX/2jj;->b:LX/1rs;

    .line 454219
    iput-object p3, p0, LX/2jj;->c:LX/2jk;

    .line 454220
    iput-object p7, p0, LX/2jj;->g:LX/2jo;

    .line 454221
    iput-object p8, p0, LX/2jj;->h:LX/2jp;

    .line 454222
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/2jj;->j:J

    .line 454223
    return-void
.end method

.method public static b(LX/2jj;Z)LX/2kW;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation

    .prologue
    .line 454189
    const/4 v7, 0x0

    .line 454190
    sget-object v0, LX/2jv;->a:[I

    iget-object v1, p0, LX/2jj;->h:LX/2jp;

    invoke-virtual {v1}, LX/2jp;->a()LX/2jw;

    move-result-object v1

    invoke-virtual {v1}, LX/2jw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 454191
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown ConnectionCachePolicy"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454192
    :pswitch_0
    iget-object v0, p0, LX/2jj;->m:LX/0QK;

    if-eqz v0, :cond_0

    .line 454193
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Deduplication isn\'t supported in the memory story, yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454194
    :cond_0
    iget-object v0, p0, LX/2jj;->f:LX/2jn;

    iget-boolean v1, p0, LX/2jj;->k:Z

    iget-object v2, p0, LX/2jj;->a:Ljava/lang/String;

    .line 454195
    new-instance v5, LX/95Y;

    const-class v3, LX/95Z;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/95Z;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-direct {v5, v1, v2, v3, v4}, LX/95Y;-><init>(ZLjava/lang/String;LX/95Z;Ljava/util/concurrent/Executor;)V

    .line 454196
    move-object v2, v5

    .line 454197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/2jj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_CC_QUERY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 454198
    :goto_0
    iget-object v0, p0, LX/2jj;->p:LX/5Mb;

    if-eqz v0, :cond_1

    .line 454199
    iget-object v0, p0, LX/2jj;->p:LX/5Mb;

    invoke-interface {v2, v0}, LX/2jy;->a(LX/5Mb;)V

    .line 454200
    :cond_1
    iget-object v0, p0, LX/2jj;->c:LX/2jk;

    iget-object v1, p0, LX/2jj;->b:LX/1rs;

    iget-object v3, p0, LX/2jj;->i:LX/2jq;

    iget-wide v4, p0, LX/2jj;->j:J

    iget-boolean v6, p0, LX/2jj;->k:Z

    iget-object v8, p0, LX/2jj;->n:LX/1s3;

    iget-object v9, p0, LX/2jj;->g:LX/2jo;

    iget-object v10, p0, LX/2jj;->a:Ljava/lang/String;

    .line 454201
    new-instance p0, LX/2kV;

    invoke-static {v9}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    invoke-direct {p0, v10, v11}, LX/2kV;-><init>(Ljava/lang/String;LX/0tX;)V

    .line 454202
    move-object v9, p0

    .line 454203
    invoke-virtual/range {v0 .. v9}, LX/2jk;->a(LX/1rs;LX/2jy;LX/2jq;JZLjava/lang/String;LX/1s3;LX/2kV;)LX/2kW;

    move-result-object v0

    .line 454204
    if-eqz p1, :cond_2

    .line 454205
    invoke-virtual {v0}, LX/2kW;->a()V

    .line 454206
    :cond_2
    return-object v0

    .line 454207
    :pswitch_1
    iget-object v0, p0, LX/2jj;->e:LX/2jm;

    iget-object v1, p0, LX/2jj;->a:Ljava/lang/String;

    iget-wide v2, p0, LX/2jj;->j:J

    iget-object v4, p0, LX/2jj;->l:LX/2js;

    iget-object v5, p0, LX/2jj;->m:LX/0QK;

    iget v6, p0, LX/2jj;->o:I

    invoke-virtual/range {v0 .. v6}, LX/2jm;->a(Ljava/lang/String;JLX/2js;LX/0QK;I)LX/2jx;

    move-result-object v2

    goto :goto_0

    .line 454208
    :pswitch_2
    iget-object v0, p0, LX/2jj;->d:LX/2jl;

    iget-object v1, p0, LX/2jj;->a:Ljava/lang/String;

    iget-wide v2, p0, LX/2jj;->j:J

    iget-object v4, p0, LX/2jj;->l:LX/2js;

    iget-object v5, p0, LX/2jj;->m:LX/0QK;

    iget v6, p0, LX/2jj;->o:I

    invoke-virtual/range {v0 .. v6}, LX/2jl;->a(Ljava/lang/String;JLX/2js;LX/0QK;I)LX/95n;

    move-result-object v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/2kW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation

    .prologue
    .line 454209
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/2jj;->b(LX/2jj;Z)LX/2kW;

    move-result-object v0

    return-object v0
.end method
