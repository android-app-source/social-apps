.class public final LX/399;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0zP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zP",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0jT;

.field public d:Z

.field public e:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/4cQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0zP;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zP",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 522352
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 522353
    invoke-direct {p0, p1, v0}, LX/399;-><init>(LX/0zP;LX/0Rf;)V

    .line 522354
    return-void
.end method

.method public constructor <init>(LX/0zP;LX/0Px;LX/0Rf;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zP",
            "<TT;>;",
            "LX/0Px",
            "<",
            "LX/4cQ;",
            ">;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 522347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 522348
    iput-object p1, p0, LX/399;->a:LX/0zP;

    .line 522349
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    iput-object v0, p0, LX/399;->b:LX/0Rf;

    .line 522350
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/399;->f:LX/0Px;

    .line 522351
    return-void
.end method

.method public constructor <init>(LX/0zP;LX/0Rf;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zP",
            "<TT;>;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 522344
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 522345
    invoke-direct {p0, p1, v0, p2}, LX/399;-><init>(LX/0zP;LX/0Px;LX/0Rf;)V

    .line 522346
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/399;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0jT;",
            ")",
            "LX/399",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 522340
    instance-of v0, p1, LX/0jS;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Fragment models are required for optimistic mutations"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 522341
    iput-object p1, p0, LX/399;->c:LX/0jT;

    .line 522342
    return-object p0

    .line 522343
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/0jT;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 522336
    iget-object v0, p0, LX/399;->c:LX/0jT;

    return-object v0
.end method

.method public final d()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 522339
    iget-object v0, p0, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 522337
    iget-boolean v0, p0, LX/399;->d:Z

    move v0, v0

    .line 522338
    if-nez v0, :cond_0

    iget-object v0, p0, LX/399;->a:LX/0zP;

    invoke-virtual {v0}, LX/0gW;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
