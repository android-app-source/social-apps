.class public LX/2D2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 383629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383630
    return-void
.end method

.method public static a(LX/0Or;LX/1wY;LX/0yH;LX/0Or;LX/0Or;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;LX/03V;)LX/2D5;
    .locals 3
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/location/IsForceAndroidPlatformImplEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1wY;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "LX/0Or",
            "<",
            "LX/2Fw;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6Z6;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2D4;",
            ">;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")",
            "LX/2D5;"
        }
    .end annotation

    .prologue
    .line 383609
    sget-object v0, LX/2D3;->a:[I

    .line 383610
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 383611
    sget-object v1, LX/1wZ;->MOCK_MPK_STATIC:LX/1wZ;

    .line 383612
    :goto_0
    move-object v1, v1

    .line 383613
    invoke-virtual {v1}, LX/1wZ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 383614
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 383615
    :pswitch_0
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2D5;

    .line 383616
    :goto_1
    return-object v0

    .line 383617
    :pswitch_1
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2D5;

    goto :goto_1

    .line 383618
    :pswitch_2
    invoke-interface {p5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2D5;

    goto :goto_1

    .line 383619
    :cond_0
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-eq v1, v2, :cond_2

    sget-object v1, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    invoke-virtual {p2, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    .line 383620
    :try_start_0
    invoke-virtual {p1}, LX/1wY;->a()I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 383621
    if-nez v2, :cond_1

    const/4 v1, 0x1

    .line 383622
    :cond_1
    :goto_2
    move v1, v1

    .line 383623
    if-nez v1, :cond_3

    .line 383624
    :cond_2
    sget-object v1, LX/1wZ;->ANDROID_PLATFORM:LX/1wZ;

    goto :goto_0

    .line 383625
    :cond_3
    sget-object v1, LX/1wZ;->GOOGLE_PLAY:LX/1wZ;

    goto :goto_0

    .line 383626
    :catch_0
    move-exception v2

    .line 383627
    invoke-static {v2}, LX/3KX;->a(Ljava/lang/RuntimeException;)V

    .line 383628
    const-string p0, "continuous_location_provider"

    const-string p2, "GooglePlayServicesUtil error"

    invoke-virtual {p7, p0, p2, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
