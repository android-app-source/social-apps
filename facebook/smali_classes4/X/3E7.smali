.class public LX/3E7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/2Tm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1sj;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClientManager;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/3E8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/2S7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/Random;
    .annotation runtime Lcom/facebook/common/random/InsecureRandom;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/35Y;


# direct methods
.method public constructor <init>(LX/0Or;LX/35Y;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/35Y;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/rtc/helpers/RtcSignalingHandler$SignalingWakelockListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 536963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 536964
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 536965
    iput-object v0, p0, LX/3E7;->b:LX/0Ot;

    .line 536966
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 536967
    iput-object v0, p0, LX/3E7;->c:LX/0Ot;

    .line 536968
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 536969
    iput-object v0, p0, LX/3E7;->d:LX/0Ot;

    .line 536970
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 536971
    iput-object v0, p0, LX/3E7;->e:LX/0Ot;

    .line 536972
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 536973
    iput-object v0, p0, LX/3E7;->g:LX/0Ot;

    .line 536974
    iput-object p1, p0, LX/3E7;->l:LX/0Or;

    .line 536975
    iput-object p2, p0, LX/3E7;->m:LX/35Y;

    .line 536976
    return-void
.end method

.method public static a(LX/3E7;LX/3ll;[BZ)V
    .locals 11

    .prologue
    const-wide/16 v2, 0x0

    .line 536928
    sget-object v1, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    .line 536929
    invoke-static {v1}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v0

    .line 536930
    iget-object v4, p1, LX/3ll;->traceInfo:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 536931
    iget-object v0, p0, LX/3E7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sj;

    iget-object v1, p1, LX/3ll;->traceInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1sj;->b(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v4

    .line 536932
    invoke-static {v4}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    .line 536933
    const-string v5, "op"

    if-eqz p3, :cond_1

    const-string v0, "webrtc_application_receive_gcm"

    :goto_0
    invoke-interface {v1, v5, v0}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536934
    const-string v0, "service"

    const-string v5, "receiver_webrtc_application_layer"

    invoke-interface {v1, v0, v5}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536935
    if-eqz p3, :cond_0

    .line 536936
    const-string v0, "from_notification"

    const-string v5, "true"

    invoke-interface {v1, v0, v5}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536937
    :cond_0
    iget-object v0, p0, LX/3E7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sj;

    sget-object v5, LX/2gR;->REQUEST_RECEIVE:LX/2gR;

    invoke-virtual {v0, v4, v5, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    move-object v9, v1

    move-object v10, v4

    .line 536938
    :goto_1
    invoke-static {p0}, LX/3E7;->b(LX/3E7;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 536939
    const-string v0, "error_code"

    const-string v1, "voip_not_enabled"

    invoke-interface {v9, v0, v1}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536940
    const-string v0, "success"

    const-string v1, "false"

    invoke-interface {v9, v0, v1}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536941
    iget-object v0, p0, LX/3E7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sj;

    sget-object v1, LX/2gR;->RESPONSE_SEND:LX/2gR;

    invoke-virtual {v0, v10, v1, v9}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 536942
    :goto_2
    return-void

    .line 536943
    :cond_1
    const-string v0, "webrtc_application_receive"

    goto :goto_0

    .line 536944
    :cond_2
    invoke-static {p0}, LX/3E7;->c(LX/3E7;)V

    .line 536945
    iget-object v0, p0, LX/3E7;->a:LX/2Tm;

    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 536946
    iget-object v0, p0, LX/3E7;->h:LX/2S7;

    const-string v1, "Thrift_engineFailure"

    const-string v8, "mqtt"

    move-wide v4, v2

    move-wide v6, v2

    invoke-virtual/range {v0 .. v8}, LX/2S7;->a(Ljava/lang/String;JJJLjava/lang/String;)V

    .line 536947
    const-string v0, "error_code"

    const-string v1, "engine_failure"

    invoke-interface {v9, v0, v1}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536948
    const-string v0, "success"

    const-string v1, "false"

    invoke-interface {v9, v0, v1}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536949
    iget-object v0, p0, LX/3E7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sj;

    sget-object v1, LX/2gR;->RESPONSE_SEND:LX/2gR;

    invoke-virtual {v0, v10, v1, v9}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    goto :goto_2

    .line 536950
    :cond_3
    iget-object v0, p0, LX/3E7;->m:LX/35Y;

    invoke-virtual {v0}, LX/35Y;->a()V

    .line 536951
    iget-object v0, p0, LX/3E7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v10}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v1

    .line 536952
    invoke-static {v1}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v2

    .line 536953
    const-string v0, "op"

    const-string v3, "app_to_engine_receive"

    invoke-interface {v2, v0, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536954
    iget-object v0, p0, LX/3E7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sj;

    sget-object v3, LX/2gR;->REQUEST_SEND:LX/2gR;

    invoke-virtual {v0, v1, v3, v2}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 536955
    iget-object v0, p0, LX/3E7;->a:LX/2Tm;

    .line 536956
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 536957
    if-eqz p3, :cond_6

    const-string v3, "gcm"

    .line 536958
    :goto_3
    iget-object v4, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v4, p2, v3}, Lcom/facebook/webrtc/WebrtcEngine;->onThriftMessageFromPeer([BLjava/lang/String;)V

    .line 536959
    :cond_4
    iget-object v0, p0, LX/3E7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sj;

    sget-object v3, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    invoke-virtual {v0, v1, v3, v2}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 536960
    const-string v0, "success"

    const-string v1, "true"

    invoke-interface {v9, v0, v1}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536961
    iget-object v0, p0, LX/3E7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sj;

    sget-object v1, LX/2gR;->RESPONSE_SEND:LX/2gR;

    invoke-virtual {v0, v10, v1, v9}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    goto/16 :goto_2

    :cond_5
    move-object v9, v0

    move-object v10, v1

    goto/16 :goto_1

    .line 536962
    :cond_6
    const-string v3, "mqtt"

    goto :goto_3
.end method

.method public static a(LX/3E7;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 536917
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536918
    :goto_0
    return-void

    .line 536919
    :cond_0
    iget-object v0, p0, LX/3E7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sj;

    invoke-virtual {v0, p1}, LX/1sj;->b(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v1

    .line 536920
    invoke-static {v1}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v2

    .line 536921
    const-string v0, "op"

    const-string v3, "webrtc_application_receive"

    invoke-interface {v2, v0, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536922
    const-string v0, "service"

    const-string v3, "receiver_webrtc_application_layer"

    invoke-interface {v2, v0, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536923
    const-string v0, "from_notification"

    const-string v3, "true"

    invoke-interface {v2, v0, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536924
    iget-object v0, p0, LX/3E7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sj;

    sget-object v3, LX/2gR;->REQUEST_RECEIVE:LX/2gR;

    invoke-virtual {v0, v1, v3, v2}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 536925
    const-string v0, "error_code"

    invoke-interface {v2, v0, p2}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536926
    const-string v0, "success"

    const-string v3, "false"

    invoke-interface {v2, v0, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536927
    iget-object v0, p0, LX/3E7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sj;

    sget-object v3, LX/2gR;->RESPONSE_SEND:LX/2gR;

    invoke-virtual {v0, v1, v3, v2}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    goto :goto_0
.end method

.method public static a(LX/3E7;[B)V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 536977
    invoke-static {p0}, LX/3E7;->b(LX/3E7;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 536978
    :goto_0
    return-void

    .line 536979
    :cond_0
    invoke-static {p0}, LX/3E7;->c(LX/3E7;)V

    .line 536980
    iget-object v0, p0, LX/3E7;->a:LX/2Tm;

    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 536981
    iget-object v0, p0, LX/3E7;->h:LX/2S7;

    const-string v1, "Thrift_engineFailure"

    const-string v8, "mqtt"

    move-wide v4, v2

    move-wide v6, v2

    invoke-virtual/range {v0 .. v8}, LX/2S7;->a(Ljava/lang/String;JJJLjava/lang/String;)V

    goto :goto_0

    .line 536982
    :cond_1
    iget-object v0, p0, LX/3E7;->m:LX/35Y;

    invoke-virtual {v0}, LX/35Y;->a()V

    .line 536983
    iget-object v0, p0, LX/3E7;->a:LX/2Tm;

    .line 536984
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 536985
    iget-object v1, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v1, p1}, Lcom/facebook/webrtc/WebrtcEngine;->handleMultiwaySignalingMessage([B)V

    .line 536986
    :cond_2
    goto :goto_0
.end method

.method private static b(LX/3E7;)Z
    .locals 1

    .prologue
    .line 536916
    iget-object v0, p0, LX/3E7;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static c(LX/3E7;)V
    .locals 7

    .prologue
    .line 536914
    iget-object v0, p0, LX/3E7;->a:LX/2Tm;

    iget-object v1, p0, LX/3E7;->j:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/webrtc/IWebrtcUiInterface;

    iget-object v2, p0, LX/3E7;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/webrtc/ConferenceCall$Listener;

    iget-object v3, p0, LX/3E7;->j:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    iget-object v4, p0, LX/3E7;->i:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    iget-object v5, p0, LX/3E7;->h:LX/2S7;

    iget-object v6, p0, LX/3E7;->f:LX/3E8;

    invoke-virtual/range {v0 .. v6}, LX/2Tm;->a(Lcom/facebook/webrtc/IWebrtcUiInterface;Lcom/facebook/webrtc/ConferenceCall$Listener;LX/EDx;Lcom/facebook/webrtc/IWebrtcConfigInterface;Lcom/facebook/webrtc/IWebrtcLoggingInterface;Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;)V

    .line 536915
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 536912
    iget-object v0, p0, LX/3E7;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    new-instance v1, LX/EFl;

    invoke-direct {v1, p0}, LX/EFl;-><init>(LX/3E7;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 536913
    return-void
.end method
