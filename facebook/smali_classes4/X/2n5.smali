.class public LX/2n5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/offlineavailability/LinkMediaAvailabilityListener;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/offlineavailability/OfflineFeedMediaAvailabilityListener$;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/40K;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/40K;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/offlineavailability/LinkMediaAvailabilityListener;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/offlineavailability/OfflineFeedMediaAvailabilityListener$;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 463209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463210
    iput-object p2, p0, LX/2n5;->a:Ljava/util/Set;

    .line 463211
    iput-object p3, p0, LX/2n5;->b:Ljava/util/Set;

    .line 463212
    return-void
.end method

.method public static a(LX/0QB;)LX/2n5;
    .locals 1

    .prologue
    .line 463208
    invoke-static {p0}, LX/2n5;->b(LX/0QB;)LX/2n5;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2n5;
    .locals 5

    .prologue
    .line 463204
    new-instance v0, LX/2n5;

    const/4 v1, 0x0

    .line 463205
    new-instance v2, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v3

    new-instance v4, LX/2n6;

    invoke-direct {v4, p0}, LX/2n6;-><init>(LX/0QB;)V

    invoke-direct {v2, v3, v4}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v2, v2

    .line 463206
    invoke-static {p0}, LX/1BH;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/2n5;-><init>(LX/40K;Ljava/util/Set;Ljava/util/Set;)V

    .line 463207
    return-object v0
.end method
