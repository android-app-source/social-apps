.class public LX/2UE;
.super LX/2UF;
.source ""


# static fields
.field public static final a:Ljava/util/Set;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/regex/Pattern;


# instance fields
.field private final c:LX/2U9;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 415601
    const-string v0, "(?:^|\\D)(\\d{4,10})(?:$|\\D)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/2UE;->b:Ljava/util/regex/Pattern;

    .line 415602
    const-string v0, "32665"

    const-string v1, "FACEBOOK"

    const-string v2, "1006"

    const-string v3, "575756"

    const-string v4, "57575601"

    const-string v5, "57575751"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "2123"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "3404"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "561619"

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/2UE;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/1Ml;LX/2U9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415598
    invoke-direct {p0, p1, p2}, LX/2UF;-><init>(Landroid/content/ContentResolver;LX/1Ml;)V

    .line 415599
    iput-object p3, p0, LX/2UE;->c:LX/2U9;

    .line 415600
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 415603
    sget-object v0, LX/2UE;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 415604
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-nez v1, :cond_0

    .line 415605
    const/4 v0, 0x0

    .line 415606
    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/Set;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/EiD;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415593
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 415594
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EiD;

    .line 415595
    iget-boolean v3, v0, LX/EiD;->e:Z

    if-eqz v3, :cond_0

    .line 415596
    iget-object v0, v0, LX/EiD;->c:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 415597
    :cond_1
    return-object v1
.end method

.method public static a(LX/2UE;I)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 415591
    iget-object v0, p0, LX/2UE;->c:LX/2U9;

    invoke-virtual {v0, p1}, LX/2U9;->a(I)V

    .line 415592
    return-void
.end method

.method public static c(LX/0QB;)LX/2UE;
    .locals 4

    .prologue
    .line 415589
    new-instance v3, LX/2UE;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v1

    check-cast v1, LX/1Ml;

    invoke-static {p0}, LX/2U9;->b(LX/0QB;)LX/2U9;

    move-result-object v2

    check-cast v2, LX/2U9;

    invoke-direct {v3, v0, v1, v2}, LX/2UE;-><init>(Landroid/content/ContentResolver;LX/1Ml;LX/2U9;)V

    .line 415590
    return-object v3
.end method
