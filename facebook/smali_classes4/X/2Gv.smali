.class public LX/2Gv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2zj;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile d:LX/2Gv;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/23i;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389607
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Gv;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/23i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389625
    iput-object p1, p0, LX/2Gv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 389626
    iput-object p2, p0, LX/2Gv;->c:LX/23i;

    .line 389627
    return-void
.end method

.method public static a(LX/0QB;)LX/2Gv;
    .locals 5

    .prologue
    .line 389611
    sget-object v0, LX/2Gv;->d:LX/2Gv;

    if-nez v0, :cond_1

    .line 389612
    const-class v1, LX/2Gv;

    monitor-enter v1

    .line 389613
    :try_start_0
    sget-object v0, LX/2Gv;->d:LX/2Gv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389614
    if-eqz v2, :cond_0

    .line 389615
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 389616
    new-instance p0, LX/2Gv;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/23i;->a(LX/0QB;)LX/23i;

    move-result-object v4

    check-cast v4, LX/23i;

    invoke-direct {p0, v3, v4}, LX/2Gv;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/23i;)V

    .line 389617
    move-object v0, p0

    .line 389618
    sput-object v0, LX/2Gv;->d:LX/2Gv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389619
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389620
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389621
    :cond_1
    sget-object v0, LX/2Gv;->d:LX/2Gv;

    return-object v0

    .line 389622
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389623
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 389610
    iget-object v0, p0, LX/2Gv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->k:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 389609
    iget-object v0, p0, LX/2Gv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->A:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 389608
    iget-object v0, p0, LX/2Gv;->c:LX/23i;

    invoke-virtual {v0}, LX/23i;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 389628
    iget-object v0, p0, LX/2Gv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->m:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 389598
    iget-object v0, p0, LX/2Gv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->p:LX/0Tn;

    sget-object v2, LX/2Gv;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 389599
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 389600
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 389601
    :goto_0
    move-object v0, v0

    .line 389602
    if-eqz v0, :cond_0

    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 389603
    iget-object v0, p0, LX/2Gv;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->n:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 389604
    const v0, 0x7f0218e9

    return v0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 389605
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 389606
    const/4 v0, 0x0

    return v0
.end method
