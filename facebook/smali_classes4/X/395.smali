.class public LX/395;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/098;


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLVideo;

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Aq;

.field public final d:Lcom/facebook/video/analytics/VideoPlayerInfo;

.field public final e:LX/0JG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0P1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/3FT;

.field public i:LX/1bf;

.field public j:LX/04g;

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field private o:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

.field public p:LX/7DJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;",
            "Lcom/facebook/video/analytics/VideoFeedStoryInfo;",
            "LX/1bf;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 522253
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0JG;Ljava/lang/String;)V

    .line 522254
    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0JG;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;",
            "Lcom/facebook/video/analytics/VideoFeedStoryInfo;",
            "LX/1bf;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/0JG;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 522255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 522256
    new-instance v0, LX/0Aq;

    invoke-direct {v0}, LX/0Aq;-><init>()V

    iput-object v0, p0, LX/395;->c:LX/0Aq;

    .line 522257
    new-instance v0, Lcom/facebook/video/analytics/VideoPlayerInfo;

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-direct {v0, v1}, Lcom/facebook/video/analytics/VideoPlayerInfo;-><init>(LX/04G;)V

    iput-object v0, p0, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 522258
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    iput-object v0, p0, LX/395;->j:LX/04g;

    .line 522259
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/395;->l:Z

    .line 522260
    new-instance v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-direct {v0}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;-><init>()V

    iput-object v0, p0, LX/395;->n:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 522261
    new-instance v0, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-direct {v0}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;-><init>()V

    iput-object v0, p0, LX/395;->o:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 522262
    iput-object p4, p0, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 522263
    iput-object p5, p0, LX/395;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 522264
    iput-object p1, p0, LX/395;->o:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 522265
    iput-object p2, p0, LX/395;->n:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 522266
    iput-object p3, p0, LX/395;->i:LX/1bf;

    .line 522267
    iget-object v0, p0, LX/395;->n:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 522268
    iget-object v1, p2, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    move-object v1, v1

    .line 522269
    iput-object v1, v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    .line 522270
    iput-object p6, p0, LX/395;->e:LX/0JG;

    .line 522271
    iput-object p7, p0, LX/395;->f:Ljava/lang/String;

    .line 522272
    return-void
.end method


# virtual methods
.method public final a(I)LX/395;
    .locals 1

    .prologue
    .line 522273
    iget-object v0, p0, LX/395;->c:LX/0Aq;

    .line 522274
    iput p1, v0, LX/0Aq;->c:I

    .line 522275
    return-object p0
.end method

.method public final a(LX/04D;)LX/395;
    .locals 1

    .prologue
    .line 522276
    iget-object v0, p0, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 522277
    iput-object p1, v0, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    .line 522278
    return-object p0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 522287
    iget-object v0, p0, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)LX/395;
    .locals 1

    .prologue
    .line 522279
    iget-object v0, p0, LX/395;->c:LX/0Aq;

    .line 522280
    iput p1, v0, LX/0Aq;->b:I

    .line 522281
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 522282
    iget-object v0, p0, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 522283
    iget-object v0, p0, LX/395;->n:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 522284
    iget-boolean p0, v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->d:Z

    move v0, p0

    .line 522285
    move v0, v0

    .line 522286
    return v0
.end method

.method public final d()LX/19o;
    .locals 1

    .prologue
    .line 522251
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 522252
    const/4 v0, 0x0

    return v0
.end method

.method public final f()LX/03z;
    .locals 1

    .prologue
    .line 522227
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 1

    .prologue
    .line 522228
    iget-object v0, p0, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->r()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 522229
    const/4 v0, -0x1

    return v0
.end method

.method public final o()LX/04g;
    .locals 1

    .prologue
    .line 522230
    iget-object v0, p0, LX/395;->n:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 522231
    iget-object p0, v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    move-object v0, p0

    .line 522232
    return-object v0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 522233
    iget-object v0, p0, LX/395;->c:LX/0Aq;

    .line 522234
    iget p0, v0, LX/0Aq;->c:I

    move v0, p0

    .line 522235
    return v0
.end method

.method public final q()LX/04D;
    .locals 1

    .prologue
    .line 522236
    iget-object v0, p0, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 522237
    iget-object p0, v0, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v0, p0

    .line 522238
    return-object v0
.end method

.method public final s()LX/162;
    .locals 1

    .prologue
    .line 522239
    iget-object v0, p0, LX/395;->n:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 522240
    iget-object p0, v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->a:LX/162;

    move-object v0, p0

    .line 522241
    return-object v0
.end method

.method public final t()LX/04g;
    .locals 1

    .prologue
    .line 522242
    iget-object v0, p0, LX/395;->n:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 522243
    iget-object p0, v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    move-object v0, p0

    .line 522244
    return-object v0
.end method

.method public final u()I
    .locals 1

    .prologue
    .line 522245
    iget-object v0, p0, LX/395;->c:LX/0Aq;

    .line 522246
    iget p0, v0, LX/0Aq;->b:I

    move v0, p0

    .line 522247
    return v0
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 522248
    iget-object v0, p0, LX/395;->o:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 522249
    iget-object p0, v0, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a:Ljava/lang/String;

    move-object v0, p0

    .line 522250
    return-object v0
.end method
