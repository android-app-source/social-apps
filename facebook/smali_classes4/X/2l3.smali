.class public LX/2l3;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;


# instance fields
.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Zb;

.field public final d:LX/2l4;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 456708
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "bookmarks/impression"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2l3;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/2l4;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 456709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456710
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2l3;->b:Ljava/util/Set;

    .line 456711
    iput-object p1, p0, LX/2l3;->c:LX/0Zb;

    .line 456712
    iput-object p2, p0, LX/2l3;->d:LX/2l4;

    .line 456713
    iput-object p3, p0, LX/2l3;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 456714
    iput-object p4, p0, LX/2l3;->f:LX/0SG;

    .line 456715
    return-void
.end method

.method public static b(LX/0QB;)LX/2l3;
    .locals 5

    .prologue
    .line 456716
    new-instance v4, LX/2l3;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 456717
    new-instance v2, LX/2l4;

    invoke-static {p0}, Lcom/facebook/bookmark/client/BookmarkClient;->a(LX/0QB;)Lcom/facebook/bookmark/client/BookmarkClient;

    move-result-object v1

    check-cast v1, LX/2l5;

    invoke-direct {v2, v1}, LX/2l4;-><init>(LX/2l5;)V

    .line 456718
    move-object v1, v2

    .line 456719
    check-cast v1, LX/2l4;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {v4, v0, v1, v2, v3}, LX/2l3;-><init>(LX/0Zb;LX/2l4;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V

    .line 456720
    return-object v4
.end method


# virtual methods
.method public final a(LX/EhY;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 456721
    iget-object v1, p1, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    .line 456722
    if-eqz v1, :cond_0

    iget-wide v2, v1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 456723
    if-eqz v1, :cond_1

    iget-object v0, v1, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    .line 456724
    :goto_1
    new-instance v1, Lcom/facebook/bookmark/ui/analytics/OpenApplicationEvent;

    invoke-direct {v1, p2, v2, v0}, Lcom/facebook/bookmark/ui/analytics/OpenApplicationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 456725
    iget-object v0, p0, LX/2l3;->c:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 456726
    return-void

    .line 456727
    :cond_0
    iget-object v0, p1, LX/EhY;->c:Ljava/lang/String;

    goto :goto_0

    .line 456728
    :cond_1
    iget-object v0, p1, LX/EhY;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Lcom/facebook/bookmark/model/Bookmark;)V
    .locals 3

    .prologue
    .line 456729
    if-nez p1, :cond_1

    .line 456730
    :cond_0
    :goto_0
    return-void

    .line 456731
    :cond_1
    iget-object v0, p0, LX/2l3;->c:LX/0Zb;

    new-instance v1, Lcom/facebook/bookmark/ui/analytics/BookmarkClickEvent;

    iget-object v2, p0, LX/2l3;->d:LX/2l4;

    invoke-direct {v1, v2, p1}, Lcom/facebook/bookmark/ui/analytics/BookmarkClickEvent;-><init>(LX/2l4;Lcom/facebook/bookmark/model/Bookmark;)V

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 456732
    iget-object v0, p1, Lcom/facebook/bookmark/model/Bookmark;->clientToken:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 456733
    iget-object v0, p0, LX/2l3;->c:LX/0Zb;

    new-instance v1, Lcom/facebook/bookmark/ui/analytics/BookmarkAdClickEvent;

    invoke-direct {v1, p1}, Lcom/facebook/bookmark/ui/analytics/BookmarkAdClickEvent;-><init>(Lcom/facebook/bookmark/model/Bookmark;)V

    invoke-interface {v0, v1}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method
