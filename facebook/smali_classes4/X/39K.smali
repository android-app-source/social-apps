.class public LX/39K;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 522558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 25

    .prologue
    .line 522559
    const/16 v21, 0x0

    .line 522560
    const/16 v20, 0x0

    .line 522561
    const/16 v19, 0x0

    .line 522562
    const/16 v18, 0x0

    .line 522563
    const/16 v17, 0x0

    .line 522564
    const/16 v16, 0x0

    .line 522565
    const/4 v15, 0x0

    .line 522566
    const/4 v14, 0x0

    .line 522567
    const/4 v13, 0x0

    .line 522568
    const/4 v12, 0x0

    .line 522569
    const/4 v11, 0x0

    .line 522570
    const/4 v10, 0x0

    .line 522571
    const/4 v9, 0x0

    .line 522572
    const/4 v8, 0x0

    .line 522573
    const/4 v7, 0x0

    .line 522574
    const/4 v6, 0x0

    .line 522575
    const/4 v5, 0x0

    .line 522576
    const/4 v4, 0x0

    .line 522577
    const/4 v3, 0x0

    .line 522578
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    .line 522579
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 522580
    const/4 v3, 0x0

    .line 522581
    :goto_0
    return v3

    .line 522582
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 522583
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_13

    .line 522584
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v22

    .line 522585
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 522586
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_1

    if-eqz v22, :cond_1

    .line 522587
    const-string v23, "add_item_action_info"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 522588
    invoke-static/range {p0 .. p1}, LX/2sZ;->a(LX/15w;LX/186;)I

    move-result v21

    goto :goto_1

    .line 522589
    :cond_2
    const-string v23, "added_item_state_info"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 522590
    invoke-static/range {p0 .. p1}, LX/2sZ;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 522591
    :cond_3
    const-string v23, "app_section"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 522592
    invoke-static/range {p0 .. p1}, LX/4Tr;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 522593
    :cond_4
    const-string v23, "application"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 522594
    invoke-static/range {p0 .. p1}, LX/2uk;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 522595
    :cond_5
    const-string v23, "curation_nux_message"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 522596
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto :goto_1

    .line 522597
    :cond_6
    const-string v23, "curation_url"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 522598
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto :goto_1

    .line 522599
    :cond_7
    const-string v23, "id"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 522600
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 522601
    :cond_8
    const-string v23, "mediaset"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_9

    .line 522602
    invoke-static/range {p0 .. p1}, LX/4PN;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 522603
    :cond_9
    const-string v23, "name"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_a

    .line 522604
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 522605
    :cond_a
    const-string v23, "new_item_default_privacy"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 522606
    invoke-static/range {p0 .. p1}, LX/39L;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 522607
    :cond_b
    const-string v23, "rating_title"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_c

    .line 522608
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 522609
    :cond_c
    const-string v23, "remove_item_action_info"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 522610
    invoke-static/range {p0 .. p1}, LX/2sZ;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 522611
    :cond_d
    const-string v23, "saved_dashboard_section"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_e

    .line 522612
    invoke-static/range {p0 .. p1}, LX/39M;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 522613
    :cond_e
    const-string v23, "style_list"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_f

    .line 522614
    const-class v8, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v8}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v8

    goto/16 :goto_1

    .line 522615
    :cond_f
    const-string v23, "supports_suggestions"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_10

    .line 522616
    const/4 v3, 0x1

    .line 522617
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 522618
    :cond_10
    const-string v23, "tracking"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_11

    .line 522619
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 522620
    :cond_11
    const-string v23, "url"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_12

    .line 522621
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 522622
    :cond_12
    const-string v23, "view_collection_prompt"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 522623
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 522624
    :cond_13
    const/16 v22, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 522625
    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 522626
    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 522627
    const/16 v20, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 522628
    const/16 v19, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 522629
    const/16 v18, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 522630
    const/16 v17, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 522631
    const/16 v16, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 522632
    const/16 v15, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 522633
    const/16 v14, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 522634
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 522635
    const/16 v12, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 522636
    const/16 v11, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 522637
    const/16 v10, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 522638
    const/16 v9, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 522639
    if-eqz v3, :cond_14

    .line 522640
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->a(IZ)V

    .line 522641
    :cond_14
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 522642
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 522643
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 522644
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 522645
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 522646
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 522647
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/39K;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 522648
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 522649
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 522650
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 522651
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 522652
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 522653
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 522654
    invoke-static {p0, p1}, LX/39K;->a(LX/15w;LX/186;)I

    move-result v1

    .line 522655
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 522656
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/16 v2, 0xe

    .line 522657
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 522658
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522659
    if-eqz v0, :cond_0

    .line 522660
    const-string v1, "add_item_action_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522661
    invoke-static {p0, v0, p2, p3}, LX/2sZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 522662
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522663
    if-eqz v0, :cond_1

    .line 522664
    const-string v1, "added_item_state_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522665
    invoke-static {p0, v0, p2, p3}, LX/2sZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 522666
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522667
    if-eqz v0, :cond_2

    .line 522668
    const-string v1, "app_section"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522669
    invoke-static {p0, v0, p2, p3}, LX/4Tr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 522670
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522671
    if-eqz v0, :cond_3

    .line 522672
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522673
    invoke-static {p0, v0, p2, p3}, LX/2uk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 522674
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522675
    if-eqz v0, :cond_4

    .line 522676
    const-string v1, "curation_nux_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522677
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522678
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522679
    if-eqz v0, :cond_5

    .line 522680
    const-string v1, "curation_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522681
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522682
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522683
    if-eqz v0, :cond_6

    .line 522684
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522685
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522686
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522687
    if-eqz v0, :cond_7

    .line 522688
    const-string v1, "mediaset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522689
    invoke-static {p0, v0, p2, p3}, LX/4PN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 522690
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522691
    if-eqz v0, :cond_8

    .line 522692
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522693
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522694
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522695
    if-eqz v0, :cond_9

    .line 522696
    const-string v1, "new_item_default_privacy"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522697
    invoke-static {p0, v0, p2, p3}, LX/39L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 522698
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522699
    if-eqz v0, :cond_a

    .line 522700
    const-string v1, "rating_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522701
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 522702
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522703
    if-eqz v0, :cond_b

    .line 522704
    const-string v1, "remove_item_action_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522705
    invoke-static {p0, v0, p2, p3}, LX/2sZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 522706
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 522707
    if-eqz v0, :cond_c

    .line 522708
    const-string v1, "saved_dashboard_section"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522709
    invoke-static {p0, v0, p2}, LX/39M;->a(LX/15i;ILX/0nX;)V

    .line 522710
    :cond_c
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 522711
    if-eqz v0, :cond_d

    .line 522712
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522713
    const-class v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 522714
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 522715
    if-eqz v0, :cond_e

    .line 522716
    const-string v1, "supports_suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522717
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 522718
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522719
    if-eqz v0, :cond_f

    .line 522720
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522721
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522722
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522723
    if-eqz v0, :cond_10

    .line 522724
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522725
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522726
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 522727
    if-eqz v0, :cond_11

    .line 522728
    const-string v1, "view_collection_prompt"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522729
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522730
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 522731
    return-void
.end method
