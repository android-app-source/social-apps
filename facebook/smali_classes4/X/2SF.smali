.class public LX/2SF;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 411951
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializer;-><init>()V

    sput-object v0, LX/2SF;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 411952
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializers$StringKeySerializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializers$StringKeySerializer;-><init>()V

    sput-object v0, LX/2SF;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 411953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 411954
    if-nez p0, :cond_0

    .line 411955
    sget-object v0, LX/2SF;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 411956
    :goto_0
    return-object v0

    .line 411957
    :cond_0
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 411958
    const-class v1, Ljava/lang/String;

    if-ne v0, v1, :cond_1

    .line 411959
    sget-object v0, LX/2SF;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0

    .line 411960
    :cond_1
    const-class v1, Ljava/lang/Object;

    if-ne v0, v1, :cond_2

    .line 411961
    sget-object v0, LX/2SF;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0

    .line 411962
    :cond_2
    const-class v1, Ljava/util/Date;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 411963
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializers$DateKeySerializer;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0

    .line 411964
    :cond_3
    const-class v1, Ljava/util/Calendar;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 411965
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/StdKeySerializers$CalendarKeySerializer;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0

    .line 411966
    :cond_4
    sget-object v0, LX/2SF;->a:Lcom/fasterxml/jackson/databind/JsonSerializer;

    goto :goto_0
.end method
