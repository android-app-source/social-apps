.class public final enum LX/2ZF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2ZF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2ZF;

.field public static final enum OTHER:LX/2ZF;

.field public static final enum QE:LX/2ZF;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 422251
    new-instance v0, LX/2ZF;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v2}, LX/2ZF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ZF;->OTHER:LX/2ZF;

    .line 422252
    new-instance v0, LX/2ZF;

    const-string v1, "QE"

    invoke-direct {v0, v1, v3}, LX/2ZF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2ZF;->QE:LX/2ZF;

    .line 422253
    const/4 v0, 0x2

    new-array v0, v0, [LX/2ZF;

    sget-object v1, LX/2ZF;->OTHER:LX/2ZF;

    aput-object v1, v0, v2

    sget-object v1, LX/2ZF;->QE:LX/2ZF;

    aput-object v1, v0, v3

    sput-object v0, LX/2ZF;->$VALUES:[LX/2ZF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 422254
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2ZF;
    .locals 1

    .prologue
    .line 422255
    const-class v0, LX/2ZF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2ZF;

    return-object v0
.end method

.method public static values()[LX/2ZF;
    .locals 1

    .prologue
    .line 422256
    sget-object v0, LX/2ZF;->$VALUES:[LX/2ZF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2ZF;

    return-object v0
.end method
