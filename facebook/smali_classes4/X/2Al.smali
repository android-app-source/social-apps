.class public LX/2Al;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0m4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0m4",
            "<*>;"
        }
    .end annotation
.end field

.field public final b:Z

.field public final c:LX/0lJ;

.field public final d:LX/0lN;

.field public final e:LX/0lW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0lW",
            "<*>;"
        }
    .end annotation
.end field

.field public final f:LX/0lU;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/2Ap;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/2Ap;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/2An;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/2At;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/2At;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Object;",
            "LX/2An;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0m4;ZLX/0lJ;LX/0lN;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;Z",
            "LX/0lJ;",
            "LX/0lN;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 377816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377817
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    .line 377818
    iput-object v0, p0, LX/2Al;->i:Ljava/util/LinkedList;

    .line 377819
    iput-object v0, p0, LX/2Al;->j:Ljava/util/LinkedList;

    .line 377820
    iput-object v0, p0, LX/2Al;->k:Ljava/util/LinkedList;

    .line 377821
    iput-object v0, p0, LX/2Al;->l:Ljava/util/LinkedList;

    .line 377822
    iput-object p1, p0, LX/2Al;->a:LX/0m4;

    .line 377823
    iput-boolean p2, p0, LX/2Al;->b:Z

    .line 377824
    iput-object p3, p0, LX/2Al;->c:LX/0lJ;

    .line 377825
    iput-object p4, p0, LX/2Al;->d:LX/0lN;

    .line 377826
    if-nez p5, :cond_0

    const-string p5, "set"

    :cond_0
    iput-object p5, p0, LX/2Al;->g:Ljava/lang/String;

    .line 377827
    invoke-virtual {p1}, LX/0m4;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, LX/2Al;->a:LX/0m4;

    invoke-virtual {v0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    :cond_1
    iput-object v0, p0, LX/2Al;->f:LX/0lU;

    .line 377828
    iget-object v0, p0, LX/2Al;->f:LX/0lU;

    if-nez v0, :cond_2

    .line 377829
    iget-object v0, p0, LX/2Al;->a:LX/0m4;

    invoke-virtual {v0}, LX/0m4;->c()LX/0lW;

    move-result-object v0

    iput-object v0, p0, LX/2Al;->e:LX/0lW;

    .line 377830
    :goto_0
    return-void

    .line 377831
    :cond_2
    iget-object v0, p0, LX/2Al;->f:LX/0lU;

    iget-object v1, p0, LX/2Al;->a:LX/0m4;

    invoke-virtual {v1}, LX/0m4;->c()LX/0lW;

    move-result-object v1

    invoke-virtual {v0, p4, v1}, LX/0lU;->a(LX/0lN;LX/0lW;)LX/0lW;

    move-result-object v0

    iput-object v0, p0, LX/2Al;->e:LX/0lW;

    goto :goto_0
.end method

.method private a(LX/2At;LX/0lU;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 377832
    if-eqz p2, :cond_4

    .line 377833
    invoke-virtual {p2, p1}, LX/0lU;->e(LX/2At;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377834
    iget-object v0, p0, LX/2Al;->j:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    .line 377835
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2Al;->j:Ljava/util/LinkedList;

    .line 377836
    :cond_0
    iget-object v0, p0, LX/2Al;->j:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 377837
    :cond_1
    :goto_0
    return-void

    .line 377838
    :cond_2
    invoke-virtual {p2, p1}, LX/0lU;->b(LX/2At;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 377839
    iget-object v0, p0, LX/2Al;->l:Ljava/util/LinkedList;

    if-nez v0, :cond_3

    .line 377840
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2Al;->l:Ljava/util/LinkedList;

    .line 377841
    :cond_3
    iget-object v0, p0, LX/2Al;->l:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 377842
    :cond_4
    if-nez p2, :cond_5

    move-object v0, v1

    .line 377843
    :goto_1
    if-nez v0, :cond_6

    .line 377844
    :goto_2
    if-nez v1, :cond_8

    .line 377845
    invoke-virtual {p1}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/2Va;->a(LX/2At;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 377846
    if-nez v2, :cond_7

    .line 377847
    invoke-virtual {p1}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/2Va;->b(LX/2At;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 377848
    if-eqz v2, :cond_1

    .line 377849
    iget-object v0, p0, LX/2Al;->e:LX/0lW;

    invoke-interface {v0, p1}, LX/0lW;->b(LX/2At;)Z

    move-result v0

    move-object v3, v1

    move v1, v0

    .line 377850
    :goto_3
    if-nez p2, :cond_b

    const/4 v0, 0x0

    .line 377851
    :goto_4
    invoke-direct {p0, v2}, LX/2Al;->c(Ljava/lang/String;)LX/2Ap;

    move-result-object v2

    invoke-virtual {v2, p1, v3, v1, v0}, LX/2Ap;->a(LX/2At;Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 377852
    :cond_5
    invoke-virtual {p2, p1}, LX/0lU;->n(LX/0lO;)LX/2Vb;

    move-result-object v0

    goto :goto_1

    .line 377853
    :cond_6
    iget-object v1, v0, LX/2Vb;->_simpleName:Ljava/lang/String;

    move-object v1, v1

    .line 377854
    goto :goto_2

    .line 377855
    :cond_7
    iget-object v0, p0, LX/2Al;->e:LX/0lW;

    invoke-interface {v0, p1}, LX/0lW;->a(LX/2At;)Z

    move-result v0

    move-object v3, v1

    move v1, v0

    goto :goto_3

    .line 377856
    :cond_8
    invoke-static {p1}, LX/2Va;->a(LX/2At;)Ljava/lang/String;

    move-result-object v0

    .line 377857
    if-nez v0, :cond_9

    .line 377858
    invoke-virtual {p1}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v0

    .line 377859
    :cond_9
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_a

    move-object v1, v0

    .line 377860
    :cond_a
    const/4 v2, 0x1

    move-object v3, v1

    move v1, v2

    move-object v2, v0

    goto :goto_3

    .line 377861
    :cond_b
    invoke-virtual {p2, p1}, LX/0lU;->c(LX/2An;)Z

    move-result v0

    goto :goto_4
.end method

.method private a(LX/4pt;)V
    .locals 6

    .prologue
    .line 377760
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    new-array v1, v1, [LX/2Ap;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Ap;

    .line 377761
    iget-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->clear()V

    .line 377762
    array-length v5, v0

    const/4 v1, 0x0

    move v4, v1

    :goto_0
    if-ge v4, v5, :cond_7

    aget-object v3, v0, v4

    .line 377763
    invoke-virtual {v3}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v1

    .line 377764
    iget-boolean v2, p0, LX/2Al;->b:Z

    if-eqz v2, :cond_2

    .line 377765
    invoke-virtual {v3}, LX/2Aq;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 377766
    invoke-virtual {v3}, LX/2Aq;->i()LX/2At;

    invoke-virtual {p1, v1}, LX/4pt;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 377767
    :goto_1
    invoke-virtual {v3}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 377768
    invoke-virtual {v3, v2}, LX/2Ap;->a(Ljava/lang/String;)LX/2Ap;

    move-result-object v1

    move-object v3, v1

    .line 377769
    :cond_0
    iget-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Ap;

    .line 377770
    if-nez v1, :cond_6

    .line 377771
    iget-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377772
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 377773
    :cond_1
    invoke-virtual {v3}, LX/2Aq;->g()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 377774
    invoke-virtual {v3}, LX/2Aq;->k()LX/2Am;

    invoke-virtual {p1, v1}, LX/4pt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    .line 377775
    :cond_2
    invoke-virtual {v3}, LX/2Aq;->f()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 377776
    invoke-virtual {v3}, LX/2Aq;->j()LX/2At;

    invoke-virtual {p1, v1}, LX/4pt;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    .line 377777
    :cond_3
    invoke-virtual {v3}, LX/2Aq;->h()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 377778
    invoke-virtual {p1, v1}, LX/4pt;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    .line 377779
    :cond_4
    invoke-virtual {v3}, LX/2Aq;->g()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 377780
    invoke-virtual {v3}, LX/2Aq;->k()LX/2Am;

    invoke-virtual {p1, v1}, LX/4pt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    .line 377781
    :cond_5
    invoke-virtual {v3}, LX/2Aq;->e()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 377782
    invoke-virtual {v3}, LX/2Aq;->i()LX/2At;

    invoke-virtual {p1, v1}, LX/4pt;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    .line 377783
    :cond_6
    invoke-virtual {v1, v3}, LX/2Ap;->a(LX/2Ap;)V

    goto :goto_2

    .line 377784
    :cond_7
    return-void

    :cond_8
    move-object v2, v1

    goto :goto_1
.end method

.method private a(Ljava/lang/Object;LX/2An;)V
    .locals 4

    .prologue
    .line 377862
    if-nez p1, :cond_1

    .line 377863
    :cond_0
    return-void

    .line 377864
    :cond_1
    iget-object v0, p0, LX/2Al;->n:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_2

    .line 377865
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/2Al;->n:Ljava/util/LinkedHashMap;

    .line 377866
    :cond_2
    iget-object v0, p0, LX/2Al;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2An;

    .line 377867
    if-eqz v0, :cond_0

    .line 377868
    if-nez p1, :cond_3

    const-string v0, "[null]"

    .line 377869
    :goto_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Duplicate injectable value with id \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' (of type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 377870
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 377871
    iget-boolean v0, p0, LX/2Al;->b:Z

    if-nez v0, :cond_1

    .line 377872
    iget-object v0, p0, LX/2Al;->m:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 377873
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2Al;->m:Ljava/util/HashSet;

    .line 377874
    :cond_0
    iget-object v0, p0, LX/2Al;->m:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 377875
    :cond_1
    return-void
.end method

.method private b(LX/2At;LX/0lU;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 377876
    if-nez p2, :cond_0

    move-object v0, v1

    .line 377877
    :goto_0
    if-nez v0, :cond_1

    .line 377878
    :goto_1
    if-nez v1, :cond_3

    .line 377879
    iget-object v0, p0, LX/2Al;->g:Ljava/lang/String;

    invoke-static {p1, v0}, LX/2Va;->c(LX/2At;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 377880
    if-nez v2, :cond_2

    .line 377881
    :goto_2
    return-void

    .line 377882
    :cond_0
    invoke-virtual {p2, p1}, LX/0lU;->v(LX/0lO;)LX/2Vb;

    move-result-object v0

    goto :goto_0

    .line 377883
    :cond_1
    iget-object v1, v0, LX/2Vb;->_simpleName:Ljava/lang/String;

    move-object v1, v1

    .line 377884
    goto :goto_1

    .line 377885
    :cond_2
    iget-object v0, p0, LX/2Al;->e:LX/0lW;

    invoke-interface {v0, p1}, LX/0lW;->c(LX/2At;)Z

    move-result v0

    move-object v3, v1

    move v1, v0

    .line 377886
    :goto_3
    if-nez p2, :cond_6

    const/4 v0, 0x0

    .line 377887
    :goto_4
    invoke-direct {p0, v2}, LX/2Al;->c(Ljava/lang/String;)LX/2Ap;

    move-result-object v2

    invoke-virtual {v2, p1, v3, v1, v0}, LX/2Ap;->b(LX/2At;Ljava/lang/String;ZZ)V

    goto :goto_2

    .line 377888
    :cond_3
    iget-object v0, p0, LX/2Al;->g:Ljava/lang/String;

    invoke-static {p1, v0}, LX/2Va;->c(LX/2At;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 377889
    if-nez v0, :cond_4

    .line 377890
    invoke-virtual {p1}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v0

    .line 377891
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    move-object v1, v0

    .line 377892
    :cond_5
    const/4 v2, 0x1

    move-object v3, v1

    move v1, v2

    move-object v2, v0

    goto :goto_3

    .line 377893
    :cond_6
    invoke-virtual {p2, p1}, LX/0lU;->c(LX/2An;)Z

    move-result v0

    goto :goto_4
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 377894
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Problem with definition of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c(Ljava/lang/String;)LX/2Ap;
    .locals 3

    .prologue
    .line 377895
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ap;

    .line 377896
    if-nez v0, :cond_0

    .line 377897
    new-instance v0, LX/2Ap;

    iget-object v1, p0, LX/2Al;->f:LX/0lU;

    iget-boolean v2, p0, LX/2Al;->b:Z

    invoke-direct {v0, p1, v1, v2}, LX/2Ap;-><init>(Ljava/lang/String;LX/0lU;Z)V

    .line 377898
    iget-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377899
    :cond_0
    return-object v0
.end method

.method private l()V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 377900
    iget-object v2, p0, LX/2Al;->f:LX/0lU;

    .line 377901
    if-nez v2, :cond_0

    move-object v1, v0

    .line 377902
    :goto_0
    if-nez v1, :cond_1

    .line 377903
    iget-object v1, p0, LX/2Al;->a:LX/0m4;

    invoke-virtual {v1}, LX/0m4;->i()Z

    move-result v1

    .line 377904
    :goto_1
    if-nez v2, :cond_2

    move-object v5, v0

    .line 377905
    :goto_2
    if-nez v1, :cond_3

    iget-object v0, p0, LX/2Al;->i:Ljava/util/LinkedList;

    if-nez v0, :cond_3

    if-nez v5, :cond_3

    .line 377906
    :goto_3
    return-void

    .line 377907
    :cond_0
    iget-object v1, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v2, v1}, LX/0lU;->h(LX/0lN;)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 377908
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_1

    .line 377909
    :cond_2
    iget-object v0, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v2, v0}, LX/0lU;->g(LX/0lN;)[Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    goto :goto_2

    .line 377910
    :cond_3
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    .line 377911
    if-eqz v1, :cond_4

    .line 377912
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    move-object v2, v0

    .line 377913
    :goto_4
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ap;

    .line 377914
    invoke-virtual {v0}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 377915
    :cond_4
    new-instance v0, Ljava/util/LinkedHashMap;

    add-int v1, v3, v3

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    move-object v2, v0

    goto :goto_4

    .line 377916
    :cond_5
    new-instance v6, Ljava/util/LinkedHashMap;

    add-int v0, v3, v3

    invoke-direct {v6, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 377917
    if-eqz v5, :cond_8

    .line 377918
    array-length v7, v5

    const/4 v0, 0x0

    move v4, v0

    :goto_6
    if-ge v4, v7, :cond_8

    aget-object v3, v5, v4

    .line 377919
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ap;

    .line 377920
    if-nez v0, :cond_a

    .line 377921
    iget-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Ap;

    .line 377922
    iget-object v9, v1, LX/2Ap;->d:Ljava/lang/String;

    move-object v9, v9

    .line 377923
    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 377924
    invoke-virtual {v1}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v0

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    .line 377925
    :goto_7
    if-eqz v0, :cond_7

    .line 377926
    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377927
    :cond_7
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_6

    .line 377928
    :cond_8
    iget-object v0, p0, LX/2Al;->i:Ljava/util/LinkedList;

    if-eqz v0, :cond_9

    .line 377929
    iget-object v0, p0, LX/2Al;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ap;

    .line 377930
    invoke-virtual {v0}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v6, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 377931
    :cond_9
    invoke-interface {v6, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 377932
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 377933
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v6}, Ljava/util/LinkedHashMap;->putAll(Ljava/util/Map;)V

    goto/16 :goto_3

    :cond_a
    move-object v1, v3

    goto :goto_7
.end method

.method private m()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 377934
    iget-object v9, p0, LX/2Al;->f:LX/0lU;

    .line 377935
    iget-boolean v0, p0, LX/2Al;->b:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/2Al;->a:LX/0m4;

    sget-object v1, LX/0m6;->ALLOW_FINAL_FIELDS_AS_MUTATORS:LX/0m6;

    invoke-virtual {v0, v1}, LX/0m4;->a(LX/0m6;)Z

    move-result v0

    if-nez v0, :cond_2

    move v1, v2

    .line 377936
    :goto_0
    iget-object v0, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->m()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Am;

    .line 377937
    invoke-virtual {v0}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v8

    .line 377938
    if-nez v9, :cond_3

    move-object v4, v5

    .line 377939
    :goto_2
    const-string v6, ""

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    move-object v7, v8

    .line 377940
    :goto_3
    if-eqz v7, :cond_7

    move v4, v2

    .line 377941
    :goto_4
    if-nez v4, :cond_a

    .line 377942
    iget-object v4, p0, LX/2Al;->e:LX/0lW;

    invoke-interface {v4, v0}, LX/0lW;->a(LX/2Am;)Z

    move-result v4

    move v6, v4

    .line 377943
    :goto_5
    if-eqz v9, :cond_8

    invoke-virtual {v9, v0}, LX/0lU;->c(LX/2An;)Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v2

    .line 377944
    :goto_6
    if-eqz v1, :cond_1

    if-nez v7, :cond_1

    if-nez v4, :cond_1

    invoke-virtual {v0}, LX/2Am;->g()I

    move-result v11

    invoke-static {v11}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v11

    if-nez v11, :cond_0

    .line 377945
    :cond_1
    invoke-direct {p0, v8}, LX/2Al;->c(Ljava/lang/String;)LX/2Ap;

    move-result-object v8

    invoke-virtual {v8, v0, v7, v6, v4}, LX/2Ap;->a(LX/2Am;Ljava/lang/String;ZZ)V

    goto :goto_1

    :cond_2
    move v1, v3

    .line 377946
    goto :goto_0

    .line 377947
    :cond_3
    iget-boolean v4, p0, LX/2Al;->b:Z

    if-eqz v4, :cond_5

    .line 377948
    invoke-virtual {v9, v0}, LX/0lU;->n(LX/0lO;)LX/2Vb;

    move-result-object v4

    .line 377949
    if-nez v4, :cond_4

    move-object v4, v5

    goto :goto_2

    .line 377950
    :cond_4
    iget-object v6, v4, LX/2Vb;->_simpleName:Ljava/lang/String;

    move-object v4, v6

    .line 377951
    goto :goto_2

    .line 377952
    :cond_5
    invoke-virtual {v9, v0}, LX/0lU;->v(LX/0lO;)LX/2Vb;

    move-result-object v4

    .line 377953
    if-nez v4, :cond_6

    move-object v4, v5

    goto :goto_2

    .line 377954
    :cond_6
    iget-object v6, v4, LX/2Vb;->_simpleName:Ljava/lang/String;

    move-object v4, v6

    .line 377955
    goto :goto_2

    :cond_7
    move v4, v3

    .line 377956
    goto :goto_4

    :cond_8
    move v4, v3

    .line 377957
    goto :goto_6

    .line 377958
    :cond_9
    return-void

    :cond_a
    move v6, v4

    goto :goto_5

    :cond_b
    move-object v7, v4

    goto :goto_3
.end method

.method private n()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 377785
    iget-object v5, p0, LX/2Al;->f:LX/0lU;

    .line 377786
    if-nez v5, :cond_1

    .line 377787
    :cond_0
    return-void

    .line 377788
    :cond_1
    iget-object v0, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vc;

    .line 377789
    iget-object v1, p0, LX/2Al;->i:Ljava/util/LinkedList;

    if-nez v1, :cond_3

    .line 377790
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, LX/2Al;->i:Ljava/util/LinkedList;

    .line 377791
    :cond_3
    invoke-virtual {v0}, LX/2Vc;->g()I

    move-result v7

    move v3, v4

    :goto_0
    if-ge v3, v7, :cond_2

    .line 377792
    invoke-virtual {v0, v3}, LX/2Au;->c(I)LX/2Vd;

    move-result-object v8

    .line 377793
    invoke-virtual {v5, v8}, LX/0lU;->v(LX/0lO;)LX/2Vb;

    move-result-object v1

    .line 377794
    if-nez v1, :cond_5

    move-object v1, v2

    .line 377795
    :goto_1
    if-eqz v1, :cond_4

    .line 377796
    invoke-direct {p0, v1}, LX/2Al;->c(Ljava/lang/String;)LX/2Ap;

    move-result-object v9

    .line 377797
    invoke-virtual {v9, v8, v1, v10, v4}, LX/2Ap;->a(LX/2Vd;Ljava/lang/String;ZZ)V

    .line 377798
    iget-object v1, p0, LX/2Al;->i:Ljava/util/LinkedList;

    invoke-virtual {v1, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 377799
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 377800
    :cond_5
    iget-object v9, v1, LX/2Vb;->_simpleName:Ljava/lang/String;

    move-object v1, v9

    .line 377801
    goto :goto_1

    .line 377802
    :cond_6
    iget-object v0, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->k()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    .line 377803
    iget-object v1, p0, LX/2Al;->i:Ljava/util/LinkedList;

    if-nez v1, :cond_8

    .line 377804
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, LX/2Al;->i:Ljava/util/LinkedList;

    .line 377805
    :cond_8
    invoke-virtual {v0}, LX/2At;->l()I

    move-result v7

    move v3, v4

    :goto_2
    if-ge v3, v7, :cond_7

    .line 377806
    invoke-virtual {v0, v3}, LX/2Au;->c(I)LX/2Vd;

    move-result-object v8

    .line 377807
    invoke-virtual {v5, v8}, LX/0lU;->v(LX/0lO;)LX/2Vb;

    move-result-object v1

    .line 377808
    if-nez v1, :cond_a

    move-object v1, v2

    .line 377809
    :goto_3
    if-eqz v1, :cond_9

    .line 377810
    invoke-direct {p0, v1}, LX/2Al;->c(Ljava/lang/String;)LX/2Ap;

    move-result-object v9

    .line 377811
    invoke-virtual {v9, v8, v1, v10, v4}, LX/2Ap;->a(LX/2Vd;Ljava/lang/String;ZZ)V

    .line 377812
    iget-object v1, p0, LX/2Al;->i:Ljava/util/LinkedList;

    invoke-virtual {v1, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 377813
    :cond_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 377814
    :cond_a
    iget-object v9, v1, LX/2Vb;->_simpleName:Ljava/lang/String;

    move-object v1, v9

    .line 377815
    goto :goto_3
.end method

.method private o()V
    .locals 5

    .prologue
    .line 377959
    iget-object v1, p0, LX/2Al;->f:LX/0lU;

    .line 377960
    iget-object v0, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->l()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    .line 377961
    invoke-virtual {v0}, LX/2At;->l()I

    move-result v3

    .line 377962
    if-nez v3, :cond_1

    .line 377963
    invoke-direct {p0, v0, v1}, LX/2Al;->a(LX/2At;LX/0lU;)V

    goto :goto_0

    .line 377964
    :cond_1
    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 377965
    invoke-direct {p0, v0, v1}, LX/2Al;->b(LX/2At;LX/0lU;)V

    goto :goto_0

    .line 377966
    :cond_2
    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 377967
    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, LX/0lU;->d(LX/2At;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 377968
    iget-object v3, p0, LX/2Al;->k:Ljava/util/LinkedList;

    if-nez v3, :cond_3

    .line 377969
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, p0, LX/2Al;->k:Ljava/util/LinkedList;

    .line 377970
    :cond_3
    iget-object v3, p0, LX/2Al;->k:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 377971
    :cond_4
    return-void
.end method

.method private p()V
    .locals 5

    .prologue
    .line 377640
    iget-object v1, p0, LX/2Al;->f:LX/0lU;

    .line 377641
    if-nez v1, :cond_1

    .line 377642
    :cond_0
    return-void

    .line 377643
    :cond_1
    iget-object v0, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->m()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Am;

    .line 377644
    invoke-virtual {v1, v0}, LX/0lU;->d(LX/2An;)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v3, v0}, LX/2Al;->a(Ljava/lang/Object;LX/2An;)V

    goto :goto_0

    .line 377645
    :cond_2
    iget-object v0, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v0}, LX/0lN;->l()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    .line 377646
    invoke-virtual {v0}, LX/2At;->l()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 377647
    invoke-virtual {v1, v0}, LX/0lU;->d(LX/2An;)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v3, v0}, LX/2Al;->a(Ljava/lang/Object;LX/2An;)V

    goto :goto_1
.end method

.method private q()V
    .locals 4

    .prologue
    .line 377648
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 377649
    iget-object v0, p0, LX/2Al;->a:LX/0m4;

    sget-object v1, LX/0m6;->INFER_PROPERTY_MUTATORS:LX/0m6;

    invoke-virtual {v0, v1}, LX/0m4;->a(LX/0m6;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 377650
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 377651
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 377652
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ap;

    .line 377653
    invoke-virtual {v0}, LX/2Ap;->w()Z

    move-result v3

    if-nez v3, :cond_1

    .line 377654
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 377655
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 377656
    :cond_1
    invoke-virtual {v0}, LX/2Ap;->x()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 377657
    invoke-virtual {v0}, LX/2Aq;->c()Z

    move-result v3

    if-nez v3, :cond_2

    .line 377658
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 377659
    invoke-virtual {v0}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Al;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 377660
    :cond_2
    invoke-virtual {v0}, LX/2Ap;->u()V

    .line 377661
    iget-boolean v3, p0, LX/2Al;->b:Z

    if-nez v3, :cond_3

    invoke-virtual {v0}, LX/2Aq;->d()Z

    move-result v3

    if-nez v3, :cond_3

    .line 377662
    invoke-virtual {v0}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, LX/2Al;->a(Ljava/lang/String;)V

    .line 377663
    :cond_3
    invoke-virtual {v0, v1}, LX/2Ap;->a(Z)V

    goto :goto_0

    .line 377664
    :cond_4
    return-void
.end method

.method private r()V
    .locals 5

    .prologue
    .line 377665
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 377666
    const/4 v1, 0x0

    .line 377667
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377668
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 377669
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ap;

    .line 377670
    invoke-virtual {v0}, LX/2Ap;->y()Ljava/lang/String;

    move-result-object v3

    .line 377671
    if-eqz v3, :cond_0

    .line 377672
    if-nez v1, :cond_1

    .line 377673
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 377674
    :cond_1
    invoke-virtual {v0, v3}, LX/2Ap;->a(Ljava/lang/String;)LX/2Ap;

    move-result-object v0

    .line 377675
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 377676
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 377677
    :cond_2
    if-eqz v1, :cond_6

    .line 377678
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ap;

    .line 377679
    invoke-virtual {v0}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v2

    .line 377680
    iget-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Ap;

    .line 377681
    if-nez v1, :cond_4

    .line 377682
    iget-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377683
    :goto_2
    iget-object v1, p0, LX/2Al;->i:Ljava/util/LinkedList;

    if-eqz v1, :cond_3

    .line 377684
    const/4 v1, 0x0

    move v2, v1

    :goto_3
    iget-object v1, p0, LX/2Al;->i:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 377685
    iget-object v1, p0, LX/2Al;->i:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Ap;

    .line 377686
    iget-object v4, v1, LX/2Ap;->d:Ljava/lang/String;

    move-object v1, v4

    .line 377687
    iget-object v4, v0, LX/2Ap;->d:Ljava/lang/String;

    move-object v4, v4

    .line 377688
    if-ne v1, v4, :cond_5

    .line 377689
    iget-object v1, p0, LX/2Al;->i:Ljava/util/LinkedList;

    invoke-virtual {v1, v2, v0}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 377690
    :cond_4
    invoke-virtual {v1, v0}, LX/2Ap;->a(LX/2Ap;)V

    goto :goto_2

    .line 377691
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 377692
    :cond_6
    return-void
.end method

.method private s()V
    .locals 5

    .prologue
    .line 377693
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 377694
    const/4 v1, 0x0

    .line 377695
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377696
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 377697
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ap;

    .line 377698
    invoke-virtual {v0}, LX/2Aq;->o()LX/2An;

    move-result-object v3

    .line 377699
    if-eqz v3, :cond_0

    .line 377700
    goto :goto_0

    .line 377701
    :cond_1
    if-eqz v1, :cond_3

    .line 377702
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ap;

    .line 377703
    invoke-virtual {v0}, LX/2Aq;->a()Ljava/lang/String;

    move-result-object v3

    .line 377704
    iget-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Ap;

    .line 377705
    if-nez v1, :cond_2

    .line 377706
    iget-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 377707
    :cond_2
    invoke-virtual {v1, v0}, LX/2Ap;->a(LX/2Ap;)V

    goto :goto_1

    .line 377708
    :cond_3
    return-void
.end method

.method private t()LX/4pt;
    .locals 4

    .prologue
    .line 377709
    iget-object v0, p0, LX/2Al;->f:LX/0lU;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 377710
    :goto_0
    if-nez v0, :cond_1

    .line 377711
    iget-object v0, p0, LX/2Al;->a:LX/0m4;

    invoke-virtual {v0}, LX/0m4;->k()LX/4pt;

    move-result-object v0

    .line 377712
    :goto_1
    return-object v0

    .line 377713
    :cond_0
    iget-object v0, p0, LX/2Al;->f:LX/0lU;

    iget-object v1, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v0, v1}, LX/0lU;->e(LX/0lN;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 377714
    :cond_1
    instance-of v1, v0, LX/4pt;

    if-eqz v1, :cond_2

    .line 377715
    check-cast v0, LX/4pt;

    goto :goto_1

    .line 377716
    :cond_2
    instance-of v1, v0, Ljava/lang/Class;

    if-nez v1, :cond_3

    .line 377717
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AnnotationIntrospector returned PropertyNamingStrategy definition of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; expected type PropertyNamingStrategy or Class<PropertyNamingStrategy> instead"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 377718
    :cond_3
    check-cast v0, Ljava/lang/Class;

    .line 377719
    const-class v1, LX/4pt;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 377720
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AnnotationIntrospector returned Class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; expected Class<PropertyNamingStrategy>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 377721
    :cond_4
    iget-object v1, p0, LX/2Al;->a:LX/0m4;

    invoke-virtual {v1}, LX/0m4;->l()LX/4py;

    move-result-object v1

    .line 377722
    if-eqz v1, :cond_5

    .line 377723
    goto :goto_2

    .line 377724
    :cond_5
    :goto_2
    iget-object v1, p0, LX/2Al;->a:LX/0m4;

    invoke-virtual {v1}, LX/0m4;->h()Z

    move-result v1

    invoke-static {v0, v1}, LX/1Xw;->b(Ljava/lang/Class;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4pt;

    goto :goto_1
.end method


# virtual methods
.method public final d()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2Aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377725
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final f()LX/2At;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 377726
    iget-object v0, p0, LX/2Al;->l:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    .line 377727
    iget-object v0, p0, LX/2Al;->l:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 377728
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Multiple value properties defined ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2Al;->l:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " vs "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2Al;->l:Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Al;->b(Ljava/lang/String;)V

    .line 377729
    :cond_0
    iget-object v0, p0, LX/2Al;->l:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    .line 377730
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/2An;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 377731
    iget-object v0, p0, LX/2Al;->j:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    .line 377732
    iget-object v0, p0, LX/2Al;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 377733
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Multiple \'any-getters\' defined ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2Al;->j:Ljava/util/LinkedList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " vs "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2Al;->j:Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Al;->b(Ljava/lang/String;)V

    .line 377734
    :cond_0
    iget-object v0, p0, LX/2Al;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2An;

    .line 377735
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()LX/2At;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 377635
    iget-object v0, p0, LX/2Al;->k:Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    .line 377636
    iget-object v0, p0, LX/2Al;->k:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 377637
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Multiple \'any-setters\' defined ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2Al;->k:Ljava/util/LinkedList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " vs "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2Al;->k:Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Al;->b(Ljava/lang/String;)V

    .line 377638
    :cond_0
    iget-object v0, p0, LX/2Al;->k:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2At;

    .line 377639
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()LX/4qt;
    .locals 3

    .prologue
    .line 377736
    iget-object v0, p0, LX/2Al;->f:LX/0lU;

    if-nez v0, :cond_1

    .line 377737
    const/4 v0, 0x0

    .line 377738
    :cond_0
    :goto_0
    return-object v0

    .line 377739
    :cond_1
    iget-object v0, p0, LX/2Al;->f:LX/0lU;

    iget-object v1, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v0, v1}, LX/0lU;->a(LX/0lO;)LX/4qt;

    move-result-object v0

    .line 377740
    if-eqz v0, :cond_0

    .line 377741
    iget-object v1, p0, LX/2Al;->f:LX/0lU;

    iget-object v2, p0, LX/2Al;->d:LX/0lN;

    invoke-virtual {v1, v2, v0}, LX/0lU;->a(LX/0lO;LX/4qt;)LX/4qt;

    move-result-object v0

    goto :goto_0
.end method

.method public final k()LX/2Al;
    .locals 3

    .prologue
    .line 377742
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 377743
    invoke-direct {p0}, LX/2Al;->m()V

    .line 377744
    invoke-direct {p0}, LX/2Al;->o()V

    .line 377745
    invoke-direct {p0}, LX/2Al;->n()V

    .line 377746
    invoke-direct {p0}, LX/2Al;->p()V

    .line 377747
    invoke-direct {p0}, LX/2Al;->q()V

    .line 377748
    invoke-direct {p0}, LX/2Al;->r()V

    .line 377749
    invoke-direct {p0}, LX/2Al;->t()LX/4pt;

    move-result-object v0

    .line 377750
    if-eqz v0, :cond_0

    .line 377751
    invoke-direct {p0, v0}, LX/2Al;->a(LX/4pt;)V

    .line 377752
    :cond_0
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ap;

    .line 377753
    invoke-virtual {v0}, LX/2Ap;->v()V

    goto :goto_0

    .line 377754
    :cond_1
    iget-object v0, p0, LX/2Al;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ap;

    .line 377755
    iget-boolean v2, p0, LX/2Al;->b:Z

    invoke-virtual {v0, v2}, LX/2Ap;->b(Z)V

    goto :goto_1

    .line 377756
    :cond_2
    iget-object v0, p0, LX/2Al;->a:LX/0m4;

    sget-object v1, LX/0m6;->USE_WRAPPER_NAME_AS_PROPERTY_NAME:LX/0m6;

    invoke-virtual {v0, v1}, LX/0m4;->a(LX/0m6;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 377757
    invoke-direct {p0}, LX/2Al;->s()V

    .line 377758
    :cond_3
    invoke-direct {p0}, LX/2Al;->l()V

    .line 377759
    return-object p0
.end method
