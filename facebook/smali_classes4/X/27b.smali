.class public final LX/27b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;)V
    .locals 0

    .prologue
    .line 372959
    iput-object p1, p0, LX/27b;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 372960
    const/4 v0, 0x6

    if-ne p2, v0, :cond_1

    .line 372961
    iget-object v0, p0, LX/27b;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bx:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 372962
    iget-object v0, p0, LX/27b;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-boolean v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    if-eqz v0, :cond_0

    .line 372963
    iget-object v0, p0, LX/27b;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    .line 372964
    iput-boolean v2, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bf:Z

    .line 372965
    :cond_0
    iget-object v0, p0, LX/27b;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bl:Landroid/widget/EditText;

    if-ne p1, v0, :cond_2

    .line 372966
    iget-object v0, p0, LX/27b;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    sget-object v1, LX/2NV;->LOGIN_APPROVALS_CODE_ENTRY:LX/2NV;

    .line 372967
    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NV;)V

    .line 372968
    :cond_1
    :goto_0
    return v2

    .line 372969
    :cond_2
    iget-object v0, p0, LX/27b;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    iget-object v0, v0, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->bk:Landroid/widget/EditText;

    if-ne p1, v0, :cond_1

    .line 372970
    iget-object v0, p0, LX/27b;->a:Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;

    sget-object v1, LX/2NV;->PASSWORD_ENTRY:LX/2NV;

    .line 372971
    invoke-static {v0, v1}, Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;->a$redex0(Lcom/facebook/katana/dbl/activity/FacebookLoginActivity;LX/2NV;)V

    .line 372972
    goto :goto_0
.end method
