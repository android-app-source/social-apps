.class public LX/2U8;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0SG;

.field public final d:LX/0lC;

.field public final e:Landroid/os/Handler;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qI;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2UE;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Uh;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 415347
    const-class v0, LX/2U8;

    sput-object v0, LX/2U8;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0lC;Landroid/os/Handler;LX/0Ot;LX/0Ot;LX/0Or;LX/0Uh;LX/0Ot;)V
    .locals 0
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            "LX/0lC;",
            "Landroid/os/Handler;",
            "LX/0Ot",
            "<",
            "LX/1qI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2UE;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/0lB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415337
    iput-object p1, p0, LX/2U8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 415338
    iput-object p2, p0, LX/2U8;->c:LX/0SG;

    .line 415339
    iput-object p3, p0, LX/2U8;->d:LX/0lC;

    .line 415340
    iput-object p4, p0, LX/2U8;->e:Landroid/os/Handler;

    .line 415341
    iput-object p5, p0, LX/2U8;->f:LX/0Ot;

    .line 415342
    iput-object p6, p0, LX/2U8;->g:LX/0Ot;

    .line 415343
    iput-object p7, p0, LX/2U8;->h:LX/0Or;

    .line 415344
    iput-object p8, p0, LX/2U8;->i:LX/0Uh;

    .line 415345
    iput-object p9, p0, LX/2U8;->j:LX/0Ot;

    .line 415346
    return-void
.end method

.method public static a(LX/3rL;Lcom/facebook/fbservice/service/ServiceException;)LX/3rL;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/fbservice/service/ServiceException;",
            ")",
            "LX/3rL",
            "<",
            "Ljava/lang/String;",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415320
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 415321
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 415322
    :cond_0
    sget-object v1, LX/03R;->UNSET:LX/03R;

    .line 415323
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 415324
    if-eqz v0, :cond_1

    .line 415325
    iget-object v2, v0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v2, v2

    .line 415326
    if-eqz v2, :cond_1

    .line 415327
    iget-object v2, v0, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v0, v2

    .line 415328
    invoke-virtual {v0}, LX/1nY;->name()Ljava/lang/String;

    move-result-object v0

    .line 415329
    :goto_0
    new-instance v2, LX/3rL;

    invoke-direct {v2, v0, v1}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 415330
    :cond_1
    const-string v0, "Unknown"

    goto :goto_0

    .line 415331
    :cond_2
    iget-object v0, p0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, 0x170

    if-ne v0, v2, :cond_3

    .line 415332
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Sentry ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "368"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 415333
    :cond_3
    iget-object v0, p0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, 0xce5

    if-ne v0, v2, :cond_4

    .line 415334
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Wrong code ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "3301"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 415335
    :cond_4
    iget-object v0, p0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private a(LX/2UD;Ljava/lang/String;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2UD;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/growth/model/Contactpoint;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415303
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 415304
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415305
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 415306
    :goto_0
    return-object v0

    .line 415307
    :cond_0
    iget-object v0, p0, LX/2U8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1, p2}, LX/2U8;->b(LX/2UD;Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 415308
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 415309
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0

    .line 415310
    :cond_1
    :try_start_0
    iget-object v2, p0, LX/2U8;->d:LX/0lC;

    .line 415311
    iget-object v3, v2, LX/0lC;->_typeFactory:LX/0li;

    move-object v2, v3

    .line 415312
    const-class v3, Ljava/util/List;

    const-class v4, Lcom/facebook/confirmation/task/PendingContactpoint;

    invoke-virtual {v2, v3, v4}, LX/0li;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/267;

    move-result-object v2

    .line 415313
    iget-object v3, p0, LX/2U8;->d:LX/0lC;

    invoke-virtual {v3, v0, v2}, LX/0lC;->a(Ljava/lang/String;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 415314
    if-eqz v0, :cond_2

    .line 415315
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/confirmation/task/PendingContactpoint;

    .line 415316
    iget-object v3, v0, Lcom/facebook/confirmation/task/PendingContactpoint;->pendingContactpoint:Lcom/facebook/growth/model/Contactpoint;

    iget-wide v4, v0, Lcom/facebook/confirmation/task/PendingContactpoint;->timestamp:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 415317
    :catch_0
    move-exception v0

    .line 415318
    sget-object v2, LX/2U8;->a:Ljava/lang/Class;

    const-string v3, "Error with parsing pending contactpoints data"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 415319
    :cond_2
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/2U8;LX/0Tn;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tn;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415292
    iget-object v0, p0, LX/2U8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 415293
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 415294
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 415295
    :goto_0
    return-object v0

    .line 415296
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/2U8;->d:LX/0lC;

    .line 415297
    iget-object v2, v1, LX/0lC;->_typeFactory:LX/0li;

    move-object v1, v2

    .line 415298
    const-class v2, Ljava/util/Set;

    const-class v3, Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0li;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/267;

    move-result-object v1

    .line 415299
    iget-object v2, p0, LX/2U8;->d:LX/0lC;

    invoke-virtual {v2, v0, v1}, LX/0lC;->a(Ljava/lang/String;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 415300
    :catch_0
    move-exception v0

    .line 415301
    sget-object v1, LX/2U8;->a:Ljava/lang/Class;

    const-string v2, "Error with parsing pending contactpoints data"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 415302
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    goto :goto_0
.end method

.method private a(LX/0Tn;Ljava/util/Set;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tn;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 415288
    :try_start_0
    iget-object v0, p0, LX/2U8;->d:LX/0lC;

    invoke-virtual {v0, p2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 415289
    iget-object v1, p0, LX/2U8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, p1, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 415290
    const/4 v0, 0x1

    .line 415291
    :goto_0
    return v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/2UD;Ljava/util/Map;Ljava/lang/String;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2UD;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/growth/model/Contactpoint;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 415279
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 415280
    :goto_0
    return v0

    .line 415281
    :cond_0
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 415282
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 415283
    new-instance v5, Lcom/facebook/confirmation/task/PendingContactpoint;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/growth/model/Contactpoint;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v5, v1, v6, v7}, Lcom/facebook/confirmation/task/PendingContactpoint;-><init>(Lcom/facebook/growth/model/Contactpoint;J)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 415284
    :catch_0
    move v0, v2

    goto :goto_0

    .line 415285
    :cond_1
    iget-object v0, p0, LX/2U8;->d:LX/0lC;

    invoke-virtual {v0, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 415286
    iget-object v1, p0, LX/2U8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-static {p1, p3}, LX/2U8;->b(LX/2UD;Ljava/lang/String;)LX/0Tn;

    move-result-object v3

    invoke-interface {v1, v3, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 415287
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/2UD;Ljava/lang/String;)LX/0Tn;
    .locals 3

    .prologue
    .line 415278
    sget-object v0, LX/3df;->f:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LX/2UD;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2U8;
    .locals 10

    .prologue
    .line 415276
    new-instance v0, LX/2U8;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    const/16 v5, 0x1bc

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3ee

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x15e7

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    const/16 v9, 0x2ba

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, LX/2U8;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0lC;Landroid/os/Handler;LX/0Ot;LX/0Ot;LX/0Or;LX/0Uh;LX/0Ot;)V

    .line 415277
    return-object v0
.end method

.method public static b(Lcom/facebook/fbservice/service/ServiceException;)Lcom/facebook/http/protocol/ApiErrorResult;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 415207
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 415208
    if-nez v0, :cond_0

    move-object v0, v1

    .line 415209
    :goto_0
    return-object v0

    .line 415210
    :cond_0
    iget-object v2, v0, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v0, v2

    .line 415211
    if-nez v0, :cond_1

    move-object v0, v1

    .line 415212
    goto :goto_0

    .line 415213
    :cond_1
    const-string v2, "result"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 415214
    instance-of v2, v0, Lcom/facebook/http/protocol/ApiErrorResult;

    if-nez v2, :cond_2

    move-object v0, v1

    .line 415215
    goto :goto_0

    .line 415216
    :cond_2
    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    goto :goto_0
.end method

.method private b(LX/2UD;Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 415264
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/growth/model/Contactpoint;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 415265
    :cond_0
    const/4 v0, 0x0

    .line 415266
    :cond_1
    :goto_0
    return v0

    .line 415267
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {p0, p1, p3}, LX/2U8;->a(LX/2UD;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 415268
    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 415269
    iget-object v1, p0, LX/2U8;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415270
    :cond_3
    invoke-direct {p0, p1, v0, p3}, LX/2U8;->a(LX/2UD;Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    .line 415271
    const-wide/16 v2, 0x0

    .line 415272
    sget-object v1, LX/2UD;->REGULAR_SMS_CONFIRMATION:LX/2UD;

    if-ne p1, v1, :cond_4

    .line 415273
    const-wide/16 v2, 0x2710

    .line 415274
    :cond_4
    if-eqz v0, :cond_1

    .line 415275
    iget-object v1, p0, LX/2U8;->e:Landroid/os/Handler;

    new-instance v4, Lcom/facebook/confirmation/task/BackgroundConfirmationHelper$3;

    invoke-direct {v4, p0}, Lcom/facebook/confirmation/task/BackgroundConfirmationHelper$3;-><init>(LX/2U8;)V

    const v5, -0x73efa391

    invoke-static {v1, v4, v2, v3, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/ServiceException;)LX/3rL;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/fbservice/service/ServiceException;",
            ")",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 415245
    invoke-static {p1}, LX/2U8;->b(Lcom/facebook/fbservice/service/ServiceException;)Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    .line 415246
    if-nez v1, :cond_0

    .line 415247
    const/4 v0, 0x0

    .line 415248
    :goto_0
    return-object v0

    .line 415249
    :cond_0
    new-instance v0, LX/Ej6;

    invoke-direct {v0, p0}, LX/Ej6;-><init>(LX/2U8;)V

    const/4 v3, 0x0

    .line 415250
    invoke-static {p1}, LX/2U8;->b(Lcom/facebook/fbservice/service/ServiceException;)Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v2

    .line 415251
    if-nez v2, :cond_2

    move-object v2, v3

    .line 415252
    :goto_1
    move-object v0, v2

    .line 415253
    check-cast v0, Ljava/util/Map;

    .line 415254
    if-eqz v0, :cond_1

    const-string v2, "error_message"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 415255
    const-string v2, "error_message"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 415256
    :goto_2
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    .line 415257
    new-instance v1, LX/3rL;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    .line 415258
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 415259
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object v4

    .line 415260
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v2, v3

    .line 415261
    goto :goto_1

    .line 415262
    :cond_3
    :try_start_0
    iget-object v2, p0, LX/2U8;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lB;

    invoke-virtual {v2, v4, v0}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_1

    .line 415263
    :catch_0
    move-object v2, v3

    goto :goto_1
.end method

.method public final a(LX/2UD;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2UD;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/growth/model/Contactpoint;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415244
    iget-object v0, p0, LX/2U8;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, v0}, LX/2U8;->a(LX/2UD;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 415242
    iget-object v0, p0, LX/2U8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3df;->d:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 415243
    return-void
.end method

.method public final a(LX/2UD;Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 415241
    invoke-direct {p0, p1, p2, p3}, LX/2U8;->b(LX/2UD;Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final varargs a(LX/2UD;[Lcom/facebook/growth/model/Contactpoint;)Z
    .locals 4

    .prologue
    .line 415236
    new-instance v1, Ljava/util/HashMap;

    invoke-virtual {p0, p1}, LX/2U8;->a(LX/2UD;)Ljava/util/Map;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 415237
    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p2, v0

    .line 415238
    invoke-interface {v1, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 415239
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 415240
    :cond_0
    iget-object v0, p0, LX/2U8;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, v1, v0}, LX/2U8;->a(LX/2UD;Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/confirmation/util/SmsReaderExperimental$SmsReaderPointer;)Z
    .locals 3

    .prologue
    .line 415232
    :try_start_0
    iget-object v0, p0, LX/2U8;->d:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 415233
    iget-object v1, p0, LX/2U8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/3df;->e:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 415234
    const/4 v0, 0x1

    .line 415235
    :goto_0
    return v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/growth/model/Contactpoint;)Z
    .locals 3

    .prologue
    .line 415220
    const/4 v0, 0x0

    .line 415221
    iget-object v1, p1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    sget-object v2, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v1, v2, :cond_1

    .line 415222
    iget-object v0, p0, LX/2U8;->i:LX/0Uh;

    const/16 v1, 0x389

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 415223
    if-eqz v0, :cond_0

    .line 415224
    sget-object v0, LX/2UD;->EXPERIMENTAL_SMS_CONFIRMATION:LX/2UD;

    move-object v1, v0

    .line 415225
    :goto_0
    if-eqz v1, :cond_2

    .line 415226
    iget-object v0, p0, LX/2U8;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, p1, v0}, LX/2U8;->b(LX/2UD;Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;)Z

    move-result v0

    .line 415227
    :goto_1
    return v0

    .line 415228
    :cond_0
    sget-object v0, LX/2UD;->REGULAR_SMS_CONFIRMATION:LX/2UD;

    move-object v1, v0

    goto :goto_0

    .line 415229
    :cond_1
    iget-object v1, p1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    sget-object v2, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    if-ne v1, v2, :cond_3

    .line 415230
    sget-object v0, LX/2UD;->OPENID_CONNECT_EMAIL_CONFIRMATION:LX/2UD;

    move-object v1, v0

    goto :goto_0

    .line 415231
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 415219
    sget-object v0, LX/3df;->b:LX/0Tn;

    invoke-direct {p0, v0, p1}, LX/2U8;->a(LX/0Tn;Ljava/util/Set;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/util/Set;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 415218
    sget-object v0, LX/3df;->c:LX/0Tn;

    invoke-direct {p0, v0, p1}, LX/2U8;->a(LX/0Tn;Ljava/util/Set;)Z

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 3

    .prologue
    .line 415217
    iget-object v0, p0, LX/2U8;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3df;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method
