.class public LX/2A8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/net/Uri;

.field public final c:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/ipc/notifications/GraphQLNotificationsAuthority;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 376970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376971
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/2A8;->a:Ljava/lang/String;

    .line 376972
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2A8;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/gql_notifications"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/2A8;->b:Landroid/net/Uri;

    .line 376973
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/2A8;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/clear_all_data"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/2A8;->c:Landroid/net/Uri;

    .line 376974
    return-void
.end method

.method public static a(LX/0QB;)LX/2A8;
    .locals 1

    .prologue
    .line 376975
    invoke-static {p0}, LX/2A8;->b(LX/0QB;)LX/2A8;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2A8;
    .locals 2

    .prologue
    .line 376976
    new-instance v1, LX/2A8;

    .line 376977
    sget-object v0, LX/2AA;->a:Ljava/lang/String;

    move-object v0, v0

    .line 376978
    move-object v0, v0

    .line 376979
    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, LX/2A8;-><init>(Ljava/lang/String;)V

    .line 376980
    return-object v1
.end method
