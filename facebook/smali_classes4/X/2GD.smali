.class public LX/2GD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile g:LX/2GD;


# instance fields
.field private final b:LX/03V;

.field private final c:LX/1rc;

.field private final d:Landroid/net/wifi/WifiManager;

.field private final e:LX/0So;

.field private final f:LX/2Cg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 387967
    const-class v0, LX/2GD;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2GD;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/1rc;Landroid/net/wifi/WifiManager;LX/0So;LX/2Cg;)V
    .locals 0
    .param p4    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387969
    iput-object p1, p0, LX/2GD;->b:LX/03V;

    .line 387970
    iput-object p2, p0, LX/2GD;->c:LX/1rc;

    .line 387971
    iput-object p3, p0, LX/2GD;->d:Landroid/net/wifi/WifiManager;

    .line 387972
    iput-object p4, p0, LX/2GD;->e:LX/0So;

    .line 387973
    iput-object p5, p0, LX/2GD;->f:LX/2Cg;

    .line 387974
    return-void
.end method

.method public static a(LX/0QB;)LX/2GD;
    .locals 9

    .prologue
    .line 387975
    sget-object v0, LX/2GD;->g:LX/2GD;

    if-nez v0, :cond_1

    .line 387976
    const-class v1, LX/2GD;

    monitor-enter v1

    .line 387977
    :try_start_0
    sget-object v0, LX/2GD;->g:LX/2GD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387978
    if-eqz v2, :cond_0

    .line 387979
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 387980
    new-instance v3, LX/2GD;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/1rc;->a(LX/0QB;)LX/1rc;

    move-result-object v5

    check-cast v5, LX/1rc;

    invoke-static {v0}, LX/0kt;->b(LX/0QB;)Landroid/net/wifi/WifiManager;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiManager;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v0}, LX/2Cg;->a(LX/0QB;)LX/2Cg;

    move-result-object v8

    check-cast v8, LX/2Cg;

    invoke-direct/range {v3 .. v8}, LX/2GD;-><init>(LX/03V;LX/1rc;Landroid/net/wifi/WifiManager;LX/0So;LX/2Cg;)V

    .line 387981
    move-object v0, v3

    .line 387982
    sput-object v0, LX/2GD;->g:LX/2GD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387983
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387984
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387985
    :cond_1
    sget-object v0, LX/2GD;->g:LX/2GD;

    return-object v0

    .line 387986
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387987
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(J)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 387988
    invoke-static {}, LX/1rc;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 387989
    :cond_0
    :goto_0
    return-object v0

    .line 387990
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/2GD;->d:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 387991
    iget-object v2, p0, LX/2GD;->f:LX/2Cg;

    invoke-virtual {v2, v1}, LX/2Cg;->a(Ljava/util/List;)V

    .line 387992
    iget-object v2, p0, LX/2GD;->e:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v1, p1, p2, v2, v3}, LX/2zX;->a(Ljava/util/List;JJ)Ljava/util/List;

    move-result-object v1

    .line 387993
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    goto :goto_0

    .line 387994
    :catch_0
    move-exception v1

    .line 387995
    iget-object v2, p0, LX/2GD;->b:LX/03V;

    sget-object v3, LX/2GD;->a:Ljava/lang/String;

    const-string v4, "No permission to access wifi scan results"

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
