.class public final enum LX/2o6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2o6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2o6;

.field public static final enum DATASAVINGS:LX/2o6;

.field public static final enum DATASAVINGS_INACTIVE:LX/2o6;

.field public static final enum DIALTONE:LX/2o6;

.field public static final enum ZERO_RATING:LX/2o6;


# instance fields
.field private final mCode:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 465650
    new-instance v0, LX/2o6;

    const-string v1, "DIALTONE"

    const-string v2, "DT"

    invoke-direct {v0, v1, v3, v2}, LX/2o6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2o6;->DIALTONE:LX/2o6;

    .line 465651
    new-instance v0, LX/2o6;

    const-string v1, "ZERO_RATING"

    const-string v2, "ZR"

    invoke-direct {v0, v1, v4, v2}, LX/2o6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2o6;->ZERO_RATING:LX/2o6;

    .line 465652
    new-instance v0, LX/2o6;

    const-string v1, "DATASAVINGS"

    const-string v2, "DS"

    invoke-direct {v0, v1, v5, v2}, LX/2o6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2o6;->DATASAVINGS:LX/2o6;

    .line 465653
    new-instance v0, LX/2o6;

    const-string v1, "DATASAVINGS_INACTIVE"

    const-string v2, "DI"

    invoke-direct {v0, v1, v6, v2}, LX/2o6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2o6;->DATASAVINGS_INACTIVE:LX/2o6;

    .line 465654
    const/4 v0, 0x4

    new-array v0, v0, [LX/2o6;

    sget-object v1, LX/2o6;->DIALTONE:LX/2o6;

    aput-object v1, v0, v3

    sget-object v1, LX/2o6;->ZERO_RATING:LX/2o6;

    aput-object v1, v0, v4

    sget-object v1, LX/2o6;->DATASAVINGS:LX/2o6;

    aput-object v1, v0, v5

    sget-object v1, LX/2o6;->DATASAVINGS_INACTIVE:LX/2o6;

    aput-object v1, v0, v6

    sput-object v0, LX/2o6;->$VALUES:[LX/2o6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 465655
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 465656
    iput-object p3, p0, LX/2o6;->mCode:Ljava/lang/String;

    .line 465657
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2o6;
    .locals 1

    .prologue
    .line 465658
    const-class v0, LX/2o6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2o6;

    return-object v0
.end method

.method public static values()[LX/2o6;
    .locals 1

    .prologue
    .line 465659
    sget-object v0, LX/2o6;->$VALUES:[LX/2o6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2o6;

    return-object v0
.end method


# virtual methods
.method public final getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465660
    iget-object v0, p0, LX/2o6;->mCode:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465661
    iget-object v0, p0, LX/2o6;->mCode:Ljava/lang/String;

    return-object v0
.end method
