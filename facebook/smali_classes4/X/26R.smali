.class public LX/26R;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/37H;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/37H;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 372175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372176
    iput-object p1, p0, LX/26R;->a:LX/0Ot;

    .line 372177
    iput-object p2, p0, LX/26R;->b:LX/0ad;

    .line 372178
    return-void
.end method

.method public static a(LX/0QB;)LX/26R;
    .locals 5

    .prologue
    .line 372179
    const-class v1, LX/26R;

    monitor-enter v1

    .line 372180
    :try_start_0
    sget-object v0, LX/26R;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 372181
    sput-object v2, LX/26R;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 372182
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372183
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 372184
    new-instance v4, LX/26R;

    const/16 v3, 0x5e6

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, p0, v3}, LX/26R;-><init>(LX/0Ot;LX/0ad;)V

    .line 372185
    move-object v0, v4

    .line 372186
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 372187
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/26R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 372188
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 372189
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(III)Z
    .locals 1

    .prologue
    .line 372190
    add-int/lit8 v0, p0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x2

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;IILX/26N;ILX/1aZ;Landroid/graphics/PointF;IILjava/lang/String;)LX/1Dg;
    .locals 13
    .param p4    # LX/26N;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1aZ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Landroid/graphics/PointF;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 372191
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move/from16 v0, p8

    move/from16 v1, p9

    move/from16 v2, p5

    move-object/from16 v3, p10

    invoke-static {v4, v0, v1, v2, v3}, LX/26Q;->a(Landroid/content/res/Resources;IIILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 372192
    const/4 v4, 0x0

    .line 372193
    iget-object v5, p0, LX/26R;->b:LX/0ad;

    sget-short v6, LX/0fe;->aO:S

    const/4 v8, 0x0

    invoke-interface {v5, v6, v8}, LX/0ad;->a(SZ)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 372194
    new-instance v4, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f021af6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    const/16 v6, 0x3e8

    invoke-direct {v4, v5, v6}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 372195
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    .line 372196
    :cond_0
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v5

    move-object/from16 v0, p6

    invoke-virtual {v5, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v5

    move-object/from16 v0, p7

    invoke-virtual {v5, v0}, LX/1up;->b(Landroid/graphics/PointF;)LX/1up;

    move-result-object v5

    const v6, 0x7f0a045d

    invoke-virtual {v5, v6}, LX/1up;->h(I)LX/1up;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1up;->c(LX/1dc;)LX/1up;

    move-result-object v4

    const v5, 0x7f020aa6

    invoke-virtual {v4, v5}, LX/1up;->j(I)LX/1up;

    move-result-object v8

    .line 372197
    invoke-static {p1}, LX/367;->d(LX/1De;)LX/1dQ;

    move-result-object v9

    .line 372198
    move/from16 v0, p8

    move/from16 v1, p9

    move/from16 v2, p5

    invoke-static {v0, v1, v2}, LX/26R;->a(III)Z

    move-result v10

    .line 372199
    invoke-interface/range {p4 .. p4}, LX/26N;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v10, :cond_1

    const/4 v4, 0x1

    move v5, v4

    .line 372200
    :goto_0
    invoke-interface/range {p4 .. p4}, LX/26N;->b()Z

    move-result v4

    if-eqz v4, :cond_2

    if-nez v10, :cond_2

    const/4 v4, 0x1

    move v6, v4

    .line 372201
    :goto_1
    if-nez v10, :cond_4

    if-nez v5, :cond_4

    if-nez v6, :cond_4

    .line 372202
    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v5

    if-nez v9, :cond_3

    const/4 v4, 0x0

    :goto_2
    invoke-interface {v5, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    .line 372203
    :goto_3
    return-object v4

    .line 372204
    :cond_1
    const/4 v4, 0x0

    move v5, v4

    goto :goto_0

    .line 372205
    :cond_2
    const/4 v4, 0x0

    move v6, v4

    goto :goto_1

    .line 372206
    :cond_3
    invoke-static {p1, p1}, LX/367;->a(LX/1De;LX/1De;)LX/1dQ;

    move-result-object v4

    goto :goto_2

    .line 372207
    :cond_4
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v11

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v8, 0x1

    invoke-interface {v4, v8}, LX/1Di;->c(I)LX/1Di;

    move-result-object v4

    const/16 v8, 0x8

    const/4 v12, 0x0

    invoke-interface {v4, v8, v12}, LX/1Di;->k(II)LX/1Di;

    move-result-object v8

    if-nez v9, :cond_5

    const/4 v4, 0x0

    :goto_4
    invoke-interface {v8, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v11, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    if-nez v10, :cond_6

    const/4 v4, 0x0

    :goto_5
    invoke-interface {v8, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    if-nez v5, :cond_7

    const/4 v4, 0x0

    :goto_6
    invoke-interface {v8, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v5

    if-nez v6, :cond_8

    const/4 v4, 0x0

    :goto_7
    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto :goto_3

    :cond_5
    invoke-static {p1, p1}, LX/367;->a(LX/1De;LX/1De;)LX/1dQ;

    move-result-object v4

    goto :goto_4

    :cond_6
    iget-object v4, p0, LX/26R;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/37H;

    invoke-virtual {v4, p1}, LX/37H;->c(LX/1De;)LX/37J;

    move-result-object v4

    move/from16 v0, p9

    invoke-virtual {v4, v0}, LX/37J;->h(I)LX/37J;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v9, 0x1

    invoke-interface {v4, v9}, LX/1Di;->c(I)LX/1Di;

    move-result-object v4

    const/16 v9, 0x8

    const/4 v10, 0x0

    invoke-interface {v4, v9, v10}, LX/1Di;->k(II)LX/1Di;

    move-result-object v4

    goto :goto_5

    :cond_7
    invoke-static/range {p1 .. p3}, LX/AmW;->a(LX/1De;II)LX/1Dg;

    move-result-object v4

    goto :goto_6

    :cond_8
    invoke-static {p1}, LX/AmP;->c(LX/1De;)LX/AmN;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v6, 0x1

    invoke-interface {v4, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v4

    const/4 v6, 0x3

    const v8, 0x7f0b0646

    invoke-interface {v4, v6, v8}, LX/1Di;->l(II)LX/1Di;

    move-result-object v4

    const/4 v6, 0x2

    const v8, 0x7f0b0647

    invoke-interface {v4, v6, v8}, LX/1Di;->l(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto :goto_7
.end method
