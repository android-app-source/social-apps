.class public LX/3Lw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 552754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 552755
    iput-object p1, p0, LX/3Lw;->a:LX/0Uh;

    .line 552756
    return-void
.end method

.method public static a(LX/0QB;)LX/3Lw;
    .locals 1

    .prologue
    .line 552758
    invoke-static {p0}, LX/3Lw;->b(LX/0QB;)LX/3Lw;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3Lw;
    .locals 2

    .prologue
    .line 552759
    new-instance v1, LX/3Lw;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v0}, LX/3Lw;-><init>(LX/0Uh;)V

    .line 552760
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 552757
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    iget-object v1, p0, LX/3Lw;->a:LX/0Uh;

    const/16 v2, 0x1ce

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
