.class public final LX/39v;
.super Landroid/graphics/drawable/StateListDrawable;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Wk;)V
    .locals 3

    .prologue
    .line 524093
    invoke-direct {p0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 524094
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a7

    aput v2, v0, v1

    .line 524095
    sget-object v1, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    if-ne p2, v1, :cond_0

    .line 524096
    const v1, 0x7f020a82

    .line 524097
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object v1, v1

    .line 524098
    invoke-virtual {p0, v0, v1}, LX/39v;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 524099
    return-void

    .line 524100
    :cond_0
    sget-object v1, LX/1Wk;->NEWSFEED_FULLBLEED_SHADOW:LX/1Wk;

    if-ne p2, v1, :cond_1

    .line 524101
    const v1, 0x7f020a83

    goto :goto_0

    .line 524102
    :cond_1
    const v1, 0x7f0218c7

    goto :goto_0
.end method


# virtual methods
.method public final onStateChange([I)Z
    .locals 1

    .prologue
    .line 524103
    invoke-super {p0, p1}, Landroid/graphics/drawable/StateListDrawable;->onStateChange([I)Z

    .line 524104
    const/4 v0, 0x0

    return v0
.end method
