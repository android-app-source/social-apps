.class public LX/2m2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# instance fields
.field private final a:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 0

    .prologue
    .line 459277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459278
    iput-object p1, p0, LX/2m2;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 459279
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    const v3, 0x280001

    .line 459269
    iget-object v0, p0, LX/2m2;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 459270
    iget-object v1, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v1

    .line 459271
    invoke-interface {v0, v3, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 459272
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 459273
    iget-object v1, p0, LX/2m2;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 459274
    iget-object v2, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v2, v2

    .line 459275
    invoke-interface {v1, v3, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 459276
    return-object v0
.end method
