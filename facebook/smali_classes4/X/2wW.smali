.class public final LX/2wW;
.super LX/2wX;
.source ""

# interfaces
.implements LX/2wY;


# instance fields
.field public final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/2we",
            "<**>;>;"
        }
    .end annotation
.end field

.field public b:LX/4un;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/2vo",
            "<*>;",
            "LX/2wJ;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/2wA;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/2vs",
            "<*>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/2vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vq",
            "<+",
            "LX/3KI;",
            "LX/2w4;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/4v2;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/2wd;

.field private final j:Ljava/util/concurrent/locks/Lock;

.field private final k:LX/2wb;

.field private l:LX/2wn;

.field private final m:I

.field public final n:Landroid/content/Context;

.field private final o:Landroid/os/Looper;

.field public volatile p:Z

.field public q:J

.field public r:J

.field public final s:LX/2wc;

.field public final t:LX/1vX;

.field private final u:LX/2wZ;

.field private final v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/2wE;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljava/lang/Integer;

.field private final x:LX/2wL;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;LX/2wA;LX/1vX;LX/2vq;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;IILjava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/locks/Lock;",
            "Landroid/os/Looper;",
            "LX/2wA;",
            "LX/1vX;",
            "LX/2vq",
            "<+",
            "LX/3KI;",
            "LX/2w4;",
            ">;",
            "Ljava/util/Map",
            "<",
            "LX/2vs",
            "<*>;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/1qf;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/1qg;",
            ">;",
            "Ljava/util/Map",
            "<",
            "LX/2vo",
            "<*>;",
            "LX/2wJ;",
            ">;II",
            "Ljava/util/ArrayList",
            "<",
            "LX/2wE;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, LX/2wX;-><init>()V

    const/4 v2, 0x0

    iput-object v2, p0, LX/2wW;->l:LX/2wn;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, LX/2wW;->a:Ljava/util/Queue;

    const-wide/32 v2, 0x1d4c0

    iput-wide v2, p0, LX/2wW;->q:J

    const-wide/16 v2, 0x1388

    iput-wide v2, p0, LX/2wW;->r:J

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, LX/2wW;->d:Ljava/util/Set;

    new-instance v2, LX/2wZ;

    invoke-direct {v2}, LX/2wZ;-><init>()V

    iput-object v2, p0, LX/2wW;->u:LX/2wZ;

    const/4 v2, 0x0

    iput-object v2, p0, LX/2wW;->w:Ljava/lang/Integer;

    const/4 v2, 0x0

    iput-object v2, p0, LX/2wW;->h:Ljava/util/Set;

    new-instance v2, LX/2wa;

    invoke-direct {v2, p0}, LX/2wa;-><init>(LX/2wW;)V

    iput-object v2, p0, LX/2wW;->x:LX/2wL;

    iput-object p1, p0, LX/2wW;->n:Landroid/content/Context;

    iput-object p2, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    new-instance v2, LX/2wb;

    iget-object v3, p0, LX/2wW;->x:LX/2wL;

    invoke-direct {v2, p3, v3}, LX/2wb;-><init>(Landroid/os/Looper;LX/2wL;)V

    iput-object v2, p0, LX/2wW;->k:LX/2wb;

    iput-object p3, p0, LX/2wW;->o:Landroid/os/Looper;

    new-instance v2, LX/2wc;

    invoke-direct {v2, p0, p3}, LX/2wc;-><init>(LX/2wW;Landroid/os/Looper;)V

    iput-object v2, p0, LX/2wW;->s:LX/2wc;

    iput-object p5, p0, LX/2wW;->t:LX/1vX;

    move/from16 v0, p11

    iput v0, p0, LX/2wW;->m:I

    iget v2, p0, LX/2wW;->m:I

    if-ltz v2, :cond_0

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, LX/2wW;->w:Ljava/lang/Integer;

    :cond_0
    iput-object p7, p0, LX/2wW;->f:Ljava/util/Map;

    iput-object p10, p0, LX/2wW;->c:Ljava/util/Map;

    move-object/from16 v0, p13

    iput-object v0, p0, LX/2wW;->v:Ljava/util/ArrayList;

    new-instance v2, LX/2wd;

    iget-object v3, p0, LX/2wW;->c:Ljava/util/Map;

    invoke-direct {v2, v3}, LX/2wd;-><init>(Ljava/util/Map;)V

    iput-object v2, p0, LX/2wW;->i:LX/2wd;

    invoke-interface {p8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1qf;

    iget-object v4, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v4, v2}, LX/2wb;->a(LX/1qf;)V

    goto :goto_0

    :cond_1
    invoke-interface {p9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1qg;

    iget-object v4, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v4, v2}, LX/2wb;->a(LX/1qg;)V

    goto :goto_1

    :cond_2
    iput-object p4, p0, LX/2wW;->e:LX/2wA;

    iput-object p6, p0, LX/2wW;->g:LX/2vq;

    return-void
.end method

.method public static a(Ljava/lang/Iterable;Z)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/2wJ;",
            ">;Z)I"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v3, 0x1

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wJ;

    invoke-interface {v0}, LX/2wJ;->n()Z

    move-result v5

    if-eqz v5, :cond_0

    move v2, v3

    :cond_0
    invoke-interface {v0}, LX/2wJ;->p()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_3

    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    const/4 v3, 0x2

    :cond_2
    :goto_2
    return v3

    :cond_3
    const/4 v3, 0x3

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static a$redex0(LX/2wW;LX/2wX;LX/4v0;Z)V
    .locals 2

    sget-object v0, LX/4vD;->c:LX/4vE;

    invoke-interface {v0, p1}, LX/4vE;->a(LX/2wX;)LX/2wg;

    move-result-object v0

    new-instance v1, LX/3KZ;

    invoke-direct {v1, p0, p2, p3, p1}, LX/3KZ;-><init>(LX/2wW;LX/4v0;ZLX/2wX;)V

    invoke-virtual {v0, v1}, LX/2wg;->a(LX/27U;)V

    return-void
.end method

.method private b(I)V
    .locals 12

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, LX/2wW;->w:Ljava/lang/Integer;

    if-nez v1, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, LX/2wW;->w:Ljava/lang/Integer;

    :cond_0
    iget-object v1, p0, LX/2wW;->l:LX/2wn;

    if-eqz v1, :cond_2

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, LX/2wW;->w:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p1}, LX/2wW;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/2wW;->w:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, LX/2wW;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x33

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot use sign-in mode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ". Mode was already set to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v1, p0, LX/2wW;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    move v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wJ;

    invoke-interface {v0}, LX/2wJ;->n()Z

    move-result v5

    if-eqz v5, :cond_3

    move v2, v3

    :cond_3
    invoke-interface {v0}, LX/2wJ;->p()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v3

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_5
    :pswitch_0
    new-instance v0, LX/2wm;

    iget-object v1, p0, LX/2wW;->n:Landroid/content/Context;

    iget-object v3, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    iget-object v4, p0, LX/2wW;->o:Landroid/os/Looper;

    iget-object v5, p0, LX/2wW;->t:LX/1vX;

    iget-object v6, p0, LX/2wW;->c:Ljava/util/Map;

    iget-object v7, p0, LX/2wW;->e:LX/2wA;

    iget-object v8, p0, LX/2wW;->f:Ljava/util/Map;

    iget-object v9, p0, LX/2wW;->g:LX/2vq;

    iget-object v10, p0, LX/2wW;->v:Ljava/util/ArrayList;

    move-object v2, p0

    move-object v11, p0

    invoke-direct/range {v0 .. v11}, LX/2wm;-><init>(Landroid/content/Context;LX/2wW;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;LX/1od;Ljava/util/Map;LX/2wA;Ljava/util/Map;LX/2vq;Ljava/util/ArrayList;LX/2wY;)V

    iput-object v0, p0, LX/2wW;->l:LX/2wn;

    goto/16 :goto_0

    :pswitch_1
    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    if-eqz v1, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    if-eqz v2, :cond_5

    iget-object v0, p0, LX/2wW;->n:Landroid/content/Context;

    iget-object v2, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    iget-object v3, p0, LX/2wW;->o:Landroid/os/Looper;

    iget-object v4, p0, LX/2wW;->t:LX/1vX;

    iget-object v5, p0, LX/2wW;->c:Ljava/util/Map;

    iget-object v6, p0, LX/2wW;->e:LX/2wA;

    iget-object v7, p0, LX/2wW;->f:Ljava/util/Map;

    iget-object v8, p0, LX/2wW;->g:LX/2vq;

    iget-object v9, p0, LX/2wW;->v:Ljava/util/ArrayList;

    move-object v1, p0

    invoke-static/range {v0 .. v9}, LX/4uW;->a(Landroid/content/Context;LX/2wW;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;LX/1od;Ljava/util/Map;LX/2wA;Ljava/util/Map;LX/2vq;Ljava/util/ArrayList;)LX/4uW;

    move-result-object v0

    iput-object v0, p0, LX/2wW;->l:LX/2wn;

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private static c(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "SIGN_IN_MODE_NONE"

    goto :goto_0

    :pswitch_1
    const-string v0, "SIGN_IN_MODE_REQUIRED"

    goto :goto_0

    :pswitch_2
    const-string v0, "SIGN_IN_MODE_OPTIONAL"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private static r(LX/2wW;)V
    .locals 1

    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0}, LX/2wb;->b()V

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0}, LX/2wn;->a()V

    return-void
.end method

.method public static s(LX/2wW;)V
    .locals 2

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-boolean v0, p0, LX/2wW;->p:Z

    move v0, v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/2wW;->r(LX/2wW;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public static t(LX/2wW;)V
    .locals 2

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, LX/2wW;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/2wW;->r(LX/2wW;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final a(LX/2vo;)LX/2wJ;
    .locals 2
    .param p1    # LX/2vo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "LX/2wJ;",
            ">(",
            "LX/2vo",
            "<TC;>;)TC;"
        }
    .end annotation

    iget-object v0, p0, LX/2wW;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wJ;

    const-string v1, "Appropriate Api was not requested."

    invoke-static {v0, v1}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public final a(LX/2we;)LX/2we;
    .locals 4
    .param p1    # LX/2we;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LX/2wK;",
            "R::",
            "LX/2NW;",
            "T:",
            "LX/2we",
            "<TR;TA;>;>(TT;)TT;"
        }
    .end annotation

    iget-object v0, p1, LX/2we;->d:LX/2vo;

    move-object v0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This task can not be enqueued (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, LX/1ol;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, LX/2wW;->c:Ljava/util/Map;

    iget-object v1, p1, LX/2we;->d:LX/2vo;

    move-object v1, v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    iget-object v0, p1, LX/2we;->e:LX/2vs;

    move-object v0, v0

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/2we;->e:LX/2vs;

    move-object v0, v0

    iget-object v2, v0, LX/2vs;->e:Ljava/lang/String;

    move-object v0, v2

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x41

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "GoogleApiClient is not configured to use "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " required for this call."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/1ol;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/2wW;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_2
    return-object p1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "the API"

    goto :goto_1

    :cond_2
    :try_start_1
    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0, p1}, LX/2wn;->a(LX/2we;)LX/2we;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object p1

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/ConnectionResult;
    .locals 3
    .param p3    # Ljava/util/concurrent/TimeUnit;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "blockingConnect must not be called on the UI thread"

    invoke-static {v0, v1}, LX/1ol;->a(ZLjava/lang/Object;)V

    const-string v0, "TimeUnit must not be null"

    invoke-static {p3, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/2wW;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2wW;->a(Ljava/lang/Iterable;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    :cond_1
    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, LX/2wW;->b(I)V

    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0}, LX/2wb;->b()V

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0, p1, p2, p3}, LX/2wn;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/ConnectionResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :cond_2
    :try_start_1
    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(I)V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x21

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Illegal sign-in mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/1ol;->b(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, LX/2wW;->b(I)V

    invoke-static {p0}, LX/2wW;->r(LX/2wW;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(IZ)V
    .locals 7

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    if-nez p2, :cond_0

    const/4 v4, 0x1

    iget-boolean v1, p0, LX/2wW;->p:Z

    move v1, v1

    if-eqz v1, :cond_3

    :cond_0
    :goto_0
    iget-object v0, p0, LX/2wW;->i:LX/2wd;

    iget-object v1, v0, LX/2wd;->a:Ljava/util/Set;

    sget-object v2, LX/2wd;->b:[LX/2we;

    invoke-interface {v1, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LX/2we;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v4, v1, v2

    new-instance v5, Lcom/google/android/gms/common/api/Status;

    const/16 v6, 0x8

    const-string p2, "The connection to Google Play services was lost"

    invoke-direct {v5, v6, p2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    invoke-virtual {v4, v5}, LX/2wf;->c(Lcom/google/android/gms/common/api/Status;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0, p1}, LX/2wb;->a(I)V

    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0}, LX/2wb;->a()V

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    invoke-static {p0}, LX/2wW;->r(LX/2wW;)V

    :cond_2
    return-void

    :cond_3
    iput-boolean v4, p0, LX/2wW;->p:Z

    iget-object v1, p0, LX/2wW;->b:LX/4un;

    if-nez v1, :cond_4

    iget-object v2, p0, LX/2wW;->n:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, LX/4uk;

    invoke-direct {v3, p0}, LX/4uk;-><init>(LX/2wW;)V

    invoke-static {v2, v3}, LX/1vX;->a(Landroid/content/Context;LX/4uR;)LX/4un;

    move-result-object v1

    iput-object v1, p0, LX/2wW;->b:LX/4un;

    :cond_4
    iget-object v1, p0, LX/2wW;->s:LX/2wc;

    iget-object v2, p0, LX/2wW;->s:LX/2wc;

    invoke-virtual {v2, v4}, LX/2wc;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    iget-wide v3, p0, LX/2wW;->q:J

    invoke-virtual {v1, v2, v3, v4}, LX/2wc;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v1, p0, LX/2wW;->s:LX/2wc;

    iget-object v2, p0, LX/2wW;->s:LX/2wc;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/2wc;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    iget-wide v3, p0, LX/2wW;->r:J

    invoke-virtual {v1, v2, v3, v4}, LX/2wc;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public final a(LX/1qf;)V
    .locals 1
    .param p1    # LX/1qf;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0, p1}, LX/2wb;->b(LX/1qf;)V

    return-void
.end method

.method public final a(LX/1qg;)V
    .locals 1
    .param p1    # LX/1qg;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0, p1}, LX/2wb;->a(LX/1qg;)V

    return-void
.end method

.method public final a(LX/4v2;)V
    .locals 2

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LX/2wW;->h:Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2wW;->h:Ljava/util/Set;

    :cond_0
    iget-object v0, p0, LX/2wW;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    :goto_0
    iget-object v0, p0, LX/2wW;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2wW;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2we;

    invoke-virtual {p0, v0}, LX/2wX;->b(LX/2we;)LX/2we;

    goto :goto_0

    :cond_0
    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0, p1}, LX/2wb;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 4

    iget-object v1, p0, LX/2wW;->n:Landroid/content/Context;

    iget v2, p1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v2, v2

    invoke-static {v1, v2}, LX/1oW;->b(Landroid/content/Context;I)Z

    move-result v3

    move v0, v3

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/2wW;->m()Z

    :cond_0
    iget-boolean v0, p0, LX/2wW;->p:Z

    move v0, v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0, p1}, LX/2wb;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0}, LX/2wb;->a()V

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "mContext="

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, LX/2wW;->n:Landroid/content/Context;

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "mResuming="

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-boolean v1, p0, LX/2wW;->p:Z

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mWorkQueue.size()="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, LX/2wW;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    iget-object v0, p0, LX/2wW;->i:LX/2wd;

    const-string v1, " mUnconsumedApiCalls.size()="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v1

    iget-object v2, v0, LX/2wd;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->println(I)V

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0, p1, p2, p3, p4}, LX/2wn;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(LX/2vs;)Z
    .locals 2
    .param p1    # LX/2vs;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2vs",
            "<*>;)Z"
        }
    .end annotation

    iget-object v0, p0, LX/2wW;->c:Ljava/util/Map;

    invoke-virtual {p1}, LX/2vs;->d()LX/2vo;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/4uz;)Z
    .locals 1

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0, p1}, LX/2wn;->a(LX/4uz;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/2we;)LX/2we;
    .locals 4
    .param p1    # LX/2we;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LX/2wK;",
            "T:",
            "LX/2we",
            "<+",
            "LX/2NW;",
            "TA;>;>(TT;)TT;"
        }
    .end annotation

    iget-object v0, p1, LX/2we;->d:LX/2vo;

    move-object v0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This task can not be executed (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, LX/1ol;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, LX/2wW;->c:Ljava/util/Map;

    iget-object v1, p1, LX/2we;->d:LX/2vo;

    move-object v1, v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    iget-object v0, p1, LX/2we;->e:LX/2vs;

    move-object v0, v0

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/2we;->e:LX/2vs;

    move-object v0, v0

    iget-object v2, v0, LX/2vs;->e:Ljava/lang/String;

    move-object v0, v2

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x41

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "GoogleApiClient is not configured to use "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " required for this call."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/1ol;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GoogleApiClient is not connected yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "the API"

    goto :goto_1

    :cond_2
    :try_start_1
    iget-boolean v0, p0, LX/2wW;->p:Z

    move v0, v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/2wW;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    :goto_2
    iget-object v0, p0, LX/2wW;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, LX/2wW;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2we;

    iget-object v1, p0, LX/2wW;->i:LX/2wd;

    invoke-virtual {v1, v0}, LX/2wd;->a(LX/2we;)V

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1}, LX/2we;->b(Lcom/google/android/gms/common/api/Status;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_3
    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_3
    return-object p1

    :cond_4
    :try_start_2
    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0, p1}, LX/2wn;->b(LX/2we;)LX/2we;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object p1

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_3
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, LX/2wW;->n:Landroid/content/Context;

    return-object v0
.end method

.method public final b(LX/1qg;)V
    .locals 1
    .param p1    # LX/1qg;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0, p1}, LX/2wb;->b(LX/1qg;)V

    return-void
.end method

.method public final b(LX/4v2;)V
    .locals 3

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LX/2wW;->h:Ljava/util/Set;

    if-nez v0, :cond_1

    const-string v0, "GoogleApiClientImpl"

    const-string v1, "Attempted to remove pending transform when no transforms are registered."

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2wW;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "GoogleApiClientImpl"

    const-string v1, "Failed to remove pending transform - this may lead to memory leaks!"

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    :try_start_2
    invoke-virtual {p0}, LX/2wW;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0}, LX/2wn;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final c()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, LX/2wW;->o:Landroid/os/Looper;

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0}, LX/2wn;->g()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v1, p0, LX/2wW;->m:I

    if-ltz v1, :cond_2

    iget-object v1, p0, LX/2wW;->w:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "Sign-in mode should have been set explicitly by auto-manage."

    invoke-static {v0, v1}, LX/1ol;->a(ZLjava/lang/Object;)V

    :cond_1
    :goto_0
    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LX/2wX;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    if-nez v0, :cond_3

    iget-object v0, p0, LX/2wW;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2wW;->a(Ljava/lang/Iterable;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_3
    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final f()Lcom/google/android/gms/common/ConnectionResult;
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "blockingConnect must not be called on the UI thread"

    invoke-static {v0, v3}, LX/1ol;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, LX/2wW;->m:I

    if-ltz v0, :cond_3

    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    :goto_1
    const-string v0, "Sign-in mode should have been set explicitly by auto-manage."

    invoke-static {v1, v0}, LX/1ol;->a(ZLjava/lang/Object;)V

    :cond_0
    :goto_2
    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, LX/2wW;->b(I)V

    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0}, LX/2wb;->b()V

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0}, LX/2wn;->b()Lcom/google/android/gms/common/ConnectionResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    :try_start_1
    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    if-nez v0, :cond_4

    iget-object v0, p0, LX/2wW;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2wW;->a(Ljava/lang/Iterable;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_4
    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final g()V
    .locals 3

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LX/2wW;->i:LX/2wd;

    invoke-virtual {v0}, LX/2wd;->a()V

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0}, LX/2wn;->c()V

    :cond_0
    iget-object v0, p0, LX/2wW;->u:LX/2wZ;

    invoke-virtual {v0}, LX/2wZ;->a()V

    iget-object v0, p0, LX/2wW;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2we;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/2we;->a(LX/2wj;)V

    invoke-virtual {v0}, LX/2wf;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2wW;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, LX/2wW;->l:LX/2wn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_1
    return-void

    :cond_2
    :try_start_2
    invoke-virtual {p0}, LX/2wW;->m()Z

    iget-object v0, p0, LX/2wW;->k:LX/2wb;

    invoke-virtual {v0}, LX/2wb;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1
.end method

.method public final h()LX/2wg;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p0}, LX/2wX;->i()Z

    move-result v0

    const-string v2, "GoogleApiClient is not connected yet."

    invoke-static {v0, v2}, LX/1ol;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, LX/2wW;->w:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API"

    invoke-static {v0, v2}, LX/1ol;->a(ZLjava/lang/Object;)V

    new-instance v0, LX/4v0;

    invoke-direct {v0, p0}, LX/4v0;-><init>(LX/2wX;)V

    iget-object v2, p0, LX/2wW;->c:Ljava/util/Map;

    sget-object v3, LX/4vD;->a:LX/2vn;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0, p0, v0, v1}, LX/2wW;->a$redex0(LX/2wW;LX/2wX;LX/4v0;Z)V

    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    new-instance v2, LX/4ui;

    invoke-direct {v2, p0, v1, v0}, LX/4ui;-><init>(LX/2wW;Ljava/util/concurrent/atomic/AtomicReference;LX/4v0;)V

    new-instance v3, LX/4uj;

    invoke-direct {v3, p0, v0}, LX/4uj;-><init>(LX/2wW;LX/4v0;)V

    new-instance v4, LX/2vz;

    iget-object v5, p0, LX/2wW;->n:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/2vz;-><init>(Landroid/content/Context;)V

    sget-object v5, LX/4vD;->b:LX/2vs;

    invoke-virtual {v4, v5}, LX/2vz;->a(LX/2vs;)LX/2vz;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/2vz;->a(LX/1qf;)LX/2vz;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/2vz;->a(LX/1qg;)LX/2vz;

    move-result-object v2

    iget-object v3, p0, LX/2wW;->s:LX/2wc;

    invoke-virtual {v2, v3}, LX/2vz;->a(Landroid/os/Handler;)LX/2vz;

    move-result-object v2

    invoke-virtual {v2}, LX/2vz;->b()LX/2wX;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    invoke-virtual {v2}, LX/2wX;->e()V

    goto :goto_1
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0}, LX/2wn;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2wW;->l:LX/2wn;

    invoke-interface {v0}, LX/2wn;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, LX/2wW;->p:Z

    move v2, v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iput-boolean v0, p0, LX/2wW;->p:Z

    iget-object v0, p0, LX/2wW;->s:LX/2wc;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, LX/2wc;->removeMessages(I)V

    iget-object v0, p0, LX/2wW;->s:LX/2wc;

    invoke-virtual {v0, v1}, LX/2wc;->removeMessages(I)V

    iget-object v0, p0, LX/2wW;->b:LX/4un;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2wW;->b:LX/4un;

    invoke-virtual {v0}, LX/4un;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, LX/2wW;->b:LX/4un;

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final n()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v1, p0, LX/2wW;->h:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return v0

    :cond_0
    :try_start_1
    iget-object v1, p0, LX/2wW;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/2wW;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final o()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    const-string v1, ""

    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p0, v1, v3, v2, v3}, LX/2wX;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final p()I
    .locals 1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
