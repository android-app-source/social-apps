.class public final LX/28V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/335;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/335;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 374177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374178
    iput-object p1, p0, LX/28V;->a:LX/0QB;

    .line 374179
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/335;",
            ">;"
        }
    .end annotation

    .prologue
    .line 374159
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/28V;

    invoke-direct {v2, p0}, LX/28V;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 374180
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/28V;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 374161
    packed-switch p2, :pswitch_data_0

    .line 374162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 374163
    :pswitch_0
    new-instance p0, LX/2gj;

    .line 374164
    new-instance v1, LX/2qs;

    invoke-static {p1}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-direct {v1, v0}, LX/2qs;-><init>(LX/0sO;)V

    .line 374165
    move-object v0, v1

    .line 374166
    check-cast v0, LX/2qs;

    invoke-static {p1}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v1

    check-cast v1, LX/0yc;

    invoke-direct {p0, v0, v1}, LX/2gj;-><init>(LX/2qs;LX/0yc;)V

    .line 374167
    move-object v0, p0

    .line 374168
    :goto_0
    return-object v0

    .line 374169
    :pswitch_1
    new-instance p0, LX/336;

    .line 374170
    new-instance v1, LX/338;

    invoke-static {p1}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-direct {v1, v0}, LX/338;-><init>(LX/0sO;)V

    .line 374171
    move-object v0, v1

    .line 374172
    check-cast v0, LX/338;

    invoke-static {p1}, LX/339;->a(LX/0QB;)LX/339;

    move-result-object v1

    check-cast v1, LX/339;

    invoke-direct {p0, v0, v1}, LX/336;-><init>(LX/338;LX/339;)V

    .line 374173
    move-object v0, p0

    .line 374174
    goto :goto_0

    .line 374175
    :pswitch_2
    invoke-static {p1}, LX/2Zo;->b(LX/0QB;)LX/2Zo;

    move-result-object v0

    goto :goto_0

    .line 374176
    :pswitch_3
    invoke-static {p1}, LX/2aH;->b(LX/0QB;)LX/2aH;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 374160
    const/4 v0, 0x4

    return v0
.end method
