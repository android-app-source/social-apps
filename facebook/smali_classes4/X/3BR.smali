.class public LX/3BR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3BR;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/03V;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 528461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528462
    iput-object p1, p0, LX/3BR;->a:LX/0Ot;

    .line 528463
    iput-object p2, p0, LX/3BR;->b:LX/0Ot;

    .line 528464
    iput-object p3, p0, LX/3BR;->c:LX/03V;

    .line 528465
    return-void
.end method

.method public static a(LX/0QB;)LX/3BR;
    .locals 6

    .prologue
    .line 528448
    sget-object v0, LX/3BR;->d:LX/3BR;

    if-nez v0, :cond_1

    .line 528449
    const-class v1, LX/3BR;

    monitor-enter v1

    .line 528450
    :try_start_0
    sget-object v0, LX/3BR;->d:LX/3BR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 528451
    if-eqz v2, :cond_0

    .line 528452
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 528453
    new-instance v4, LX/3BR;

    const/16 v3, 0xc1c

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0xbc

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {v4, v5, p0, v3}, LX/3BR;-><init>(LX/0Ot;LX/0Ot;LX/03V;)V

    .line 528454
    move-object v0, v4

    .line 528455
    sput-object v0, LX/3BR;->d:LX/3BR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528456
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 528457
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 528458
    :cond_1
    sget-object v0, LX/3BR;->d:LX/3BR;

    return-object v0

    .line 528459
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 528460
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;[JIJJJ)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 528443
    iget-object v0, p0, LX/3BR;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, p1, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 528444
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 528445
    const-string v1, "oxygen_map"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "percentile_25"

    aget-wide v4, p2, v3

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v1

    const-string v2, "percentile_50"

    const/4 v3, 0x1

    aget-wide v4, p2, v3

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v1

    const-string v2, "percentile_75"

    const/4 v3, 0x2

    aget-wide v4, p2, v3

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v1

    const-string v2, "percentile_90"

    const/4 v3, 0x3

    aget-wide v4, p2, v3

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v1

    const-string v2, "percentile_99"

    const/4 v3, 0x4

    aget-wide v4, p2, v3

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v1

    const-string v2, "instance_size"

    invoke-virtual {v1, v2, p3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v1

    const-string v2, "max_sample_value"

    invoke-virtual {v1, v2, p4, p5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v1

    const-string v2, "min_sample_value"

    invoke-virtual {v1, v2, p6, p7}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v1

    const-string v2, "sum_sample_value"

    invoke-virtual {v1, v2, p8, p9}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 528446
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 528447
    :cond_0
    return-void
.end method
