.class public LX/2qy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/preference/Preference;

.field public final b:Landroid/content/SharedPreferences;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Landroid/preference/Preference;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .param p1    # Landroid/preference/Preference;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 471555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 471556
    iput-object p1, p0, LX/2qy;->a:Landroid/preference/Preference;

    .line 471557
    new-instance v0, LX/4i4;

    invoke-direct {v0, p2}, LX/4i4;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    iput-object v0, p0, LX/2qy;->b:Landroid/content/SharedPreferences;

    .line 471558
    iput-object p2, p0, LX/2qy;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 471559
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 471560
    iget-object v0, p0, LX/2qy;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v1, LX/0Tn;

    iget-object v2, p0, LX/2qy;->a:Landroid/preference/Preference;

    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0Tn;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Tn;)V
    .locals 2

    .prologue
    .line 471561
    iget-object v0, p0, LX/2qy;->a:Landroid/preference/Preference;

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 471562
    return-void
.end method

.method public final a(Z)Z
    .locals 3

    .prologue
    .line 471563
    iget-object v0, p0, LX/2qy;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v1, LX/0Tn;

    iget-object v2, p0, LX/2qy;->a:Landroid/preference/Preference;

    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0Tn;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 471564
    iget-object v0, p0, LX/2qy;->a:Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/2qy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 471565
    iget-object v0, p0, LX/2qy;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 471566
    new-instance v1, LX/0Tn;

    iget-object v2, p0, LX/2qy;->a:Landroid/preference/Preference;

    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0Tn;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 471567
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 471568
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
