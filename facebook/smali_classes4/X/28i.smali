.class public LX/28i;
.super LX/16B;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/28i;


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:LX/0Sh;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0c5;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/03V;


# direct methods
.method public constructor <init>(LX/0Sh;Ljava/util/Set;LX/03V;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/Set",
            "<",
            "LX/0c5;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374381
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 374382
    const-class v0, LX/28i;

    iput-object v0, p0, LX/28i;->a:Ljava/lang/Class;

    .line 374383
    iput-object p1, p0, LX/28i;->b:LX/0Sh;

    .line 374384
    iput-object p2, p0, LX/28i;->c:Ljava/util/Set;

    .line 374385
    iput-object p3, p0, LX/28i;->d:LX/03V;

    .line 374386
    return-void
.end method

.method public static a(LX/0QB;)LX/28i;
    .locals 7

    .prologue
    .line 374387
    sget-object v0, LX/28i;->e:LX/28i;

    if-nez v0, :cond_1

    .line 374388
    const-class v1, LX/28i;

    monitor-enter v1

    .line 374389
    :try_start_0
    sget-object v0, LX/28i;->e:LX/28i;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 374390
    if-eqz v2, :cond_0

    .line 374391
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 374392
    new-instance v5, LX/28i;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    .line 374393
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    new-instance p0, LX/28j;

    invoke-direct {p0, v0}, LX/28j;-><init>(LX/0QB;)V

    invoke-direct {v4, v6, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v4

    .line 374394
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {v5, v3, v6, v4}, LX/28i;-><init>(LX/0Sh;Ljava/util/Set;LX/03V;)V

    .line 374395
    move-object v0, v5

    .line 374396
    sput-object v0, LX/28i;->e:LX/28i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374397
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 374398
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 374399
    :cond_1
    sget-object v0, LX/28i;->e:LX/28i;

    return-object v0

    .line 374400
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 374401
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;LX/41V;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 374402
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 374403
    const/4 v1, 0x0

    move v2, v0

    .line 374404
    :goto_0
    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 374405
    :try_start_0
    iget-object v3, p2, LX/41V;->a:LX/0c5;

    invoke-interface {v3}, LX/0c5;->clearUserData()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 374406
    const/4 v0, 0x1

    .line 374407
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-eqz v2, :cond_2

    .line 374408
    :cond_1
    if-eqz v0, :cond_4

    .line 374409
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "clearInternal-recovered-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 374410
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Hit exceptions before successfully clearing: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 374411
    iget-object v2, p0, LX/28i;->d:LX/03V;

    invoke-virtual {v2, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 374412
    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 374413
    :cond_2
    return-void

    .line 374414
    :catch_0
    move-exception v3

    .line 374415
    invoke-virtual {v3}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374416
    new-instance v5, Ljava/io/StringWriter;

    invoke-direct {v5}, Ljava/io/StringWriter;-><init>()V

    .line 374417
    new-instance v6, Ljava/io/PrintWriter;

    invoke-direct {v6, v5}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v3, v6}, Ljava/lang/RuntimeException;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 374418
    invoke-virtual {v5}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374419
    if-nez v1, :cond_3

    move-object v1, v3

    .line 374420
    :cond_3
    const-wide/16 v6, 0x1e

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 374421
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 374422
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "clearInternal-failed-"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 374423
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "All retries failed for clearing: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 374424
    iget-object v3, p0, LX/28i;->d:LX/03V;

    invoke-virtual {v3, v0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 374425
    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 374426
    throw v1

    :catch_1
    goto :goto_1
.end method


# virtual methods
.method public final h()V
    .locals 5

    .prologue
    .line 374427
    iget-object v0, p0, LX/28i;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 374428
    const/4 v1, 0x0

    .line 374429
    iget-object v0, p0, LX/28i;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0c5;

    .line 374430
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/41V;

    invoke-direct {v4, p0, v0}, LX/41V;-><init>(LX/28i;LX/0c5;)V

    invoke-direct {p0, v3, v4}, LX/28i;->a(Ljava/lang/String;LX/41V;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 374431
    :catch_0
    move-exception v0

    .line 374432
    if-nez v1, :cond_2

    :goto_1
    move-object v1, v0

    .line 374433
    goto :goto_0

    .line 374434
    :cond_0
    if-eqz v1, :cond_1

    .line 374435
    throw v1

    .line 374436
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method
