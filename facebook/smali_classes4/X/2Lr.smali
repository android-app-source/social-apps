.class public LX/2Lr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:Landroid/content/Context;

.field private c:LX/0SG;

.field public d:Lcom/facebook/loom/config/SystemControlConfiguration;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0SG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395445
    iput-object p1, p0, LX/2Lr;->b:Landroid/content/Context;

    .line 395446
    iput-object p2, p0, LX/2Lr;->c:LX/0SG;

    .line 395447
    new-instance v0, Lcom/facebook/loom/config/SystemControlConfiguration;

    invoke-direct {v0}, Lcom/facebook/loom/config/SystemControlConfiguration;-><init>()V

    iput-object v0, p0, LX/2Lr;->d:Lcom/facebook/loom/config/SystemControlConfiguration;

    .line 395448
    iput-object p3, p0, LX/2Lr;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 395449
    iput-object p4, p0, LX/2Lr;->e:LX/0Or;

    .line 395450
    return-void
.end method

.method public static b(LX/2Lr;)V
    .locals 2

    .prologue
    .line 395451
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, LX/2Lr;->b(LX/2Lr;J)V

    .line 395452
    return-void
.end method

.method public static declared-synchronized b(LX/2Lr;J)V
    .locals 7

    .prologue
    .line 395453
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2Lr;->d(LX/2Lr;)J

    move-result-wide v0

    invoke-static {p0}, LX/2Lr;->d(LX/2Lr;)J

    move-result-wide v2

    invoke-static {p0, v2, v3}, LX/2Lr;->d(LX/2Lr;J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 395454
    iget-object v2, p0, LX/2Lr;->d:Lcom/facebook/loom/config/SystemControlConfiguration;

    invoke-virtual {v2}, Lcom/facebook/loom/config/SystemControlConfiguration;->c()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-eqz v2, :cond_1

    .line 395455
    :cond_0
    iget-object v2, p0, LX/2Lr;->d:Lcom/facebook/loom/config/SystemControlConfiguration;

    invoke-virtual {v2}, Lcom/facebook/loom/config/SystemControlConfiguration;->c()J

    move-result-wide v2

    div-long/2addr v0, v2

    .line 395456
    invoke-static {p0}, LX/2Lr;->c(LX/2Lr;)J

    move-result-wide v2

    iget-object v4, p0, LX/2Lr;->d:Lcom/facebook/loom/config/SystemControlConfiguration;

    invoke-virtual {v4}, Lcom/facebook/loom/config/SystemControlConfiguration;->b()J

    move-result-wide v4

    mul-long/2addr v4, v0

    add-long/2addr v2, v4

    add-long/2addr v2, p1

    invoke-static {p0, v2, v3}, LX/2Lr;->c(LX/2Lr;J)V

    .line 395457
    invoke-static {p0}, LX/2Lr;->d(LX/2Lr;)J

    move-result-wide v2

    invoke-static {p0, v2, v3}, LX/2Lr;->d(LX/2Lr;J)J

    move-result-wide v2

    iget-object v4, p0, LX/2Lr;->d:Lcom/facebook/loom/config/SystemControlConfiguration;

    invoke-virtual {v4}, Lcom/facebook/loom/config/SystemControlConfiguration;->c()J

    move-result-wide v4

    mul-long/2addr v0, v4

    add-long/2addr v0, v2

    invoke-static {p0, v0, v1}, LX/2Lr;->e(LX/2Lr;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395458
    :cond_1
    monitor-exit p0

    return-void

    .line 395459
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c(LX/2Lr;)J
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 395460
    iget-object v0, p0, LX/2Lr;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/09i;->c:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(LX/2Lr;J)V
    .locals 5

    .prologue
    .line 395461
    iget-object v0, p0, LX/2Lr;->d:Lcom/facebook/loom/config/SystemControlConfiguration;

    invoke-virtual {v0}, Lcom/facebook/loom/config/SystemControlConfiguration;->a()J

    move-result-wide v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 395462
    iget-object v2, p0, LX/2Lr;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/09i;->c:LX/0Tn;

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 395463
    return-void
.end method

.method public static d(LX/2Lr;)J
    .locals 4

    .prologue
    .line 395464
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/2Lr;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static d(LX/2Lr;J)J
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 395465
    iget-object v0, p0, LX/2Lr;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/09i;->b:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static e(LX/2Lr;J)V
    .locals 3

    .prologue
    .line 395466
    iget-object v0, p0, LX/2Lr;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/09i;->b:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 395467
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 395468
    invoke-static {p0}, LX/2Lr;->b(LX/2Lr;)V

    .line 395469
    iget-object v0, p0, LX/2Lr;->b:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 395470
    if-nez v0, :cond_0

    move v0, v1

    .line 395471
    :goto_0
    return v0

    .line 395472
    :cond_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 395473
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    .line 395474
    goto :goto_0

    .line 395475
    :cond_2
    iget-object v0, p0, LX/2Lr;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v0, v3, :cond_3

    move v0, v2

    .line 395476
    goto :goto_0

    .line 395477
    :cond_3
    invoke-static {p0}, LX/2Lr;->c(LX/2Lr;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    move v0, v2

    .line 395478
    goto :goto_0

    :cond_4
    move v0, v1

    .line 395479
    goto :goto_0
.end method
