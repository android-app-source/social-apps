.class public final LX/3Ob;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/divebar/contacts/DivebarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V
    .locals 0

    .prologue
    .line 560613
    iput-object p1, p0, LX/3Ob;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 560607
    sget-object v0, Lcom/facebook/divebar/contacts/DivebarFragment;->a:Ljava/lang/Class;

    const-string v1, "Failed to get location"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 560608
    iget-object v0, p0, LX/3Ob;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/divebar/contacts/DivebarFragment;->a$redex0(Lcom/facebook/divebar/contacts/DivebarFragment;Lcom/facebook/location/ImmutableLocation;)V

    .line 560609
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 560610
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 560611
    iget-object v0, p0, LX/3Ob;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    invoke-static {v0, p1}, Lcom/facebook/divebar/contacts/DivebarFragment;->a$redex0(Lcom/facebook/divebar/contacts/DivebarFragment;Lcom/facebook/location/ImmutableLocation;)V

    .line 560612
    return-void
.end method
