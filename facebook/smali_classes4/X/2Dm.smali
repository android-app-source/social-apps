.class public LX/2Dm;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 384653
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 384654
    return-void
.end method

.method public static a(LX/2Cl;)LX/03R;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/annotations/IsFastPwErrorArEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 384655
    sget-object v0, LX/2Cm;->FAST_PW_ERROR_AR:LX/2Cm;

    invoke-virtual {p0, v0}, LX/2Cl;->a(LX/2Cm;)LX/2KF;

    move-result-object v0

    invoke-virtual {v0}, LX/2KF;->a()LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/annotations/IsLoginWithPhoneNumberSupported;
    .end annotation

    .prologue
    .line 384656
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 384657
    return-void
.end method
