.class public final LX/2gK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0fJ;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentBaseActivity;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 448279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448280
    return-void
.end method

.method public static b(LX/0QB;)LX/2gK;
    .locals 3

    .prologue
    .line 448281
    new-instance v0, LX/2gK;

    invoke-direct {v0}, LX/2gK;-><init>()V

    .line 448282
    const/16 v1, 0xc16

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0xb

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 448283
    iput-object v1, v0, LX/2gK;->a:LX/0Or;

    iput-object v2, v0, LX/2gK;->b:LX/0Or;

    .line 448284
    return-object v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 448285
    iget-object v0, p0, LX/2gK;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 448286
    iget-object v1, p0, LX/2gK;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0fJ;

    .line 448287
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 448288
    invoke-interface {v1, v0}, LX/0fJ;->a(Landroid/content/Intent;)V

    .line 448289
    return-object v0
.end method
