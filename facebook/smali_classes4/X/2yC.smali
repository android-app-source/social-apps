.class public final LX/2yC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 481112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 481094
    if-nez p1, :cond_0

    .line 481095
    :goto_0
    return v0

    .line 481096
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 481097
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 481098
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    const/4 v4, 0x0

    .line 481099
    if-nez v3, :cond_1

    .line 481100
    :goto_1
    move v3, v4

    .line 481101
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 481102
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 481103
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 481104
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 481105
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 481106
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 481107
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 481108
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 481109
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 481110
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 481111
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLAppStoreApplication;)I
    .locals 18

    .prologue
    .line 481043
    if-nez p1, :cond_0

    .line 481044
    const/4 v2, 0x0

    .line 481045
    :goto_0
    return v2

    .line 481046
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 481047
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 481048
    const/4 v2, 0x0

    .line 481049
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->k()LX/0Px;

    move-result-object v4

    .line 481050
    if-eqz v4, :cond_4

    .line 481051
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    new-array v7, v2, [I

    .line 481052
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 481053
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLImage;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2yC;->b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v2

    aput v2, v7, v3

    .line 481054
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 481055
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v2}, LX/186;->a([IZ)I

    move-result v2

    move v3, v2

    .line 481056
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2yC;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v7

    .line 481057
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->m()Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 481058
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->n()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 481059
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o()Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 481060
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2yC;->b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v11

    .line 481061
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->q()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v12

    .line 481062
    const/4 v2, 0x0

    .line 481063
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r()LX/0Px;

    move-result-object v13

    .line 481064
    if-eqz v13, :cond_3

    .line 481065
    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v2

    new-array v14, v2, [I

    .line 481066
    const/4 v2, 0x0

    move v4, v2

    :goto_3
    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v2

    if-ge v4, v2, :cond_2

    .line 481067
    invoke-virtual {v13, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLImage;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/2yC;->b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v2

    aput v2, v14, v4

    .line 481068
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 481069
    :cond_2
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2}, LX/186;->a([IZ)I

    move-result v2

    .line 481070
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->s()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/2yC;->a(LX/186;Lcom/facebook/graphql/model/GraphQLApplication;)I

    move-result v4

    .line 481071
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->t()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 481072
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->u()LX/0Px;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LX/186;->c(Ljava/util/List;)I

    move-result v14

    .line 481073
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/2yC;->b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v15

    .line 481074
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->x()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 481075
    const/16 v17, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 481076
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 481077
    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 481078
    const/4 v5, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v3}, LX/186;->b(II)V

    .line 481079
    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 481080
    const/4 v3, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 481081
    const/4 v3, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 481082
    const/4 v3, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 481083
    const/4 v3, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 481084
    const/16 v3, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 481085
    const/16 v3, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 481086
    const/16 v2, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 481087
    const/16 v2, 0xb

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 481088
    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 481089
    const/16 v2, 0xd

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 481090
    const/16 v2, 0xe

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->w()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 481091
    const/16 v2, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 481092
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 481093
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_4
    move v3, v2

    goto/16 :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLApplication;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 481032
    if-nez p1, :cond_0

    .line 481033
    :goto_0
    return v0

    .line 481034
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 481035
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 481036
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 481037
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 481038
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 481039
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 481040
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 481041
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 481042
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 481025
    if-nez p1, :cond_0

    .line 481026
    :goto_0
    return v0

    .line 481027
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 481028
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 481029
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 481030
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 481031
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 480975
    if-nez p1, :cond_0

    .line 480976
    :goto_0
    return v0

    .line 480977
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 480978
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 480979
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 480980
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 480981
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 480982
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 480983
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 480984
    if-nez p1, :cond_0

    .line 480985
    :goto_0
    return v2

    .line 480986
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    .line 480987
    if-eqz v3, :cond_2

    .line 480988
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 480989
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 480990
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    const/4 v6, 0x0

    .line 480991
    if-nez v0, :cond_3

    .line 480992
    :goto_2
    move v0, v6

    .line 480993
    aput v0, v4, v1

    .line 480994
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 480995
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 480996
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 480997
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 480998
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 480999
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 481000
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 481001
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 481002
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v7

    const/4 v8, 0x0

    .line 481003
    if-nez v7, :cond_4

    .line 481004
    :goto_4
    move v7, v8

    .line 481005
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 481006
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 481007
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 481008
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 481009
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    .line 481010
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 481011
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    const/4 v12, 0x0

    .line 481012
    if-nez v11, :cond_5

    .line 481013
    :goto_5
    move v11, v12

    .line 481014
    const/4 v12, 0x3

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 481015
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 481016
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v10}, LX/186;->b(II)V

    .line 481017
    const/4 v8, 0x2

    invoke-virtual {p0, v8, v11}, LX/186;->b(II)V

    .line 481018
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 481019
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4

    .line 481020
    :cond_5
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 481021
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 481022
    invoke-virtual {p0, v12, v0}, LX/186;->b(II)V

    .line 481023
    invoke-virtual {p0}, LX/186;->d()I

    move-result v12

    .line 481024
    invoke-virtual {p0, v12}, LX/186;->d(I)V

    goto :goto_5
.end method
