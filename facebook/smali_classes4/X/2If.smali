.class public LX/2If;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2F1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2If;


# instance fields
.field private final a:LX/30I;


# direct methods
.method public constructor <init>(LX/30I;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392034
    iput-object p1, p0, LX/2If;->a:LX/30I;

    .line 392035
    return-void
.end method

.method public static a(LX/0QB;)LX/2If;
    .locals 4

    .prologue
    .line 392036
    sget-object v0, LX/2If;->b:LX/2If;

    if-nez v0, :cond_1

    .line 392037
    const-class v1, LX/2If;

    monitor-enter v1

    .line 392038
    :try_start_0
    sget-object v0, LX/2If;->b:LX/2If;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 392039
    if-eqz v2, :cond_0

    .line 392040
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 392041
    new-instance p0, LX/2If;

    invoke-static {v0}, LX/30I;->b(LX/0QB;)LX/30I;

    move-result-object v3

    check-cast v3, LX/30I;

    invoke-direct {p0, v3}, LX/2If;-><init>(LX/30I;)V

    .line 392042
    move-object v0, p0

    .line 392043
    sput-object v0, LX/2If;->b:LX/2If;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392044
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 392045
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 392046
    :cond_1
    sget-object v0, LX/2If;->b:LX/2If;

    return-object v0

    .line 392047
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 392048
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 392049
    if-nez p3, :cond_0

    .line 392050
    const/4 v0, 0x0

    .line 392051
    :goto_0
    return-object v0

    .line 392052
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "contacts_upload_state"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 392053
    const-string v1, "contacts_upload"

    .line 392054
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 392055
    const-string v1, "continuous_upload_state"

    iget-object v2, p0, LX/2If;->a:LX/30I;

    .line 392056
    invoke-static {v2}, LX/30I;->d(LX/30I;)V

    .line 392057
    invoke-static {v2}, LX/30I;->c(LX/30I;)LX/0Tn;

    move-result-object p0

    .line 392058
    if-nez p0, :cond_1

    .line 392059
    sget-object p0, LX/03R;->UNSET:LX/03R;

    .line 392060
    :goto_1
    move-object v2, p0

    .line 392061
    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    :cond_1
    iget-object p1, v2, LX/30I;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p1, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object p0

    goto :goto_1
.end method
