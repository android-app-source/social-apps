.class public LX/2yS;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/AE2;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AE3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 481941
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/2yS;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/AE3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 481942
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 481943
    iput-object p1, p0, LX/2yS;->b:LX/0Ot;

    .line 481944
    return-void
.end method

.method public static a(LX/0QB;)LX/2yS;
    .locals 4

    .prologue
    .line 481945
    const-class v1, LX/2yS;

    monitor-enter v1

    .line 481946
    :try_start_0
    sget-object v0, LX/2yS;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 481947
    sput-object v2, LX/2yS;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 481948
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481949
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 481950
    new-instance v3, LX/2yS;

    const/16 p0, 0x1791

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2yS;-><init>(LX/0Ot;)V

    .line 481951
    move-object v0, v3

    .line 481952
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 481953
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2yS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 481954
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 481955
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1De;Landroid/view/View$OnClickListener;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Landroid/view/View$OnClickListener;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 481956
    const v0, 0x4b950c3

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;Landroid/view/View$OnClickListener;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Landroid/view/View$OnClickListener;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 481957
    const v0, 0x4b950c3

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 481958
    check-cast p2, LX/AE1;

    .line 481959
    iget-object v0, p0, LX/2yS;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AE3;

    iget-object v2, p2, LX/AE1;->a:Ljava/lang/CharSequence;

    iget-object v3, p2, LX/AE1;->b:LX/1dc;

    iget v4, p2, LX/AE1;->c:I

    iget-object v5, p2, LX/AE1;->d:LX/1dc;

    iget v6, p2, LX/AE1;->e:I

    iget-boolean v7, p2, LX/AE1;->f:Z

    iget-object v8, p2, LX/AE1;->g:Ljava/lang/CharSequence;

    iget v9, p2, LX/AE1;->h:I

    iget v10, p2, LX/AE1;->i:I

    iget-object v11, p2, LX/AE1;->j:Landroid/util/SparseArray;

    iget-object v12, p2, LX/AE1;->k:Landroid/view/View$OnClickListener;

    move-object v1, p1

    invoke-virtual/range {v0 .. v12}, LX/AE3;->a(LX/1De;Ljava/lang/CharSequence;LX/1dc;ILX/1dc;IZLjava/lang/CharSequence;IILandroid/util/SparseArray;Landroid/view/View$OnClickListener;)LX/1Dg;

    move-result-object v0

    .line 481960
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 481961
    invoke-static {}, LX/1dS;->b()V

    .line 481962
    iget v0, p1, LX/1dQ;->b:I

    .line 481963
    packed-switch v0, :pswitch_data_0

    .line 481964
    :goto_0
    return-object v3

    .line 481965
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 481966
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Landroid/view/View$OnClickListener;

    .line 481967
    iget-object p1, p0, LX/2yS;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/AE3;

    .line 481968
    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 481969
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4b950c3
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/AE2;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 481970
    new-instance v1, LX/AE1;

    invoke-direct {v1, p0}, LX/AE1;-><init>(LX/2yS;)V

    .line 481971
    sget-object v2, LX/2yS;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AE2;

    .line 481972
    if-nez v2, :cond_0

    .line 481973
    new-instance v2, LX/AE2;

    invoke-direct {v2}, LX/AE2;-><init>()V

    .line 481974
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/AE2;->a$redex0(LX/AE2;LX/1De;IILX/AE1;)V

    .line 481975
    move-object v1, v2

    .line 481976
    move-object v0, v1

    .line 481977
    return-object v0
.end method
