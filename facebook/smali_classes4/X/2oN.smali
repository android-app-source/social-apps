.class public final enum LX/2oN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2oN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2oN;

.field public static final enum NONE:LX/2oN;

.field public static final enum STATIC_COUNTDOWN:LX/2oN;

.field public static final enum TRANSITION:LX/2oN;

.field public static final enum VIDEO_AD:LX/2oN;

.field public static final enum VOD_NO_VIDEO_AD:LX/2oN;

.field public static final enum WAIT_FOR_ADS:LX/2oN;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 466210
    new-instance v0, LX/2oN;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/2oN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2oN;->NONE:LX/2oN;

    .line 466211
    new-instance v0, LX/2oN;

    const-string v1, "TRANSITION"

    invoke-direct {v0, v1, v4}, LX/2oN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2oN;->TRANSITION:LX/2oN;

    .line 466212
    new-instance v0, LX/2oN;

    const-string v1, "VIDEO_AD"

    invoke-direct {v0, v1, v5}, LX/2oN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2oN;->VIDEO_AD:LX/2oN;

    .line 466213
    new-instance v0, LX/2oN;

    const-string v1, "WAIT_FOR_ADS"

    invoke-direct {v0, v1, v6}, LX/2oN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2oN;->WAIT_FOR_ADS:LX/2oN;

    .line 466214
    new-instance v0, LX/2oN;

    const-string v1, "STATIC_COUNTDOWN"

    invoke-direct {v0, v1, v7}, LX/2oN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2oN;->STATIC_COUNTDOWN:LX/2oN;

    .line 466215
    new-instance v0, LX/2oN;

    const-string v1, "VOD_NO_VIDEO_AD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/2oN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2oN;->VOD_NO_VIDEO_AD:LX/2oN;

    .line 466216
    const/4 v0, 0x6

    new-array v0, v0, [LX/2oN;

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    aput-object v1, v0, v3

    sget-object v1, LX/2oN;->TRANSITION:LX/2oN;

    aput-object v1, v0, v4

    sget-object v1, LX/2oN;->VIDEO_AD:LX/2oN;

    aput-object v1, v0, v5

    sget-object v1, LX/2oN;->WAIT_FOR_ADS:LX/2oN;

    aput-object v1, v0, v6

    sget-object v1, LX/2oN;->STATIC_COUNTDOWN:LX/2oN;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/2oN;->VOD_NO_VIDEO_AD:LX/2oN;

    aput-object v2, v0, v1

    sput-object v0, LX/2oN;->$VALUES:[LX/2oN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 466209
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2oN;
    .locals 1

    .prologue
    .line 466208
    const-class v0, LX/2oN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2oN;

    return-object v0
.end method

.method public static values()[LX/2oN;
    .locals 1

    .prologue
    .line 466207
    sget-object v0, LX/2oN;->$VALUES:[LX/2oN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2oN;

    return-object v0
.end method
