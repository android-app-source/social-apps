.class public LX/23B;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 361986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361987
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;I)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 361971
    invoke-static {p0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v2

    .line 361972
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-nez v1, :cond_1

    .line 361973
    :cond_0
    :goto_0
    return-object v0

    .line 361974
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->H()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 361975
    if-nez v1, :cond_2

    .line 361976
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 361977
    :cond_2
    if-nez v1, :cond_3

    .line 361978
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->k()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 361979
    :cond_3
    if-eqz v1, :cond_0

    .line 361980
    invoke-static {v1}, LX/2dc;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/2dc;

    move-result-object v0

    .line 361981
    iput p1, v0, LX/2dc;->c:I

    .line 361982
    move-object v0, v0

    .line 361983
    iput p1, v0, LX/2dc;->i:I

    .line 361984
    move-object v0, v0

    .line 361985
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z
    .locals 2

    .prologue
    .line 361968
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 361969
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 361970
    if-nez v0, :cond_2

    invoke-static {p0}, LX/23B;->e(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, LX/23B;->f(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 361959
    invoke-static {p0}, LX/23B;->h(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z
    .locals 1

    .prologue
    .line 361967
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 361966
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;
    .locals 2

    .prologue
    .line 361963
    invoke-static {p0}, LX/23B;->f(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 361964
    const/4 v0, 0x0

    .line 361965
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    goto :goto_0
.end method

.method public static h(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)I
    .locals 1

    .prologue
    .line 361960
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 361961
    :cond_0
    const/4 v0, 0x0

    .line 361962
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
