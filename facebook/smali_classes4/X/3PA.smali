.class public final LX/3PA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/util/Map",
        "<",
        "Lcom/facebook/user/model/UserKey;",
        "LX/6Lx;",
        ">;",
        "LX/3OS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3LL;


# direct methods
.method public constructor <init>(LX/3LL;)V
    .locals 0

    .prologue
    .line 561148
    iput-object p1, p0, LX/3PA;->a:LX/3LL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 561149
    check-cast p1, Ljava/util/Map;

    .line 561150
    iget-object v0, p0, LX/3PA;->a:LX/3LL;

    iget-object v0, v0, LX/3LL;->h:LX/0Uh;

    const/16 v1, 0x534

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 561151
    new-instance v0, LX/3OS;

    sget-object v1, LX/3OT;->CHAT_CONTEXT_DISABLED:LX/3OT;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/3OS;-><init>(LX/3OT;LX/0Px;)V

    move-object v0, v0

    .line 561152
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 561153
    :goto_0
    return-object v0

    .line 561154
    :cond_0
    iget-object v0, p0, LX/3PA;->a:LX/3LL;

    iget-object v0, v0, LX/3LL;->b:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->b()LX/1rv;

    move-result-object v0

    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-eq v0, v1, :cond_1

    .line 561155
    new-instance v0, LX/3OS;

    sget-object v1, LX/3OT;->LOCATION_DISABLED:LX/3OT;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/3OS;-><init>(LX/3OT;LX/0Px;)V

    move-object v0, v0

    .line 561156
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 561157
    :cond_1
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 561158
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 561159
    invoke-static {v0}, LX/3OS;->a(LX/0Px;)LX/3OS;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 561160
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 561161
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 561162
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Lx;

    invoke-interface {v1}, LX/6Lx;->b()Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;

    move-result-object v1

    .line 561163
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ChatContextsGraphQLModels$ChatContextModel;->b()Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->NEARBY:Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    if-ne v1, v4, :cond_4

    .line 561164
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 561165
    :cond_5
    iget-object v0, p0, LX/3PA;->a:LX/3LL;

    iget-object v0, v0, LX/3LL;->d:LX/3LM;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 561166
    iget-object v2, v0, LX/3LM;->e:LX/0TD;

    new-instance v3, LX/DAl;

    invoke-direct {v3, v0, v1}, LX/DAl;-><init>(LX/3LM;LX/0Px;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 561167
    new-instance v1, LX/JHn;

    invoke-direct {v1, p0}, LX/JHn;-><init>(LX/3PA;)V

    iget-object v2, p0, LX/3PA;->a:LX/3LL;

    iget-object v2, v2, LX/3LL;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto/16 :goto_0
.end method
