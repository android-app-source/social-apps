.class public LX/2TH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile p:LX/2TH;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/2TM;

.field private final d:LX/0aG;

.field private final e:LX/2TI;

.field public final f:LX/2TJ;

.field public final g:LX/2TK;

.field public final h:LX/12x;

.field public final i:LX/03V;

.field private final j:LX/2TP;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ERh;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ERr;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ES8;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/01T;

.field private o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 413975
    const-class v0, LX/2TH;

    sput-object v0, LX/2TH;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0aG;LX/2TI;LX/2TJ;LX/2TK;LX/2TM;LX/12x;LX/03V;LX/2TP;LX/0Ot;LX/0Ot;LX/0Ot;LX/01T;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0aG;",
            "LX/2TI;",
            "LX/2TJ;",
            "LX/2TK;",
            "LX/2TM;",
            "Lcom/facebook/common/alarm/FbAlarmManager;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2TP;",
            "LX/0Ot",
            "<",
            "LX/ERh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ERr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ES8;",
            ">;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413895
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2TH;->o:Z

    .line 413896
    iput-object p1, p0, LX/2TH;->b:Landroid/content/Context;

    .line 413897
    iput-object p2, p0, LX/2TH;->d:LX/0aG;

    .line 413898
    iput-object p3, p0, LX/2TH;->e:LX/2TI;

    .line 413899
    iput-object p4, p0, LX/2TH;->f:LX/2TJ;

    .line 413900
    iput-object p5, p0, LX/2TH;->g:LX/2TK;

    .line 413901
    iput-object p6, p0, LX/2TH;->c:LX/2TM;

    .line 413902
    iput-object p7, p0, LX/2TH;->h:LX/12x;

    .line 413903
    iput-object p8, p0, LX/2TH;->i:LX/03V;

    .line 413904
    iput-object p9, p0, LX/2TH;->j:LX/2TP;

    .line 413905
    iput-object p10, p0, LX/2TH;->k:LX/0Ot;

    .line 413906
    iput-object p11, p0, LX/2TH;->l:LX/0Ot;

    .line 413907
    iput-object p12, p0, LX/2TH;->m:LX/0Ot;

    .line 413908
    iput-object p13, p0, LX/2TH;->n:LX/01T;

    .line 413909
    return-void
.end method

.method public static a(LX/0QB;)LX/2TH;
    .locals 3

    .prologue
    .line 413965
    sget-object v0, LX/2TH;->p:LX/2TH;

    if-nez v0, :cond_1

    .line 413966
    const-class v1, LX/2TH;

    monitor-enter v1

    .line 413967
    :try_start_0
    sget-object v0, LX/2TH;->p:LX/2TH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413968
    if-eqz v2, :cond_0

    .line 413969
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2TH;->b(LX/0QB;)LX/2TH;

    move-result-object v0

    sput-object v0, LX/2TH;->p:LX/2TH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413970
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413971
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413972
    :cond_1
    sget-object v0, LX/2TH;->p:LX/2TH;

    return-object v0

    .line 413973
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413974
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/2TH;
    .locals 14

    .prologue
    .line 413963
    new-instance v0, LX/2TH;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v2

    check-cast v2, LX/0aG;

    invoke-static {p0}, LX/2TI;->a(LX/0QB;)LX/2TI;

    move-result-object v3

    check-cast v3, LX/2TI;

    invoke-static {p0}, LX/2TJ;->a(LX/0QB;)LX/2TJ;

    move-result-object v4

    check-cast v4, LX/2TJ;

    invoke-static {p0}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v5

    check-cast v5, LX/2TK;

    invoke-static {p0}, LX/2TM;->c(LX/0QB;)LX/2TM;

    move-result-object v6

    check-cast v6, LX/2TM;

    invoke-static {p0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v7

    check-cast v7, LX/12x;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {p0}, LX/2TP;->a(LX/0QB;)LX/2TP;

    move-result-object v9

    check-cast v9, LX/2TP;

    const/16 v10, 0x37a4

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x37a9

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x37ae

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v13

    check-cast v13, LX/01T;

    invoke-direct/range {v0 .. v13}, LX/2TH;-><init>(Landroid/content/Context;LX/0aG;LX/2TI;LX/2TJ;LX/2TK;LX/2TM;LX/12x;LX/03V;LX/2TP;LX/0Ot;LX/0Ot;LX/0Ot;LX/01T;)V

    .line 413964
    return-object v0
.end method

.method public static e(LX/2TH;)V
    .locals 4

    .prologue
    .line 413961
    iget-object v0, p0, LX/2TH;->b:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/2TH;->b:Landroid/content/Context;

    const-class v3, Lcom/facebook/vault/service/VaultObserverService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 413962
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 413976
    iget-object v0, p0, LX/2TH;->b:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/2TH;->b:Landroid/content/Context;

    const-class v3, Lcom/facebook/vault/service/VaultObserverService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 413977
    iget-object v0, p0, LX/2TH;->b:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/2TH;->b:Landroid/content/Context;

    const-class v3, Lcom/facebook/vault/service/VaultSyncService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 413978
    iget-object v0, p0, LX/2TH;->b:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/2TH;->b:Landroid/content/Context;

    const-class v3, Lcom/facebook/vault/service/VaultSyncJobProcessor;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 413979
    iget-object v0, p0, LX/2TH;->g:LX/2TK;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/2TK;->b(I)Landroid/content/Intent;

    move-result-object v0

    .line 413980
    iget-object v1, p0, LX/2TH;->b:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x20000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 413981
    if-eqz v0, :cond_0

    .line 413982
    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 413983
    iget-object v1, p0, LX/2TH;->h:LX/12x;

    invoke-virtual {v1, v0}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 413984
    :cond_0
    new-instance v0, Lcom/facebook/vault/service/VaultManager$2;

    invoke-direct {v0, p0}, Lcom/facebook/vault/service/VaultManager$2;-><init>(LX/2TH;)V

    const v1, -0x7740280d

    invoke-static {v0, v1}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 413985
    iget-object v0, p0, LX/2TH;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ERr;

    invoke-virtual {v0}, LX/ERr;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "end_vault_upload"

    if-eq v0, v1, :cond_1

    .line 413986
    iget-object v0, p0, LX/2TH;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ERr;

    invoke-virtual {v0}, LX/ERr;->c()V

    .line 413987
    :cond_1
    iget-object v0, p0, LX/2TH;->j:LX/2TP;

    invoke-virtual {v0}, LX/2TP;->b()V

    .line 413988
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 413941
    iget-object v0, p0, LX/2TH;->e:LX/2TI;

    invoke-virtual {v0}, LX/2TI;->a()Ljava/lang/String;

    move-result-object v0

    .line 413942
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sync mode changed to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413943
    iget-object v1, p0, LX/2TH;->c:LX/2TM;

    .line 413944
    const-string v2, "OFF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 413945
    sget-object v2, LX/2TM;->l:Ljava/lang/String;

    invoke-static {v1, v2}, LX/2TM;->e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 413946
    :goto_0
    const-string v3, "mode"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 413947
    invoke-static {v1, v2}, LX/2TM;->a(LX/2TM;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 413948
    iget-object v3, v1, LX/2TM;->y:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 413949
    iget-object v0, p0, LX/2TH;->g:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413950
    iget-object v0, p0, LX/2TH;->j:LX/2TP;

    invoke-virtual {v0}, LX/2TP;->a()V

    .line 413951
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2TH;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/vault/service/VaultManagerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 413952
    sget-object v1, Lcom/facebook/vault/service/VaultManagerService;->a:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 413953
    iget-object v1, p0, LX/2TH;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 413954
    :goto_1
    return-void

    .line 413955
    :cond_0
    iget-object v0, p0, LX/2TH;->j:LX/2TP;

    invoke-virtual {v0}, LX/2TP;->b()V

    .line 413956
    iget-object v0, p0, LX/2TH;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ERh;

    invoke-virtual {v0}, LX/ERh;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 413957
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2TH;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/vault/service/VaultUpdateService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 413958
    iget-object v1, p0, LX/2TH;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 413959
    :cond_1
    invoke-direct {p0}, LX/2TH;->f()V

    goto :goto_1

    .line 413960
    :cond_2
    sget-object v2, LX/2TM;->k:Ljava/lang/String;

    invoke-static {v1, v2}, LX/2TM;->e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    goto :goto_0
.end method

.method public final b()V
    .locals 12

    .prologue
    .line 413932
    iget-object v0, p0, LX/2TH;->f:LX/2TJ;

    .line 413933
    iget-object v4, v0, LX/2TJ;->c:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 413934
    iget-object v8, v0, LX/2TJ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, LX/2TR;->k:LX/0Tn;

    const-wide/16 v10, 0x0

    invoke-interface {v8, v9, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v8

    move-wide v6, v8

    .line 413935
    sub-long/2addr v4, v6

    const-wide/32 v6, 0x240c8400

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    move v0, v4

    .line 413936
    if-eqz v0, :cond_0

    .line 413937
    iget-object v0, p0, LX/2TH;->d:LX/0aG;

    const-string v1, "fetch_blacklisted_sync_paths"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const v3, 0x24c8237d

    invoke-static {v0, v1, v2, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 413938
    new-instance v1, LX/ERn;

    invoke-direct {v1, p0}, LX/ERn;-><init>(LX/2TH;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 413939
    :goto_1
    return-void

    .line 413940
    :cond_0
    invoke-static {p0}, LX/2TH;->e(LX/2TH;)V

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 4

    .prologue
    .line 413923
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2TH;->o:Z

    .line 413924
    invoke-direct {p0}, LX/2TH;->f()V

    .line 413925
    iget-object v0, p0, LX/2TH;->b:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 413926
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 413927
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/2TH;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":vault"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 413928
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 413929
    iget-object v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 413930
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0

    .line 413931
    :cond_1
    return-void
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 413910
    iget-object v0, p0, LX/2TH;->n:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-le v0, v1, :cond_1

    .line 413911
    :cond_0
    :goto_0
    return-void

    .line 413912
    :cond_1
    iget-boolean v0, p0, LX/2TH;->o:Z

    if-nez v0, :cond_3

    .line 413913
    iget-object v0, p0, LX/2TH;->g:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 413914
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2TH;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/vault/service/VaultManagerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 413915
    sget-object v1, Lcom/facebook/vault/service/VaultManagerService;->a:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 413916
    iget-object v1, p0, LX/2TH;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 413917
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2TH;->o:Z

    .line 413918
    :cond_2
    :goto_1
    iget-object v0, p0, LX/2TH;->g:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 413919
    iget-object v0, p0, LX/2TH;->j:LX/2TP;

    invoke-virtual {v0}, LX/2TP;->a()V

    goto :goto_0

    .line 413920
    :cond_3
    iget-object v0, p0, LX/2TH;->g:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 413921
    invoke-virtual {p0}, LX/2TH;->b()V

    goto :goto_1

    .line 413922
    :cond_4
    iget-object v0, p0, LX/2TH;->j:LX/2TP;

    invoke-virtual {v0}, LX/2TP;->b()V

    goto :goto_0
.end method
