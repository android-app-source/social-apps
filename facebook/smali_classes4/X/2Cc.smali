.class public LX/2Cc;
.super LX/0SQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0SQ",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/wifiscan/WifiScanResult;",
        ">;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:LX/1rc;

.field private final c:Landroid/net/wifi/WifiManager;

.field private final d:LX/0SG;

.field private final e:LX/0So;

.field private final f:LX/0Xl;

.field public final g:Ljava/util/concurrent/ScheduledExecutorService;

.field private final h:LX/2Cg;

.field public i:LX/1sO;

.field private j:Z

.field public k:Ljava/util/concurrent/ScheduledFuture;

.field private l:LX/0Yb;


# direct methods
.method public constructor <init>(Landroid/os/Handler;LX/1rc;Landroid/net/wifi/WifiManager;LX/0So;LX/0SG;LX/2Cg;LX/0Xl;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/location/threading/ForLocationNonUiThread;
        .end annotation
    .end param
    .param p4    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/GlobalFbBroadcast;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForLightweightTaskHandlerThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 383099
    invoke-direct {p0}, LX/0SQ;-><init>()V

    .line 383100
    iput-object p1, p0, LX/2Cc;->a:Landroid/os/Handler;

    .line 383101
    iput-object p2, p0, LX/2Cc;->b:LX/1rc;

    .line 383102
    iput-object p3, p0, LX/2Cc;->c:Landroid/net/wifi/WifiManager;

    .line 383103
    iput-object p5, p0, LX/2Cc;->d:LX/0SG;

    .line 383104
    iput-object p6, p0, LX/2Cc;->h:LX/2Cg;

    .line 383105
    iput-object p4, p0, LX/2Cc;->e:LX/0So;

    .line 383106
    iput-object p7, p0, LX/2Cc;->f:LX/0Xl;

    .line 383107
    iput-object p8, p0, LX/2Cc;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 383108
    return-void
.end method

.method private static a(LX/2Cc;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 383096
    invoke-direct {p0}, LX/2Cc;->e()V

    .line 383097
    invoke-virtual {p0, p1}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z

    .line 383098
    return-void
.end method

.method private static a(LX/2Cc;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/wifiscan/WifiScanResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 383093
    invoke-direct {p0}, LX/2Cc;->e()V

    .line 383094
    invoke-virtual {p0, p1}, LX/0SQ;->set(Ljava/lang/Object;)Z

    .line 383095
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/2Cc;)V
    .locals 6

    .prologue
    .line 383085
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2Cc;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 383086
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 383087
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2Cc;->c:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    .line 383088
    iget-object v1, p0, LX/2Cc;->h:LX/2Cg;

    invoke-virtual {v1, v0}, LX/2Cg;->a(Ljava/util/List;)V

    .line 383089
    iget-object v1, p0, LX/2Cc;->i:LX/1sO;

    iget-wide v2, v1, LX/1sO;->b:J

    iget-object v1, p0, LX/2Cc;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    invoke-static {v0, v2, v3, v4, v5}, LX/2zX;->a(Ljava/util/List;JJ)Ljava/util/List;

    move-result-object v0

    .line 383090
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 383091
    iget-object v1, p0, LX/2Cc;->d:LX/0SG;

    iget-object v2, p0, LX/2Cc;->e:LX/0So;

    invoke-static {v0, v1, v2}, Lcom/facebook/wifiscan/WifiScanResult;->a(Ljava/util/List;LX/0SG;LX/0So;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/2Cc;->a(LX/2Cc;Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 383092
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(LX/1sO;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 383056
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 383057
    iget-boolean v0, p0, LX/2Cc;->j:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "already running"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 383058
    invoke-virtual {p0}, LX/0SQ;->isDone()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "already done"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 383059
    iput-boolean v1, p0, LX/2Cc;->j:Z

    .line 383060
    iput-object p1, p0, LX/2Cc;->i:LX/1sO;

    .line 383061
    iget-object v0, p0, LX/2Cc;->b:LX/1rc;

    invoke-virtual {v0}, LX/1rc;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 383062
    new-instance v0, LX/2x2;

    sget-object v1, LX/2x3;->NOT_SUPPORTED:LX/2x3;

    invoke-direct {v0, v1}, LX/2x2;-><init>(LX/2x3;)V

    throw v0

    :cond_0
    move v0, v2

    .line 383063
    goto :goto_0

    :cond_1
    move v0, v2

    .line 383064
    goto :goto_1

    .line 383065
    :cond_2
    invoke-static {}, LX/1rc;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 383066
    new-instance v0, LX/2x2;

    sget-object v1, LX/2x3;->NOT_SUPPORTED:LX/2x3;

    invoke-direct {v0, v1}, LX/2x2;-><init>(LX/2x3;)V

    throw v0

    .line 383067
    :cond_3
    iget-object v0, p0, LX/2Cc;->b:LX/1rc;

    invoke-virtual {v0}, LX/1rc;->c()Z

    move-result v0

    if-nez v0, :cond_4

    .line 383068
    new-instance v0, LX/2x2;

    sget-object v1, LX/2x3;->PERMISSION_DENIED:LX/2x3;

    invoke-direct {v0, v1}, LX/2x2;-><init>(LX/2x3;)V

    throw v0

    .line 383069
    :cond_4
    iget-object v0, p0, LX/2Cc;->b:LX/1rc;

    invoke-virtual {v0}, LX/1rc;->d()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, LX/2Cc;->b:LX/1rc;

    invoke-virtual {v0}, LX/1rc;->e()LX/03R;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_5

    .line 383070
    new-instance v0, LX/2x2;

    sget-object v1, LX/2x3;->USER_DISABLED:LX/2x3;

    invoke-direct {v0, v1}, LX/2x2;-><init>(LX/2x3;)V

    throw v0

    .line 383071
    :cond_5
    iget-object v0, p0, LX/2Cc;->c:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    .line 383072
    iget-object v1, p0, LX/2Cc;->h:LX/2Cg;

    invoke-virtual {v1, v0}, LX/2Cg;->a(Ljava/util/List;)V

    .line 383073
    iget-object v1, p0, LX/2Cc;->i:LX/1sO;

    iget-wide v2, v1, LX/1sO;->b:J

    iget-object v1, p0, LX/2Cc;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    invoke-static {v0, v2, v3, v4, v5}, LX/2zX;->a(Ljava/util/List;JJ)Ljava/util/List;

    move-result-object v0

    .line 383074
    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 383075
    iget-object v1, p0, LX/2Cc;->d:LX/0SG;

    iget-object v2, p0, LX/2Cc;->e:LX/0So;

    invoke-static {v0, v1, v2}, Lcom/facebook/wifiscan/WifiScanResult;->a(Ljava/util/List;LX/0SG;LX/0So;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/2Cc;->a(LX/2Cc;Ljava/util/List;)V

    .line 383076
    :cond_6
    return-void

    .line 383077
    :cond_7
    iget-object v0, p0, LX/2Cc;->i:LX/1sO;

    iget-wide v0, v0, LX/1sO;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_8

    .line 383078
    new-instance v0, LX/2x2;

    sget-object v1, LX/2x3;->TIMEOUT:LX/2x3;

    invoke-direct {v0, v1}, LX/2x2;-><init>(LX/2x3;)V

    throw v0

    .line 383079
    :cond_8
    iget-object v6, p0, LX/2Cc;->g:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v7, Lcom/facebook/wifiscan/WifiScanOperation$2;

    invoke-direct {v7, p0}, Lcom/facebook/wifiscan/WifiScanOperation$2;-><init>(LX/2Cc;)V

    iget-object v8, p0, LX/2Cc;->i:LX/1sO;

    iget-wide v8, v8, LX/1sO;->a:J

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v6, v7, v8, v9, v10}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v6

    iput-object v6, p0, LX/2Cc;->k:Ljava/util/concurrent/ScheduledFuture;

    .line 383080
    iget-object v0, p0, LX/2Cc;->f:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/2Cc;->a:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    const-string v1, "android.net.wifi.SCAN_RESULTS"

    new-instance v2, LX/2Cj;

    invoke-direct {v2, p0}, LX/2Cj;-><init>(LX/2Cc;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/2Cc;->l:LX/0Yb;

    .line 383081
    iget-object v0, p0, LX/2Cc;->l:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 383082
    iget-object v0, p0, LX/2Cc;->c:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    move-result v0

    .line 383083
    if-nez v0, :cond_6

    .line 383084
    new-instance v0, LX/2x2;

    sget-object v1, LX/2x3;->UNKNOWN_ERROR:LX/2x3;

    invoke-direct {v0, v1}, LX/2x2;-><init>(LX/2x3;)V

    throw v0
.end method

.method public static declared-synchronized b$redex0(LX/2Cc;)V
    .locals 2

    .prologue
    .line 383052
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/2Cc;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 383053
    :goto_0
    monitor-exit p0

    return-void

    .line 383054
    :cond_0
    :try_start_1
    new-instance v0, LX/2x2;

    sget-object v1, LX/2x3;->TIMEOUT:LX/2x3;

    invoke-direct {v0, v1}, LX/2x2;-><init>(LX/2x3;)V

    invoke-static {p0, v0}, LX/2Cc;->a(LX/2Cc;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 383055
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 383037
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Cc;->j:Z

    .line 383038
    iput-object v1, p0, LX/2Cc;->i:LX/1sO;

    .line 383039
    iget-object v0, p0, LX/2Cc;->l:LX/0Yb;

    if-eqz v0, :cond_1

    .line 383040
    iget-object v0, p0, LX/2Cc;->l:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383041
    iget-object v0, p0, LX/2Cc;->l:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 383042
    :cond_0
    iput-object v1, p0, LX/2Cc;->l:LX/0Yb;

    .line 383043
    :cond_1
    iget-object v0, p0, LX/2Cc;->k:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_2

    .line 383044
    :goto_0
    return-void

    .line 383045
    :cond_2
    iget-object v0, p0, LX/2Cc;->k:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 383046
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Cc;->k:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(LX/1sO;)V
    .locals 1

    .prologue
    .line 383047
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, LX/2Cc;->b(LX/1sO;)V
    :try_end_0
    .catch LX/2x2; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383048
    :goto_0
    monitor-exit p0

    return-void

    .line 383049
    :catch_0
    move-exception v0

    .line 383050
    :try_start_1
    invoke-static {p0, v0}, LX/2Cc;->a(LX/2Cc;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 383051
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
