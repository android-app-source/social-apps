.class public LX/3FA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/3FA;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dM;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Jvz;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y2;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3FB;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0bW;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationProcessor;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2dM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Jvz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0y2;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3FB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0bW;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/placetips/gpscore/PlaceTipsGpsLocationProcessor;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 538575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538576
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3FA;->i:Z

    .line 538577
    iput-object p1, p0, LX/3FA;->a:LX/0Ot;

    .line 538578
    iput-object p2, p0, LX/3FA;->b:LX/0Ot;

    .line 538579
    iput-object p3, p0, LX/3FA;->c:LX/0Ot;

    .line 538580
    iput-object p4, p0, LX/3FA;->d:LX/0Ot;

    .line 538581
    iput-object p5, p0, LX/3FA;->e:LX/0Ot;

    .line 538582
    iput-object p6, p0, LX/3FA;->f:LX/0Ot;

    .line 538583
    iput-object p7, p0, LX/3FA;->g:LX/0Ot;

    .line 538584
    iput-object p8, p0, LX/3FA;->h:LX/0Ot;

    .line 538585
    return-void
.end method

.method public static a(LX/0QB;)LX/3FA;
    .locals 12

    .prologue
    .line 538562
    sget-object v0, LX/3FA;->j:LX/3FA;

    if-nez v0, :cond_1

    .line 538563
    const-class v1, LX/3FA;

    monitor-enter v1

    .line 538564
    :try_start_0
    sget-object v0, LX/3FA;->j:LX/3FA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 538565
    if-eqz v2, :cond_0

    .line 538566
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 538567
    new-instance v3, LX/3FA;

    const/16 v4, 0xf5a

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2f67

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xc7e

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xf56

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xf58

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2f66

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x12b1

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1032

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, LX/3FA;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 538568
    move-object v0, v3

    .line 538569
    sput-object v0, LX/3FA;->j:LX/3FA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 538570
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 538571
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 538572
    :cond_1
    sget-object v0, LX/3FA;->j:LX/3FA;

    return-object v0

    .line 538573
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 538574
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3FA;Lcom/facebook/location/ImmutableLocation;)V
    .locals 12
    .param p0    # LX/3FA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 538586
    if-nez p1, :cond_0

    .line 538587
    invoke-static {}, LX/0Vg;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {p0, v0}, LX/3FA;->a$redex0(LX/3FA;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 538588
    :goto_0
    return-void

    .line 538589
    :cond_0
    iget-object v0, p0, LX/3FA;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0bW;

    const-string v1, "New Location reported: %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-interface {v0, v1, v2}, LX/0bW;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538590
    iget-object v0, p0, LX/3FA;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3FB;

    .line 538591
    sget-object v1, LX/3FC;->START_PAGE_LOOKUP:LX/3FC;

    invoke-virtual {v0, v1}, LX/3FB;->a(LX/3FC;)V

    .line 538592
    iget-object v1, p0, LX/3FA;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jvz;

    .line 538593
    if-nez p1, :cond_1

    .line 538594
    new-instance v6, Ljava/lang/NullPointerException;

    const-string v7, "Can\'t get location trigger for null location"

    invoke-direct {v6, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 538595
    :goto_1
    move-object v2, v6

    .line 538596
    const/4 v1, 0x2

    new-array v3, v1, [Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, LX/3FA;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v1, v3, v4

    iget-object v1, p0, LX/3FA;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jvw;

    .line 538597
    invoke-static {p1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v6, LX/Jvt;

    invoke-direct {v6, v1}, LX/Jvt;-><init>(LX/Jvw;)V

    iget-object v7, v1, LX/Jvw;->a:Ljava/util/concurrent/Executor;

    invoke-static {v4, v6, v7}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v1, v4

    .line 538598
    aput-object v1, v3, v5

    invoke-static {v3}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 538599
    iget-object v1, p0, LX/3FA;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    sget-object v4, LX/3FG;->PROCESS_LOCATION:LX/3FG;

    new-instance v5, LX/Jw2;

    invoke-direct {v5, p0, v0, p1, v2}, LX/Jw2;-><init>(LX/3FA;LX/3FB;Lcom/facebook/location/ImmutableLocation;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v1, v4, v3, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 538600
    :cond_1
    new-instance v6, LX/3Aj;

    invoke-direct {v6}, LX/3Aj;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    move-result-object v7

    .line 538601
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v6

    invoke-virtual {v6}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 538602
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v6

    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    .line 538603
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->f()LX/0am;

    move-result-object v6

    .line 538604
    invoke-virtual {v6}, LX/0am;->isPresent()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 538605
    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/3Aj;->d(Ljava/lang/Double;)LX/3Aj;

    .line 538606
    :cond_3
    iget-object v6, v1, LX/Jvz;->b:LX/0yD;

    invoke-virtual {v6, p1}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v8

    .line 538607
    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-ltz v6, :cond_4

    .line 538608
    invoke-static {v8, v9}, LX/1lQ;->n(J)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/3Aj;->e(Ljava/lang/Double;)LX/3Aj;

    .line 538609
    :cond_4
    new-instance v6, LX/4Go;

    invoke-direct {v6}, LX/4Go;-><init>()V

    .line 538610
    const-string v8, "viewer_coordinates"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 538611
    move-object v6, v6

    .line 538612
    new-instance v7, LX/Cdw;

    invoke-direct {v7}, LX/Cdw;-><init>()V

    move-object v7, v7

    .line 538613
    const-string v8, "query_data"

    invoke-virtual {v7, v8, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v8

    const-string v9, "supported_client_interfaces"

    iget-object v6, v1, LX/Jvz;->c:LX/0QR;

    invoke-interface {v6}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v9, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 538614
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    sget-object v7, LX/0zS;->c:LX/0zS;

    invoke-virtual {v6, v7}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    .line 538615
    iget-object v7, v1, LX/Jvz;->a:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    new-instance v7, LX/Jvy;

    invoke-direct {v7, v1}, LX/Jvy;-><init>(LX/Jvz;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v8

    invoke-static {v6, v7, v8}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    goto/16 :goto_1
.end method

.method public static a$redex0(LX/3FA;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLInterfaces$LocationTriggerWithReactionUnits;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 538560
    iget-object v0, p0, LX/3FA;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    sget-object v2, LX/3FG;->PROCESS_LOCATION:LX/3FG;

    const/4 v1, 0x2

    new-array v3, v1, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v4, 0x0

    iget-object v1, p0, LX/3FA;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object p1, v3, v1

    invoke-static {v3}, LX/0Vg;->b([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v3, LX/3FH;

    invoke-direct {v3, p0}, LX/3FH;-><init>(LX/3FA;)V

    invoke-virtual {v0, v2, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 538561
    return-void
.end method

.method public static e(LX/3FA;)V
    .locals 4

    .prologue
    .line 538557
    iget-object v0, p0, LX/3FA;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0y2;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, LX/0y2;->a(J)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 538558
    invoke-static {p0, v0}, LX/3FA;->a(LX/3FA;Lcom/facebook/location/ImmutableLocation;)V

    .line 538559
    return-void
.end method
