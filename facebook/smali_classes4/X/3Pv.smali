.class public LX/3Pv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Pw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/3Pv;


# instance fields
.field private final b:LX/0Uh;

.field private final c:LX/2Ct;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 562339
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/notifications/constants/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->REQUEST_LOCATION_UPDATE:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/3Pv;->a:LX/2QP;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/2Ct;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562341
    iput-object p1, p0, LX/3Pv;->b:LX/0Uh;

    .line 562342
    iput-object p2, p0, LX/3Pv;->c:LX/2Ct;

    .line 562343
    return-void
.end method

.method public static a(LX/0QB;)LX/3Pv;
    .locals 5

    .prologue
    .line 562344
    sget-object v0, LX/3Pv;->d:LX/3Pv;

    if-nez v0, :cond_1

    .line 562345
    const-class v1, LX/3Pv;

    monitor-enter v1

    .line 562346
    :try_start_0
    sget-object v0, LX/3Pv;->d:LX/3Pv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 562347
    if-eqz v2, :cond_0

    .line 562348
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 562349
    new-instance p0, LX/3Pv;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/2Ct;->a(LX/0QB;)LX/2Ct;

    move-result-object v4

    check-cast v4, LX/2Ct;

    invoke-direct {p0, v3, v4}, LX/3Pv;-><init>(LX/0Uh;LX/2Ct;)V

    .line 562350
    move-object v0, p0

    .line 562351
    sput-object v0, LX/3Pv;->d:LX/3Pv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562352
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 562353
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 562354
    :cond_1
    sget-object v0, LX/3Pv;->d:LX/3Pv;

    return-object v0

    .line 562355
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 562356
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/0lF;)V
    .locals 12

    .prologue
    .line 562357
    const-string v0, "params"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 562358
    iget-object v1, p0, LX/3Pv;->c:LX/2Ct;

    invoke-static {v0}, LX/3Pv;->b(LX/0lF;)Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;

    move-result-object v0

    const/4 v5, 0x0

    .line 562359
    iget-object v2, v1, LX/2Ct;->k:LX/2Cx;

    .line 562360
    iget-object v6, v2, LX/2Cx;->b:LX/0Zb;

    const-string v7, "background_location_obtain_single_location_start"

    invoke-static {v7}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "delay_distance_meters"

    iget v9, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->a:F

    float-to-double v10, v9

    invoke-virtual {v7, v8, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "trace_id"

    iget-object v9, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->b:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "priority"

    iget-object v9, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->c:Lcom/facebook/location/FbLocationOperationParams;

    iget-object v9, v9, Lcom/facebook/location/FbLocationOperationParams;->a:LX/0yF;

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "desired_age_ms"

    iget-object v9, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->c:Lcom/facebook/location/FbLocationOperationParams;

    iget-wide v10, v9, Lcom/facebook/location/FbLocationOperationParams;->b:J

    invoke-virtual {v7, v8, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "desired_accuracy_meters"

    iget-object v9, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->c:Lcom/facebook/location/FbLocationOperationParams;

    iget v9, v9, Lcom/facebook/location/FbLocationOperationParams;->c:F

    float-to-double v10, v9

    invoke-virtual {v7, v8, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "timeout_ms"

    iget-object v9, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->c:Lcom/facebook/location/FbLocationOperationParams;

    iget-wide v10, v9, Lcom/facebook/location/FbLocationOperationParams;->d:J

    invoke-virtual {v7, v8, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 562361
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 562362
    iget-object v3, v1, LX/2Ct;->i:LX/0aU;

    const-string v4, "BACKGROUND_LOCATION_REPORTING_ACTION_OBTAIN_SINGLE_LOCATION_FINISHED"

    invoke-virtual {v3, v4}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 562363
    iget-object v3, v1, LX/2Ct;->h:Landroid/content/Context;

    invoke-static {v3, v5, v2, v5}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 562364
    iget-object v3, v1, LX/2Ct;->h:Landroid/content/Context;

    invoke-static {v3, v0, v2}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->a(Landroid/content/Context;Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;Landroid/app/PendingIntent;)V

    .line 562365
    return-void
.end method

.method private static b(LX/0lF;)Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 562366
    if-nez p0, :cond_0

    .line 562367
    new-instance p0, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {p0, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 562368
    :cond_0
    sget-object v0, LX/0yF;->BALANCED_POWER_AND_ACCURACY:LX/0yF;

    .line 562369
    const-string v1, "lg"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, LX/16N;->a(LX/0lF;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 562370
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    .line 562371
    :cond_1
    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-string v1, "lam"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const/high16 v2, 0x42480000    # 50.0f

    invoke-static {v1, v2}, LX/16N;->a(LX/0lF;F)F

    move-result v1

    .line 562372
    iput v1, v0, LX/1S7;->c:F

    .line 562373
    move-object v0, v0

    .line 562374
    const-string v1, "lda"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-wide/16 v2, 0x3a98

    invoke-static {v1, v2, v3}, LX/16N;->a(LX/0lF;J)J

    move-result-wide v2

    .line 562375
    iput-wide v2, v0, LX/1S7;->b:J

    .line 562376
    move-object v0, v0

    .line 562377
    const-string v1, "lt"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-wide/16 v2, 0x1b58

    invoke-static {v1, v2, v3}, LX/16N;->a(LX/0lF;J)J

    move-result-wide v2

    .line 562378
    iput-wide v2, v0, LX/1S7;->d:J

    .line 562379
    move-object v0, v0

    .line 562380
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    .line 562381
    new-instance v1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;

    const-string v2, "ld"

    invoke-virtual {p0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/16N;->a(LX/0lF;F)F

    move-result v2

    const-string v3, "li"

    invoke-virtual {p0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;-><init>(FLjava/lang/String;Lcom/facebook/location/FbLocationOperationParams;)V

    return-object v1
.end method


# virtual methods
.method public final a()LX/2QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562382
    sget-object v0, LX/3Pv;->a:LX/2QP;

    return-object v0
.end method

.method public final a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 2

    .prologue
    .line 562383
    iget-object v0, p0, LX/3Pv;->b:LX/0Uh;

    const/16 v1, 0x2e9

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_0

    .line 562384
    :goto_0
    return-void

    .line 562385
    :cond_0
    invoke-direct {p0, p1}, LX/3Pv;->a(LX/0lF;)V

    goto :goto_0
.end method
