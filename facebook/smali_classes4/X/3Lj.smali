.class public LX/3Lj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:LX/3Lj;


# instance fields
.field public final b:LX/3Lk;

.field private final c:LX/0Sh;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private final e:LX/0Wd;

.field public final f:LX/0Xl;

.field private g:I

.field private h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcVoicemailInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/lang/Object;

.field private final j:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final k:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 551967
    const-class v0, LX/3Lj;

    sput-object v0, LX/3Lj;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3Lk;LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0Xl;LX/0Wd;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p5    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 551956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 551957
    const/4 v0, -0x1

    iput v0, p0, LX/3Lj;->g:I

    .line 551958
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/3Lj;->i:Ljava/lang/Object;

    .line 551959
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/3Lj;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 551960
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/3Lj;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 551961
    iput-object p1, p0, LX/3Lj;->b:LX/3Lk;

    .line 551962
    iput-object p2, p0, LX/3Lj;->c:LX/0Sh;

    .line 551963
    iput-object p3, p0, LX/3Lj;->d:Ljava/util/concurrent/ExecutorService;

    .line 551964
    iput-object p4, p0, LX/3Lj;->f:LX/0Xl;

    .line 551965
    iput-object p5, p0, LX/3Lj;->e:LX/0Wd;

    .line 551966
    return-void
.end method

.method public static a(LX/0QB;)LX/3Lj;
    .locals 9

    .prologue
    .line 551943
    sget-object v0, LX/3Lj;->l:LX/3Lj;

    if-nez v0, :cond_1

    .line 551944
    const-class v1, LX/3Lj;

    monitor-enter v1

    .line 551945
    :try_start_0
    sget-object v0, LX/3Lj;->l:LX/3Lj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 551946
    if-eqz v2, :cond_0

    .line 551947
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 551948
    new-instance v3, LX/3Lj;

    invoke-static {v0}, LX/3Lk;->a(LX/0QB;)LX/3Lk;

    move-result-object v4

    check-cast v4, LX/3Lk;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v8

    check-cast v8, LX/0Wd;

    invoke-direct/range {v3 .. v8}, LX/3Lj;-><init>(LX/3Lk;LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0Xl;LX/0Wd;)V

    .line 551949
    move-object v0, v3

    .line 551950
    sput-object v0, LX/3Lj;->l:LX/3Lj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551951
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 551952
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551953
    :cond_1
    sget-object v0, LX/3Lj;->l:LX/3Lj;

    return-object v0

    .line 551954
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 551955
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3Lj;I)V
    .locals 2

    .prologue
    .line 551932
    iget-object v1, p0, LX/3Lj;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 551933
    :try_start_0
    iget v0, p0, LX/3Lj;->g:I

    if-ne p1, v0, :cond_0

    .line 551934
    monitor-exit v1

    .line 551935
    :goto_0
    return-void

    .line 551936
    :cond_0
    iput p1, p0, LX/3Lj;->g:I

    .line 551937
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 551938
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 551939
    const-string v1, "VOICEMAIL_LOG_BADGE_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 551940
    iget-object v1, p0, LX/3Lj;->f:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 551941
    goto :goto_0

    .line 551942
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(LX/3Lj;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcVoicemailInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 551924
    iget-object v1, p0, LX/3Lj;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 551925
    :try_start_0
    iput-object p1, p0, LX/3Lj;->h:LX/0Px;

    .line 551926
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 551927
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 551928
    const-string v1, "VOICEMAIL_LOG_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 551929
    iget-object v1, p0, LX/3Lj;->f:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 551930
    return-void

    .line 551931
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static e(LX/3Lj;)LX/0Px;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/models/RtcVoicemailInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551884
    iget-object v0, p0, LX/3Lj;->c:LX/0Sh;

    const-string v1, "Recent Voicemails DB accessed from UI Thread"

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 551885
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 551886
    iget-object v0, p0, LX/3Lj;->b:LX/3Lk;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 551887
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v1

    if-nez v1, :cond_1

    .line 551888
    :cond_0
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 551889
    :goto_0
    return-object v0

    .line 551890
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 551891
    const-string v2, "voicemail_played"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551892
    const-string v2, " = 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551893
    const/4 v1, 0x1

    const-string v2, "voicemail_summary"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "voicemail_time desc"

    const-string v9, "100"

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 551894
    if-nez v1, :cond_2

    .line 551895
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 551896
    :cond_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_3

    .line 551897
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 551898
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 551899
    :cond_3
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    .line 551900
    const-string v0, "caller_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 551901
    const-string v0, "callee_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 551902
    const-string v0, "call_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 551903
    const-string v0, "voicemail_time"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 551904
    const-string v0, "voicemail_duration"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 551905
    const-string v0, "download_uri"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 551906
    const-string v0, "message_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 551907
    const-string v0, "voicemail_played"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 551908
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_5

    .line 551909
    new-instance v11, Lcom/facebook/rtc/models/RtcVoicemailInfo;

    invoke-direct {v11}, Lcom/facebook/rtc/models/RtcVoicemailInfo;-><init>()V

    .line 551910
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v11, Lcom/facebook/rtc/models/RtcVoicemailInfo;->a:Ljava/lang/String;

    .line 551911
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v11, Lcom/facebook/rtc/models/RtcVoicemailInfo;->b:Ljava/lang/String;

    .line 551912
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v11, Lcom/facebook/rtc/models/RtcVoicemailInfo;->c:Ljava/lang/String;

    .line 551913
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    iput-wide v12, v11, Lcom/facebook/rtc/models/RtcVoicemailInfo;->d:J

    .line 551914
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    iput-wide v12, v11, Lcom/facebook/rtc/models/RtcVoicemailInfo;->e:J

    .line 551915
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v11, Lcom/facebook/rtc/models/RtcVoicemailInfo;->f:Ljava/lang/String;

    .line 551916
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v11, Lcom/facebook/rtc/models/RtcVoicemailInfo;->g:Ljava/lang/String;

    .line 551917
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, v11, Lcom/facebook/rtc/models/RtcVoicemailInfo;->h:Z

    .line 551918
    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 551919
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 551920
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 551921
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 551922
    :cond_5
    :try_start_2
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 551923
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Z)V
    .locals 14

    .prologue
    .line 551882
    iget-object v0, p0, LX/3Lj;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move/from16 v12, p10

    invoke-direct/range {v1 .. v12}, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;-><init>(LX/3Lj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Z)V

    const v2, -0x723156e2

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 551883
    return-void
.end method
