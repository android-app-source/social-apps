.class public abstract LX/398;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:LX/0cG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0cG",
            "<",
            "LX/47M;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 522320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 522321
    new-instance v0, LX/0cG;

    invoke-direct {v0}, LX/0cG;-><init>()V

    iput-object v0, p0, LX/398;->a:LX/0cG;

    .line 522322
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 522327
    invoke-virtual {p0}, LX/398;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 522328
    :cond_0
    :goto_0
    return-object v0

    .line 522329
    :cond_1
    const-string v1, "v\\d+\\.\\d+\\/"

    const-string v2, ""

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 522330
    :try_start_0
    iget-object v2, p0, LX/398;->a:LX/0cG;

    invoke-virtual {v2, v1}, LX/0cG;->a(Ljava/lang/String;)LX/47X;
    :try_end_0
    .catch LX/47T; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 522331
    if-eqz v1, :cond_0

    .line 522332
    iget-object v0, v1, LX/47X;->a:Ljava/lang/Object;

    check-cast v0, LX/47M;

    iget-object v1, v1, LX/47X;->b:Landroid/os/Bundle;

    invoke-interface {v0, p1, v1}, LX/47M;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 522333
    :catch_0
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/0Or;Landroid/os/Bundle;)V
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 522323
    :try_start_0
    iget-object v0, p0, LX/398;->a:LX/0cG;

    new-instance v1, LX/47R;

    invoke-direct {v1, p2, p3}, LX/47R;-><init>(LX/0Or;Landroid/os/Bundle;)V

    invoke-virtual {v0, p1, v1}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch LX/47U; {:try_start_0 .. :try_end_0} :catch_0

    .line 522324
    :goto_0
    return-void

    .line 522325
    :catch_0
    move-exception v0

    .line 522326
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Invalid uri template: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/47M;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 522316
    :try_start_0
    iget-object v0, p0, LX/398;->a:LX/0cG;

    invoke-virtual {v0, p1, p2}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch LX/47U; {:try_start_0 .. :try_end_0} :catch_0

    .line 522317
    :goto_0
    return-void

    .line 522318
    :catch_0
    move-exception v0

    .line 522319
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Invalid uri template: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 522334
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 522335
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Class;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 522314
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 522315
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V
    .locals 5
    .param p4    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;I",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 522310
    :try_start_0
    iget-object v0, p0, LX/398;->a:LX/0cG;

    new-instance v1, LX/47O;

    invoke-direct {v1, p3, p2, p4}, LX/47O;-><init>(ILjava/lang/Class;Landroid/os/Bundle;)V

    invoke-virtual {v0, p1, v1}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch LX/47U; {:try_start_0 .. :try_end_0} :catch_0

    .line 522311
    :goto_0
    return-void

    .line 522312
    :catch_0
    move-exception v0

    .line 522313
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Invalid uri template: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 522301
    :try_start_0
    iget-object v0, p0, LX/398;->a:LX/0cG;

    new-instance v1, LX/47Q;

    invoke-direct {v1, p2, p3}, LX/47Q;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    invoke-virtual {v0, p1, v1}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch LX/47U; {:try_start_0 .. :try_end_0} :catch_0

    .line 522302
    :goto_0
    return-void

    .line 522303
    :catch_0
    move-exception v0

    .line 522304
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Invalid uri template: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 522306
    :try_start_0
    iget-object v0, p0, LX/398;->a:LX/0cG;

    new-instance v1, LX/47P;

    invoke-direct {v1, p2}, LX/47P;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, LX/0cG;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch LX/47U; {:try_start_0 .. :try_end_0} :catch_0

    .line 522307
    :goto_0
    return-void

    .line 522308
    :catch_0
    move-exception v0

    .line 522309
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Invalid uri template: %s"

    invoke-static {v2, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 522305
    const/4 v0, 0x1

    return v0
.end method
