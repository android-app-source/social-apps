.class public final LX/2F6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/analytics/reporters/periodic/DeviceInfoPeriodicReporterAdditionalInfo;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/analytics/reporters/periodic/DeviceInfoPeriodicReporterAdditionalInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 386503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386504
    iput-object p1, p0, LX/2F6;->a:LX/0QB;

    .line 386505
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/analytics/reporters/periodic/DeviceInfoPeriodicReporterAdditionalInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 386502
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/2F6;

    invoke-direct {v2, p0}, LX/2F6;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 386506
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2F6;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 386497
    packed-switch p2, :pswitch_data_0

    .line 386498
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 386499
    :pswitch_0
    new-instance p2, LX/2FG;

    invoke-static {p1}, LX/0sY;->b(LX/0QB;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p1}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v2

    check-cast v2, Landroid/content/ContentResolver;

    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    invoke-direct {p2, v0, v1, v2, p0}, LX/2FG;-><init>(Landroid/view/accessibility/AccessibilityManager;LX/03V;Landroid/content/ContentResolver;Landroid/content/res/Resources;)V

    .line 386500
    move-object v0, p2

    .line 386501
    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 386496
    const/4 v0, 0x1

    return v0
.end method
