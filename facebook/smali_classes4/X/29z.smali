.class public LX/29z;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 376868
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 376869
    return-void
.end method

.method public static a(LX/0Xp;LX/0bD;)LX/2A0;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 376843
    new-instance v0, LX/2A0;

    invoke-direct {v0, p0, p1}, LX/2A0;-><init>(LX/0Xp;LX/0bD;)V

    return-object v0
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/lang/Boolean;)LX/4Ad;
    .locals 4
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 376854
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376855
    new-instance v0, LX/4Ad;

    new-array v1, v2, [Ljava/lang/String;

    .line 376856
    sget-object v2, LX/007;->f:Ljava/lang/String;

    move-object v2, v2

    .line 376857
    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/4Ad;-><init>(Landroid/content/ContentResolver;Ljava/util/List;)V

    .line 376858
    :goto_0
    return-object v0

    .line 376859
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 376860
    sget-object v1, LX/007;->g:Ljava/lang/String;

    move-object v1, v1

    .line 376861
    aput-object v1, v0, v3

    const-string v1, "com.facebook.lite"

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 376862
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 376863
    if-eqz v0, :cond_1

    .line 376864
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 376865
    const-string v2, "com.facebook.rememberme"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 376866
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 376867
    :goto_1
    new-instance v1, LX/4Ad;

    invoke-direct {v1, p0, v0}, LX/4Ad;-><init>(Landroid/content/ContentResolver;Ljava/util/List;)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(LX/0W9;)Landroid/content/Intent;
    .locals 3
    .annotation runtime Lcom/facebook/auth/login/ForWebPasswordRecovery;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 376850
    const-string v0, "https://m.facebook.com/recover/initiate"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 376851
    invoke-virtual {p0}, LX/0W9;->d()Ljava/lang/String;

    move-result-object v1

    .line 376852
    const-string v2, "locale"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 376853
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1
.end method

.method public static a(LX/0WJ;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/auth/annotations/AuthTokenString;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 376846
    invoke-virtual {p0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 376847
    if-eqz v0, :cond_0

    .line 376848
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, p0

    .line 376849
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/facebook/auth/annotations/ShouldRequestSessionCookiesWithAuth;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 376845
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 376844
    return-void
.end method
