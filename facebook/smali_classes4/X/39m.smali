.class public LX/39m;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 523914
    sget-object v0, LX/1vY;->LIKE_LINK:LX/1vY;

    invoke-static {v0}, LX/1vZ;->a(LX/1vY;)Landroid/util/SparseArray;

    move-result-object v0

    sput-object v0, LX/39m;->a:Landroid/util/SparseArray;

    .line 523915
    sget-object v0, LX/1vY;->COMMENT_LINK:LX/1vY;

    invoke-static {v0}, LX/1vZ;->a(LX/1vY;)Landroid/util/SparseArray;

    move-result-object v0

    sput-object v0, LX/39m;->b:Landroid/util/SparseArray;

    .line 523916
    sget-object v0, LX/1vY;->SHARE_LINK:LX/1vY;

    invoke-static {v0}, LX/1vZ;->a(LX/1vY;)Landroid/util/SparseArray;

    move-result-object v0

    sput-object v0, LX/39m;->c:Landroid/util/SparseArray;

    .line 523917
    sget-object v0, LX/1vY;->SAVE_ACTION:LX/1vY;

    invoke-static {v0}, LX/1vZ;->a(LX/1vY;)Landroid/util/SparseArray;

    move-result-object v0

    sput-object v0, LX/39m;->d:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 523912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523913
    return-void
.end method

.method public static a(LX/20X;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 2

    .prologue
    .line 523905
    sget-object v0, LX/39n;->a:[I

    invoke-virtual {p0}, LX/20X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 523906
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FooterButtonId not recognised"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 523907
    :pswitch_0
    const v0, 0x7f080fc8

    .line 523908
    :goto_0
    return v0

    .line 523909
    :pswitch_1
    const v0, 0x7f080fcc

    goto :goto_0

    .line 523910
    :pswitch_2
    const v0, 0x7f080fd9

    goto :goto_0

    .line 523911
    :pswitch_3
    invoke-static {p1}, LX/0sa;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f081859

    goto :goto_0

    :cond_0
    const v0, 0x7f081858

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(LX/20X;Lcom/facebook/graphql/model/GraphQLStory;I)I
    .locals 1

    .prologue
    .line 523918
    sget-object v0, LX/20X;->LIKE:LX/20X;

    invoke-virtual {p0, v0}, LX/20X;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, LX/20X;->SAVE:LX/20X;

    invoke-virtual {p0, v0}, LX/20X;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, LX/0sa;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const p2, -0xa76f01

    :cond_2
    return p2
.end method

.method public static a(LX/1De;Lcom/facebook/graphql/model/GraphQLStory;LX/20X;LX/1vg;I)LX/1dc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/20X;",
            "LX/1vg;",
            "I)",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 523898
    sget-object v0, LX/39n;->a:[I

    invoke-virtual {p2}, LX/20X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 523899
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FooterButtonId not recognised"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 523900
    :pswitch_0
    const v0, 0x7f0219c6

    .line 523901
    :goto_0
    invoke-virtual {p3, p0}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    invoke-static {p2, p1, p4}, LX/39m;->a(LX/20X;Lcom/facebook/graphql/model/GraphQLStory;I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/2xv;->i(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    return-object v0

    .line 523902
    :pswitch_1
    const v0, 0x7f0219c3

    goto :goto_0

    .line 523903
    :pswitch_2
    const v0, 0x7f0219c9

    goto :goto_0

    .line 523904
    :pswitch_3
    const v0, 0x7f020781

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(LX/20X;)Landroid/util/SparseArray;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/20X;",
            ")",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 523883
    sget-object v0, LX/39n;->a:[I

    invoke-virtual {p0}, LX/20X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 523884
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FooterButtonId not recognised"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 523885
    :pswitch_0
    sget-object v0, LX/39m;->a:Landroid/util/SparseArray;

    .line 523886
    :goto_0
    return-object v0

    .line 523887
    :pswitch_1
    sget-object v0, LX/39m;->b:Landroid/util/SparseArray;

    goto :goto_0

    .line 523888
    :pswitch_2
    sget-object v0, LX/39m;->c:Landroid/util/SparseArray;

    goto :goto_0

    .line 523889
    :pswitch_3
    sget-object v0, LX/39m;->d:Landroid/util/SparseArray;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(LX/20X;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 2

    .prologue
    .line 523890
    sget-object v0, LX/39n;->a:[I

    invoke-virtual {p0}, LX/20X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 523891
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FooterButtonId not recognised"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 523892
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f081107

    .line 523893
    :goto_0
    return v0

    .line 523894
    :cond_0
    const v0, 0x7f081106

    goto :goto_0

    .line 523895
    :pswitch_1
    const v0, 0x7f081108

    goto :goto_0

    .line 523896
    :pswitch_2
    const v0, 0x7f081109

    goto :goto_0

    .line 523897
    :pswitch_3
    invoke-static {p1}, LX/0sa;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f081859

    goto :goto_0

    :cond_1
    const v0, 0x7f081858

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
