.class public final LX/3C4;
.super LX/0Tz;
.source ""


# static fields
.field public static a:Ljava/lang/String;

.field public static b:LX/0U1;

.field public static c:LX/0U1;

.field public static d:LX/0U1;

.field private static final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 529638
    const-string v0, "location_signal_package_snapshots"

    sput-object v0, LX/3C4;->a:Ljava/lang/String;

    .line 529639
    new-instance v0, LX/0U1;

    const-string v1, "snapshot"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3C4;->b:LX/0U1;

    .line 529640
    new-instance v0, LX/0U1;

    const-string v1, "timestamp"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3C4;->c:LX/0U1;

    .line 529641
    new-instance v0, LX/0U1;

    const-string v1, "deferred"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3C4;->d:LX/0U1;

    .line 529642
    sget-object v0, LX/3C4;->b:LX/0U1;

    sget-object v1, LX/3C4;->c:LX/0U1;

    sget-object v2, LX/3C4;->d:LX/0U1;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3C4;->e:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 529643
    sget-object v0, LX/3C4;->a:Ljava/lang/String;

    sget-object v1, LX/3C4;->e:LX/0Px;

    invoke-direct {p0, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 529644
    return-void
.end method
