.class public LX/2Mu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Long;",
        "Lcom/facebook/messaging/media/photoquality/PhotoQualityQueryResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Mu;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397828
    return-void
.end method

.method public static a(LX/0QB;)LX/2Mu;
    .locals 3

    .prologue
    .line 397829
    sget-object v0, LX/2Mu;->a:LX/2Mu;

    if-nez v0, :cond_1

    .line 397830
    const-class v1, LX/2Mu;

    monitor-enter v1

    .line 397831
    :try_start_0
    sget-object v0, LX/2Mu;->a:LX/2Mu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 397832
    if-eqz v2, :cond_0

    .line 397833
    :try_start_1
    new-instance v0, LX/2Mu;

    invoke-direct {v0}, LX/2Mu;-><init>()V

    .line 397834
    move-object v0, v0

    .line 397835
    sput-object v0, LX/2Mu;->a:LX/2Mu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397836
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 397837
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 397838
    :cond_1
    sget-object v0, LX/2Mu;->a:LX/2Mu;

    return-object v0

    .line 397839
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 397840
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 397841
    check-cast p1, Ljava/lang/Long;

    .line 397842
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 397843
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "fbid"

    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397844
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "getPhotoQuality"

    .line 397845
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 397846
    move-object v1, v1

    .line 397847
    const-string v2, "POST"

    .line 397848
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 397849
    move-object v1, v1

    .line 397850
    const-string v2, "me/message_media_quality"

    .line 397851
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 397852
    move-object v1, v1

    .line 397853
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 397854
    move-object v0, v1

    .line 397855
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 397856
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 397857
    move-object v0, v0

    .line 397858
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 397859
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 397860
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/messaging/media/photoquality/PhotoQualityQueryResult;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/photoquality/PhotoQualityQueryResult;

    return-object v0
.end method
