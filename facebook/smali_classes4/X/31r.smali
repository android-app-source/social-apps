.class public final LX/31r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/io/File;


# direct methods
.method public constructor <init>(Lcom/facebook/resources/impl/loading/LanguagePackDownloader;Landroid/content/Context;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 488173
    iput-object p1, p0, LX/31r;->a:Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488174
    iput-object p2, p0, LX/31r;->b:Landroid/content/Context;

    .line 488175
    iput-object p3, p0, LX/31r;->c:Ljava/io/File;

    .line 488176
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 488177
    iget-object v0, p0, LX/31r;->a:Lcom/facebook/resources/impl/loading/LanguagePackDownloader;

    iget-object v0, v0, Lcom/facebook/resources/impl/loading/LanguagePackDownloader;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/316;

    iget-object v1, p0, LX/31r;->b:Landroid/content/Context;

    iget-object v2, p0, LX/31r;->c:Ljava/io/File;

    .line 488178
    iget-object p0, v0, LX/316;->b:LX/315;

    invoke-interface {p0, v1, p1, v2}, LX/315;->a(Landroid/content/Context;Ljava/io/InputStream;Ljava/io/File;)V

    .line 488179
    const/4 v0, 0x0

    return-object v0
.end method
