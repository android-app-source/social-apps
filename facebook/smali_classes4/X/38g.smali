.class public LX/38g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile j:LX/38g;


# instance fields
.field private final b:LX/38h;

.field public final c:LX/37e;

.field public final d:LX/37f;

.field public final e:LX/0SF;

.field public f:LX/7JI;

.field public g:LX/2wX;

.field public h:LX/38i;

.field public i:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 521231
    const-class v0, LX/38g;

    sput-object v0, LX/38g;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/38h;LX/37e;LX/37f;LX/0SF;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521225
    iput-object p3, p0, LX/38g;->d:LX/37f;

    .line 521226
    iput-object p1, p0, LX/38g;->b:LX/38h;

    .line 521227
    new-instance v0, LX/38i;

    invoke-direct {v0}, LX/38i;-><init>()V

    iput-object v0, p0, LX/38g;->h:LX/38i;

    .line 521228
    iput-object p2, p0, LX/38g;->c:LX/37e;

    .line 521229
    iput-object p4, p0, LX/38g;->e:LX/0SF;

    .line 521230
    return-void
.end method

.method public static a(LX/0QB;)LX/38g;
    .locals 7

    .prologue
    .line 521211
    sget-object v0, LX/38g;->j:LX/38g;

    if-nez v0, :cond_1

    .line 521212
    const-class v1, LX/38g;

    monitor-enter v1

    .line 521213
    :try_start_0
    sget-object v0, LX/38g;->j:LX/38g;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 521214
    if-eqz v2, :cond_0

    .line 521215
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 521216
    new-instance p0, LX/38g;

    invoke-static {v0}, LX/38h;->a(LX/0QB;)LX/38h;

    move-result-object v3

    check-cast v3, LX/38h;

    invoke-static {v0}, LX/37e;->a(LX/0QB;)LX/37e;

    move-result-object v4

    check-cast v4, LX/37e;

    invoke-static {v0}, LX/37f;->a(LX/0QB;)LX/37f;

    move-result-object v5

    check-cast v5, LX/37f;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SF;

    invoke-direct {p0, v3, v4, v5, v6}, LX/38g;-><init>(LX/38h;LX/37e;LX/37f;LX/0SF;)V

    .line 521217
    move-object v0, p0

    .line 521218
    sput-object v0, LX/38g;->j:LX/38g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521219
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 521220
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 521221
    :cond_1
    sget-object v0, LX/38g;->j:LX/38g;

    return-object v0

    .line 521222
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 521223
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/04D;J)V
    .locals 9

    .prologue
    const/16 v4, 0xd

    .line 521179
    :try_start_0
    iget-object v0, p0, LX/38g;->b:LX/38h;

    iget-object v1, p2, LX/04D;->origin:Ljava/lang/String;

    iget-object v2, p2, LX/04D;->subOrigin:Ljava/lang/String;

    .line 521180
    new-instance v3, LX/7JU;

    invoke-direct {v3}, LX/7JU;-><init>()V

    move-object v3, v3

    .line 521181
    const-string p2, "targetID"

    invoke-virtual {v3, p2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 521182
    const-string p2, "playerOrigin"

    invoke-virtual {v3, p2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 521183
    const-string p2, "playerSuborigin"

    invoke-virtual {v3, p2, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 521184
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 521185
    iget-object p2, v0, LX/38h;->b:LX/0tX;

    invoke-virtual {p2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v0, v3

    .line 521186
    const v1, 0x1652a9b7

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 521187
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 521188
    if-nez v1, :cond_0

    .line 521189
    iget-object v0, p0, LX/38g;->d:LX/37f;

    const-string v1, "CastPlayer.play: Graph QL Failure"

    invoke-virtual {v0, v4, v1}, LX/37f;->a(ILjava/lang/String;)V

    .line 521190
    iget-object v0, p0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->c()V

    .line 521191
    :goto_0
    return-void

    .line 521192
    :catch_0
    move-exception v0

    .line 521193
    :goto_1
    iget-object v1, p0, LX/38g;->d:LX/37f;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CastPlayer.play: Graph QL Failure: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, LX/37f;->a(ILjava/lang/String;)V

    .line 521194
    iget-object v0, p0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->c()V

    goto :goto_0

    .line 521195
    :cond_0
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 521196
    const-string v2, "cmd"

    const-string v3, "next_video"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 521197
    const-string v2, "payload"

    .line 521198
    iget-object v3, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v3

    .line 521199
    check-cast v0, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/chromecast/graphql/FBVideoCastPayloadQueryModels$FBVideoCastPayloadQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 521200
    iget-object v0, p0, LX/38g;->f:LX/7JI;

    .line 521201
    iget v5, v0, LX/7JI;->g:I

    const/4 v6, 0x3

    if-eq v5, v6, :cond_1

    iget v5, v0, LX/7JI;->g:I

    const/4 v6, 0x6

    if-eq v5, v6, :cond_1

    iget v5, v0, LX/7JI;->g:I

    const/4 v6, 0x7

    if-eq v5, v6, :cond_1

    .line 521202
    iget-object v5, v0, LX/7JI;->a:LX/38g;

    iget-object v5, v5, LX/38g;->c:LX/37e;

    sget-object v6, LX/7JJ;->FbAppPlayer_Play:LX/7JJ;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Incorrect state: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, v0, LX/7JI;->g:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/37e;->a(LX/7JJ;Ljava/lang/String;)V

    .line 521203
    :cond_1
    iput-wide p3, v0, LX/7JI;->k:J

    .line 521204
    const/4 v5, 0x4

    invoke-static {v0, v5}, LX/7JI;->a(LX/7JI;I)V

    .line 521205
    const-string v5, "experience_command"

    iget-object v6, v0, LX/7JI;->j:Ljava/lang/String;

    new-instance v7, LX/7JB;

    invoke-direct {v7, v0}, LX/7JB;-><init>(LX/7JI;)V

    invoke-static {v0, v5, v6, v1, v7}, LX/7JI;->a(LX/7JI;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;LX/27U;)V

    .line 521206
    goto :goto_0

    .line 521207
    :catch_1
    move-exception v0

    .line 521208
    iget-object v1, p0, LX/38g;->d:LX/37f;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CastPlayer.play: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, LX/37f;->a(ILjava/lang/String;)V

    .line 521209
    iget-object v0, p0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->c()V

    goto/16 :goto_0

    .line 521210
    :catch_2
    move-exception v0

    goto/16 :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 521178
    iget-object v0, p0, LX/38g;->f:LX/7JI;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
