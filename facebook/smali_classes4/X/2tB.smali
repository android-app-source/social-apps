.class public LX/2tB;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2tB;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 474784
    invoke-direct {p0}, LX/398;-><init>()V

    .line 474785
    sget-object v0, LX/0ax;->dc:Ljava/lang/String;

    sget-object v1, LX/0ax;->dd:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 474786
    sget-object v0, LX/0ax;->dd:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->NOTIFICATIONS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 474787
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 474788
    const-string v1, "title_bar_search_button_visible"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 474789
    sget-object v1, LX/0ax;->de:Ljava/lang/String;

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->NOTIFICATION_SETTINGS_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 474790
    sget-object v1, LX/0ax;->df:Ljava/lang/String;

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->NOTIFICATION_SETTINGS_ALERTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 474791
    return-void
.end method

.method public static a(LX/0QB;)LX/2tB;
    .locals 3

    .prologue
    .line 474792
    sget-object v0, LX/2tB;->a:LX/2tB;

    if-nez v0, :cond_1

    .line 474793
    const-class v1, LX/2tB;

    monitor-enter v1

    .line 474794
    :try_start_0
    sget-object v0, LX/2tB;->a:LX/2tB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 474795
    if-eqz v2, :cond_0

    .line 474796
    :try_start_1
    new-instance v0, LX/2tB;

    invoke-direct {v0}, LX/2tB;-><init>()V

    .line 474797
    move-object v0, v0

    .line 474798
    sput-object v0, LX/2tB;->a:LX/2tB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 474799
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 474800
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 474801
    :cond_1
    sget-object v0, LX/2tB;->a:LX/2tB;

    return-object v0

    .line 474802
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 474803
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
