.class public LX/3Kx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3Kx;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 549531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549532
    iput-object p1, p0, LX/3Kx;->a:Landroid/content/res/Resources;

    .line 549533
    return-void
.end method

.method private static a(I)I
    .locals 1

    .prologue
    .line 549534
    const v0, 0x7f080449

    if-ne p0, v0, :cond_1

    .line 549535
    const p0, 0x7f08045f

    .line 549536
    :cond_0
    :goto_0
    return p0

    .line 549537
    :cond_1
    const v0, 0x7f08044a

    if-ne p0, v0, :cond_2

    .line 549538
    const p0, 0x7f080460

    goto :goto_0

    .line 549539
    :cond_2
    const v0, 0x7f08044b

    if-ne p0, v0, :cond_3

    .line 549540
    const p0, 0x7f080461

    goto :goto_0

    .line 549541
    :cond_3
    const v0, 0x7f08044c

    if-ne p0, v0, :cond_4

    .line 549542
    const p0, 0x7f080462

    goto :goto_0

    .line 549543
    :cond_4
    const v0, 0x7f08044d

    if-ne p0, v0, :cond_5

    .line 549544
    const p0, 0x7f080463

    goto :goto_0

    .line 549545
    :cond_5
    const v0, 0x7f08044e

    if-ne p0, v0, :cond_6

    .line 549546
    const p0, 0x7f080464

    goto :goto_0

    .line 549547
    :cond_6
    const v0, 0x7f08044f

    if-ne p0, v0, :cond_7

    .line 549548
    const p0, 0x7f080465

    goto :goto_0

    .line 549549
    :cond_7
    const v0, 0x7f080450

    if-ne p0, v0, :cond_8

    .line 549550
    const p0, 0x7f080466

    goto :goto_0

    .line 549551
    :cond_8
    const v0, 0x7f080459

    if-ne p0, v0, :cond_9

    .line 549552
    const p0, 0x7f080467

    goto :goto_0

    .line 549553
    :cond_9
    const v0, 0x7f08045a

    if-ne p0, v0, :cond_a

    .line 549554
    const p0, 0x7f080468

    goto :goto_0

    .line 549555
    :cond_a
    const v0, 0x7f08045b

    if-ne p0, v0, :cond_b

    .line 549556
    const p0, 0x7f080469

    goto :goto_0

    .line 549557
    :cond_b
    const v0, 0x7f08045c

    if-ne p0, v0, :cond_c

    .line 549558
    const p0, 0x7f08046a

    goto :goto_0

    .line 549559
    :cond_c
    const v0, 0x7f08045d

    if-ne p0, v0, :cond_d

    .line 549560
    const p0, 0x7f08046b

    goto :goto_0

    .line 549561
    :cond_d
    const v0, 0x7f08045e

    if-ne p0, v0, :cond_0

    .line 549562
    const p0, 0x7f08046c

    goto :goto_0
.end method

.method public static a(JLX/3Oz;)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 549563
    cmp-long v2, p0, v0

    if-gtz v2, :cond_0

    .line 549564
    :goto_0
    return-wide v0

    .line 549565
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, p0

    .line 549566
    sget-object v4, LX/3Oz;->AVAILABLE:LX/3Oz;

    if-ne p2, v4, :cond_2

    .line 549567
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p0

    :cond_1
    :goto_1
    move-wide v0, p0

    .line 549568
    goto :goto_0

    .line 549569
    :cond_2
    const-wide/32 v4, 0x14997000

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    move-wide p0, v0

    .line 549570
    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/3Kx;
    .locals 4

    .prologue
    .line 549571
    sget-object v0, LX/3Kx;->b:LX/3Kx;

    if-nez v0, :cond_1

    .line 549572
    const-class v1, LX/3Kx;

    monitor-enter v1

    .line 549573
    :try_start_0
    sget-object v0, LX/3Kx;->b:LX/3Kx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 549574
    if-eqz v2, :cond_0

    .line 549575
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 549576
    new-instance p0, LX/3Kx;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/3Kx;-><init>(Landroid/content/res/Resources;)V

    .line 549577
    move-object v0, p0

    .line 549578
    sput-object v0, LX/3Kx;->b:LX/3Kx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 549579
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 549580
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 549581
    :cond_1
    sget-object v0, LX/3Kx;->b:LX/3Kx;

    return-object v0

    .line 549582
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 549583
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3Kx;IILX/3P1;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 549584
    sget-object v0, LX/3P1;->UPPER_CASE:LX/3P1;

    if-ne v0, p3, :cond_0

    .line 549585
    invoke-static {p1}, LX/3Kx;->a(I)I

    move-result p1

    .line 549586
    :cond_0
    iget-object v0, p0, LX/3Kx;->a:Landroid/content/res/Resources;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/3Kx;ILX/3P1;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 549587
    sget-object v0, LX/3P1;->UPPER_CASE:LX/3P1;

    if-ne v0, p2, :cond_0

    invoke-static {p1}, LX/3Kx;->a(I)I

    move-result p1

    .line 549588
    :cond_0
    iget-object v0, p0, LX/3Kx;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(JLX/3P1;)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x3c

    .line 549589
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 549590
    const/4 v0, 0x0

    .line 549591
    :goto_0
    return-object v0

    .line 549592
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 549593
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 549594
    div-int/lit8 v1, v0, 0x3c

    .line 549595
    div-int/lit8 v2, v1, 0x3c

    .line 549596
    div-int/lit8 v3, v2, 0x18

    .line 549597
    if-ge v0, v4, :cond_1

    .line 549598
    const v0, 0x7f080459

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, p3}, LX/3Kx;->a(LX/3Kx;IILX/3P1;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 549599
    :cond_1
    if-ge v1, v4, :cond_2

    .line 549600
    const v0, 0x7f080459

    invoke-static {p0, v0, v1, p3}, LX/3Kx;->a(LX/3Kx;IILX/3P1;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 549601
    :cond_2
    const/16 v0, 0x18

    if-ge v2, v0, :cond_3

    .line 549602
    const v0, 0x7f08045a

    invoke-static {p0, v0, v2, p3}, LX/3Kx;->a(LX/3Kx;IILX/3P1;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 549603
    :cond_3
    const v0, 0x7f08045b

    invoke-static {p0, v0, v3, p3}, LX/3Kx;->a(LX/3Kx;IILX/3P1;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/user/model/LastActive;LX/3Ox;LX/3P0;LX/3P1;)Ljava/lang/String;
    .locals 10
    .param p1    # Lcom/facebook/user/model/LastActive;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 549604
    if-nez p1, :cond_1

    .line 549605
    iget-object v0, p2, LX/3Ox;->b:LX/3Oz;

    move-object v0, v0

    .line 549606
    sget-object v1, LX/3Oz;->AVAILABLE:LX/3Oz;

    if-ne v0, v1, :cond_0

    sget-object v0, LX/3P0;->VERBOSE:LX/3P0;

    if-ne p3, v0, :cond_0

    .line 549607
    iget-object v0, p0, LX/3Kx;->a:Landroid/content/res/Resources;

    const v1, 0x7f080449

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 549608
    :goto_0
    return-object v0

    .line 549609
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 549610
    :cond_1
    if-eqz p1, :cond_2

    .line 549611
    iget-wide v3, p1, Lcom/facebook/user/model/LastActive;->a:J

    move-wide v0, v3

    .line 549612
    :goto_1
    iget-object v2, p2, LX/3Ox;->b:LX/3Oz;

    move-object v2, v2

    .line 549613
    invoke-static {v0, v1, v2}, LX/3Kx;->a(JLX/3Oz;)J

    move-result-wide v0

    .line 549614
    sget-object v2, LX/3P0;->SHORT:LX/3P0;

    if-ne v2, p3, :cond_3

    .line 549615
    invoke-virtual {p0, v0, v1, p4}, LX/3Kx;->a(JLX/3P1;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 549616
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 549617
    :cond_3
    sget-object v2, LX/3P0;->ABBREVIATED:LX/3P0;

    if-ne v2, p3, :cond_4

    .line 549618
    const/16 v7, 0x3c

    .line 549619
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-gtz v3, :cond_5

    .line 549620
    const/4 v3, 0x0

    .line 549621
    :goto_2
    move-object v0, v3

    .line 549622
    goto :goto_0

    .line 549623
    :cond_4
    const/16 v8, 0x3c

    const/4 v7, 0x1

    .line 549624
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-gtz v3, :cond_9

    .line 549625
    const/4 v3, 0x0

    .line 549626
    :goto_3
    move-object v0, v3

    .line 549627
    goto :goto_0

    .line 549628
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    .line 549629
    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    long-to-int v3, v3

    .line 549630
    div-int/lit8 v4, v3, 0x3c

    .line 549631
    div-int/lit8 v5, v4, 0x3c

    .line 549632
    div-int/lit8 v6, v5, 0x18

    .line 549633
    if-ge v3, v7, :cond_6

    .line 549634
    const v3, 0x7f08045c

    const/4 v4, 0x1

    invoke-static {p0, v3, v4, p4}, LX/3Kx;->a(LX/3Kx;IILX/3P1;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 549635
    :cond_6
    if-ge v4, v7, :cond_7

    .line 549636
    const v3, 0x7f08045c

    invoke-static {p0, v3, v4, p4}, LX/3Kx;->a(LX/3Kx;IILX/3P1;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 549637
    :cond_7
    const/16 v3, 0x18

    if-ge v5, v3, :cond_8

    .line 549638
    const v3, 0x7f08045d

    invoke-static {p0, v3, v5, p4}, LX/3Kx;->a(LX/3Kx;IILX/3P1;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 549639
    :cond_8
    const v3, 0x7f08045e

    invoke-static {p0, v3, v6, p4}, LX/3Kx;->a(LX/3Kx;IILX/3P1;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 549640
    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    .line 549641
    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    long-to-int v3, v3

    .line 549642
    div-int/lit8 v4, v3, 0x3c

    .line 549643
    div-int/lit8 v5, v4, 0x3c

    .line 549644
    div-int/lit8 v6, v5, 0x18

    .line 549645
    if-ge v3, v8, :cond_a

    .line 549646
    const v3, 0x7f080449

    invoke-static {p0, v3, p4}, LX/3Kx;->a(LX/3Kx;ILX/3P1;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 549647
    :cond_a
    if-ne v4, v7, :cond_b

    .line 549648
    const v3, 0x7f08044b

    invoke-static {p0, v3, p4}, LX/3Kx;->a(LX/3Kx;ILX/3P1;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 549649
    :cond_b
    if-ge v4, v8, :cond_c

    .line 549650
    const v3, 0x7f08044c

    invoke-static {p0, v3, v4, p4}, LX/3Kx;->a(LX/3Kx;IILX/3P1;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 549651
    :cond_c
    if-ne v5, v7, :cond_d

    .line 549652
    const v3, 0x7f08044d

    invoke-static {p0, v3, p4}, LX/3Kx;->a(LX/3Kx;ILX/3P1;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 549653
    :cond_d
    const/16 v3, 0x18

    if-ge v5, v3, :cond_e

    .line 549654
    const v3, 0x7f08044e

    invoke-static {p0, v3, v5, p4}, LX/3Kx;->a(LX/3Kx;IILX/3P1;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 549655
    :cond_e
    if-ne v6, v7, :cond_f

    .line 549656
    const v3, 0x7f08044f

    invoke-static {p0, v3, p4}, LX/3Kx;->a(LX/3Kx;ILX/3P1;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    .line 549657
    :cond_f
    const v3, 0x7f080450

    invoke-static {p0, v3, v6, p4}, LX/3Kx;->a(LX/3Kx;IILX/3P1;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3
.end method
