.class public LX/2n0;
.super LX/2n1;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/2n2;

.field private final c:LX/0WJ;

.field private final d:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 462489
    const-class v0, LX/2n0;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2n0;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2n2;LX/0WJ;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 462448
    invoke-direct {p0}, LX/2n1;-><init>()V

    .line 462449
    iput-object p1, p0, LX/2n0;->b:LX/2n2;

    .line 462450
    iput-object p2, p0, LX/2n0;->c:LX/0WJ;

    .line 462451
    iput-object p3, p0, LX/2n0;->d:LX/0Zb;

    .line 462452
    return-void
.end method

.method public static b(LX/0QB;)LX/2n0;
    .locals 4

    .prologue
    .line 462487
    new-instance v3, LX/2n0;

    invoke-static {p0}, LX/2n2;->a(LX/0QB;)LX/2n2;

    move-result-object v0

    check-cast v0, LX/2n2;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v1

    check-cast v1, LX/0WJ;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-direct {v3, v0, v1, v2}, LX/2n0;-><init>(LX/2n2;LX/0WJ;LX/0Zb;)V

    .line 462488
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/Intent;
    .locals 0

    .prologue
    .line 462485
    invoke-virtual {p0, p1}, LX/2n0;->a(Landroid/content/Intent;)V

    .line 462486
    return-object p1
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 462453
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 462454
    if-nez v0, :cond_0

    .line 462455
    :goto_0
    return-void

    .line 462456
    :cond_0
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "fb_linkshim_single_link_attempt"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 462457
    invoke-static {v0}, LX/2yo;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 462458
    invoke-virtual {v0, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 462459
    const-string v0, "result"

    const-string v2, "fail_url_not_rewritten"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 462460
    iget-object v0, p0, LX/2n0;->d:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 462461
    :cond_1
    iget-object v3, p0, LX/2n0;->c:LX/0WJ;

    invoke-virtual {v3}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 462462
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v3, v4

    .line 462463
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 462464
    const-string v0, "result"

    const-string v2, "fail_cookies_not_found"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 462465
    iget-object v0, p0, LX/2n0;->d:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 462466
    :cond_2
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 462467
    const-string v4, "Referer"

    const-string v5, "http://m.facebook.com"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 462468
    const-string v4, "com.android.browser.headers"

    invoke-virtual {p1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 462469
    invoke-virtual {p1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 462470
    iget-object v2, p0, LX/2n0;->b:LX/2n2;

    .line 462471
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 462472
    const-string v4, "linkshim_link_extra"

    .line 462473
    const-string v5, "https://www.facebook.com"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 462474
    const-string v5, "/si/ajax/l/render_linkshim_log"

    invoke-virtual {v6, v5}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 462475
    const-string v5, "s"

    invoke-static {v6, v0, v5}, LX/2n2;->a(Landroid/net/Uri$Builder;Landroid/net/Uri;Ljava/lang/String;)V

    .line 462476
    const-string v5, "u"

    invoke-static {v6, v0, v5}, LX/2n2;->a(Landroid/net/Uri$Builder;Landroid/net/Uri;Ljava/lang/String;)V

    .line 462477
    const-string v5, "h"

    invoke-static {v6, v0, v5}, LX/2n2;->a(Landroid/net/Uri$Builder;Landroid/net/Uri;Ljava/lang/String;)V

    .line 462478
    const-string v5, "__a"

    const-string p1, "1"

    invoke-virtual {v6, v5, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 462479
    const-string p1, "__user"

    iget-object v5, v2, LX/2n2;->b:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, p1, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 462480
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    move-object v5, v5

    .line 462481
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 462482
    iget-object v4, v2, LX/2n2;->a:LX/0aG;

    const-string v5, "linkshim_click"

    const v6, -0x6252d8a3

    invoke-static {v4, v5, v3, v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    .line 462483
    const-string v0, "result"

    const-string v2, "success"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 462484
    iget-object v0, p0, LX/2n0;->d:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0
.end method
