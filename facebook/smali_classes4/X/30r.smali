.class public LX/30r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/30r;


# instance fields
.field private final a:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation
.end field

.field public final b:LX/0pn;

.field public final c:LX/0Zb;

.field public final d:Ljava/util/Random;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;LX/0pn;LX/0Zb;Ljava/util/Random;LX/0Or;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p4    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsCacheStateLoggingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "LX/0pn;",
            "LX/0Zb;",
            "Ljava/util/Random;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 486356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486357
    iput-object p1, p0, LX/30r;->a:Landroid/os/Handler;

    .line 486358
    iput-object p2, p0, LX/30r;->b:LX/0pn;

    .line 486359
    iput-object p3, p0, LX/30r;->c:LX/0Zb;

    .line 486360
    iput-object p4, p0, LX/30r;->d:Ljava/util/Random;

    .line 486361
    iput-object p5, p0, LX/30r;->e:LX/0Or;

    .line 486362
    return-void
.end method

.method public static a(LX/0QB;)LX/30r;
    .locals 9

    .prologue
    .line 486363
    sget-object v0, LX/30r;->f:LX/30r;

    if-nez v0, :cond_1

    .line 486364
    const-class v1, LX/30r;

    monitor-enter v1

    .line 486365
    :try_start_0
    sget-object v0, LX/30r;->f:LX/30r;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 486366
    if-eqz v2, :cond_0

    .line 486367
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 486368
    new-instance v3, LX/30r;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v5

    check-cast v5, LX/0pn;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v7

    check-cast v7, Ljava/util/Random;

    const/16 v8, 0x1497

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/30r;-><init>(Landroid/os/Handler;LX/0pn;LX/0Zb;Ljava/util/Random;LX/0Or;)V

    .line 486369
    move-object v0, v3

    .line 486370
    sput-object v0, LX/30r;->f:LX/30r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 486371
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 486372
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 486373
    :cond_1
    sget-object v0, LX/30r;->f:LX/30r;

    return-object v0

    .line 486374
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 486375
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feedtype/FeedType;I)V
    .locals 3

    .prologue
    .line 486376
    iget-object v0, p0, LX/30r;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486377
    iget-object v0, p0, LX/30r;->d:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    rem-int/lit16 v0, v0, 0xc8

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 486378
    if-nez v0, :cond_1

    .line 486379
    :cond_0
    :goto_1
    return-void

    .line 486380
    :cond_1
    new-instance v0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;-><init>(LX/30r;Lcom/facebook/api/feedtype/FeedType;I)V

    .line 486381
    iget-object v1, p0, LX/30r;->a:Landroid/os/Handler;

    const v2, 0x6a0f7aa

    invoke-static {v1, v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
