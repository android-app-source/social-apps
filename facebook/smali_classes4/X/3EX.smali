.class public final LX/3EX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Dw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/3EX;


# instance fields
.field private final b:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 537725
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "llimit"

    sget v2, LX/3Dx;->eL:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "ulimit"

    sget v2, LX/3Dx;->eM:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "_v"

    sget v2, LX/3Dx;->eK:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/3EX;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 537726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537727
    iput-object p1, p0, LX/3EX;->b:LX/0ad;

    .line 537728
    return-void
.end method

.method public static a(LX/0QB;)LX/3EX;
    .locals 4

    .prologue
    .line 537729
    sget-object v0, LX/3EX;->c:LX/3EX;

    if-nez v0, :cond_1

    .line 537730
    const-class v1, LX/3EX;

    monitor-enter v1

    .line 537731
    :try_start_0
    sget-object v0, LX/3EX;->c:LX/3EX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 537732
    if-eqz v2, :cond_0

    .line 537733
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 537734
    new-instance p0, LX/3EX;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/3EX;-><init>(LX/0ad;)V

    .line 537735
    move-object v0, p0

    .line 537736
    sput-object v0, LX/3EX;->c:LX/3EX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 537737
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 537738
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 537739
    :cond_1
    sget-object v0, LX/3EX;->c:LX/3EX;

    return-object v0

    .line 537740
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 537741
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 537742
    sget-object v0, LX/3EX;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 537743
    if-nez v0, :cond_0

    .line 537744
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 537745
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/3EX;->b:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v2, v3, v0, p2}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 537746
    const-string v0, "rtc_packetloss_limits"

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 537747
    return-object p2
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 537748
    iget-object v0, p0, LX/3EX;->b:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget v2, LX/3Dx;->eL:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(LX/0c0;I)V

    .line 537749
    return-void
.end method
