.class public LX/2M1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Tn;

.field private static volatile j:LX/2M1;


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/2Ly;

.field private final d:LX/0Zb;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/2aZ;

.field private final h:LX/0Uh;

.field private i:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/FCX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 396090
    sget-object v0, LX/0db;->a:LX/0Tn;

    const-string v1, "rich_media_reliability_serialized"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2M1;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/2Ly;LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/2aZ;LX/0Uh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/2Ly;",
            "LX/0Zb;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/2aZ;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 396143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396144
    const/4 v0, 0x0

    iput-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    .line 396145
    iput-object p1, p0, LX/2M1;->b:LX/0SG;

    .line 396146
    iput-object p2, p0, LX/2M1;->c:LX/2Ly;

    .line 396147
    iput-object p3, p0, LX/2M1;->d:LX/0Zb;

    .line 396148
    iput-object p4, p0, LX/2M1;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 396149
    iput-object p5, p0, LX/2M1;->f:LX/0Ot;

    .line 396150
    iput-object p6, p0, LX/2M1;->g:LX/2aZ;

    .line 396151
    iput-object p7, p0, LX/2M1;->h:LX/0Uh;

    .line 396152
    return-void
.end method

.method public static a(LX/0QB;)LX/2M1;
    .locals 11

    .prologue
    .line 396130
    sget-object v0, LX/2M1;->j:LX/2M1;

    if-nez v0, :cond_1

    .line 396131
    const-class v1, LX/2M1;

    monitor-enter v1

    .line 396132
    :try_start_0
    sget-object v0, LX/2M1;->j:LX/2M1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 396133
    if-eqz v2, :cond_0

    .line 396134
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 396135
    new-instance v3, LX/2M1;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v5

    check-cast v5, LX/2Ly;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v8, 0x259

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/2aZ;->a(LX/0QB;)LX/2aZ;

    move-result-object v9

    check-cast v9, LX/2aZ;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct/range {v3 .. v10}, LX/2M1;-><init>(LX/0SG;LX/2Ly;LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/2aZ;LX/0Uh;)V

    .line 396136
    move-object v0, v3

    .line 396137
    sput-object v0, LX/2M1;->j:LX/2M1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396138
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 396139
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 396140
    :cond_1
    sget-object v0, LX/2M1;->j:LX/2M1;

    return-object v0

    .line 396141
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 396142
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a()V
    .locals 4

    .prologue
    .line 396116
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 396117
    :goto_0
    monitor-exit p0

    return-void

    .line 396118
    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 396119
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 396120
    iget-object v2, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 396121
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->flush()V

    .line 396122
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 396123
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    .line 396124
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    .line 396125
    iget-object v0, p0, LX/2M1;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2M1;->a:LX/0Tn;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 396126
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 396127
    :try_start_2
    iget-object v0, p0, LX/2M1;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "rich_media_reliabilities_serialization_failed"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 396128
    iget-object v0, p0, LX/2M1;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2M1;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 396129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/2M1;Ljava/lang/String;LX/FCX;Lcom/facebook/messaging/model/send/SendError;)V
    .locals 6

    .prologue
    .line 396091
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "rich_media_msg_send"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 396092
    const-string v1, "otd"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396093
    const-string v1, "msgType"

    iget-object v2, p2, LX/FCX;->messageType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396094
    const-string v1, "media_send_source"

    iget-object v2, p2, LX/FCX;->mediaSource:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396095
    const-string v1, "mqttAttempts"

    iget v2, p2, LX/FCX;->mqttAttempts:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396096
    const-string v1, "graphAttempts"

    iget v2, p2, LX/FCX;->graphAttempts:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396097
    const-string v1, "preparationAttempts"

    iget v2, p2, LX/FCX;->preparationAttempts:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396098
    const-string v1, "attachmentCount"

    iget v2, p2, LX/FCX;->numberOfSubAttachments:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396099
    const-string v1, "totalSize"

    iget v2, p2, LX/FCX;->sizeInBytesOfSubAttachments:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396100
    const-string v1, "total_original_size"

    iget v2, p2, LX/FCX;->sizeInBytesOriginally:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396101
    const-string v1, "mediaType"

    iget-object v2, p2, LX/FCX;->mediaType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396102
    const-string v1, "mimeType"

    iget-object v2, p2, LX/FCX;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396103
    const-string v1, "outcome"

    iget-object v2, p2, LX/FCX;->outcome:LX/FCW;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396104
    if-nez p3, :cond_0

    .line 396105
    const-string v1, "sendSuccess"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396106
    const-string v1, "sendLatency"

    iget-object v2, p0, LX/2M1;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p2, LX/FCX;->startTimestamp:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396107
    :goto_0
    const-string v1, "duration"

    iget-wide v2, p2, LX/FCX;->mediaDurationMs:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396108
    const-string v1, "downsized_height"

    iget v2, p2, LX/FCX;->downsizedHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396109
    const-string v1, "downsized_width"

    iget v2, p2, LX/FCX;->downsizedWidth:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396110
    iget-object v1, p0, LX/2M1;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 396111
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396112
    monitor-exit p0

    return-void

    .line 396113
    :cond_0
    :try_start_1
    const-string v1, "sendSuccess"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 396114
    const-string v1, "finalError"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 396115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/2M1;)Ljava/util/LinkedHashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/FCX;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 396076
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/2M1;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 396077
    :goto_0
    monitor-exit p0

    return-object v0

    .line 396078
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2M1;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2M1;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 396079
    if-nez v0, :cond_1

    .line 396080
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 396081
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 396082
    :cond_1
    const/4 v1, 0x0

    :try_start_2
    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 396083
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 396084
    new-instance v0, Ljava/io/ObjectInputStream;

    invoke-direct {v0, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 396085
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashMap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 396086
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 396087
    :try_start_3
    iget-object v0, p0, LX/2M1;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "rich_media_reliabilities_deserialization_failed"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 396088
    iget-object v0, p0, LX/2M1;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2M1;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 396089
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private static c(Lcom/facebook/messaging/model/messages/Message;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 396072
    iget-object v3, p0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 396073
    int-to-long v6, v2

    iget-wide v8, v0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    add-long/2addr v6, v8

    long-to-int v2, v6

    .line 396074
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 396075
    :cond_0
    return v2
.end method

.method private static declared-synchronized c(LX/2M1;)V
    .locals 1

    .prologue
    .line 396153
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/2M1;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396154
    monitor-exit p0

    return-void

    .line 396155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()Z
    .locals 1

    .prologue
    .line 396068
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_0

    .line 396069
    invoke-static {p0}, LX/2M1;->b(LX/2M1;)Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    .line 396070
    :cond_0
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 396071
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static e(LX/2M1;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 396067
    iget-object v1, p0, LX/2M1;->h:LX/0Uh;

    const/16 v2, 0x1b4

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, LX/2M1;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/FCY;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 7

    .prologue
    .line 396051
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2M1;->e(LX/2M1;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 396052
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 396053
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCX;

    .line 396054
    if-nez v0, :cond_3

    .line 396055
    new-instance v1, LX/FCX;

    iget-object v0, p0, LX/2M1;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, p0, LX/2M1;->c:LX/2Ly;

    invoke-virtual {v0, p2}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v0}, LX/5zj;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    invoke-direct/range {v1 .. v6}, LX/FCX;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    .line 396056
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396057
    :goto_1
    iget v0, v1, LX/FCX;->sizeInBytesOfSubAttachments:I

    if-nez v0, :cond_2

    .line 396058
    invoke-static {p2}, LX/2M1;->c(Lcom/facebook/messaging/model/messages/Message;)I

    move-result v0

    iput v0, v1, LX/FCX;->sizeInBytesOfSubAttachments:I

    .line 396059
    :cond_2
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-wide v2, v0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    iput-wide v2, v1, LX/FCX;->mediaDurationMs:J

    .line 396060
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    iput v0, v1, LX/FCX;->downsizedHeight:I

    .line 396061
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    iput v0, v1, LX/FCX;->downsizedWidth:I

    .line 396062
    sget-object v0, LX/FCY;->MQTT:LX/FCY;

    if-ne p1, v0, :cond_4

    .line 396063
    iget v0, v1, LX/FCX;->mqttAttempts:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, LX/FCX;->mqttAttempts:I

    .line 396064
    :goto_2
    invoke-static {p0}, LX/2M1;->c(LX/2M1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 396065
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 396066
    :cond_4
    iget v0, v1, LX/FCX;->graphAttempts:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, LX/FCX;->graphAttempts:I

    goto :goto_2
.end method

.method public final declared-synchronized a(LX/FCY;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 396010
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2M1;->e(LX/2M1;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 396011
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 396012
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCX;

    .line 396013
    if-eqz v0, :cond_0

    .line 396014
    sget-object v1, LX/FCY;->MQTT:LX/FCY;

    if-ne p1, v1, :cond_2

    .line 396015
    sget-object v1, LX/FCW;->SUCCESS_MQTT:LX/FCW;

    iput-object v1, v0, LX/FCX;->outcome:LX/FCW;

    .line 396016
    :goto_1
    const/4 v1, 0x0

    invoke-static {p0, p2, v0, v1}, LX/2M1;->a(LX/2M1;Ljava/lang/String;LX/FCX;Lcom/facebook/messaging/model/send/SendError;)V

    .line 396017
    invoke-static {p0}, LX/2M1;->c(LX/2M1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 396018
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 396019
    :cond_2
    :try_start_2
    sget-object v1, LX/FCW;->SUCCESS_GRAPH:LX/FCW;

    iput-object v1, v0, LX/FCX;->outcome:LX/FCW;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 7

    .prologue
    .line 396040
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2M1;->e(LX/2M1;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 396041
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 396042
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCX;

    .line 396043
    if-nez v0, :cond_2

    .line 396044
    new-instance v1, LX/FCX;

    iget-object v0, p0, LX/2M1;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, p0, LX/2M1;->c:LX/2Ly;

    invoke-virtual {v0, p1}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v0}, LX/5zj;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    invoke-direct/range {v1 .. v6}, LX/FCX;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    .line 396045
    invoke-static {p1}, LX/2M1;->c(Lcom/facebook/messaging/model/messages/Message;)I

    move-result v0

    iput v0, v1, LX/FCX;->sizeInBytesOriginally:I

    .line 396046
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v0}, LX/2MK;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/FCX;->mediaType:Ljava/lang/String;

    .line 396047
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    iput-object v0, v1, LX/FCX;->mimeType:Ljava/lang/String;

    .line 396048
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396049
    :cond_2
    invoke-static {p0}, LX/2M1;->c(LX/2M1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 396050
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/messaging/model/send/SendError;)V
    .locals 2

    .prologue
    .line 396031
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2M1;->e(LX/2M1;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 396032
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 396033
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCX;

    .line 396034
    if-eqz v0, :cond_0

    .line 396035
    iget-object v1, p2, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-boolean v1, v1, LX/6fP;->shouldNotBeRetried:Z

    if-eqz v1, :cond_2

    sget-object v1, LX/FCW;->FAILURE_PERMANENT:LX/FCW;

    :goto_1
    iput-object v1, v0, LX/FCX;->outcome:LX/FCW;

    .line 396036
    invoke-static {p0, p1, v0, p2}, LX/2M1;->a(LX/2M1;Ljava/lang/String;LX/FCX;Lcom/facebook/messaging/model/send/SendError;)V

    .line 396037
    invoke-static {p0}, LX/2M1;->c(LX/2M1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 396038
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 396039
    :cond_2
    :try_start_2
    sget-object v1, LX/FCW;->FAILURE_RETRYABLE:LX/FCW;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized b(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 7

    .prologue
    .line 396022
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2M1;->e(LX/2M1;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 396023
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 396024
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCX;

    .line 396025
    if-nez v0, :cond_2

    .line 396026
    new-instance v1, LX/FCX;

    iget-object v0, p0, LX/2M1;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, p0, LX/2M1;->c:LX/2Ly;

    invoke-virtual {v0, p1}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v0}, LX/5zj;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    invoke-direct/range {v1 .. v6}, LX/FCX;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    .line 396027
    iget-object v0, p0, LX/2M1;->i:Ljava/util/LinkedHashMap;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396028
    :goto_1
    iget v0, v1, LX/FCX;->preparationAttempts:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, LX/FCX;->preparationAttempts:I

    .line 396029
    invoke-static {p0}, LX/2M1;->c(LX/2M1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 396030
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method

.method public final init()V
    .locals 1

    .prologue
    .line 396020
    iget-object v0, p0, LX/2M1;->g:LX/2aZ;

    invoke-virtual {v0}, LX/2aZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396021
    :cond_0
    return-void
.end method
