.class public LX/2lv;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1qM;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 459176
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1qM;
    .locals 11

    .prologue
    .line 459177
    const-class v1, LX/2lv;

    monitor-enter v1

    .line 459178
    :try_start_0
    sget-object v0, LX/2lv;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 459179
    sput-object v2, LX/2lv;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 459180
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459181
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 459182
    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/2l7;->a(LX/0QB;)LX/2l7;

    move-result-object v4

    check-cast v4, LX/2l7;

    invoke-static {v0}, LX/2lw;->a(LX/0QB;)LX/2lw;

    move-result-object v5

    check-cast v5, LX/2lw;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v6

    check-cast v6, LX/0lC;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v7

    check-cast v7, LX/11H;

    invoke-static {v0}, LX/2lx;->a(LX/0QB;)LX/2lx;

    move-result-object v8

    check-cast v8, LX/2lx;

    invoke-static {v0}, LX/2ly;->a(LX/0QB;)Ljava/lang/Integer;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-static {v0}, LX/2m0;->a(LX/0QB;)LX/2m0;

    move-result-object v10

    check-cast v10, LX/2m0;

    invoke-static/range {v3 .. v10}, LX/2lz;->a(Lcom/facebook/performancelogger/PerformanceLogger;LX/2l7;LX/2lw;LX/0lC;LX/11H;LX/2lx;Ljava/lang/Integer;LX/2m0;)LX/1qM;

    move-result-object v3

    move-object v0, v3

    .line 459183
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 459184
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1qM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 459185
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 459186
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 459187
    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {p0}, LX/2l7;->a(LX/0QB;)LX/2l7;

    move-result-object v1

    check-cast v1, LX/2l7;

    invoke-static {p0}, LX/2lw;->a(LX/0QB;)LX/2lw;

    move-result-object v2

    check-cast v2, LX/2lw;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v4

    check-cast v4, LX/11H;

    invoke-static {p0}, LX/2lx;->a(LX/0QB;)LX/2lx;

    move-result-object v5

    check-cast v5, LX/2lx;

    invoke-static {p0}, LX/2ly;->a(LX/0QB;)Ljava/lang/Integer;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-static {p0}, LX/2m0;->a(LX/0QB;)LX/2m0;

    move-result-object v7

    check-cast v7, LX/2m0;

    invoke-static/range {v0 .. v7}, LX/2lz;->a(Lcom/facebook/performancelogger/PerformanceLogger;LX/2l7;LX/2lw;LX/0lC;LX/11H;LX/2lx;Ljava/lang/Integer;LX/2m0;)LX/1qM;

    move-result-object v0

    return-object v0
.end method
