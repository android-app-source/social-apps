.class public final LX/39h;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/39d;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1zt;

.field public c:LX/1Wk;

.field public d:Z

.field public e:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/39d;


# direct methods
.method public constructor <init>(LX/39d;)V
    .locals 1

    .prologue
    .line 523678
    iput-object p1, p0, LX/39h;->f:LX/39d;

    .line 523679
    move-object v0, p1

    .line 523680
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 523681
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 523682
    const-string v0, "ReactionsFooterLikeButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 523683
    if-ne p0, p1, :cond_1

    .line 523684
    :cond_0
    :goto_0
    return v0

    .line 523685
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 523686
    goto :goto_0

    .line 523687
    :cond_3
    check-cast p1, LX/39h;

    .line 523688
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 523689
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 523690
    if-eq v2, v3, :cond_0

    .line 523691
    iget-object v2, p0, LX/39h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/39h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/39h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 523692
    goto :goto_0

    .line 523693
    :cond_5
    iget-object v2, p1, LX/39h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 523694
    :cond_6
    iget-object v2, p0, LX/39h;->b:LX/1zt;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/39h;->b:LX/1zt;

    iget-object v3, p1, LX/39h;->b:LX/1zt;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 523695
    goto :goto_0

    .line 523696
    :cond_8
    iget-object v2, p1, LX/39h;->b:LX/1zt;

    if-nez v2, :cond_7

    .line 523697
    :cond_9
    iget-object v2, p0, LX/39h;->c:LX/1Wk;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/39h;->c:LX/1Wk;

    iget-object v3, p1, LX/39h;->c:LX/1Wk;

    invoke-virtual {v2, v3}, LX/1Wk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 523698
    goto :goto_0

    .line 523699
    :cond_b
    iget-object v2, p1, LX/39h;->c:LX/1Wk;

    if-nez v2, :cond_a

    .line 523700
    :cond_c
    iget-boolean v2, p0, LX/39h;->d:Z

    iget-boolean v3, p1, LX/39h;->d:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 523701
    goto :goto_0

    .line 523702
    :cond_d
    iget-object v2, p0, LX/39h;->e:LX/1Pr;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/39h;->e:LX/1Pr;

    iget-object v3, p1, LX/39h;->e:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 523703
    goto :goto_0

    .line 523704
    :cond_e
    iget-object v2, p1, LX/39h;->e:LX/1Pr;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
