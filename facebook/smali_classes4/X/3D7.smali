.class public final LX/3D7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/3D6;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:LX/2nq;

.field public final synthetic d:Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;LX/3D6;LX/1Pq;LX/2nq;)V
    .locals 0

    .prologue
    .line 531554
    iput-object p1, p0, LX/3D7;->d:Lcom/facebook/notifications/multirow/partdefinition/NotificationsSinglePartDefinition;

    iput-object p2, p0, LX/3D7;->a:LX/3D6;

    iput-object p3, p0, LX/3D7;->b:LX/1Pq;

    iput-object p4, p0, LX/3D7;->c:LX/2nq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x2

    const v3, -0x7c79be3b

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 531555
    iget-object v0, p0, LX/3D7;->a:LX/3D6;

    .line 531556
    iget-object v4, v0, LX/3D6;->a:LX/03R;

    move-object v0, v4

    .line 531557
    sget-object v4, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v0, v4}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 531558
    iget-object v0, p0, LX/3D7;->a:LX/3D6;

    .line 531559
    iget-object v4, v0, LX/3D6;->a:LX/03R;

    move-object v0, v4

    .line 531560
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    if-nez v0, :cond_1

    .line 531561
    :goto_1
    iget-object v0, p0, LX/3D7;->a:LX/3D6;

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v2

    .line 531562
    iput-object v2, v0, LX/3D6;->a:LX/03R;

    .line 531563
    iget-object v0, p0, LX/3D7;->b:LX/1Pq;

    check-cast v0, LX/2kk;

    iget-object v2, p0, LX/3D7;->c:LX/2nq;

    invoke-interface {v0, v2, v1}, LX/2kk;->a(LX/2nq;Z)Z

    .line 531564
    iget-object v0, p0, LX/3D7;->b:LX/1Pq;

    check-cast v0, LX/2kl;

    invoke-interface {v0}, LX/2kl;->md_()LX/2jc;

    move-result-object v2

    iget-object v0, p0, LX/3D7;->c:LX/2nq;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v1, :cond_2

    sget-object v0, LX/Cfc;->COLLAPSE_RICH_ATTACHMENT_TAP:LX/Cfc;

    invoke-virtual {v0}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-interface {v2, v4, v0}, LX/2jc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 531565
    const v0, 0x68cd0b86

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    :cond_0
    move v0, v2

    .line 531566
    goto :goto_0

    :cond_1
    move v1, v2

    .line 531567
    goto :goto_1

    .line 531568
    :cond_2
    sget-object v0, LX/Cfc;->EXPAND_RICH_ATTACHMENT_TAP:LX/Cfc;

    invoke-virtual {v0}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
