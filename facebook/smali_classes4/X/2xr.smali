.class public LX/2xr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0aG;

.field private final b:LX/0pf;

.field private final c:Z


# direct methods
.method public constructor <init>(LX/0aG;Ljava/lang/Boolean;LX/0pf;)V
    .locals 1
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/api/feed/annotation/IsHScrollPositionPersistentCachingEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 479484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 479485
    iput-object p1, p0, LX/2xr;->a:LX/0aG;

    .line 479486
    iput-object p3, p0, LX/2xr;->b:LX/0pf;

    .line 479487
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/2xr;->c:Z

    .line 479488
    return-void
.end method

.method public static a(LX/0QB;)LX/2xr;
    .locals 1

    .prologue
    .line 479473
    invoke-static {p0}, LX/2xr;->b(LX/0QB;)LX/2xr;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2xr;
    .locals 5

    .prologue
    .line 479480
    new-instance v3, LX/2xr;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    .line 479481
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v2, 0x3d0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v1, v1

    .line 479482
    check-cast v1, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v2

    check-cast v2, LX/0pf;

    invoke-direct {v3, v0, v1, v2}, LX/2xr;-><init>(LX/0aG;Ljava/lang/Boolean;LX/0pf;)V

    .line 479483
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 479474
    iget-boolean v0, p0, LX/2xr;->c:Z

    if-nez v0, :cond_0

    .line 479475
    :goto_0
    return-object v4

    .line 479476
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 479477
    new-instance v0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v5}, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 479478
    const-string v1, "setHScrollUnitVisibleItemIndexKey"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 479479
    iget-object v0, p0, LX/2xr;->a:LX/0aG;

    const-string v1, "set_hscroll_unit_visible_item_index"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const v5, 0x3c91e6ab

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    goto :goto_0
.end method
