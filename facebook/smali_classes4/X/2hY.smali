.class public abstract LX/2hY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2dj;

.field public final b:LX/2hZ;

.field public final c:LX/2do;

.field public d:Z


# direct methods
.method public constructor <init>(LX/2dj;LX/2do;LX/2hZ;)V
    .locals 1

    .prologue
    .line 450102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450103
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2hY;->d:Z

    .line 450104
    iput-object p1, p0, LX/2hY;->a:LX/2dj;

    .line 450105
    iput-object p2, p0, LX/2hY;->c:LX/2do;

    .line 450106
    iput-object p3, p0, LX/2hY;->b:LX/2hZ;

    .line 450107
    return-void
.end method


# virtual methods
.method public final a(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V
    .locals 3

    .prologue
    .line 450096
    iget-object v0, p0, LX/2hY;->c:LX/2do;

    new-instance v1, LX/2f2;

    invoke-direct {v1, p1, p2, p3, p4}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 450097
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 450098
    iget-boolean v0, p0, LX/2hY;->d:Z

    move v0, v0

    .line 450099
    if-eqz v0, :cond_0

    .line 450100
    :goto_0
    return-void

    .line 450101
    :cond_0
    iget-object v0, p0, LX/2hY;->b:LX/2hZ;

    invoke-virtual {v0, p1}, LX/2hZ;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
