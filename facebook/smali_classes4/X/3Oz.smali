.class public final enum LX/3Oz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Oz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Oz;

.field public static final enum AVAILABLE:LX/3Oz;

.field public static final enum NONE:LX/3Oz;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 561016
    new-instance v0, LX/3Oz;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v2}, LX/3Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oz;->AVAILABLE:LX/3Oz;

    .line 561017
    new-instance v0, LX/3Oz;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/3Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Oz;->NONE:LX/3Oz;

    .line 561018
    const/4 v0, 0x2

    new-array v0, v0, [LX/3Oz;

    sget-object v1, LX/3Oz;->AVAILABLE:LX/3Oz;

    aput-object v1, v0, v2

    sget-object v1, LX/3Oz;->NONE:LX/3Oz;

    aput-object v1, v0, v3

    sput-object v0, LX/3Oz;->$VALUES:[LX/3Oz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 561019
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Oz;
    .locals 1

    .prologue
    .line 561020
    const-class v0, LX/3Oz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Oz;

    return-object v0
.end method

.method public static values()[LX/3Oz;
    .locals 1

    .prologue
    .line 561021
    sget-object v0, LX/3Oz;->$VALUES:[LX/3Oz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Oz;

    return-object v0
.end method
