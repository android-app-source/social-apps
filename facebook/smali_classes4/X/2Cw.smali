.class public LX/2Cw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/2Cw;


# instance fields
.field private final a:LX/0Uh;

.field public final b:LX/0SG;

.field public final c:Landroid/content/Context;

.field public final d:LX/0aU;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/2Cx;


# direct methods
.method public constructor <init>(LX/0Uh;LX/0SG;Landroid/content/Context;LX/0aU;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Cx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 383470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383471
    iput-object p1, p0, LX/2Cw;->a:LX/0Uh;

    .line 383472
    iput-object p2, p0, LX/2Cw;->b:LX/0SG;

    .line 383473
    iput-object p3, p0, LX/2Cw;->c:Landroid/content/Context;

    .line 383474
    iput-object p5, p0, LX/2Cw;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 383475
    iput-object p4, p0, LX/2Cw;->d:LX/0aU;

    .line 383476
    iput-object p6, p0, LX/2Cw;->f:LX/2Cx;

    .line 383477
    return-void
.end method

.method public static a(LX/0QB;)LX/2Cw;
    .locals 10

    .prologue
    .line 383457
    sget-object v0, LX/2Cw;->g:LX/2Cw;

    if-nez v0, :cond_1

    .line 383458
    const-class v1, LX/2Cw;

    monitor-enter v1

    .line 383459
    :try_start_0
    sget-object v0, LX/2Cw;->g:LX/2Cw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 383460
    if-eqz v2, :cond_0

    .line 383461
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 383462
    new-instance v3, LX/2Cw;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v7

    check-cast v7, LX/0aU;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Cx;->a(LX/0QB;)LX/2Cx;

    move-result-object v9

    check-cast v9, LX/2Cx;

    invoke-direct/range {v3 .. v9}, LX/2Cw;-><init>(LX/0Uh;LX/0SG;Landroid/content/Context;LX/0aU;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Cx;)V

    .line 383463
    move-object v0, v3

    .line 383464
    sput-object v0, LX/2Cw;->g:LX/2Cw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 383465
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 383466
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 383467
    :cond_1
    sget-object v0, LX/2Cw;->g:LX/2Cw;

    return-object v0

    .line 383468
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 383469
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Cw;LX/HlN;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 383449
    invoke-static {p0}, LX/2Cw;->d(LX/2Cw;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 383450
    :goto_0
    return-void

    .line 383451
    :cond_0
    iget-object v0, p0, LX/2Cw;->f:LX/2Cx;

    .line 383452
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 383453
    iget-object v1, v0, LX/2Cx;->b:LX/0Zb;

    const-string v2, "background_location_setting_refresh_start"

    invoke-static {v2}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v4, "reason"

    invoke-virtual {v2, v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383454
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 383455
    iget-object v1, p0, LX/2Cw;->d:LX/0aU;

    const-string v2, "BACKGROUND_LOCATION_REPORTING_ACTION_FETCH_IS_ENABLED_FINISHED"

    invoke-virtual {v1, v2}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 383456
    iget-object v1, p0, LX/2Cw;->c:Landroid/content/Context;

    iget-object v2, p0, LX/2Cw;->c:Landroid/content/Context;

    invoke-static {v2, v3, v0, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->a(Landroid/content/Context;Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static b(LX/2Cw;)LX/03R;
    .locals 3

    .prologue
    .line 383478
    iget-object v0, p0, LX/2Cw;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2Fn;->a:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 383479
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 383480
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2Cw;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2Fn;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(LX/2Cw;)Z
    .locals 2

    .prologue
    .line 383448
    iget-object v0, p0, LX/2Cw;->a:LX/0Uh;

    const/16 v1, 0x2e9

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0am;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 383431
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 383432
    iget-object v0, p0, LX/2Cw;->f:LX/2Cx;

    .line 383433
    iget-object v1, v0, LX/2Cx;->b:LX/0Zb;

    const-string v2, "background_location_setting_refresh_failure"

    invoke-static {v2}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383434
    :goto_0
    return-void

    .line 383435
    :cond_0
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 383436
    iget-object v1, p0, LX/2Cw;->f:LX/2Cx;

    .line 383437
    iget-object v2, v1, LX/2Cx;->b:LX/0Zb;

    const-string v3, "background_location_setting_refresh_success"

    invoke-static {v3}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "is_location_history_enabled"

    invoke-virtual {v3, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383438
    invoke-static {p0}, LX/2Cw;->b(LX/2Cw;)LX/03R;

    move-result-object v1

    .line 383439
    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-eq v1, v2, :cond_1

    invoke-virtual {v1}, LX/03R;->asBoolean()Z

    move-result v1

    if-eq v1, v0, :cond_2

    .line 383440
    :cond_1
    iget-object v1, p0, LX/2Cw;->f:LX/2Cx;

    .line 383441
    iget-object v2, v1, LX/2Cx;->b:LX/0Zb;

    const-string v3, "background_location_setting_change"

    invoke-static {v3}, LX/2Cx;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "is_location_history_enabled"

    invoke-virtual {v3, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 383442
    iget-object v1, p0, LX/2Cw;->d:LX/0aU;

    const-string v2, "BACKGROUND_LOCATION_REPORTING_SETTINGS_CHANGED_ACTION"

    invoke-virtual {v1, v2}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 383443
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 383444
    const-string v1, "is_location_history_enabled"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 383445
    iget-object v1, p0, LX/2Cw;->c:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 383446
    :cond_2
    iget-object v3, p0, LX/2Cw;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/2Fn;->a:LX/0Tn;

    invoke-interface {v3, v4, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    sget-object v4, LX/2Fn;->b:LX/0Tn;

    iget-object v5, p0, LX/2Cw;->b:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 383447
    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 383430
    invoke-static {p0}, LX/2Cw;->b(LX/2Cw;)LX/03R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public final init()V
    .locals 10

    .prologue
    .line 383420
    invoke-static {p0}, LX/2Cw;->d(LX/2Cw;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 383421
    :cond_0
    :goto_0
    return-void

    .line 383422
    :cond_1
    const/4 v0, 0x0

    .line 383423
    invoke-static {p0}, LX/2Cw;->b(LX/2Cw;)LX/03R;

    move-result-object v1

    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v1, v2, :cond_3

    .line 383424
    sget-object v0, LX/HlN;->FIRST:LX/HlN;

    .line 383425
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 383426
    invoke-static {p0, v0}, LX/2Cw;->a(LX/2Cw;LX/HlN;)V

    goto :goto_0

    .line 383427
    :cond_3
    iget-object v6, p0, LX/2Cw;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/2Fn;->b:LX/0Tn;

    const-wide/16 v8, -0x1

    invoke-interface {v6, v7, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    move-wide v2, v6

    .line 383428
    const-wide/32 v4, 0x2932e00

    add-long/2addr v2, v4

    iget-object v1, p0, LX/2Cw;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gtz v1, :cond_2

    .line 383429
    sget-object v0, LX/HlN;->TIME:LX/HlN;

    goto :goto_1
.end method
