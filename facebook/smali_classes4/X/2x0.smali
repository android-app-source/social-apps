.class public final LX/2x0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final synthetic a:LX/2wz;


# direct methods
.method public constructor <init>(LX/2wz;)V
    .locals 0

    iput-object p1, p0, LX/2x0;->a:LX/2wz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    iget-object v0, p0, LX/2x0;->a:LX/2wz;

    iget-object v0, v0, LX/2wz;->a:LX/2wN;

    iget-object v1, v0, LX/2wN;->a:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/2x0;->a:LX/2wz;

    iput-object p2, v0, LX/2wz;->f:Landroid/os/IBinder;

    iget-object v0, p0, LX/2x0;->a:LX/2wz;

    iput-object p1, v0, LX/2wz;->h:Landroid/content/ComponentName;

    iget-object v0, p0, LX/2x0;->a:LX/2wz;

    iget-object v0, v0, LX/2wz;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ServiceConnection;

    invoke-interface {v0, p1, p2}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2x0;->a:LX/2wz;

    const/4 v2, 0x1

    iput v2, v0, LX/2wz;->d:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    iget-object v0, p0, LX/2x0;->a:LX/2wz;

    iget-object v0, v0, LX/2wz;->a:LX/2wN;

    iget-object v1, v0, LX/2wN;->a:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/2x0;->a:LX/2wz;

    const/4 v2, 0x0

    iput-object v2, v0, LX/2wz;->f:Landroid/os/IBinder;

    iget-object v0, p0, LX/2x0;->a:LX/2wz;

    iput-object p1, v0, LX/2wz;->h:Landroid/content/ComponentName;

    iget-object v0, p0, LX/2x0;->a:LX/2wz;

    iget-object v0, v0, LX/2wz;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ServiceConnection;

    invoke-interface {v0, p1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2x0;->a:LX/2wz;

    const/4 v2, 0x2

    iput v2, v0, LX/2wz;->d:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
