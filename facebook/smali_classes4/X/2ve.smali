.class public final enum LX/2ve;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2ve;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2ve;

.field public static final enum ANDROID_PLATFORM_PREF:LX/2ve;

.field public static final enum DEFAULT:LX/2ve;

.field public static final enum GOOGLE_PLAY_PREF:LX/2ve;

.field public static final enum MOCK_MPK_STATIC_PREF:LX/2ve;


# instance fields
.field public final key:I

.field public final locationImplementation:LX/1wZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 478364
    new-instance v0, LX/2ve;

    const-string v1, "DEFAULT"

    const-string v4, "Default (Auto)"

    const/4 v5, 0x0

    move v3, v2

    invoke-direct/range {v0 .. v5}, LX/2ve;-><init>(Ljava/lang/String;IILjava/lang/String;LX/1wZ;)V

    sput-object v0, LX/2ve;->DEFAULT:LX/2ve;

    .line 478365
    new-instance v3, LX/2ve;

    const-string v4, "ANDROID_PLATFORM_PREF"

    const-string v7, "Android Platform"

    sget-object v8, LX/1wZ;->ANDROID_PLATFORM:LX/1wZ;

    move v5, v9

    move v6, v9

    invoke-direct/range {v3 .. v8}, LX/2ve;-><init>(Ljava/lang/String;IILjava/lang/String;LX/1wZ;)V

    sput-object v3, LX/2ve;->ANDROID_PLATFORM_PREF:LX/2ve;

    .line 478366
    new-instance v3, LX/2ve;

    const-string v4, "GOOGLE_PLAY_PREF"

    const-string v7, "Google Play Services"

    sget-object v8, LX/1wZ;->GOOGLE_PLAY:LX/1wZ;

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, LX/2ve;-><init>(Ljava/lang/String;IILjava/lang/String;LX/1wZ;)V

    sput-object v3, LX/2ve;->GOOGLE_PLAY_PREF:LX/2ve;

    .line 478367
    new-instance v3, LX/2ve;

    const-string v4, "MOCK_MPK_STATIC_PREF"

    const-string v7, "MPK Static"

    sget-object v8, LX/1wZ;->MOCK_MPK_STATIC:LX/1wZ;

    move v5, v11

    move v6, v11

    invoke-direct/range {v3 .. v8}, LX/2ve;-><init>(Ljava/lang/String;IILjava/lang/String;LX/1wZ;)V

    sput-object v3, LX/2ve;->MOCK_MPK_STATIC_PREF:LX/2ve;

    .line 478368
    const/4 v0, 0x4

    new-array v0, v0, [LX/2ve;

    sget-object v1, LX/2ve;->DEFAULT:LX/2ve;

    aput-object v1, v0, v2

    sget-object v1, LX/2ve;->ANDROID_PLATFORM_PREF:LX/2ve;

    aput-object v1, v0, v9

    sget-object v1, LX/2ve;->GOOGLE_PLAY_PREF:LX/2ve;

    aput-object v1, v0, v10

    sget-object v1, LX/2ve;->MOCK_MPK_STATIC_PREF:LX/2ve;

    aput-object v1, v0, v11

    sput-object v0, LX/2ve;->$VALUES:[LX/2ve;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;LX/1wZ;)V
    .locals 0
    .param p5    # LX/1wZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "LX/1wZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 478369
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 478370
    iput-object p4, p0, LX/2ve;->name:Ljava/lang/String;

    .line 478371
    iput p3, p0, LX/2ve;->key:I

    .line 478372
    iput-object p5, p0, LX/2ve;->locationImplementation:LX/1wZ;

    .line 478373
    return-void
.end method

.method public static get(I)LX/2ve;
    .locals 5

    .prologue
    .line 478374
    invoke-static {}, LX/2ve;->values()[LX/2ve;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 478375
    iget v4, v0, LX/2ve;->key:I

    if-ne v4, p0, :cond_0

    .line 478376
    :goto_1
    return-object v0

    .line 478377
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 478378
    :cond_1
    sget-object v0, LX/2ve;->DEFAULT:LX/2ve;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/2ve;
    .locals 1

    .prologue
    .line 478379
    const-class v0, LX/2ve;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2ve;

    return-object v0
.end method

.method public static values()[LX/2ve;
    .locals 1

    .prologue
    .line 478380
    sget-object v0, LX/2ve;->$VALUES:[LX/2ve;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2ve;

    return-object v0
.end method
