.class public LX/2v7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 477542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v1, 0x19

    .line 477538
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v1, :cond_1

    .line 477539
    :cond_0
    :goto_0
    return-object p1

    .line 477540
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 477541
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080023

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 477537
    const v0, 0x7f08111d

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z
    .locals 2

    .prologue
    .line 477536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x1e53800c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z
    .locals 2

    .prologue
    .line 477535
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x6001edea

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 3

    .prologue
    .line 477527
    invoke-static {p0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 477528
    const/4 v1, 0x0

    .line 477529
    if-eqz v0, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 477530
    if-nez v2, :cond_1

    .line 477531
    :cond_0
    :goto_1
    move v0, v1

    .line 477532
    return v0

    .line 477533
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v2

    .line 477534
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->VIDEO_DR_STYLE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-ne v2, p0, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 477526
    const v0, 0x46a1c4a4

    invoke-static {p0, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 477521
    const v1, -0x1e53800c

    invoke-static {p0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 477522
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 477523
    :cond_0
    :goto_0
    return v0

    .line 477524
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v1

    .line 477525
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->BUTTON_WITH_TEXT_ONLY:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->ATTACHMENT_AND_ENDSCREEN:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->VIDEO_DR_STYLE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 477543
    const v0, -0x22a42d2a    # -9.8999738E17f

    invoke-static {p0, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 477544
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 477519
    const v0, 0x278a7d5

    invoke-static {p0, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 477520
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 477517
    const v0, -0x5e32ca2f

    invoke-static {p0, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 477518
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 477512
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RICH_MEDIA_COLLECTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {p0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v1

    move v1, v1

    .line 477513
    if-nez v1, :cond_0

    .line 477514
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->RICH_MEDIA:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {p0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v1

    move v1, v1

    .line 477515
    if-eqz v1, :cond_1

    .line 477516
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, LX/2v7;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0}, LX/2v7;->g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0}, LX/2v7;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0}, LX/2v7;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0}, LX/2v7;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static j(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 477507
    const v1, -0x1e53800c

    invoke-static {p0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 477508
    if-eqz v1, :cond_0

    .line 477509
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ae()Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    move-result-object v1

    .line 477510
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;->VIDEO_DR_STYLE:Lcom/facebook/graphql/enums/GraphQLCallToActionStyle;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 477511
    :cond_0
    return v0
.end method

.method public static k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 477503
    const v1, -0x1e53800c

    invoke-static {p0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 477504
    if-eqz v1, :cond_0

    .line 477505
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->DONATE_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->INTERNAL_FLOW:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    .line 477506
    :cond_0
    return v0
.end method

.method public static l(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 477490
    if-nez p0, :cond_0

    move v0, v1

    .line 477491
    :goto_0
    return v0

    .line 477492
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v3

    .line 477493
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    .line 477494
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 477495
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 477496
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const p0, -0x1e53800c

    if-ne v5, p0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v5

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v5, p0, :cond_4

    const/4 v5, 0x1

    :goto_2
    move v5, v5

    .line 477497
    if-nez v5, :cond_1

    .line 477498
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const p0, 0x5a1cd4ef

    if-ne v5, p0, :cond_5

    const/4 v5, 0x1

    :goto_3
    move v0, v5

    .line 477499
    if-eqz v0, :cond_2

    .line 477500
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 477501
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 477502
    goto :goto_0

    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    goto :goto_3
.end method

.method public static n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 2

    .prologue
    .line 477488
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    .line 477489
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO_DIRECT_RESPONSE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
