.class public final enum LX/2ub;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2ub;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2ub;

.field public static final enum BACKGROUND:LX/2ub;

.field public static final enum COLD_START:LX/2ub;

.field public static final enum CONNECTIVITY:LX/2ub;

.field public static final enum FRAGMENT_LOADED:LX/2ub;

.field public static final enum GROUPS:LX/2ub;

.field public static final enum MQTT_FULL:LX/2ub;

.field public static final enum MQTT_NEW:LX/2ub;

.field public static final enum PAGES:LX/2ub;

.field public static final enum PAGES_COUNT_CHANGED:LX/2ub;

.field public static final enum PULL_TO_REFRESH:LX/2ub;

.field public static final enum PUSH:LX/2ub;

.field public static final enum SCROLL:LX/2ub;

.field public static final enum UNKNOWN:LX/2ub;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 476015
    new-instance v0, LX/2ub;

    const-string v1, "COLD_START"

    const-string v2, "cold_start"

    invoke-direct {v0, v1, v4, v2}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->COLD_START:LX/2ub;

    .line 476016
    new-instance v0, LX/2ub;

    const-string v1, "PULL_TO_REFRESH"

    const-string v2, "pull_to_refresh"

    invoke-direct {v0, v1, v5, v2}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    .line 476017
    new-instance v0, LX/2ub;

    const-string v1, "BACKGROUND"

    const-string v2, "background"

    invoke-direct {v0, v1, v6, v2}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->BACKGROUND:LX/2ub;

    .line 476018
    new-instance v0, LX/2ub;

    const-string v1, "MQTT_FULL"

    const-string v2, "mqtt"

    invoke-direct {v0, v1, v7, v2}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->MQTT_FULL:LX/2ub;

    .line 476019
    new-instance v0, LX/2ub;

    const-string v1, "MQTT_NEW"

    const-string v2, "mqtt"

    invoke-direct {v0, v1, v8, v2}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->MQTT_NEW:LX/2ub;

    .line 476020
    new-instance v0, LX/2ub;

    const-string v1, "PUSH"

    const/4 v2, 0x5

    const-string v3, "push"

    invoke-direct {v0, v1, v2, v3}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->PUSH:LX/2ub;

    .line 476021
    new-instance v0, LX/2ub;

    const-string v1, "SCROLL"

    const/4 v2, 0x6

    const-string v3, "scroll"

    invoke-direct {v0, v1, v2, v3}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->SCROLL:LX/2ub;

    .line 476022
    new-instance v0, LX/2ub;

    const-string v1, "PAGES"

    const/4 v2, 0x7

    const-string v3, "pages"

    invoke-direct {v0, v1, v2, v3}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->PAGES:LX/2ub;

    .line 476023
    new-instance v0, LX/2ub;

    const-string v1, "PAGES_COUNT_CHANGED"

    const/16 v2, 0x8

    const-string v3, "pages_count_changed"

    invoke-direct {v0, v1, v2, v3}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->PAGES_COUNT_CHANGED:LX/2ub;

    .line 476024
    new-instance v0, LX/2ub;

    const-string v1, "GROUPS"

    const/16 v2, 0x9

    const-string v3, "groups"

    invoke-direct {v0, v1, v2, v3}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->GROUPS:LX/2ub;

    .line 476025
    new-instance v0, LX/2ub;

    const-string v1, "FRAGMENT_LOADED"

    const/16 v2, 0xa

    const-string v3, "fragment_loaded"

    invoke-direct {v0, v1, v2, v3}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->FRAGMENT_LOADED:LX/2ub;

    .line 476026
    new-instance v0, LX/2ub;

    const-string v1, "CONNECTIVITY"

    const/16 v2, 0xb

    const-string v3, "connectivity"

    invoke-direct {v0, v1, v2, v3}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->CONNECTIVITY:LX/2ub;

    .line 476027
    new-instance v0, LX/2ub;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xc

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/2ub;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2ub;->UNKNOWN:LX/2ub;

    .line 476028
    const/16 v0, 0xd

    new-array v0, v0, [LX/2ub;

    sget-object v1, LX/2ub;->COLD_START:LX/2ub;

    aput-object v1, v0, v4

    sget-object v1, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    aput-object v1, v0, v5

    sget-object v1, LX/2ub;->BACKGROUND:LX/2ub;

    aput-object v1, v0, v6

    sget-object v1, LX/2ub;->MQTT_FULL:LX/2ub;

    aput-object v1, v0, v7

    sget-object v1, LX/2ub;->MQTT_NEW:LX/2ub;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2ub;->PUSH:LX/2ub;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2ub;->SCROLL:LX/2ub;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2ub;->PAGES:LX/2ub;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2ub;->PAGES_COUNT_CHANGED:LX/2ub;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2ub;->GROUPS:LX/2ub;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2ub;->FRAGMENT_LOADED:LX/2ub;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/2ub;->CONNECTIVITY:LX/2ub;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/2ub;->UNKNOWN:LX/2ub;

    aput-object v2, v0, v1

    sput-object v0, LX/2ub;->$VALUES:[LX/2ub;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 476012
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 476013
    iput-object p3, p0, LX/2ub;->name:Ljava/lang/String;

    .line 476014
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/2ub;
    .locals 5

    .prologue
    .line 476029
    if-nez p0, :cond_1

    .line 476030
    sget-object v0, LX/2ub;->UNKNOWN:LX/2ub;

    .line 476031
    :cond_0
    :goto_0
    return-object v0

    .line 476032
    :cond_1
    invoke-static {}, LX/2ub;->values()[LX/2ub;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 476033
    iget-object v4, v0, LX/2ub;->name:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 476034
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 476035
    :cond_2
    sget-object v0, LX/2ub;->UNKNOWN:LX/2ub;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/2ub;
    .locals 1

    .prologue
    .line 476011
    const-class v0, LX/2ub;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2ub;

    return-object v0
.end method

.method public static values()[LX/2ub;
    .locals 1

    .prologue
    .line 476010
    sget-object v0, LX/2ub;->$VALUES:[LX/2ub;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2ub;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 476009
    iget-object v0, p0, LX/2ub;->name:Ljava/lang/String;

    return-object v0
.end method
