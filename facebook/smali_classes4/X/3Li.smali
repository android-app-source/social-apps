.class public final LX/3Li;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 551871
    new-instance v0, LX/0U1;

    const-string v1, "user_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Li;->a:LX/0U1;

    .line 551872
    new-instance v0, LX/0U1;

    const-string v1, "last_call_time"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Li;->b:LX/0U1;

    .line 551873
    new-instance v0, LX/0U1;

    const-string v1, "log_id"

    const-string v2, "INTEGER PRIMARY KEY AUTOINCREMENT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Li;->c:LX/0U1;

    .line 551874
    new-instance v0, LX/0U1;

    const-string v1, "duration"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Li;->d:LX/0U1;

    .line 551875
    new-instance v0, LX/0U1;

    const-string v1, "answered"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Li;->e:LX/0U1;

    .line 551876
    new-instance v0, LX/0U1;

    const-string v1, "direction"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Li;->f:LX/0U1;

    .line 551877
    new-instance v0, LX/0U1;

    const-string v1, "call_type"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Li;->g:LX/0U1;

    .line 551878
    new-instance v0, LX/0U1;

    const-string v1, "acknowledged"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Li;->h:LX/0U1;

    .line 551879
    new-instance v0, LX/0U1;

    const-string v1, "seen"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Li;->i:LX/0U1;

    .line 551880
    new-instance v0, LX/0U1;

    const-string v1, "thread_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Li;->j:LX/0U1;

    .line 551881
    new-instance v0, LX/0U1;

    const-string v1, "on_going"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3Li;->k:LX/0U1;

    return-void
.end method
