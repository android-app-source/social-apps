.class public final enum LX/2XZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2XZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2XZ;

.field public static final enum CONNECTED:LX/2XZ;

.field public static final enum FREE_TIER_ONLY:LX/2XZ;

.field public static final enum NOT_CONNECTED:LX/2XZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 419944
    new-instance v0, LX/2XZ;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v2}, LX/2XZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2XZ;->CONNECTED:LX/2XZ;

    .line 419945
    new-instance v0, LX/2XZ;

    const-string v1, "FREE_TIER_ONLY"

    invoke-direct {v0, v1, v3}, LX/2XZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2XZ;->FREE_TIER_ONLY:LX/2XZ;

    .line 419946
    new-instance v0, LX/2XZ;

    const-string v1, "NOT_CONNECTED"

    invoke-direct {v0, v1, v4}, LX/2XZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2XZ;->NOT_CONNECTED:LX/2XZ;

    .line 419947
    const/4 v0, 0x3

    new-array v0, v0, [LX/2XZ;

    sget-object v1, LX/2XZ;->CONNECTED:LX/2XZ;

    aput-object v1, v0, v2

    sget-object v1, LX/2XZ;->FREE_TIER_ONLY:LX/2XZ;

    aput-object v1, v0, v3

    sget-object v1, LX/2XZ;->NOT_CONNECTED:LX/2XZ;

    aput-object v1, v0, v4

    sput-object v0, LX/2XZ;->$VALUES:[LX/2XZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 419941
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2XZ;
    .locals 1

    .prologue
    .line 419943
    const-class v0, LX/2XZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2XZ;

    return-object v0
.end method

.method public static values()[LX/2XZ;
    .locals 1

    .prologue
    .line 419942
    sget-object v0, LX/2XZ;->$VALUES:[LX/2XZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2XZ;

    return-object v0
.end method
