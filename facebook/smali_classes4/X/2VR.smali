.class public LX/2VR;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2VR;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 417707
    const-string v0, "quick_experiment"

    const/16 v1, 0x14

    new-instance v2, LX/2VS;

    invoke-direct {v2}, LX/2VS;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 417708
    return-void
.end method

.method public static a(LX/0QB;)LX/2VR;
    .locals 3

    .prologue
    .line 417709
    sget-object v0, LX/2VR;->a:LX/2VR;

    if-nez v0, :cond_1

    .line 417710
    const-class v1, LX/2VR;

    monitor-enter v1

    .line 417711
    :try_start_0
    sget-object v0, LX/2VR;->a:LX/2VR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 417712
    if-eqz v2, :cond_0

    .line 417713
    :try_start_1
    new-instance v0, LX/2VR;

    invoke-direct {v0}, LX/2VR;-><init>()V

    .line 417714
    move-object v0, v0

    .line 417715
    sput-object v0, LX/2VR;->a:LX/2VR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 417716
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 417717
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 417718
    :cond_1
    sget-object v0, LX/2VR;->a:LX/2VR;

    return-object v0

    .line 417719
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 417720
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
