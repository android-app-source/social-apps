.class public LX/2TY;
.super LX/2TZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2TZ",
        "<",
        "LX/3hT;",
        ">;"
    }
.end annotation


# instance fields
.field public b:I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 414439
    invoke-direct {p0, p1}, LX/2TZ;-><init>(Landroid/database/Cursor;)V

    .line 414440
    iput v0, p0, LX/2TY;->b:I

    .line 414441
    iput v0, p0, LX/2TY;->c:I

    .line 414442
    iput v0, p0, LX/2TY;->d:I

    .line 414443
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 414444
    iget v0, p0, LX/2TY;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 414445
    const-string v0, "fbid"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2TY;->b:I

    .line 414446
    const-string v0, "is_mobile_pushable"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2TY;->c:I

    .line 414447
    const-string v0, "is_messenger_user"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/2TY;->d:I

    .line 414448
    :cond_0
    new-instance v0, Lcom/facebook/user/model/UserKey;

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    iget v2, p0, LX/2TY;->b:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 414449
    iget v1, p0, LX/2TY;->c:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v1

    .line 414450
    iget v2, p0, LX/2TY;->d:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 414451
    new-instance v3, LX/3hT;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/03R;->asBoolean(Z)Z

    move-result v1

    invoke-direct {v3, v0, v1, v2}, LX/3hT;-><init>(Lcom/facebook/user/model/UserKey;ZZ)V

    return-object v3
.end method
