.class public LX/2d3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;


# instance fields
.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 442836
    sget-object v0, LX/2d4;->a:LX/0Tn;

    .line 442837
    sput-object v0, LX/2d3;->a:LX/0Tn;

    const-string v1, "post_compose_nux_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2d3;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 442832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442833
    iput-object p1, p0, LX/2d3;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 442834
    return-void
.end method

.method public static b(LX/0QB;)LX/2d3;
    .locals 2

    .prologue
    .line 442838
    new-instance v1, LX/2d3;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v0}, LX/2d3;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 442839
    return-object v1
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442835
    sget-object v0, LX/2d3;->b:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 442831
    iget-object v0, p0, LX/2d3;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2d3;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
