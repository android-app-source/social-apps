.class public final LX/3OE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:LX/3Na;


# direct methods
.method public constructor <init>(LX/3Na;)V
    .locals 0

    .prologue
    .line 560337
    iput-object p1, p0, LX/3OE;->a:LX/3Na;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 560338
    iget-object v0, p0, LX/3OE;->a:LX/3Na;

    iget-object v3, v0, LX/3Na;->b:Landroid/view/ViewGroup;

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 560339
    iget-object v0, p0, LX/3OE;->a:LX/3Na;

    iget-object v0, v0, LX/3Na;->d:Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/divebar/contacts/DivebarChatAvailabilityWarning;->setVisibility(I)V

    .line 560340
    return-void

    :cond_0
    move v0, v2

    .line 560341
    goto :goto_0

    :cond_1
    move v1, v2

    .line 560342
    goto :goto_1
.end method
