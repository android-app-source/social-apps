.class public final LX/32R;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/0yI;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/32R;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0yI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 490033
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 490034
    return-void
.end method

.method public static a(LX/0QB;)LX/32R;
    .locals 4

    .prologue
    .line 490035
    sget-object v0, LX/32R;->a:LX/32R;

    if-nez v0, :cond_1

    .line 490036
    const-class v1, LX/32R;

    monitor-enter v1

    .line 490037
    :try_start_0
    sget-object v0, LX/32R;->a:LX/32R;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 490038
    if-eqz v2, :cond_0

    .line 490039
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 490040
    new-instance v3, LX/32R;

    const/16 p0, 0x13f6

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/32R;-><init>(LX/0Ot;)V

    .line 490041
    move-object v0, v3

    .line 490042
    sput-object v0, LX/32R;->a:LX/32R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490043
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 490044
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 490045
    :cond_1
    sget-object v0, LX/32R;->a:LX/32R;

    return-object v0

    .line 490046
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 490047
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 490048
    check-cast p3, LX/0yI;

    .line 490049
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 490050
    const-string v1, "com.facebook.zero.ZERO_RATING_CLEAR_SETTINGS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 490051
    invoke-virtual {p3}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->l()V

    .line 490052
    :cond_0
    :goto_0
    return-void

    .line 490053
    :cond_1
    const-string v1, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490054
    const-string v0, "zero_token_request_reason"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/32P;

    .line 490055
    if-nez v0, :cond_2

    .line 490056
    sget-object v0, LX/32P;->UNKNOWN_REASON:LX/32P;

    .line 490057
    :cond_2
    invoke-virtual {p3, v0}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(LX/32P;)V

    .line 490058
    goto :goto_0
.end method
