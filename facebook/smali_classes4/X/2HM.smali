.class public LX/2HM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Gb;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/2HM;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gn;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2Gc;

.field private final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 390283
    const-class v0, LX/2HM;

    sput-object v0, LX/2HM;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/2Gc;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2Gn;",
            ">;",
            "LX/2Gc;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390314
    iput-object p1, p0, LX/2HM;->b:LX/0Ot;

    .line 390315
    iput-object p2, p0, LX/2HM;->c:LX/2Gc;

    .line 390316
    invoke-virtual {p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2HM;->d:Ljava/lang/String;

    .line 390317
    return-void
.end method

.method public static a(LX/0QB;)LX/2HM;
    .locals 6

    .prologue
    .line 390300
    sget-object v0, LX/2HM;->e:LX/2HM;

    if-nez v0, :cond_1

    .line 390301
    const-class v1, LX/2HM;

    monitor-enter v1

    .line 390302
    :try_start_0
    sget-object v0, LX/2HM;->e:LX/2HM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390303
    if-eqz v2, :cond_0

    .line 390304
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390305
    new-instance v5, LX/2HM;

    const/16 v3, 0xffc

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/2Gc;->a(LX/0QB;)LX/2Gc;

    move-result-object v3

    check-cast v3, LX/2Gc;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {v5, p0, v3, v4}, LX/2HM;-><init>(LX/0Ot;LX/2Gc;Landroid/content/Context;)V

    .line 390306
    move-object v0, v5

    .line 390307
    sput-object v0, LX/2HM;->e:LX/2HM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390308
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390309
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390310
    :cond_1
    sget-object v0, LX/2HM;->e:LX/2HM;

    return-object v0

    .line 390311
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390312
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 390299
    iget-object v0, p0, LX/2HM;->c:LX/2Gc;

    sget-object v1, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gc;->a(LX/2Ge;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/2Ge;
    .locals 1

    .prologue
    .line 390298
    sget-object v0, LX/2Ge;->FBNS_LITE:LX/2Ge;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 390295
    invoke-direct {p0}, LX/2HM;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390296
    iget-object v0, p0, LX/2HM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gn;

    invoke-virtual {v0, p1}, LX/2Gn;->a(Ljava/lang/String;)V

    .line 390297
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 390292
    invoke-direct {p0}, LX/2HM;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390293
    iget-object v0, p0, LX/2HM;->c:LX/2Gc;

    iget-object v1, p0, LX/2HM;->d:Ljava/lang/String;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler$FbnsLiteCallbackReceiver;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, LX/2Gc;->a(Ljava/lang/String;[Ljava/lang/Class;)V

    .line 390294
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 390289
    invoke-direct {p0}, LX/2HM;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390290
    iget-object v0, p0, LX/2HM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gn;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2Gn;->a(Z)V

    .line 390291
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 390286
    invoke-direct {p0}, LX/2HM;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390287
    iget-object v0, p0, LX/2HM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2Gn;->a(Z)V

    .line 390288
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 390284
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/2HM;->a(Ljava/lang/String;)V

    .line 390285
    return-void
.end method
