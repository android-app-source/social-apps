.class public final enum LX/2EU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2EU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2EU;

.field public static final enum CHANNEL_CONNECTED:LX/2EU;

.field public static final enum CHANNEL_CONNECTING:LX/2EU;

.field public static final enum CHANNEL_DISCONNECTED:LX/2EU;

.field public static final enum UNKNOWN:LX/2EU;


# instance fields
.field private mClockSkewDetected:Z

.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 385447
    new-instance v0, LX/2EU;

    const-string v1, "CHANNEL_CONNECTING"

    invoke-direct {v0, v1, v2, v2}, LX/2EU;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2EU;->CHANNEL_CONNECTING:LX/2EU;

    .line 385448
    new-instance v0, LX/2EU;

    const-string v1, "CHANNEL_CONNECTED"

    invoke-direct {v0, v1, v3, v3}, LX/2EU;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2EU;->CHANNEL_CONNECTED:LX/2EU;

    .line 385449
    new-instance v0, LX/2EU;

    const-string v1, "CHANNEL_DISCONNECTED"

    invoke-direct {v0, v1, v4, v4}, LX/2EU;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2EU;->CHANNEL_DISCONNECTED:LX/2EU;

    .line 385450
    new-instance v0, LX/2EU;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5, v5}, LX/2EU;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/2EU;->UNKNOWN:LX/2EU;

    .line 385451
    const/4 v0, 0x4

    new-array v0, v0, [LX/2EU;

    sget-object v1, LX/2EU;->CHANNEL_CONNECTING:LX/2EU;

    aput-object v1, v0, v2

    sget-object v1, LX/2EU;->CHANNEL_CONNECTED:LX/2EU;

    aput-object v1, v0, v3

    sget-object v1, LX/2EU;->CHANNEL_DISCONNECTED:LX/2EU;

    aput-object v1, v0, v4

    sget-object v1, LX/2EU;->UNKNOWN:LX/2EU;

    aput-object v1, v0, v5

    sput-object v0, LX/2EU;->$VALUES:[LX/2EU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 385444
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 385445
    iput p3, p0, LX/2EU;->value:I

    .line 385446
    return-void
.end method

.method public static fromValue(I)LX/2EU;
    .locals 5

    .prologue
    .line 385439
    invoke-static {}, LX/2EU;->values()[LX/2EU;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 385440
    iget v4, v0, LX/2EU;->value:I

    if-ne v4, p0, :cond_0

    .line 385441
    :goto_1
    return-object v0

    .line 385442
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 385443
    :cond_1
    sget-object v0, LX/2EU;->UNKNOWN:LX/2EU;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/2EU;
    .locals 1

    .prologue
    .line 385433
    const-class v0, LX/2EU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2EU;

    return-object v0
.end method

.method public static values()[LX/2EU;
    .locals 1

    .prologue
    .line 385438
    sget-object v0, LX/2EU;->$VALUES:[LX/2EU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2EU;

    return-object v0
.end method


# virtual methods
.method public final isClockSkewDetected()Z
    .locals 1

    .prologue
    .line 385437
    iget-boolean v0, p0, LX/2EU;->mClockSkewDetected:Z

    return v0
.end method

.method public final setClockSkewDetected(Z)V
    .locals 0

    .prologue
    .line 385435
    iput-boolean p1, p0, LX/2EU;->mClockSkewDetected:Z

    .line 385436
    return-void
.end method

.method public final toValue()I
    .locals 1

    .prologue
    .line 385434
    iget v0, p0, LX/2EU;->value:I

    return v0
.end method
