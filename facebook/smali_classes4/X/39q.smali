.class public final LX/39q;
.super LX/1dc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1dc",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/1Wk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 523975
    invoke-static {}, LX/39p;->a()LX/39p;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1dc;-><init>(LX/1n4;)V

    .line 523976
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 523977
    const-string v0, "PressedStateDrawableReference"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 523978
    if-ne p0, p1, :cond_1

    .line 523979
    :cond_0
    :goto_0
    return v0

    .line 523980
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 523981
    goto :goto_0

    .line 523982
    :cond_3
    check-cast p1, LX/39q;

    .line 523983
    iget-object v2, p0, LX/39q;->a:LX/1Wk;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/39q;->a:LX/1Wk;

    iget-object v3, p1, LX/39q;->a:LX/1Wk;

    invoke-virtual {v2, v3}, LX/1Wk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 523984
    goto :goto_0

    .line 523985
    :cond_4
    iget-object v2, p1, LX/39q;->a:LX/1Wk;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
