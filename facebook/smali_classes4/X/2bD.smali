.class public LX/2bD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2bD;


# instance fields
.field private final a:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .param p1    # Ljava/lang/Class;
        .annotation runtime Lcom/facebook/push/fbpushdata/common/FbPushDataHandlerServiceClass;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 428651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 428652
    iput-object p1, p0, LX/2bD;->a:Ljava/lang/Class;

    .line 428653
    return-void
.end method

.method public static a(LX/0QB;)LX/2bD;
    .locals 4

    .prologue
    .line 428636
    sget-object v0, LX/2bD;->b:LX/2bD;

    if-nez v0, :cond_1

    .line 428637
    const-class v1, LX/2bD;

    monitor-enter v1

    .line 428638
    :try_start_0
    sget-object v0, LX/2bD;->b:LX/2bD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 428639
    if-eqz v2, :cond_0

    .line 428640
    :try_start_1
    new-instance p0, LX/2bD;

    .line 428641
    const-class v3, Lcom/facebook/push/fbpushdata/FbPushDataHandlerService;

    move-object v3, v3

    .line 428642
    move-object v3, v3

    .line 428643
    check-cast v3, Ljava/lang/Class;

    invoke-direct {p0, v3}, LX/2bD;-><init>(Ljava/lang/Class;)V

    .line 428644
    move-object v0, p0

    .line 428645
    sput-object v0, LX/2bD;->b:LX/2bD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 428646
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 428647
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 428648
    :cond_1
    sget-object v0, LX/2bD;->b:LX/2bD;

    return-object v0

    .line 428649
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 428650
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;LX/3B4;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 428654
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, LX/2bD;->a(Landroid/content/Context;Ljava/lang/String;LX/3B4;Ljava/lang/String;Ljava/lang/String;)V

    .line 428655
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;LX/3B4;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 428633
    invoke-virtual/range {p0 .. p5}, LX/2bD;->b(Landroid/content/Context;Ljava/lang/String;LX/3B4;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 428634
    :try_start_0
    invoke-static {p1, v0}, LX/37V;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 428635
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;LX/3B4;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 428627
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/2bD;->a:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 428628
    const-string v1, "push_content"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 428629
    const-string v1, "push_source"

    invoke-virtual {p3}, LX/3B4;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 428630
    const-string v1, "extra_notification_sender"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 428631
    const-string v1, "extra_notification_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 428632
    return-object v0
.end method
