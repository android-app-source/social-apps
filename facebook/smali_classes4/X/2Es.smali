.class public final LX/2Es;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "StartServiceParams.this"
    .end annotation
.end field


# instance fields
.field public final b:Landroid/os/Messenger;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/2DI;

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:I

.field public final g:LX/2Er;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Landroid/content/Context;

.field private i:LX/2Xu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Landroid/os/PowerManager$WakeLock;


# direct methods
.method private constructor <init>(Landroid/os/Messenger;Landroid/os/Bundle;Ljava/lang/String;LX/2DI;ILX/2Er;Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/os/Messenger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/2Er;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 386070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386071
    iput-object p1, p0, LX/2Es;->b:Landroid/os/Messenger;

    .line 386072
    iput-object p2, p0, LX/2Es;->c:Landroid/os/Bundle;

    .line 386073
    iput-object p3, p0, LX/2Es;->e:Ljava/lang/String;

    .line 386074
    iput-object p4, p0, LX/2Es;->d:LX/2DI;

    .line 386075
    iput p5, p0, LX/2Es;->f:I

    .line 386076
    iput-object p7, p0, LX/2Es;->h:Landroid/content/Context;

    .line 386077
    iput-object p6, p0, LX/2Es;->g:LX/2Er;

    .line 386078
    return-void
.end method

.method public static a(LX/2bw;Landroid/os/Bundle;Ljava/lang/String;LX/2DI;ILX/2Er;)LX/2Es;
    .locals 8
    .param p0    # LX/2bw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/2Er;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 386079
    if-eqz p0, :cond_0

    .line 386080
    new-instance v0, LX/2bx;

    invoke-direct {v0, p0}, LX/2bx;-><init>(LX/2bw;)V

    .line 386081
    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, v0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    .line 386082
    invoke-static {}, LX/2Es;->h()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386083
    :goto_0
    new-instance v0, LX/2Es;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, LX/2Es;-><init>(Landroid/os/Messenger;Landroid/os/Bundle;Ljava/lang/String;LX/2DI;ILX/2Er;Landroid/content/Context;)V

    return-object v0

    :cond_0
    move-object v1, v7

    goto :goto_0
.end method

.method public static a(Landroid/os/Bundle;Landroid/content/Context;)LX/2Es;
    .locals 8

    .prologue
    const/4 v4, -0x1

    .line 386084
    const-string v0, "_messenger"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    .line 386085
    const-string v0, "_extras"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 386086
    const-string v0, "_hack_action"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 386087
    const-string v0, "_job_id"

    invoke-virtual {p0, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 386088
    if-ne v5, v4, :cond_0

    .line 386089
    new-instance v0, LX/403;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "_job_id is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_job_id"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/403;-><init>(Ljava/lang/String;)V

    throw v0

    .line 386090
    :cond_0
    const-string v0, "_fallback_config"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    .line 386091
    new-instance v0, LX/2Es;

    new-instance v4, LX/2DI;

    const-string v6, "_upload_job_config"

    invoke-virtual {p0, v6}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    invoke-direct {v4, v6}, LX/2DI;-><init>(Landroid/os/Bundle;)V

    if-eqz v7, :cond_1

    new-instance v6, LX/2Er;

    invoke-direct {v6, v7}, LX/2Er;-><init>(Landroid/os/Bundle;)V

    :goto_0
    move-object v7, p1

    invoke-direct/range {v0 .. v7}, LX/2Es;-><init>(Landroid/os/Messenger;Landroid/os/Bundle;Ljava/lang/String;LX/2DI;ILX/2Er;Landroid/content/Context;)V

    return-object v0

    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static h()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 386092
    const-class v1, LX/2Es;

    monitor-enter v1

    .line 386093
    :try_start_0
    sget-object v0, LX/2Es;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 386094
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/2Es;->a:Ljava/util/List;

    .line 386095
    :cond_0
    sget-object v0, LX/2Es;->a:Ljava/util/List;

    monitor-exit v1

    return-object v0

    .line 386096
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 8

    .prologue
    .line 386097
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 386098
    const-string v1, "_messenger"

    iget-object v2, p0, LX/2Es;->b:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 386099
    const-string v1, "_extras"

    iget-object v2, p0, LX/2Es;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 386100
    const-string v1, "_hack_action"

    iget-object v2, p0, LX/2Es;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 386101
    const-string v1, "_upload_job_config"

    new-instance v2, Landroid/os/Bundle;

    iget-object v3, p0, LX/2Es;->d:LX/2DI;

    .line 386102
    new-instance v4, LX/2Et;

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-direct {v4, v5}, LX/2Et;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v3, v4}, LX/2DI;->a(LX/2Ev;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Bundle;

    move-object v3, v4

    .line 386103
    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 386104
    const-string v1, "_job_id"

    iget v2, p0, LX/2Es;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 386105
    iget-object v1, p0, LX/2Es;->g:LX/2Er;

    if-eqz v1, :cond_0

    .line 386106
    const-string v1, "_fallback_config"

    iget-object v2, p0, LX/2Es;->g:LX/2Er;

    .line 386107
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 386108
    const-string v5, "min_delay_ms"

    iget-wide v6, v2, LX/2Er;->a:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 386109
    const-string v5, "max_delay_ms"

    iget-wide v6, v2, LX/2Er;->b:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 386110
    move-object v2, v4

    .line 386111
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 386112
    :cond_0
    return-object v0
.end method

.method public final b()LX/2Xu;
    .locals 2

    .prologue
    .line 386113
    iget-object v0, p0, LX/2Es;->i:LX/2Xu;

    if-nez v0, :cond_0

    .line 386114
    new-instance v0, LX/2Xv;

    iget-object v1, p0, LX/2Es;->h:Landroid/content/Context;

    invoke-static {v1}, LX/2En;->a(Landroid/content/Context;)LX/2En;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/2Xv;-><init>(LX/2Es;LX/2En;)V

    iput-object v0, p0, LX/2Es;->i:LX/2Xu;

    .line 386115
    :cond_0
    iget-object v0, p0, LX/2Es;->i:LX/2Xu;

    return-object v0
.end method
