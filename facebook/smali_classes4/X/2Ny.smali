.class public LX/2Ny;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Ny;


# instance fields
.field private final a:LX/2MM;


# direct methods
.method public constructor <init>(LX/2MM;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 400339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400340
    iput-object p1, p0, LX/2Ny;->a:LX/2MM;

    .line 400341
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ny;
    .locals 4

    .prologue
    .line 400342
    sget-object v0, LX/2Ny;->b:LX/2Ny;

    if-nez v0, :cond_1

    .line 400343
    const-class v1, LX/2Ny;

    monitor-enter v1

    .line 400344
    :try_start_0
    sget-object v0, LX/2Ny;->b:LX/2Ny;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 400345
    if-eqz v2, :cond_0

    .line 400346
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 400347
    new-instance p0, LX/2Ny;

    invoke-static {v0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v3

    check-cast v3, LX/2MM;

    invoke-direct {p0, v3}, LX/2Ny;-><init>(LX/2MM;)V

    .line 400348
    move-object v0, p0

    .line 400349
    sput-object v0, LX/2Ny;->b:LX/2Ny;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 400350
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 400351
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 400352
    :cond_1
    sget-object v0, LX/2Ny;->b:LX/2Ny;

    return-object v0

    .line 400353
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 400354
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 400355
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-static {v0}, LX/03l;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)LX/46f;
    .locals 3

    .prologue
    .line 400356
    iget-object v0, p0, LX/2Ny;->a:LX/2MM;

    sget-object v1, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, p1, v1}, LX/2MM;->a(Landroid/net/Uri;LX/46h;)LX/46f;

    move-result-object v0

    .line 400357
    iget-object v1, v0, LX/46f;->a:Ljava/io/File;

    if-nez v1, :cond_0

    .line 400358
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failed to resolve backing file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400359
    :cond_0
    return-object v0
.end method
