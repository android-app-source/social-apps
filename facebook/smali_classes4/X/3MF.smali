.class public LX/3MF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final g:Landroid/net/Uri;

.field private static final h:[Ljava/lang/String;

.field private static final i:[Ljava/lang/String;

.field public static final j:Ljava/util/regex/Pattern;

.field private static volatile k:LX/3MF;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/ContentResolver;

.field private final d:LX/2Or;

.field private final e:LX/3MG;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Lx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 553861
    const-string v0, "content://mms-sms/canonical-addresses"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/3MF;->a:Landroid/net/Uri;

    .line 553862
    sget-object v0, LX/2Uo;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "simple"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/3MF;->g:Landroid/net/Uri;

    .line 553863
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "recipient_ids"

    aput-object v1, v0, v3

    sput-object v0, LX/3MF;->h:[Ljava/lang/String;

    .line 553864
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "address"

    aput-object v1, v0, v4

    sput-object v0, LX/3MF;->i:[Ljava/lang/String;

    .line 553865
    const-string v0, "(.*?[A-Za-z0-9._%+-]+)@([A-Za-z0-9.-]+\\.[A-Za-z]{2,}.*?)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/3MF;->j:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/2Or;LX/3MG;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/2Or;",
            "LX/3MG;",
            "LX/0Ot",
            "<",
            "LX/3Lx;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 553854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 553855
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/3MF;->b:Ljava/util/Map;

    .line 553856
    iput-object p1, p0, LX/3MF;->c:Landroid/content/ContentResolver;

    .line 553857
    iput-object p2, p0, LX/3MF;->d:LX/2Or;

    .line 553858
    iput-object p3, p0, LX/3MF;->e:LX/3MG;

    .line 553859
    iput-object p4, p0, LX/3MF;->f:LX/0Ot;

    .line 553860
    return-void
.end method

.method public static a(LX/0QB;)LX/3MF;
    .locals 7

    .prologue
    .line 553841
    sget-object v0, LX/3MF;->k:LX/3MF;

    if-nez v0, :cond_1

    .line 553842
    const-class v1, LX/3MF;

    monitor-enter v1

    .line 553843
    :try_start_0
    sget-object v0, LX/3MF;->k:LX/3MF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 553844
    if-eqz v2, :cond_0

    .line 553845
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 553846
    new-instance v6, LX/3MF;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v4

    check-cast v4, LX/2Or;

    invoke-static {v0}, LX/3MG;->a(LX/0QB;)LX/3MG;

    move-result-object v5

    check-cast v5, LX/3MG;

    const/16 p0, 0x123d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/3MF;-><init>(Landroid/content/ContentResolver;LX/2Or;LX/3MG;LX/0Ot;)V

    .line 553847
    move-object v0, v6

    .line 553848
    sput-object v0, LX/3MF;->k:LX/3MF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 553849
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 553850
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 553851
    :cond_1
    sget-object v0, LX/3MF;->k:LX/3MF;

    return-object v0

    .line 553852
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 553853
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 553757
    const-string v0, "address"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 553758
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 553759
    iget-object v1, p0, LX/3MF;->d:LX/2Or;

    invoke-virtual {v1}, LX/2Or;->r()Z

    move-result v1

    if-nez v1, :cond_0

    .line 553760
    invoke-virtual {p0, v0}, LX/3MF;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 553761
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 553832
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 553833
    const-string v1, ""

    .line 553834
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 553835
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 553836
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553837
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553838
    const-string v0, " "

    :goto_1
    move-object v1, v0

    .line 553839
    goto :goto_0

    .line 553840
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private static a(LX/3MF;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 553813
    const-string v0, "SmsRecipientUtil.primeCache"

    const v1, 0x57211374

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 553814
    :try_start_0
    iget-object v0, p0, LX/3MF;->c:Landroid/content/ContentResolver;

    sget-object v1, LX/3MF;->a:Landroid/net/Uri;

    sget-object v2, LX/3MF;->i:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id limit 100"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 553815
    if-nez v1, :cond_1

    .line 553816
    if-eqz v1, :cond_0

    .line 553817
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 553818
    :cond_0
    const v0, 0x7254744a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 553819
    :goto_0
    return-void

    .line 553820
    :cond_1
    :try_start_1
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 553821
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 553822
    invoke-direct {p0, v1}, LX/3MF;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 553823
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 553824
    iget-object v3, p0, LX/3MF;->b:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 553825
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    .line 553826
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 553827
    :cond_2
    const v1, 0x4d3a413f    # 1.95302384E8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 553828
    :cond_3
    if-eqz v1, :cond_4

    .line 553829
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 553830
    :cond_4
    const v0, 0x168ffa35

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 553831
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 553800
    invoke-static {p0}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 553801
    sget-object v0, LX/3MF;->j:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 553802
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 553803
    new-instance v0, LX/44w;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 553804
    :goto_0
    move-object v0, v0

    .line 553805
    if-eqz v0, :cond_2

    .line 553806
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "XXXX@"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/44w;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 553807
    :goto_1
    return-object v0

    .line 553808
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 553809
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 553810
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XXXX"

    invoke-virtual {v1, v2, v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 553811
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v0, v0, -0x4

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "XXXX"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 553812
    :cond_2
    const-string v0, "XXXX"

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/3MF;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 553779
    const-string v0, "SmsRecipientUtil.getAddressesFromDb"

    const v1, -0x6620b089

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 553780
    :try_start_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 553781
    const-string v0, "_id"

    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 553782
    :try_start_1
    iget-object v0, p0, LX/3MF;->c:Landroid/content/ContentResolver;

    sget-object v1, LX/3MF;->a:Landroid/net/Uri;

    sget-object v2, LX/3MF;->i:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v1

    .line 553783
    if-nez v1, :cond_1

    .line 553784
    if-eqz v1, :cond_0

    .line 553785
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 553786
    :cond_0
    const v0, 0x59af2536

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v6

    :goto_0
    return-object v0

    .line 553787
    :cond_1
    :try_start_3
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 553788
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 553789
    invoke-direct {p0, v1}, LX/3MF;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 553790
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 553791
    iget-object v3, p0, LX/3MF;->b:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553792
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 553793
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    .line 553794
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 553795
    :catchall_1
    move-exception v0

    const v1, -0xe0bafa4

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 553796
    :cond_3
    if-eqz v1, :cond_4

    .line 553797
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 553798
    :cond_4
    const v0, -0x122a66b9

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v6

    goto :goto_0

    .line 553799
    :catchall_2
    move-exception v0

    move-object v1, v7

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)LX/0tf;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "LX/0tf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 553762
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 553763
    :cond_0
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    .line 553764
    :goto_0
    return-object v0

    .line 553765
    :cond_1
    iget-object v0, p0, LX/3MF;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 553766
    invoke-static {p0}, LX/3MF;->a(LX/3MF;)V

    .line 553767
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 553768
    new-instance v1, LX/0tf;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, LX/0tf;-><init>(I)V

    .line 553769
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 553770
    iget-object v0, p0, LX/3MF;->b:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 553771
    iget-object v0, p0, LX/3MF;->b:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v4, v5, v0}, LX/0tf;->b(JLjava/lang/Object;)V

    goto :goto_1

    .line 553772
    :cond_3
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 553773
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 553774
    invoke-static {p0, v2}, LX/3MF;->b(LX/3MF;Ljava/util/List;)Ljava/util/List;

    .line 553775
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 553776
    iget-object v0, p0, LX/3MF;->b:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 553777
    iget-object v0, p0, LX/3MF;->b:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v4, v5, v0}, LX/0tf;->b(JLjava/lang/Object;)V

    goto :goto_2

    :cond_6
    move-object v0, v1

    .line 553778
    goto/16 :goto_0
.end method

.method public final a(J)Ljava/lang/String;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 553749
    const-string v0, "_id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 553750
    iget-object v0, p0, LX/3MF;->c:Landroid/content/ContentResolver;

    sget-object v1, LX/3MF;->g:Landroid/net/Uri;

    sget-object v2, LX/3MF;->h:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 553751
    if-eqz v1, :cond_0

    .line 553752
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 553753
    const-string v0, "recipient_ids"

    invoke-static {v1, v0}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 553754
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 553755
    :cond_0
    :goto_0
    return-object v5

    .line 553756
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 553729
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 553730
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 553731
    :cond_0
    return-object v0

    .line 553732
    :cond_1
    iget-object v0, p0, LX/3MF;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 553733
    invoke-static {p0}, LX/3MF;->a(LX/3MF;)V

    .line 553734
    :cond_2
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 553735
    new-instance v4, Ljava/util/ArrayList;

    array-length v0, v3

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 553736
    new-instance v0, Ljava/util/ArrayList;

    array-length v2, v3

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 553737
    array-length v5, v3

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_4

    aget-object v6, v3, v2

    .line 553738
    :try_start_0
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 553739
    iget-object v7, p0, LX/3MF;->b:Ljava/util/Map;

    invoke-interface {v7, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 553740
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 553741
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 553742
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 553743
    invoke-static {p0, v4}, LX/3MF;->b(LX/3MF;Ljava/util/List;)Ljava/util/List;

    .line 553744
    :cond_5
    array-length v2, v3

    :goto_2
    if-ge v1, v2, :cond_0

    aget-object v4, v3, v1

    .line 553745
    :try_start_1
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 553746
    iget-object v5, p0, LX/3MF;->b:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 553747
    iget-object v5, p0, LX/3MF;->b:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 553748
    :cond_6
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :catch_0
    goto :goto_3

    :catch_1
    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 553713
    iget-object v0, p0, LX/3MF;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Lx;

    .line 553714
    invoke-virtual {v0, p1}, LX/3Lx;->a(Ljava/lang/String;)LX/4hT;

    move-result-object v1

    .line 553715
    if-nez v1, :cond_1

    .line 553716
    :cond_0
    :goto_0
    return-object p1

    .line 553717
    :cond_1
    iget-object v2, v0, LX/3Lx;->b:LX/3Lz;

    invoke-virtual {v2, v1}, LX/3Lz;->isValidNumber(LX/4hT;)Z

    move-result v2

    move v2, v2

    .line 553718
    if-eqz v2, :cond_2

    .line 553719
    invoke-virtual {v0, v1}, LX/3Lx;->b(LX/4hT;)Ljava/lang/String;

    move-result-object v0

    .line 553720
    if-eqz v0, :cond_2

    move-object p1, v0

    .line 553721
    goto :goto_0

    .line 553722
    :cond_2
    invoke-static {p1}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 553723
    iget-object v0, p0, LX/3MF;->e:LX/3MG;

    invoke-virtual {v0, p1}, LX/3MG;->b(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 553724
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 553725
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v0

    .line 553726
    iget-object v1, v0, Lcom/facebook/user/model/UserPhoneNumber;->c:Ljava/lang/String;

    move-object v0, v1

    .line 553727
    if-eqz v0, :cond_0

    move-object p1, v0

    .line 553728
    goto :goto_0
.end method
