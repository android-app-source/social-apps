.class public LX/3DV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3DW;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/net/Uri;

.field public final c:LX/1Lw;

.field private final d:Lcom/facebook/common/callercontext/CallerContext;

.field private final e:Ljava/lang/String;

.field private final f:LX/03V;

.field private final g:LX/0WJ;

.field private final h:LX/0lC;

.field public final i:LX/1Lu;

.field private j:Lcom/facebook/http/interfaces/RequestPriority;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/15D",
            "<*>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 535163
    const-class v0, LX/3DV;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3DV;->a:Ljava/lang/String;

    .line 535164
    const-string v0, "application/vnd.apple.mpegurl"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3DV;->l:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;LX/1Lw;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/String;LX/03V;LX/0WJ;LX/0lC;LX/1Lu;)V
    .locals 1

    .prologue
    .line 535151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 535152
    iput-object p1, p0, LX/3DV;->b:Landroid/net/Uri;

    .line 535153
    iput-object p2, p0, LX/3DV;->c:LX/1Lw;

    .line 535154
    iput-object p3, p0, LX/3DV;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 535155
    iput-object p4, p0, LX/3DV;->j:Lcom/facebook/http/interfaces/RequestPriority;

    .line 535156
    iput-object p5, p0, LX/3DV;->e:Ljava/lang/String;

    .line 535157
    iput-object p6, p0, LX/3DV;->f:LX/03V;

    .line 535158
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/3DV;->k:Ljava/util/Set;

    .line 535159
    iput-object p7, p0, LX/3DV;->g:LX/0WJ;

    .line 535160
    iput-object p8, p0, LX/3DV;->h:LX/0lC;

    .line 535161
    iput-object p9, p0, LX/3DV;->i:LX/1Lu;

    .line 535162
    return-void
.end method

.method private static a(Lorg/apache/http/Header;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 535146
    invoke-interface {p0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 535147
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 535148
    if-ltz v0, :cond_0

    .line 535149
    invoke-interface {p0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 535150
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized a(LX/15D;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 535143
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3DV;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535144
    monitor-exit p0

    return-void

    .line 535145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(LX/3DV;Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 2

    .prologue
    .line 535134
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/2yp;->d(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535135
    iget-object v0, p0, LX/3DV;->g:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 535136
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 535137
    :goto_0
    if-eqz v0, :cond_0

    .line 535138
    const-string v1, "Cookie"

    invoke-interface {p1, v1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 535139
    :cond_0
    return-void

    .line 535140
    :cond_1
    iget-object v1, p0, LX/3DV;->h:LX/0lC;

    .line 535141
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v0, p0

    .line 535142
    invoke-static {v1, v0}, Lcom/facebook/auth/credentials/SessionCookie;->b(LX/0lC;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a$redex0(LX/3DV;Lorg/apache/http/HttpResponse;)V
    .locals 6

    .prologue
    .line 535165
    const-string v0, "Content-Length"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 535166
    if-eqz v0, :cond_1

    .line 535167
    :cond_0
    :goto_0
    return-void

    .line 535168
    :cond_1
    const-string v0, "Content-Type"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 535169
    if-eqz v0, :cond_0

    .line 535170
    sget-object v1, LX/3DV;->l:Ljava/util/List;

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535171
    new-instance v0, Lorg/apache/http/entity/BufferedHttpEntity;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/entity/BufferedHttpEntity;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 535172
    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 535173
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    .line 535174
    const-wide/16 v2, 0x5000

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 535175
    iget-object v2, p0, LX/3DV;->f:LX/03V;

    sget-object v3, LX/3DV;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Resource with no length is too large! ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " bytes) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LX/3DV;->b:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 535176
    :cond_2
    const-string v2, "Content-Length"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Lorg/apache/http/HttpResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private declared-synchronized b(JJLX/3Di;)LX/15D;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "LX/3Di;",
            ")",
            "LX/15D",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    const/4 v3, 0x1

    .line 535118
    monitor-enter p0

    :try_start_0
    new-instance v7, Lorg/apache/http/client/methods/HttpGet;

    iget-object v0, p0, LX/3DV;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 535119
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "bytes="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 535120
    cmp-long v1, p3, v8

    if-lez v1, :cond_0

    .line 535121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-wide/16 v4, 0x1

    sub-long v4, p3, v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 535122
    :cond_0
    const-string v1, "Range"

    invoke-virtual {v7, v1, v0}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 535123
    invoke-static {p0, v7}, LX/3DV;->a(LX/3DV;Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 535124
    invoke-virtual {v7}, Lorg/apache/http/client/methods/HttpGet;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 535125
    cmp-long v0, p1, v8

    if-nez v0, :cond_1

    .line 535126
    :goto_0
    cmp-long v0, p3, v8

    if-lez v0, :cond_2

    sub-long v4, p3, p1

    .line 535127
    :goto_1
    new-instance v1, LX/3Dk;

    move-object v2, p0

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, LX/3Dk;-><init>(LX/3DV;ZJLX/3Di;)V

    .line 535128
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/15E;->a(Lorg/apache/http/client/methods/HttpUriRequest;)LX/15E;

    move-result-object v0

    iget-object v2, p0, LX/3DV;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/15E;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/15E;

    move-result-object v0

    iget-object v2, p0, LX/3DV;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/15E;->a(Ljava/lang/String;)LX/15E;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/15E;->a(Z)LX/15E;

    move-result-object v0

    sget-object v2, LX/14P;->RETRY_SAFE:LX/14P;

    invoke-virtual {v0, v2}, LX/15E;->a(LX/14P;)LX/15E;

    move-result-object v0

    iget-object v2, p0, LX/3DV;->j:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2}, LX/15E;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/15E;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/15E;->a(Lorg/apache/http/client/ResponseHandler;)LX/15E;

    move-result-object v0

    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 535129
    invoke-virtual {v1, v0}, LX/3Dk;->a(LX/15D;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535130
    monitor-exit p0

    return-object v0

    .line 535131
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 535132
    :cond_2
    const-wide/16 v4, -0x1

    goto :goto_1

    .line 535133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 535114
    const-string v0, "Content-Length"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 535115
    if-eqz v0, :cond_0

    .line 535116
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 535117
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized b(LX/3DV;LX/15D;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 535111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3DV;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535112
    monitor-exit p0

    return-void

    .line 535113
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c(Lorg/apache/http/HttpResponse;)LX/3Dd;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 535084
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    .line 535085
    sparse-switch v4, :sswitch_data_0

    .line 535086
    new-instance v0, LX/7P2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad status code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4}, LX/7P2;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 535087
    :sswitch_0
    invoke-static {p0}, LX/3DV;->b(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    .line 535088
    :cond_0
    :goto_0
    const-wide/16 v2, -0x1

    .line 535089
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 535090
    :goto_1
    cmp-long v5, v2, v6

    if-gez v5, :cond_2

    .line 535091
    new-instance v1, LX/7P2;

    const-string v2, "Invalid length value: %s (status %d)"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/7P2;-><init>(Ljava/lang/String;)V

    throw v1

    .line 535092
    :sswitch_1
    const-string v0, "Content-Range"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 535093
    if-eqz v0, :cond_1

    .line 535094
    invoke-static {v0}, LX/3DV;->a(Lorg/apache/http/Header;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 535095
    :cond_1
    new-instance v0, LX/7P2;

    const-string v1, "No Content-Range header"

    invoke-direct {v0, v1}, LX/7P2;-><init>(Ljava/lang/String;)V

    throw v0

    .line 535096
    :sswitch_2
    const-string v0, "Content-Range"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 535097
    if-eqz v0, :cond_6

    .line 535098
    invoke-static {v0}, LX/3DV;->a(Lorg/apache/http/Header;)Ljava/lang/String;

    move-result-object v0

    .line 535099
    :goto_2
    if-nez v0, :cond_0

    .line 535100
    invoke-static {p0}, LX/3DV;->b(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 535101
    :cond_2
    const-string v0, "Content-Type"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 535102
    if-nez v0, :cond_3

    .line 535103
    cmp-long v4, v2, v6

    if-lez v4, :cond_3

    .line 535104
    new-instance v0, LX/7P2;

    const-string v1, "No Content-Type header"

    invoke-direct {v0, v1}, LX/7P2;-><init>(Ljava/lang/String;)V

    throw v0

    .line 535105
    :cond_3
    new-instance v4, LX/3Dd;

    if-eqz v0, :cond_5

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-direct {v4, v2, v3, v0}, LX/3Dd;-><init>(JLjava/lang/String;)V

    .line 535106
    const-string v0, "Cache-Control"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 535107
    if-eqz v0, :cond_4

    .line 535108
    const-string v1, "Cache-Control"

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, LX/3Dd;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 535109
    :cond_4
    return-object v4

    :cond_5
    move-object v0, v1

    .line 535110
    goto :goto_3

    :catch_0
    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xce -> :sswitch_1
        0x1a0 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public final a(JJLX/3Di;)V
    .locals 3

    .prologue
    .line 535073
    invoke-direct/range {p0 .. p5}, LX/3DV;->b(JJLX/3Di;)LX/15D;

    move-result-object v0

    .line 535074
    invoke-direct {p0, v0}, LX/3DV;->a(LX/15D;)V

    .line 535075
    iget-object v1, p0, LX/3DV;->c:LX/1Lw;

    .line 535076
    iget-object v2, v1, LX/1Lw;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v2, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v2

    .line 535077
    iget-object v1, v2, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v2, v1

    .line 535078
    move-object v1, v2

    .line 535079
    if-nez v1, :cond_0

    .line 535080
    sget-object v1, LX/3DV;->a:Ljava/lang/String;

    const-string v2, "No future returned from request procesor ?!"

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 535081
    invoke-static {p0, v0}, LX/3DV;->b(LX/3DV;LX/15D;)V

    .line 535082
    :goto_0
    return-void

    .line 535083
    :cond_0
    new-instance v2, LX/3GB;

    invoke-direct {v2, p0, v0, p5}, LX/3GB;-><init>(LX/3DV;LX/15D;LX/3Di;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 4

    .prologue
    .line 535065
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/3DV;->j:Lcom/facebook/http/interfaces/RequestPriority;

    .line 535066
    iget-object v0, p0, LX/3DV;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 535067
    iget-object v0, p0, LX/3DV;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15D;

    .line 535068
    iget-object v2, p0, LX/3DV;->c:LX/1Lw;

    .line 535069
    iget-object v3, v2, LX/1Lw;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v3, v0, p1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535070
    goto :goto_0

    .line 535071
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 535072
    :cond_0
    monitor-exit p0

    return-void
.end method
