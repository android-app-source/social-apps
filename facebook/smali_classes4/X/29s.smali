.class public LX/29s;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 376757
    const-class v0, LX/29s;

    sput-object v0, LX/29s;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 376753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376754
    iput-object p1, p0, LX/29s;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 376755
    iput-object p2, p0, LX/29s;->c:LX/0lC;

    .line 376756
    return-void
.end method

.method public static a(LX/29s;LX/0Tn;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Tn;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 376735
    iget-object v1, p0, LX/29s;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, p1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 376736
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 376737
    :goto_0
    return-object v0

    .line 376738
    :cond_0
    :try_start_0
    iget-object v2, p0, LX/29s;->c:LX/0lC;

    invoke-virtual {v2, v1, p2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 376739
    :catch_0
    move-exception v1

    .line 376740
    sget-object v2, LX/29s;->a:Ljava/lang/Class;

    const-string v3, "Error reading %s from shared prefs"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(LX/29s;LX/0Tn;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 376746
    const-string v0, ""

    .line 376747
    if-eqz p2, :cond_0

    .line 376748
    :try_start_0
    iget-object v1, p0, LX/29s;->c:LX/0lC;

    invoke-virtual {v1, p2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 376749
    :cond_0
    :goto_0
    iget-object v1, p0, LX/29s;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, p1, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 376750
    return-void

    .line 376751
    :catch_0
    move-exception v1

    .line 376752
    sget-object v2, LX/29s;->a:Ljava/lang/Class;

    const-string v3, "Error writing %s to shared prefs"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final b()Lcom/facebook/appirater/api/AppRaterReport;
    .locals 2

    .prologue
    .line 376743
    sget-object v0, LX/BZn;->c:LX/0Tn;

    const-class v1, Lcom/facebook/appirater/api/AppRaterReport;

    invoke-static {p0, v0, v1}, LX/29s;->a(LX/29s;LX/0Tn;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appirater/api/AppRaterReport;

    .line 376744
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/appirater/api/AppRaterReport;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 376745
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 376742
    iget-object v0, p0, LX/29s;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/BZn;->e:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 376741
    iget-object v0, p0, LX/29s;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/BZn;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
