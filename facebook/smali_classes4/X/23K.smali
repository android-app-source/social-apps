.class public LX/23K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:LX/1De;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/components/feed/StackComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1RZ",
            "<****>;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1RZ",
            "<****>;>;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1RZ",
            "<****>;>;"
        }
    .end annotation
.end field

.field public f:LX/0jW;

.field public g:I

.field public h:LX/1PW;

.field public i:LX/1Qx;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/components/feed/StackComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 363910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 363911
    new-instance v0, LX/1De;

    invoke-direct {v0, p1}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/23K;->a:LX/1De;

    .line 363912
    iput-object p2, p0, LX/23K;->b:LX/0Ot;

    .line 363913
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/23K;->c:Ljava/util/List;

    .line 363914
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/23K;->d:Ljava/util/List;

    .line 363915
    const/4 v0, 0x0

    iput v0, p0, LX/23K;->g:I

    .line 363916
    return-void
.end method

.method private a(I)LX/1X1;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 363917
    invoke-static {p0, p1}, LX/23K;->d(LX/23K;I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 363918
    if-ltz v0, :cond_4

    iget-object v1, p0, LX/23K;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, LX/23K;->e:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    :goto_0
    move-object v4, v0

    .line 363919
    iget-object v0, p0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/1RZ;

    .line 363920
    invoke-static {p0, p1}, LX/23K;->d(LX/23K;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 363921
    iget-object v1, p0, LX/23K;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, LX/23K;->e:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    :goto_1
    move-object v6, v0

    .line 363922
    iget-object v0, p0, LX/23K;->h:LX/1PW;

    if-eqz v4, :cond_1

    .line 363923
    iget-object v1, v4, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v1, v1

    .line 363924
    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 363925
    :goto_2
    iget-object v2, v8, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v2, v2

    .line 363926
    check-cast v2, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v6, :cond_2

    .line 363927
    iget-object v3, v6, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v3, v3

    .line 363928
    check-cast v3, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    :goto_3
    if-eqz v4, :cond_3

    .line 363929
    iget-object v7, v4, LX/1RZ;->d:Ljava/lang/Object;

    move-object v4, v7

    .line 363930
    :goto_4
    if-eqz v6, :cond_0

    .line 363931
    iget-object v5, v6, LX/1RZ;->d:Ljava/lang/Object;

    move-object v5, v5

    .line 363932
    :cond_0
    iget-object v6, v8, LX/1RZ;->c:LX/1Rb;

    move-object v6, v6

    .line 363933
    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, LX/1Wz;->a(LX/1PW;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;LX/1Rb;Z)LX/1Q9;

    move-result-object v1

    .line 363934
    iget-object v0, p0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    .line 363935
    iget-object v2, v0, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v0, v2

    .line 363936
    check-cast v0, LX/1Rj;

    .line 363937
    iget-object v2, p0, LX/23K;->a:LX/1De;

    .line 363938
    iget-object v3, v8, LX/1RZ;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 363939
    iget-object v4, p0, LX/23K;->h:LX/1PW;

    invoke-interface {v0, v2, v3, v4}, LX/1Rj;->a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;

    move-result-object v0

    .line 363940
    iget-object v2, p0, LX/23K;->h:LX/1PW;

    invoke-static {v1, v2}, LX/1Wz;->a(LX/1Q9;LX/1PW;)V

    .line 363941
    return-object v0

    :cond_1
    move-object v1, v5

    .line 363942
    goto :goto_2

    :cond_2
    move-object v3, v5

    goto :goto_3

    :cond_3
    move-object v4, v5

    goto :goto_4

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/23K;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 363943
    iget-object v0, p0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363944
    :goto_0
    return-void

    .line 363945
    :cond_0
    iget-object v0, p0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 363946
    iget-object v0, p0, LX/23K;->d:Ljava/util/List;

    iget-object v2, p0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363947
    invoke-direct {p0}, LX/23K;->b()V

    goto :goto_0

    .line 363948
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v0, v1

    .line 363949
    :goto_1
    iget-object v2, p0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 363950
    invoke-direct {p0, v0}, LX/23K;->a(I)LX/1X1;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 363951
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 363952
    :cond_2
    iget-object v0, p0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    .line 363953
    iget-object v1, p0, LX/23K;->c:Ljava/util/List;

    iget-object v2, p0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1RZ;

    .line 363954
    iget-object v2, v0, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v2, v2

    .line 363955
    check-cast v2, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 363956
    iget-object v3, v0, LX/1RZ;->d:Ljava/lang/Object;

    move-object v5, v3

    .line 363957
    iget-object v3, v1, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v3, v3

    .line 363958
    check-cast v3, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 363959
    iget-object v6, v1, LX/1RZ;->d:Ljava/lang/Object;

    move-object v6, v6

    .line 363960
    new-instance v7, LX/1RZ;

    iget-object v1, p0, LX/23K;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 363961
    iget-object v8, v0, LX/1RZ;->b:LX/1RE;

    move-object v0, v8

    .line 363962
    new-instance v8, LX/242;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    iget-object v9, p0, LX/23K;->f:LX/0jW;

    new-instance v10, LX/243;

    invoke-direct {v10, v2, v5}, LX/243;-><init>(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)V

    new-instance v2, LX/243;

    invoke-direct {v2, v3, v6}, LX/243;-><init>(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)V

    invoke-direct {v8, v4, v9, v10, v2}, LX/242;-><init>(LX/0Px;LX/0jW;LX/243;LX/243;)V

    iget-object v2, p0, LX/23K;->i:LX/1Qx;

    invoke-direct {v7, v1, v0, v8, v2}, LX/1RZ;-><init>(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1RE;Ljava/lang/Object;LX/1Qx;)V

    .line 363963
    iget-object v0, p0, LX/23K;->d:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363964
    iget v0, p0, LX/23K;->g:I

    iget-object v1, p0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v0, v1

    iput v0, p0, LX/23K;->g:I

    .line 363965
    invoke-direct {p0}, LX/23K;->b()V

    goto/16 :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 363966
    iget-object v0, p0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 363967
    const/4 v0, 0x0

    iput-object v0, p0, LX/23K;->f:LX/0jW;

    .line 363968
    return-void
.end method

.method public static d(LX/23K;I)I
    .locals 2

    .prologue
    .line 363969
    iget v0, p0, LX/23K;->g:I

    iget-object v1, p0, LX/23K;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    return v0
.end method
