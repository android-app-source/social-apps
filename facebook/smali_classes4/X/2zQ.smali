.class public LX/2zQ;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2zQ;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483392
    invoke-direct {p0}, LX/398;-><init>()V

    .line 483393
    sget-object v0, LX/0ax;->cj:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->BOOKMARKS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 483394
    sget-object v0, LX/0ax;->ck:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->BOOKMARKS_SECTION_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 483395
    return-void
.end method

.method public static a(LX/0QB;)LX/2zQ;
    .locals 3

    .prologue
    .line 483396
    sget-object v0, LX/2zQ;->a:LX/2zQ;

    if-nez v0, :cond_1

    .line 483397
    const-class v1, LX/2zQ;

    monitor-enter v1

    .line 483398
    :try_start_0
    sget-object v0, LX/2zQ;->a:LX/2zQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 483399
    if-eqz v2, :cond_0

    .line 483400
    :try_start_1
    new-instance v0, LX/2zQ;

    invoke-direct {v0}, LX/2zQ;-><init>()V

    .line 483401
    move-object v0, v0

    .line 483402
    sput-object v0, LX/2zQ;->a:LX/2zQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483403
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 483404
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 483405
    :cond_1
    sget-object v0, LX/2zQ;->a:LX/2zQ;

    return-object v0

    .line 483406
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 483407
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
