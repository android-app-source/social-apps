.class public final LX/2GK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1Eh;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1Eh;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 388350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388351
    iput-object p1, p0, LX/2GK;->a:LX/0QB;

    .line 388352
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1Eh;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 388348
    new-instance v0, LX/2GK;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2GK;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 388349
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 388315
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2GK;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 388317
    packed-switch p2, :pswitch_data_0

    .line 388318
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 388319
    :pswitch_0
    invoke-static {p1}, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;->a(LX/0QB;)Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;

    move-result-object v0

    .line 388320
    :goto_0
    return-object v0

    .line 388321
    :pswitch_1
    invoke-static {p1}, LX/2Tw;->a(LX/0QB;)LX/2Tw;

    move-result-object v0

    goto :goto_0

    .line 388322
    :pswitch_2
    invoke-static {p1}, LX/2U6;->a(LX/0QB;)LX/2U6;

    move-result-object v0

    goto :goto_0

    .line 388323
    :pswitch_3
    invoke-static {p1}, Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;->a(LX/0QB;)Lcom/facebook/aldrin/task/UpdateAldrinUserStatusBackgroundTask;

    move-result-object v0

    goto :goto_0

    .line 388324
    :pswitch_4
    invoke-static {p1}, LX/2U7;->b(LX/0QB;)LX/2U7;

    move-result-object v0

    goto :goto_0

    .line 388325
    :pswitch_5
    invoke-static {p1}, Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;->a(LX/0QB;)Lcom/facebook/confirmation/task/OpenIDConnectEmailConfirmationBackgroundTask;

    move-result-object v0

    goto :goto_0

    .line 388326
    :pswitch_6
    invoke-static {p1}, LX/2UB;->b(LX/0QB;)LX/2UB;

    move-result-object v0

    goto :goto_0

    .line 388327
    :pswitch_7
    invoke-static {p1}, Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;->a(LX/0QB;)Lcom/facebook/confirmation/task/SmsConfirmationReaderBackgroundTaskExperimental;

    move-result-object v0

    goto :goto_0

    .line 388328
    :pswitch_8
    invoke-static {p1}, LX/2UK;->a(LX/0QB;)LX/2UK;

    move-result-object v0

    goto :goto_0

    .line 388329
    :pswitch_9
    new-instance v1, LX/2UM;

    invoke-static {p1}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-direct {v1, v0}, LX/2UM;-><init>(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;)V

    .line 388330
    move-object v0, v1

    .line 388331
    goto :goto_0

    .line 388332
    :pswitch_a
    invoke-static {p1}, LX/2UO;->a(LX/0QB;)LX/2UO;

    move-result-object v0

    goto :goto_0

    .line 388333
    :pswitch_b
    new-instance v2, LX/2UV;

    invoke-static {p1}, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->a(LX/0QB;)Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    invoke-static {p1}, LX/2Os;->a(LX/0QB;)LX/2Os;

    move-result-object v4

    check-cast v4, LX/2Os;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {p1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {p1}, LX/2UX;->b(LX/0QB;)LX/2UX;

    move-result-object v8

    check-cast v8, LX/2UX;

    invoke-direct/range {v2 .. v8}, LX/2UV;-><init>(Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;LX/2Os;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/2UX;)V

    .line 388334
    move-object v0, v2

    .line 388335
    goto :goto_0

    .line 388336
    :pswitch_c
    invoke-static {p1}, LX/2UY;->a(LX/0QB;)LX/2UY;

    move-result-object v0

    goto :goto_0

    .line 388337
    :pswitch_d
    invoke-static {p1}, LX/2Uh;->a(LX/0QB;)LX/2Uh;

    move-result-object v0

    goto :goto_0

    .line 388338
    :pswitch_e
    invoke-static {p1}, LX/2Ul;->a(LX/0QB;)LX/2Ul;

    move-result-object v0

    goto :goto_0

    .line 388339
    :pswitch_f
    invoke-static {p1}, LX/2Un;->a(LX/0QB;)LX/2Un;

    move-result-object v0

    goto :goto_0

    .line 388340
    :pswitch_10
    invoke-static {p1}, LX/2Ur;->a(LX/0QB;)LX/2Ur;

    move-result-object v0

    goto :goto_0

    .line 388341
    :pswitch_11
    invoke-static {p1}, LX/2Us;->a(LX/0QB;)LX/2Us;

    move-result-object v0

    goto/16 :goto_0

    .line 388342
    :pswitch_12
    invoke-static {p1}, LX/2Ut;->a(LX/0QB;)LX/2Ut;

    move-result-object v0

    goto/16 :goto_0

    .line 388343
    :pswitch_13
    invoke-static {p1}, LX/2Uu;->a(LX/0QB;)LX/2Uu;

    move-result-object v0

    goto/16 :goto_0

    .line 388344
    :pswitch_14
    invoke-static {p1}, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->a(LX/0QB;)Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    move-result-object v0

    goto/16 :goto_0

    .line 388345
    :pswitch_15
    invoke-static {p1}, LX/1Ef;->a(LX/0QB;)LX/1Ef;

    move-result-object v0

    goto/16 :goto_0

    .line 388346
    :pswitch_16
    invoke-static {p1}, LX/2V0;->b(LX/0QB;)LX/2V0;

    move-result-object v0

    goto/16 :goto_0

    .line 388347
    :pswitch_17
    invoke-static {p1}, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->a(LX/0QB;)Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 388316
    const/16 v0, 0x18

    return v0
.end method
