.class public LX/2UK;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/2UK;


# instance fields
.field private final b:Lcom/facebook/contactlogs/ContactLogsUploadRunner;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 415769
    const-class v0, LX/2UK;

    sput-object v0, LX/2UK;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/contactlogs/ContactLogsUploadRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 415770
    const-string v0, "ContactLogsUploadBackgroundTask"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 415771
    iput-object p1, p0, LX/2UK;->b:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    .line 415772
    return-void
.end method

.method public static a(LX/0QB;)LX/2UK;
    .locals 4

    .prologue
    .line 415773
    sget-object v0, LX/2UK;->c:LX/2UK;

    if-nez v0, :cond_1

    .line 415774
    const-class v1, LX/2UK;

    monitor-enter v1

    .line 415775
    :try_start_0
    sget-object v0, LX/2UK;->c:LX/2UK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 415776
    if-eqz v2, :cond_0

    .line 415777
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 415778
    new-instance p0, LX/2UK;

    invoke-static {v0}, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->a(LX/0QB;)Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    move-result-object v3

    check-cast v3, Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-direct {p0, v3}, LX/2UK;-><init>(Lcom/facebook/contactlogs/ContactLogsUploadRunner;)V

    .line 415779
    move-object v0, p0

    .line 415780
    sput-object v0, LX/2UK;->c:LX/2UK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415781
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 415782
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 415783
    :cond_1
    sget-object v0, LX/2UK;->c:LX/2UK;

    return-object v0

    .line 415784
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 415785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415786
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    sget-object v1, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 415787
    iget-object v0, p0, LX/2UK;->b:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-virtual {v0}, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->c()Z

    move-result v0

    return v0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415788
    iget-object v0, p0, LX/2UK;->b:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-virtual {v0}, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->a()LX/1ML;

    move-result-object v1

    .line 415789
    if-nez v1, :cond_0

    .line 415790
    const/4 v0, 0x0

    .line 415791
    :goto_0
    return-object v0

    .line 415792
    :cond_0
    new-instance v0, LX/3f0;

    sget-object v2, LX/2UK;->a:Ljava/lang/Class;

    invoke-direct {v0, v2}, LX/3f0;-><init>(Ljava/lang/Class;)V

    .line 415793
    invoke-static {v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method
