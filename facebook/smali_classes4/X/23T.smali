.class public LX/23T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/23U;

.field private final b:LX/23V;

.field private final c:LX/23W;

.field private final d:LX/23X;

.field private final e:LX/1V6;

.field private final f:LX/23Y;

.field private final g:LX/23Z;

.field private final h:LX/23a;

.field private final i:LX/23b;

.field private final j:LX/23c;

.field private final k:LX/23d;

.field private final l:LX/23e;

.field private final m:LX/23f;


# direct methods
.method public constructor <init>(LX/23U;LX/23V;LX/23W;LX/23X;LX/1V6;LX/23Y;LX/23Z;LX/23a;LX/23b;LX/23c;LX/23d;LX/23e;LX/23f;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 364794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 364795
    iput-object p1, p0, LX/23T;->a:LX/23U;

    .line 364796
    iput-object p2, p0, LX/23T;->b:LX/23V;

    .line 364797
    iput-object p3, p0, LX/23T;->c:LX/23W;

    .line 364798
    iput-object p4, p0, LX/23T;->d:LX/23X;

    .line 364799
    iput-object p5, p0, LX/23T;->e:LX/1V6;

    .line 364800
    iput-object p6, p0, LX/23T;->f:LX/23Y;

    .line 364801
    iput-object p7, p0, LX/23T;->g:LX/23Z;

    .line 364802
    iput-object p8, p0, LX/23T;->h:LX/23a;

    .line 364803
    iput-object p9, p0, LX/23T;->i:LX/23b;

    .line 364804
    iput-object p10, p0, LX/23T;->j:LX/23c;

    .line 364805
    iput-object p11, p0, LX/23T;->k:LX/23d;

    .line 364806
    iput-object p12, p0, LX/23T;->l:LX/23e;

    .line 364807
    iput-object p13, p0, LX/23T;->m:LX/23f;

    .line 364808
    return-void
.end method

.method public static a(LX/0QB;)LX/23T;
    .locals 1

    .prologue
    .line 364793
    invoke-static {p0}, LX/23T;->b(LX/0QB;)LX/23T;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/9gr;)LX/9g2;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 364783
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 364784
    iget-object v0, p1, LX/9gr;->b:Ljava/lang/Class;

    move-object v0, v0

    .line 364785
    const-class v1, LX/5kD;

    if-ne v0, v1, :cond_0

    .line 364786
    iget-object v0, p0, LX/23T;->a:LX/23U;

    .line 364787
    new-instance v2, LX/9g4;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v6

    check-cast v6, LX/1My;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    move-object v3, p1

    invoke-direct/range {v2 .. v7}, LX/9g4;-><init>(LX/9gr;LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/1My;LX/03V;)V

    .line 364788
    move-object v0, v2

    .line 364789
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/23T;->b:LX/23V;

    .line 364790
    new-instance v2, LX/9g3;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v6

    check-cast v6, LX/1My;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    move-object v3, p1

    invoke-direct/range {v2 .. v7}, LX/9g3;-><init>(LX/9gr;LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/1My;LX/03V;)V

    .line 364791
    move-object v0, v2

    .line 364792
    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/23T;
    .locals 14

    .prologue
    .line 364781
    new-instance v0, LX/23T;

    const-class v1, LX/23U;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/23U;

    const-class v2, LX/23V;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/23V;

    const-class v3, LX/23W;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/23W;

    const-class v4, LX/23X;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/23X;

    const-class v5, LX/1V6;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1V6;

    const-class v6, LX/23Y;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/23Y;

    const-class v7, LX/23Z;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/23Z;

    const-class v8, LX/23a;

    invoke-interface {p0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/23a;

    const-class v9, LX/23b;

    invoke-interface {p0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/23b;

    const-class v10, LX/23c;

    invoke-interface {p0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/23c;

    const-class v11, LX/23d;

    invoke-interface {p0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/23d;

    const-class v12, LX/23e;

    invoke-interface {p0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/23e;

    const-class v13, LX/23f;

    invoke-interface {p0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/23f;

    invoke-direct/range {v0 .. v13}, LX/23T;-><init>(LX/23U;LX/23V;LX/23W;LX/23X;LX/1V6;LX/23Y;LX/23Z;LX/23a;LX/23b;LX/23c;LX/23d;LX/23e;LX/23f;)V

    .line 364782
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;Lcom/facebook/common/callercontext/CallerContext;)LX/9g2;
    .locals 1
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 364724
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 364725
    invoke-virtual {p0, p1, p2}, LX/23T;->b(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;Lcom/facebook/common/callercontext/CallerContext;)LX/9gr;

    move-result-object v0

    .line 364726
    invoke-direct {p0, v0}, LX/23T;->a(LX/9gr;)LX/9g2;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;Lcom/facebook/common/callercontext/CallerContext;)LX/9gr;
    .locals 9
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 364727
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 364728
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    iget-object v1, p0, LX/23T;->c:LX/23W;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 364729
    iget-object v1, p0, LX/23T;->c:LX/23W;

    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    .line 364730
    new-instance p0, LX/9gu;

    invoke-static {v1}, LX/9h9;->a(LX/0QB;)LX/9h9;

    move-result-object v2

    check-cast v2, LX/9h9;

    invoke-direct {p0, v0, p2, v2}, LX/9gu;-><init>(Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;)V

    .line 364731
    move-object v0, p0

    .line 364732
    :goto_0
    return-object v0

    .line 364733
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    const-class v1, LX/23X;

    if-ne v0, v1, :cond_1

    .line 364734
    iget-object v1, p0, LX/23T;->d:LX/23X;

    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    .line 364735
    new-instance p0, LX/9h0;

    invoke-static {v1}, LX/9h9;->a(LX/0QB;)LX/9h9;

    move-result-object v2

    check-cast v2, LX/9h9;

    invoke-direct {p0, v0, p2, v2}, LX/9h0;-><init>(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;)V

    .line 364736
    move-object v0, p0

    .line 364737
    goto :goto_0

    .line 364738
    :cond_1
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    iget-object v1, p0, LX/23T;->e:LX/1V6;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 364739
    iget-object v1, p0, LX/23T;->e:LX/1V6;

    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-virtual {v1, v0, p2}, LX/1V6;->a(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;)LX/9h1;

    move-result-object v0

    goto :goto_0

    .line 364740
    :cond_2
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    iget-object v1, p0, LX/23T;->f:LX/23Y;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_3

    .line 364741
    iget-object v1, p0, LX/23T;->f:LX/23Y;

    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    .line 364742
    new-instance p0, LX/9h3;

    invoke-static {v1}, LX/9h9;->a(LX/0QB;)LX/9h9;

    move-result-object v2

    check-cast v2, LX/9h9;

    const/16 p1, 0x259

    invoke-static {v1, p1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-direct {p0, v0, p2, v2, p1}, LX/9h3;-><init>(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;LX/0Ot;)V

    .line 364743
    move-object v0, p0

    .line 364744
    goto :goto_0

    .line 364745
    :cond_3
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    iget-object v1, p0, LX/23T;->g:LX/23Z;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_4

    .line 364746
    iget-object v1, p0, LX/23T;->g:LX/23Z;

    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;

    .line 364747
    new-instance v3, LX/9h4;

    invoke-static {v1}, LX/9h9;->a(LX/0QB;)LX/9h9;

    move-result-object v6

    check-cast v6, LX/9h9;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v1}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v8

    check-cast v8, LX/0sX;

    move-object v4, v0

    move-object v5, p2

    invoke-direct/range {v3 .. v8}, LX/9h4;-><init>(Lcom/facebook/photos/mediafetcher/query/param/MediaTypeQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;LX/03V;LX/0sX;)V

    .line 364748
    move-object v0, v3

    .line 364749
    goto :goto_0

    .line 364750
    :cond_4
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    iget-object v1, p0, LX/23T;->h:LX/23a;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_5

    .line 364751
    iget-object v1, p0, LX/23T;->h:LX/23a;

    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    .line 364752
    new-instance v3, LX/9gw;

    invoke-static {v1}, LX/9h9;->a(LX/0QB;)LX/9h9;

    move-result-object v2

    check-cast v2, LX/9h9;

    invoke-direct {v3, v0, p2, v2}, LX/9gw;-><init>(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;)V

    .line 364753
    move-object v0, v3

    .line 364754
    goto/16 :goto_0

    .line 364755
    :cond_5
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    iget-object v1, p0, LX/23T;->i:LX/23b;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_6

    .line 364756
    iget-object v1, p0, LX/23T;->i:LX/23b;

    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    .line 364757
    new-instance v3, LX/9gx;

    invoke-static {v1}, LX/9h9;->a(LX/0QB;)LX/9h9;

    move-result-object v2

    check-cast v2, LX/9h9;

    invoke-direct {v3, v0, p2, v2}, LX/9gx;-><init>(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;)V

    .line 364758
    move-object v0, v3

    .line 364759
    goto/16 :goto_0

    .line 364760
    :cond_6
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    iget-object v1, p0, LX/23T;->j:LX/23c;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_7

    .line 364761
    iget-object v1, p0, LX/23T;->j:LX/23c;

    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    .line 364762
    new-instance v3, LX/9gy;

    invoke-static {v1}, LX/9h9;->a(LX/0QB;)LX/9h9;

    move-result-object v2

    check-cast v2, LX/9h9;

    invoke-direct {v3, v0, p2, v2}, LX/9gy;-><init>(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;)V

    .line 364763
    move-object v0, v3

    .line 364764
    goto/16 :goto_0

    .line 364765
    :cond_7
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    iget-object v1, p0, LX/23T;->k:LX/23d;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_8

    .line 364766
    iget-object v1, p0, LX/23T;->k:LX/23d;

    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    .line 364767
    new-instance v4, LX/9gz;

    invoke-static {v1}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v2

    check-cast v2, LX/0se;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {v4, v0, p2, v2, v3}, LX/9gz;-><init>(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/0se;LX/03V;)V

    .line 364768
    move-object v0, v4

    .line 364769
    goto/16 :goto_0

    .line 364770
    :cond_8
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    iget-object v1, p0, LX/23T;->l:LX/23e;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_9

    .line 364771
    iget-object v1, p0, LX/23T;->l:LX/23e;

    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;

    .line 364772
    new-instance v4, LX/9gv;

    invoke-static {v1}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v2

    check-cast v2, LX/0tG;

    invoke-static {v1}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v3

    check-cast v3, LX/0tI;

    invoke-direct {v4, v0, p2, v2, v3}, LX/9gv;-><init>(Lcom/facebook/photos/mediafetcher/query/param/CategoryQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/0tG;LX/0tI;)V

    .line 364773
    move-object v0, v4

    .line 364774
    goto/16 :goto_0

    .line 364775
    :cond_9
    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a:Ljava/lang/Class;

    iget-object v1, p0, LX/23T;->m:LX/23f;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_a

    .line 364776
    iget-object v1, p0, LX/23T;->m:LX/23f;

    iget-object v0, p1, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->b:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    check-cast v0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    .line 364777
    new-instance v4, LX/9gs;

    invoke-static {v1}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v2

    check-cast v2, LX/0tG;

    invoke-static {v1}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v3

    check-cast v3, LX/0tI;

    invoke-direct {v4, v0, p2, v2, v3}, LX/9gs;-><init>(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/0tG;LX/0tI;)V

    .line 364778
    move-object v0, v4

    .line 364779
    goto/16 :goto_0

    .line 364780
    :cond_a
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not create query for rule: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
