.class public LX/3Kr;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/0Tn;

.field public static final B:LX/0Tn;

.field public static final C:LX/0Tn;

.field public static final D:LX/0Tn;

.field public static final E:LX/0Tn;

.field public static final F:LX/0Tn;

.field public static final G:LX/0Tn;

.field public static final H:LX/0Tn;

.field public static final I:LX/0Tn;

.field public static final J:LX/0Tn;

.field public static final K:LX/0Tn;

.field public static final L:LX/0Tn;

.field public static final M:LX/0Tn;

.field public static final N:LX/0Tn;

.field public static final O:LX/0Tn;

.field public static final P:LX/0Tn;

.field public static final Q:LX/0Tn;

.field public static final R:LX/0Tn;

.field public static final S:LX/0Tn;

.field public static final T:LX/0Tn;

.field public static final U:LX/0Tn;

.field public static final V:LX/0Tn;

.field public static final W:LX/0Tn;

.field public static final X:LX/0Tn;

.field public static final Y:LX/0Tn;

.field public static final Z:LX/0Tn;

.field public static final a:LX/0Tn;

.field public static final aa:LX/0Tn;

.field public static final ab:LX/0Tn;

.field public static final ac:LX/0Tn;

.field public static final ad:LX/0Tn;

.field public static final ae:LX/0Tn;

.field public static final af:LX/0Tn;

.field public static final ag:LX/0Tn;

.field public static final ah:LX/0Tn;

.field private static final ai:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;

.field public static final t:LX/0Tn;

.field public static final u:LX/0Tn;

.field public static final v:LX/0Tn;

.field public static final w:LX/0Tn;

.field public static final x:LX/0Tn;

.field public static final y:LX/0Tn;

.field public static final z:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 549258
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "sms_integration/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 549259
    sput-object v0, LX/3Kr;->ai:LX/0Tn;

    const-string v1, "defaultapp/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->a:LX/0Tn;

    .line 549260
    sget-object v0, LX/3Kr;->ai:LX/0Tn;

    const-string v1, "user/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->b:LX/0Tn;

    .line 549261
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_in_readonly_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->c:LX/0Tn;

    .line 549262
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_readonly_set_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->d:LX/0Tn;

    .line 549263
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_fullmode_set_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->e:LX/0Tn;

    .line 549264
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_seen_survey_page"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->f:LX/0Tn;

    .line 549265
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_readonly_chathead_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->g:LX/0Tn;

    .line 549266
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_readonly_last_chathead_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->h:LX/0Tn;

    .line 549267
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_readonly_chathead_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->i:LX/0Tn;

    .line 549268
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_in_anonymous_row_mode"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->j:LX/0Tn;

    .line 549269
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_anonymous_promo_set_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->k:LX/0Tn;

    .line 549270
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_last_anonymous_chathead_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->l:LX/0Tn;

    .line 549271
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_anonymous_chathead_impression_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->m:LX/0Tn;

    .line 549272
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_anonymous_chathead_impression_count_today"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->n:LX/0Tn;

    .line 549273
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_anonymous_chathead_rate_limit"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->o:LX/0Tn;

    .line 549274
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_newest_received_thread_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->p:LX/0Tn;

    .line 549275
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_anonymous_promo_ignore_delay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->q:LX/0Tn;

    .line 549276
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_anonymous_promo_row_seen_today"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->r:LX/0Tn;

    .line 549277
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_takeover_thread_view_tutorial_impression_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->s:LX/0Tn;

    .line 549278
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_takeover_thread_view_tutorial_last_impression_ts"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->t:LX/0Tn;

    .line 549279
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_takeover_previous_default_app"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->u:LX/0Tn;

    .line 549280
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_promo_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->v:LX/0Tn;

    .line 549281
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_nux_complete"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->w:LX/0Tn;

    .line 549282
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_nux_v2_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->x:LX/0Tn;

    .line 549283
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_initial_enroll_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->y:LX/0Tn;

    .line 549284
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_nux_postponed_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->z:LX/0Tn;

    .line 549285
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_nux_v2_seen_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->A:LX/0Tn;

    .line 549286
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_nux_blocks"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->B:LX/0Tn;

    .line 549287
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_interstitial_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->C:LX/0Tn;

    .line 549288
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_device_status_reported"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->D:LX/0Tn;

    .line 549289
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_is_enabled_for_tracking"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->E:LX/0Tn;

    .line 549290
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_is_default_app_for_tracking"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->F:LX/0Tn;

    .line 549291
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "messenger_been_sms_default_app"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->G:LX/0Tn;

    .line 549292
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_internal_force_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->H:LX/0Tn;

    .line 549293
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_internal_test_campaign"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->I:LX/0Tn;

    .line 549294
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_internal_no_readonly_notification"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->J:LX/0Tn;

    .line 549295
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_info_qp_disabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->K:LX/0Tn;

    .line 549296
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_full_qp_disabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->L:LX/0Tn;

    .line 549297
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_returning_readonly_qp_disabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->M:LX/0Tn;

    .line 549298
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_is_enabled_for_settings_ui"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->N:LX/0Tn;

    .line 549299
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_launcher_for_settings_ui"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->O:LX/0Tn;

    .line 549300
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_inbox_filter_last_selection"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->P:LX/0Tn;

    .line 549301
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_inbox_filter_show_initially_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->Q:LX/0Tn;

    .line 549302
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_inbox_filter_search_tag_tooltip"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->R:LX/0Tn;

    .line 549303
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_inbox_filter_last_selected_tab_index"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->S:LX/0Tn;

    .line 549304
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_inbox_filter_sms_tag_selected"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->T:LX/0Tn;

    .line 549305
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_inbox_filter_last_seen_sms_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->U:LX/0Tn;

    .line 549306
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_inbox_filter_last_seen_all_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->V:LX/0Tn;

    .line 549307
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_spam_number_next_fetch_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->W:LX/0Tn;

    .line 549308
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_local_spam_and_ranking_next_update_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->X:LX/0Tn;

    .line 549309
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_user_matching_initial_run"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->Y:LX/0Tn;

    .line 549310
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_custom_theme_color"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->Z:LX/0Tn;

    .line 549311
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_business_number_next_fetch_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->aa:LX/0Tn;

    .line 549312
    sget-object v0, LX/3Kr;->a:LX/0Tn;

    const-string v1, "sms_show_sms_white_list"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->ab:LX/0Tn;

    .line 549313
    sget-object v0, LX/3Kr;->ai:LX/0Tn;

    const-string v1, "sms_auto_retrieve"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->ac:LX/0Tn;

    .line 549314
    sget-object v0, LX/3Kr;->ai:LX/0Tn;

    const-string v1, "sms_auto_retrieve_roaming"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->ad:LX/0Tn;

    .line 549315
    sget-object v0, LX/3Kr;->ai:LX/0Tn;

    const-string v1, "sms_chat_heads"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->ae:LX/0Tn;

    .line 549316
    sget-object v0, LX/3Kr;->ai:LX/0Tn;

    const-string v1, "sms_fallback_number"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->af:LX/0Tn;

    .line 549317
    sget-object v0, LX/3Kr;->ai:LX/0Tn;

    const-string v1, "sms_debug_msg_errors/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->ag:LX/0Tn;

    .line 549318
    sget-object v0, LX/3Kr;->ai:LX/0Tn;

    const-string v1, "sms_notification_sound"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3Kr;->ah:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 549319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
