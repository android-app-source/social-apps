.class public LX/2Dk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/2Dl;


# direct methods
.method public constructor <init>(LX/0Zb;LX/2Dl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384615
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384616
    iput-object p1, p0, LX/2Dk;->a:LX/0Zb;

    .line 384617
    iput-object p2, p0, LX/2Dk;->b:LX/2Dl;

    .line 384618
    return-void
.end method


# virtual methods
.method public final a(LX/27f;I)V
    .locals 4

    .prologue
    .line 384619
    const/4 v0, 0x0

    .line 384620
    packed-switch p2, :pswitch_data_0

    .line 384621
    iget-object v1, p1, LX/27f;->groupNames:[Ljava/lang/String;

    aget-object v1, v1, p2

    .line 384622
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "offline_experiment_exposure"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "offlineexperiment"

    .line 384623
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 384624
    move-object v2, v2

    .line 384625
    const-string v3, "appinstallid"

    iget-object p2, p0, LX/2Dk;->b:LX/2Dl;

    invoke-virtual {p2}, LX/2Dl;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "exp_name"

    iget-object p2, p1, LX/27f;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "exp_group"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 384626
    iget-object v3, p0, LX/2Dk;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 384627
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
