.class public final LX/32l;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/32j;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/32k;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 490963
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 490964
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "draweeController"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "actualImageFocusPoint"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/32l;->b:[Ljava/lang/String;

    .line 490965
    iput v3, p0, LX/32l;->c:I

    .line 490966
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/32l;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/32l;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/32l;LX/1De;IILX/32k;)V
    .locals 1

    .prologue
    .line 490973
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 490974
    iput-object p4, p0, LX/32l;->a:LX/32k;

    .line 490975
    iget-object v0, p0, LX/32l;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 490976
    return-void
.end method


# virtual methods
.method public final a(LX/1aZ;)LX/32l;
    .locals 2

    .prologue
    .line 490967
    iget-object v0, p0, LX/32l;->a:LX/32k;

    iput-object p1, v0, LX/32k;->a:LX/1aZ;

    .line 490968
    iget-object v0, p0, LX/32l;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 490969
    return-object p0
.end method

.method public final a(Landroid/graphics/PointF;)LX/32l;
    .locals 2

    .prologue
    .line 490970
    iget-object v0, p0, LX/32l;->a:LX/32k;

    iput-object p1, v0, LX/32k;->b:Landroid/graphics/PointF;

    .line 490971
    iget-object v0, p0, LX/32l;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 490972
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 490949
    invoke-super {p0}, LX/1X5;->a()V

    .line 490950
    const/4 v0, 0x0

    iput-object v0, p0, LX/32l;->a:LX/32k;

    .line 490951
    sget-object v0, LX/32j;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 490952
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/32j;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 490953
    iget-object v1, p0, LX/32l;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/32l;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/32l;->c:I

    if-ge v1, v2, :cond_2

    .line 490954
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 490955
    :goto_0
    iget v2, p0, LX/32l;->c:I

    if-ge v0, v2, :cond_1

    .line 490956
    iget-object v2, p0, LX/32l;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 490957
    iget-object v2, p0, LX/32l;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 490958
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 490959
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 490960
    :cond_2
    iget-object v0, p0, LX/32l;->a:LX/32k;

    .line 490961
    invoke-virtual {p0}, LX/32l;->a()V

    .line 490962
    return-object v0
.end method
