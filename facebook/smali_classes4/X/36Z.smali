.class public LX/36Z;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3hY;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/36Z",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3hY;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 499296
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 499297
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/36Z;->b:LX/0Zi;

    .line 499298
    iput-object p1, p0, LX/36Z;->a:LX/0Ot;

    .line 499299
    return-void
.end method

.method public static a(LX/0QB;)LX/36Z;
    .locals 4

    .prologue
    .line 499313
    const-class v1, LX/36Z;

    monitor-enter v1

    .line 499314
    :try_start_0
    sget-object v0, LX/36Z;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 499315
    sput-object v2, LX/36Z;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 499316
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499317
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 499318
    new-instance v3, LX/36Z;

    const/16 p0, 0x9b5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/36Z;-><init>(LX/0Ot;)V

    .line 499319
    move-object v0, v3

    .line 499320
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 499321
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/36Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499322
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 499323
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 499302
    check-cast p2, LX/CAV;

    .line 499303
    iget-object v0, p0, LX/36Z;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hY;

    iget-object v1, p2, LX/CAV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/CAV;->b:LX/1Pp;

    .line 499304
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 499305
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 499306
    invoke-static {v3}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 499307
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 499308
    if-eqz v3, :cond_0

    .line 499309
    const-string v4, "extra_action_on_fragment_create"

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 499310
    :cond_0
    iget-object p2, v0, LX/3hY;->a:LX/0Or;

    iget-object v4, v0, LX/3hY;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/17Y;

    iget-object v5, v0, LX/3hY;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    const/4 v1, 0x0

    invoke-static {p0, p2, v4, v5, v1}, LX/CAe;->a(Landroid/os/Bundle;LX/0Or;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/CAZ;)Landroid/view/View$OnClickListener;

    move-result-object v4

    move-object v4, v4

    .line 499311
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    iget-object p0, v0, LX/3hY;->d:LX/1Vm;

    invoke-virtual {p0, p1}, LX/1Vm;->c(LX/1De;)LX/C2N;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/C2N;->a(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/C2N;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/C2N;->a(LX/1Pp;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 499312
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 499300
    invoke-static {}, LX/1dS;->b()V

    .line 499301
    const/4 v0, 0x0

    return-object v0
.end method
