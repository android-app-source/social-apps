.class public LX/3Pm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/3Pn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3Pn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562213
    iput-object p1, p0, LX/3Pm;->a:LX/3Pn;

    .line 562214
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/4VT;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562199
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;

    const/4 v0, 0x0

    .line 562200
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 562201
    :cond_0
    :goto_0
    return-object v0

    .line 562202
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    move-result-object v1

    const/4 v3, 0x0

    .line 562203
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel$AttachmentsModel;

    invoke-virtual {v2}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel$AttachmentsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    move-result-object v2

    if-nez v2, :cond_3

    .line 562204
    :cond_2
    const/4 v2, 0x0

    .line 562205
    :goto_1
    move-object v1, v2

    .line 562206
    if-eqz v1, :cond_0

    .line 562207
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;

    move-result-object v2

    invoke-static {v2}, LX/5Hb;->a(Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel$ChangedPageModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/5JC;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    move-result-object v2

    .line 562208
    if-eqz v2, :cond_0

    .line 562209
    new-instance v0, LX/6A9;

    invoke-direct {v0, v1, v2}, LX/6A9;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V

    .line 562210
    move-object v0, v0

    .line 562211
    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel$AttachmentsModel;

    invoke-virtual {v2}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$StoryFieldsForAddPlaceRecommendationModel$AttachmentsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListAttachmentTargetModel;->m()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 562198
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562197
    const/4 v0, 0x0

    return-object v0
.end method
