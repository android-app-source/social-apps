.class public abstract LX/2ne;
.super Landroid/database/CursorWrapper;
.source ""


# instance fields
.field private final a:Ljava/lang/Exception;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 464943
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 464944
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 464945
    if-eqz v0, :cond_0

    .line 464946
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Allocated here"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/2ne;->a:Ljava/lang/Exception;

    .line 464947
    :goto_0
    return-void

    .line 464948
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/2ne;->a:Ljava/lang/Exception;

    goto :goto_0
.end method


# virtual methods
.method public final finalize()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 464949
    invoke-virtual {p0}, LX/2ne;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 464950
    :goto_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 464951
    if-eqz v0, :cond_0

    .line 464952
    const-string v0, "FinalizerDetectingCursor"

    iget-object v2, p0, LX/2ne;->a:Ljava/lang/Exception;

    const-string v3, "Failed to call close() on cursor"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 464953
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 464954
    goto :goto_0
.end method
