.class public LX/3Pa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3Pb;


# direct methods
.method public constructor <init>(LX/3Pb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562104
    iput-object p1, p0, LX/3Pa;->a:LX/3Pb;

    .line 562105
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/4VT;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562106
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;

    .line 562107
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;->k()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;

    move-result-object v0

    invoke-static {v0}, LX/6AF;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$StoryFieldsForPlaceMutationModel;)Ljava/lang/String;

    move-result-object v0

    .line 562108
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 562109
    :cond_0
    const/4 v0, 0x0

    .line 562110
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, LX/3Pa;->a:LX/3Pb;

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;->a()LX/0Px;

    move-result-object v3

    .line 562111
    new-instance p1, LX/6AD;

    invoke-static {v1}, LX/6Ou;->b(LX/0QB;)LX/6Ou;

    move-result-object p0

    check-cast p0, LX/6Ou;

    invoke-direct {p1, p0, v0, v2, v3}, LX/6AD;-><init>(LX/6Ou;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    .line 562112
    move-object v0, p1

    .line 562113
    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 562114
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$DeletePlaceRecommendationFromCommentMutationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562115
    const/4 v0, 0x0

    return-object v0
.end method
