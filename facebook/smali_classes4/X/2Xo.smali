.class public LX/2Xo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Sh;

.field private final b:LX/1mM;

.field private final c:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final d:LX/0WJ;

.field private final e:LX/0S2;

.field private final f:LX/2Xq;


# direct methods
.method public constructor <init>(LX/0Sh;LX/1mM;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0WJ;LX/0S2;LX/2Xq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 420274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420275
    iput-object p1, p0, LX/2Xo;->a:LX/0Sh;

    .line 420276
    iput-object p2, p0, LX/2Xo;->b:LX/1mM;

    .line 420277
    iput-object p3, p0, LX/2Xo;->c:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 420278
    iput-object p4, p0, LX/2Xo;->d:LX/0WJ;

    .line 420279
    iput-object p5, p0, LX/2Xo;->e:LX/0S2;

    .line 420280
    iput-object p6, p0, LX/2Xo;->f:LX/2Xq;

    .line 420281
    return-void
.end method

.method public static a(LX/0QB;)LX/2Xo;
    .locals 8

    .prologue
    .line 420303
    new-instance v1, LX/2Xo;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/1mM;->getInstance__com_facebook_fbservice_service_BlueServiceQueueManager__INJECTED_BY_TemplateInjector(LX/0QB;)LX/1mM;

    move-result-object v3

    check-cast v3, LX/1mM;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v4

    check-cast v4, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v5

    check-cast v5, LX/0WJ;

    const-class v6, LX/0S2;

    invoke-interface {p0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0S2;

    invoke-static {p0}, LX/2Xp;->a(LX/0QB;)LX/2Xp;

    move-result-object v7

    check-cast v7, LX/2Xq;

    invoke-direct/range {v1 .. v7}, LX/2Xo;-><init>(LX/0Sh;LX/1mM;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0WJ;LX/0S2;LX/2Xq;)V

    .line 420304
    move-object v0, v1

    .line 420305
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 420282
    iget-object v2, p0, LX/2Xo;->a:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 420283
    iget-object v2, p0, LX/2Xo;->b:LX/1mM;

    invoke-virtual {v2}, LX/1mM;->enterLameDuckMode()V

    .line 420284
    :try_start_0
    iget-object v2, p0, LX/2Xo;->c:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 420285
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/facebook/http/common/FbHttpRequestProcessor;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 420286
    :try_start_1
    iget-object v2, p0, LX/2Xo;->c:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 420287
    invoke-static {v2}, Lcom/facebook/http/common/FbHttpRequestProcessor;->j(Lcom/facebook/http/common/FbHttpRequestProcessor;)LX/1hk;

    move-result-object v3

    invoke-interface {v3}, LX/1hk;->a()V

    .line 420288
    iget-object v2, p0, LX/2Xo;->b:LX/1mM;

    invoke-virtual {v2}, LX/1mM;->blockUntilAllQueuesDrained()V

    .line 420289
    iget-object v2, p0, LX/2Xo;->f:LX/2Xq;

    invoke-interface {v2}, LX/2Xq;->a()V

    .line 420290
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 420291
    iget-object v2, p0, LX/2Xo;->d:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->b()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 420292
    iget-object v2, p0, LX/2Xo;->d:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    if-nez v2, :cond_1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 420293
    iget-object v0, p0, LX/2Xo;->e:LX/0S2;

    invoke-virtual {v0}, LX/0S2;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420294
    :try_start_2
    iget-object v0, p0, LX/2Xo;->c:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 420295
    iget-object v0, p0, LX/2Xo;->b:LX/1mM;

    invoke-virtual {v0}, LX/1mM;->exitLameDuckMode()V

    .line 420296
    iget-object v0, p0, LX/2Xo;->f:LX/2Xq;

    invoke-interface {v0}, LX/2Xq;->b()V

    .line 420297
    return-void

    :cond_0
    move v2, v1

    .line 420298
    goto :goto_0

    :cond_1
    move v0, v1

    .line 420299
    goto :goto_1

    .line 420300
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, LX/2Xo;->c:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 420301
    :catchall_1
    move-exception v0

    iget-object v1, p0, LX/2Xo;->b:LX/1mM;

    invoke-virtual {v1}, LX/1mM;->exitLameDuckMode()V

    .line 420302
    iget-object v1, p0, LX/2Xo;->f:LX/2Xq;

    invoke-interface {v1}, LX/2Xq;->b()V

    throw v0
.end method
