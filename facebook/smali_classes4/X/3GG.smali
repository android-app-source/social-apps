.class public LX/3GG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/EDU;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 540849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540850
    return-void
.end method

.method public static a(LX/0QB;)LX/3GG;
    .locals 1

    .prologue
    .line 540851
    new-instance v0, LX/3GG;

    invoke-direct {v0}, LX/3GG;-><init>()V

    .line 540852
    move-object v0, v0

    .line 540853
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 540854
    check-cast p1, LX/EDU;

    .line 540855
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 540856
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "access_token"

    .line 540857
    iget-object v3, p1, LX/EDU;->a:Ljava/lang/String;

    move-object v3, v3

    .line 540858
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 540859
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "user_name"

    .line 540860
    iget-object v3, p1, LX/EDU;->b:Ljava/lang/String;

    move-object v3, v3

    .line 540861
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 540862
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "pwd"

    .line 540863
    iget-object v3, p1, LX/EDU;->c:Ljava/lang/String;

    move-object v3, v3

    .line 540864
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 540865
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    const-string v2, "webrtcTurnAllocationFetch"

    .line 540866
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 540867
    move-object v1, v1

    .line 540868
    const-string v2, "GET"

    .line 540869
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 540870
    move-object v1, v1

    .line 540871
    const-string v2, "method/voicechat.discoverturn"

    .line 540872
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 540873
    move-object v1, v1

    .line 540874
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 540875
    move-object v0, v1

    .line 540876
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 540877
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 540878
    move-object v0, v0

    .line 540879
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 540880
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 540881
    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
