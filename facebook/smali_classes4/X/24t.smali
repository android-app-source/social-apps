.class public final LX/24t;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/24r;


# direct methods
.method public constructor <init>(LX/24r;)V
    .locals 0

    .prologue
    .line 367824
    iput-object p1, p0, LX/24t;->a:LX/24r;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 367825
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367826
    iget-object v0, p0, LX/24t;->a:LX/24r;

    iget-object v0, v0, LX/24r;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v1

    const v2, 0x254522f8

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 367827
    :cond_0
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 367828
    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v0

    .line 367829
    if-nez v0, :cond_0

    .line 367830
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "DataSourceFailed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 367831
    :cond_0
    iget-object v1, p0, LX/24t;->a:LX/24r;

    iget-object v1, v1, LX/24r;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 367832
    return-void
.end method
