.class public final LX/32k;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/32j;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1aZ;

.field public b:Landroid/graphics/PointF;

.field public final synthetic c:LX/32j;


# direct methods
.method public constructor <init>(LX/32j;)V
    .locals 1

    .prologue
    .line 490930
    iput-object p1, p0, LX/32k;->c:LX/32j;

    .line 490931
    move-object v0, p1

    .line 490932
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 490933
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490934
    const-string v0, "PhotoAttachmentFrescoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 490935
    if-ne p0, p1, :cond_1

    .line 490936
    :cond_0
    :goto_0
    return v0

    .line 490937
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 490938
    goto :goto_0

    .line 490939
    :cond_3
    check-cast p1, LX/32k;

    .line 490940
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 490941
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 490942
    if-eq v2, v3, :cond_0

    .line 490943
    iget-object v2, p0, LX/32k;->a:LX/1aZ;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/32k;->a:LX/1aZ;

    iget-object v3, p1, LX/32k;->a:LX/1aZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 490944
    goto :goto_0

    .line 490945
    :cond_5
    iget-object v2, p1, LX/32k;->a:LX/1aZ;

    if-nez v2, :cond_4

    .line 490946
    :cond_6
    iget-object v2, p0, LX/32k;->b:Landroid/graphics/PointF;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/32k;->b:Landroid/graphics/PointF;

    iget-object v3, p1, LX/32k;->b:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 490947
    goto :goto_0

    .line 490948
    :cond_7
    iget-object v2, p1, LX/32k;->b:Landroid/graphics/PointF;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
