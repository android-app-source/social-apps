.class public LX/2G1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile v:LX/2G1;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ps;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2G2;

.field public final c:LX/0pj;

.field public final d:LX/1Gm;

.field public final e:LX/1ia;

.field public final f:LX/2G3;

.field public final g:LX/2G5;

.field private final h:Landroid/content/Context;

.field public final i:LX/0TL;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2IR;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/1sz;

.field private final l:LX/0Uo;

.field private final m:LX/2G6;

.field private n:Ljava/lang/reflect/Field;

.field private o:Ljava/lang/reflect/Field;

.field private p:Ljava/lang/reflect/Method;

.field private q:Ljava/lang/reflect/Method;

.field private r:Ljava/lang/reflect/Method;

.field private s:Ljava/lang/reflect/Method;

.field private t:I

.field private u:I


# direct methods
.method public constructor <init>(LX/0Ot;LX/2G2;LX/0pj;LX/1Gm;LX/1ia;LX/2G3;LX/2G5;Landroid/content/Context;LX/0TL;LX/0Ot;LX/1sz;LX/0Uo;LX/2G6;)V
    .locals 1
    .param p7    # LX/2G5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0ps;",
            ">;",
            "LX/2G2;",
            "LX/0pj;",
            "LX/1Gm;",
            "LX/1ia;",
            "LX/2G3;",
            "LX/2G5;",
            "Landroid/content/Context;",
            "LX/0TL;",
            "LX/0Ot",
            "<",
            "LX/2IR;",
            ">;",
            "LX/1sz;",
            "LX/0Uo;",
            "LX/2G6;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 387633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387634
    iput v0, p0, LX/2G1;->t:I

    .line 387635
    iput v0, p0, LX/2G1;->u:I

    .line 387636
    iput-object p1, p0, LX/2G1;->a:LX/0Ot;

    .line 387637
    iput-object p2, p0, LX/2G1;->b:LX/2G2;

    .line 387638
    iput-object p3, p0, LX/2G1;->c:LX/0pj;

    .line 387639
    iput-object p4, p0, LX/2G1;->d:LX/1Gm;

    .line 387640
    iput-object p5, p0, LX/2G1;->e:LX/1ia;

    .line 387641
    iput-object p6, p0, LX/2G1;->f:LX/2G3;

    .line 387642
    iput-object p7, p0, LX/2G1;->g:LX/2G5;

    .line 387643
    iput-object p8, p0, LX/2G1;->h:Landroid/content/Context;

    .line 387644
    iput-object p9, p0, LX/2G1;->i:LX/0TL;

    .line 387645
    iput-object p10, p0, LX/2G1;->j:LX/0Ot;

    .line 387646
    iput-object p11, p0, LX/2G1;->k:LX/1sz;

    .line 387647
    iput-object p12, p0, LX/2G1;->l:LX/0Uo;

    .line 387648
    iput-object p13, p0, LX/2G1;->m:LX/2G6;

    .line 387649
    return-void
.end method

.method private a(ILX/2GO;)LX/0m9;
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/16 v8, 0x13

    const/4 v1, 0x0

    .line 387650
    new-instance v2, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v2}, Landroid/os/Debug$MemoryInfo;-><init>()V

    .line 387651
    invoke-static {v2}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    .line 387652
    const-wide/16 v4, 0x64

    invoke-virtual {p2}, LX/2GO;->c()J

    move-result-wide v6

    mul-long/2addr v4, v6

    long-to-float v0, v4

    invoke-virtual {p2}, LX/2GO;->a()J

    move-result-wide v4

    long-to-float v3, v4

    div-float/2addr v0, v3

    float-to-int v0, v0

    .line 387653
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 387654
    const-string v4, "device_total_mem"

    .line 387655
    iget-wide v9, p2, LX/2GO;->b:J

    move-wide v6, v9

    .line 387656
    invoke-virtual {v3, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 387657
    const-string v4, "mem_available"

    invoke-virtual {p2}, LX/2GO;->a()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 387658
    const-string v4, "mem_threshold"

    invoke-virtual {p2}, LX/2GO;->c()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 387659
    const-string v4, "mem_is_low"

    invoke-virtual {p2}, LX/2GO;->d()Z

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 387660
    const-string v4, "mem_pct_total"

    invoke-virtual {v3, v4, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387661
    const-string v0, "mem_class"

    invoke-virtual {v3, v0, p1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387662
    const-string v0, "total_pd"

    invoke-virtual {v2}, Landroid/os/Debug$MemoryInfo;->getTotalPrivateDirty()I

    move-result v4

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387663
    const-string v0, "total_pss"

    invoke-virtual {v2}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v4

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387664
    const-string v0, "total_sd"

    invoke-virtual {v2}, Landroid/os/Debug$MemoryInfo;->getTotalSharedDirty()I

    move-result v4

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387665
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_0

    .line 387666
    const-string v0, "total_pc"

    invoke-virtual {v2}, Landroid/os/Debug$MemoryInfo;->getTotalPrivateClean()I

    move-result v4

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387667
    const-string v0, "total_sc"

    invoke-virtual {v2}, Landroid/os/Debug$MemoryInfo;->getTotalSharedClean()I

    move-result v4

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387668
    const-string v0, "total_swappable"

    invoke-virtual {v2}, Landroid/os/Debug$MemoryInfo;->getTotalSwappablePss()I

    move-result v4

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387669
    :cond_0
    const-string v0, "dalvik_pd"

    iget v4, v2, Landroid/os/Debug$MemoryInfo;->dalvikPrivateDirty:I

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387670
    const-string v0, "dalvik_pss"

    iget v4, v2, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387671
    const-string v0, "dalvik_sd"

    iget v4, v2, Landroid/os/Debug$MemoryInfo;->dalvikSharedDirty:I

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387672
    const-string v0, "native_pd"

    iget v4, v2, Landroid/os/Debug$MemoryInfo;->nativePrivateDirty:I

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387673
    const-string v0, "native_pss"

    iget v4, v2, Landroid/os/Debug$MemoryInfo;->nativePss:I

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387674
    const-string v0, "native_sd"

    iget v4, v2, Landroid/os/Debug$MemoryInfo;->nativeSharedDirty:I

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387675
    const-string v0, "other_pd"

    iget v4, v2, Landroid/os/Debug$MemoryInfo;->otherPrivateDirty:I

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387676
    const-string v0, "other_pss"

    iget v4, v2, Landroid/os/Debug$MemoryInfo;->otherPss:I

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387677
    const-string v0, "other_sd"

    iget v4, v2, Landroid/os/Debug$MemoryInfo;->otherSharedDirty:I

    mul-int/lit16 v4, v4, 0x400

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387678
    const-string v0, "gc_total_count"

    invoke-static {}, Landroid/os/Debug;->getGlobalGcInvocationCount()I

    move-result v4

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387679
    const-string v0, "gc_freed_size"

    invoke-static {}, Landroid/os/Debug;->getGlobalFreedSize()I

    move-result v4

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387680
    const-string v0, "gc_freed_count"

    invoke-static {}, Landroid/os/Debug;->getGlobalFreedCount()I

    move-result v4

    invoke-virtual {v3, v0, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387681
    const-string v0, "native_heap_size"

    invoke-static {}, Landroid/os/Debug;->getNativeHeapSize()J

    move-result-wide v4

    invoke-virtual {v3, v0, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 387682
    const-string v0, "native_heap_allocated"

    invoke-static {}, Landroid/os/Debug;->getNativeHeapAllocatedSize()J

    move-result-wide v4

    invoke-virtual {v3, v0, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 387683
    const-string v0, "native_heap_free"

    invoke-static {}, Landroid/os/Debug;->getNativeHeapFreeSize()J

    move-result-wide v4

    invoke-virtual {v3, v0, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 387684
    iget-object v0, p0, LX/2G1;->m:LX/2G6;

    invoke-virtual {v0}, LX/2G6;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 387685
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 387686
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2IP;

    .line 387687
    invoke-virtual {v0}, LX/2IP;->a()LX/3zf;

    move-result-object v0

    .line 387688
    if-eqz v0, :cond_1

    .line 387689
    const-string v4, "jsc_count"

    iget-wide v6, v0, LX/3zf;->a:J

    invoke-virtual {v3, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 387690
    const-string v4, "jsc_malloc"

    iget-wide v6, v0, LX/3zf;->b:J

    invoke-virtual {v3, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 387691
    const-string v4, "jsc_block"

    iget-wide v6, v0, LX/3zf;->c:J

    invoke-virtual {v3, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 387692
    :cond_2
    :try_start_0
    const-class v4, Landroid/os/Debug$MemoryInfo;

    .line 387693
    iget-object v0, p0, LX/2G1;->n:Ljava/lang/reflect/Field;

    if-nez v0, :cond_3

    .line 387694
    const-string v0, "NUM_OTHER_STATS"

    invoke-virtual {v4, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, LX/2G1;->n:Ljava/lang/reflect/Field;

    .line 387695
    iget-object v0, p0, LX/2G1;->n:Ljava/lang/reflect/Field;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/2G1;->t:I

    .line 387696
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    if-lt v0, v8, :cond_4

    .line 387697
    :try_start_1
    iget-object v0, p0, LX/2G1;->o:Ljava/lang/reflect/Field;

    if-nez v0, :cond_4

    .line 387698
    const-string v0, "NUM_DVK_STATS"

    invoke-virtual {v4, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, LX/2G1;->o:Ljava/lang/reflect/Field;

    .line 387699
    iget-object v0, p0, LX/2G1;->o:Ljava/lang/reflect/Field;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/2G1;->u:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_4

    .line 387700
    :cond_4
    :goto_0
    :try_start_2
    iget-object v0, p0, LX/2G1;->p:Ljava/lang/reflect/Method;

    if-nez v0, :cond_5

    .line 387701
    const-string v0, "getOtherLabel"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v4, v0, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/2G1;->p:Ljava/lang/reflect/Method;

    .line 387702
    :cond_5
    iget-object v0, p0, LX/2G1;->q:Ljava/lang/reflect/Method;

    if-nez v0, :cond_6

    .line 387703
    const-string v0, "getOtherPss"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v4, v0, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/2G1;->q:Ljava/lang/reflect/Method;

    .line 387704
    :cond_6
    iget-object v0, p0, LX/2G1;->r:Ljava/lang/reflect/Method;

    if-nez v0, :cond_7

    .line 387705
    const-string v0, "getOtherPrivateDirty"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v4, v0, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/2G1;->r:Ljava/lang/reflect/Method;

    .line 387706
    :cond_7
    iget-object v0, p0, LX/2G1;->s:Ljava/lang/reflect/Method;

    if-nez v0, :cond_8

    .line 387707
    const-string v0, "getOtherSharedDirty"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v4, v0, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/2G1;->s:Ljava/lang/reflect/Method;

    :cond_8
    move v0, v1

    .line 387708
    :goto_1
    iget v4, p0, LX/2G1;->t:I

    if-ge v0, v4, :cond_a

    .line 387709
    const-string v4, "other"

    invoke-direct {p0, v2, v4, v3, v0}, LX/2G1;->a(Landroid/os/Debug$MemoryInfo;Ljava/lang/String;LX/0m9;I)V

    .line 387710
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 387711
    :catch_0
    move-exception v0

    .line 387712
    const-string v5, "AnalyticsDeviceUtils"

    const-string v6, "Unable to find NUM_DVK_STATS field"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v0, v6, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_0

    .line 387713
    :catch_1
    move-exception v0

    .line 387714
    :goto_2
    const-string v2, "AnalyticsDeviceUtils"

    const-string v4, "Unable to send detailed memory info"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 387715
    :cond_9
    const-string v0, "is_backgrounded"

    iget-object v1, p0, LX/2G1;->l:LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->j()Z

    move-result v1

    invoke-virtual {v3, v0, v1}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 387716
    const-string v0, "ever_foregrounded"

    iget-object v1, p0, LX/2G1;->l:LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->m()Z

    move-result v1

    invoke-virtual {v3, v0, v1}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 387717
    return-object v3

    :cond_a
    move v0, v1

    .line 387718
    :goto_3
    :try_start_3
    iget v4, p0, LX/2G1;->u:I

    if-ge v0, v4, :cond_9

    .line 387719
    const-string v4, "dalvik"

    iget v5, p0, LX/2G1;->t:I

    add-int/2addr v5, v0

    invoke-direct {p0, v2, v4, v3, v5}, LX/2G1;->a(Landroid/os/Debug$MemoryInfo;Ljava/lang/String;LX/0m9;I)V
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_4

    .line 387720
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 387721
    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_2
.end method

.method public static a(LX/2G1;)LX/0m9;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 387722
    iget-object v0, p0, LX/2G1;->h:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 387723
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    .line 387724
    iget-object v1, p0, LX/2G1;->k:LX/1sz;

    invoke-virtual {v1}, LX/1sz;->a()LX/2GO;

    move-result-object v1

    .line 387725
    invoke-direct {p0, v0, v1}, LX/2G1;->a(ILX/2GO;)LX/0m9;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/2G1;
    .locals 3

    .prologue
    .line 387726
    sget-object v0, LX/2G1;->v:LX/2G1;

    if-nez v0, :cond_1

    .line 387727
    const-class v1, LX/2G1;

    monitor-enter v1

    .line 387728
    :try_start_0
    sget-object v0, LX/2G1;->v:LX/2G1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387729
    if-eqz v2, :cond_0

    .line 387730
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2G1;->b(LX/0QB;)LX/2G1;

    move-result-object v0

    sput-object v0, LX/2G1;->v:LX/2G1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387731
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387732
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387733
    :cond_1
    sget-object v0, LX/2G1;->v:LX/2G1;

    return-object v0

    .line 387734
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387735
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 387736
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 387737
    const/16 v0, 0x20

    const/16 v1, 0x5f

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 387738
    :sswitch_0
    const-string v1, "code mmap"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "image mmap"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "Graphics"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "GL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "Memtrack"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    .line 387739
    :pswitch_0
    const-string v0, "oat_mmap"

    goto :goto_1

    .line 387740
    :pswitch_1
    const-string v0, "art_mmap"

    goto :goto_1

    .line 387741
    :pswitch_2
    const-string v0, "egl_mtrack"

    goto :goto_1

    .line 387742
    :pswitch_3
    const-string v0, "gl_mtrack"

    goto :goto_1

    .line 387743
    :pswitch_4
    const-string v0, "other_mtrack"

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5979e68c -> :sswitch_1
        -0x46a7dfde -> :sswitch_0
        -0x21d7f34a -> :sswitch_4
        0x8e5 -> :sswitch_3
        0x9db0eab -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Landroid/os/Debug$MemoryInfo;Ljava/lang/String;LX/0m9;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 387744
    iget-object v0, p0, LX/2G1;->p:Ljava/lang/reflect/Method;

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/2G1;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 387745
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_pss_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/2G1;->q:Ljava/lang/reflect/Method;

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    invoke-virtual {p3, v2, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387746
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_pd_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/2G1;->r:Ljava/lang/reflect/Method;

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    invoke-virtual {p3, v2, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387747
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_sd_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/2G1;->s:Ljava/lang/reflect/Method;

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    invoke-virtual {p3, v1, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 387748
    return-void
.end method

.method private static b(LX/0QB;)LX/2G1;
    .locals 14

    .prologue
    .line 387749
    new-instance v0, LX/2G1;

    const/16 v1, 0x4c1

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/2G2;->a(LX/0QB;)LX/2G2;

    move-result-object v2

    check-cast v2, LX/2G2;

    invoke-static {p0}, LX/0pj;->a(LX/0QB;)LX/0pj;

    move-result-object v3

    check-cast v3, LX/0pj;

    invoke-static {p0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v4

    check-cast v4, LX/1Gm;

    invoke-static {p0}, LX/1ia;->a(LX/0QB;)LX/1ia;

    move-result-object v5

    check-cast v5, LX/1ia;

    invoke-static {p0}, LX/2G3;->a(LX/0QB;)LX/2G3;

    move-result-object v6

    check-cast v6, LX/2G3;

    invoke-static {p0}, LX/2G4;->a(LX/0QB;)LX/2G5;

    move-result-object v7

    check-cast v7, LX/2G5;

    const-class v8, Landroid/content/Context;

    invoke-interface {p0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {p0}, LX/0TK;->a(LX/0QB;)LX/0TL;

    move-result-object v9

    check-cast v9, LX/0TL;

    const/16 v10, 0x93

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/1sI;->a(LX/0QB;)LX/1sz;

    move-result-object v11

    check-cast v11, LX/1sz;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v12

    check-cast v12, LX/0Uo;

    invoke-static {p0}, LX/2G6;->b(LX/0QB;)LX/2G6;

    move-result-object v13

    check-cast v13, LX/2G6;

    invoke-direct/range {v0 .. v13}, LX/2G1;-><init>(LX/0Ot;LX/2G2;LX/0pj;LX/1Gm;LX/1ia;LX/2G3;LX/2G5;Landroid/content/Context;LX/0TL;LX/0Ot;LX/1sz;LX/0Uo;LX/2G6;)V

    .line 387750
    return-object v0
.end method
