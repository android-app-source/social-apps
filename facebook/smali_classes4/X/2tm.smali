.class public LX/2tm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:LX/17Q;

.field private b:LX/0Zb;

.field private c:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final d:LX/0id;

.field private e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private f:Lcom/facebook/content/SecureContextHelper;

.field private g:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/17Q;LX/0Zb;Lcom/facebook/performancelogger/PerformanceLogger;LX/0id;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;LX/0Or;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsPermalinkOpportunisticLoggingEnabled;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17Q;",
            "LX/0Zb;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0id;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 475219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475220
    iput-object p1, p0, LX/2tm;->a:LX/17Q;

    .line 475221
    iput-object p2, p0, LX/2tm;->b:LX/0Zb;

    .line 475222
    iput-object p3, p0, LX/2tm;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 475223
    iput-object p4, p0, LX/2tm;->d:LX/0id;

    .line 475224
    iput-object p5, p0, LX/2tm;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 475225
    iput-object p6, p0, LX/2tm;->f:Lcom/facebook/content/SecureContextHelper;

    .line 475226
    iput-object p7, p0, LX/2tm;->h:LX/0Or;

    .line 475227
    iput-object p8, p0, LX/2tm;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 475228
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x2

    const v0, 0x51d2671f

    invoke-static {v1, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 475229
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 475230
    check-cast v0, Landroid/widget/TextView;

    .line 475231
    invoke-virtual {v0}, Landroid/widget/TextView;->hasSelection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475232
    const v0, -0x7d453787

    invoke-static {v1, v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 475233
    :goto_0
    return-void

    .line 475234
    :cond_0
    iget-object v0, p0, LX/2tm;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 475235
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 475236
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 475237
    iget-object v1, p0, LX/2tm;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 475238
    iget-object v1, p0, LX/2tm;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 475239
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/2tm;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    .line 475240
    invoke-static {v4}, LX/17Q;->F(LX/0lF;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 475241
    const/4 v6, 0x0

    .line 475242
    :goto_1
    move-object v1, v6

    .line 475243
    iget-object v3, p0, LX/2tm;->b:LX/0Zb;

    invoke-interface {v3, v1}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 475244
    :cond_1
    iget-object v1, p0, LX/2tm;->d:LX/0id;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "FeedStoryPermalink"

    invoke-virtual {v1, v3, v4}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 475245
    iget-object v1, p0, LX/2tm;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v3, 0xa0008

    const-string v4, "NNF_PermalinkFromFeedLoad"

    invoke-interface {v1, v3, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 475246
    iget-object v1, p0, LX/2tm;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v1, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v0

    .line 475247
    iget-object v1, p0, LX/2tm;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 475248
    const v0, -0x6df8d2e

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 475249
    :cond_2
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "open_permalink_view"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "story_legacy_api_post_id"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "story_graphql_id"

    invoke-virtual {v6, v7, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "native_newsfeed"

    .line 475250
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 475251
    move-object v6, v6

    .line 475252
    goto :goto_1
.end method
