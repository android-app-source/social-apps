.class public LX/2DS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 384209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384210
    const-string v0, "touch_exploration_enabled"

    iput-object v0, p0, LX/2DS;->b:Ljava/lang/String;

    .line 384211
    const-string v0, "toggled_via_gesture"

    iput-object v0, p0, LX/2DS;->c:Ljava/lang/String;

    .line 384212
    iput-object p1, p0, LX/2DS;->a:LX/0Zb;

    .line 384213
    return-void
.end method


# virtual methods
.method public final a(ZZ)V
    .locals 2

    .prologue
    .line 384214
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "touch_exploration_state"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 384215
    const-string v1, "touch_exploration_enabled"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 384216
    const-string v1, "toggled_via_gesture"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 384217
    iget-object v1, p0, LX/2DS;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 384218
    return-void
.end method
