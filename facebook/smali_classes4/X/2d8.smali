.class public final LX/2d8;
.super LX/1SG;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1SG",
        "<",
        "LX/2cy;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2cy;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2cy;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2dH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 442953
    invoke-direct {p0}, LX/1SG;-><init>()V

    .line 442954
    new-instance v0, LX/2d9;

    invoke-direct {v0, p0, p1}, LX/2d9;-><init>(LX/2d8;LX/0Ot;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-static {p2, v0, v1}, LX/1SK;->a(LX/0Ot;LX/0QK;Ljava/util/concurrent/Executor;)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/2d8;->a:LX/0Ot;

    .line 442955
    return-void
.end method

.method public static b(LX/0QB;)LX/2d8;
    .locals 3

    .prologue
    .line 442956
    new-instance v0, LX/2d8;

    const/16 v1, 0xf65

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xf99

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2d8;-><init>(LX/0Ot;LX/0Ot;)V

    .line 442957
    return-object v0
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 442958
    invoke-virtual {p0}, LX/2d8;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2cy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442959
    iget-object v0, p0, LX/2d8;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 442960
    invoke-virtual {p0}, LX/2d8;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
