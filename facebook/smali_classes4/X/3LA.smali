.class public LX/3LA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3L9",
        "<",
        "Landroid/view/View;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550064
    iput-object p1, p0, LX/3LA;->a:Landroid/content/Context;

    .line 550065
    return-void
.end method

.method public static b(LX/0QB;)LX/3LA;
    .locals 2

    .prologue
    .line 550066
    new-instance v1, LX/3LA;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/3LA;-><init>(Landroid/content/Context;)V

    .line 550067
    return-object v1
.end method


# virtual methods
.method public final a(LX/3OO;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 550083
    check-cast p2, Landroid/view/View;

    .line 550084
    check-cast p2, LX/Jho;

    .line 550085
    if-nez p2, :cond_0

    .line 550086
    new-instance p2, LX/Jho;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Jho;-><init>(Landroid/content/Context;)V

    .line 550087
    :cond_0
    invoke-virtual {p2, p1}, LX/Jho;->setContactRow(LX/3OO;)V

    .line 550088
    return-object p2
.end method

.method public final a(LX/3OQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 550068
    check-cast p2, Landroid/view/View;

    .line 550069
    sget-object v0, LX/Ji5;->f:LX/3OQ;

    if-ne p1, v0, :cond_1

    .line 550070
    check-cast p2, LX/Jhs;

    .line 550071
    if-nez p2, :cond_0

    .line 550072
    new-instance p2, LX/Jhs;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Jhs;-><init>(Landroid/content/Context;)V

    .line 550073
    :cond_0
    :goto_0
    return-object p2

    :cond_1
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public final a(LX/3OU;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 550074
    check-cast p2, Landroid/view/View;

    .line 550075
    check-cast p2, LX/Jih;

    .line 550076
    if-nez p2, :cond_0

    .line 550077
    new-instance p2, LX/Jih;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Jih;-><init>(Landroid/content/Context;)V

    .line 550078
    :cond_0
    iget-object v0, p1, LX/3OU;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, LX/Jih;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 550079
    iget-object v0, p1, LX/3OU;->a:LX/3OS;

    .line 550080
    iput-object v0, p2, LX/Jih;->b:LX/3OS;

    .line 550081
    iget-object p0, p2, LX/Jih;->a:Lcom/facebook/messaging/ui/name/ThreadNameView;

    invoke-static {p2}, LX/Jih;->getSubtitleData(LX/Jih;)LX/FON;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2Wj;->setData(Ljava/lang/Object;)V

    .line 550082
    return-object p2
.end method

.method public final a(LX/3Ou;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 550009
    check-cast p2, Landroid/view/View;

    .line 550010
    check-cast p2, LX/Js7;

    .line 550011
    if-nez p2, :cond_0

    .line 550012
    new-instance p2, LX/Js7;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Js7;-><init>(Landroid/content/Context;)V

    .line 550013
    :cond_0
    iget-object v0, p1, LX/3Ou;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, LX/Js7;->setText(Ljava/lang/String;)V

    .line 550014
    iget-object v0, p1, LX/3Ou;->b:Ljava/lang/String;

    invoke-virtual {p2, v0}, LX/Js7;->setActionButtonText(Ljava/lang/String;)V

    .line 550015
    iget-object v0, p1, LX/3Ou;->c:Landroid/view/View$OnClickListener;

    .line 550016
    iput-object v0, p2, LX/Js7;->c:Landroid/view/View$OnClickListener;

    .line 550017
    return-object p2
.end method

.method public final a(LX/DAO;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 550049
    check-cast p2, Landroid/view/View;

    .line 550050
    check-cast p2, LX/JhW;

    .line 550051
    if-nez p2, :cond_0

    .line 550052
    new-instance p2, LX/JhW;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/JhW;-><init>(Landroid/content/Context;)V

    .line 550053
    :cond_0
    iput-object p1, p2, LX/JhW;->e:LX/DAO;

    .line 550054
    invoke-static {p2}, LX/JhW;->b$redex0(LX/JhW;)V

    .line 550055
    return-object p2
.end method

.method public final a(LX/DAU;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 550056
    check-cast p2, Landroid/view/View;

    .line 550057
    check-cast p2, LX/Jhc;

    .line 550058
    if-nez p2, :cond_0

    .line 550059
    new-instance p2, LX/Jhc;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Jhc;-><init>(Landroid/content/Context;)V

    .line 550060
    :cond_0
    iput-object p1, p2, LX/Jhc;->A:LX/DAU;

    .line 550061
    invoke-static {p2}, LX/Jhc;->a(LX/Jhc;)V

    .line 550062
    return-object p2
.end method

.method public final a(LX/DAV;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 550025
    check-cast p2, Landroid/view/View;

    .line 550026
    check-cast p2, LX/Jhw;

    .line 550027
    if-nez p2, :cond_0

    .line 550028
    new-instance p2, LX/Jhw;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Jhw;-><init>(Landroid/content/Context;)V

    .line 550029
    :cond_0
    iput-object p1, p2, LX/Jhw;->c:LX/DAV;

    .line 550030
    new-instance v1, Lcom/facebook/user/model/UserKey;

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v3, p2, LX/Jhw;->c:LX/DAV;

    iget-object v3, v3, LX/DAV;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 550031
    iget-object v2, p2, LX/Jhw;->d:Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 550032
    iget-object v1, p2, LX/Jhw;->e:Landroid/widget/TextView;

    iget-object v2, p2, LX/Jhw;->c:LX/DAV;

    iget-object v2, v2, LX/DAV;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 550033
    iget-object v2, p2, LX/Jhw;->f:Landroid/widget/TextView;

    iget-object v3, p2, LX/Jhw;->b:LX/6lN;

    iget-object v1, p2, LX/Jhw;->c:LX/DAV;

    iget-object v1, v1, LX/DAV;->e:LX/0Px;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, LX/6lN;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 550034
    iget-object v1, p2, LX/Jhw;->g:Landroid/widget/TextView;

    const/16 v7, 0x21

    const/4 v4, 0x0

    .line 550035
    iget-object v2, p2, LX/Jhw;->c:LX/DAV;

    iget-object v2, v2, LX/DAV;->e:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, p2, LX/Jhw;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-virtual {v3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 550036
    iget-object v2, p2, LX/Jhw;->c:LX/DAV;

    iget-object v2, v2, LX/DAV;->e:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    .line 550037
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v2, "\\b"

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, LX/Jhw;->c:LX/DAV;

    iget-object v6, v2, LX/DAV;->d:Ljava/lang/String;

    iget-object v2, p2, LX/Jhw;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\\b"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 550038
    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 550039
    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 550040
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-nez v3, :cond_1

    .line 550041
    const/4 v2, -0x1

    invoke-static {v4, v2}, LX/Jhw;->a(Landroid/text/Spannable;I)Landroid/text/Spannable;

    move-result-object v2

    .line 550042
    :goto_0
    move-object v2, v2

    .line 550043
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 550044
    return-object p2

    .line 550045
    :cond_1
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    .line 550046
    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v3, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    iget-object v5, p2, LX/Jhw;->c:LX/DAV;

    iget-object v5, v5, LX/DAV;->d:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v2

    invoke-interface {v4, v3, v2, v5, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 550047
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p2}, LX/Jhw;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0194

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {v3, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v5, p2, LX/Jhw;->c:LX/DAV;

    iget-object v5, v5, LX/DAV;->d:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v2

    invoke-interface {v4, v3, v2, v5, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 550048
    invoke-static {v4, v2}, LX/Jhw;->a(Landroid/text/Spannable;I)Landroid/text/Spannable;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/DAW;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 550018
    check-cast p2, Landroid/view/View;

    .line 550019
    check-cast p2, LX/Jhr;

    .line 550020
    if-nez p2, :cond_0

    .line 550021
    new-instance p2, LX/Jhr;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Jhr;-><init>(Landroid/content/Context;)V

    .line 550022
    :cond_0
    iput-object p1, p2, LX/Jhr;->h:LX/DAW;

    .line 550023
    invoke-static {p2}, LX/Jhr;->b(LX/Jhr;)V

    .line 550024
    return-object p2
.end method

.method public final a(LX/DAX;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 550002
    check-cast p2, Landroid/view/View;

    .line 550003
    check-cast p2, LX/Ji1;

    .line 550004
    if-nez p2, :cond_0

    .line 550005
    new-instance p2, LX/Ji1;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Ji1;-><init>(Landroid/content/Context;)V

    .line 550006
    :cond_0
    iget-object v0, p1, LX/DAX;->a:Ljava/lang/String;

    .line 550007
    iget-object p0, p2, LX/Ji1;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 550008
    return-object p2
.end method

.method public final a(LX/DAY;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 549981
    check-cast p2, Landroid/view/View;

    .line 549982
    check-cast p2, LX/Ji2;

    .line 549983
    if-nez p2, :cond_0

    .line 549984
    new-instance p2, LX/Ji2;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Ji2;-><init>(Landroid/content/Context;)V

    .line 549985
    :cond_0
    iput-object p1, p2, LX/Ji2;->b:LX/DAY;

    .line 549986
    iget-object v0, p2, LX/Ji2;->c:Lcom/facebook/user/tiles/UserTileView;

    iget-object v1, p2, LX/Ji2;->a:LX/FJw;

    iget-object p0, p2, LX/Ji2;->b:LX/DAY;

    iget-object p0, p0, LX/DAY;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v1, p0}, LX/FJw;->a(Lcom/facebook/user/model/User;)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 549987
    iget-object v0, p2, LX/Ji2;->d:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    iget-object v1, p2, LX/Ji2;->b:LX/DAY;

    iget-object v1, v1, LX/DAY;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setText(Ljava/lang/CharSequence;)V

    .line 549988
    const/4 p1, 0x0

    .line 549989
    iget-object v0, p2, LX/Ji2;->b:LX/DAY;

    .line 549990
    iget-object v1, v0, LX/DAY;->a:Lcom/facebook/user/model/User;

    .line 549991
    iget-boolean v0, v1, Lcom/facebook/user/model/User;->V:Z

    move v1, v0

    .line 549992
    move v0, v1

    .line 549993
    if-eqz v0, :cond_1

    .line 549994
    iget-object v0, p2, LX/Ji2;->d:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    iget v1, p2, LX/Ji2;->f:I

    invoke-virtual {v0, v1}, LX/2Wj;->setTextColor(I)V

    .line 549995
    iget-object v0, p2, LX/Ji2;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 549996
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/Ji2;->setEnabled(Z)V

    .line 549997
    :goto_0
    return-object p2

    .line 549998
    :cond_1
    iget-object v0, p2, LX/Ji2;->d:Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    invoke-virtual {p2}, LX/Ji2;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p0, 0x7f0a0218

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/2Wj;->setTextColor(I)V

    .line 549999
    iget-object v0, p2, LX/Ji2;->e:Landroid/widget/TextView;

    invoke-virtual {p2}, LX/Ji2;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p0, 0x7f0a0218

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 550000
    iget-object v0, p2, LX/Ji2;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 550001
    invoke-virtual {p2, p1}, LX/Ji2;->setEnabled(Z)V

    goto :goto_0
.end method

.method public final a(LX/DAa;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549974
    check-cast p2, Landroid/view/View;

    .line 549975
    check-cast p2, LX/Js7;

    .line 549976
    if-nez p2, :cond_0

    .line 549977
    new-instance p2, LX/Js7;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Js7;-><init>(Landroid/content/Context;)V

    .line 549978
    :cond_0
    iget-object v0, p1, LX/DAa;->a:Ljava/lang/String;

    move-object v0, v0

    .line 549979
    invoke-virtual {p2, v0}, LX/Js7;->setText(Ljava/lang/String;)V

    .line 549980
    return-object p2
.end method

.method public final a(LX/DAb;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549969
    check-cast p2, Landroid/view/View;

    .line 549970
    check-cast p2, LX/JiR;

    .line 549971
    if-nez p2, :cond_0

    .line 549972
    new-instance p2, LX/JiR;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/JiR;-><init>(Landroid/content/Context;)V

    .line 549973
    :cond_0
    return-object p2
.end method

.method public final a(LX/DAj;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 549961
    check-cast p2, Landroid/view/View;

    .line 549962
    check-cast p2, LX/Jie;

    .line 549963
    if-nez p2, :cond_0

    .line 549964
    new-instance p2, LX/Jie;

    iget-object v0, p0, LX/3LA;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Jie;-><init>(Landroid/content/Context;)V

    .line 549965
    :cond_0
    iget-object v0, p1, LX/DAj;->a:Ljava/lang/String;

    .line 549966
    iget-object p0, p2, LX/Jie;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 549967
    iget-object v0, p1, LX/DAj;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, LX/Jie;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 549968
    return-object p2
.end method
