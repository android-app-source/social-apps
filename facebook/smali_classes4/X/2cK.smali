.class public final enum LX/2cK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2cK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2cK;

.field public static final enum IGNORED:LX/2cK;

.field public static final enum SUBSCRIBED:LX/2cK;

.field public static final enum UNSUBSCRIBED:LX/2cK;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 440644
    new-instance v0, LX/2cK;

    const-string v1, "IGNORED"

    invoke-direct {v0, v1, v2}, LX/2cK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2cK;->IGNORED:LX/2cK;

    .line 440645
    new-instance v0, LX/2cK;

    const-string v1, "UNSUBSCRIBED"

    invoke-direct {v0, v1, v3}, LX/2cK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2cK;->UNSUBSCRIBED:LX/2cK;

    .line 440646
    new-instance v0, LX/2cK;

    const-string v1, "SUBSCRIBED"

    invoke-direct {v0, v1, v4}, LX/2cK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2cK;->SUBSCRIBED:LX/2cK;

    .line 440647
    const/4 v0, 0x3

    new-array v0, v0, [LX/2cK;

    sget-object v1, LX/2cK;->IGNORED:LX/2cK;

    aput-object v1, v0, v2

    sget-object v1, LX/2cK;->UNSUBSCRIBED:LX/2cK;

    aput-object v1, v0, v3

    sget-object v1, LX/2cK;->SUBSCRIBED:LX/2cK;

    aput-object v1, v0, v4

    sput-object v0, LX/2cK;->$VALUES:[LX/2cK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 440648
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2cK;
    .locals 1

    .prologue
    .line 440649
    const-class v0, LX/2cK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2cK;

    return-object v0
.end method

.method public static values()[LX/2cK;
    .locals 1

    .prologue
    .line 440650
    sget-object v0, LX/2cK;->$VALUES:[LX/2cK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2cK;

    return-object v0
.end method
