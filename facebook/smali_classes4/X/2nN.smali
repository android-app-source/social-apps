.class public LX/2nN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 464102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 464103
    return-void
.end method

.method public static a(LX/0Px;ILandroid/content/res/Resources;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;",
            ">;I",
            "Landroid/content/res/Resources;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 464096
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 464097
    sget-object v4, LX/CBr;->NEWSFEED_LARGE_IMAGE_MARGIN_OPTION:LX/CBr;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object v5

    if-ne v4, v5, :cond_0

    .line 464098
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, LX/C4m;->a(Ljava/lang/String;ILandroid/content/res/Resources;)I

    move-result v0

    .line 464099
    :goto_1
    return v0

    .line 464100
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 464101
    goto :goto_1
.end method

.method public static a(LX/0Px;)LX/CBl;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;",
            ">;)",
            "LX/CBl;"
        }
    .end annotation

    .prologue
    .line 464090
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 464091
    sget-object v3, LX/CBr;->FEED_TAP_ACTION:LX/CBr;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 464092
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBl;->fromString(Ljava/lang/String;)LX/CBl;

    move-result-object v0

    .line 464093
    :goto_1
    return-object v0

    .line 464094
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464095
    :cond_1
    sget-object v0, LX/CBl;->UNKNOWN:LX/CBl;

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;LX/2mJ;LX/13P;)Landroid/view/View$OnClickListener;
    .locals 4

    .prologue
    .line 464081
    invoke-static {p0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v1

    .line 464082
    invoke-static {p0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v2

    .line 464083
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->a(LX/0Px;)LX/CBl;

    move-result-object v0

    .line 464084
    sget-object v3, LX/CBl;->USE_PRIMARY_ACTION:LX/CBl;

    if-ne v0, v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v3

    invoke-static {v3}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 464085
    new-instance v0, LX/CBG;

    invoke-direct {v0, p1, v2, p2, v1}, LX/CBG;-><init>(LX/2mJ;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;LX/13P;Lcom/facebook/graphql/model/GraphQLQuickPromotion;)V

    .line 464086
    :goto_0
    return-object v0

    .line 464087
    :cond_0
    sget-object v3, LX/CBl;->USE_SECONDARY_ACTION:LX/CBl;

    if-ne v0, v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v0

    invoke-static {v0}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 464088
    new-instance v0, LX/CBH;

    invoke-direct {v0, p1, v2, p2, v1}, LX/CBH;-><init>(LX/2mJ;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;LX/13P;Lcom/facebook/graphql/model/GraphQLQuickPromotion;)V

    goto :goto_0

    .line 464089
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 464080
    invoke-static {p0}, LX/2nN;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z
    .locals 4
    .param p0    # Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 464076
    invoke-static {p0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v1

    .line 464077
    invoke-static {p0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v2

    .line 464078
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 464079
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v3

    invoke-static {v3}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v1

    invoke-static {v1}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/2nM;->NEWSFEED_BRANDED_BACKGROUND_COLORED_IMAGE:LX/2nM;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/2nM;->fromString(Ljava/lang/String;)LX/2nM;

    move-result-object v2

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Z
    .locals 1

    .prologue
    .line 464075
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0Px;ILandroid/content/res/Resources;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;",
            ">;I",
            "Landroid/content/res/Resources;",
            ")I"
        }
    .end annotation

    .prologue
    .line 464070
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 464071
    sget-object v3, LX/CBr;->CONTENT_BLOCK_BOTTOM_MARGIN:LX/CBr;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object v4

    if-ne v3, v4, :cond_1

    .line 464072
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, LX/C4m;->b(Ljava/lang/String;ILandroid/content/res/Resources;)I

    move-result p1

    .line 464073
    :cond_0
    return p1

    .line 464074
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 464069
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z
    .locals 4
    .param p0    # Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 464104
    invoke-static {p0}, LX/2nL;->c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    move-result-object v1

    .line 464105
    invoke-static {p0}, LX/2nL;->b(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    move-result-object v2

    .line 464106
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 464107
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->p()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v3

    invoke-static {v3}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v1

    invoke-static {v1}, LX/2nN;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/2nM;->NEWSFEED_BRANDED_BACKGROUND_COLORED_IMAGE:LX/2nM;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->l()Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionTemplate;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/2nM;->fromString(Ljava/lang/String;)LX/2nM;

    move-result-object v2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(LX/0Px;)LX/2nO;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;",
            ">;)",
            "LX/2nO;"
        }
    .end annotation

    .prologue
    .line 464063
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 464064
    sget-object v3, LX/CBr;->PRIMARY_ACTION_BUTTON_GLYPH:LX/CBr;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 464065
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2nO;->fromString(Ljava/lang/String;)LX/2nO;

    move-result-object v0

    .line 464066
    :goto_1
    return-object v0

    .line 464067
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464068
    :cond_1
    sget-object v0, LX/2nO;->UNKNOWN:LX/2nO;

    goto :goto_1
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z
    .locals 1

    .prologue
    .line 464062
    invoke-static {p0}, LX/2nL;->d(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/0Px;)LX/CBq;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;",
            ">;)",
            "LX/CBq;"
        }
    .end annotation

    .prologue
    .line 464056
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 464057
    sget-object v3, LX/CBr;->SECONDARY_ACTION_BUTTON_GLYPH:LX/CBr;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 464058
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBq;->fromString(Ljava/lang/String;)LX/CBq;

    move-result-object v0

    .line 464059
    :goto_1
    return-object v0

    .line 464060
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464061
    :cond_1
    sget-object v0, LX/CBq;->UNKNOWN:LX/CBq;

    goto :goto_1
.end method

.method public static e(LX/0Px;)LX/CBp;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;",
            ">;)",
            "LX/CBp;"
        }
    .end annotation

    .prologue
    .line 464050
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 464051
    sget-object v3, LX/CBr;->PRIMARY_ACTION_BUTTON_STYLE:LX/CBr;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 464052
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBp;->fromString(Ljava/lang/String;)LX/CBp;

    move-result-object v0

    .line 464053
    :goto_1
    return-object v0

    .line 464054
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464055
    :cond_1
    sget-object v0, LX/CBp;->UNKNOWN:LX/CBp;

    goto :goto_1
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 464043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;->n()LX/0Px;

    move-result-object v3

    .line 464044
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 464045
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v5, 0x650f0e32

    if-ne v0, v5, :cond_0

    .line 464046
    const/4 v0, 0x1

    .line 464047
    :goto_1
    return v0

    .line 464048
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 464049
    goto :goto_1
.end method

.method public static g(LX/0Px;)LX/CBk;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;",
            ">;)",
            "LX/CBk;"
        }
    .end annotation

    .prologue
    .line 464037
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 464038
    sget-object v3, LX/CBr;->FAVICON_COLOR:LX/CBr;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 464039
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBk;->fromString(Ljava/lang/String;)LX/CBk;

    move-result-object v0

    .line 464040
    :goto_1
    return-object v0

    .line 464041
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 464042
    :cond_1
    sget-object v0, LX/CBk;->UNKNOWN:LX/CBk;

    goto :goto_1
.end method

.method public static h(LX/0Px;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/high16 v1, -0x1000000

    .line 464024
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_3

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 464025
    sget-object v4, LX/CBr;->ARGB_TEXT_COLOR:LX/CBr;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object v5

    if-ne v4, v5, :cond_2

    .line 464026
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->a()Ljava/lang/String;

    move-result-object v0

    .line 464027
    if-nez v0, :cond_0

    move v0, v1

    .line 464028
    :goto_1
    return v0

    .line 464029
    :cond_0
    const-string v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 464030
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 464031
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 464032
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 464033
    :cond_1
    :try_start_0
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_1

    .line 464034
    :catch_0
    move v0, v1

    goto :goto_1

    .line 464035
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 464036
    goto :goto_1
.end method

.method public static i(LX/0Px;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 464011
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_3

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;

    .line 464012
    sget-object v4, LX/CBr;->ARGB_BACKGROUND_COLOR:LX/CBr;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/CBr;->fromString(Ljava/lang/String;)LX/CBr;

    move-result-object v5

    if-ne v4, v5, :cond_2

    .line 464013
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQPTemplateParameter;->a()Ljava/lang/String;

    move-result-object v0

    .line 464014
    if-nez v0, :cond_0

    move v0, v1

    .line 464015
    :goto_1
    return v0

    .line 464016
    :cond_0
    const-string v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 464017
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 464018
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 464019
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 464020
    :cond_1
    :try_start_0
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_1

    .line 464021
    :catch_0
    move v0, v1

    goto :goto_1

    .line 464022
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 464023
    goto :goto_1
.end method
