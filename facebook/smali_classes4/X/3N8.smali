.class public LX/3N8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 558343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558344
    iput-object p1, p0, LX/3N8;->a:LX/0Uh;

    .line 558345
    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;)Landroid/util/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesInterfaces$BookingRequestDetail$;",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesInterfaces$BookingRequestDetail$;",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesInterfaces$BookingRequestDetail$;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 558335
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 558336
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 558337
    :goto_0
    return-object v0

    .line 558338
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 558339
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 558340
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 558341
    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$PageModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 558342
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/3N8;Ljava/lang/String;Ljava/lang/String;IIILcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 558255
    const/4 v2, 0x0

    .line 558256
    if-lez p3, :cond_0

    .line 558257
    const/4 v2, 0x1

    .line 558258
    :cond_0
    if-lez p4, :cond_1

    .line 558259
    add-int/lit8 v2, v2, 0x1

    .line 558260
    :cond_1
    if-lez p5, :cond_2

    .line 558261
    add-int/lit8 v2, v2, 0x1

    .line 558262
    :cond_2
    move v2, v2

    .line 558263
    if-ne v2, v1, :cond_6

    .line 558264
    if-eqz p6, :cond_4

    .line 558265
    :cond_3
    :goto_0
    return-object p6

    .line 558266
    :cond_4
    if-eqz p7, :cond_5

    move-object p6, p7

    .line 558267
    goto :goto_0

    :cond_5
    move-object p6, p8

    .line 558268
    goto :goto_0

    .line 558269
    :cond_6
    invoke-static {p1, p2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 558270
    iget-object v1, p0, LX/3N8;->a:LX/0Uh;

    const/16 v2, 0x61e

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 558271
    if-lez p5, :cond_7

    move-object p6, p8

    .line 558272
    goto :goto_0

    .line 558273
    :cond_7
    if-lez p4, :cond_3

    move-object p6, p7

    .line 558274
    goto :goto_0

    .line 558275
    :cond_8
    if-lez p4, :cond_9

    move-object p6, p7

    .line 558276
    goto :goto_0

    .line 558277
    :cond_9
    if-lez p5, :cond_3

    move-object p6, p8

    .line 558278
    goto :goto_0

    .line 558279
    :cond_a
    add-int v2, p4, p5

    if-ne v2, v1, :cond_b

    move v0, v1

    .line 558280
    :cond_b
    if-eqz v0, :cond_3

    if-le p3, v1, :cond_3

    .line 558281
    if-ne p4, v1, :cond_c

    move-object p6, p7

    goto :goto_0

    :cond_c
    move-object p6, p8

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5Vt;LX/6g6;)V
    .locals 16

    .prologue
    .line 558282
    invoke-interface/range {p1 .. p1}, LX/5Vt;->ae()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;

    move-result-object v2

    .line 558283
    invoke-interface/range {p1 .. p1}, LX/5Vt;->af()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;

    move-result-object v11

    .line 558284
    invoke-interface/range {p1 .. p1}, LX/5Vt;->ah()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;

    move-result-object v12

    .line 558285
    if-nez v2, :cond_1

    if-nez v11, :cond_1

    if-nez v12, :cond_1

    .line 558286
    :cond_0
    :goto_0
    return-void

    .line 558287
    :cond_1
    const/4 v5, 0x0

    .line 558288
    const/4 v6, 0x0

    .line 558289
    const/4 v7, 0x0

    .line 558290
    const/4 v8, 0x0

    .line 558291
    const/4 v9, 0x0

    .line 558292
    const/4 v10, 0x0

    .line 558293
    const/4 v4, 0x0

    .line 558294
    const/4 v3, 0x0

    .line 558295
    new-instance v13, LX/6fo;

    invoke-direct {v13}, LX/6fo;-><init>()V

    .line 558296
    if-eqz v2, :cond_2

    .line 558297
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;->b()I

    move-result v5

    .line 558298
    invoke-virtual {v13, v5}, LX/6fo;->b(I)LX/6fo;

    .line 558299
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;->a()LX/0Px;

    move-result-object v14

    invoke-virtual {v14}, LX/0Px;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_2

    .line 558300
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;->a()LX/0Px;

    move-result-object v2

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel$EdgesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;

    move-result-object v8

    .line 558301
    :cond_2
    if-eqz v11, :cond_3

    .line 558302
    invoke-virtual {v11}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;->b()I

    move-result v6

    .line 558303
    invoke-virtual {v13, v6}, LX/6fo;->a(I)LX/6fo;

    .line 558304
    invoke-virtual {v11}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 558305
    invoke-virtual {v11}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;->a()LX/0Px;

    move-result-object v2

    const/4 v9, 0x0

    invoke-virtual {v2, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel$EdgesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;

    move-result-object v9

    .line 558306
    :cond_3
    if-eqz v12, :cond_4

    .line 558307
    invoke-virtual {v12}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;->b()I

    move-result v7

    .line 558308
    invoke-virtual {v13, v7}, LX/6fo;->c(I)LX/6fo;

    .line 558309
    invoke-virtual {v12}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 558310
    invoke-virtual {v12}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;->a()LX/0Px;

    move-result-object v2

    const/4 v10, 0x0

    invoke-virtual {v2, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel$EdgesModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;

    move-result-object v10

    .line 558311
    :cond_4
    add-int v2, v5, v6

    add-int/2addr v2, v7

    if-eqz v2, :cond_0

    .line 558312
    invoke-static {v8, v9, v10}, LX/3N8;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;)Landroid/util/Pair;

    move-result-object v11

    .line 558313
    if-eqz v11, :cond_f

    .line 558314
    iget-object v2, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 558315
    iget-object v3, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    .line 558316
    invoke-virtual {v13, v2}, LX/6fo;->b(Ljava/lang/String;)LX/6fo;

    .line 558317
    invoke-virtual {v13, v3}, LX/6fo;->a(Ljava/lang/String;)LX/6fo;

    move-object v11, v3

    move-object v3, v2

    .line 558318
    :goto_1
    invoke-interface/range {p1 .. p1}, LX/5Vp;->aj()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v2, p0

    invoke-static/range {v2 .. v10}, LX/3N8;->a(LX/3N8;Ljava/lang/String;Ljava/lang/String;IIILcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;

    move-result-object v7

    .line 558319
    new-instance v8, LX/6fc;

    invoke-direct {v8}, LX/6fc;-><init>()V

    .line 558320
    if-eqz v7, :cond_5

    .line 558321
    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    move-result-object v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    .line 558322
    :goto_2
    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->cX_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    move-result-object v4

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    move v6, v4

    .line 558323
    :goto_3
    if-eqz v2, :cond_9

    .line 558324
    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;->b()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    .line 558325
    if-eqz v4, :cond_8

    const/4 v4, 0x1

    :goto_4
    if-eqz v4, :cond_a

    .line 558326
    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;->b()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 558327
    const/4 v9, 0x0

    invoke-virtual {v5, v4, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 558328
    :goto_5
    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/6fc;->a(Ljava/lang/String;)LX/6fc;

    move-result-object v9

    if-eqz v2, :cond_b

    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;->d()Ljava/lang/String;

    move-result-object v5

    :goto_6
    invoke-virtual {v9, v5}, LX/6fc;->b(Ljava/lang/String;)LX/6fc;

    move-result-object v5

    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v9

    invoke-virtual {v5, v9}, LX/6fc;->a(Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)LX/6fc;

    move-result-object v5

    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->cW_()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, LX/6fc;->c(Ljava/lang/String;)LX/6fc;

    move-result-object v5

    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->e()J

    move-result-wide v14

    invoke-virtual {v5, v14, v15}, LX/6fc;->a(J)LX/6fc;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/6fc;->e(Ljava/lang/String;)LX/6fc;

    move-result-object v3

    invoke-virtual {v3, v11}, LX/6fc;->d(Ljava/lang/String;)LX/6fc;

    move-result-object v3

    if-eqz v2, :cond_c

    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$ProductItemModel;->c()Ljava/lang/String;

    move-result-object v2

    :goto_7
    invoke-virtual {v3, v2}, LX/6fc;->f(Ljava/lang/String;)LX/6fc;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/6fc;->g(Ljava/lang/String;)LX/6fc;

    move-result-object v3

    if-eqz v6, :cond_d

    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->cX_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;->b()Ljava/lang/String;

    move-result-object v2

    :goto_8
    invoke-virtual {v3, v2}, LX/6fc;->h(Ljava/lang/String;)LX/6fc;

    move-result-object v3

    if-eqz v6, :cond_e

    invoke-virtual {v7}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel;->cX_()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestDetailModel$UserModel;->c()Ljava/lang/String;

    move-result-object v2

    :goto_9
    invoke-virtual {v3, v2}, LX/6fc;->i(Ljava/lang/String;)LX/6fc;

    .line 558329
    :cond_5
    invoke-virtual {v8}, LX/6fc;->a()Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    move-result-object v2

    invoke-virtual {v13, v2}, LX/6fo;->a(Lcom/facebook/messaging/model/threads/BookingRequestDetail;)LX/6fo;

    .line 558330
    invoke-virtual {v13}, LX/6fo;->a()Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/6g6;

    goto/16 :goto_0

    .line 558331
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 558332
    :cond_7
    const/4 v4, 0x0

    move v6, v4

    goto/16 :goto_3

    .line 558333
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 558334
    :cond_b
    const/4 v5, 0x0

    goto :goto_6

    :cond_c
    const/4 v2, 0x0

    goto :goto_7

    :cond_d
    const/4 v2, 0x0

    goto :goto_8

    :cond_e
    const/4 v2, 0x0

    goto :goto_9

    :cond_f
    move-object v11, v3

    move-object v3, v4

    goto/16 :goto_1
.end method
