.class public LX/39o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/359;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/359",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/359;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 523920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523921
    iput-object p1, p0, LX/39o;->a:LX/359;

    .line 523922
    return-void
.end method

.method public static a(LX/0QB;)LX/39o;
    .locals 4

    .prologue
    .line 523923
    const-class v1, LX/39o;

    monitor-enter v1

    .line 523924
    :try_start_0
    sget-object v0, LX/39o;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 523925
    sput-object v2, LX/39o;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523926
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523927
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 523928
    new-instance p0, LX/39o;

    invoke-static {v0}, LX/359;->a(LX/0QB;)LX/359;

    move-result-object v3

    check-cast v3, LX/359;

    invoke-direct {p0, v3}, LX/39o;-><init>(LX/359;)V

    .line 523929
    move-object v0, p0

    .line 523930
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 523931
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/39o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523932
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 523933
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;LX/1Wk;Ljava/lang/String;Ljava/lang/CharSequence;Landroid/util/SparseArray;Ljava/lang/CharSequence;IIFFFLX/1dc;I)LX/1Dg;
    .locals 6
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pr;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Wk;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p7    # Landroid/util/SparseArray;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p11    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p12    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p13    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p14    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p15    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "LX/1Wk;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/CharSequence;",
            "IIFFF",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;I)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 523934
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/39p;->a(LX/1De;)LX/39s;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/39s;->a(LX/1Wk;)LX/39s;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->c(LX/1n6;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p7}, LX/1Dh;->b(Landroid/util/SparseArray;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p6}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v2

    .line 523935
    if-eqz p14, :cond_0

    .line 523936
    move-object/from16 v0, p14

    invoke-static {p1, v0}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    .line 523937
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b009c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int v3, v3, p15

    .line 523938
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    .line 523939
    move-object/from16 v0, p14

    invoke-static {p1, v1, v0}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 523940
    iget-object v1, p0, LX/39o;->a:LX/359;

    invoke-virtual {v1, p1}, LX/359;->c(LX/1De;)LX/35B;

    move-result-object v1

    move-object/from16 v0, p14

    invoke-virtual {v1, v0}, LX/35B;->a(LX/1dc;)LX/35B;

    move-result-object v1

    const v5, 0x800005

    invoke-virtual {v1, v5}, LX/35B;->j(I)LX/35B;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/35B;->a(LX/1Pr;)LX/35B;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/35B;->b(Ljava/lang/String;)LX/35B;

    move-result-object v5

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, LX/0jW;

    invoke-virtual {v5, v1}, LX/35B;->a(LX/0jW;)LX/35B;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v5, 0x4

    neg-int v4, v4

    invoke-interface {v1, v5, v4}, LX/1Di;->a(II)LX/1Di;

    move-result-object v1

    const/4 v4, 0x5

    invoke-interface {v1, v4, v3}, LX/1Di;->a(II)LX/1Di;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, LX/1Di;->a(Z)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 523941
    :cond_0
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v3, 0x7f0b009d

    invoke-virtual {v1, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p9}, LX/1ne;->m(I)LX/1ne;

    move-result-object v1

    move/from16 v0, p10

    invoke-virtual {v1, v0}, LX/1ne;->k(I)LX/1ne;

    move-result-object v1

    move/from16 v0, p11

    invoke-virtual {v1, v0}, LX/1ne;->d(F)LX/1ne;

    move-result-object v1

    move/from16 v0, p12

    invoke-virtual {v1, v0}, LX/1ne;->e(F)LX/1ne;

    move-result-object v1

    move/from16 v0, p13

    invoke-virtual {v1, v0}, LX/1ne;->c(F)LX/1ne;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v3}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v3, 0x3

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1
.end method
