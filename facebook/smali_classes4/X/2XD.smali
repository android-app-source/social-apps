.class public final enum LX/2XD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2XD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2XD;

.field public static final enum LATEST_N_STORIES:LX/2XD;

.field public static final enum LATEST_N_STORIES_AFTER_A_CURSOR:LX/2XD;

.field public static final enum LATEST_N_STORIES_BEFORE_A_CURSOR:LX/2XD;

.field public static final enum N_STORIES_BETWEEN_CURSORS:LX/2XD;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 419562
    new-instance v0, LX/2XD;

    const-string v1, "LATEST_N_STORIES"

    invoke-direct {v0, v1, v2}, LX/2XD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2XD;->LATEST_N_STORIES:LX/2XD;

    .line 419563
    new-instance v0, LX/2XD;

    const-string v1, "LATEST_N_STORIES_BEFORE_A_CURSOR"

    invoke-direct {v0, v1, v3}, LX/2XD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2XD;->LATEST_N_STORIES_BEFORE_A_CURSOR:LX/2XD;

    .line 419564
    new-instance v0, LX/2XD;

    const-string v1, "LATEST_N_STORIES_AFTER_A_CURSOR"

    invoke-direct {v0, v1, v4}, LX/2XD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2XD;->LATEST_N_STORIES_AFTER_A_CURSOR:LX/2XD;

    .line 419565
    new-instance v0, LX/2XD;

    const-string v1, "N_STORIES_BETWEEN_CURSORS"

    invoke-direct {v0, v1, v5}, LX/2XD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2XD;->N_STORIES_BETWEEN_CURSORS:LX/2XD;

    .line 419566
    const/4 v0, 0x4

    new-array v0, v0, [LX/2XD;

    sget-object v1, LX/2XD;->LATEST_N_STORIES:LX/2XD;

    aput-object v1, v0, v2

    sget-object v1, LX/2XD;->LATEST_N_STORIES_BEFORE_A_CURSOR:LX/2XD;

    aput-object v1, v0, v3

    sget-object v1, LX/2XD;->LATEST_N_STORIES_AFTER_A_CURSOR:LX/2XD;

    aput-object v1, v0, v4

    sget-object v1, LX/2XD;->N_STORIES_BETWEEN_CURSORS:LX/2XD;

    aput-object v1, v0, v5

    sput-object v0, LX/2XD;->$VALUES:[LX/2XD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 419546
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getQueryType(Lcom/facebook/api/feed/FetchFeedParams;)LX/2XD;
    .locals 1

    .prologue
    .line 419547
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 419548
    if-eqz v0, :cond_0

    .line 419549
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 419550
    if-eqz v0, :cond_0

    .line 419551
    sget-object v0, LX/2XD;->N_STORIES_BETWEEN_CURSORS:LX/2XD;

    .line 419552
    :goto_0
    return-object v0

    .line 419553
    :cond_0
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 419554
    if-eqz v0, :cond_1

    .line 419555
    sget-object v0, LX/2XD;->LATEST_N_STORIES_AFTER_A_CURSOR:LX/2XD;

    goto :goto_0

    .line 419556
    :cond_1
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 419557
    if-eqz v0, :cond_2

    .line 419558
    sget-object v0, LX/2XD;->LATEST_N_STORIES_BEFORE_A_CURSOR:LX/2XD;

    goto :goto_0

    .line 419559
    :cond_2
    sget-object v0, LX/2XD;->LATEST_N_STORIES:LX/2XD;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/2XD;
    .locals 1

    .prologue
    .line 419560
    const-class v0, LX/2XD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2XD;

    return-object v0
.end method

.method public static values()[LX/2XD;
    .locals 1

    .prologue
    .line 419561
    sget-object v0, LX/2XD;->$VALUES:[LX/2XD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2XD;

    return-object v0
.end method
