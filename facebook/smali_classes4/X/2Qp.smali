.class public LX/2Qp;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/2Qp;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 408924
    const-string v0, "pending_app_calls"

    const/4 v1, 0x4

    new-instance v2, LX/2Qq;

    invoke-direct {v2}, LX/2Qq;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 408925
    return-void
.end method

.method public static a(LX/0QB;)LX/2Qp;
    .locals 3

    .prologue
    .line 408926
    sget-object v0, LX/2Qp;->a:LX/2Qp;

    if-nez v0, :cond_1

    .line 408927
    const-class v1, LX/2Qp;

    monitor-enter v1

    .line 408928
    :try_start_0
    sget-object v0, LX/2Qp;->a:LX/2Qp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 408929
    if-eqz v2, :cond_0

    .line 408930
    :try_start_1
    new-instance v0, LX/2Qp;

    invoke-direct {v0}, LX/2Qp;-><init>()V

    .line 408931
    move-object v0, v0

    .line 408932
    sput-object v0, LX/2Qp;->a:LX/2Qp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408933
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 408934
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 408935
    :cond_1
    sget-object v0, LX/2Qp;->a:LX/2Qp;

    return-object v0

    .line 408936
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 408937
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
