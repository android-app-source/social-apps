.class public LX/2ZJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2XA;
.implements LX/2ZK;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile j:LX/2ZJ;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2K5;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2K2;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:LX/0Uo;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Uh;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2K2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 422312
    const-class v0, LX/2ZJ;

    sput-object v0, LX/2ZJ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Uo;LX/0Or;LX/0Uh;LX/0Or;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/gk/internal/GkConfigurationFetchPeriodMillis;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/2K5;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2K2;",
            ">;>;",
            "LX/0Uo;",
            "LX/0Or",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStoreManager;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 422340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422341
    iput-object p1, p0, LX/2ZJ;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 422342
    iput-object p2, p0, LX/2ZJ;->c:LX/0Ot;

    .line 422343
    iput-object p3, p0, LX/2ZJ;->d:LX/0Ot;

    .line 422344
    iput-object p4, p0, LX/2ZJ;->e:LX/0Uo;

    .line 422345
    iput-object p5, p0, LX/2ZJ;->f:LX/0Or;

    .line 422346
    iput-object p6, p0, LX/2ZJ;->g:LX/0Uh;

    .line 422347
    iput-object p7, p0, LX/2ZJ;->h:LX/0Or;

    .line 422348
    return-void
.end method

.method public static a(LX/0QB;)LX/2ZJ;
    .locals 11

    .prologue
    .line 422324
    sget-object v0, LX/2ZJ;->j:LX/2ZJ;

    if-nez v0, :cond_1

    .line 422325
    const-class v1, LX/2ZJ;

    monitor-enter v1

    .line 422326
    :try_start_0
    sget-object v0, LX/2ZJ;->j:LX/2ZJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 422327
    if-eqz v2, :cond_0

    .line 422328
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 422329
    new-instance v3, LX/2ZJ;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v5, 0xaaf

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 422330
    new-instance v6, LX/2ZL;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    invoke-direct {v6, v7}, LX/2ZL;-><init>(LX/0QB;)V

    move-object v6, v6

    .line 422331
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    invoke-static {v6, v7}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v6

    move-object v6, v6

    .line 422332
    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v7

    check-cast v7, LX/0Uo;

    const/16 v8, 0x15dc

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    const/16 v10, 0x15e7

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/2ZJ;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Uo;LX/0Or;LX/0Uh;LX/0Or;)V

    .line 422333
    move-object v0, v3

    .line 422334
    sput-object v0, LX/2ZJ;->j:LX/2ZJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 422335
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 422336
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 422337
    :cond_1
    sget-object v0, LX/2ZJ;->j:LX/2ZJ;

    return-object v0

    .line 422338
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 422339
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized f(LX/2ZJ;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2K2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 422317
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2ZJ;->i:Ljava/util/List;

    if-nez v0, :cond_0

    .line 422318
    iget-object v0, p0, LX/2ZJ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 422319
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 422320
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/2ZJ;->i:Ljava/util/List;

    .line 422321
    :cond_0
    :goto_0
    iget-object v0, p0, LX/2ZJ;->i:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 422322
    :cond_1
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, LX/2ZJ;->i:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 422323
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 422313
    iget-object v0, p0, LX/2ZJ;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 422314
    if-nez v0, :cond_0

    .line 422315
    :goto_0
    return-void

    .line 422316
    :cond_0
    iget-object v1, p0, LX/2ZJ;->g:LX/0Uh;

    invoke-virtual {v1, v0}, LX/0Uh;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()LX/2ZE;
    .locals 2

    .prologue
    .line 422349
    new-instance v0, LX/2ZM;

    invoke-direct {v0, p0}, LX/2ZM;-><init>(LX/2ZJ;)V

    return-object v0
.end method

.method public final c()LX/2ZE;
    .locals 2

    .prologue
    .line 422296
    new-instance v0, LX/2ZM;

    invoke-direct {v0, p0}, LX/2ZM;-><init>(LX/2ZJ;)V

    return-object v0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 422297
    iget-object v0, p0, LX/2ZJ;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 422298
    if-nez v0, :cond_0

    .line 422299
    iget-object v0, p0, LX/2ZJ;->g:LX/0Uh;

    invoke-virtual {v0}, LX/0Uh;->c()V

    .line 422300
    :goto_0
    return-void

    .line 422301
    :cond_0
    iget-object v1, p0, LX/2ZJ;->g:LX/0Uh;

    .line 422302
    iget-object v2, v1, LX/0Uh;->e:LX/0Ug;

    if-eqz v2, :cond_1

    .line 422303
    iget-object v2, v1, LX/0Uh;->e:LX/0Ug;

    iget-object v3, v1, LX/0Uh;->h:LX/0Ui;

    .line 422304
    invoke-static {v2, v0}, LX/0Ug;->b(LX/0Ug;Ljava/lang/String;)LX/0Ua;

    move-result-object p0

    .line 422305
    if-eqz p0, :cond_1

    .line 422306
    invoke-virtual {p0, v3}, LX/0Ua;->b(LX/0Ui;)V

    .line 422307
    :cond_1
    invoke-virtual {v1}, LX/0Uh;->c()V

    .line 422308
    goto :goto_0
.end method

.method public final dq_()Z
    .locals 1

    .prologue
    .line 422309
    iget-object v0, p0, LX/2ZJ;->g:LX/0Uh;

    invoke-virtual {v0}, LX/0Uh;->d()Z

    move-result v0

    return v0
.end method

.method public final dr_()J
    .locals 2

    .prologue
    .line 422310
    iget-object v0, p0, LX/2ZJ;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()LX/2ZF;
    .locals 1

    .prologue
    .line 422311
    sget-object v0, LX/2ZF;->OTHER:LX/2ZF;

    return-object v0
.end method
