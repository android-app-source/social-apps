.class public LX/2Ul;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2Ul;


# instance fields
.field public final a:LX/2Uj;

.field private final b:LX/0TD;

.field public c:Ljava/net/DatagramSocket;

.field public d:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/net/SocketAddress;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Uj;LX/0TD;)V
    .locals 1
    .param p1    # LX/2Uj;
        .annotation runtime Lcom/facebook/messaging/media/upload/udp/UDPOutgoingPacketQueue;
        .end annotation
    .end param
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416533
    const-string v0, "udp_sending_task"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 416534
    iput-object p1, p0, LX/2Ul;->a:LX/2Uj;

    .line 416535
    iput-object p2, p0, LX/2Ul;->b:LX/0TD;

    .line 416536
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/2Ul;->d:Ljava/util/concurrent/atomic/AtomicReference;

    .line 416537
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ul;
    .locals 5

    .prologue
    .line 416516
    sget-object v0, LX/2Ul;->e:LX/2Ul;

    if-nez v0, :cond_1

    .line 416517
    const-class v1, LX/2Ul;

    monitor-enter v1

    .line 416518
    :try_start_0
    sget-object v0, LX/2Ul;->e:LX/2Ul;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416519
    if-eqz v2, :cond_0

    .line 416520
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416521
    new-instance p0, LX/2Ul;

    invoke-static {v0}, LX/2Um;->a(LX/0QB;)LX/2Uj;

    move-result-object v3

    check-cast v3, LX/2Uj;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-direct {p0, v3, v4}, LX/2Ul;-><init>(LX/2Uj;LX/0TD;)V

    .line 416522
    move-object v0, p0

    .line 416523
    sput-object v0, LX/2Ul;->e:LX/2Ul;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416524
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416525
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416526
    :cond_1
    sget-object v0, LX/2Ul;->e:LX/2Ul;

    return-object v0

    .line 416527
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416528
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 416531
    const-class v0, Lcom/facebook/messaging/background/annotations/MessagesLocalTaskTag;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 416532
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416530
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 416529
    iget-object v0, p0, LX/2Ul;->c:Ljava/net/DatagramSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Ul;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Ul;->a:LX/2Uj;

    invoke-virtual {v0}, LX/2Uj;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416515
    iget-object v0, p0, LX/2Ul;->b:LX/0TD;

    new-instance v1, LX/FIj;

    invoke-direct {v1, p0}, LX/FIj;-><init>(LX/2Ul;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
