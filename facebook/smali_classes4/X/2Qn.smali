.class public final LX/2Qn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0sv;


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;Ljava/lang/String;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 408893
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/2Qn;-><init>(LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V

    .line 408894
    return-void
.end method

.method public constructor <init>(LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 408895
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408896
    iput-object p1, p0, LX/2Qn;->a:LX/0Px;

    .line 408897
    iput-object p2, p0, LX/2Qn;->b:Ljava/lang/String;

    .line 408898
    iput-object p3, p0, LX/2Qn;->c:LX/0Px;

    .line 408899
    iput-object p4, p0, LX/2Qn;->d:Ljava/lang/String;

    .line 408900
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 408901
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 408902
    iget-object v1, p0, LX/2Qn;->a:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Qn;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/2Qn;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Qn;->c:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Qn;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 408903
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "All fields for foreign key must be specified"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 408904
    :cond_1
    const-string v1, "FOREIGN KEY ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408905
    const-string v1, ", "

    invoke-static {v1}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v1

    iget-object v2, p0, LX/2Qn;->a:LX/0Px;

    sget-object v3, LX/0U1;->c:LX/0QK;

    invoke-static {v2, v3}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408906
    const-string v1, ") REFERENCES "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408907
    iget-object v1, p0, LX/2Qn;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408908
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408909
    const-string v1, ", "

    invoke-static {v1}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v1

    iget-object v2, p0, LX/2Qn;->c:LX/0Px;

    sget-object v3, LX/0U1;->c:LX/0QK;

    invoke-static {v2, v3}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408910
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408911
    iget-object v1, p0, LX/2Qn;->d:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 408912
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/2Qn;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408913
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
