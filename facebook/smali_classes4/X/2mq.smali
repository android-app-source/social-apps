.class public LX/2mq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2mq;


# instance fields
.field private final a:F

.field public final b:Ljava/lang/String;

.field public final c:Z


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 462114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462115
    sget v0, LX/1rJ;->L:F

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/2mq;->a:F

    .line 462116
    sget-char v0, LX/1rJ;->M:C

    const-string v1, "center"

    invoke-interface {p1, v0, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2mq;->b:Ljava/lang/String;

    .line 462117
    sget-short v0, LX/1rJ;->N:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2mq;->c:Z

    .line 462118
    return-void
.end method

.method public static a(LX/0QB;)LX/2mq;
    .locals 4

    .prologue
    .line 462119
    sget-object v0, LX/2mq;->d:LX/2mq;

    if-nez v0, :cond_1

    .line 462120
    const-class v1, LX/2mq;

    monitor-enter v1

    .line 462121
    :try_start_0
    sget-object v0, LX/2mq;->d:LX/2mq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 462122
    if-eqz v2, :cond_0

    .line 462123
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 462124
    new-instance p0, LX/2mq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/2mq;-><init>(LX/0ad;)V

    .line 462125
    move-object v0, p0

    .line 462126
    sput-object v0, LX/2mq;->d:LX/2mq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 462127
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 462128
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 462129
    :cond_1
    sget-object v0, LX/2mq;->d:LX/2mq;

    return-object v0

    .line 462130
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 462131
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 4

    .prologue
    .line 462132
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCoverImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v1

    .line 462133
    if-eqz v1, :cond_0

    .line 462134
    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    sget-object v2, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v2}, LX/1af;->a(LX/1Up;)V

    .line 462135
    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 462136
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setNeedCentering(Z)V

    .line 462137
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 2

    .prologue
    .line 462138
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 462139
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/2v7;->n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 462140
    :cond_0
    :goto_0
    return v0

    .line 462141
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 462142
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, LX/2mq;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 462143
    iget v2, p0, LX/2mq;->a:F

    cmpl-float v0, v2, v0

    if-nez v0, :cond_2

    .line 462144
    iget v0, p0, LX/2mq;->a:F

    goto :goto_0

    .line 462145
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 462146
    iget v1, p0, LX/2mq;->a:F

    cmpg-float v1, v1, v0

    if-ltz v1, :cond_0

    iget v0, p0, LX/2mq;->a:F

    goto :goto_0
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 3

    .prologue
    .line 462147
    if-eqz p2, :cond_1

    invoke-static {p2}, LX/2v7;->n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 462148
    invoke-static {v0}, LX/2mq;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/2mq;->b:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/2mq;->b:Ljava/lang/String;

    const-string v2, "top"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, LX/2mq;->c:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 462149
    if-eqz v0, :cond_1

    invoke-static {p2}, LX/1VO;->g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 462150
    :cond_1
    :goto_1
    return-void

    .line 462151
    :cond_2
    invoke-static {p1}, LX/2mq;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
