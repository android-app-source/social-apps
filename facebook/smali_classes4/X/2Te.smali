.class public final LX/2Te;
.super LX/1M3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1M3",
        "<TK;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Tb;


# direct methods
.method public constructor <init>(LX/2Tb;)V
    .locals 0

    .prologue
    .line 414553
    iput-object p1, p0, LX/2Te;->a:LX/2Tb;

    invoke-direct {p0}, LX/1M3;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/1M1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1M1",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 414552
    iget-object v0, p0, LX/2Te;->a:LX/2Tb;

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 414535
    instance-of v0, p1, LX/4wx;

    if-eqz v0, :cond_1

    .line 414536
    check-cast p1, LX/4wx;

    .line 414537
    iget-object v0, p0, LX/2Te;->a:LX/2Tb;

    iget-object v0, v0, LX/2Tb;->b:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 414538
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1}, LX/4wx;->b()I

    move-result v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 414539
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 414540
    goto :goto_0

    :cond_1
    move v0, v1

    .line 414541
    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 414551
    iget-object v0, p0, LX/2Te;->a:LX/2Tb;

    iget-object v0, v0, LX/2Tb;->b:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->n()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TK;>;>;"
        }
    .end annotation

    .prologue
    .line 414550
    iget-object v0, p0, LX/2Te;->a:LX/2Tb;

    invoke-virtual {v0}, LX/2Tb;->b()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 414543
    instance-of v0, p1, LX/4wx;

    if-eqz v0, :cond_0

    .line 414544
    check-cast p1, LX/4wx;

    .line 414545
    iget-object v0, p0, LX/2Te;->a:LX/2Tb;

    iget-object v0, v0, LX/2Tb;->b:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 414546
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {p1}, LX/4wx;->b()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 414547
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 414548
    const/4 v0, 0x1

    .line 414549
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 414542
    iget-object v0, p0, LX/2Te;->a:LX/2Tb;

    invoke-virtual {v0}, LX/2Tb;->c()I

    move-result v0

    return v0
.end method
