.class public LX/2nU;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 464643
    const-wide/16 v0, 0x0

    sput-wide v0, LX/2nU;->a:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 464642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 464641
    const-string v0, "%016x%08x"

    invoke-static {p0, p1}, LX/2nU;->b(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const v2, 0x3fffffff    # 1.9999999f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 464634
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v3, 0x18

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 464635
    if-lez p1, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 464636
    invoke-static {p0}, LX/2nU;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 464637
    invoke-static {p0}, LX/2nU;->e(Ljava/lang/String;)I

    move-result v1

    .line 464638
    const-string v2, "%s%08x"

    sub-int/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 464639
    goto :goto_0

    :cond_1
    move v1, v2

    .line 464640
    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 464630
    :try_start_0
    invoke-static {p0}, LX/2nU;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 464631
    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    .line 464632
    invoke-static {v0, v1}, LX/2nU;->b(J)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 464633
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private static declared-synchronized b(J)J
    .locals 6

    .prologue
    .line 464624
    const-class v1, LX/2nU;

    monitor-enter v1

    :try_start_0
    sget-wide v2, LX/2nU;->a:J

    cmp-long v0, p0, v2

    if-lez v0, :cond_0

    .line 464625
    sput-wide p0, LX/2nU;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464626
    :goto_0
    monitor-exit v1

    return-wide p0

    .line 464627
    :cond_0
    :try_start_1
    sget-wide v2, LX/2nU;->a:J

    const-wide/16 v4, 0x1

    add-long p0, v2, v4

    .line 464628
    sput-wide p0, LX/2nU;->a:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 464629
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 464623
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/2nU;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x18

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 464616
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 464617
    if-ltz p1, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 464618
    const-string v0, "%s%08x"

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7fffffff

    sub-int/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 464619
    goto :goto_0

    :cond_1
    move v1, v2

    .line 464620
    goto :goto_1
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 464622
    const/4 v0, 0x0

    const/16 v1, 0x10

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/String;)I
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 464621
    const/16 v0, 0x18

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
