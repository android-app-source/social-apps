.class public LX/2qr;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field public final a:I

.field public final b:Landroid/view/View;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 471467
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 471468
    iput-object p1, p0, LX/2qr;->b:Landroid/view/View;

    .line 471469
    iput p2, p0, LX/2qr;->a:I

    .line 471470
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 471479
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/2qr;->applyTransformation(FLandroid/view/animation/Transformation;)V

    .line 471480
    return-void
.end method

.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 471475
    iget v0, p0, LX/2qr;->c:I

    int-to-float v0, v0

    iget v1, p0, LX/2qr;->a:I

    iget v2, p0, LX/2qr;->c:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 471476
    iget-object v1, p0, LX/2qr;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 471477
    iget-object v0, p0, LX/2qr;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 471478
    return-void
.end method

.method public final initialize(IIII)V
    .locals 0

    .prologue
    .line 471472
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/animation/Animation;->initialize(IIII)V

    .line 471473
    iput p2, p0, LX/2qr;->c:I

    .line 471474
    return-void
.end method

.method public final willChangeBounds()Z
    .locals 1

    .prologue
    .line 471471
    const/4 v0, 0x1

    return v0
.end method
