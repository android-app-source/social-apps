.class public LX/2a5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1fT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttPushHandler__INJECTED_BY_TemplateInjector:LX/2a5;


# instance fields
.field private final mOmnistoreMqttJniHandler:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 423351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 423352
    iput-object p1, p0, LX/2a5;->mOmnistoreMqttJniHandler:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    .line 423353
    return-void
.end method

.method public static getInstance__com_facebook_omnistore_mqtt_OmnistoreMqttPushHandler__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2a5;
    .locals 4

    .prologue
    .line 423354
    sget-object v0, LX/2a5;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttPushHandler__INJECTED_BY_TemplateInjector:LX/2a5;

    if-nez v0, :cond_1

    .line 423355
    const-class v1, LX/2a5;

    monitor-enter v1

    .line 423356
    :try_start_0
    sget-object v0, LX/2a5;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttPushHandler__INJECTED_BY_TemplateInjector:LX/2a5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 423357
    if-eqz v2, :cond_0

    .line 423358
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 423359
    new-instance p0, LX/2a5;

    invoke-static {v0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->getInstance__com_facebook_omnistore_mqtt_OmnistoreMqttJniHandler__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    move-result-object v3

    check-cast v3, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    invoke-direct {p0, v3}, LX/2a5;-><init>(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;)V

    .line 423360
    move-object v0, p0

    .line 423361
    sput-object v0, LX/2a5;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttPushHandler__INJECTED_BY_TemplateInjector:LX/2a5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423362
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 423363
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 423364
    :cond_1
    sget-object v0, LX/2a5;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttPushHandler__INJECTED_BY_TemplateInjector:LX/2a5;

    return-object v0

    .line 423365
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 423366
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public getHandlerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 423367
    const-string v0, "OmnistoreMqttPushHandler"

    return-object v0
.end method

.method public onMessage(Ljava/lang/String;[BJ)V
    .locals 1

    .prologue
    .line 423368
    const-string v0, "/t_omnistore_sync"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/t_omnistore_sync_low_pri"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 423369
    :goto_0
    return-void

    .line 423370
    :cond_0
    iget-object v0, p0, LX/2a5;->mOmnistoreMqttJniHandler:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    invoke-virtual {v0, p2}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->handleOmnistoreSyncMessage([B)V

    goto :goto_0
.end method
