.class public LX/37H;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/37J;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/37K;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 500909
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/37H;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/37K;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 500906
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 500907
    iput-object p1, p0, LX/37H;->b:LX/0Ot;

    .line 500908
    return-void
.end method

.method public static a(LX/0QB;)LX/37H;
    .locals 4

    .prologue
    .line 500895
    const-class v1, LX/37H;

    monitor-enter v1

    .line 500896
    :try_start_0
    sget-object v0, LX/37H;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 500897
    sput-object v2, LX/37H;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 500898
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500899
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 500900
    new-instance v3, LX/37H;

    const/16 p0, 0x5e7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/37H;-><init>(LX/0Ot;)V

    .line 500901
    move-object v0, v3

    .line 500902
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 500903
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/37H;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 500904
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 500905
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 500881
    check-cast p2, LX/37I;

    .line 500882
    iget-object v0, p0, LX/37H;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget v0, p2, LX/37I;->a:I

    .line 500883
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b1083

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f081a3c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v3, p0

    invoke-virtual {p1, v2, v3}, LX/1De;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    sget-object v2, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v1, v2}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0a05d8

    invoke-interface {v1, v2}, LX/1Di;->x(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 500884
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 500893
    invoke-static {}, LX/1dS;->b()V

    .line 500894
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/37J;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 500885
    new-instance v1, LX/37I;

    invoke-direct {v1, p0}, LX/37I;-><init>(LX/37H;)V

    .line 500886
    sget-object v2, LX/37H;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/37J;

    .line 500887
    if-nez v2, :cond_0

    .line 500888
    new-instance v2, LX/37J;

    invoke-direct {v2}, LX/37J;-><init>()V

    .line 500889
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/37J;->a$redex0(LX/37J;LX/1De;IILX/37I;)V

    .line 500890
    move-object v1, v2

    .line 500891
    move-object v0, v1

    .line 500892
    return-object v0
.end method
