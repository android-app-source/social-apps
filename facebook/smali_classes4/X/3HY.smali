.class public final enum LX/3HY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3HY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3HY;

.field public static final enum EXTRA_SMALL:LX/3HY;

.field public static final enum REGULAR:LX/3HY;

.field public static final enum SMALL:LX/3HY;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 543678
    new-instance v0, LX/3HY;

    const-string v1, "REGULAR"

    invoke-direct {v0, v1, v2}, LX/3HY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3HY;->REGULAR:LX/3HY;

    .line 543679
    new-instance v0, LX/3HY;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v3}, LX/3HY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3HY;->SMALL:LX/3HY;

    .line 543680
    new-instance v0, LX/3HY;

    const-string v1, "EXTRA_SMALL"

    invoke-direct {v0, v1, v4}, LX/3HY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3HY;->EXTRA_SMALL:LX/3HY;

    .line 543681
    const/4 v0, 0x3

    new-array v0, v0, [LX/3HY;

    sget-object v1, LX/3HY;->REGULAR:LX/3HY;

    aput-object v1, v0, v2

    sget-object v1, LX/3HY;->SMALL:LX/3HY;

    aput-object v1, v0, v3

    sget-object v1, LX/3HY;->EXTRA_SMALL:LX/3HY;

    aput-object v1, v0, v4

    sput-object v0, LX/3HY;->$VALUES:[LX/3HY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 543682
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3HY;
    .locals 1

    .prologue
    .line 543683
    const-class v0, LX/3HY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3HY;

    return-object v0
.end method

.method public static values()[LX/3HY;
    .locals 1

    .prologue
    .line 543684
    sget-object v0, LX/3HY;->$VALUES:[LX/3HY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3HY;

    return-object v0
.end method
