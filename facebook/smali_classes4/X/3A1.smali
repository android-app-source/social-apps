.class public LX/3A1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0ka;

.field public final c:LX/3A2;

.field private final d:LX/0So;

.field private final e:LX/3A3;

.field public final f:LX/79P;

.field private final g:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Or;LX/0ka;LX/3A2;LX/0So;LX/3A3;LX/79P;LX/0ad;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/rtcpresence/annotations/IsVoipEnabledForUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0ka;",
            "LX/3A2;",
            "LX/0So;",
            "LX/3A3;",
            "LX/79P;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 524614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524615
    iput-object p1, p0, LX/3A1;->a:LX/0Or;

    .line 524616
    iput-object p2, p0, LX/3A1;->b:LX/0ka;

    .line 524617
    iput-object p3, p0, LX/3A1;->c:LX/3A2;

    .line 524618
    iput-object p4, p0, LX/3A1;->d:LX/0So;

    .line 524619
    iput-object p5, p0, LX/3A1;->e:LX/3A3;

    .line 524620
    iput-object p6, p0, LX/3A1;->f:LX/79P;

    .line 524621
    iput-object p7, p0, LX/3A1;->g:LX/0ad;

    .line 524622
    return-void
.end method

.method public static a(LX/0QB;)LX/3A1;
    .locals 1

    .prologue
    .line 524623
    invoke-static {p0}, LX/3A1;->b(LX/0QB;)LX/3A1;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3A1;
    .locals 8

    .prologue
    .line 524624
    new-instance v0, LX/3A1;

    const/16 v1, 0x1568

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v2

    check-cast v2, LX/0ka;

    invoke-static {p0}, LX/3A2;->a(LX/0QB;)LX/3A2;

    move-result-object v3

    check-cast v3, LX/3A2;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {p0}, LX/3A3;->a(LX/0QB;)LX/3A3;

    move-result-object v5

    check-cast v5, LX/3A3;

    .line 524625
    const/4 v6, 0x0

    move-object v6, v6

    .line 524626
    move-object v6, v6

    .line 524627
    check-cast v6, LX/79P;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-direct/range {v0 .. v7}, LX/3A1;-><init>(LX/0Or;LX/0ka;LX/3A2;LX/0So;LX/3A3;LX/79P;LX/0ad;)V

    .line 524628
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/UserKey;)LX/79S;
    .locals 7

    .prologue
    .line 524629
    const-wide/16 v5, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 524630
    iget-object v1, p0, LX/3A1;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v1, v1

    .line 524631
    if-nez v1, :cond_1

    .line 524632
    new-instance v1, LX/79S;

    sget-object v4, LX/79O;->j:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, LX/79S;-><init>(ZLjava/lang/String;Ljava/lang/String;J)V

    .line 524633
    :cond_0
    :goto_0
    move-object v0, v1

    .line 524634
    return-object v0

    .line 524635
    :cond_1
    iget-object v1, p0, LX/3A1;->b:LX/0ka;

    invoke-virtual {v1}, LX/0ka;->c()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 524636
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 524637
    if-nez v1, :cond_2

    .line 524638
    new-instance v1, LX/79S;

    sget-object v4, LX/79O;->f:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, LX/79S;-><init>(ZLjava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    .line 524639
    :cond_2
    if-nez p1, :cond_3

    .line 524640
    new-instance v1, LX/79S;

    sget-object v4, LX/79O;->d:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, LX/79S;-><init>(ZLjava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    .line 524641
    :cond_3
    iget-object v1, p0, LX/3A1;->c:LX/3A2;

    invoke-virtual {v1, p1}, LX/3A2;->a(Lcom/facebook/user/model/UserKey;)LX/79S;

    move-result-object v1

    .line 524642
    if-nez v1, :cond_0

    .line 524643
    iget-object v1, p0, LX/3A1;->f:LX/79P;

    if-nez v1, :cond_5

    .line 524644
    const/4 v1, 0x0

    .line 524645
    :goto_2
    move-object v1, v1

    .line 524646
    if-nez v1, :cond_0

    .line 524647
    new-instance v1, LX/79S;

    sget-object v4, LX/79O;->c:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, LX/79S;-><init>(ZLjava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    iget-object v1, p0, LX/3A1;->f:LX/79P;

    invoke-interface {v1}, LX/79P;->a()LX/79S;

    move-result-object v1

    goto :goto_2
.end method

.method public final a(Lcom/facebook/user/model/UserKey;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 524648
    iget-object v6, p0, LX/3A1;->c:LX/3A2;

    new-instance v0, LX/79S;

    iget-object v1, p0, LX/3A1;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, LX/79S;-><init>(ZLjava/lang/String;Ljava/lang/String;J)V

    invoke-virtual {v6, p1, v0}, LX/3A2;->a(Lcom/facebook/user/model/UserKey;LX/79S;)V

    .line 524649
    return-void
.end method
