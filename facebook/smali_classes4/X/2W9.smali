.class public LX/2W9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 418585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/File;LX/32Z;)V
    .locals 5

    .prologue
    .line 418565
    invoke-interface {p1, p0}, LX/32Z;->a(Ljava/io/File;)V

    .line 418566
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 418567
    if-eqz v1, :cond_1

    .line 418568
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 418569
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 418570
    invoke-static {v3, p1}, LX/2W9;->a(Ljava/io/File;LX/32Z;)V

    .line 418571
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 418572
    :cond_0
    invoke-interface {p1, v3}, LX/32Z;->b(Ljava/io/File;)V

    goto :goto_1

    .line 418573
    :cond_1
    invoke-interface {p1, p0}, LX/32Z;->c(Ljava/io/File;)V

    .line 418574
    return-void
.end method

.method public static a(Ljava/io/File;)Z
    .locals 5

    .prologue
    .line 418578
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 418579
    const/4 v0, 0x1

    .line 418580
    if-eqz v3, :cond_0

    .line 418581
    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v2, v3, v1

    .line 418582
    invoke-static {v2}, LX/2W9;->b(Ljava/io/File;)Z

    move-result v2

    and-int/2addr v2, v0

    .line 418583
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 418584
    :cond_0
    return v0
.end method

.method public static b(Ljava/io/File;)Z
    .locals 1

    .prologue
    .line 418575
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418576
    invoke-static {p0}, LX/2W9;->a(Ljava/io/File;)Z

    .line 418577
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    return v0
.end method
