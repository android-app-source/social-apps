.class public LX/2Af;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;


# instance fields
.field private final b:LX/12x;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 377456
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "stop_install_notifier"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Af;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/12x;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 377469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 377470
    iput-object p1, p0, LX/2Af;->b:LX/12x;

    .line 377471
    return-void
.end method

.method public static b(LX/0QB;)LX/2Af;
    .locals 2

    .prologue
    .line 377467
    new-instance v1, LX/2Af;

    invoke-static {p0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v0

    check-cast v0, LX/12x;

    invoke-direct {v1, v0}, LX/2Af;-><init>(LX/12x;)V

    .line 377468
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;II)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 377457
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/installnotifier/InstallNotifierReceiver;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 377458
    const-string v1, "InstallNotifier"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 377459
    const-string v1, "notif_cnt"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 377460
    const-string v1, "interval"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 377461
    const/high16 v1, 0x8000000

    invoke-static {p1, v6, v0, v1}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 377462
    iget-object v1, p0, LX/2Af;->b:LX/12x;

    .line 377463
    sget-object v2, LX/0SF;->a:LX/0SF;

    move-object v2, v2

    .line 377464
    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    int-to-long v4, p2

    add-long/2addr v2, v4

    .line 377465
    invoke-virtual {v1, v6, v2, v3, v0}, LX/12x;->c(IJLandroid/app/PendingIntent;)V

    .line 377466
    return-void
.end method
