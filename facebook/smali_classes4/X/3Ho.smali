.class public final LX/3Ho;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ow;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;)V
    .locals 0

    .prologue
    .line 544981
    iput-object p1, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 544923
    const-class v0, LX/2ow;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 12

    .prologue
    .line 544924
    check-cast p1, LX/2ow;

    .line 544925
    sget-object v0, LX/D6z;->c:[I

    iget-object v1, p1, LX/2ow;->a:LX/2oN;

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 544926
    :cond_0
    :goto_0
    return-void

    .line 544927
    :pswitch_0
    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    .line 544928
    invoke-virtual {v0}, LX/3Ga;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 544929
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->v:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 544930
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v1, :cond_1

    .line 544931
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 544932
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 544933
    :cond_1
    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-boolean v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->C:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->A:LX/D7C;

    if-eqz v0, :cond_0

    .line 544934
    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->A:LX/D7C;

    .line 544935
    iget-object v1, v0, LX/D7C;->c:LX/D7B;

    if-eqz v1, :cond_2

    .line 544936
    iget-object v1, v0, LX/D7C;->c:LX/D7B;

    invoke-virtual {v1}, LX/D7B;->cancel()V

    .line 544937
    const/4 v1, 0x0

    iput-object v1, v0, LX/D7C;->c:LX/D7B;

    .line 544938
    :cond_2
    goto :goto_0

    .line 544939
    :pswitch_1
    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->v:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 544940
    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->v:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 544941
    :cond_3
    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 544942
    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto :goto_0

    .line 544943
    :pswitch_2
    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v1, p1, LX/2ow;->c:LX/7M6;

    iget-object v1, v1, LX/7M6;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x0

    .line 544944
    invoke-virtual {v0}, LX/3Ga;->g()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 544945
    sget-object v4, LX/3H0;->NONE:LX/3H0;

    .line 544946
    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->B:LX/D6v;

    if-eqz v6, :cond_4

    .line 544947
    iget-object v4, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->B:LX/D6v;

    .line 544948
    iget-object v6, v4, LX/D6v;->w:LX/3H0;

    move-object v4, v6

    .line 544949
    :cond_4
    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->x:Landroid/widget/TextView;

    if-eqz v6, :cond_5

    .line 544950
    iget-object v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->x:Landroid/widget/TextView;

    sget-object v7, LX/3H0;->NONLIVE_POST_ROLL:LX/3H0;

    if-ne v4, v7, :cond_9

    const/16 v4, 0x8

    :goto_1
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 544951
    :cond_5
    iget-object v4, v0, LX/2oy;->j:LX/2pb;

    if-eqz v4, :cond_a

    iget-object v4, v0, LX/2oy;->j:LX/2pb;

    .line 544952
    iget-object v6, v4, LX/2pb;->D:LX/04G;

    move-object v4, v6

    .line 544953
    :goto_2
    if-eqz v4, :cond_6

    .line 544954
    sget-object v6, LX/D6z;->a:[I

    invoke-virtual {v4}, LX/04G;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_1

    .line 544955
    :cond_6
    :goto_3
    iget-object v4, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->v:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 544956
    iget-object v4, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->y:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v4, :cond_7

    if-eqz v1, :cond_7

    .line 544957
    invoke-static {v0, v1}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->b(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 544958
    :cond_7
    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-boolean v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->C:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->A:LX/D7C;

    if-eqz v0, :cond_0

    .line 544959
    iget-object v0, p0, LX/3Ho;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->A:LX/D7C;

    iget-object v1, p1, LX/2ow;->c:LX/7M6;

    iget-object v1, v1, LX/7M6;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p1, LX/2ow;->c:LX/7M6;

    iget-wide v2, v2, LX/7M6;->a:J

    .line 544960
    iget-object v4, v0, LX/D7C;->c:LX/D7B;

    if-eqz v4, :cond_8

    .line 544961
    iget-object v4, v0, LX/D7C;->c:LX/D7B;

    invoke-virtual {v4}, LX/D7B;->cancel()V

    .line 544962
    :cond_8
    invoke-static {v1}, LX/3HR;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v11

    .line 544963
    new-instance v4, LX/D7B;

    int-to-long v6, v11

    const/4 v10, 0x0

    move-object v5, v0

    move-wide v8, v2

    invoke-direct/range {v4 .. v10}, LX/D7B;-><init>(LX/D7C;JJB)V

    iput-object v4, v0, LX/D7C;->c:LX/D7B;

    .line 544964
    iget-object v4, v0, LX/D7C;->c:LX/D7B;

    invoke-virtual {v4}, LX/D7B;->start()Landroid/os/CountDownTimer;

    .line 544965
    iget-object v4, v0, LX/D7C;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v11}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 544966
    goto/16 :goto_0

    :cond_9
    move v4, v5

    .line 544967
    goto :goto_1

    .line 544968
    :cond_a
    const/4 v4, 0x0

    goto :goto_2

    .line 544969
    :pswitch_3
    iget-object v4, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->w:Lcom/facebook/video/commercialbreak/plugins/CommercialBreakVideoContainerFrameLayout;

    .line 544970
    iput-boolean v5, v4, Lcom/facebook/video/commercialbreak/plugins/CommercialBreakVideoContainerFrameLayout;->a:Z

    .line 544971
    iget-object v4, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->x:Landroid/widget/TextView;

    if-eqz v4, :cond_6

    .line 544972
    iget-object v4, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->x:Landroid/widget/TextView;

    const/4 v6, 0x3

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_3

    .line 544973
    :pswitch_4
    iget-wide v6, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->E:D

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v6, v8

    if-lez v4, :cond_b

    .line 544974
    iget-object v4, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->w:Lcom/facebook/video/commercialbreak/plugins/CommercialBreakVideoContainerFrameLayout;

    .line 544975
    iput-boolean v5, v4, Lcom/facebook/video/commercialbreak/plugins/CommercialBreakVideoContainerFrameLayout;->a:Z

    .line 544976
    :goto_4
    iget-object v4, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->x:Landroid/widget/TextView;

    if-eqz v4, :cond_6

    .line 544977
    iget-object v4, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->x:Landroid/widget/TextView;

    const/16 v6, 0x11

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_3

    .line 544978
    :cond_b
    iget-object v4, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;->w:Lcom/facebook/video/commercialbreak/plugins/CommercialBreakVideoContainerFrameLayout;

    const/4 v6, 0x1

    .line 544979
    iput-boolean v6, v4, Lcom/facebook/video/commercialbreak/plugins/CommercialBreakVideoContainerFrameLayout;->a:Z

    .line 544980
    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
