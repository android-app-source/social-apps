.class public final LX/2HX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;

.field private static volatile d:LX/2HX;


# instance fields
.field public b:Landroid/content/Context;
    .annotation build Lcom/facebook/inject/ForAppContext;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 390611
    const-class v0, LX/2HX;

    sput-object v0, LX/2HX;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 390607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390608
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 390609
    iput-object v0, p0, LX/2HX;->c:LX/0Ot;

    .line 390610
    return-void
.end method

.method public static a(LX/2HX;Ljava/lang/Throwable;)LX/0lF;
    .locals 8

    .prologue
    .line 390589
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 390590
    const-string v0, "excls"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 390591
    const-string v0, "msg"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 390592
    invoke-virtual {p1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    .line 390593
    new-instance v3, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/162;-><init>(LX/0mC;)V

    .line 390594
    const/4 v0, 0x0

    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_0

    .line 390595
    aget-object v4, v2, v0

    .line 390596
    new-instance v5, LX/0m9;

    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v6}, LX/0m9;-><init>(LX/0mC;)V

    .line 390597
    const-string v6, "cls"

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 390598
    const-string v6, "method"

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 390599
    const-string v6, "ln"

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v4

    invoke-virtual {v5, v6, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 390600
    invoke-virtual {v3, v5}, LX/162;->a(LX/0lF;)LX/162;

    .line 390601
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 390602
    :cond_0
    const-string v0, "stack"

    invoke-virtual {v1, v0, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 390603
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 390604
    if-eqz v0, :cond_1

    .line 390605
    const-string v2, "cause"

    invoke-static {p0, v0}, LX/2HX;->a(LX/2HX;Ljava/lang/Throwable;)LX/0lF;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 390606
    :cond_1
    return-object v1
.end method

.method public static a(LX/0QB;)LX/2HX;
    .locals 5

    .prologue
    .line 390516
    sget-object v0, LX/2HX;->d:LX/2HX;

    if-nez v0, :cond_1

    .line 390517
    const-class v1, LX/2HX;

    monitor-enter v1

    .line 390518
    :try_start_0
    sget-object v0, LX/2HX;->d:LX/2HX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 390519
    if-eqz v2, :cond_0

    .line 390520
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 390521
    new-instance v4, LX/2HX;

    invoke-direct {v4}, LX/2HX;-><init>()V

    .line 390522
    const-class v3, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 p0, 0xbc

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 390523
    iput-object v3, v4, LX/2HX;->b:Landroid/content/Context;

    iput-object p0, v4, LX/2HX;->c:LX/0Ot;

    .line 390524
    move-object v0, v4

    .line 390525
    sput-object v0, LX/2HX;->d:LX/2HX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390526
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 390527
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390528
    :cond_1
    sget-object v0, LX/2HX;->d:LX/2HX;

    return-object v0

    .line 390529
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 390530
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/08a;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 390567
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "main_dex_store_optimization_complete"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 390568
    const-string v2, "success"

    iget v0, p1, LX/08a;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390569
    const-string v0, "nrOptimizationsAttempted"

    iget v2, p1, LX/08a;->nrOptimizationsAttempted:I

    invoke-virtual {v3, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390570
    const-string v0, "nrOptimizationsFailed"

    iget v2, p1, LX/08a;->nrOptimizationsFailed:I

    invoke-virtual {v3, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move v0, v1

    .line 390571
    :goto_1
    const/4 v2, 0x4

    if-ge v0, v2, :cond_1

    .line 390572
    invoke-static {v0}, LX/08a;->getCounterName(I)Ljava/lang/String;

    move-result-object v2

    .line 390573
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_LAST_ATTEMPT"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 390574
    iget-object v5, p1, LX/08a;->counters:[J

    aget-wide v6, v5, v0

    invoke-virtual {v3, v2, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390575
    iget-object v2, p1, LX/08a;->lastAttemptCounters:[J

    aget-wide v6, v2, v0

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390576
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 390577
    goto :goto_0

    .line 390578
    :cond_1
    const-string v0, ""

    iget-object v2, p1, LX/08a;->lastFailureExceptionJson:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 390579
    :try_start_0
    new-instance v0, LX/1BC;

    invoke-direct {v0}, LX/1BC;-><init>()V

    .line 390580
    iget-object v2, p1, LX/08a;->lastFailureExceptionJson:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 390581
    invoke-virtual {v0}, LX/15w;->J()LX/0lG;

    move-result-object v0

    check-cast v0, LX/0lF;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 390582
    :goto_2
    const-string v1, "lastFailureExceptionJson"

    invoke-virtual {v3, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390583
    :cond_2
    iget-object v0, p0, LX/2HX;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 390584
    return-void

    .line 390585
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 390586
    sget-object v0, LX/2HX;->a:Ljava/lang/Class;

    const-string v4, "failure to decode exception JSON!!!!"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v4, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 390587
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    move-object v0, v1

    .line 390588
    check-cast v0, LX/0m9;

    const-string v4, "error"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "error reading error JSON: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final init()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 390531
    invoke-static {}, LX/008;->getMainDexStoreLoadInformation()LX/02X;

    move-result-object v0

    .line 390532
    if-nez v0, :cond_1

    .line 390533
    sget-object v0, LX/2HX;->a:Ljava/lang/Class;

    const-string v1, "missing dex load information!"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 390534
    :cond_0
    :goto_0
    return-void

    .line 390535
    :cond_1
    iget v1, v0, LX/02X;->loadResult:I

    .line 390536
    iget v2, v0, LX/02X;->loadResult:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_9

    .line 390537
    const/4 v5, 0x1

    .line 390538
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "main_dex_store_regen"

    invoke-direct {v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 390539
    iget v2, v0, LX/02X;->loadResult:I

    .line 390540
    and-int/lit8 v4, v2, 0x10

    if-eqz v4, :cond_2

    .line 390541
    const-string v4, "LOAD_RESULT_RECOVERED_FROM_CORRUPTION"

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390542
    :cond_2
    and-int/lit8 v4, v2, 0x40

    if-eqz v4, :cond_3

    .line 390543
    const-string v4, "LOAD_RESULT_RECOVERED_FROM_BAD_GEN"

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390544
    :cond_3
    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_4

    .line 390545
    const-string v2, "LOAD_RESULT_REGEN_FORCED"

    invoke-virtual {v3, v2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390546
    :cond_4
    iget-object v2, v0, LX/02X;->regenRetryCause:Ljava/lang/Throwable;

    if-eqz v2, :cond_5

    .line 390547
    const-string v2, "regenRetryCause"

    iget-object v4, v0, LX/02X;->regenRetryCause:Ljava/lang/Throwable;

    invoke-static {p0, v4}, LX/2HX;->a(LX/2HX;Ljava/lang/Throwable;)LX/0lF;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390548
    :cond_5
    iget-object v2, v0, LX/02X;->fallbackCause:Ljava/lang/Throwable;

    if-eqz v2, :cond_6

    .line 390549
    const-string v2, "fallbackCause"

    iget-object v4, v0, LX/02X;->fallbackCause:Ljava/lang/Throwable;

    invoke-static {p0, v4}, LX/2HX;->a(LX/2HX;Ljava/lang/Throwable;)LX/0lF;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390550
    :cond_6
    iget-object v2, v0, LX/02X;->xdexFailureCause:Ljava/lang/Throwable;

    if-eqz v2, :cond_7

    .line 390551
    const-string v2, "xdexFailureCause"

    iget-object v4, v0, LX/02X;->xdexFailureCause:Ljava/lang/Throwable;

    invoke-static {p0, v4}, LX/2HX;->a(LX/2HX;Ljava/lang/Throwable;)LX/0lF;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390552
    :cond_7
    iget-object v2, v0, LX/02X;->odexSchemeName:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 390553
    const-string v2, "odexSchemeName"

    iget-object v4, v0, LX/02X;->odexSchemeName:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 390554
    :cond_8
    iget-object v2, p0, LX/2HX;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 390555
    :cond_9
    invoke-static {}, LX/008;->getMainDexStore()LX/02U;

    move-result-object v2

    .line 390556
    const/4 v0, 0x0

    .line 390557
    :try_start_0
    invoke-virtual {v2}, LX/02U;->getAndClearCompletedOptimizationLog()LX/08a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 390558
    :goto_1
    if-eqz v0, :cond_a

    .line 390559
    invoke-direct {p0, v0}, LX/2HX;->a(LX/08a;)V

    .line 390560
    :cond_a
    and-int/lit8 v0, v1, 0x2

    if-eqz v0, :cond_0

    .line 390561
    new-instance v0, LX/08E;

    invoke-direct {v0}, LX/08E;-><init>()V

    invoke-virtual {v0}, LX/08E;->build()LX/08F;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/02U;->getNextRecommendedOptimizationAttemptTime(LX/08F;)J

    move-result-wide v0

    .line 390562
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 390563
    const-wide/16 v2, 0x2710

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 390564
    new-instance v2, LX/Hop;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, LX/Hop;-><init>(LX/2HX;Landroid/os/Looper;)V

    invoke-virtual {v2, v7, v0, v1}, LX/Hop;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 390565
    :catch_0
    move-exception v3

    .line 390566
    sget-object v4, LX/2HX;->a:Ljava/lang/Class;

    const-string v5, "error reading dex error log"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v3, v5, v6}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
