.class public LX/32V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;

.field private static final t:LX/0Tn;

.field private static final u:LX/0Tn;

.field private static final v:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 490078
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "contacts_upload/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->t:LX/0Tn;

    .line 490079
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "contacts/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->u:LX/0Tn;

    .line 490080
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "contacts_persist/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->v:LX/0Tn;

    .line 490081
    sget-object v0, LX/32V;->u:LX/0Tn;

    const-string v1, "continuous_import"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->a:LX/0Tn;

    .line 490082
    sget-object v0, LX/32V;->v:LX/0Tn;

    const-string v1, "continuous_import"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->b:LX/0Tn;

    .line 490083
    sget-object v0, LX/32V;->u:LX/0Tn;

    const-string v1, "starting_contact_import"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->c:LX/0Tn;

    .line 490084
    sget-object v0, LX/32V;->u:LX/0Tn;

    const-string v1, "contacts_upload_running"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->d:LX/0Tn;

    .line 490085
    sget-object v0, LX/32V;->u:LX/0Tn;

    const-string v1, "upload_contacts_batch_size"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->e:LX/0Tn;

    .line 490086
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "last_full_upload_success_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->f:LX/0Tn;

    .line 490087
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "first_full_upload_success_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->g:LX/0Tn;

    .line 490088
    sget-object v0, LX/32V;->v:LX/0Tn;

    const-string v1, "last_contacts_upload_attempt_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->h:LX/0Tn;

    .line 490089
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "last_phone_address_book_update"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->i:LX/0Tn;

    .line 490090
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "phone_address_book_version_hash"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->j:LX/0Tn;

    .line 490091
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "last_upload_client_root_hash"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->k:LX/0Tn;

    .line 490092
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "continuous_import_upsell_decline_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->l:LX/0Tn;

    .line 490093
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "continuous_import_upsell_decline_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->m:LX/0Tn;

    .line 490094
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "continuous_import_upsell_completed_version"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->n:LX/0Tn;

    .line 490095
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "contacts_upload_import_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->o:LX/0Tn;

    .line 490096
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "has_skipped_local_contacts_changes_check"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->p:LX/0Tn;

    .line 490097
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "has_seen_contacts_upload_dialog"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->q:LX/0Tn;

    .line 490098
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "contacts_upload_dialog_show_generic_error_screen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->r:LX/0Tn;

    .line 490099
    sget-object v0, LX/32V;->t:LX/0Tn;

    const-string v1, "new_contacts_count_for_tab_badge"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/32V;->s:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 490100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490101
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 490102
    sget-object v0, LX/32V;->t:LX/0Tn;

    sget-object v1, LX/32V;->u:LX/0Tn;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
