.class public LX/2P4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static volatile k:LX/2P4;


# instance fields
.field private final c:LX/0SF;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2NA;

.field private final g:LX/2P0;

.field private final h:LX/2P6;

.field private final i:LX/2Oy;

.field private j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 405381
    const-class v0, LX/2P4;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2P4;->b:Ljava/lang/String;

    .line 405382
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/2P5;->a:LX/0U1;

    .line 405383
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405384
    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/2P5;->d:LX/0U1;

    .line 405385
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405386
    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/2P5;->e:LX/0U1;

    .line 405387
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405388
    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/2P5;->f:LX/0U1;

    .line 405389
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405390
    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/2P5;->o:LX/0U1;

    .line 405391
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405392
    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/2P5;->n:LX/0U1;

    .line 405393
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405394
    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2P5;->m:LX/0U1;

    .line 405395
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405396
    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2P5;->h:LX/0U1;

    .line 405397
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405398
    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2P5;->i:LX/0U1;

    .line 405399
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405400
    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2P5;->j:LX/0U1;

    .line 405401
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405402
    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/2P5;->k:LX/0U1;

    .line 405403
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405404
    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/2P5;->l:LX/0U1;

    .line 405405
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405406
    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/2P5;->b:LX/0U1;

    .line 405407
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405408
    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/2P5;->p:LX/0U1;

    .line 405409
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405410
    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/2P2;->b:LX/0U1;

    .line 405411
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405412
    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/2P2;->c:LX/0U1;

    .line 405413
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405414
    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/2P2;->d:LX/0U1;

    .line 405415
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405416
    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/2P2;->e:LX/0U1;

    .line 405417
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405418
    aput-object v2, v0, v1

    sput-object v0, LX/2P4;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0SF;LX/0Or;LX/0Or;LX/2NA;LX/2P0;LX/2P6;LX/2Oy;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SF;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;",
            "LX/2NA;",
            "LX/2P0;",
            "LX/2P6;",
            "LX/2Oy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 405432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 405433
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2P4;->j:Z

    .line 405434
    iput-object p1, p0, LX/2P4;->c:LX/0SF;

    .line 405435
    iput-object p2, p0, LX/2P4;->d:LX/0Or;

    .line 405436
    iput-object p3, p0, LX/2P4;->e:LX/0Or;

    .line 405437
    iput-object p4, p0, LX/2P4;->f:LX/2NA;

    .line 405438
    iput-object p5, p0, LX/2P4;->g:LX/2P0;

    .line 405439
    iput-object p6, p0, LX/2P4;->h:LX/2P6;

    .line 405440
    iput-object p7, p0, LX/2P4;->i:LX/2Oy;

    .line 405441
    return-void
.end method

.method public static a(LX/0QB;)LX/2P4;
    .locals 11

    .prologue
    .line 405419
    sget-object v0, LX/2P4;->k:LX/2P4;

    if-nez v0, :cond_1

    .line 405420
    const-class v1, LX/2P4;

    monitor-enter v1

    .line 405421
    :try_start_0
    sget-object v0, LX/2P4;->k:LX/2P4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 405422
    if-eqz v2, :cond_0

    .line 405423
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 405424
    new-instance v3, LX/2P4;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SF;

    const/16 v5, 0x12cb

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0xdc6

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/2NA;->b(LX/0QB;)LX/2NA;

    move-result-object v7

    check-cast v7, LX/2NA;

    invoke-static {v0}, LX/2P0;->a(LX/0QB;)LX/2P0;

    move-result-object v8

    check-cast v8, LX/2P0;

    invoke-static {v0}, LX/2P6;->a(LX/0QB;)LX/2P6;

    move-result-object v9

    check-cast v9, LX/2P6;

    invoke-static {v0}, LX/2Oy;->a(LX/0QB;)LX/2Oy;

    move-result-object v10

    check-cast v10, LX/2Oy;

    invoke-direct/range {v3 .. v10}, LX/2P4;-><init>(LX/0SF;LX/0Or;LX/0Or;LX/2NA;LX/2P0;LX/2P6;LX/2Oy;)V

    .line 405425
    move-object v0, v3

    .line 405426
    sput-object v0, LX/2P4;->k:LX/2P4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405427
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 405428
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 405429
    :cond_1
    sget-object v0, LX/2P4;->k:LX/2P4;

    return-object v0

    .line 405430
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 405431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JI)Landroid/util/Pair;
    .locals 14
    .param p1    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "JI)",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadsCollection;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 405329
    iget-boolean v2, p0, LX/2P4;->j:Z

    if-eqz v2, :cond_1

    .line 405330
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a()Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v2

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    .line 405331
    :cond_0
    :goto_0
    return-object v2

    .line 405332
    :cond_1
    const/4 v2, 0x0

    .line 405333
    const/4 v9, 0x0

    .line 405334
    if-eqz p1, :cond_7

    .line 405335
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gtz v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 405336
    if-gtz p4, :cond_6

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 405337
    sget-object v2, LX/2P5;->a:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v2

    move-object v6, v2

    .line 405338
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "threads LEFT JOIN thread_participants ON "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/2P5;->b:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, LX/2P2;->a:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 405339
    :try_start_0
    iget-object v2, p0, LX/2P4;->i:LX/2Oy;

    invoke-virtual {v2}, LX/2Oy;->b()V
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_2

    .line 405340
    iget-object v2, p0, LX/2P4;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, LX/2gJ;

    const/4 v11, 0x0

    .line 405341
    :try_start_1
    invoke-virtual {v10}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 405342
    sget-object v4, LX/2P4;->a:[Ljava/lang/String;

    if-nez v6, :cond_9

    const/4 v5, 0x0

    :goto_4
    if-nez v6, :cond_a

    const/4 v6, 0x0

    :goto_5
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v4

    .line 405343
    const/4 v3, 0x0

    .line 405344
    :try_start_2
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 405345
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 405346
    :cond_2
    :goto_6
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 405347
    invoke-direct {p0, v4}, LX/2P4;->a(Landroid/database/Cursor;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v6

    .line 405348
    if-eqz v6, :cond_2

    .line 405349
    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 405350
    iget-object v6, p0, LX/2P4;->h:LX/2P6;

    invoke-virtual {v6, v4}, LX/2P6;->b(Landroid/database/Cursor;)Lcom/facebook/user/model/User;

    move-result-object v6

    .line 405351
    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    goto :goto_6

    .line 405352
    :catch_0
    move-exception v2

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 405353
    :catchall_0
    move-exception v3

    move-object v12, v3

    move-object v3, v2

    move-object v2, v12

    :goto_7
    if-eqz v4, :cond_3

    if-eqz v3, :cond_f

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_3
    :goto_8
    :try_start_5
    throw v2
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 405354
    :catch_1
    move-exception v2

    :try_start_6
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 405355
    :catchall_1
    move-exception v3

    move-object v12, v3

    move-object v3, v2

    move-object v2, v12

    :goto_9
    if-eqz v10, :cond_4

    if-eqz v3, :cond_10

    :try_start_7
    invoke-virtual {v10}, LX/2gJ;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4

    :cond_4
    :goto_a
    throw v2

    .line 405356
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 405357
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 405358
    :cond_7
    const-wide/16 v4, 0x0

    cmp-long v3, p2, v4

    if-lez v3, :cond_8

    .line 405359
    sget-object v2, LX/2P5;->e:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0uu;->b(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 405360
    :cond_8
    sget-object v3, LX/2P5;->e:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v9

    .line 405361
    if-lez p4, :cond_11

    .line 405362
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " LIMIT "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v6, v2

    goto/16 :goto_3

    .line 405363
    :catch_2
    move-exception v2

    .line 405364
    const/4 v3, 0x1

    iput-boolean v3, p0, LX/2P4;->j:Z

    .line 405365
    sget-object v3, LX/2P4;->b:Ljava/lang/String;

    const-string v4, "Failed to make master key available"

    invoke-static {v3, v4, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 405366
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a()Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v2

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    goto/16 :goto_0

    .line 405367
    :cond_9
    :try_start_8
    invoke-virtual {v6}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4

    :cond_a
    invoke-virtual {v6}, LX/0ux;->b()[Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-result-object v6

    goto/16 :goto_5

    .line 405368
    :cond_b
    :try_start_9
    iget-object v6, p0, LX/2P4;->d:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 405369
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 405370
    if-lez p4, :cond_c

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v2

    move/from16 v0, p4

    if-ge v2, v0, :cond_e

    :cond_c
    const/4 v2, 0x1

    .line 405371
    :goto_b
    new-instance v7, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-direct {v7, v6, v2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    .line 405372
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {v7, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    move-result-object v2

    .line 405373
    if-eqz v4, :cond_d

    :try_start_a
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 405374
    :cond_d
    if-eqz v10, :cond_0

    invoke-virtual {v10}, LX/2gJ;->close()V

    goto/16 :goto_0

    .line 405375
    :cond_e
    const/4 v2, 0x0

    goto :goto_b

    .line 405376
    :catch_3
    move-exception v4

    :try_start_b
    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 405377
    :catchall_2
    move-exception v2

    move-object v3, v11

    goto/16 :goto_9

    .line 405378
    :cond_f
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto/16 :goto_8

    .line 405379
    :catch_4
    move-exception v4

    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto/16 :goto_a

    :cond_10
    invoke-virtual {v10}, LX/2gJ;->close()V

    goto/16 :goto_a

    .line 405380
    :catchall_3
    move-exception v2

    goto/16 :goto_7

    :cond_11
    move-object v6, v2

    goto/16 :goto_3
.end method

.method private a(Landroid/database/Cursor;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 14

    .prologue
    .line 405270
    sget-object v0, LX/2P5;->a:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 405271
    sget-object v0, LX/2P5;->d:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 405272
    sget-object v0, LX/2P5;->e:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v4

    .line 405273
    sget-object v0, LX/2P5;->f:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v6

    .line 405274
    sget-object v0, LX/2P5;->m:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->d(Landroid/database/Cursor;)I

    move-result v8

    .line 405275
    sget-object v0, LX/2P5;->p:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->f(Landroid/database/Cursor;)[B

    move-result-object v0

    .line 405276
    const/4 v1, 0x0

    .line 405277
    :try_start_0
    iget-object v9, p0, LX/2P4;->i:LX/2Oy;

    invoke-virtual {v9, v0}, LX/2Oy;->b([B)[B
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 405278
    :goto_0
    sget-object v1, LX/2P5;->i:LX/0U1;

    invoke-virtual {v1, p1}, LX/0U1;->f(Landroid/database/Cursor;)[B

    move-result-object v1

    .line 405279
    sget-object v9, LX/2P5;->k:LX/0U1;

    invoke-virtual {v9, p1}, LX/0U1;->f(Landroid/database/Cursor;)[B

    move-result-object v9

    .line 405280
    iget-object v10, p0, LX/2P4;->i:LX/2Oy;

    invoke-virtual {v10, v0, v1}, LX/2Oy;->c([B[B)Ljava/lang/String;

    move-result-object v1

    .line 405281
    sget-object v10, LX/2P5;->j:LX/0U1;

    invoke-virtual {v10, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v10

    .line 405282
    iget-object v11, p0, LX/2P4;->i:LX/2Oy;

    invoke-virtual {v11, v0, v9}, LX/2Oy;->c([B[B)Ljava/lang/String;

    move-result-object v9

    .line 405283
    sget-object v0, LX/2P5;->h:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v11

    .line 405284
    sget-object v0, LX/2P5;->l:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->d(Landroid/database/Cursor;)I

    move-result v0

    const/4 v12, 0x1

    if-ne v0, v12, :cond_0

    const/4 v0, 0x1

    .line 405285
    :goto_1
    invoke-static {v2}, LX/Dpq;->a(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 405286
    const/4 v0, 0x0

    .line 405287
    :goto_2
    return-object v0

    .line 405288
    :catch_0
    move-exception v0

    .line 405289
    :goto_3
    sget-object v9, LX/2P4;->b:Ljava/lang/String;

    const-string v10, "Failed to decrypt master key"

    invoke-static {v9, v10, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    .line 405290
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 405291
    :cond_1
    invoke-static {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    .line 405292
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 405293
    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->f()Z

    move-result v12

    invoke-static {v12}, LX/0PB;->checkState(Z)V

    .line 405294
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v12

    .line 405295
    sget-object v13, LX/6ek;->INBOX:LX/6ek;

    .line 405296
    iput-object v13, v12, LX/6g6;->A:LX/6ek;

    .line 405297
    iput-object v2, v12, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 405298
    const/4 v2, 0x1

    .line 405299
    iput-boolean v2, v12, LX/6g6;->w:Z

    .line 405300
    iput-object v3, v12, LX/6g6;->g:Ljava/lang/String;

    .line 405301
    invoke-virtual {v12, v4, v5}, LX/6g6;->e(J)LX/6g6;

    .line 405302
    invoke-virtual {v12, v6, v7}, LX/6g6;->f(J)LX/6g6;

    .line 405303
    iput v8, v12, LX/6g6;->I:I

    .line 405304
    iget-object v2, p0, LX/2P4;->f:LX/2NA;

    invoke-virtual {v2, v11}, LX/2NA;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/MessageDraft;

    move-result-object v2

    .line 405305
    iput-object v2, v12, LX/6g6;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    .line 405306
    iput-boolean v0, v12, LX/6g6;->u:Z

    .line 405307
    iput-object v9, v12, LX/6g6;->q:Ljava/lang/String;

    .line 405308
    invoke-static {p1}, LX/2P6;->a(Landroid/database/Cursor;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    .line 405309
    iget-object v2, p0, LX/2P4;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 405310
    new-instance v3, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 405311
    iget-object v4, v2, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 405312
    invoke-virtual {v2}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 405313
    new-instance v2, LX/6fz;

    invoke-direct {v2}, LX/6fz;-><init>()V

    .line 405314
    iput-object v3, v2, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 405315
    move-object v2, v2

    .line 405316
    invoke-virtual {v2}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v2

    move-object v2, v2

    .line 405317
    invoke-static {v0, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 405318
    iput-object v3, v12, LX/6g6;->h:Ljava/util/List;

    .line 405319
    if-eqz v10, :cond_2

    .line 405320
    iput-object v1, v12, LX/6g6;->o:Ljava/lang/String;

    .line 405321
    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 405322
    iget-object v0, v2, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 405323
    iput-object v0, v12, LX/6g6;->p:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 405324
    :cond_2
    :goto_4
    invoke-virtual {v12}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    goto/16 :goto_2

    .line 405325
    :cond_3
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 405326
    iput-object v0, v12, LX/6g6;->p:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 405327
    goto :goto_4

    .line 405328
    :catch_1
    move-exception v0

    goto/16 :goto_3

    :catch_2
    move-exception v0

    goto/16 :goto_3
.end method


# virtual methods
.method public final a(J)LX/0Px;
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 405243
    sget-object v0, LX/2P5;->b:LX/0U1;

    .line 405244
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 405245
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 405246
    iget-object v0, p0, LX/2P4;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 405247
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 405248
    const-string v1, "threads"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v5, LX/2P5;->a:LX/0U1;

    .line 405249
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 405250
    aput-object v5, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, LX/2P5;->e:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 405251
    :try_start_1
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 405252
    :cond_0
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 405253
    sget-object v1, LX/2P5;->a:LX/0U1;

    invoke-virtual {v1, v2}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 405254
    invoke-static {v1}, LX/Dpq;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 405255
    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 405256
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 405257
    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    .line 405258
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 405259
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v2, :cond_1

    if-eqz v1, :cond_6

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_1
    :goto_2
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 405260
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 405261
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_2

    if-eqz v9, :cond_7

    :try_start_6
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    :cond_2
    :goto_4
    throw v0

    .line 405262
    :cond_3
    :try_start_7
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result-object v0

    .line 405263
    if-eqz v2, :cond_4

    :try_start_8
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 405264
    :cond_4
    if-eqz v8, :cond_5

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_5
    return-object v0

    .line 405265
    :catch_2
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 405266
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 405267
    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    .line 405268
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_7
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 405269
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 405442
    invoke-static {}, LX/0uu;->b()LX/0uw;

    move-result-object v1

    .line 405443
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 405444
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 405445
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 405446
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 405447
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x25

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 405448
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "threads."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, LX/2P5;->d:LX/0U1;

    .line 405449
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 405450
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    .line 405451
    sget-object v6, LX/2P2;->b:LX/0U1;

    .line 405452
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 405453
    invoke-static {v6, v4}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v6

    .line 405454
    sget-object v7, LX/2P2;->c:LX/0U1;

    .line 405455
    iget-object p1, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, p1

    .line 405456
    invoke-static {v7, v4}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 405457
    invoke-virtual {v1, v5}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 405458
    invoke-virtual {v1, v6}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 405459
    invoke-virtual {v1, v4}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 405460
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 405461
    :cond_1
    move-object v4, v1

    .line 405462
    new-instance v11, LX/0Pz;

    invoke-direct {v11}, LX/0Pz;-><init>()V

    .line 405463
    iget-object v0, p0, LX/2P4;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, LX/2gJ;

    const/4 v10, 0x0

    .line 405464
    :try_start_0
    invoke-virtual {v9}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 405465
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "threads LEFT JOIN thread_participants ON "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/2P5;->b:LX/0U1;

    .line 405466
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405467
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/2P2;->a:LX/0U1;

    .line 405468
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 405469
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 405470
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v5, LX/2P5;->a:LX/0U1;

    .line 405471
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 405472
    aput-object v5, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, LX/2P5;->e:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    const-string v8, "5"

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 405473
    const/4 v1, 0x0

    .line 405474
    :goto_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 405475
    sget-object v0, LX/2P5;->a:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 405476
    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 405477
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 405478
    invoke-virtual {v11, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_1

    .line 405479
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 405480
    :catchall_0
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    :goto_2
    if-eqz v2, :cond_2

    if-eqz v1, :cond_7

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_2
    :goto_3
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 405481
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 405482
    :catchall_1
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    :goto_4
    if-eqz v9, :cond_3

    if-eqz v1, :cond_8

    :try_start_6
    invoke-virtual {v9}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    :cond_3
    :goto_5
    throw v0

    .line 405483
    :cond_4
    if-eqz v2, :cond_5

    :try_start_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 405484
    :cond_5
    if-eqz v9, :cond_6

    invoke-virtual {v9}, LX/2gJ;->close()V

    .line 405485
    :cond_6
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 405486
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 405487
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_6
    if-ge v2, v5, :cond_a

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 405488
    const-wide/16 v6, -0x1

    const/4 v1, -0x1

    invoke-direct {p0, v0, v6, v7, v1}, LX/2P4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JI)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 405489
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->e()I

    move-result v1

    const/4 v6, 0x1

    if-ne v1, v6, :cond_9

    const/4 v1, 0x1

    :goto_7
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 405490
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a(I)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 405491
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 405492
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 405493
    :catch_2
    move-exception v2

    :try_start_8
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 405494
    :catchall_2
    move-exception v0

    move-object v1, v10

    goto :goto_4

    .line 405495
    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_3

    .line 405496
    :catch_3
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_8
    invoke-virtual {v9}, LX/2gJ;->close()V

    goto :goto_5

    .line 405497
    :cond_9
    const/4 v1, 0x0

    goto :goto_7

    .line 405498
    :cond_a
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 405499
    :catchall_3
    move-exception v0

    goto :goto_2
.end method

.method public final a()LX/0Rf;
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 405220
    iget-object v0, p0, LX/2P4;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 405221
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 405222
    const-string v1, "threads"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, LX/2P5;->a:LX/0U1;

    .line 405223
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 405224
    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 405225
    :try_start_1
    new-instance v0, LX/0cA;

    invoke-direct {v0}, LX/0cA;-><init>()V

    .line 405226
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 405227
    sget-object v1, LX/2P5;->a:LX/0U1;

    invoke-virtual {v1, v2}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 405228
    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 405229
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 405230
    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    .line 405231
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 405232
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v2, :cond_0

    if-eqz v1, :cond_5

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_0
    :goto_2
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 405233
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 405234
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_1

    if-eqz v9, :cond_6

    :try_start_6
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    :cond_1
    :goto_4
    throw v0

    .line 405235
    :cond_2
    :try_start_7
    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result-object v0

    .line 405236
    if-eqz v2, :cond_3

    :try_start_8
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 405237
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_4
    return-object v0

    .line 405238
    :catch_2
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 405239
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 405240
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    .line 405241
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 405242
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Dop;
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "Recycle"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 405195
    sget-object v0, LX/2P5;->a:LX/0U1;

    .line 405196
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 405197
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 405198
    iget-object v0, p0, LX/2P4;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 405199
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 405200
    const-string v1, "threads"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v5, LX/2P5;->q:LX/0U1;

    .line 405201
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 405202
    aput-object v5, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 405203
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 405204
    sget-object v0, LX/2P5;->q:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->d(Landroid/database/Cursor;)I

    move-result v0

    invoke-static {v0}, LX/Dop;->from(I)LX/Dop;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v0

    .line 405205
    if-eqz v2, :cond_0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 405206
    :cond_0
    if-eqz v8, :cond_1

    invoke-virtual {v8}, LX/2gJ;->close()V

    .line 405207
    :cond_1
    :goto_0
    return-object v0

    .line 405208
    :cond_2
    if-eqz v2, :cond_3

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 405209
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/2gJ;->close()V

    .line 405210
    :cond_4
    sget-object v0, LX/Dop;->NOT_STARTED:LX/Dop;

    goto :goto_0

    .line 405211
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 405212
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v2, :cond_5

    if-eqz v1, :cond_7

    :try_start_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_5
    :goto_2
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 405213
    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 405214
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_6

    if-eqz v9, :cond_8

    :try_start_8
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    :cond_6
    :goto_4
    throw v0

    .line 405215
    :catch_2
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 405216
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 405217
    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    .line 405218
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 405219
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 9

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 405172
    const/4 v0, -0x1

    invoke-direct {p0, p1, v6, v7, v0}, LX/2P4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JI)Landroid/util/Pair;

    move-result-object v3

    .line 405173
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 405174
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 405175
    sget-object v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->a:Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 405176
    :goto_0
    return-object v0

    .line 405177
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->e()I

    move-result v4

    if-ne v4, v1, :cond_2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 405178
    invoke-virtual {v0, v2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a(I)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 405179
    const/4 v0, 0x0

    .line 405180
    if-lez p2, :cond_1

    .line 405181
    iget-object v0, p0, LX/2P4;->g:LX/2P0;

    invoke-virtual {v0, p1, v6, v7, p2}, LX/2P0;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JI)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v0

    .line 405182
    :cond_1
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v2

    .line 405183
    sget-object v4, LX/6j5;->TINCAN:LX/6j5;

    .line 405184
    iput-object v4, v2, LX/6iO;->a:LX/6j5;

    .line 405185
    iget-object v4, p0, LX/2P4;->c:LX/0SF;

    invoke-virtual {v4}, LX/0SF;->a()J

    move-result-wide v4

    .line 405186
    iput-wide v4, v2, LX/6iO;->g:J

    .line 405187
    sget-object v4, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_STALE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 405188
    iput-object v4, v2, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 405189
    iput-object v0, v2, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 405190
    iput-object v1, v2, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 405191
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, LX/0Px;

    .line 405192
    iput-object v0, v2, LX/6iO;->e:LX/0Px;

    .line 405193
    invoke-virtual {v2}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 405194
    goto :goto_1
.end method

.method public final b()LX/0P1;
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 405090
    iget-object v0, p0, LX/2P4;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 405091
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 405092
    const-string v1, "threads"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, LX/2P5;->a:LX/0U1;

    .line 405093
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 405094
    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, LX/2P5;->n:LX/0U1;

    .line 405095
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 405096
    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 405097
    :try_start_1
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    .line 405098
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 405099
    sget-object v1, LX/2P5;->a:LX/0U1;

    invoke-virtual {v1, v2}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 405100
    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 405101
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 405102
    sget-object v3, LX/2P5;->n:LX/0U1;

    invoke-virtual {v3, v2}, LX/0U1;->c(Landroid/database/Cursor;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 405103
    invoke-virtual {v0, v1, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    .line 405104
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 405105
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v2, :cond_0

    if-eqz v1, :cond_5

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_0
    :goto_2
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 405106
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 405107
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_1

    if-eqz v9, :cond_6

    :try_start_6
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    :cond_1
    :goto_4
    throw v0

    .line 405108
    :cond_2
    :try_start_7
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result-object v0

    .line 405109
    if-eqz v2, :cond_3

    :try_start_8
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 405110
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_4
    return-object v0

    .line 405111
    :catch_2
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 405112
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 405113
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    .line 405114
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 405115
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public final b(J)Ljava/lang/String;
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "Recycle"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 405147
    sget-object v0, LX/2P5;->b:LX/0U1;

    .line 405148
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 405149
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 405150
    iget-object v0, p0, LX/2P4;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 405151
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 405152
    const-string v1, "threads"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v5, LX/2P5;->c:LX/0U1;

    .line 405153
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 405154
    aput-object v5, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 405155
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 405156
    sget-object v0, LX/2P5;->c:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v0

    .line 405157
    if-eqz v2, :cond_0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 405158
    :cond_0
    if-eqz v8, :cond_1

    invoke-virtual {v8}, LX/2gJ;->close()V

    .line 405159
    :cond_1
    :goto_0
    return-object v0

    .line 405160
    :cond_2
    if-eqz v2, :cond_3

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 405161
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_4
    move-object v0, v9

    .line 405162
    goto :goto_0

    .line 405163
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 405164
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v2, :cond_5

    if-eqz v1, :cond_7

    :try_start_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_5
    :goto_2
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 405165
    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 405166
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_6

    if-eqz v9, :cond_8

    :try_start_8
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    :cond_6
    :goto_4
    throw v0

    .line 405167
    :catch_2
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 405168
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 405169
    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    .line 405170
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 405171
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)[B
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "Recycle"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 405116
    if-nez p1, :cond_1

    move-object v0, v9

    .line 405117
    :cond_0
    :goto_0
    return-object v0

    .line 405118
    :cond_1
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, LX/2P5;->a:LX/0U1;

    .line 405119
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 405120
    aput-object v1, v2, v0

    const/4 v0, 0x1

    sget-object v1, LX/2P5;->p:LX/0U1;

    .line 405121
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 405122
    aput-object v1, v2, v0

    .line 405123
    sget-object v0, LX/2P5;->a:LX/0U1;

    .line 405124
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 405125
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 405126
    iget-object v0, p0, LX/2P4;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 405127
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 405128
    const-string v1, "threads"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 405129
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 405130
    sget-object v0, LX/2P5;->p:LX/0U1;

    .line 405131
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 405132
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v0

    .line 405133
    if-eqz v2, :cond_2

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 405134
    :cond_2
    if-eqz v8, :cond_0

    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_0

    .line 405135
    :cond_3
    if-eqz v2, :cond_4

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 405136
    :cond_4
    if-eqz v8, :cond_5

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_5
    move-object v0, v9

    .line 405137
    goto :goto_0

    .line 405138
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 405139
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v2, :cond_6

    if-eqz v1, :cond_8

    :try_start_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_6
    :goto_2
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 405140
    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 405141
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_7

    if-eqz v9, :cond_9

    :try_start_8
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    :cond_7
    :goto_4
    throw v0

    .line 405142
    :catch_2
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 405143
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 405144
    :cond_8
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    .line 405145
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_9
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 405146
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method
