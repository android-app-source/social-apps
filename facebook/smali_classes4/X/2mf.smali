.class public final LX/2mf;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 461045
    new-instance v0, LX/0U1;

    const-string v1, "group_id"

    const-string v2, "TEXT PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2mf;->a:LX/0U1;

    .line 461046
    new-instance v0, LX/0U1;

    const-string v1, "group_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2mf;->b:LX/0U1;

    .line 461047
    new-instance v0, LX/0U1;

    const-string v1, "group_order"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2mf;->c:LX/0U1;

    .line 461048
    new-instance v0, LX/0U1;

    const-string v1, "group_total_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2mf;->d:LX/0U1;

    .line 461049
    new-instance v0, LX/0U1;

    const-string v1, "group_visible_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2mf;->e:LX/0U1;

    return-void
.end method
