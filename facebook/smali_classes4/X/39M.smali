.class public LX/39M;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 522846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 522847
    const/4 v0, 0x0

    .line 522848
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 522849
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 522850
    :goto_0
    return v1

    .line 522851
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_2

    .line 522852
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 522853
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 522854
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_0

    if-eqz v4, :cond_0

    .line 522855
    const-string v5, "section_type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 522856
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    goto :goto_1

    .line 522857
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 522858
    :cond_2
    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 522859
    if-eqz v0, :cond_3

    .line 522860
    invoke-virtual {p1, v1, v3}, LX/186;->a(ILjava/lang/Enum;)V

    .line 522861
    :cond_3
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move-object v3, v0

    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 522862
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 522863
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(IIS)S

    move-result v0

    .line 522864
    if-eqz v0, :cond_0

    .line 522865
    const-string v0, "section_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 522866
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 522867
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 522868
    return-void
.end method
