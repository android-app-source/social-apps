.class public LX/2Ms;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Uh;

.field private final b:LX/2Mo;

.field private final c:LX/0W3;

.field private final d:LX/2Mt;

.field private final e:Landroid/content/res/Resources;

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0Uh;LX/2Mo;LX/0W3;LX/2Mt;Landroid/content/res/Resources;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397774
    iput-object p1, p0, LX/2Ms;->a:LX/0Uh;

    .line 397775
    iput-object p2, p0, LX/2Ms;->b:LX/2Mo;

    .line 397776
    iput-object p3, p0, LX/2Ms;->c:LX/0W3;

    .line 397777
    iput-object p4, p0, LX/2Ms;->d:LX/2Mt;

    .line 397778
    iput-object p5, p0, LX/2Ms;->e:Landroid/content/res/Resources;

    .line 397779
    iput-object p6, p0, LX/2Ms;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 397780
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ms;
    .locals 1

    .prologue
    .line 397772
    invoke-static {p0}, LX/2Ms;->b(LX/0QB;)LX/2Ms;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 4

    .prologue
    .line 397771
    iget-wide v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    const-wide/32 v2, 0x17d7840

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    iget v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    mul-int/2addr v0, v1

    const v1, 0x2faf080

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2Ms;
    .locals 7

    .prologue
    .line 397781
    new-instance v0, LX/2Ms;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/2Mo;->a(LX/0QB;)LX/2Mo;

    move-result-object v2

    check-cast v2, LX/2Mo;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {p0}, LX/2Mt;->a(LX/0QB;)LX/2Mt;

    move-result-object v4

    check-cast v4, LX/2Mt;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v0 .. v6}, LX/2Ms;-><init>(LX/0Uh;LX/2Mo;LX/0W3;LX/2Mt;Landroid/content/res/Resources;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 397782
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/media/photoquality/PhotoQuality;
    .locals 3

    .prologue
    .line 397755
    invoke-virtual {p0}, LX/2Ms;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397756
    sget-object v0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->a:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    .line 397757
    :cond_0
    :goto_0
    return-object v0

    .line 397758
    :cond_1
    if-eqz p1, :cond_2

    .line 397759
    iget-object v0, p0, LX/2Ms;->a:LX/0Uh;

    const/16 v1, 0x1ae

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 397760
    iget-object v0, p0, LX/2Ms;->d:LX/2Mt;

    .line 397761
    invoke-static {v0, p1}, LX/2Mt;->c(LX/2Mt;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 397762
    iget-object v1, v0, LX/2Mt;->h:LX/0QI;

    invoke-interface {v1, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;

    .line 397763
    if-eqz v1, :cond_3

    iget-object v1, v1, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->a:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    :goto_1
    move-object v0, v1

    .line 397764
    if-eqz v0, :cond_2

    .line 397765
    iget v1, v0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->b:I

    move v1, v1

    .line 397766
    if-gtz v1, :cond_0

    .line 397767
    :cond_2
    iget-object v0, p0, LX/2Ms;->e:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v1, p0, LX/2Ms;->e:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 397768
    const/16 v1, 0x3c0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 397769
    const/16 v1, 0x800

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 397770
    new-instance v0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    invoke-direct {v0, v1}, Lcom/facebook/messaging/media/photoquality/PhotoQuality;-><init>(I)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 397735
    iget-object v1, p0, LX/2Ms;->c:LX/0W3;

    sget-wide v2, LX/0X5;->ko:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    move-result v1

    if-nez v1, :cond_1

    .line 397736
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-lez v1, :cond_2

    iget v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 397737
    iget v2, p2, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->b:I

    move v2, v2

    .line 397738
    if-le v1, v2, :cond_3

    :cond_2
    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    iget-object v1, p0, LX/2Ms;->c:LX/0W3;

    sget-wide v4, LX/0X5;->kq:J

    invoke-interface {v1, v4, v5}, LX/0W4;->c(J)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/media/photoquality/PhotoQuality;
    .locals 3

    .prologue
    .line 397746
    if-eqz p1, :cond_0

    .line 397747
    iget-object v0, p0, LX/2Ms;->a:LX/0Uh;

    const/16 v1, 0x1ae

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397748
    iget-object v0, p0, LX/2Ms;->d:LX/2Mt;

    .line 397749
    iget-object v1, v0, LX/2Mt;->h:LX/0QI;

    invoke-interface {v1, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;

    .line 397750
    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->b:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    :goto_0
    move-object v0, v1

    .line 397751
    if-eqz v0, :cond_0

    .line 397752
    iget v1, v0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->b:I

    move v1, v1

    .line 397753
    if-lez v1, :cond_0

    .line 397754
    :goto_1
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    const/16 v1, 0x2d0

    invoke-direct {v0, v1}, Lcom/facebook/messaging/media/photoquality/PhotoQuality;-><init>(I)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 397739
    iget-object v0, p0, LX/2Ms;->a:LX/0Uh;

    const/16 v1, 0x156

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 397740
    if-eqz v0, :cond_0

    .line 397741
    iget-object v0, p0, LX/2Ms;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2N1;->b:LX/0Tn;

    sget-object v2, LX/CN0;->NEVER:LX/CN0;

    iget v2, v2, LX/CN0;->value:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 397742
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 397743
    iget-object v1, p0, LX/2Ms;->b:LX/2Mo;

    invoke-virtual {v1}, LX/2Mo;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/CN0;->WIFI_ONLY:LX/CN0;

    iget v1, v1, LX/CN0;->value:I

    if-ne v0, v1, :cond_0

    .line 397744
    const/4 v0, 0x1

    .line 397745
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
