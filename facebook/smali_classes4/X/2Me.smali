.class public LX/2Me;
.super LX/2Mf;
.source ""


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 397041
    const/16 v0, 0x2d0

    const v1, 0xd7000

    const/16 v2, 0x1e

    const/16 v3, 0xa

    invoke-direct {p0, v0, v1, v2, v3}, LX/2Mf;-><init>(IIII)V

    .line 397042
    return-void
.end method

.method public static a(LX/0QB;)LX/2Me;
    .locals 1

    .prologue
    .line 397043
    invoke-static {p0}, LX/2Me;->b(LX/0QB;)LX/2Me;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2Me;
    .locals 2

    .prologue
    .line 397044
    new-instance v1, LX/2Me;

    invoke-direct {v1}, LX/2Me;-><init>()V

    .line 397045
    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    .line 397046
    iput-object v0, v1, LX/2Me;->a:LX/0ad;

    .line 397047
    return-object v1
.end method


# virtual methods
.method public final a()LX/2Mg;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v6, -0x1

    .line 397048
    iget-object v0, p0, LX/2Me;->a:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget v2, LX/6eX;->i:I

    const/16 v3, 0x2d0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;II)I

    move-result v1

    .line 397049
    iget-object v0, p0, LX/2Me;->a:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget v3, LX/6eX;->g:I

    const/16 v4, 0x1e

    invoke-interface {v0, v2, v3, v4}, LX/0ad;->a(LX/0c0;II)I

    move-result v3

    .line 397050
    iget-object v0, p0, LX/2Me;->a:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget v4, LX/6eX;->h:I

    const/16 v5, 0xa

    invoke-interface {v0, v2, v4, v5}, LX/0ad;->a(LX/0c0;II)I

    move-result v4

    .line 397051
    iget-object v0, p0, LX/2Me;->a:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget v5, LX/6eX;->f:I

    const/16 v7, 0x35c

    invoke-interface {v0, v2, v5, v7}, LX/0ad;->a(LX/0c0;II)I

    move-result v0

    mul-int/lit16 v2, v0, 0x400

    .line 397052
    iget-object v0, p0, LX/2Me;->a:LX/0ad;

    sget-object v5, LX/0c0;->Cached:LX/0c0;

    sget-short v7, LX/6eX;->e:S

    const/4 v8, 0x0

    invoke-interface {v0, v5, v7, v8}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v0, v5, :cond_1

    .line 397053
    const/4 v0, 0x1

    .line 397054
    :goto_0
    move v5, v0

    .line 397055
    iget-object v0, p0, LX/2Me;->a:LX/0ad;

    sget-object v7, LX/0c0;->Cached:LX/0c0;

    sget-short v8, LX/6eX;->b:S

    invoke-interface {v0, v7, v8, v9}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397056
    iget-object v0, p0, LX/2Me;->a:LX/0ad;

    sget-object v7, LX/0c0;->Cached:LX/0c0;

    sget v8, LX/6eX;->a:I

    invoke-interface {v0, v7, v8, v6}, LX/0ad;->a(LX/0c0;II)I

    move-result v6

    .line 397057
    :cond_0
    iget-object v0, p0, LX/2Me;->a:LX/0ad;

    sget-object v7, LX/0c0;->Cached:LX/0c0;

    sget-short v8, LX/6eX;->j:S

    invoke-interface {v0, v7, v8, v9}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v7

    .line 397058
    new-instance v0, LX/2Mg;

    int-to-float v3, v3

    invoke-direct/range {v0 .. v7}, LX/2Mg;-><init>(IIFIIIZ)V

    return-object v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method
