.class public LX/2oR;
.super Landroid/util/LruCache;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/19l;

.field private final b:LX/2oS;


# direct methods
.method public constructor <init>(LX/19l;)V
    .locals 3

    .prologue
    .line 466391
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Landroid/util/LruCache;-><init>(I)V

    .line 466392
    new-instance v0, LX/2oS;

    invoke-direct {v0, p0}, LX/2oS;-><init>(LX/2oR;)V

    iput-object v0, p0, LX/2oR;->b:LX/2oS;

    .line 466393
    iput-object p1, p0, LX/2oR;->a:LX/19l;

    .line 466394
    iget-object v0, p0, LX/2oR;->a:LX/19l;

    iget-object v0, v0, LX/19l;->a:LX/16V;

    const-class v1, LX/2oT;

    iget-object v2, p0, LX/2oR;->b:LX/2oS;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 466395
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/2oR;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 466383
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/2oR;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 466384
    const/16 v1, 0x194

    if-eq p2, v1, :cond_0

    const/16 v1, 0x19a

    if-ne p2, v1, :cond_3

    .line 466385
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p2, :cond_2

    .line 466386
    :cond_1
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 466387
    invoke-super {p0, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466388
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    .line 466389
    :cond_3
    :try_start_1
    invoke-super {p0, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 466390
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 466380
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 466381
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x194

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/16 v1, 0x19a

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 466382
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
