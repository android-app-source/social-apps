.class public LX/2sE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2sN;


# static fields
.field public static final a:LX/2sE;


# instance fields
.field private final b:LX/15i;

.field private final c:I

.field private final d:Z

.field private final e:I

.field private final f:I

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 472473
    new-instance v0, LX/2sE;

    invoke-direct {v0}, LX/2sE;-><init>()V

    sput-object v0, LX/2sE;->a:LX/2sE;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 472474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472475
    const/4 v0, 0x0

    iput-object v0, p0, LX/2sE;->b:LX/15i;

    .line 472476
    iput v1, p0, LX/2sE;->c:I

    .line 472477
    iput-boolean v1, p0, LX/2sE;->d:Z

    .line 472478
    iput v1, p0, LX/2sE;->e:I

    .line 472479
    iput v1, p0, LX/2sE;->f:I

    .line 472480
    iput v1, p0, LX/2sE;->g:I

    .line 472481
    return-void
.end method

.method public constructor <init>(LX/15i;IZI)V
    .locals 1

    .prologue
    .line 472482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472483
    iput-object p1, p0, LX/2sE;->b:LX/15i;

    .line 472484
    iput p2, p0, LX/2sE;->c:I

    .line 472485
    iput-boolean p3, p0, LX/2sE;->d:Z

    .line 472486
    iput p4, p0, LX/2sE;->e:I

    .line 472487
    invoke-virtual {p1, p2}, LX/15i;->d(I)I

    move-result v0

    iput v0, p0, LX/2sE;->f:I

    .line 472488
    const/4 v0, 0x0

    iput v0, p0, LX/2sE;->g:I

    .line 472489
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 472490
    iget v0, p0, LX/2sE;->g:I

    iget v1, p0, LX/2sE;->f:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/1vs;
    .locals 4

    .prologue
    .line 472491
    iget v0, p0, LX/2sE;->g:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/2sE;->g:I

    iget v1, p0, LX/2sE;->f:I

    if-lt v0, v1, :cond_1

    .line 472492
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Out of bound for iteration"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 472493
    :cond_1
    iget-object v0, p0, LX/2sE;->b:LX/15i;

    iget v1, p0, LX/2sE;->c:I

    iget v2, p0, LX/2sE;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/2sE;->g:I

    invoke-virtual {v0, v1, v2}, LX/15i;->q(II)I

    move-result v1

    .line 472494
    iget v0, p0, LX/2sE;->e:I

    .line 472495
    iget-boolean v2, p0, LX/2sE;->d:Z

    if-eqz v2, :cond_2

    .line 472496
    iget-object v0, p0, LX/2sE;->b:LX/15i;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->i(II)S

    move-result v0

    .line 472497
    iget-object v2, p0, LX/2sE;->b:LX/15i;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, LX/15i;->g(II)I

    move-result v1

    .line 472498
    :cond_2
    iget-object v2, p0, LX/2sE;->b:LX/15i;

    invoke-static {v2, v1, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    return-object v0
.end method
