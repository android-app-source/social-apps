.class public LX/2re;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2re;


# instance fields
.field private final a:LX/2RE;

.field private final b:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(LX/2RE;Landroid/content/ContentResolver;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 472055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472056
    iput-object p1, p0, LX/2re;->a:LX/2RE;

    .line 472057
    iput-object p2, p0, LX/2re;->b:Landroid/content/ContentResolver;

    .line 472058
    return-void
.end method

.method public static a(LX/0QB;)LX/2re;
    .locals 5

    .prologue
    .line 472059
    sget-object v0, LX/2re;->c:LX/2re;

    if-nez v0, :cond_1

    .line 472060
    const-class v1, LX/2re;

    monitor-enter v1

    .line 472061
    :try_start_0
    sget-object v0, LX/2re;->c:LX/2re;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 472062
    if-eqz v2, :cond_0

    .line 472063
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 472064
    new-instance p0, LX/2re;

    invoke-static {v0}, LX/2RE;->a(LX/0QB;)LX/2RE;

    move-result-object v3

    check-cast v3, LX/2RE;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v4

    check-cast v4, Landroid/content/ContentResolver;

    invoke-direct {p0, v3, v4}, LX/2re;-><init>(LX/2RE;Landroid/content/ContentResolver;)V

    .line 472065
    move-object v0, p0

    .line 472066
    sput-object v0, LX/2re;->c:LX/2re;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 472067
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 472068
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 472069
    :cond_1
    sget-object v0, LX/2re;->c:LX/2re;

    return-object v0

    .line 472070
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 472071
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 472072
    iget-object v0, p0, LX/2re;->b:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/2re;->a:LX/2RE;

    iget-object v1, v1, LX/2RE;->g:LX/2RP;

    iget-object v1, v1, LX/2RP;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 472073
    if-gez v0, :cond_0

    .line 472074
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to delete contacts database"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 472075
    :cond_0
    return-void
.end method
