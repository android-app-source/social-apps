.class public LX/2Ob;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2OW;

.field private final b:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2OW;)V
    .locals 1

    .prologue
    .line 402442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402443
    iput-object p1, p0, LX/2Ob;->a:LX/2OW;

    .line 402444
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/2Ob;->b:LX/01J;

    .line 402445
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402454
    iget-object v0, p0, LX/2Ob;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402455
    iget-object v0, p0, LX/2Ob;->b:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadEventReminder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 402446
    iget-object v0, p0, LX/2Ob;->a:LX/2OW;

    invoke-virtual {v0}, LX/2OW;->b()V

    .line 402447
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 402448
    iget-object v3, p0, LX/2Ob;->b:LX/01J;

    .line 402449
    iget-object v4, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v4, v4

    .line 402450
    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->f:LX/0P1;

    move-object v0, v5

    .line 402451
    invoke-virtual {v3, v4, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402452
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 402453
    :cond_0
    return-void
.end method
