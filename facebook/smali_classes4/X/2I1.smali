.class public LX/2I1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2AX;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider__INJECTED_BY_TemplateInjector:LX/2I1;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 391397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance__com_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2I1;
    .locals 3

    .prologue
    .line 391398
    sget-object v0, LX/2I1;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider__INJECTED_BY_TemplateInjector:LX/2I1;

    if-nez v0, :cond_1

    .line 391399
    const-class v1, LX/2I1;

    monitor-enter v1

    .line 391400
    :try_start_0
    sget-object v0, LX/2I1;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider__INJECTED_BY_TemplateInjector:LX/2I1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 391401
    if-eqz v2, :cond_0

    .line 391402
    :try_start_1
    new-instance p0, LX/2I1;

    invoke-direct {p0}, LX/2I1;-><init>()V

    .line 391403
    move-object v0, p0

    .line 391404
    sput-object v0, LX/2I1;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider__INJECTED_BY_TemplateInjector:LX/2I1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391405
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 391406
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 391407
    :cond_1
    sget-object v0, LX/2I1;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider__INJECTED_BY_TemplateInjector:LX/2I1;

    return-object v0

    .line 391408
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 391409
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public get()LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391410
    new-instance v0, LX/1se;

    const-string v1, "/t_omnistore_sync"

    sget-object v2, LX/2I2;->ACKNOWLEDGED_DELIVERY:LX/2I2;

    invoke-virtual {v2}, LX/2I2;->getValue()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v1, LX/2C3;->ALWAYS:LX/2C3;

    new-instance v2, LX/1se;

    const-string v3, "/t_omnistore_sync_low_pri"

    sget-object v4, LX/2I2;->ACKNOWLEDGED_DELIVERY:LX/2I2;

    invoke-virtual {v4}, LX/2I2;->getValue()I

    move-result v4

    invoke-direct {v2, v3, v4}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v3, LX/2C3;->ALWAYS:LX/2C3;

    invoke-static {v0, v1, v2, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method
