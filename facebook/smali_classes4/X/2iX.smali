.class public final LX/2iX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2iY;


# instance fields
.field public final synthetic a:LX/2iK;


# direct methods
.method public constructor <init>(LX/2iK;)V
    .locals 0

    .prologue
    .line 451788
    iput-object p1, p0, LX/2iX;->a:LX/2iK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/friends/model/FriendRequest;)V
    .locals 8

    .prologue
    .line 451789
    iget-object v0, p0, LX/2iX;->a:LX/2iK;

    iget-object v0, v0, LX/2iK;->e:LX/2iU;

    const/4 v6, 0x0

    .line 451790
    iget-object v1, v0, LX/2iU;->d:LX/2hs;

    invoke-virtual {v1}, LX/2hs;->d()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 451791
    invoke-static {v0}, LX/2iU;->a(LX/2iU;)V

    .line 451792
    invoke-virtual {p1}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v3

    .line 451793
    iget-object v2, v0, LX/2iU;->a:LX/2dj;

    sget-object v5, LX/2h8;->SUGGESTION:LX/2h8;

    move-object v7, v6

    invoke-virtual/range {v2 .. v7}, LX/2dj;->b(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 451794
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v5, 0x1

    invoke-static {v0, v3, v4, v2, v5}, LX/2iU;->a$redex0(LX/2iU;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 451795
    iget-object v2, v0, LX/2iU;->h:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "RESPOND_TO_FRIEND_SUGGEST_TASK"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/EyZ;

    invoke-direct {v6, v0, v3, v4, p1}, LX/EyZ;-><init>(LX/2iU;JLcom/facebook/friends/model/FriendRequest;)V

    invoke-virtual {v2, v5, v1, v6}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 451796
    return-void
.end method

.method public final b(Lcom/facebook/friends/model/FriendRequest;)V
    .locals 6

    .prologue
    .line 451797
    iget-object v0, p0, LX/2iX;->a:LX/2iK;

    iget-object v0, v0, LX/2iK;->e:LX/2iU;

    .line 451798
    iget-object v1, v0, LX/2iU;->d:LX/2hs;

    invoke-virtual {v1}, LX/2hs;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 451799
    invoke-static {v0}, LX/2iU;->a(LX/2iU;)V

    .line 451800
    iget-object v1, p1, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    move-object v1, v1

    .line 451801
    iget-object v2, v0, LX/2iU;->a:LX/2dj;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, LX/2dj;->a(J)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 451802
    iget-object v3, v0, LX/2iU;->g:LX/2iM;

    sget-object v4, LX/2lu;->REJECTED:LX/2lu;

    const/4 v5, 0x0

    invoke-interface {v3, v1, v4, v5}, LX/2iM;->a(Ljava/lang/String;LX/2lu;Z)V

    .line 451803
    iget-object v3, v0, LX/2iU;->h:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RESPOND_TO_FRIEND_SUGGEST_TASK"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v4, LX/Eya;

    invoke-direct {v4, v0, p1}, LX/Eya;-><init>(LX/2iU;Lcom/facebook/friends/model/FriendRequest;)V

    invoke-virtual {v3, v1, v2, v4}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 451804
    return-void
.end method
