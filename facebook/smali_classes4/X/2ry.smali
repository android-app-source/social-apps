.class public LX/2ry;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/String;

.field public E:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

.field public F:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public G:Ljava/lang/String;

.field public H:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public I:I

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:J

.field public h:Ljava/lang/String;

.field public i:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

.field public r:Ljava/lang/String;

.field public s:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

.field public t:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

.field public u:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

.field public v:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

.field public w:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

.field public x:D

.field public y:Ljava/lang/String;

.field public z:Lcom/facebook/productionprompts/model/PromptDisplayReason;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 472213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472214
    iput-object p1, p0, LX/2ry;->a:Ljava/lang/String;

    .line 472215
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472216
    iget-object v0, p0, LX/2ry;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472217
    iget-object v0, p0, LX/2ry;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472218
    iget-object v0, p0, LX/2ry;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472219
    iget-object v0, p0, LX/2ry;->D:Ljava/lang/String;

    return-object v0
.end method

.method public final E()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;
    .locals 1

    .prologue
    .line 472220
    iget-object v0, p0, LX/2ry;->E:Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    return-object v0
.end method

.method public final F()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 472221
    iget-object v0, p0, LX/2ry;->F:LX/0Px;

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472222
    iget-object v0, p0, LX/2ry;->G:Ljava/lang/String;

    return-object v0
.end method

.method public final H()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 472224
    iget-object v0, p0, LX/2ry;->H:LX/0Px;

    return-object v0
.end method

.method public final I()I
    .locals 1

    .prologue
    .line 472223
    iget v0, p0, LX/2ry;->I:I

    return v0
.end method

.method public final J()Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 5

    .prologue
    .line 472231
    iget-object v1, p0, LX/2ry;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 472232
    iget-wide v1, p0, LX/2ry;->g:J

    iget-wide v3, p0, LX/2ry;->f:J

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 472233
    invoke-static {p0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a(LX/2ry;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    return-object v0

    .line 472234
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472230
    iget-object v0, p0, LX/2ry;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472229
    iget-object v0, p0, LX/2ry;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472228
    iget-object v0, p0, LX/2ry;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472227
    iget-object v0, p0, LX/2ry;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472226
    iget-object v0, p0, LX/2ry;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 472225
    iget-wide v0, p0, LX/2ry;->f:J

    return-wide v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 472211
    iget-wide v0, p0, LX/2ry;->g:J

    return-wide v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472212
    iget-object v0, p0, LX/2ry;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 1

    .prologue
    .line 472193
    iget-object v0, p0, LX/2ry;->i:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472194
    iget-object v0, p0, LX/2ry;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472195
    iget-object v0, p0, LX/2ry;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472196
    iget-object v0, p0, LX/2ry;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472197
    iget-object v0, p0, LX/2ry;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472198
    iget-object v0, p0, LX/2ry;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472199
    iget-object v0, p0, LX/2ry;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472200
    iget-object v0, p0, LX/2ry;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Lcom/facebook/productionprompts/model/ProfilePictureOverlay;
    .locals 1

    .prologue
    .line 472201
    iget-object v0, p0, LX/2ry;->q:Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472202
    iget-object v0, p0, LX/2ry;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;
    .locals 1

    .prologue
    .line 472203
    iget-object v0, p0, LX/2ry;->s:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    return-object v0
.end method

.method public final t()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;
    .locals 1

    .prologue
    .line 472204
    iget-object v0, p0, LX/2ry;->t:Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel$SuggestedCompositionModel$EdgesModel$NodeModel$MaskEffectModel;

    return-object v0
.end method

.method public final u()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;
    .locals 1

    .prologue
    .line 472205
    iget-object v0, p0, LX/2ry;->u:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    return-object v0
.end method

.method public final v()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;
    .locals 1

    .prologue
    .line 472206
    iget-object v0, p0, LX/2ry;->v:Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    return-object v0
.end method

.method public final w()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;
    .locals 1

    .prologue
    .line 472207
    iget-object v0, p0, LX/2ry;->w:Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    return-object v0
.end method

.method public final x()D
    .locals 2

    .prologue
    .line 472208
    iget-wide v0, p0, LX/2ry;->x:D

    return-wide v0
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472209
    iget-object v0, p0, LX/2ry;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final z()Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .locals 1

    .prologue
    .line 472210
    iget-object v0, p0, LX/2ry;->z:Lcom/facebook/productionprompts/model/PromptDisplayReason;

    return-object v0
.end method
