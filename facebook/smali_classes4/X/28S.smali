.class public LX/28S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/29g;",
        "Lcom/facebook/auth/component/AuthenticationResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/2Xm;

.field private final b:LX/0hw;

.field private final c:LX/0dC;


# direct methods
.method public constructor <init>(LX/2Xm;LX/0hw;LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 374025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374026
    iput-object p1, p0, LX/28S;->a:LX/2Xm;

    .line 374027
    iput-object p2, p0, LX/28S;->b:LX/0hw;

    .line 374028
    iput-object p3, p0, LX/28S;->c:LX/0dC;

    .line 374029
    return-void
.end method

.method public static a(LX/0QB;)LX/28S;
    .locals 1

    .prologue
    .line 374030
    invoke-static {p0}, LX/28S;->b(LX/0QB;)LX/28S;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/28S;
    .locals 4

    .prologue
    .line 374031
    new-instance v3, LX/28S;

    invoke-static {p0}, LX/2Xm;->b(LX/0QB;)LX/2Xm;

    move-result-object v0

    check-cast v0, LX/2Xm;

    invoke-static {p0}, LX/0hw;->b(LX/0QB;)LX/0hw;

    move-result-object v1

    check-cast v1, LX/0hw;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v2

    check-cast v2, LX/0dC;

    invoke-direct {v3, v0, v1, v2}, LX/28S;-><init>(LX/2Xm;LX/0hw;LX/0dC;)V

    .line 374032
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 374033
    check-cast p1, LX/29g;

    .line 374034
    iget-object v0, p1, LX/29g;->a:Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    .line 374035
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 374036
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "adid"

    iget-object v3, p0, LX/28S;->b:LX/0hw;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, LX/0hw;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374037
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374038
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    iget-object v3, p0, LX/28S;->c:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374039
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "email"

    .line 374040
    iget-object v3, v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->a:Ljava/lang/String;

    move-object v3, v3

    .line 374041
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374042
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "password"

    .line 374043
    iget-object v3, v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->b:Ljava/lang/String;

    move-object v3, v3

    .line 374044
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374045
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "pin"

    .line 374046
    iget-object v3, v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->c:Ljava/lang/String;

    move-object v3, v3

    .line 374047
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374048
    iget-object v1, v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->d:LX/41B;

    move-object v1, v1

    .line 374049
    invoke-virtual {v1}, LX/41B;->getServerValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 374050
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "credentials_type"

    .line 374051
    iget-object v3, v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->d:LX/41B;

    move-object v0, v3

    .line 374052
    invoke-virtual {v0}, LX/41B;->getServerValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374053
    :cond_0
    iget-boolean v0, p1, LX/29g;->d:Z

    if-eqz v0, :cond_1

    .line 374054
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "generate_session_cookies"

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374055
    :cond_1
    iget-object v0, p1, LX/29g;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 374056
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "error_detail_type"

    iget-object v2, p1, LX/29g;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374057
    :cond_2
    iget-object v0, p1, LX/29g;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 374058
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "machine_id"

    iget-object v2, p1, LX/29g;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374059
    :goto_0
    iget-object v0, p1, LX/29g;->c:Landroid/location/Location;

    if-eqz v0, :cond_3

    .line 374060
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "login_latitude"

    iget-object v2, p1, LX/29g;->c:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374061
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "login_longitude"

    iget-object v2, p1, LX/29g;->c:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374062
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "login_location_accuracy_m"

    iget-object v2, p1, LX/29g;->c:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374063
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "login_location_timestamp_ms"

    iget-object v2, p1, LX/29g;->c:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374064
    :cond_3
    new-instance v0, LX/14N;

    sget-object v1, LX/11I;->AUTHENTICATE:LX/11I;

    iget-object v1, v1, LX/11I;->requestNameString:Ljava/lang/String;

    const-string v2, "POST"

    const-string v3, "method/auth.login"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 374065
    :cond_4
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "generate_machine_id"

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 374066
    check-cast p1, LX/29g;

    .line 374067
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 374068
    iget-object v0, p1, LX/29g;->a:Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;

    .line 374069
    iget-object v1, v0, Lcom/facebook/auth/credentials/DeviceBasedLoginCredentials;->a:Ljava/lang/String;

    move-object v0, v1

    .line 374070
    iget-object v1, p0, LX/28S;->a:LX/2Xm;

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v2

    iget-boolean v3, p1, LX/29g;->d:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2Xm;->a(LX/0lF;Ljava/lang/String;ZLjava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method
