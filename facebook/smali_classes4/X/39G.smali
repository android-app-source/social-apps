.class public LX/39G;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/39G;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 522475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 522476
    return-void
.end method

.method public static a(LX/0QB;)LX/39G;
    .locals 3

    .prologue
    .line 522477
    sget-object v0, LX/39G;->a:LX/39G;

    if-nez v0, :cond_1

    .line 522478
    const-class v1, LX/39G;

    monitor-enter v1

    .line 522479
    :try_start_0
    sget-object v0, LX/39G;->a:LX/39G;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 522480
    if-eqz v2, :cond_0

    .line 522481
    :try_start_1
    new-instance v0, LX/39G;

    invoke-direct {v0}, LX/39G;-><init>()V

    .line 522482
    move-object v0, v0

    .line 522483
    sput-object v0, LX/39G;->a:LX/39G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 522484
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 522485
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 522486
    :cond_1
    sget-object v0, LX/39G;->a:LX/39G;

    return-object v0

    .line 522487
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 522488
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/Integer;Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;)V
    .locals 1

    .prologue
    .line 522489
    invoke-virtual {p2, p0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setReactorsCount(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 522490
    invoke-static {p0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setCommentsCount(I)V

    .line 522491
    invoke-static {p0}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSharesCount(I)V

    .line 522492
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setViewsCount(I)V

    .line 522493
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setProfileVideoViewsCount(Ljava/lang/String;)V

    .line 522494
    return-void
.end method
