.class public LX/2Ei;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final d:Ljava/lang/String;

.field private static volatile s:LX/2Ei;


# instance fields
.field public a:LX/2EG;

.field public b:F

.field public c:Lcom/facebook/proxygen/RadioStatusMonitor;

.field public final e:LX/0Uh;

.field public final f:LX/1BA;

.field public final g:LX/0u7;

.field private final h:LX/1r1;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ee8;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/03V;

.field public final k:LX/Ee3;

.field private l:Lcom/facebook/common/perftest/PerfTestConfig;

.field public m:Z

.field private n:LX/2Ej;

.field public o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1ql;",
            ">;"
        }
    .end annotation
.end field

.field private p:J

.field public q:Landroid/content/Context;

.field public r:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 385884
    const-class v0, LX/2Ei;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Ei;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Uh;LX/1BA;LX/0u7;LX/1r1;Landroid/content/Context;LX/0Ot;LX/03V;Lcom/facebook/common/perftest/PerfTestConfig;)V
    .locals 12
    .param p5    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/localstats/LocalStatsLogger;",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            "LX/1r1;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/Ee8;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 385869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385870
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/2Ei;->m:Z

    .line 385871
    iput-object p1, p0, LX/2Ei;->e:LX/0Uh;

    .line 385872
    iput-object p2, p0, LX/2Ei;->f:LX/1BA;

    .line 385873
    iput-object p3, p0, LX/2Ei;->g:LX/0u7;

    .line 385874
    move-object/from16 v0, p4

    iput-object v0, p0, LX/2Ei;->h:LX/1r1;

    .line 385875
    move-object/from16 v0, p5

    iput-object v0, p0, LX/2Ei;->q:Landroid/content/Context;

    .line 385876
    move-object/from16 v0, p6

    iput-object v0, p0, LX/2Ei;->i:LX/0Ot;

    .line 385877
    move-object/from16 v0, p7

    iput-object v0, p0, LX/2Ei;->j:LX/03V;

    .line 385878
    move-object/from16 v0, p8

    iput-object v0, p0, LX/2Ei;->l:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 385879
    new-instance v3, LX/2EG;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    invoke-direct/range {v3 .. v11}, LX/2EG;-><init>(DDDD)V

    iput-object v3, p0, LX/2Ei;->a:LX/2EG;

    .line 385880
    invoke-static {}, LX/2Ej;->a()LX/2Ej;

    move-result-object v2

    iput-object v2, p0, LX/2Ei;->n:LX/2Ej;

    .line 385881
    const-wide/16 v2, 0x0

    iput-wide v2, p0, LX/2Ei;->p:J

    .line 385882
    iget-object v2, p0, LX/2Ei;->q:Landroid/content/Context;

    invoke-static {v2}, LX/Ee3;->a(Landroid/content/Context;)LX/Ee3;

    move-result-object v2

    iput-object v2, p0, LX/2Ei;->k:LX/Ee3;

    .line 385883
    return-void
.end method

.method private static a(LX/1ql;JJ)LX/0m9;
    .locals 5

    .prologue
    .line 385700
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 385701
    const-string v0, "tag"

    .line 385702
    iget-object v1, p0, LX/1ql;->d:Ljava/lang/String;

    move-object v1, v1

    .line 385703
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 385704
    const-string v0, "held_time_ms"

    invoke-virtual {v2, v0, p1, p2}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 385705
    const-string v0, "count"

    invoke-virtual {v2, v0, p3, p4}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 385706
    const-string v0, "flags"

    .line 385707
    iget v1, p0, LX/1ql;->g:I

    move v1, v1

    .line 385708
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 385709
    const-string v0, "is_ref_counted"

    invoke-virtual {p0}, LX/1ql;->f()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 385710
    invoke-virtual {p0}, LX/1ql;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 385711
    new-instance v3, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 385712
    invoke-virtual {p0}, LX/1ql;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 385713
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v3, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Long;)LX/0m9;

    goto :goto_0

    .line 385714
    :cond_0
    const-string v0, "extra"

    invoke-virtual {v2, v0, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 385715
    :cond_1
    return-object v2
.end method

.method public static a(LX/0QB;)LX/2Ei;
    .locals 12

    .prologue
    .line 385856
    sget-object v0, LX/2Ei;->s:LX/2Ei;

    if-nez v0, :cond_1

    .line 385857
    const-class v1, LX/2Ei;

    monitor-enter v1

    .line 385858
    :try_start_0
    sget-object v0, LX/2Ei;->s:LX/2Ei;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 385859
    if-eqz v2, :cond_0

    .line 385860
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 385861
    new-instance v3, LX/2Ei;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v5

    check-cast v5, LX/1BA;

    invoke-static {v0}, LX/0u7;->a(LX/0QB;)LX/0u7;

    move-result-object v6

    check-cast v6, LX/0u7;

    invoke-static {v0}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v7

    check-cast v7, LX/1r1;

    const-class v8, Landroid/content/Context;

    const-class v9, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v8, v9}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    const/16 v9, 0x174b

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v11

    check-cast v11, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct/range {v3 .. v11}, LX/2Ei;-><init>(LX/0Uh;LX/1BA;LX/0u7;LX/1r1;Landroid/content/Context;LX/0Ot;LX/03V;Lcom/facebook/common/perftest/PerfTestConfig;)V

    .line 385862
    move-object v0, v3

    .line 385863
    sput-object v0, LX/2Ei;->s:LX/2Ei;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 385864
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 385865
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 385866
    :cond_1
    sget-object v0, LX/2Ei;->s:LX/2Ei;

    return-object v0

    .line 385867
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 385868
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2Ei;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 385852
    :try_start_0
    iget-object v0, p0, LX/2Ei;->f:LX/1BA;

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, LX/1BA;->a(IJ)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 385853
    :goto_0
    return-void

    .line 385854
    :catch_0
    move-exception v0

    .line 385855
    sget-object v1, LX/2Ei;->d:Ljava/lang/String;

    const-string v2, "Invalid long value: %s for key: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 15

    .prologue
    const/4 v4, 0x0

    .line 385804
    invoke-static {}, LX/2Ej;->a()LX/2Ej;

    move-result-object v6

    .line 385805
    iget-object v0, p0, LX/2Ei;->n:LX/2Ej;

    const/4 v5, 0x0

    .line 385806
    if-eqz v6, :cond_0

    if-nez v0, :cond_8

    .line 385807
    :cond_0
    const/4 v1, 0x0

    .line 385808
    :goto_0
    move-object v0, v1

    .line 385809
    if-nez v0, :cond_1

    .line 385810
    :goto_1
    return-void

    .line 385811
    :cond_1
    invoke-virtual {v0}, LX/2Ej;->b()[Landroid/util/SparseIntArray;

    move-result-object v7

    .line 385812
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    move v5, v4

    .line 385813
    :goto_2
    :try_start_0
    array-length v0, v7

    if-ge v5, v0, :cond_6

    .line 385814
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 385815
    aget-object v10, v7, v5

    .line 385816
    invoke-virtual {v10}, Landroid/util/SparseIntArray;->size()I

    move-result v11

    move v2, v4

    move v1, v4

    move v3, v4

    .line 385817
    :goto_3
    if-ge v2, v11, :cond_2

    .line 385818
    invoke-virtual {v10, v2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    .line 385819
    if-ge v1, v0, :cond_7

    move v1, v2

    .line 385820
    :goto_4
    add-int/lit8 v2, v2, 0x1

    move v3, v1

    move v1, v0

    goto :goto_3

    :cond_2
    move v0, v4

    .line 385821
    :goto_5
    if-ge v0, v11, :cond_5

    .line 385822
    invoke-virtual {v10, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v1

    .line 385823
    if-nez v1, :cond_3

    if-ne v0, v3, :cond_4

    .line 385824
    :cond_3
    invoke-virtual {v10, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 385825
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 385826
    :cond_5
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 385827
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    .line 385828
    :cond_6
    iget-object v0, p0, LX/2Ei;->f:LX/1BA;

    const v1, 0x6e003d

    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1BA;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 385829
    :goto_6
    iput-object v6, p0, LX/2Ei;->n:LX/2Ej;

    goto :goto_1

    .line 385830
    :catch_0
    move-exception v0

    .line 385831
    iget-object v1, p0, LX/2Ei;->j:LX/03V;

    sget-object v2, LX/2Ei;->d:Ljava/lang/String;

    const-string v3, "Unable to save CPU time"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_7
    move v0, v1

    move v1, v3

    goto :goto_4

    .line 385832
    :cond_8
    new-instance v2, LX/2Ej;

    invoke-direct {v2}, LX/2Ej;-><init>()V

    .line 385833
    iget-object v1, v6, LX/2Ej;->d:[Landroid/util/SparseIntArray;

    array-length v8, v1

    .line 385834
    new-array v1, v8, [Landroid/util/SparseIntArray;

    iput-object v1, v2, LX/2Ej;->d:[Landroid/util/SparseIntArray;

    move v7, v5

    .line 385835
    :goto_7
    if-ge v7, v8, :cond_d

    .line 385836
    iget-object v1, v6, LX/2Ej;->d:[Landroid/util/SparseIntArray;

    aget-object v9, v1, v7

    .line 385837
    iget-object v1, v0, LX/2Ej;->d:[Landroid/util/SparseIntArray;

    aget-object v10, v1, v7

    .line 385838
    if-eqz v9, :cond_9

    .line 385839
    if-nez v10, :cond_a

    .line 385840
    iget-object v1, v2, LX/2Ej;->d:[Landroid/util/SparseIntArray;

    aput-object v9, v1, v7

    .line 385841
    :cond_9
    :goto_8
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_7

    .line 385842
    :cond_a
    invoke-virtual {v9}, Landroid/util/SparseIntArray;->size()I

    move-result v11

    .line 385843
    new-instance v12, Landroid/util/SparseIntArray;

    invoke-direct {v12, v11}, Landroid/util/SparseIntArray;-><init>(I)V

    move v3, v5

    .line 385844
    :goto_9
    if-ge v3, v11, :cond_c

    .line 385845
    invoke-virtual {v9, v3}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v13

    .line 385846
    invoke-virtual {v9, v3}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v1

    .line 385847
    invoke-virtual {v10, v13, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v14

    .line 385848
    if-lt v1, v14, :cond_b

    sub-int/2addr v1, v14

    :cond_b
    invoke-virtual {v12, v13, v1}, Landroid/util/SparseIntArray;->append(II)V

    .line 385849
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_9

    .line 385850
    :cond_c
    iget-object v1, v2, LX/2Ej;->d:[Landroid/util/SparseIntArray;

    aput-object v12, v1, v7

    goto :goto_8

    :cond_d
    move-object v1, v2

    .line 385851
    goto/16 :goto_0
.end method

.method public final d()V
    .locals 15

    .prologue
    const v8, 0x6e002e

    const v7, 0x6e0008

    const v6, 0x6e0007

    const v5, 0x6e0006

    const v4, 0x6e0005

    .line 385764
    invoke-static {}, Lcom/facebook/proxygen/RadioStatusMonitor;->getMonitorInstance()Lcom/facebook/proxygen/RadioStatusMonitor;

    move-result-object v0

    iput-object v0, p0, LX/2Ei;->c:Lcom/facebook/proxygen/RadioStatusMonitor;

    .line 385765
    iget-object v0, p0, LX/2Ei;->c:Lcom/facebook/proxygen/RadioStatusMonitor;

    if-eqz v0, :cond_2

    .line 385766
    iget-object v0, p0, LX/2Ei;->c:Lcom/facebook/proxygen/RadioStatusMonitor;

    invoke-virtual {v0}, Lcom/facebook/proxygen/RadioStatusMonitor;->getRadioData()Ljava/util/HashMap;

    move-result-object v1

    .line 385767
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 385768
    :cond_0
    :goto_0
    return-void

    .line 385769
    :cond_1
    const v2, 0x6e0037

    const-string v0, "full_power_time"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v2, v0}, LX/2Ei;->a(LX/2Ei;ILjava/lang/String;)V

    .line 385770
    const v2, 0x6e0038

    const-string v0, "low_power_time"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v2, v0}, LX/2Ei;->a(LX/2Ei;ILjava/lang/String;)V

    .line 385771
    const-string v0, "total_up_bytes"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v6, v0}, LX/2Ei;->a(LX/2Ei;ILjava/lang/String;)V

    .line 385772
    const-string v0, "total_down_bytes"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v7, v0}, LX/2Ei;->a(LX/2Ei;ILjava/lang/String;)V

    .line 385773
    const-string v0, "total_request_count"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v5, v0}, LX/2Ei;->a(LX/2Ei;ILjava/lang/String;)V

    .line 385774
    const-string v0, "total_wakeup_count"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v4, v0}, LX/2Ei;->a(LX/2Ei;ILjava/lang/String;)V

    .line 385775
    iget-object v2, p0, LX/2Ei;->f:LX/1BA;

    const-string v0, "aggr_data_details"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v8, v0}, LX/1BA;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 385776
    :cond_2
    sget-object v0, LX/07M;->a:LX/07M;

    move-object v0, v0

    .line 385777
    iget-object v1, p0, LX/2Ei;->f:LX/1BA;

    .line 385778
    iget-object v9, v0, LX/07M;->c:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v11, 0x0

    invoke-virtual {v9, v11, v12}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    move-result-wide v9

    move-wide v2, v9

    .line 385779
    invoke-virtual {v1, v4, v2, v3}, LX/1BA;->a(IJ)V

    .line 385780
    iget-object v1, p0, LX/2Ei;->f:LX/1BA;

    .line 385781
    iget-object v9, v0, LX/07M;->b:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v11, 0x0

    invoke-virtual {v9, v11, v12}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    move-result-wide v9

    move-wide v2, v9

    .line 385782
    invoke-virtual {v1, v5, v2, v3}, LX/1BA;->a(IJ)V

    .line 385783
    iget-object v1, p0, LX/2Ei;->f:LX/1BA;

    .line 385784
    iget-object v9, v0, LX/07M;->d:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v11, 0x0

    invoke-virtual {v9, v11, v12}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    move-result-wide v9

    move-wide v2, v9

    .line 385785
    invoke-virtual {v1, v6, v2, v3}, LX/1BA;->a(IJ)V

    .line 385786
    iget-object v1, p0, LX/2Ei;->f:LX/1BA;

    .line 385787
    iget-object v9, v0, LX/07M;->e:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v11, 0x0

    invoke-virtual {v9, v11, v12}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    move-result-wide v9

    move-wide v2, v9

    .line 385788
    invoke-virtual {v1, v7, v2, v3}, LX/1BA;->a(IJ)V

    .line 385789
    new-instance v2, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v1}, LX/162;-><init>(LX/0mC;)V

    .line 385790
    invoke-virtual {v0}, LX/07M;->f()Ljava/util/Map;

    move-result-object v0

    .line 385791
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 385792
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/07N;

    .line 385793
    new-instance v9, LX/0m9;

    sget-object v10, LX/0mC;->a:LX/0mC;

    invoke-direct {v9, v10}, LX/0m9;-><init>(LX/0mC;)V

    .line 385794
    const-string v10, "comp"

    invoke-virtual {v9, v10, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 385795
    const-string v10, "wakeups"

    .line 385796
    iget-wide v13, v0, LX/07N;->b:J

    move-wide v11, v13

    .line 385797
    invoke-virtual {v9, v10, v11, v12}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 385798
    const-string v10, "requests"

    .line 385799
    iget-wide v13, v0, LX/07N;->a:J

    move-wide v11, v13

    .line 385800
    invoke-virtual {v9, v10, v11, v12}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 385801
    move-object v0, v9

    .line 385802
    invoke-virtual {v2, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_1

    .line 385803
    :cond_3
    iget-object v0, p0, LX/2Ei;->f:LX/1BA;

    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v8, v1}, LX/1BA;->a(ILjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final e()V
    .locals 15

    .prologue
    const-wide/16 v4, 0x0

    .line 385739
    new-instance v12, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v12, v0}, LX/162;-><init>(LX/0mC;)V

    .line 385740
    iget-object v0, p0, LX/2Ei;->h:LX/1r1;

    .line 385741
    iget-object v1, v0, LX/1r1;->d:Ljava/util/Map;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    move-object v13, v1

    .line 385742
    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move-wide v2, v4

    move-wide v6, v4

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ql;

    .line 385743
    iget-object v1, v0, LX/1ql;->e:Ljava/lang/String;

    move-object v1, v1

    .line 385744
    iget-object v8, p0, LX/2Ei;->o:Ljava/util/Map;

    if-nez v8, :cond_3

    const/4 v8, 0x0

    .line 385745
    :goto_1
    move-object v1, v8

    .line 385746
    if-eqz v1, :cond_1

    .line 385747
    invoke-virtual {v0}, LX/1ql;->i()J

    move-result-wide v8

    invoke-virtual {v1}, LX/1ql;->i()J

    move-result-wide v10

    sub-long v10, v8, v10

    .line 385748
    invoke-virtual {v0}, LX/1ql;->j()I

    move-result v8

    invoke-virtual {v1}, LX/1ql;->j()I

    move-result v1

    sub-int v1, v8, v1

    int-to-long v8, v1

    .line 385749
    :goto_2
    cmp-long v1, v10, v4

    if-lez v1, :cond_0

    .line 385750
    invoke-static {v0, v10, v11, v8, v9}, LX/2Ei;->a(LX/1ql;JJ)LX/0m9;

    move-result-object v0

    invoke-virtual {v12, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 385751
    :cond_0
    add-long/2addr v6, v10

    .line 385752
    add-long v0, v2, v8

    move-wide v2, v0

    .line 385753
    goto :goto_0

    .line 385754
    :cond_1
    invoke-virtual {v0}, LX/1ql;->i()J

    move-result-wide v10

    .line 385755
    invoke-virtual {v0}, LX/1ql;->j()I

    move-result v1

    int-to-long v8, v1

    goto :goto_2

    .line 385756
    :cond_2
    iget-object v0, p0, LX/2Ei;->f:LX/1BA;

    const v1, 0x6e0013

    invoke-virtual {v0, v1, v2, v3}, LX/1BA;->a(IJ)V

    .line 385757
    iget-object v0, p0, LX/2Ei;->f:LX/1BA;

    const v1, 0x6e0012

    invoke-virtual {v0, v1, v6, v7}, LX/1BA;->a(IJ)V

    .line 385758
    iget-object v0, p0, LX/2Ei;->f:LX/1BA;

    const v1, 0x6e0014

    invoke-virtual {v12}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1BA;->a(ILjava/lang/String;)V

    .line 385759
    iput-object v13, p0, LX/2Ei;->o:Ljava/util/Map;

    .line 385760
    iget-object v0, p0, LX/2Ei;->h:LX/1r1;

    invoke-virtual {v0}, LX/1r1;->b()J

    move-result-wide v0

    .line 385761
    iget-object v2, p0, LX/2Ei;->f:LX/1BA;

    const v3, 0x6e0015

    iget-wide v4, p0, LX/2Ei;->p:J

    sub-long v4, v0, v4

    invoke-virtual {v2, v3, v4, v5}, LX/1BA;->a(IJ)V

    .line 385762
    iput-wide v0, p0, LX/2Ei;->p:J

    .line 385763
    return-void

    :cond_3
    iget-object v8, p0, LX/2Ei;->o:Ljava/util/Map;

    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1ql;

    goto :goto_1
.end method

.method public final init()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 385716
    const/4 v0, 0x0

    .line 385717
    iget-object v1, p0, LX/2Ei;->e:LX/0Uh;

    const/16 v3, 0x5c7

    invoke-virtual {v1, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 385718
    if-nez v0, :cond_3

    .line 385719
    sget-object v0, LX/0Bx;->c:LX/0By;

    invoke-static {v0}, LX/0Bx;->b(LX/0Bw;)V

    .line 385720
    :cond_2
    :goto_0
    return-void

    .line 385721
    :cond_3
    :try_start_0
    const-string v3, "com.android.internal.os.PowerProfile"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/content/Context;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/2Ei;->q:Landroid/content/Context;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 385722
    const-string v4, "com.android.internal.os.PowerProfile"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v5, "getAveragePower"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "battery.capacity"

    aput-object v7, v5, v6

    invoke-virtual {v4, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    .line 385723
    double-to-int v3, v3

    .line 385724
    :goto_1
    move v0, v3

    .line 385725
    iput v0, p0, LX/2Ei;->r:I

    .line 385726
    iget-object v0, p0, LX/2Ei;->f:LX/1BA;

    new-instance v1, LX/2FF;

    invoke-direct {v1, p0}, LX/2FF;-><init>(LX/2Ei;)V

    invoke-virtual {v0, v1}, LX/1BA;->a(LX/1iq;)V

    .line 385727
    iput-boolean v2, p0, LX/2Ei;->m:Z

    .line 385728
    iget-object v0, p0, LX/2Ei;->g:LX/0u7;

    invoke-virtual {v0}, LX/0u7;->a()F

    move-result v0

    iput v0, p0, LX/2Ei;->b:F

    .line 385729
    iget-object v0, p0, LX/2Ei;->g:LX/0u7;

    new-instance v1, LX/2FK;

    invoke-direct {v1, p0}, LX/2FK;-><init>(LX/2Ei;)V

    invoke-virtual {v0, v1}, LX/0u7;->a(LX/2Zz;)V

    .line 385730
    iget-object v0, p0, LX/2Ei;->e:LX/0Uh;

    const/16 v1, 0x245

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 385731
    iget-object v0, p0, LX/2Ei;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ee8;

    .line 385732
    iget-object v1, v0, LX/Ee8;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Ee8;->b:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 385733
    :goto_2
    goto/16 :goto_0

    .line 385734
    :catch_0
    move-exception v3

    .line 385735
    sget-object v4, LX/2Ei;->d:Ljava/lang/String;

    const-string v5, "Error getting battery capacity"

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 385736
    const/4 v3, -0x1

    goto :goto_1

    .line 385737
    :cond_4
    iget-object v1, v0, LX/Ee8;->h:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v3, LX/Ee7;

    invoke-direct {v3, v0}, LX/Ee7;-><init>(LX/Ee8;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    iget-object v2, v0, LX/Ee8;->d:Landroid/os/Handler;

    invoke-interface {v1, v2}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, v0, LX/Ee8;->i:LX/0Yb;

    .line 385738
    iget-object v1, v0, LX/Ee8;->i:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    goto :goto_2
.end method
