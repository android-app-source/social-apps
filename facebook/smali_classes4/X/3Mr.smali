.class public final enum LX/3Mr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3Mr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3Mr;

.field public static final enum FILTERING:LX/3Mr;

.field public static final enum FINISHED:LX/3Mr;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 556402
    new-instance v0, LX/3Mr;

    const-string v1, "FILTERING"

    invoke-direct {v0, v1, v2}, LX/3Mr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Mr;->FILTERING:LX/3Mr;

    .line 556403
    new-instance v0, LX/3Mr;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v3}, LX/3Mr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3Mr;->FINISHED:LX/3Mr;

    .line 556404
    const/4 v0, 0x2

    new-array v0, v0, [LX/3Mr;

    sget-object v1, LX/3Mr;->FILTERING:LX/3Mr;

    aput-object v1, v0, v2

    sget-object v1, LX/3Mr;->FINISHED:LX/3Mr;

    aput-object v1, v0, v3

    sput-object v0, LX/3Mr;->$VALUES:[LX/3Mr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 556407
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3Mr;
    .locals 1

    .prologue
    .line 556406
    const-class v0, LX/3Mr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3Mr;

    return-object v0
.end method

.method public static values()[LX/3Mr;
    .locals 1

    .prologue
    .line 556405
    sget-object v0, LX/3Mr;->$VALUES:[LX/3Mr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3Mr;

    return-object v0
.end method
