.class public final LX/3I3;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/3JC;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Hz;


# direct methods
.method public constructor <init>(LX/3Hz;)V
    .locals 0

    .prologue
    .line 545474
    iput-object p1, p0, LX/3I3;->a:LX/3Hz;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/3JC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 545475
    const-class v0, LX/3JC;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 545476
    check-cast p1, LX/3JC;

    .line 545477
    iget-object v0, p0, LX/3I3;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->c:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/3I3;->a:LX/3Hz;

    iget-object v1, v1, LX/3Hz;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v0

    .line 545478
    iget-object v1, v0, LX/D6v;->w:LX/3H0;

    move-object v0, v1

    .line 545479
    sget-object v1, LX/3H0;->NONLIVE_POST_ROLL:LX/3H0;

    if-ne v0, v1, :cond_0

    .line 545480
    iget-object v0, p0, LX/3I3;->a:LX/3Hz;

    const/4 v1, 0x1

    .line 545481
    iput-boolean v1, v0, LX/3Hz;->D:Z

    .line 545482
    :cond_0
    sget-object v0, LX/D7H;->b:[I

    iget-object v1, p1, LX/3JC;->a:LX/2qV;

    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 545483
    :cond_1
    :goto_0
    return-void

    .line 545484
    :pswitch_0
    iget-object v0, p0, LX/3I3;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->z:LX/D6v;

    if-eqz v0, :cond_2

    .line 545485
    iget-object v0, p0, LX/3I3;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->z:LX/D6v;

    sget-object v1, LX/BSR;->ERROR:LX/BSR;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/BSR;)V

    .line 545486
    :cond_2
    iget-object v0, p0, LX/3I3;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->e:LX/03V;

    sget-object v1, LX/3Hz;->q:Ljava/lang/String;

    const-string v2, "Commercial break RVP playback error in VOD"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 545487
    :pswitch_1
    iget-object v0, p0, LX/3I3;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->z:LX/D6v;

    if-eqz v0, :cond_1

    .line 545488
    iget-object v0, p0, LX/3I3;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->z:LX/D6v;

    invoke-virtual {v0}, LX/D6v;->c()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
