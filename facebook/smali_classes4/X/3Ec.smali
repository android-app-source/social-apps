.class public LX/3Ec;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/3Ed;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/3Ed;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/3Ed;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 537888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537889
    iput-object p1, p0, LX/3Ec;->a:Landroid/content/Context;

    .line 537890
    iput-object p2, p0, LX/3Ec;->b:LX/0Or;

    .line 537891
    iput-object p3, p0, LX/3Ec;->c:LX/3Ed;

    .line 537892
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ec;
    .locals 1

    .prologue
    .line 537887
    invoke-static {p0}, LX/3Ec;->b(LX/0QB;)LX/3Ec;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3Ec;
    .locals 4

    .prologue
    .line 537893
    new-instance v2, LX/3Ec;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 v1, 0x19e

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/3Ed;->a(LX/0QB;)LX/3Ed;

    move-result-object v1

    check-cast v1, LX/3Ed;

    invoke-direct {v2, v0, v3, v1}, LX/3Ec;-><init>(Landroid/content/Context;LX/0Or;LX/3Ed;)V

    .line 537894
    return-object v2
.end method


# virtual methods
.method public final a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 3

    .prologue
    .line 537883
    iget-object v0, p0, LX/3Ec;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 537884
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 537885
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 537886
    invoke-static {p1, p2, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 2

    .prologue
    .line 537871
    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v0

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v0, v1, :cond_0

    .line 537872
    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/3Ec;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 537873
    :goto_0
    return-object v0

    .line 537874
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v0

    sget-object v1, LX/0XG;->PHONE_NUMBER:LX/0XG;

    if-ne v0, v1, :cond_1

    .line 537875
    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->g()Ljava/lang/String;

    move-result-object v0

    .line 537876
    iget-object v1, p0, LX/3Ec;->a:Landroid/content/Context;

    invoke-static {v1, v0}, LX/5e6;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    .line 537877
    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0

    .line 537878
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v0

    sget-object v1, LX/0XG;->EMAIL:LX/0XG;

    if-ne v0, v1, :cond_2

    .line 537879
    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->h()Ljava/lang/String;

    move-result-object v0

    .line 537880
    iget-object v1, p0, LX/3Ec;->a:Landroid/content/Context;

    invoke-static {v1, v0}, LX/5e6;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    .line 537881
    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0

    .line 537882
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported UserKey type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 3

    .prologue
    .line 537867
    iget-object v0, p0, LX/3Ec;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 537868
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 537869
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 537870
    invoke-static {p1, p2, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    return-object v0
.end method
