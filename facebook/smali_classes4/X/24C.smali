.class public LX/24C;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0SG;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(LX/0SG;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 366826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366827
    iput-object p1, p0, LX/24C;->a:LX/0SG;

    .line 366828
    iput-object p2, p0, LX/24C;->b:LX/03V;

    .line 366829
    return-void
.end method

.method private a(LX/82K;LX/8sk;)J
    .locals 5

    .prologue
    .line 366788
    iget-wide v0, p1, LX/82K;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 366789
    :goto_0
    if-eqz v0, :cond_1

    iget-wide v0, p1, LX/82K;->b:J

    .line 366790
    :goto_1
    iput-wide v0, p1, LX/82K;->b:J

    .line 366791
    iput-wide v0, p2, LX/8sk;->d:J

    .line 366792
    return-wide v0

    .line 366793
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 366794
    :cond_1
    iget-object v0, p0, LX/24C;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    goto :goto_1
.end method

.method private static a(LX/8sk;Ljava/util/ArrayList;)J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8sk;",
            "Ljava/util/ArrayList",
            "<",
            "LX/8sj;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 366820
    const-wide/16 v2, 0x0

    .line 366821
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8sj;

    .line 366822
    invoke-interface {v0}, LX/8sj;->a()J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 366823
    iget-object v5, p0, LX/8sk;->b:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366824
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 366825
    :cond_0
    return-wide v2
.end method


# virtual methods
.method public final a(LX/0jW;LX/1Pr;Ljava/lang/String;)LX/82L;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0jW;",
            "LX/1Pr;",
            "Ljava/lang/String;",
            ")",
            "LX/82L",
            "<TD;>;"
        }
    .end annotation

    .prologue
    .line 366817
    new-instance v0, LX/82J;

    invoke-direct {v0, p0, p3}, LX/82J;-><init>(LX/24C;Ljava/lang/String;)V

    move-object v0, v0

    .line 366818
    invoke-interface {p2, v0, p1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/82K;

    .line 366819
    new-instance v1, LX/82L;

    invoke-direct {v1, v0}, LX/82L;-><init>(LX/82K;)V

    return-object v1
.end method

.method public final a(LX/82L;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/82L",
            "<TD;>;)V"
        }
    .end annotation

    .prologue
    .line 366808
    iget-object v0, p1, LX/82L;->b:LX/8sm;

    if-nez v0, :cond_0

    .line 366809
    iget-object v0, p0, LX/24C;->b:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "T7742735: unbind() called without matching bind()"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 366810
    :goto_0
    return-void

    .line 366811
    :cond_0
    iget-object v0, p1, LX/82L;->b:LX/8sm;

    .line 366812
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8sm;->g:Z

    .line 366813
    iget-object v1, v0, LX/8sm;->e:Landroid/animation/Animator;

    if-eqz v1, :cond_1

    .line 366814
    iget-object v1, v0, LX/8sm;->e:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    .line 366815
    :cond_1
    invoke-static {v0}, LX/8sm;->c$redex0(LX/8sm;)V

    .line 366816
    const/4 v0, 0x0

    iput-object v0, p1, LX/82L;->b:LX/8sm;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/24J;LX/82L;Landroid/view/View;)V
    .locals 16
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;TD;",
            "LX/24J",
            "<TD;TV;>;",
            "LX/82L",
            "<TD;>;TV;)V"
        }
    .end annotation

    .prologue
    .line 366795
    move-object/from16 v0, p4

    iget-object v14, v0, LX/82L;->a:LX/82K;

    .line 366796
    iget-object v4, v14, LX/82K;->a:Ljava/lang/Object;

    if-nez v4, :cond_1

    .line 366797
    if-nez p1, :cond_0

    move-object/from16 p1, p2

    :cond_0
    move-object/from16 v0, p1

    iput-object v0, v14, LX/82K;->a:Ljava/lang/Object;

    .line 366798
    :cond_1
    iget-object v7, v14, LX/82K;->a:Ljava/lang/Object;

    .line 366799
    move-object/from16 v0, p0

    iget-object v4, v0, LX/24C;->a:LX/0SG;

    invoke-static {v4}, LX/8sk;->a(LX/0SG;)LX/8sk;

    move-result-object v15

    .line 366800
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, LX/24C;->a(LX/82K;LX/8sk;)J

    move-result-wide v10

    .line 366801
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 366802
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    invoke-interface {v0, v4, v7, v1, v2}, LX/24J;->a(Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)V

    .line 366803
    invoke-static {v15, v4}, LX/24C;->a(LX/8sk;Ljava/util/ArrayList;)J

    move-result-wide v12

    .line 366804
    new-instance v4, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;

    move-object/from16 v5, p0

    move-object/from16 v6, p3

    move-object/from16 v8, p2

    move-object/from16 v9, p5

    invoke-direct/range {v4 .. v14}, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;-><init>(LX/24C;LX/24J;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;JJLX/82K;)V

    invoke-virtual {v15, v4}, LX/8sk;->a(Ljava/lang/Runnable;)LX/8sk;

    .line 366805
    invoke-virtual {v15}, LX/8sk;->a()LX/8sm;

    move-result-object v4

    move-object/from16 v0, p4

    iput-object v4, v0, LX/82L;->b:LX/8sm;

    .line 366806
    move-object/from16 v0, p4

    iget-object v4, v0, LX/82L;->b:LX/8sm;

    invoke-virtual {v4}, LX/8sm;->a()V

    .line 366807
    return-void
.end method
