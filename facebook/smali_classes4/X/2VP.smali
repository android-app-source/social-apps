.class public LX/2VP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2VQ;


# direct methods
.method public constructor <init>(LX/2VQ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 417668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417669
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2VQ;

    iput-object v0, p0, LX/2VP;->a:LX/2VQ;

    .line 417670
    return-void
.end method

.method public static a(LX/0QB;)LX/2VP;
    .locals 1

    .prologue
    .line 417671
    invoke-static {p0}, LX/2VP;->b(LX/0QB;)LX/2VP;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2VP;
    .locals 2

    .prologue
    .line 417672
    new-instance v1, LX/2VP;

    invoke-static {p0}, LX/2VQ;->a(LX/0QB;)LX/2VQ;

    move-result-object v0

    check-cast v0, LX/2VQ;

    invoke-direct {v1, v0}, LX/2VP;-><init>(LX/2VQ;)V

    .line 417673
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/util/Collection;Ljava/lang/Iterable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 417674
    if-nez p1, :cond_0

    .line 417675
    :goto_0
    return-void

    .line 417676
    :cond_0
    iget-object v0, p0, LX/2VP;->a:LX/2VQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 417677
    const v0, -0x75f4d6df

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 417678
    :try_start_0
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 417679
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 417680
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 417681
    sget-object p0, LX/2VT;->a:LX/0U1;

    .line 417682
    iget-object p1, p0, LX/0U1;->d:Ljava/lang/String;

    move-object p0, p1

    .line 417683
    invoke-virtual {v4, p0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 417684
    sget-object p0, LX/2VT;->b:LX/0U1;

    .line 417685
    iget-object p1, p0, LX/0U1;->d:Ljava/lang/String;

    move-object p0, p1

    .line 417686
    invoke-virtual {v4, p0, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 417687
    const-string p0, "metainfo"

    const/4 p1, 0x0

    const p2, -0x5b23f449

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {v2, p0, p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v4, -0x89714b5

    invoke-static {v4}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 417688
    goto :goto_1

    .line 417689
    :catchall_0
    move-exception v0

    const v1, -0x221fe432

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 417690
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 417691
    const v0, -0x4ba36ed8

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0
.end method
