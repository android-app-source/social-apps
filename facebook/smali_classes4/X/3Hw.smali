.class public final LX/3Hw;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/31H;


# direct methods
.method public constructor <init>(LX/31H;)V
    .locals 0

    .prologue
    .line 545239
    iput-object p1, p0, LX/3Hw;->a:LX/31H;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 545240
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 545241
    check-cast p1, LX/2ou;

    .line 545242
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v0, v0, LX/31H;->u:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    if-eqz v0, :cond_0

    .line 545243
    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v0, v0, LX/31H;->a:LX/3Hu;

    .line 545244
    iget-object v1, v0, LX/3Hu;->a:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/2mm;->m:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(LX/0c0;S)V

    .line 545245
    :cond_0
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_2

    .line 545246
    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v0, v0, LX/31H;->u:Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    if-eqz v0, :cond_1

    .line 545247
    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v1, p0, LX/3Hw;->a:LX/31H;

    iget-object v1, v1, LX/31H;->b:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v2, p0, LX/3Hw;->a:LX/31H;

    iget-object v2, v2, LX/31H;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v1

    .line 545248
    iput-object v1, v0, LX/31H;->t:LX/D6v;

    .line 545249
    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v0, v0, LX/31H;->t:LX/D6v;

    sget-object v1, LX/3H0;->NONLIVE_POST_ROLL:LX/3H0;

    .line 545250
    iput-object v1, v0, LX/D6v;->w:LX/3H0;

    .line 545251
    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-nez v0, :cond_3

    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    .line 545252
    :goto_0
    iget-object v1, p0, LX/3Hw;->a:LX/31H;

    iget-object v1, v1, LX/31H;->t:LX/D6v;

    .line 545253
    iput-object v0, v1, LX/D6v;->G:LX/04D;

    .line 545254
    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v0, v0, LX/31H;->t:LX/D6v;

    iget-object v1, p0, LX/3Hw;->a:LX/31H;

    iget-object v1, v1, LX/31H;->n:LX/3GF;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/3GF;)V

    .line 545255
    :cond_1
    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v0, v0, LX/31H;->t:LX/D6v;

    if-eqz v0, :cond_2

    .line 545256
    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v0, v0, LX/31H;->b:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/3Hw;->a:LX/31H;

    iget-object v1, v1, LX/31H;->s:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->b(Ljava/lang/String;I)LX/D6r;

    move-result-object v0

    sget-object v1, LX/D6r;->SUCCESS:LX/D6r;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v0, v0, LX/31H;->t:LX/D6v;

    iget-object v0, v0, LX/D6v;->a:LX/2oN;

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-ne v0, v1, :cond_2

    .line 545257
    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v0, v0, LX/31H;->t:LX/D6v;

    sget-object v1, LX/BSU;->NONLIVE_POST_ROLL:LX/BSU;

    invoke-virtual {v0, v1}, LX/D6v;->b(LX/BSU;)V

    .line 545258
    :cond_2
    return-void

    .line 545259
    :cond_3
    iget-object v0, p0, LX/3Hw;->a:LX/31H;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->s()LX/04D;

    move-result-object v0

    goto :goto_0
.end method
