.class public final LX/3Ac;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/3Ac;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3Ad;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CDr;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 525737
    const/4 v0, 0x0

    sput-object v0, LX/3Ac;->a:LX/3Ac;

    .line 525738
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/3Ac;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 525726
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 525727
    new-instance v0, LX/CDr;

    invoke-direct {v0}, LX/CDr;-><init>()V

    iput-object v0, p0, LX/3Ac;->c:LX/CDr;

    .line 525728
    return-void
.end method

.method public static c(LX/1De;)LX/3Ad;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 525729
    new-instance v1, LX/CDq;

    invoke-direct {v1}, LX/CDq;-><init>()V

    .line 525730
    sget-object v2, LX/3Ac;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Ad;

    .line 525731
    if-nez v2, :cond_0

    .line 525732
    new-instance v2, LX/3Ad;

    invoke-direct {v2}, LX/3Ad;-><init>()V

    .line 525733
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/3Ad;->a$redex0(LX/3Ad;LX/1De;IILX/CDq;)V

    .line 525734
    move-object v1, v2

    .line 525735
    move-object v0, v1

    .line 525736
    return-object v0
.end method

.method public static declared-synchronized q()LX/3Ac;
    .locals 2

    .prologue
    .line 525722
    const-class v1, LX/3Ac;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/3Ac;->a:LX/3Ac;

    if-nez v0, :cond_0

    .line 525723
    new-instance v0, LX/3Ac;

    invoke-direct {v0}, LX/3Ac;-><init>()V

    sput-object v0, LX/3Ac;->a:LX/3Ac;

    .line 525724
    :cond_0
    sget-object v0, LX/3Ac;->a:LX/3Ac;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 525725
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 525715
    check-cast p2, LX/CDq;

    .line 525716
    iget v1, p2, LX/CDq;->a:I

    iget v2, p2, LX/CDq;->b:I

    iget v3, p2, LX/CDq;->c:I

    iget v4, p2, LX/CDq;->d:I

    iget v5, p2, LX/CDq;->e:I

    move-object v0, p1

    const/4 p2, 0x7

    const/4 p1, 0x6

    const/4 v7, 0x0

    .line 525717
    if-nez v1, :cond_0

    const/4 v6, 0x1

    .line 525718
    :goto_0
    invoke-static {v0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    if-eqz v6, :cond_1

    move v8, v7

    :goto_1
    invoke-interface {v9, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-static {v0}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v8

    const p0, 0x7f0a009f

    invoke-virtual {v8, p0}, LX/25Q;->i(I)LX/25Q;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    if-eqz v6, :cond_2

    const v8, 0x7f0b0043

    :goto_2
    invoke-interface {p0, v8}, LX/1Di;->i(I)LX/1Di;

    move-result-object v8

    if-eqz v6, :cond_3

    :goto_3
    invoke-interface {v8, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p2, v2}, LX/1Di;->e(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p1, v3}, LX/1Di;->e(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p2, v4}, LX/1Di;->a(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p1, v5}, LX/1Di;->a(II)LX/1Di;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v6, v7}, LX/1Di;->b(F)LX/1Di;

    move-result-object v6

    invoke-interface {v9, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 525719
    return-object v0

    :cond_0
    move v6, v7

    .line 525720
    goto :goto_0

    .line 525721
    :cond_1
    const/4 v8, 0x2

    goto :goto_1

    :cond_2
    move v8, v7

    goto :goto_2

    :cond_3
    const v7, 0x7f0b0043

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 525713
    invoke-static {}, LX/1dS;->b()V

    .line 525714
    const/4 v0, 0x0

    return-object v0
.end method
