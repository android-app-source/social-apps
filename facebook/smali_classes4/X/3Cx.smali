.class public LX/3Cx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 531169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 531170
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 531171
    :cond_0
    :goto_0
    return v2

    :cond_1
    move-object v0, p1

    .line 531172
    check-cast v0, Landroid/view/ViewGroup;

    move v1, v2

    .line 531173
    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 531174
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 531175
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    invoke-static {v3, v4}, LX/8t0;->a(Landroid/view/View;I)V

    .line 531176
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 531177
    :cond_2
    instance-of v0, p1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz v0, :cond_0

    .line 531178
    check-cast p1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-static {p1, v0}, LX/8t0;->a(Lcom/facebook/fbui/widget/layout/ImageBlockLayout;I)V

    goto :goto_0
.end method
