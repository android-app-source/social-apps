.class public LX/2y0;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3gK;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/2y0",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3gK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 480028
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 480029
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/2y0;->b:LX/0Zi;

    .line 480030
    iput-object p1, p0, LX/2y0;->a:LX/0Ot;

    .line 480031
    return-void
.end method

.method public static a(LX/0QB;)LX/2y0;
    .locals 4

    .prologue
    .line 480032
    const-class v1, LX/2y0;

    monitor-enter v1

    .line 480033
    :try_start_0
    sget-object v0, LX/2y0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 480034
    sput-object v2, LX/2y0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 480035
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480036
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 480037
    new-instance v3, LX/2y0;

    const/16 p0, 0x717

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/2y0;-><init>(LX/0Ot;)V

    .line 480038
    move-object v0, v3

    .line 480039
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 480040
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2y0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480041
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 480042
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 480043
    check-cast p2, LX/3gE;

    .line 480044
    iget-object v0, p0, LX/2y0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3gK;

    iget-object v1, p2, LX/3gE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/3gE;->b:LX/1Pq;

    iget-object v3, p2, LX/3gE;->c:LX/2ei;

    const/4 v8, 0x1

    .line 480045
    invoke-static {v1}, Lcom/facebook/feed/rows/sections/header/BaseExplanationPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1dl;

    move-result-object v4

    .line 480046
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, LX/3gK;->a:LX/3gL;

    const/4 v7, 0x0

    .line 480047
    new-instance p0, LX/3gM;

    invoke-direct {p0, v6}, LX/3gM;-><init>(LX/3gL;)V

    .line 480048
    iget-object p2, v6, LX/3gL;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/3gN;

    .line 480049
    if-nez p2, :cond_0

    .line 480050
    new-instance p2, LX/3gN;

    invoke-direct {p2, v6}, LX/3gN;-><init>(LX/3gL;)V

    .line 480051
    :cond_0
    invoke-static {p2, p1, v7, v7, p0}, LX/3gN;->a$redex0(LX/3gN;LX/1De;IILX/3gM;)V

    .line 480052
    move-object p0, p2

    .line 480053
    move-object v7, p0

    .line 480054
    move-object v6, v7

    .line 480055
    iget-object v7, v6, LX/3gN;->a:LX/3gM;

    iput-object v1, v7, LX/3gM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 480056
    iget-object v7, v6, LX/3gN;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v7, p0}, Ljava/util/BitSet;->set(I)V

    .line 480057
    move-object v6, v6

    .line 480058
    iget-object v7, v6, LX/3gN;->a:LX/3gM;

    iput-object v2, v7, LX/3gM;->b:LX/1Pq;

    .line 480059
    iget-object v7, v6, LX/3gN;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v7, p0}, Ljava/util/BitSet;->set(I)V

    .line 480060
    move-object v6, v6

    .line 480061
    invoke-virtual {v3}, LX/2ei;->getColorResource()I

    move-result v7

    .line 480062
    iget-object p0, v6, LX/3gN;->a:LX/3gM;

    invoke-virtual {v6, v7}, LX/1Dp;->d(I)I

    move-result p2

    iput p2, p0, LX/3gM;->c:I

    .line 480063
    iget-object p0, v6, LX/3gN;->e:Ljava/util/BitSet;

    const/4 p2, 0x2

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 480064
    move-object v6, v6

    .line 480065
    invoke-virtual {v3}, LX/2ei;->getFontSizeResource()I

    move-result v7

    .line 480066
    iget-object p0, v6, LX/3gN;->a:LX/3gM;

    invoke-virtual {v6, v7}, LX/1Dp;->e(I)I

    move-result p2

    iput p2, p0, LX/3gM;->d:I

    .line 480067
    iget-object p0, v6, LX/3gN;->e:Ljava/util/BitSet;

    const/4 p2, 0x3

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 480068
    move-object v6, v6

    .line 480069
    invoke-virtual {v3}, LX/2ei;->getFontStyle()I

    move-result v7

    .line 480070
    iget-object p0, v6, LX/3gN;->a:LX/3gM;

    iput v7, p0, LX/3gM;->e:I

    .line 480071
    iget-object p0, v6, LX/3gN;->e:Ljava/util/BitSet;

    const/4 p2, 0x4

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 480072
    move-object v6, v6

    .line 480073
    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v6, v7}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b08fe

    invoke-interface {v6, v8, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {v4}, LX/1ve;->a(LX/1dl;)Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v8}, LX/1Dh;->d(Z)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 480074
    return-object v0

    :cond_1
    iget-object v6, v0, LX/3gK;->b:LX/1vb;

    invoke-virtual {v6, p1}, LX/1vb;->c(LX/1De;)LX/1vd;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object v6

    check-cast v2, LX/1Pk;

    invoke-interface {v2}, LX/1Pk;->e()LX/1SX;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object v4

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 480075
    invoke-static {}, LX/1dS;->b()V

    .line 480076
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/3gF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/2y0",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 480077
    new-instance v1, LX/3gE;

    invoke-direct {v1, p0}, LX/3gE;-><init>(LX/2y0;)V

    .line 480078
    iget-object v2, p0, LX/2y0;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3gF;

    .line 480079
    if-nez v2, :cond_0

    .line 480080
    new-instance v2, LX/3gF;

    invoke-direct {v2, p0}, LX/3gF;-><init>(LX/2y0;)V

    .line 480081
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/3gF;->a$redex0(LX/3gF;LX/1De;IILX/3gE;)V

    .line 480082
    move-object v1, v2

    .line 480083
    move-object v0, v1

    .line 480084
    return-object v0
.end method
