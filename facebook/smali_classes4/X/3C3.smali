.class public LX/3C3;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3C3;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 529624
    const-string v0, "location_package"

    const/16 v1, 0xc

    new-instance v2, LX/3C4;

    invoke-direct {v2}, LX/3C4;-><init>()V

    new-instance v3, LX/3C5;

    invoke-direct {v3}, LX/3C5;-><init>()V

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 529625
    return-void
.end method

.method public static a(LX/0QB;)LX/3C3;
    .locals 3

    .prologue
    .line 529626
    sget-object v0, LX/3C3;->a:LX/3C3;

    if-nez v0, :cond_1

    .line 529627
    const-class v1, LX/3C3;

    monitor-enter v1

    .line 529628
    :try_start_0
    sget-object v0, LX/3C3;->a:LX/3C3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 529629
    if-eqz v2, :cond_0

    .line 529630
    :try_start_1
    new-instance v0, LX/3C3;

    invoke-direct {v0}, LX/3C3;-><init>()V

    .line 529631
    move-object v0, v0

    .line 529632
    sput-object v0, LX/3C3;->a:LX/3C3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529633
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 529634
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 529635
    :cond_1
    sget-object v0, LX/3C3;->a:LX/3C3;

    return-object v0

    .line 529636
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 529637
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
