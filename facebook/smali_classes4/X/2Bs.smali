.class public LX/2Bs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2Bs;


# instance fields
.field private final a:LX/29F;

.field private final b:Landroid/content/pm/PackageManager;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/03V;


# direct methods
.method public constructor <init>(LX/29F;Landroid/content/pm/PackageManager;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 381566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381567
    iput-object p1, p0, LX/2Bs;->a:LX/29F;

    .line 381568
    iput-object p2, p0, LX/2Bs;->b:Landroid/content/pm/PackageManager;

    .line 381569
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/2Bs;->c:Ljava/util/Set;

    .line 381570
    iput-object p3, p0, LX/2Bs;->d:LX/03V;

    .line 381571
    return-void
.end method

.method public static a(LX/0QB;)LX/2Bs;
    .locals 6

    .prologue
    .line 381572
    sget-object v0, LX/2Bs;->e:LX/2Bs;

    if-nez v0, :cond_1

    .line 381573
    const-class v1, LX/2Bs;

    monitor-enter v1

    .line 381574
    :try_start_0
    sget-object v0, LX/2Bs;->e:LX/2Bs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 381575
    if-eqz v2, :cond_0

    .line 381576
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 381577
    new-instance p0, LX/2Bs;

    invoke-static {v0}, LX/29F;->a(LX/0QB;)LX/29F;

    move-result-object v3

    check-cast v3, LX/29F;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageManager;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/2Bs;-><init>(LX/29F;Landroid/content/pm/PackageManager;LX/03V;)V

    .line 381578
    move-object v0, p0

    .line 381579
    sput-object v0, LX/2Bs;->e:LX/2Bs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 381580
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 381581
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 381582
    :cond_1
    sget-object v0, LX/2Bs;->e:LX/2Bs;

    return-object v0

    .line 381583
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 381584
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 381585
    iget-object v1, p0, LX/2Bs;->c:Ljava/util/Set;

    monitor-enter v1

    .line 381586
    :try_start_0
    iget-object v0, p0, LX/2Bs;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381587
    monitor-exit v1

    .line 381588
    :goto_0
    return-void

    .line 381589
    :cond_0
    iget-object v0, p0, LX/2Bs;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 381590
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381591
    iget-object v0, p0, LX/2Bs;->b:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p1, v2, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_0

    .line 381592
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(Landroid/content/Context;Landroid/content/ComponentName;)V
    .locals 4

    .prologue
    .line 381593
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 381594
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 381595
    const/4 v0, 0x0

    :goto_0
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 381596
    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    aget-object v3, v3, v0

    iget-object v3, v3, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-eqz v2, :cond_1

    .line 381597
    :cond_0
    return-void

    .line 381598
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 381599
    :catch_0
    move-exception v0

    .line 381600
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "No package found for component name: %s"

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 381601
    :catch_1
    move-exception v0

    .line 381602
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Landroid/os/DeadObjectException;

    if-nez v1, :cond_0

    .line 381603
    throw v0

    .line 381604
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No service found for class name: %s in package: %s"

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static c(LX/2Bs;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 381605
    new-instance v1, Landroid/content/ComponentName;

    const-class v0, Lcom/facebook/mqttlite/MqttService;

    invoke-direct {v1, p1, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 381606
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Explicit component selection is not allowed"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 381607
    invoke-direct {p0, v1}, LX/2Bs;->a(Landroid/content/ComponentName;)V

    .line 381608
    invoke-virtual {p2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 381609
    return-void

    .line 381610
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;ILjava/lang/String;)LX/2XW;
    .locals 7

    .prologue
    .line 381611
    invoke-static {p0, p1, p2}, LX/2Bs;->c(LX/2Bs;Landroid/content/Context;Landroid/content/Intent;)V

    .line 381612
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 381613
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    .line 381614
    iget-object v1, p0, LX/2Bs;->a:LX/29F;

    invoke-virtual {v1, p2, p3, p4}, LX/29F;->a(Landroid/content/Intent;Landroid/content/ServiceConnection;I)LX/2XW;

    move-result-object v1

    .line 381615
    iget-boolean v2, v1, LX/2XW;->a:Z

    if-nez v2, :cond_0

    .line 381616
    const-string v2, "PushServiceTargetingHelper"

    const-string v3, "Unable to bind to %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381617
    invoke-static {p1, v0}, LX/2Bs;->a(Landroid/content/Context;Landroid/content/ComponentName;)V

    .line 381618
    :cond_0
    return-object v1
.end method

.method public final a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;
    .locals 6

    .prologue
    .line 381619
    invoke-static {p0, p1, p2}, LX/2Bs;->c(LX/2Bs;Landroid/content/Context;Landroid/content/Intent;)V

    .line 381620
    const/4 v0, 0x0

    .line 381621
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 381622
    :goto_0
    if-nez v0, :cond_0

    .line 381623
    const-string v1, "PushServiceTargetingHelper"

    const-string v2, "Unable to startService, the service %s was not found"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381624
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-static {p1, v1}, LX/2Bs;->a(Landroid/content/Context;Landroid/content/ComponentName;)V

    .line 381625
    :cond_0
    return-object v0

    .line 381626
    :catch_0
    move-exception v1

    .line 381627
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v2, v2, Landroid/os/DeadObjectException;

    if-eqz v2, :cond_1

    .line 381628
    iget-object v1, p0, LX/2Bs;->d:LX/03V;

    const-string v2, "PushServiceTargetingHelper"

    const-string v3, "DeadObjectException caught during start service"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 381629
    :cond_1
    throw v1
.end method

.method public final a(Landroid/content/ServiceConnection;)V
    .locals 1

    .prologue
    .line 381630
    iget-object v0, p0, LX/2Bs;->a:LX/29F;

    invoke-virtual {v0, p1}, LX/29F;->a(Landroid/content/ServiceConnection;)V

    .line 381631
    return-void
.end method
