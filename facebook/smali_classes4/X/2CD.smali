.class public final LX/2CD;
.super LX/1CO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1CO",
        "<",
        "LX/0bI;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2CA;


# direct methods
.method public constructor <init>(LX/2CA;)V
    .locals 0

    .prologue
    .line 382194
    iput-object p1, p0, LX/2CD;->a:LX/2CA;

    invoke-direct {p0}, LX/1CO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/0bI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 382195
    const-class v0, LX/0bI;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 11

    .prologue
    .line 382196
    check-cast p1, LX/0bI;

    .line 382197
    iget-object v0, p0, LX/2CD;->a:LX/2CA;

    iget-object v0, v0, LX/2CA;->c:LX/2CB;

    .line 382198
    iget-object v1, p1, LX/0bI;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 382199
    iget-object v2, v0, LX/2CB;->h:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 382200
    invoke-static {v0}, LX/2CB;->d(LX/2CB;)V

    .line 382201
    iget-object v2, v0, LX/2CB;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/29r;

    invoke-virtual {v2}, LX/29r;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 382202
    iget-object v2, v0, LX/2CB;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    .line 382203
    :cond_0
    :goto_0
    iget-object v0, p0, LX/2CD;->a:LX/2CA;

    iget-object v0, v0, LX/2CA;->d:LX/29r;

    .line 382204
    iget-object v1, p1, LX/0bI;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 382205
    invoke-virtual {v0}, LX/29r;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, v0, LX/29r;->u:Z

    if-nez v2, :cond_1

    iget-object v2, v0, LX/29r;->c:LX/29s;

    invoke-virtual {v2}, LX/29s;->d()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, LX/29r;->k(LX/29r;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 382206
    :cond_1
    :goto_1
    return-void

    .line 382207
    :cond_2
    invoke-static {v0}, LX/2CB;->e(LX/2CB;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 382208
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/2CB;->a:Z

    .line 382209
    iget-object v2, v0, LX/2CB;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BZj;

    .line 382210
    const-string v4, "Facebook"

    move-object v3, v4

    .line 382211
    const-string v5, "com.facebook.katana"

    move-object v4, v5

    .line 382212
    iget-object v5, v0, LX/2CB;->j:LX/29q;

    const/4 v0, 0x1

    const/4 v10, 0x0

    .line 382213
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 382214
    :cond_3
    :goto_2
    goto :goto_0

    .line 382215
    :cond_4
    new-instance v6, Landroid/view/ContextThemeWrapper;

    const v7, 0x7f0e05cc

    invoke-direct {v6, v1, v7}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 382216
    new-instance v7, LX/0ju;

    invoke-direct {v7, v6}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 382217
    iget-object v6, v2, LX/BZj;->b:Landroid/content/res/Resources;

    const v8, 0x7f08294a

    new-array v9, v0, [Ljava/lang/Object;

    aput-object v3, v9, v10

    invoke-virtual {v6, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 382218
    iget-object v6, v2, LX/BZj;->b:Landroid/content/res/Resources;

    const v8, 0x7f082949

    new-array v9, v0, [Ljava/lang/Object;

    aput-object v3, v9, v10

    invoke-virtual {v6, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 382219
    iget-object v6, v2, LX/BZj;->b:Landroid/content/res/Resources;

    const v8, 0x7f08294b

    new-array v9, v0, [Ljava/lang/Object;

    aput-object v3, v9, v10

    invoke-virtual {v6, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v8, LX/BZg;

    invoke-direct {v8, v2, v5, v4, v1}, LX/BZg;-><init>(LX/BZj;LX/29q;Ljava/lang/String;Landroid/app/Activity;)V

    invoke-virtual {v7, v6, v8}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 382220
    const v6, 0x7f08294c

    new-instance v8, LX/BZh;

    invoke-direct {v8, v2, v5}, LX/BZh;-><init>(LX/BZj;LX/29q;)V

    invoke-virtual {v7, v6, v8}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 382221
    new-instance v6, LX/BZi;

    invoke-direct {v6, v2, v5}, LX/BZi;-><init>(LX/BZj;LX/29q;)V

    invoke-virtual {v7, v6}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    .line 382222
    invoke-virtual {v7}, LX/0ju;->b()LX/2EJ;

    goto :goto_2

    .line 382223
    :cond_5
    const/4 v2, 0x0

    iput v2, v0, LX/29r;->j:I

    .line 382224
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, LX/29r;->s:Ljava/lang/ref/WeakReference;

    .line 382225
    iget-object v2, v0, LX/29r;->c:LX/29s;

    invoke-virtual {v2}, LX/29s;->b()Lcom/facebook/appirater/api/AppRaterReport;

    move-result-object v2

    .line 382226
    if-eqz v2, :cond_6

    .line 382227
    invoke-static {v0}, LX/29r;->m(LX/29r;)V

    goto/16 :goto_1

    .line 382228
    :cond_6
    invoke-static {v0}, LX/29r;->i(LX/29r;)Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 382229
    iget-object v2, v0, LX/29r;->c:LX/29s;

    invoke-virtual {v2}, LX/29s;->e()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 382230
    invoke-static {v0}, LX/29r;->g(LX/29r;)Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 382231
    invoke-static {v0}, LX/29r;->l(LX/29r;)V

    goto/16 :goto_1

    .line 382232
    :cond_7
    invoke-virtual {v0}, LX/29r;->d()Lcom/facebook/appirater/api/FetchISRConfigResult;

    move-result-object v2

    .line 382233
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/appirater/api/FetchISRConfigResult;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/appirater/api/FetchISRConfigResult;->b()Z

    move-result v3

    if-nez v3, :cond_1

    iget-boolean v3, v2, Lcom/facebook/appirater/api/FetchISRConfigResult;->shouldAskUser:Z

    if-eqz v3, :cond_1

    .line 382234
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/29r;->u:Z

    .line 382235
    iget-object v3, v0, LX/29r;->e:Landroid/os/Handler;

    iget-object v4, v0, LX/29r;->o:Ljava/lang/Runnable;

    iget-wide v6, v2, Lcom/facebook/appirater/api/FetchISRConfigResult;->delayAskingMillis:J

    const v2, 0x6b0fcc65

    invoke-static {v3, v4, v6, v7, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto/16 :goto_1
.end method
