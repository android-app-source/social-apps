.class public LX/2P7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0lC;


# direct methods
.method public constructor <init>(LX/0SG;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 405739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 405740
    iput-object p1, p0, LX/2P7;->a:LX/0SG;

    .line 405741
    iput-object p2, p0, LX/2P7;->b:LX/0lC;

    .line 405742
    return-void
.end method

.method public static a(LX/0lF;)Lcom/facebook/user/model/PicSquare;
    .locals 5

    .prologue
    .line 405733
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 405734
    invoke-virtual {p0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 405735
    const-string v3, "size"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->d(LX/0lF;)I

    move-result v3

    .line 405736
    const-string v4, "url"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 405737
    new-instance v4, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-direct {v4, v3, v0}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 405738
    :cond_0
    new-instance v0, Lcom/facebook/user/model/PicSquare;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/user/model/PicSquare;-><init>(LX/0Px;)V

    return-object v0
.end method

.method private static a(LX/2P7;LX/0XG;LX/0lF;)Lcom/facebook/user/model/User;
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 405586
    new-instance v3, LX/0XI;

    invoke-direct {v3}, LX/0XI;-><init>()V

    .line 405587
    const-string v0, "uid"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Missing id field on profile"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 405588
    const-string v0, "uid"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 405589
    if-nez v0, :cond_0

    .line 405590
    const-string v0, "id"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 405591
    :cond_0
    invoke-virtual {v3, p1, v0}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 405592
    const-string v0, "contact_email"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->a(LX/0lF;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 405593
    new-instance v0, Lcom/facebook/user/model/UserEmailAddress;

    const-string v2, "contact_email"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;I)V

    .line 405594
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 405595
    iput-object v0, v3, LX/0XI;->c:Ljava/util/List;

    .line 405596
    :cond_1
    :goto_0
    const-string v0, "phones"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->a(LX/0lF;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 405597
    const-string v0, "phones"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 405598
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 405599
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0lF;

    .line 405600
    const-string v6, "full_number"

    invoke-virtual {v5, v6}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 405601
    const-string v6, "full_number"

    invoke-virtual {v5, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v7

    .line 405602
    const-string v6, "display_number"

    invoke-virtual {v5, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 405603
    :goto_2
    sget-object v10, LX/03R;->UNSET:LX/03R;

    .line 405604
    const-string v8, "is_verified"

    invoke-virtual {v5, v8}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 405605
    const-string v8, "is_verified"

    invoke-virtual {v5, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    invoke-virtual {v8}, LX/0lF;->u()Z

    move-result v8

    if-eqz v8, :cond_5

    sget-object v8, LX/03R;->YES:LX/03R;

    :goto_3
    move-object v10, v8

    .line 405606
    :cond_2
    const/4 v9, 0x0

    .line 405607
    const-string v8, "android_type"

    invoke-virtual {v5, v8}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 405608
    const-string v8, "android_type"

    invoke-virtual {v5, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->d(LX/0lF;)I

    move-result v9

    .line 405609
    :cond_3
    :goto_4
    new-instance v5, Lcom/facebook/user/model/UserPhoneNumber;

    move-object v8, v7

    invoke-direct/range {v5 .. v10}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/03R;)V

    invoke-virtual {v11, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 405610
    :cond_4
    const-string v6, "country_code"

    invoke-virtual {v5, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 405611
    const-string v7, "number"

    invoke-virtual {v5, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v7

    .line 405612
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "+"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v7, v6

    .line 405613
    goto :goto_2

    .line 405614
    :cond_5
    sget-object v8, LX/03R;->NO:LX/03R;

    goto :goto_3

    .line 405615
    :cond_6
    const-string v8, "type"

    invoke-virtual {v5, v8}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 405616
    const-string v8, "type"

    invoke-virtual {v5, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 405617
    const-string v8, "other_phone"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 405618
    const/4 v9, 0x7

    goto :goto_4

    .line 405619
    :cond_7
    const-string v8, "cell"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 405620
    const/4 v9, 0x2

    goto :goto_4

    .line 405621
    :cond_8
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v0, v5

    .line 405622
    iput-object v0, v3, LX/0XI;->d:Ljava/util/List;

    .line 405623
    :cond_9
    const/4 v2, 0x0

    .line 405624
    const-string v0, "first_name"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 405625
    const-string v0, "first_name"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 405626
    :goto_5
    const-string v4, "last_name"

    invoke-virtual {p2, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 405627
    const-string v4, "last_name"

    invoke-virtual {p2, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 405628
    :goto_6
    const-string v5, "name"

    invoke-virtual {p2, v5}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 405629
    const-string v2, "name"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 405630
    :cond_a
    new-instance v5, Lcom/facebook/user/model/Name;

    invoke-direct {v5, v0, v4, v2}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    .line 405631
    iput-object v0, v3, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 405632
    const-string v0, "birth_date_year"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    const-string v0, "birth_date_year"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    :goto_7
    const-string v2, "birth_date_month"

    invoke-virtual {p2, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_26

    const-string v2, "birth_date_month"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->d(LX/0lF;)I

    move-result v2

    :goto_8
    const-string v4, "birth_date_day"

    invoke-virtual {p2, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v1, "birth_date_day"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->d(LX/0lF;)I

    move-result v1

    :cond_b
    invoke-virtual {v3, v0, v2, v1}, LX/0XI;->a(III)LX/0XI;

    .line 405633
    :try_start_0
    const-string v0, "gender"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 405634
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 405635
    :cond_c
    sget-object v0, LX/0XJ;->UNKNOWN:LX/0XJ;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 405636
    :goto_9
    move-object v0, v0

    .line 405637
    iput-object v0, v3, LX/0XI;->m:LX/0XJ;

    .line 405638
    const-string v0, "profile_pic_square"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 405639
    const-string v0, "profile_pic_square"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/2P7;->a(LX/0lF;)Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    .line 405640
    iput-object v0, v3, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 405641
    :cond_d
    const-string v0, "pic_square"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 405642
    const-string v0, "pic_square"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 405643
    iput-object v0, v3, LX/0XI;->n:Ljava/lang/String;

    .line 405644
    :cond_e
    const-string v0, "pic_cover"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 405645
    const-string v0, "pic_cover"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 405646
    iput-object v0, v3, LX/0XI;->o:Ljava/lang/String;

    .line 405647
    :cond_f
    const-string v0, "rank"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 405648
    const-string v0, "rank"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->e(LX/0lF;)D

    move-result-wide v0

    double-to-float v0, v0

    .line 405649
    iput v0, v3, LX/0XI;->t:F

    .line 405650
    :cond_10
    const-string v0, "is_pushable"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 405651
    const-string v0, "is_pushable"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    if-eqz v0, :cond_27

    sget-object v0, LX/03R;->YES:LX/03R;

    .line 405652
    :goto_a
    iput-object v0, v3, LX/0XI;->u:LX/03R;

    .line 405653
    :goto_b
    const-string v0, "is_employee"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 405654
    const-string v0, "is_employee"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    .line 405655
    iput-boolean v0, v3, LX/0XI;->v:Z

    .line 405656
    :cond_11
    const-string v0, "is_work_user"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 405657
    const-string v0, "is_work_user"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    .line 405658
    iput-boolean v0, v3, LX/0XI;->w:Z

    .line 405659
    :cond_12
    const-string v0, "type"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 405660
    const-string v0, "type"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 405661
    iput-object v0, v3, LX/0XI;->y:Ljava/lang/String;

    .line 405662
    :cond_13
    const-string v0, "is_messenger_user"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 405663
    const-string v0, "is_messenger_user"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    .line 405664
    iput-boolean v0, v3, LX/0XI;->z:Z

    .line 405665
    :cond_14
    const-string v0, "is_commerce"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 405666
    const-string v0, "is_commerce"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    .line 405667
    iput-boolean v0, v3, LX/0XI;->A:Z

    .line 405668
    :cond_15
    const-string v0, "messenger_install_time"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 405669
    const-string v0, "messenger_install_time"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    move-result-wide v0

    .line 405670
    iput-wide v0, v3, LX/0XI;->D:J

    .line 405671
    :cond_16
    const-string v0, "added_time"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 405672
    const-string v0, "added_time"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    move-result-wide v0

    .line 405673
    iput-wide v0, v3, LX/0XI;->E:J

    .line 405674
    :cond_17
    const-string v0, "is_partial"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 405675
    const-string v0, "is_partial"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    .line 405676
    iput-boolean v0, v3, LX/0XI;->M:Z

    .line 405677
    :cond_18
    const-string v0, "is_minor"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 405678
    const-string v0, "is_minor"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    .line 405679
    iput-boolean v0, v3, LX/0XI;->N:Z

    .line 405680
    :cond_19
    const-string v0, "can_viewer_message"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 405681
    const-string v0, "can_viewer_message"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    .line 405682
    iput-boolean v0, v3, LX/0XI;->am:Z

    .line 405683
    :cond_1a
    const-string v0, "profile_picture_is_silhouette"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 405684
    const-string v0, "profile_picture_is_silhouette"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    .line 405685
    iput-object v0, v3, LX/0XI;->O:LX/03R;

    .line 405686
    :cond_1b
    iget-object v0, p0, LX/2P7;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 405687
    iput-wide v0, v3, LX/0XI;->an:J

    .line 405688
    const-string v0, "montage_thread_fbid"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 405689
    const-string v0, "montage_thread_fbid"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    move-result-wide v0

    .line 405690
    iput-wide v0, v3, LX/0XI;->X:J

    .line 405691
    :cond_1c
    const-string v0, "can_see_viewer_montage_thread"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 405692
    const-string v0, "can_see_viewer_montage_thread"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    .line 405693
    iput-boolean v0, v3, LX/0XI;->Y:Z

    .line 405694
    :cond_1d
    const-string v0, "is_deactivated_allowed_on_messenger"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 405695
    const-string v0, "is_deactivated_allowed_on_messenger"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    .line 405696
    iput-boolean v0, v3, LX/0XI;->S:Z

    .line 405697
    :cond_1e
    const-string v0, "is_messenger_only_deactivated"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 405698
    const-string v0, "is_messenger_only_deactivated"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    .line 405699
    iput-boolean v0, v3, LX/0XI;->af:Z

    .line 405700
    :cond_1f
    const-string v0, "user_custom_tags"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 405701
    const-string v0, "user_custom_tags"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/2P7;->b(LX/0lF;)LX/0Px;

    move-result-object v0

    .line 405702
    iput-object v0, v3, LX/0XI;->e:LX/0Px;

    .line 405703
    :cond_20
    const-string v0, "can_viewer_send_money"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 405704
    const-string v0, "can_viewer_send_money"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->u()Z

    move-result v0

    .line 405705
    iput-boolean v0, v3, LX/0XI;->aa:Z

    .line 405706
    :cond_21
    const-string v0, "viewer_connection_status"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 405707
    const-string v0, "viewer_connection_status"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XK;->fromDbValue(Ljava/lang/String;)LX/0XK;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0XI;->a(LX/0XK;)LX/0XI;

    .line 405708
    :cond_22
    invoke-virtual {v3}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0

    .line 405709
    :cond_23
    const-string v0, "emails"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 405710
    const-string v0, "emails"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 405711
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 405712
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_24

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lF;

    .line 405713
    new-instance v6, Lcom/facebook/user/model/UserEmailAddress;

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const/4 v7, 0x0

    invoke-direct {v6, v2, v7}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v4, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_c

    .line 405714
    :cond_24
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 405715
    iput-object v0, v3, LX/0XI;->c:Ljava/util/List;

    .line 405716
    goto/16 :goto_0

    :cond_25
    move v0, v1

    .line 405717
    goto/16 :goto_7

    :cond_26
    move v2, v1

    goto/16 :goto_8

    .line 405718
    :cond_27
    sget-object v0, LX/03R;->NO:LX/03R;

    goto/16 :goto_a

    .line 405719
    :cond_28
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 405720
    iput-object v0, v3, LX/0XI;->u:LX/03R;

    .line 405721
    goto/16 :goto_b

    :cond_29
    move-object v4, v2

    goto/16 :goto_6

    :cond_2a
    move-object v0, v2

    goto/16 :goto_5

    .line 405722
    :cond_2b
    :try_start_1
    invoke-static {v0}, LX/0XJ;->valueOf(Ljava/lang/String;)LX/0XJ;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto/16 :goto_9

    .line 405723
    :catch_0
    sget-object v0, LX/0XJ;->UNKNOWN:LX/0XJ;

    goto/16 :goto_9
.end method

.method public static b(LX/0lF;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserCustomTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 405724
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 405725
    invoke-virtual {p0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 405726
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 405727
    const-string v2, "name"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 405728
    const-string v3, "color"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->d(LX/0lF;)I

    move-result v3

    .line 405729
    const-string v4, "fillColor"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->d(LX/0lF;)I

    move-result v4

    .line 405730
    const-string v5, "borderColor"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v5

    .line 405731
    new-instance v0, Lcom/facebook/user/model/UserCustomTag;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/user/model/UserCustomTag;-><init>(Ljava/lang/String;Ljava/lang/String;III)V

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 405732
    :cond_0
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2P7;
    .locals 3

    .prologue
    .line 405584
    new-instance v2, LX/2P7;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-direct {v2, v0, v1}, LX/2P7;-><init>(LX/0SG;LX/0lC;)V

    .line 405585
    return-object v2
.end method

.method public static c(LX/0lF;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "LX/0Px",
            "<",
            "LX/4nX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 405566
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 405567
    const-string v1, "commerce_faq_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "commerce_faq_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 405568
    sget-object v1, LX/4nX;->COMMERCE_FAQ_ENABLED:LX/4nX;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 405569
    :cond_0
    const-string v1, "in_messenger_shopping_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "in_messenger_shopping_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 405570
    sget-object v1, LX/4nX;->IN_MESSENGER_SHOPPING_ENABLED:LX/4nX;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 405571
    :cond_1
    const-string v1, "commerce_nux_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "commerce_nux_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 405572
    sget-object v1, LX/4nX;->COMMERCE_NUX_ENABLED:LX/4nX;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 405573
    :cond_2
    const-string v1, "structured_menu_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "structured_menu_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 405574
    sget-object v1, LX/4nX;->STRUCTURED_MENU_ENABLED:LX/4nX;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 405575
    :cond_3
    const-string v1, "user_control_topic_manage_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "user_control_topic_manage_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 405576
    sget-object v1, LX/4nX;->USER_CONTROL_TOPIC_MANAGE_ENABLED:LX/4nX;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 405577
    :cond_4
    const-string v1, "null_state_cta_button_always_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "null_state_cta_button_always_enabled"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 405578
    sget-object v1, LX/4nX;->NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED:LX/4nX;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 405579
    :cond_5
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0XG;Ljava/lang/String;)Lcom/facebook/user/model/User;
    .locals 3

    .prologue
    .line 405580
    :try_start_0
    iget-object v0, p0, LX/2P7;->b:LX/0lC;

    invoke-virtual {v0, p2}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 405581
    invoke-static {p0, p1, v0}, LX/2P7;->a(LX/2P7;LX/0XG;LX/0lF;)Lcom/facebook/user/model/User;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 405582
    :catch_0
    move-exception v0

    .line 405583
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected serialization exception"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
