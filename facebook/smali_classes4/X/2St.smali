.class public LX/2St;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile m:LX/2St;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0a8;

.field public final e:LX/2AY;

.field public final f:LX/0Xl;

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/2Sw;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "LX/0Tn;",
            "LX/2Sw;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Ljava/lang/Integer;",
            "LX/2Sw;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2Sw;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/2T0;

.field public final l:LX/2Sv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 413586
    const-class v0, LX/2St;

    sput-object v0, LX/2St;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0a8;LX/2AY;LX/0Xl;Ljava/util/Set;LX/2Sv;)V
    .locals 1
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperListeners;",
            "LX/2AY;",
            "LX/0Xl;",
            "Ljava/util/Set",
            "<",
            "LX/2Sw;",
            ">;",
            "Lcom/facebook/sync/SyncContextChecker;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 413573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413574
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/2St;->h:LX/0Xu;

    .line 413575
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v0

    iput-object v0, p0, LX/2St;->i:LX/0Xu;

    .line 413576
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/2St;->j:Ljava/util/Map;

    .line 413577
    iput-object p1, p0, LX/2St;->b:Landroid/content/Context;

    .line 413578
    iput-object p2, p0, LX/2St;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 413579
    iput-object p3, p0, LX/2St;->d:LX/0a8;

    .line 413580
    iput-object p4, p0, LX/2St;->e:LX/2AY;

    .line 413581
    iput-object p5, p0, LX/2St;->f:LX/0Xl;

    .line 413582
    iput-object p6, p0, LX/2St;->g:Ljava/util/Set;

    .line 413583
    iput-object p7, p0, LX/2St;->l:LX/2Sv;

    .line 413584
    const/4 v0, 0x0

    iput-object v0, p0, LX/2St;->k:LX/2T0;

    .line 413585
    return-void
.end method

.method public static a(LX/0QB;)LX/2St;
    .locals 12

    .prologue
    .line 413505
    sget-object v0, LX/2St;->m:LX/2St;

    if-nez v0, :cond_1

    .line 413506
    const-class v1, LX/2St;

    monitor-enter v1

    .line 413507
    :try_start_0
    sget-object v0, LX/2St;->m:LX/2St;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 413508
    if-eqz v2, :cond_0

    .line 413509
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 413510
    new-instance v3, LX/2St;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Zn;->a(LX/0QB;)LX/0a8;

    move-result-object v6

    check-cast v6, LX/0a8;

    invoke-static {v0}, LX/2AY;->a(LX/0QB;)LX/2AY;

    move-result-object v7

    check-cast v7, LX/2AY;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v8

    check-cast v8, LX/0Xl;

    .line 413511
    new-instance v9, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v10

    new-instance v11, LX/2Su;

    invoke-direct {v11, v0}, LX/2Su;-><init>(LX/0QB;)V

    invoke-direct {v9, v10, v11}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v9, v9

    .line 413512
    invoke-static {v0}, LX/2Sv;->b(LX/0QB;)LX/2Sv;

    move-result-object v10

    check-cast v10, LX/2Sv;

    invoke-direct/range {v3 .. v10}, LX/2St;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0a8;LX/2AY;LX/0Xl;Ljava/util/Set;LX/2Sv;)V

    .line 413513
    move-object v0, v3

    .line 413514
    sput-object v0, LX/2St;->m:LX/2St;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413515
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 413516
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 413517
    :cond_1
    sget-object v0, LX/2St;->m:LX/2St;

    return-object v0

    .line 413518
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 413519
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2St;Ljava/util/Collection;LX/2T6;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/2Sw;",
            ">;",
            "LX/2T6;",
            ")V"
        }
    .end annotation

    .prologue
    .line 413568
    iget-object v0, p0, LX/2St;->l:LX/2Sv;

    invoke-virtual {v0}, LX/2Sv;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 413569
    :cond_0
    return-void

    .line 413570
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sw;

    .line 413571
    invoke-interface {v0}, LX/2Sw;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 413572
    invoke-interface {v0, p2}, LX/2Sw;->a(LX/2T6;)V

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 413555
    iget-object v0, p0, LX/2St;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sw;

    .line 413556
    invoke-interface {v0}, LX/2Sw;->d()Ljava/lang/String;

    move-result-object v2

    .line 413557
    if-eqz v2, :cond_0

    .line 413558
    iget-object v3, p0, LX/2St;->j:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 413559
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Multiple handlers for the same refresh action: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 413560
    :cond_1
    iget-object v3, p0, LX/2St;->j:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 413561
    :cond_2
    iget-object v0, p0, LX/2St;->f:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    .line 413562
    new-instance v2, LX/2T5;

    invoke-direct {v2, p0}, LX/2T5;-><init>(LX/2St;)V

    .line 413563
    iget-object v0, p0, LX/2St;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 413564
    iget-object v0, p0, LX/2St;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 413565
    invoke-interface {v1, v0, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    goto :goto_1

    .line 413566
    :cond_3
    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 413567
    :cond_4
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 8

    .prologue
    .line 413520
    const/4 v3, 0x0

    .line 413521
    iget-object v0, p0, LX/2St;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sw;

    .line 413522
    invoke-interface {v0}, LX/2Sw;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_0
    if-ge v2, v6, :cond_1

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 413523
    iget-object v7, p0, LX/2St;->h:LX/0Xu;

    invoke-interface {v7, v1, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 413524
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 413525
    :cond_1
    invoke-interface {v0}, LX/2Sw;->c()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_0

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 413526
    iget-object v7, p0, LX/2St;->i:LX/0Xu;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v7, v1, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 413527
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 413528
    :cond_2
    new-instance v0, LX/2T0;

    invoke-direct {v0, p0}, LX/2T0;-><init>(LX/2St;)V

    iput-object v0, p0, LX/2St;->k:LX/2T0;

    .line 413529
    iget-object v0, p0, LX/2St;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/2St;->h:LX/0Xu;

    invoke-interface {v1}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, LX/2St;->k:LX/2T0;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;LX/0dN;)V

    .line 413530
    iget-object v0, p0, LX/2St;->d:LX/0a8;

    iget-object v1, p0, LX/2St;->k:LX/2T0;

    iget-object v2, p0, LX/2St;->i:LX/0Xu;

    invoke-interface {v2}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v2

    .line 413531
    instance-of v3, v2, LX/2T1;

    if-eqz v3, :cond_3

    .line 413532
    check-cast v2, LX/2T1;

    .line 413533
    invoke-virtual {v2}, LX/2T1;->size()I

    move-result v3

    .line 413534
    new-array v4, v3, [I

    .line 413535
    iget-object v5, v2, LX/2T1;->array:[I

    iget v6, v2, LX/2T1;->start:I

    const/4 v7, 0x0

    invoke-static {v5, v6, v4, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 413536
    move-object v3, v4

    .line 413537
    :goto_2
    move-object v2, v3

    .line 413538
    invoke-virtual {v0, v1, v2}, LX/0a8;->a(LX/0aB;[I)V

    .line 413539
    new-instance v0, LX/0Yd;

    const-string v1, "android.intent.action.LOCALE_CHANGED"

    new-instance v2, LX/2T2;

    invoke-direct {v2, p0}, LX/2T2;-><init>(LX/2St;)V

    invoke-direct {v0, v1, v2}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    .line 413540
    iget-object v1, p0, LX/2St;->b:Landroid/content/Context;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 413541
    iget-object v0, p0, LX/2St;->f:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    new-instance v2, LX/2T3;

    invoke-direct {v2, p0}, LX/2T3;-><init>(LX/2St;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    .line 413542
    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 413543
    iget-object v0, p0, LX/2St;->f:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v2, LX/2T4;

    invoke-direct {v2, p0}, LX/2T4;-><init>(LX/2St;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    .line 413544
    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 413545
    invoke-direct {p0}, LX/2St;->d()V

    .line 413546
    iget-object v0, p0, LX/2St;->g:Ljava/util/Set;

    sget-object v1, LX/2T6;->NORMAL:LX/2T6;

    invoke-static {p0, v0, v1}, LX/2St;->a$redex0(LX/2St;Ljava/util/Collection;LX/2T6;)V

    .line 413547
    return-void

    .line 413548
    :cond_3
    invoke-interface {v2}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v6

    .line 413549
    array-length v7, v6

    .line 413550
    new-array v4, v7, [I

    .line 413551
    const/4 v3, 0x0

    move v5, v3

    :goto_3
    if-ge v5, v7, :cond_4

    .line 413552
    aget-object v3, v6, v5

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    aput v3, v4, v5

    .line 413553
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_3

    :cond_4
    move-object v3, v4

    .line 413554
    goto :goto_2
.end method
