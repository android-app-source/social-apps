.class public LX/26D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/26E;

.field private static t:LX/0Xm;


# instance fields
.field public final b:LX/26F;

.field public final c:LX/26G;

.field private final d:LX/0sX;

.field public final e:LX/0tK;

.field private final f:LX/26H;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bz5;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0oz;

.field private final i:LX/1qa;

.field private final j:LX/26I;

.field private final k:LX/1WM;

.field public final l:LX/1WN;

.field private final m:LX/1eq;

.field public final n:LX/0ad;

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1VL;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hU;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/16I;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/121;",
            ">;"
        }
    .end annotation
.end field

.field public final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AjI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 371782
    new-instance v0, LX/26E;

    invoke-direct {v0, v1, v1, v1}, LX/26E;-><init>(ZII)V

    sput-object v0, LX/26D;->a:LX/26E;

    return-void
.end method

.method public constructor <init>(LX/26F;LX/26G;LX/0sX;LX/0tK;LX/26H;LX/0Ot;LX/0oz;LX/1qa;LX/26I;LX/1WM;LX/1WN;LX/1eq;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/26F;",
            "LX/26G;",
            "LX/0sX;",
            "LX/0tK;",
            "LX/26H;",
            "LX/0Ot",
            "<",
            "LX/Bz5;",
            ">;",
            "LX/0oz;",
            "LX/1qa;",
            "LX/26I;",
            "LX/1WM;",
            "LX/1WN;",
            "LX/1eq;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/1VL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2hU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/16I;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/121;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AjI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 371783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 371784
    iput-object p1, p0, LX/26D;->b:LX/26F;

    .line 371785
    iput-object p2, p0, LX/26D;->c:LX/26G;

    .line 371786
    iput-object p3, p0, LX/26D;->d:LX/0sX;

    .line 371787
    iput-object p4, p0, LX/26D;->e:LX/0tK;

    .line 371788
    iput-object p5, p0, LX/26D;->f:LX/26H;

    .line 371789
    iput-object p6, p0, LX/26D;->g:LX/0Ot;

    .line 371790
    iput-object p7, p0, LX/26D;->h:LX/0oz;

    .line 371791
    iput-object p8, p0, LX/26D;->i:LX/1qa;

    .line 371792
    iput-object p9, p0, LX/26D;->j:LX/26I;

    .line 371793
    iput-object p10, p0, LX/26D;->k:LX/1WM;

    .line 371794
    iput-object p11, p0, LX/26D;->l:LX/1WN;

    .line 371795
    iput-object p12, p0, LX/26D;->m:LX/1eq;

    .line 371796
    iput-object p13, p0, LX/26D;->n:LX/0ad;

    .line 371797
    iput-object p14, p0, LX/26D;->o:LX/0Ot;

    .line 371798
    move-object/from16 v0, p15

    iput-object v0, p0, LX/26D;->p:LX/0Ot;

    .line 371799
    move-object/from16 v0, p16

    iput-object v0, p0, LX/26D;->q:LX/0Ot;

    .line 371800
    move-object/from16 v0, p17

    iput-object v0, p0, LX/26D;->r:LX/0Ot;

    .line 371801
    move-object/from16 v0, p18

    iput-object v0, p0, LX/26D;->s:LX/0Ot;

    .line 371802
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;ILcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;)I
    .locals 1
    .param p2    # Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 371803
    if-eqz p2, :cond_0

    .line 371804
    const/4 v0, 0x0

    invoke-static {p2, v0}, LX/AzA;->b(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;Z)I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x1

    .line 371805
    :goto_0
    return v0

    .line 371806
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 371807
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fo()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    .line 371808
    :goto_1
    if-eqz v0, :cond_2

    .line 371809
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 371810
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 371811
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/26D;
    .locals 3

    .prologue
    .line 371812
    const-class v1, LX/26D;

    monitor-enter v1

    .line 371813
    :try_start_0
    sget-object v0, LX/26D;->t:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 371814
    sput-object v2, LX/26D;->t:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 371815
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371816
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/26D;->b(LX/0QB;)LX/26D;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 371817
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/26D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 371818
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 371819
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1Po;Lcom/facebook/graphql/model/GraphQLMedia;)LX/33B;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371820
    invoke-static {p0, p1, p2}, LX/26D;->b(LX/26D;LX/1Po;Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/26D;->k:LX/1WM;

    invoke-virtual {v0}, LX/1WM;->a()LX/33B;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;LX/9hN;ILcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;)LX/39A;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/9hN;",
            "I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1bf;",
            ")",
            "LX/39A;"
        }
    .end annotation

    .prologue
    .line 371821
    new-instance v0, LX/Byz;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    move v6, p3

    invoke-direct/range {v0 .. v6}, LX/Byz;-><init>(LX/26D;Landroid/content/Context;LX/9hN;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;I)V

    return-object v0
.end method

.method private static a(Landroid/view/View;ILX/1bf;)LX/9hN;
    .locals 1
    .param p2    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371822
    if-eqz p2, :cond_0

    instance-of v0, p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    if-nez v0, :cond_1

    .line 371823
    :cond_0
    const/4 v0, 0x0

    .line 371824
    :goto_0
    return-object v0

    .line 371825
    :cond_1
    check-cast p0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 371826
    new-instance v0, LX/Byy;

    invoke-direct {v0, p0, p1, p2}, LX/Byy;-><init>(Lcom/facebook/feed/collage/ui/CollageAttachmentView;ILX/1bf;)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;LX/1aZ;LX/1bf;)LX/9hN;
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 371827
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 371828
    :cond_0
    :goto_0
    return-object v0

    .line 371829
    :cond_1
    invoke-static {p0, p1}, LX/BaD;->a(Landroid/view/View;LX/1aZ;)LX/9hP;

    move-result-object v1

    .line 371830
    if-eqz v1, :cond_0

    .line 371831
    new-instance v0, LX/Byx;

    invoke-direct {v0, v1, p2}, LX/Byx;-><init>(LX/9hP;LX/1bf;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 371832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 371833
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 371834
    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v3, 0x6c2aa72f

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    .line 371835
    :goto_1
    if-eqz v1, :cond_0

    invoke-static {v2}, LX/Az0;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    move-object v1, v0

    .line 371836
    goto :goto_0

    .line 371837
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 371838
    sget-object v2, LX/397;->PHOTO:LX/397;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 371839
    :goto_0
    invoke-static {p0, v1, v2}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;ILX/397;)V

    .line 371840
    iget-object p1, p0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->f:LX/396;

    iput-boolean v0, p1, LX/396;->d:Z

    .line 371841
    return-void

    :cond_0
    move v0, v1

    .line 371842
    goto :goto_0
.end method

.method public static a(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;Lcom/facebook/graphql/model/GraphQLStoryAttachment;II)V
    .locals 2

    .prologue
    .line 371843
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 371844
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fo()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    .line 371845
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v0

    .line 371846
    :goto_1
    sget-object v1, LX/397;->PHOTO:LX/397;

    .line 371847
    invoke-static {p0, v0, v1}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;ILX/397;)V

    .line 371848
    return-void

    .line 371849
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 371850
    :cond_1
    add-int v0, p2, p3

    goto :goto_1
.end method

.method public static a(ILX/26M;II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 371636
    invoke-virtual {p1}, LX/26M;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v1, p2, -0x1

    if-ne p0, v1, :cond_0

    if-gt p3, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/26D;Landroid/content/Context;LX/9hN;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;LX/74S;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/9hN;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1bf;",
            "LX/74S;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 371780
    iget-object v0, p0, LX/26D;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bz5;

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, p1

    move-object v2, p2

    move v3, p6

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v8}, LX/Bz5;->a(Landroid/content/Context;LX/9hN;ILcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;LX/74S;ZZ)V

    .line 371781
    return-void
.end method

.method private static b(LX/0QB;)LX/26D;
    .locals 21

    .prologue
    .line 371851
    new-instance v2, LX/26D;

    const-class v3, LX/26F;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/26F;

    const-class v4, LX/26G;

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/26G;

    invoke-static/range {p0 .. p0}, LX/0sX;->a(LX/0QB;)LX/0sX;

    move-result-object v5

    check-cast v5, LX/0sX;

    invoke-static/range {p0 .. p0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v6

    check-cast v6, LX/0tK;

    const-class v7, LX/26H;

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/26H;

    const/16 v8, 0x1e3b

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v9

    check-cast v9, LX/0oz;

    invoke-static/range {p0 .. p0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v10

    check-cast v10, LX/1qa;

    invoke-static/range {p0 .. p0}, LX/26I;->a(LX/0QB;)LX/26I;

    move-result-object v11

    check-cast v11, LX/26I;

    invoke-static/range {p0 .. p0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v12

    check-cast v12, LX/1WM;

    invoke-static/range {p0 .. p0}, LX/1WN;->a(LX/0QB;)LX/1WN;

    move-result-object v13

    check-cast v13, LX/1WN;

    invoke-static/range {p0 .. p0}, LX/1eq;->a(LX/0QB;)LX/1eq;

    move-result-object v14

    check-cast v14, LX/1eq;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v15

    check-cast v15, LX/0ad;

    const/16 v16, 0x5e3

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x12c1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x2cc

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0xbd8

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x1c85

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-direct/range {v2 .. v20}, LX/26D;-><init>(LX/26F;LX/26G;LX/0sX;LX/0tK;LX/26H;LX/0Ot;LX/0oz;LX/1qa;LX/26I;LX/1WM;LX/1WN;LX/1eq;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 371852
    return-object v2
.end method

.method public static b(LX/26D;LX/1Po;Lcom/facebook/graphql/model/GraphQLMedia;)Z
    .locals 2

    .prologue
    .line 371634
    invoke-interface {p1}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    .line 371635
    sget-object v1, LX/1Qt;->FEED:LX/1Qt;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/1Qt;->GOOD_FRIENDS_FEED:LX/1Qt;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, LX/26D;->k:LX/1WM;

    invoke-virtual {v0, p2}, LX/1WM;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 371637
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 371638
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 371639
    iget-object v1, p0, LX/26D;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1VL;

    invoke-virtual {v1, p1}, LX/1VL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;

    move-result-object v3

    .line 371640
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v1

    .line 371641
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 371642
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fo()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v2

    .line 371643
    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v0

    sub-int/2addr v0, v1

    .line 371644
    :goto_1
    if-lez v0, :cond_0

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    .line 371645
    :cond_0
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v1, :cond_4

    .line 371646
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 371647
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 371648
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 371649
    :goto_3
    return v0

    .line 371650
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 371651
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    sub-int/2addr v0, v1

    goto :goto_1

    .line 371652
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 371653
    :cond_4
    const/4 v0, -0x1

    goto :goto_3
.end method

.method public final a(LX/1Ad;LX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/1aZ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ad;",
            "LX/1bf;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ")",
            "LX/1aZ;"
        }
    .end annotation

    .prologue
    .line 371654
    invoke-virtual {p1, p2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    .line 371655
    iget-object v0, p0, LX/26D;->i:LX/1qa;

    const/4 v2, 0x0

    .line 371656
    iget-object v1, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 371657
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0, v1, p4}, LX/1qa;->a(LX/1qa;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 371658
    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    if-eq v1, p2, :cond_0

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    if-ne v1, p2, :cond_1

    .line 371659
    :cond_0
    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 371660
    :goto_0
    if-nez v1, :cond_2

    move-object v1, v2

    .line 371661
    :goto_1
    move-object v0, v1

    .line 371662
    invoke-virtual {p1, v0}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    .line 371663
    iget-object v0, p0, LX/26D;->n:LX/0ad;

    sget-short v1, LX/0fe;->aP:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 371664
    invoke-virtual {p1, v0}, LX/1Ae;->a(Z)LX/1Ae;

    .line 371665
    invoke-virtual {p1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    return-object v0

    .line 371666
    :cond_1
    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    if-ne v1, p2, :cond_3

    .line 371667
    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    goto :goto_0

    .line 371668
    :cond_2
    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    sget-object v2, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 371669
    iput-object v2, v1, LX/1bX;->b:LX/1bY;

    .line 371670
    move-object v1, v1

    .line 371671
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    goto :goto_1

    :cond_3
    move-object v1, v2

    goto :goto_0
.end method

.method public final a(LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/26M;)LX/1bf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Po;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/26M;",
            ")",
            "LX/1bf;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371672
    iget-object v0, p3, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 371673
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 371674
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 371675
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 371676
    iget-object v2, p0, LX/26D;->i:LX/1qa;

    invoke-virtual {v2, p2, v0}, LX/1qa;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/1bf;

    move-result-object v0

    .line 371677
    if-nez v0, :cond_0

    .line 371678
    const/4 v0, 0x0

    .line 371679
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v0

    const/4 v2, 0x1

    .line 371680
    iput-boolean v2, v0, LX/1bX;->g:Z

    .line 371681
    move-object v0, v0

    .line 371682
    invoke-direct {p0, p1, v1}, LX/26D;->a(LX/1Po;Lcom/facebook/graphql/model/GraphQLMedia;)LX/33B;

    move-result-object v1

    .line 371683
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 371684
    move-object v0, v0

    .line 371685
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;)LX/26E;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/0Px",
            "<",
            "LX/26M;",
            ">;)",
            "LX/26E;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 371686
    const/4 v2, 0x0

    .line 371687
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_4

    invoke-virtual {p2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/26N;

    .line 371688
    invoke-interface {v1}, LX/26N;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 371689
    const/4 v1, 0x1

    .line 371690
    :goto_1
    move v1, v1

    .line 371691
    if-eqz v1, :cond_0

    .line 371692
    sget-object v0, LX/26D;->a:LX/26E;

    .line 371693
    :goto_2
    return-object v0

    .line 371694
    :cond_0
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v1

    move v2, v0

    move v3, v1

    move v4, v0

    move v1, v0

    .line 371695
    :goto_3
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 371696
    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26M;

    .line 371697
    iget-object v5, v0, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v5

    .line 371698
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v5

    .line 371699
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 371700
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 371701
    iget-object v5, p0, LX/26D;->m:LX/1eq;

    invoke-virtual {v5, v0}, LX/1eq;->b(Lcom/facebook/graphql/model/GraphQLMedia;)I

    move-result v5

    add-int/2addr v2, v5

    .line 371702
    if-nez v4, :cond_1

    iget-object v5, p0, LX/26D;->m:LX/1eq;

    .line 371703
    invoke-static {v5, p1, v0}, LX/1eq;->e(LX/1eq;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-static {v5, p1, v0}, LX/1eq;->c(LX/1eq;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v6

    if-eqz v6, :cond_5

    const/4 v6, 0x1

    :goto_4
    move v0, v6

    .line 371704
    if-eqz v0, :cond_1

    .line 371705
    const/4 v4, 0x1

    move v3, v1

    .line 371706
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 371707
    :cond_2
    new-instance v0, LX/26E;

    invoke-direct {v0, v4, v3, v2}, LX/26E;-><init>(ZII)V

    goto :goto_2

    .line 371708
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_4
    move v1, v2

    .line 371709
    goto :goto_1

    :cond_5
    const/4 v6, 0x0

    goto :goto_4
.end method

.method public final a(LX/1PT;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/26O;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1PT;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/26O",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation

    .prologue
    .line 371710
    iget-object v0, p0, LX/26D;->e:LX/0tK;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0tK;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371711
    iget-object v0, p0, LX/26D;->b:LX/26F;

    invoke-virtual {v0, p2}, LX/26F;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Aj3;

    move-result-object v0

    .line 371712
    :goto_0
    move-object v0, v0

    .line 371713
    iget-object v1, p0, LX/26D;->f:LX/26H;

    invoke-virtual {v1, v0}, LX/26H;->a(LX/26L;)LX/26O;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, LX/26D;->c:LX/26G;

    invoke-virtual {v0, p2}, LX/26G;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/26K;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/3EE;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I)",
            "LX/3EE;"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 371714
    new-instance v1, LX/3EE;

    iget-object v0, p0, LX/26D;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1VL;

    invoke-virtual {v0, p1}, LX/1VL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    new-instance v2, LX/CDf;

    const/4 v3, 0x0

    invoke-direct {v2, v4, v4, v3}, LX/CDf;-><init>(III)V

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    new-instance v3, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-direct {v1, v0, v4, v2, v3}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    return-object v1
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 371715
    iget-object v0, p0, LX/26D;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2hU;

    iget-object v1, p0, LX/26D;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16I;

    invoke-static {p1, v0, v1}, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->a(Landroid/content/Context;LX/2hU;LX/16I;)Z

    .line 371716
    return-void
.end method

.method public final a(Landroid/content/Context;LX/9hN;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/26M;LX/1bf;LX/74S;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/9hN;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I",
            "LX/26M;",
            "LX/1bf;",
            "LX/74S;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 371717
    invoke-virtual {p5}, LX/26M;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    move-object/from16 v5, p7

    move v6, p4

    .line 371718
    invoke-static/range {v0 .. v6}, LX/26D;->a$redex0(LX/26D;Landroid/content/Context;LX/9hN;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;LX/74S;I)V

    .line 371719
    :goto_0
    return-void

    .line 371720
    :cond_0
    iget-object v0, p0, LX/26D;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bz5;

    if-nez p8, :cond_1

    const/4 v8, 0x1

    :goto_1
    move-object v1, p1

    move-object v2, p2

    move v3, p4

    move-object v4, p3

    move-object v5, p6

    move-object/from16 v6, p7

    move/from16 v7, p8

    invoke-virtual/range {v0 .. v8}, LX/Bz5;->a(Landroid/content/Context;LX/9hN;ILcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;LX/74S;ZZ)V

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/1bf;LX/9hN;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I",
            "LX/1bf;",
            "LX/9hN;",
            ")V"
        }
    .end annotation

    .prologue
    .line 371721
    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/26D;->a(Landroid/content/Context;LX/9hN;ILcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;)LX/39A;

    move-result-object v1

    .line 371722
    iget-object v0, p0, LX/26D;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v2, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080e4a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 371723
    const-class v0, Landroid/support/v4/app/FragmentActivity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 371724
    if-nez v0, :cond_0

    .line 371725
    const/4 v0, 0x0

    invoke-interface {v1, v0}, LX/39A;->a(Ljava/lang/Object;)V

    .line 371726
    :goto_0
    return-void

    .line 371727
    :cond_0
    iget-object v1, p0, LX/26D;->r:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/121;

    sget-object v2, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/121;->a(LX/0yY;LX/0gc;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/26M;LX/1Po;LX/1aZ;)V
    .locals 9
    .param p6    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Po;",
            ":",
            "LX/1Pq;",
            ">(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I",
            "LX/26M;",
            "TE;",
            "LX/1aZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 371728
    iget-object v0, p4, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 371729
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 371730
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 371731
    iget-object v1, p0, LX/26D;->i:LX/1qa;

    invoke-virtual {v1, p2, v0}, LX/1qa;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/1bf;

    move-result-object v1

    .line 371732
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 371733
    :goto_0
    if-eqz p6, :cond_1

    invoke-static {p1, p6, v0}, LX/26D;->a(Landroid/view/View;LX/1aZ;LX/1bf;)LX/9hN;

    move-result-object v7

    .line 371734
    :goto_1
    iget-object v0, p0, LX/26D;->j:LX/26I;

    invoke-interface {p5}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    invoke-virtual {p4}, LX/26M;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object v8, p5

    check-cast v8, LX/1Pq;

    move-object v1, p2

    move v2, p3

    move-object v3, p1

    invoke-virtual/range {v0 .. v8}, LX/26I;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILandroid/view/View;LX/1PT;Ljava/lang/String;ZLX/9hN;LX/1Pq;)V

    .line 371735
    return-void

    .line 371736
    :cond_0
    invoke-static {v1}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-direct {p0, p5, v0}, LX/26D;->a(LX/1Po;Lcom/facebook/graphql/model/GraphQLMedia;)LX/33B;

    move-result-object v0

    .line 371737
    iput-object v0, v1, LX/1bX;->j:LX/33B;

    .line 371738
    move-object v0, v1

    .line 371739
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0

    .line 371740
    :cond_1
    invoke-static {p1, p3, v0}, LX/26D;->a(Landroid/view/View;ILX/1bf;)LX/9hN;

    move-result-object v7

    goto :goto_1
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pt;LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "LX/1Pt;",
            "LX/0Px",
            "<",
            "LX/26M;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 371741
    const/4 v1, 0x0

    .line 371742
    iget-object v0, p0, LX/26D;->n:LX/0ad;

    sget-short v2, LX/1Dd;->E:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 371743
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/26D;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AjI;

    invoke-virtual {v0}, LX/AjI;->a()Z

    move-result v0

    if-nez v0, :cond_a

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 371744
    if-eqz v0, :cond_2

    invoke-interface {p3}, LX/1Pt;->l()Z

    move-result v0

    if-nez v0, :cond_2

    .line 371745
    :cond_1
    return-void

    .line 371746
    :cond_2
    iget-object v0, p0, LX/26D;->n:LX/0ad;

    sget v1, LX/1Dd;->F:I

    const/16 v2, 0xa

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v1

    .line 371747
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 371748
    invoke-virtual {p4}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_5

    invoke-virtual {p4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26M;

    .line 371749
    iget-object v5, v0, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v5

    .line 371750
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v5

    .line 371751
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 371752
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 371753
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    .line 371754
    :goto_2
    if-eqz v0, :cond_3

    .line 371755
    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 371756
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 371757
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 371758
    :cond_5
    move-object v3, v3

    .line 371759
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_3
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 371760
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 371761
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v6

    if-nez v6, :cond_9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 371762
    const/4 v6, 0x0

    .line 371763
    iget-object p2, p0, LX/26D;->h:LX/0oz;

    invoke-virtual {p2}, LX/0oz;->c()LX/0p3;

    move-result-object p2

    .line 371764
    sget-object p4, LX/0p3;->POOR:LX/0p3;

    if-eq p4, p2, :cond_6

    sget-object p4, LX/0p3;->UNKNOWN:LX/0p3;

    if-ne p4, p2, :cond_c

    :cond_6
    const/4 p2, 0x1

    :goto_4
    move p2, p2

    .line 371765
    if-eqz p2, :cond_b

    .line 371766
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    .line 371767
    if-eqz p2, :cond_7

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v6

    .line 371768
    :cond_7
    :goto_5
    move-object v0, v6

    .line 371769
    if-eqz v0, :cond_8

    .line 371770
    invoke-interface {p3, v0, p1}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 371771
    :cond_8
    add-int/lit8 v0, v1, -0x1

    .line 371772
    if-lez v0, :cond_1

    .line 371773
    :goto_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_6

    :cond_a
    move v0, v1

    goto/16 :goto_0

    .line 371774
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    .line 371775
    if-eqz p2, :cond_7

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v6

    goto :goto_5

    :cond_c
    const/4 p2, 0x0

    goto :goto_4
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 371776
    iget-object v1, p0, LX/26D;->d:LX/0sX;

    invoke-virtual {v1}, LX/0sX;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 371777
    :cond_0
    :goto_0
    return-object v0

    .line 371778
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 371779
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
