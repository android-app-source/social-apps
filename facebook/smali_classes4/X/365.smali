.class public final LX/365;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/360;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/26M;

.field public c:LX/1bf;

.field public d:LX/1aZ;

.field public e:Landroid/graphics/PointF;

.field public f:I

.field public g:I

.field public h:I

.field public i:LX/1Pm;

.field public j:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

.field public k:Z

.field public final synthetic l:LX/360;


# direct methods
.method public constructor <init>(LX/360;)V
    .locals 1

    .prologue
    .line 497939
    iput-object p1, p0, LX/365;->l:LX/360;

    .line 497940
    move-object v0, p1

    .line 497941
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 497942
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 497943
    const-string v0, "CollageAttachmentStoryItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 497944
    if-ne p0, p1, :cond_1

    .line 497945
    :cond_0
    :goto_0
    return v0

    .line 497946
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 497947
    goto :goto_0

    .line 497948
    :cond_3
    check-cast p1, LX/365;

    .line 497949
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 497950
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 497951
    if-eq v2, v3, :cond_0

    .line 497952
    iget-object v2, p0, LX/365;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/365;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/365;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 497953
    goto :goto_0

    .line 497954
    :cond_5
    iget-object v2, p1, LX/365;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 497955
    :cond_6
    iget-object v2, p0, LX/365;->b:LX/26M;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/365;->b:LX/26M;

    iget-object v3, p1, LX/365;->b:LX/26M;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 497956
    goto :goto_0

    .line 497957
    :cond_8
    iget-object v2, p1, LX/365;->b:LX/26M;

    if-nez v2, :cond_7

    .line 497958
    :cond_9
    iget-object v2, p0, LX/365;->c:LX/1bf;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/365;->c:LX/1bf;

    iget-object v3, p1, LX/365;->c:LX/1bf;

    invoke-virtual {v2, v3}, LX/1bf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 497959
    goto :goto_0

    .line 497960
    :cond_b
    iget-object v2, p1, LX/365;->c:LX/1bf;

    if-nez v2, :cond_a

    .line 497961
    :cond_c
    iget-object v2, p0, LX/365;->d:LX/1aZ;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/365;->d:LX/1aZ;

    iget-object v3, p1, LX/365;->d:LX/1aZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 497962
    goto :goto_0

    .line 497963
    :cond_e
    iget-object v2, p1, LX/365;->d:LX/1aZ;

    if-nez v2, :cond_d

    .line 497964
    :cond_f
    iget-object v2, p0, LX/365;->e:Landroid/graphics/PointF;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/365;->e:Landroid/graphics/PointF;

    iget-object v3, p1, LX/365;->e:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 497965
    goto :goto_0

    .line 497966
    :cond_11
    iget-object v2, p1, LX/365;->e:Landroid/graphics/PointF;

    if-nez v2, :cond_10

    .line 497967
    :cond_12
    iget v2, p0, LX/365;->f:I

    iget v3, p1, LX/365;->f:I

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 497968
    goto/16 :goto_0

    .line 497969
    :cond_13
    iget v2, p0, LX/365;->g:I

    iget v3, p1, LX/365;->g:I

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 497970
    goto/16 :goto_0

    .line 497971
    :cond_14
    iget v2, p0, LX/365;->h:I

    iget v3, p1, LX/365;->h:I

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 497972
    goto/16 :goto_0

    .line 497973
    :cond_15
    iget-object v2, p0, LX/365;->i:LX/1Pm;

    if-eqz v2, :cond_17

    iget-object v2, p0, LX/365;->i:LX/1Pm;

    iget-object v3, p1, LX/365;->i:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 497974
    goto/16 :goto_0

    .line 497975
    :cond_17
    iget-object v2, p1, LX/365;->i:LX/1Pm;

    if-nez v2, :cond_16

    .line 497976
    :cond_18
    iget-object v2, p0, LX/365;->j:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    if-eqz v2, :cond_1a

    iget-object v2, p0, LX/365;->j:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    iget-object v3, p1, LX/365;->j:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    .line 497977
    goto/16 :goto_0

    .line 497978
    :cond_1a
    iget-object v2, p1, LX/365;->j:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    if-nez v2, :cond_19

    .line 497979
    :cond_1b
    iget-boolean v2, p0, LX/365;->k:Z

    iget-boolean v3, p1, LX/365;->k:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 497980
    goto/16 :goto_0
.end method
