.class public LX/3QS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;

.field private static volatile i:LX/3QS;


# instance fields
.field private final c:LX/0SG;

.field private final d:LX/03V;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2N4;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0W3;

.field private final h:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "LX/FKt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 564517
    const-class v0, LX/3QS;

    .line 564518
    sput-object v0, LX/3QS;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3QS;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/03V;LX/0Or;LX/0Or;LX/0W3;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2N4;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 564509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564510
    iput-object p1, p0, LX/3QS;->c:LX/0SG;

    .line 564511
    iput-object p2, p0, LX/3QS;->d:LX/03V;

    .line 564512
    iput-object p3, p0, LX/3QS;->e:LX/0Or;

    .line 564513
    iput-object p4, p0, LX/3QS;->f:LX/0Or;

    .line 564514
    invoke-static {}, LX/0UK;->b()Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    iput-object v0, p0, LX/3QS;->h:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 564515
    iput-object p5, p0, LX/3QS;->g:LX/0W3;

    .line 564516
    return-void
.end method

.method public static a(LX/0QB;)LX/3QS;
    .locals 9

    .prologue
    .line 564496
    sget-object v0, LX/3QS;->i:LX/3QS;

    if-nez v0, :cond_1

    .line 564497
    const-class v1, LX/3QS;

    monitor-enter v1

    .line 564498
    :try_start_0
    sget-object v0, LX/3QS;->i:LX/3QS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 564499
    if-eqz v2, :cond_0

    .line 564500
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 564501
    new-instance v3, LX/3QS;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const/16 v6, 0xce8

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0xd18

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    invoke-direct/range {v3 .. v8}, LX/3QS;-><init>(LX/0SG;LX/03V;LX/0Or;LX/0Or;LX/0W3;)V

    .line 564502
    move-object v0, v3

    .line 564503
    sput-object v0, LX/3QS;->i:LX/3QS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 564504
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 564505
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 564506
    :cond_1
    sget-object v0, LX/3QS;->i:LX/3QS;

    return-object v0

    .line 564507
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 564508
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/io/File;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 564445
    iget-object v0, p0, LX/3QS;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 564446
    new-instance v2, Ljava/io/File;

    const-string v3, "recent_messages_json.txt"

    invoke-direct {v2, p1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 564447
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 564448
    new-instance v4, Ljava/io/PrintWriter;

    invoke-direct {v4, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 564449
    :try_start_0
    invoke-direct {p0, v0, v1}, LX/3QS;->a(J)Lorg/json/JSONObject;

    move-result-object v0

    .line 564450
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 564451
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 564452
    invoke-static {v4, v5}, LX/1md;->a(Ljava/io/Closeable;Z)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v4, v5}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method

.method private a(J)Lorg/json/JSONObject;
    .locals 11

    .prologue
    .line 564485
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 564486
    iget-object v0, p0, LX/3QS;->h:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FKt;

    .line 564487
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 564488
    iget-object v1, v0, LX/FKt;->b:Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentSkipListMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 564489
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FKs;

    .line 564490
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6fK;

    invoke-virtual {v1}, LX/6fK;->name()Ljava/lang/String;

    move-result-object v1

    iget-wide v8, v2, LX/FKs;->a:J

    iget-object v2, v2, LX/FKs;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v8, v9, v2}, LX/3QS;->a(JLcom/facebook/messaging/model/messages/Message;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 564491
    :cond_0
    iget-object v1, v0, LX/FKt;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 564492
    const-string v2, "REPORT_TIME_CACHE"

    iget-object v0, p0, LX/3QS;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Og;

    invoke-virtual {v0, v1}, LX/2Og;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v0

    invoke-static {p1, p2, v0}, LX/3QS;->a(JLcom/facebook/messaging/model/messages/MessagesCollection;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v5, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 564493
    const-string v2, "REPORT_TIME_DB"

    iget-object v0, p0, LX/3QS;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N4;

    const/4 v6, 0x1

    invoke-virtual {v0, v1, v6}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-static {p1, p2, v0}, LX/3QS;->a(JLcom/facebook/messaging/model/messages/MessagesCollection;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v5, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 564494
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 564495
    :cond_1
    return-object v3
.end method

.method private static a(JLcom/facebook/messaging/model/messages/Message;)Lorg/json/JSONObject;
    .locals 6
    .param p2    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 564482
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 564483
    :cond_0
    const/4 v0, 0x0

    .line 564484
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "recordTime"

    invoke-virtual {v0, v2, p0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "id"

    iget-object v3, p2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "timestampMs"

    iget-wide v4, p2, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "sentTimestampMs"

    iget-wide v4, p2, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "senderInfo"

    iget-object v3, p2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "actionId"

    iget-wide v4, p2, Lcom/facebook/messaging/model/messages/Message;->g:J

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "numAttachments"

    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "numShares"

    iget-object v3, p2, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "offlineThreadingId"

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "isNonAuthoritative"

    iget-boolean v2, p2, Lcom/facebook/messaging/model/messages/Message;->o:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "channelSource"

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    goto :goto_2
.end method

.method private static a(JLcom/facebook/messaging/model/messages/MessagesCollection;)Lorg/json/JSONObject;
    .locals 2
    .param p2    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 564519
    if-nez p2, :cond_0

    .line 564520
    const/4 v0, 0x0

    .line 564521
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/facebook/messaging/model/messages/MessagesCollection;->c()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/3QS;->a(JLcom/facebook/messaging/model/messages/Message;)Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6fK;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 6
    .param p2    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 564464
    if-nez p2, :cond_1

    .line 564465
    :cond_0
    :goto_0
    return-void

    .line 564466
    :cond_1
    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 564467
    if-nez v1, :cond_2

    .line 564468
    sget-object v0, LX/3QS;->a:Ljava/lang/Class;

    const-string v1, "Tried to track message without threadkey"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 564469
    :cond_2
    iget-object v0, p0, LX/3QS;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 564470
    iget-object v0, p0, LX/3QS;->h:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 564471
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 564472
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FKt;

    .line 564473
    iget-object v5, v0, LX/FKt;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v5, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 564474
    invoke-virtual {v0, v2, v3, p1, p2}, LX/FKt;->a(JLX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    .line 564475
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 564476
    iget-object v1, p0, LX/3QS;->h:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 564477
    :cond_4
    new-instance v0, LX/FKt;

    invoke-direct {v0, v1}, LX/FKt;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 564478
    invoke-virtual {v0, v2, v3, p1, p2}, LX/FKt;->a(JLX/6fK;Lcom/facebook/messaging/model/messages/Message;)V

    .line 564479
    iget-object v1, p0, LX/3QS;->h:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 564480
    iget-object v0, p0, LX/3QS;->h:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    .line 564481
    iget-object v0, p0, LX/3QS;->h:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove()Ljava/lang/Object;

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 564462
    iget-object v0, p0, LX/3QS;->h:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 564463
    return-void
.end method

.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 564453
    :try_start_0
    invoke-direct {p0, p1}, LX/3QS;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 564454
    const-string v1, "recent_messages_json.txt"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 564455
    :goto_0
    return-object v0

    .line 564456
    :catch_0
    move-exception v0

    .line 564457
    iget-object v1, p0, LX/3QS;->d:LX/03V;

    sget-object v2, LX/3QS;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 564458
    throw v0

    .line 564459
    :catch_1
    move-exception v0

    .line 564460
    iget-object v1, p0, LX/3QS;->d:LX/03V;

    sget-object v2, LX/3QS;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 564461
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 564439
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 564440
    :try_start_0
    invoke-direct {p0, p1}, LX/3QS;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 564441
    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v3, "recent_messages_json.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "text/plain"

    invoke-direct {v2, v3, v1, v4}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 564442
    return-object v0

    .line 564443
    :catch_0
    move-exception v0

    .line 564444
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Failed to write recent messages file"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 564438
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 564437
    iget-object v0, p0, LX/3QS;->g:LX/0W3;

    sget-wide v2, LX/0X5;->bg:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
