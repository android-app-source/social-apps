.class public LX/3O9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field public final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/facebook/contacts/picker/ContactPickerHeaderViewController;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/3OA;

.field public d:LX/3OB;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 560122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560123
    iput-object p1, p0, LX/3O9;->a:Landroid/view/ViewGroup;

    .line 560124
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/3O9;->b:Ljava/util/Queue;

    .line 560125
    new-instance v0, LX/3OA;

    invoke-direct {v0, p0}, LX/3OA;-><init>(LX/3O9;)V

    iput-object v0, p0, LX/3O9;->c:LX/3OA;

    .line 560126
    return-void
.end method

.method public static b(LX/3O9;)V
    .locals 7

    .prologue
    .line 560130
    invoke-static {p0}, LX/3O9;->c(LX/3O9;)V

    .line 560131
    :cond_0
    iget-object v0, p0, LX/3O9;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 560132
    iget-object v0, p0, LX/3O9;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OB;

    .line 560133
    iget-object v1, v0, LX/3OB;->e:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 560134
    const/4 v1, 0x1

    .line 560135
    :cond_1
    :goto_0
    move v1, v1

    .line 560136
    if-eqz v1, :cond_0

    .line 560137
    iget-object v1, p0, LX/3O9;->a:Landroid/view/ViewGroup;

    iget-object v2, p0, LX/3O9;->c:LX/3OA;

    .line 560138
    iget-object v3, v0, LX/3OB;->b:LX/0iA;

    sget-object v4, LX/3OB;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v5, LX/3OC;

    invoke-virtual {v3, v4, v5}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v3

    check-cast v3, LX/3OC;

    .line 560139
    if-nez v3, :cond_4

    .line 560140
    :goto_1
    iput-object v0, p0, LX/3O9;->d:LX/3OB;

    .line 560141
    :cond_2
    return-void

    .line 560142
    :cond_3
    iget-object v1, v0, LX/3OB;->b:LX/0iA;

    sget-object v2, LX/3OB;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/3OC;

    invoke-virtual {v1, v2, v3}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)Z

    move-result v1

    .line 560143
    if-nez v1, :cond_1

    .line 560144
    invoke-virtual {v0}, LX/3OB;->b()V

    goto :goto_0

    .line 560145
    :cond_4
    iput-object v1, v0, LX/3OB;->f:Landroid/view/ViewGroup;

    .line 560146
    iput-object v2, v0, LX/3OB;->g:LX/3OA;

    .line 560147
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v4

    .line 560148
    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "qp_definition"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 560149
    iget-object v5, v0, LX/3OB;->c:Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;

    invoke-virtual {v3}, LX/3OC;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v0, LX/3OB;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v1, v4, v3, v6}, Lcom/facebook/quickpromotion/ui/QuickPromotionDivebarViewFactory;->a(Landroid/view/ViewGroup;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v3

    iput-object v3, v0, LX/3OB;->e:Landroid/view/View;

    .line 560150
    iget-object v3, v0, LX/3OB;->f:Landroid/view/ViewGroup;

    iget-object v4, v0, LX/3OB;->e:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1
.end method

.method public static c(LX/3O9;)V
    .locals 1

    .prologue
    .line 560127
    iget-object v0, p0, LX/3O9;->d:LX/3OB;

    if-eqz v0, :cond_0

    .line 560128
    const/4 v0, 0x0

    iput-object v0, p0, LX/3O9;->d:LX/3OB;

    .line 560129
    :cond_0
    return-void
.end method
