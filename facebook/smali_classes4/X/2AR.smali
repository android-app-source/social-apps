.class public final LX/2AR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 377332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 377333
    new-instance v0, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult;

    invoke-direct {v0, p1}, Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 377334
    new-array v0, p1, [Lcom/facebook/languages/switcher/protocol/SuggestedLocalesResult;

    return-object v0
.end method
