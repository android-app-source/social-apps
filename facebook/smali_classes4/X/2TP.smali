.class public LX/2TP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2TP;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414393
    iput-object p1, p0, LX/2TP;->a:Landroid/content/Context;

    .line 414394
    return-void
.end method

.method public static a(LX/0QB;)LX/2TP;
    .locals 4

    .prologue
    .line 414379
    sget-object v0, LX/2TP;->b:LX/2TP;

    if-nez v0, :cond_1

    .line 414380
    const-class v1, LX/2TP;

    monitor-enter v1

    .line 414381
    :try_start_0
    sget-object v0, LX/2TP;->b:LX/2TP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 414382
    if-eqz v2, :cond_0

    .line 414383
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 414384
    new-instance p0, LX/2TP;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/2TP;-><init>(Landroid/content/Context;)V

    .line 414385
    move-object v0, p0

    .line 414386
    sput-object v0, LX/2TP;->b:LX/2TP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414387
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 414388
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 414389
    :cond_1
    sget-object v0, LX/2TP;->b:LX/2TP;

    return-object v0

    .line 414390
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 414391
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 414395
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, LX/2TP;->a:Landroid/content/Context;

    const-class v2, Lcom/facebook/vault/service/VaultConnectivityChangeReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 414396
    iget-object v1, p0, LX/2TP;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v1

    .line 414397
    if-eq v1, p1, :cond_0

    .line 414398
    iget-object v1, p0, LX/2TP;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 414399
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 414377
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/2TP;->a(I)V

    .line 414378
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 414375
    const/4 v0, 0x2

    invoke-direct {p0, v0}, LX/2TP;->a(I)V

    .line 414376
    return-void
.end method
