.class public final LX/2dS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 443083
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 443084
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 443085
    :goto_0
    return v1

    .line 443086
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_5

    .line 443087
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 443088
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 443089
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 443090
    const-string v10, "feature_enabled"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 443091
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v8, v4

    move v4, v2

    goto :goto_1

    .line 443092
    :cond_1
    const-string v10, "gravity_learn_more_link"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 443093
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 443094
    :cond_2
    const-string v10, "location_tracking_enabled"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 443095
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 443096
    :cond_3
    const-string v10, "notifications_enabled"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 443097
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 443098
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 443099
    :cond_5
    const/4 v9, 0x4

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 443100
    if-eqz v4, :cond_6

    .line 443101
    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 443102
    :cond_6
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 443103
    if-eqz v3, :cond_7

    .line 443104
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 443105
    :cond_7
    if-eqz v0, :cond_8

    .line 443106
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 443107
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto :goto_1
.end method
