.class public LX/29y;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/2A0;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/2A0;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 376829
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2A0;
    .locals 4

    .prologue
    .line 376830
    sget-object v0, LX/29y;->a:LX/2A0;

    if-nez v0, :cond_1

    .line 376831
    const-class v1, LX/29y;

    monitor-enter v1

    .line 376832
    :try_start_0
    sget-object v0, LX/29y;->a:LX/2A0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 376833
    if-eqz v2, :cond_0

    .line 376834
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 376835
    invoke-static {v0}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v3

    check-cast v3, LX/0Xp;

    invoke-static {v0}, LX/0bD;->a(LX/0QB;)LX/0bD;

    move-result-object p0

    check-cast p0, LX/0bD;

    invoke-static {v3, p0}, LX/29z;->a(LX/0Xp;LX/0bD;)LX/2A0;

    move-result-object v3

    move-object v0, v3

    .line 376836
    sput-object v0, LX/29y;->a:LX/2A0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376837
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 376838
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 376839
    :cond_1
    sget-object v0, LX/29y;->a:LX/2A0;

    return-object v0

    .line 376840
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 376841
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 376842
    invoke-static {p0}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v0

    check-cast v0, LX/0Xp;

    invoke-static {p0}, LX/0bD;->a(LX/0QB;)LX/0bD;

    move-result-object v1

    check-cast v1, LX/0bD;

    invoke-static {v0, v1}, LX/29z;->a(LX/0Xp;LX/0bD;)LX/2A0;

    move-result-object v0

    return-object v0
.end method
