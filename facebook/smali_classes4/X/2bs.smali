.class public LX/2bs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final d:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final e:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final q:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final r:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final s:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final t:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 439861
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "privacy/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 439862
    sput-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "selected_privacy_option"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->b:LX/0Tn;

    .line 439863
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "breakfast_club_composer_nux_v2"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->c:LX/0Tn;

    .line 439864
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "small_audience_privacy_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->d:LX/0Tn;

    .line 439865
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "small_audience_privacy_nux_config"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->e:LX/0Tn;

    .line 439866
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "REVIEW_PRIVACY_REMINDER_NUX"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->f:LX/0Tn;

    .line 439867
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "education_banner_nux_config"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->g:LX/0Tn;

    .line 439868
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "audience_alignment_should_show_tux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->h:LX/0Tn;

    .line 439869
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "audience_alignment_only_me_should_show_tux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->i:LX/0Tn;

    .line 439870
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "audience_alignment_should_show_roadblock"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->s:LX/0Tn;

    .line 439871
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "audience_alignment_seen_tux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->t:LX/0Tn;

    .line 439872
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "newcomer_audience_should_show_selector"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->j:LX/0Tn;

    .line 439873
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "newcomer_audience_seen_selector"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->k:LX/0Tn;

    .line 439874
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "sticky_guardrail_config"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->l:LX/0Tn;

    .line 439875
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "default_privacy_client_override"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->m:LX/0Tn;

    .line 439876
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "default_privacy_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->n:LX/0Tn;

    .line 439877
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "inline_privacy_survey_config"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->o:LX/0Tn;

    .line 439878
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "alignment_roadblock_preference"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->p:LX/0Tn;

    .line 439879
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "alignment_roadblock_impressions"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->q:LX/0Tn;

    .line 439880
    sget-object v0, LX/2bs;->a:LX/0Tn;

    const-string v1, "alignment_roadblock_finished_state"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2bs;->r:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 439881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439882
    return-void
.end method

.method public static a(LX/0QB;)LX/2bs;
    .locals 1

    .prologue
    .line 439883
    new-instance v0, LX/2bs;

    invoke-direct {v0}, LX/2bs;-><init>()V

    .line 439884
    move-object v0, v0

    .line 439885
    return-object v0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 439886
    sget-object v0, LX/2bs;->c:LX/0Tn;

    sget-object v1, LX/2bs;->d:LX/0Tn;

    sget-object v2, LX/2bs;->e:LX/0Tn;

    sget-object v3, LX/2bs;->f:LX/0Tn;

    sget-object v4, LX/2bs;->h:LX/0Tn;

    sget-object v5, LX/2bs;->i:LX/0Tn;

    const/4 v6, 0x6

    new-array v6, v6, [LX/0Tn;

    const/4 v7, 0x0

    sget-object v8, LX/2bs;->s:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, LX/2bs;->t:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, LX/2bs;->g:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, LX/2bs;->j:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    sget-object v8, LX/2bs;->k:LX/0Tn;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    sget-object v8, LX/2bs;->q:LX/0Tn;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0Rf;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 439887
    sget-object v0, LX/2bs;->b:LX/0Tn;

    sget-object v1, LX/2bs;->l:LX/0Tn;

    sget-object v2, LX/2bs;->m:LX/0Tn;

    sget-object v3, LX/2bs;->n:LX/0Tn;

    sget-object v4, LX/2bs;->o:LX/0Tn;

    sget-object v5, LX/2bs;->p:LX/0Tn;

    const/4 v6, 0x0

    new-array v6, v6, [LX/0Tn;

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
