.class public LX/2eu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Z


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 445996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445997
    const/16 v0, 0x3b3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/2eu;->a:Z

    .line 445998
    return-void
.end method

.method public static a(LX/0QB;)LX/2eu;
    .locals 4

    .prologue
    .line 445999
    const-class v1, LX/2eu;

    monitor-enter v1

    .line 446000
    :try_start_0
    sget-object v0, LX/2eu;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 446001
    sput-object v2, LX/2eu;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 446002
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446003
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 446004
    new-instance p0, LX/2eu;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/2eu;-><init>(LX/0Uh;)V

    .line 446005
    move-object v0, p0

    .line 446006
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 446007
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2eu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446008
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 446009
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
