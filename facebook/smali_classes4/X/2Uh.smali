.class public LX/2Uh;
.super LX/1Eg;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/2Uh;


# instance fields
.field public final a:LX/2Uj;

.field public final b:LX/0SG;

.field private final c:LX/0TD;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/media/upload/udp/UDPReceivingBackgroundTask$ServerMessageNotificationListener;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/01T;

.field public f:Ljava/net/DatagramSocket;


# direct methods
.method public constructor <init>(LX/2Uj;LX/0SG;LX/0TD;LX/01T;)V
    .locals 1
    .param p1    # LX/2Uj;
        .annotation runtime Lcom/facebook/messaging/media/upload/udp/UDPIncomingPacketQueue;
        .end annotation
    .end param
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416456
    const-string v0, "udp_receiving_task"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 416457
    iput-object p1, p0, LX/2Uh;->a:LX/2Uj;

    .line 416458
    iput-object p2, p0, LX/2Uh;->b:LX/0SG;

    .line 416459
    iput-object p3, p0, LX/2Uh;->c:LX/0TD;

    .line 416460
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2Uh;->d:Ljava/util/List;

    .line 416461
    iput-object p4, p0, LX/2Uh;->e:LX/01T;

    .line 416462
    return-void
.end method

.method public static a(LX/0QB;)LX/2Uh;
    .locals 7

    .prologue
    .line 416463
    sget-object v0, LX/2Uh;->g:LX/2Uh;

    if-nez v0, :cond_1

    .line 416464
    const-class v1, LX/2Uh;

    monitor-enter v1

    .line 416465
    :try_start_0
    sget-object v0, LX/2Uh;->g:LX/2Uh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416466
    if-eqz v2, :cond_0

    .line 416467
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416468
    new-instance p0, LX/2Uh;

    invoke-static {v0}, LX/2Ui;->a(LX/0QB;)LX/2Uj;

    move-result-object v3

    check-cast v3, LX/2Uj;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v6

    check-cast v6, LX/01T;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2Uh;-><init>(LX/2Uj;LX/0SG;LX/0TD;LX/01T;)V

    .line 416469
    move-object v0, p0

    .line 416470
    sput-object v0, LX/2Uh;->g:LX/2Uh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416471
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416472
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416473
    :cond_1
    sget-object v0, LX/2Uh;->g:LX/2Uh;

    return-object v0

    .line 416474
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416475
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 416476
    const-class v0, Lcom/facebook/messaging/background/annotations/MessagesLocalTaskTag;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 416477
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416478
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 416479
    iget-object v0, p0, LX/2Uh;->e:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/2Uh;->f:Ljava/net/DatagramSocket;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416480
    iget-object v0, p0, LX/2Uh;->c:LX/0TD;

    new-instance v1, LX/FIi;

    invoke-direct {v1, p0}, LX/FIi;-><init>(LX/2Uh;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
