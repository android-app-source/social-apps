.class public LX/3Qb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Pw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/3Qb;


# instance fields
.field private final b:LX/3Qc;

.field private final c:LX/3Qn;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 565378
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/notifications/constants/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->BOOTSTRAP_UPDATED:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/3Qb;->a:LX/2QP;

    return-void
.end method

.method public constructor <init>(LX/3Qc;LX/3Qn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565380
    iput-object p1, p0, LX/3Qb;->b:LX/3Qc;

    .line 565381
    iput-object p2, p0, LX/3Qb;->c:LX/3Qn;

    .line 565382
    return-void
.end method

.method public static a(LX/0QB;)LX/3Qb;
    .locals 5

    .prologue
    .line 565383
    sget-object v0, LX/3Qb;->d:LX/3Qb;

    if-nez v0, :cond_1

    .line 565384
    const-class v1, LX/3Qb;

    monitor-enter v1

    .line 565385
    :try_start_0
    sget-object v0, LX/3Qb;->d:LX/3Qb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 565386
    if-eqz v2, :cond_0

    .line 565387
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 565388
    new-instance p0, LX/3Qb;

    invoke-static {v0}, LX/3Qc;->a(LX/0QB;)LX/3Qc;

    move-result-object v3

    check-cast v3, LX/3Qc;

    invoke-static {v0}, LX/3Qn;->a(LX/0QB;)LX/3Qn;

    move-result-object v4

    check-cast v4, LX/3Qn;

    invoke-direct {p0, v3, v4}, LX/3Qb;-><init>(LX/3Qc;LX/3Qn;)V

    .line 565389
    move-object v0, p0

    .line 565390
    sput-object v0, LX/3Qb;->d:LX/3Qb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565391
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 565392
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565393
    :cond_1
    sget-object v0, LX/3Qb;->d:LX/3Qb;

    return-object v0

    .line 565394
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 565395
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 565396
    sget-object v0, LX/3Qb;->a:LX/2QP;

    return-object v0
.end method

.method public final a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 3

    .prologue
    .line 565397
    iget-object v0, p0, LX/3Qb;->b:LX/3Qc;

    .line 565398
    invoke-static {v0}, LX/3Qc;->l(LX/3Qc;)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, LX/3Qc;->a$redex0(LX/3Qc;J)V

    .line 565399
    iget-object v0, p0, LX/3Qb;->c:LX/3Qn;

    .line 565400
    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, LX/3Qn;->c(LX/3Qn;J)V

    .line 565401
    return-void
.end method
