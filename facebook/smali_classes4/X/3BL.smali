.class public LX/3BL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3BL;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1V8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1V8;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 528060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528061
    iput-object p1, p0, LX/3BL;->b:LX/0Ot;

    .line 528062
    iput-object p2, p0, LX/3BL;->a:LX/0Ot;

    .line 528063
    return-void
.end method

.method public static a(IF)I
    .locals 1

    .prologue
    .line 528059
    int-to-float v0, p0

    div-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method public static a(LX/0QB;)LX/3BL;
    .locals 5

    .prologue
    .line 528043
    sget-object v0, LX/3BL;->c:LX/3BL;

    if-nez v0, :cond_1

    .line 528044
    const-class v1, LX/3BL;

    monitor-enter v1

    .line 528045
    :try_start_0
    sget-object v0, LX/3BL;->c:LX/3BL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 528046
    if-eqz v2, :cond_0

    .line 528047
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 528048
    new-instance v3, LX/3BL;

    const/16 v4, 0x759

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x1b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/3BL;-><init>(LX/0Ot;LX/0Ot;)V

    .line 528049
    move-object v0, v3

    .line 528050
    sput-object v0, LX/3BL;->c:LX/3BL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 528051
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 528052
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 528053
    :cond_1
    sget-object v0, LX/3BL;->c:LX/3BL;

    return-object v0

    .line 528054
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 528055
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 528056
    iget-object v0, p0, LX/3BL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 528057
    iget-object v0, p0, LX/3BL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1V8;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    iget v3, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v0, v2, p1, v3}, LX/1V8;->a(LX/1Ua;Lcom/facebook/feed/rows/core/props/FeedProps;F)I

    move-result v0

    .line 528058
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    return v0
.end method
