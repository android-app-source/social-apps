.class public final enum LX/28H;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/28H;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/28H;

.field public static final enum FETCH_LANGUAGE_LIST:LX/28H;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 373845
    new-instance v0, LX/28H;

    const-string v1, "FETCH_LANGUAGE_LIST"

    invoke-direct {v0, v1, v2}, LX/28H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/28H;->FETCH_LANGUAGE_LIST:LX/28H;

    .line 373846
    const/4 v0, 0x1

    new-array v0, v0, [LX/28H;

    sget-object v1, LX/28H;->FETCH_LANGUAGE_LIST:LX/28H;

    aput-object v1, v0, v2

    sput-object v0, LX/28H;->$VALUES:[LX/28H;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 373847
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/28H;
    .locals 1

    .prologue
    .line 373848
    const-class v0, LX/28H;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/28H;

    return-object v0
.end method

.method public static values()[LX/28H;
    .locals 1

    .prologue
    .line 373849
    sget-object v0, LX/28H;->$VALUES:[LX/28H;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/28H;

    return-object v0
.end method
