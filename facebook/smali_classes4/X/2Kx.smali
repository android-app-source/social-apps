.class public LX/2Kx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field private static final e:Ljava/lang/String;

.field private static f:Ljava/security/MessageDigest;

.field private static volatile i:LX/2Kx;


# instance fields
.field public g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private h:LX/2Ky;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 394208
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "glc/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 394209
    sput-object v0, LX/2Kx;->a:LX/0Tn;

    const-string v1, "device_hash/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Kx;->b:LX/0Tn;

    .line 394210
    sget-object v0, LX/2Kx;->a:LX/0Tn;

    const-string v1, "device_fbid/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Kx;->c:LX/0Tn;

    .line 394211
    sget-object v0, LX/2Kx;->a:LX/0Tn;

    const-string v1, "all_libs_uploaded/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Kx;->d:LX/0Tn;

    .line 394212
    const-class v0, LX/2Kx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Kx;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Ky;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 394119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394120
    iput-object p1, p0, LX/2Kx;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 394121
    iput-object p2, p0, LX/2Kx;->h:LX/2Ky;

    .line 394122
    return-void
.end method

.method public static a(LX/0QB;)LX/2Kx;
    .locals 5

    .prologue
    .line 394123
    sget-object v0, LX/2Kx;->i:LX/2Kx;

    if-nez v0, :cond_1

    .line 394124
    const-class v1, LX/2Kx;

    monitor-enter v1

    .line 394125
    :try_start_0
    sget-object v0, LX/2Kx;->i:LX/2Kx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 394126
    if-eqz v2, :cond_0

    .line 394127
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 394128
    new-instance p0, LX/2Kx;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Ky;->a(LX/0QB;)LX/2Ky;

    move-result-object v4

    check-cast v4, LX/2Ky;

    invoke-direct {p0, v3, v4}, LX/2Kx;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Ky;)V

    .line 394129
    move-object v0, p0

    .line 394130
    sput-object v0, LX/2Kx;->i:LX/2Kx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394131
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 394132
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394133
    :cond_1
    sget-object v0, LX/2Kx;->i:LX/2Kx;

    return-object v0

    .line 394134
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 394135
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/io/File;)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v2, 0x2000

    const/4 v0, 0x0

    .line 394136
    if-eqz p0, :cond_0

    invoke-static {}, LX/2Kx;->g()Ljava/security/MessageDigest;

    move-result-object v1

    if-nez v1, :cond_1

    .line 394137
    :cond_0
    :goto_0
    return-object v0

    .line 394138
    :cond_1
    new-array v1, v2, [B

    .line 394139
    :try_start_0
    new-instance v2, Ljava/security/DigestInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {}, LX/2Kx;->g()Ljava/security/MessageDigest;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/security/DigestInputStream;-><init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394140
    :cond_2
    const/4 v3, 0x0

    const/16 v4, 0x2000

    :try_start_1
    invoke-virtual {v2, v1, v3, v4}, Ljava/security/DigestInputStream;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 394141
    invoke-virtual {v2}, Ljava/security/DigestInputStream;->close()V

    .line 394142
    sget-object v1, LX/2Kx;->f:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v1, v3}, LX/1u4;->a([BZ)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 394143
    invoke-static {v2}, LX/2Kx;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 394144
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 394145
    :goto_1
    :try_start_2
    sget-object v3, LX/2Kx;->e:Ljava/lang/String;

    const-string v4, "Failed to compute file hash"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 394146
    invoke-static {v2}, LX/2Kx;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    invoke-static {v2}, LX/2Kx;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 394147
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static a()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/GiZ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 394148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 394149
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v3, "/system/lib"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v0}, LX/2Kx;->a(Ljava/io/File;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    .line 394150
    :goto_0
    return-object v0

    .line 394151
    :catch_0
    move-exception v0

    .line 394152
    sget-object v2, LX/2Kx;->e:Ljava/lang/String;

    const-string v3, "Failed to compute system lib hashes"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 394153
    goto :goto_0

    .line 394154
    :catch_1
    move-exception v0

    .line 394155
    sget-object v2, LX/2Kx;->e:Ljava/lang/String;

    const-string v3, "Failed to compute system lib hashes"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 394156
    goto :goto_0
.end method

.method public static a(Ljava/io/Closeable;)V
    .locals 3

    .prologue
    .line 394157
    if-nez p0, :cond_0

    .line 394158
    :goto_0
    return-void

    .line 394159
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 394160
    :catch_0
    move-exception v0

    .line 394161
    sget-object v1, LX/2Kx;->e:Ljava/lang/String;

    const-string v2, "Error while closing stream: "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Ljava/io/File;Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/ArrayList",
            "<",
            "LX/GiZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 394162
    if-nez p0, :cond_1

    .line 394163
    :cond_0
    return-void

    .line 394164
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 394165
    if-eqz v1, :cond_0

    .line 394166
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 394167
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_3

    .line 394168
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".*\\.so.*"

    invoke-virtual {v4, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 394169
    new-instance v4, LX/GiZ;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3}, LX/2Kx;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v3, v7}, LX/GiZ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 394170
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 394171
    :cond_3
    invoke-static {v3, p1}, LX/2Kx;->a(Ljava/io/File;Ljava/util/ArrayList;)V

    goto :goto_1
.end method

.method public static b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 394172
    invoke-static {}, LX/2Kx;->g()Ljava/security/MessageDigest;

    move-result-object v0

    if-nez v0, :cond_0

    .line 394173
    const/4 v0, 0x0

    .line 394174
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/2Kx;->f:Ljava/security/MessageDigest;

    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1u4;->a([BZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static g()Ljava/security/MessageDigest;
    .locals 1

    .prologue
    .line 394175
    sget-object v0, LX/2Kx;->f:Ljava/security/MessageDigest;

    if-nez v0, :cond_0

    .line 394176
    const-string v0, "SHA-1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    sput-object v0, LX/2Kx;->f:Ljava/security/MessageDigest;

    .line 394177
    :cond_0
    sget-object v0, LX/2Kx;->f:Ljava/security/MessageDigest;

    return-object v0
.end method

.method private i()V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 394178
    iget-object v0, p0, LX/2Kx;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 394179
    sget-object v1, LX/2Kx;->b:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 394180
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 394181
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 394182
    iget-object v0, p0, LX/2Kx;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 394183
    sget-object v1, LX/2Kx;->c:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 394184
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 394185
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 394186
    iget-object v0, p0, LX/2Kx;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 394187
    sget-object v1, LX/2Kx;->d:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 394188
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 394189
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 394190
    iget-object v0, p0, LX/2Kx;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 394191
    sget-object v1, LX/2Kx;->b:LX/0Tn;

    invoke-static {}, LX/2Kx;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 394192
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 394193
    return-void
.end method

.method public final d()Z
    .locals 4

    .prologue
    .line 394194
    invoke-static {}, LX/2Kx;->b()Ljava/lang/String;

    move-result-object v0

    .line 394195
    iget-object v1, p0, LX/2Kx;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2Kx;->b:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 394196
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 394197
    iget-object v1, p0, LX/2Kx;->h:LX/2Ky;

    .line 394198
    iget-object v2, v1, LX/2Ky;->a:LX/0Uh;

    const/16 v3, 0x4d7

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 394199
    if-nez v1, :cond_0

    .line 394200
    :goto_0
    return v0

    .line 394201
    :cond_0
    invoke-virtual {p0}, LX/2Kx;->d()Z

    move-result v1

    if-nez v1, :cond_2

    .line 394202
    const/4 v0, 0x0

    .line 394203
    iget-object v1, p0, LX/2Kx;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2Kx;->d:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 394204
    goto :goto_0

    .line 394205
    :cond_2
    invoke-direct {p0}, LX/2Kx;->i()V

    .line 394206
    invoke-virtual {p0, v0}, LX/2Kx;->a(Z)V

    .line 394207
    const/4 v0, 0x1

    goto :goto_0
.end method
