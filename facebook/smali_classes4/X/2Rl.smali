.class public LX/2Rl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:LX/0Tn;

.field private static volatile i:LX/2Rl;


# instance fields
.field private final c:Z

.field private final d:Landroid/content/Context;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/0Xl;

.field private final g:LX/0kb;

.field private final h:LX/0s6;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 410118
    const-class v0, LX/2Rl;

    sput-object v0, LX/2Rl;->a:Ljava/lang/Class;

    .line 410119
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "package_removed_for_fbns/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Rl;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;LX/0kb;LX/0s6;)V
    .locals 1
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410098
    iput-object p1, p0, LX/2Rl;->d:Landroid/content/Context;

    .line 410099
    iput-object p2, p0, LX/2Rl;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 410100
    iput-object p3, p0, LX/2Rl;->f:LX/0Xl;

    .line 410101
    iput-object p4, p0, LX/2Rl;->g:LX/0kb;

    .line 410102
    iput-object p5, p0, LX/2Rl;->h:LX/0s6;

    .line 410103
    iget-object v0, p0, LX/2Rl;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2RD;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/2Rl;->c:Z

    .line 410104
    return-void
.end method

.method public static a(LX/0QB;)LX/2Rl;
    .locals 9

    .prologue
    .line 410105
    sget-object v0, LX/2Rl;->i:LX/2Rl;

    if-nez v0, :cond_1

    .line 410106
    const-class v1, LX/2Rl;

    monitor-enter v1

    .line 410107
    :try_start_0
    sget-object v0, LX/2Rl;->i:LX/2Rl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 410108
    if-eqz v2, :cond_0

    .line 410109
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 410110
    new-instance v3, LX/2Rl;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v7

    check-cast v7, LX/0kb;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v8

    check-cast v8, LX/0s6;

    invoke-direct/range {v3 .. v8}, LX/2Rl;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;LX/0kb;LX/0s6;)V

    .line 410111
    move-object v0, v3

    .line 410112
    sput-object v0, LX/2Rl;->i:LX/2Rl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410113
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 410114
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 410115
    :cond_1
    sget-object v0, LX/2Rl;->i:LX/2Rl;

    return-object v0

    .line 410116
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 410117
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2Rl;)V
    .locals 4

    .prologue
    .line 410087
    iget-object v0, p0, LX/2Rl;->g:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 410088
    :cond_0
    return-void

    .line 410089
    :cond_1
    iget-object v0, p0, LX/2Rl;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2Rl;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    .line 410090
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 410091
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 410092
    sget-object v2, LX/2Rl;->b:LX/0Tn;

    invoke-virtual {v0, v2}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v0

    .line 410093
    iget-object v2, p0, LX/2Rl;->h:LX/0s6;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/01H;->d(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 410094
    if-eqz v2, :cond_2

    .line 410095
    invoke-virtual {p0, v0}, LX/2Rl;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 410096
    :cond_2
    iget-object v2, p0, LX/2Rl;->d:Landroid/content/Context;

    const-string v3, "retry"

    invoke-static {v2, v0, v3}, Lcom/facebook/push/crossapp/PackageRemovedReporterService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 410085
    iget-object v0, p0, LX/2Rl;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v0, LX/2Rl;->b:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 410086
    return-void
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 410080
    iget-boolean v0, p0, LX/2Rl;->c:Z

    if-nez v0, :cond_0

    .line 410081
    :goto_0
    return-void

    .line 410082
    :cond_0
    new-instance v0, LX/2Rr;

    invoke-direct {v0, p0}, LX/2Rr;-><init>(LX/2Rl;)V

    .line 410083
    iget-object v1, p0, LX/2Rl;->f:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 410084
    invoke-static {p0}, LX/2Rl;->a$redex0(LX/2Rl;)V

    goto :goto_0
.end method
