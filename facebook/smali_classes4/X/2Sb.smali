.class public LX/2Sb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2Sb;


# instance fields
.field public final a:LX/2Sc;


# direct methods
.method public constructor <init>(LX/2Sc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 412711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412712
    iput-object p1, p0, LX/2Sb;->a:LX/2Sc;

    .line 412713
    return-void
.end method

.method public static a(LX/0QB;)LX/2Sb;
    .locals 4

    .prologue
    .line 412714
    sget-object v0, LX/2Sb;->b:LX/2Sb;

    if-nez v0, :cond_1

    .line 412715
    const-class v1, LX/2Sb;

    monitor-enter v1

    .line 412716
    :try_start_0
    sget-object v0, LX/2Sb;->b:LX/2Sb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 412717
    if-eqz v2, :cond_0

    .line 412718
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 412719
    new-instance p0, LX/2Sb;

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v3

    check-cast v3, LX/2Sc;

    invoke-direct {p0, v3}, LX/2Sb;-><init>(LX/2Sc;)V

    .line 412720
    move-object v0, p0

    .line 412721
    sput-object v0, LX/2Sb;->b:LX/2Sb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412722
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 412723
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 412724
    :cond_1
    sget-object v0, LX/2Sb;->b:LX/2Sb;

    return-object v0

    .line 412725
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 412726
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2Sb;Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;Ljava/lang/String;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;
    .locals 7
    .param p1    # Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;
        .annotation build Lcom/facebook/graphql/calls/RecentSearchesFactoryValue;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 412727
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 412728
    :cond_0
    const/4 v0, 0x0

    .line 412729
    :goto_0
    return-object v0

    .line 412730
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->o()Ljava/lang/String;

    move-result-object v0

    .line 412731
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    .line 412732
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 412733
    const v2, -0x1bce060e

    if-eq v1, v2, :cond_2

    const v2, 0x361ab677

    if-ne v1, v2, :cond_e

    :cond_2
    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 412734
    if-nez v2, :cond_3

    .line 412735
    new-instance v0, LX/7C4;

    sget-object v2, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Missing id for recent search of type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 412736
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->p()Ljava/lang/String;

    move-result-object v2

    .line 412737
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 412738
    iget-object v3, p0, LX/2Sb;->a:LX/2Sc;

    sget-object v4, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    const-string v5, "Missing name for recent search"

    invoke-virtual {v3, v4, v5}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 412739
    :cond_4
    new-instance v3, LX/CwR;

    invoke-direct {v3}, LX/CwR;-><init>()V

    .line 412740
    iput-object v0, v3, LX/CwR;->b:Ljava/lang/String;

    .line 412741
    move-object v0, v3

    .line 412742
    iput-object v2, v0, LX/CwR;->a:Ljava/lang/String;

    .line 412743
    move-object v0, v0

    .line 412744
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 412745
    iput-object v2, v0, LX/CwR;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 412746
    move-object v2, v0

    .line 412747
    const v0, 0x30654a2e

    if-ne v1, v0, :cond_b

    .line 412748
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 412749
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing path for shortcut: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 412750
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 412751
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing fallback_path for shortcut: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 412752
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 412753
    iput-object v0, v2, LX/CwR;->f:Landroid/net/Uri;

    .line 412754
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 412755
    iput-object v0, v2, LX/CwR;->g:Landroid/net/Uri;

    .line 412756
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 412757
    iput-object v0, v2, LX/CwR;->e:Landroid/net/Uri;

    .line 412758
    :cond_7
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->u()I

    move-result v0

    .line 412759
    iput v0, v2, LX/CwR;->i:I

    .line 412760
    const/4 v0, 0x1

    .line 412761
    iput-boolean v0, v2, LX/CwR;->d:Z

    .line 412762
    const-string v0, "video_search"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 412763
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 412764
    iput-object v0, v2, LX/CwR;->k:LX/0Px;

    .line 412765
    :cond_8
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->s()Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;->LOCAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    if-ne v0, v1, :cond_d

    .line 412766
    sget-object v0, LX/CwF;->local:LX/CwF;

    .line 412767
    iput-object v0, v2, LX/CwR;->h:LX/CwF;

    .line 412768
    :cond_9
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 412769
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 412770
    iput-object v0, v2, LX/CwR;->l:Ljava/lang/String;

    .line 412771
    :cond_a
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->t()Ljava/lang/String;

    move-result-object v0

    .line 412772
    iput-object v0, v2, LX/CwR;->m:Ljava/lang/String;

    .line 412773
    invoke-virtual {v2}, LX/CwR;->j()Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    goto/16 :goto_0

    .line 412774
    :cond_b
    const v0, 0x41e065f

    if-ne v1, v0, :cond_c

    .line 412775
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v1, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 412776
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 412777
    iput-object v0, v2, LX/CwR;->e:Landroid/net/Uri;

    .line 412778
    goto :goto_2

    .line 412779
    :cond_c
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 412780
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 412781
    iput-object v0, v2, LX/CwR;->e:Landroid/net/Uri;

    .line 412782
    goto/16 :goto_2

    .line 412783
    :cond_d
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->s()Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;->LOCAL_CATEGORY:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    if-ne v0, v1, :cond_9

    .line 412784
    sget-object v0, LX/CwF;->local_category:LX/CwF;

    .line 412785
    iput-object v0, v2, LX/CwR;->h:LX/CwF;

    .line 412786
    goto :goto_3

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public static a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;
    .locals 14

    .prologue
    const v7, 0x579fce4e

    const/16 v6, 0x400

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 412787
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 412788
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    .line 412789
    new-instance v0, LX/9zk;

    invoke-direct {v0}, LX/9zk;-><init>()V

    .line 412790
    iget-object v2, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    move-object v2, v2

    .line 412791
    iput-object v2, v0, LX/9zk;->g:Ljava/lang/String;

    .line 412792
    move-object v0, v0

    .line 412793
    iget-object v2, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v2, v2

    .line 412794
    iput-object v2, v0, LX/9zk;->h:Ljava/lang/String;

    .line 412795
    move-object v0, v0

    .line 412796
    iget-object v2, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v2, v2

    .line 412797
    iput-object v2, v0, LX/9zk;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 412798
    move-object v0, v0

    .line 412799
    iget v2, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->i:I

    move v2, v2

    .line 412800
    iput v2, v0, LX/9zk;->m:I

    .line 412801
    move-object v0, v0

    .line 412802
    iget-object v2, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->m:Ljava/lang/String;

    move-object v2, v2

    .line 412803
    iput-object v2, v0, LX/9zk;->l:Ljava/lang/String;

    .line 412804
    move-object v2, v0

    .line 412805
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->h:LX/CwF;

    move-object v0, v0

    .line 412806
    sget-object v3, LX/CwF;->local:LX/CwF;

    if-ne v0, v3, :cond_0

    .line 412807
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;->LOCAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    .line 412808
    iput-object v0, v2, LX/9zk;->k:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    .line 412809
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->h:LX/CwF;

    move-object v0, v0

    .line 412810
    sget-object v3, LX/CwF;->local_category:LX/CwF;

    if-ne v0, v3, :cond_1

    .line 412811
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;->LOCAL_CATEGORY:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    .line 412812
    iput-object v0, v2, LX/9zk;->k:Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    .line 412813
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 412814
    if-nez v0, :cond_5

    const/4 v0, 0x0

    .line 412815
    :goto_0
    if-eqz v0, :cond_3

    .line 412816
    const v3, 0x30654a2e

    if-ne v1, v3, :cond_6

    .line 412817
    iput-object v0, v2, LX/9zk;->f:Ljava/lang/String;

    .line 412818
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->f:Landroid/net/Uri;

    move-object v0, v0

    .line 412819
    if-eqz v0, :cond_2

    .line 412820
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->f:Landroid/net/Uri;

    move-object v0, v0

    .line 412821
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 412822
    iput-object v0, v2, LX/9zk;->i:Ljava/lang/String;

    .line 412823
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->g:Landroid/net/Uri;

    move-object v0, v0

    .line 412824
    if-eqz v0, :cond_3

    .line 412825
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->g:Landroid/net/Uri;

    move-object v0, v0

    .line 412826
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 412827
    iput-object v0, v2, LX/9zk;->c:Ljava/lang/String;

    .line 412828
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->l:Ljava/lang/String;

    move-object v0, v0

    .line 412829
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 412830
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->l:Ljava/lang/String;

    move-object v0, v0

    .line 412831
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 412832
    iput-object v0, v2, LX/9zk;->b:LX/0Px;

    .line 412833
    :cond_4
    new-instance v0, LX/9zj;

    invoke-direct {v0}, LX/9zj;-><init>()V

    invoke-virtual {v2}, LX/9zk;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 412834
    iput-object v1, v0, LX/9zj;->a:Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    .line 412835
    move-object v0, v0

    .line 412836
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 412837
    new-instance v8, LX/186;

    const/16 v9, 0x80

    invoke-direct {v8, v9}, LX/186;-><init>(I)V

    .line 412838
    iget-object v9, v0, LX/9zj;->a:Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    invoke-static {v8, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 412839
    invoke-virtual {v8, v12}, LX/186;->c(I)V

    .line 412840
    invoke-virtual {v8, v11, v9}, LX/186;->b(II)V

    .line 412841
    invoke-virtual {v8}, LX/186;->d()I

    move-result v9

    .line 412842
    invoke-virtual {v8, v9}, LX/186;->d(I)V

    .line 412843
    invoke-virtual {v8}, LX/186;->e()[B

    move-result-object v8

    invoke-static {v8}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 412844
    invoke-virtual {v9, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 412845
    new-instance v8, LX/15i;

    move-object v11, v10

    move-object v13, v10

    invoke-direct/range {v8 .. v13}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 412846
    new-instance v9, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;

    invoke-direct {v9, v8}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;-><init>(LX/15i;)V

    .line 412847
    move-object v0, v9

    .line 412848
    return-object v0

    .line 412849
    :cond_5
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 412850
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 412851
    :cond_6
    const v3, 0x41e065f

    if-ne v1, v3, :cond_7

    .line 412852
    new-instance v1, LX/186;

    invoke-direct {v1, v6}, LX/186;-><init>(I)V

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v5}, LX/186;->c(I)V

    invoke-virtual {v1, v4, v0}, LX/186;->b(II)V

    invoke-static {v1, v7}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 412853
    new-instance v3, LX/186;

    invoke-direct {v3, v6}, LX/186;-><init>(I)V

    invoke-static {v1, v0, v7, v3}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    invoke-virtual {v3, v5}, LX/186;->c(I)V

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    const v0, 0x3a37a3b5

    invoke-static {v3, v0}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 412854
    invoke-virtual {v2, v1, v0}, LX/9zk;->a(LX/15i;I)LX/9zk;

    goto/16 :goto_1

    .line 412855
    :cond_7
    new-instance v1, LX/4aM;

    invoke-direct {v1}, LX/4aM;-><init>()V

    .line 412856
    iput-object v0, v1, LX/4aM;->b:Ljava/lang/String;

    .line 412857
    move-object v0, v1

    .line 412858
    invoke-virtual {v0}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 412859
    iput-object v0, v2, LX/9zk;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 412860
    goto/16 :goto_1
.end method
