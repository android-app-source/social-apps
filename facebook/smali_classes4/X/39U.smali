.class public LX/39U;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public final a:LX/2y3;

.field public final b:LX/1V4;

.field public final c:LX/2sO;

.field public final d:LX/BzX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BzX",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/C0M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C0M",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final f:LX/Bzg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bzg",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final g:LX/C05;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C05",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final h:LX/C2y;

.field public final i:LX/1WM;

.field public final j:LX/35i;

.field public final k:LX/Bzl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bzl",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/BzX;LX/C0M;LX/Bzg;LX/C05;LX/C2y;LX/2sO;LX/1V4;LX/1WM;LX/35i;LX/Bzl;LX/2y3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 523066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523067
    iput-object p1, p0, LX/39U;->d:LX/BzX;

    .line 523068
    iput-object p2, p0, LX/39U;->e:LX/C0M;

    .line 523069
    iput-object p3, p0, LX/39U;->f:LX/Bzg;

    .line 523070
    iput-object p4, p0, LX/39U;->g:LX/C05;

    .line 523071
    iput-object p5, p0, LX/39U;->h:LX/C2y;

    .line 523072
    iput-object p6, p0, LX/39U;->c:LX/2sO;

    .line 523073
    iput-object p7, p0, LX/39U;->b:LX/1V4;

    .line 523074
    iput-object p8, p0, LX/39U;->i:LX/1WM;

    .line 523075
    iput-object p9, p0, LX/39U;->j:LX/35i;

    .line 523076
    iput-object p10, p0, LX/39U;->k:LX/Bzl;

    .line 523077
    iput-object p11, p0, LX/39U;->a:LX/2y3;

    .line 523078
    return-void
.end method

.method public static a(LX/0QB;)LX/39U;
    .locals 15

    .prologue
    .line 523079
    const-class v1, LX/39U;

    monitor-enter v1

    .line 523080
    :try_start_0
    sget-object v0, LX/39U;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 523081
    sput-object v2, LX/39U;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523082
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523083
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 523084
    new-instance v3, LX/39U;

    invoke-static {v0}, LX/BzX;->a(LX/0QB;)LX/BzX;

    move-result-object v4

    check-cast v4, LX/BzX;

    invoke-static {v0}, LX/C0M;->a(LX/0QB;)LX/C0M;

    move-result-object v5

    check-cast v5, LX/C0M;

    invoke-static {v0}, LX/Bzg;->a(LX/0QB;)LX/Bzg;

    move-result-object v6

    check-cast v6, LX/Bzg;

    invoke-static {v0}, LX/C05;->a(LX/0QB;)LX/C05;

    move-result-object v7

    check-cast v7, LX/C05;

    invoke-static {v0}, LX/C2y;->a(LX/0QB;)LX/C2y;

    move-result-object v8

    check-cast v8, LX/C2y;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v9

    check-cast v9, LX/2sO;

    invoke-static {v0}, LX/1V4;->a(LX/0QB;)LX/1V4;

    move-result-object v10

    check-cast v10, LX/1V4;

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v11

    check-cast v11, LX/1WM;

    invoke-static {v0}, LX/35i;->a(LX/0QB;)LX/35i;

    move-result-object v12

    check-cast v12, LX/35i;

    invoke-static {v0}, LX/Bzl;->a(LX/0QB;)LX/Bzl;

    move-result-object v13

    check-cast v13, LX/Bzl;

    invoke-static {v0}, LX/2y3;->a(LX/0QB;)LX/2y3;

    move-result-object v14

    check-cast v14, LX/2y3;

    invoke-direct/range {v3 .. v14}, LX/39U;-><init>(LX/BzX;LX/C0M;LX/Bzg;LX/C05;LX/C2y;LX/2sO;LX/1V4;LX/1WM;LX/35i;LX/Bzl;LX/2y3;)V

    .line 523085
    move-object v0, v3

    .line 523086
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 523087
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/39U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523088
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 523089
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
