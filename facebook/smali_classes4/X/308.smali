.class public LX/308;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1se;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1se;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 484079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484080
    iput-object p1, p0, LX/308;->a:LX/1se;

    .line 484081
    iput-object p2, p0, LX/308;->b:Ljava/lang/String;

    .line 484082
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 484084
    if-ne p0, p1, :cond_1

    .line 484085
    :cond_0
    :goto_0
    return v0

    .line 484086
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 484087
    goto :goto_0

    .line 484088
    :cond_3
    check-cast p1, LX/308;

    .line 484089
    iget-object v2, p0, LX/308;->b:Ljava/lang/String;

    iget-object v3, p1, LX/308;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/308;->a:LX/1se;

    iget-object v3, p1, LX/308;->a:LX/1se;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 484090
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/308;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/308;->a:LX/1se;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 484083
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "topic"

    iget-object v2, p0, LX/308;->a:LX/1se;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "category"

    iget-object v2, p0, LX/308;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
