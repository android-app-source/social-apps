.class public final enum LX/2ei;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2ei;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2ei;

.field public static final enum SOCIAL_CONTEXT:LX/2ei;

.field public static final enum SOCIAL_CONTEXT_SUTRO:LX/2ei;

.field public static final enum STORY_HEADER:LX/2ei;

.field public static final enum STORY_HEADER_SUTRO:LX/2ei;

.field public static final enum SUGGESTED_CONTENT:LX/2ei;

.field public static final enum SUGGESTED_CONTENT_SUTRO:LX/2ei;


# instance fields
.field private mColor:I

.field private final mColorResource:I

.field private mColorSet:Z

.field private mFontSize:I

.field private final mFontSizeResource:I

.field private mFontSizeSet:Z

.field private mFontStyle:I


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 445449
    new-instance v0, LX/2ei;

    const-string v1, "SUGGESTED_CONTENT"

    const v3, 0x7f0a015d

    const v5, 0x7f0b0050

    invoke-direct/range {v0 .. v5}, LX/2ei;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    .line 445450
    new-instance v5, LX/2ei;

    const-string v6, "SOCIAL_CONTEXT"

    const v8, 0x7f0a010c

    const v10, 0x7f0b004f

    move v7, v4

    move v9, v2

    invoke-direct/range {v5 .. v10}, LX/2ei;-><init>(Ljava/lang/String;IIII)V

    sput-object v5, LX/2ei;->SOCIAL_CONTEXT:LX/2ei;

    .line 445451
    new-instance v5, LX/2ei;

    const-string v6, "STORY_HEADER"

    const v8, 0x7f0a010c

    const v10, 0x7f0b0050

    move v7, v11

    move v9, v2

    invoke-direct/range {v5 .. v10}, LX/2ei;-><init>(Ljava/lang/String;IIII)V

    sput-object v5, LX/2ei;->STORY_HEADER:LX/2ei;

    .line 445452
    new-instance v5, LX/2ei;

    const-string v6, "SUGGESTED_CONTENT_SUTRO"

    const v8, 0x7f0a00aa

    const v10, 0x7f0b0050

    move v7, v12

    move v9, v4

    invoke-direct/range {v5 .. v10}, LX/2ei;-><init>(Ljava/lang/String;IIII)V

    sput-object v5, LX/2ei;->SUGGESTED_CONTENT_SUTRO:LX/2ei;

    .line 445453
    new-instance v5, LX/2ei;

    const-string v6, "SOCIAL_CONTEXT_SUTRO"

    const v8, 0x7f0a00aa

    const v10, 0x7f0b004f

    move v7, v13

    move v9, v2

    invoke-direct/range {v5 .. v10}, LX/2ei;-><init>(Ljava/lang/String;IIII)V

    sput-object v5, LX/2ei;->SOCIAL_CONTEXT_SUTRO:LX/2ei;

    .line 445454
    new-instance v5, LX/2ei;

    const-string v6, "STORY_HEADER_SUTRO"

    const/4 v7, 0x5

    const v8, 0x7f0a00aa

    const v10, 0x7f0b0050

    move v9, v2

    invoke-direct/range {v5 .. v10}, LX/2ei;-><init>(Ljava/lang/String;IIII)V

    sput-object v5, LX/2ei;->STORY_HEADER_SUTRO:LX/2ei;

    .line 445455
    const/4 v0, 0x6

    new-array v0, v0, [LX/2ei;

    sget-object v1, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    aput-object v1, v0, v2

    sget-object v1, LX/2ei;->SOCIAL_CONTEXT:LX/2ei;

    aput-object v1, v0, v4

    sget-object v1, LX/2ei;->STORY_HEADER:LX/2ei;

    aput-object v1, v0, v11

    sget-object v1, LX/2ei;->SUGGESTED_CONTENT_SUTRO:LX/2ei;

    aput-object v1, v0, v12

    sget-object v1, LX/2ei;->SOCIAL_CONTEXT_SUTRO:LX/2ei;

    aput-object v1, v0, v13

    const/4 v1, 0x5

    sget-object v2, LX/2ei;->STORY_HEADER_SUTRO:LX/2ei;

    aput-object v2, v0, v1

    sput-object v0, LX/2ei;->$VALUES:[LX/2ei;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 445442
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 445443
    iput-boolean v0, p0, LX/2ei;->mColorSet:Z

    .line 445444
    iput-boolean v0, p0, LX/2ei;->mFontSizeSet:Z

    .line 445445
    iput p3, p0, LX/2ei;->mColorResource:I

    .line 445446
    iput p4, p0, LX/2ei;->mFontStyle:I

    .line 445447
    iput p5, p0, LX/2ei;->mFontSizeResource:I

    .line 445448
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2ei;
    .locals 1

    .prologue
    .line 445441
    const-class v0, LX/2ei;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2ei;

    return-object v0
.end method

.method public static values()[LX/2ei;
    .locals 1

    .prologue
    .line 445433
    sget-object v0, LX/2ei;->$VALUES:[LX/2ei;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2ei;

    return-object v0
.end method


# virtual methods
.method public final getColor(Landroid/content/res/Resources;)I
    .locals 1

    .prologue
    .line 445456
    iget-boolean v0, p0, LX/2ei;->mColorSet:Z

    if-nez v0, :cond_0

    .line 445457
    iget v0, p0, LX/2ei;->mColorResource:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/2ei;->mColor:I

    .line 445458
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2ei;->mColorSet:Z

    .line 445459
    :cond_0
    iget v0, p0, LX/2ei;->mColor:I

    return v0
.end method

.method public final getColorResource()I
    .locals 1

    .prologue
    .line 445440
    iget v0, p0, LX/2ei;->mColorResource:I

    return v0
.end method

.method public final getFontSize(Landroid/content/res/Resources;)I
    .locals 1

    .prologue
    .line 445436
    iget-boolean v0, p0, LX/2ei;->mFontSizeSet:Z

    if-nez v0, :cond_0

    .line 445437
    iget v0, p0, LX/2ei;->mFontSizeResource:I

    invoke-static {p1, v0}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    iput v0, p0, LX/2ei;->mFontSize:I

    .line 445438
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2ei;->mFontSizeSet:Z

    .line 445439
    :cond_0
    iget v0, p0, LX/2ei;->mFontSize:I

    return v0
.end method

.method public final getFontSizeResource()I
    .locals 1

    .prologue
    .line 445435
    iget v0, p0, LX/2ei;->mFontSizeResource:I

    return v0
.end method

.method public final getFontStyle()I
    .locals 1

    .prologue
    .line 445434
    iget v0, p0, LX/2ei;->mFontStyle:I

    return v0
.end method
