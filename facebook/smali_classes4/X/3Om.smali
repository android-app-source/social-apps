.class public LX/3Om;
.super LX/2TZ;
.source ""

# interfaces
.implements LX/3On;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2TZ",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;",
        "LX/3On;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 560749
    const-class v0, LX/3Om;

    sput-object v0, LX/3Om;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 560750
    invoke-direct {p0, p1}, LX/2TZ;-><init>(Landroid/database/Cursor;)V

    .line 560751
    return-void
.end method

.method private static b(Landroid/database/Cursor;)Lcom/facebook/user/model/User;
    .locals 26
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 560752
    :try_start_0
    const-string v2, "fbid"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 560753
    const-string v2, "first_name"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 560754
    const-string v4, "last_name"

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 560755
    const-string v5, "display_name"

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 560756
    const-string v6, "small_picture_url"

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 560757
    const-string v7, "big_picture_url"

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 560758
    const-string v8, "huge_picture_url"

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 560759
    const-string v9, "small_picture_size"

    move-object/from16 v0, p0

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 560760
    const-string v10, "big_picture_size"

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 560761
    const-string v11, "huge_picture_size"

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 560762
    const-string v12, "communication_rank"

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 560763
    const-string v13, "is_mobile_pushable"

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 560764
    const-string v14, "is_messenger_user"

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 560765
    const-string v15, "messenger_install_time_ms"

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 560766
    const-string v16, "added_time_ms"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 560767
    const-string v17, "link_type"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 560768
    const-string v18, "bday_month"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 560769
    const-string v19, "bday_day"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 560770
    const-string v20, "is_partial"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 560771
    const-string v21, "messenger_invite_priority"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 560772
    const-string v22, "viewer_connection_status"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 560773
    const-string v23, "phonebook_section_key"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 560774
    new-instance v24, Lcom/facebook/user/model/Name;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-direct {v0, v2, v4, v5}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 560775
    move-object/from16 v0, p0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 560776
    move-object/from16 v0, p0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 560777
    move-object/from16 v0, p0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 560778
    const/4 v2, 0x0

    .line 560779
    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    if-eqz v25, :cond_0

    .line 560780
    new-instance v2, Lcom/facebook/user/model/PicSquare;

    new-instance v4, Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-object/from16 v0, p0

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v5, v9}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    new-instance v5, Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v9, v7}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    new-instance v7, Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v9, v8}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-direct {v2, v4, v5, v7}, Lcom/facebook/user/model/PicSquare;-><init>(Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;)V

    .line 560781
    :cond_0
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, LX/3Oq;->getFromDbValue(I)LX/3Oq;

    move-result-object v4

    .line 560782
    sget-object v5, LX/3Oq;->FRIEND:LX/3Oq;

    invoke-virtual {v4, v5}, LX/3Oq;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 560783
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 560784
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 560785
    new-instance v8, LX/0XI;

    invoke-direct {v8}, LX/0XI;-><init>()V

    sget-object v9, LX/0XG;->FACEBOOK:LX/0XG;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v9, v3}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, LX/0XI;->b(Lcom/facebook/user/model/Name;)LX/0XI;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/0XI;->f(Ljava/lang/String;)LX/0XI;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0XI;->a(Lcom/facebook/user/model/PicSquare;)LX/0XI;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-virtual {v2, v3}, LX/0XI;->a(F)LX/0XI;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0XI;->a(LX/03R;)LX/0XI;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/0XI;->c(Z)LX/0XI;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, LX/0XI;->a(J)LX/0XI;

    move-result-object v2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, LX/0XI;->b(J)LX/0XI;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0XI;->h(Z)LX/0XI;

    move-result-object v2

    invoke-virtual {v2, v5, v7}, LX/0XI;->a(II)LX/0XI;

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v4, v2

    const-wide/16 v6, 0x1

    cmp-long v2, v4, v6

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v3, v2}, LX/0XI;->m(Z)LX/0XI;

    move-result-object v2

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-virtual {v2, v3}, LX/0XI;->b(F)LX/0XI;

    move-result-object v2

    .line 560786
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 560787
    invoke-static {v3}, LX/0XK;->fromDbValue(Ljava/lang/String;)LX/0XK;

    move-result-object v3

    .line 560788
    invoke-virtual {v2, v3}, LX/0XI;->a(LX/0XK;)LX/0XI;

    .line 560789
    if-ltz v23, :cond_1

    .line 560790
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0XI;->i(Ljava/lang/String;)LX/0XI;

    .line 560791
    :cond_1
    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 560792
    :goto_1
    return-object v2

    .line 560793
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 560794
    :catch_0
    move-exception v2

    .line 560795
    sget-object v3, LX/3Om;->b:Ljava/lang/Class;

    const-string v4, "Exception deserializing user from contact"

    invoke-static {v3, v4, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 560796
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 560797
    invoke-static {p1}, LX/3Om;->b(Landroid/database/Cursor;)Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0
.end method
