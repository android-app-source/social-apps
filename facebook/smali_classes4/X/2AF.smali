.class public abstract LX/2AF;
.super LX/2AG;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/2AG",
        "<TK;TV;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/0Sh;

.field public volatile b:Z

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0o5;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Comparator;LX/0Sh;)V
    .locals 1
    .param p1    # Ljava/util/Comparator;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<TV;>;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .prologue
    .line 377090
    invoke-direct {p0, p1}, LX/2AG;-><init>(Ljava/util/Comparator;)V

    .line 377091
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2AF;->b:Z

    .line 377092
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/2AF;->c:Ljava/util/Set;

    .line 377093
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iput-object v0, p0, LX/2AF;->a:LX/0Sh;

    .line 377094
    return-void
.end method

.method private declared-synchronized g()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0o5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377089
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2AF;->c:Ljava/util/Set;

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/0o5;)V
    .locals 1

    .prologue
    .line 377070
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 377071
    iget-object v0, p0, LX/2AF;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377072
    monitor-exit p0

    return-void

    .line 377073
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/0o5;)V
    .locals 1

    .prologue
    .line 377082
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 377083
    iget-object v0, p0, LX/2AF;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 377084
    iget-object v0, p0, LX/2AF;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377085
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 377086
    invoke-virtual {p0}, LX/2AG;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377087
    :cond_0
    monitor-exit p0

    return-void

    .line 377088
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 377078
    invoke-super {p0}, LX/2AG;->e()V

    .line 377079
    iget-boolean v0, p0, LX/2AF;->b:Z

    if-eqz v0, :cond_0

    .line 377080
    invoke-virtual {p0}, LX/2AF;->f()V

    .line 377081
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 377074
    invoke-virtual {p0}, LX/2AG;->b()Ljava/util/Collection;

    move-result-object v0

    .line 377075
    invoke-direct {p0}, LX/2AF;->g()Ljava/util/Set;

    move-result-object v1

    .line 377076
    iget-object v2, p0, LX/2AF;->a:LX/0Sh;

    new-instance v3, Lcom/facebook/notifications/cache/BaseListenableCache$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/facebook/notifications/cache/BaseListenableCache$1;-><init>(LX/2AF;Ljava/lang/Iterable;Ljava/util/Collection;)V

    invoke-virtual {v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 377077
    return-void
.end method
