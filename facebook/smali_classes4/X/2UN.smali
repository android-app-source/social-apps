.class public final LX/2UN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2It;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(LX/2It;LX/03V;)V
    .locals 0

    .prologue
    .line 415822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415823
    iput-object p1, p0, LX/2UN;->a:LX/2It;

    .line 415824
    iput-object p2, p0, LX/2UN;->b:LX/03V;

    .line 415825
    return-void
.end method

.method public static a$redex0(LX/2UN;LX/3OK;J)J
    .locals 4

    .prologue
    .line 415826
    :try_start_0
    iget-object v0, p0, LX/2UN;->a:LX/2It;

    invoke-virtual {v0, p1, p2, p3}, LX/2Iu;->a(LX/0To;J)J
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    .line 415827
    :catch_0
    move-exception v0

    .line 415828
    iget-object v1, p0, LX/2UN;->b:LX/03V;

    const-string v2, "AddressBookPeriodicRunner.DBPropertyUtilWithValueRetrievalErrorHandling"

    const-string v3, "Failed to get value in getValueForKeyAsLong"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 415829
    new-instance v1, LX/6Lm;

    invoke-direct {v1, v0}, LX/6Lm;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a$redex0(LX/2UN;LX/3OK;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 415830
    :try_start_0
    iget-object v0, p0, LX/2UN;->a:LX/2It;

    invoke-virtual {v0, p1, p2}, LX/2Iu;->a(LX/0To;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 415831
    :catch_0
    move-exception v0

    .line 415832
    iget-object v1, p0, LX/2UN;->b:LX/03V;

    const-string v2, "AddressBookPeriodicRunner.DBPropertyUtilWithValueRetrievalErrorHandling"

    const-string v3, "Failed to get value in getValueForKey"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 415833
    new-instance v1, LX/6Lm;

    invoke-direct {v1, v0}, LX/6Lm;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
