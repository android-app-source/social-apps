.class public final LX/23u;
.super LX/0ur;
.source ""


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLFeedBackendData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation
.end field

.field public G:J

.field public H:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Z

.field public K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLHotConversationInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Lcom/facebook/graphql/model/GraphQLIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Lcom/facebook/graphql/model/GraphQLStoryInsights;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Z

.field public S:Z

.field public T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Z

.field public V:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Z

.field public X:Z

.field public Y:Z

.field public Z:Z

.field public aA:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;",
            ">;"
        }
    .end annotation
.end field

.field public aB:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation
.end field

.field public aC:I

.field public aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aN:Lcom/facebook/graphql/model/GraphQLPostTranslatability;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:Lcom/facebook/graphql/model/GraphQLTranslation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;",
            ">;"
        }
    .end annotation
.end field

.field public aS:Z

.field public aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:I

.field public ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;",
            ">;"
        }
    .end annotation
.end field

.field public ag:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public ah:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/graphql/model/GraphQLBoostedComponent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:Lcom/facebook/graphql/model/GraphQLSticker;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aq:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

.field public at:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public ay:Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:J

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphAction;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLBackdatedTime;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:J

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

.field public z:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 366100
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 366101
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    iput-object v0, p0, LX/23u;->y:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 366102
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, LX/23u;->as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 366103
    const/4 v0, 0x0

    iput-object v0, p0, LX/23u;->aU:LX/0x2;

    .line 366104
    instance-of v0, p0, LX/23u;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 366105
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;
    .locals 4

    .prologue
    .line 365998
    new-instance v1, LX/23u;

    invoke-direct {v1}, LX/23u;-><init>()V

    .line 365999
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 366000
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->b:LX/0Px;

    .line 366001
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->C()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->c:LX/0Px;

    .line 366002
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->d:LX/0Px;

    .line 366003
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    iput-object v0, v1, LX/23u;->e:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 366004
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->F()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->f:LX/0Px;

    .line 366005
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    iput-object v0, v1, LX/23u;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 366006
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    iput-object v0, v1, LX/23u;->h:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 366007
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->I()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->i:LX/0Px;

    .line 366008
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, v1, LX/23u;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 366009
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->k:LX/0Px;

    .line 366010
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->L()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v0

    iput-object v0, v1, LX/23u;->l:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    .line 366011
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/23u;->m:Ljava/lang/String;

    .line 366012
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->M()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->n:Z

    .line 366013
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->N()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->o:Z

    .line 366014
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->p:Z

    .line 366015
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->P()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->q:Z

    .line 366016
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Q()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->r:Z

    .line 366017
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->R()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->s:Z

    .line 366018
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/23u;->t:Ljava/lang/String;

    .line 366019
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->T()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, v1, LX/23u;->u:Lcom/facebook/graphql/model/GraphQLStory;

    .line 366020
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    iput-wide v2, v1, LX/23u;->v:J

    .line 366021
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/23u;->w:Ljava/lang/String;

    .line 366022
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/23u;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 366023
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bl()Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    move-result-object v0

    iput-object v0, v1, LX/23u;->y:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 366024
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->W()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v0

    iput-object v0, v1, LX/23u;->z:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 366025
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    iput-object v0, v1, LX/23u;->A:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 366026
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bk()Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    move-result-object v0

    iput-object v0, v1, LX/23u;->B:Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    .line 366027
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Y()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v0

    iput-object v0, v1, LX/23u;->C:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    .line 366028
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, v1, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 366029
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    iput-object v0, v1, LX/23u;->E:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 366030
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aa()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->F:LX/0Px;

    .line 366031
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->F_()J

    move-result-wide v2

    iput-wide v2, v1, LX/23u;->G:J

    .line 366032
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ab()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v0

    iput-object v0, v1, LX/23u;->H:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    .line 366033
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ac()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, v1, LX/23u;->I:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 366034
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ad()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->J:Z

    .line 366035
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->C_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/23u;->K:Ljava/lang/String;

    .line 366036
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ae()Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    move-result-object v0

    iput-object v0, v1, LX/23u;->L:Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    .line 366037
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v0

    iput-object v0, v1, LX/23u;->M:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 366038
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/23u;->N:Ljava/lang/String;

    .line 366039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    iput-object v0, v1, LX/23u;->O:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 366040
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v0

    iput-object v0, v1, LX/23u;->P:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 366041
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aj()Lcom/facebook/graphql/model/GraphQLStoryInsights;

    move-result-object v0

    iput-object v0, v1, LX/23u;->Q:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    .line 366042
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bf()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->R:Z

    .line 366043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ak()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->S:Z

    .line 366044
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/23u;->T:Ljava/lang/String;

    .line 366045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bj()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->U:Z

    .line 366046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bm()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    iput-object v0, v1, LX/23u;->V:Lcom/facebook/graphql/model/FeedUnit;

    .line 366047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->am()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->W:Z

    .line 366048
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->an()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->X:Z

    .line 366049
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ao()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->Y:Z

    .line 366050
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ap()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->Z:Z

    .line 366051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aq()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aa:Ljava/lang/String;

    .line 366052
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ar()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ab:Ljava/lang/String;

    .line 366053
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->as()I

    move-result v0

    iput v0, v1, LX/23u;->ac:I

    .line 366054
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->at()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 366055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->au()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ae:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 366056
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bg()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->af:LX/0Px;

    .line 366057
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->av()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ag:LX/0Px;

    .line 366058
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ah:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 366059
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aw()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ai:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 366060
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ax()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aj:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 366061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ak:Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 366062
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    iput-object v0, v1, LX/23u;->al:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 366063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aA()Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    move-result-object v0

    iput-object v0, v1, LX/23u;->am:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    .line 366064
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aB()Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    move-result-object v0

    iput-object v0, v1, LX/23u;->an:Lcom/facebook/graphql/model/GraphQLStoryPromptCompositionsConnection;

    .line 366065
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bh()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ao:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 366066
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aC()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ap:Lcom/facebook/graphql/model/GraphQLSticker;

    .line 366067
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aq:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 366068
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aE()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ar:LX/0Px;

    .line 366069
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    iput-object v0, v1, LX/23u;->as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 366070
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    iput-object v0, v1, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 366071
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/23u;->au:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 366072
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aI()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/23u;->av:Ljava/lang/String;

    .line 366073
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aw:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 366074
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aK()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ax:LX/0Px;

    .line 366075
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    iput-object v0, v1, LX/23u;->ay:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 366076
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aM()J

    move-result-wide v2

    iput-wide v2, v1, LX/23u;->az:J

    .line 366077
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aN()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aA:LX/0Px;

    .line 366078
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aO()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aB:LX/0Px;

    .line 366079
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v0

    iput v0, v1, LX/23u;->aC:I

    .line 366080
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aQ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 366081
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aR()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 366082
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aS()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aF:Lcom/facebook/graphql/model/GraphQLStory;

    .line 366083
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aG:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 366084
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 366085
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 366086
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aJ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 366087
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aK:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 366088
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aX()Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aL:Lcom/facebook/graphql/model/GraphQLStoryTopicsContext;

    .line 366089
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aM:Ljava/lang/String;

    .line 366090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aN:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 366091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aZ()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aO:Lcom/facebook/graphql/model/GraphQLTranslation;

    .line 366092
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ba()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aP:Ljava/lang/String;

    .line 366093
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bb()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aQ:Lcom/facebook/graphql/model/GraphQLActor;

    .line 366094
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bc()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aR:LX/0Px;

    .line 366095
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bd()Z

    move-result v0

    iput-boolean v0, v1, LX/23u;->aS:Z

    .line 366096
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    iput-object v0, v1, LX/23u;->aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 366097
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 366098
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/23u;->aU:LX/0x2;

    .line 366099
    return-object v1
.end method


# virtual methods
.method public final a(J)LX/23u;
    .locals 1

    .prologue
    .line 365996
    iput-wide p1, p0, LX/23u;->v:J

    .line 365997
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLEntity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365994
    iput-object p1, p0, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 365995
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365992
    iput-object p1, p0, LX/23u;->P:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 365993
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPlace;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLPlace;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365990
    iput-object p1, p0, LX/23u;->A:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 365991
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLPrivacyScope;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365988
    iput-object p1, p0, LX/23u;->al:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 365989
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLProfile;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365986
    iput-object p1, p0, LX/23u;->aK:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 365987
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLSticker;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLSticker;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365984
    iput-object p1, p0, LX/23u;->ap:Lcom/facebook/graphql/model/GraphQLSticker;

    .line 365985
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365982
    iput-object p1, p0, LX/23u;->aG:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 365983
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLWithTagsConnection;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365980
    iput-object p1, p0, LX/23u;->aT:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 365981
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 2

    .prologue
    .line 365978
    new-instance v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLStory;-><init>(LX/23u;)V

    .line 365979
    return-object v0
.end method

.method public final b(J)LX/23u;
    .locals 1

    .prologue
    .line 366106
    iput-wide p1, p0, LX/23u;->G:J

    .line 366107
    return-object p0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLPlace;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLPlace;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365954
    iput-object p1, p0, LX/23u;->O:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 365955
    return-object p0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365958
    iput-object p1, p0, LX/23u;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 365959
    return-object p0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365960
    iput-object p1, p0, LX/23u;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 365961
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/23u;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365956
    iput-object p1, p0, LX/23u;->t:Ljava/lang/String;

    .line 365957
    return-object p0
.end method

.method public final b(Z)LX/23u;
    .locals 0

    .prologue
    .line 365962
    iput-boolean p1, p0, LX/23u;->o:Z

    .line 365963
    return-object p0
.end method

.method public final c(LX/0Px;)LX/23u;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;)",
            "LX/23u;"
        }
    .end annotation

    .prologue
    .line 365964
    iput-object p1, p0, LX/23u;->d:LX/0Px;

    .line 365965
    return-object p0
.end method

.method public final c(Z)LX/23u;
    .locals 0

    .prologue
    .line 365966
    iput-boolean p1, p0, LX/23u;->p:Z

    .line 365967
    return-object p0
.end method

.method public final e(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365968
    iput-object p1, p0, LX/23u;->aD:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 365969
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/23u;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365970
    iput-object p1, p0, LX/23u;->N:Ljava/lang/String;

    .line 365971
    return-object p0
.end method

.method public final f(LX/0Px;)LX/23u;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/23u;"
        }
    .end annotation

    .prologue
    .line 365972
    iput-object p1, p0, LX/23u;->k:LX/0Px;

    .line 365973
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/23u;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365974
    iput-object p1, p0, LX/23u;->T:Ljava/lang/String;

    .line 365975
    return-object p0
.end method

.method public final g(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/23u;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365976
    iput-object p1, p0, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 365977
    return-object p0
.end method
