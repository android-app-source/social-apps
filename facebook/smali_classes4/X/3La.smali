.class public final LX/3La;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LX/3LZ;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>(Ljava/util/EnumSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "LX/3LZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 550824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550825
    const/4 v0, -0x1

    iput v0, p0, LX/3La;->b:I

    .line 550826
    iput-object p1, p0, LX/3La;->a:Ljava/util/EnumSet;

    .line 550827
    return-void
.end method


# virtual methods
.method public final A()I
    .locals 1

    .prologue
    .line 550828
    iget v0, p0, LX/3La;->b:I

    return v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 550819
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->FAVORITE_FRIENDS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 550829
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->TOP_FRIENDS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 550830
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->ONLINE_FRIENDS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 550831
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->ONLINE_FRIENDS_COEFFICIENT_SORTED:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 550832
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->FRIENDS_ON_MESSENGER:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 550833
    if-ne p0, p1, :cond_1

    .line 550834
    :cond_0
    :goto_0
    return v0

    .line 550835
    :cond_1
    instance-of v2, p1, LX/3La;

    if-nez v2, :cond_2

    move v0, v1

    .line 550836
    goto :goto_0

    .line 550837
    :cond_2
    check-cast p1, LX/3La;

    .line 550838
    iget-object v2, p0, LX/3La;->a:Ljava/util/EnumSet;

    iget-object v3, p1, LX/3La;->a:Ljava/util/EnumSet;

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, LX/3La;->b:I

    iget v3, p1, LX/3La;->b:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    .line 550839
    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 550840
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->TOP_FRIENDS_ON_MESSENGER:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 550841
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->NOT_ON_MESSENGER_FRIENDS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 550842
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->PHAT_CONTACTS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 550843
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 550844
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->RECENT_CALLS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 550845
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->RTC_CALLLOGS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 550822
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->RTC_ONGOING_GROUP_CALLS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 550823
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->RTC_VOICEMAILS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 550806
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->TOP_PUSHABLE_FRIENDS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 550807
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->TOP_CONTACTS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 550808
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->ALL_FRIENDS_COEFFICIENT_SORTED:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 550809
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->ALL_FRIENDS_NAME_SORTED:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 550810
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->SMS_INVITE_ALL_PHONE_CONTACTS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 550811
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->SMS_INVITE_MOBILE_CONTACTS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 550812
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->TOP_PHONE_CONTACTS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 550813
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->TOP_PHONE_CONTACTS_NULL_STATE:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 550814
    const-class v0, LX/3La;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "listsToLoad"

    iget-object v2, p0, LX/3La;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "maxContacts"

    iget v2, p0, LX/3La;->b:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 2

    .prologue
    .line 550815
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->PHONE_CONTACTS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 550816
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->ALL_CONTACTS_WITH_CAP:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 550817
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->PSTN_CALL_LOG_FRIENDS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 550818
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->ALL_CONTACTS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final y()Z
    .locals 2

    .prologue
    .line 550820
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->PROMOTIONAL_CONTACTS:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final z()Z
    .locals 2

    .prologue
    .line 550821
    iget-object v0, p0, LX/3La;->a:Ljava/util/EnumSet;

    sget-object v1, LX/3LZ;->PAGES:LX/3LZ;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
