.class public final LX/34k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "LX/2ta;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;)V
    .locals 0

    .prologue
    .line 495654
    iput-object p1, p0, LX/34k;->a:Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 495612
    check-cast p1, Ljava/util/List;

    const/4 v4, 0x1

    .line 495613
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 495614
    :cond_0
    const/4 v0, 0x0

    .line 495615
    :goto_0
    return-object v0

    .line 495616
    :cond_1
    new-instance v1, LX/2ta;

    invoke-direct {v1}, LX/2ta;-><init>()V

    .line 495617
    iget-object v0, p0, LX/34k;->a:Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    iget-object v2, v0, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->d:LX/1S8;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    .line 495618
    if-nez v0, :cond_3

    .line 495619
    const/4 v5, 0x0

    .line 495620
    :goto_1
    move-object v0, v5

    .line 495621
    iput-object v0, v1, LX/2ta;->a:LX/3Aj;

    .line 495622
    new-instance v0, LX/3Ak;

    invoke-direct {v0}, LX/3Ak;-><init>()V

    iput-object v0, v1, LX/2ta;->b:LX/3Ak;

    .line 495623
    iget-object v0, v1, LX/2ta;->b:LX/3Ak;

    iget-object v2, p0, LX/34k;->a:Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    .line 495624
    iget-object v5, v2, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->c:Landroid/content/Context;

    iget-object v6, v2, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->b:LX/0SG;

    invoke-static {v5, v6}, LX/2TU;->a(Landroid/content/Context;LX/0SG;)Lcom/facebook/wifiscan/WifiScanResult;

    move-result-object v6

    .line 495625
    if-nez v6, :cond_7

    .line 495626
    const/4 v5, 0x0

    .line 495627
    :goto_2
    move-object v2, v5

    .line 495628
    const-string v3, "connected_wifi"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 495629
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 495630
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 495631
    iget-object v3, v1, LX/2ta;->b:LX/3Ak;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    new-instance v4, LX/DCd;

    invoke-direct {v4, p0, v2}, LX/DCd;-><init>(LX/34k;Ljava/lang/Long;)V

    invoke-static {v0, v4}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    .line 495632
    const-string v2, "ambient_wifis"

    invoke-virtual {v3, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 495633
    :cond_2
    move-object v0, v1

    .line 495634
    goto :goto_0

    .line 495635
    :cond_3
    new-instance v5, LX/3Aj;

    invoke-direct {v5}, LX/3Aj;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    move-result-object v6

    .line 495636
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v5

    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 495637
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v5

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    .line 495638
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->f()LX/0am;

    move-result-object v5

    .line 495639
    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 495640
    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/3Aj;->d(Ljava/lang/Double;)LX/3Aj;

    .line 495641
    :cond_5
    iget-object v5, v2, LX/1S8;->a:LX/0yD;

    invoke-virtual {v5, v0}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v7

    .line 495642
    const-wide/16 v9, 0x0

    cmp-long v5, v7, v9

    if-ltz v5, :cond_6

    .line 495643
    invoke-static {v7, v8}, LX/1lQ;->n(J)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/3Aj;->e(Ljava/lang/Double;)LX/3Aj;

    :cond_6
    move-object v5, v6

    .line 495644
    goto/16 :goto_1

    .line 495645
    :cond_7
    new-instance v5, LX/4Gr;

    invoke-direct {v5}, LX/4Gr;-><init>()V

    .line 495646
    iget-object v7, v6, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    if-eqz v7, :cond_8

    .line 495647
    iget-object v7, v6, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    invoke-virtual {v5, v7}, LX/4Gr;->a(Ljava/lang/String;)LX/4Gr;

    .line 495648
    :cond_8
    iget-object v7, v6, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    if-eqz v7, :cond_9

    .line 495649
    iget-object v7, v6, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    invoke-virtual {v5, v7}, LX/4Gr;->b(Ljava/lang/String;)LX/4Gr;

    .line 495650
    :cond_9
    iget-object v7, v6, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    if-eqz v7, :cond_a

    .line 495651
    iget-object v7, v6, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    invoke-virtual {v5, v7}, LX/4Gr;->b(Ljava/lang/Integer;)LX/4Gr;

    .line 495652
    :cond_a
    iget v6, v6, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/4Gr;->a(Ljava/lang/Integer;)LX/4Gr;

    .line 495653
    const-wide/16 v7, 0x0

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/4Gr;->a(Ljava/lang/Double;)LX/4Gr;

    goto/16 :goto_2
.end method
