.class public abstract LX/2wH;
.super LX/2wI;
.source ""

# interfaces
.implements LX/2wJ;
.implements LX/2wL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/os/IInterface;",
        ">",
        "LX/2wI",
        "<TT;>;",
        "LX/2wJ;",
        "LX/2wL;"
    }
.end annotation


# instance fields
.field public final d:LX/2wA;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/accounts/Account;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;ILX/2wA;LX/1qf;LX/1qg;)V
    .locals 9

    invoke-static {p1}, LX/2wM;->a(Landroid/content/Context;)LX/2wM;

    move-result-object v3

    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v4, v0

    invoke-static {p5}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1qf;

    invoke-static {p6}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1qg;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, LX/2wH;-><init>(Landroid/content/Context;Landroid/os/Looper;LX/2wM;LX/1vX;ILX/2wA;LX/1qf;LX/1qg;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/2wM;LX/1vX;ILX/2wA;LX/1qf;LX/1qg;)V
    .locals 9

    invoke-static/range {p7 .. p7}, LX/2wH;->a(LX/1qf;)LX/2wP;

    move-result-object v6

    invoke-static/range {p8 .. p8}, LX/2wH;->a(LX/1qg;)LX/2wR;

    move-result-object v7

    invoke-virtual {p6}, LX/2wA;->h()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v8}, LX/2wI;-><init>(Landroid/content/Context;Landroid/os/Looper;LX/2wM;LX/1od;ILX/2wP;LX/2wR;Ljava/lang/String;)V

    iput-object p6, p0, LX/2wH;->d:LX/2wA;

    invoke-virtual {p6}, LX/2wA;->b()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, LX/2wH;->f:Landroid/accounts/Account;

    invoke-virtual {p6}, LX/2wA;->e()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/2wH;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/2wH;->e:Ljava/util/Set;

    return-void
.end method

.method private static a(LX/1qf;)LX/2wP;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2wO;

    invoke-direct {v0, p0}, LX/2wO;-><init>(LX/1qf;)V

    goto :goto_0
.end method

.method private static a(LX/1qg;)LX/2wR;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2wQ;

    invoke-direct {v0, p0}, LX/2wQ;-><init>(LX/1qg;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/Set;)Ljava/util/Set;
    .locals 3
    .param p0    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;"
        }
    .end annotation

    move-object v1, p0

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Scope;

    invoke-interface {p0, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expanding scopes is not permitted, use implied scopes instead"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v1
.end method


# virtual methods
.method public final i()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, LX/2wH;->f:Landroid/accounts/Account;

    return-object v0
.end method

.method public final r()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/common/api/Scope;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, LX/2wH;->e:Ljava/util/Set;

    return-object v0
.end method
