.class public LX/3NJ;
.super LX/3Ml;
.source ""


# instance fields
.field private final c:LX/3NK;


# direct methods
.method public constructor <init>(LX/0Zr;LX/3NK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558992
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 558993
    iput-object p2, p0, LX/3NJ;->c:LX/3NK;

    .line 558994
    return-void
.end method

.method public static a(LX/0QB;)LX/3NJ;
    .locals 7

    .prologue
    .line 558995
    new-instance v2, LX/3NJ;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v0

    check-cast v0, LX/0Zr;

    .line 558996
    new-instance v6, LX/3NK;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/3MV;->a(LX/0QB;)LX/3MV;

    move-result-object v4

    check-cast v4, LX/3MV;

    invoke-static {p0}, LX/3MW;->b(LX/0QB;)LX/3MW;

    move-result-object v5

    check-cast v5, LX/3MW;

    invoke-direct {v6, v1, v3, v4, v5}, LX/3NK;-><init>(LX/0Sh;LX/0tX;LX/3MV;LX/3MW;)V

    .line 558997
    move-object v1, v6

    .line 558998
    check-cast v1, LX/3NK;

    invoke-direct {v2, v0, v1}, LX/3NJ;-><init>(LX/0Zr;LX/3NK;)V

    .line 558999
    move-object v0, v2

    .line 559000
    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 5
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 559001
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 559002
    :goto_0
    new-instance v1, LX/39y;

    invoke-direct {v1}, LX/39y;-><init>()V

    .line 559003
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 559004
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    .line 559005
    const/4 v0, -0x1

    iput v0, v1, LX/39y;->b:I

    move-object v0, v1

    .line 559006
    :goto_1
    return-object v0

    .line 559007
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 559008
    :cond_1
    :try_start_0
    iget-object v2, p0, LX/3NJ;->c:LX/3NK;

    const/4 v3, 0x6

    invoke-virtual {v2, v0, v3}, LX/3NK;->a(Ljava/lang/String;I)LX/0Px;

    move-result-object v0

    .line 559009
    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 559010
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 559011
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 559012
    iget-object v4, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v4, v0}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 559013
    :catch_0
    move-exception v0

    .line 559014
    const-string v2, "ContactPickerVcEndpointPageFilter"

    const-string v3, "exception with filtering VC endpoint pages"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 559015
    const/4 v0, 0x0

    iput v0, v1, LX/39y;->b:I

    .line 559016
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 559017
    goto :goto_1

    .line 559018
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 559019
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    iput v2, v1, LX/39y;->b:I

    .line 559020
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 559021
    goto :goto_1
.end method
