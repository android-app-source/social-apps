.class public LX/2Tm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;
.implements Lcom/facebook/webrtc/IWebrtcCallMonitorInterface;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile z:LX/2Tm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0Vw;

.field private final d:LX/03V;

.field public final e:LX/0So;

.field private final f:LX/1MZ;

.field public final g:LX/2Hu;

.field private final h:LX/0a8;

.field private final i:LX/00H;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Sh;

.field private final l:LX/1sj;

.field private final m:LX/0Uh;

.field private n:Lcom/facebook/webrtc/IWebrtcUiInterface;

.field private o:Lcom/facebook/webrtc/ConferenceCall$Listener;

.field private p:LX/EDx;

.field private q:Lcom/facebook/webrtc/IWebrtcConfigInterface;

.field private r:Lcom/facebook/webrtc/IWebrtcLoggingInterface;

.field private s:Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;

.field private t:LX/0aB;

.field public volatile u:J

.field public volatile v:Lcom/facebook/webrtc/WebrtcEngine;

.field private w:LX/1rD;

.field private x:J

.field public y:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 414733
    const-class v0, LX/2Tm;

    sput-object v0, LX/2Tm;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Vw;LX/03V;LX/1MZ;LX/2Hu;LX/0So;LX/0Sh;LX/1sj;LX/1rD;LX/00H;LX/0a8;LX/0Or;LX/0Uh;)V
    .locals 2
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/mqtt/capabilities/MqttEndpointCapability;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Vw;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1MZ;",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClientManager;",
            "LX/0So;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/1sj;",
            "LX/1rD;",
            "LX/00H;",
            "Lcom/facebook/gk/store/GatekeeperListeners;",
            "LX/0Or",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414717
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/2Tm;->x:J

    .line 414718
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2Tm;->y:Z

    .line 414719
    iput-object p1, p0, LX/2Tm;->b:Landroid/content/Context;

    .line 414720
    iput-object p2, p0, LX/2Tm;->c:LX/0Vw;

    .line 414721
    iput-object p3, p0, LX/2Tm;->d:LX/03V;

    .line 414722
    iput-object p5, p0, LX/2Tm;->g:LX/2Hu;

    .line 414723
    iput-object p4, p0, LX/2Tm;->f:LX/1MZ;

    .line 414724
    iput-object p6, p0, LX/2Tm;->e:LX/0So;

    .line 414725
    iput-object p7, p0, LX/2Tm;->k:LX/0Sh;

    .line 414726
    iput-object p9, p0, LX/2Tm;->w:LX/1rD;

    .line 414727
    iput-object p8, p0, LX/2Tm;->l:LX/1sj;

    .line 414728
    iput-object p11, p0, LX/2Tm;->h:LX/0a8;

    .line 414729
    iput-object p12, p0, LX/2Tm;->j:LX/0Or;

    .line 414730
    iput-object p10, p0, LX/2Tm;->i:LX/00H;

    .line 414731
    iput-object p13, p0, LX/2Tm;->m:LX/0Uh;

    .line 414732
    return-void
.end method

.method public static a(LX/0QB;)LX/2Tm;
    .locals 3

    .prologue
    .line 414706
    sget-object v0, LX/2Tm;->z:LX/2Tm;

    if-nez v0, :cond_1

    .line 414707
    const-class v1, LX/2Tm;

    monitor-enter v1

    .line 414708
    :try_start_0
    sget-object v0, LX/2Tm;->z:LX/2Tm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 414709
    if-eqz v2, :cond_0

    .line 414710
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/2Tm;->b(LX/0QB;)LX/2Tm;

    move-result-object v0

    sput-object v0, LX/2Tm;->z:LX/2Tm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414711
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 414712
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 414713
    :cond_1
    sget-object v0, LX/2Tm;->z:LX/2Tm;

    return-object v0

    .line 414714
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 414715
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 414701
    iget-object v0, p0, LX/2Tm;->f:LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414702
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 414703
    :goto_0
    return-void

    .line 414704
    :cond_0
    new-instance v0, LX/7TT;

    invoke-direct {v0, p0, p1}, LX/7TT;-><init>(LX/2Tm;Ljava/lang/Runnable;)V

    .line 414705
    iget-object v1, p0, LX/2Tm;->k:LX/0Sh;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/2Tm;
    .locals 14

    .prologue
    .line 414699
    new-instance v0, LX/2Tm;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0Vw;->a(LX/0QB;)LX/0Vw;

    move-result-object v2

    check-cast v2, LX/0Vw;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v4

    check-cast v4, LX/1MZ;

    invoke-static {p0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v5

    check-cast v5, LX/2Hu;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {p0}, LX/1sj;->a(LX/0QB;)LX/1sj;

    move-result-object v8

    check-cast v8, LX/1sj;

    invoke-static {p0}, LX/1rD;->a(LX/0QB;)LX/1rD;

    move-result-object v9

    check-cast v9, LX/1rD;

    const-class v10, LX/00H;

    invoke-interface {p0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/00H;

    invoke-static {p0}, LX/0Zn;->a(LX/0QB;)LX/0a8;

    move-result-object v11

    check-cast v11, LX/0a8;

    const/16 v12, 0x15dd

    invoke-static {p0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-direct/range {v0 .. v13}, LX/2Tm;-><init>(Landroid/content/Context;LX/0Vw;LX/03V;LX/1MZ;LX/2Hu;LX/0So;LX/0Sh;LX/1sj;LX/1rD;LX/00H;LX/0a8;LX/0Or;LX/0Uh;)V

    .line 414700
    return-object v0
.end method

.method private g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 414686
    iget-object v1, p0, LX/2Tm;->i:LX/00H;

    .line 414687
    iget-object v2, v1, LX/00H;->j:LX/01T;

    move-object v1, v2

    .line 414688
    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/2Tm;->i:LX/00H;

    .line 414689
    iget-object v2, v1, LX/00H;->j:LX/01T;

    move-object v1, v2

    .line 414690
    sget-object v2, LX/01T;->PHONE:LX/01T;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/2Tm;->i:LX/00H;

    .line 414691
    iget-object v2, v1, LX/00H;->j:LX/01T;

    move-object v1, v2

    .line 414692
    sget-object v2, LX/01T;->ALOHA:LX/01T;

    if-ne v1, v2, :cond_2

    .line 414693
    :cond_0
    iget-object v1, p0, LX/2Tm;->m:LX/0Uh;

    const/16 v2, 0x56c

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 414694
    :cond_1
    :goto_0
    return v0

    .line 414695
    :cond_2
    iget-object v1, p0, LX/2Tm;->i:LX/00H;

    .line 414696
    iget-object v2, v1, LX/00H;->j:LX/01T;

    move-object v1, v2

    .line 414697
    sget-object v2, LX/01T;->FB4A:LX/01T;

    if-ne v1, v2, :cond_1

    .line 414698
    iget-object v1, p0, LX/2Tm;->m:LX/0Uh;

    const/16 v2, 0xeb

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 19

    .prologue
    .line 414683
    invoke-virtual/range {p0 .. p0}, LX/2Tm;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 414684
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    const-string v7, "instant_video"

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x1

    const/4 v15, 0x0

    move-wide/from16 v4, p1

    move-object/from16 v6, p6

    move-object/from16 v8, p7

    move-wide/from16 v12, p3

    move-object/from16 v14, p5

    move-object/from16 v16, p8

    move-object/from16 v17, p9

    invoke-virtual/range {v3 .. v17}, Lcom/facebook/webrtc/WebrtcEngine;->startCustomCallToDevice(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZJLjava/lang/String;ZLjava/lang/String;[B)V

    .line 414685
    :cond_0
    return-void
.end method

.method public final a(JLandroid/view/View;)V
    .locals 1

    .prologue
    .line 414680
    invoke-virtual {p0}, LX/2Tm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414681
    iget-object v0, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/webrtc/WebrtcEngine;->setRendererWindow(JLandroid/view/View;)V

    .line 414682
    :cond_0
    return-void
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;[B)V
    .locals 13

    .prologue
    .line 414677
    invoke-virtual {p0}, LX/2Tm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414678
    new-instance v0, Lcom/facebook/webrtc/WebrtcManager$2;

    move-object v1, p0

    move-wide v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/facebook/webrtc/WebrtcManager$2;-><init>(LX/2Tm;JLjava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;[B)V

    invoke-direct {p0, v0}, LX/2Tm;->a(Ljava/lang/Runnable;)V

    .line 414679
    :cond_0
    return-void
.end method

.method public final a(LX/7TP;)V
    .locals 1

    .prologue
    .line 414674
    invoke-virtual {p0}, LX/2Tm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414675
    iget-object v0, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v0, p1}, Lcom/facebook/webrtc/WebrtcEngine;->a(LX/7TP;)V

    .line 414676
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/webrtc/IWebrtcUiInterface;Lcom/facebook/webrtc/ConferenceCall$Listener;LX/EDx;Lcom/facebook/webrtc/IWebrtcConfigInterface;Lcom/facebook/webrtc/IWebrtcLoggingInterface;Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;)V
    .locals 8

    .prologue
    .line 414734
    iput-object p1, p0, LX/2Tm;->n:Lcom/facebook/webrtc/IWebrtcUiInterface;

    .line 414735
    iput-object p2, p0, LX/2Tm;->o:Lcom/facebook/webrtc/ConferenceCall$Listener;

    .line 414736
    iput-object p3, p0, LX/2Tm;->p:LX/EDx;

    .line 414737
    iput-object p4, p0, LX/2Tm;->q:Lcom/facebook/webrtc/IWebrtcConfigInterface;

    .line 414738
    iput-object p5, p0, LX/2Tm;->r:Lcom/facebook/webrtc/IWebrtcLoggingInterface;

    .line 414739
    iput-object p6, p0, LX/2Tm;->s:Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;

    .line 414740
    iget-object v0, p0, LX/2Tm;->n:Lcom/facebook/webrtc/IWebrtcUiInterface;

    invoke-interface {v0, p0}, Lcom/facebook/webrtc/IWebrtcUiInterface;->setWebrtcManager(LX/2Tm;)V

    .line 414741
    iget-object v0, p0, LX/2Tm;->s:Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;

    invoke-interface {v0, p0}, Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;->setWebrtcManager(LX/2Tm;)V

    .line 414742
    invoke-direct {p0}, LX/2Tm;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/2Tm;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 414743
    :cond_0
    :goto_0
    return-void

    .line 414744
    :cond_1
    monitor-enter p0

    .line 414745
    :try_start_0
    iget-object v0, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    if-nez v0, :cond_3

    .line 414746
    new-instance v0, Lcom/facebook/webrtc/WebrtcEngine;

    iget-object v1, p0, LX/2Tm;->b:Landroid/content/Context;

    iget-object v2, p0, LX/2Tm;->s:Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;

    iget-object v3, p0, LX/2Tm;->n:Lcom/facebook/webrtc/IWebrtcUiInterface;

    iget-object v4, p0, LX/2Tm;->q:Lcom/facebook/webrtc/IWebrtcConfigInterface;

    iget-object v5, p0, LX/2Tm;->r:Lcom/facebook/webrtc/IWebrtcLoggingInterface;

    iget-object v7, p0, LX/2Tm;->o:Lcom/facebook/webrtc/ConferenceCall$Listener;

    move-object v6, p0

    invoke-direct/range {v0 .. v7}, Lcom/facebook/webrtc/WebrtcEngine;-><init>(Landroid/content/Context;Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;Lcom/facebook/webrtc/IWebrtcUiInterface;Lcom/facebook/webrtc/IWebrtcConfigInterface;Lcom/facebook/webrtc/IWebrtcLoggingInterface;Lcom/facebook/webrtc/IWebrtcCallMonitorInterface;Lcom/facebook/webrtc/ConferenceCall$Listener;)V

    iput-object v0, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    .line 414747
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "instant_video"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "pstn_upsell"

    aput-object v2, v0, v1

    .line 414748
    invoke-virtual {p0}, LX/2Tm;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 414749
    iget-object v1, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v1, v0}, Lcom/facebook/webrtc/WebrtcEngine;->setSupportedCallTypes([Ljava/lang/String;)V

    .line 414750
    :cond_2
    iget-object v0, p0, LX/2Tm;->q:Lcom/facebook/webrtc/IWebrtcConfigInterface;

    iget-object v1, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-interface {v0, v1}, Lcom/facebook/webrtc/IWebrtcConfigInterface;->setWebrtcEngine(Lcom/facebook/webrtc/WebrtcEngine;)V

    .line 414751
    :cond_3
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;JJLjava/lang/String;ILjava/lang/String;)V
    .locals 10

    .prologue
    .line 414655
    invoke-virtual {p0}, LX/2Tm;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 414656
    :goto_0
    return-void

    .line 414657
    :cond_0
    sget-object v1, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    .line 414658
    invoke-static {v1}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v0

    .line 414659
    if-eqz p1, :cond_1

    .line 414660
    iget-object v0, p0, LX/2Tm;->l:LX/1sj;

    invoke-virtual {v0, p1}, LX/1sj;->b(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v1

    .line 414661
    invoke-static {v1}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v0

    .line 414662
    const-string v2, "op"

    const-string v3, "webrtc_response"

    invoke-interface {v0, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414663
    const-string v2, "service"

    const-string v3, "sender_webrtc_application_layer"

    invoke-interface {v0, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414664
    const-string v2, "msg_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414665
    const-string v2, "call_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414666
    :cond_1
    if-nez p7, :cond_2

    .line 414667
    const-string v2, "success"

    const-string v3, "true"

    invoke-interface {v0, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414668
    iget-object v2, p0, LX/2Tm;->l:LX/1sj;

    sget-object v3, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    invoke-virtual {v2, v1, v3, v0}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 414669
    iget-object v0, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/facebook/webrtc/WebrtcEngine;->onMessageSendSuccess(JJ)V

    goto :goto_0

    .line 414670
    :cond_2
    const-string v2, "success"

    const-string v3, "false"

    invoke-interface {v0, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414671
    const-string v2, "error_code"

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414672
    iget-object v2, p0, LX/2Tm;->l:LX/1sj;

    sget-object v3, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    invoke-virtual {v2, v1, v3, v0}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 414673
    iget-object v1, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    move-wide v2, p2

    move-wide v4, p4

    move/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p6

    invoke-virtual/range {v1 .. v8}, Lcom/facebook/webrtc/WebrtcEngine;->onMessageSendError(JJILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZZZ)V
    .locals 1

    .prologue
    .line 414652
    invoke-virtual {p0}, LX/2Tm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414653
    iget-object v0, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/webrtc/WebrtcEngine;->acceptCall(Ljava/lang/String;ZZZ)V

    .line 414654
    :cond_0
    return-void
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 414651
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/2Tm;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 414621
    monitor-enter p0

    .line 414622
    :try_start_0
    iget-object v1, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    if-eqz v1, :cond_0

    .line 414623
    iget-object v0, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    .line 414624
    const/4 v1, 0x0

    iput-object v1, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    .line 414625
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414626
    if-eqz v0, :cond_1

    .line 414627
    invoke-virtual {v0}, Lcom/facebook/webrtc/WebrtcEngine;->a()V

    .line 414628
    :cond_1
    iget-object v0, p0, LX/2Tm;->p:LX/EDx;

    if-eqz v0, :cond_2

    .line 414629
    iget-object v0, p0, LX/2Tm;->p:LX/EDx;

    const-wide/16 v6, 0x0

    const/4 v10, 0x0

    const/4 v8, 0x0

    .line 414630
    sget-object v2, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    invoke-virtual {v2}, LX/7TQ;->ordinal()I

    move-result v4

    const-string v5, ""

    const-string v9, ""

    move-object v3, v0

    invoke-virtual/range {v3 .. v9}, LX/EDx;->hideCallUI(ILjava/lang/String;JZLjava/lang/String;)V

    .line 414631
    iput-wide v6, v0, LX/EDx;->ac:J

    .line 414632
    invoke-static {v0, v10}, LX/EDx;->a(LX/EDx;Lcom/facebook/webrtc/ConferenceCall;)V

    .line 414633
    iput-object v10, v0, LX/EDx;->ae:Ljava/util/Map;

    .line 414634
    iput-object v10, v0, LX/EDx;->af:LX/EGE;

    .line 414635
    iput-boolean v8, v0, LX/EDx;->ag:Z

    .line 414636
    iput-wide v6, v0, LX/EDx;->ak:J

    .line 414637
    iput-object v10, v0, LX/EDx;->al:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 414638
    iput-boolean v8, v0, LX/EDx;->aB:Z

    .line 414639
    iput-boolean v8, v0, LX/EDx;->aC:Z

    .line 414640
    iput-boolean v8, v0, LX/EDx;->aF:Z

    .line 414641
    iput-boolean v8, v0, LX/EDx;->aH:Z

    .line 414642
    iput-wide v6, v0, LX/EDx;->aI:J

    .line 414643
    iput-wide v6, v0, LX/EDx;->aM:J

    .line 414644
    iput-wide v6, v0, LX/EDx;->aN:J

    .line 414645
    sget-object v2, LX/7TQ;->CallEndIgnoreCall:LX/7TQ;

    iput-object v2, v0, LX/EDx;->bl:LX/7TQ;

    .line 414646
    iput-object v10, v0, LX/EDx;->bo:Ljava/lang/String;

    .line 414647
    iput-object v10, v0, LX/EDx;->bn:Ljava/lang/String;

    .line 414648
    iput-boolean v8, v0, LX/EDx;->aR:Z

    .line 414649
    :cond_2
    return-void

    .line 414650
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 19

    .prologue
    .line 414618
    invoke-virtual/range {p0 .. p0}, LX/2Tm;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 414619
    move-object/from16 v0, p0

    iget-object v3, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    const-string v7, "pstn_upsell"

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v15, 0x1

    move-wide/from16 v4, p1

    move-object/from16 v6, p6

    move-object/from16 v8, p7

    move-wide/from16 v12, p3

    move-object/from16 v14, p5

    move-object/from16 v16, p8

    move-object/from16 v17, p9

    invoke-virtual/range {v3 .. v17}, Lcom/facebook/webrtc/WebrtcEngine;->startCustomCallToDevice(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZJLjava/lang/String;ZLjava/lang/String;[B)V

    .line 414620
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 414615
    invoke-virtual {p0}, LX/2Tm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414616
    iget-object v0, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v0, p1}, Lcom/facebook/webrtc/WebrtcEngine;->setSpeakerOn(Z)V

    .line 414617
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 414612
    invoke-virtual {p0}, LX/2Tm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414613
    iget-object v0, p0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v0, p1}, Lcom/facebook/webrtc/WebrtcEngine;->setBluetoothState(Z)V

    .line 414614
    :cond_0
    return-void
.end method

.method public final init()V
    .locals 3

    .prologue
    .line 414601
    iget-object v0, p0, LX/2Tm;->i:LX/00H;

    .line 414602
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 414603
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2Tm;->i:LX/00H;

    .line 414604
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 414605
    sget-object v1, LX/01T;->PHONE:LX/01T;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2Tm;->i:LX/00H;

    .line 414606
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 414607
    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_1

    .line 414608
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2Tm;->u:J

    .line 414609
    new-instance v0, LX/2Tn;

    invoke-direct {v0, p0}, LX/2Tn;-><init>(LX/2Tm;)V

    iput-object v0, p0, LX/2Tm;->t:LX/0aB;

    .line 414610
    iget-object v0, p0, LX/2Tm;->h:LX/0a8;

    iget-object v1, p0, LX/2Tm;->t:LX/0aB;

    const/16 v2, 0x56c

    invoke-virtual {v0, v1, v2}, LX/0a8;->a(LX/0aB;I)V

    .line 414611
    :cond_1
    return-void
.end method

.method public final onCallEnded(JJJ)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x3e8

    .line 414592
    iget-wide v0, p0, LX/2Tm;->x:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 414593
    iget-object v0, p0, LX/2Tm;->w:LX/1rD;

    invoke-virtual {v0}, LX/1rD;->a()J

    move-result-wide v0

    div-long v4, v0, v6

    .line 414594
    iget-wide v0, p0, LX/2Tm;->x:J

    div-long v2, v0, v6

    .line 414595
    iget-object v0, p0, LX/2Tm;->w:LX/1rD;

    const-string v1, "voip"

    invoke-virtual/range {v0 .. v5}, LX/1rD;->a(Ljava/lang/String;JJ)V

    .line 414596
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/2Tm;->x:J

    .line 414597
    iget-object v0, p0, LX/2Tm;->r:Lcom/facebook/webrtc/IWebrtcLoggingInterface;

    invoke-interface {v0}, Lcom/facebook/webrtc/IWebrtcLoggingInterface;->resumeLogUpload()V

    .line 414598
    iget-object v0, p0, LX/2Tm;->c:LX/0Vw;

    const-string v1, "voip_bytes_sent"

    invoke-virtual {v0, v1, p3, p4}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 414599
    iget-object v0, p0, LX/2Tm;->c:LX/0Vw;

    const-string v1, "voip_bytes_received"

    invoke-virtual {v0, v1, p5, p6}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 414600
    return-void
.end method

.method public final onCallStarted(J)V
    .locals 2

    .prologue
    .line 414587
    iget-object v0, p0, LX/2Tm;->w:LX/1rD;

    invoke-virtual {v0}, LX/1rD;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/2Tm;->x:J

    .line 414588
    iget-object v0, p0, LX/2Tm;->r:Lcom/facebook/webrtc/IWebrtcLoggingInterface;

    invoke-interface {v0}, Lcom/facebook/webrtc/IWebrtcLoggingInterface;->pauseLogUpload()V

    .line 414589
    iget-object v0, p0, LX/2Tm;->r:Lcom/facebook/webrtc/IWebrtcLoggingInterface;

    invoke-interface {v0}, Lcom/facebook/webrtc/IWebrtcLoggingInterface;->logInitialBatteryLevel()V

    .line 414590
    iget-object v0, p0, LX/2Tm;->r:Lcom/facebook/webrtc/IWebrtcLoggingInterface;

    invoke-interface {v0}, Lcom/facebook/webrtc/IWebrtcLoggingInterface;->logScreenResolution()V

    .line 414591
    return-void
.end method

.method public final onInitializingCall(J)V
    .locals 1

    .prologue
    .line 414585
    iget-object v0, p0, LX/2Tm;->r:Lcom/facebook/webrtc/IWebrtcLoggingInterface;

    invoke-interface {v0, p1, p2}, Lcom/facebook/webrtc/IWebrtcLoggingInterface;->setLastCallId(J)V

    .line 414586
    return-void
.end method
