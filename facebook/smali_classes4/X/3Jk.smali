.class public LX/3Jk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final a:[F

.field private final b:[F


# direct methods
.method public constructor <init>(FFFF)V
    .locals 8

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 547956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547957
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 547958
    invoke-virtual {v0, v1, v1}, Landroid/graphics/Path;->moveTo(FF)V

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, v5

    .line 547959
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 547960
    new-instance v1, Landroid/graphics/PathMeasure;

    invoke-direct {v1, v0, v7}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    .line 547961
    invoke-virtual {v1}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v2

    .line 547962
    const v0, 0x3cf5c28f    # 0.03f

    div-float v0, v2, v0

    float-to-int v0, v0

    add-int/lit8 v3, v0, 0x1

    .line 547963
    new-array v0, v3, [F

    iput-object v0, p0, LX/3Jk;->a:[F

    .line 547964
    new-array v0, v3, [F

    iput-object v0, p0, LX/3Jk;->b:[F

    .line 547965
    const/4 v0, 0x2

    new-array v4, v0, [F

    move v0, v7

    .line 547966
    :goto_0
    if-ge v0, v3, :cond_0

    .line 547967
    int-to-float v5, v0

    mul-float/2addr v5, v2

    add-int/lit8 v6, v3, -0x1

    int-to-float v6, v6

    div-float/2addr v5, v6

    .line 547968
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v4, v6}, Landroid/graphics/PathMeasure;->getPosTan(F[F[F)Z

    .line 547969
    iget-object v5, p0, LX/3Jk;->a:[F

    aget v6, v4, v7

    aput v6, v5, v0

    .line 547970
    iget-object v5, p0, LX/3Jk;->b:[F

    const/4 v6, 0x1

    aget v6, v4, v6

    aput v6, v5, v0

    .line 547971
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 547972
    :cond_0
    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .locals 5

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 547973
    cmpg-float v2, p1, v0

    if-gtz v2, :cond_0

    .line 547974
    :goto_0
    return v0

    .line 547975
    :cond_0
    cmpl-float v2, p1, v1

    if-ltz v2, :cond_1

    move v0, v1

    .line 547976
    goto :goto_0

    .line 547977
    :cond_1
    const/4 v2, 0x0

    .line 547978
    iget-object v1, p0, LX/3Jk;->a:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    move v3, v2

    .line 547979
    :goto_1
    sub-int v2, v1, v3

    const/4 v4, 0x1

    if-le v2, v4, :cond_3

    .line 547980
    add-int v2, v3, v1

    div-int/lit8 v2, v2, 0x2

    .line 547981
    iget-object v4, p0, LX/3Jk;->a:[F

    aget v4, v4, v2

    cmpg-float v4, p1, v4

    if-gez v4, :cond_2

    move v1, v2

    .line 547982
    goto :goto_1

    :cond_2
    move v3, v2

    .line 547983
    goto :goto_1

    .line 547984
    :cond_3
    iget-object v2, p0, LX/3Jk;->a:[F

    aget v2, v2, v1

    iget-object v4, p0, LX/3Jk;->a:[F

    aget v4, v4, v3

    sub-float/2addr v2, v4

    .line 547985
    cmpl-float v0, v2, v0

    if-nez v0, :cond_4

    .line 547986
    iget-object v0, p0, LX/3Jk;->b:[F

    aget v0, v0, v3

    goto :goto_0

    .line 547987
    :cond_4
    iget-object v0, p0, LX/3Jk;->a:[F

    aget v0, v0, v3

    sub-float v0, p1, v0

    .line 547988
    div-float/2addr v0, v2

    .line 547989
    iget-object v2, p0, LX/3Jk;->b:[F

    aget v2, v2, v3

    .line 547990
    iget-object v3, p0, LX/3Jk;->b:[F

    aget v1, v3, v1

    .line 547991
    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_0
.end method
