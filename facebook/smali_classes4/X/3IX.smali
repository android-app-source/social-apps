.class public final LX/3IX;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7Lm;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/Video360HeadingPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/Video360HeadingPlugin;)V
    .locals 0

    .prologue
    .line 546482
    iput-object p1, p0, LX/3IX;->a:Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7Lm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 546483
    const-class v0, LX/7Lm;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 546484
    check-cast p1, LX/7Lm;

    .line 546485
    iget-object v0, p1, LX/7Lm;->a:LX/7Ll;

    sget-object v1, LX/7Ll;->GUIDE_ON:LX/7Ll;

    if-ne v0, v1, :cond_1

    .line 546486
    iget-object v0, p0, LX/3IX;->a:Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->b:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d()V

    .line 546487
    :cond_0
    :goto_0
    return-void

    .line 546488
    :cond_1
    iget-object v0, p1, LX/7Lm;->a:LX/7Ll;

    sget-object v1, LX/7Ll;->GUIDE_OFF:LX/7Ll;

    if-ne v0, v1, :cond_0

    .line 546489
    iget-object v0, p0, LX/3IX;->a:Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->b:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e()V

    goto :goto_0
.end method
