.class public LX/3L5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oi;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3QW;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/00H;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3A0;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/presence/PresenceManager;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/mqtt/capability/MqttVoipCapability;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation runtime Lcom/facebook/rtcpresence/annotations/IsVoipBlockedByCountry;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Or;
    .annotation runtime Lcom/facebook/rtcpresence/annotations/IsVoipEnabledForUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation runtime Lcom/facebook/rtc/annotations/IsRtcAudioConferencingEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Or;
    .annotation runtime Lcom/facebook/rtc/annotations/IsRtcVideoConferencingEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 549896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549897
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 549898
    iput-object v0, p0, LX/3L5;->a:LX/0Ot;

    .line 549899
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 549900
    iput-object v0, p0, LX/3L5;->b:LX/0Ot;

    .line 549901
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 549902
    iput-object v0, p0, LX/3L5;->c:LX/0Ot;

    .line 549903
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 549904
    iput-object v0, p0, LX/3L5;->d:LX/0Ot;

    .line 549905
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 549906
    iput-object v0, p0, LX/3L5;->e:LX/0Ot;

    .line 549907
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 549908
    iput-object v0, p0, LX/3L5;->f:LX/0Ot;

    .line 549909
    return-void
.end method

.method public static b(LX/0QB;)LX/3L5;
    .locals 11

    .prologue
    .line 549910
    new-instance v0, LX/3L5;

    invoke-direct {v0}, LX/3L5;-><init>()V

    .line 549911
    const/16 v1, 0x12c7

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xd36

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const-class v3, LX/00H;

    invoke-interface {p0, v3}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x110e

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xfa5

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x100c

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1566

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x1568

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x155f

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x1560

    invoke-static {p0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    .line 549912
    iput-object v1, v0, LX/3L5;->a:LX/0Ot;

    iput-object v2, v0, LX/3L5;->b:LX/0Ot;

    iput-object v3, v0, LX/3L5;->c:LX/0Ot;

    iput-object v4, v0, LX/3L5;->d:LX/0Ot;

    iput-object v5, v0, LX/3L5;->e:LX/0Ot;

    iput-object v6, v0, LX/3L5;->f:LX/0Ot;

    iput-object v7, v0, LX/3L5;->g:LX/0Or;

    iput-object v8, v0, LX/3L5;->h:LX/0Or;

    iput-object v9, v0, LX/3L5;->i:LX/0Or;

    iput-object v10, v0, LX/3L5;->j:LX/0Or;

    .line 549913
    return-object v0
.end method
