.class public LX/37h;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile j:LX/37h;


# instance fields
.field private final b:LX/37e;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/String;

.field public final e:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "LX/384;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "LX/38c;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/37i;

.field public h:LX/384;

.field public i:LX/38T;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 501955
    const-class v0, LX/37h;

    sput-object v0, LX/37h;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/37d;LX/37e;Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 501946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501947
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LX/37h;->e:Ljava/util/Vector;

    .line 501948
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LX/37h;->f:Ljava/util/Vector;

    .line 501949
    iget-object v0, p1, LX/37d;->a:Ljava/lang/String;

    move-object v0, v0

    .line 501950
    iput-object v0, p0, LX/37h;->d:Ljava/lang/String;

    .line 501951
    iput-object p2, p0, LX/37h;->b:LX/37e;

    .line 501952
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/37h;->c:Landroid/content/Context;

    .line 501953
    invoke-direct {p0}, LX/37h;->c()V

    .line 501954
    return-void
.end method

.method public static a(LX/0QB;)LX/37h;
    .locals 6

    .prologue
    .line 501933
    sget-object v0, LX/37h;->j:LX/37h;

    if-nez v0, :cond_1

    .line 501934
    const-class v1, LX/37h;

    monitor-enter v1

    .line 501935
    :try_start_0
    sget-object v0, LX/37h;->j:LX/37h;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 501936
    if-eqz v2, :cond_0

    .line 501937
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 501938
    new-instance p0, LX/37h;

    invoke-static {v0}, LX/37d;->a(LX/0QB;)LX/37d;

    move-result-object v3

    check-cast v3, LX/37d;

    invoke-static {v0}, LX/37e;->a(LX/0QB;)LX/37e;

    move-result-object v4

    check-cast v4, LX/37e;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5}, LX/37h;-><init>(LX/37d;LX/37e;Landroid/content/Context;)V

    .line 501939
    move-object v0, p0

    .line 501940
    sput-object v0, LX/37h;->j:LX/37h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501941
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 501942
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 501943
    :cond_1
    sget-object v0, LX/37h;->j:LX/37h;

    return-object v0

    .line 501944
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 501945
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b$redex0(LX/37h;LX/384;)V
    .locals 2

    .prologue
    .line 501925
    iget-object v0, p1, LX/384;->c:Ljava/lang/String;

    move-object v0, v0

    .line 501926
    invoke-virtual {p0, v0}, LX/37h;->a(Ljava/lang/String;)LX/384;

    move-result-object v0

    .line 501927
    if-eqz v0, :cond_0

    .line 501928
    iget-object v1, p0, LX/37h;->e:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 501929
    :cond_0
    invoke-direct {p0, p1}, LX/37h;->e(LX/384;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 501930
    :goto_0
    return-void

    .line 501931
    :cond_1
    iget-object v0, p0, LX/37h;->e:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 501932
    invoke-static {p0}, LX/37h;->e(LX/37h;)V

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 501890
    iget-object v0, p0, LX/37h;->g:LX/37i;

    if-eqz v0, :cond_0

    .line 501891
    :goto_0
    return-void

    .line 501892
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/37h;->c:Landroid/content/Context;

    invoke-static {v0}, LX/37i;->a(Landroid/content/Context;)LX/37i;

    move-result-object v0

    iput-object v0, p0, LX/37h;->g:LX/37i;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 501893
    new-instance v0, LX/38R;

    invoke-direct {v0}, LX/38R;-><init>()V

    iget-object v1, p0, LX/37h;->d:Ljava/lang/String;

    invoke-static {v1}, LX/38S;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/38R;->a(Ljava/lang/String;)LX/38R;

    move-result-object v0

    invoke-virtual {v0}, LX/38R;->a()LX/38T;

    move-result-object v0

    iput-object v0, p0, LX/37h;->i:LX/38T;

    .line 501894
    iget-object v0, p0, LX/37h;->g:LX/37i;

    iget-object v1, p0, LX/37h;->i:LX/38T;

    new-instance v2, LX/38U;

    invoke-direct {v2, p0}, LX/38U;-><init>(LX/37h;)V

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, LX/37i;->a(LX/38T;LX/38V;I)V

    .line 501895
    goto :goto_0

    .line 501896
    :catch_0
    move-exception v0

    .line 501897
    iget-object v1, p0, LX/37h;->b:LX/37e;

    sget-object v2, LX/7JJ;->CastDevicesManager_TryInitialize:LX/7JJ;

    invoke-virtual {v1, v2, v0}, LX/37e;->a(LX/7JJ;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static d(LX/37h;LX/384;)V
    .locals 3
    .param p0    # LX/37h;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 501920
    iput-object p1, p0, LX/37h;->h:LX/384;

    .line 501921
    iget-object v0, p0, LX/37h;->f:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/37h;->f:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38c;

    .line 501922
    invoke-interface {v0, p1}, LX/38c;->a(LX/384;)V

    .line 501923
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 501924
    :cond_0
    return-void
.end method

.method public static e(LX/37h;)V
    .locals 3

    .prologue
    .line 501916
    iget-object v0, p0, LX/37h;->f:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/37h;->f:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/38c;

    .line 501917
    invoke-interface {v0}, LX/38c;->a()V

    .line 501918
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 501919
    :cond_0
    return-void
.end method

.method private e(LX/384;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 501908
    iget-object v1, p1, LX/384;->p:Landroid/os/Bundle;

    move-object v1, v1

    .line 501909
    invoke-static {v1}, Lcom/google/android/gms/cast/CastDevice;->b(Landroid/os/Bundle;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v1

    .line 501910
    iget-object v2, p0, LX/37h;->i:LX/38T;

    invoke-virtual {p1, v2}, LX/384;->a(LX/38T;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    iget v2, v1, Lcom/google/android/gms/cast/CastDevice;->j:I

    and-int/2addr v2, v0

    if-ne v2, v0, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 501911
    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/android/gms/cast/CastDevice;->c:Ljava/lang/String;

    const-string p0, "__cast_nearby__"

    invoke-virtual {v2, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 501912
    if-nez v1, :cond_1

    .line 501913
    :cond_0
    const/4 v0, 0x0

    .line 501914
    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 501915
    return v0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/384;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 501902
    iget-object v0, p0, LX/37h;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/37h;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/384;

    .line 501903
    iget-object v3, v0, LX/384;->c:Ljava/lang/String;

    move-object v3, v3

    .line 501904
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 501905
    :goto_1
    return-object v0

    .line 501906
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 501907
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/38c;)V
    .locals 1

    .prologue
    .line 501898
    invoke-direct {p0}, LX/37h;->c()V

    .line 501899
    iget-object v0, p0, LX/37h;->f:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 501900
    iget-object v0, p0, LX/37h;->f:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 501901
    return-void
.end method
