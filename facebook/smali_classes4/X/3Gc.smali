.class public final LX/3Gc;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;LX/2oy;)V
    .locals 0

    .prologue
    .line 541361
    iput-object p1, p0, LX/3Gc;->a:Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;

    .line 541362
    invoke-direct {p0, p2}, LX/2oa;-><init>(LX/2oy;)V

    .line 541363
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541360
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 541342
    check-cast p1, LX/2ou;

    .line 541343
    iget-object v0, p0, LX/3Gc;->a:Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;

    iget-object v1, p1, LX/2ou;->b:LX/2qV;

    .line 541344
    invoke-virtual {v1}, LX/2qV;->isPlayingState()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->v:LX/2qV;

    sget-object p0, LX/2qV;->PAUSED:LX/2qV;

    if-ne v2, p0, :cond_2

    sget-object v2, LX/2qV;->SEEKING:LX/2qV;

    if-ne v1, v2, :cond_2

    .line 541345
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->x:Z

    .line 541346
    iget-object v2, v0, LX/2oy;->i:LX/2oj;

    new-instance p0, LX/7Lw;

    sget-object p1, LX/7MP;->AUTO:LX/7MP;

    invoke-direct {p0, p1}, LX/7Lw;-><init>(LX/7MP;)V

    invoke-virtual {v2, p0}, LX/2oj;->a(LX/2ol;)V

    .line 541347
    iget-object v2, v0, LX/2oy;->i:LX/2oj;

    new-instance p0, LX/2pW;

    sget-object p1, LX/2pO;->DEFAULT:LX/2pO;

    invoke-direct {p0, p1}, LX/2pW;-><init>(LX/2pO;)V

    invoke-virtual {v2, p0}, LX/2oj;->a(LX/2ol;)V

    .line 541348
    iget-object v2, v0, LX/2oy;->i:LX/2oj;

    new-instance p0, LX/2pX;

    sget-object p1, LX/2pR;->DEFAULT:LX/2pR;

    invoke-direct {p0, p1}, LX/2pX;-><init>(LX/2pR;)V

    invoke-virtual {v2, p0}, LX/2oj;->a(LX/2ol;)V

    .line 541349
    invoke-static {v0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->w(Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;)V

    .line 541350
    :cond_1
    :goto_0
    iput-object v1, v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->v:LX/2qV;

    .line 541351
    return-void

    .line 541352
    :cond_2
    sget-object v2, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v1, v2, :cond_3

    iget-object v2, v0, LX/3Ga;->a:LX/2pa;

    invoke-virtual {v0, v2}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->b(LX/2pa;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 541353
    invoke-static {v0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->k(Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;)V

    .line 541354
    iget-object v2, v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->r:Landroid/widget/TextView;

    const p0, 0x7f080d5a

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setText(I)V

    .line 541355
    sget-object v2, LX/Btp;->ENDSCREEN:LX/Btp;

    iput-object v2, v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->b:LX/Btp;

    goto :goto_0

    .line 541356
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, LX/2qV;->PAUSED:LX/2qV;

    if-ne v1, v2, :cond_1

    iget-object v2, v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->v:LX/2qV;

    sget-object p0, LX/2qV;->SEEKING:LX/2qV;

    if-eq v2, p0, :cond_1

    iget-object v2, v0, LX/3Ga;->a:LX/2pa;

    invoke-virtual {v0, v2}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->b(LX/2pa;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->w:Z

    if-eqz v2, :cond_1

    .line 541357
    sget-object v2, LX/Btp;->PAUSESCREEN:LX/Btp;

    iput-object v2, v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->b:LX/Btp;

    .line 541358
    invoke-static {v0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->k(Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;)V

    .line 541359
    iget-object v2, v0, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->r:Landroid/widget/TextView;

    const p0, 0x7f080d5b

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
