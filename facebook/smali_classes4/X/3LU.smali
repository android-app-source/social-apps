.class public LX/3LU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/17Y;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:Landroid/support/v4/app/FragmentActivity;

.field public final d:LX/3LV;

.field private final e:LX/3LQ;


# direct methods
.method public constructor <init>(LX/17Y;Lcom/facebook/content/SecureContextHelper;Landroid/support/v4/app/FragmentActivity;LX/3LV;LX/3LQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550645
    iput-object p1, p0, LX/3LU;->a:LX/17Y;

    .line 550646
    iput-object p2, p0, LX/3LU;->b:Lcom/facebook/content/SecureContextHelper;

    .line 550647
    iput-object p3, p0, LX/3LU;->c:Landroid/support/v4/app/FragmentActivity;

    .line 550648
    iput-object p4, p0, LX/3LU;->d:LX/3LV;

    .line 550649
    iput-object p5, p0, LX/3LU;->e:LX/3LQ;

    .line 550650
    return-void
.end method

.method public static a(LX/0QB;)LX/3LU;
    .locals 9

    .prologue
    .line 550616
    new-instance v1, LX/3LU;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v2

    check-cast v2, LX/17Y;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/1No;->b(LX/0QB;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Landroid/support/v4/app/FragmentActivity;

    .line 550617
    new-instance v7, LX/3LV;

    invoke-direct {v7}, LX/3LV;-><init>()V

    .line 550618
    const/16 v5, 0x259

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v5

    check-cast v5, LX/0if;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v6

    check-cast v6, LX/0W9;

    .line 550619
    iput-object v8, v7, LX/3LV;->a:LX/0Ot;

    iput-object v5, v7, LX/3LV;->b:LX/0if;

    iput-object v6, v7, LX/3LV;->c:LX/0W9;

    .line 550620
    move-object v5, v7

    .line 550621
    check-cast v5, LX/3LV;

    invoke-static {p0}, LX/3LQ;->a(LX/0QB;)LX/3LQ;

    move-result-object v6

    check-cast v6, LX/3LQ;

    invoke-direct/range {v1 .. v6}, LX/3LU;-><init>(LX/17Y;Lcom/facebook/content/SecureContextHelper;Landroid/support/v4/app/FragmentActivity;LX/3LV;LX/3LQ;)V

    .line 550622
    move-object v0, v1

    .line 550623
    return-object v0
.end method

.method private static a(LX/3LU;Ljava/lang/String;LX/3OQ;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 550624
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 550625
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 550626
    if-eqz p1, :cond_0

    .line 550627
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550628
    :cond_0
    const/4 v0, 0x0

    .line 550629
    instance-of v2, p2, LX/3OO;

    if-eqz v2, :cond_4

    .line 550630
    check-cast p2, LX/3OO;

    .line 550631
    iget-object v0, p2, LX/3OO;->r:LX/3OI;

    move-object v0, v0

    .line 550632
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 550633
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 550634
    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550635
    :cond_2
    iget-object v0, p0, LX/3LU;->c:Landroid/support/v4/app/FragmentActivity;

    const-class v2, LX/0f2;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f2;

    .line 550636
    if-eqz v0, :cond_5

    .line 550637
    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 550638
    :goto_1
    move-object v0, v0

    .line 550639
    if-eqz v0, :cond_3

    .line 550640
    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550641
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 550642
    :cond_4
    instance-of v2, p2, LX/DAU;

    if-eqz v2, :cond_1

    .line 550643
    const-string v0, "groups"

    goto :goto_0

    :cond_5
    const-string v0, "unknown"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/3OQ;ILjava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 550603
    iget-object v0, p0, LX/3LU;->d:LX/3LV;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 550604
    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    move v3, v3

    .line 550605
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/3LV;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;ZILjava/lang/String;Ljava/lang/Integer;)V

    .line 550606
    invoke-static {p0, p4, p2}, LX/3LU;->a(LX/3LU;Ljava/lang/String;LX/3OQ;)Ljava/lang/String;

    move-result-object v0

    .line 550607
    iget-object v6, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 550608
    iget-object v7, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v7, v7, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v8, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v7, v8, :cond_0

    .line 550609
    sget-object v7, LX/0ax;->ai:Ljava/lang/String;

    iget-wide v8, v6, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 550610
    :goto_0
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 550611
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 550612
    const-string v6, "trigger"

    invoke-virtual {v7, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 550613
    iget-object v6, p0, LX/3LU;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v8, p0, LX/3LU;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-interface {v6, v7, v8}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 550614
    return v2

    .line 550615
    :cond_0
    sget-object v7, LX/0ax;->ao:Ljava/lang/String;

    iget-wide v8, v6, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public final a(Lcom/facebook/user/model/User;ZZLX/3OQ;Ljava/lang/String;I)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 550572
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 550573
    iget-object v0, p0, LX/3LU;->d:LX/3LV;

    invoke-virtual {v0, p1, v2, p6, p5}, LX/3LV;->a(Lcom/facebook/user/model/User;ZILjava/lang/String;)V

    .line 550574
    :cond_0
    if-eqz p3, :cond_1

    if-eqz p1, :cond_1

    .line 550575
    iget-object v0, p0, LX/3LU;->e:LX/3LQ;

    .line 550576
    iget-object v1, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 550577
    new-instance p2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p3, "nearby_friends_dive_bar_contact_tapping"

    invoke-direct {p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p3, "background_location"

    .line 550578
    iput-object p3, p2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 550579
    move-object p2, p2

    .line 550580
    const-string p3, "target_id"

    invoke-virtual {p2, p3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    .line 550581
    iget-object p3, v0, LX/3LQ;->a:LX/0Zb;

    invoke-interface {p3, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 550582
    :cond_1
    invoke-static {p0, p5, p4}, LX/3LU;->a(LX/3LU;Ljava/lang/String;LX/3OQ;)Ljava/lang/String;

    move-result-object v0

    .line 550583
    const/4 v1, 0x0

    .line 550584
    instance-of p2, p4, LX/3OO;

    if-nez p2, :cond_4

    .line 550585
    :cond_2
    :goto_0
    move v1, v1

    .line 550586
    if-eqz v1, :cond_3

    .line 550587
    sget-object v1, LX/0ax;->bE:Ljava/lang/String;

    .line 550588
    iget-object p2, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p2, p2

    .line 550589
    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 550590
    iget-object p2, p0, LX/3LU;->a:LX/17Y;

    iget-object p3, p0, LX/3LU;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-interface {p2, p3, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 550591
    iget-object p2, p0, LX/3LU;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object p3, p0, LX/3LU;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-interface {p2, v1, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 550592
    :goto_1
    return v2

    .line 550593
    :cond_3
    sget-object v1, LX/0ax;->ai:Ljava/lang/String;

    .line 550594
    iget-object p2, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p2, p2

    .line 550595
    invoke-static {v1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 550596
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    .line 550597
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 550598
    const-string v1, "trigger"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 550599
    iget-object v1, p0, LX/3LU;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object p3, p0, LX/3LU;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-interface {v1, p2, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 550600
    :cond_4
    check-cast p4, LX/3OO;

    .line 550601
    iget-object p2, p4, LX/3OO;->r:LX/3OI;

    move-object p2, p2

    .line 550602
    sget-object p3, LX/3OH;->SELF_PROFILE:LX/3OH;

    if-ne p2, p3, :cond_2

    const/4 v1, 0x1

    goto :goto_0
.end method
