.class public final LX/2gW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final synthetic a:LX/2gV;

.field public final b:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(LX/2gV;)V
    .locals 2

    .prologue
    .line 448562
    iput-object p1, p0, LX/2gW;->a:LX/2gV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448563
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/2gW;->b:Ljava/util/concurrent/CountDownLatch;

    .line 448564
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 448565
    iget-object v0, p0, LX/2gW;->a:LX/2gV;

    invoke-static {p1}, LX/1tG;->a(Landroid/os/IBinder;)LX/1tH;

    move-result-object v1

    invoke-static {v0, v1}, LX/2gV;->a$redex0(LX/2gV;LX/1tH;)V

    .line 448566
    return-void
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6

    .prologue
    .line 448567
    iget-object v0, p0, LX/2gW;->a:LX/2gV;

    iget-object v0, v0, LX/2gV;->f:LX/0ZR;

    new-instance v1, LX/2Yu;

    iget-object v2, p0, LX/2gW;->a:LX/2gV;

    iget-object v2, v2, LX/2gV;->e:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    const-string v4, "ServiceConnected (MqttPushServiceClientManager)"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-direct {v1, v2, v3, v4, v5}, LX/2Yu;-><init>(JLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0ZK;->a(LX/0ki;)V

    .line 448568
    invoke-virtual {p0, p2}, LX/2gW;->a(Landroid/os/IBinder;)V

    .line 448569
    iget-object v0, p0, LX/2gW;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 448570
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 6

    .prologue
    .line 448571
    iget-object v0, p0, LX/2gW;->a:LX/2gV;

    iget-object v0, v0, LX/2gV;->f:LX/0ZR;

    new-instance v1, LX/2Yu;

    iget-object v2, p0, LX/2gW;->a:LX/2gV;

    iget-object v2, v2, LX/2gV;->e:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    const-string v4, "ServiceDisconnected (MqttPushServiceClientManager)"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-direct {v1, v2, v3, v4, v5}, LX/2Yu;-><init>(JLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0ZK;->a(LX/0ki;)V

    .line 448572
    iget-object v0, p0, LX/2gW;->a:LX/2gV;

    invoke-virtual {v0}, LX/2gV;->g()V

    .line 448573
    return-void
.end method
