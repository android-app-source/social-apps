.class public LX/2OT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 402287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402288
    iput-object p1, p0, LX/2OT;->a:LX/0Or;

    .line 402289
    iput-object p2, p0, LX/2OT;->b:LX/0Or;

    .line 402290
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 402291
    if-eqz p0, :cond_0

    const-string v0, "m_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 402292
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "m_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(LX/2OT;Landroid/net/Uri$Builder;)V
    .locals 2

    .prologue
    .line 402293
    iget-object v0, p0, LX/2OT;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 402294
    if-eqz v0, :cond_0

    .line 402295
    const-string v1, "access_token"

    .line 402296
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, p0

    .line 402297
    invoke-virtual {p1, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 402298
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/2OT;
    .locals 3

    .prologue
    .line 402299
    new-instance v0, LX/2OT;

    const/16 v1, 0xb5a

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x19e

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/2OT;-><init>(LX/0Or;LX/0Or;)V

    .line 402300
    return-object v0
.end method


# virtual methods
.method public final a()Landroid/net/Uri$Builder;
    .locals 2

    .prologue
    .line 402301
    iget-object v0, p0, LX/2OT;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0s9;

    invoke-interface {v0}, LX/0s9;->a()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 402302
    const-string v1, "method/messaging.getAttachment"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 402303
    invoke-static {p0, v0}, LX/2OT;->a(LX/2OT;Landroid/net/Uri$Builder;)V

    .line 402304
    return-object v0
.end method
