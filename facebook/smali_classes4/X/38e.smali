.class public final LX/38e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/38f;


# instance fields
.field public final synthetic a:LX/37g;

.field public final synthetic b:LX/38d;


# direct methods
.method public constructor <init>(LX/38d;LX/37g;)V
    .locals 0

    .prologue
    .line 521177
    iput-object p1, p0, LX/38e;->b:LX/38d;

    iput-object p2, p0, LX/38e;->a:LX/37g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 521160
    iget-object v0, p0, LX/38e;->a:LX/37g;

    invoke-virtual {v0}, LX/37g;->d()LX/38p;

    move-result-object v0

    .line 521161
    invoke-virtual {v0}, LX/38p;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 521162
    iget-object v0, p0, LX/38e;->b:LX/38d;

    iget-object v0, v0, LX/38d;->b:LX/37a;

    sget-object v1, LX/38d;->a:LX/0Tn;

    iget-object v2, p0, LX/38e;->b:LX/38d;

    invoke-static {v2}, LX/38d;->e(LX/38d;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/37a;->a(LX/0Tn;Ljava/lang/String;)V

    .line 521163
    iget-object v0, p0, LX/38e;->b:LX/38d;

    .line 521164
    iget-object v1, v0, LX/38d;->f:LX/0Yb;

    if-nez v1, :cond_0

    .line 521165
    iget-object v1, v0, LX/38d;->c:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance p0, LX/7J8;

    invoke-direct {p0, v0}, LX/7J8;-><init>(LX/38d;)V

    invoke-interface {v1, v2, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, v0, LX/38d;->f:LX/0Yb;

    .line 521166
    :cond_0
    iget-object v1, v0, LX/38d;->d:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    iput-boolean v1, v0, LX/38d;->g:Z

    .line 521167
    iget-object v1, v0, LX/38d;->f:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 521168
    :cond_1
    :goto_0
    return-void

    .line 521169
    :cond_2
    invoke-virtual {v0}, LX/38p;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 521170
    iget-object v0, p0, LX/38e;->b:LX/38d;

    iget-object v0, v0, LX/38d;->b:LX/37a;

    sget-object v1, LX/38d;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/37a;->a(LX/0Tn;)V

    .line 521171
    iget-object v0, p0, LX/38e;->b:LX/38d;

    .line 521172
    iget-object v1, v0, LX/38d;->f:LX/0Yb;

    if-eqz v1, :cond_3

    .line 521173
    iget-object v1, v0, LX/38d;->f:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 521174
    const/4 v1, 0x0

    iput-object v1, v0, LX/38d;->f:LX/0Yb;

    .line 521175
    :cond_3
    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 521176
    return-void
.end method
