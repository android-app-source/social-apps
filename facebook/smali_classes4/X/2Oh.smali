.class public LX/2Oh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field public final a:LX/2Oi;

.field private final b:LX/2Oj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 402917
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2Oh;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Oi;LX/2Oj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 402951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 402952
    iput-object p1, p0, LX/2Oh;->a:LX/2Oi;

    .line 402953
    iput-object p2, p0, LX/2Oh;->b:LX/2Oj;

    .line 402954
    return-void
.end method

.method public static a(LX/0QB;)LX/2Oh;
    .locals 8

    .prologue
    .line 402922
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 402923
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 402924
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 402925
    if-nez v1, :cond_0

    .line 402926
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402927
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 402928
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 402929
    sget-object v1, LX/2Oh;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 402930
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 402931
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 402932
    :cond_1
    if-nez v1, :cond_4

    .line 402933
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 402934
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 402935
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 402936
    new-instance p0, LX/2Oh;

    invoke-static {v0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v1

    check-cast v1, LX/2Oi;

    invoke-static {v0}, LX/2Oj;->a(LX/0QB;)LX/2Oj;

    move-result-object v7

    check-cast v7, LX/2Oj;

    invoke-direct {p0, v1, v7}, LX/2Oh;-><init>(LX/2Oi;LX/2Oj;)V

    .line 402937
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 402938
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 402939
    if-nez v1, :cond_2

    .line 402940
    sget-object v0, LX/2Oh;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oh;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 402941
    :goto_1
    if-eqz v0, :cond_3

    .line 402942
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 402943
    :goto_3
    check-cast v0, LX/2Oh;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 402944
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 402945
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 402946
    :catchall_1
    move-exception v0

    .line 402947
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 402948
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 402949
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 402950
    :cond_2
    :try_start_8
    sget-object v0, LX/2Oh;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oh;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 402918
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    .line 402919
    iget-object v1, p0, LX/2Oh;->b:LX/2Oj;

    if-eqz v0, :cond_1

    iget-object v2, p0, LX/2Oh;->a:LX/2Oi;

    invoke-virtual {v2, v0}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, LX/2Oj;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v0

    .line 402920
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    :cond_0
    return-object v0

    .line 402921
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
