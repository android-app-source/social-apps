.class public LX/30L;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:D

.field public f:D


# direct methods
.method public constructor <init>(LX/5pG;)V
    .locals 4

    .prologue
    .line 484322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484323
    const-string v0, "heap_stats"

    invoke-interface {p1, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    .line 484324
    const-string v1, "size"

    invoke-interface {v0, v1}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, LX/30L;->a:I

    .line 484325
    const-string v1, "extra_size"

    invoke-interface {v0, v1}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, LX/30L;->b:I

    .line 484326
    const-string v1, "capacity"

    invoke-interface {v0, v1}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, LX/30L;->c:I

    .line 484327
    const-string v1, "object_count"

    invoke-interface {v0, v1}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/30L;->d:I

    .line 484328
    const-string v0, "gc_stats"

    invoke-interface {p1, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    .line 484329
    const-string v1, "last_eden_gc_length"

    invoke-interface {v0, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, p0, LX/30L;->e:D

    .line 484330
    const-string v1, "last_full_gc_length"

    invoke-interface {v0, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/30L;->f:D

    .line 484331
    return-void
.end method
