.class public LX/3PQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3PR;


# direct methods
.method public constructor <init>(LX/3PR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562043
    iput-object p1, p0, LX/3PQ;->a:LX/3PR;

    .line 562044
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/4VT;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 562033
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    const/4 v0, 0x0

    .line 562034
    invoke-static {p1}, LX/6AF;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;)LX/0Px;

    move-result-object v1

    .line 562035
    if-nez v1, :cond_1

    .line 562036
    :cond_0
    :goto_0
    return-object v0

    .line 562037
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;

    move-result-object v2

    invoke-static {v2}, LX/6AF;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$CommentFieldsForPlaceMutationModel;)Ljava/lang/String;

    move-result-object v2

    .line 562038
    if-eqz v2, :cond_0

    .line 562039
    new-instance v0, LX/6A7;

    invoke-direct {v0, v2, v1}, LX/6A7;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 562040
    move-object v0, v0

    .line 562041
    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 562031
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$AddPlaceListItemToCommentMutationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562032
    const/4 v0, 0x0

    return-object v0
.end method
