.class public LX/2S5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile p:LX/2S5;


# instance fields
.field private b:D

.field private final c:Landroid/media/AudioManager;

.field private final d:LX/1Er;

.field public final e:LX/0Zb;

.field public final f:LX/0SG;

.field private final g:LX/2S6;

.field private final h:Ljava/util/concurrent/ExecutorService;

.field public i:J

.field public j:J

.field public k:Z

.field public l:Z

.field private m:Z

.field private n:Ljava/io/File;

.field private o:Landroid/media/MediaRecorder;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 410658
    const-class v0, LX/2S5;

    sput-object v0, LX/2S5;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/media/AudioManager;LX/1Er;LX/0Zb;LX/0SG;Ljava/util/concurrent/ExecutorService;)V
    .locals 2
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 410649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410650
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2S5;->b:D

    .line 410651
    iput-object p1, p0, LX/2S5;->c:Landroid/media/AudioManager;

    .line 410652
    iput-object p2, p0, LX/2S5;->d:LX/1Er;

    .line 410653
    iput-object p3, p0, LX/2S5;->e:LX/0Zb;

    .line 410654
    iput-object p4, p0, LX/2S5;->f:LX/0SG;

    .line 410655
    new-instance v0, LX/2S6;

    invoke-direct {v0, p0}, LX/2S6;-><init>(LX/2S5;)V

    iput-object v0, p0, LX/2S5;->g:LX/2S6;

    .line 410656
    iput-object p5, p0, LX/2S5;->h:Ljava/util/concurrent/ExecutorService;

    .line 410657
    return-void
.end method

.method public static a(LX/0QB;)LX/2S5;
    .locals 9

    .prologue
    .line 410636
    sget-object v0, LX/2S5;->p:LX/2S5;

    if-nez v0, :cond_1

    .line 410637
    const-class v1, LX/2S5;

    monitor-enter v1

    .line 410638
    :try_start_0
    sget-object v0, LX/2S5;->p:LX/2S5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 410639
    if-eqz v2, :cond_0

    .line 410640
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 410641
    new-instance v3, LX/2S5;

    invoke-static {v0}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    invoke-static {v0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v5

    check-cast v5, LX/1Er;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v3 .. v8}, LX/2S5;-><init>(Landroid/media/AudioManager;LX/1Er;LX/0Zb;LX/0SG;Ljava/util/concurrent/ExecutorService;)V

    .line 410642
    move-object v0, v3

    .line 410643
    sput-object v0, LX/2S5;->p:LX/2S5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410644
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 410645
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 410646
    :cond_1
    sget-object v0, LX/2S5;->p:LX/2S5;

    return-object v0

    .line 410647
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 410648
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/2S5;LX/Dcu;)Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 410659
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    if-nez v0, :cond_0

    .line 410660
    :goto_0
    return-object v1

    .line 410661
    :cond_0
    :try_start_0
    iget-boolean v0, p0, LX/2S5;->k:Z

    if-eqz v0, :cond_1

    .line 410662
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    .line 410663
    invoke-direct {p0}, LX/2S5;->i()V

    .line 410664
    :cond_1
    sget-object v0, LX/Dcu;->SUCCESS:LX/Dcu;

    if-eq p1, v0, :cond_2

    sget-object v0, LX/Dcu;->TIME_LIMIT_REACHED_SUCCESS:LX/Dcu;

    if-ne p1, v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 410665
    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/2S5;->m:Z

    if-nez v0, :cond_3

    .line 410666
    iget-object v0, p0, LX/2S5;->n:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 410667
    :goto_2
    iput-boolean v4, p0, LX/2S5;->k:Z

    .line 410668
    iget-object v2, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->reset()V

    .line 410669
    iget-object v2, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->release()V

    .line 410670
    iput-object v1, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    .line 410671
    :goto_3
    iput-boolean v4, p0, LX/2S5;->l:Z

    move-object v1, v0

    .line 410672
    goto :goto_0

    .line 410673
    :catch_0
    move-exception v0

    .line 410674
    :try_start_1
    sget-object v2, LX/2S5;->a:Ljava/lang/Class;

    const-string v3, ""

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410675
    iput-boolean v4, p0, LX/2S5;->k:Z

    .line 410676
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    .line 410677
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 410678
    iput-object v1, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    move-object v0, v1

    .line 410679
    goto :goto_3

    .line 410680
    :catchall_0
    move-exception v0

    iput-boolean v4, p0, LX/2S5;->k:Z

    .line 410681
    iget-object v2, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->reset()V

    .line 410682
    iget-object v2, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->release()V

    .line 410683
    iput-object v1, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    throw v0

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private i()V
    .locals 4

    .prologue
    .line 410604
    iget-object v0, p0, LX/2S5;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/2S5;->i:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/2S5;->j:J

    .line 410605
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2S5;->i:J

    .line 410606
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 410616
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/2S5;->b:D

    .line 410617
    iget-object v5, p0, LX/2S5;->f:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    iput-wide v5, p0, LX/2S5;->i:J

    .line 410618
    const-wide/16 v5, 0x0

    iput-wide v5, p0, LX/2S5;->j:J

    .line 410619
    :try_start_0
    new-instance v0, Landroid/media/MediaRecorder;

    invoke-direct {v0}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    .line 410620
    iget-object v0, p0, LX/2S5;->d:LX/1Er;

    const-string v1, "orca-audio-"

    const-string v2, ".mp4"

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/2S5;->n:Ljava/io/File;

    .line 410621
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 410622
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 410623
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 410624
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    .line 410625
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    const/16 v1, 0x1f40

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    .line 410626
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    iget-object v1, p0, LX/2S5;->n:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 410627
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->prepare()V

    .line 410628
    iget-object v0, p0, LX/2S5;->c:Landroid/media/AudioManager;

    iget-object v1, p0, LX/2S5;->g:LX/2S6;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    if-eq v4, v0, :cond_0

    .line 410629
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to acquire the audio focus."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 410630
    :catch_0
    move-exception v0

    .line 410631
    iget-object v1, p0, LX/2S5;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/messaging/audio/record/AudioRecorder$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/messaging/audio/record/AudioRecorder$1;-><init>(LX/2S5;Ljava/lang/Exception;)V

    const v0, -0x261ac953

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 410632
    sget-object v0, LX/Dcu;->ERROR:LX/Dcu;

    invoke-static {p0, v0}, LX/2S5;->a(LX/2S5;LX/Dcu;)Landroid/net/Uri;

    .line 410633
    :goto_0
    return-void

    .line 410634
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->start()V

    .line 410635
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2S5;->k:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 410607
    :try_start_0
    iget-boolean v0, p0, LX/2S5;->k:Z

    if-eqz v0, :cond_0

    .line 410608
    iget-object v0, p0, LX/2S5;->o:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    .line 410609
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2S5;->k:Z

    .line 410610
    invoke-direct {p0}, LX/2S5;->i()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 410611
    :cond_0
    :goto_0
    iput-boolean v4, p0, LX/2S5;->l:Z

    .line 410612
    return-void

    .line 410613
    :catch_0
    move-exception v0

    .line 410614
    iput-boolean v4, p0, LX/2S5;->m:Z

    .line 410615
    sget-object v1, LX/2S5;->a:Ljava/lang/Class;

    iget-boolean v2, p0, LX/2S5;->m:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
