.class public final LX/3K0;
.super LX/3Jv;
.source ""


# direct methods
.method public constructor <init>(LX/3Jx;[F)V
    .locals 0

    .prologue
    .line 548210
    invoke-direct {p0, p1, p2}, LX/3Jv;-><init>(LX/3Jx;[F)V

    .line 548211
    return-void
.end method


# virtual methods
.method public final a(LX/9Ua;)V
    .locals 2

    .prologue
    .line 548212
    iget-object v0, p0, LX/3Jv;->a:LX/3Jx;

    iget-object v1, p0, LX/3Jv;->b:[F

    invoke-virtual {p0, p1, v0, v1}, LX/3K0;->a(LX/9Ua;LX/3Jx;[F)V

    .line 548213
    return-void
.end method

.method public final a(LX/9Ua;LX/3Jx;[F)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 548214
    sget-object v0, LX/3Jy;->b:[I

    invoke-virtual {p2}, LX/3Jx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 548215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "No such argument format %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548216
    :pswitch_0
    aget v1, p3, v4

    aget v2, p3, v3

    aget v3, p3, v5

    aget v4, p3, v6

    aget v5, p3, v7

    const/4 v0, 0x5

    aget v6, p3, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, LX/9Ua;->b(FFFFFF)V

    .line 548217
    :goto_0
    return-void

    .line 548218
    :pswitch_1
    aget v1, p3, v4

    aget v2, p3, v3

    aget v3, p3, v5

    aget v4, p3, v6

    aget v5, p3, v7

    const/4 v0, 0x5

    aget v6, p3, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, LX/9Ua;->a(FFFFFF)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
