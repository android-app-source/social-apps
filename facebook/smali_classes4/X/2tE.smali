.class public final LX/2tE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/katana/urimap/IntentHandlerUtil;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/urimap/IntentHandlerUtil;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 474859
    iput-object p1, p0, LX/2tE;->b:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    iput-object p2, p0, LX/2tE;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 474860
    iget-object v0, p0, LX/2tE;->b:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    iget-object v1, p0, LX/2tE;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->cL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 474861
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 474852
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 474853
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;

    .line 474854
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;->a:Lcom/facebook/notifications/model/SMSNotificationURL;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;->a:Lcom/facebook/notifications/model/SMSNotificationURL;

    iget-object v1, v1, Lcom/facebook/notifications/model/SMSNotificationURL;->notificationLongUrlList:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;->a:Lcom/facebook/notifications/model/SMSNotificationURL;

    iget-object v1, v1, Lcom/facebook/notifications/model/SMSNotificationURL;->notificationLongUrlList:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 474855
    iget-object v0, v0, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;->a:Lcom/facebook/notifications/model/SMSNotificationURL;

    iget-object v0, v0, Lcom/facebook/notifications/model/SMSNotificationURL;->notificationLongUrlList:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/SMSNotificationURL$NotificationLongURL;

    iget-object v0, v0, Lcom/facebook/notifications/model/SMSNotificationURL$NotificationLongURL;->longUrl:Ljava/lang/String;

    .line 474856
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/2tE;->b:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    iget-object v2, p0, LX/2tE;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474857
    :goto_0
    return-void

    .line 474858
    :cond_0
    iget-object v0, p0, LX/2tE;->b:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    iget-object v1, p0, LX/2tE;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->cL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method
