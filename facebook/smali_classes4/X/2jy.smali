.class public interface abstract LX/2jy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(LX/0Rl;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<TTEdge;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(LX/2kJ;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kJ",
            "<TTEdge;>;)V"
        }
    .end annotation
.end method

.method public abstract a(LX/2nj;LX/3DP;LX/5Mb;JLX/3Cb;)V
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "LX/5Mb",
            "<TTEdge;>;J",
            "LX/3Cb;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(LX/5Mb;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5Mb",
            "<TTEdge;>;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;LX/3Cf;)V
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/3Cf",
            "<TTEdge;>;)V"
        }
    .end annotation
.end method

.method public abstract a(LX/2nj;LX/3DP;I)Z
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTEdge;TTEdge;)Z"
        }
    .end annotation
.end method

.method public abstract b()V
.end method

.method public abstract b(LX/2kJ;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kJ",
            "<TTEdge;>;)V"
        }
    .end annotation
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()Z
.end method
