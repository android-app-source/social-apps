.class public final LX/2OH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V
    .locals 0

    .prologue
    .line 400680
    iput-object p1, p0, LX/2OH;->a:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x54e2740d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 400681
    iget-object v1, p0, LX/2OH;->a:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/16 v8, 0x0

    .line 400682
    const-string v4, "resource"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 400683
    invoke-static {v10}, LX/6ed;->b(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v11

    .line 400684
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 400685
    const-string v5, "com.facebook.orca.media.upload.MEDIA_UPLOAD_COMPLETE"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "com.facebook.orca.media.upload.MEDIA_GET_FBID_COMPLETE"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 400686
    :cond_0
    iget-object v4, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n:LX/0Xl;

    invoke-static {v10}, LX/FGn;->d(Lcom/facebook/ui/media/attachments/MediaResource;)Landroid/content/Intent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 400687
    :goto_0
    const/16 v1, 0x27

    const v2, -0x5d691087

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 400688
    :cond_1
    const-string v5, "com.facebook.orca.media.upload.MEDIA_TRANSCODE_PROGRESS"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 400689
    const-string v4, "p"

    invoke-virtual {p2, v4, v8, v9}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v6

    .line 400690
    :cond_2
    :goto_1
    iget-object v4, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    invoke-static {v10}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2MO;->a(LX/6ed;)Z

    move-result v5

    .line 400691
    iget-object v4, v10, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-static/range {v4 .. v9}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(LX/2MK;ZDD)D

    move-result-wide v4

    .line 400692
    iget-object v6, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->G:LX/0QI;

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-interface {v6, v11, v7}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 400693
    iget-object v6, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n:LX/0Xl;

    .line 400694
    const-string v7, "com.facebook.orca.media.upload.PROCESS_MEDIA_TOTAL_PROGRESS"

    invoke-static {v7, v10, v4, v5}, LX/FGn;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;D)Landroid/content/Intent;

    move-result-object v7

    move-object v4, v7

    .line 400695
    invoke-interface {v6, v4}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 400696
    :cond_3
    const-string v5, "com.facebook.orca.media.upload.MEDIA_TRANSCODE_COMPLETE"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 400697
    const-string v5, "com.facebook.orca.media.upload.MEDIA_UPLOAD_PROGRESS"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 400698
    const-string v4, "p"

    invoke-virtual {p2, v4, v8, v9}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v8

    goto :goto_1

    :cond_4
    move-wide v6, v8

    goto :goto_1
.end method
