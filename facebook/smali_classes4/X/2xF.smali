.class public interface abstract LX/2xF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Landroid/location/Location;
.end method

.method public abstract a(Ljava/lang/String;)Lcom/google/android/gms/location/ActivityRecognitionResult;
.end method

.method public abstract a(JZLandroid/app/PendingIntent;)V
.end method

.method public abstract a(LX/2xS;)V
.end method

.method public abstract a(LX/2xV;)V
.end method

.method public abstract a(LX/7a5;Ljava/lang/String;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;LX/4uo;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;LX/7a5;Ljava/lang/String;)V
.end method

.method public abstract a(Landroid/location/Location;)V
.end method

.method public abstract a(Landroid/location/Location;I)V
.end method

.method public abstract a(Lcom/google/android/gms/location/ActivityRecognitionRequest;Landroid/app/PendingIntent;LX/4uo;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/GeofencingRequest;Landroid/app/PendingIntent;LX/7a5;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/GestureRequest;Landroid/app/PendingIntent;LX/4uo;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;LX/2xV;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;LX/2xV;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationSettingsRequest;LX/7a8;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/internal/LocationRequestInternal;LX/2xV;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/internal/LocationRequestUpdateData;)V
.end method

.method public abstract a(Ljava/util/List;Landroid/app/PendingIntent;LX/7a5;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/internal/ParcelableGeofence;",
            ">;",
            "Landroid/app/PendingIntent;",
            "LX/7a5;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Z)V
.end method

.method public abstract a([Ljava/lang/String;LX/7a5;Ljava/lang/String;)V
.end method

.method public abstract b(Ljava/lang/String;)Landroid/location/Location;
.end method

.method public abstract b(Landroid/app/PendingIntent;)V
.end method

.method public abstract b(Landroid/app/PendingIntent;LX/4uo;)V
.end method

.method public abstract c(Ljava/lang/String;)Lcom/google/android/gms/location/LocationAvailability;
.end method

.method public abstract c(Landroid/app/PendingIntent;LX/4uo;)V
.end method

.method public abstract d(Landroid/app/PendingIntent;LX/4uo;)V
.end method

.method public abstract e(Landroid/app/PendingIntent;LX/4uo;)V
.end method
