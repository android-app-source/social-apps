.class public LX/2TK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/content/Context;

.field public d:LX/2TI;

.field private e:LX/2TL;

.field public f:LX/0ka;

.field private g:LX/0u7;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 414074
    const-class v0, LX/2TK;

    sput-object v0, LX/2TK;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/2TI;LX/2TL;LX/0u7;LX/0ka;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/vault/annotations/IsVaultEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2TI;",
            "LX/2TL;",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            "LX/0ka;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414067
    iput-object p1, p0, LX/2TK;->c:Landroid/content/Context;

    .line 414068
    iput-object p2, p0, LX/2TK;->a:LX/0Or;

    .line 414069
    iput-object p3, p0, LX/2TK;->d:LX/2TI;

    .line 414070
    iput-object p4, p0, LX/2TK;->e:LX/2TL;

    .line 414071
    iput-object p6, p0, LX/2TK;->f:LX/0ka;

    .line 414072
    iput-object p5, p0, LX/2TK;->g:LX/0u7;

    .line 414073
    return-void
.end method

.method public static a(LX/0QB;)LX/2TK;
    .locals 1

    .prologue
    .line 414065
    invoke-static {p0}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2TK;
    .locals 7

    .prologue
    .line 414063
    new-instance v0, LX/2TK;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x1590

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/2TI;->a(LX/0QB;)LX/2TI;

    move-result-object v3

    check-cast v3, LX/2TI;

    invoke-static {p0}, LX/2TL;->a(LX/0QB;)LX/2TL;

    move-result-object v4

    check-cast v4, LX/2TL;

    invoke-static {p0}, LX/0u7;->a(LX/0QB;)LX/0u7;

    move-result-object v5

    check-cast v5, LX/0u7;

    invoke-static {p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v6

    check-cast v6, LX/0ka;

    invoke-direct/range {v0 .. v6}, LX/2TK;-><init>(Landroid/content/Context;LX/0Or;LX/2TI;LX/2TL;LX/0u7;LX/0ka;)V

    .line 414064
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 414028
    iget-object v0, p0, LX/2TK;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 414040
    invoke-virtual {p0}, LX/2TK;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 414041
    :goto_0
    return v0

    .line 414042
    :cond_0
    iget-object v0, p0, LX/2TK;->e:LX/2TL;

    invoke-virtual {v0}, LX/2TL;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    move v0, v1

    .line 414043
    goto :goto_0

    .line 414044
    :cond_1
    iget-object v0, p0, LX/2TK;->c:Landroid/content/Context;

    invoke-static {v0}, LX/ERe;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 414045
    goto :goto_0

    .line 414046
    :cond_2
    const/16 v0, 0x8

    if-ne p1, v0, :cond_3

    .line 414047
    const/4 v0, 0x1

    goto :goto_0

    .line 414048
    :cond_3
    invoke-virtual {p0}, LX/2TK;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 414049
    const/16 v0, 0xc

    if-eq p1, v0, :cond_4

    const/16 v0, 0xb

    if-ne p1, v0, :cond_7

    :cond_4
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 414050
    if-nez v0, :cond_5

    move v0, v1

    .line 414051
    goto :goto_0

    .line 414052
    :cond_5
    iget-object v0, p0, LX/2TK;->c:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 414053
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 414054
    goto :goto_0

    .line 414055
    :cond_6
    iget-object v0, p0, LX/2TK;->d:LX/2TI;

    invoke-virtual {v0}, LX/2TI;->a()Ljava/lang/String;

    move-result-object v0

    .line 414056
    const-string v1, "WIFI_ONLY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 414057
    iget-object v0, p0, LX/2TK;->f:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->b()Z

    move-result v0

    .line 414058
    :goto_2
    move v0, v0

    .line 414059
    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 414060
    :cond_8
    const-string v1, "MOBILE_RADIO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 414061
    iget-object v0, p0, LX/2TK;->c:Landroid/content/Context;

    invoke-static {v0}, LX/ERe;->a(Landroid/content/Context;)Z

    move-result v0

    goto :goto_2

    .line 414062
    :cond_9
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b(I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 414037
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/2TK;->c:Landroid/content/Context;

    const-class v2, Lcom/facebook/vault/service/VaultSyncService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 414038
    const-string v1, "sync_reason"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 414039
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 414035
    iget-object v0, p0, LX/2TK;->d:LX/2TI;

    invoke-virtual {v0}, LX/2TI;->a()Ljava/lang/String;

    move-result-object v0

    .line 414036
    invoke-virtual {p0}, LX/2TK;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 414032
    invoke-virtual {p0, p1}, LX/2TK;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414033
    :try_start_0
    iget-object v0, p0, LX/2TK;->c:Landroid/content/Context;

    invoke-virtual {p0, p1}, LX/2TK;->b(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 414034
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 414031
    iget-object v0, p0, LX/2TK;->f:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->b()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 414030
    iget-object v0, p0, LX/2TK;->g:LX/0u7;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, LX/0u7;->a(I)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 414029
    iget-object v0, p0, LX/2TK;->e:LX/2TL;

    invoke-virtual {v0}, LX/2TL;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
