.class public LX/2HS;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0Tn;


# instance fields
.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/00H;

.field public final e:LX/0dC;

.field public final f:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 390429
    const-class v0, LX/2HS;

    sput-object v0, LX/2HS;->a:Ljava/lang/Class;

    .line 390430
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "fbns_tokens/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2HS;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/00H;LX/0dC;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390432
    iput-object p1, p0, LX/2HS;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 390433
    iput-object p2, p0, LX/2HS;->d:LX/00H;

    .line 390434
    iput-object p3, p0, LX/2HS;->e:LX/0dC;

    .line 390435
    sget-object v0, LX/2HS;->b:LX/0Tn;

    iget-object v1, p0, LX/2HS;->d:LX/00H;

    invoke-virtual {v1}, LX/00H;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/2HS;->f:LX/0Tn;

    .line 390436
    return-void
.end method
