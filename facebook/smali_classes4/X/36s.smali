.class public LX/36s;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public b:I

.field public d:Landroid/net/Uri;

.field public e:J

.field public f:J

.field public g:J

.field public h:LX/36t;

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 500517
    const-class v0, LX/36s;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/36s;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 500509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 500510
    iput-wide v0, p0, LX/36s;->e:J

    .line 500511
    iput-wide v0, p0, LX/36s;->f:J

    .line 500512
    sget-object v0, LX/36t;->PROGRESSIVE:LX/36t;

    iput-object v0, p0, LX/36s;->h:LX/36t;

    .line 500513
    const/4 v0, -0x1

    iput v0, p0, LX/36s;->b:I

    .line 500514
    iput-object p1, p0, LX/36s;->d:Landroid/net/Uri;

    .line 500515
    iput-object p2, p0, LX/36s;->a:Ljava/lang/String;

    .line 500516
    return-void
.end method


# virtual methods
.method public final a(JII)I
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 500518
    cmp-long v0, p1, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 500519
    iget-wide v0, p0, LX/36s;->e:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-wide v0, p0, LX/36s;->f:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 500520
    iget-wide v0, p0, LX/36s;->g:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 500521
    iget-wide v2, p0, LX/36s;->e:J

    long-to-double v2, v2

    iget-wide v4, p0, LX/36s;->f:J

    long-to-double v4, v4

    long-to-double v0, v0

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v0, v6

    const-wide/high16 v6, 0x4020000000000000L    # 8.0

    div-double/2addr v0, v6

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 500522
    :goto_1
    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 500523
    if-lez p4, :cond_0

    .line 500524
    invoke-static {v0, p4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 500525
    :cond_0
    return v0

    .line 500526
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 500527
    :cond_2
    const v0, 0x7a120

    goto :goto_1
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 500508
    iget-wide v0, p0, LX/36s;->f:J

    return-wide v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 500507
    iget-wide v0, p0, LX/36s;->g:J

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 500495
    if-ne p0, p1, :cond_1

    .line 500496
    :cond_0
    :goto_0
    return v0

    .line 500497
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 500498
    goto :goto_0

    .line 500499
    :cond_3
    check-cast p1, LX/36s;

    .line 500500
    iget-wide v2, p0, LX/36s;->e:J

    iget-wide v4, p1, LX/36s;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 500501
    goto :goto_0

    .line 500502
    :cond_4
    iget-wide v2, p0, LX/36s;->f:J

    iget-wide v4, p1, LX/36s;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 500503
    goto :goto_0

    .line 500504
    :cond_5
    iget-wide v2, p0, LX/36s;->g:J

    iget-wide v4, p1, LX/36s;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 500505
    goto :goto_0

    .line 500506
    :cond_6
    iget-object v2, p0, LX/36s;->d:Landroid/net/Uri;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/36s;->d:Landroid/net/Uri;

    iget-object v3, p1, LX/36s;->d:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p1, LX/36s;->d:Landroid/net/Uri;

    if-nez v2, :cond_7

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 500489
    iget-object v0, p0, LX/36s;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/36s;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    .line 500490
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LX/36s;->e:J

    iget-wide v4, p0, LX/36s;->e:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 500491
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LX/36s;->f:J

    iget-wide v4, p0, LX/36s;->f:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 500492
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LX/36s;->g:J

    iget-wide v4, p0, LX/36s;->g:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 500493
    return v0

    .line 500494
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
