.class public LX/2UV;
.super LX/1Eg;
.source ""


# instance fields
.field private final a:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

.field private final b:LX/2Os;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0SG;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field public final f:LX/2UX;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;LX/2Os;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/2UX;)V
    .locals 1
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416102
    const-string v0, "UNSEEN_COUNTS"

    invoke-direct {p0, v0}, LX/1Eg;-><init>(Ljava/lang/String;)V

    .line 416103
    iput-object p1, p0, LX/2UV;->a:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    .line 416104
    iput-object p2, p0, LX/2UV;->b:LX/2Os;

    .line 416105
    iput-object p3, p0, LX/2UV;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 416106
    iput-object p4, p0, LX/2UV;->d:LX/0SG;

    .line 416107
    iput-object p5, p0, LX/2UV;->e:Ljava/util/concurrent/ExecutorService;

    .line 416108
    iput-object p6, p0, LX/2UV;->f:LX/2UX;

    .line 416109
    return-void
.end method


# virtual methods
.method public final h()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416110
    sget-object v0, LX/2VD;->USER_LOGGED_IN:LX/2VD;

    sget-object v1, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 416111
    iget-object v2, p0, LX/2UV;->b:LX/2Os;

    invoke-virtual {v2}, LX/2Os;->a()Ljava/util/List;

    move-result-object v2

    .line 416112
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v2, v1, :cond_1

    .line 416113
    :cond_0
    :goto_0
    return v0

    .line 416114
    :cond_1
    iget-object v2, p0, LX/2UV;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 416115
    iget-object v4, p0, LX/2UV;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/2Vv;->k:LX/0Tn;

    invoke-interface {v4, v5, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 416116
    sub-long v4, v2, v4

    const-wide/32 v6, 0xdbba0

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    .line 416117
    iget-object v4, p0, LX/2UV;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/2Vv;->l:LX/0Tn;

    invoke-interface {v4, v5, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 416118
    sub-long/2addr v2, v4

    const-wide/32 v4, 0x6ddd00

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final j()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2YS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416119
    iget-object v0, p0, LX/2UV;->f:LX/2UX;

    const-string v1, "mswitchaccounts_unseen_fetch"

    invoke-virtual {v0, v1}, LX/2UX;->a(Ljava/lang/String;)V

    .line 416120
    iget-object v0, p0, LX/2UV;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2Vv;->k:LX/0Tn;

    iget-object v2, p0, LX/2UV;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 416121
    iget-object v0, p0, LX/2UV;->a:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    invoke-virtual {v0}, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->a()LX/1ML;

    move-result-object v0

    .line 416122
    new-instance v1, LX/Jd2;

    invoke-direct {v1, p0}, LX/Jd2;-><init>(LX/2UV;)V

    iget-object v2, p0, LX/2UV;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 416123
    new-instance v1, LX/3f0;

    const-class v2, LX/2UV;

    invoke-direct {v1, v2}, LX/3f0;-><init>(Ljava/lang/Class;)V

    .line 416124
    iget-object v2, p0, LX/2UV;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 416125
    return-object v1
.end method
