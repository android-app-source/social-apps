.class public LX/2To;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2To;


# instance fields
.field private final a:LX/0s9;

.field private final b:LX/0ad;

.field private final c:Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;

.field private final d:LX/29a;


# direct methods
.method public constructor <init>(LX/0s9;LX/0ad;Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;LX/29a;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414785
    iput-object p1, p0, LX/2To;->a:LX/0s9;

    .line 414786
    iput-object p2, p0, LX/2To;->b:LX/0ad;

    .line 414787
    iput-object p3, p0, LX/2To;->c:Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;

    .line 414788
    iput-object p4, p0, LX/2To;->d:LX/29a;

    .line 414789
    return-void
.end method

.method public static a(LX/0QB;)LX/2To;
    .locals 7

    .prologue
    .line 414763
    sget-object v0, LX/2To;->e:LX/2To;

    if-nez v0, :cond_1

    .line 414764
    const-class v1, LX/2To;

    monitor-enter v1

    .line 414765
    :try_start_0
    sget-object v0, LX/2To;->e:LX/2To;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 414766
    if-eqz v2, :cond_0

    .line 414767
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 414768
    new-instance p0, LX/2To;

    invoke-static {v0}, LX/14Y;->b(LX/0QB;)LX/0s9;

    move-result-object v3

    check-cast v3, LX/0s9;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;->b(LX/0QB;)Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;

    move-result-object v5

    check-cast v5, Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;

    invoke-static {v0}, LX/29a;->a(LX/0QB;)LX/29a;

    move-result-object v6

    check-cast v6, LX/29a;

    invoke-direct {p0, v3, v4, v5, v6}, LX/2To;-><init>(LX/0s9;LX/0ad;Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;LX/29a;)V

    .line 414769
    move-object v0, p0

    .line 414770
    sput-object v0, LX/2To;->e:LX/2To;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414771
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 414772
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 414773
    :cond_1
    sget-object v0, LX/2To;->e:LX/2To;

    return-object v0

    .line 414774
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 414775
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 4

    .prologue
    .line 414776
    iget-object v0, p0, LX/2To;->d:LX/29a;

    .line 414777
    iget-object v1, v0, LX/29a;->c:Lcom/facebook/xanalytics/XAnalyticsNative;

    move-object v0, v1

    .line 414778
    iget-object v1, p0, LX/2To;->b:LX/0ad;

    sget v2, LX/2Tp;->a:I

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/xanalytics/XAnalyticsNative;->updateMultibatchSize(I)V

    .line 414779
    const/4 v1, 0x0

    iget-object v2, p0, LX/2To;->a:LX/0s9;

    invoke-interface {v2}, LX/0s9;->b()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/2To;->c:Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/xanalytics/XAnalyticsNative;->updateTigonInstance(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/tigon/iface/TigonServiceHolder;)J

    .line 414780
    iget-object v1, p0, LX/2To;->d:LX/29a;

    .line 414781
    iget-object v2, v1, LX/29a;->g:Ljava/lang/String;

    move-object v1, v2

    .line 414782
    invoke-virtual {v0, v1}, Lcom/facebook/xanalytics/XAnalyticsNative;->resumeUploading(Ljava/lang/String;)V

    .line 414783
    return-void
.end method
