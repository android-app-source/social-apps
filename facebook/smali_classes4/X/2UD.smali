.class public final enum LX/2UD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2UD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2UD;

.field public static final enum EXPERIMENTAL_SMS_CONFIRMATION:LX/2UD;

.field public static final enum OPENID_CONNECT_EMAIL_CONFIRMATION:LX/2UD;

.field public static final enum REGULAR_SMS_CONFIRMATION:LX/2UD;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 415582
    new-instance v0, LX/2UD;

    const-string v1, "REGULAR_SMS_CONFIRMATION"

    invoke-direct {v0, v1, v2}, LX/2UD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2UD;->REGULAR_SMS_CONFIRMATION:LX/2UD;

    .line 415583
    new-instance v0, LX/2UD;

    const-string v1, "EXPERIMENTAL_SMS_CONFIRMATION"

    invoke-direct {v0, v1, v3}, LX/2UD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2UD;->EXPERIMENTAL_SMS_CONFIRMATION:LX/2UD;

    .line 415584
    new-instance v0, LX/2UD;

    const-string v1, "OPENID_CONNECT_EMAIL_CONFIRMATION"

    invoke-direct {v0, v1, v4}, LX/2UD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2UD;->OPENID_CONNECT_EMAIL_CONFIRMATION:LX/2UD;

    .line 415585
    const/4 v0, 0x3

    new-array v0, v0, [LX/2UD;

    sget-object v1, LX/2UD;->REGULAR_SMS_CONFIRMATION:LX/2UD;

    aput-object v1, v0, v2

    sget-object v1, LX/2UD;->EXPERIMENTAL_SMS_CONFIRMATION:LX/2UD;

    aput-object v1, v0, v3

    sget-object v1, LX/2UD;->OPENID_CONNECT_EMAIL_CONFIRMATION:LX/2UD;

    aput-object v1, v0, v4

    sput-object v0, LX/2UD;->$VALUES:[LX/2UD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 415586
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2UD;
    .locals 1

    .prologue
    .line 415587
    const-class v0, LX/2UD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2UD;

    return-object v0
.end method

.method public static values()[LX/2UD;
    .locals 1

    .prologue
    .line 415588
    sget-object v0, LX/2UD;->$VALUES:[LX/2UD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2UD;

    return-object v0
.end method
