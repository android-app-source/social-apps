.class public final LX/28l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0dc;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0dc;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 374578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374579
    iput-object p1, p0, LX/28l;->a:LX/0QB;

    .line 374580
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 374581
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/28l;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 374582
    packed-switch p2, :pswitch_data_0

    .line 374583
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 374584
    :pswitch_0
    new-instance v0, LX/2ab;

    invoke-direct {v0}, LX/2ab;-><init>()V

    .line 374585
    move-object v0, v0

    .line 374586
    move-object v0, v0

    .line 374587
    :goto_0
    return-object v0

    .line 374588
    :pswitch_1
    new-instance v0, LX/GAw;

    invoke-direct {v0}, LX/GAw;-><init>()V

    .line 374589
    move-object v0, v0

    .line 374590
    move-object v0, v0

    .line 374591
    goto :goto_0

    .line 374592
    :pswitch_2
    new-instance v0, LX/10R;

    invoke-direct {v0}, LX/10R;-><init>()V

    .line 374593
    move-object v0, v0

    .line 374594
    move-object v0, v0

    .line 374595
    goto :goto_0

    .line 374596
    :pswitch_3
    new-instance v0, LX/2Fn;

    invoke-direct {v0}, LX/2Fn;-><init>()V

    .line 374597
    move-object v0, v0

    .line 374598
    move-object v0, v0

    .line 374599
    goto :goto_0

    .line 374600
    :pswitch_4
    invoke-static {p1}, LX/Hlk;->a(LX/0QB;)LX/Hlk;

    move-result-object v0

    goto :goto_0

    .line 374601
    :pswitch_5
    new-instance v0, LX/6D7;

    invoke-direct {v0}, LX/6D7;-><init>()V

    .line 374602
    move-object v0, v0

    .line 374603
    move-object v0, v0

    .line 374604
    goto :goto_0

    .line 374605
    :pswitch_6
    new-instance v0, LX/0dp;

    invoke-direct {v0}, LX/0dp;-><init>()V

    .line 374606
    move-object v0, v0

    .line 374607
    move-object v0, v0

    .line 374608
    goto :goto_0

    .line 374609
    :pswitch_7
    invoke-static {p1}, LX/30R;->a(LX/0QB;)LX/30R;

    move-result-object v0

    goto :goto_0

    .line 374610
    :pswitch_8
    new-instance v0, LX/3g1;

    invoke-direct {v0}, LX/3g1;-><init>()V

    .line 374611
    move-object v0, v0

    .line 374612
    move-object v0, v0

    .line 374613
    goto :goto_0

    .line 374614
    :pswitch_9
    new-instance v0, LX/DAD;

    invoke-direct {v0}, LX/DAD;-><init>()V

    .line 374615
    move-object v0, v0

    .line 374616
    move-object v0, v0

    .line 374617
    goto :goto_0

    .line 374618
    :pswitch_a
    new-instance v0, LX/32V;

    invoke-direct {v0}, LX/32V;-><init>()V

    .line 374619
    move-object v0, v0

    .line 374620
    move-object v0, v0

    .line 374621
    goto :goto_0

    .line 374622
    :pswitch_b
    new-instance v0, LX/7vb;

    invoke-direct {v0}, LX/7vb;-><init>()V

    .line 374623
    move-object v0, v0

    .line 374624
    move-object v0, v0

    .line 374625
    goto :goto_0

    .line 374626
    :pswitch_c
    new-instance v0, LX/AwE;

    invoke-direct {v0}, LX/AwE;-><init>()V

    .line 374627
    move-object v0, v0

    .line 374628
    move-object v0, v0

    .line 374629
    goto :goto_0

    .line 374630
    :pswitch_d
    new-instance v0, LX/1CA;

    invoke-direct {v0}, LX/1CA;-><init>()V

    .line 374631
    move-object v0, v0

    .line 374632
    move-object v0, v0

    .line 374633
    goto :goto_0

    .line 374634
    :pswitch_e
    new-instance v0, LX/B6A;

    invoke-direct {v0}, LX/B6A;-><init>()V

    .line 374635
    move-object v0, v0

    .line 374636
    move-object v0, v0

    .line 374637
    goto :goto_0

    .line 374638
    :pswitch_f
    new-instance v0, LX/0eC;

    invoke-direct {v0}, LX/0eC;-><init>()V

    .line 374639
    move-object v0, v0

    .line 374640
    move-object v0, v0

    .line 374641
    goto :goto_0

    .line 374642
    :pswitch_10
    new-instance v0, LX/Jco;

    invoke-direct {v0}, LX/Jco;-><init>()V

    .line 374643
    move-object v0, v0

    .line 374644
    move-object v0, v0

    .line 374645
    goto :goto_0

    .line 374646
    :pswitch_11
    new-instance v0, LX/6gE;

    invoke-direct {v0}, LX/6gE;-><init>()V

    .line 374647
    move-object v0, v0

    .line 374648
    move-object v0, v0

    .line 374649
    goto/16 :goto_0

    .line 374650
    :pswitch_12
    invoke-static {p1}, LX/FJV;->a(LX/0QB;)LX/FJV;

    move-result-object v0

    goto/16 :goto_0

    .line 374651
    :pswitch_13
    new-instance v0, LX/Jow;

    invoke-direct {v0}, LX/Jow;-><init>()V

    .line 374652
    move-object v0, v0

    .line 374653
    move-object v0, v0

    .line 374654
    goto/16 :goto_0

    .line 374655
    :pswitch_14
    new-instance v0, LX/0db;

    invoke-direct {v0}, LX/0db;-><init>()V

    .line 374656
    move-object v0, v0

    .line 374657
    move-object v0, v0

    .line 374658
    goto/16 :goto_0

    .line 374659
    :pswitch_15
    new-instance v0, LX/1p8;

    invoke-direct {v0}, LX/1p8;-><init>()V

    .line 374660
    move-object v0, v0

    .line 374661
    move-object v0, v0

    .line 374662
    goto/16 :goto_0

    .line 374663
    :pswitch_16
    new-instance v0, LX/CeL;

    invoke-direct {v0}, LX/CeL;-><init>()V

    .line 374664
    move-object v0, v0

    .line 374665
    move-object v0, v0

    .line 374666
    goto/16 :goto_0

    .line 374667
    :pswitch_17
    invoke-static {p1}, LX/2bs;->a(LX/0QB;)LX/2bs;

    move-result-object v0

    goto/16 :goto_0

    .line 374668
    :pswitch_18
    new-instance v0, LX/1vE;

    invoke-direct {v0}, LX/1vE;-><init>()V

    .line 374669
    move-object v0, v0

    .line 374670
    move-object v0, v0

    .line 374671
    goto/16 :goto_0

    .line 374672
    :pswitch_19
    new-instance v0, LX/FXO;

    invoke-direct {v0}, LX/FXO;-><init>()V

    .line 374673
    move-object v0, v0

    .line 374674
    move-object v0, v0

    .line 374675
    goto/16 :goto_0

    .line 374676
    :pswitch_1a
    new-instance v0, LX/2TR;

    invoke-direct {v0}, LX/2TR;-><init>()V

    .line 374677
    move-object v0, v0

    .line 374678
    move-object v0, v0

    .line 374679
    goto/16 :goto_0

    .line 374680
    :pswitch_1b
    new-instance v0, LX/2ns;

    invoke-direct {v0}, LX/2ns;-><init>()V

    .line 374681
    move-object v0, v0

    .line 374682
    move-object v0, v0

    .line 374683
    goto/16 :goto_0

    .line 374684
    :pswitch_1c
    new-instance v0, LX/Hgv;

    invoke-direct {v0}, LX/Hgv;-><init>()V

    .line 374685
    move-object v0, v0

    .line 374686
    move-object v0, v0

    .line 374687
    goto/16 :goto_0

    .line 374688
    :pswitch_1d
    invoke-static {p1}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v0

    goto/16 :goto_0

    .line 374689
    :pswitch_1e
    invoke-static {p1}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a(LX/0QB;)Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    move-result-object v0

    goto/16 :goto_0

    .line 374690
    :pswitch_1f
    invoke-static {p1}, LX/1rM;->a(LX/0QB;)LX/1rM;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 374691
    const/16 v0, 0x20

    return v0
.end method
