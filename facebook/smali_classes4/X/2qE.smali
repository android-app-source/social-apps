.class public final enum LX/2qE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2qE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2qE;

.field public static final enum STATE_CREATED:LX/2qE;

.field public static final enum STATE_DESTROYED:LX/2qE;

.field public static final enum STATE_UNKNOWN:LX/2qE;

.field public static final enum STATE_UPDATED:LX/2qE;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 470754
    new-instance v0, LX/2qE;

    const-string v1, "STATE_UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v3, v2}, LX/2qE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qE;->STATE_UNKNOWN:LX/2qE;

    .line 470755
    new-instance v0, LX/2qE;

    const-string v1, "STATE_CREATED"

    const-string v2, "created"

    invoke-direct {v0, v1, v4, v2}, LX/2qE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qE;->STATE_CREATED:LX/2qE;

    .line 470756
    new-instance v0, LX/2qE;

    const-string v1, "STATE_UPDATED"

    const-string v2, "updated"

    invoke-direct {v0, v1, v5, v2}, LX/2qE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qE;->STATE_UPDATED:LX/2qE;

    .line 470757
    new-instance v0, LX/2qE;

    const-string v1, "STATE_DESTROYED"

    const-string v2, "destroyed"

    invoke-direct {v0, v1, v6, v2}, LX/2qE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qE;->STATE_DESTROYED:LX/2qE;

    .line 470758
    const/4 v0, 0x4

    new-array v0, v0, [LX/2qE;

    sget-object v1, LX/2qE;->STATE_UNKNOWN:LX/2qE;

    aput-object v1, v0, v3

    sget-object v1, LX/2qE;->STATE_CREATED:LX/2qE;

    aput-object v1, v0, v4

    sget-object v1, LX/2qE;->STATE_UPDATED:LX/2qE;

    aput-object v1, v0, v5

    sget-object v1, LX/2qE;->STATE_DESTROYED:LX/2qE;

    aput-object v1, v0, v6

    sput-object v0, LX/2qE;->$VALUES:[LX/2qE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 470759
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 470760
    iput-object p3, p0, LX/2qE;->value:Ljava/lang/String;

    .line 470761
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2qE;
    .locals 1

    .prologue
    .line 470762
    const-class v0, LX/2qE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2qE;

    return-object v0
.end method

.method public static values()[LX/2qE;
    .locals 1

    .prologue
    .line 470763
    sget-object v0, LX/2qE;->$VALUES:[LX/2qE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2qE;

    return-object v0
.end method
