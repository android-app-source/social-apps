.class public LX/2Ej;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:I

.field private static c:[I


# instance fields
.field public d:[Landroid/util/SparseIntArray;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 385966
    const-class v0, LX/2Ej;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Ej;->a:Ljava/lang/String;

    .line 385967
    const/4 v0, -0x1

    sput v0, LX/2Ej;->b:I

    .line 385968
    const/4 v0, 0x0

    sput-object v0, LX/2Ej;->c:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 385965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()LX/2Ej;
    .locals 1

    .prologue
    .line 385964
    new-instance v0, LX/2Ej;

    invoke-direct {v0}, LX/2Ej;-><init>()V

    invoke-direct {v0}, LX/2Ej;->c()LX/2Ej;

    move-result-object v0

    return-object v0
.end method

.method private static b(I)Landroid/util/SparseIntArray;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 385942
    new-instance v3, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/sys/devices/system/cpu/cpu"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/cpufreq/stats/time_in_state"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 385943
    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    move-result v1

    if-nez v1, :cond_1

    .line 385944
    :cond_0
    :goto_0
    return-object v0

    .line 385945
    :cond_1
    :try_start_0
    new-instance v1, Landroid/util/SparseIntArray;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Landroid/util/SparseIntArray;-><init>(I)V

    .line 385946
    new-instance v2, Ljava/util/Scanner;

    invoke-direct {v2, v3}, Ljava/util/Scanner;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/util/InputMismatchException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385947
    :goto_1
    :try_start_1
    invoke-virtual {v2}, Ljava/util/Scanner;->hasNextInt()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 385948
    invoke-virtual {v2}, Ljava/util/Scanner;->nextInt()I

    move-result v3

    .line 385949
    invoke-virtual {v2}, Ljava/util/Scanner;->nextInt()I

    move-result v4

    .line 385950
    invoke-virtual {v1, v3, v4}, Landroid/util/SparseIntArray;->append(II)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/InputMismatchException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 385951
    :catch_0
    move-exception v1

    .line 385952
    :goto_2
    :try_start_2
    sget-object v3, LX/2Ej;->a:Ljava/lang/String;

    const-string v4, "Unable to read cpu time in state file for cpu %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v1, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 385953
    if-eqz v2, :cond_0

    .line 385954
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    move-object v0, v1

    goto :goto_0

    .line 385955
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 385956
    :goto_3
    :try_start_3
    sget-object v3, LX/2Ej;->a:Ljava/lang/String;

    const-string v4, "Unable to parse cpu time in state file for cpu %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v1, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 385957
    if-eqz v2, :cond_0

    .line 385958
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    goto :goto_0

    .line 385959
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_4
    if-eqz v2, :cond_3

    .line 385960
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    :cond_3
    throw v0

    .line 385961
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 385962
    :catch_2
    move-exception v1

    goto :goto_3

    .line 385963
    :catch_3
    move-exception v1

    move-object v2, v0

    goto :goto_2
.end method

.method private static c(I)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 385969
    new-instance v3, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "/sys/devices/system/cpu/cpu"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/topology/core_siblings"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 385970
    :try_start_0
    new-instance v2, Ljava/util/Scanner;

    invoke-direct {v2, v3}, Ljava/util/Scanner;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/InputMismatchException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385971
    :try_start_1
    invoke-virtual {v2}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 385972
    invoke-virtual {v2}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/util/InputMismatchException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 385973
    :cond_0
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    .line 385974
    const/16 v2, 0x10

    :try_start_2
    invoke-static {v0, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v0

    .line 385975
    :goto_0
    return v0

    .line 385976
    :catch_0
    move-exception v2

    move-object v8, v2

    move-object v2, v0

    move-object v0, v8

    .line 385977
    :goto_1
    :try_start_3
    sget-object v3, LX/2Ej;->a:Ljava/lang/String;

    const-string v4, "Unable to read cpu topology file for cpu %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v0, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 385978
    if-eqz v2, :cond_1

    .line 385979
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    :cond_1
    move v0, v1

    goto :goto_0

    .line 385980
    :catch_1
    move-exception v2

    move-object v8, v2

    move-object v2, v0

    move-object v0, v8

    .line 385981
    :goto_2
    :try_start_4
    sget-object v3, LX/2Ej;->a:Ljava/lang/String;

    const-string v4, "Unable to parse cpu topology file for cpu %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v0, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 385982
    if-eqz v2, :cond_2

    .line 385983
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    :cond_2
    move v0, v1

    goto :goto_0

    .line 385984
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v2, :cond_3

    .line 385985
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    :cond_3
    throw v0

    .line 385986
    :catch_2
    move v0, v1

    goto :goto_0

    .line 385987
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 385988
    :catch_3
    move-exception v0

    goto :goto_2

    .line 385989
    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method private c()LX/2Ej;
    .locals 5

    .prologue
    .line 385933
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    .line 385934
    :try_start_0
    invoke-static {}, LX/2Ej;->e()I

    move-result v2

    .line 385935
    new-array v0, v2, [Landroid/util/SparseIntArray;

    iput-object v0, p0, LX/2Ej;->d:[Landroid/util/SparseIntArray;

    .line 385936
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 385937
    iget-object v3, p0, LX/2Ej;->d:[Landroid/util/SparseIntArray;

    invoke-static {v0}, LX/2Ej;->b(I)Landroid/util/SparseIntArray;

    move-result-object v4

    aput-object v4, v3, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385938
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 385939
    :cond_0
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 385940
    return-object p0

    .line 385941
    :catchall_0
    move-exception v0

    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v0
.end method

.method private static d()[I
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 385912
    sget-object v1, LX/2Ej;->c:[I

    if-eqz v1, :cond_0

    .line 385913
    sget-object v0, LX/2Ej;->c:[I

    .line 385914
    :goto_0
    return-object v0

    .line 385915
    :cond_0
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v4

    .line 385916
    :try_start_0
    invoke-static {}, LX/2Ej;->e()I

    move-result v5

    .line 385917
    new-array v6, v5, [I

    move v1, v0

    move v2, v0

    .line 385918
    :goto_1
    if-ge v0, v5, :cond_3

    .line 385919
    shl-int v3, v8, v0

    and-int/2addr v3, v2

    if-eqz v3, :cond_1

    .line 385920
    add-int/lit8 v0, v0, 0x1

    .line 385921
    goto :goto_1

    .line 385922
    :cond_1
    invoke-static {v0}, LX/2Ej;->c(I)I

    move-result v7

    .line 385923
    if-gez v7, :cond_2

    .line 385924
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    shl-int v2, v8, v5

    add-int/lit8 v2, v2, -0x1

    aput v2, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385925
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_0

    .line 385926
    :cond_2
    or-int v3, v2, v7

    .line 385927
    add-int/lit8 v2, v1, 0x1

    :try_start_1
    aput v7, v6, v1

    move v1, v2

    move v2, v3

    .line 385928
    goto :goto_1

    .line 385929
    :cond_3
    new-array v0, v1, [I

    sput-object v0, LX/2Ej;->c:[I

    .line 385930
    const/4 v0, 0x0

    sget-object v2, LX/2Ej;->c:[I

    const/4 v3, 0x0

    invoke-static {v6, v0, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 385931
    sget-object v0, LX/2Ej;->c:[I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 385932
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v0
.end method

.method private static e()I
    .locals 2

    .prologue
    .line 385903
    sget v0, LX/2Ej;->b:I

    if-ltz v0, :cond_0

    .line 385904
    sget v0, LX/2Ej;->b:I

    .line 385905
    :goto_0
    return v0

    .line 385906
    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/devices/system/cpu/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 385907
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 385908
    const/4 v0, 0x0

    goto :goto_0

    .line 385909
    :cond_1
    new-instance v1, LX/2Ek;

    invoke-direct {v1}, LX/2Ek;-><init>()V

    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    .line 385910
    array-length v0, v0

    .line 385911
    sput v0, LX/2Ej;->b:I

    goto :goto_0
.end method


# virtual methods
.method public final b()[Landroid/util/SparseIntArray;
    .locals 14

    .prologue
    .line 385885
    invoke-static {}, LX/2Ej;->d()[I

    move-result-object v1

    .line 385886
    array-length v0, v1

    new-array v2, v0, [Landroid/util/SparseIntArray;

    .line 385887
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_2

    .line 385888
    aget v3, v1, v0

    const/4 v5, 0x0

    .line 385889
    new-instance v7, Landroid/util/SparseIntArray;

    const/16 v4, 0x10

    invoke-direct {v7, v4}, Landroid/util/SparseIntArray;-><init>(I)V

    .line 385890
    iget-object v4, p0, LX/2Ej;->d:[Landroid/util/SparseIntArray;

    array-length v8, v4

    move v6, v5

    :goto_1
    if-ge v6, v8, :cond_1

    .line 385891
    iget-object v4, p0, LX/2Ej;->d:[Landroid/util/SparseIntArray;

    aget-object v9, v4, v6

    .line 385892
    const/4 v4, 0x1

    shl-int/2addr v4, v6

    and-int/2addr v4, v3

    if-eqz v4, :cond_0

    if-eqz v9, :cond_0

    .line 385893
    invoke-virtual {v9}, Landroid/util/SparseIntArray;->size()I

    move-result v10

    move v4, v5

    .line 385894
    :goto_2
    if-ge v4, v10, :cond_0

    .line 385895
    invoke-virtual {v9, v4}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v11

    .line 385896
    invoke-virtual {v9, v4}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v12

    invoke-virtual {v7, v11, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v13

    add-int/2addr v12, v13

    invoke-virtual {v7, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 385897
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 385898
    :cond_0
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_1

    .line 385899
    :cond_1
    move-object v3, v7

    .line 385900
    aput-object v3, v2, v0

    .line 385901
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 385902
    :cond_2
    return-object v2
.end method
