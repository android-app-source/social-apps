.class public final LX/3Me;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/divebar/contacts/DivebarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/divebar/contacts/DivebarFragment;)V
    .locals 0

    .prologue
    .line 555155
    iput-object p1, p0, LX/3Me;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0xce2e423

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 555156
    iget-object v1, p0, LX/3Me;->a:Lcom/facebook/divebar/contacts/DivebarFragment;

    const/4 v2, 0x1

    .line 555157
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    .line 555158
    const/4 p0, 0x0

    .line 555159
    const-string p3, "com.facebook.presence.PRESENCE_MANAGER_SETTINGS_CHANGED"

    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_2

    .line 555160
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 555161
    invoke-static {v1}, Lcom/facebook/divebar/contacts/DivebarFragment;->o(Lcom/facebook/divebar/contacts/DivebarFragment;)V

    .line 555162
    :cond_1
    const/16 v1, 0x27

    const v2, -0x7c69fa16

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 555163
    :cond_2
    const-string p3, "com.facebook.contacts.ACTION_CONTACT_SYNC_PROGRESS"

    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_0

    .line 555164
    const-string p3, "com.facebook.contacts.FAVORITE_CONTACT_SYNC_PROGRESS"

    invoke-virtual {p3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    move v2, p0

    goto :goto_0
.end method
