.class public final enum LX/2Bh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Bh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Bh;

.field public static final enum FLATTENABLE:LX/2Bh;

.field public static final enum NULL:LX/2Bh;

.field public static final enum PARCELABLE:LX/2Bh;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 381421
    new-instance v0, LX/2Bh;

    const-string v1, "NULL"

    invoke-direct {v0, v1, v2}, LX/2Bh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Bh;->NULL:LX/2Bh;

    new-instance v0, LX/2Bh;

    const-string v1, "PARCELABLE"

    invoke-direct {v0, v1, v3}, LX/2Bh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Bh;->PARCELABLE:LX/2Bh;

    new-instance v0, LX/2Bh;

    const-string v1, "FLATTENABLE"

    invoke-direct {v0, v1, v4}, LX/2Bh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Bh;->FLATTENABLE:LX/2Bh;

    .line 381422
    const/4 v0, 0x3

    new-array v0, v0, [LX/2Bh;

    sget-object v1, LX/2Bh;->NULL:LX/2Bh;

    aput-object v1, v0, v2

    sget-object v1, LX/2Bh;->PARCELABLE:LX/2Bh;

    aput-object v1, v0, v3

    sget-object v1, LX/2Bh;->FLATTENABLE:LX/2Bh;

    aput-object v1, v0, v4

    sput-object v0, LX/2Bh;->$VALUES:[LX/2Bh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 381423
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromObject(Ljava/lang/Object;)LX/2Bh;
    .locals 1

    .prologue
    .line 381424
    if-nez p0, :cond_0

    .line 381425
    sget-object v0, LX/2Bh;->NULL:LX/2Bh;

    .line 381426
    :goto_0
    return-object v0

    .line 381427
    :cond_0
    instance-of v0, p0, Landroid/os/Parcelable;

    if-eqz v0, :cond_1

    .line 381428
    sget-object v0, LX/2Bh;->PARCELABLE:LX/2Bh;

    goto :goto_0

    .line 381429
    :cond_1
    sget-object v0, LX/2Bh;->FLATTENABLE:LX/2Bh;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Bh;
    .locals 1

    .prologue
    .line 381430
    const-class v0, LX/2Bh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Bh;

    return-object v0
.end method

.method public static values()[LX/2Bh;
    .locals 1

    .prologue
    .line 381431
    sget-object v0, LX/2Bh;->$VALUES:[LX/2Bh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Bh;

    return-object v0
.end method
