.class public LX/2Lw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Tn;

.field private static volatile k:LX/2Lw;


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/2Ly;

.field private final d:LX/0Zb;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0W3;

.field private final i:LX/2M1;

.field private j:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/FCV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 395798
    sget-object v0, LX/0db;->a:LX/0Tn;

    const-string v1, "reliability_serialized"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/2Lw;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/2Ly;LX/0Zb;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0W3;LX/2M1;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/analytics/perf/IsClientReliabilityLoggerEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/2Ly;",
            "LX/0Zb;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/2M1;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 395787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395788
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    .line 395789
    iput-object p1, p0, LX/2Lw;->b:LX/0SG;

    .line 395790
    iput-object p2, p0, LX/2Lw;->c:LX/2Ly;

    .line 395791
    iput-object p3, p0, LX/2Lw;->d:LX/0Zb;

    .line 395792
    iput-object p4, p0, LX/2Lw;->e:LX/0Or;

    .line 395793
    iput-object p5, p0, LX/2Lw;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 395794
    iput-object p6, p0, LX/2Lw;->g:LX/0Ot;

    .line 395795
    iput-object p7, p0, LX/2Lw;->h:LX/0W3;

    .line 395796
    iput-object p8, p0, LX/2Lw;->i:LX/2M1;

    .line 395797
    return-void
.end method

.method public static a(LX/0QB;)LX/2Lw;
    .locals 12

    .prologue
    .line 395774
    sget-object v0, LX/2Lw;->k:LX/2Lw;

    if-nez v0, :cond_1

    .line 395775
    const-class v1, LX/2Lw;

    monitor-enter v1

    .line 395776
    :try_start_0
    sget-object v0, LX/2Lw;->k:LX/2Lw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 395777
    if-eqz v2, :cond_0

    .line 395778
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 395779
    new-instance v3, LX/2Lw;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v5

    check-cast v5, LX/2Ly;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    const/16 v7, 0x14d9

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v10

    check-cast v10, LX/0W3;

    invoke-static {v0}, LX/2M1;->a(LX/0QB;)LX/2M1;

    move-result-object v11

    check-cast v11, LX/2M1;

    invoke-direct/range {v3 .. v11}, LX/2Lw;-><init>(LX/0SG;LX/2Ly;LX/0Zb;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0W3;LX/2M1;)V

    .line 395780
    move-object v0, v3

    .line 395781
    sput-object v0, LX/2Lw;->k:LX/2Lw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395782
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 395783
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 395784
    :cond_1
    sget-object v0, LX/2Lw;->k:LX/2Lw;

    return-object v0

    .line 395785
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 395786
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 395769
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "msg_reliability"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 395770
    const-string v1, "reliabilities_map"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 395771
    iget-object v1, p0, LX/2Lw;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395772
    monitor-exit p0

    return-void

    .line 395773
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()Ljava/lang/String;
    .locals 12
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 395743
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 395744
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 395745
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FCV;

    .line 395746
    iget-object v3, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    int-to-long v4, v3

    invoke-direct {p0}, LX/2Lw;->g()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    iget-wide v4, v1, LX/FCV;->sendAttemptTimestamp:J

    iget-object v3, p0, LX/2Lw;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    invoke-direct {p0}, LX/2Lw;->e()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    sub-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 395747
    const/4 v0, 0x0

    .line 395748
    :goto_0
    monitor-exit p0

    return-object v0

    .line 395749
    :cond_0
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 395750
    :goto_1
    iget-object v4, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->size()I

    move-result v4

    int-to-long v4, v4

    invoke-direct {p0}, LX/2Lw;->g()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gtz v4, :cond_1

    sget-object v4, LX/FCU;->UNKNOWN:LX/FCU;

    iget-object v5, v1, LX/FCV;->outcome:LX/FCU;

    invoke-virtual {v4, v5}, LX/FCU;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-wide v4, v1, LX/FCV;->sendAttemptTimestamp:J

    iget-object v6, p0, LX/2Lw;->b:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-direct {p0}, LX/2Lw;->f()J

    move-result-wide v8

    sub-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-gez v4, :cond_4

    .line 395751
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 395752
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-lez v10, :cond_2

    .line 395753
    const/16 v10, 0x2c

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 395754
    :cond_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395755
    iget-object v10, v1, LX/FCV;->messageType:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395756
    iget v10, v1, LX/FCV;->mqttAttempts:I

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395757
    iget v10, v1, LX/FCV;->graphAttempts:I

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395758
    iget-object v10, v1, LX/FCV;->outcome:LX/FCU;

    sget-object v11, LX/FCU;->FAILURE_PERMANENT:LX/FCU;

    if-eq v10, v11, :cond_3

    iget-object v10, v1, LX/FCV;->outcome:LX/FCU;

    sget-object v11, LX/FCU;->FAILURE_RETRYABLE:LX/FCU;

    if-eq v10, v11, :cond_3

    iget-object v10, v1, LX/FCV;->outcome:LX/FCU;

    sget-object v11, LX/FCU;->UNKNOWN:LX/FCU;

    if-ne v10, v11, :cond_5

    .line 395759
    :cond_3
    iget-wide v10, v1, LX/FCV;->sendAttemptTimestamp:J

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 395760
    :goto_2
    const-string v10, ":"

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395761
    iget-object v10, v1, LX/FCV;->outcome:LX/FCU;

    iget-object v10, v10, LX/FCU;->rawValue:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395762
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 395763
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 395764
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 395765
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FCV;

    goto/16 :goto_1

    .line 395766
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto/16 :goto_0

    .line 395767
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 395768
    :cond_5
    iget-wide v10, v1, LX/FCV;->timeSinceFirstSendAttempt:J

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method private declared-synchronized c()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 395729
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 395730
    :goto_0
    monitor-exit p0

    return-void

    .line 395731
    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 395732
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 395733
    iget-object v2, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 395734
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->flush()V

    .line 395735
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 395736
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    .line 395737
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    .line 395738
    iget-object v0, p0, LX/2Lw;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2Lw;->a:LX/0Tn;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395739
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 395740
    :try_start_2
    iget-object v0, p0, LX/2Lw;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "reliabilities_serialization_failed"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 395741
    iget-object v0, p0, LX/2Lw;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2Lw;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 395742
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()Ljava/util/LinkedHashMap;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/FCV;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 395715
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/2Lw;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 395716
    :goto_0
    monitor-exit p0

    return-object v0

    .line 395717
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2Lw;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2Lw;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 395718
    if-nez v0, :cond_1

    .line 395719
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395720
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 395721
    :cond_1
    const/4 v1, 0x0

    :try_start_2
    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 395722
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 395723
    new-instance v0, Ljava/io/ObjectInputStream;

    invoke-direct {v0, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 395724
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashMap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 395725
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 395726
    :try_start_3
    iget-object v0, p0, LX/2Lw;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "bad_reliabilities_deserialization"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 395727
    iget-object v0, p0, LX/2Lw;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2Lw;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 395728
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private e()J
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 395713
    iget-object v0, p0, LX/2Lw;->h:LX/0W3;

    sget-wide v2, LX/0X5;->fD:J

    const-wide/16 v4, 0x5460

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    .line 395714
    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method private f()J
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 395711
    iget-object v0, p0, LX/2Lw;->h:LX/0W3;

    sget-wide v2, LX/0X5;->fC:J

    const-wide/16 v4, 0x2a30

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    .line 395712
    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method private g()J
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 395710
    iget-object v0, p0, LX/2Lw;->h:LX/0W3;

    sget-wide v2, LX/0X5;->fB:J

    const/16 v1, 0x1f4

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method private declared-synchronized h()V
    .locals 1

    .prologue
    .line 395638
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/2Lw;->i()Z

    .line 395639
    invoke-direct {p0}, LX/2Lw;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395640
    monitor-exit p0

    return-void

    .line 395641
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized i()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 395703
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/2Lw;->j(LX/2Lw;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 395704
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 395705
    :cond_1
    :try_start_1
    invoke-direct {p0}, LX/2Lw;->b()Ljava/lang/String;

    move-result-object v1

    .line 395706
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 395707
    invoke-direct {p0, v1}, LX/2Lw;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395708
    const/4 v0, 0x1

    goto :goto_0

    .line 395709
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized j(LX/2Lw;)Z
    .locals 1

    .prologue
    .line 395699
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    if-nez v0, :cond_0

    .line 395700
    invoke-direct {p0}, LX/2Lw;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    .line 395701
    :cond_0
    iget-object v0, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 395702
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 395693
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Lw;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 395694
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 395695
    :cond_1
    :try_start_1
    invoke-direct {p0}, LX/2Lw;->i()Z

    move-result v0

    .line 395696
    if-eqz v0, :cond_0

    .line 395697
    invoke-direct {p0}, LX/2Lw;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395698
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/FCY;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 4

    .prologue
    .line 395681
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Lw;->i:LX/2M1;

    invoke-virtual {v0, p1, p2}, LX/2M1;->a(LX/FCY;Lcom/facebook/messaging/model/messages/Message;)V

    .line 395682
    iget-object v0, p0, LX/2Lw;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/2Lw;->j(LX/2Lw;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 395683
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 395684
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCV;

    .line 395685
    if-nez v0, :cond_2

    .line 395686
    new-instance v0, LX/FCV;

    iget-object v1, p0, LX/2Lw;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v1, p0, LX/2Lw;->c:LX/2Ly;

    invoke-virtual {v1, p2}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, LX/FCV;-><init>(JLjava/lang/String;)V

    .line 395687
    iget-object v1, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395688
    :cond_2
    sget-object v1, LX/FCY;->MQTT:LX/FCY;

    if-ne p1, v1, :cond_3

    .line 395689
    iget v1, v0, LX/FCV;->mqttAttempts:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/FCV;->mqttAttempts:I

    .line 395690
    :goto_1
    invoke-direct {p0}, LX/2Lw;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395691
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 395692
    :cond_3
    iget v1, v0, LX/FCV;->graphAttempts:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/FCV;->graphAttempts:I

    goto :goto_1
.end method

.method public final declared-synchronized a(LX/FCY;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 395665
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Lw;->i:LX/2M1;

    invoke-virtual {v0, p1, p2}, LX/2M1;->a(LX/FCY;Ljava/lang/String;)V

    .line 395666
    iget-object v0, p0, LX/2Lw;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/2Lw;->j(LX/2Lw;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 395667
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 395668
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCV;

    .line 395669
    if-nez v0, :cond_2

    .line 395670
    if-nez p3, :cond_0

    .line 395671
    iget-object v0, p0, LX/2Lw;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "no_send_attempt_on_success"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No previous send attempt for msg with offline threading id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395672
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 395673
    :cond_2
    :try_start_2
    iget v1, v0, LX/FCV;->graphAttempts:I

    iget v2, v0, LX/FCV;->mqttAttempts:I

    add-int/2addr v1, v2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 395674
    iget-object v0, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 395675
    invoke-direct {p0}, LX/2Lw;->h()V

    goto :goto_0

    .line 395676
    :cond_3
    iget-object v1, p0, LX/2Lw;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, v0, LX/FCV;->sendAttemptTimestamp:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, LX/FCV;->timeSinceFirstSendAttempt:J

    .line 395677
    sget-object v1, LX/FCY;->MQTT:LX/FCY;

    if-ne p1, v1, :cond_4

    .line 395678
    sget-object v1, LX/FCU;->SUCCESS_MQTT:LX/FCU;

    iput-object v1, v0, LX/FCV;->outcome:LX/FCU;

    .line 395679
    :goto_1
    invoke-direct {p0}, LX/2Lw;->h()V

    goto :goto_0

    .line 395680
    :cond_4
    sget-object v1, LX/FCU;->SUCCESS_GRAPH:LX/FCU;

    iput-object v1, v0, LX/FCV;->outcome:LX/FCU;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 4

    .prologue
    .line 395658
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Lw;->i:LX/2M1;

    invoke-virtual {v0, p1}, LX/2M1;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 395659
    iget-object v0, p0, LX/2Lw;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/2Lw;->j(LX/2Lw;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 395660
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 395661
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 395662
    new-instance v0, LX/FCV;

    iget-object v1, p0, LX/2Lw;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v1, p0, LX/2Lw;->c:LX/2Ly;

    invoke-virtual {v1, p1}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, LX/FCV;-><init>(JLjava/lang/String;)V

    .line 395663
    iget-object v1, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395664
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/messaging/model/send/SendError;)V
    .locals 6

    .prologue
    .line 395647
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Lw;->i:LX/2M1;

    invoke-virtual {v0, p1, p2}, LX/2M1;->a(Ljava/lang/String;Lcom/facebook/messaging/model/send/SendError;)V

    .line 395648
    iget-object v0, p0, LX/2Lw;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/2Lw;->j(LX/2Lw;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 395649
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 395650
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/2Lw;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCV;

    .line 395651
    if-nez v0, :cond_2

    .line 395652
    iget-object v0, p0, LX/2Lw;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "no_send_attempt_on_failure"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No previous send attempt for msg with offline threading id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395653
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 395654
    :cond_2
    :try_start_2
    iget-object v1, p0, LX/2Lw;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, v0, LX/FCV;->sendAttemptTimestamp:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, LX/FCV;->timeSinceFirstSendAttempt:J

    .line 395655
    iget-object v1, p2, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-boolean v1, v1, LX/6fP;->shouldNotBeRetried:Z

    if-eqz v1, :cond_3

    sget-object v1, LX/FCU;->FAILURE_PERMANENT:LX/FCU;

    :goto_1
    iput-object v1, v0, LX/FCV;->outcome:LX/FCU;

    .line 395656
    invoke-direct {p0}, LX/2Lw;->h()V

    goto :goto_0

    .line 395657
    :cond_3
    sget-object v1, LX/FCU;->FAILURE_RETRYABLE:LX/FCU;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized b(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 1

    .prologue
    .line 395644
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Lw;->i:LX/2M1;

    invoke-virtual {v0, p1}, LX/2M1;->b(Lcom/facebook/messaging/model/messages/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395645
    monitor-exit p0

    return-void

    .line 395646
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final init()V
    .locals 0

    .prologue
    .line 395642
    invoke-virtual {p0}, LX/2Lw;->a()V

    .line 395643
    return-void
.end method
