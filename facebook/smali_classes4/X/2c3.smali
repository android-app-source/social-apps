.class public final LX/2c3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2c3;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2sP;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2c9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2sP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2c9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 440010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440011
    iput-object p1, p0, LX/2c3;->a:LX/0Ot;

    .line 440012
    iput-object p2, p0, LX/2c3;->b:LX/0Ot;

    .line 440013
    return-void
.end method

.method public static a(LX/0QB;)LX/2c3;
    .locals 5

    .prologue
    .line 440014
    sget-object v0, LX/2c3;->c:LX/2c3;

    if-nez v0, :cond_1

    .line 440015
    const-class v1, LX/2c3;

    monitor-enter v1

    .line 440016
    :try_start_0
    sget-object v0, LX/2c3;->c:LX/2c3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 440017
    if-eqz v2, :cond_0

    .line 440018
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 440019
    new-instance v3, LX/2c3;

    const/16 v4, 0xfc4

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0xfc2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/2c3;-><init>(LX/0Ot;LX/0Ot;)V

    .line 440020
    move-object v0, v3

    .line 440021
    sput-object v0, LX/2c3;->c:LX/2c3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 440022
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 440023
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 440024
    :cond_1
    sget-object v0, LX/2c3;->c:LX/2c3;

    return-object v0

    .line 440025
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 440026
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/privacy/model/PrivacyOptionsResult;Lcom/facebook/privacy/model/PrivacyOptionsResult;)V
    .locals 2
    .param p2    # Lcom/facebook/privacy/model/PrivacyOptionsResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 440027
    if-eqz p1, :cond_1

    .line 440028
    iget-object v0, p0, LX/2c3;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2c9;

    sget-object v1, LX/2Zv;->COMPOSER_OPTIONS_FETCHED:LX/2Zv;

    invoke-virtual {v0, v1}, LX/2c9;->a(LX/2Zv;)V

    .line 440029
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 440030
    iget-object v0, p0, LX/2c3;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2c9;

    sget-object v1, LX/2Zv;->STICKY_PRIVACY_CHANGED_BY_FETCH:LX/2Zv;

    invoke-virtual {v0, v1}, LX/2c9;->a(LX/2Zv;)V

    .line 440031
    :cond_0
    iget-object v0, p0, LX/2c3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2sP;

    invoke-virtual {v0, p1}, LX/2sP;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)V

    .line 440032
    :cond_1
    return-void
.end method

.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 440033
    const-string v0, "fetch_privacy_options"

    .line 440034
    iget-object v1, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v1

    .line 440035
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 440036
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 440037
    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    .line 440038
    const-string v2, "privacy.data_freshness"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 440039
    if-nez v2, :cond_6

    .line 440040
    :cond_0
    :goto_0
    move-object v1, v1

    .line 440041
    iget-object v0, p0, LX/2c3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2sP;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/2sP;->a(Z)Lcom/facebook/privacy/model/PrivacyOptionsResult;

    move-result-object v2

    .line 440042
    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-eq v1, v0, :cond_2

    .line 440043
    iget-object v0, p0, LX/2c3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2sP;

    invoke-virtual {v0}, LX/2sP;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-ne v1, v0, :cond_2

    .line 440044
    :cond_1
    if-nez v2, :cond_5

    .line 440045
    iget-object v0, p0, LX/2c3;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2c9;

    sget-object v1, LX/2Zv;->BLOCK_FOR_CACHED_COMPOSER_OPTIONS:LX/2Zv;

    invoke-virtual {v0, v1}, LX/2c9;->a(LX/2Zv;)V

    .line 440046
    :cond_2
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 440047
    iget-boolean v0, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 440048
    if-eqz v0, :cond_3

    .line 440049
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 440050
    invoke-virtual {p0, v0, v2}, LX/2c3;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;Lcom/facebook/privacy/model/PrivacyOptionsResult;)V

    :cond_3
    move-object v0, v1

    .line 440051
    :goto_1
    move-object v0, v0

    .line 440052
    :goto_2
    return-object v0

    :cond_4
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_2

    .line 440053
    :cond_5
    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 440054
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 440055
    if-eqz v0, :cond_2

    iget-object v3, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v3, :cond_2

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 440056
    goto :goto_1

    .line 440057
    :cond_6
    invoke-static {v2}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v3

    .line 440058
    if-eqz v3, :cond_0

    .line 440059
    sget-object v2, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    invoke-virtual {v2, v3}, LX/0rS;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual {v2, v3}, LX/0rS;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    sget-object v2, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    invoke-virtual {v2, v3}, LX/0rS;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    move-object v1, v3

    .line 440060
    goto/16 :goto_0

    .line 440061
    :cond_8
    const/4 v2, 0x0

    goto :goto_3
.end method
