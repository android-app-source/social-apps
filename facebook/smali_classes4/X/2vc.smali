.class public LX/2vc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 478338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478339
    return-void
.end method

.method public static a(LX/0Or;LX/0yH;LX/1wY;Lcom/facebook/common/perftest/PerfTestConfig;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)LX/1wZ;
    .locals 2
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/location/IsForceAndroidPlatformImplEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "LX/1wY;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")",
            "LX/1wZ;"
        }
    .end annotation

    .prologue
    .line 478340
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478341
    sget-object v0, LX/1wZ;->MOCK_MPK_STATIC:LX/1wZ;

    .line 478342
    :goto_0
    return-object v0

    .line 478343
    :cond_0
    sget-object v0, LX/2sj;->b:LX/0Tn;

    sget-object v1, LX/2ve;->DEFAULT:LX/2ve;

    iget v1, v1, LX/2ve;->key:I

    invoke-interface {p4, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    invoke-static {v0}, LX/2ve;->get(I)LX/2ve;

    move-result-object v0

    .line 478344
    sget-object v1, LX/2ve;->DEFAULT:LX/2ve;

    if-eq v0, v1, :cond_2

    .line 478345
    invoke-static {p2, p5}, LX/2vc;->b(LX/1wY;LX/03V;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, LX/2ve;->GOOGLE_PLAY_PREF:LX/2ve;

    if-ne v0, v1, :cond_1

    .line 478346
    sget-object v0, LX/1wZ;->ANDROID_PLATFORM:LX/1wZ;

    goto :goto_0

    .line 478347
    :cond_1
    iget-object v0, v0, LX/2ve;->locationImplementation:LX/1wZ;

    goto :goto_0

    .line 478348
    :cond_2
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_3

    sget-object v0, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    invoke-virtual {p1, v0}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p2, p5}, LX/2vc;->b(LX/1wY;LX/03V;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 478349
    :cond_3
    sget-object v0, LX/1wZ;->ANDROID_PLATFORM:LX/1wZ;

    goto :goto_0

    .line 478350
    :cond_4
    sget-object v0, LX/1wZ;->GOOGLE_PLAY:LX/1wZ;

    goto :goto_0
.end method

.method public static a(LX/0Or;LX/1wY;LX/0yH;LX/0Or;LX/0Or;LX/0Or;Lcom/facebook/common/perftest/PerfTestConfig;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)Lcom/facebook/location/BaseFbLocationManager;
    .locals 7
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/location/IsForceAndroidPlatformImplEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1wY;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "LX/0Or",
            "<",
            "LX/2vg;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6Z8;",
            ">;",
            "LX/0Or",
            "<",
            "LX/35T;",
            ">;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")",
            "Lcom/facebook/location/FbLocationManager;"
        }
    .end annotation

    .prologue
    .line 478351
    sget-object v6, LX/2vd;->a:[I

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    move-object v3, p6

    move-object v4, p7

    move-object v5, p8

    invoke-static/range {v0 .. v5}, LX/2vc;->a(LX/0Or;LX/0yH;LX/1wY;Lcom/facebook/common/perftest/PerfTestConfig;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;)LX/1wZ;

    move-result-object v0

    invoke-virtual {v0}, LX/1wZ;->ordinal()I

    move-result v0

    aget v0, v6, v0

    packed-switch v0, :pswitch_data_0

    .line 478352
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 478353
    :pswitch_0
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/BaseFbLocationManager;

    .line 478354
    :goto_0
    return-object v0

    .line 478355
    :pswitch_1
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/BaseFbLocationManager;

    goto :goto_0

    .line 478356
    :pswitch_2
    invoke-interface {p5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/BaseFbLocationManager;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/1wY;LX/03V;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 478357
    :try_start_0
    invoke-virtual {p0}, LX/1wY;->a()I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 478358
    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 478359
    :cond_0
    :goto_0
    return v0

    .line 478360
    :catch_0
    move-exception v1

    .line 478361
    invoke-static {v1}, LX/3KX;->a(Ljava/lang/RuntimeException;)V

    .line 478362
    const-string v2, "location_manager_provider"

    const-string v3, "GooglePlayServicesUtil error"

    invoke-virtual {p1, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
