.class public LX/3QZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Pw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/3QZ;


# instance fields
.field private final b:LX/2bV;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 565279
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/notifications/constants/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_READ:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_SEEN:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FBNS_NOTIFICATIONS_SYNC:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/3QZ;->a:LX/2QP;

    return-void
.end method

.method public constructor <init>(LX/2bV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 565280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565281
    iput-object p1, p0, LX/3QZ;->b:LX/2bV;

    .line 565282
    return-void
.end method

.method public static a(LX/0QB;)LX/3QZ;
    .locals 4

    .prologue
    .line 565283
    sget-object v0, LX/3QZ;->c:LX/3QZ;

    if-nez v0, :cond_1

    .line 565284
    const-class v1, LX/3QZ;

    monitor-enter v1

    .line 565285
    :try_start_0
    sget-object v0, LX/3QZ;->c:LX/3QZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 565286
    if-eqz v2, :cond_0

    .line 565287
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 565288
    new-instance p0, LX/3QZ;

    invoke-static {v0}, LX/2bV;->a(LX/0QB;)LX/2bV;

    move-result-object v3

    check-cast v3, LX/2bV;

    invoke-direct {p0, v3}, LX/3QZ;-><init>(LX/2bV;)V

    .line 565289
    move-object v0, p0

    .line 565290
    sput-object v0, LX/3QZ;->c:LX/3QZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 565291
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 565292
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565293
    :cond_1
    sget-object v0, LX/3QZ;->c:LX/3QZ;

    return-object v0

    .line 565294
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 565295
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 565296
    sget-object v0, LX/3QZ;->a:LX/2QP;

    return-object v0
.end method

.method public final a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 3

    .prologue
    .line 565297
    const-string v0, "type"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 565298
    const-string v1, "params"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 565299
    if-nez v1, :cond_1

    .line 565300
    :cond_0
    :goto_0
    return-void

    .line 565301
    :cond_1
    const-string v2, "payload"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 565302
    if-eqz v1, :cond_0

    .line 565303
    iget-object v2, p0, LX/3QZ;->b:LX/2bV;

    invoke-virtual {v2, v0, v1}, LX/2bV;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
