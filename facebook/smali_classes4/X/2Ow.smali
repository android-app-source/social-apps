.class public LX/2Ow;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2Ow;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Bae;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DdY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/Bae;",
            ">;",
            "LX/0Or",
            "<",
            "LX/DdY;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 403555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403556
    iput-object p1, p0, LX/2Ow;->a:Landroid/content/Context;

    .line 403557
    iput-object p2, p0, LX/2Ow;->b:LX/0Or;

    .line 403558
    iput-object p3, p0, LX/2Ow;->c:LX/0Or;

    .line 403559
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ow;
    .locals 6

    .prologue
    .line 403560
    sget-object v0, LX/2Ow;->d:LX/2Ow;

    if-nez v0, :cond_1

    .line 403561
    const-class v1, LX/2Ow;

    monitor-enter v1

    .line 403562
    :try_start_0
    sget-object v0, LX/2Ow;->d:LX/2Ow;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 403563
    if-eqz v2, :cond_0

    .line 403564
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 403565
    new-instance v4, LX/2Ow;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v5, 0x17f2

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x26f1

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/2Ow;-><init>(Landroid/content/Context;LX/0Or;LX/0Or;)V

    .line 403566
    move-object v0, v4

    .line 403567
    sput-object v0, LX/2Ow;->d:LX/2Ow;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403568
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 403569
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 403570
    :cond_1
    sget-object v0, LX/2Ow;->d:LX/2Ow;

    return-object v0

    .line 403571
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 403572
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/DdH;Lcom/facebook/messaging/model/threadkey/ThreadKey;JLcom/facebook/fbtrace/FbTraceNode;J)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 403573
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 403574
    const-string v1, "broadcast_cause"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 403575
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-ltz v1, :cond_0

    .line 403576
    const-string v1, "sound_trigger_identifier"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LX/DdH;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 403577
    :cond_0
    const-string v1, "fbtrace_node"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 403578
    const-string v1, "sequence_id"

    invoke-virtual {v0, v1, p5, p6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 403579
    return-object v0
.end method

.method public static a(LX/DdH;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 403580
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 403581
    const-string v1, "broadcast_cause"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 403582
    if-eqz p1, :cond_0

    .line 403583
    const-string v1, "sound_trigger_identifier"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LX/DdH;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 403584
    :cond_0
    return-object v0
.end method

.method public static final a(Lcom/facebook/messaging/model/threads/ThreadSummary;JLcom/facebook/fbtrace/FbTraceNode;)Landroid/os/Bundle;
    .locals 8

    .prologue
    .line 403585
    sget-object v0, LX/DdH;->DELIVERY_RECEIPT:LX/DdH;

    iget-object v1, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v5, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    move-wide v2, p1

    move-object v4, p3

    invoke-static/range {v0 .. v6}, LX/2Ow;->a(LX/DdH;Lcom/facebook/messaging/model/threadkey/ThreadKey;JLcom/facebook/fbtrace/FbTraceNode;J)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2Ow;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 403586
    iget-object v0, p0, LX/2Ow;->a:Landroid/content/Context;

    invoke-static {v0}, LX/0Xp;->a(Landroid/content/Context;)LX/0Xp;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 403587
    iget-object v0, p0, LX/2Ow;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bae;

    iget-object v1, p0, LX/2Ow;->a:Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 403588
    return-void
.end method

.method public static a(LX/2Ow;Ljava/lang/String;Ljava/util/ArrayList;J)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 403604
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 403605
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 403606
    const-string v1, "multiple_thread_keys"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 403607
    const-wide/16 v2, -0x1

    cmp-long v1, p3, v2

    if-eqz v1, :cond_0

    .line 403608
    const-string v1, "action_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 403609
    :cond_0
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403610
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 403589
    const-wide/16 v0, -0x1

    invoke-static {p0, p1, p2, v0, v1}, LX/2Ow;->a(LX/2Ow;Ljava/lang/String;Ljava/util/ArrayList;J)V

    .line 403590
    return-void
.end method

.method private static e(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 403591
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 403592
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 403593
    sget-object v1, LX/0aY;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 403594
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403595
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 403596
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/0aY;->r:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 403597
    const-string v1, "EXTRA_BADGE_COUNT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 403598
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403599
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 403600
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/0aY;->t:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 403601
    const-string v1, "UPDATE_TIME_MS"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 403602
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403603
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 403546
    sget-object v0, LX/0aY;->c:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0, v1}, LX/2Ow;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 403547
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 403548
    iget-boolean v0, p1, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-eqz v0, :cond_0

    .line 403549
    :goto_0
    return-void

    .line 403550
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 403551
    sget-object v1, LX/0aY;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 403552
    const-string v1, "message_id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 403553
    const-string v1, "offline_threading_id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 403554
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 1

    .prologue
    .line 403483
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2Ow;->a(LX/0Px;)V

    .line 403484
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 403485
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 403486
    sget-object v3, LX/0aY;->c:Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-wide/16 v5, -0x1

    move-object v2, p0

    move-object v7, p2

    .line 403487
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 403488
    invoke-virtual {v8, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 403489
    const-string v9, "multiple_thread_keys"

    invoke-virtual {v8, v9, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 403490
    const-wide/16 v10, -0x1

    cmp-long v9, v5, v10

    if-eqz v9, :cond_0

    .line 403491
    const-string v9, "action_id"

    invoke-virtual {v8, v9, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 403492
    :cond_0
    if-eqz v7, :cond_1

    .line 403493
    const-string v9, "broadcast_extras"

    invoke-virtual {v8, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 403494
    :cond_1
    invoke-static {v2, v8}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403495
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 403496
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 403497
    sget-object v1, LX/0aY;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 403498
    const-string v1, "thread_key"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 403499
    const-string v1, "offline_threading_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 403500
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403501
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 403502
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 403503
    sget-object v1, LX/0aY;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 403504
    const-string v1, "thread_key"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 403505
    const-string v1, "message_ids"

    invoke-static {p2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 403506
    const-string v1, "offline_threading_ids"

    invoke-static {p3}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 403507
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403508
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 403509
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/0aY;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 403510
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403511
    return-void
.end method

.method public final b(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 403512
    sget-object v0, LX/0aY;->d:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0, v1}, LX/2Ow;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 403513
    return-void
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 403514
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 403515
    sget-object v1, LX/0aY;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 403516
    const-string v1, "thread_key"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 403517
    const-string v1, "offline_threading_id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 403518
    const-string v1, "is_sent_payment_message"

    invoke-static {p1}, LX/2Ow;->e(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 403519
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403520
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 403521
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/0aY;->s:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 403522
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403523
    return-void
.end method

.method public final c(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 403524
    sget-object v0, LX/0aY;->e:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0, v1}, LX/2Ow;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 403525
    return-void
.end method

.method public final c(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 403526
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 403527
    sget-object v1, LX/0aY;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 403528
    const-string v1, "thread_key"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 403529
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v1, v1, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 403530
    const-string v1, "error_message"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v2, v2, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 403531
    :cond_0
    const-string v1, "error_number"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget v2, v2, Lcom/facebook/messaging/model/send/SendError;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 403532
    if-eqz p1, :cond_1

    .line 403533
    const-string v1, "message_id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 403534
    const-string v1, "offline_threading_id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 403535
    const-string v1, "is_sent_payment_message"

    invoke-static {p1}, LX/2Ow;->e(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 403536
    :cond_1
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403537
    return-void
.end method

.method public final c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 1

    .prologue
    .line 403538
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2Ow;->b(LX/0Px;)V

    .line 403539
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 403540
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, LX/2Ow;->a(J)V

    .line 403541
    return-void
.end method

.method public final e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 2

    .prologue
    .line 403542
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/0aY;->M:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 403543
    const-string v1, "thread_key"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 403544
    invoke-static {p0, v0}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 403545
    return-void
.end method
