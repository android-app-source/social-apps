.class public final LX/3O3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3O4;


# instance fields
.field public final synthetic a:LX/3Na;


# direct methods
.method public constructor <init>(LX/3Na;)V
    .locals 0

    .prologue
    .line 560098
    iput-object p1, p0, LX/3O3;->a:LX/3Na;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/3OQ;I)V
    .locals 8

    .prologue
    .line 560099
    iget-object v0, p0, LX/3O3;->a:LX/3Na;

    .line 560100
    iget-object v1, v0, LX/3Na;->g:LX/3LU;

    if-eqz v1, :cond_1

    .line 560101
    instance-of v1, p1, LX/3OO;

    if-eqz v1, :cond_3

    .line 560102
    iget-object v1, v0, LX/3Na;->f:LX/2lE;

    sget-object v2, LX/3zK;->DIVEBAR:LX/3zK;

    invoke-virtual {v1, v2}, LX/2lE;->a(LX/3zK;)V

    move-object v4, p1

    .line 560103
    check-cast v4, LX/3OO;

    .line 560104
    invoke-static {v0, p1}, LX/3Na;->setLastNavigationTapPoint(LX/3Na;LX/3OQ;)V

    .line 560105
    iget-object v1, v0, LX/3Na;->g:LX/3LU;

    .line 560106
    iget-object v2, v4, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v2, v2

    .line 560107
    iget-object v3, v0, LX/3Na;->a:LX/3Nb;

    .line 560108
    sget-object v5, LX/3Nd;->FILTERED:LX/3Nd;

    iget-object v6, v3, LX/3Nc;->e:LX/3Nd;

    if-eq v5, v6, :cond_0

    sget-object v5, LX/3Nd;->FILTERING:LX/3Nd;

    iget-object v6, v3, LX/3Nc;->e:LX/3Nd;

    if-ne v5, v6, :cond_4

    :cond_0
    const/4 v5, 0x1

    :goto_0
    move v3, v5

    .line 560109
    iget-object v5, v4, LX/3OO;->r:LX/3OI;

    move-object v4, v5

    .line 560110
    sget-object v5, LX/3OH;->NEARBY_FRIENDS:LX/3OH;

    if-ne v4, v5, :cond_2

    const/4 v4, 0x1

    :goto_1
    const-string v6, "divebar"

    move-object v5, p1

    move v7, p2

    invoke-virtual/range {v1 .. v7}, LX/3LU;->a(Lcom/facebook/user/model/User;ZZLX/3OQ;Ljava/lang/String;I)Z

    .line 560111
    :cond_1
    :goto_2
    return-void

    .line 560112
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 560113
    :cond_3
    instance-of v1, p1, LX/DAU;

    if-eqz v1, :cond_1

    move-object v1, p1

    .line 560114
    check-cast v1, LX/DAU;

    .line 560115
    iget-object v2, v0, LX/3Na;->g:LX/3LU;

    .line 560116
    iget-object v3, v1, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v1, v3

    .line 560117
    const-string v3, "divebar"

    invoke-virtual {v2, v1, p1, p2, v3}, LX/3LU;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/3OQ;ILjava/lang/String;)Z

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    goto :goto_0
.end method
