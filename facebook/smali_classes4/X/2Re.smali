.class public LX/2Re;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/text/BreakIterator;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409971
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/BreakIterator;->getWordInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    move-result-object v0

    iput-object v0, p0, LX/2Re;->a:Ljava/text/BreakIterator;

    .line 409972
    return-void
.end method

.method public static a(LX/2Re;Ljava/lang/String;Z)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 409973
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 409974
    iget-object v0, p0, LX/2Re;->a:Ljava/text/BreakIterator;

    invoke-virtual {v0, p1}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 409975
    iget-object v0, p0, LX/2Re;->a:Ljava/text/BreakIterator;

    invoke-virtual {v0}, Ljava/text/BreakIterator;->first()I

    move-result v1

    .line 409976
    iget-object v0, p0, LX/2Re;->a:Ljava/text/BreakIterator;

    invoke-virtual {v0}, Ljava/text/BreakIterator;->next()I

    move-result v0

    move v4, v0

    move v0, v1

    move v1, v4

    .line 409977
    :goto_0
    const/4 v3, -0x1

    if-eq v1, v3, :cond_2

    .line 409978
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 409979
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 409980
    if-eqz p2, :cond_0

    .line 409981
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 409982
    :cond_0
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 409983
    :cond_1
    iget-object v0, p0, LX/2Re;->a:Ljava/text/BreakIterator;

    invoke-virtual {v0}, Ljava/text/BreakIterator;->next()I

    move-result v0

    move v4, v0

    move v0, v1

    move v1, v4

    goto :goto_0

    .line 409984
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/2Re;
    .locals 1

    .prologue
    .line 409967
    new-instance v0, LX/2Re;

    invoke-direct {v0}, LX/2Re;-><init>()V

    .line 409968
    move-object v0, v0

    .line 409969
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 409966
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/2Re;->a(LX/2Re;Ljava/lang/String;Z)LX/0Px;

    move-result-object v0

    return-object v0
.end method
