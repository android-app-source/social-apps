.class public LX/2sx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 474033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 59

    .prologue
    .line 474034
    const/16 v51, 0x0

    .line 474035
    const/16 v50, 0x0

    .line 474036
    const/16 v49, 0x0

    .line 474037
    const/16 v48, 0x0

    .line 474038
    const/16 v47, 0x0

    .line 474039
    const/16 v46, 0x0

    .line 474040
    const/16 v45, 0x0

    .line 474041
    const/16 v44, 0x0

    .line 474042
    const/16 v43, 0x0

    .line 474043
    const/16 v42, 0x0

    .line 474044
    const-wide/16 v40, 0x0

    .line 474045
    const/16 v39, 0x0

    .line 474046
    const/16 v38, 0x0

    .line 474047
    const/16 v37, 0x0

    .line 474048
    const/16 v36, 0x0

    .line 474049
    const/16 v35, 0x0

    .line 474050
    const/16 v34, 0x0

    .line 474051
    const/16 v33, 0x0

    .line 474052
    const/16 v32, 0x0

    .line 474053
    const/16 v31, 0x0

    .line 474054
    const/16 v30, 0x0

    .line 474055
    const/16 v29, 0x0

    .line 474056
    const/16 v28, 0x0

    .line 474057
    const/16 v27, 0x0

    .line 474058
    const/16 v26, 0x0

    .line 474059
    const/16 v25, 0x0

    .line 474060
    const/16 v24, 0x0

    .line 474061
    const/16 v23, 0x0

    .line 474062
    const/16 v22, 0x0

    .line 474063
    const/16 v21, 0x0

    .line 474064
    const/16 v20, 0x0

    .line 474065
    const/16 v19, 0x0

    .line 474066
    const/16 v18, 0x0

    .line 474067
    const/16 v17, 0x0

    .line 474068
    const/16 v16, 0x0

    .line 474069
    const/4 v15, 0x0

    .line 474070
    const/4 v14, 0x0

    .line 474071
    const/4 v13, 0x0

    .line 474072
    const/4 v12, 0x0

    .line 474073
    const/4 v11, 0x0

    .line 474074
    const/4 v10, 0x0

    .line 474075
    const/4 v9, 0x0

    .line 474076
    const/4 v8, 0x0

    .line 474077
    const/4 v7, 0x0

    .line 474078
    const/4 v6, 0x0

    .line 474079
    const/4 v5, 0x0

    .line 474080
    const/4 v4, 0x0

    .line 474081
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v52

    sget-object v53, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    if-eq v0, v1, :cond_31

    .line 474082
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 474083
    const/4 v4, 0x0

    .line 474084
    :goto_0
    return v4

    .line 474085
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v53, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v53

    if-eq v4, v0, :cond_23

    .line 474086
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 474087
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 474088
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v53

    sget-object v54, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    if-eq v0, v1, :cond_0

    if-eqz v4, :cond_0

    .line 474089
    const-string v53, "approximate_position"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_1

    .line 474090
    const/4 v4, 0x1

    .line 474091
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    move/from16 v52, v19

    move/from16 v19, v4

    goto :goto_1

    .line 474092
    :cond_1
    const-string v53, "attached_story"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_2

    .line 474093
    invoke-static/range {p0 .. p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v51, v4

    goto :goto_1

    .line 474094
    :cond_2
    const-string v53, "attachments"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_3

    .line 474095
    invoke-static/range {p0 .. p1}, LX/2as;->b(LX/15w;LX/186;)I

    move-result v4

    move/from16 v50, v4

    goto :goto_1

    .line 474096
    :cond_3
    const-string v53, "author"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_4

    .line 474097
    invoke-static/range {p0 .. p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v49, v4

    goto :goto_1

    .line 474098
    :cond_4
    const-string v53, "body"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_5

    .line 474099
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v48, v4

    goto :goto_1

    .line 474100
    :cond_5
    const-string v53, "body_markdown_html"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_6

    .line 474101
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v47, v4

    goto/16 :goto_1

    .line 474102
    :cond_6
    const-string v53, "can_viewer_delete"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_7

    .line 474103
    const/4 v4, 0x1

    .line 474104
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v46, v9

    move v9, v4

    goto/16 :goto_1

    .line 474105
    :cond_7
    const-string v53, "can_viewer_edit"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_8

    .line 474106
    const/4 v4, 0x1

    .line 474107
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move/from16 v45, v8

    move v8, v4

    goto/16 :goto_1

    .line 474108
    :cond_8
    const-string v53, "comment_parent"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_9

    .line 474109
    invoke-static/range {p0 .. p1}, LX/2sx;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v44, v4

    goto/16 :goto_1

    .line 474110
    :cond_9
    const-string v53, "constituent_title"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_a

    .line 474111
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v43, v4

    goto/16 :goto_1

    .line 474112
    :cond_a
    const-string v53, "created_time"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_b

    .line 474113
    const/4 v4, 0x1

    .line 474114
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v5, v4

    goto/16 :goto_1

    .line 474115
    :cond_b
    const-string v53, "edit_history"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_c

    .line 474116
    invoke-static/range {p0 .. p1}, LX/2bf;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v42, v4

    goto/16 :goto_1

    .line 474117
    :cond_c
    const-string v53, "feedback"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_d

    .line 474118
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v41, v4

    goto/16 :goto_1

    .line 474119
    :cond_d
    const-string v53, "id"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_e

    .line 474120
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v40, v4

    goto/16 :goto_1

    .line 474121
    :cond_e
    const-string v53, "interesting_replies"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_f

    .line 474122
    invoke-static/range {p0 .. p1}, LX/3b8;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v39, v4

    goto/16 :goto_1

    .line 474123
    :cond_f
    const-string v53, "is_featured"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_10

    .line 474124
    const/4 v4, 0x1

    .line 474125
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    move/from16 v38, v18

    move/from16 v18, v4

    goto/16 :goto_1

    .line 474126
    :cond_10
    const-string v53, "is_marked_as_spam"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_11

    .line 474127
    const/4 v4, 0x1

    .line 474128
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    move/from16 v37, v17

    move/from16 v17, v4

    goto/16 :goto_1

    .line 474129
    :cond_11
    const-string v53, "is_pinned"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_12

    .line 474130
    const/4 v4, 0x1

    .line 474131
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    move/from16 v36, v16

    move/from16 v16, v4

    goto/16 :goto_1

    .line 474132
    :cond_12
    const-string v53, "parent_feedback"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_13

    .line 474133
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v35, v4

    goto/16 :goto_1

    .line 474134
    :cond_13
    const-string v53, "permalink_title"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_14

    .line 474135
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v34, v4

    goto/16 :goto_1

    .line 474136
    :cond_14
    const-string v53, "private_reply_context"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_15

    .line 474137
    invoke-static/range {p0 .. p1}, LX/4Rg;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v33, v4

    goto/16 :goto_1

    .line 474138
    :cond_15
    const-string v53, "request_id"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_16

    .line 474139
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v32, v4

    goto/16 :goto_1

    .line 474140
    :cond_16
    const-string v53, "sort_key"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_17

    .line 474141
    const/4 v4, 0x1

    .line 474142
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    move/from16 v31, v15

    move v15, v4

    goto/16 :goto_1

    .line 474143
    :cond_17
    const-string v53, "timestamp_in_video"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_18

    .line 474144
    const/4 v4, 0x1

    .line 474145
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    move/from16 v30, v14

    move v14, v4

    goto/16 :goto_1

    .line 474146
    :cond_18
    const-string v53, "translatability_for_viewer"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_19

    .line 474147
    invoke-static/range {p0 .. p1}, LX/2be;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v29, v4

    goto/16 :goto_1

    .line 474148
    :cond_19
    const-string v53, "translated_body_for_viewer"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_1a

    .line 474149
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v28, v4

    goto/16 :goto_1

    .line 474150
    :cond_1a
    const-string v53, "url"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_1b

    .line 474151
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v27, v4

    goto/16 :goto_1

    .line 474152
    :cond_1b
    const-string v53, "written_while_video_was_live"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_1c

    .line 474153
    const/4 v4, 0x1

    .line 474154
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    move/from16 v26, v13

    move v13, v4

    goto/16 :goto_1

    .line 474155
    :cond_1c
    const-string v53, "legacy_fbid"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_1d

    .line 474156
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v25, v4

    goto/16 :goto_1

    .line 474157
    :cond_1d
    const-string v53, "can_edit_constituent_title"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_1e

    .line 474158
    const/4 v4, 0x1

    .line 474159
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v24, v12

    move v12, v4

    goto/16 :goto_1

    .line 474160
    :cond_1e
    const-string v53, "rapid_reporting_prompt"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_1f

    .line 474161
    invoke-static/range {p0 .. p1}, LX/2bZ;->a(LX/15w;LX/186;)I

    move-result v4

    move/from16 v23, v4

    goto/16 :goto_1

    .line 474162
    :cond_1f
    const-string v53, "live_streaming_comment_priority"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_20

    .line 474163
    const/4 v4, 0x1

    .line 474164
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v22, v11

    move v11, v4

    goto/16 :goto_1

    .line 474165
    :cond_20
    const-string v53, "spam_display_mode"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v53

    if-eqz v53, :cond_21

    .line 474166
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move/from16 v21, v4

    goto/16 :goto_1

    .line 474167
    :cond_21
    const-string v53, "can_viewer_share"

    move-object/from16 v0, v53

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 474168
    const/4 v4, 0x1

    .line 474169
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v20, v10

    move v10, v4

    goto/16 :goto_1

    .line 474170
    :cond_22
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 474171
    :cond_23
    const/16 v4, 0x24

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 474172
    if-eqz v19, :cond_24

    .line 474173
    const/4 v4, 0x1

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v52

    move/from16 v2, v19

    invoke-virtual {v0, v4, v1, v2}, LX/186;->a(III)V

    .line 474174
    :cond_24
    const/4 v4, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474175
    const/4 v4, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474176
    const/4 v4, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474177
    const/4 v4, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474178
    const/4 v4, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474179
    if-eqz v9, :cond_25

    .line 474180
    const/4 v4, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 474181
    :cond_25
    if-eqz v8, :cond_26

    .line 474182
    const/16 v4, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 474183
    :cond_26
    const/16 v4, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474184
    const/16 v4, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474185
    if-eqz v5, :cond_27

    .line 474186
    const/16 v5, 0xb

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 474187
    :cond_27
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474188
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474189
    const/16 v4, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474190
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474191
    if-eqz v18, :cond_28

    .line 474192
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 474193
    :cond_28
    if-eqz v17, :cond_29

    .line 474194
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 474195
    :cond_29
    if-eqz v16, :cond_2a

    .line 474196
    const/16 v4, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 474197
    :cond_2a
    const/16 v4, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474198
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474199
    const/16 v4, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474200
    const/16 v4, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474201
    if-eqz v15, :cond_2b

    .line 474202
    const/16 v4, 0x17

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 474203
    :cond_2b
    if-eqz v14, :cond_2c

    .line 474204
    const/16 v4, 0x18

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 474205
    :cond_2c
    const/16 v4, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474206
    const/16 v4, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474207
    const/16 v4, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474208
    if-eqz v13, :cond_2d

    .line 474209
    const/16 v4, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 474210
    :cond_2d
    const/16 v4, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474211
    if-eqz v12, :cond_2e

    .line 474212
    const/16 v4, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 474213
    :cond_2e
    const/16 v4, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474214
    if-eqz v11, :cond_2f

    .line 474215
    const/16 v4, 0x20

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 474216
    :cond_2f
    const/16 v4, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 474217
    if-eqz v10, :cond_30

    .line 474218
    const/16 v4, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 474219
    :cond_30
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_31
    move/from16 v52, v51

    move/from16 v51, v50

    move/from16 v50, v49

    move/from16 v49, v48

    move/from16 v48, v47

    move/from16 v47, v46

    move/from16 v46, v45

    move/from16 v45, v44

    move/from16 v44, v43

    move/from16 v43, v42

    move/from16 v42, v39

    move/from16 v39, v36

    move/from16 v36, v33

    move/from16 v33, v30

    move/from16 v30, v27

    move/from16 v27, v24

    move/from16 v24, v21

    move/from16 v21, v18

    move/from16 v18, v12

    move v12, v6

    move/from16 v55, v19

    move/from16 v19, v16

    move/from16 v16, v10

    move v10, v4

    move/from16 v56, v20

    move/from16 v20, v17

    move/from16 v17, v11

    move v11, v5

    move v5, v13

    move v13, v7

    move-wide/from16 v6, v40

    move/from16 v40, v37

    move/from16 v41, v38

    move/from16 v38, v35

    move/from16 v37, v34

    move/from16 v35, v32

    move/from16 v34, v31

    move/from16 v31, v28

    move/from16 v32, v29

    move/from16 v28, v25

    move/from16 v29, v26

    move/from16 v26, v23

    move/from16 v25, v22

    move/from16 v22, v55

    move/from16 v23, v56

    move/from16 v57, v15

    move v15, v9

    move/from16 v9, v57

    move/from16 v58, v14

    move v14, v8

    move/from16 v8, v58

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 474220
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 474221
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 474222
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/2sx;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 474223
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 474224
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 474225
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 474226
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 474227
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 474228
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 474229
    invoke-static {p0, p1}, LX/2sx;->a(LX/15w;LX/186;)I

    move-result v1

    .line 474230
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 474231
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 474232
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 474233
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 474234
    if-eqz v0, :cond_0

    .line 474235
    const-string v1, "approximate_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474236
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 474237
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474238
    if-eqz v0, :cond_1

    .line 474239
    const-string v1, "attached_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474240
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 474241
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474242
    if-eqz v0, :cond_2

    .line 474243
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474244
    invoke-static {p0, v0, p2, p3}, LX/2as;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 474245
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474246
    if-eqz v0, :cond_3

    .line 474247
    const-string v1, "author"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474248
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 474249
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474250
    if-eqz v0, :cond_4

    .line 474251
    const-string v1, "body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474252
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 474253
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474254
    if-eqz v0, :cond_5

    .line 474255
    const-string v1, "body_markdown_html"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474256
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 474257
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 474258
    if-eqz v0, :cond_6

    .line 474259
    const-string v1, "can_viewer_delete"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474260
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 474261
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 474262
    if-eqz v0, :cond_7

    .line 474263
    const-string v1, "can_viewer_edit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474264
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 474265
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474266
    if-eqz v0, :cond_8

    .line 474267
    const-string v1, "comment_parent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474268
    invoke-static {p0, v0, p2, p3}, LX/2sx;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 474269
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 474270
    if-eqz v0, :cond_9

    .line 474271
    const-string v1, "constituent_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474272
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 474273
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 474274
    cmp-long v2, v0, v4

    if-eqz v2, :cond_a

    .line 474275
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474276
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 474277
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474278
    if-eqz v0, :cond_b

    .line 474279
    const-string v1, "edit_history"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474280
    invoke-static {p0, v0, p2, p3}, LX/2bf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 474281
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474282
    if-eqz v0, :cond_c

    .line 474283
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474284
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 474285
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 474286
    if-eqz v0, :cond_d

    .line 474287
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474288
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 474289
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474290
    if-eqz v0, :cond_e

    .line 474291
    const-string v1, "interesting_replies"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474292
    invoke-static {p0, v0, p2, p3}, LX/3b8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 474293
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 474294
    if-eqz v0, :cond_f

    .line 474295
    const-string v1, "is_featured"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474296
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 474297
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 474298
    if-eqz v0, :cond_10

    .line 474299
    const-string v1, "is_marked_as_spam"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474300
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 474301
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 474302
    if-eqz v0, :cond_11

    .line 474303
    const-string v1, "is_pinned"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474304
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 474305
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474306
    if-eqz v0, :cond_12

    .line 474307
    const-string v1, "parent_feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474308
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 474309
    :cond_12
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474310
    if-eqz v0, :cond_13

    .line 474311
    const-string v1, "permalink_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474312
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 474313
    :cond_13
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474314
    if-eqz v0, :cond_14

    .line 474315
    const-string v1, "private_reply_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474316
    invoke-static {p0, v0, p2, p3}, LX/4Rg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 474317
    :cond_14
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 474318
    if-eqz v0, :cond_15

    .line 474319
    const-string v1, "request_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474320
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 474321
    :cond_15
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 474322
    if-eqz v0, :cond_16

    .line 474323
    const-string v1, "sort_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474324
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 474325
    :cond_16
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 474326
    if-eqz v0, :cond_17

    .line 474327
    const-string v1, "timestamp_in_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474328
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 474329
    :cond_17
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474330
    if-eqz v0, :cond_18

    .line 474331
    const-string v1, "translatability_for_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474332
    invoke-static {p0, v0, p2, p3}, LX/2be;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 474333
    :cond_18
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474334
    if-eqz v0, :cond_19

    .line 474335
    const-string v1, "translated_body_for_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474336
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 474337
    :cond_19
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 474338
    if-eqz v0, :cond_1a

    .line 474339
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474340
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 474341
    :cond_1a
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 474342
    if-eqz v0, :cond_1b

    .line 474343
    const-string v1, "written_while_video_was_live"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474344
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 474345
    :cond_1b
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 474346
    if-eqz v0, :cond_1c

    .line 474347
    const-string v1, "legacy_fbid"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474348
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 474349
    :cond_1c
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 474350
    if-eqz v0, :cond_1d

    .line 474351
    const-string v1, "can_edit_constituent_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474352
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 474353
    :cond_1d
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 474354
    if-eqz v0, :cond_1e

    .line 474355
    const-string v1, "rapid_reporting_prompt"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474356
    invoke-static {p0, v0, p2, p3}, LX/2bZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 474357
    :cond_1e
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 474358
    if-eqz v0, :cond_1f

    .line 474359
    const-string v1, "live_streaming_comment_priority"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474360
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 474361
    :cond_1f
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 474362
    if-eqz v0, :cond_20

    .line 474363
    const-string v1, "spam_display_mode"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474364
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 474365
    :cond_20
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 474366
    if-eqz v0, :cond_21

    .line 474367
    const-string v1, "can_viewer_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 474368
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 474369
    :cond_21
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 474370
    return-void
.end method
