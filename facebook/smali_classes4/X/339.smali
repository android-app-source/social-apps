.class public LX/339;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile n:LX/339;


# instance fields
.field public a:Ljava/lang/String;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/33A;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field private i:Z

.field public j:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public l:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public m:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/2by;",
            "LX/2c0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/33A;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/33A;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 491623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491624
    iput-object p1, p0, LX/339;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 491625
    iput-object p2, p0, LX/339;->c:LX/33A;

    .line 491626
    iput-object p3, p0, LX/339;->d:LX/0Ot;

    .line 491627
    iput-object p4, p0, LX/339;->e:LX/0Ot;

    .line 491628
    iput-object p5, p0, LX/339;->f:LX/0Ot;

    .line 491629
    iput-boolean v3, p0, LX/339;->g:Z

    .line 491630
    iput-boolean v3, p0, LX/339;->h:Z

    .line 491631
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, LX/2by;->AUDIENCE_ALIGNMENT_EDUCATOR:LX/2by;

    new-instance v2, LX/2bz;

    invoke-direct {v2, p0}, LX/2bz;-><init>(LX/339;)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2by;->AUDIENCE_ALIGNMENT_ONLY_ME_EDUCATOR:LX/2by;

    new-instance v2, LX/2bH;

    invoke-direct {v2, p0}, LX/2bH;-><init>(LX/339;)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/2by;->NEWCOMER_AUDIENCE_EDUCATOR:LX/2by;

    new-instance v2, LX/2c1;

    invoke-direct {v2, p0}, LX/2c1;-><init>(LX/339;)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/339;->m:LX/0P1;

    .line 491632
    return-void
.end method

.method public static a(LX/0QB;)LX/339;
    .locals 9

    .prologue
    .line 491655
    sget-object v0, LX/339;->n:LX/339;

    if-nez v0, :cond_1

    .line 491656
    const-class v1, LX/339;

    monitor-enter v1

    .line 491657
    :try_start_0
    sget-object v0, LX/339;->n:LX/339;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 491658
    if-eqz v2, :cond_0

    .line 491659
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 491660
    new-instance v3, LX/339;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/33A;->a(LX/0QB;)LX/33A;

    move-result-object v5

    check-cast v5, LX/33A;

    const/16 v6, 0x2fc7

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2e3

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x259

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/339;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/33A;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 491661
    move-object v0, v3

    .line 491662
    sput-object v0, LX/339;->n:LX/339;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491663
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 491664
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 491665
    :cond_1
    sget-object v0, LX/339;->n:LX/339;

    return-object v0

    .line 491666
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 491667
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/5nW;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 491644
    sget-object v0, LX/5nW;->EXPOSED:LX/5nW;

    if-ne p1, v0, :cond_1

    .line 491645
    iget-boolean v0, p0, LX/339;->i:Z

    if-eqz v0, :cond_0

    .line 491646
    :goto_0
    return-void

    .line 491647
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/339;->i:Z

    .line 491648
    :cond_1
    iget-object v0, p0, LX/339;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v1, p0, LX/339;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 491649
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 491650
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 491651
    const-string v6, "params"

    new-instance v7, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;

    invoke-direct {v7, p1, v2, v3, p2}, Lcom/facebook/privacy/protocol/ReportAAAOnlyMeActionParams;-><init>(LX/5nW;JLjava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 491652
    iget-object v6, v0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v7, "report_aaa_only_me_action"

    sget-object v9, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v10, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v11, 0x1861a79d

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    .line 491653
    invoke-static {v0, v6}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 491654
    goto :goto_0
.end method

.method public final a(LX/5nZ;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 491633
    sget-object v0, LX/5nZ;->EXPOSED:LX/5nZ;

    if-ne p1, v0, :cond_1

    .line 491634
    iget-boolean v0, p0, LX/339;->i:Z

    if-eqz v0, :cond_0

    .line 491635
    :goto_0
    return-void

    .line 491636
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/339;->i:Z

    .line 491637
    :cond_1
    iget-object v0, p0, LX/339;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v1, p0, LX/339;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 491638
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 491639
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 491640
    const-string v6, "params"

    new-instance v7, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;

    invoke-direct {v7, p1, v2, v3, p2}, Lcom/facebook/privacy/protocol/ReportAAATuxActionParams;-><init>(LX/5nZ;JLjava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 491641
    iget-object v6, v0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v7, "report_aaa_tux_action"

    sget-object v9, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v10, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v11, 0x7a584f7a

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    .line 491642
    invoke-static {v0, v6}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 491643
    goto :goto_0
.end method

.method public final a(LX/5nf;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 491618
    sget-object v0, LX/5nf;->EXPOSED:LX/5nf;

    if-ne p1, v0, :cond_1

    .line 491619
    iget-boolean v0, p0, LX/339;->i:Z

    if-eqz v0, :cond_0

    .line 491620
    :goto_0
    return-void

    .line 491621
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/339;->i:Z

    .line 491622
    :cond_1
    iget-object v0, p0, LX/339;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v1, p0, LX/339;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/5nf;JZLjava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;)V
    .locals 3

    .prologue
    .line 491668
    if-nez p1, :cond_0

    .line 491669
    :goto_0
    return-void

    .line 491670
    :cond_0
    iget-object v0, p0, LX/339;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2bs;->j:LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->k()Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    sget-object v1, LX/2bs;->n:LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->l()Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    sget-object v1, LX/2bs;->m:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 491614
    iget-object v0, p0, LX/339;->a:Ljava/lang/String;

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 491615
    iput-object p1, p0, LX/339;->a:Ljava/lang/String;

    .line 491616
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/339;->i:Z

    .line 491617
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;)V
    .locals 3

    .prologue
    .line 491583
    if-nez p1, :cond_0

    .line 491584
    :goto_0
    return-void

    .line 491585
    :cond_0
    invoke-virtual {p0, p1}, LX/339;->a(Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;)V

    .line 491586
    iget-object v0, p0, LX/339;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2bs;->h:LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->a()Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    sget-object v1, LX/2bs;->i:LX/0Tn;

    invoke-virtual {p1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->j()Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 491587
    iget-object v0, p0, LX/339;->c:LX/33A;

    invoke-virtual {p1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$AudienceInfoFieldsModel;->m()Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;

    move-result-object v1

    .line 491588
    if-nez v1, :cond_1

    .line 491589
    invoke-virtual {v0}, LX/33A;->b()V

    .line 491590
    :goto_1
    goto :goto_0

    .line 491591
    :cond_1
    new-instance v2, LX/3lY;

    invoke-direct {v2}, LX/3lY;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->a()Z

    move-result p0

    .line 491592
    iput-boolean p0, v2, LX/3lY;->a:Z

    .line 491593
    move-object v2, v2

    .line 491594
    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object p0

    .line 491595
    iput-object p0, v2, LX/3lY;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 491596
    move-object v2, v2

    .line 491597
    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object p0

    .line 491598
    iput-object p0, v2, LX/3lY;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 491599
    move-object v2, v2

    .line 491600
    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/FetchAudienceInfoModels$ComposerInlinePrivacySurveyFieldsModel;->k()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object p0

    .line 491601
    iput-object p0, v2, LX/3lY;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 491602
    move-object v2, v2

    .line 491603
    invoke-virtual {v2}, LX/3lY;->a()Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    move-result-object v2

    .line 491604
    invoke-static {v0, v2}, LX/33A;->a(LX/33A;Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;)V

    goto :goto_1
.end method

.method public final b(LX/2by;)Z
    .locals 1

    .prologue
    .line 491610
    iget-object v0, p0, LX/339;->m:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2c0;

    .line 491611
    if-nez v0, :cond_0

    .line 491612
    const/4 v0, 0x0

    .line 491613
    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, LX/2c0;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 491609
    iget-object v0, p0, LX/339;->a:Ljava/lang/String;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(LX/2by;)Z
    .locals 1

    .prologue
    .line 491605
    iget-object v0, p0, LX/339;->m:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2c0;

    .line 491606
    if-nez v0, :cond_0

    .line 491607
    const/4 v0, 0x0

    .line 491608
    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, LX/2c0;->b()Z

    move-result v0

    goto :goto_0
.end method
