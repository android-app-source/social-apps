.class public LX/2Gi;
.super LX/2Gj;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Gi;


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/2Cw;


# direct methods
.method public constructor <init>(LX/0Uh;LX/2Cw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 389164
    invoke-direct {p0}, LX/2Gj;-><init>()V

    .line 389165
    iput-object p1, p0, LX/2Gi;->a:LX/0Uh;

    .line 389166
    iput-object p2, p0, LX/2Gi;->b:LX/2Cw;

    .line 389167
    return-void
.end method

.method public static a(LX/0QB;)LX/2Gi;
    .locals 5

    .prologue
    .line 389146
    sget-object v0, LX/2Gi;->c:LX/2Gi;

    if-nez v0, :cond_1

    .line 389147
    const-class v1, LX/2Gi;

    monitor-enter v1

    .line 389148
    :try_start_0
    sget-object v0, LX/2Gi;->c:LX/2Gi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 389149
    if-eqz v2, :cond_0

    .line 389150
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 389151
    new-instance p0, LX/2Gi;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/2Cw;->a(LX/0QB;)LX/2Cw;

    move-result-object v4

    check-cast v4, LX/2Cw;

    invoke-direct {p0, v3, v4}, LX/2Gi;-><init>(LX/0Uh;LX/2Cw;)V

    .line 389152
    move-object v0, p0

    .line 389153
    sput-object v0, LX/2Gi;->c:LX/2Gi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 389154
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 389155
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 389156
    :cond_1
    sget-object v0, LX/2Gi;->c:LX/2Gi;

    return-object v0

    .line 389157
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 389158
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0lF;
    .locals 3

    .prologue
    .line 389161
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 389162
    const-string v1, "collecting"

    iget-object v2, p0, LX/2Gi;->b:LX/2Cw;

    invoke-virtual {v2}, LX/2Cw;->a()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 389163
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 389160
    const-string v0, "background_location"

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 389159
    iget-object v0, p0, LX/2Gi;->a:LX/0Uh;

    const/16 v1, 0x2e9

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
