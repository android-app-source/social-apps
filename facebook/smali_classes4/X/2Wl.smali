.class public final enum LX/2Wl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Wl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Wl;

.field public static final enum LOGIN_FINISH:LX/2Wl;

.field public static final enum LOGIN_START:LX/2Wl;

.field public static final enum NUX_FINISH:LX/2Wl;

.field public static final enum NUX_START:LX/2Wl;

.field public static final enum NUX_STEP_VIEW:LX/2Wl;


# instance fields
.field private final mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 419304
    new-instance v0, LX/2Wl;

    const-string v1, "NUX_STEP_VIEW"

    const-string v2, "account_nux_step_view"

    invoke-direct {v0, v1, v3, v2}, LX/2Wl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Wl;->NUX_STEP_VIEW:LX/2Wl;

    .line 419305
    new-instance v0, LX/2Wl;

    const-string v1, "LOGIN_START"

    const-string v2, "login_start"

    invoke-direct {v0, v1, v4, v2}, LX/2Wl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Wl;->LOGIN_START:LX/2Wl;

    .line 419306
    new-instance v0, LX/2Wl;

    const-string v1, "LOGIN_FINISH"

    const-string v2, "login_finish"

    invoke-direct {v0, v1, v5, v2}, LX/2Wl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Wl;->LOGIN_FINISH:LX/2Wl;

    .line 419307
    new-instance v0, LX/2Wl;

    const-string v1, "NUX_START"

    const-string v2, "nux_start"

    invoke-direct {v0, v1, v6, v2}, LX/2Wl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Wl;->NUX_START:LX/2Wl;

    .line 419308
    new-instance v0, LX/2Wl;

    const-string v1, "NUX_FINISH"

    const-string v2, "nux_finish"

    invoke-direct {v0, v1, v7, v2}, LX/2Wl;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2Wl;->NUX_FINISH:LX/2Wl;

    .line 419309
    const/4 v0, 0x5

    new-array v0, v0, [LX/2Wl;

    sget-object v1, LX/2Wl;->NUX_STEP_VIEW:LX/2Wl;

    aput-object v1, v0, v3

    sget-object v1, LX/2Wl;->LOGIN_START:LX/2Wl;

    aput-object v1, v0, v4

    sget-object v1, LX/2Wl;->LOGIN_FINISH:LX/2Wl;

    aput-object v1, v0, v5

    sget-object v1, LX/2Wl;->NUX_START:LX/2Wl;

    aput-object v1, v0, v6

    sget-object v1, LX/2Wl;->NUX_FINISH:LX/2Wl;

    aput-object v1, v0, v7

    sput-object v0, LX/2Wl;->$VALUES:[LX/2Wl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 419301
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 419302
    iput-object p3, p0, LX/2Wl;->mEventName:Ljava/lang/String;

    .line 419303
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Wl;
    .locals 1

    .prologue
    .line 419298
    const-class v0, LX/2Wl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Wl;

    return-object v0
.end method

.method public static values()[LX/2Wl;
    .locals 1

    .prologue
    .line 419300
    sget-object v0, LX/2Wl;->$VALUES:[LX/2Wl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Wl;

    return-object v0
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 419299
    iget-object v0, p0, LX/2Wl;->mEventName:Ljava/lang/String;

    return-object v0
.end method
