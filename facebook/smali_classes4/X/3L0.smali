.class public LX/3L0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3GL;

.field private final b:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(LX/3GL;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 549710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549711
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, LX/3L0;->b:Ljava/lang/StringBuilder;

    .line 549712
    iput-object p1, p0, LX/3L0;->a:LX/3GL;

    .line 549713
    return-void
.end method

.method private static a(LX/3L0;Ljava/lang/CharSequence;ZLandroid/text/TextPaint;)F
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 549714
    iget-object v0, p0, LX/3L0;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 549715
    if-eqz p2, :cond_0

    .line 549716
    iget-object v0, p0, LX/3L0;->b:Ljava/lang/StringBuilder;

    iget-object v1, p0, LX/3L0;->a:LX/3GL;

    invoke-virtual {v1}, LX/3GL;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 549717
    :goto_0
    iget-object v0, p0, LX/3L0;->b:Ljava/lang/StringBuilder;

    iget-object v1, p0, LX/3L0;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {p3, v0, v2, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    return v0

    .line 549718
    :cond_0
    iget-object v0, p0, LX/3L0;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static a([FI)F
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 549719
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 549720
    :cond_0
    const/4 v0, 0x0

    .line 549721
    :goto_0
    return v0

    .line 549722
    :cond_1
    array-length v0, p0

    if-eq v0, v1, :cond_2

    const/16 v0, 0xa

    if-ge p1, v0, :cond_3

    .line 549723
    :cond_2
    const/4 v0, 0x0

    aget v0, p0, v0

    goto :goto_0

    .line 549724
    :cond_3
    array-length v0, p0

    if-eq v0, v2, :cond_4

    const/16 v0, 0x64

    if-ge p1, v0, :cond_5

    .line 549725
    :cond_4
    aget v0, p0, v1

    goto :goto_0

    .line 549726
    :cond_5
    aget v0, p0, v2

    goto :goto_0
.end method

.method private static a(LX/3L0;Ljava/lang/String;Landroid/text/TextPaint;F)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 549727
    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "\u2026"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/3L0;->a:LX/3GL;

    invoke-virtual {v1}, LX/3GL;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 549728
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    invoke-virtual {p2, v2, v5, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v3

    .line 549729
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 549730
    invoke-virtual {p2, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 549731
    :goto_0
    cmpg-float v4, p3, v0

    if-gez v4, :cond_0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-le v1, v4, :cond_0

    .line 549732
    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointBefore(I)I

    move-result v0

    .line 549733
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v0

    sub-int/2addr v1, v0

    .line 549734
    invoke-virtual {p2, p1, v5, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v0

    add-float/2addr v0, v3

    .line 549735
    goto :goto_0

    .line 549736
    :cond_0
    cmpl-float v0, p3, v0

    if-ltz v0, :cond_1

    .line 549737
    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 549738
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(LX/3L0;Ljava/lang/StringBuilder;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 549739
    if-eqz p3, :cond_0

    .line 549740
    iget-object v0, p0, LX/3L0;->a:LX/3GL;

    invoke-virtual {v0}, LX/3GL;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549741
    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549742
    return-void
.end method

.method public static b(LX/0QB;)LX/3L0;
    .locals 2

    .prologue
    .line 549743
    new-instance v1, LX/3L0;

    invoke-static {p0}, LX/3GL;->b(LX/0QB;)LX/3GL;

    move-result-object v0

    check-cast v0, LX/3GL;

    invoke-direct {v1, v0}, LX/3L0;-><init>(LX/3GL;)V

    .line 549744
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/util/List;FIFLandroid/text/TextPaint;[F)LX/FOS;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;FIF",
            "Landroid/text/TextPaint;",
            "[F)",
            "LX/FOS;"
        }
    .end annotation

    .prologue
    .line 549745
    invoke-virtual/range {p5 .. p5}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v3

    .line 549746
    iget v4, v3, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v3, v3, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float v10, v4, v3

    .line 549747
    move/from16 v0, p4

    invoke-static {v0, v10}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 549748
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v11

    .line 549749
    invoke-static/range {p1 .. p1}, LX/FOR;->a(Ljava/util/Collection;)LX/FOR;

    move-result-object v12

    .line 549750
    const/4 v3, -0x1

    .line 549751
    move-object/from16 v0, p0

    iget-object v5, v0, LX/3L0;->a:LX/3GL;

    invoke-virtual {v5}, LX/3GL;->a()C

    move-result v13

    .line 549752
    invoke-static {v13}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v14

    .line 549753
    new-instance v15, Ljava/lang/StringBuilder;

    const/16 v5, 0x64

    invoke-direct {v15, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 549754
    :goto_0
    if-lez p3, :cond_f

    invoke-virtual {v12}, LX/FOR;->b()I

    move-result v5

    if-lez v5, :cond_f

    cmpl-float v5, v4, v10

    if-ltz v5, :cond_f

    .line 549755
    add-int/lit8 p3, p3, -0x1

    .line 549756
    sub-float v9, v4, v10

    .line 549757
    const/4 v4, 0x0

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    move/from16 v5, p2

    move v6, v3

    .line 549758
    :goto_1
    invoke-virtual {v12}, LX/FOR;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 549759
    invoke-virtual {v12}, LX/FOR;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 549760
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-static {v0, v3, v4, v1}, LX/3L0;->a(LX/3L0;Ljava/lang/CharSequence;ZLandroid/text/TextPaint;)F

    move-result v7

    .line 549761
    invoke-virtual {v12}, LX/FOR;->b()I

    move-result v4

    const/4 v8, 0x1

    if-le v4, v8, :cond_11

    if-lez p3, :cond_11

    .line 549762
    add-float v4, v7, v14

    .line 549763
    :goto_3
    if-lez p3, :cond_5

    cmpl-float v8, v9, v10

    if-ltz v8, :cond_5

    .line 549764
    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    .line 549765
    invoke-virtual {v12}, LX/FOR;->next()Ljava/lang/Object;

    .line 549766
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_1

    const/4 v4, 0x1

    :goto_4
    move-object/from16 v0, p0

    invoke-static {v0, v15, v3, v4}, LX/3L0;->a(LX/3L0;Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    move v4, v6

    .line 549767
    :goto_5
    sub-float v3, v5, v7

    move v5, v3

    move v6, v4

    .line 549768
    goto :goto_1

    .line 549769
    :cond_0
    const/4 v4, 0x0

    goto :goto_2

    .line 549770
    :cond_1
    const/4 v4, 0x0

    goto :goto_4

    .line 549771
    :cond_2
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 549772
    :cond_3
    :goto_6
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_4

    .line 549773
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    move v3, v6

    move v4, v9

    .line 549774
    goto :goto_0

    .line 549775
    :cond_5
    invoke-virtual {v12}, LX/FOR;->b()I

    move-result v4

    const/4 v8, 0x1

    if-ne v4, v8, :cond_8

    .line 549776
    cmpg-float v4, v5, v7

    if-gez v4, :cond_6

    .line 549777
    const/4 v6, 0x1

    .line 549778
    :goto_7
    invoke-virtual {v12}, LX/FOR;->next()Ljava/lang/Object;

    move v4, v6

    goto :goto_5

    .line 549779
    :cond_6
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_7

    const/4 v4, 0x1

    :goto_8
    move-object/from16 v0, p0

    invoke-static {v0, v15, v3, v4}, LX/3L0;->a(LX/3L0;Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    goto :goto_7

    :cond_7
    const/4 v4, 0x0

    goto :goto_8

    .line 549780
    :cond_8
    invoke-virtual {v12}, LX/FOR;->b()I

    move-result v8

    .line 549781
    invoke-virtual {v12}, LX/FOR;->b()I

    move-result v4

    move-object/from16 v0, p6

    invoke-static {v0, v4}, LX/3L0;->a([FI)F

    move-result v16

    .line 549782
    sub-float v4, v5, v7

    cmpg-float v4, v4, v16

    if-gez v4, :cond_d

    .line 549783
    const/4 v4, 0x5

    if-ge v8, v4, :cond_c

    .line 549784
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 549785
    :goto_9
    invoke-virtual {v12}, LX/FOR;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 549786
    invoke-virtual {v12}, LX/FOR;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 549787
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-nez v7, :cond_9

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-eqz v7, :cond_a

    :cond_9
    const/4 v7, 0x1

    :goto_a
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1, v4, v7}, LX/3L0;->a(LX/3L0;Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    goto :goto_9

    :cond_a
    const/4 v7, 0x0

    goto :goto_a

    .line 549788
    :cond_b
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, p5

    invoke-static {v0, v1, v4, v2}, LX/3L0;->a(LX/3L0;Ljava/lang/CharSequence;ZLandroid/text/TextPaint;)F

    move-result v4

    .line 549789
    cmpg-float v4, v4, v5

    if-gtz v4, :cond_c

    .line 549790
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 549791
    :cond_c
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-nez v4, :cond_10

    .line 549792
    sub-float v4, v5, v16

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-static {v0, v3, v1, v4}, LX/3L0;->a(LX/3L0;Ljava/lang/String;Landroid/text/TextPaint;F)Ljava/lang/String;

    move-result-object v3

    .line 549793
    if-eqz v3, :cond_10

    .line 549794
    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549795
    const-string v3, "\u2026"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549796
    add-int/lit8 v3, v8, -0x1

    :goto_b
    move v6, v3

    .line 549797
    goto/16 :goto_6

    .line 549798
    :cond_d
    invoke-virtual {v12}, LX/FOR;->next()Ljava/lang/Object;

    .line 549799
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_e

    const/4 v4, 0x1

    :goto_c
    move-object/from16 v0, p0

    invoke-static {v0, v15, v3, v4}, LX/3L0;->a(LX/3L0;Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    move v4, v6

    goto/16 :goto_5

    :cond_e
    const/4 v4, 0x0

    goto :goto_c

    .line 549800
    :cond_f
    new-instance v4, LX/FOS;

    invoke-direct {v4, v11, v3}, LX/FOS;-><init>(Ljava/util/List;I)V

    return-object v4

    :cond_10
    move v3, v8

    goto :goto_b

    :cond_11
    move v4, v7

    goto/16 :goto_3
.end method
