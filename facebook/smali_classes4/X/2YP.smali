.class public final LX/2YP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Wd;


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation


# instance fields
.field private final a:LX/2Wd;

.field private final b:LX/2YJ;


# direct methods
.method public constructor <init>(LX/2Wd;LX/2YJ;)V
    .locals 0

    .prologue
    .line 420885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420886
    iput-object p1, p0, LX/2YP;->a:LX/2Wd;

    .line 420887
    iput-object p2, p0, LX/2YP;->b:LX/2YJ;

    .line 420888
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 420889
    iget-object v0, p0, LX/2YP;->a:LX/2Wd;

    invoke-interface {v0}, LX/2We;->a()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 420890
    iget-object v0, p0, LX/2YP;->a:LX/2Wd;

    invoke-interface {v0, p1}, LX/2We;->a(Ljava/io/Writer;)V

    .line 420891
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 420892
    iget-object v0, p0, LX/2YP;->a:LX/2Wd;

    invoke-interface {v0}, LX/2We;->b()Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 420893
    iget-object v0, p0, LX/2YP;->a:LX/2Wd;

    invoke-interface {v0}, LX/2Wd;->c()V

    .line 420894
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 420895
    iget-object v0, p0, LX/2YP;->a:LX/2Wd;

    invoke-interface {v0}, LX/2Wd;->d()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 7

    .prologue
    .line 420896
    iget-object v0, p0, LX/2YP;->a:LX/2Wd;

    invoke-interface {v0}, LX/2Wd;->e()V

    .line 420897
    iget-object v0, p0, LX/2YP;->b:LX/2YJ;

    .line 420898
    invoke-static {}, LX/2D9;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 420899
    invoke-static {}, LX/2D9;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 420900
    const/4 v1, 0x0

    iget-object v2, v0, LX/2YJ;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    .line 420901
    iget-object v1, v0, LX/2YJ;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2YA;

    .line 420902
    instance-of v6, v1, LX/2YL;

    if-eqz v6, :cond_0

    .line 420903
    iget-object v6, v1, LX/2YB;->a:Ljava/io/File;

    move-object v1, v6

    .line 420904
    invoke-static {v1, v4}, LX/2Y6;->b(Ljava/io/File;Ljava/lang/String;)V

    .line 420905
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 420906
    :cond_0
    instance-of v6, v1, LX/2Wa;

    if-eqz v6, :cond_1

    .line 420907
    iget-object v6, v1, LX/2YB;->a:Ljava/io/File;

    move-object v1, v6

    .line 420908
    invoke-static {v1, v3}, LX/2Y6;->b(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_1

    .line 420909
    :cond_1
    iget-object v6, v1, LX/2YB;->a:Ljava/io/File;

    move-object v1, v6

    .line 420910
    invoke-static {v1}, LX/2Y6;->b(Ljava/io/File;)V

    goto :goto_1

    .line 420911
    :cond_2
    iget-object v1, v0, LX/2YJ;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 420912
    return-void
.end method
