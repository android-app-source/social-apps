.class public LX/25D;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 368495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 368496
    if-nez p0, :cond_1

    .line 368497
    :cond_0
    :goto_0
    return-object v0

    .line 368498
    :cond_1
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;

    if-eqz v1, :cond_2

    .line 368499
    check-cast p0, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-static {v0}, LX/2em;->a(Lcom/facebook/graphql/model/GraphQLUser;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto :goto_0

    .line 368500
    :cond_2
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;

    if-eqz v1, :cond_3

    .line 368501
    check-cast p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->v()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/6X4;->b(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto :goto_0

    .line 368502
    :cond_3
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    if-eqz v1, :cond_4

    .line 368503
    check-cast p0, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-static {v0}, LX/2em;->a(Lcom/facebook/graphql/model/GraphQLUser;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto :goto_0

    .line 368504
    :cond_4
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;

    if-eqz v1, :cond_5

    .line 368505
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-static {v0}, LX/3mz;->a(Lcom/facebook/graphql/model/GraphQLGroup;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto :goto_0

    .line 368506
    :cond_5
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;

    if-eqz v1, :cond_6

    .line 368507
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-static {v0}, LX/3mz;->a(Lcom/facebook/graphql/model/GraphQLGroup;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto :goto_0

    .line 368508
    :cond_6
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    if-eqz v1, :cond_7

    .line 368509
    check-cast p0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-static {v0}, LX/3mz;->a(Lcom/facebook/graphql/model/GraphQLGroup;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto :goto_0

    .line 368510
    :cond_7
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;

    if-eqz v1, :cond_8

    .line 368511
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/6X4;->b(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto :goto_0

    .line 368512
    :cond_8
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    if-eqz v1, :cond_9

    .line 368513
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-static {v0}, LX/2em;->a(Lcom/facebook/graphql/model/GraphQLUser;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto :goto_0

    .line 368514
    :cond_9
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;

    if-eqz v1, :cond_a

    .line 368515
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayKnowFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-static {v0}, LX/2em;->a(Lcom/facebook/graphql/model/GraphQLUser;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto/16 :goto_0

    .line 368516
    :cond_a
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    if-eqz v1, :cond_b

    .line 368517
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->B()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/6X4;->b(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto/16 :goto_0

    .line 368518
    :cond_b
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnitItem;

    if-eqz v1, :cond_c

    .line 368519
    check-cast p0, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnitItem;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/6X4;->b(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto/16 :goto_0

    .line 368520
    :cond_c
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    if-eqz v1, :cond_d

    .line 368521
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-static {v0}, LX/2em;->a(Lcom/facebook/graphql/model/GraphQLUser;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto/16 :goto_0

    .line 368522
    :cond_d
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_e

    .line 368523
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 368524
    if-eqz v1, :cond_0

    .line 368525
    invoke-static {v1}, LX/33N;->a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    goto/16 :goto_0

    .line 368526
    :cond_e
    const-string v1, "FeedUnitItemProfileHelper"

    const-string v2, "Should not call getProfileAsProfileForFeedUnitItem() function bypassing in non-customized feed unit item type"

    invoke-static {v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
