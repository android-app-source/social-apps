.class public abstract LX/2oa;
.super LX/0b2;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/2ol;",
        ">",
        "LX/0b2",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 467169
    invoke-direct {p0}, LX/0b2;-><init>()V

    .line 467170
    return-void
.end method

.method public constructor <init>(LX/2oy;)V
    .locals 1

    .prologue
    .line 467178
    invoke-direct {p0}, LX/0b2;-><init>()V

    .line 467179
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2oa;->a:Ljava/lang/ref/WeakReference;

    .line 467180
    return-void
.end method


# virtual methods
.method public final a(LX/0b7;)Z
    .locals 2

    .prologue
    .line 467171
    const/4 v1, 0x1

    .line 467172
    iget-object v0, p0, LX/2oa;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    move v0, v1

    .line 467173
    :goto_0
    return v0

    .line 467174
    :cond_0
    iget-object v0, p0, LX/2oa;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 467175
    if-eqz v0, :cond_1

    .line 467176
    iget-boolean p0, v0, LX/2oy;->l:Z

    move v0, p0

    .line 467177
    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
