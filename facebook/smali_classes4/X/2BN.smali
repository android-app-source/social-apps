.class public LX/2BN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0Tn;

.field public final c:LX/2BL;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;LX/2BL;)V
    .locals 0

    .prologue
    .line 379039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 379040
    iput-object p1, p0, LX/2BN;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 379041
    iput-object p2, p0, LX/2BN;->b:LX/0Tn;

    .line 379042
    iput-object p3, p0, LX/2BN;->c:LX/2BL;

    .line 379043
    return-void
.end method

.method public static b(LX/2BN;Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 379044
    iget-object v0, p0, LX/2BN;->b:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 379045
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 379046
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 379047
    if-eqz p3, :cond_0

    .line 379048
    const-string v0, "kvm_null_flag"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "%s can\'t be stored as a value in KVM, as it is a reserved keyword."

    new-array v1, v1, [Ljava/lang/Object;

    const-string v4, "kvm_null_flag"

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 379049
    :cond_0
    iget-object v0, p0, LX/2BN;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-static {p0, p1}, LX/2BN;->b(LX/2BN;Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1, p2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 379050
    return-void

    :cond_1
    move v0, v2

    .line 379051
    goto :goto_0
.end method
