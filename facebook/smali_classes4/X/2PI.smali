.class public abstract LX/2PI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/omnistore/module/OmnistoreStoredProcedureComponent;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<T",
            "L;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I

.field private d:LX/2cC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 406194
    const-class v0, LX/2PI;

    sput-object v0, LX/2PI;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 406195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406196
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    .line 406197
    iput p1, p0, LX/2PI;->c:I

    .line 406198
    return-void
.end method

.method private static b([B)LX/Dph;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 406199
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    .line 406200
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 406201
    new-instance v2, LX/1sr;

    invoke-direct {v2, v1}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    .line 406202
    invoke-interface {v0, v2}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 406203
    :try_start_0
    const/16 v12, 0x8

    const/4 v9, 0x0

    .line 406204
    invoke-virtual {v0}, LX/1su;->r()LX/1sv;

    move-object v8, v9

    move-object v7, v9

    move-object v6, v9

    move-object v5, v9

    .line 406205
    :goto_0
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    move-result-object v4

    .line 406206
    iget-byte v10, v4, LX/1sw;->b:B

    if-eqz v10, :cond_6

    .line 406207
    iget-short v10, v4, LX/1sw;->c:S

    packed-switch v10, :pswitch_data_0

    .line 406208
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {v0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 406209
    :pswitch_0
    iget-byte v10, v4, LX/1sw;->b:B

    if-ne v10, v12, :cond_0

    .line 406210
    invoke-virtual {v0}, LX/1su;->m()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0

    .line 406211
    :cond_0
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {v0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 406212
    :pswitch_1
    iget-byte v10, v4, LX/1sw;->b:B

    if-ne v10, v12, :cond_1

    .line 406213
    invoke-virtual {v0}, LX/1su;->m()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_0

    .line 406214
    :cond_1
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {v0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 406215
    :pswitch_2
    iget-byte v10, v4, LX/1sw;->b:B

    const/16 v11, 0xb

    if-ne v10, v11, :cond_2

    .line 406216
    invoke-virtual {v0}, LX/1su;->q()[B

    move-result-object v7

    goto :goto_0

    .line 406217
    :cond_2
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {v0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 406218
    :pswitch_3
    iget-byte v10, v4, LX/1sw;->b:B

    const/16 v11, 0xc

    if-ne v10, v11, :cond_4

    .line 406219
    new-instance v4, LX/Dpi;

    invoke-direct {v4}, LX/Dpi;-><init>()V

    .line 406220
    new-instance v4, LX/Dpi;

    invoke-direct {v4}, LX/Dpi;-><init>()V

    .line 406221
    const/4 v8, 0x0

    iput v8, v4, LX/Dpi;->setField_:I

    .line 406222
    const/4 v8, 0x0

    iput-object v8, v4, LX/Dpi;->value_:Ljava/lang/Object;

    .line 406223
    invoke-virtual {v0}, LX/1su;->r()LX/1sv;

    .line 406224
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    move-result-object v8

    .line 406225
    invoke-virtual {v4, v0, v8}, LX/Dpi;->a(LX/1su;LX/1sw;)Ljava/lang/Object;

    move-result-object v10

    iput-object v10, v4, LX/Dpi;->value_:Ljava/lang/Object;

    .line 406226
    iget-object v10, v4, LX/6kT;->value_:Ljava/lang/Object;

    if-eqz v10, :cond_3

    .line 406227
    iget-short v8, v8, LX/1sw;->c:S

    iput v8, v4, LX/Dpi;->setField_:I

    .line 406228
    :cond_3
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    .line 406229
    invoke-virtual {v0}, LX/1su;->e()V

    .line 406230
    move-object v4, v4

    .line 406231
    move-object v8, v4

    .line 406232
    goto :goto_0

    .line 406233
    :cond_4
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {v0, v4}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 406234
    :pswitch_4
    iget-byte v10, v4, LX/1sw;->b:B

    const/16 v11, 0xa

    if-ne v10, v11, :cond_5

    .line 406235
    invoke-virtual {v0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    goto/16 :goto_0

    .line 406236
    :cond_5
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {v0, v4}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 406237
    :cond_6
    invoke-virtual {v0}, LX/1su;->e()V

    .line 406238
    new-instance v4, LX/Dph;

    invoke-direct/range {v4 .. v9}, LX/Dph;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;[BLX/Dpi;Ljava/lang/Long;)V

    .line 406239
    invoke-static {v4}, LX/Dph;->a(LX/Dph;)V

    .line 406240
    move-object v0, v4
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406241
    invoke-virtual {v2}, LX/1sr;->a()V

    :goto_1
    return-object v0

    .line 406242
    :catch_0
    move-exception v0

    .line 406243
    :goto_2
    :try_start_1
    sget-object v1, LX/2PI;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406244
    invoke-virtual {v2}, LX/1sr;->a()V

    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/1sr;->a()V

    throw v0

    .line 406245
    :catch_1
    move-exception v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(LX/Dph;)V
    .param p1    # LX/Dph;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "L;",
            ")V"
        }
    .end annotation

    .prologue
    .line 406192
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 406193
    return-void
.end method

.method public final a([B)V
    .locals 1

    .prologue
    .line 406172
    iget-object v0, p0, LX/2PI;->d:LX/2cC;

    invoke-virtual {v0, p1}, LX/2cC;->callStoredProcedure([B)V

    .line 406173
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 406191
    iget-object v0, p0, LX/2PI;->d:LX/2cC;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized onSenderAvailable(LX/2cC;)V
    .locals 3

    .prologue
    .line 406185
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/2PI;->d:LX/2cC;

    .line 406186
    invoke-virtual {p0}, LX/2PI;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406187
    :goto_0
    monitor-exit p0

    return-void

    .line 406188
    :catch_0
    move-exception v0

    .line 406189
    :try_start_1
    sget-object v1, LX/2PI;->b:Ljava/lang/Class;

    const-string v2, "Error processing available stored procedure sender"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 406190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onSenderInvalidated()V
    .locals 3

    .prologue
    .line 406180
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/2PI;->d:LX/2cC;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406181
    :goto_0
    monitor-exit p0

    return-void

    .line 406182
    :catch_0
    move-exception v0

    .line 406183
    :try_start_1
    sget-object v1, LX/2PI;->b:Ljava/lang/Class;

    const-string v2, "Error processing invalidated stored procedure sender"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 406184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onStoredProcedureResult(Ljava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    .line 406174
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-static {v0}, LX/2PI;->b([B)LX/Dph;

    move-result-object v0

    .line 406175
    invoke-virtual {p0, v0}, LX/2PI;->a(LX/Dph;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406176
    :goto_0
    monitor-exit p0

    return-void

    .line 406177
    :catch_0
    move-exception v0

    .line 406178
    :try_start_1
    sget-object v1, LX/2PI;->b:Ljava/lang/Class;

    const-string v2, "Error processing stored procedure result"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 406179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
