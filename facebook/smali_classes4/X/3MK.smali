.class public LX/3MK;
.super LX/1qS;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3MK;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/3ML;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 554287
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "smstakeover_db"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 554288
    return-void
.end method

.method public static a(LX/0QB;)LX/3MK;
    .locals 7

    .prologue
    .line 554289
    sget-object v0, LX/3MK;->a:LX/3MK;

    if-nez v0, :cond_1

    .line 554290
    const-class v1, LX/3MK;

    monitor-enter v1

    .line 554291
    :try_start_0
    sget-object v0, LX/3MK;->a:LX/3MK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 554292
    if-eqz v2, :cond_0

    .line 554293
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 554294
    new-instance p0, LX/3MK;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v5

    check-cast v5, LX/1qU;

    .line 554295
    new-instance v6, LX/3ML;

    invoke-direct {v6}, LX/3ML;-><init>()V

    .line 554296
    move-object v6, v6

    .line 554297
    move-object v6, v6

    .line 554298
    check-cast v6, LX/3ML;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3MK;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/3ML;)V

    .line 554299
    move-object v0, p0

    .line 554300
    sput-object v0, LX/3MK;->a:LX/3MK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554301
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 554302
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 554303
    :cond_1
    sget-object v0, LX/3MK;->a:LX/3MK;

    return-object v0

    .line 554304
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 554305
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final d()I
    .locals 1

    .prologue
    .line 554286
    const/16 v0, 0x2800

    return v0
.end method
