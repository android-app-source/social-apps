.class public LX/2Q0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile l:LX/2Q0;


# instance fields
.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2Ow;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2My;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/297;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Jln;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 407487
    const-class v0, LX/2Q0;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2Q0;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 407482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407483
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 407484
    iput-object v0, p0, LX/2Q0;->g:LX/0Ot;

    .line 407485
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Q0;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 407486
    return-void
.end method

.method public static a(LX/0QB;)LX/2Q0;
    .locals 12

    .prologue
    .line 407488
    sget-object v0, LX/2Q0;->l:LX/2Q0;

    if-nez v0, :cond_1

    .line 407489
    const-class v1, LX/2Q0;

    monitor-enter v1

    .line 407490
    :try_start_0
    sget-object v0, LX/2Q0;->l:LX/2Q0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 407491
    if-eqz v2, :cond_0

    .line 407492
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 407493
    new-instance v3, LX/2Q0;

    invoke-direct {v3}, LX/2Q0;-><init>()V

    .line 407494
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, LX/0Tf;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v7

    check-cast v7, LX/0aG;

    invoke-static {v0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v8

    check-cast v8, LX/2Ow;

    const/16 v9, 0xd6c

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/297;->b(LX/0QB;)LX/297;

    move-result-object v10

    check-cast v10, LX/297;

    const/16 v11, 0x27a7

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->b(LX/0QB;)Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;

    move-result-object p0

    check-cast p0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;

    .line 407495
    iput-object v4, v3, LX/2Q0;->b:LX/0Uh;

    iput-object v5, v3, LX/2Q0;->c:LX/0Tf;

    iput-object v6, v3, LX/2Q0;->d:LX/0Xl;

    iput-object v7, v3, LX/2Q0;->e:LX/0aG;

    iput-object v8, v3, LX/2Q0;->f:LX/2Ow;

    iput-object v9, v3, LX/2Q0;->g:LX/0Ot;

    iput-object v10, v3, LX/2Q0;->h:LX/297;

    iput-object v11, v3, LX/2Q0;->i:LX/0Or;

    iput-object p0, v3, LX/2Q0;->j:Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;

    .line 407496
    move-object v0, v3

    .line 407497
    sput-object v0, LX/2Q0;->l:LX/2Q0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 407498
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 407499
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 407500
    :cond_1
    sget-object v0, LX/2Q0;->l:LX/2Q0;

    return-object v0

    .line 407501
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 407502
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized b(LX/2Q0;)V
    .locals 5

    .prologue
    .line 407476
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2Q0;->b:LX/0Uh;

    const/16 v1, 0x10e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 407477
    :goto_0
    monitor-exit p0

    return-void

    .line 407478
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/2Q0;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    .line 407479
    iget-object v0, p0, LX/2Q0;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 407480
    :cond_1
    iget-object v0, p0, LX/2Q0;->c:LX/0Tf;

    new-instance v1, Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;

    invoke-direct {v1, p0}, Lcom/facebook/orca/notify/UnreadThreadsBadgeCountCalculator$2;-><init>(LX/2Q0;)V

    const-wide/16 v2, 0x1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    iput-object v0, p0, LX/2Q0;->k:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 407481
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 407471
    new-instance v0, LX/2Q2;

    invoke-direct {v0, p0}, LX/2Q2;-><init>(LX/2Q0;)V

    .line 407472
    iget-object v1, p0, LX/2Q0;->d:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    sget-object v2, LX/0aY;->s:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    .line 407473
    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 407474
    invoke-static {p0}, LX/2Q0;->b(LX/2Q0;)V

    .line 407475
    return-void
.end method
