.class public LX/3BA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 527440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(IILX/31M;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 527441
    sget-object v2, LX/31M;->RIGHT:LX/31M;

    if-ne p2, v2, :cond_2

    .line 527442
    if-lez p0, :cond_1

    .line 527443
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 527444
    goto :goto_0

    .line 527445
    :cond_2
    add-int/lit8 v2, p1, -0x1

    if-lt p0, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/view/View;ZLX/31M;II)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    .line 527446
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 527447
    check-cast v0, Landroid/view/ViewGroup;

    .line 527448
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v3

    .line 527449
    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v4

    .line 527450
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 527451
    add-int/lit8 v2, v2, -0x1

    :goto_0
    if-ltz v2, :cond_1

    .line 527452
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 527453
    add-int v6, p3, v3

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v7

    if-lt v6, v7, :cond_0

    add-int v6, p3, v3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v7

    if-ge v6, v7, :cond_0

    add-int v6, p4, v4

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v7

    if-lt v6, v7, :cond_0

    add-int v6, p4, v4

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v7

    if-ge v6, v7, :cond_0

    add-int v6, p3, v3

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    add-int v7, p4, v4

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v5, v1, p2, v6, v7}, LX/3BA;->a(Landroid/view/View;ZLX/31M;II)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v1

    .line 527454
    :goto_1
    return v0

    .line 527455
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 527456
    :cond_1
    if-eqz p1, :cond_3

    const/4 v7, 0x1

    .line 527457
    instance-of v0, p0, LX/31N;

    if-eqz v0, :cond_4

    .line 527458
    :cond_2
    :goto_2
    move v0, v7

    .line 527459
    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 527460
    :cond_4
    instance-of v0, p0, LX/0hY;

    if-eqz v0, :cond_5

    .line 527461
    check-cast p0, LX/0hY;

    invoke-interface {p0, p2, p3, p4}, LX/0hY;->a(LX/31M;II)Z

    move-result v7

    goto :goto_2

    .line 527462
    :cond_5
    instance-of v0, p0, Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_6

    .line 527463
    check-cast p0, Landroid/support/v4/view/ViewPager;

    .line 527464
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    .line 527465
    if-nez v0, :cond_c

    .line 527466
    const/4 v0, 0x0

    .line 527467
    :goto_3
    move v7, v0

    .line 527468
    goto :goto_2

    .line 527469
    :cond_6
    instance-of v0, p0, Landroid/widget/Gallery;

    if-eqz v0, :cond_7

    .line 527470
    check-cast p0, Landroid/widget/Gallery;

    .line 527471
    invoke-virtual {p0}, Landroid/widget/Gallery;->getSelectedItemPosition()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/Gallery;->getCount()I

    move-result v2

    invoke-static {v0, v2, p2}, LX/3BA;->a(IILX/31M;)Z

    move-result v0

    move v7, v0

    .line 527472
    goto :goto_2

    .line 527473
    :cond_7
    instance-of v0, p0, Landroid/widget/HorizontalScrollView;

    if-eqz v0, :cond_9

    .line 527474
    check-cast p0, Landroid/widget/HorizontalScrollView;

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 527475
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getChildCount()I

    move-result v3

    if-gtz v3, :cond_d

    .line 527476
    :cond_8
    :goto_4
    move v7, v0

    .line 527477
    goto :goto_2

    .line 527478
    :cond_9
    instance-of v0, p0, Landroid/webkit/WebView;

    if-nez v0, :cond_2

    .line 527479
    instance-of v0, p0, Landroid/widget/ViewAnimator;

    if-eqz v0, :cond_a

    move-object v0, p0

    .line 527480
    check-cast v0, Landroid/widget/ViewAnimator;

    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->getCurrentView()Landroid/view/View;

    move-result-object v6

    .line 527481
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v5

    move-object v0, p2

    move v2, p3

    move v3, p4

    const/4 v8, 0x1

    .line 527482
    if-eqz v6, :cond_f

    add-int p1, v2, v4

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result p3

    if-lt p1, p3, :cond_f

    add-int p1, v2, v4

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result p3

    if-ge p1, p3, :cond_f

    add-int p1, v3, v5

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result p3

    if-lt p1, p3, :cond_f

    add-int p1, v3, v5

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result p3

    if-ge p1, p3, :cond_f

    add-int p1, v2, v4

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result p3

    sub-int/2addr p1, p3

    add-int p3, v3, v5

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result p4

    sub-int/2addr p3, p4

    invoke-static {v6, v8, v0, p1, p3}, LX/3BA;->a(Landroid/view/View;ZLX/31M;II)Z

    move-result p1

    if-eqz p1, :cond_f

    :goto_5
    move v0, v8

    .line 527483
    if-nez v0, :cond_2

    .line 527484
    :cond_a
    sget-object v0, LX/31M;->RIGHT:LX/31M;

    if-ne p2, v0, :cond_b

    const/4 v0, -0x1

    :goto_6
    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;I)Z

    move-result v7

    goto/16 :goto_2

    :cond_b
    move v0, v7

    goto :goto_6

    :cond_c
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    invoke-static {v2, v0, p2}, LX/3BA;->a(IILX/31M;)Z

    move-result v0

    goto/16 :goto_3

    .line 527485
    :cond_d
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    .line 527486
    invoke-virtual {p0, v0}, Landroid/widget/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 527487
    if-le v4, v3, :cond_8

    .line 527488
    sub-int v3, v4, v3

    .line 527489
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v4

    .line 527490
    sget-object v5, LX/31M;->RIGHT:LX/31M;

    if-ne p2, v5, :cond_e

    .line 527491
    if-lez v4, :cond_8

    move v0, v2

    goto/16 :goto_4

    .line 527492
    :cond_e
    if-ge v4, v3, :cond_8

    move v0, v2

    goto/16 :goto_4

    :cond_f
    const/4 v8, 0x0

    goto :goto_5
.end method

.method public static a(Landroid/view/ViewGroup;LX/31M;II)Z
    .locals 1

    .prologue
    .line 527493
    const/4 v0, 0x0

    invoke-static {p0, v0, p1, p2, p3}, LX/3BA;->a(Landroid/view/View;ZLX/31M;II)Z

    move-result v0

    return v0
.end method
