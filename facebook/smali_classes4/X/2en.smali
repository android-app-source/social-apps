.class public final LX/2en;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/2ep;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 2

    .prologue
    .line 445703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445704
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/2en;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/2en;->a:Ljava/lang/String;

    .line 445705
    iput-object p2, p0, LX/2en;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 445706
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 445702
    new-instance v0, LX/2ep;

    iget-object v1, p0, LX/2en;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v0, v1}, LX/2ep;-><init>(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 445701
    iget-object v0, p0, LX/2en;->a:Ljava/lang/String;

    return-object v0
.end method
