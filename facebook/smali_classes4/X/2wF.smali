.class public LX/2wF;
.super LX/2wG;
.source ""


# instance fields
.field public final e:LX/2wV;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/1qf;LX/1qg;Ljava/lang/String;LX/2wA;)V
    .locals 2

    invoke-direct/range {p0 .. p6}, LX/2wG;-><init>(Landroid/content/Context;Landroid/os/Looper;LX/1qf;LX/1qg;Ljava/lang/String;LX/2wA;)V

    new-instance v0, LX/2wV;

    iget-object v1, p0, LX/2wG;->d:LX/2wU;

    invoke-direct {v0, p1, v1}, LX/2wV;-><init>(Landroid/content/Context;LX/2wU;)V

    iput-object v0, p0, LX/2wF;->e:LX/2wV;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/LocationRequest;LX/2vi;Landroid/os/Looper;LX/2xS;)V
    .locals 2

    iget-object v1, p0, LX/2wF;->e:LX/2wV;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/2wF;->e:LX/2wV;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/2wV;->a(Lcom/google/android/gms/location/LocationRequest;LX/2vi;Landroid/os/Looper;LX/2xS;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f()V
    .locals 4

    iget-object v1, p0, LX/2wF;->e:LX/2wV;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, LX/2wI;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, LX/2wF;->e:LX/2wV;

    invoke-virtual {v0}, LX/2wV;->b()V

    iget-object v0, p0, LX/2wF;->e:LX/2wV;

    invoke-virtual {v0}, LX/2wV;->c()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    invoke-super {p0}, LX/2wG;->f()V

    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    const-string v2, "LocationClientImpl"

    const-string v3, "Client disconnected before listeners could be cleaned up"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
