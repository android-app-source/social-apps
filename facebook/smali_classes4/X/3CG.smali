.class public final LX/3CG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1nV;


# instance fields
.field public final synthetic a:LX/1Qt;

.field public final synthetic b:LX/1xx;


# direct methods
.method public constructor <init>(LX/1xx;LX/1Qt;)V
    .locals 0

    .prologue
    .line 530029
    iput-object p1, p0, LX/3CG;->b:LX/1xx;

    iput-object p2, p0, LX/3CG;->a:LX/1Qt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(LX/1yS;)V
    .locals 8

    .prologue
    .line 530030
    move-object v4, p1

    check-cast v4, LX/8sF;

    .line 530031
    const-string v0, "unknown"

    .line 530032
    iget-object v1, p0, LX/3CG;->a:LX/1Qt;

    sget-object v2, LX/1Qt;->GROUPS:LX/1Qt;

    if-ne v1, v2, :cond_1

    .line 530033
    const-string v0, "group_feed"

    move-object v1, v0

    .line 530034
    :goto_0
    iget-object v0, p0, LX/3CG;->b:LX/1xx;

    iget-object v0, v0, LX/1xx;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1g8;

    invoke-virtual {v4}, LX/8sF;->h()Ljava/lang/String;

    move-result-object v2

    .line 530035
    iget-object v3, v4, LX/1yS;->i:Ljava/lang/String;

    move-object v3, v3

    .line 530036
    iget-object v5, v0, LX/1g8;->a:LX/0Zb;

    const-string v6, "group_feed_member_name_clicked_mobile"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 530037
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 530038
    const-string v6, "group_id"

    invoke-virtual {v5, v6, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 530039
    const-string v6, "member_id"

    invoke-virtual {v5, v6, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 530040
    const-string v6, "source"

    invoke-virtual {v5, v6, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 530041
    invoke-virtual {v5}, LX/0oG;->d()V

    .line 530042
    :cond_0
    iget-object v0, p0, LX/3CG;->b:LX/1xx;

    iget-object v1, p0, LX/3CG;->b:LX/1xx;

    iget-object v1, v1, LX/1xx;->a:Landroid/content/Context;

    .line 530043
    iget-object v2, p1, LX/1yS;->i:Ljava/lang/String;

    move-object v2, v2

    .line 530044
    iget-object v3, v4, LX/8sF;->d:Ljava/lang/String;

    move-object v3, v3

    .line 530045
    invoke-virtual {v4}, LX/8sF;->h()Ljava/lang/String;

    move-result-object v4

    .line 530046
    iget-object v5, p1, LX/1yS;->c:Ljava/lang/String;

    move-object v5, v5

    .line 530047
    new-instance v6, LX/34b;

    invoke-direct {v6, v1}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 530048
    invoke-virtual {v6, v3}, LX/34b;->a(Ljava/lang/String;)V

    .line 530049
    const v7, 0x7f0824c9

    invoke-virtual {v6, v7}, LX/34c;->e(I)LX/3Ai;

    move-result-object v7

    .line 530050
    const p0, 0x7f02098f

    invoke-virtual {v7, p0}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 530051
    new-instance p0, LX/B0o;

    invoke-direct {p0, v0, v1, v5}, LX/B0o;-><init>(LX/1xx;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v7, p0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 530052
    const v7, 0x7f0824ca

    invoke-virtual {v6, v7}, LX/34c;->e(I)LX/3Ai;

    move-result-object v7

    .line 530053
    const p0, 0x7f020895

    invoke-virtual {v7, p0}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 530054
    new-instance p0, LX/B0p;

    invoke-direct {p0, v0, v4, v2, v1}, LX/B0p;-><init>(LX/1xx;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v7, p0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 530055
    new-instance v7, LX/3Af;

    invoke-direct {v7, v1}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 530056
    invoke-virtual {v7, v6}, LX/3Af;->a(LX/1OM;)V

    .line 530057
    invoke-virtual {v7}, LX/3Af;->show()V

    .line 530058
    return-void

    .line 530059
    :cond_1
    iget-object v1, p0, LX/3CG;->a:LX/1Qt;

    sget-object v2, LX/1Qt;->GROUPS_MEMBER_INFO:LX/1Qt;

    if-ne v1, v2, :cond_2

    .line 530060
    const-string v0, "group_member_feed"

    move-object v1, v0

    goto/16 :goto_0

    :cond_2
    move-object v1, v0

    goto/16 :goto_0
.end method
