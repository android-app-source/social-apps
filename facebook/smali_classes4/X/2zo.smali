.class public LX/2zo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2AX;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/2zo;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Z

.field private final c:LX/0Uh;

.field private final d:LX/01T;


# direct methods
.method public constructor <init>(LX/0Or;Ljava/lang/Boolean;LX/0Uh;LX/01T;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerAppIconBadgingEnabled;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483919
    iput-object p1, p0, LX/2zo;->a:LX/0Or;

    .line 483920
    iput-object p4, p0, LX/2zo;->d:LX/01T;

    .line 483921
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/2zo;->b:Z

    .line 483922
    iput-object p3, p0, LX/2zo;->c:LX/0Uh;

    .line 483923
    return-void
.end method

.method public static a(LX/0QB;)LX/2zo;
    .locals 7

    .prologue
    .line 483924
    sget-object v0, LX/2zo;->e:LX/2zo;

    if-nez v0, :cond_1

    .line 483925
    const-class v1, LX/2zo;

    monitor-enter v1

    .line 483926
    :try_start_0
    sget-object v0, LX/2zo;->e:LX/2zo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 483927
    if-eqz v2, :cond_0

    .line 483928
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 483929
    new-instance v6, LX/2zo;

    const/16 v3, 0x14e3

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v5

    check-cast v5, LX/01T;

    invoke-direct {v6, p0, v3, v4, v5}, LX/2zo;-><init>(LX/0Or;Ljava/lang/Boolean;LX/0Uh;LX/01T;)V

    .line 483930
    move-object v0, v6

    .line 483931
    sput-object v0, LX/2zo;->e:LX/2zo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483932
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 483933
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 483934
    :cond_1
    sget-object v0, LX/2zo;->e:LX/2zo;

    return-object v0

    .line 483935
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 483936
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "LX/1se;",
            "LX/2C3;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 483937
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 483938
    iget-object v0, p0, LX/2zo;->c:LX/0Uh;

    const/16 v2, 0x10e

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 483939
    iget-object v0, p0, LX/2zo;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/2zo;->b:Z

    if-nez v0, :cond_1

    .line 483940
    new-instance v0, LX/1se;

    const-string v2, "/t_inbox"

    invoke-direct {v0, v2, v3}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v2, LX/2C3;->ALWAYS:LX/2C3;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483941
    :cond_0
    :goto_0
    new-instance v0, LX/1se;

    const-string v2, "/pp"

    invoke-direct {v0, v2, v3}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v2, LX/2C3;->ALWAYS:LX/2C3;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483942
    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0

    .line 483943
    :cond_1
    iget-object v0, p0, LX/2zo;->d:LX/01T;

    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v2, :cond_0

    .line 483944
    new-instance v0, LX/1se;

    const-string v2, "/t_inbox"

    invoke-direct {v0, v2, v3}, LX/1se;-><init>(Ljava/lang/String;I)V

    sget-object v2, LX/2C3;->APP_USE:LX/2C3;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
