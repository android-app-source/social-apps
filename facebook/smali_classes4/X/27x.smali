.class public LX/27x;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/27x;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/49e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/49e;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 373329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373330
    iput-object p1, p0, LX/27x;->a:Ljava/util/Set;

    .line 373331
    return-void
.end method

.method public static a(LX/0QB;)LX/27x;
    .locals 4

    .prologue
    .line 373332
    sget-object v0, LX/27x;->b:LX/27x;

    if-nez v0, :cond_1

    .line 373333
    const-class v1, LX/27x;

    monitor-enter v1

    .line 373334
    :try_start_0
    sget-object v0, LX/27x;->b:LX/27x;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 373335
    if-eqz v2, :cond_0

    .line 373336
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 373337
    new-instance v3, LX/27x;

    invoke-static {v0}, LX/27u;->a(LX/0QB;)Ljava/util/Set;

    move-result-object p0

    invoke-direct {v3, p0}, LX/27x;-><init>(Ljava/util/Set;)V

    .line 373338
    move-object v0, v3

    .line 373339
    sput-object v0, LX/27x;->b:LX/27x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373340
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 373341
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 373342
    :cond_1
    sget-object v0, LX/27x;->b:LX/27x;

    return-object v0

    .line 373343
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 373344
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
