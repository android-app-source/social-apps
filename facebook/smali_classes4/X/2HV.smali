.class public LX/2HV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/1sd;

.field public final c:LX/0WJ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1sd;LX/0WJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 390489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390490
    iput-object p1, p0, LX/2HV;->a:Landroid/content/Context;

    .line 390491
    iput-object p2, p0, LX/2HV;->b:LX/1sd;

    .line 390492
    iput-object p3, p0, LX/2HV;->c:LX/0WJ;

    .line 390493
    return-void
.end method

.method public static a(LX/2HV;Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 390494
    iget-object v0, p0, LX/2HV;->b:LX/1sd;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, LX/1sd;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 390495
    :cond_0
    :goto_0
    return-object v2

    .line 390496
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 390497
    sget-object v1, LX/1ZO;->c:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 390498
    if-nez v1, :cond_2

    .line 390499
    if-eqz v1, :cond_0

    .line 390500
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 390501
    :cond_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    .line 390502
    if-eqz v1, :cond_0

    .line 390503
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 390504
    :cond_3
    :try_start_1
    const-string v0, "attribution_json"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 390505
    if-gez v0, :cond_4

    .line 390506
    if-eqz v1, :cond_0

    .line 390507
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 390508
    :cond_4
    :try_start_2
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 390509
    if-eqz v1, :cond_0

    .line 390510
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 390511
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    .line 390512
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method
