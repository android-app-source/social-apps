.class public LX/2Dq;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 384700
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 384701
    return-void
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/Boolean;LX/2Do;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/work/config/community/WorkCommunityName;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 384702
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 384703
    :goto_0
    return-object v0

    .line 384704
    :cond_0
    sget-object v1, LX/4oz;->e:LX/0Tn;

    invoke-interface {p0, v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 384705
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 384706
    invoke-virtual {p2}, LX/2Do;->a()V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 384707
    goto :goto_0
.end method

.method public static b(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/Boolean;LX/2Do;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/work/config/community/WorkCommunityId;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 384708
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 384709
    :goto_0
    return-object v0

    .line 384710
    :cond_0
    sget-object v1, LX/4oz;->d:LX/0Tn;

    invoke-interface {p0, v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 384711
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 384712
    invoke-virtual {p2}, LX/2Do;->a()V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 384713
    goto :goto_0
.end method

.method public static c(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/Boolean;LX/2Do;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/work/config/community/WorkCommunitySubdomain;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 384714
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 384715
    :goto_0
    return-object v0

    .line 384716
    :cond_0
    sget-object v1, LX/4oz;->f:LX/0Tn;

    invoke-interface {p0, v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 384717
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 384718
    invoke-virtual {p2}, LX/2Do;->a()V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 384719
    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 384720
    return-void
.end method
