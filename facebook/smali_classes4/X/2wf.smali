.class public abstract LX/2wf;
.super LX/2wg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "LX/2NW;",
        ">",
        "LX/2wg",
        "<TR;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/2xO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2xO",
            "<TR;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2wX;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/Object;

.field private final e:Ljava/util/concurrent/CountDownLatch;

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/27U;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/27U",
            "<-TR;>;"
        }
    .end annotation
.end field

.field public h:LX/2NW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private i:LX/4uT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4uT;"
        }
    .end annotation
.end field

.field private volatile j:Z

.field private k:Z

.field private l:Z

.field private m:LX/3Ke;

.field private volatile n:LX/4v2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4v2",
            "<TR;>;"
        }
    .end annotation
.end field

.field private o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/2xN;

    invoke-direct {v0}, LX/2xN;-><init>()V

    sput-object v0, LX/2wf;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0}, LX/2wg;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/2wf;->d:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/2wf;->e:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2wf;->f:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2wf;->o:Z

    new-instance v0, LX/2xO;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2xO;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/2wf;->b:LX/2xO;

    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2wf;->c:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public constructor <init>(LX/2wX;)V
    .locals 2

    invoke-direct {p0}, LX/2wg;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/2wf;->d:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/2wf;->e:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2wf;->f:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2wf;->o:Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/2wX;->c()Landroid/os/Looper;

    move-result-object v0

    :goto_0
    new-instance v1, LX/2xO;

    invoke-direct {v1, v0}, LX/2xO;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, LX/2wf;->b:LX/2xO;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2wf;->c:Ljava/lang/ref/WeakReference;

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/2NW;)V
    .locals 6

    instance-of v1, p0, LX/2xc;

    if-eqz v1, :cond_0

    :try_start_0
    move-object v0, p0

    check-cast v0, LX/2xc;

    move-object v1, v0

    invoke-interface {v1}, LX/2xc;->nc_()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "BasePendingResult"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x12

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unable to release "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private c(LX/2NW;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    const/4 v1, 0x0

    iput-object p1, p0, LX/2wf;->h:LX/2NW;

    iput-object v1, p0, LX/2wf;->m:LX/3Ke;

    iget-object v0, p0, LX/2wf;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v0, p0, LX/2wf;->h:LX/2NW;

    invoke-interface {v0}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    iget-boolean v0, p0, LX/2wf;->k:Z

    if-eqz v0, :cond_1

    iput-object v1, p0, LX/2wf;->g:LX/27U;

    :cond_0
    :goto_0
    iget-object v0, p0, LX/2wf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    :cond_1
    iget-object v0, p0, LX/2wf;->g:LX/27U;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/2wf;->h:LX/2NW;

    instance-of v0, v0, LX/2xc;

    if-eqz v0, :cond_0

    new-instance v0, LX/4uT;

    invoke-direct {v0, p0}, LX/4uT;-><init>(LX/2wf;)V

    iput-object v0, p0, LX/2wf;->i:LX/4uT;

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/2wf;->b:LX/2xO;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/2xO;->removeMessages(I)V

    iget-object v0, p0, LX/2wf;->b:LX/2xO;

    iget-object v1, p0, LX/2wf;->g:LX/27U;

    invoke-direct {p0}, LX/2wf;->d()LX/2NW;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2xO;->a(LX/27U;LX/2NW;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, LX/2wf;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private c()Z
    .locals 2

    iget-object v1, p0, LX/2wf;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, LX/2wf;->k:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private d()LX/2NW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    const/4 v0, 0x1

    iget-object v1, p0, LX/2wf;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, p0, LX/2wf;->j:Z

    if-nez v2, :cond_0

    :goto_0
    const-string v2, "Result has already been consumed."

    invoke-static {v0, v2}, LX/1ol;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, LX/2wf;->g()Z

    move-result v0

    const-string v2, "Result is not ready."

    invoke-static {v0, v2}, LX/1ol;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, LX/2wf;->h:LX/2NW;

    const/4 v2, 0x0

    iput-object v2, p0, LX/2wf;->h:LX/2NW;

    const/4 v2, 0x0

    iput-object v2, p0, LX/2wf;->g:LX/27U;

    const/4 v2, 0x1

    iput-boolean v2, p0, LX/2wf;->j:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LX/2wf;->f()V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()LX/2NW;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "await must not be called on the UI thread"

    invoke-static {v0, v3}, LX/1ol;->a(ZLjava/lang/Object;)V

    iget-boolean v0, p0, LX/2wf;->j:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Result has already been consumed"

    invoke-static {v0, v3}, LX/1ol;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, LX/2wf;->n:LX/4v2;

    if-nez v0, :cond_2

    :goto_2
    const-string v0, "Cannot await if then() has been called."

    invoke-static {v1, v0}, LX/1ol;->a(ZLjava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, LX/2wf;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    invoke-virtual {p0}, LX/2wf;->g()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, LX/1ol;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, LX/2wf;->d()LX/2NW;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :catch_0
    sget-object v0, Lcom/google/android/gms/common/api/Status;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, LX/2wf;->c(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_3
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)LX/2NW;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TR;"
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-lez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "await must not be called on the UI thread when time is greater than zero."

    invoke-static {v0, v3}, LX/1ol;->a(ZLjava/lang/Object;)V

    iget-boolean v0, p0, LX/2wf;->j:Z

    if-nez v0, :cond_3

    move v0, v2

    :goto_1
    const-string v3, "Result has already been consumed."

    invoke-static {v0, v3}, LX/1ol;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, LX/2wf;->n:LX/4v2;

    if-nez v0, :cond_4

    :goto_2
    const-string v0, "Cannot await if then() has been called."

    invoke-static {v2, v0}, LX/1ol;->a(ZLjava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, LX/2wf;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/common/api/Status;->d:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, LX/2wf;->c(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_3
    invoke-virtual {p0}, LX/2wf;->g()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, LX/1ol;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, LX/2wf;->d()LX/2NW;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_2

    :catch_0
    sget-object v0, Lcom/google/android/gms/common/api/Status;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, LX/2wf;->c(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_3
.end method

.method public abstract a(Lcom/google/android/gms/common/api/Status;)LX/2NW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/Status;",
            ")TR;"
        }
    .end annotation
.end method

.method public final a(LX/27U;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/27U",
            "<-TR;>;)V"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, LX/2wf;->d:Ljava/lang/Object;

    monitor-enter v3

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/2wf;->g:LX/27U;

    monitor-exit v3

    :goto_0
    return-void

    :cond_0
    iget-boolean v2, p0, LX/2wf;->j:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_1
    const-string v4, "Result has already been consumed."

    invoke-static {v2, v4}, LX/1ol;->a(ZLjava/lang/Object;)V

    iget-object v2, p0, LX/2wf;->n:LX/4v2;

    if-nez v2, :cond_2

    :goto_2
    const-string v1, "Cannot set callbacks if then() has been called."

    invoke-static {v0, v1}, LX/1ol;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, LX/2wf;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    :try_start_1
    invoke-virtual {p0}, LX/2wf;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/2wf;->b:LX/2xO;

    invoke-direct {p0}, LX/2wf;->d()LX/2NW;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/2xO;->a(LX/27U;LX/2NW;)V

    :goto_3
    monitor-exit v3

    goto :goto_0

    :cond_4
    iput-object p1, p0, LX/2wf;->g:LX/27U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method public final a(LX/2NW;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, LX/2wf;->d:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, LX/2wf;->l:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, LX/2wf;->k:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, LX/2wf;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_0
    invoke-static {p1}, LX/2wf;->b(LX/2NW;)V

    monitor-exit v3

    :goto_0
    return-void

    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/2wf;->g()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_2
    const-string v4, "Results have already been set"

    invoke-static {v2, v4}, LX/1ol;->a(ZLjava/lang/Object;)V

    iget-boolean v2, p0, LX/2wf;->j:Z

    if-nez v2, :cond_3

    :goto_3
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, LX/1ol;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, LX/2wf;->c(LX/2NW;)V

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v2, v1

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3
.end method

.method public final b()Ljava/lang/Integer;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    iget-object v1, p0, LX/2wf;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, LX/2wf;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, LX/2wf;->a(Lcom/google/android/gms/common/api/Status;)LX/2NW;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/2wf;->a(LX/2NW;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2wf;->l:Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()V
    .locals 0

    return-void
.end method

.method public final g()Z
    .locals 4

    iget-object v0, p0, LX/2wf;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    iget-object v1, p0, LX/2wf;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, LX/2wf;->k:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/2wf;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LX/2wf;->h:LX/2NW;

    invoke-static {v0}, LX/2wf;->b(LX/2NW;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2wf;->k:Z

    sget-object v0, Lcom/google/android/gms/common/api/Status;->e:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, LX/2wf;->a(Lcom/google/android/gms/common/api/Status;)LX/2NW;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2wf;->c(LX/2NW;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final i()Z
    .locals 2

    iget-object v1, p0, LX/2wf;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/2wf;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wX;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/2wf;->o:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, LX/2wf;->h()V

    :cond_1
    invoke-direct {p0}, LX/2wf;->c()Z

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final j()V
    .locals 1

    iget-boolean v0, p0, LX/2wf;->o:Z

    if-nez v0, :cond_0

    sget-object v0, LX/2wf;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/2wf;->o:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
