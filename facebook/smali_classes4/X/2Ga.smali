.class public LX/2Ga;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Gb;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/2Ga;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gg;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/registration/FacebookPushServerRegistrar;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2Gc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 388931
    const-class v0, LX/2Ga;

    sput-object v0, LX/2Ga;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Or;LX/2Gc;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2Gg;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/registration/FacebookPushServerRegistrar;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2Gc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 388925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388926
    iput-object p1, p0, LX/2Ga;->b:LX/0Ot;

    .line 388927
    iput-object p2, p0, LX/2Ga;->c:LX/0Ot;

    .line 388928
    iput-object p3, p0, LX/2Ga;->d:LX/0Or;

    .line 388929
    iput-object p4, p0, LX/2Ga;->e:LX/2Gc;

    .line 388930
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ga;
    .locals 7

    .prologue
    .line 388912
    sget-object v0, LX/2Ga;->f:LX/2Ga;

    if-nez v0, :cond_1

    .line 388913
    const-class v1, LX/2Ga;

    monitor-enter v1

    .line 388914
    :try_start_0
    sget-object v0, LX/2Ga;->f:LX/2Ga;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 388915
    if-eqz v2, :cond_0

    .line 388916
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 388917
    new-instance v4, LX/2Ga;

    const/16 v3, 0xfe8

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x1027

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x15e7

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2Gc;->a(LX/0QB;)LX/2Gc;

    move-result-object v3

    check-cast v3, LX/2Gc;

    invoke-direct {v4, v5, v6, p0, v3}, LX/2Ga;-><init>(LX/0Ot;LX/0Ot;LX/0Or;LX/2Gc;)V

    .line 388918
    move-object v0, v4

    .line 388919
    sput-object v0, LX/2Ga;->f:LX/2Ga;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 388920
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 388921
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 388922
    :cond_1
    sget-object v0, LX/2Ga;->f:LX/2Ga;

    return-object v0

    .line 388923
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 388924
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 388911
    iget-object v0, p0, LX/2Ga;->e:LX/2Gc;

    sget-object v1, LX/2Ge;->ADM:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gc;->a(LX/2Ge;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/2Ge;
    .locals 1

    .prologue
    .line 388910
    sget-object v0, LX/2Ge;->ADM:LX/2Ge;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 388907
    invoke-direct {p0}, LX/2Ga;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388908
    :goto_0
    return-void

    .line 388909
    :cond_0
    iget-object v0, p0, LX/2Ga;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->ADM:LX/2Ge;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 388895
    :try_start_0
    invoke-direct {p0}, LX/2Ga;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388896
    iget-object v0, p0, LX/2Ga;->e:LX/2Gc;

    const-string v1, "com.amazon.device.messaging"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Lcom/facebook/push/adm/ADMBroadcastReceiver;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Lcom/facebook/push/adm/ADMBroadcastReceiver$MessageAlertReceiver;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-class v4, Lcom/facebook/push/adm/ADMRegistrarService;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-class v4, Lcom/facebook/push/adm/ADMService;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, LX/2Gc;->a(Ljava/lang/String;[Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    .line 388897
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 388904
    invoke-direct {p0}, LX/2Ga;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388905
    :goto_0
    return-void

    .line 388906
    :cond_0
    iget-object v0, p0, LX/2Ga;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gg;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2Gg;->a(Z)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 388900
    invoke-direct {p0}, LX/2Ga;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 388901
    :cond_0
    :goto_0
    return-void

    .line 388902
    :cond_1
    iget-object v0, p0, LX/2Ga;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 388903
    iget-object v0, p0, LX/2Ga;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2Gg;->a(Z)V

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 388898
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/2Ga;->a(Ljava/lang/String;)V

    .line 388899
    return-void
.end method
