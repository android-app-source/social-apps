.class public LX/3QH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Pw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/3QH;


# instance fields
.field private final b:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2gc;

.field private final d:LX/0lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 563480
    const-class v0, LX/3QH;

    sput-object v0, LX/3QH;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2gc;LX/0lC;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 563475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563476
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/notifications/constants/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->FBNS_MOBILE_REQUESTS_COUNT:Lcom/facebook/notifications/constants/NotificationType;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    iput-object v0, p0, LX/3QH;->b:LX/2QP;

    .line 563477
    iput-object p1, p0, LX/3QH;->c:LX/2gc;

    .line 563478
    iput-object p2, p0, LX/3QH;->d:LX/0lC;

    .line 563479
    return-void
.end method

.method public static a(LX/0QB;)LX/3QH;
    .locals 5

    .prologue
    .line 563481
    sget-object v0, LX/3QH;->e:LX/3QH;

    if-nez v0, :cond_1

    .line 563482
    const-class v1, LX/3QH;

    monitor-enter v1

    .line 563483
    :try_start_0
    sget-object v0, LX/3QH;->e:LX/3QH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 563484
    if-eqz v2, :cond_0

    .line 563485
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 563486
    new-instance p0, LX/3QH;

    invoke-static {v0}, LX/2gc;->b(LX/0QB;)LX/2gc;

    move-result-object v3

    check-cast v3, LX/2gc;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-direct {p0, v3, v4}, LX/3QH;-><init>(LX/2gc;LX/0lC;)V

    .line 563487
    move-object v0, p0

    .line 563488
    sput-object v0, LX/3QH;->e:LX/3QH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 563489
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 563490
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 563491
    :cond_1
    sget-object v0, LX/3QH;->e:LX/3QH;

    return-object v0

    .line 563492
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 563493
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2QP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2QP",
            "<",
            "Lcom/facebook/notifications/constants/NotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 563474
    iget-object v0, p0, LX/3QH;->b:LX/2QP;

    return-object v0
.end method

.method public final a(LX/0lF;Lcom/facebook/push/PushProperty;)V
    .locals 3

    .prologue
    .line 563464
    const-string v0, "params"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 563465
    if-nez v0, :cond_1

    .line 563466
    :cond_0
    :goto_0
    return-void

    .line 563467
    :cond_1
    const-string v1, "payload"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 563468
    if-eqz v0, :cond_0

    .line 563469
    :try_start_0
    iget-object v1, p0, LX/3QH;->d:LX/0lC;

    invoke-virtual {v1, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 563470
    const-string v1, "num_unseen"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    .line 563471
    iget-object v1, p0, LX/3QH;->c:LX/2gc;

    invoke-virtual {v1, v0}, LX/2gc;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 563472
    :catch_0
    move-exception v0

    .line 563473
    sget-object v1, LX/3QH;->a:Ljava/lang/Class;

    const-string v2, "Failed to read notification payload"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
