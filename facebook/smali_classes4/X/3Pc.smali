.class public LX/3Pc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HL",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3Pd;


# direct methods
.method public constructor <init>(LX/3Pd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 562125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562126
    iput-object p1, p0, LX/3Pc;->a:LX/3Pd;

    .line 562127
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/4VT;
    .locals 2

    .prologue
    .line 562120
    check-cast p1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    .line 562121
    iget-object v0, p0, LX/3Pc;->a:LX/3Pd;

    .line 562122
    new-instance p0, LX/6AE;

    invoke-static {v0}, LX/6Ou;->b(LX/0QB;)LX/6Ou;

    move-result-object v1

    check-cast v1, LX/6Ou;

    invoke-direct {p0, p1, v1}, LX/6AE;-><init>(Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;LX/6Ou;)V

    .line 562123
    move-object v0, p0

    .line 562124
    return-object v0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 562118
    const-class v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562119
    const/4 v0, 0x0

    return-object v0
.end method
