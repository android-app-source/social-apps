.class public final LX/2sC;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7Lr;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/Video360Plugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V
    .locals 0

    .prologue
    .line 472453
    iput-object p1, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7Lr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 472454
    const-class v0, LX/7Lr;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 472455
    check-cast p1, LX/7Lr;

    .line 472456
    iget-object v0, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->Q:LX/3IP;

    invoke-virtual {v0}, LX/3IP;->a()V

    .line 472457
    iget-object v0, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-static {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->H(Lcom/facebook/video/player/plugins/Video360Plugin;)V

    .line 472458
    iget-object v0, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/Video360Plugin;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472459
    iget v0, p1, LX/7Lr;->a:I

    packed-switch v0, :pswitch_data_0

    .line 472460
    :cond_0
    :goto_0
    return-void

    .line 472461
    :pswitch_0
    iget-object v0, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->g()LX/2qW;

    move-result-object v0

    invoke-virtual {v0}, LX/2qW;->f()V

    .line 472462
    iget-object v0, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->c:LX/1C2;

    iget-object v1, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, v2, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->s()LX/04D;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1C2;->b(Ljava/lang/String;LX/04D;)LX/1C2;

    .line 472463
    iget-object v0, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    const/4 v1, 0x1

    .line 472464
    iput-boolean v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->H:Z

    .line 472465
    goto :goto_0

    .line 472466
    :pswitch_1
    iget-object v0, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->g()LX/2qW;

    move-result-object v0

    iget v1, p1, LX/7Lr;->b:F

    iget v2, p1, LX/7Lr;->c:F

    invoke-virtual {v0, v1, v2}, LX/2qW;->c(FF)V

    goto :goto_0

    .line 472467
    :pswitch_2
    iget-object v0, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->g()LX/2qW;

    move-result-object v0

    iget v1, p1, LX/7Lr;->b:F

    iget v2, p1, LX/7Lr;->c:F

    invoke-virtual {v0, v1, v2}, LX/2qW;->d(FF)V

    .line 472468
    iget-object v0, p0, LX/2sC;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    const/4 v1, 0x0

    .line 472469
    iput-boolean v1, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->H:Z

    .line 472470
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
