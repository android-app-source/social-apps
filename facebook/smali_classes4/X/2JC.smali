.class public LX/2JC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final d:Ljava/lang/Object;


# instance fields
.field private final b:LX/30d;

.field private final c:LX/2Iv;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 392627
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "local_contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "contact_hash"

    aput-object v2, v0, v1

    sput-object v0, LX/2JC;->a:[Ljava/lang/String;

    .line 392628
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/2JC;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/30d;LX/2Iv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 392629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392630
    iput-object p1, p0, LX/2JC;->b:LX/30d;

    .line 392631
    iput-object p2, p0, LX/2JC;->c:LX/2Iv;

    .line 392632
    return-void
.end method

.method public static a(LX/0QB;)LX/2JC;
    .locals 8

    .prologue
    .line 392633
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 392634
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 392635
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 392636
    if-nez v1, :cond_0

    .line 392637
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392638
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 392639
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 392640
    sget-object v1, LX/2JC;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 392641
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 392642
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 392643
    :cond_1
    if-nez v1, :cond_4

    .line 392644
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 392645
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 392646
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 392647
    new-instance p0, LX/2JC;

    invoke-static {v0}, LX/30d;->b(LX/0QB;)LX/30d;

    move-result-object v1

    check-cast v1, LX/30d;

    invoke-static {v0}, LX/2Iv;->a(LX/0QB;)LX/2Iv;

    move-result-object v7

    check-cast v7, LX/2Iv;

    invoke-direct {p0, v1, v7}, LX/2JC;-><init>(LX/30d;LX/2Iv;)V

    .line 392648
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 392649
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 392650
    if-nez v1, :cond_2

    .line 392651
    sget-object v0, LX/2JC;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2JC;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 392652
    :goto_1
    if-eqz v0, :cond_3

    .line 392653
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 392654
    :goto_3
    check-cast v0, LX/2JC;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 392655
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 392656
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 392657
    :catchall_1
    move-exception v0

    .line 392658
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 392659
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 392660
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 392661
    :cond_2
    :try_start_8
    sget-object v0, LX/2JC;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2JC;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a()LX/Ejw;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 392662
    iget-object v0, p0, LX/2JC;->c:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "contacts_upload_snapshot"

    sget-object v2, LX/2JC;->a:[Ljava/lang/String;

    const-string v7, "local_contact_id"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 392663
    if-nez v0, :cond_0

    .line 392664
    iget-object v0, p0, LX/2JC;->b:LX/30d;

    const-string v1, "snapshot_iterator_cursor_null"

    invoke-virtual {v0, v1}, LX/30d;->a(Ljava/lang/String;)V

    .line 392665
    :goto_0
    return-object v3

    :cond_0
    new-instance v3, LX/Ejw;

    invoke-direct {v3, v0}, LX/Ejw;-><init>(Landroid/database/Cursor;)V

    goto :goto_0
.end method
