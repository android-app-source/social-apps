.class public abstract LX/2Xb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Wd;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field public static final c:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[B>;"
        }
    .end annotation
.end field

.field public static final d:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[C>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/2Y2;

.field public final b:Ljava/lang/Object;

.field public volatile e:LX/2DZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 419998
    new-instance v0, LX/2Wf;

    invoke-direct {v0}, LX/2Wf;-><init>()V

    sput-object v0, LX/2Xb;->c:Ljava/lang/ThreadLocal;

    .line 419999
    new-instance v0, LX/2Wg;

    invoke-direct {v0}, LX/2Wg;-><init>()V

    sput-object v0, LX/2Xb;->d:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(LX/2Y2;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 419994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419995
    iput-object p1, p0, LX/2Xb;->a:LX/2Y2;

    .line 419996
    iput-object p2, p0, LX/2Xb;->b:Ljava/lang/Object;

    .line 419997
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 419990
    iget-object v0, p0, LX/2Xb;->e:LX/2DZ;

    if-nez v0, :cond_0

    .line 419991
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mBatchLock is null, and it should be locked"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419992
    :cond_0
    iget-object v0, p0, LX/2Xb;->e:LX/2DZ;

    invoke-virtual {v0, p0}, LX/2DZ;->e(Ljava/lang/Object;)V

    .line 419993
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 419987
    invoke-virtual {p0}, LX/2Xb;->h()I

    move-result v0

    .line 419988
    const/16 v1, 0x100

    move v1, v1

    .line 419989
    add-int/2addr v0, v1

    return v0
.end method

.method public final a(Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 419985
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/2Xb;->a(Ljava/io/Writer;Z)V

    .line 419986
    return-void
.end method

.method public final a(Ljava/io/Writer;Z)V
    .locals 3

    .prologue
    .line 419956
    invoke-virtual {p0}, LX/2Xb;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 419957
    invoke-virtual {p0}, LX/2Xb;->k()V

    .line 419958
    :cond_0
    invoke-virtual {p0, p1}, LX/2Xb;->b(Ljava/io/Writer;)V

    .line 419959
    const/4 v1, 0x1

    .line 419960
    new-instance v0, LX/2De;

    invoke-direct {v0, p1}, LX/2De;-><init>(Ljava/io/Writer;)V

    .line 419961
    iput-boolean v1, v0, LX/2De;->d:Z

    .line 419962
    iput-boolean v1, v0, LX/2De;->b:Z

    .line 419963
    move-object v0, v0

    .line 419964
    iget-object v1, p0, LX/2Xb;->a:LX/2Y2;

    .line 419965
    invoke-static {v0}, LX/2De;->c(LX/2De;)V

    .line 419966
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/2De;->c:Z

    .line 419967
    iget-object v2, v0, LX/2De;->a:Ljava/io/Writer;

    const/16 p0, 0x5d

    invoke-virtual {v2, p0}, Ljava/io/Writer;->write(I)V

    .line 419968
    if-nez p2, :cond_1

    .line 419969
    iget-object v2, v0, LX/2De;->a:Ljava/io/Writer;

    const/16 p0, 0x2c

    invoke-virtual {v2, p0}, Ljava/io/Writer;->write(I)V

    .line 419970
    iget-object v2, v0, LX/2De;->a:Ljava/io/Writer;

    invoke-virtual {v1, v2}, LX/2Y2;->a(Ljava/io/Writer;)V

    .line 419971
    :cond_1
    iget-object v2, v0, LX/2De;->a:Ljava/io/Writer;

    const/16 p0, 0x7d

    invoke-virtual {v2, p0}, Ljava/io/Writer;->write(I)V

    .line 419972
    invoke-virtual {p1}, Ljava/io/Writer;->flush()V

    .line 419973
    return-void
.end method

.method public abstract b(Ljava/io/Writer;)V
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 419984
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 420000
    invoke-direct {p0}, LX/2Xb;->f()V

    .line 420001
    iget-object v0, p0, LX/2Xb;->e:LX/2DZ;

    invoke-virtual {v0, p0}, LX/2DZ;->f(Ljava/lang/Object;)V

    .line 420002
    invoke-virtual {p0}, LX/2Xb;->j()V

    .line 420003
    iget-object v0, p0, LX/2Xb;->e:LX/2DZ;

    invoke-virtual {v0}, LX/2DZ;->a()V

    .line 420004
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Xb;->e:LX/2DZ;

    .line 420005
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 419982
    iget-object v0, p0, LX/2Xb;->e:LX/2DZ;

    .line 419983
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, LX/2DZ;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 419978
    invoke-direct {p0}, LX/2Xb;->f()V

    .line 419979
    invoke-virtual {p0}, LX/2Xb;->i()V

    .line 419980
    iget-object v0, p0, LX/2Xb;->e:LX/2DZ;

    invoke-virtual {v0, p0}, LX/2DZ;->a(Ljava/lang/Object;)V

    .line 419981
    return-void
.end method

.method public abstract g()LX/2DZ;
.end method

.method public abstract h()I
.end method

.method public abstract i()V
.end method

.method public abstract j()V
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 419975
    invoke-virtual {p0}, LX/2Xb;->g()LX/2DZ;

    move-result-object v0

    iput-object v0, p0, LX/2Xb;->e:LX/2DZ;

    .line 419976
    iget-object v0, p0, LX/2Xb;->e:LX/2DZ;

    invoke-virtual {v0, p0}, LX/2DZ;->c(Ljava/lang/Object;)V

    .line 419977
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 419974
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{lockKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2Xb;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";hasLock="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/2Xb;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
