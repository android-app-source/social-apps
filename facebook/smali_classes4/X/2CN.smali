.class public LX/2CN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "LX/0lF;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0lC;

.field private final b:LX/11M;

.field private c:Lorg/apache/http/HttpResponse;


# direct methods
.method public constructor <init>(LX/0lC;LX/11M;)V
    .locals 0

    .prologue
    .line 382669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382670
    iput-object p1, p0, LX/2CN;->a:LX/0lC;

    .line 382671
    iput-object p2, p0, LX/2CN;->b:LX/11M;

    .line 382672
    return-void
.end method


# virtual methods
.method public final a(Lorg/apache/http/HttpResponse;)LX/0lF;
    .locals 2

    .prologue
    .line 382665
    iput-object p1, p0, LX/2CN;->c:Lorg/apache/http/HttpResponse;

    .line 382666
    iget-object v0, p0, LX/2CN;->b:LX/11M;

    invoke-virtual {v0, p1}, LX/11M;->a(Lorg/apache/http/HttpResponse;)V

    .line 382667
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 382668
    iget-object v1, p0, LX/2CN;->a:LX/0lC;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0lC;->a(Ljava/io/InputStream;)LX/0lF;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 382664
    invoke-virtual {p0, p1}, LX/2CN;->a(Lorg/apache/http/HttpResponse;)LX/0lF;

    move-result-object v0

    return-object v0
.end method
