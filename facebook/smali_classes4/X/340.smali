.class public LX/340;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JK7;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JL9;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JLA;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/98j;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0ad;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/34A;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0ad;LX/0Or;LX/JKg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/JK7;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;",
            ">;",
            "LX/0Or",
            "<",
            "LX/JL9;",
            ">;",
            "LX/0Or",
            "<",
            "LX/JLA;",
            ">;",
            "LX/0Or",
            "<",
            "LX/98j;",
            ">;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/34A;",
            ">;",
            "LX/JKg;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 494422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 494423
    iput-object p1, p0, LX/340;->a:LX/0Or;

    .line 494424
    iput-object p2, p0, LX/340;->b:LX/0Or;

    .line 494425
    iput-object p3, p0, LX/340;->c:LX/0Or;

    .line 494426
    iput-object p4, p0, LX/340;->d:LX/0Or;

    .line 494427
    iput-object p5, p0, LX/340;->e:LX/0Or;

    .line 494428
    iput-object p6, p0, LX/340;->f:LX/0ad;

    .line 494429
    iput-object p7, p0, LX/340;->g:LX/0Or;

    .line 494430
    sput-object p8, Lcom/facebook/react/bridge/ReactMarker;->a:LX/5pc;

    .line 494431
    return-void
.end method
