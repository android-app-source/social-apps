.class public LX/38d;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static volatile h:LX/38d;


# instance fields
.field public b:LX/37a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/facebook/video/chromecast/CastNetworkMonitor$CastNetworkListener;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Yb;

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 521155
    sget-object v0, LX/37a;->a:LX/0Tn;

    const-string v1, "ssid"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/38d;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/37g;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 521156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521157
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LX/38d;->e:Ljava/util/Vector;

    .line 521158
    new-instance v0, LX/38e;

    invoke-direct {v0, p0, p1}, LX/38e;-><init>(LX/38d;LX/37g;)V

    invoke-virtual {p1, v0}, LX/37g;->a(LX/38f;)V

    .line 521159
    return-void
.end method

.method public static a(LX/0QB;)LX/38d;
    .locals 6

    .prologue
    .line 521140
    sget-object v0, LX/38d;->h:LX/38d;

    if-nez v0, :cond_1

    .line 521141
    const-class v1, LX/38d;

    monitor-enter v1

    .line 521142
    :try_start_0
    sget-object v0, LX/38d;->h:LX/38d;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 521143
    if-eqz v2, :cond_0

    .line 521144
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 521145
    new-instance p0, LX/38d;

    invoke-static {v0}, LX/37g;->a(LX/0QB;)LX/37g;

    move-result-object v3

    check-cast v3, LX/37g;

    invoke-direct {p0, v3}, LX/38d;-><init>(LX/37g;)V

    .line 521146
    invoke-static {v0}, LX/37a;->a(LX/0QB;)LX/37a;

    move-result-object v3

    check-cast v3, LX/37a;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    .line 521147
    iput-object v3, p0, LX/38d;->b:LX/37a;

    iput-object v4, p0, LX/38d;->c:LX/0Xl;

    iput-object v5, p0, LX/38d;->d:LX/0kb;

    .line 521148
    move-object v0, p0

    .line 521149
    sput-object v0, LX/38d;->h:LX/38d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521150
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 521151
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 521152
    :cond_1
    sget-object v0, LX/38d;->h:LX/38d;

    return-object v0

    .line 521153
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 521154
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static e(LX/38d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 521136
    iget-object v0, p0, LX/38d;->d:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->o()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 521137
    if-eqz v0, :cond_0

    .line 521138
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    .line 521139
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
