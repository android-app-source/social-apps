.class public LX/2bk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/2bk;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0Uh;

.field private final c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Uh;Ljava/lang/Boolean;)V
    .locals 1
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 439605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439606
    iput-object p1, p0, LX/2bk;->a:Landroid/content/Context;

    .line 439607
    iput-object p2, p0, LX/2bk;->b:LX/0Uh;

    .line 439608
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/2bk;->c:Z

    .line 439609
    return-void
.end method

.method public static a(LX/0QB;)LX/2bk;
    .locals 6

    .prologue
    .line 439610
    sget-object v0, LX/2bk;->d:LX/2bk;

    if-nez v0, :cond_1

    .line 439611
    const-class v1, LX/2bk;

    monitor-enter v1

    .line 439612
    :try_start_0
    sget-object v0, LX/2bk;->d:LX/2bk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 439613
    if-eqz v2, :cond_0

    .line 439614
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 439615
    new-instance p0, LX/2bk;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-direct {p0, v3, v4, v5}, LX/2bk;-><init>(Landroid/content/Context;LX/0Uh;Ljava/lang/Boolean;)V

    .line 439616
    move-object v0, p0

    .line 439617
    sput-object v0, LX/2bk;->d:LX/2bk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439618
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 439619
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 439620
    :cond_1
    sget-object v0, LX/2bk;->d:LX/2bk;

    return-object v0

    .line 439621
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 439622
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/2bk;)V
    .locals 6

    .prologue
    .line 439623
    iget-boolean v0, p0, LX/2bk;->c:Z

    if-eqz v0, :cond_0

    .line 439624
    :goto_0
    return-void

    .line 439625
    :cond_0
    iget-object v0, p0, LX/2bk;->b:LX/0Uh;

    const/16 v1, 0x382

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 439626
    const/4 v2, 0x1

    .line 439627
    if-eqz v0, :cond_1

    move v1, v2

    .line 439628
    :goto_1
    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, p0, LX/2bk;->a:Landroid/content/Context;

    const-class v5, Lcom/facebook/katana/urimap/LinkSharingNativeComposerAliasActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 439629
    iget-object v4, p0, LX/2bk;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v3, v1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 439630
    goto :goto_0

    .line 439631
    :cond_1
    const/4 v1, 0x2

    goto :goto_1
.end method


# virtual methods
.method public final init()V
    .locals 0

    .prologue
    .line 439632
    invoke-static {p0}, LX/2bk;->a$redex0(LX/2bk;)V

    .line 439633
    return-void
.end method
