.class public final LX/2YE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "LX/2YB;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Iterator;

.field public final synthetic b:LX/2YA;


# direct methods
.method public constructor <init>(LX/2YA;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 420816
    iput-object p1, p0, LX/2YE;->b:LX/2YA;

    iput-object p2, p0, LX/2YE;->a:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 420815
    iget-object v0, p0, LX/2YE;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 420817
    iget-object v1, p0, LX/2YE;->b:LX/2YA;

    iget-object v0, p0, LX/2YE;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v1, v0}, LX/2YA;->a(Ljava/io/File;)LX/2YB;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 420813
    iget-object v0, p0, LX/2YE;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 420814
    return-void
.end method
