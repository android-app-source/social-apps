.class public LX/3C6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;",
        "Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/3C6;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0So;

.field private final c:LX/0dC;

.field private final d:LX/2Cx;


# direct methods
.method public constructor <init>(LX/0SG;LX/0So;LX/0dC;LX/2Cx;)V
    .locals 0
    .param p2    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 529654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529655
    iput-object p1, p0, LX/3C6;->a:LX/0SG;

    .line 529656
    iput-object p2, p0, LX/3C6;->b:LX/0So;

    .line 529657
    iput-object p3, p0, LX/3C6;->c:LX/0dC;

    .line 529658
    iput-object p4, p0, LX/3C6;->d:LX/2Cx;

    .line 529659
    return-void
.end method

.method public static a(LX/0QB;)LX/3C6;
    .locals 7

    .prologue
    .line 529792
    sget-object v0, LX/3C6;->e:LX/3C6;

    if-nez v0, :cond_1

    .line 529793
    const-class v1, LX/3C6;

    monitor-enter v1

    .line 529794
    :try_start_0
    sget-object v0, LX/3C6;->e:LX/3C6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 529795
    if-eqz v2, :cond_0

    .line 529796
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 529797
    new-instance p0, LX/3C6;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v5

    check-cast v5, LX/0dC;

    invoke-static {v0}, LX/2Cx;->a(LX/0QB;)LX/2Cx;

    move-result-object v6

    check-cast v6, LX/2Cx;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3C6;-><init>(LX/0SG;LX/0So;LX/0dC;LX/2Cx;)V

    .line 529798
    move-object v0, p0

    .line 529799
    sput-object v0, LX/3C6;->e:LX/3C6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529800
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 529801
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 529802
    :cond_1
    sget-object v0, LX/3C6;->e:LX/3C6;

    return-object v0

    .line 529803
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 529804
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Hla;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 529720
    :try_start_0
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 529721
    sget-object v0, LX/1jl;->a:LX/0lp;

    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v2

    .line 529722
    invoke-virtual {v2}, LX/0nX;->d()V

    .line 529723
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hla;

    .line 529724
    const/4 v4, 0x1

    const/4 v9, 0x1

    .line 529725
    if-eqz v4, :cond_0

    .line 529726
    invoke-virtual {v2}, LX/0nX;->f()V

    .line 529727
    :cond_0
    iget-object v5, v0, LX/Hla;->a:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 529728
    const-string v5, "type"

    iget-object v6, v0, LX/Hla;->a:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 529729
    :cond_1
    iget-object v5, v0, LX/Hla;->b:LX/HlX;

    if-eqz v5, :cond_3

    .line 529730
    const-string v5, "coordinates"

    invoke-virtual {v2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 529731
    iget-object v5, v0, LX/Hla;->b:LX/HlX;

    .line 529732
    if-eqz v9, :cond_2

    .line 529733
    invoke-virtual {v2}, LX/0nX;->f()V

    .line 529734
    :cond_2
    const-string v10, "latitude"

    iget-wide v12, v5, LX/HlX;->a:D

    invoke-virtual {v2, v10, v12, v13}, LX/0nX;->a(Ljava/lang/String;D)V

    .line 529735
    const-string v10, "longitude"

    iget-wide v12, v5, LX/HlX;->b:D

    invoke-virtual {v2, v10, v12, v13}, LX/0nX;->a(Ljava/lang/String;D)V

    .line 529736
    if-eqz v9, :cond_3

    .line 529737
    invoke-virtual {v2}, LX/0nX;->g()V

    .line 529738
    :cond_3
    iget-object v5, v0, LX/Hla;->c:Ljava/lang/Long;

    if-eqz v5, :cond_4

    .line 529739
    const-string v5, "age_ms"

    iget-object v6, v0, LX/Hla;->c:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v2, v5, v7, v8}, LX/0nX;->a(Ljava/lang/String;J)V

    .line 529740
    :cond_4
    iget-object v5, v0, LX/Hla;->d:Ljava/lang/Float;

    if-eqz v5, :cond_5

    .line 529741
    const-string v5, "accuracy_meters"

    iget-object v6, v0, LX/Hla;->d:Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v2, v5, v6}, LX/0nX;->a(Ljava/lang/String;F)V

    .line 529742
    :cond_5
    iget-object v5, v0, LX/Hla;->e:Ljava/lang/Double;

    if-eqz v5, :cond_6

    .line 529743
    const-string v5, "altitude_meters"

    iget-object v6, v0, LX/Hla;->e:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-virtual {v2, v5, v7, v8}, LX/0nX;->a(Ljava/lang/String;D)V

    .line 529744
    :cond_6
    iget-object v5, v0, LX/Hla;->f:Ljava/lang/Float;

    if-eqz v5, :cond_7

    .line 529745
    const-string v5, "bearing_degrees"

    iget-object v6, v0, LX/Hla;->f:Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v2, v5, v6}, LX/0nX;->a(Ljava/lang/String;F)V

    .line 529746
    :cond_7
    iget-object v5, v0, LX/Hla;->g:Ljava/lang/Float;

    if-eqz v5, :cond_8

    .line 529747
    const-string v5, "speed_meters_per_sec"

    iget-object v6, v0, LX/Hla;->g:Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v2, v5, v6}, LX/0nX;->a(Ljava/lang/String;F)V

    .line 529748
    :cond_8
    iget-object v5, v0, LX/Hla;->h:Ljava/lang/Long;

    if-eqz v5, :cond_9

    .line 529749
    const-string v5, "start_age_ms"

    iget-object v6, v0, LX/Hla;->h:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v2, v5, v7, v8}, LX/0nX;->a(Ljava/lang/String;J)V

    .line 529750
    :cond_9
    iget-object v5, v0, LX/Hla;->i:Ljava/lang/Long;

    if-eqz v5, :cond_a

    .line 529751
    const-string v5, "end_age_ms"

    iget-object v6, v0, LX/Hla;->i:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v2, v5, v7, v8}, LX/0nX;->a(Ljava/lang/String;J)V

    .line 529752
    :cond_a
    iget-object v5, v0, LX/Hla;->j:Ljava/lang/Integer;

    if-eqz v5, :cond_b

    .line 529753
    const-string v5, "geofence_radius_meters"

    iget-object v6, v0, LX/Hla;->j:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v2, v5, v6}, LX/0nX;->a(Ljava/lang/String;I)V

    .line 529754
    :cond_b
    iget-object v5, v0, LX/Hla;->k:LX/HlY;

    if-eqz v5, :cond_11

    .line 529755
    const-string v5, "wifi_info"

    invoke-virtual {v2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 529756
    iget-object v5, v0, LX/Hla;->k:LX/HlY;

    const/4 v8, 0x1

    .line 529757
    if-eqz v9, :cond_c

    .line 529758
    invoke-virtual {v2}, LX/0nX;->f()V

    .line 529759
    :cond_c
    iget-object v6, v5, LX/HlY;->a:Ljava/util/List;

    if-eqz v6, :cond_f

    .line 529760
    const-string v6, "scan_results"

    invoke-virtual {v2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 529761
    invoke-virtual {v2}, LX/0nX;->d()V

    .line 529762
    iget-object v6, v5, LX/HlY;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_d
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/HlZ;

    .line 529763
    if-eqz v6, :cond_d

    .line 529764
    invoke-static {v2, v6, v8}, LX/Hlg;->a(LX/0nX;LX/HlZ;Z)V

    goto :goto_1

    .line 529765
    :cond_e
    invoke-virtual {v2}, LX/0nX;->e()V

    .line 529766
    :cond_f
    iget-object v6, v5, LX/HlY;->b:LX/HlZ;

    if-eqz v6, :cond_10

    .line 529767
    const-string v6, "connected"

    invoke-virtual {v2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 529768
    iget-object v6, v5, LX/HlY;->b:LX/HlZ;

    invoke-static {v2, v6, v8}, LX/Hlg;->a(LX/0nX;LX/HlZ;Z)V

    .line 529769
    :cond_10
    if-eqz v9, :cond_11

    .line 529770
    invoke-virtual {v2}, LX/0nX;->g()V

    .line 529771
    :cond_11
    iget-object v5, v0, LX/Hla;->l:LX/HlV;

    if-eqz v5, :cond_16

    .line 529772
    const-string v5, "bluetooth_info"

    invoke-virtual {v2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 529773
    iget-object v5, v0, LX/Hla;->l:LX/HlV;

    .line 529774
    if-eqz v9, :cond_12

    .line 529775
    invoke-virtual {v2}, LX/0nX;->f()V

    .line 529776
    :cond_12
    iget-object v6, v5, LX/HlV;->a:Ljava/util/List;

    if-eqz v6, :cond_15

    .line 529777
    const-string v6, "scan_results"

    invoke-virtual {v2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 529778
    invoke-virtual {v2}, LX/0nX;->d()V

    .line 529779
    iget-object v6, v5, LX/HlV;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_13
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_14

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/HlT;

    .line 529780
    if-eqz v6, :cond_13

    .line 529781
    const/4 v8, 0x1

    invoke-static {v2, v6, v8}, LX/Hlb;->a(LX/0nX;LX/HlT;Z)V

    goto :goto_2

    .line 529782
    :cond_14
    invoke-virtual {v2}, LX/0nX;->e()V

    .line 529783
    :cond_15
    if-eqz v9, :cond_16

    .line 529784
    invoke-virtual {v2}, LX/0nX;->g()V

    .line 529785
    :cond_16
    if-eqz v4, :cond_17

    .line 529786
    invoke-virtual {v2}, LX/0nX;->g()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 529787
    :cond_17
    goto/16 :goto_0

    .line 529788
    :catch_0
    const-string v0, "[]"

    :goto_3
    return-object v0

    .line 529789
    :cond_18
    :try_start_1
    invoke-virtual {v2}, LX/0nX;->e()V

    .line 529790
    invoke-virtual {v2}, LX/0nX;->close()V

    .line 529791
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_3
.end method

.method private b(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;)Ljava/util/List;
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 529685
    move-object/from16 v0, p0

    iget-object v6, v0, LX/3C6;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v8

    .line 529686
    move-object/from16 v0, p0

    iget-object v6, v0, LX/3C6;->b:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v10

    .line 529687
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 529688
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->a:LX/0Px;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, LX/0Px;->size()I

    move-result v23

    const/4 v6, 0x0

    move/from16 v20, v6

    :goto_0
    move/from16 v0, v20

    move/from16 v1, v23

    if-ge v0, v1, :cond_b

    move-object/from16 v0, v22

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    move-object v12, v6

    check-cast v12, Lcom/facebook/location/LocationSignalDataPackage;

    .line 529689
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v19

    .line 529690
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v18

    .line 529691
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v13

    .line 529692
    const-wide/high16 v16, -0x8000000000000000L

    .line 529693
    const-wide/high16 v14, -0x8000000000000000L

    .line 529694
    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    if-eqz v6, :cond_0

    .line 529695
    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v6}, Lcom/facebook/location/ImmutableLocation;->i()LX/0am;

    move-result-object v19

    .line 529696
    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v6}, Lcom/facebook/location/ImmutableLocation;->j()LX/0am;

    move-result-object v18

    .line 529697
    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v6}, Lcom/facebook/location/ImmutableLocation;->k()LX/0am;

    move-result-object v13

    .line 529698
    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-static {v6, v8, v9}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;J)J

    move-result-wide v14

    .line 529699
    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-static {v6, v10, v11}, LX/0yD;->b(Lcom/facebook/location/ImmutableLocation;J)J

    move-result-wide v16

    .line 529700
    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v6, v14, v6

    if-eqz v6, :cond_d

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v6, v16, v6

    if-eqz v6, :cond_d

    sub-long v6, v14, v16

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    const-wide/32 v24, 0xea60

    cmp-long v6, v6, v24

    if-lez v6, :cond_d

    .line 529701
    move-object/from16 v0, p0

    iget-object v6, v0, LX/3C6;->d:LX/2Cx;

    iget-object v7, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual/range {v6 .. v11}, LX/2Cx;->a(Lcom/facebook/location/ImmutableLocation;JJ)V

    move-wide/from16 v6, v16

    move-object/from16 v16, v18

    move-object/from16 v17, v19

    .line 529702
    :goto_1
    new-instance v24, LX/HlW;

    invoke-virtual/range {v17 .. v17}, LX/0am;->isPresent()Z

    move-result v18

    if-eqz v18, :cond_1

    const-string v18, "GEOFENCE"

    :goto_2
    iget-object v0, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    move-object/from16 v19, v0

    if-eqz v19, :cond_2

    new-instance v19, LX/HlX;

    iget-object v0, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v26

    iget-object v0, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v28

    move-object/from16 v0, v19

    move-wide/from16 v1, v26

    move-wide/from16 v3, v28

    invoke-direct {v0, v1, v2, v3, v4}, LX/HlX;-><init>(DD)V

    :goto_3
    const-wide/high16 v26, -0x8000000000000000L

    cmp-long v25, v6, v26

    if-eqz v25, :cond_3

    const-wide/16 v26, 0x0

    cmp-long v25, v6, v26

    if-ltz v25, :cond_3

    :goto_4
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    if-eqz v6, :cond_4

    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v6}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v6

    invoke-virtual {v6}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    :goto_5
    move-object/from16 v0, v24

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v7, v6}, LX/HlW;-><init>(Ljava/lang/String;LX/HlX;Ljava/lang/Long;Ljava/lang/Float;)V

    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    if-eqz v6, :cond_5

    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v6}, Lcom/facebook/location/ImmutableLocation;->d()LX/0am;

    move-result-object v6

    invoke-virtual {v6}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Double;

    :goto_6
    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, LX/HlW;->a(Ljava/lang/Double;)LX/HlW;

    move-result-object v7

    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    if-eqz v6, :cond_6

    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v6}, Lcom/facebook/location/ImmutableLocation;->e()LX/0am;

    move-result-object v6

    invoke-virtual {v6}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    :goto_7
    invoke-virtual {v7, v6}, LX/HlW;->a(Ljava/lang/Float;)LX/HlW;

    move-result-object v7

    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    if-eqz v6, :cond_7

    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->a:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v6}, Lcom/facebook/location/ImmutableLocation;->f()LX/0am;

    move-result-object v6

    invoke-virtual {v6}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    :goto_8
    invoke-virtual {v7, v6}, LX/HlW;->b(Ljava/lang/Float;)LX/HlW;

    move-result-object v7

    invoke-virtual/range {v17 .. v17}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-virtual/range {v17 .. v17}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    sub-long v14, v8, v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    :goto_9
    invoke-virtual {v7, v6}, LX/HlW;->a(Ljava/lang/Long;)LX/HlW;

    move-result-object v7

    invoke-virtual/range {v16 .. v16}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual/range {v16 .. v16}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    sub-long v14, v8, v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    :goto_a
    invoke-virtual {v7, v6}, LX/HlW;->b(Ljava/lang/Long;)LX/HlW;

    move-result-object v7

    invoke-virtual {v13}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {v13}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->intValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    :goto_b
    invoke-virtual {v7, v6}, LX/HlW;->a(Ljava/lang/Integer;)LX/HlW;

    move-result-object v6

    iget-object v7, v12, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    iget-object v13, v12, Lcom/facebook/location/LocationSignalDataPackage;->c:Lcom/facebook/wifiscan/WifiScanResult;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/3C6;->a:LX/0SG;

    invoke-static {v7, v13, v14}, LX/HlY;->a(Ljava/util/List;Lcom/facebook/wifiscan/WifiScanResult;LX/0SG;)LX/HlY;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/HlW;->a(LX/HlY;)LX/HlW;

    move-result-object v6

    iget-object v7, v12, Lcom/facebook/location/LocationSignalDataPackage;->g:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/3C6;->a:LX/0SG;

    invoke-static {v7, v12}, LX/HlV;->a(Ljava/util/List;LX/0SG;)LX/HlV;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/HlW;->a(LX/HlV;)LX/HlW;

    move-result-object v6

    invoke-virtual {v6}, LX/HlW;->a()LX/Hla;

    move-result-object v6

    .line 529703
    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 529704
    add-int/lit8 v6, v20, 0x1

    move/from16 v20, v6

    goto/16 :goto_0

    .line 529705
    :cond_0
    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    if-eqz v6, :cond_d

    iget-object v6, v12, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_d

    .line 529706
    const-wide v6, 0x7fffffffffffffffL

    .line 529707
    iget-object v14, v12, Lcom/facebook/location/LocationSignalDataPackage;->d:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    move-wide v14, v6

    :goto_c
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/wifiscan/WifiScanResult;

    .line 529708
    invoke-static {v6, v8, v9}, LX/0yD;->a(Lcom/facebook/wifiscan/WifiScanResult;J)J

    move-result-wide v6

    invoke-static {v6, v7, v14, v15}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    move-wide v14, v6

    .line 529709
    goto :goto_c

    .line 529710
    :cond_1
    const-string v18, "LOCATION"

    goto/16 :goto_2

    :cond_2
    const/16 v19, 0x0

    goto/16 :goto_3

    :cond_3
    move-wide v6, v14

    goto/16 :goto_4

    :cond_4
    const/4 v6, 0x0

    goto/16 :goto_5

    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_6

    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_7

    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_8

    :cond_8
    const/4 v6, 0x0

    goto/16 :goto_9

    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_a

    :cond_a
    const/4 v6, 0x0

    goto :goto_b

    .line 529711
    :cond_b
    invoke-static/range {v21 .. v21}, LX/3C6;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v6

    .line 529712
    new-instance v8, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "locations"

    invoke-direct {v8, v7, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 529713
    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-virtual {v6}, LX/0mC;->b()LX/162;

    move-result-object v9

    .line 529714
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;->b:LX/0Px;

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v6, 0x0

    move v7, v6

    :goto_d
    if-ge v7, v11, :cond_c

    invoke-virtual {v10, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 529715
    invoke-virtual {v9, v6}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 529716
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_d

    .line 529717
    :cond_c
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "trace_ids"

    invoke-virtual {v9}, LX/162;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 529718
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v9, "deviceid"

    move-object/from16 v0, p0

    iget-object v10, v0, LX/3C6;->c:LX/0dC;

    invoke-virtual {v10}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v9, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 529719
    invoke-static {v8, v6, v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    return-object v6

    :cond_d
    move-wide/from16 v6, v16

    move-object/from16 v16, v18

    move-object/from16 v17, v19

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 2

    .prologue
    .line 529668
    check-cast p1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;

    .line 529669
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "update-background-location"

    .line 529670
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 529671
    move-object v0, v0

    .line 529672
    const-string v1, "POST"

    .line 529673
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 529674
    move-object v0, v0

    .line 529675
    const-string v1, "me/locationupdates"

    .line 529676
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 529677
    move-object v0, v0

    .line 529678
    invoke-direct {p0, p1}, LX/3C6;->b(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;)Ljava/util/List;

    move-result-object v1

    .line 529679
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 529680
    move-object v0, v0

    .line 529681
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 529682
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 529683
    move-object v0, v0

    .line 529684
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 529660
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 529661
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 529662
    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 529663
    const-string v2, "tracking"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 529664
    const-string v3, "best_device"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 529665
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v1}, LX/0lF;->p()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, LX/0lF;->p()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, LX/0lF;->p()Z

    move-result v4

    if-nez v4, :cond_1

    .line 529666
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 529667
    :cond_1
    new-instance v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;

    invoke-virtual {v1}, LX/0lF;->F()Z

    move-result v1

    invoke-virtual {v2}, LX/0lF;->F()Z

    move-result v2

    invoke-virtual {v3}, LX/0lF;->F()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;-><init>(ZZZ)V

    return-object v0
.end method
