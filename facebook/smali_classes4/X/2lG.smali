.class public LX/2lG;
.super Lcom/facebook/bookmark/ui/BaseViewItemFactory;
.source ""


# static fields
.field private static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/2lG;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final e:LX/0Zb;

.field private final f:Landroid/content/Context;

.field private final g:LX/0Ym;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 457429
    const-class v0, LX/2lG;

    sput-object v0, LX/2lG;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Landroid/view/LayoutInflater;LX/0Zb;LX/0Ym;LX/0xB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 457436
    invoke-direct {p0, p2, p3, p6}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;-><init>(Landroid/app/Activity;Landroid/view/LayoutInflater;LX/0xB;)V

    .line 457437
    iput-object p1, p0, LX/2lG;->f:Landroid/content/Context;

    .line 457438
    iput-object p4, p0, LX/2lG;->e:LX/0Zb;

    .line 457439
    iput-object p5, p0, LX/2lG;->g:LX/0Ym;

    .line 457440
    return-void
.end method

.method public static a(LX/0QB;)LX/2lG;
    .locals 1

    .prologue
    .line 457435
    invoke-static {p0}, LX/2lG;->b(LX/0QB;)LX/2lG;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/2lG;
    .locals 7

    .prologue
    .line 457433
    new-instance v0, LX/2lG;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, LX/0Ym;->a(LX/0QB;)LX/0Ym;

    move-result-object v5

    check-cast v5, LX/0Ym;

    invoke-static {p0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v6

    check-cast v6, LX/0xB;

    invoke-direct/range {v0 .. v6}, LX/2lG;-><init>(Landroid/content/Context;Landroid/app/Activity;Landroid/view/LayoutInflater;LX/0Zb;LX/0Ym;LX/0xB;)V

    .line 457434
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/bookmark/model/Bookmark;)I
    .locals 1

    .prologue
    .line 457430
    invoke-super {p0, p1}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(Lcom/facebook/bookmark/model/Bookmark;)I

    move-result v0

    .line 457431
    if-lez v0, :cond_0

    .line 457432
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
