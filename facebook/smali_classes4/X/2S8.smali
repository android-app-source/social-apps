.class public LX/2S8;
.super LX/12F;
.source ""


# static fields
.field public static final m:[B

.field private static final v:[B

.field private static final w:[B

.field private static final x:[B


# instance fields
.field public final n:Ljava/io/OutputStream;

.field public o:[B

.field public p:I

.field public final q:I

.field public final r:I

.field public s:[C

.field public final t:I

.field public u:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 411609
    invoke-static {}, LX/12H;->h()[B

    move-result-object v0

    sput-object v0, LX/2S8;->m:[B

    .line 411610
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/2S8;->v:[B

    .line 411611
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, LX/2S8;->w:[B

    .line 411612
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, LX/2S8;->x:[B

    return-void

    .line 411613
    :array_0
    .array-data 1
        0x6et
        0x75t
        0x6ct
        0x6ct
    .end array-data

    .line 411614
    :array_1
    .array-data 1
        0x74t
        0x72t
        0x75t
        0x65t
    .end array-data

    .line 411615
    :array_2
    .array-data 1
        0x66t
        0x61t
        0x6ct
        0x73t
        0x65t
    .end array-data
.end method

.method public constructor <init>(LX/12A;ILX/0lD;Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 411434
    invoke-direct {p0, p1, p2, p3}, LX/12F;-><init>(LX/12A;ILX/0lD;)V

    .line 411435
    const/4 v0, 0x0

    iput v0, p0, LX/2S8;->p:I

    .line 411436
    iput-object p4, p0, LX/2S8;->n:Ljava/io/OutputStream;

    .line 411437
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2S8;->u:Z

    .line 411438
    invoke-virtual {p1}, LX/12A;->f()[B

    move-result-object v0

    iput-object v0, p0, LX/2S8;->o:[B

    .line 411439
    iget-object v0, p0, LX/2S8;->o:[B

    array-length v0, v0

    iput v0, p0, LX/2S8;->q:I

    .line 411440
    iget v0, p0, LX/2S8;->q:I

    shr-int/lit8 v0, v0, 0x3

    iput v0, p0, LX/2S8;->r:I

    .line 411441
    invoke-virtual {p1}, LX/12A;->h()[C

    move-result-object v0

    iput-object v0, p0, LX/2S8;->s:[C

    .line 411442
    iget-object v0, p0, LX/2S8;->s:[C

    array-length v0, v0

    iput v0, p0, LX/2S8;->t:I

    .line 411443
    sget-object v0, LX/0ls;->ESCAPE_NON_ASCII:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411444
    const/16 v0, 0x7f

    invoke-virtual {p0, v0}, LX/0nX;->a(I)LX/0nX;

    .line 411445
    :cond_0
    return-void
.end method

.method private a(I[CII)I
    .locals 3

    .prologue
    .line 411446
    const v0, 0xd800

    if-lt p1, v0, :cond_1

    .line 411447
    const v0, 0xdfff

    if-gt p1, v0, :cond_1

    .line 411448
    if-lt p3, p4, :cond_0

    .line 411449
    const-string v0, "Split surrogate on writeRaw() input (last character)"

    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 411450
    :cond_0
    aget-char v0, p2, p3

    invoke-direct {p0, p1, v0}, LX/2S8;->a(II)V

    .line 411451
    add-int/lit8 p3, p3, 0x1

    .line 411452
    :goto_0
    return p3

    .line 411453
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    .line 411454
    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    shr-int/lit8 v2, p1, 0xc

    or-int/lit16 v2, v2, 0xe0

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 411455
    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    shr-int/lit8 v2, p1, 0x6

    and-int/lit8 v2, v2, 0x3f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 411456
    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    and-int/lit8 v2, p1, 0x3f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    goto :goto_0
.end method

.method private a([BII[BI)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 411457
    array-length v1, p4

    .line 411458
    add-int v0, p2, v1

    if-le v0, p3, :cond_2

    .line 411459
    iput p2, p0, LX/2S8;->p:I

    .line 411460
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411461
    iget v0, p0, LX/2S8;->p:I

    .line 411462
    array-length v2, p1

    if-le v1, v2, :cond_1

    .line 411463
    iget-object v2, p0, LX/2S8;->n:Ljava/io/OutputStream;

    invoke-virtual {v2, p4, v3, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 411464
    :cond_0
    :goto_0
    return v0

    .line 411465
    :cond_1
    invoke-static {p4, v3, p1, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 411466
    add-int/2addr v0, v1

    .line 411467
    :goto_1
    mul-int/lit8 v1, p5, 0x6

    add-int/2addr v1, v0

    if-le v1, p3, :cond_0

    .line 411468
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411469
    iget v0, p0, LX/2S8;->p:I

    goto :goto_0

    :cond_2
    move v0, p2

    goto :goto_1
.end method

.method private a([BILX/0lc;I)I
    .locals 6

    .prologue
    .line 411470
    invoke-interface {p3}, LX/0lc;->d()[B

    move-result-object v4

    .line 411471
    array-length v0, v4

    .line 411472
    const/4 v1, 0x6

    if-le v0, v1, :cond_0

    .line 411473
    iget v3, p0, LX/2S8;->q:I

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/2S8;->a([BII[BI)I

    move-result v0

    .line 411474
    :goto_0
    return v0

    .line 411475
    :cond_0
    const/4 v1, 0x0

    invoke-static {v4, v1, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 411476
    add-int/2addr v0, p2

    goto :goto_0
.end method

.method private a(II)V
    .locals 4

    .prologue
    .line 411477
    invoke-static {p1, p2}, LX/2S8;->c(II)I

    move-result v0

    .line 411478
    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v1, v1, 0x4

    iget v2, p0, LX/2S8;->q:I

    if-le v1, v2, :cond_0

    .line 411479
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411480
    :cond_0
    iget-object v1, p0, LX/2S8;->o:[B

    .line 411481
    iget v2, p0, LX/2S8;->p:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/2S8;->p:I

    shr-int/lit8 v3, v0, 0x12

    or-int/lit16 v3, v3, 0xf0

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 411482
    iget v2, p0, LX/2S8;->p:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/2S8;->p:I

    shr-int/lit8 v3, v0, 0xc

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 411483
    iget v2, p0, LX/2S8;->p:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/2S8;->p:I

    shr-int/lit8 v3, v0, 0x6

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 411484
    iget v2, p0, LX/2S8;->p:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/2S8;->p:I

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    .line 411485
    return-void
.end method

.method private a(LX/0lc;Z)V
    .locals 5

    .prologue
    const/16 v4, 0x22

    .line 411486
    if-eqz p2, :cond_4

    .line 411487
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->c(LX/0nX;)V

    .line 411488
    :goto_0
    sget-object v0, LX/0ls;->QUOTE_FIELD_NAMES:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    .line 411489
    if-eqz v0, :cond_1

    .line 411490
    iget v1, p0, LX/2S8;->p:I

    iget v2, p0, LX/2S8;->q:I

    if-lt v1, v2, :cond_0

    .line 411491
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411492
    :cond_0
    iget-object v1, p0, LX/2S8;->o:[B

    iget v2, p0, LX/2S8;->p:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/2S8;->p:I

    aput-byte v4, v1, v2

    .line 411493
    :cond_1
    invoke-interface {p1}, LX/0lc;->e()[B

    move-result-object v1

    invoke-direct {p0, v1}, LX/2S8;->b([B)V

    .line 411494
    if-eqz v0, :cond_3

    .line 411495
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_2

    .line 411496
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411497
    :cond_2
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v4, v0, v1

    .line 411498
    :cond_3
    return-void

    .line 411499
    :cond_4
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->h(LX/0nX;)V

    goto :goto_0
.end method

.method private b(II)I
    .locals 5

    .prologue
    .line 411500
    iget-object v1, p0, LX/2S8;->o:[B

    .line 411501
    const v0, 0xd800

    if-lt p1, v0, :cond_0

    const v0, 0xdfff

    if-gt p1, v0, :cond_0

    .line 411502
    add-int/lit8 v0, p2, 0x1

    const/16 v2, 0x5c

    aput-byte v2, v1, p2

    .line 411503
    add-int/lit8 v2, v0, 0x1

    const/16 v3, 0x75

    aput-byte v3, v1, v0

    .line 411504
    add-int/lit8 v0, v2, 0x1

    sget-object v3, LX/2S8;->m:[B

    shr-int/lit8 v4, p1, 0xc

    and-int/lit8 v4, v4, 0xf

    aget-byte v3, v3, v4

    aput-byte v3, v1, v2

    .line 411505
    add-int/lit8 v2, v0, 0x1

    sget-object v3, LX/2S8;->m:[B

    shr-int/lit8 v4, p1, 0x8

    and-int/lit8 v4, v4, 0xf

    aget-byte v3, v3, v4

    aput-byte v3, v1, v0

    .line 411506
    add-int/lit8 v3, v2, 0x1

    sget-object v0, LX/2S8;->m:[B

    shr-int/lit8 v4, p1, 0x4

    and-int/lit8 v4, v4, 0xf

    aget-byte v0, v0, v4

    aput-byte v0, v1, v2

    .line 411507
    add-int/lit8 v0, v3, 0x1

    sget-object v2, LX/2S8;->m:[B

    and-int/lit8 v4, p1, 0xf

    aget-byte v2, v2, v4

    aput-byte v2, v1, v3

    .line 411508
    :goto_0
    return v0

    .line 411509
    :cond_0
    add-int/lit8 v0, p2, 0x1

    shr-int/lit8 v2, p1, 0xc

    or-int/lit16 v2, v2, 0xe0

    int-to-byte v2, v2

    aput-byte v2, v1, p2

    .line 411510
    add-int/lit8 v2, v0, 0x1

    shr-int/lit8 v3, p1, 0x6

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v1, v0

    .line 411511
    add-int/lit8 v0, v2, 0x1

    and-int/lit8 v3, p1, 0x3f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    goto :goto_0
.end method

.method private b(J)V
    .locals 5

    .prologue
    const/16 v3, 0x22

    .line 411512
    iget v0, p0, LX/2S8;->p:I

    add-int/lit8 v0, v0, 0x17

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411513
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411514
    :cond_0
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411515
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    invoke-static {p1, p2, v0, v1}, LX/13I;->a(J[BI)I

    move-result v0

    iput v0, p0, LX/2S8;->p:I

    .line 411516
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411517
    return-void
.end method

.method private b(LX/0ln;[BII)V
    .locals 6

    .prologue
    .line 411518
    add-int/lit8 v1, p4, -0x3

    .line 411519
    iget v0, p0, LX/2S8;->q:I

    add-int/lit8 v2, v0, -0x6

    .line 411520
    iget v0, p1, LX/0ln;->c:I

    move v0, v0

    .line 411521
    shr-int/lit8 v0, v0, 0x2

    .line 411522
    :cond_0
    :goto_0
    if-gt p3, v1, :cond_2

    .line 411523
    iget v3, p0, LX/2S8;->p:I

    if-le v3, v2, :cond_1

    .line 411524
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411525
    :cond_1
    add-int/lit8 v3, p3, 0x1

    aget-byte v4, p2, p3

    shl-int/lit8 v4, v4, 0x8

    .line 411526
    add-int/lit8 v5, v3, 0x1

    aget-byte v3, p2, v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v3, v4

    .line 411527
    shl-int/lit8 v3, v3, 0x8

    add-int/lit8 p3, v5, 0x1

    aget-byte v4, p2, v5

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    .line 411528
    iget-object v4, p0, LX/2S8;->o:[B

    iget v5, p0, LX/2S8;->p:I

    invoke-virtual {p1, v3, v4, v5}, LX/0ln;->a(I[BI)I

    move-result v3

    iput v3, p0, LX/2S8;->p:I

    .line 411529
    add-int/lit8 v0, v0, -0x1

    if-gtz v0, :cond_0

    .line 411530
    iget-object v0, p0, LX/2S8;->o:[B

    iget v3, p0, LX/2S8;->p:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/2S8;->p:I

    const/16 v4, 0x5c

    aput-byte v4, v0, v3

    .line 411531
    iget-object v0, p0, LX/2S8;->o:[B

    iget v3, p0, LX/2S8;->p:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/2S8;->p:I

    const/16 v4, 0x6e

    aput-byte v4, v0, v3

    .line 411532
    iget v0, p1, LX/0ln;->c:I

    move v0, v0

    .line 411533
    shr-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 411534
    :cond_2
    sub-int v1, p4, p3

    .line 411535
    if-lez v1, :cond_5

    .line 411536
    iget v0, p0, LX/2S8;->p:I

    if-le v0, v2, :cond_3

    .line 411537
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411538
    :cond_3
    add-int/lit8 v2, p3, 0x1

    aget-byte v0, p2, p3

    shl-int/lit8 v0, v0, 0x10

    .line 411539
    const/4 v3, 0x2

    if-ne v1, v3, :cond_4

    .line 411540
    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v0, v2

    .line 411541
    :cond_4
    iget-object v2, p0, LX/2S8;->o:[B

    iget v3, p0, LX/2S8;->p:I

    invoke-virtual {p1, v0, v1, v2, v3}, LX/0ln;->a(II[BI)I

    move-result v0

    iput v0, p0, LX/2S8;->p:I

    .line 411542
    :cond_5
    return-void
.end method

.method private b(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 411543
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411544
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411545
    :cond_0
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411546
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    .line 411547
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_1

    .line 411548
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411549
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411550
    return-void
.end method

.method private b(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/16 v4, 0x22

    const/4 v3, 0x0

    .line 411551
    if-eqz p2, :cond_3

    .line 411552
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->c(LX/0nX;)V

    .line 411553
    :goto_0
    sget-object v0, LX/0ls;->QUOTE_FIELD_NAMES:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 411554
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411555
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411556
    :cond_0
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v4, v0, v1

    .line 411557
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 411558
    iget v1, p0, LX/2S8;->t:I

    if-gt v0, v1, :cond_5

    .line 411559
    iget-object v1, p0, LX/2S8;->s:[C

    invoke-virtual {p1, v3, v0, v1, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 411560
    iget v1, p0, LX/2S8;->r:I

    if-gt v0, v1, :cond_4

    .line 411561
    iget v1, p0, LX/2S8;->p:I

    add-int/2addr v1, v0

    iget v2, p0, LX/2S8;->q:I

    if-le v1, v2, :cond_1

    .line 411562
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411563
    :cond_1
    iget-object v1, p0, LX/2S8;->s:[C

    invoke-direct {p0, v1, v3, v0}, LX/2S8;->e([CII)V

    .line 411564
    :goto_1
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_2

    .line 411565
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411566
    :cond_2
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v4, v0, v1

    .line 411567
    :goto_2
    return-void

    .line 411568
    :cond_3
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->h(LX/0nX;)V

    goto :goto_0

    .line 411569
    :cond_4
    iget-object v1, p0, LX/2S8;->s:[C

    invoke-direct {p0, v1, v3, v0}, LX/2S8;->d([CII)V

    goto :goto_1

    .line 411570
    :cond_5
    invoke-direct {p0, p1}, LX/2S8;->l(Ljava/lang/String;)V

    goto :goto_1

    .line 411571
    :cond_6
    invoke-direct {p0, p1}, LX/2S8;->l(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private b(S)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 411572
    iget v0, p0, LX/2S8;->p:I

    add-int/lit8 v0, v0, 0x8

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411573
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411574
    :cond_0
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411575
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    invoke-static {p1, v0, v1}, LX/13I;->a(I[BI)I

    move-result v0

    iput v0, p0, LX/2S8;->p:I

    .line 411576
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411577
    return-void
.end method

.method private final b([B)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 411578
    array-length v0, p1

    .line 411579
    iget v1, p0, LX/2S8;->p:I

    add-int/2addr v1, v0

    iget v2, p0, LX/2S8;->q:I

    if-le v1, v2, :cond_0

    .line 411580
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411581
    const/16 v1, 0x200

    if-le v0, v1, :cond_0

    .line 411582
    iget-object v1, p0, LX/2S8;->n:Ljava/io/OutputStream;

    invoke-virtual {v1, p1, v3, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 411583
    :goto_0
    return-void

    .line 411584
    :cond_0
    iget-object v1, p0, LX/2S8;->o:[B

    iget v2, p0, LX/2S8;->p:I

    invoke-static {p1, v3, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 411585
    iget v1, p0, LX/2S8;->p:I

    add-int/2addr v0, v1

    iput v0, p0, LX/2S8;->p:I

    goto :goto_0
.end method

.method private static c(II)I
    .locals 3

    .prologue
    const v2, 0xdc00

    .line 411586
    if-lt p1, v2, :cond_0

    const v0, 0xdfff

    if-le p1, v0, :cond_1

    .line 411587
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Incomplete surrogate pair: first char 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", second 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 411588
    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 411589
    :cond_1
    const/high16 v0, 0x10000

    const v1, 0xd800

    sub-int v1, p0, v1

    shl-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    sub-int v1, p1, v2

    add-int/2addr v0, v1

    .line 411590
    return v0
.end method

.method private c(I)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 411805
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411806
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411807
    :cond_0
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411808
    iget-object v0, p0, LX/2S8;->s:[C

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, LX/2S8;->d([CII)V

    .line 411809
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_1

    .line 411810
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411811
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411812
    return-void
.end method

.method private final c([CII)V
    .locals 6

    .prologue
    .line 411591
    iget v1, p0, LX/2S8;->q:I

    .line 411592
    iget-object v2, p0, LX/2S8;->o:[B

    move v0, p2

    .line 411593
    :goto_0
    if-ge v0, p3, :cond_2

    .line 411594
    :cond_0
    aget-char v3, p1, v0

    .line 411595
    const/16 v4, 0x80

    if-ge v3, v4, :cond_3

    .line 411596
    iget v4, p0, LX/2S8;->p:I

    if-lt v4, v1, :cond_1

    .line 411597
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411598
    :cond_1
    iget v4, p0, LX/2S8;->p:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/2S8;->p:I

    int-to-byte v3, v3

    aput-byte v3, v2, v4

    .line 411599
    add-int/lit8 v0, v0, 0x1

    if-lt v0, p3, :cond_0

    .line 411600
    :cond_2
    return-void

    .line 411601
    :cond_3
    iget v3, p0, LX/2S8;->p:I

    add-int/lit8 v3, v3, 0x3

    iget v4, p0, LX/2S8;->q:I

    if-lt v3, v4, :cond_4

    .line 411602
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411603
    :cond_4
    add-int/lit8 p2, v0, 0x1

    aget-char v0, p1, v0

    .line 411604
    const/16 v3, 0x800

    if-ge v0, v3, :cond_5

    .line 411605
    iget v3, p0, LX/2S8;->p:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/2S8;->p:I

    shr-int/lit8 v4, v0, 0x6

    or-int/lit16 v4, v4, 0xc0

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 411606
    iget v3, p0, LX/2S8;->p:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/2S8;->p:I

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    move v0, p2

    goto :goto_0

    .line 411607
    :cond_5
    invoke-direct {p0, v0, p1, p2, p3}, LX/2S8;->a(I[CII)I

    move v0, p2

    .line 411608
    goto :goto_0
.end method

.method private d(II)I
    .locals 6

    .prologue
    const/16 v4, 0x30

    .line 411792
    iget-object v1, p0, LX/2S8;->o:[B

    .line 411793
    add-int/lit8 v0, p2, 0x1

    const/16 v2, 0x5c

    aput-byte v2, v1, p2

    .line 411794
    add-int/lit8 v2, v0, 0x1

    const/16 v3, 0x75

    aput-byte v3, v1, v0

    .line 411795
    const/16 v0, 0xff

    if-le p1, v0, :cond_0

    .line 411796
    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v3, v0, 0xff

    .line 411797
    add-int/lit8 v4, v2, 0x1

    sget-object v0, LX/2S8;->m:[B

    shr-int/lit8 v5, v3, 0x4

    aget-byte v0, v0, v5

    aput-byte v0, v1, v2

    .line 411798
    add-int/lit8 v0, v4, 0x1

    sget-object v2, LX/2S8;->m:[B

    and-int/lit8 v3, v3, 0xf

    aget-byte v2, v2, v3

    aput-byte v2, v1, v4

    .line 411799
    and-int/lit16 p1, p1, 0xff

    .line 411800
    :goto_0
    add-int/lit8 v2, v0, 0x1

    sget-object v3, LX/2S8;->m:[B

    shr-int/lit8 v4, p1, 0x4

    aget-byte v3, v3, v4

    aput-byte v3, v1, v0

    .line 411801
    add-int/lit8 v0, v2, 0x1

    sget-object v3, LX/2S8;->m:[B

    and-int/lit8 v4, p1, 0xf

    aget-byte v3, v3, v4

    aput-byte v3, v1, v2

    .line 411802
    return v0

    .line 411803
    :cond_0
    add-int/lit8 v3, v2, 0x1

    aput-byte v4, v1, v2

    .line 411804
    add-int/lit8 v0, v3, 0x1

    aput-byte v4, v1, v3

    goto :goto_0
.end method

.method private d(I)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 411786
    iget v0, p0, LX/2S8;->p:I

    add-int/lit8 v0, v0, 0xd

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411787
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411788
    :cond_0
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411789
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    invoke-static {p1, v0, v1}, LX/13I;->a(I[BI)I

    move-result v0

    iput v0, p0, LX/2S8;->p:I

    .line 411790
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411791
    return-void
.end method

.method private final d([CII)V
    .locals 3

    .prologue
    .line 411778
    :cond_0
    iget v0, p0, LX/2S8;->r:I

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 411779
    iget v1, p0, LX/2S8;->p:I

    add-int/2addr v1, v0

    iget v2, p0, LX/2S8;->q:I

    if-le v1, v2, :cond_1

    .line 411780
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411781
    :cond_1
    invoke-direct {p0, p1, p2, v0}, LX/2S8;->e([CII)V

    .line 411782
    add-int/2addr p2, v0

    .line 411783
    sub-int/2addr p3, v0

    .line 411784
    if-gtz p3, :cond_0

    .line 411785
    return-void
.end method

.method private e(I)V
    .locals 1

    .prologue
    .line 411768
    packed-switch p1, :pswitch_data_0

    .line 411769
    invoke-static {}, LX/4pl;->b()V

    .line 411770
    :cond_0
    :goto_0
    return-void

    .line 411771
    :pswitch_0
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->f(LX/0nX;)V

    goto :goto_0

    .line 411772
    :pswitch_1
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->d(LX/0nX;)V

    goto :goto_0

    .line 411773
    :pswitch_2
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->a(LX/0nX;)V

    goto :goto_0

    .line 411774
    :pswitch_3
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12V;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 411775
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->g(LX/0nX;)V

    goto :goto_0

    .line 411776
    :cond_1
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12V;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411777
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->h(LX/0nX;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private e(LX/0lc;)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 411752
    sget-object v0, LX/0ls;->QUOTE_FIELD_NAMES:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 411753
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    invoke-interface {p1, v0, v1}, LX/0lc;->a([BI)I

    move-result v0

    .line 411754
    if-gez v0, :cond_0

    .line 411755
    invoke-interface {p1}, LX/0lc;->e()[B

    move-result-object v0

    invoke-direct {p0, v0}, LX/2S8;->b([B)V

    .line 411756
    :goto_0
    return-void

    .line 411757
    :cond_0
    iget v1, p0, LX/2S8;->p:I

    add-int/2addr v0, v1

    iput v0, p0, LX/2S8;->p:I

    goto :goto_0

    .line 411758
    :cond_1
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_2

    .line 411759
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411760
    :cond_2
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411761
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    invoke-interface {p1, v0, v1}, LX/0lc;->a([BI)I

    move-result v0

    .line 411762
    if-gez v0, :cond_4

    .line 411763
    invoke-interface {p1}, LX/0lc;->e()[B

    move-result-object v0

    invoke-direct {p0, v0}, LX/2S8;->b([B)V

    .line 411764
    :goto_1
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_3

    .line 411765
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411766
    :cond_3
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    goto :goto_0

    .line 411767
    :cond_4
    iget v1, p0, LX/2S8;->p:I

    add-int/2addr v0, v1

    iput v0, p0, LX/2S8;->p:I

    goto :goto_1
.end method

.method private final e([CII)V
    .locals 6

    .prologue
    .line 411734
    add-int v2, p3, p2

    .line 411735
    iget v0, p0, LX/2S8;->p:I

    .line 411736
    iget-object v3, p0, LX/2S8;->o:[B

    .line 411737
    iget-object v4, p0, LX/12F;->i:[I

    .line 411738
    :goto_0
    if-ge p2, v2, :cond_0

    .line 411739
    aget-char v5, p1, p2

    .line 411740
    const/16 v1, 0x7f

    if-gt v5, v1, :cond_0

    aget v1, v4, v5

    if-nez v1, :cond_0

    .line 411741
    add-int/lit8 v1, v0, 0x1

    int-to-byte v5, v5

    aput-byte v5, v3, v0

    .line 411742
    add-int/lit8 p2, p2, 0x1

    move v0, v1

    .line 411743
    goto :goto_0

    .line 411744
    :cond_0
    iput v0, p0, LX/2S8;->p:I

    .line 411745
    if-ge p2, v2, :cond_1

    .line 411746
    iget-object v0, p0, LX/12F;->k:LX/4pa;

    if-eqz v0, :cond_2

    .line 411747
    invoke-direct {p0, p1, p2, v2}, LX/2S8;->h([CII)V

    .line 411748
    :cond_1
    :goto_1
    return-void

    .line 411749
    :cond_2
    iget v0, p0, LX/12F;->j:I

    if-nez v0, :cond_3

    .line 411750
    invoke-direct {p0, p1, p2, v2}, LX/2S8;->f([CII)V

    goto :goto_1

    .line 411751
    :cond_3
    invoke-direct {p0, p1, p2, v2}, LX/2S8;->g([CII)V

    goto :goto_1
.end method

.method private final f([CII)V
    .locals 7

    .prologue
    .line 411710
    iget v0, p0, LX/2S8;->p:I

    sub-int v1, p3, p2

    mul-int/lit8 v1, v1, 0x6

    add-int/2addr v0, v1

    iget v1, p0, LX/2S8;->q:I

    if-le v0, v1, :cond_0

    .line 411711
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411712
    :cond_0
    iget v0, p0, LX/2S8;->p:I

    .line 411713
    iget-object v3, p0, LX/2S8;->o:[B

    .line 411714
    iget-object v4, p0, LX/12F;->i:[I

    .line 411715
    :goto_0
    if-ge p2, p3, :cond_5

    .line 411716
    add-int/lit8 v2, p2, 0x1

    aget-char v5, p1, p2

    .line 411717
    const/16 v1, 0x7f

    if-gt v5, v1, :cond_3

    .line 411718
    aget v1, v4, v5

    if-nez v1, :cond_1

    .line 411719
    add-int/lit8 v1, v0, 0x1

    int-to-byte v5, v5

    aput-byte v5, v3, v0

    move v0, v1

    move p2, v2

    .line 411720
    goto :goto_0

    .line 411721
    :cond_1
    aget v1, v4, v5

    .line 411722
    if-lez v1, :cond_2

    .line 411723
    add-int/lit8 v5, v0, 0x1

    const/16 v6, 0x5c

    aput-byte v6, v3, v0

    .line 411724
    add-int/lit8 v0, v5, 0x1

    int-to-byte v1, v1

    aput-byte v1, v3, v5

    move p2, v2

    goto :goto_0

    .line 411725
    :cond_2
    invoke-direct {p0, v5, v0}, LX/2S8;->d(II)I

    move-result v0

    move p2, v2

    .line 411726
    goto :goto_0

    .line 411727
    :cond_3
    const/16 v1, 0x7ff

    if-gt v5, v1, :cond_4

    .line 411728
    add-int/lit8 v1, v0, 0x1

    shr-int/lit8 v6, v5, 0x6

    or-int/lit16 v6, v6, 0xc0

    int-to-byte v6, v6

    aput-byte v6, v3, v0

    .line 411729
    add-int/lit8 v0, v1, 0x1

    and-int/lit8 v5, v5, 0x3f

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    aput-byte v5, v3, v1

    move p2, v2

    goto :goto_0

    .line 411730
    :cond_4
    invoke-direct {p0, v5, v0}, LX/2S8;->b(II)I

    move-result v0

    move p2, v2

    .line 411731
    goto :goto_0

    .line 411732
    :cond_5
    iput v0, p0, LX/2S8;->p:I

    .line 411733
    return-void
.end method

.method private final g([CII)V
    .locals 8

    .prologue
    .line 411682
    iget v0, p0, LX/2S8;->p:I

    sub-int v1, p3, p2

    mul-int/lit8 v1, v1, 0x6

    add-int/2addr v0, v1

    iget v1, p0, LX/2S8;->q:I

    if-le v0, v1, :cond_0

    .line 411683
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411684
    :cond_0
    iget v0, p0, LX/2S8;->p:I

    .line 411685
    iget-object v3, p0, LX/2S8;->o:[B

    .line 411686
    iget-object v4, p0, LX/12F;->i:[I

    .line 411687
    iget v5, p0, LX/12F;->j:I

    .line 411688
    :goto_0
    if-ge p2, p3, :cond_6

    .line 411689
    add-int/lit8 v2, p2, 0x1

    aget-char v6, p1, p2

    .line 411690
    const/16 v1, 0x7f

    if-gt v6, v1, :cond_3

    .line 411691
    aget v1, v4, v6

    if-nez v1, :cond_1

    .line 411692
    add-int/lit8 v1, v0, 0x1

    int-to-byte v6, v6

    aput-byte v6, v3, v0

    move v0, v1

    move p2, v2

    .line 411693
    goto :goto_0

    .line 411694
    :cond_1
    aget v1, v4, v6

    .line 411695
    if-lez v1, :cond_2

    .line 411696
    add-int/lit8 v6, v0, 0x1

    const/16 v7, 0x5c

    aput-byte v7, v3, v0

    .line 411697
    add-int/lit8 v0, v6, 0x1

    int-to-byte v1, v1

    aput-byte v1, v3, v6

    move p2, v2

    goto :goto_0

    .line 411698
    :cond_2
    invoke-direct {p0, v6, v0}, LX/2S8;->d(II)I

    move-result v0

    move p2, v2

    .line 411699
    goto :goto_0

    .line 411700
    :cond_3
    if-le v6, v5, :cond_4

    .line 411701
    invoke-direct {p0, v6, v0}, LX/2S8;->d(II)I

    move-result v0

    move p2, v2

    .line 411702
    goto :goto_0

    .line 411703
    :cond_4
    const/16 v1, 0x7ff

    if-gt v6, v1, :cond_5

    .line 411704
    add-int/lit8 v1, v0, 0x1

    shr-int/lit8 v7, v6, 0x6

    or-int/lit16 v7, v7, 0xc0

    int-to-byte v7, v7

    aput-byte v7, v3, v0

    .line 411705
    add-int/lit8 v0, v1, 0x1

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v3, v1

    move p2, v2

    goto :goto_0

    .line 411706
    :cond_5
    invoke-direct {p0, v6, v0}, LX/2S8;->b(II)I

    move-result v0

    move p2, v2

    .line 411707
    goto :goto_0

    .line 411708
    :cond_6
    iput v0, p0, LX/2S8;->p:I

    .line 411709
    return-void
.end method

.method private h([CII)V
    .locals 10

    .prologue
    .line 411642
    iget v0, p0, LX/2S8;->p:I

    sub-int v1, p3, p2

    mul-int/lit8 v1, v1, 0x6

    add-int/2addr v0, v1

    iget v1, p0, LX/2S8;->q:I

    if-le v0, v1, :cond_0

    .line 411643
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411644
    :cond_0
    iget v1, p0, LX/2S8;->p:I

    .line 411645
    iget-object v4, p0, LX/2S8;->o:[B

    .line 411646
    iget-object v5, p0, LX/12F;->i:[I

    .line 411647
    iget v0, p0, LX/12F;->j:I

    if-gtz v0, :cond_1

    const v0, 0xffff

    .line 411648
    :goto_0
    iget-object v6, p0, LX/12F;->k:LX/4pa;

    .line 411649
    :goto_1
    if-ge p2, p3, :cond_a

    .line 411650
    add-int/lit8 v3, p2, 0x1

    aget-char v7, p1, p2

    .line 411651
    const/16 v2, 0x7f

    if-gt v7, v2, :cond_6

    .line 411652
    aget v2, v5, v7

    if-nez v2, :cond_2

    .line 411653
    add-int/lit8 v2, v1, 0x1

    int-to-byte v7, v7

    aput-byte v7, v4, v1

    move v1, v2

    move p2, v3

    .line 411654
    goto :goto_1

    .line 411655
    :cond_1
    iget v0, p0, LX/12F;->j:I

    goto :goto_0

    .line 411656
    :cond_2
    aget v2, v5, v7

    .line 411657
    if-lez v2, :cond_3

    .line 411658
    add-int/lit8 v7, v1, 0x1

    const/16 v8, 0x5c

    aput-byte v8, v4, v1

    .line 411659
    add-int/lit8 v1, v7, 0x1

    int-to-byte v2, v2

    aput-byte v2, v4, v7

    move p2, v3

    goto :goto_1

    .line 411660
    :cond_3
    const/4 v8, -0x2

    if-ne v2, v8, :cond_5

    .line 411661
    invoke-virtual {v6}, LX/4pa;->b()LX/0lc;

    move-result-object v2

    .line 411662
    if-nez v2, :cond_4

    .line 411663
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Invalid custom escape definitions; custom escape not found for character code 0x"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", although was supposed to have one"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/12G;->i(Ljava/lang/String;)V

    .line 411664
    :cond_4
    sub-int v7, p3, v3

    invoke-direct {p0, v4, v1, v2, v7}, LX/2S8;->a([BILX/0lc;I)I

    move-result v1

    move p2, v3

    .line 411665
    goto :goto_1

    .line 411666
    :cond_5
    invoke-direct {p0, v7, v1}, LX/2S8;->d(II)I

    move-result v1

    move p2, v3

    .line 411667
    goto :goto_1

    .line 411668
    :cond_6
    if-le v7, v0, :cond_7

    .line 411669
    invoke-direct {p0, v7, v1}, LX/2S8;->d(II)I

    move-result v1

    move p2, v3

    .line 411670
    goto :goto_1

    .line 411671
    :cond_7
    invoke-virtual {v6}, LX/4pa;->b()LX/0lc;

    move-result-object v2

    .line 411672
    if-eqz v2, :cond_8

    .line 411673
    sub-int v7, p3, v3

    invoke-direct {p0, v4, v1, v2, v7}, LX/2S8;->a([BILX/0lc;I)I

    move-result v1

    move p2, v3

    .line 411674
    goto :goto_1

    .line 411675
    :cond_8
    const/16 v2, 0x7ff

    if-gt v7, v2, :cond_9

    .line 411676
    add-int/lit8 v2, v1, 0x1

    shr-int/lit8 v8, v7, 0x6

    or-int/lit16 v8, v8, 0xc0

    int-to-byte v8, v8

    aput-byte v8, v4, v1

    .line 411677
    add-int/lit8 v1, v2, 0x1

    and-int/lit8 v7, v7, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v4, v2

    move p2, v3

    goto/16 :goto_1

    .line 411678
    :cond_9
    invoke-direct {p0, v7, v1}, LX/2S8;->b(II)I

    move-result v1

    move p2, v3

    .line 411679
    goto/16 :goto_1

    .line 411680
    :cond_a
    iput v1, p0, LX/2S8;->p:I

    .line 411681
    return-void
.end method

.method private j(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/16 v4, 0x22

    const/4 v3, 0x0

    .line 411624
    sget-object v0, LX/0ls;->QUOTE_FIELD_NAMES:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 411625
    invoke-direct {p0, p1}, LX/2S8;->l(Ljava/lang/String;)V

    .line 411626
    :goto_0
    return-void

    .line 411627
    :cond_0
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_1

    .line 411628
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411629
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v4, v0, v1

    .line 411630
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 411631
    iget v1, p0, LX/2S8;->t:I

    if-gt v0, v1, :cond_5

    .line 411632
    iget-object v1, p0, LX/2S8;->s:[C

    invoke-virtual {p1, v3, v0, v1, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 411633
    iget v1, p0, LX/2S8;->r:I

    if-gt v0, v1, :cond_4

    .line 411634
    iget v1, p0, LX/2S8;->p:I

    add-int/2addr v1, v0

    iget v2, p0, LX/2S8;->q:I

    if-le v1, v2, :cond_2

    .line 411635
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411636
    :cond_2
    iget-object v1, p0, LX/2S8;->s:[C

    invoke-direct {p0, v1, v3, v0}, LX/2S8;->e([CII)V

    .line 411637
    :goto_1
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_3

    .line 411638
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411639
    :cond_3
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v4, v0, v1

    goto :goto_0

    .line 411640
    :cond_4
    iget-object v1, p0, LX/2S8;->s:[C

    invoke-direct {p0, v1, v3, v0}, LX/2S8;->d([CII)V

    goto :goto_1

    .line 411641
    :cond_5
    invoke-direct {p0, p1}, LX/2S8;->l(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private k(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 411616
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411617
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411618
    :cond_0
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411619
    invoke-direct {p0, p1}, LX/2S8;->l(Ljava/lang/String;)V

    .line 411620
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_1

    .line 411621
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411622
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411623
    return-void
.end method

.method private l()V
    .locals 5

    .prologue
    .line 411417
    iget v0, p0, LX/2S8;->p:I

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411418
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411419
    :cond_0
    sget-object v0, LX/2S8;->v:[B

    const/4 v1, 0x0

    iget-object v2, p0, LX/2S8;->o:[B

    iget v3, p0, LX/2S8;->p:I

    const/4 v4, 0x4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 411420
    iget v0, p0, LX/2S8;->p:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/2S8;->p:I

    .line 411421
    return-void
.end method

.method private final l(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 411422
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 411423
    iget-object v3, p0, LX/2S8;->s:[C

    move v2, v0

    move v0, v1

    .line 411424
    :goto_0
    if-lez v2, :cond_1

    .line 411425
    iget v4, p0, LX/2S8;->r:I

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 411426
    add-int v5, v0, v4

    invoke-virtual {p1, v0, v5, v3, v1}, Ljava/lang/String;->getChars(II[CI)V

    .line 411427
    iget v5, p0, LX/2S8;->p:I

    add-int/2addr v5, v4

    iget v6, p0, LX/2S8;->q:I

    if-le v5, v6, :cond_0

    .line 411428
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411429
    :cond_0
    invoke-direct {p0, v3, v1, v4}, LX/2S8;->e([CII)V

    .line 411430
    add-int/2addr v0, v4

    .line 411431
    sub-int/2addr v2, v4

    .line 411432
    goto :goto_0

    .line 411433
    :cond_1
    return-void
.end method

.method private m()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 411262
    iget v0, p0, LX/2S8;->p:I

    .line 411263
    if-lez v0, :cond_0

    .line 411264
    iput v3, p0, LX/2S8;->p:I

    .line 411265
    iget-object v1, p0, LX/2S8;->n:Ljava/io/OutputStream;

    iget-object v2, p0, LX/2S8;->o:[B

    invoke-virtual {v1, v2, v3, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 411266
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(C)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 411407
    iget v0, p0, LX/2S8;->p:I

    add-int/lit8 v0, v0, 0x3

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411408
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411409
    :cond_0
    iget-object v0, p0, LX/2S8;->o:[B

    .line 411410
    const/16 v1, 0x7f

    if-gt p1, v1, :cond_1

    .line 411411
    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 411412
    :goto_0
    return-void

    .line 411413
    :cond_1
    const/16 v1, 0x800

    if-ge p1, v1, :cond_2

    .line 411414
    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    shr-int/lit8 v2, p1, 0x6

    or-int/lit16 v2, v2, 0xc0

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 411415
    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    and-int/lit8 v2, p1, 0x3f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    goto :goto_0

    .line 411416
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v2, v2}, LX/2S8;->a(I[CII)I

    goto :goto_0
.end method

.method public final a(D)V
    .locals 1

    .prologue
    .line 411257
    iget-boolean v0, p0, LX/12G;->d:Z

    if-nez v0, :cond_1

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, LX/0ls;->QUOTE_NON_NUMERIC_NUMBERS:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 411258
    :cond_1
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 411259
    :goto_0
    return-void

    .line 411260
    :cond_2
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411261
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 411252
    iget-boolean v0, p0, LX/12G;->d:Z

    if-nez v0, :cond_1

    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, LX/0ls;->QUOTE_NON_NUMERIC_NUMBERS:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 411253
    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 411254
    :goto_0
    return-void

    .line 411255
    :cond_2
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411256
    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 411245
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411246
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_0

    .line 411247
    invoke-direct {p0, p1, p2}, LX/2S8;->b(J)V

    .line 411248
    :goto_0
    return-void

    .line 411249
    :cond_0
    iget v0, p0, LX/2S8;->p:I

    add-int/lit8 v0, v0, 0x15

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_1

    .line 411250
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411251
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    invoke-static {p1, p2, v0, v1}, LX/13I;->a(J[BI)I

    move-result v0

    iput v0, p0, LX/2S8;->p:I

    goto :goto_0
.end method

.method public final a(LX/0ln;[BII)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 411236
    const-string v0, "write binary value"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411237
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411238
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411239
    :cond_0
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411240
    add-int v0, p3, p4

    invoke-direct {p0, p1, p2, p3, v0}, LX/2S8;->b(LX/0ln;[BII)V

    .line 411241
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_1

    .line 411242
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411243
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411244
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 411224
    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1, p1}, LX/12U;->a(Ljava/lang/String;)I

    move-result v1

    .line 411225
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 411226
    const-string v2, "Can not write a field name, expecting a value"

    invoke-static {v2}, LX/12G;->i(Ljava/lang/String;)V

    .line 411227
    :cond_0
    iget-object v2, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v2, :cond_2

    .line 411228
    if-ne v1, v0, :cond_1

    :goto_0
    invoke-direct {p0, p1, v0}, LX/2S8;->b(Ljava/lang/String;Z)V

    .line 411229
    :goto_1
    return-void

    .line 411230
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 411231
    :cond_2
    if-ne v1, v0, :cond_4

    .line 411232
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_3

    .line 411233
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411234
    :cond_3
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    const/16 v2, 0x2c

    aput-byte v2, v0, v1

    .line 411235
    :cond_4
    invoke-direct {p0, p1}, LX/2S8;->j(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/math/BigDecimal;)V
    .locals 1

    .prologue
    .line 411217
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411218
    if-nez p1, :cond_0

    .line 411219
    invoke-direct {p0}, LX/2S8;->l()V

    .line 411220
    :goto_0
    return-void

    .line 411221
    :cond_0
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_1

    .line 411222
    invoke-direct {p0, p1}, LX/2S8;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 411223
    :cond_1
    invoke-virtual {p1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/math/BigInteger;)V
    .locals 1

    .prologue
    .line 411210
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411211
    if-nez p1, :cond_0

    .line 411212
    invoke-direct {p0}, LX/2S8;->l()V

    .line 411213
    :goto_0
    return-void

    .line 411214
    :cond_0
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_1

    .line 411215
    invoke-direct {p0, p1}, LX/2S8;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 411216
    :cond_1
    invoke-virtual {p1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(S)V
    .locals 2

    .prologue
    .line 411203
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411204
    iget v0, p0, LX/2S8;->p:I

    add-int/lit8 v0, v0, 0x6

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411205
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411206
    :cond_0
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_1

    .line 411207
    invoke-direct {p0, p1}, LX/2S8;->b(S)V

    .line 411208
    :goto_0
    return-void

    .line 411209
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    invoke-static {p1, v0, v1}, LX/13I;->a(I[BI)I

    move-result v0

    iput v0, p0, LX/2S8;->p:I

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 411194
    const-string v0, "write boolean value"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411195
    iget v0, p0, LX/2S8;->p:I

    add-int/lit8 v0, v0, 0x5

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411196
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411197
    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, LX/2S8;->w:[B

    .line 411198
    :goto_0
    array-length v1, v0

    .line 411199
    const/4 v2, 0x0

    iget-object v3, p0, LX/2S8;->o:[B

    iget v4, p0, LX/2S8;->p:I

    invoke-static {v0, v2, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 411200
    iget v0, p0, LX/2S8;->p:I

    add-int/2addr v0, v1

    iput v0, p0, LX/2S8;->p:I

    .line 411201
    return-void

    .line 411202
    :cond_1
    sget-object v0, LX/2S8;->x:[B

    goto :goto_0
.end method

.method public final a([CII)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 411181
    const-string v0, "write text value"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411182
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411183
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411184
    :cond_0
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411185
    iget v0, p0, LX/2S8;->r:I

    if-gt p3, v0, :cond_3

    .line 411186
    iget v0, p0, LX/2S8;->p:I

    add-int/2addr v0, p3

    iget v1, p0, LX/2S8;->q:I

    if-le v0, v1, :cond_1

    .line 411187
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411188
    :cond_1
    invoke-direct {p0, p1, p2, p3}, LX/2S8;->e([CII)V

    .line 411189
    :goto_0
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_2

    .line 411190
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411191
    :cond_2
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411192
    return-void

    .line 411193
    :cond_3
    invoke-direct {p0, p1, p2, p3}, LX/2S8;->d([CII)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 411174
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411175
    iget v0, p0, LX/2S8;->p:I

    add-int/lit8 v0, v0, 0xb

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411176
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411177
    :cond_0
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_1

    .line 411178
    invoke-direct {p0, p1}, LX/2S8;->d(I)V

    .line 411179
    :goto_0
    return-void

    .line 411180
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    invoke-static {p1, v0, v1}, LX/13I;->a(I[BI)I

    move-result v0

    iput v0, p0, LX/2S8;->p:I

    goto :goto_0
.end method

.method public final b(LX/0lc;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 411162
    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/12U;->a(Ljava/lang/String;)I

    move-result v1

    .line 411163
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 411164
    const-string v2, "Can not write a field name, expecting a value"

    invoke-static {v2}, LX/12G;->i(Ljava/lang/String;)V

    .line 411165
    :cond_0
    iget-object v2, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v2, :cond_2

    .line 411166
    if-ne v1, v0, :cond_1

    :goto_0
    invoke-direct {p0, p1, v0}, LX/2S8;->a(LX/0lc;Z)V

    .line 411167
    :goto_1
    return-void

    .line 411168
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 411169
    :cond_2
    if-ne v1, v0, :cond_4

    .line 411170
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_3

    .line 411171
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411172
    :cond_3
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    const/16 v2, 0x2c

    aput-byte v2, v0, v1

    .line 411173
    :cond_4
    invoke-direct {p0, p1}, LX/2S8;->e(LX/0lc;)V

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0x22

    const/4 v4, 0x0

    .line 411145
    const-string v0, "write text value"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411146
    if-nez p1, :cond_0

    .line 411147
    invoke-direct {p0}, LX/2S8;->l()V

    .line 411148
    :goto_0
    return-void

    .line 411149
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 411150
    iget v1, p0, LX/2S8;->t:I

    if-le v0, v1, :cond_1

    .line 411151
    invoke-direct {p0, p1}, LX/2S8;->k(Ljava/lang/String;)V

    goto :goto_0

    .line 411152
    :cond_1
    iget-object v1, p0, LX/2S8;->s:[C

    invoke-virtual {p1, v4, v0, v1, v4}, Ljava/lang/String;->getChars(II[CI)V

    .line 411153
    iget v1, p0, LX/2S8;->r:I

    if-le v0, v1, :cond_2

    .line 411154
    invoke-direct {p0, v0}, LX/2S8;->c(I)V

    goto :goto_0

    .line 411155
    :cond_2
    iget v1, p0, LX/2S8;->p:I

    add-int/2addr v1, v0

    iget v2, p0, LX/2S8;->q:I

    if-lt v1, v2, :cond_3

    .line 411156
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411157
    :cond_3
    iget-object v1, p0, LX/2S8;->o:[B

    iget v2, p0, LX/2S8;->p:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/2S8;->p:I

    aput-byte v5, v1, v2

    .line 411158
    iget-object v1, p0, LX/2S8;->s:[C

    invoke-direct {p0, v1, v4, v0}, LX/2S8;->e([CII)V

    .line 411159
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_4

    .line 411160
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411161
    :cond_4
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v5, v0, v1

    goto :goto_0
.end method

.method public final b([CII)V
    .locals 6

    .prologue
    .line 411267
    add-int v0, p3, p3

    add-int/2addr v0, p3

    .line 411268
    iget v1, p0, LX/2S8;->p:I

    add-int/2addr v1, v0

    iget v2, p0, LX/2S8;->q:I

    if-le v1, v2, :cond_2

    .line 411269
    iget v1, p0, LX/2S8;->q:I

    if-ge v1, v0, :cond_1

    .line 411270
    invoke-direct {p0, p1, p2, p3}, LX/2S8;->c([CII)V

    .line 411271
    :cond_0
    return-void

    .line 411272
    :cond_1
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411273
    :cond_2
    add-int v1, p3, p2

    move v0, p2

    .line 411274
    :goto_0
    if-ge v0, v1, :cond_0

    .line 411275
    :goto_1
    aget-char v2, p1, v0

    .line 411276
    const/16 v3, 0x7f

    if-gt v2, v3, :cond_3

    .line 411277
    iget-object v3, p0, LX/2S8;->o:[B

    iget v4, p0, LX/2S8;->p:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/2S8;->p:I

    int-to-byte v2, v2

    aput-byte v2, v3, v4

    .line 411278
    add-int/lit8 v0, v0, 0x1

    if-ge v0, v1, :cond_0

    goto :goto_1

    .line 411279
    :cond_3
    add-int/lit8 p2, v0, 0x1

    aget-char v0, p1, v0

    .line 411280
    const/16 v2, 0x800

    if-ge v0, v2, :cond_4

    .line 411281
    iget-object v2, p0, LX/2S8;->o:[B

    iget v3, p0, LX/2S8;->p:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/2S8;->p:I

    shr-int/lit8 v4, v0, 0x6

    or-int/lit16 v4, v4, 0xc0

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 411282
    iget-object v2, p0, LX/2S8;->o:[B

    iget v3, p0, LX/2S8;->p:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/2S8;->p:I

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    move v0, p2

    goto :goto_0

    .line 411283
    :cond_4
    invoke-direct {p0, v0, p1, p2, v1}, LX/2S8;->a(I[CII)I

    move v0, p2

    .line 411284
    goto :goto_0
.end method

.method public final c(LX/0lc;)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 411285
    const-string v0, "write text value"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411286
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_0

    .line 411287
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411288
    :cond_0
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411289
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    invoke-interface {p1, v0, v1}, LX/0lc;->a([BI)I

    move-result v0

    .line 411290
    if-gez v0, :cond_2

    .line 411291
    invoke-interface {p1}, LX/0lc;->e()[B

    move-result-object v0

    invoke-direct {p0, v0}, LX/2S8;->b([B)V

    .line 411292
    :goto_0
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_1

    .line 411293
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411294
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    aput-byte v3, v0, v1

    .line 411295
    return-void

    .line 411296
    :cond_2
    iget v1, p0, LX/2S8;->p:I

    add-int/2addr v0, v1

    iput v0, p0, LX/2S8;->p:I

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 411297
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    move v2, v3

    .line 411298
    :goto_0
    if-lez v1, :cond_1

    .line 411299
    iget-object v4, p0, LX/2S8;->s:[C

    .line 411300
    array-length v0, v4

    .line 411301
    if-ge v1, v0, :cond_0

    move v0, v1

    .line 411302
    :cond_0
    add-int v5, v2, v0

    invoke-virtual {p1, v2, v5, v4, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 411303
    invoke-virtual {p0, v4, v3, v0}, LX/0nX;->b([CII)V

    .line 411304
    add-int/2addr v2, v0

    .line 411305
    sub-int/2addr v1, v0

    .line 411306
    goto :goto_0

    .line 411307
    :cond_1
    return-void
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 411308
    invoke-super {p0}, LX/12F;->close()V

    .line 411309
    iget-object v0, p0, LX/2S8;->o:[B

    if-eqz v0, :cond_1

    sget-object v0, LX/0ls;->AUTO_CLOSE_JSON_CONTENT:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 411310
    :goto_0
    iget-object v0, p0, LX/12G;->e:LX/12U;

    move-object v0, v0

    .line 411311
    invoke-virtual {v0}, LX/12V;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 411312
    invoke-virtual {p0}, LX/0nX;->e()V

    goto :goto_0

    .line 411313
    :cond_0
    invoke-virtual {v0}, LX/12V;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 411314
    invoke-virtual {p0}, LX/0nX;->g()V

    goto :goto_0

    .line 411315
    :cond_1
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411316
    iget-object v0, p0, LX/2S8;->n:Ljava/io/OutputStream;

    if-eqz v0, :cond_3

    .line 411317
    iget-object v0, p0, LX/12F;->h:LX/12A;

    .line 411318
    iget-boolean v1, v0, LX/12A;->c:Z

    move v0, v1

    .line 411319
    if-nez v0, :cond_2

    sget-object v0, LX/0ls;->AUTO_CLOSE_TARGET:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 411320
    :cond_2
    iget-object v0, p0, LX/2S8;->n:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 411321
    :cond_3
    :goto_1
    invoke-virtual {p0}, LX/2S8;->j()V

    .line 411322
    return-void

    .line 411323
    :cond_4
    sget-object v0, LX/0ls;->FLUSH_PASSED_TO_STREAM:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 411324
    iget-object v0, p0, LX/2S8;->n:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 411325
    const-string v0, "start an array"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411326
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12U;->j()LX/12U;

    move-result-object v0

    iput-object v0, p0, LX/2S8;->e:LX/12U;

    .line 411327
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v0, :cond_0

    .line 411328
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->e(LX/0nX;)V

    .line 411329
    :goto_0
    return-void

    .line 411330
    :cond_0
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_1

    .line 411331
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411332
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    const/16 v2, 0x5b

    aput-byte v2, v0, v1

    goto :goto_0
.end method

.method public final d(LX/0lc;)V
    .locals 2

    .prologue
    .line 411333
    invoke-interface {p1}, LX/0lc;->d()[B

    move-result-object v0

    .line 411334
    array-length v1, v0

    if-lez v1, :cond_0

    .line 411335
    invoke-direct {p0, v0}, LX/2S8;->b([B)V

    .line 411336
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 411337
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12V;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 411338
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current context not an ARRAY but "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1}, LX/12V;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 411339
    :cond_0
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v0, :cond_1

    .line 411340
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1}, LX/12V;->f()I

    move-result v1

    invoke-interface {v0, p0, v1}, LX/0lZ;->b(LX/0nX;I)V

    .line 411341
    :goto_0
    iget-object v0, p0, LX/12G;->e:LX/12U;

    .line 411342
    iget-object v1, v0, LX/12U;->c:LX/12U;

    move-object v0, v1

    .line 411343
    iput-object v0, p0, LX/2S8;->e:LX/12U;

    .line 411344
    return-void

    .line 411345
    :cond_1
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_2

    .line 411346
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411347
    :cond_2
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    const/16 v2, 0x5d

    aput-byte v2, v0, v1

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 411348
    const-string v0, "write number"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411349
    iget-boolean v0, p0, LX/12G;->d:Z

    if-eqz v0, :cond_0

    .line 411350
    invoke-direct {p0, p1}, LX/2S8;->b(Ljava/lang/Object;)V

    .line 411351
    :goto_0
    return-void

    .line 411352
    :cond_0
    invoke-virtual {p0, p1}, LX/0nX;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 411353
    const-string v0, "start an object"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411354
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12U;->k()LX/12U;

    move-result-object v0

    iput-object v0, p0, LX/2S8;->e:LX/12U;

    .line 411355
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v0, :cond_0

    .line 411356
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    invoke-interface {v0, p0}, LX/0lZ;->b(LX/0nX;)V

    .line 411357
    :goto_0
    return-void

    .line 411358
    :cond_0
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_1

    .line 411359
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411360
    :cond_1
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    const/16 v2, 0x7b

    aput-byte v2, v0, v1

    goto :goto_0
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 411361
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411362
    iget-object v0, p0, LX/2S8;->n:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 411363
    sget-object v0, LX/0ls;->FLUSH_PASSED_TO_STREAM:LX/0ls;

    invoke-virtual {p0, v0}, LX/12G;->a(LX/0ls;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411364
    iget-object v0, p0, LX/2S8;->n:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 411365
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 411366
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12V;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 411367
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current context not an object but "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1}, LX/12V;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/12G;->i(Ljava/lang/String;)V

    .line 411368
    :cond_0
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    if-eqz v0, :cond_1

    .line 411369
    iget-object v0, p0, LX/0nX;->a:LX/0lZ;

    iget-object v1, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v1}, LX/12V;->f()I

    move-result v1

    invoke-interface {v0, p0, v1}, LX/0lZ;->a(LX/0nX;I)V

    .line 411370
    :goto_0
    iget-object v0, p0, LX/12G;->e:LX/12U;

    .line 411371
    iget-object v1, v0, LX/12U;->c:LX/12U;

    move-object v0, v1

    .line 411372
    iput-object v0, p0, LX/2S8;->e:LX/12U;

    .line 411373
    return-void

    .line 411374
    :cond_1
    iget v0, p0, LX/2S8;->p:I

    iget v1, p0, LX/2S8;->q:I

    if-lt v0, v1, :cond_2

    .line 411375
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411376
    :cond_2
    iget-object v0, p0, LX/2S8;->o:[B

    iget v1, p0, LX/2S8;->p:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/2S8;->p:I

    const/16 v2, 0x7d

    aput-byte v2, v0, v1

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 411377
    const-string v0, "write null value"

    invoke-virtual {p0, v0}, LX/2S8;->h(Ljava/lang/String;)V

    .line 411378
    invoke-direct {p0}, LX/2S8;->l()V

    .line 411379
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 411380
    iget-object v0, p0, LX/12G;->e:LX/12U;

    invoke-virtual {v0}, LX/12U;->m()I

    move-result v0

    .line 411381
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 411382
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", expecting field name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/12G;->i(Ljava/lang/String;)V

    .line 411383
    :cond_0
    iget-object v1, p0, LX/0nX;->a:LX/0lZ;

    if-nez v1, :cond_3

    .line 411384
    packed-switch v0, :pswitch_data_0

    .line 411385
    :cond_1
    :goto_0
    return-void

    .line 411386
    :pswitch_0
    const/16 v0, 0x2c

    .line 411387
    :goto_1
    iget v1, p0, LX/2S8;->p:I

    iget v2, p0, LX/2S8;->q:I

    if-lt v1, v2, :cond_2

    .line 411388
    invoke-direct {p0}, LX/2S8;->m()V

    .line 411389
    :cond_2
    iget-object v1, p0, LX/2S8;->o:[B

    iget v2, p0, LX/2S8;->p:I

    aput-byte v0, v1, v2

    .line 411390
    iget v0, p0, LX/2S8;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/2S8;->p:I

    goto :goto_0

    .line 411391
    :pswitch_1
    const/16 v0, 0x3a

    .line 411392
    goto :goto_1

    .line 411393
    :pswitch_2
    iget-object v0, p0, LX/12F;->l:LX/0lc;

    if-eqz v0, :cond_1

    .line 411394
    iget-object v0, p0, LX/12F;->l:LX/0lc;

    invoke-interface {v0}, LX/0lc;->d()[B

    move-result-object v0

    .line 411395
    array-length v1, v0

    if-lez v1, :cond_1

    .line 411396
    invoke-direct {p0, v0}, LX/2S8;->b([B)V

    goto :goto_0

    .line 411397
    :cond_3
    invoke-direct {p0, v0}, LX/2S8;->e(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 411398
    iget-object v0, p0, LX/2S8;->o:[B

    .line 411399
    if-eqz v0, :cond_0

    iget-boolean v1, p0, LX/2S8;->u:Z

    if-eqz v1, :cond_0

    .line 411400
    iput-object v2, p0, LX/2S8;->o:[B

    .line 411401
    iget-object v1, p0, LX/12F;->h:LX/12A;

    invoke-virtual {v1, v0}, LX/12A;->b([B)V

    .line 411402
    :cond_0
    iget-object v0, p0, LX/2S8;->s:[C

    .line 411403
    if-eqz v0, :cond_1

    .line 411404
    iput-object v2, p0, LX/2S8;->s:[C

    .line 411405
    iget-object v1, p0, LX/12F;->h:LX/12A;

    invoke-virtual {v1, v0}, LX/12A;->b([C)V

    .line 411406
    :cond_1
    return-void
.end method
