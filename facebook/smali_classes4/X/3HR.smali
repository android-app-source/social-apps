.class public LX/3HR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2mZ;


# direct methods
.method public constructor <init>(LX/2mZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 543457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 543458
    iput-object p1, p0, LX/3HR;->a:LX/2mZ;

    .line 543459
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 543460
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 543461
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 543462
    if-nez v0, :cond_0

    move v0, v1

    .line 543463
    :goto_0
    return v0

    .line 543464
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 543465
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aR()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/video/engine/VideoPlayerParams;)LX/2pa;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            ")",
            "LX/2pa;"
        }
    .end annotation

    .prologue
    .line 543466
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "InterstitialGraphQLStoryPropsKey"

    invoke-virtual {v0, v1, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 543467
    new-instance v1, LX/2pZ;

    invoke-direct {v1}, LX/2pZ;-><init>()V

    .line 543468
    iput-object p2, v1, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 543469
    move-object v1, v1

    .line 543470
    invoke-virtual {v1, v0}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 543471
    return-object v0
.end method

.method public static b(LX/0QB;)LX/3HR;
    .locals 2

    .prologue
    .line 543472
    new-instance v1, LX/3HR;

    const-class v0, LX/2mZ;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/2mZ;

    invoke-direct {v1, v0}, LX/3HR;-><init>(LX/2mZ;)V

    .line 543473
    return-object v1
.end method


# virtual methods
.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/video/engine/VideoPlayerParams;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/video/engine/VideoPlayerParams;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 543474
    invoke-static {p1}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 543475
    if-nez v2, :cond_0

    move-object v0, v1

    .line 543476
    :goto_0
    return-object v0

    .line 543477
    :cond_0
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 543478
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 543479
    if-nez v0, :cond_1

    move-object v0, v1

    .line 543480
    goto :goto_0

    .line 543481
    :cond_1
    iget-object v1, p0, LX/3HR;->a:LX/2mZ;

    invoke-virtual {v1, v2, v0}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v0

    .line 543482
    invoke-virtual {v0}, LX/3Im;->a()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    goto :goto_0
.end method
