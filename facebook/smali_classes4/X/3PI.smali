.class public LX/3PI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3HG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3HG",
        "<",
        "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/3GS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/3PJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/3PH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 561954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 561955
    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)LX/3Bq;
    .locals 6

    .prologue
    .line 561956
    check-cast p1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;

    .line 561957
    iget-object v0, p0, LX/3PI;->c:LX/3PH;

    invoke-virtual {v0}, LX/3PH;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 561958
    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 561959
    :cond_0
    const/4 v0, 0x0

    .line 561960
    :goto_0
    return-object v0

    .line 561961
    :cond_1
    new-instance v0, LX/4VL;

    iget-object v1, p0, LX/3PI;->d:LX/0Uh;

    const/4 v2, 0x1

    new-array v2, v2, [LX/4VT;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 561962
    new-instance p0, LX/6A0;

    invoke-direct {p0, v4, v5}, LX/6A0;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 561963
    move-object v4, p0

    .line 561964
    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, LX/4VL;-><init>(LX/0Uh;[LX/4VT;)V

    goto :goto_0

    .line 561965
    :cond_2
    iget-object v0, p0, LX/3PI;->a:LX/3GS;

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 561966
    new-instance v4, LX/69z;

    invoke-direct {v4, v1, v2}, LX/69z;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 561967
    invoke-static {v0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v3

    check-cast v3, LX/20j;

    .line 561968
    iput-object v3, v4, LX/69z;->b:LX/20j;

    .line 561969
    move-object v0, v4

    .line 561970
    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 561971
    const-class v0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;

    return-object v0
.end method

.method public final b()LX/69p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/iface/ModelProcessor",
            "<",
            "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 561972
    const/4 v0, 0x0

    return-object v0
.end method
