.class public LX/2Qr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static volatile c:LX/2Qr;


# instance fields
.field public final b:LX/2Qi;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 408943
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pending_app_calls."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/2Qo;->a:LX/0U1;

    .line 408944
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 408945
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/2Qo;->b:LX/0U1;

    .line 408946
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 408947
    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/2Qo;->c:LX/0U1;

    .line 408948
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 408949
    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/2Qo;->d:LX/0U1;

    .line 408950
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 408951
    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/2Qo;->e:LX/0U1;

    .line 408952
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 408953
    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/2Qo;->f:LX/0U1;

    .line 408954
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 408955
    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2Qo;->g:LX/0U1;

    .line 408956
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 408957
    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2Qo;->h:LX/0U1;

    .line 408958
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 408959
    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2Qo;->i:LX/0U1;

    .line 408960
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 408961
    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/2Qo;->j:LX/0U1;

    .line 408962
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 408963
    aput-object v2, v0, v1

    sput-object v0, LX/2Qr;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2Qi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 408964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408965
    iput-object p1, p0, LX/2Qr;->b:LX/2Qi;

    .line 408966
    return-void
.end method

.method public static a(LX/0QB;)LX/2Qr;
    .locals 4

    .prologue
    .line 408967
    sget-object v0, LX/2Qr;->c:LX/2Qr;

    if-nez v0, :cond_1

    .line 408968
    const-class v1, LX/2Qr;

    monitor-enter v1

    .line 408969
    :try_start_0
    sget-object v0, LX/2Qr;->c:LX/2Qr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 408970
    if-eqz v2, :cond_0

    .line 408971
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 408972
    new-instance p0, LX/2Qr;

    invoke-static {v0}, LX/2Qi;->a(LX/0QB;)LX/2Qi;

    move-result-object v3

    check-cast v3, LX/2Qi;

    invoke-direct {p0, v3}, LX/2Qr;-><init>(LX/2Qi;)V

    .line 408973
    move-object v0, p0

    .line 408974
    sput-object v0, LX/2Qr;->c:LX/2Qr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408975
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 408976
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 408977
    :cond_1
    sget-object v0, LX/2Qr;->c:LX/2Qr;

    return-object v0

    .line 408978
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 408979
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/2Qr;Ljava/lang/String;)LX/0Px;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/platform/common/action/PlatformAppCall;",
            ">;"
        }
    .end annotation

    .prologue
    .line 408980
    new-instance v12, LX/0Pz;

    invoke-direct {v12}, LX/0Pz;-><init>()V

    .line 408981
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 408982
    const-string v2, "pending_app_calls"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 408983
    const/4 v4, 0x0

    .line 408984
    const/4 v5, 0x0

    .line 408985
    if-eqz p1, :cond_0

    .line 408986
    sget-object v2, LX/2Qo;->a:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v2, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 408987
    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    .line 408988
    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    .line 408989
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/2Qr;->b:LX/2Qi;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v3, LX/2Qr;->a:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 408990
    :try_start_0
    sget-object v1, LX/2Qo;->a:LX/0U1;

    invoke-virtual {v1, v13}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v14

    .line 408991
    sget-object v1, LX/2Qo;->b:LX/0U1;

    invoke-virtual {v1, v13}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v15

    .line 408992
    sget-object v1, LX/2Qo;->c:LX/0U1;

    invoke-virtual {v1, v13}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v16

    .line 408993
    sget-object v1, LX/2Qo;->d:LX/0U1;

    invoke-virtual {v1, v13}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v17

    .line 408994
    sget-object v1, LX/2Qo;->e:LX/0U1;

    invoke-virtual {v1, v13}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v18

    .line 408995
    sget-object v1, LX/2Qo;->f:LX/0U1;

    invoke-virtual {v1, v13}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v19

    .line 408996
    sget-object v1, LX/2Qo;->g:LX/0U1;

    invoke-virtual {v1, v13}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v20

    .line 408997
    sget-object v1, LX/2Qo;->h:LX/0U1;

    invoke-virtual {v1, v13}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v21

    .line 408998
    sget-object v1, LX/2Qo;->i:LX/0U1;

    invoke-virtual {v1, v13}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v22

    .line 408999
    sget-object v1, LX/2Qo;->j:LX/0U1;

    invoke-virtual {v1, v13}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v23

    .line 409000
    :goto_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 409001
    new-instance v1, Lcom/facebook/platform/common/action/PlatformAppCall;

    invoke-interface {v13, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v13, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move/from16 v0, v16

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    move/from16 v0, v17

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move/from16 v0, v18

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move/from16 v0, v19

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move/from16 v0, v20

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move/from16 v0, v21

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    move/from16 v0, v22

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    move/from16 v0, v23

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v1 .. v11}, Lcom/facebook/platform/common/action/PlatformAppCall;-><init>(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 409002
    :catchall_0
    move-exception v1

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v1

    .line 409003
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 409004
    :cond_2
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 409005
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    return-object v1
.end method
