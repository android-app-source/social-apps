.class public LX/2gt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 449234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 449235
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 449236
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 449237
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 449238
    invoke-static {p0, p1}, LX/2gt;->b(LX/15w;LX/186;)I

    move-result v1

    .line 449239
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 449240
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 58

    .prologue
    .line 449241
    const/16 v54, 0x0

    .line 449242
    const/16 v53, 0x0

    .line 449243
    const/16 v52, 0x0

    .line 449244
    const/16 v51, 0x0

    .line 449245
    const/16 v50, 0x0

    .line 449246
    const/16 v49, 0x0

    .line 449247
    const/16 v48, 0x0

    .line 449248
    const/16 v47, 0x0

    .line 449249
    const/16 v46, 0x0

    .line 449250
    const/16 v45, 0x0

    .line 449251
    const/16 v44, 0x0

    .line 449252
    const/16 v43, 0x0

    .line 449253
    const/16 v42, 0x0

    .line 449254
    const/16 v41, 0x0

    .line 449255
    const/16 v40, 0x0

    .line 449256
    const/16 v39, 0x0

    .line 449257
    const/16 v38, 0x0

    .line 449258
    const/16 v37, 0x0

    .line 449259
    const/16 v36, 0x0

    .line 449260
    const/16 v35, 0x0

    .line 449261
    const/16 v34, 0x0

    .line 449262
    const/16 v33, 0x0

    .line 449263
    const/16 v32, 0x0

    .line 449264
    const/16 v31, 0x0

    .line 449265
    const/16 v30, 0x0

    .line 449266
    const/16 v29, 0x0

    .line 449267
    const/16 v28, 0x0

    .line 449268
    const/16 v27, 0x0

    .line 449269
    const/16 v26, 0x0

    .line 449270
    const/16 v25, 0x0

    .line 449271
    const/16 v24, 0x0

    .line 449272
    const/16 v23, 0x0

    .line 449273
    const/16 v22, 0x0

    .line 449274
    const/16 v21, 0x0

    .line 449275
    const/16 v20, 0x0

    .line 449276
    const/16 v19, 0x0

    .line 449277
    const/16 v18, 0x0

    .line 449278
    const/16 v17, 0x0

    .line 449279
    const/16 v16, 0x0

    .line 449280
    const/4 v15, 0x0

    .line 449281
    const/4 v14, 0x0

    .line 449282
    const/4 v13, 0x0

    .line 449283
    const/4 v12, 0x0

    .line 449284
    const/4 v11, 0x0

    .line 449285
    const/4 v10, 0x0

    .line 449286
    const/4 v9, 0x0

    .line 449287
    const/4 v8, 0x0

    .line 449288
    const/4 v7, 0x0

    .line 449289
    const/4 v6, 0x0

    .line 449290
    const/4 v5, 0x0

    .line 449291
    const/4 v4, 0x0

    .line 449292
    const/4 v3, 0x0

    .line 449293
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v55

    sget-object v56, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    if-eq v0, v1, :cond_1

    .line 449294
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 449295
    const/4 v3, 0x0

    .line 449296
    :goto_0
    return v3

    .line 449297
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 449298
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v55

    sget-object v56, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    if-eq v0, v1, :cond_2d

    .line 449299
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v55

    .line 449300
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 449301
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v56

    sget-object v57, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v56

    move-object/from16 v1, v57

    if-eq v0, v1, :cond_1

    if-eqz v55, :cond_1

    .line 449302
    const-string v56, "__type__"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-nez v56, :cond_2

    const-string v56, "__typename"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_3

    .line 449303
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/String;)I

    move-result v54

    goto :goto_1

    .line 449304
    :cond_3
    const-string v56, "autoplay_on_cell"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_4

    .line 449305
    const/4 v11, 0x1

    .line 449306
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v53

    goto :goto_1

    .line 449307
    :cond_4
    const-string v56, "autoplay_on_wifi"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_5

    .line 449308
    const/4 v10, 0x1

    .line 449309
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v52

    goto :goto_1

    .line 449310
    :cond_5
    const-string v56, "availability_indicator_label"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_6

    .line 449311
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v51

    goto :goto_1

    .line 449312
    :cond_6
    const-string v56, "bounding_box"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_7

    .line 449313
    invoke-static/range {p0 .. p1}, LX/4NK;->a(LX/15w;LX/186;)I

    move-result v50

    goto :goto_1

    .line 449314
    :cond_7
    const-string v56, "game_description"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_8

    .line 449315
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v49

    goto/16 :goto_1

    .line 449316
    :cond_8
    const-string v56, "game_name"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_9

    .line 449317
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    goto/16 :goto_1

    .line 449318
    :cond_9
    const-string v56, "game_orientation"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_a

    .line 449319
    const/4 v9, 0x1

    .line 449320
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v47

    invoke-static/range {v47 .. v47}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-result-object v47

    goto/16 :goto_1

    .line 449321
    :cond_a
    const-string v56, "game_uri"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_b

    .line 449322
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v46

    goto/16 :goto_1

    .line 449323
    :cond_b
    const-string v56, "icon_uri"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_c

    .line 449324
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v45

    goto/16 :goto_1

    .line 449325
    :cond_c
    const-string v56, "instant_game_id"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_d

    .line 449326
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    goto/16 :goto_1

    .line 449327
    :cond_d
    const-string v56, "label"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_e

    .line 449328
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v43

    goto/16 :goto_1

    .line 449329
    :cond_e
    const-string v56, "lat_long_list"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_f

    .line 449330
    invoke-static/range {p0 .. p1}, LX/2sz;->b(LX/15w;LX/186;)I

    move-result v42

    goto/16 :goto_1

    .line 449331
    :cond_f
    const-string v56, "layout_height"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_10

    .line 449332
    const/4 v8, 0x1

    .line 449333
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v41

    goto/16 :goto_1

    .line 449334
    :cond_10
    const-string v56, "layout_width"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_11

    .line 449335
    const/4 v7, 0x1

    .line 449336
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v40

    goto/16 :goto_1

    .line 449337
    :cond_11
    const-string v56, "layout_x"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_12

    .line 449338
    const/4 v6, 0x1

    .line 449339
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v39

    goto/16 :goto_1

    .line 449340
    :cond_12
    const-string v56, "layout_y"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_13

    .line 449341
    const/4 v5, 0x1

    .line 449342
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v38

    goto/16 :goto_1

    .line 449343
    :cond_13
    const-string v56, "location"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_14

    .line 449344
    invoke-static/range {p0 .. p1}, LX/2sz;->a(LX/15w;LX/186;)I

    move-result v37

    goto/16 :goto_1

    .line 449345
    :cond_14
    const-string v56, "logo"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_15

    .line 449346
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v36

    goto/16 :goto_1

    .line 449347
    :cond_15
    const-string v56, "mobile_game_uri"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_16

    .line 449348
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    goto/16 :goto_1

    .line 449349
    :cond_16
    const-string v56, "nearby_locations"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_17

    .line 449350
    invoke-static/range {p0 .. p1}, LX/2sz;->b(LX/15w;LX/186;)I

    move-result v34

    goto/16 :goto_1

    .line 449351
    :cond_17
    const-string v56, "show_objectionable_warning"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_18

    .line 449352
    const/4 v4, 0x1

    .line 449353
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto/16 :goto_1

    .line 449354
    :cond_18
    const-string v56, "splash_uri"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_19

    .line 449355
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto/16 :goto_1

    .line 449356
    :cond_19
    const-string v56, "video_uri"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1a

    .line 449357
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    goto/16 :goto_1

    .line 449358
    :cond_1a
    const-string v56, "place_rec_info"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1b

    .line 449359
    invoke-static/range {p0 .. p1}, LX/4RR;->a(LX/15w;LX/186;)I

    move-result v30

    goto/16 :goto_1

    .line 449360
    :cond_1b
    const-string v56, "instant_experience_app_id"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1c

    .line 449361
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    goto/16 :goto_1

    .line 449362
    :cond_1c
    const-string v56, "instant_experience_link_uri"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1d

    .line 449363
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 449364
    :cond_1d
    const-string v56, "contextual_title"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1e

    .line 449365
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 449366
    :cond_1e
    const-string v56, "parent_story"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1f

    .line 449367
    invoke-static/range {p0 .. p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 449368
    :cond_1f
    const-string v56, "attributes"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_20

    .line 449369
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 449370
    :cond_20
    const-string v56, "instant_experience_domain_whitelist"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_21

    .line 449371
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 449372
    :cond_21
    const-string v56, "instant_experience_app"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_22

    .line 449373
    invoke-static/range {p0 .. p1}, LX/2uk;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 449374
    :cond_22
    const-string v56, "instant_experience_user_app_scoped_id"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_23

    .line 449375
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 449376
    :cond_23
    const-string v56, "instant_experience_user_page_scoped_id"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_24

    .line 449377
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 449378
    :cond_24
    const-string v56, "broadcaster"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_25

    .line 449379
    invoke-static/range {p0 .. p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 449380
    :cond_25
    const-string v56, "video_broadcast_schedule"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_26

    .line 449381
    invoke-static/range {p0 .. p1}, LX/4UC;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 449382
    :cond_26
    const-string v56, "instant_experience_feature_enabled_list"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_27

    .line 449383
    invoke-static/range {p0 .. p1}, LX/4RY;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 449384
    :cond_27
    const-string v56, "destination_id"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_28

    .line 449385
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 449386
    :cond_28
    const-string v56, "destination_type"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_29

    .line 449387
    const/4 v3, 0x1

    .line 449388
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    move-result-object v16

    goto/16 :goto_1

    .line 449389
    :cond_29
    const-string v56, "native_template_view"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_2a

    .line 449390
    invoke-static/range {p0 .. p1}, LX/4Pn;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 449391
    :cond_2a
    const-string v56, "instant_experience_page"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_2b

    .line 449392
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 449393
    :cond_2b
    const-string v56, "instant_experience_ad_id"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_2c

    .line 449394
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 449395
    :cond_2c
    const-string v56, "group"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_0

    .line 449396
    invoke-static/range {p0 .. p1}, LX/30j;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 449397
    :cond_2d
    const/16 v55, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 449398
    const/16 v55, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v55

    move/from16 v2, v54

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 449399
    if-eqz v11, :cond_2e

    .line 449400
    const/4 v11, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 449401
    :cond_2e
    if-eqz v10, :cond_2f

    .line 449402
    const/4 v10, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 449403
    :cond_2f
    const/4 v10, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 449404
    const/4 v10, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 449405
    const/4 v10, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 449406
    const/4 v10, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 449407
    if-eqz v9, :cond_30

    .line 449408
    const/4 v9, 0x7

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-virtual {v0, v9, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 449409
    :cond_30
    const/16 v9, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 449410
    const/16 v9, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 449411
    const/16 v9, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 449412
    const/16 v9, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 449413
    const/16 v9, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 449414
    if-eqz v8, :cond_31

    .line 449415
    const/16 v8, 0xd

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v8, v1, v9}, LX/186;->a(III)V

    .line 449416
    :cond_31
    if-eqz v7, :cond_32

    .line 449417
    const/16 v7, 0xe

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v7, v1, v8}, LX/186;->a(III)V

    .line 449418
    :cond_32
    if-eqz v6, :cond_33

    .line 449419
    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v6, v1, v7}, LX/186;->a(III)V

    .line 449420
    :cond_33
    if-eqz v5, :cond_34

    .line 449421
    const/16 v5, 0x10

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v5, v1, v6}, LX/186;->a(III)V

    .line 449422
    :cond_34
    const/16 v5, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 449423
    const/16 v5, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 449424
    const/16 v5, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 449425
    const/16 v5, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 449426
    if-eqz v4, :cond_35

    .line 449427
    const/16 v4, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 449428
    :cond_35
    const/16 v4, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449429
    const/16 v4, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449430
    const/16 v4, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449431
    const/16 v4, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449432
    const/16 v4, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449433
    const/16 v4, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449434
    const/16 v4, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449435
    const/16 v4, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449436
    const/16 v4, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449437
    const/16 v4, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449438
    const/16 v4, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449439
    const/16 v4, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449440
    const/16 v4, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449441
    const/16 v4, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449442
    const/16 v4, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449443
    const/16 v4, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 449444
    if-eqz v3, :cond_36

    .line 449445
    const/16 v3, 0x26

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 449446
    :cond_36
    const/16 v3, 0x27

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 449447
    const/16 v3, 0x28

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 449448
    const/16 v3, 0x29

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 449449
    const/16 v3, 0x2a

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 449450
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x26

    const/16 v5, 0x1e

    const/16 v4, 0x1d

    const/4 v3, 0x7

    const/4 v2, 0x0

    .line 449451
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 449452
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 449453
    if-eqz v0, :cond_0

    .line 449454
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449455
    invoke-static {p0, p1, v2, p2}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 449456
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 449457
    if-eqz v0, :cond_1

    .line 449458
    const-string v1, "autoplay_on_cell"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449459
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 449460
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 449461
    if-eqz v0, :cond_2

    .line 449462
    const-string v1, "autoplay_on_wifi"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449463
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 449464
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449465
    if-eqz v0, :cond_3

    .line 449466
    const-string v1, "availability_indicator_label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449467
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 449468
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449469
    if-eqz v0, :cond_4

    .line 449470
    const-string v1, "bounding_box"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449471
    invoke-static {p0, v0, p2}, LX/4NK;->a(LX/15i;ILX/0nX;)V

    .line 449472
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449473
    if-eqz v0, :cond_5

    .line 449474
    const-string v1, "game_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449475
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449476
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449477
    if-eqz v0, :cond_6

    .line 449478
    const-string v1, "game_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449479
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449480
    :cond_6
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 449481
    if-eqz v0, :cond_7

    .line 449482
    const-string v0, "game_orientation"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449483
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449484
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449485
    if-eqz v0, :cond_8

    .line 449486
    const-string v1, "game_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449487
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449488
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449489
    if-eqz v0, :cond_9

    .line 449490
    const-string v1, "icon_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449491
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449492
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449493
    if-eqz v0, :cond_a

    .line 449494
    const-string v1, "instant_game_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449495
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449496
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449497
    if-eqz v0, :cond_b

    .line 449498
    const-string v1, "label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449499
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449500
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449501
    if-eqz v0, :cond_c

    .line 449502
    const-string v1, "lat_long_list"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449503
    invoke-static {p0, v0, p2, p3}, LX/2sz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 449504
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 449505
    if-eqz v0, :cond_d

    .line 449506
    const-string v1, "layout_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449507
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 449508
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 449509
    if-eqz v0, :cond_e

    .line 449510
    const-string v1, "layout_width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449511
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 449512
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 449513
    if-eqz v0, :cond_f

    .line 449514
    const-string v1, "layout_x"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449515
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 449516
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 449517
    if-eqz v0, :cond_10

    .line 449518
    const-string v1, "layout_y"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449519
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 449520
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449521
    if-eqz v0, :cond_11

    .line 449522
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449523
    invoke-static {p0, v0, p2}, LX/2sz;->a(LX/15i;ILX/0nX;)V

    .line 449524
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449525
    if-eqz v0, :cond_12

    .line 449526
    const-string v1, "logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449527
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 449528
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449529
    if-eqz v0, :cond_13

    .line 449530
    const-string v1, "mobile_game_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449531
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449532
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449533
    if-eqz v0, :cond_14

    .line 449534
    const-string v1, "nearby_locations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449535
    invoke-static {p0, v0, p2, p3}, LX/2sz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 449536
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 449537
    if-eqz v0, :cond_15

    .line 449538
    const-string v1, "show_objectionable_warning"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449539
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 449540
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449541
    if-eqz v0, :cond_16

    .line 449542
    const-string v1, "splash_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449543
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449544
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449545
    if-eqz v0, :cond_17

    .line 449546
    const-string v1, "video_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449547
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449548
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449549
    if-eqz v0, :cond_18

    .line 449550
    const-string v1, "place_rec_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449551
    invoke-static {p0, v0, p2, p3}, LX/4RR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 449552
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449553
    if-eqz v0, :cond_19

    .line 449554
    const-string v1, "instant_experience_app_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449555
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449556
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449557
    if-eqz v0, :cond_1a

    .line 449558
    const-string v1, "instant_experience_link_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449559
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449560
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449561
    if-eqz v0, :cond_1b

    .line 449562
    const-string v1, "contextual_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449563
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 449564
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449565
    if-eqz v0, :cond_1c

    .line 449566
    const-string v1, "parent_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449567
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 449568
    :cond_1c
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 449569
    if-eqz v0, :cond_1d

    .line 449570
    const-string v0, "attributes"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449571
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 449572
    :cond_1d
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 449573
    if-eqz v0, :cond_1e

    .line 449574
    const-string v0, "instant_experience_domain_whitelist"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449575
    invoke-virtual {p0, p1, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 449576
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449577
    if-eqz v0, :cond_1f

    .line 449578
    const-string v1, "instant_experience_app"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449579
    invoke-static {p0, v0, p2, p3}, LX/2uk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 449580
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449581
    if-eqz v0, :cond_20

    .line 449582
    const-string v1, "instant_experience_user_app_scoped_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449583
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449584
    :cond_20
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449585
    if-eqz v0, :cond_21

    .line 449586
    const-string v1, "instant_experience_user_page_scoped_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449587
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449588
    :cond_21
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449589
    if-eqz v0, :cond_22

    .line 449590
    const-string v1, "broadcaster"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449591
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 449592
    :cond_22
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449593
    if-eqz v0, :cond_23

    .line 449594
    const-string v1, "video_broadcast_schedule"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449595
    invoke-static {p0, v0, p2, p3}, LX/4UC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 449596
    :cond_23
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449597
    if-eqz v0, :cond_24

    .line 449598
    const-string v1, "instant_experience_feature_enabled_list"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449599
    invoke-static {p0, v0, p2}, LX/4RY;->a(LX/15i;ILX/0nX;)V

    .line 449600
    :cond_24
    const/16 v0, 0x25

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449601
    if-eqz v0, :cond_25

    .line 449602
    const-string v1, "destination_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449603
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449604
    :cond_25
    invoke-virtual {p0, p1, v6, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 449605
    if-eqz v0, :cond_26

    .line 449606
    const-string v0, "destination_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449607
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449608
    :cond_26
    const/16 v0, 0x27

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449609
    if-eqz v0, :cond_27

    .line 449610
    const-string v1, "native_template_view"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449611
    invoke-static {p0, v0, p2, p3}, LX/4Pn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 449612
    :cond_27
    const/16 v0, 0x28

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449613
    if-eqz v0, :cond_28

    .line 449614
    const-string v1, "instant_experience_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449615
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 449616
    :cond_28
    const/16 v0, 0x29

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 449617
    if-eqz v0, :cond_29

    .line 449618
    const-string v1, "instant_experience_ad_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449619
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 449620
    :cond_29
    const/16 v0, 0x2a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 449621
    if-eqz v0, :cond_2a

    .line 449622
    const-string v1, "group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 449623
    invoke-static {p0, v0, p2, p3}, LX/30j;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 449624
    :cond_2a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 449625
    return-void
.end method
