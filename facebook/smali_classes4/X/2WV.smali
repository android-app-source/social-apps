.class public LX/2WV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/2Y0;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 418973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418974
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, LX/2WV;->a:Landroid/util/SparseArray;

    .line 418975
    iput-object p1, p0, LX/2WV;->b:Landroid/content/Context;

    .line 418976
    return-void
.end method

.method private static declared-synchronized a(LX/2WV;Lcom/facebook/analytics2/logger/HandlerThreadFactory;ILX/2WX;LX/2Xu;)LX/2Xy;
    .locals 3

    .prologue
    .line 418977
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2WV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2WV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Y0;

    .line 418978
    iget-object v1, v0, LX/2Y0;->a:LX/2Xy;

    move-object v0, v1

    .line 418979
    if-eqz v0, :cond_0

    .line 418980
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to create a new handler when one already exists for jobId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418981
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 418982
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UploadJobHandlerManager-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/facebook/analytics2/logger/HandlerThreadFactory;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 418983
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 418984
    iget-object v1, p0, LX/2WV;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 418985
    new-instance v2, LX/2Xy;

    invoke-direct {v2, v1, v0, p3, p4}, LX/2Xy;-><init>(Landroid/content/Context;Landroid/os/Looper;LX/2WX;LX/2Xu;)V

    move-object v1, v2

    .line 418986
    iget-object v0, p0, LX/2WV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Y0;

    .line 418987
    if-nez v0, :cond_1

    .line 418988
    new-instance v0, LX/2Y0;

    invoke-direct {v0}, LX/2Y0;-><init>()V

    .line 418989
    iget-object v2, p0, LX/2WV;->a:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 418990
    :cond_1
    iput-object v1, v0, LX/2Y0;->a:LX/2Xy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418991
    monitor-exit p0

    return-object v1
.end method

.method private declared-synchronized a(LX/2WX;LX/2Xw;)V
    .locals 3

    .prologue
    .line 418992
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2WV;->b:Landroid/content/Context;

    invoke-static {v0}, LX/0pF;->a(Landroid/content/Context;)LX/0pF;

    move-result-object v0

    iget-object v1, p1, LX/2WX;->b:LX/2DI;

    .line 418993
    iget-object v2, v1, LX/2DI;->d:Ljava/lang/String;

    move-object v1, v2

    .line 418994
    invoke-virtual {v0, v1}, LX/0pF;->b(Ljava/lang/String;)Lcom/facebook/analytics2/logger/HandlerThreadFactory;

    move-result-object v0

    .line 418995
    iget v1, p1, LX/2WX;->a:I

    invoke-static {p0, v0, v1, p1, p2}, LX/2WV;->a(LX/2WV;Lcom/facebook/analytics2/logger/HandlerThreadFactory;ILX/2WX;LX/2Xu;)LX/2Xy;

    move-result-object v0

    .line 418996
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2Xy;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Xy;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418997
    monitor-exit p0

    return-void

    .line 418998
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/2WV;LX/2WX;LX/2Y0;LX/2Xu;)Z
    .locals 1
    .param p1    # LX/2WX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 418964
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 418965
    :try_start_0
    iget-object v0, p2, LX/2Y0;->a:LX/2Xy;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418966
    :goto_0
    if-eqz v0, :cond_1

    .line 418967
    const/4 v0, 0x0

    .line 418968
    :goto_1
    monitor-exit p0

    return v0

    .line 418969
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 418970
    :cond_1
    :try_start_1
    invoke-static {p0, p1, p3}, LX/2WV;->c(LX/2WV;LX/2WX;LX/2Xu;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418971
    const/4 v0, 0x1

    goto :goto_1

    .line 418972
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized c(LX/2WV;LX/2WX;LX/2Xu;)V
    .locals 2

    .prologue
    .line 418942
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/2Xw;

    iget v1, p1, LX/2WX;->a:I

    invoke-direct {v0, p0, v1, p2}, LX/2Xw;-><init>(LX/2WV;ILX/2Xu;)V

    invoke-direct {p0, p1, v0}, LX/2WV;->a(LX/2WX;LX/2Xw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418943
    monitor-exit p0

    return-void

    .line 418944
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 418956
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2WV;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Y0;

    .line 418957
    if-eqz v0, :cond_1

    .line 418958
    iget-object p1, v0, LX/2Y0;->a:LX/2Xy;

    move-object v0, p1

    .line 418959
    :goto_0
    if-eqz v0, :cond_0

    .line 418960
    const/4 p1, 0x3

    invoke-virtual {v0, p1}, LX/2Xy;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {v0, p1}, LX/2Xy;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418961
    :cond_0
    monitor-exit p0

    return-void

    .line 418962
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 418963
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2WX;LX/2Xu;)Z
    .locals 2

    .prologue
    .line 418953
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2WV;->a:Landroid/util/SparseArray;

    iget v1, p1, LX/2WX;->a:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Y0;

    .line 418954
    invoke-static {p0, p1, v0, p2}, LX/2WV;->a(LX/2WV;LX/2WX;LX/2Y0;LX/2Xu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 418955
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/2WX;LX/2Xu;)V
    .locals 2

    .prologue
    .line 418945
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/2WV;->a:Landroid/util/SparseArray;

    iget v1, p1, LX/2WX;->a:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Y0;

    .line 418946
    invoke-static {p0, p1, v0, p2}, LX/2WV;->a(LX/2WV;LX/2WX;LX/2Y0;LX/2Xu;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 418947
    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Y0;

    new-instance v1, Lcom/facebook/analytics2/logger/UploadJobHandlerManager$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/analytics2/logger/UploadJobHandlerManager$1;-><init>(LX/2WV;LX/2WX;LX/2Xu;)V

    .line 418948
    iget-object p1, v0, LX/2Y0;->b:Ljava/util/ArrayDeque;

    if-nez p1, :cond_0

    .line 418949
    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, v0, LX/2Y0;->b:Ljava/util/ArrayDeque;

    .line 418950
    :cond_0
    iget-object p1, v0, LX/2Y0;->b:Ljava/util/ArrayDeque;

    invoke-virtual {p1, v1}, Ljava/util/ArrayDeque;->offer(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418951
    :cond_1
    monitor-exit p0

    return-void

    .line 418952
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
