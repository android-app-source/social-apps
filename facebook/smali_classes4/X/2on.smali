.class public final LX/2on;
.super Landroid/os/Handler;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0SG;

.field private d:Z

.field private e:I

.field private f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 467273
    const-class v0, LX/2on;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2on;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/player/RichVideoPlayer;LX/0SG;)V
    .locals 1

    .prologue
    .line 467302
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 467303
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467304
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467305
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2on;->b:Ljava/lang/ref/WeakReference;

    .line 467306
    iput-object p2, p0, LX/2on;->c:LX/0SG;

    .line 467307
    return-void
.end method

.method private a(F)V
    .locals 1

    .prologue
    .line 467298
    invoke-direct {p0}, LX/2on;->c()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 467299
    if-eqz v0, :cond_0

    .line 467300
    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->setVolume(F)V

    .line 467301
    :cond_0
    return-void
.end method

.method private c()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 467297
    iget-object v0, p0, LX/2on;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 467295
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, LX/2on;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 467296
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 467287
    iget v0, p0, LX/2on;->e:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 467288
    iget-object v0, p0, LX/2on;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/2on;->f:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    iget v1, p0, LX/2on;->e:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 467289
    float-to-double v0, v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 467290
    invoke-direct {p0, v0}, LX/2on;->a(F)V

    .line 467291
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 467292
    invoke-direct {p0}, LX/2on;->d()V

    .line 467293
    :cond_0
    return-void

    .line 467294
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 467281
    iget-boolean v0, p0, LX/2on;->d:Z

    if-eqz v0, :cond_0

    .line 467282
    invoke-direct {p0}, LX/2on;->c()Lcom/facebook/video/player/RichVideoPlayer;

    .line 467283
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2on;->d:Z

    .line 467284
    iget-object v0, p0, LX/2on;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/2on;->f:J

    .line 467285
    invoke-direct {p0}, LX/2on;->d()V

    .line 467286
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 467277
    invoke-direct {p0}, LX/2on;->c()Lcom/facebook/video/player/RichVideoPlayer;

    .line 467278
    iput-boolean v0, p0, LX/2on;->d:Z

    .line 467279
    invoke-virtual {p0, v0}, LX/2on;->removeMessages(I)V

    .line 467280
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 467274
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 467275
    invoke-direct {p0}, LX/2on;->e()V

    .line 467276
    :cond_0
    return-void
.end method
