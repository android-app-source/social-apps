.class public LX/36D;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C5s;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C5u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 498420
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/36D;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C5u;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 498417
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 498418
    iput-object p1, p0, LX/36D;->b:LX/0Ot;

    .line 498419
    return-void
.end method

.method public static a(LX/0QB;)LX/36D;
    .locals 4

    .prologue
    .line 498421
    const-class v1, LX/36D;

    monitor-enter v1

    .line 498422
    :try_start_0
    sget-object v0, LX/36D;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 498423
    sput-object v2, LX/36D;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498424
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498425
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 498426
    new-instance v3, LX/36D;

    const/16 p0, 0x1f72

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/36D;-><init>(LX/0Ot;)V

    .line 498427
    move-object v0, v3

    .line 498428
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 498429
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/36D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498430
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 498431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 498416
    const v0, 0x4d442695    # 2.05678928E8f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 498410
    check-cast p2, LX/C5t;

    .line 498411
    iget-object v0, p0, LX/36D;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 498412
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    const p0, 0x7f081032

    invoke-virtual {v0, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v0

    const p0, -0xb1a99b

    invoke-virtual {v0, p0}, LX/1ne;->m(I)LX/1ne;

    move-result-object v0

    const p0, 0x7f0b004e

    invoke-virtual {v0, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const/4 p0, 0x1

    invoke-virtual {v0, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v0

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v0

    sget-object p0, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v0, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    .line 498413
    const p0, 0x4d442695    # 2.05678928E8f

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 498414
    invoke-interface {v0, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 498415
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 498385
    invoke-static {}, LX/1dS;->b()V

    .line 498386
    iget v0, p1, LX/1dQ;->b:I

    .line 498387
    packed-switch v0, :pswitch_data_0

    .line 498388
    :goto_0
    return-object v2

    .line 498389
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 498390
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 498391
    check-cast v1, LX/C5t;

    .line 498392
    iget-object v3, p0, LX/36D;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C5u;

    iget-object v4, v1, LX/C5t;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 498393
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 498394
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 498395
    iget-object p2, v3, LX/C5u;->b:LX/0Uh;

    const/16 p0, 0x4d8

    const/4 v1, 0x0

    invoke-virtual {p2, p0, v1}, LX/0Uh;->a(IZ)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p0, 0x2

    const/4 v0, 0x0

    .line 498396
    if-nez p1, :cond_1

    move p2, p0

    .line 498397
    :goto_1
    move p2, p2

    .line 498398
    :goto_2
    new-instance p0, LX/1ZV;

    invoke-direct {p0, p1, p2}, LX/1ZV;-><init>(Lcom/facebook/graphql/model/GraphQLStory;I)V

    .line 498399
    iget-object p1, v3, LX/C5u;->a:LX/0bH;

    invoke-virtual {p1, p0}, LX/0b4;->a(LX/0b7;)V

    .line 498400
    goto :goto_0

    .line 498401
    :cond_0
    const/4 p2, 0x2

    goto :goto_2

    .line 498402
    :cond_1
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object p2

    .line 498403
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->l()I

    move-result p2

    if-lez p2, :cond_2

    .line 498404
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object p2

    .line 498405
    if-eqz p2, :cond_2

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 498406
    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object p2

    .line 498407
    if-eqz p2, :cond_2

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne p2, v1, :cond_2

    .line 498408
    const/16 p2, 0xa

    goto :goto_1

    :cond_2
    move p2, p0

    .line 498409
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x4d442695
        :pswitch_0
    .end packed-switch
.end method
