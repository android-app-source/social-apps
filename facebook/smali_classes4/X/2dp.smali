.class public LX/2dp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2do;

.field private final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0kL;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/2do;LX/1Ck;LX/0kL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;",
            "LX/2do;",
            "LX/1Ck;",
            "LX/0kL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444327
    iput-object p1, p0, LX/2dp;->a:Landroid/content/Context;

    .line 444328
    iput-object p2, p0, LX/2dp;->b:LX/0Ot;

    .line 444329
    iput-object p3, p0, LX/2dp;->c:LX/2do;

    .line 444330
    iput-object p4, p0, LX/2dp;->d:LX/1Ck;

    .line 444331
    iput-object p5, p0, LX/2dp;->e:LX/0kL;

    .line 444332
    return-void
.end method

.method public static b(LX/0QB;)LX/2dp;
    .locals 6

    .prologue
    .line 444333
    new-instance v0, LX/2dp;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0xa71

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v3

    check-cast v3, LX/2do;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-direct/range {v0 .. v5}, LX/2dp;-><init>(Landroid/content/Context;LX/0Ot;LX/2do;LX/1Ck;LX/0kL;)V

    .line 444334
    return-object v0
.end method


# virtual methods
.method public final a(LX/2iQ;ZJLjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p6    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 444335
    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v1 .. v8}, LX/2dp;->a(LX/2iQ;ZJLjava/lang/String;Ljava/lang/String;LX/84Y;)V

    .line 444336
    return-void
.end method

.method public final a(LX/2iQ;ZJLjava/lang/String;Ljava/lang/String;LX/84Y;)V
    .locals 11
    .param p6    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/84Y;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 444337
    iget-object v0, p0, LX/2dp;->c:LX/2do;

    new-instance v1, LX/2iD;

    invoke-direct {v1, p3, p4, p2}, LX/2iD;-><init>(JZ)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 444338
    iget-object v8, p0, LX/2dp;->d:LX/1Ck;

    const-string v9, "PERFORM_FUTURE_FRIENDING_TASK"

    new-instance v0, LX/84W;

    move-object v1, p0

    move v2, p2

    move-object v3, p1

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, LX/84W;-><init>(LX/2dp;ZLX/2iQ;Ljava/lang/String;Ljava/lang/String;J)V

    new-instance v1, LX/84X;

    move-object v2, p0

    move-object/from16 v3, p7

    move-wide v4, p3

    move v6, p2

    invoke-direct/range {v1 .. v6}, LX/84X;-><init>(LX/2dp;LX/84Y;JZ)V

    invoke-virtual {v8, v9, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 444339
    return-void
.end method
