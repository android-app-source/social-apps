.class public LX/33C;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/2sb;


# direct methods
.method public constructor <init>(LX/2sb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 491730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491731
    iput-object p1, p0, LX/33C;->a:LX/2sb;

    .line 491732
    return-void
.end method

.method public static a(LX/0QB;)LX/33C;
    .locals 4

    .prologue
    .line 491734
    const-class v1, LX/33C;

    monitor-enter v1

    .line 491735
    :try_start_0
    sget-object v0, LX/33C;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 491736
    sput-object v2, LX/33C;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 491737
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491738
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 491739
    new-instance p0, LX/33C;

    invoke-static {v0}, LX/2sb;->a(LX/0QB;)LX/2sb;

    move-result-object v3

    check-cast v3, LX/2sb;

    invoke-direct {p0, v3}, LX/33C;-><init>(LX/2sb;)V

    .line 491740
    move-object v0, p0

    .line 491741
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 491742
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/33C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491743
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 491744
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 491733
    invoke-static {p0}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
