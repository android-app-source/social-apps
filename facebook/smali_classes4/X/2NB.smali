.class public LX/2NB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2N8;

.field private final b:LX/2NC;


# direct methods
.method public constructor <init>(LX/2N8;LX/2NC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 398642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398643
    iput-object p1, p0, LX/2NB;->a:LX/2N8;

    .line 398644
    iput-object p2, p0, LX/2NB;->b:LX/2NC;

    .line 398645
    return-void
.end method

.method private a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/0m9;
    .locals 5

    .prologue
    .line 398646
    if-nez p1, :cond_1

    .line 398647
    const/4 v0, 0x0

    .line 398648
    :cond_0
    :goto_0
    return-object v0

    .line 398649
    :cond_1
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 398650
    const-string v1, "uri"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398651
    const-string v1, "type"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    iget-object v2, v2, LX/2MK;->DBSerialValue:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398652
    const-string v1, "source"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    iget-object v2, v2, LX/5zj;->DBSerialValue:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398653
    const-string v1, "thumbnailUri"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    invoke-static {v2}, LX/2NB;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398654
    const-string v1, "mediaItemId"

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 398655
    const-string v1, "originalMediaResource"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {p0, v2}, LX/2NB;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 398656
    const-string v1, "duration"

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 398657
    const-string v1, "width"

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 398658
    const-string v1, "height"

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 398659
    const-string v1, "orientationHint"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    iget v2, v2, LX/47d;->exifValue:I

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 398660
    const-string v1, "offlineThreadingId"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398661
    const-string v1, "mimeType"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398662
    const-string v1, "fileSize"

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 398663
    const-string v1, "cropArea"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    const/16 p0, 0x2c

    .line 398664
    if-nez v2, :cond_2

    .line 398665
    const/4 v3, 0x0

    .line 398666
    :goto_1
    move-object v2, v3

    .line 398667
    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398668
    const-string v1, "wantFirstFrameForThumbnail"

    iget-boolean v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->u:Z

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 398669
    const-string v1, "trimStartTimeMs"

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 398670
    const-string v1, "trimEndTimeMs"

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 398671
    const-string v1, "fbid"

    invoke-virtual {p1}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398672
    const-string v1, "externalContentUri"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    invoke-static {v2}, LX/2NB;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398673
    const-string v1, "isTrustedExternalContentProvider"

    iget-boolean v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->C:Z

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 398674
    const-string v1, "contentAppAttribution"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    invoke-static {v2}, LX/2NC;->a(Lcom/facebook/messaging/model/attribution/ContentAppAttribution;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398675
    const-string v1, "renderAsSticker"

    iget-boolean v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->E:Z

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 398676
    const-string v1, "isVoicemail"

    iget-boolean v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->F:Z

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 398677
    const-string v1, "callId"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->G:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398678
    const-string v1, "encryptionKeyBase64"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 398679
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v1, :cond_0

    .line 398680
    const-string v1, "threadKey"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto/16 :goto_0

    .line 398681
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 398682
    iget v4, v2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 398683
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 398684
    iget v4, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 398685
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 398686
    iget v4, v2, Landroid/graphics/RectF;->right:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 398687
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 398688
    iget v4, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 398689
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1
.end method

.method public static a(LX/0QB;)LX/2NB;
    .locals 1

    .prologue
    .line 398690
    invoke-static {p0}, LX/2NB;->b(LX/0QB;)LX/2NB;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0lF;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 398691
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0lF;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398692
    :cond_0
    const/4 v0, 0x0

    .line 398693
    :goto_0
    return-object v0

    .line 398694
    :cond_1
    new-instance v0, Lcom/facebook/ui/media/attachments/MediaUploadResult;

    const-string v1, "fbid"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/ui/media/attachments/MediaUploadResult;-><init>(Ljava/lang/String;)V

    .line 398695
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v1

    const-string v2, "uri"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 398696
    iput-object v2, v1, LX/5zn;->b:Landroid/net/Uri;

    .line 398697
    move-object v1, v1

    .line 398698
    const-string v2, "type"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/2MK;->fromDBSerialValue(Ljava/lang/String;)LX/2MK;

    move-result-object v2

    .line 398699
    iput-object v2, v1, LX/5zn;->c:LX/2MK;

    .line 398700
    move-object v1, v1

    .line 398701
    const-string v2, "source"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 398702
    if-nez v2, :cond_2

    .line 398703
    sget-object v3, LX/5zj;->UNSPECIFIED:LX/5zj;

    .line 398704
    :goto_1
    move-object v2, v3

    .line 398705
    iput-object v2, v1, LX/5zn;->d:LX/5zj;

    .line 398706
    move-object v1, v1

    .line 398707
    const-string v2, "thumbnailUri"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/2NB;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 398708
    iput-object v2, v1, LX/5zn;->f:Landroid/net/Uri;

    .line 398709
    move-object v1, v1

    .line 398710
    const-string v2, "mediaItemId"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->c(LX/0lF;)J

    move-result-wide v2

    .line 398711
    iput-wide v2, v1, LX/5zn;->h:J

    .line 398712
    move-object v1, v1

    .line 398713
    const-string v2, "originalMediaResource"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-direct {p0, v2}, LX/2NB;->a(LX/0lF;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v2

    .line 398714
    iput-object v2, v1, LX/5zn;->g:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 398715
    move-object v1, v1

    .line 398716
    const-string v2, "duration"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->c(LX/0lF;)J

    move-result-wide v2

    .line 398717
    iput-wide v2, v1, LX/5zn;->i:J

    .line 398718
    move-object v1, v1

    .line 398719
    const-string v2, "width"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->d(LX/0lF;)I

    move-result v2

    .line 398720
    iput v2, v1, LX/5zn;->j:I

    .line 398721
    move-object v1, v1

    .line 398722
    const-string v2, "height"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->d(LX/0lF;)I

    move-result v2

    .line 398723
    iput v2, v1, LX/5zn;->k:I

    .line 398724
    move-object v1, v1

    .line 398725
    const-string v2, "orientationHint"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2, v4}, LX/16N;->a(LX/0lF;I)I

    move-result v2

    invoke-static {v2}, LX/47d;->fromExifInterfaceOrientation(I)LX/47d;

    move-result-object v2

    .line 398726
    iput-object v2, v1, LX/5zn;->l:LX/47d;

    .line 398727
    move-object v1, v1

    .line 398728
    const-string v2, "offlineThreadingId"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 398729
    iput-object v2, v1, LX/5zn;->o:Ljava/lang/String;

    .line 398730
    move-object v1, v1

    .line 398731
    const-string v2, "mimeType"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 398732
    iput-object v2, v1, LX/5zn;->q:Ljava/lang/String;

    .line 398733
    move-object v1, v1

    .line 398734
    const-string v2, "fileSize"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->d(LX/0lF;)I

    move-result v2

    int-to-long v2, v2

    .line 398735
    iput-wide v2, v1, LX/5zn;->r:J

    .line 398736
    move-object v1, v1

    .line 398737
    const-string v2, "cropArea"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 398738
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 398739
    sget-object v3, Lcom/facebook/ui/media/attachments/MediaResource;->b:Landroid/graphics/RectF;

    .line 398740
    :goto_2
    move-object v2, v3

    .line 398741
    iput-object v2, v1, LX/5zn;->s:Landroid/graphics/RectF;

    .line 398742
    move-object v1, v1

    .line 398743
    const-string v2, "wantFirstFrameForThumbnail"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->g(LX/0lF;)Z

    move-result v2

    .line 398744
    iput-boolean v2, v1, LX/5zn;->t:Z

    .line 398745
    move-object v1, v1

    .line 398746
    const-string v2, "trimStartTimeMs"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const/4 v3, -0x1

    invoke-static {v2, v3}, LX/16N;->a(LX/0lF;I)I

    move-result v2

    .line 398747
    iput v2, v1, LX/5zn;->u:I

    .line 398748
    move-object v1, v1

    .line 398749
    const-string v2, "trimEndTimeMs"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const/4 v3, -0x2

    invoke-static {v2, v3}, LX/16N;->a(LX/0lF;I)I

    move-result v2

    .line 398750
    iput v2, v1, LX/5zn;->v:I

    .line 398751
    move-object v1, v1

    .line 398752
    iput-object v0, v1, LX/5zn;->w:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 398753
    move-object v0, v1

    .line 398754
    const-string v1, "externalContentUri"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/2NB;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 398755
    iput-object v1, v0, LX/5zn;->z:Landroid/net/Uri;

    .line 398756
    move-object v0, v0

    .line 398757
    const-string v1, "isTrustedExternalContentProvider"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    .line 398758
    iput-boolean v1, v0, LX/5zn;->A:Z

    .line 398759
    move-object v0, v0

    .line 398760
    iget-object v1, p0, LX/2NB;->b:LX/2NC;

    const-string v2, "contentAppAttribution"

    invoke-virtual {p1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2NC;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object v1

    .line 398761
    iput-object v1, v0, LX/5zn;->y:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 398762
    move-object v0, v0

    .line 398763
    const-string v1, "renderAsSticker"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1, v4}, LX/16N;->a(LX/0lF;Z)Z

    move-result v1

    .line 398764
    iput-boolean v1, v0, LX/5zn;->C:Z

    .line 398765
    move-object v0, v0

    .line 398766
    const-string v1, "isVoicemail"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1, v4}, LX/16N;->a(LX/0lF;Z)Z

    move-result v1

    .line 398767
    iput-boolean v1, v0, LX/5zn;->D:Z

    .line 398768
    move-object v0, v0

    .line 398769
    const-string v1, "callId"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 398770
    iput-object v1, v0, LX/5zn;->E:Ljava/lang/String;

    .line 398771
    move-object v0, v0

    .line 398772
    const-string v1, "encryptionKeyBase64"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 398773
    iput-object v1, v0, LX/5zn;->F:Ljava/lang/String;

    .line 398774
    move-object v0, v0

    .line 398775
    const-string v1, "threadKey"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 398776
    iput-object v1, v0, LX/5zn;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 398777
    move-object v0, v0

    .line 398778
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    invoke-static {v2}, LX/5zj;->fromDBSerialValue(Ljava/lang/String;)LX/5zj;

    move-result-object v3

    goto/16 :goto_1

    .line 398779
    :cond_3
    const-string v3, ","

    invoke-static {v3}, LX/2Cb;->on(Ljava/lang/String;)LX/2Cb;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 398780
    new-instance v5, Landroid/graphics/RectF;

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-direct {v5, v7, v8, v9, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object v3, v5

    goto/16 :goto_2
.end method

.method private static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 398781
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/2NB;
    .locals 3

    .prologue
    .line 398782
    new-instance v2, LX/2NB;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v0

    check-cast v0, LX/2N8;

    invoke-static {p0}, LX/2NC;->b(LX/0QB;)LX/2NC;

    move-result-object v1

    check-cast v1, LX/2NC;

    invoke-direct {v2, v0, v1}, LX/2NB;-><init>(LX/2N8;LX/2NC;)V

    .line 398783
    return-object v2
.end method

.method private static b(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 398784
    if-eqz p0, :cond_0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 398785
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398786
    :cond_0
    const/4 v0, 0x0

    .line 398787
    :goto_0
    return-object v0

    .line 398788
    :cond_1
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 398789
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 398790
    invoke-direct {p0, v0}, LX/2NB;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/0m9;

    move-result-object v0

    .line 398791
    invoke-virtual {v1, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_1

    .line 398792
    :cond_2
    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398793
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398794
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 398795
    :goto_0
    return-object v0

    .line 398796
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 398797
    iget-object v0, p0, LX/2NB;->a:LX/2N8;

    invoke-virtual {v0, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 398798
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 398799
    invoke-direct {p0, v0}, LX/2NB;->a(LX/0lF;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 398800
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 398801
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
