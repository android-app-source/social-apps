.class public LX/2Tq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:LX/2Tr;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Tr;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/iptest/annotations/IsZeroIPTestsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Tr;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 414792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414793
    iput-object p1, p0, LX/2Tq;->a:LX/2Tr;

    .line 414794
    iput-object p2, p0, LX/2Tq;->b:LX/0Or;

    .line 414795
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 9

    .prologue
    .line 414796
    iget-object v0, p0, LX/2Tq;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414797
    iget-object v0, p0, LX/2Tq;->a:LX/2Tr;

    .line 414798
    invoke-virtual {v0}, LX/2Tr;->a()V

    .line 414799
    iget-object v2, v0, LX/2Tr;->d:LX/12x;

    const/4 v3, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/32 v6, 0x1b7740

    add-long/2addr v4, v6

    const-wide/32 v6, 0x2932e00

    invoke-static {v0}, LX/2Tr;->d(LX/2Tr;)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, LX/12x;->a(IJJLandroid/app/PendingIntent;)V

    .line 414800
    :cond_0
    return-void
.end method
