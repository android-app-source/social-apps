.class public final LX/312;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/312;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/311;

.field public b:I

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(LX/311;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 486654
    iput-object p1, p0, LX/312;->a:LX/311;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486655
    iput p2, p0, LX/312;->b:I

    .line 486656
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, LX/312;->d:I

    .line 486657
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    :goto_0
    iput v0, p0, LX/312;->e:I

    .line 486658
    return-void

    .line 486659
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 486660
    iget v0, p0, LX/312;->b:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 486661
    iget v0, p0, LX/312;->e:I

    return v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 486662
    check-cast p1, LX/312;

    .line 486663
    iget v0, p0, LX/312;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 486664
    iget v1, p1, LX/312;->b:I

    move v1, v1

    .line 486665
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method
