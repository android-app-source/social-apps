.class public final LX/3G9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/location/write/graphql/LocationMutationsModels$LocationUpdateMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1sE;


# direct methods
.method public constructor <init>(LX/1sE;)V
    .locals 0

    .prologue
    .line 540664
    iput-object p1, p0, LX/3G9;->a:LX/1sE;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 540665
    iget-object v0, p0, LX/3G9;->a:LX/1sE;

    iget-object v0, v0, LX/1sE;->d:LX/1ra;

    invoke-virtual {v0, p1}, LX/1ra;->a(Ljava/lang/Throwable;)V

    .line 540666
    iget-object v0, p0, LX/3G9;->a:LX/1sE;

    const/4 v1, 0x0

    .line 540667
    iput-object v1, v0, LX/1sE;->g:LX/1Mv;

    .line 540668
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 540669
    iget-object v0, p0, LX/3G9;->a:LX/1sE;

    iget-object v0, v0, LX/1sE;->d:LX/1ra;

    const-wide/high16 v10, -0x8000000000000000L

    const/4 v3, 0x1

    .line 540670
    invoke-static {v0, v3}, LX/1ra;->a(LX/1ra;Z)V

    .line 540671
    invoke-static {v0, v3}, LX/1ra;->b(LX/1ra;Z)V

    .line 540672
    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/1ra;->c(LX/1ra;Z)V

    .line 540673
    invoke-static {v0, v3}, LX/1ra;->d(LX/1ra;Z)V

    .line 540674
    iget v2, v0, LX/1ra;->o:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/1ra;->o:I

    .line 540675
    const-string v2, "fgl_write_success"

    invoke-static {v0, v2}, LX/1ra;->a(LX/1ra;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    .line 540676
    if-eqz v2, :cond_0

    .line 540677
    iget-object v3, v0, LX/1ra;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    .line 540678
    iget-wide v6, v0, LX/1ra;->h:J

    sub-long v6, v4, v6

    .line 540679
    iget-wide v8, v0, LX/1ra;->f:J

    sub-long/2addr v4, v8

    .line 540680
    const-string v3, "write_duration_ms"

    invoke-virtual {v2, v3, v6, v7}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 540681
    const-string v3, "request_duration_ms"

    invoke-virtual {v2, v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 540682
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 540683
    :cond_0
    iput-wide v10, v0, LX/1ra;->h:J

    .line 540684
    iput-wide v10, v0, LX/1ra;->g:J

    .line 540685
    iput-wide v10, v0, LX/1ra;->f:J

    .line 540686
    iget-object v0, p0, LX/3G9;->a:LX/1sE;

    const/4 v1, 0x0

    .line 540687
    iput-object v1, v0, LX/1sE;->g:LX/1Mv;

    .line 540688
    return-void
.end method
