.class public LX/3Hz;
.super LX/2oy;
.source ""

# interfaces
.implements LX/3Gr;


# static fields
.field public static final q:Ljava/lang/String;

.field public static final r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/2oy;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/2pa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Z

.field private C:Z

.field public D:Z

.field public E:LX/3H0;

.field public F:J

.field public G:J

.field public a:LX/3I1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3I2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/3HR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/3HS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/3H4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final s:LX/3I0;

.field public final t:LX/3I5;

.field public u:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/2pa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:LX/0lF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:LX/D6v;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 545375
    const-class v0, LX/3Hz;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Hz;->q:Ljava/lang/String;

    .line 545376
    const-class v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3Hz;->r:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 545377
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Hz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 545378
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 545379
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3Hz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545380
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    .prologue
    .line 545381
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 545382
    new-instance v0, LX/3I0;

    invoke-direct {v0, p0}, LX/3I0;-><init>(LX/3Hz;)V

    iput-object v0, p0, LX/3Hz;->s:LX/3I0;

    .line 545383
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p3

    move-object v2, p0

    check-cast v2, LX/3Hz;

    invoke-static {p3}, LX/3I1;->a(LX/0QB;)LX/3I1;

    move-result-object v3

    check-cast v3, LX/3I1;

    invoke-static {p3}, LX/3I2;->a(LX/0QB;)LX/3I2;

    move-result-object v4

    check-cast v4, LX/3I2;

    invoke-static {p3}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-static {p3}, LX/3HR;->b(LX/0QB;)LX/3HR;

    move-result-object v6

    check-cast v6, LX/3HR;

    invoke-static {p3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {p3}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v8

    check-cast v8, LX/0So;

    invoke-static {p3}, LX/3HS;->a(LX/0QB;)LX/3HS;

    move-result-object p1

    check-cast p1, LX/3HS;

    invoke-static {p3}, LX/3H4;->a(LX/0QB;)LX/3H4;

    move-result-object p2

    check-cast p2, LX/3H4;

    const/16 v0, 0x122d

    invoke-static {p3, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p3

    iput-object v3, v2, LX/3Hz;->a:LX/3I1;

    iput-object v4, v2, LX/3Hz;->b:LX/3I2;

    iput-object v5, v2, LX/3Hz;->c:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iput-object v6, v2, LX/3Hz;->d:LX/3HR;

    iput-object v7, v2, LX/3Hz;->e:LX/03V;

    iput-object v8, v2, LX/3Hz;->f:LX/0So;

    iput-object p1, v2, LX/3Hz;->n:LX/3HS;

    iput-object p2, v2, LX/3Hz;->o:LX/3H4;

    iput-object p3, v2, LX/3Hz;->p:LX/0Or;

    .line 545384
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3I3;

    invoke-direct {v1, p0}, LX/3I3;-><init>(LX/3Hz;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545385
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/3I4;

    invoke-direct {v1, p0}, LX/3I4;-><init>(LX/3Hz;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545386
    new-instance v0, LX/3I5;

    invoke-direct {v0, p0}, LX/3I5;-><init>(LX/3Hz;)V

    iput-object v0, p0, LX/3Hz;->t:LX/3I5;

    .line 545387
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 545388
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    iget-object v0, v0, LX/D6v;->a:LX/2oN;

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-eq v0, v1, :cond_0

    .line 545389
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    iget-object v1, p0, LX/3Hz;->y:LX/0lF;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/0lF;)V

    .line 545390
    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 545391
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    iget-object v0, v0, LX/D6v;->a:LX/2oN;

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-eq v0, v1, :cond_0

    .line 545392
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    iget-object v1, p0, LX/3Hz;->y:LX/0lF;

    invoke-virtual {v0, v1}, LX/D6v;->b(LX/0lF;)V

    .line 545393
    :cond_0
    return-void
.end method

.method public static x$redex0(LX/3Hz;)Z
    .locals 8

    .prologue
    .line 545317
    iget-object v0, p0, LX/3Hz;->E:LX/3H0;

    sget-object v1, LX/3H0;->VOD:LX/3H0;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/3Hz;->a:LX/3I1;

    .line 545318
    iget v4, v0, LX/3I1;->c:I

    int-to-long v4, v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    move-wide v0, v4

    .line 545319
    iget-object v2, p0, LX/3Hz;->a:LX/3I1;

    iget v2, v2, LX/3I1;->d:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    .line 545320
    :goto_0
    iget-object v2, p0, LX/2oy;->j:LX/2pb;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->A()J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-gtz v0, :cond_2

    .line 545321
    :cond_0
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->A()J

    .line 545322
    const/4 v0, 0x0

    .line 545323
    :goto_1
    return v0

    .line 545324
    :cond_1
    iget-object v0, p0, LX/3Hz;->b:LX/3I2;

    .line 545325
    iget v4, v0, LX/3I2;->d:I

    int-to-long v4, v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    move-wide v0, v4

    .line 545326
    iget-object v2, p0, LX/3Hz;->b:LX/3I2;

    iget v2, v2, LX/3I2;->c:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_0

    .line 545327
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 545328
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 545329
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 545330
    :cond_0
    :goto_0
    return-void

    .line 545331
    :cond_1
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 545332
    instance-of v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_2

    .line 545333
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, LX/3Hz;->w:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 545334
    :cond_2
    iget-object v0, p0, LX/3Hz;->w:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 545335
    if-eqz v0, :cond_0

    .line 545336
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 545337
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 545338
    if-eqz v0, :cond_0

    .line 545339
    invoke-static {v0}, LX/393;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 545340
    sget-object v1, LX/3H0;->VOD:LX/3H0;

    iput-object v1, p0, LX/3Hz;->E:LX/3H0;

    .line 545341
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aj()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3Hz;->u:LX/0Px;

    .line 545342
    iget-object v0, p0, LX/3Hz;->u:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Hz;->u:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 545343
    iget-object v0, p0, LX/3Hz;->u:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_2
    if-ge v1, v3, :cond_3

    iget-object v0, p0, LX/3Hz;->u:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;

    .line 545344
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;->l()Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->MID_ROLL:Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    if-ne v0, v4, :cond_c

    .line 545345
    iput-boolean v5, p0, LX/3Hz;->C:Z

    .line 545346
    :cond_3
    iget-boolean v0, p0, LX/3Hz;->C:Z

    if-eqz v0, :cond_0

    .line 545347
    iget-object v0, p0, LX/3Hz;->E:LX/3H0;

    sget-object v1, LX/3H0;->VOD:LX/3H0;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, LX/3Hz;->a:LX/3I1;

    iget-boolean v0, v0, LX/3I1;->e:Z

    if-nez v0, :cond_4

    .line 545348
    iget-object v0, p0, LX/3Hz;->a:LX/3I1;

    invoke-virtual {v0}, LX/3I1;->b()V

    .line 545349
    :cond_4
    iget-object v0, p0, LX/3Hz;->E:LX/3H0;

    sget-object v1, LX/3H0;->VOD:LX/3H0;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, LX/3Hz;->a:LX/3I1;

    iget-boolean v0, v0, LX/3I1;->b:Z

    if-nez v0, :cond_6

    :cond_5
    iget-object v0, p0, LX/3Hz;->E:LX/3H0;

    sget-object v1, LX/3H0;->NONLIVE:LX/3H0;

    if-ne v0, v1, :cond_7

    iget-object v0, p0, LX/3Hz;->b:LX/3I2;

    iget-boolean v0, v0, LX/3I2;->b:Z

    if-eqz v0, :cond_7

    .line 545350
    :cond_6
    iput-object p1, p0, LX/3Hz;->v:LX/2pa;

    .line 545351
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, LX/3Hz;->x:Ljava/lang/String;

    .line 545352
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    iput-boolean v0, p0, LX/3Hz;->B:Z

    .line 545353
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iput-object v0, p0, LX/3Hz;->y:LX/0lF;

    .line 545354
    iget-object v0, p0, LX/3Hz;->c:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/3Hz;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v0

    iput-object v0, p0, LX/3Hz;->z:LX/D6v;

    .line 545355
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    iget-object v1, p0, LX/3Hz;->E:LX/3H0;

    .line 545356
    iput-object v1, v0, LX/D6v;->w:LX/3H0;

    .line 545357
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    iget-boolean v1, p0, LX/3Hz;->B:Z

    .line 545358
    iput-boolean v1, v0, LX/D6v;->C:Z

    .line 545359
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->s()LX/04D;

    move-result-object v1

    .line 545360
    iput-object v1, v0, LX/D6v;->G:LX/04D;

    .line 545361
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    iget-object v1, p0, LX/3Hz;->s:LX/3I0;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/3GF;)V

    .line 545362
    iput-boolean v2, p0, LX/3Hz;->D:Z

    .line 545363
    iget-object v0, p0, LX/3Hz;->c:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/3Hz;->x:Ljava/lang/String;

    iget-object v2, p0, LX/3Hz;->v:LX/2pa;

    iget-wide v2, v2, LX/2pa;->d:D

    double-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(Ljava/lang/String;F)V

    .line 545364
    iget-object v0, p0, LX/3Hz;->n:LX/3HS;

    invoke-virtual {v0, p0}, LX/3HS;->a(LX/3Gr;)V

    .line 545365
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_7

    .line 545366
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 545367
    iput-boolean v5, v0, LX/2pb;->O:Z

    .line 545368
    :cond_7
    if-eqz p2, :cond_0

    .line 545369
    iget-object v0, p0, LX/3Hz;->E:LX/3H0;

    sget-object v1, LX/3H0;->NONLIVE:LX/3H0;

    if-ne v0, v1, :cond_8

    iget-object v0, p0, LX/3Hz;->b:LX/3I2;

    iget-boolean v0, v0, LX/3I2;->b:Z

    if-nez v0, :cond_9

    :cond_8
    iget-object v0, p0, LX/3Hz;->E:LX/3H0;

    sget-object v1, LX/3H0;->VOD:LX/3H0;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/3Hz;->a:LX/3I1;

    iget-boolean v0, v0, LX/3I1;->b:Z

    if-nez v0, :cond_9

    iget-object v0, p0, LX/3Hz;->a:LX/3I1;

    iget-boolean v0, v0, LX/3I1;->e:Z

    if-eqz v0, :cond_0

    .line 545370
    :cond_9
    iget-object v6, p0, LX/3Hz;->c:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v7, p0, LX/3Hz;->x:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g(Ljava/lang/String;)LX/2oN;

    move-result-object v6

    sget-object v7, LX/2oN;->NONE:LX/2oN;

    if-ne v6, v7, :cond_a

    .line 545371
    iget-object v6, p0, LX/3Hz;->t:LX/3I5;

    const/4 v7, 0x1

    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v7, v8, v9}, LX/3I5;->sendEmptyMessageDelayed(IJ)Z

    .line 545372
    :cond_a
    goto/16 :goto_0

    .line 545373
    :cond_b
    sget-object v1, LX/3H0;->NONLIVE:LX/3H0;

    iput-object v1, p0, LX/3Hz;->E:LX/3H0;

    goto/16 :goto_1

    .line 545374
    :cond_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2
.end method

.method public final a(LX/D6q;)V
    .locals 2

    .prologue
    .line 545313
    sget-object v0, LX/D7H;->c:[I

    invoke-virtual {p1}, LX/D6q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 545314
    :cond_0
    :goto_0
    return-void

    .line 545315
    :pswitch_0
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    if-eqz v0, :cond_0

    .line 545316
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    sget-object v1, LX/BSR;->HIDE_AD:LX/BSR;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/BSR;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 545305
    iput-object v1, p0, LX/3Hz;->w:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 545306
    iput-object v1, p0, LX/3Hz;->x:Ljava/lang/String;

    .line 545307
    iget-object v0, p0, LX/3Hz;->t:LX/3I5;

    invoke-virtual {v0, v1}, LX/3I5;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 545308
    iget-object v0, p0, LX/3Hz;->n:LX/3HS;

    invoke-virtual {v0, p0}, LX/3HS;->b(LX/3Gr;)V

    .line 545309
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Hz;->C:Z

    .line 545310
    iput-object v1, p0, LX/3Hz;->z:LX/D6v;

    .line 545311
    iput-object v1, p0, LX/3Hz;->y:LX/0lF;

    .line 545312
    return-void
.end method

.method public final dH_()V
    .locals 0

    .prologue
    .line 545303
    invoke-direct {p0}, LX/3Hz;->i()V

    .line 545304
    return-void
.end method

.method public final dI_()V
    .locals 2

    .prologue
    .line 545297
    iget-object v0, p0, LX/3Hz;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/3Hz;->F:J

    .line 545298
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 545299
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 545300
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-eq v0, v1, :cond_0

    .line 545301
    invoke-direct {p0}, LX/3Hz;->i()V

    .line 545302
    :cond_0
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 545294
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    if-eqz v0, :cond_0

    .line 545295
    iget-object v0, p0, LX/3Hz;->z:LX/D6v;

    invoke-virtual {v0}, LX/D6v;->f()V

    .line 545296
    :cond_0
    return-void
.end method

.method public final s()V
    .locals 0

    .prologue
    .line 545292
    invoke-direct {p0}, LX/3Hz;->j()V

    .line 545293
    return-void
.end method

.method public final t()V
    .locals 8

    .prologue
    .line 545275
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 545276
    iget-object v1, v0, LX/2pb;->D:LX/04G;

    move-object v0, v1

    .line 545277
    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-eq v0, v1, :cond_0

    .line 545278
    invoke-direct {p0}, LX/3Hz;->j()V

    .line 545279
    :cond_0
    iget-object v2, p0, LX/3Hz;->E:LX/3H0;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/3Hz;->E:LX/3H0;

    sget-object v3, LX/3H0;->VOD:LX/3H0;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, LX/3Hz;->f:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/3Hz;->F:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 545280
    :cond_1
    :goto_0
    return-void

    .line 545281
    :cond_2
    iget-object v2, p0, LX/3Hz;->x:Ljava/lang/String;

    if-nez v2, :cond_3

    const-string v2, "-1"

    move-object v3, v2

    .line 545282
    :goto_1
    iget-object v2, p0, LX/3Hz;->z:LX/D6v;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/3Hz;->z:LX/D6v;

    iget v2, v2, LX/D6v;->d:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_4

    .line 545283
    iget-object v2, p0, LX/3Hz;->p:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0gt;

    const-string v4, "337334756618657"

    .line 545284
    iput-object v4, v2, LX/0gt;->a:Ljava/lang/String;

    .line 545285
    move-object v2, v2

    .line 545286
    const-string v4, "video_id"

    invoke-virtual {v2, v4, v3}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v2

    const-string v3, "ad_id"

    iget-object v4, p0, LX/3Hz;->z:LX/D6v;

    invoke-virtual {v4}, LX/D6v;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v2

    const-string v3, "elapsed_time_since_ad"

    iget-object v4, p0, LX/3Hz;->f:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    iget-wide v6, p0, LX/3Hz;->G:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v2

    const-string v3, "ad_position_in_video"

    iget-object v4, p0, LX/3Hz;->z:LX/D6v;

    iget v4, v4, LX/D6v;->d:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v2

    const-string v3, "integration_point_name"

    const-string v4, "WASLIVE_COMMERICAL_BREAK_IMPRESSION_STOP_WATCHING"

    invoke-virtual {v2, v3, v4}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v2

    invoke-virtual {p0}, LX/3Hz;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 545287
    :cond_3
    iget-object v2, p0, LX/3Hz;->x:Ljava/lang/String;

    move-object v3, v2

    goto :goto_1

    .line 545288
    :cond_4
    iget-object v2, p0, LX/3Hz;->p:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0gt;

    const-string v4, "183500755390608"

    .line 545289
    iput-object v4, v2, LX/0gt;->a:Ljava/lang/String;

    .line 545290
    move-object v2, v2

    .line 545291
    const-string v4, "video_id"

    invoke-virtual {v2, v4, v3}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v2

    const-string v3, "integration_point_name"

    const-string v4, "WASLIVE_NO_COMMERCIAL_BREAK_IMPRESSION_STOP_WATCHING"

    invoke-virtual {v2, v3, v4}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v2

    invoke-virtual {p0}, LX/3Hz;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0gt;->a(Landroid/content/Context;)V

    goto/16 :goto_0
.end method
