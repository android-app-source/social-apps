.class public LX/2TE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 413866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/CellInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 413867
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    .line 413868
    new-instance v0, LX/JcK;

    sget-object v1, LX/JcJ;->INCOMPATIBLE_DEVICE:LX/JcJ;

    invoke-direct {v0, v1}, LX/JcK;-><init>(LX/JcJ;)V

    throw v0

    .line 413869
    :cond_0
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 413870
    :try_start_0
    invoke-static {v0}, LX/2TF;->a(Landroid/telephony/TelephonyManager;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 413871
    return-object v0

    .line 413872
    :catch_0
    new-instance v0, LX/JcK;

    sget-object v1, LX/JcJ;->PERMISSION_DENIED:LX/JcJ;

    invoke-direct {v0, v1}, LX/JcK;-><init>(LX/JcJ;)V

    throw v0
.end method
