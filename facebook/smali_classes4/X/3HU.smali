.class public final LX/3HU;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)V
    .locals 0

    .prologue
    .line 543533
    iput-object p1, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 543534
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 543535
    check-cast p1, LX/2ou;

    .line 543536
    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    iget-object v1, p1, LX/2ou;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 543537
    :cond_0
    :goto_0
    return-void

    .line 543538
    :cond_1
    sget-object v0, LX/D7A;->a:[I

    iget-object v1, p1, LX/2ou;->b:LX/2qV;

    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 543539
    :pswitch_0
    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->getLiveStreamingFormat(Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;)LX/BSW;

    move-result-object v0

    sget-object v1, LX/BSW;->HLS:LX/BSW;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-boolean v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->E:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_0

    .line 543540
    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    sget-object v1, LX/BSU;->STALL_STREAM:LX/BSU;

    invoke-virtual {v0, v1}, LX/D6v;->a(LX/BSU;)V

    goto :goto_0

    .line 543541
    :pswitch_1
    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    const/4 v1, 0x1

    .line 543542
    iput-boolean v1, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->E:Z

    .line 543543
    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    .line 543544
    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->d(Ljava/lang/String;)J

    move-result-wide v0

    .line 543545
    iget-object v2, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v2, v2, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->i()I

    move-result v2

    if-lez v2, :cond_0

    .line 543546
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 543547
    iget-object v2, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v2, v2, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->i()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v2, v0

    if-gtz v0, :cond_2

    .line 543548
    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    invoke-virtual {v0}, LX/D6v;->a()V

    goto :goto_0

    .line 543549
    :cond_2
    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->a:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->A:Ljava/lang/String;

    iget-object v2, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v2, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iget v2, v2, LX/D6v;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->c(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 543550
    :pswitch_2
    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v0, :cond_0

    .line 543551
    iget-object v0, p0, LX/3HU;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    .line 543552
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/D6v;->x:Z

    .line 543553
    const/4 v1, -0x1

    iput v1, v0, LX/D6v;->d:I

    .line 543554
    iget-object v1, v0, LX/D6v;->l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v2, v0, LX/D6v;->i:Ljava/lang/String;

    .line 543555
    iget-object v0, v1, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->k:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 543556
    iget-object v0, v1, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->m:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 543557
    iget-object v0, v1, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->n:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 543558
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
