.class public final LX/2qt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/2gj;


# direct methods
.method public constructor <init>(LX/2gj;)V
    .locals 0

    .prologue
    .line 471488
    iput-object p1, p0, LX/2qt;->a:LX/2gj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 471489
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 471490
    iget-object v1, p0, LX/2qt;->a:LX/2gj;

    iget-object v1, v1, LX/2gj;->b:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 471491
    :cond_0
    :goto_0
    return-object v0

    .line 471492
    :cond_1
    iget-object v1, p0, LX/2qt;->a:LX/2gj;

    iget-object v1, v1, LX/2gj;->b:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 471493
    iget-object v0, p0, LX/2qt;->a:LX/2gj;

    iget-object v0, v0, LX/2gj;->a:LX/2qs;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetchDialtoneQuota"

    .line 471494
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 471495
    move-object v0, v0

    .line 471496
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 471497
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 471498
    const-string v0, "fetchDialtoneQuota"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$FetchDialtonePhotoQuotaModel;

    .line 471499
    if-eqz v0, :cond_0

    .line 471500
    invoke-virtual {v0}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$FetchDialtonePhotoQuotaModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 471501
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471502
    if-eqz v0, :cond_0

    .line 471503
    iget-object v2, p0, LX/2qt;->a:LX/2gj;

    iget-object v2, v2, LX/2gj;->b:LX/0yc;

    invoke-virtual {v2, v1, v0}, LX/0yc;->a(LX/15i;I)V

    .line 471504
    :cond_0
    return-void

    .line 471505
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
