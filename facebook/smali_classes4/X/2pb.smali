.class public LX/2pb;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private A:Z

.field public B:Z

.field private C:Z

.field public D:LX/04G;

.field public E:LX/2q7;

.field private F:LX/2q7;

.field private G:LX/2q7;

.field public H:LX/2oj;

.field private I:LX/2p8;

.field public J:LX/7QP;

.field public K:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public M:Z

.field public N:Z

.field public O:Z

.field public final P:LX/2pq;

.field public Q:LX/04g;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:LX/0Yd;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2oa",
            "<+",
            "LX/2ol;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:LX/2ph;

.field public final d:LX/0So;

.field public final e:LX/2pg;

.field private final f:LX/19P;

.field public final g:LX/1C2;

.field public final h:LX/2pd;

.field private final i:LX/03V;

.field private final j:LX/0Uh;

.field private final k:LX/13l;

.field public final l:LX/19j;

.field public final m:LX/099;

.field public final n:Z

.field public final o:Landroid/os/Handler;

.field public final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Landroid/media/AudioManager;

.field private final r:LX/2pc;

.field public final s:Landroid/content/Context;

.field private final t:Z

.field private final u:Z

.field private final v:Z

.field private final w:Z

.field public final x:Z

.field public y:LX/2qV;

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 468538
    const-class v0, LX/2pb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/2pb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;LX/04D;Ljava/lang/Boolean;LX/0So;Landroid/content/Context;LX/19P;LX/1C2;Landroid/os/Handler;LX/03V;LX/0Uh;LX/13l;Landroid/media/AudioManager;LX/19j;LX/099;LX/0wq;)V
    .locals 12
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/04D;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 468539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 468540
    new-instance v1, LX/2pc;

    invoke-direct {v1, p0}, LX/2pc;-><init>(LX/2pb;)V

    iput-object v1, p0, LX/2pb;->r:LX/2pc;

    .line 468541
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/2pb;->B:Z

    .line 468542
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2pb;->C:Z

    .line 468543
    const/4 v1, 0x0

    iput-object v1, p0, LX/2pb;->L:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 468544
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2pb;->O:Z

    .line 468545
    const-string v1, "PlaybackController.simpleInits"

    const v2, 0x3c044171

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 468546
    :try_start_0
    move-object/from16 v0, p6

    iput-object v0, p0, LX/2pb;->s:Landroid/content/Context;

    .line 468547
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/2pb;->n:Z

    .line 468548
    move-object/from16 v0, p7

    iput-object v0, p0, LX/2pb;->f:LX/19P;

    .line 468549
    move-object/from16 v0, p8

    iput-object v0, p0, LX/2pb;->g:LX/1C2;

    .line 468550
    move-object/from16 v0, p10

    iput-object v0, p0, LX/2pb;->i:LX/03V;

    .line 468551
    move-object/from16 v0, p11

    iput-object v0, p0, LX/2pb;->j:LX/0Uh;

    .line 468552
    new-instance v1, LX/2pd;

    invoke-direct {v1, p0}, LX/2pd;-><init>(LX/2pb;)V

    iput-object v1, p0, LX/2pb;->h:LX/2pd;

    .line 468553
    new-instance v1, LX/2pg;

    invoke-direct {v1, p0}, LX/2pg;-><init>(LX/2pb;)V

    iput-object v1, p0, LX/2pb;->e:LX/2pg;

    .line 468554
    move-object/from16 v0, p5

    iput-object v0, p0, LX/2pb;->d:LX/0So;

    .line 468555
    move-object/from16 v0, p12

    iput-object v0, p0, LX/2pb;->k:LX/13l;

    .line 468556
    new-instance v1, LX/2ph;

    invoke-direct {v1, p0}, LX/2ph;-><init>(LX/2pb;)V

    iput-object v1, p0, LX/2pb;->c:LX/2ph;

    .line 468557
    move-object/from16 v0, p13

    iput-object v0, p0, LX/2pb;->q:Landroid/media/AudioManager;

    .line 468558
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/2pb;->b:Ljava/util/List;

    .line 468559
    iget-object v1, p0, LX/2pb;->b:Ljava/util/List;

    new-instance v2, LX/2pi;

    invoke-direct {v2, p0}, LX/2pi;-><init>(LX/2pb;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468560
    iget-object v1, p0, LX/2pb;->b:Ljava/util/List;

    new-instance v2, LX/2pj;

    invoke-direct {v2, p0}, LX/2pj;-><init>(LX/2pb;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468561
    iget-object v1, p0, LX/2pb;->b:Ljava/util/List;

    new-instance v2, LX/2pk;

    invoke-direct {v2, p0}, LX/2pk;-><init>(LX/2pb;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468562
    iget-object v1, p0, LX/2pb;->b:Ljava/util/List;

    new-instance v2, LX/2pl;

    invoke-direct {v2, p0}, LX/2pl;-><init>(LX/2pb;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468563
    iget-object v1, p0, LX/2pb;->b:Ljava/util/List;

    new-instance v2, LX/2pm;

    invoke-direct {v2, p0}, LX/2pm;-><init>(LX/2pb;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468564
    iget-object v1, p0, LX/2pb;->b:Ljava/util/List;

    new-instance v2, LX/2pn;

    invoke-direct {v2, p0}, LX/2pn;-><init>(LX/2pb;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468565
    iget-object v1, p0, LX/2pb;->b:Ljava/util/List;

    new-instance v2, LX/2po;

    invoke-direct {v2, p0}, LX/2po;-><init>(LX/2pb;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468566
    iget-object v1, p0, LX/2pb;->b:Ljava/util/List;

    new-instance v2, LX/2pp;

    invoke-direct {v2, p0}, LX/2pp;-><init>(LX/2pb;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468567
    move-object/from16 v0, p9

    iput-object v0, p0, LX/2pb;->o:Landroid/os/Handler;

    .line 468568
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/2pb;->p:Ljava/util/List;

    .line 468569
    move-object/from16 v0, p14

    iput-object v0, p0, LX/2pb;->l:LX/19j;

    .line 468570
    move-object/from16 v0, p15

    iput-object v0, p0, LX/2pb;->m:LX/099;

    .line 468571
    invoke-virtual/range {p16 .. p16}, LX/0wq;->b()Z

    move-result v1

    iput-boolean v1, p0, LX/2pb;->t:Z

    .line 468572
    iget-boolean v1, p0, LX/2pb;->t:Z

    if-eqz v1, :cond_0

    invoke-virtual/range {p16 .. p16}, LX/0wq;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, LX/2pb;->u:Z

    .line 468573
    iget-boolean v1, p0, LX/2pb;->t:Z

    if-eqz v1, :cond_1

    invoke-virtual/range {p16 .. p16}, LX/0wq;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, LX/2pb;->v:Z

    .line 468574
    move-object/from16 v0, p16

    iget-boolean v1, v0, LX/0wq;->aa:Z

    iput-boolean v1, p0, LX/2pb;->w:Z

    .line 468575
    move-object/from16 v0, p16

    iget-boolean v1, v0, LX/0wq;->ab:Z

    iput-boolean v1, p0, LX/2pb;->x:Z

    .line 468576
    iget-object v1, p0, LX/2pb;->j:LX/0Uh;

    sget v2, LX/19n;->v:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v5

    .line 468577
    new-instance v1, LX/2pq;

    iget-object v3, p0, LX/2pb;->i:LX/03V;

    move-object v2, p0

    move-object/from16 v4, p5

    invoke-direct/range {v1 .. v5}, LX/2pq;-><init>(LX/2pb;LX/03V;LX/0So;Z)V

    iput-object v1, p0, LX/2pb;->P:LX/2pq;

    .line 468578
    const/4 v1, -0x1

    invoke-static {p0, v1}, LX/2pb;->b(LX/2pb;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468579
    const v1, 0x2d81b2f1

    invoke-static {v1}, LX/02m;->a(I)V

    .line 468580
    const-string v1, "VideoPlayerManager.createVideoPlayer"

    const v2, -0x581dbe94

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 468581
    :try_start_1
    iget-boolean v1, p0, LX/2pb;->t:Z

    if-nez v1, :cond_2

    .line 468582
    iget-object v1, p0, LX/2pb;->f:LX/19P;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, LX/2pb;->h:LX/2pd;

    const/4 v6, 0x0

    iget-object v7, p0, LX/2pb;->g:LX/1C2;

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    move-object/from16 v2, p6

    move-object v11, p3

    invoke-virtual/range {v1 .. v11}, LX/19P;->a(Landroid/content/Context;Landroid/util/AttributeSet;ILX/2pf;LX/2qI;LX/1C2;ZZZLX/04D;)LX/2q7;

    move-result-object v1

    iput-object v1, p0, LX/2pb;->E:LX/2q7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 468583
    :goto_2
    const v1, -0x3ce16838

    invoke-static {v1}, LX/02m;->a(I)V

    .line 468584
    invoke-virtual {p0, p3}, LX/2pb;->a(LX/04D;)V

    .line 468585
    sget-object v1, LX/2qV;->UNPREPARED:LX/2qV;

    iput-object v1, p0, LX/2pb;->y:LX/2qV;

    .line 468586
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    :goto_3
    iput-object v1, p0, LX/2pb;->L:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 468587
    return-void

    .line 468588
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 468589
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 468590
    :catchall_0
    move-exception v1

    const v2, 0x28c42941

    invoke-static {v2}, LX/02m;->a(I)V

    throw v1

    .line 468591
    :cond_2
    :try_start_2
    iget-boolean v1, p0, LX/2pb;->v:Z

    if-nez v1, :cond_4

    .line 468592
    iget-object v1, p0, LX/2pb;->f:LX/19P;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, LX/2pb;->h:LX/2pd;

    const/4 v6, 0x0

    iget-object v7, p0, LX/2pb;->g:LX/1C2;

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v2, p6

    move-object v11, p3

    invoke-virtual/range {v1 .. v11}, LX/19P;->a(Landroid/content/Context;Landroid/util/AttributeSet;ILX/2pf;LX/2qI;LX/1C2;ZZZLX/04D;)LX/2q7;

    move-result-object v1

    iput-object v1, p0, LX/2pb;->F:LX/2q7;

    .line 468593
    iget-object v1, p0, LX/2pb;->F:LX/2q7;

    invoke-interface {v1, p3}, LX/2q7;->a(LX/04D;)V

    .line 468594
    iget-object v1, p0, LX/2pb;->f:LX/19P;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, LX/2pb;->h:LX/2pd;

    const/4 v6, 0x0

    iget-object v7, p0, LX/2pb;->g:LX/1C2;

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v2, p6

    move-object v11, p3

    invoke-virtual/range {v1 .. v11}, LX/19P;->a(Landroid/content/Context;Landroid/util/AttributeSet;ILX/2pf;LX/2qI;LX/1C2;ZZZLX/04D;)LX/2q7;

    move-result-object v1

    iput-object v1, p0, LX/2pb;->G:LX/2q7;

    .line 468595
    iget-object v1, p0, LX/2pb;->G:LX/2q7;

    invoke-interface {v1, p3}, LX/2q7;->a(LX/04D;)V

    .line 468596
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, v1, v2}, LX/2pb;->a(ZZ)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/2pb;->F:LX/2q7;

    :goto_4
    iput-object v1, p0, LX/2pb;->E:LX/2q7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    .line 468597
    :catchall_1
    move-exception v1

    const v2, -0x9d06d36

    invoke-static {v2}, LX/02m;->a(I)V

    throw v1

    .line 468598
    :cond_3
    :try_start_3
    iget-object v1, p0, LX/2pb;->G:LX/2q7;

    goto :goto_4

    .line 468599
    :cond_4
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, v1, v2}, LX/2pb;->a(ZZ)Z

    move-result v1

    invoke-direct {p0, v1}, LX/2pb;->d(Z)V

    .line 468600
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, v1, v2}, LX/2pb;->a(ZZ)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/2pb;->F:LX/2q7;

    :goto_5
    iput-object v1, p0, LX/2pb;->E:LX/2q7;

    goto/16 :goto_2

    :cond_5
    iget-object v1, p0, LX/2pb;->G:LX/2q7;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_5

    .line 468601
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_3
.end method

.method public static D(LX/2pb;)V
    .locals 8

    .prologue
    .line 468602
    invoke-direct {p0}, LX/2pb;->E()V

    .line 468603
    invoke-virtual {p0}, LX/2pb;->h()I

    move-result v1

    .line 468604
    int-to-float v0, v1

    invoke-virtual {p0}, LX/2pb;->k()I

    move-result v2

    int-to-float v2, v2

    div-float v2, v0, v2

    .line 468605
    iget-object v0, p0, LX/2pb;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

    .line 468606
    iget v4, v0, Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;->a:F

    move v4, v4

    .line 468607
    cmpg-float v4, v2, v4

    if-gez v4, :cond_1

    .line 468608
    iget-object v4, p0, LX/2pb;->o:Landroid/os/Handler;

    invoke-virtual {p0}, LX/2pb;->k()I

    move-result v5

    int-to-float v5, v5

    .line 468609
    iget v6, v0, Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;->a:F

    move v6, v6

    .line 468610
    mul-float/2addr v5, v6

    float-to-int v5, v5

    sub-int/2addr v5, v1

    int-to-long v6, v5

    const v5, 0x76c14fe

    invoke-static {v4, v0, v6, v7, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    .line 468611
    :cond_1
    iget v4, v0, Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;->b:F

    move v4, v4

    .line 468612
    cmpg-float v4, v2, v4

    if-gez v4, :cond_0

    .line 468613
    iget-object v4, p0, LX/2pb;->o:Landroid/os/Handler;

    const v5, 0x50a20aa1

    invoke-static {v4, v0, v5}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 468614
    :cond_2
    return-void
.end method

.method private E()V
    .locals 3

    .prologue
    .line 468615
    iget-object v0, p0, LX/2pb;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

    .line 468616
    iget-object v2, p0, LX/2pb;->o:Landroid/os/Handler;

    invoke-static {v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 468617
    :cond_0
    return-void
.end method

.method public static H(LX/2pb;)V
    .locals 5

    .prologue
    .line 468618
    iget-object v1, p0, LX/2pb;->q:Landroid/media/AudioManager;

    monitor-enter v1

    .line 468619
    :try_start_0
    iget-boolean v0, p0, LX/2pb;->C:Z

    if-nez v0, :cond_0

    .line 468620
    iget-object v0, p0, LX/2pb;->q:Landroid/media/AudioManager;

    iget-object v2, p0, LX/2pb;->r:LX/2pc;

    const/4 v3, 0x3

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 468621
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2pb;->C:Z

    .line 468622
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static I(LX/2pb;)V
    .locals 3

    .prologue
    .line 468623
    iget-object v1, p0, LX/2pb;->q:Landroid/media/AudioManager;

    monitor-enter v1

    .line 468624
    :try_start_0
    iget-boolean v0, p0, LX/2pb;->C:Z

    if-eqz v0, :cond_0

    .line 468625
    iget-object v0, p0, LX/2pb;->q:Landroid/media/AudioManager;

    iget-object v2, p0, LX/2pb;->r:LX/2pc;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 468626
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2pb;->C:Z

    .line 468627
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(ZZ)Z
    .locals 1

    .prologue
    .line 468628
    if-eqz p1, :cond_0

    iget-boolean v0, p0, LX/2pb;->t:Z

    if-nez v0, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    iget-boolean v0, p0, LX/2pb;->u:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/2pb;LX/2qV;)V
    .locals 1

    .prologue
    .line 468629
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;LX/04g;)V

    .line 468630
    return-void
.end method

.method public static a$redex0(LX/2pb;LX/2qV;LX/04g;)V
    .locals 4
    .param p1    # LX/2qV;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 468631
    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    .line 468632
    iget-object v1, p0, LX/2pb;->y:LX/2qV;

    if-eq v1, p1, :cond_2

    .line 468633
    iput-object p1, p0, LX/2pb;->y:LX/2qV;

    .line 468634
    iget-object v1, p0, LX/2pb;->H:LX/2oj;

    if-eqz v1, :cond_0

    .line 468635
    iget-object v1, p0, LX/2pb;->H:LX/2oj;

    new-instance v2, LX/2ou;

    iget-object v3, p0, LX/2pb;->K:Ljava/lang/String;

    invoke-direct {v2, v3, p1, p2}, LX/2ou;-><init>(Ljava/lang/String;LX/2qV;LX/04g;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    .line 468636
    :cond_0
    iget-object v1, p0, LX/2pb;->y:LX/2qV;

    invoke-virtual {v1}, LX/2qV;->isPlayingState()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, LX/2pb;->o()Z

    move-result v1

    if-nez v1, :cond_1

    .line 468637
    invoke-static {p0}, LX/2pb;->I(LX/2pb;)V

    .line 468638
    :cond_1
    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne p1, v1, :cond_3

    .line 468639
    invoke-static {p0}, LX/2pb;->D(LX/2pb;)V

    .line 468640
    iget-boolean v0, p0, LX/2pb;->O:Z

    if-eqz v0, :cond_2

    .line 468641
    iget-object v0, p0, LX/2pb;->m:LX/099;

    iget-object v1, p0, LX/2pb;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/099;->b(Ljava/lang/String;)V

    .line 468642
    :cond_2
    :goto_0
    return-void

    .line 468643
    :cond_3
    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_2

    .line 468644
    invoke-direct {p0}, LX/2pb;->E()V

    goto :goto_0
.end method

.method public static a$redex0(LX/2pb;LX/45B;)V
    .locals 9

    .prologue
    .line 468645
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->w()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v8

    .line 468646
    iget-object v0, p0, LX/2pb;->g:LX/1C2;

    sget-object v1, LX/45B;->CONNECTED:LX/45B;

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v8, :cond_1

    iget-object v2, v8, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    .line 468647
    :goto_1
    iget-object v3, p0, LX/2pb;->D:LX/04G;

    move-object v3, v3

    .line 468648
    sget-object v4, LX/04g;->BY_USER:LX/04g;

    iget-object v4, v4, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, LX/2pb;->h()I

    move-result v5

    iget-object v6, p0, LX/2pb;->K:Ljava/lang/String;

    invoke-virtual {p0}, LX/2pb;->s()LX/04D;

    move-result-object v7

    invoke-virtual/range {v0 .. v8}, LX/1C2;->a(ZLX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 468649
    return-void

    .line 468650
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static a$redex0(LX/2pb;Ljava/lang/Exception;Lcom/facebook/video/engine/VideoPlayerParams;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 468696
    sget-object v0, LX/2qV;->ERROR:LX/2qV;

    invoke-static {p0, v0}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    .line 468697
    iget-object v0, p0, LX/2pb;->g:LX/1C2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error setting video path. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    iget-object v3, p2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p2, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    iget-object v6, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v6}, LX/2q7;->g()LX/04D;

    move-result-object v6

    iget-object v7, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v7}, LX/2q7;->r()Ljava/lang/String;

    move-result-object v7

    move-object v8, p2

    move-object v9, p1

    move-object v10, v5

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 468698
    return-void
.end method

.method public static b(LX/2pb;I)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 468651
    if-gez p1, :cond_0

    if-ne p1, v4, :cond_1

    .line 468652
    :cond_0
    iput p1, p0, LX/2pb;->z:I

    .line 468653
    :goto_0
    return-void

    .line 468654
    :cond_1
    iget-object v0, p0, LX/2pb;->i:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/2pb;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".setLastPlayPositionSafely"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid lastPlayPosition: %d"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 468655
    iput v4, p0, LX/2pb;->z:I

    goto :goto_0
.end method

.method private d(Z)V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 468656
    iget-object v0, p0, LX/2pb;->F:LX/2q7;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    iget-object v0, p0, LX/2pb;->G:LX/2q7;

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    .line 468657
    :cond_1
    :goto_0
    return-void

    .line 468658
    :cond_2
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    if-nez v0, :cond_3

    sget-object v10, LX/04D;->UNKNOWN:LX/04D;

    .line 468659
    :goto_1
    if-eqz p1, :cond_4

    .line 468660
    iget-object v0, p0, LX/2pb;->f:LX/19P;

    iget-object v1, p0, LX/2pb;->s:Landroid/content/Context;

    iget-object v4, p0, LX/2pb;->h:LX/2pd;

    iget-object v6, p0, LX/2pb;->g:LX/1C2;

    move-object v5, v2

    move v8, v3

    move v9, p1

    invoke-virtual/range {v0 .. v10}, LX/19P;->a(Landroid/content/Context;Landroid/util/AttributeSet;ILX/2pf;LX/2qI;LX/1C2;ZZZLX/04D;)LX/2q7;

    move-result-object v0

    iput-object v0, p0, LX/2pb;->F:LX/2q7;

    goto :goto_0

    .line 468661
    :cond_3
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->g()LX/04D;

    move-result-object v10

    goto :goto_1

    .line 468662
    :cond_4
    iget-object v0, p0, LX/2pb;->f:LX/19P;

    iget-object v1, p0, LX/2pb;->s:Landroid/content/Context;

    iget-object v4, p0, LX/2pb;->h:LX/2pd;

    iget-object v6, p0, LX/2pb;->g:LX/1C2;

    move-object v5, v2

    move v8, v3

    move v9, p1

    invoke-virtual/range {v0 .. v10}, LX/19P;->a(Landroid/content/Context;Landroid/util/AttributeSet;ILX/2pf;LX/2qI;LX/1C2;ZZZLX/04D;)LX/2q7;

    move-result-object v0

    iput-object v0, p0, LX/2pb;->G:LX/2q7;

    goto :goto_0
.end method


# virtual methods
.method public final A()J
    .locals 2

    .prologue
    .line 468663
    iget-object v0, p0, LX/2pb;->m:LX/099;

    iget-object v1, p0, LX/2pb;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/099;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final B()V
    .locals 2

    .prologue
    .line 468664
    iget-object v0, p0, LX/2pb;->m:LX/099;

    iget-object v1, p0, LX/2pb;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/099;->d(Ljava/lang/String;)V

    .line 468665
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 468666
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(F)V

    .line 468667
    return-void
.end method

.method public final a(ILX/04g;)V
    .locals 2

    .prologue
    .line 468668
    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    if-ne v0, v1, :cond_1

    if-lez p1, :cond_1

    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q8;->b()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q8;->b()I

    move-result v0

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0xc8

    if-ge v0, v1, :cond_1

    .line 468669
    :cond_0
    :goto_0
    return-void

    .line 468670
    :cond_1
    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->SEEKING:LX/2qV;

    if-eq v0, v1, :cond_3

    .line 468671
    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_6

    :cond_2
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/2pb;->A:Z

    .line 468672
    :cond_3
    sget-object v0, LX/2qV;->SEEKING:LX/2qV;

    invoke-static {p0, v0}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    .line 468673
    iget-boolean v0, p0, LX/2pb;->A:Z

    if-eqz v0, :cond_4

    .line 468674
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    sget-object v1, LX/04g;->BY_SEEKBAR_CONTROLLER:LX/04g;

    invoke-interface {v0, v1}, LX/2q7;->c(LX/04g;)V

    .line 468675
    iget-boolean v0, p0, LX/2pb;->O:Z

    if-eqz v0, :cond_4

    .line 468676
    iget-object v0, p0, LX/2pb;->m:LX/099;

    iget-object v1, p0, LX/2pb;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/099;->c(Ljava/lang/String;)V

    .line 468677
    :cond_4
    invoke-virtual {p0}, LX/2pb;->k()I

    move-result v0

    sub-int/2addr v0, p1

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_5

    .line 468678
    const/4 v0, 0x0

    add-int/lit16 v1, p1, -0x3e8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 468679
    :cond_5
    move v0, p1

    .line 468680
    iget-object v1, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v1, v0, p2}, LX/2q7;->a(ILX/04g;)V

    .line 468681
    invoke-static {p0, v0}, LX/2pb;->b(LX/2pb;I)V

    .line 468682
    iget-boolean v0, p0, LX/2pb;->A:Z

    if-eqz v0, :cond_7

    .line 468683
    sget-object v0, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    invoke-static {p0, v0}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    .line 468684
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    sget-object v1, LX/04g;->BY_SEEKBAR_CONTROLLER:LX/04g;

    invoke-interface {v0, v1}, LX/2q7;->a(LX/04g;)V

    .line 468685
    iget-boolean v0, p0, LX/2pb;->O:Z

    if-eqz v0, :cond_0

    .line 468686
    iget-object v0, p0, LX/2pb;->m:LX/099;

    iget-object v1, p0, LX/2pb;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/099;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 468687
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    .line 468688
    :cond_7
    sget-object v0, LX/2qV;->PAUSED:LX/2qV;

    invoke-static {p0, v0}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    goto :goto_0
.end method

.method public final a(LX/04D;)V
    .locals 1

    .prologue
    .line 468476
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/04D;)V

    .line 468477
    return-void
.end method

.method public final a(LX/04G;)V
    .locals 1

    .prologue
    .line 468689
    iput-object p1, p0, LX/2pb;->D:LX/04G;

    .line 468690
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/04G;)V

    .line 468691
    return-void
.end method

.method public final a(LX/04H;)V
    .locals 1

    .prologue
    .line 468692
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/04H;)V

    .line 468693
    return-void
.end method

.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 468694
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, LX/2pb;->a(LX/04g;I)V

    .line 468695
    return-void
.end method

.method public final a(LX/04g;I)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 468508
    iget-object v1, p0, LX/2pb;->y:LX/2qV;

    sget-object v2, LX/2qV;->PLAYING:LX/2qV;

    if-eq v1, v2, :cond_5

    iget-object v1, p0, LX/2pb;->y:LX/2qV;

    sget-object v2, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-eq v1, v2, :cond_5

    iget-object v1, p0, LX/2pb;->y:LX/2qV;

    sget-object v2, LX/2qV;->SEEKING:LX/2qV;

    if-eq v1, v2, :cond_5

    .line 468509
    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    invoke-static {p0, v1, p1}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;LX/04g;)V

    .line 468510
    iget v1, p0, LX/2pb;->z:I

    if-eq v1, v0, :cond_0

    iget v0, p0, LX/2pb;->z:I

    .line 468511
    :cond_0
    new-instance v1, LX/7K4;

    invoke-direct {v1, v0, p2}, LX/7K4;-><init>(II)V

    .line 468512
    iget-object v0, p0, LX/2pb;->k:LX/13l;

    const/4 v2, 0x1

    .line 468513
    iget-boolean p2, p0, LX/2pb;->N:Z

    if-eqz p2, :cond_7

    .line 468514
    :cond_1
    :goto_0
    move v2, v2

    .line 468515
    invoke-virtual {v0, p0, v2}, LX/13l;->a(LX/2pb;Z)Z

    move-result v0

    .line 468516
    if-eqz v0, :cond_6

    .line 468517
    iget-boolean v0, p0, LX/2pb;->B:Z

    if-nez v0, :cond_2

    .line 468518
    invoke-static {p0}, LX/2pb;->H(LX/2pb;)V

    .line 468519
    :cond_2
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0, p1, v1}, LX/2q7;->a(LX/04g;LX/7K4;)V

    .line 468520
    iget-object v0, p0, LX/2pb;->R:LX/0Yd;

    if-nez v0, :cond_3

    .line 468521
    const/4 v0, 0x1

    invoke-static {v0}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v0

    .line 468522
    const-string v1, "android.intent.action.HEADSET_PLUG"

    new-instance v2, LX/7NG;

    invoke-direct {v2, p0}, LX/7NG;-><init>(LX/2pb;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468523
    new-instance v1, LX/0Yd;

    invoke-direct {v1, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, LX/2pb;->R:LX/0Yd;

    .line 468524
    :cond_3
    iget-object v0, p0, LX/2pb;->s:Landroid/content/Context;

    if-eqz v0, :cond_4

    .line 468525
    iget-object v0, p0, LX/2pb;->s:Landroid/content/Context;

    iget-object v1, p0, LX/2pb;->R:LX/0Yd;

    new-instance v2, Landroid/content/IntentFilter;

    const-string p1, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v2, p1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 468526
    :cond_4
    iget-boolean v0, p0, LX/2pb;->O:Z

    if-eqz v0, :cond_5

    .line 468527
    iget-object v0, p0, LX/2pb;->m:LX/099;

    iget-object v1, p0, LX/2pb;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/099;->b(Ljava/lang/String;)V

    .line 468528
    :cond_5
    :goto_1
    return-void

    .line 468529
    :cond_6
    sget-object v0, LX/2qV;->ERROR:LX/2qV;

    invoke-static {p0, v0}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    goto :goto_1

    .line 468530
    :cond_7
    iget-boolean p2, p0, LX/2pb;->x:Z

    if-nez p2, :cond_1

    .line 468531
    sget-object p2, LX/04g;->BY_AUTOPLAY:LX/04g;

    if-ne p1, p2, :cond_1

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(LX/2oi;LX/04g;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 468532
    sget-object v0, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    if-eq p1, v0, :cond_1

    invoke-virtual {p0}, LX/2pb;->f()LX/2oi;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 468533
    :cond_0
    :goto_0
    return-void

    .line 468534
    :cond_1
    sget-object v0, LX/2oi;->CUSTOM_DEFINITION:LX/2oi;

    if-ne p1, v0, :cond_2

    if-eqz p3, :cond_2

    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 468535
    :cond_2
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q8;->b()I

    move-result v0

    .line 468536
    iget-object v1, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v1, p1, p2, p3}, LX/2q7;->a(LX/2oi;LX/04g;Ljava/lang/String;)V

    .line 468537
    iget-object v1, p0, LX/2pb;->E:LX/2q7;

    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-interface {v1, v0, v2}, LX/2q7;->a(ILX/04g;)V

    goto :goto_0
.end method

.method public final a(LX/2p8;)V
    .locals 2

    .prologue
    .line 468441
    iput-object p1, p0, LX/2pb;->I:LX/2p8;

    .line 468442
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    iget-object v1, p0, LX/2pb;->I:LX/2p8;

    invoke-interface {v0, v1}, LX/2q7;->a(LX/2p8;)V

    .line 468443
    return-void
.end method

.method public final a(LX/2qG;)V
    .locals 1

    .prologue
    .line 468444
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/2qG;)V

    .line 468445
    return-void
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;)V
    .locals 1

    .prologue
    .line 468446
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/2pb;->a(Lcom/facebook/video/engine/VideoPlayerParams;Z)V

    .line 468447
    return-void
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;Z)V
    .locals 3

    .prologue
    .line 468448
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/video/engine/VideoPlayerParams;->a()Z

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/video/engine/VideoPlayerParams;->b()Z

    move-result v1

    invoke-direct {p0, v0, v1}, LX/2pb;->a(ZZ)Z

    move-result v1

    .line 468449
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    iget-object v2, p0, LX/2pb;->F:LX/2q7;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 468450
    iget-boolean v0, p0, LX/2pb;->v:Z

    if-eqz v0, :cond_0

    .line 468451
    invoke-direct {p0, v1}, LX/2pb;->d(Z)V

    .line 468452
    :cond_0
    if-eqz v1, :cond_3

    iget-object v0, p0, LX/2pb;->F:LX/2q7;

    :goto_1
    iput-object v0, p0, LX/2pb;->E:LX/2q7;

    .line 468453
    invoke-virtual {p1}, Lcom/facebook/video/engine/VideoPlayerParams;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    :goto_2
    iput-object v0, p0, LX/2pb;->L:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 468454
    iget-object v0, p0, LX/2pb;->I:LX/2p8;

    invoke-virtual {p0, v0}, LX/2pb;->a(LX/2p8;)V

    .line 468455
    :cond_1
    new-instance v0, LX/2qi;

    invoke-direct {v0, p0, p1}, LX/2qi;-><init>(LX/2pb;Lcom/facebook/video/engine/VideoPlayerParams;)V

    .line 468456
    iget-object v1, p0, LX/2pb;->e:LX/2pg;

    invoke-virtual {v1}, LX/2pg;->a()V

    .line 468457
    iget-object v1, p0, LX/2pb;->P:LX/2pq;

    invoke-virtual {v1}, LX/2pq;->a()V

    .line 468458
    iget-object v1, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v1, p1, v0, p2}, LX/2q7;->a(Lcom/facebook/video/engine/VideoPlayerParams;LX/2qi;Z)V

    .line 468459
    iget-object v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, LX/2pb;->K:Ljava/lang/String;

    .line 468460
    const/4 v0, -0x1

    invoke-static {p0, v0}, LX/2pb;->b(LX/2pb;I)V

    .line 468461
    sget-object v0, LX/2qV;->PREPARED:LX/2qV;

    invoke-static {p0, v0}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;)V

    .line 468462
    :goto_3
    return-void

    .line 468463
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 468464
    :cond_3
    iget-object v0, p0, LX/2pb;->G:LX/2q7;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 468465
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 468466
    :catch_0
    move-exception v0

    .line 468467
    invoke-static {p0, v0, p1}, LX/2pb;->a$redex0(LX/2pb;Ljava/lang/Exception;Lcom/facebook/video/engine/VideoPlayerParams;)V

    goto :goto_3
.end method

.method public final b(LX/04g;)V
    .locals 2

    .prologue
    .line 468468
    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PAUSE:LX/2qV;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-eq v0, v1, :cond_1

    .line 468469
    sget-object v0, LX/2qV;->ATTEMPT_TO_PAUSE:LX/2qV;

    invoke-static {p0, v0, p1}, LX/2pb;->a$redex0(LX/2pb;LX/2qV;LX/04g;)V

    .line 468470
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->c(LX/04g;)V

    .line 468471
    iget-object v0, p0, LX/2pb;->s:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pb;->R:LX/0Yd;

    if-eqz v0, :cond_0

    .line 468472
    :try_start_0
    iget-object v0, p0, LX/2pb;->s:Landroid/content/Context;

    iget-object v1, p0, LX/2pb;->R:LX/0Yd;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 468473
    :cond_0
    :goto_0
    iget-boolean v0, p0, LX/2pb;->O:Z

    if-eqz v0, :cond_1

    .line 468474
    iget-object v0, p0, LX/2pb;->m:LX/099;

    iget-object v1, p0, LX/2pb;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/099;->c(Ljava/lang/String;)V

    .line 468475
    :cond_1
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 468478
    iget-object v0, p0, LX/2pb;->H:LX/2oj;

    if-eqz v0, :cond_0

    .line 468479
    iget-object v0, p0, LX/2pb;->H:LX/2oj;

    new-instance v1, LX/2ou;

    iget-object v2, p0, LX/2pb;->K:Ljava/lang/String;

    sget-object v3, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    invoke-direct {v1, v2, v3}, LX/2ou;-><init>(Ljava/lang/String;LX/2qV;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 468480
    :cond_0
    return-void
.end method

.method public final c(LX/04g;)V
    .locals 1

    .prologue
    .line 468481
    iput-object p1, p0, LX/2pb;->Q:LX/04g;

    .line 468482
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->d(LX/04g;)V

    .line 468483
    return-void
.end method

.method public final f()LX/2oi;
    .locals 1

    .prologue
    .line 468484
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->e()LX/2oi;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 468485
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 468486
    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PAUSE:LX/2qV;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_1

    .line 468487
    :cond_0
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q8;->b()I

    move-result v0

    .line 468488
    :goto_0
    return v0

    .line 468489
    :cond_1
    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, LX/2pb;->w:Z

    if-eqz v0, :cond_2

    .line 468490
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q8;->b()I

    move-result v0

    goto :goto_0

    .line 468491
    :cond_2
    iget v0, p0, LX/2pb;->z:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget v0, p0, LX/2pb;->z:I

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 468492
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->s()I

    move-result v0

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 468493
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->t()I

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 468494
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->l()I

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 468495
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q8;->a()Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 468496
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->h()Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 468497
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->i()Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 468498
    iget-object v0, p0, LX/2pb;->y:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()LX/04D;
    .locals 1

    .prologue
    .line 468499
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->g()LX/04D;

    move-result-object v0

    return-object v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 468500
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->m()I

    move-result v0

    return v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 468501
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->p()LX/7IE;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->p()LX/7IE;

    move-result-object v0

    iget-object v0, v0, LX/7IE;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()I
    .locals 1

    .prologue
    .line 468502
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->p()LX/7IE;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->p()LX/7IE;

    move-result-object v0

    .line 468503
    iget p0, v0, LX/7IE;->f:I

    move v0, p0

    .line 468504
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()I
    .locals 1

    .prologue
    .line 468505
    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->p()LX/7IE;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pb;->E:LX/2q7;

    invoke-interface {v0}, LX/2q7;->p()LX/7IE;

    move-result-object v0

    .line 468506
    iget p0, v0, LX/7IE;->g:I

    move v0, p0

    .line 468507
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
