.class public final LX/2ek;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:LX/2dx;

.field public final synthetic d:LX/1Ps;

.field public final synthetic e:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2dx;LX/1Ps;)V
    .locals 0

    .prologue
    .line 445466
    iput-object p1, p0, LX/2ek;->e:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iput-object p2, p0, LX/2ek;->a:LX/0Px;

    iput-object p3, p0, LX/2ek;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/2ek;->c:LX/2dx;

    iput-object p5, p0, LX/2ek;->d:LX/1Ps;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 445467
    const/4 v0, 0x0

    iget-object v1, p0, LX/2ek;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 445468
    iget-object v0, p0, LX/2ek;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 445469
    iget-object v4, p0, LX/2ek;->e:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v1, p0, LX/2ek;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 445470
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v5

    .line 445471
    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    add-int/lit8 v5, v2, 0x1

    .line 445472
    iget-object v6, v4, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->m:LX/0ad;

    invoke-static {v1, v5, v6}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;ILX/0ad;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 445473
    :goto_1
    iget-object v1, p0, LX/2ek;->e:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->f:Lcom/facebook/feedplugins/pymk/rows/PersonYouMayKnowPagePartDefinition;

    new-instance v4, LX/2el;

    iget-object v5, p0, LX/2ek;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v6, p0, LX/2ek;->c:LX/2dx;

    invoke-direct {v4, v5, v0, v6}, LX/2el;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;LX/2dx;)V

    invoke-virtual {p1, v1, v4}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 445474
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 445475
    :cond_0
    iget-object v0, p0, LX/2ek;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 445476
    iget-object v0, p0, LX/2ek;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 445477
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 445478
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-static {v0}, LX/2e2;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 445479
    :cond_1
    iget-object v0, p0, LX/2ek;->e:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->j:Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;

    invoke-virtual {p1, v0, v7}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 445480
    :goto_2
    return-void

    .line 445481
    :cond_2
    iget-object v0, p0, LX/2ek;->e:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->k:Lcom/facebook/feedplugins/pymk/rows/PymkLoadingPartDefinition;

    invoke-virtual {p1, v0, v7}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_2

    .line 445482
    :cond_3
    iget-object v6, v4, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->b:LX/2e1;

    .line 445483
    iget-object v8, v6, LX/2e1;->a:LX/0Zb;

    const-string v9, "pymk_ccu_promo_card_shown"

    invoke-static {v6, v9}, LX/2e1;->a(LX/2e1;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "ccu_promo_card_position"

    invoke-virtual {v9, v10, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    invoke-interface {v8, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 445484
    iget-object v6, v4, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->g:Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;

    invoke-virtual {p1, v6, v1}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final c(I)V
    .locals 7

    .prologue
    .line 445485
    iget-object v0, p0, LX/2ek;->e:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->e:LX/1LV;

    iget-object v0, p0, LX/2ek;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 445486
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 445487
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v1, v0, p1}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 445488
    iget-object v0, p0, LX/2ek;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 445489
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 445490
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    iget-object v1, p0, LX/2ek;->a:LX/0Px;

    invoke-static {v0, v1, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 445491
    iget-object v0, p0, LX/2ek;->e:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->h:LX/2e2;

    iget-object v0, p0, LX/2ek;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 445492
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 445493
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 445494
    invoke-virtual {v1, v0, p1}, LX/2e2;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 445495
    :goto_0
    iget-object v1, p0, LX/2ek;->e:Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v0, p0, LX/2ek;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 445496
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 445497
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    iget-object v2, p0, LX/2ek;->d:LX/1Ps;

    const/4 p0, 0x1

    .line 445498
    iget-object v3, v1, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->m:LX/0ad;

    sget v4, LX/2dw;->b:I

    const/4 v5, -0x1

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    if-ne p1, v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->o()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    if-le p1, v3, :cond_3

    .line 445499
    :cond_0
    :goto_1
    return-void

    .line 445500
    :cond_1
    invoke-static {v0}, LX/2e2;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 445501
    invoke-virtual {v1, v0}, LX/2e2;->b(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V

    goto :goto_0

    .line 445502
    :cond_2
    invoke-virtual {v1, v0}, LX/2e2;->c(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V

    goto :goto_0

    .line 445503
    :cond_3
    iget-object v3, v1, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->b:LX/2e1;

    .line 445504
    iget-object v4, v3, LX/2e1;->a:LX/0Zb;

    const-string v5, "pymk_ccu_footer_scrolling_promo_shown"

    invoke-static {v3, v5}, LX/2e1;->a(LX/2e1;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "ccu_promo_scrolling_position"

    invoke-virtual {v5, v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 445505
    new-instance v4, LX/2e7;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    new-instance v5, LX/2e8;

    sget-object v6, LX/3ka;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {v5, p0, v6}, LX/2e8;-><init>(ZLcom/facebook/interstitial/manager/InterstitialTrigger;)V

    invoke-direct {v4, v3, v5}, LX/2e7;-><init>(Ljava/lang/String;LX/2e8;)V

    move-object v3, v2

    .line 445506
    check-cast v3, LX/1Pr;

    invoke-interface {v3, v4, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-object v3, v2

    .line 445507
    check-cast v3, LX/1Pr;

    new-instance v5, LX/2e8;

    sget-object v6, LX/3ka;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {v5, p0, v6}, LX/2e8;-><init>(ZLcom/facebook/interstitial/manager/InterstitialTrigger;)V

    invoke-interface {v3, v4, v5}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 445508
    check-cast v2, LX/1Pq;

    new-array v3, p0, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-interface {v2, v3}, LX/1Pq;->a([Ljava/lang/Object;)V

    goto :goto_1
.end method
