.class public LX/23q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/3FQ;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/23r;

.field private final b:LX/1VK;

.field private final c:LX/23s;

.field private final d:LX/14w;

.field private final e:LX/1VF;


# direct methods
.method public constructor <init>(LX/23r;LX/1VK;LX/23s;LX/14w;LX/1VF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365346
    iput-object p1, p0, LX/23q;->a:LX/23r;

    .line 365347
    iput-object p2, p0, LX/23q;->b:LX/1VK;

    .line 365348
    iput-object p3, p0, LX/23q;->c:LX/23s;

    .line 365349
    iput-object p4, p0, LX/23q;->d:LX/14w;

    .line 365350
    iput-object p5, p0, LX/23q;->e:LX/1VF;

    .line 365351
    return-void
.end method

.method public static a(LX/0QB;)LX/23q;
    .locals 9

    .prologue
    .line 365352
    const-class v1, LX/23q;

    monitor-enter v1

    .line 365353
    :try_start_0
    sget-object v0, LX/23q;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 365354
    sput-object v2, LX/23q;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 365355
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365356
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 365357
    new-instance v3, LX/23q;

    const-class v4, LX/23r;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/23r;

    const-class v5, LX/1VK;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1VK;

    invoke-static {v0}, LX/23s;->a(LX/0QB;)LX/23s;

    move-result-object v6

    check-cast v6, LX/23s;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v7

    check-cast v7, LX/14w;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v8

    check-cast v8, LX/1VF;

    invoke-direct/range {v3 .. v8}, LX/23q;-><init>(LX/23r;LX/1VK;LX/23s;LX/14w;LX/1VF;)V

    .line 365358
    move-object v0, v3

    .line 365359
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 365360
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/23q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365361
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 365362
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/2oL;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/2oL;"
        }
    .end annotation

    .prologue
    .line 365363
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 365364
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 365365
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 365366
    new-instance v2, LX/2oK;

    iget-object v3, p0, LX/23q;->b:LX/1VK;

    invoke-direct {v2, v1, v0, v3}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    .line 365367
    check-cast p2, LX/1Pr;

    .line 365368
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 365369
    check-cast v0, LX/0jW;

    invoke-interface {p2, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oL;

    return-object v0
.end method


# virtual methods
.method public final a(LX/3FV;LX/1Po;)LX/3Qx;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3FV;",
            "TE;)",
            "LX/3Qx;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 365370
    iget-object v0, p1, LX/3FV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 365371
    if-nez v1, :cond_0

    move-object v0, v2

    .line 365372
    :goto_0
    return-object v0

    .line 365373
    :cond_0
    invoke-static {v1}, LX/1VF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v2

    .line 365374
    goto :goto_0

    .line 365375
    :cond_1
    iget-object v0, p1, LX/3FV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 365376
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 365377
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 365378
    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v4

    .line 365379
    iget-object v3, p0, LX/23q;->c:LX/23s;

    iget-object v5, p1, LX/3FV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v3, v5, v4}, LX/23s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;)LX/04H;

    move-result-object v5

    .line 365380
    new-instance v3, LX/3J7;

    invoke-direct {v3, p0}, LX/3J7;-><init>(LX/23q;)V

    .line 365381
    iget-object v6, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 365382
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 365383
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 365384
    iget-object v6, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 365385
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 365386
    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v10

    .line 365387
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v7, 0x0

    move v8, v7

    move-object v9, v6

    :goto_1
    if-ge v8, v11, :cond_2

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/flatbuffers/Flattenable;

    .line 365388
    instance-of v7, v6, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v7, :cond_2

    .line 365389
    sget-object v12, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    move-object v7, v6

    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v3, v7}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v12, v7}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 365390
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 365391
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move-object v9, v6

    goto :goto_1

    .line 365392
    :cond_2
    iget-object v6, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 365393
    if-ne v9, v6, :cond_e

    .line 365394
    :goto_2
    move-object v6, v1

    .line 365395
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 365396
    if-eqz v0, :cond_d

    .line 365397
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    .line 365398
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bw()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v0

    .line 365399
    if-eqz v0, :cond_c

    .line 365400
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->k()Ljava/lang/String;

    move-result-object v0

    move-object v3, v1

    .line 365401
    :goto_3
    if-nez v0, :cond_b

    iget-object v1, p1, LX/3FV;->d:LX/D4s;

    if-eqz v1, :cond_b

    .line 365402
    iget-object v0, p1, LX/3FV;->d:LX/D4s;

    .line 365403
    iget-object v1, v0, LX/D4s;->a:Ljava/lang/String;

    move-object v0, v1

    .line 365404
    move-object v1, v0

    .line 365405
    :goto_4
    iget-object v0, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 365406
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 365407
    sget-object v7, LX/04H;->ELIGIBLE:LX/04H;

    if-ne v5, v7, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    if-nez v3, :cond_4

    :cond_3
    move-object v0, v2

    .line 365408
    goto/16 :goto_0

    .line 365409
    :cond_4
    iget-object v0, p1, LX/3FV;->c:LX/3FQ;

    if-eqz v0, :cond_5

    iget-object v0, p1, LX/3FV;->c:LX/3FQ;

    invoke-interface {v0}, LX/3FQ;->getVideoStoryPersistentState()LX/2oM;

    move-result-object v0

    if-nez v0, :cond_9

    .line 365410
    :cond_5
    iget-object v0, p1, LX/3FV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, v0, p2}, LX/23q;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/2oL;

    move-result-object v2

    .line 365411
    :goto_5
    iget-object v0, p1, LX/3FV;->h:LX/04D;

    if-eqz v0, :cond_a

    .line 365412
    iget-object v0, p1, LX/3FV;->h:LX/04D;

    .line 365413
    :goto_6
    new-instance v3, LX/3Qv;

    invoke-direct {v3}, LX/3Qv;-><init>()V

    .line 365414
    iput-object v6, v3, LX/3Qv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 365415
    move-object v3, v3

    .line 365416
    invoke-static {v4}, LX/23s;->a(LX/1Qt;)Ljava/lang/String;

    move-result-object v4

    .line 365417
    iput-object v4, v3, LX/3Qv;->d:Ljava/lang/String;

    .line 365418
    move-object v3, v3

    .line 365419
    invoke-interface {v2}, LX/2oM;->c()LX/04g;

    move-result-object v4

    .line 365420
    iput-object v4, v3, LX/3Qv;->h:LX/04g;

    .line 365421
    move-object v3, v3

    .line 365422
    iput-object v0, v3, LX/3Qv;->g:LX/04D;

    .line 365423
    move-object v0, v3

    .line 365424
    iget-object v3, p1, LX/3FV;->f:LX/0JG;

    .line 365425
    iput-object v3, v0, LX/3Qv;->l:LX/0JG;

    .line 365426
    move-object v0, v0

    .line 365427
    iget-object v3, p1, LX/3FV;->g:Ljava/lang/String;

    .line 365428
    iput-object v3, v0, LX/3Qv;->m:Ljava/lang/String;

    .line 365429
    move-object v0, v0

    .line 365430
    iget-object v3, p1, LX/3FV;->d:LX/D4s;

    .line 365431
    iput-object v3, v0, LX/3Qv;->k:LX/D4s;

    .line 365432
    move-object v3, v0

    .line 365433
    if-eqz v1, :cond_6

    .line 365434
    invoke-virtual {v3, v1}, LX/3Qv;->a(Ljava/lang/String;)LX/3Qv;

    .line 365435
    :cond_6
    iget-object v0, p1, LX/3FV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 365436
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 365437
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_8

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    const/4 v5, 0x0

    .line 365438
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->y()LX/0Px;

    move-result-object v7

    .line 365439
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    :goto_7
    if-ge v6, v8, :cond_f

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 365440
    sget-object v9, LX/4Zn;->a:[I

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->ordinal()I

    move-result v4

    aget v4, v9, v4

    packed-switch v4, :pswitch_data_0

    .line 365441
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_7

    .line 365442
    :pswitch_0
    const/4 v4, 0x1

    .line 365443
    :goto_8
    move v0, v4

    .line 365444
    if-eqz v0, :cond_8

    .line 365445
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 365446
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 365447
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v1}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v5

    .line 365448
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 365449
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    const/4 v1, 0x0

    move v4, v1

    :goto_9
    if-ge v4, v7, :cond_7

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 365450
    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365451
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_9

    .line 365452
    :cond_7
    move-object v0, v6

    .line 365453
    iput-object v0, v3, LX/3Qv;->b:Ljava/util/List;

    .line 365454
    :cond_8
    iget-object v0, p0, LX/23q;->a:LX/23r;

    invoke-virtual {v3}, LX/3Qv;->a()LX/3Qw;

    move-result-object v1

    iget-object v3, p1, LX/3FV;->b:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v4, p1, LX/3FV;->c:LX/3FQ;

    iget-object v5, p1, LX/3FV;->e:LX/D6L;

    invoke-virtual/range {v0 .. v5}, LX/23r;->a(LX/3Qw;LX/2oM;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D6L;)LX/3Qx;

    move-result-object v0

    goto/16 :goto_0

    .line 365455
    :cond_9
    iget-object v0, p1, LX/3FV;->c:LX/3FQ;

    invoke-interface {v0}, LX/3FQ;->getVideoStoryPersistentState()LX/2oM;

    move-result-object v2

    goto/16 :goto_5

    .line 365456
    :cond_a
    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v0

    goto/16 :goto_6

    :cond_b
    move-object v1, v0

    goto/16 :goto_4

    :cond_c
    move-object v0, v2

    move-object v3, v1

    goto/16 :goto_3

    :cond_d
    move-object v0, v2

    move-object v3, v2

    goto/16 :goto_3

    :cond_e
    invoke-virtual {v10, v9}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v7

    invoke-virtual {v10, v6, v7}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v6

    invoke-static {v9, v6}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    goto/16 :goto_2

    :cond_f
    move v4, v5

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
