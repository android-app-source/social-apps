.class public LX/2bF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 428703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 428704
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_8

    .line 428705
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 428706
    :goto_0
    return v1

    .line 428707
    :cond_0
    const-string v9, "base_state"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 428708
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v3

    move-object v6, v3

    move v3, v2

    .line 428709
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 428710
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 428711
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 428712
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 428713
    const-string v9, "allow"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 428714
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 428715
    :cond_2
    const-string v9, "deny"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 428716
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 428717
    :cond_3
    const-string v9, "tag_expansion_state"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 428718
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    goto :goto_1

    .line 428719
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 428720
    :cond_5
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 428721
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 428722
    if-eqz v3, :cond_6

    .line 428723
    invoke-virtual {p1, v2, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 428724
    :cond_6
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 428725
    if-eqz v0, :cond_7

    .line 428726
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 428727
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v3, v1

    move-object v4, v0

    move v5, v1

    move-object v6, v0

    move v7, v1

    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 428728
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 428729
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 428730
    if-eqz v0, :cond_0

    .line 428731
    const-string v0, "allow"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428732
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 428733
    :cond_0
    invoke-virtual {p0, p1, v2, v1}, LX/15i;->a(IIS)S

    move-result v0

    .line 428734
    if-eqz v0, :cond_1

    .line 428735
    const-string v0, "base_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428736
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 428737
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 428738
    if-eqz v0, :cond_2

    .line 428739
    const-string v0, "deny"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428740
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 428741
    :cond_2
    invoke-virtual {p0, p1, v4, v1}, LX/15i;->a(IIS)S

    move-result v0

    .line 428742
    if-eqz v0, :cond_3

    .line 428743
    const-string v0, "tag_expansion_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 428744
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 428745
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 428746
    return-void
.end method
