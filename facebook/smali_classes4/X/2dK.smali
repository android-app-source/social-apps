.class public LX/2dK;
.super LX/1SG;
.source ""

# interfaces
.implements LX/2dL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1SG",
        "<",
        "LX/2cu;",
        ">;",
        "LX/2dL;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/2dK;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2cu;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2dM;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 443012
    invoke-direct {p0}, LX/1SG;-><init>()V

    .line 443013
    new-instance v0, LX/2dO;

    invoke-direct {v0, p0}, LX/2dO;-><init>(LX/2dK;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    .line 443014
    invoke-static {p1}, LX/1SK;->a(Lcom/google/common/util/concurrent/ListenableFuture;)LX/0Ot;

    move-result-object v2

    invoke-static {v2, v0, v1}, LX/1SK;->a(LX/0Ot;LX/0QK;Ljava/util/concurrent/Executor;)LX/0Ot;

    move-result-object v2

    move-object v0, v2

    .line 443015
    iput-object v0, p0, LX/2dK;->a:LX/0Ot;

    .line 443016
    return-void
.end method

.method public static a(LX/0QB;)LX/2dK;
    .locals 4

    .prologue
    .line 443017
    sget-object v0, LX/2dK;->b:LX/2dK;

    if-nez v0, :cond_1

    .line 443018
    const-class v1, LX/2dK;

    monitor-enter v1

    .line 443019
    :try_start_0
    sget-object v0, LX/2dK;->b:LX/2dK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 443020
    if-eqz v2, :cond_0

    .line 443021
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 443022
    new-instance p0, LX/2dK;

    invoke-static {v0}, LX/2dM;->b(LX/0QB;)LX/2dM;

    move-result-object v3

    check-cast v3, LX/2dM;

    invoke-direct {p0, v3}, LX/2dK;-><init>(LX/2dM;)V

    .line 443023
    move-object v0, p0

    .line 443024
    sput-object v0, LX/2dK;->b:LX/2dK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 443025
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 443026
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 443027
    :cond_1
    sget-object v0, LX/2dK;->b:LX/2dK;

    return-object v0

    .line 443028
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 443029
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 443030
    invoke-virtual {p0}, LX/2dK;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2cu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443031
    iget-object v0, p0, LX/2dK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 443032
    invoke-virtual {p0}, LX/2dK;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
