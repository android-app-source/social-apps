.class public LX/257;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/257;


# instance fields
.field public a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 368240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 368241
    iput-object p1, p0, LX/257;->a:LX/0Zb;

    .line 368242
    return-void
.end method

.method public static a(LX/0QB;)LX/257;
    .locals 4

    .prologue
    .line 368243
    sget-object v0, LX/257;->b:LX/257;

    if-nez v0, :cond_1

    .line 368244
    const-class v1, LX/257;

    monitor-enter v1

    .line 368245
    :try_start_0
    sget-object v0, LX/257;->b:LX/257;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 368246
    if-eqz v2, :cond_0

    .line 368247
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 368248
    new-instance p0, LX/257;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/257;-><init>(LX/0Zb;)V

    .line 368249
    move-object v0, p0

    .line 368250
    sput-object v0, LX/257;->b:LX/257;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 368251
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 368252
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 368253
    :cond_1
    sget-object v0, LX/257;->b:LX/257;

    return-object v0

    .line 368254
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 368255
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
