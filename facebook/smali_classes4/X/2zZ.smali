.class public LX/2zZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2K2;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:Lcom/facebook/gk/store/GatekeeperWriter;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/gk/store/GatekeeperWriter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483517
    iput-object p1, p0, LX/2zZ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 483518
    iput-object p2, p0, LX/2zZ;->b:Lcom/facebook/gk/store/GatekeeperWriter;

    .line 483519
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 483520
    iget-object v0, p0, LX/2zZ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2ai;->c:LX/0Tn;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 483521
    const-string v0, "gatekeepers"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v0

    .line 483522
    iget-object v1, p0, LX/2zZ;->b:Lcom/facebook/gk/store/GatekeeperWriter;

    invoke-interface {v1}, Lcom/facebook/gk/store/GatekeeperWriter;->e()LX/2LD;

    move-result-object v1

    invoke-interface {v1, v0}, LX/2LD;->a([Z)LX/2LD;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/2LD;->a(Z)V

    .line 483523
    return-void
.end method
