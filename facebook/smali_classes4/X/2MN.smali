.class public final LX/2MN;
.super LX/0QM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0QM",
        "<",
        "LX/6ed;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2ML;


# direct methods
.method public constructor <init>(LX/2ML;)V
    .locals 0

    .prologue
    .line 396534
    iput-object p1, p0, LX/2MN;->a:LX/2ML;

    invoke-direct {p0}, LX/0QM;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 396535
    check-cast p1, LX/6ed;

    .line 396536
    iget-object v0, p0, LX/2MN;->a:LX/2ML;

    iget-object v1, p1, LX/6ed;->a:Landroid/net/Uri;

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 396537
    iget-object v2, v0, LX/2ML;->c:LX/2MM;

    invoke-virtual {v2, v1}, LX/2MM;->a(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v2

    .line 396538
    if-nez v2, :cond_0

    .line 396539
    sget-object v2, LX/2ML;->a:Ljava/lang/Class;

    const-string v3, "Couldn\'t resolve backing file for media resource: %s"

    new-array v4, v9, [Ljava/lang/Object;

    aput-object v1, v4, v8

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 396540
    const-string v2, ""

    .line 396541
    :goto_0
    move-object v0, v2

    .line 396542
    return-object v0

    .line 396543
    :cond_0
    iget-object v3, v0, LX/2ML;->d:LX/0W3;

    sget-wide v4, LX/0X5;->fJ:J

    const-string v6, "FANeflaawkeANLGireg43"

    invoke-interface {v3, v4, v5, v6}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 396544
    :try_start_0
    const-string v4, "HmacSHA256"

    invoke-static {v4}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v4

    .line 396545
    new-instance v5, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    const-string v7, "HmacSHA256"

    invoke-direct {v5, v6, v7}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 396546
    invoke-virtual {v4, v5}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 396547
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 396548
    new-instance v5, LX/6ec;

    invoke-direct {v5, v0, v4, v6, v7}, LX/6ec;-><init>(LX/2ML;Ljavax/crypto/Mac;J)V

    .line 396549
    invoke-static {v2}, LX/1t3;->a(Ljava/io/File;)LX/1vI;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/1vI;->a(LX/6ec;)Ljava/lang/Object;

    move-result-object v4

    move-object v2, v4

    .line 396550
    check-cast v2, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 396551
    :catch_0
    move-exception v2

    .line 396552
    sget-object v4, LX/2ML;->a:Ljava/lang/Class;

    const-string v5, "Couldn\'t generate sha256 hash of file: %s with salt: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v8

    aput-object v3, v6, v9

    invoke-static {v4, v2, v5, v6}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 396553
    const-string v2, ""

    goto :goto_0
.end method
