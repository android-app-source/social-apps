.class public final LX/2pu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2pf;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/19P;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2qU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/19P;LX/2pf;)V
    .locals 1

    .prologue
    .line 469098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 469099
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2pu;->b:Ljava/lang/ref/WeakReference;

    .line 469100
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    .line 469101
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 469088
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469089
    :goto_0
    return-void

    .line 469090
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->a()V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 469091
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469092
    :goto_0
    return-void

    .line 469093
    :cond_0
    iget-object v0, p0, LX/2pu;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    iget-object v1, p0, LX/2pu;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2qU;

    invoke-static {v0, v1}, LX/19P;->j(LX/19P;LX/2qU;)V

    .line 469094
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(I)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 469095
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469096
    :goto_0
    return-void

    .line 469097
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->a(II)V

    goto :goto_0
.end method

.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 469102
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469103
    :goto_0
    return-void

    .line 469104
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(LX/04g;)V

    goto :goto_0
.end method

.method public final a(LX/04g;Z)V
    .locals 1

    .prologue
    .line 469108
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469109
    :goto_0
    return-void

    .line 469110
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->a(LX/04g;Z)V

    goto :goto_0
.end method

.method public final a(LX/2oi;)V
    .locals 1

    .prologue
    .line 469105
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469106
    :goto_0
    return-void

    .line 469107
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(LX/2oi;)V

    goto :goto_0
.end method

.method public final a(LX/2qD;)V
    .locals 1

    .prologue
    .line 469117
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 469118
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(LX/2qD;)V

    .line 469119
    :cond_0
    return-void
.end method

.method public final a(LX/7Jj;)V
    .locals 1

    .prologue
    .line 469120
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469121
    :goto_0
    return-void

    .line 469122
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(LX/7Jj;)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 469114
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469115
    :goto_0
    return-void

    .line 469116
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 469111
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469112
    :goto_0
    return-void

    .line 469113
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/7Jj;)V
    .locals 1

    .prologue
    .line 469082
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469083
    :goto_0
    return-void

    .line 469084
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->a(Ljava/lang/String;LX/7Jj;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 469085
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469086
    :goto_0
    return-void

    .line 469087
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 469028
    iget-object v0, p0, LX/2pu;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    iget-object v1, p0, LX/2pu;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2qU;

    invoke-virtual {v0, v1}, LX/19P;->b(LX/2qU;)V

    .line 469029
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469030
    :goto_0
    return-void

    .line 469031
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->b()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 469032
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469033
    :goto_0
    return-void

    .line 469034
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->b(I)V

    goto :goto_0
.end method

.method public final b(LX/04g;)V
    .locals 1

    .prologue
    .line 469035
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469036
    :goto_0
    return-void

    .line 469037
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->b(LX/04g;)V

    goto :goto_0
.end method

.method public final b(LX/04g;Z)V
    .locals 1

    .prologue
    .line 469038
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469039
    :goto_0
    return-void

    .line 469040
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->b(LX/04g;Z)V

    goto :goto_0
.end method

.method public final b(LX/2qD;)V
    .locals 1

    .prologue
    .line 469041
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 469042
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->b(LX/2qD;)V

    .line 469043
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 10

    .prologue
    .line 469044
    iget-object v0, p0, LX/2pu;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19P;

    iget-object v1, p0, LX/2pu;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2qU;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 469045
    invoke-static {v0, v1}, LX/19P;->h(LX/19P;LX/2qU;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 469046
    :goto_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469047
    :goto_1
    return-void

    .line 469048
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->c()V

    goto :goto_1

    .line 469049
    :cond_1
    const-wide/16 v8, 0x5

    move-wide v2, v8

    .line 469050
    iget-object v4, v0, LX/19P;->c:Ljava/util/List;

    invoke-static {v4}, LX/13m;->a(Ljava/util/Collection;)V

    .line 469051
    iget-object v4, v0, LX/19P;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v4, v2

    if-ltz v2, :cond_2

    .line 469052
    iget-object v2, v0, LX/19P;->c:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2qU;

    .line 469053
    iget-object v3, v0, LX/19P;->c:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 469054
    if-eqz v2, :cond_2

    .line 469055
    invoke-virtual {v2}, LX/2qU;->n()V

    .line 469056
    new-array v3, v7, [Ljava/lang/Object;

    .line 469057
    iget v4, v2, LX/2qU;->c:I

    move v2, v4

    .line 469058
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v6

    .line 469059
    :cond_2
    iget-object v2, v0, LX/19P;->c:Ljava/util/List;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 469060
    new-array v2, v7, [Ljava/lang/Object;

    .line 469061
    iget v3, v1, LX/2qU;->c:I

    move v3, v3

    .line 469062
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    goto :goto_0
.end method

.method public final c(LX/04g;)V
    .locals 1

    .prologue
    .line 469063
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469064
    :goto_0
    return-void

    .line 469065
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->c(LX/04g;)V

    goto :goto_0
.end method

.method public final c(LX/04g;Z)V
    .locals 1

    .prologue
    .line 469066
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469067
    :goto_0
    return-void

    .line 469068
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->c(LX/04g;Z)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 469078
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 469079
    if-eqz v0, :cond_0

    .line 469080
    invoke-interface {v0}, LX/2pf;->d()V

    .line 469081
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 469069
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469070
    :goto_0
    return-void

    .line 469071
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->e()V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 469072
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469073
    :goto_0
    return-void

    .line 469074
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->f()V

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 469075
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 469076
    :goto_0
    return-void

    .line 469077
    :cond_0
    iget-object v0, p0, LX/2pu;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->g()V

    goto :goto_0
.end method
