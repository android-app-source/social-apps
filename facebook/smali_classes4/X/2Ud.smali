.class public LX/2Ud;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0SG;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/2Ud;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0SG;

.field private c:LX/0Xl;

.field public d:LX/0So;

.field private e:LX/2Uc;

.field public f:J

.field public g:LX/2Ue;

.field public h:J


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0Xl;LX/0So;LX/2Uc;)V
    .locals 4
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 416397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416398
    sget-object v0, LX/2Ue;->UNKNOWN:LX/2Ue;

    iput-object v0, p0, LX/2Ud;->g:LX/2Ue;

    .line 416399
    iput-object p1, p0, LX/2Ud;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 416400
    iput-object p2, p0, LX/2Ud;->b:LX/0SG;

    .line 416401
    iput-object p3, p0, LX/2Ud;->c:LX/0Xl;

    .line 416402
    iput-object p4, p0, LX/2Ud;->d:LX/0So;

    .line 416403
    iput-object p5, p0, LX/2Ud;->e:LX/2Uc;

    .line 416404
    invoke-interface {p2}, LX/0SG;->a()J

    move-result-wide v0

    invoke-interface {p4}, LX/0So;->now()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/2Ud;->h:J

    .line 416405
    iget-object v0, p0, LX/2Ud;->e:LX/2Uc;

    invoke-virtual {v0}, LX/2Uc;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416406
    invoke-virtual {p0}, LX/2Ud;->d()V

    .line 416407
    sget-object v0, LX/2Ue;->ACCURATE:LX/2Ue;

    iput-object v0, p0, LX/2Ud;->g:LX/2Ue;

    .line 416408
    :cond_0
    :goto_0
    return-void

    .line 416409
    :cond_1
    iget-object v0, p0, LX/2Ud;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6cQ;->a:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416410
    iget-object v0, p0, LX/2Ud;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6cQ;->a:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/2Ud;->b(J)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/2Ud;
    .locals 9

    .prologue
    .line 416411
    sget-object v0, LX/2Ud;->i:LX/2Ud;

    if-nez v0, :cond_1

    .line 416412
    const-class v1, LX/2Ud;

    monitor-enter v1

    .line 416413
    :try_start_0
    sget-object v0, LX/2Ud;->i:LX/2Ud;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 416414
    if-eqz v2, :cond_0

    .line 416415
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 416416
    new-instance v3, LX/2Ud;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v0}, LX/2Uc;->b(LX/0QB;)LX/2Uc;

    move-result-object v8

    check-cast v8, LX/2Uc;

    invoke-direct/range {v3 .. v8}, LX/2Ud;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0Xl;LX/0So;LX/2Uc;)V

    .line 416417
    move-object v0, v3

    .line 416418
    sput-object v0, LX/2Ud;->i:LX/2Ud;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416419
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 416420
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 416421
    :cond_1
    sget-object v0, LX/2Ud;->i:LX/2Ud;

    return-object v0

    .line 416422
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 416423
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 416424
    iget-object v0, p0, LX/2Ud;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/2Ud;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)J
    .locals 3

    .prologue
    .line 416425
    sget-object v0, LX/6cR;->a:[I

    iget-object v1, p0, LX/2Ud;->g:LX/2Ue;

    invoke-virtual {v1}, LX/2Ue;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 416426
    :goto_0
    :pswitch_0
    return-wide p1

    .line 416427
    :pswitch_1
    iget-wide v0, p0, LX/2Ud;->f:J

    sub-long/2addr p1, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(J)V
    .locals 5

    .prologue
    .line 416428
    iget-wide v0, p0, LX/2Ud;->f:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 416429
    iput-wide p1, p0, LX/2Ud;->f:J

    .line 416430
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 416431
    sget-object v0, LX/2Ue;->SKEWED:LX/2Ue;

    iput-object v0, p0, LX/2Ud;->g:LX/2Ue;

    .line 416432
    :goto_0
    iget-object v0, p0, LX/2Ud;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/6cQ;->a:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 416433
    iget-object v0, p0, LX/2Ud;->c:LX/0Xl;

    const-string v1, "com.facebook.orca.SKEW_CHANGED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 416434
    :cond_0
    return-void

    .line 416435
    :cond_1
    sget-object v0, LX/2Ue;->ACCURATE:LX/2Ue;

    iput-object v0, p0, LX/2Ud;->g:LX/2Ue;

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 416436
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, LX/2Ud;->b(J)V

    .line 416437
    return-void
.end method
