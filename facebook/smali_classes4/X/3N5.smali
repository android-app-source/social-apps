.class public LX/3N5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3N5;


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 558191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558192
    return-void
.end method

.method public static a(LX/0QB;)LX/3N5;
    .locals 4

    .prologue
    .line 558193
    sget-object v0, LX/3N5;->b:LX/3N5;

    if-nez v0, :cond_1

    .line 558194
    const-class v1, LX/3N5;

    monitor-enter v1

    .line 558195
    :try_start_0
    sget-object v0, LX/3N5;->b:LX/3N5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 558196
    if-eqz v2, :cond_0

    .line 558197
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 558198
    new-instance p0, LX/3N5;

    invoke-direct {p0}, LX/3N5;-><init>()V

    .line 558199
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    .line 558200
    iput-object v3, p0, LX/3N5;->a:LX/0Uh;

    .line 558201
    move-object v0, p0

    .line 558202
    sput-object v0, LX/3N5;->b:LX/3N5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 558203
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 558204
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 558205
    :cond_1
    sget-object v0, LX/3N5;->b:LX/3N5;

    return-object v0

    .line 558206
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 558207
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 558208
    iget-object v0, p0, LX/3N5;->a:LX/0Uh;

    const/16 v1, 0x16e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
