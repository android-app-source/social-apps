.class public final enum LX/2Ho;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2Ho;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2Ho;

.field public static final enum ACTIVE:LX/2Ho;

.field public static final enum PAUSED:LX/2Ho;

.field public static final enum STOPPED:LX/2Ho;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 391102
    new-instance v0, LX/2Ho;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v2}, LX/2Ho;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ho;->ACTIVE:LX/2Ho;

    .line 391103
    new-instance v0, LX/2Ho;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, LX/2Ho;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ho;->PAUSED:LX/2Ho;

    .line 391104
    new-instance v0, LX/2Ho;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v4}, LX/2Ho;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/2Ho;->STOPPED:LX/2Ho;

    .line 391105
    const/4 v0, 0x3

    new-array v0, v0, [LX/2Ho;

    sget-object v1, LX/2Ho;->ACTIVE:LX/2Ho;

    aput-object v1, v0, v2

    sget-object v1, LX/2Ho;->PAUSED:LX/2Ho;

    aput-object v1, v0, v3

    sget-object v1, LX/2Ho;->STOPPED:LX/2Ho;

    aput-object v1, v0, v4

    sput-object v0, LX/2Ho;->$VALUES:[LX/2Ho;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 391107
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2Ho;
    .locals 1

    .prologue
    .line 391108
    const-class v0, LX/2Ho;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Ho;

    return-object v0
.end method

.method public static values()[LX/2Ho;
    .locals 1

    .prologue
    .line 391106
    sget-object v0, LX/2Ho;->$VALUES:[LX/2Ho;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2Ho;

    return-object v0
.end method
