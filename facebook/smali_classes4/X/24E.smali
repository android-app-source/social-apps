.class public final enum LX/24E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/24E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/24E;

.field public static final enum ALL:LX/24E;

.field public static final enum MEDIA_REMINDER:LX/24E;

.field public static final enum NONE:LX/24E;


# instance fields
.field private final mParamValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 366837
    new-instance v0, LX/24E;

    const-string v1, "NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v3, v2}, LX/24E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/24E;->NONE:LX/24E;

    .line 366838
    new-instance v0, LX/24E;

    const-string v1, "MEDIA_REMINDER"

    const-string v2, "media_reminder"

    invoke-direct {v0, v1, v4, v2}, LX/24E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/24E;->MEDIA_REMINDER:LX/24E;

    .line 366839
    new-instance v0, LX/24E;

    const-string v1, "ALL"

    const-string v2, "all"

    invoke-direct {v0, v1, v5, v2}, LX/24E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/24E;->ALL:LX/24E;

    .line 366840
    const/4 v0, 0x3

    new-array v0, v0, [LX/24E;

    sget-object v1, LX/24E;->NONE:LX/24E;

    aput-object v1, v0, v3

    sget-object v1, LX/24E;->MEDIA_REMINDER:LX/24E;

    aput-object v1, v0, v4

    sget-object v1, LX/24E;->ALL:LX/24E;

    aput-object v1, v0, v5

    sput-object v0, LX/24E;->$VALUES:[LX/24E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 366844
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 366845
    iput-object p3, p0, LX/24E;->mParamValue:Ljava/lang/String;

    .line 366846
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/24E;
    .locals 1

    .prologue
    .line 366843
    const-class v0, LX/24E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/24E;

    return-object v0
.end method

.method public static values()[LX/24E;
    .locals 1

    .prologue
    .line 366842
    sget-object v0, LX/24E;->$VALUES:[LX/24E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/24E;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 366841
    iget-object v0, p0, LX/24E;->mParamValue:Ljava/lang/String;

    return-object v0
.end method
