.class public LX/2pe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2pf;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 468863
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/2pe;-><init>(LX/2pf;)V

    .line 468864
    return-void
.end method

.method private constructor <init>(LX/2pf;)V
    .locals 1

    .prologue
    .line 468865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 468866
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    .line 468867
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 468868
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468869
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->a()V

    .line 468870
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 468871
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468872
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(I)V

    .line 468873
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 468874
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468875
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->a(II)V

    .line 468876
    :cond_0
    return-void
.end method

.method public a(LX/04g;)V
    .locals 1

    .prologue
    .line 468892
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468893
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(LX/04g;)V

    .line 468894
    :cond_0
    return-void
.end method

.method public a(LX/04g;Z)V
    .locals 1

    .prologue
    .line 468877
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468878
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->a(LX/04g;Z)V

    .line 468879
    :cond_0
    return-void
.end method

.method public a(LX/2oi;)V
    .locals 1

    .prologue
    .line 468880
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468881
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(LX/2oi;)V

    .line 468882
    :cond_0
    return-void
.end method

.method public a(LX/2qD;)V
    .locals 1

    .prologue
    .line 468883
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468884
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(LX/2qD;)V

    .line 468885
    :cond_0
    return-void
.end method

.method public final a(LX/7Jj;)V
    .locals 1

    .prologue
    .line 468886
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468887
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(LX/7Jj;)V

    .line 468888
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 468889
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468890
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(Landroid/graphics/Bitmap;)V

    .line 468891
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 468857
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468858
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(Ljava/lang/String;)V

    .line 468859
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;LX/7Jj;)V
    .locals 1

    .prologue
    .line 468860
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468861
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->a(Ljava/lang/String;LX/7Jj;)V

    .line 468862
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 468817
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 468818
    :goto_0
    return-void

    .line 468819
    :cond_0
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 468820
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468821
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->b()V

    .line 468822
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 468823
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468824
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->b(I)V

    .line 468825
    :cond_0
    return-void
.end method

.method public b(LX/04g;)V
    .locals 1

    .prologue
    .line 468826
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468827
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->b(LX/04g;)V

    .line 468828
    :cond_0
    return-void
.end method

.method public final b(LX/04g;Z)V
    .locals 1

    .prologue
    .line 468829
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468830
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->b(LX/04g;Z)V

    .line 468831
    :cond_0
    return-void
.end method

.method public b(LX/2qD;)V
    .locals 1

    .prologue
    .line 468832
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468833
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->b(LX/2qD;)V

    .line 468834
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 468835
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468836
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->c()V

    .line 468837
    :cond_0
    return-void
.end method

.method public c(LX/04g;)V
    .locals 1

    .prologue
    .line 468838
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468839
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1}, LX/2pf;->c(LX/04g;)V

    .line 468840
    :cond_0
    return-void
.end method

.method public final c(LX/04g;Z)V
    .locals 1

    .prologue
    .line 468841
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468842
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0, p1, p2}, LX/2pf;->c(LX/04g;Z)V

    .line 468843
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 468844
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    .line 468845
    if-eqz v0, :cond_0

    .line 468846
    invoke-interface {v0}, LX/2pf;->d()V

    .line 468847
    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 468848
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468849
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->e()V

    .line 468850
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 468851
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468852
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->f()V

    .line 468853
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 468854
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 468855
    iget-object v0, p0, LX/2pe;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pf;

    invoke-interface {v0}, LX/2pf;->g()V

    .line 468856
    :cond_0
    return-void
.end method
