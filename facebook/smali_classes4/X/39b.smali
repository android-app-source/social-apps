.class public LX/39b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static m:LX/0Xm;


# instance fields
.field private final a:LX/39c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/39c",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/39d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/39d",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:LX/39e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/39e",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final d:LX/1zf;

.field private final e:LX/20g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/20g",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/20q;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/20K;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/20P;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Landroid/os/Handler;

.field private final l:LX/21T;


# direct methods
.method public constructor <init>(LX/39c;LX/39d;LX/39e;LX/1zf;LX/20g;LX/20q;LX/0Or;LX/20P;LX/0Ot;LX/0Ot;Landroid/os/Handler;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;)V
    .locals 1
    .param p11    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/39c;",
            "LX/39d;",
            "LX/39e;",
            "LX/1zf;",
            "LX/20g;",
            "LX/20q;",
            "LX/0Or",
            "<",
            "LX/20K;",
            ">;",
            "LX/20P;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "Landroid/os/Handler;",
            "Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 523392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523393
    iput-object p1, p0, LX/39b;->a:LX/39c;

    .line 523394
    iput-object p2, p0, LX/39b;->b:LX/39d;

    .line 523395
    iput-object p3, p0, LX/39b;->c:LX/39e;

    .line 523396
    iput-object p4, p0, LX/39b;->d:LX/1zf;

    .line 523397
    iput-object p5, p0, LX/39b;->e:LX/20g;

    .line 523398
    iput-object p6, p0, LX/39b;->f:LX/20q;

    .line 523399
    iput-object p7, p0, LX/39b;->g:LX/0Or;

    .line 523400
    iput-object p8, p0, LX/39b;->h:LX/20P;

    .line 523401
    iput-object p9, p0, LX/39b;->i:LX/0Ot;

    .line 523402
    iput-object p10, p0, LX/39b;->j:LX/0Ot;

    .line 523403
    iput-object p11, p0, LX/39b;->k:Landroid/os/Handler;

    .line 523404
    invoke-virtual {p12}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a()LX/21T;

    move-result-object v0

    iput-object v0, p0, LX/39b;->l:LX/21T;

    .line 523405
    return-void
.end method

.method private static a(LX/39b;Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 523390
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 523391
    iget-object v1, p0, LX/39b;->d:LX/1zf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1zf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/39b;LX/20K;LX/39f;)LX/20b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/20K;",
            "LX/39b",
            "<TE;>.ComponentsReactionsDockSupport;)",
            "LX/20b;"
        }
    .end annotation

    .prologue
    .line 523389
    iget-object v0, p0, LX/39b;->h:LX/20P;

    new-instance v1, LX/39g;

    invoke-direct {v1, p0, p1, p2}, LX/39g;-><init>(LX/39b;LX/20K;LX/39f;)V

    invoke-virtual {v0, v1}, LX/20P;->a(LX/20E;)LX/20b;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/39b;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/21H;)LX/21M;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Po;",
            "Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiState;",
            ")",
            "LX/21M;"
        }
    .end annotation

    .prologue
    .line 523377
    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/217;->a(LX/1PT;)Ljava/lang/String;

    move-result-object v1

    .line 523378
    invoke-static {}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->newBuilder()LX/21A;

    move-result-object v0

    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    .line 523379
    iput-object v2, v0, LX/21A;->a:LX/162;

    .line 523380
    move-object v0, v0

    .line 523381
    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-static {v2}, LX/217;->b(LX/1PT;)Ljava/lang/String;

    move-result-object v2

    .line 523382
    iput-object v2, v0, LX/21A;->b:Ljava/lang/String;

    .line 523383
    move-object v0, v0

    .line 523384
    iput-object v1, v0, LX/21A;->c:Ljava/lang/String;

    .line 523385
    move-object v2, v0

    .line 523386
    iget-object v3, p0, LX/39b;->e:LX/20g;

    .line 523387
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 523388
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3, v0, v2, v1, p3}, LX/20g;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/21A;Ljava/lang/String;LX/21H;)LX/21M;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/39b;
    .locals 3

    .prologue
    .line 523369
    const-class v1, LX/39b;

    monitor-enter v1

    .line 523370
    :try_start_0
    sget-object v0, LX/39b;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 523371
    sput-object v2, LX/39b;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523372
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523373
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/39b;->b(LX/0QB;)LX/39b;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 523374
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/39b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523375
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 523376
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/39b;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/21H;)LX/20Z;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiState;",
            ")",
            "LX/20Z;"
        }
    .end annotation

    .prologue
    .line 523315
    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/217;->a(LX/1PT;)Ljava/lang/String;

    move-result-object v5

    .line 523316
    invoke-static {}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->newBuilder()LX/21A;

    move-result-object v0

    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 523317
    iput-object v1, v0, LX/21A;->a:LX/162;

    .line 523318
    move-object v0, v0

    .line 523319
    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-static {v1}, LX/217;->b(LX/1PT;)Ljava/lang/String;

    move-result-object v1

    .line 523320
    iput-object v1, v0, LX/21A;->b:Ljava/lang/String;

    .line 523321
    move-object v0, v0

    .line 523322
    iput-object v5, v0, LX/21A;->c:Ljava/lang/String;

    .line 523323
    move-object v4, v0

    .line 523324
    iget-object v0, p0, LX/39b;->e:LX/20g;

    const/4 v3, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, LX/20g;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;ZLX/21A;Ljava/lang/String;LX/21H;)LX/20Z;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/39b;Lcom/facebook/graphql/model/GraphQLStory;)LX/20z;
    .locals 3

    .prologue
    .line 523366
    iget-object v0, p0, LX/39b;->e:LX/20g;

    invoke-virtual {v0, p1}, LX/20g;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/20z;

    move-result-object v0

    .line 523367
    iget-object v1, p0, LX/39b;->d:LX/1zf;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {v2}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, LX/1zf;->a(I)LX/1zt;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/20z;->a(LX/1zt;)V

    .line 523368
    return-object v0
.end method

.method private static b(LX/0QB;)LX/39b;
    .locals 13

    .prologue
    .line 523364
    new-instance v0, LX/39b;

    invoke-static {p0}, LX/39c;->a(LX/0QB;)LX/39c;

    move-result-object v1

    check-cast v1, LX/39c;

    invoke-static {p0}, LX/39d;->a(LX/0QB;)LX/39d;

    move-result-object v2

    check-cast v2, LX/39d;

    invoke-static {p0}, LX/39e;->a(LX/0QB;)LX/39e;

    move-result-object v3

    check-cast v3, LX/39e;

    invoke-static {p0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v4

    check-cast v4, LX/1zf;

    invoke-static {p0}, LX/20g;->a(LX/0QB;)LX/20g;

    move-result-object v5

    check-cast v5, LX/20g;

    invoke-static {p0}, LX/20q;->a(LX/0QB;)LX/20q;

    move-result-object v6

    check-cast v6, LX/20q;

    const/16 v7, 0x7b6

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const-class v8, LX/20P;

    invoke-interface {p0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/20P;

    const/16 v9, 0x103d

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x3567

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v11

    check-cast v11, Landroid/os/Handler;

    invoke-static {p0}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;

    invoke-direct/range {v0 .. v12}, LX/39b;-><init>(LX/39c;LX/39d;LX/39e;LX/1zf;LX/20g;LX/20q;LX/0Or;LX/20P;LX/0Ot;LX/0Ot;Landroid/os/Handler;Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;)V

    .line 523365
    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/1Wk;IILX/1zt;Lcom/facebook/feed/rows/core/props/FeedProps;LX/20K;LX/39f;LX/20b;LX/20b;)LX/1Dg;
    .locals 8
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Po;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Wk;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "LX/1Wk;",
            "II",
            "LX/1zt;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/20K;",
            "LX/39b",
            "<TE;>.ComponentsReactionsDockSupport;",
            "LX/20b;",
            "LX/20b;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 523334
    move-object/from16 v0, p10

    invoke-static {v0, p1}, LX/39f;->a(LX/39f;LX/1De;)LX/1De;

    .line 523335
    move-object/from16 v0, p10

    invoke-static {v0, p2}, LX/39f;->a(LX/39f;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 523336
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b00a6

    invoke-interface {v1, v2}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    .line 523337
    const/4 v1, 0x3

    if-ne p6, v1, :cond_1

    .line 523338
    iget-object v1, p0, LX/39b;->c:LX/39e;

    invoke-virtual {v1, p1}, LX/39e;->c(LX/1De;)LX/C48;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/C48;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C48;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/C48;->a(LX/1Po;)LX/C48;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    invoke-virtual {v1, p7}, LX/C48;->a(LX/1zt;)LX/C48;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LX/C48;->a(Z)LX/C48;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/C48;->h(I)LX/C48;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 523339
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    .line 523340
    :goto_1
    return-object v1

    .line 523341
    :cond_0
    const/4 p7, 0x0

    goto :goto_0

    .line 523342
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v3

    .line 523343
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    .line 523344
    iget-object v1, p0, LX/39b;->l:LX/21T;

    const/4 v5, 0x1

    invoke-virtual {v1, v5, v3, v4}, LX/21T;->a(ZZZ)LX/21Y;

    move-result-object v1

    .line 523345
    invoke-virtual {v1, p5}, LX/21Y;->a(I)LX/21c;

    move-result-object v5

    .line 523346
    invoke-static {v5}, LX/21c;->hasIcons(LX/21c;)Z

    move-result v6

    .line 523347
    invoke-virtual {v1, v5}, LX/21Y;->a(LX/21c;)[F

    move-result-object v5

    .line 523348
    move-object/from16 v0, p8

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 523349
    :goto_2
    iget-object v1, p0, LX/39b;->b:LX/39d;

    invoke-virtual {v1, p1}, LX/39d;->c(LX/1De;)LX/39i;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/39i;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39i;

    move-result-object v1

    invoke-virtual {v1, p7}, LX/39i;->a(LX/1zt;)LX/39i;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/39i;->a(LX/1Wk;)LX/39i;

    move-result-object v7

    move-object v1, p3

    check-cast v1, LX/1Pr;

    invoke-virtual {v7, v1}, LX/39i;->a(LX/1Pr;)LX/39i;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/39i;->a(Z)LX/39i;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-static {p1}, LX/1Wn;->d(LX/1De;)LX/1dQ;

    move-result-object v7

    invoke-interface {v1, v7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v7

    if-nez p6, :cond_6

    move-object/from16 v0, p11

    invoke-static {p1, v0}, LX/1Wn;->a(LX/1De;LX/20b;)LX/1dQ;

    move-result-object v1

    :goto_3
    invoke-interface {v7, v1}, LX/1Di;->c(LX/1dQ;)LX/1Di;

    move-result-object v1

    const/4 v7, 0x0

    invoke-interface {v1, v7}, LX/1Di;->e(I)LX/1Di;

    move-result-object v1

    const/4 v7, 0x0

    aget v7, v5, v7

    invoke-interface {v1, v7}, LX/1Di;->b(F)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 523350
    const/4 v1, 0x1

    .line 523351
    if-eqz v3, :cond_2

    .line 523352
    iget-object v1, p0, LX/39b;->a:LX/39c;

    invoke-virtual {v1, p1}, LX/39c;->c(LX/1De;)LX/39u;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/39u;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39u;

    move-result-object v1

    sget-object v3, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v1, v3}, LX/39u;->a(LX/20X;)LX/39u;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/39u;->a(LX/1Wk;)LX/39u;

    move-result-object v3

    move-object v1, p3

    check-cast v1, LX/1Pr;

    invoke-virtual {v3, v1}, LX/39u;->a(LX/1Pr;)LX/39u;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/39u;->a(Z)LX/39u;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-static {p1}, LX/1Wn;->e(LX/1De;)LX/1dQ;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    if-nez p12, :cond_7

    const/4 v1, 0x0

    :goto_4
    invoke-interface {v3, v1}, LX/1Di;->c(LX/1dQ;)LX/1Di;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, LX/1Di;->e(I)LX/1Di;

    move-result-object v3

    const/4 v7, 0x1

    const/4 v1, 0x2

    aget v7, v5, v7

    invoke-interface {v3, v7}, LX/1Di;->b(F)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 523353
    :cond_2
    if-eqz v4, :cond_3

    .line 523354
    iget-object v3, p0, LX/39b;->a:LX/39c;

    invoke-virtual {v3, p1}, LX/39c;->c(LX/1De;)LX/39u;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/39u;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39u;

    move-result-object v3

    sget-object v4, LX/20X;->SHARE:LX/20X;

    invoke-virtual {v3, v4}, LX/39u;->a(LX/20X;)LX/39u;

    move-result-object v3

    invoke-virtual {v3, p4}, LX/39u;->a(LX/1Wk;)LX/39u;

    move-result-object v3

    check-cast p3, LX/1Pr;

    invoke-virtual {v3, p3}, LX/39u;->a(LX/1Pr;)LX/39u;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/39u;->a(Z)LX/39u;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-static {p1}, LX/1Wn;->f(LX/1De;)LX/1dQ;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Di;->e(I)LX/1Di;

    move-result-object v3

    aget v1, v5, v1

    invoke-interface {v3, v1}, LX/1Di;->b(F)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 523355
    :cond_3
    const/4 v1, 0x1

    if-ne p6, v1, :cond_8

    .line 523356
    invoke-static {p1}, LX/C4H;->c(LX/1De;)LX/C4F;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 523357
    :cond_4
    :goto_5
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_1

    .line 523358
    :cond_5
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 523359
    iget-object v7, p0, LX/39b;->d:LX/1zf;

    invoke-static {v1}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v7, v1}, LX/1zf;->a(I)LX/1zt;

    move-result-object p7

    goto/16 :goto_2

    .line 523360
    :cond_6
    move-object/from16 v0, p9

    invoke-static {p1, v0}, LX/1Wn;->a(LX/1De;LX/20K;)LX/1dQ;

    move-result-object v1

    goto/16 :goto_3

    .line 523361
    :cond_7
    move-object/from16 v0, p12

    invoke-static {p1, v0}, LX/1Wn;->a(LX/1De;LX/20b;)LX/1dQ;

    move-result-object v1

    goto/16 :goto_4

    .line 523362
    :cond_8
    const/4 v1, 0x2

    if-ne p6, v1, :cond_4

    .line 523363
    invoke-static {p1}, LX/C4D;->c(LX/1De;)LX/C4B;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    goto :goto_5
.end method

.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V
    .locals 9
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Po;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "LX/1np",
            "<",
            "LX/20K;",
            ">;",
            "LX/1np",
            "<",
            "LX/39b",
            "<TE;>.ComponentsReactionsDockSupport;>;",
            "LX/1np",
            "<",
            "LX/20b;",
            ">;",
            "LX/1np",
            "<",
            "LX/20b;",
            ">;",
            "LX/1np",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiState;",
            ">;",
            "LX/1np",
            "<",
            "LX/20Z;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 523325
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 523326
    iget-object v1, p0, LX/39b;->e:LX/20g;

    invoke-virtual {v1, p2, p3}, LX/20g;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/21H;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 523327
    new-instance v1, LX/39f;

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0, v2}, LX/39b;->b(LX/39b;Lcom/facebook/graphql/model/GraphQLStory;)LX/20z;

    move-result-object v3

    invoke-virtual/range {p8 .. p8}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/21H;

    invoke-static {p0, p2, p3, v2}, LX/39b;->a(LX/39b;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/21H;)LX/21M;

    move-result-object v4

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0, v2}, LX/39b;->a(LX/39b;Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v5

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v6

    invoke-virtual/range {p8 .. p8}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/21H;

    const/4 v8, 0x0

    move-object v2, p0

    invoke-direct/range {v1 .. v8}, LX/39f;-><init>(LX/39b;LX/20z;LX/21M;LX/0Px;LX/1PT;LX/21H;B)V

    invoke-virtual {p5, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 523328
    iget-object v1, p0, LX/39b;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p4, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 523329
    invoke-virtual {p4}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20K;

    invoke-virtual {p5}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/39f;

    invoke-static {p0, v1, v2}, LX/39b;->a(LX/39b;LX/20K;LX/39f;)LX/20b;

    move-result-object v1

    invoke-virtual {p6, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 523330
    iget-object v1, p0, LX/39b;->f:LX/20q;

    invoke-virtual {v1}, LX/20q;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/39b;->e:LX/20g;

    invoke-virtual {v1, p2}, LX/20g;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/20b;

    move-result-object v1

    :goto_0
    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 523331
    invoke-virtual/range {p8 .. p8}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/21H;

    invoke-static {p0, p2, p3, v1}, LX/39b;->b(LX/39b;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/21H;)LX/20Z;

    move-result-object v1

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 523332
    return-void

    .line 523333
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
