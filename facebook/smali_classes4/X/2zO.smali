.class public LX/2zO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13H;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483353
    return-void
.end method

.method private static a(Ljava/lang/String;LX/0Rf;)LX/1Y7;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/1Y7;"
        }
    .end annotation

    .prologue
    .line 483354
    invoke-static {p0}, LX/6Xz;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 483355
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 483356
    sget-object v0, LX/1Y7;->a:LX/1Y7;

    .line 483357
    :goto_0
    return-object v0

    .line 483358
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 483359
    :cond_1
    sget-object v0, LX/1Y7;->b:LX/1Y7;

    goto :goto_0

    .line 483360
    :cond_2
    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 483361
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 483362
    sget-object v0, LX/1Y7;->b:LX/1Y7;

    goto :goto_0

    .line 483363
    :cond_3
    sget-object v0, LX/1Y7;->a:LX/1Y7;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;
    .locals 4

    .prologue
    .line 483364
    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v1

    .line 483365
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/facebook/interstitial/manager/InterstitialTrigger;->a:Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    if-nez v0, :cond_2

    .line 483366
    :cond_0
    sget-object v0, LX/1Y7;->a:LX/1Y7;

    .line 483367
    :cond_1
    :goto_0
    return-object v0

    .line 483368
    :cond_2
    iget-object v0, p2, Lcom/facebook/interstitial/manager/InterstitialTrigger;->a:Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    invoke-virtual {v0}, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a()LX/0Rf;

    move-result-object v2

    .line 483369
    if-eqz v1, :cond_4

    .line 483370
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-static {v0, v2}, LX/2zO;->a(Ljava/lang/String;LX/0Rf;)LX/1Y7;

    move-result-object v0

    .line 483371
    iget-boolean v3, v0, LX/1Y7;->c:Z

    if-eqz v3, :cond_1

    .line 483372
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-static {v0, v2}, LX/2zO;->a(Ljava/lang/String;LX/0Rf;)LX/1Y7;

    move-result-object v0

    .line 483373
    iget-boolean v3, v0, LX/1Y7;->c:Z

    if-eqz v3, :cond_1

    .line 483374
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v0, :cond_3

    .line 483375
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v0, v2}, LX/2zO;->a(Ljava/lang/String;LX/0Rf;)LX/1Y7;

    move-result-object v0

    .line 483376
    iget-boolean v3, v0, LX/1Y7;->c:Z

    if-eqz v3, :cond_1

    .line 483377
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    invoke-static {v0, v2}, LX/2zO;->a(Ljava/lang/String;LX/0Rf;)LX/1Y7;

    move-result-object v0

    .line 483378
    iget-boolean v3, v0, LX/1Y7;->c:Z

    if-eqz v3, :cond_1

    .line 483379
    :cond_3
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v0, :cond_4

    .line 483380
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v0, v2}, LX/2zO;->a(Ljava/lang/String;LX/0Rf;)LX/1Y7;

    move-result-object v0

    .line 483381
    iget-boolean v3, v0, LX/1Y7;->c:Z

    if-eqz v3, :cond_1

    .line 483382
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    invoke-static {v0, v2}, LX/2zO;->a(Ljava/lang/String;LX/0Rf;)LX/1Y7;

    move-result-object v0

    .line 483383
    iget-boolean v1, v0, LX/1Y7;->c:Z

    if-eqz v1, :cond_1

    .line 483384
    :cond_4
    sget-object v0, LX/1Y7;->a:LX/1Y7;

    goto :goto_0
.end method
