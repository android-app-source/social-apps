.class public final LX/32d;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/1kK;

.field private b:LX/1lP;

.field private c:LX/32e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 490769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(LX/1RN;)V
    .locals 1

    .prologue
    .line 490770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490771
    iget-object v0, p1, LX/1RN;->a:LX/1kK;

    iput-object v0, p0, LX/32d;->a:LX/1kK;

    .line 490772
    iget-object v0, p1, LX/1RN;->b:LX/1lP;

    iput-object v0, p0, LX/32d;->b:LX/1lP;

    .line 490773
    iget-object v0, p1, LX/1RN;->c:LX/32e;

    iput-object v0, p0, LX/32d;->c:LX/32e;

    .line 490774
    return-void
.end method


# virtual methods
.method public final a()LX/1RN;
    .locals 5

    .prologue
    .line 490775
    new-instance v3, LX/1RN;

    iget-object v0, p0, LX/32d;->a:LX/1kK;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kK;

    iget-object v1, p0, LX/32d;->b:LX/1lP;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lP;

    iget-object v2, p0, LX/32d;->c:LX/32e;

    if-nez v2, :cond_0

    sget-object v2, LX/32e;->c:LX/32e;

    :goto_0
    invoke-direct {v3, v0, v1, v2}, LX/1RN;-><init>(LX/1kK;LX/1lP;LX/32e;)V

    return-object v3

    :cond_0
    iget-object v2, p0, LX/32d;->c:LX/32e;

    goto :goto_0
.end method

.method public final a(LX/1kK;)LX/32d;
    .locals 1

    .prologue
    .line 490776
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kK;

    iput-object v0, p0, LX/32d;->a:LX/1kK;

    .line 490777
    return-object p0
.end method

.method public final a(LX/1lP;)LX/32d;
    .locals 1

    .prologue
    .line 490778
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lP;

    iput-object v0, p0, LX/32d;->b:LX/1lP;

    .line 490779
    return-object p0
.end method

.method public final a(LX/32e;)LX/32d;
    .locals 1

    .prologue
    .line 490780
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32e;

    iput-object v0, p0, LX/32d;->c:LX/32e;

    .line 490781
    return-object p0
.end method
