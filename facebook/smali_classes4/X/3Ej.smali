.class public LX/3Ej;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field private final f:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 537967
    const-class v0, LX/3Ej;

    sput-object v0, LX/3Ej;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/01U;LX/0aU;Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/01U;",
            "LX/0aU;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 537968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537969
    iput-object p1, p0, LX/3Ej;->b:Landroid/content/Context;

    .line 537970
    iput-object p2, p0, LX/3Ej;->c:LX/0Or;

    .line 537971
    invoke-virtual {p3}, LX/01U;->getPermission()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3Ej;->d:Ljava/lang/String;

    .line 537972
    const-string v0, "notification.ACTION_UPDATE"

    invoke-virtual {p4, v0}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3Ej;->e:Ljava/lang/String;

    .line 537973
    iput-object p5, p0, LX/3Ej;->f:Landroid/os/Handler;

    .line 537974
    return-void
.end method
