.class public LX/2ds;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2eR;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/2dr;


# direct methods
.method public constructor <init>(LX/2dr;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 444562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444563
    iput-object p1, p0, LX/2ds;->d:LX/2dr;

    .line 444564
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2ds;->a:Ljava/util/List;

    .line 444565
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2ds;->b:Ljava/util/HashMap;

    .line 444566
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2ds;->c:Ljava/util/HashMap;

    .line 444567
    return-void
.end method

.method public static a(LX/0QB;)LX/2ds;
    .locals 4

    .prologue
    .line 444551
    const-class v1, LX/2ds;

    monitor-enter v1

    .line 444552
    :try_start_0
    sget-object v0, LX/2ds;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 444553
    sput-object v2, LX/2ds;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 444554
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444555
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 444556
    new-instance p0, LX/2ds;

    invoke-static {v0}, LX/2dr;->a(LX/0QB;)LX/2dr;

    move-result-object v3

    check-cast v3, LX/2dr;

    invoke-direct {p0, v3}, LX/2ds;-><init>(LX/2dr;)V

    .line 444557
    move-object v0, p0

    .line 444558
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 444559
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2ds;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 444560
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 444561
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/2ds;LX/2eR;)I
    .locals 2

    .prologue
    .line 444549
    invoke-interface {p1}, LX/2eR;->a()Ljava/lang/Class;

    move-result-object v0

    .line 444550
    iget-object v1, p0, LX/2ds;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/2ds;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2eR;)I
    .locals 4

    .prologue
    .line 444542
    iget-object v0, p0, LX/2ds;->b:Ljava/util/HashMap;

    invoke-interface {p1}, LX/2eR;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 444543
    iget-object v0, p0, LX/2ds;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 444544
    iget-object v1, p0, LX/2ds;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 444545
    iget-object v1, p0, LX/2ds;->b:Ljava/util/HashMap;

    invoke-interface {p1}, LX/2eR;->a()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444546
    iget-object v1, p0, LX/2ds;->d:LX/2dr;

    .line 444547
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/1YF;->a(II)V

    .line 444548
    :cond_0
    iget-object v0, p0, LX/2ds;->b:Ljava/util/HashMap;

    invoke-interface {p1}, LX/2eR;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final a(I)LX/2eR;
    .locals 1

    .prologue
    .line 444541
    iget-object v0, p0, LX/2ds;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2eR;

    return-object v0
.end method
