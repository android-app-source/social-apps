.class public final LX/2aG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/2Zo;


# direct methods
.method public constructor <init>(LX/2Zo;)V
    .locals 0

    .prologue
    .line 424551
    iput-object p1, p0, LX/2aG;->a:LX/2Zo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 424552
    iget-object v0, p0, LX/2aG;->a:LX/2Zo;

    iget-object v0, v0, LX/2Zo;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetchComposerPrivacyOptions"

    .line 424553
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 424554
    move-object v0, v0

    .line 424555
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 424556
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 424557
    const-string v0, "fetchComposerPrivacyOptions"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel;

    .line 424558
    invoke-static {v0}, LX/35e;->a(Lcom/facebook/privacy/protocol/FetchComposerPrivacyOptionsModels$ViewerPrivacyOptionsModel;)Lcom/facebook/graphql/model/GraphQLViewer;

    move-result-object v1

    .line 424559
    iget-object v0, p0, LX/2aG;->a:LX/2Zo;

    iget-object v0, v0, LX/2Zo;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3R3;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/3R3;->a(Lcom/facebook/graphql/model/GraphQLViewer;Z)Lcom/facebook/privacy/model/PrivacyOptionsResult;

    move-result-object v1

    .line 424560
    iget-object v0, p0, LX/2aG;->a:LX/2Zo;

    iget-object v0, v0, LX/2Zo;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2c3;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2c3;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;Lcom/facebook/privacy/model/PrivacyOptionsResult;)V

    .line 424561
    return-void
.end method
