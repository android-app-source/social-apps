.class public LX/2PX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2PX;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/2PY;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2PY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 406620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406621
    iput-object p1, p0, LX/2PX;->a:Landroid/content/Context;

    .line 406622
    iput-object p2, p0, LX/2PX;->b:LX/2PY;

    .line 406623
    return-void
.end method

.method public static a(LX/0QB;)LX/2PX;
    .locals 5

    .prologue
    .line 406624
    sget-object v0, LX/2PX;->c:LX/2PX;

    if-nez v0, :cond_1

    .line 406625
    const-class v1, LX/2PX;

    monitor-enter v1

    .line 406626
    :try_start_0
    sget-object v0, LX/2PX;->c:LX/2PX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 406627
    if-eqz v2, :cond_0

    .line 406628
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 406629
    new-instance p0, LX/2PX;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2PY;->b(LX/0QB;)LX/2PY;

    move-result-object v4

    check-cast v4, LX/2PY;

    invoke-direct {p0, v3, v4}, LX/2PX;-><init>(Landroid/content/Context;LX/2PY;)V

    .line 406630
    move-object v0, p0

    .line 406631
    sput-object v0, LX/2PX;->c:LX/2PX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 406632
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 406633
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 406634
    :cond_1
    sget-object v0, LX/2PX;->c:LX/2PX;

    return-object v0

    .line 406635
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 406636
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 4

    .prologue
    .line 406637
    iget-object v0, p0, LX/2PX;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, LX/2PX;->a:Landroid/content/Context;

    const-class v3, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 406638
    return-void
.end method

.method public final init()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 406639
    iget-object v0, p0, LX/2PX;->b:LX/2PY;

    .line 406640
    iget-object v2, v0, LX/2PY;->a:LX/0ad;

    sget-short v3, LX/15r;->y:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 406641
    if-eqz v0, :cond_0

    move v0, v1

    .line 406642
    :goto_0
    iget-object v2, p0, LX/2PX;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, p0, LX/2PX;->a:Landroid/content/Context;

    const-class v5, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 406643
    return-void

    .line 406644
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
