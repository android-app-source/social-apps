.class public LX/2wE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qf;
.implements LX/1qg;


# instance fields
.field public final a:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:I

.field public c:LX/2wm;


# direct methods
.method public constructor <init>(LX/2vs;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2vs",
            "<*>;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/2wE;->a:LX/2vs;

    iput p2, p0, LX/2wE;->b:I

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, LX/2wE;->c:LX/2wm;

    const-string v1, "Callbacks must be attached to a GoogleApiClient instance before connecting the client."

    invoke-static {v0, v1}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    invoke-direct {p0}, LX/2wE;->a()V

    iget-object v0, p0, LX/2wE;->c:LX/2wm;

    invoke-virtual {v0, p1}, LX/2wm;->a(I)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, LX/2wE;->a()V

    iget-object v0, p0, LX/2wE;->c:LX/2wm;

    invoke-virtual {v0, p1}, LX/2wm;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, LX/2wE;->a()V

    iget-object v0, p0, LX/2wE;->c:LX/2wm;

    iget-object v1, p0, LX/2wE;->a:LX/2vs;

    iget v2, p0, LX/2wE;->b:I

    invoke-virtual {v0, p1, v1, v2}, LX/2wm;->a(Lcom/google/android/gms/common/ConnectionResult;LX/2vs;I)V

    return-void
.end method
