.class public final LX/2Xj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/2XA;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/2XA;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 420117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420118
    iput-object p1, p0, LX/2Xj;->a:LX/0QB;

    .line 420119
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2XA;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 420120
    new-instance v0, LX/2Xj;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Xj;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 420121
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 420122
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2Xj;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 420123
    packed-switch p2, :pswitch_data_0

    .line 420124
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 420125
    :pswitch_0
    new-instance p0, LX/2X9;

    invoke-static {p1}, LX/2VM;->b(LX/0QB;)LX/2VM;

    move-result-object v0

    check-cast v0, LX/2VM;

    const/16 v1, 0x15e7

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ae;

    invoke-direct {p0, v0, p2, v1}, LX/2X9;-><init>(LX/2VM;LX/0Or;LX/0ae;)V

    .line 420126
    move-object v0, p0

    .line 420127
    :goto_0
    return-object v0

    .line 420128
    :pswitch_1
    new-instance v1, LX/2ZG;

    const/16 v0, 0x17d

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v0, 0x196

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 v0, 0x17cc

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-static {p1}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v0

    check-cast v0, LX/01T;

    invoke-direct {v1, v2, p0, p2, v0}, LX/2ZG;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/01T;)V

    .line 420129
    move-object v0, v1

    .line 420130
    goto :goto_0

    .line 420131
    :pswitch_2
    invoke-static {p1}, LX/2ZJ;->a(LX/0QB;)LX/2ZJ;

    move-result-object v0

    goto :goto_0

    .line 420132
    :pswitch_3
    invoke-static {p1}, LX/2ZN;->b(LX/0QB;)LX/2ZN;

    move-result-object v0

    goto :goto_0

    .line 420133
    :pswitch_4
    invoke-static {p1}, LX/2ZP;->b(LX/0QB;)LX/2ZP;

    move-result-object v0

    goto :goto_0

    .line 420134
    :pswitch_5
    new-instance v2, LX/2Ya;

    const/16 v3, 0xdf5

    invoke-static {p1, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xdf4

    invoke-static {p1, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p1}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    const/16 v6, 0xb8b

    invoke-static {p1, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0xdf3

    invoke-static {p1, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x19e

    invoke-static {p1, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xbc

    invoke-static {p1, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x259

    invoke-static {p1, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xdf8

    invoke-static {p1, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v2 .. v11}, LX/2Ya;-><init>(LX/0Or;LX/0Or;LX/0Uh;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 420135
    move-object v0, v2

    .line 420136
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 420137
    const/4 v0, 0x6

    return v0
.end method
