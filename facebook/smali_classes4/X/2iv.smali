.class public LX/2iv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g0",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/2kM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:LX/0qs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qs",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 452227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452228
    return-void
.end method

.method public static a(LX/0QB;)LX/2iv;
    .locals 1

    .prologue
    .line 452224
    new-instance v0, LX/2iv;

    invoke-direct {v0}, LX/2iv;-><init>()V

    .line 452225
    move-object v0, v0

    .line 452226
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, -0x1

    .line 452218
    if-nez p1, :cond_1

    move v0, v1

    .line 452219
    :cond_0
    :goto_0
    return v0

    .line 452220
    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, LX/2iv;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 452221
    invoke-virtual {p0, v0}, LX/2iv;->b(I)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v0}, LX/2iv;->b(I)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v0}, LX/2iv;->b(I)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 452222
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 452223
    goto :goto_0
.end method

.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 452217
    invoke-virtual {p0, p1}, LX/2iv;->b(I)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 452229
    iget-object v0, p0, LX/2iv;->b:LX/0qs;

    if-eqz v0, :cond_0

    move v0, p1

    .line 452230
    :goto_0
    add-int v1, p1, p2

    if-ge v0, v1, :cond_0

    .line 452231
    iget-object v1, p0, LX/2iv;->b:LX/0qs;

    invoke-virtual {p0, v0}, LX/2iv;->b(I)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, LX/0qs;->a(ILjava/lang/Object;Z)V

    .line 452232
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 452233
    :cond_0
    return-void
.end method

.method public final a(LX/0qs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qs",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 452213
    iget-object v0, p0, LX/2iv;->b:LX/0qs;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 452214
    iput-object p1, p0, LX/2iv;->b:LX/0qs;

    .line 452215
    return-void

    .line 452216
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;
    .locals 1

    .prologue
    .line 452212
    iget-object v0, p0, LX/2iv;->a:LX/2kM;

    invoke-interface {v0, p1}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    return-object v0
.end method

.method public final b(II)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 452203
    iget-object v0, p0, LX/2iv;->b:LX/0qs;

    if-eqz v0, :cond_2

    .line 452204
    invoke-static {v7, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 452205
    invoke-virtual {p0}, LX/2iv;->size()I

    move-result v1

    add-int v2, p1, p2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 452206
    if-ne p1, v0, :cond_0

    add-int v2, p1, p2

    if-eq v2, v1, :cond_1

    .line 452207
    :cond_0
    const-string v2, "NotificationsConnectionCollection"

    const-string v3, "onEdgeChanged from %d to %d, but clamped to %d to %d"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    add-int v6, p1, p2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 452208
    :cond_1
    :goto_0
    if-ge v0, v1, :cond_2

    .line 452209
    iget-object v2, p0, LX/2iv;->b:LX/0qs;

    const/4 v3, 0x0

    invoke-virtual {p0, v0}, LX/2iv;->b(I)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v4

    invoke-interface {v2, v0, v3, v4, v7}, LX/0qs;->a(ILjava/lang/Object;Ljava/lang/Object;Z)V

    .line 452210
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 452211
    :cond_2
    return-void
.end method

.method public final b(LX/0qs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qs",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 452199
    iget-object v0, p0, LX/2iv;->b:LX/0qs;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 452200
    const/4 v0, 0x0

    iput-object v0, p0, LX/2iv;->b:LX/0qs;

    .line 452201
    return-void

    .line 452202
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(II)V
    .locals 4

    .prologue
    .line 452194
    iget-object v0, p0, LX/2iv;->b:LX/0qs;

    if-eqz v0, :cond_0

    move v0, p1

    .line 452195
    :goto_0
    add-int v1, p1, p2

    if-ge v0, v1, :cond_0

    .line 452196
    iget-object v1, p0, LX/2iv;->b:LX/0qs;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, p1, v2, v3}, LX/0qs;->b(ILjava/lang/Object;Z)V

    .line 452197
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 452198
    :cond_0
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 452193
    iget-object v0, p0, LX/2iv;->a:LX/2kM;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/2iv;->a:LX/2kM;

    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    goto :goto_0
.end method
