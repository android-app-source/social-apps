.class public LX/2Fk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/2Fk;


# instance fields
.field private final a:LX/0VT;

.field private final b:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;LX/0VT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 387255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387256
    iput-object p1, p0, LX/2Fk;->b:Landroid/content/pm/PackageManager;

    .line 387257
    iput-object p2, p0, LX/2Fk;->a:LX/0VT;

    .line 387258
    return-void
.end method

.method public static a(LX/0QB;)LX/2Fk;
    .locals 5

    .prologue
    .line 387259
    sget-object v0, LX/2Fk;->c:LX/2Fk;

    if-nez v0, :cond_1

    .line 387260
    const-class v1, LX/2Fk;

    monitor-enter v1

    .line 387261
    :try_start_0
    sget-object v0, LX/2Fk;->c:LX/2Fk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 387262
    if-eqz v2, :cond_0

    .line 387263
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 387264
    new-instance p0, LX/2Fk;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    invoke-static {v0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v4

    check-cast v4, LX/0VT;

    invoke-direct {p0, v3, v4}, LX/2Fk;-><init>(Landroid/content/pm/PackageManager;LX/0VT;)V

    .line 387265
    move-object v0, p0

    .line 387266
    sput-object v0, LX/2Fk;->c:LX/2Fk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387267
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 387268
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387269
    :cond_1
    sget-object v0, LX/2Fk;->c:LX/2Fk;

    return-object v0

    .line 387270
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 387271
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
