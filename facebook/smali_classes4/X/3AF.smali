.class public LX/3AF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1zC;

.field private final b:LX/1Uf;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/1zC;LX/1Uf;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 524949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524950
    iput-object p1, p0, LX/3AF;->a:LX/1zC;

    .line 524951
    iput-object p2, p0, LX/3AF;->b:LX/1Uf;

    .line 524952
    iput-object p3, p0, LX/3AF;->c:Landroid/content/res/Resources;

    .line 524953
    return-void
.end method

.method public static a(LX/0QB;)LX/3AF;
    .locals 6

    .prologue
    .line 524965
    const-class v1, LX/3AF;

    monitor-enter v1

    .line 524966
    :try_start_0
    sget-object v0, LX/3AF;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 524967
    sput-object v2, LX/3AF;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 524968
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524969
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 524970
    new-instance p0, LX/3AF;

    invoke-static {v0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v3

    check-cast v3, LX/1zC;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v4

    check-cast v4, LX/1Uf;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, LX/3AF;-><init>(LX/1zC;LX/1Uf;Landroid/content/res/Resources;)V

    .line 524971
    move-object v0, p0

    .line 524972
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 524973
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3AF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 524974
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 524975
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Landroid/text/SpannableStringBuilder;
    .locals 4

    .prologue
    .line 524959
    iget-object v0, p0, LX/3AF;->b:LX/1Uf;

    invoke-static {p1}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v0

    .line 524960
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 524961
    :goto_0
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 524962
    iget-object v0, p0, LX/3AF;->a:LX/1zC;

    iget-object v2, p0, LX/3AF;->c:Landroid/content/res/Resources;

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 524963
    return-object v1

    .line 524964
    :cond_1
    invoke-static {v0}, LX/1Uf;->a(Landroid/text/Spannable;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 524954
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 524955
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->n()Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v0

    .line 524956
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->SEE_CONVERSION:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    if-ne v0, v1, :cond_0

    .line 524957
    iget-object v0, p0, LX/3AF;->c:Landroid/content/res/Resources;

    const v1, 0x7f081114

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 524958
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3AF;->c:Landroid/content/res/Resources;

    const v1, 0x7f081113

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
