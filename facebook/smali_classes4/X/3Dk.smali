.class public final LX/3Dk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3DV;

.field private final b:Z

.field private final c:J

.field private final d:LX/3Di;

.field private e:LX/15D;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/15D",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3DV;ZJLX/3Di;)V
    .locals 1

    .prologue
    .line 535492
    iput-object p1, p0, LX/3Dk;->a:LX/3DV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 535493
    iput-boolean p2, p0, LX/3Dk;->b:Z

    .line 535494
    iput-wide p3, p0, LX/3Dk;->c:J

    .line 535495
    iput-object p5, p0, LX/3Dk;->d:LX/3Di;

    .line 535496
    return-void
.end method


# virtual methods
.method public final a(LX/15D;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 535497
    iput-object p1, p0, LX/3Dk;->e:LX/15D;

    .line 535498
    return-void
.end method

.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 535499
    const/4 v8, 0x0

    .line 535500
    iget-object v0, p0, LX/3Dk;->a:LX/3DV;

    invoke-static {v0, p1}, LX/3DV;->a$redex0(LX/3DV;Lorg/apache/http/HttpResponse;)V

    .line 535501
    :try_start_0
    invoke-static {p1}, LX/3DV;->c(Lorg/apache/http/HttpResponse;)LX/3Dd;

    move-result-object v1

    move-object v0, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 535502
    iget-object v1, p0, LX/3Dk;->d:LX/3Di;

    invoke-interface {v1, v0}, LX/3Di;->a(LX/3Dd;)Ljava/io/OutputStream;

    move-result-object v2

    .line 535503
    if-nez v2, :cond_0

    .line 535504
    :goto_0
    return-object v8

    .line 535505
    :catch_0
    move-exception v0

    .line 535506
    iget-object v1, p0, LX/3Dk;->d:LX/3Di;

    invoke-interface {v1, v0}, LX/3Di;->a(Ljava/io/IOException;)V

    goto :goto_0

    .line 535507
    :cond_0
    :try_start_1
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 535508
    iget-wide v4, p0, LX/3Dk;->c:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    .line 535509
    new-instance v0, LX/45Z;

    iget-wide v4, p0, LX/3Dk;->c:J

    const/4 v3, 0x0

    invoke-direct {v0, v1, v4, v5, v3}, LX/45Z;-><init>(Ljava/io/InputStream;JZ)V

    .line 535510
    :goto_1
    iget-object v1, p0, LX/3Dk;->a:LX/3DV;

    iget-object v1, v1, LX/3DV;->i:LX/1Lu;

    invoke-virtual {v1, v2}, LX/1Lu;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 535511
    :try_start_2
    new-instance v3, LX/7Ol;

    invoke-direct {v3, v0}, LX/7Ol;-><init>(Ljava/io/InputStream;)V

    invoke-static {v3, v1}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 535512
    :try_start_3
    iget-object v0, p0, LX/3Dk;->a:LX/3DV;

    iget-object v0, v0, LX/3DV;->i:LX/1Lu;

    invoke-virtual {v0, v1}, LX/1Lu;->b(Ljava/io/OutputStream;)V

    .line 535513
    iget-object v0, p0, LX/3Dk;->d:LX/3Di;

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, LX/3Di;->a(Ljava/io/OutputStream;Ljava/io/IOException;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 535514
    :catch_1
    move-exception v0

    .line 535515
    iget-object v1, p0, LX/3Dk;->a:LX/3DV;

    iget-object v1, v1, LX/3DV;->c:LX/1Lw;

    iget-object v3, p0, LX/3Dk;->e:LX/15D;

    invoke-virtual {v1, v3}, LX/1Lw;->c(LX/15D;)Z

    .line 535516
    iget-object v1, p0, LX/3Dk;->d:LX/3Di;

    invoke-interface {v1, v2, v0}, LX/3Di;->a(Ljava/io/OutputStream;Ljava/io/IOException;)V

    .line 535517
    new-instance v1, LX/7P3;

    invoke-direct {v1, p0, v0}, LX/7P3;-><init>(LX/3Dk;Ljava/io/IOException;)V

    throw v1

    .line 535518
    :catchall_0
    move-exception v0

    :try_start_4
    iget-object v3, p0, LX/3Dk;->a:LX/3DV;

    iget-object v3, v3, LX/3DV;->i:LX/1Lu;

    invoke-virtual {v3, v1}, LX/1Lu;->b(Ljava/io/OutputStream;)V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method
