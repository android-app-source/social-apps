.class public LX/29O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field public final a:LX/2W8;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Sg;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:LX/0TE;


# direct methods
.method public constructor <init>(LX/2W8;LX/0Ot;LX/0Sg;Ljava/util/concurrent/Executor;LX/0TE;)V
    .locals 0
    .param p1    # LX/2W8;
        .annotation runtime Lcom/facebook/common/noncriticalinit/AfterUILoadedOnBackgroundThread;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2W8;",
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "Ljava/util/concurrent/Executor;",
            "LX/0TE;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 376236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376237
    iput-object p1, p0, LX/29O;->a:LX/2W8;

    .line 376238
    iput-object p2, p0, LX/29O;->b:LX/0Ot;

    .line 376239
    iput-object p3, p0, LX/29O;->c:LX/0Sg;

    .line 376240
    iput-object p4, p0, LX/29O;->d:Ljava/util/concurrent/Executor;

    .line 376241
    iput-object p5, p0, LX/29O;->e:LX/0TE;

    .line 376242
    return-void
.end method

.method public static a(LX/29O;LX/0Up;)V
    .locals 6

    .prologue
    .line 376243
    :try_start_0
    invoke-interface {p1}, LX/0Up;->init()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 376244
    :goto_0
    invoke-static {p0}, LX/29O;->b(LX/29O;)V

    .line 376245
    return-void

    .line 376246
    :catch_0
    move-exception v0

    .line 376247
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 376248
    if-eqz v1, :cond_0

    .line 376249
    throw v0

    .line 376250
    :cond_0
    const-string v1, "NonCriticalInitializer"

    const-string v2, "INeedInit failed: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static b(LX/29O;)V
    .locals 5

    .prologue
    .line 376251
    iget-object v0, p0, LX/29O;->a:LX/2W8;

    invoke-interface {v0}, LX/2W8;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376252
    iget-object v0, p0, LX/29O;->c:LX/0Sg;

    const-string v1, "NonCriticalInitializer"

    new-instance v2, Lcom/facebook/common/noncriticalinit/NonCriticalInitializer$1;

    invoke-direct {v2, p0}, Lcom/facebook/common/noncriticalinit/NonCriticalInitializer$1;-><init>(LX/29O;)V

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY:LX/0VZ;

    sget-object v4, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    move-result-object v0

    .line 376253
    iget-object v1, p0, LX/29O;->e:LX/0TE;

    iget-object v2, p0, LX/29O;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 376254
    :goto_0
    return-void

    .line 376255
    :cond_0
    iget-object v0, p0, LX/29O;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uq;

    .line 376256
    :try_start_0
    invoke-virtual {v0}, LX/0Uq;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 376257
    :goto_1
    goto :goto_0

    .line 376258
    :catch_0
    move-exception v0

    .line 376259
    const-string v1, "NonCriticalInitializer"

    const-string v2, "AppInitLock failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public final init()V
    .locals 0

    .prologue
    .line 376260
    invoke-static {p0}, LX/29O;->b(LX/29O;)V

    .line 376261
    return-void
.end method
