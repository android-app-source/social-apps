.class public LX/2rf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 472105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472106
    return-void
.end method

.method public static a(LX/0jL;LX/0iq;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)LX/0jL;
    .locals 4
    .param p2    # Lcom/facebook/composer/attachments/ComposerAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0iq;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
            "<TMutation;>;:",
            "Lcom/facebook/composer/privacy/model/ComposerPrivacyData$SetsPrivacyData",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
            "<TMutation;>;>(TMutation;TModelData;",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            "LX/875;",
            ")TMutation;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 472089
    if-nez p2, :cond_0

    move-object v0, p0

    .line 472090
    check-cast v0, LX/0jL;

    new-instance v1, LX/7lP;

    invoke-interface {p1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v2

    invoke-direct {v1, v2}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 472091
    iput-boolean v3, v1, LX/7lP;->a:Z

    .line 472092
    move-object v1, v1

    .line 472093
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/7lP;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)LX/7lP;

    move-result-object v1

    .line 472094
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 472095
    iput-object v2, v1, LX/7lP;->g:LX/0Px;

    .line 472096
    move-object v1, v1

    .line 472097
    iput-boolean v3, v1, LX/7lP;->h:Z

    .line 472098
    move-object v1, v1

    .line 472099
    invoke-virtual {v1}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Ljava/lang/Object;

    .line 472100
    :cond_0
    check-cast p0, LX/0jL;

    .line 472101
    if-eqz p2, :cond_1

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    :goto_0
    move-object v0, v0

    .line 472102
    invoke-virtual {p0, v0}, LX/0jL;->c(LX/0Px;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    check-cast v0, LX/0jL;

    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setMediaSource(LX/875;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    return-object v0

    .line 472103
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 472104
    goto :goto_0
.end method

.method public static a(Lcom/facebook/composer/attachments/ComposerAttachment;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/facebook/composer/attachments/ComposerAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 472076
    if-nez p0, :cond_0

    .line 472077
    const/4 v0, 0x0

    .line 472078
    :goto_0
    return-object v0

    .line 472079
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 472080
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 472081
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/74n;Landroid/net/Uri;)Lcom/facebook/ipc/media/MediaItem;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 472088
    sget-object v0, LX/74j;->CREATIVECAM_MEDIA:LX/74j;

    invoke-virtual {p0, p1, v0}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0io;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0io;",
            ">(TModelData;)Z"
        }
    .end annotation

    .prologue
    .line 472086
    invoke-static {p0}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    .line 472087
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0io;",
            ">(TModelData;)",
            "Lcom/facebook/composer/attachments/ComposerAttachment;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 472083
    invoke-interface {p0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-gt v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 472084
    invoke-interface {p0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    return-object v0

    .line 472085
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/0io;)Landroid/net/Uri;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0io;",
            ">(TModelData;)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 472082
    invoke-static {p0}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    invoke-static {v0}, LX/2rf;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
