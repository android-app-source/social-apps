.class public final LX/29N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/0ra;


# direct methods
.method public constructor <init>(LX/0ra;)V
    .locals 0

    .prologue
    .line 376218
    iput-object p1, p0, LX/29N;->a:LX/0ra;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x55eedd2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 376219
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 376220
    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 376221
    iget-object v1, p0, LX/29N;->a:LX/0ra;

    .line 376222
    invoke-static {v1}, LX/0ra;->f(LX/0ra;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 376223
    invoke-static {v1}, LX/0ra;->e(LX/0ra;)V

    .line 376224
    sget-object v2, LX/32G;->OnAppBackgrounded:LX/32G;

    invoke-static {v1, v2}, LX/0ra;->b(LX/0ra;LX/32G;)V

    .line 376225
    :cond_0
    :goto_0
    const v1, 0x509f09f

    invoke-static {v1, v0}, LX/02F;->e(II)V

    return-void

    .line 376226
    :cond_1
    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 376227
    iget-object v1, p0, LX/29N;->a:LX/0ra;

    .line 376228
    iget-object v2, v1, LX/0ra;->g:LX/0ad;

    sget-short p0, LX/2sh;->a:S

    const/4 p1, 0x0

    invoke-interface {v2, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 376229
    sget-object v2, LX/1ac;->a:Ljava/util/Set;

    move-object v2, v2

    .line 376230
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1aX;

    .line 376231
    iget-object p1, v2, LX/1aX;->g:LX/1ab;

    sget-object v1, LX/1at;->ON_HOLDER_UNTRIM:LX/1at;

    invoke-virtual {p1, v1}, LX/1ab;->a(LX/1at;)V

    .line 376232
    const/4 p1, 0x0

    iput-boolean p1, v2, LX/1aX;->d:Z

    .line 376233
    invoke-static {v2}, LX/1aX;->n(LX/1aX;)V

    .line 376234
    goto :goto_1

    .line 376235
    :cond_2
    goto :goto_0
.end method
