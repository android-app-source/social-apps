.class public final LX/2rB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AF6;

.field public final synthetic b:LX/AFg;


# direct methods
.method public constructor <init>(LX/AFg;LX/AF6;)V
    .locals 0

    .prologue
    .line 471736
    iput-object p1, p0, LX/2rB;->b:LX/AFg;

    iput-object p2, p0, LX/2rB;->a:LX/AF6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 471737
    sget-object v0, LX/AFg;->a:Ljava/lang/String;

    const-string v1, "failed to get direct share threads"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 471738
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 471739
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 471740
    iget-object v0, p0, LX/2rB;->a:LX/AF6;

    if-nez v0, :cond_0

    .line 471741
    :goto_0
    return-void

    .line 471742
    :cond_0
    if-nez p1, :cond_1

    .line 471743
    iget-object v0, p0, LX/2rB;->a:LX/AF6;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/AF6;->a(LX/0Px;)V

    goto :goto_0

    .line 471744
    :cond_1
    const/4 v2, 0x0

    .line 471745
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 471746
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    .line 471747
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 471748
    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    .line 471749
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 471750
    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    .line 471751
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 471752
    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v5

    .line 471753
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 471754
    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->j()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    .line 471755
    :goto_1
    if-ge v3, v7, :cond_4

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;

    .line 471756
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 471757
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v8

    .line 471758
    invoke-virtual {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;->k()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v1, v2

    .line 471759
    :goto_2
    if-ge v1, v10, :cond_3

    invoke-virtual {v9, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;

    .line 471760
    new-instance v11, LX/AGr;

    invoke-direct {v11, v0, v8, v5}, LX/AGr;-><init>(Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel$ThreadsModel$EdgesModel;Lcom/facebook/audience/model/AudienceControlData;Lcom/facebook/audience/model/AudienceControlData;)V

    .line 471761
    invoke-virtual {v11}, LX/AGp;->a()LX/7h0;

    move-result-object v0

    .line 471762
    if-eqz v0, :cond_2

    .line 471763
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 471764
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 471765
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 471766
    :cond_4
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 471767
    iget-object v1, p0, LX/2rB;->a:LX/AF6;

    invoke-virtual {v1, v0}, LX/AF6;->a(LX/0Px;)V

    goto/16 :goto_0
.end method
