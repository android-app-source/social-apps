.class public LX/35M;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/35M",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 497044
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 497045
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/35M;->b:LX/0Zi;

    .line 497046
    iput-object p1, p0, LX/35M;->a:LX/0Ot;

    .line 497047
    return-void
.end method

.method public static a(LX/0QB;)LX/35M;
    .locals 4

    .prologue
    .line 497048
    const-class v1, LX/35M;

    monitor-enter v1

    .line 497049
    :try_start_0
    sget-object v0, LX/35M;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 497050
    sput-object v2, LX/35M;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 497051
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497052
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 497053
    new-instance v3, LX/35M;

    const/16 p0, 0x915

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/35M;-><init>(LX/0Ot;)V

    .line 497054
    move-object v0, v3

    .line 497055
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 497056
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/35M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497057
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 497058
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 497059
    const v0, -0x749af1cb

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 497060
    check-cast p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;

    .line 497061
    iget-object v0, p0, LX/35M;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;

    iget-object v1, p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v5, 0x2

    .line 497062
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    .line 497063
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 497064
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    .line 497065
    const/4 v6, 0x0

    .line 497066
    if-eqz v4, :cond_1

    .line 497067
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 497068
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 497069
    :goto_0
    iget-object v6, v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->e:LX/1nu;

    invoke-virtual {v6, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v4

    const v6, 0x7f02111f

    invoke-virtual {v4, v6}, LX/1nw;->h(I)LX/1nw;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v6, 0x7f0b08f9

    invoke-interface {v4, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    const v6, 0x7f0b08f9

    invoke-interface {v4, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const/4 v6, 0x5

    const p0, 0x7f0b00d4

    invoke-interface {v4, v6, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 497070
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b08f9

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020d83

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x4

    const v6, 0x7f0b00d4

    invoke-interface {v4, v5, v6}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v4

    .line 497071
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    const v6, 0x7f080ffc

    invoke-virtual {v5, v6}, LX/1ne;->h(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004e

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x1010212

    invoke-virtual {v5, v6}, LX/1ne;->o(I)LX/1ne;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x4

    const p0, 0x7f0b00d5

    invoke-interface {v5, v6, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x5

    const p0, 0x7f0b1073

    invoke-interface {v5, v6, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    move-object v5, v5

    .line 497072
    invoke-interface {v5, v7}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 497073
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 497074
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 497075
    iget-object v6, v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->i:Lcom/facebook/feedback/ui/CommentComposerHelper;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/facebook/feedback/ui/CommentComposerHelper;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 497076
    const/4 v5, 0x0

    .line 497077
    :goto_1
    move-object v5, v5

    .line 497078
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 497079
    const v4, -0x749af1cb

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 497080
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 497081
    return-object v0

    .line 497082
    :cond_0
    iget-object v4, v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03V;

    const-class p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    const-string p2, "User acting as Page but no profile picture available!"

    invoke-virtual {v4, p0, p2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v6

    goto/16 :goto_0

    .line 497083
    :cond_1
    iget-object v4, v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->g:LX/0WJ;

    invoke-virtual {v4}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v4

    .line 497084
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 497085
    invoke-virtual {v4}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_0

    :cond_2
    move-object v4, v6

    goto/16 :goto_0

    .line 497086
    :cond_3
    iget-object v5, v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->f:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    const v6, 0x7f0207b4

    invoke-virtual {v5, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v5

    const v6, 0x7f0a00e6

    invoke-virtual {v5, v6}, LX/2xv;->j(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    .line 497087
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v5

    sget-object v6, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b1074

    invoke-interface {v5, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b1074

    invoke-interface {v5, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v5

    const/4 v6, 0x5

    const v7, 0x7f0b1075

    invoke-interface {v5, v6, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 497088
    invoke-static {}, LX/1dS;->b()V

    .line 497089
    iget v0, p1, LX/1dQ;->b:I

    .line 497090
    packed-switch v0, :pswitch_data_0

    .line 497091
    :goto_0
    return-object v2

    .line 497092
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 497093
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 497094
    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;

    .line 497095
    iget-object v3, p0, LX/35M;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;

    iget-object v4, v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->c:LX/1Pn;

    .line 497096
    iget-object p2, v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->c:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/1EN;

    check-cast p1, LX/1Po;

    invoke-interface {p1}, LX/1Po;->c()LX/1PT;

    move-result-object p0

    sget-object v1, LX/An0;->INLINE_COMMENT_COMPOSER:LX/An0;

    invoke-virtual {p2, v4, p0, v0, v1}, LX/1EN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Landroid/view/View;LX/An0;)V

    .line 497097
    iget-object p2, v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponentSpec;->h:LX/1Ve;

    invoke-virtual {p2, v4}, LX/1Ve;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 497098
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x749af1cb
        :pswitch_0
    .end packed-switch
.end method
