.class public final LX/2th;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/QuicksilverActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/QuicksilverActivity;)V
    .locals 0

    .prologue
    .line 475144
    iput-object p1, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 475159
    iget-object v0, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v1, LX/8TE;->GAME_INFO_FETCH:LX/8TE;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 475160
    iget-object v0, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v1, LX/8TE;->FUNNEL_GAME_INFO_QUERY_SUCCESS:LX/8TE;

    invoke-virtual {v0, v1}, LX/8TD;->b(LX/8TE;)V

    .line 475161
    iget-object v0, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    if-eqz v0, :cond_0

    .line 475162
    iget-object v0, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/QuicksilverFragment;->b()V

    .line 475163
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 475151
    iget-object v0, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-static {v0}, Lcom/facebook/quicksilver/QuicksilverActivity;->r(Lcom/facebook/quicksilver/QuicksilverActivity;)LX/8Tb;

    move-result-object v0

    .line 475152
    iget-object v1, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-static {v1}, Lcom/facebook/quicksilver/QuicksilverActivity;->s(Lcom/facebook/quicksilver/QuicksilverActivity;)V

    .line 475153
    iget-object v1, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    iget-object v1, v1, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    if-eqz v1, :cond_0

    .line 475154
    iget-object v1, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    iget-object v1, v1, Lcom/facebook/quicksilver/QuicksilverActivity;->q:Lcom/facebook/quicksilver/QuicksilverFragment;

    invoke-virtual {v1}, Lcom/facebook/quicksilver/QuicksilverFragment;->c()V

    .line 475155
    :cond_0
    iget-object v1, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    iget-object v1, v1, Lcom/facebook/quicksilver/QuicksilverActivity;->A:LX/8TS;

    invoke-virtual {v1, v0}, LX/8TS;->a(LX/8Tb;)LX/8TS;

    .line 475156
    iget-object v0, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-static {v0, p1}, Lcom/facebook/quicksilver/QuicksilverActivity;->b(Lcom/facebook/quicksilver/QuicksilverActivity;Ljava/lang/String;)V

    .line 475157
    iget-object v0, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v1, LX/8TE;->FUNNEL_GAME_INFO_QUERY_START:LX/8TE;

    invoke-virtual {v0, v1}, LX/8TD;->b(LX/8TE;)V

    .line 475158
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 475147
    iget-object v0, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v1, LX/8TE;->GAME_INFO_FETCH:LX/8TE;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 475148
    iget-object v0, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8TD;

    sget-object v1, LX/8TE;->FUNNEL_GAME_INFO_QUERY_FAILURE:LX/8TE;

    invoke-virtual {v0, v1}, LX/8TD;->b(LX/8TE;)V

    .line 475149
    iget-object v0, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    iget-object v0, v0, Lcom/facebook/quicksilver/QuicksilverActivity;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8T9;

    sget-object v1, LX/8Tp;->GAME_INFO_FETCH_FAILURE:LX/8Tp;

    invoke-virtual {v0, v1}, LX/8T9;->a(LX/8Tp;)V

    .line 475150
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Vb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 475145
    iget-object v0, p0, LX/2th;->a:Lcom/facebook/quicksilver/QuicksilverActivity;

    new-instance v1, Lcom/facebook/quicksilver/QuicksilverActivity$1$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/quicksilver/QuicksilverActivity$1$1;-><init>(LX/2th;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/QuicksilverActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 475146
    return-void
.end method
