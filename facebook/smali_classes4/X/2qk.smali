.class public final enum LX/2qk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/2qk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/2qk;

.field public static final enum EXTERNAL:LX/2qk;

.field public static final enum FROM_BIND:LX/2qk;

.field public static final enum FROM_DESTROY_SURFACE:LX/2qk;

.field public static final enum FROM_ERROR:LX/2qk;

.field public static final enum FROM_INLINE_TO_FULLSCREEN_TRANSITION:LX/2qk;

.field public static final enum FROM_ONCOMPLETE:LX/2qk;

.field public static final enum FROM_PREPARE:LX/2qk;

.field public static final enum FROM_RESET:LX/2qk;

.field public static final enum FROM_SET_VIDEO_RESOLUTION:LX/2qk;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 471405
    new-instance v0, LX/2qk;

    const-string v1, "EXTERNAL"

    const-string v2, "external"

    invoke-direct {v0, v1, v4, v2}, LX/2qk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qk;->EXTERNAL:LX/2qk;

    .line 471406
    new-instance v0, LX/2qk;

    const-string v1, "FROM_BIND"

    const-string v2, "from_bind"

    invoke-direct {v0, v1, v5, v2}, LX/2qk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qk;->FROM_BIND:LX/2qk;

    .line 471407
    new-instance v0, LX/2qk;

    const-string v1, "FROM_ONCOMPLETE"

    const-string v2, "from_oncomplete"

    invoke-direct {v0, v1, v6, v2}, LX/2qk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qk;->FROM_ONCOMPLETE:LX/2qk;

    .line 471408
    new-instance v0, LX/2qk;

    const-string v1, "FROM_ERROR"

    const-string v2, "from_error"

    invoke-direct {v0, v1, v7, v2}, LX/2qk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qk;->FROM_ERROR:LX/2qk;

    .line 471409
    new-instance v0, LX/2qk;

    const-string v1, "FROM_DESTROY_SURFACE"

    const-string v2, "from_surface_destroy"

    invoke-direct {v0, v1, v8, v2}, LX/2qk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qk;->FROM_DESTROY_SURFACE:LX/2qk;

    .line 471410
    new-instance v0, LX/2qk;

    const-string v1, "FROM_RESET"

    const/4 v2, 0x5

    const-string v3, "from_reset"

    invoke-direct {v0, v1, v2, v3}, LX/2qk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qk;->FROM_RESET:LX/2qk;

    .line 471411
    new-instance v0, LX/2qk;

    const-string v1, "FROM_PREPARE"

    const/4 v2, 0x6

    const-string v3, "from_prepare"

    invoke-direct {v0, v1, v2, v3}, LX/2qk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qk;->FROM_PREPARE:LX/2qk;

    .line 471412
    new-instance v0, LX/2qk;

    const-string v1, "FROM_SET_VIDEO_RESOLUTION"

    const/4 v2, 0x7

    const-string v3, "from_set_video_resolution"

    invoke-direct {v0, v1, v2, v3}, LX/2qk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qk;->FROM_SET_VIDEO_RESOLUTION:LX/2qk;

    .line 471413
    new-instance v0, LX/2qk;

    const-string v1, "FROM_INLINE_TO_FULLSCREEN_TRANSITION"

    const/16 v2, 0x8

    const-string v3, "inline_to_fullscreen_transition"

    invoke-direct {v0, v1, v2, v3}, LX/2qk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/2qk;->FROM_INLINE_TO_FULLSCREEN_TRANSITION:LX/2qk;

    .line 471414
    const/16 v0, 0x9

    new-array v0, v0, [LX/2qk;

    sget-object v1, LX/2qk;->EXTERNAL:LX/2qk;

    aput-object v1, v0, v4

    sget-object v1, LX/2qk;->FROM_BIND:LX/2qk;

    aput-object v1, v0, v5

    sget-object v1, LX/2qk;->FROM_ONCOMPLETE:LX/2qk;

    aput-object v1, v0, v6

    sget-object v1, LX/2qk;->FROM_ERROR:LX/2qk;

    aput-object v1, v0, v7

    sget-object v1, LX/2qk;->FROM_DESTROY_SURFACE:LX/2qk;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/2qk;->FROM_RESET:LX/2qk;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/2qk;->FROM_PREPARE:LX/2qk;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/2qk;->FROM_SET_VIDEO_RESOLUTION:LX/2qk;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/2qk;->FROM_INLINE_TO_FULLSCREEN_TRANSITION:LX/2qk;

    aput-object v2, v0, v1

    sput-object v0, LX/2qk;->$VALUES:[LX/2qk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 471415
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 471416
    iput-object p3, p0, LX/2qk;->value:Ljava/lang/String;

    .line 471417
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/2qk;
    .locals 1

    .prologue
    .line 471418
    const-class v0, LX/2qk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2qk;

    return-object v0
.end method

.method public static values()[LX/2qk;
    .locals 1

    .prologue
    .line 471419
    sget-object v0, LX/2qk;->$VALUES:[LX/2qk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/2qk;

    return-object v0
.end method
