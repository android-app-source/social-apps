.class public LX/3Ct;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 530902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530903
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ct;
    .locals 4

    .prologue
    .line 530904
    const-class v1, LX/3Ct;

    monitor-enter v1

    .line 530905
    :try_start_0
    sget-object v0, LX/3Ct;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 530906
    sput-object v2, LX/3Ct;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 530907
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530908
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 530909
    new-instance p0, LX/3Ct;

    invoke-direct {p0}, LX/3Ct;-><init>()V

    .line 530910
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    .line 530911
    iput-object v3, p0, LX/3Ct;->a:LX/0ad;

    .line 530912
    move-object v0, p0

    .line 530913
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 530914
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3Ct;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530915
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 530916
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
