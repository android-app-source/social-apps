.class public abstract LX/3G3;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:J


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:J

.field public final e:J

.field public final f:I

.field public final g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 540392
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x7

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/3G3;->a:J

    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;Ljava/lang/String;IJ)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 540370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 540371
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540372
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p4

    .line 540373
    :cond_0
    iput p1, p0, LX/3G3;->f:I

    .line 540374
    iput-wide p2, p0, LX/3G3;->e:J

    .line 540375
    move-object v0, p4

    .line 540376
    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/3G3;->b:Ljava/lang/String;

    .line 540377
    iput-object p5, p0, LX/3G3;->c:Ljava/lang/String;

    .line 540378
    iput p6, p0, LX/3G3;->g:I

    .line 540379
    iput-wide p7, p0, LX/3G3;->d:J

    .line 540380
    return-void
.end method


# virtual methods
.method public final a(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 540388
    iget-wide v2, p0, LX/3G3;->e:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    .line 540389
    :cond_0
    :goto_0
    return v0

    .line 540390
    :cond_1
    iget-wide v2, p0, LX/3G3;->d:J

    iget-wide v4, p0, LX/3G3;->e:J

    add-long/2addr v2, v4

    .line 540391
    cmp-long v1, p1, v2

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 540387
    iget-wide v0, p0, LX/3G3;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 540382
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 540383
    :goto_0
    return v0

    .line 540384
    :cond_0
    instance-of v0, p1, LX/3G3;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 540385
    :cond_1
    check-cast p1, LX/3G3;

    .line 540386
    iget-object v0, p0, LX/3G3;->b:Ljava/lang/String;

    iget-object v1, p1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 540381
    iget-object v0, p0, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
