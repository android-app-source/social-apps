.class public LX/34c;
.super LX/1OM;
.source ""

# interfaces
.implements Landroid/view/Menu;
.implements LX/34d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Landroid/view/Menu;",
        "LX/34d;"
    }
.end annotation


# instance fields
.field public a:LX/5OA;

.field public b:LX/3Ah;

.field public c:Landroid/content/Context;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Landroid/content/res/ColorStateList;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 495505
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 495506
    iput-boolean v0, p0, LX/34c;->e:Z

    .line 495507
    iput-boolean v0, p0, LX/34c;->g:Z

    .line 495508
    iput-object p1, p0, LX/34c;->c:Landroid/content/Context;

    .line 495509
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/34c;->d:Ljava/util/List;

    .line 495510
    return-void
.end method

.method private d(Landroid/view/MenuItem;)Landroid/view/SubMenu;
    .locals 2

    .prologue
    .line 495471
    instance-of v0, p1, LX/3Ai;

    if-eqz v0, :cond_0

    .line 495472
    new-instance v0, LX/5OC;

    iget-object v1, p0, LX/34c;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/5OC;-><init>(Landroid/content/Context;)V

    .line 495473
    iput-object p0, v0, LX/5OC;->d:LX/34c;

    .line 495474
    iput-object p1, v0, LX/5OC;->c:Landroid/view/MenuItem;

    .line 495475
    iget-object v1, p0, LX/34c;->a:LX/5OA;

    invoke-virtual {v0, v1}, LX/34c;->a(LX/5OA;)V

    .line 495476
    iget-object v1, p0, LX/34c;->b:LX/3Ah;

    invoke-virtual {v0, v1}, LX/34c;->a(LX/3Ah;)V

    .line 495477
    check-cast p1, LX/3Ai;

    .line 495478
    iput-object v0, p1, LX/3Ai;->n:Landroid/view/SubMenu;

    .line 495479
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 495511
    invoke-virtual {p0, p1}, LX/34c;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 495512
    new-instance v0, LX/5OD;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5OD;-><init>(Landroid/content/Context;)V

    .line 495513
    new-instance v1, LX/5OB;

    invoke-direct {v1, v0}, LX/5OB;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(III)LX/3Ai;
    .locals 1

    .prologue
    .line 495514
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, p1, p2, p3}, LX/3Ai;-><init>(Landroid/view/Menu;III)V

    .line 495515
    invoke-virtual {p0, v0}, LX/34c;->c(Landroid/view/MenuItem;)V

    .line 495516
    return-object v0
.end method

.method public final a(IILjava/lang/CharSequence;)LX/3Ai;
    .locals 1

    .prologue
    .line 495517
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, p1, p2, p3}, LX/3Ai;-><init>(Landroid/view/Menu;IILjava/lang/CharSequence;)V

    .line 495518
    invoke-virtual {p0, v0}, LX/34c;->c(Landroid/view/MenuItem;)V

    .line 495519
    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/3Ai;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 495520
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, v1, v1, p1}, LX/3Ai;-><init>(Landroid/view/Menu;IILjava/lang/CharSequence;)V

    .line 495521
    invoke-virtual {p0, v0}, LX/34c;->c(Landroid/view/MenuItem;)V

    .line 495522
    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)LX/3Ai;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 495523
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, v1, v1, p1}, LX/3Ai;-><init>(Landroid/view/Menu;IILjava/lang/CharSequence;)V

    .line 495524
    invoke-virtual {v0, p2}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 495525
    invoke-virtual {p0, v0}, LX/34c;->c(Landroid/view/MenuItem;)V

    .line 495526
    return-object v0
.end method

.method public a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 495527
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, LX/5OD;

    .line 495528
    invoke-virtual {p0, p2}, LX/34c;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5OD;->a(Landroid/view/MenuItem;)V

    .line 495529
    iget-boolean v1, p0, LX/34c;->e:Z

    invoke-virtual {v0, v1}, LX/5OD;->a(Z)V

    .line 495530
    iget-boolean v1, p0, LX/34c;->g:Z

    if-eqz v1, :cond_0

    .line 495531
    iget-object v1, p0, LX/34c;->f:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, LX/5OD;->a(Landroid/content/res/ColorStateList;)V

    .line 495532
    :cond_0
    return-void
.end method

.method public final a(LX/3Ah;)V
    .locals 3

    .prologue
    .line 495533
    iget-object v0, p0, LX/34c;->b:LX/3Ah;

    if-eq v0, p1, :cond_1

    .line 495534
    iput-object p1, p0, LX/34c;->b:LX/3Ah;

    .line 495535
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 495536
    invoke-interface {v0}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 495537
    invoke-interface {v0}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, LX/34c;

    .line 495538
    iget-object v2, p0, LX/34c;->b:LX/3Ah;

    invoke-virtual {v0, v2}, LX/34c;->a(LX/3Ah;)V

    goto :goto_0

    .line 495539
    :cond_1
    return-void
.end method

.method public final a(LX/5OA;)V
    .locals 3

    .prologue
    .line 495540
    iget-object v0, p0, LX/34c;->a:LX/5OA;

    if-eq v0, p1, :cond_1

    .line 495541
    iput-object p1, p0, LX/34c;->a:LX/5OA;

    .line 495542
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 495543
    invoke-interface {v0}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 495544
    invoke-interface {v0}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, LX/34c;

    .line 495545
    iget-object v2, p0, LX/34c;->a:LX/5OA;

    invoke-virtual {v0, v2}, LX/34c;->a(LX/5OA;)V

    goto :goto_0

    .line 495546
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)V
    .locals 1

    .prologue
    .line 495567
    instance-of v0, p1, LX/3Ai;

    if-nez v0, :cond_2

    .line 495568
    iget-object v0, p0, LX/34c;->a:LX/5OA;

    if-eqz v0, :cond_0

    .line 495569
    iget-object v0, p0, LX/34c;->a:LX/5OA;

    invoke-interface {v0, p1}, LX/5OA;->a(Landroid/view/MenuItem;)Z

    .line 495570
    :cond_0
    invoke-virtual {p0}, LX/34c;->close()V

    .line 495571
    :cond_1
    :goto_0
    return-void

    .line 495572
    :cond_2
    check-cast p1, LX/3Ai;

    .line 495573
    invoke-virtual {p1}, LX/3Ai;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 495574
    invoke-virtual {p1}, LX/3Ai;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 495575
    invoke-virtual {p1}, LX/3Ai;->hasSubMenu()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 495576
    invoke-virtual {p1}, LX/3Ai;->getSubMenu()Landroid/view/SubMenu;

    .line 495577
    iget-object v0, p0, LX/34c;->b:LX/3Ah;

    if-eqz v0, :cond_1

    .line 495578
    iget-object v0, p0, LX/34c;->b:LX/3Ah;

    invoke-interface {v0}, LX/3Ah;->a()V

    goto :goto_0

    .line 495579
    :cond_3
    iget-object v0, p0, LX/34c;->a:LX/5OA;

    if-eqz v0, :cond_4

    .line 495580
    iget-object v0, p0, LX/34c;->a:LX/5OA;

    invoke-interface {v0, p1}, LX/5OA;->a(Landroid/view/MenuItem;)Z

    .line 495581
    :cond_4
    invoke-virtual {p0}, LX/34c;->close()V

    goto :goto_0
.end method

.method public final synthetic add(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 495547
    invoke-virtual {p0, p1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic add(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 495566
    invoke-virtual {p0, p2, p3, p4}, LX/34c;->a(III)LX/3Ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 495565
    invoke-virtual {p0, p2, p3, p4}, LX/34c;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 495564
    invoke-virtual {p0, p1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 1

    .prologue
    .line 495563
    const/4 v0, 0x0

    return v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 495561
    invoke-virtual {p0, p1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    .line 495562
    invoke-direct {p0, v0}, LX/34c;->d(Landroid/view/MenuItem;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 495559
    invoke-virtual {p0, p2, p3, p4}, LX/34c;->a(III)LX/3Ai;

    move-result-object v0

    .line 495560
    invoke-direct {p0, v0}, LX/34c;->d(Landroid/view/MenuItem;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 495557
    invoke-virtual {p0, p2, p3, p4}, LX/34c;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 495558
    invoke-direct {p0, v0}, LX/34c;->d(Landroid/view/MenuItem;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 495503
    invoke-virtual {p0, p1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 495504
    invoke-direct {p0, v0}, LX/34c;->d(Landroid/view/MenuItem;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/view/MenuItem;)I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 495548
    invoke-interface {p1}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    if-nez v0, :cond_1

    move v1, v2

    .line 495549
    :cond_0
    :goto_0
    return v1

    .line 495550
    :cond_1
    const/4 v0, 0x0

    .line 495551
    iget-object v1, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 495552
    if-eq v0, p1, :cond_0

    .line 495553
    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 495554
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 495555
    goto :goto_1

    :cond_2
    move v1, v2

    .line 495556
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final c(Landroid/view/MenuItem;)V
    .locals 4

    .prologue
    .line 495421
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495422
    :goto_0
    return-void

    .line 495423
    :cond_0
    const/4 v0, 0x0

    .line 495424
    iget-object v1, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 495425
    invoke-interface {v0}, Landroid/view/MenuItem;->getOrder()I

    move-result v0

    invoke-interface {p1}, Landroid/view/MenuItem;->getOrder()I

    move-result v3

    if-le v0, v3, :cond_1

    .line 495426
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 495427
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0

    .line 495428
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 495429
    goto :goto_1

    .line 495430
    :cond_2
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 495431
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 495432
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 495433
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 495434
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 495435
    iget-object v0, p0, LX/34c;->b:LX/3Ah;

    if-eqz v0, :cond_0

    .line 495436
    iget-object v0, p0, LX/34c;->b:LX/3Ah;

    invoke-interface {v0}, LX/3Ah;->b()V

    .line 495437
    :cond_0
    return-void
.end method

.method public e()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 495438
    iget-object v1, p0, LX/34c;->d:Ljava/util/List;

    if-nez v1, :cond_0

    .line 495439
    :goto_0
    return v0

    .line 495440
    :cond_0
    iget-object v1, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 495441
    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 495442
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 495443
    goto :goto_1

    :cond_1
    move v0, v1

    .line 495444
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final e(I)LX/3Ai;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 495445
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, v1, v1, p1}, LX/3Ai;-><init>(Landroid/view/Menu;III)V

    .line 495446
    invoke-virtual {p0, v0}, LX/34c;->c(Landroid/view/MenuItem;)V

    .line 495447
    return-object v0
.end method

.method public final e(II)LX/3Ai;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 495448
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, v1, v1, p1}, LX/3Ai;-><init>(Landroid/view/Menu;III)V

    .line 495449
    invoke-virtual {v0, p2}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 495450
    invoke-virtual {p0, v0}, LX/34c;->c(Landroid/view/MenuItem;)V

    .line 495451
    return-object v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 495452
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 495453
    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .locals 3

    .prologue
    .line 495454
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 495455
    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 495456
    :goto_0
    return-object v0

    .line 495457
    :cond_1
    invoke-interface {v0}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 495458
    invoke-interface {v0}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    .line 495459
    invoke-interface {v0, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 495460
    if-eqz v0, :cond_0

    goto :goto_0

    .line 495461
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .locals 3

    .prologue
    .line 495462
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 495463
    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 495464
    add-int/lit8 p1, p1, -0x1

    .line 495465
    :cond_1
    if-gez p1, :cond_0

    .line 495466
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasVisibleItems()Z
    .locals 2

    .prologue
    .line 495467
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 495468
    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495469
    const/4 v0, 0x1

    .line 495470
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ij_()I
    .locals 1

    .prologue
    .line 495420
    invoke-virtual {p0}, LX/34c;->e()I

    move-result v0

    return v0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 495480
    const/4 v0, 0x0

    return v0
.end method

.method public final performIdentifierAction(II)Z
    .locals 2

    .prologue
    .line 495481
    invoke-virtual {p0, p1}, LX/34c;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 495482
    instance-of v1, v0, LX/3Ai;

    if-eqz v1, :cond_0

    .line 495483
    check-cast v0, LX/3Ai;

    invoke-virtual {v0}, LX/3Ai;->a()Z

    move-result v0

    .line 495484
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1

    .prologue
    .line 495485
    const/4 v0, 0x0

    return v0
.end method

.method public final removeGroup(I)V
    .locals 0

    .prologue
    .line 495486
    return-void
.end method

.method public final removeItem(I)V
    .locals 4

    .prologue
    .line 495487
    const/4 v0, 0x0

    .line 495488
    iget-object v1, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 495489
    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_1

    move-object v1, v0

    .line 495490
    goto :goto_0

    .line 495491
    :cond_1
    invoke-interface {v0}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 495492
    invoke-interface {v0}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    .line 495493
    invoke-interface {v0, p1}, Landroid/view/SubMenu;->removeItem(I)V

    goto :goto_0

    .line 495494
    :cond_2
    if-eqz v1, :cond_3

    .line 495495
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 495496
    :cond_3
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 495497
    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .locals 0

    .prologue
    .line 495498
    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .locals 0

    .prologue
    .line 495499
    return-void
.end method

.method public final setGroupVisible(IZ)V
    .locals 0

    .prologue
    .line 495500
    return-void
.end method

.method public final setQwertyMode(Z)V
    .locals 0

    .prologue
    .line 495501
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 495502
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/34c;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
