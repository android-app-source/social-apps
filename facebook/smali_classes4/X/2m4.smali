.class public LX/2m4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PARAMS:",
        "Ljava/lang/Object;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1qM;"
    }
.end annotation


# static fields
.field public static final PARCELABLE_RESULT_CONVERTER:LX/2m8;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2m8",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field public static final STRING_RESULT_CONVERTER:LX/2m8;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2m8",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final VOID_PARAM_GETTER:LX/2m6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2m6",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public static final VOID_RESULT_CONVERTER:LX/2m8;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2m8",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mApiMethod:LX/0e6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end field

.field private final mParamGetter:LX/2m6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2m6",
            "<+TPARAMS;>;"
        }
    .end annotation
.end field

.field private final mResultConverter:LX/2m8;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2m8",
            "<-TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end field

.field private final mSingleMethodRunner:LX/11H;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 459310
    new-instance v0, LX/2m5;

    invoke-direct {v0}, LX/2m5;-><init>()V

    sput-object v0, LX/2m4;->VOID_PARAM_GETTER:LX/2m6;

    .line 459311
    new-instance v0, LX/2m7;

    invoke-direct {v0}, LX/2m7;-><init>()V

    sput-object v0, LX/2m4;->VOID_RESULT_CONVERTER:LX/2m8;

    .line 459312
    new-instance v0, LX/2m9;

    invoke-direct {v0}, LX/2m9;-><init>()V

    sput-object v0, LX/2m4;->STRING_RESULT_CONVERTER:LX/2m8;

    .line 459313
    new-instance v0, LX/2mA;

    invoke-direct {v0}, LX/2mA;-><init>()V

    sput-object v0, LX/2m4;->PARCELABLE_RESULT_CONVERTER:LX/2m8;

    return-void
.end method

.method public constructor <init>(LX/11H;LX/0e6;LX/2m6;LX/2m8;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;",
            "LX/2m6",
            "<+TPARAMS;>;",
            "LX/2m8",
            "<-TRESU",
            "LT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 459314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459315
    iput-object p1, p0, LX/2m4;->mSingleMethodRunner:LX/11H;

    .line 459316
    iput-object p2, p0, LX/2m4;->mApiMethod:LX/0e6;

    .line 459317
    iput-object p3, p0, LX/2m4;->mParamGetter:LX/2m6;

    .line 459318
    iput-object p4, p0, LX/2m4;->mResultConverter:LX/2m8;

    .line 459319
    return-void
.end method


# virtual methods
.method public handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 459320
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 459321
    iget-object v1, p0, LX/2m4;->mParamGetter:LX/2m6;

    invoke-interface {v1, v0}, LX/2m6;->getParams(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v0

    .line 459322
    iget-object v1, p0, LX/2m4;->mSingleMethodRunner:LX/11H;

    iget-object v2, p0, LX/2m4;->mApiMethod:LX/0e6;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 459323
    iget-object v1, p0, LX/2m4;->mResultConverter:LX/2m8;

    invoke-interface {v1, v0}, LX/2m8;->forResult(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
