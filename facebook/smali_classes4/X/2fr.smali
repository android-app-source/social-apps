.class public final LX/2fr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

.field public final synthetic b:Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)V
    .locals 0

    .prologue
    .line 447617
    iput-object p1, p0, LX/2fr;->b:Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;

    iput-object p2, p0, LX/2fr;->a:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x38e40e62

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 447618
    iget-object v0, p0, LX/2fr;->b:Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;

    iget-object v2, p0, LX/2fr;->a:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    .line 447619
    iget-object v4, v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->a:LX/0Zb;

    invoke-static {v2}, LX/1mc;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    :goto_0
    const-string v6, "saved_collection_ego_action_link_clicked"

    invoke-static {v5, v3, v6}, LX/17Q;->b(LX/0lF;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 447620
    iget-object v0, p0, LX/2fr;->a:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v2

    .line 447621
    iget-object v0, p0, LX/2fr;->a:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->o()LX/2ci;

    move-result-object v0

    iget-object v0, v0, LX/2ci;->b:Ljava/lang/String;

    .line 447622
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 447623
    const-string v4, "/"

    invoke-static {v4}, LX/1IA;->anyOf(Ljava/lang/CharSequence;)LX/1IA;

    move-result-object v4

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/1IA;->trimFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "saved"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    move v0, v3

    .line 447624
    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    .line 447625
    const/4 v0, 0x0

    .line 447626
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m()Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 447627
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->m()Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSavedDashboardSection;->a()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v0

    .line 447628
    :cond_0
    iget-object v3, p0, LX/2fr;->b:Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->g:LX/79x;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->n()Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_NETEGO_SEE_ALL_LINK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    invoke-virtual {v3, v4, v0, v2, v5}, LX/79x;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;)V

    .line 447629
    :goto_1
    const v0, -0x67dd2c14

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 447630
    :cond_1
    iget-object v0, p0, LX/2fr;->b:Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/saved/rows/SavedCollectionFooterPartDefinition;->b:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/2fr;->a:Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->o()LX/2ci;

    move-result-object v3

    iget-object v3, v3, LX/2ci;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    .line 447631
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
