.class public LX/2pC;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 467808
    const-class v0, LX/2pC;

    sput-object v0, LX/2pC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 467876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/2oj;LX/2oj;Ljava/util/List;)V
    .locals 2
    .param p0    # LX/2oj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/2oj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2oj;",
            "LX/2oj;",
            "Ljava/util/List",
            "<",
            "LX/2oa",
            "<+",
            "LX/2ol;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 467869
    if-eqz p1, :cond_0

    .line 467870
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oa;

    .line 467871
    invoke-virtual {p1, v0}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_0

    .line 467872
    :cond_0
    if-eqz p0, :cond_1

    .line 467873
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oa;

    .line 467874
    invoke-virtual {p0, v0}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_1

    .line 467875
    :cond_1
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/View;DDZZ)V
    .locals 10

    .prologue
    .line 467867
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-static/range {v0 .. v8}, LX/2pC;->a(Landroid/view/View;Landroid/view/View;DDZZZ)V

    .line 467868
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/View;DDZZZ)V
    .locals 6

    .prologue
    .line 467843
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 467844
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 467845
    if-lez v1, :cond_0

    if-lez v0, :cond_0

    const-wide/16 v2, 0x0

    cmpg-double v2, p2, v2

    if-gtz v2, :cond_1

    .line 467846
    :cond_0
    :goto_0
    return-void

    .line 467847
    :cond_1
    int-to-double v2, v1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v2, v4

    int-to-double v4, v0

    div-double/2addr v2, v4

    .line 467848
    const-wide/16 v4, 0x0

    cmpl-double v4, p4, v4

    if-lez v4, :cond_9

    .line 467849
    cmpl-double v2, p4, v2

    if-lez v2, :cond_6

    .line 467850
    int-to-double v2, v1

    div-double/2addr v2, p4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 467851
    :goto_1
    int-to-double v2, v1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v2, v4

    int-to-double v4, v0

    div-double/2addr v2, v4

    move-wide v4, v2

    move v3, v0

    .line 467852
    :goto_2
    cmpg-double v0, p2, v4

    if-gez v0, :cond_2

    if-eqz p6, :cond_3

    :cond_2
    cmpl-double v0, p2, v4

    if-lez v0, :cond_7

    if-eqz p6, :cond_7

    .line 467853
    :cond_3
    int-to-double v4, v3

    mul-double/2addr v4, p2

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    move v2, v3

    .line 467854
    :goto_3
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 467855
    if-eqz p8, :cond_4

    invoke-static {v4, v2, v0}, LX/2pC;->a(Landroid/view/ViewGroup$LayoutParams;II)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 467856
    :cond_4
    iput v2, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 467857
    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 467858
    invoke-virtual {p1, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 467859
    :cond_5
    if-eqz p7, :cond_8

    if-eqz p6, :cond_8

    .line 467860
    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 467861
    sub-int v0, v3, v2

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    .line 467862
    :cond_6
    int-to-double v2, v0

    mul-double/2addr v2, p4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    goto :goto_1

    .line 467863
    :cond_7
    int-to-double v4, v1

    div-double/2addr v4, p2

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    move v2, v0

    move v0, v1

    .line 467864
    goto :goto_3

    .line 467865
    :cond_8
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 467866
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    :cond_9
    move-wide v4, v2

    move v3, v0

    goto :goto_2
.end method

.method public static a(Landroid/view/View;Landroid/view/View;DZZ)V
    .locals 8

    .prologue
    .line 467841
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v6, p4

    move v7, p5

    invoke-static/range {v0 .. v7}, LX/2pC;->a(Landroid/view/View;Landroid/view/View;DDZZ)V

    .line 467842
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 467833
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-nez v0, :cond_1

    .line 467834
    :cond_0
    sget-object v0, LX/2pC;->a:Ljava/lang/Class;

    const-string v1, "Can\'t rotate and scale video view. Invalid width or height."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 467835
    :goto_0
    return-void

    .line 467836
    :cond_1
    invoke-virtual {p0, p2}, Landroid/view/View;->setRotation(F)V

    .line 467837
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 467838
    :goto_1
    invoke-virtual {p0, v0}, Landroid/view/View;->setScaleX(F)V

    .line 467839
    invoke-virtual {p0, v0}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0

    .line 467840
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_1
.end method

.method public static a(Landroid/view/View;Landroid/view/View;Landroid/graphics/RectF;D)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 467818
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 467819
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 467820
    if-lez v1, :cond_0

    if-lez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    cmpg-float v2, v2, v3

    if-lez v2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    cmpg-float v2, v2, v3

    if-lez v2, :cond_0

    const-wide/16 v2, 0x0

    cmpg-double v2, p3, v2

    if-gtz v2, :cond_1

    .line 467821
    :cond_0
    :goto_0
    return-void

    .line 467822
    :cond_1
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    .line 467823
    mul-double/2addr v2, p3

    .line 467824
    int-to-double v4, v1

    int-to-double v6, v0

    div-double/2addr v4, v6

    .line 467825
    cmpl-double v4, v2, v4

    if-lez v4, :cond_2

    .line 467826
    int-to-double v4, v1

    div-double v2, v4, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->rint(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 467827
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 467828
    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 467829
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 467830
    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 467831
    :cond_2
    int-to-double v4, v0

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->rint(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 467832
    goto :goto_1
.end method

.method public static a(Landroid/widget/ImageView;IILandroid/animation/Animator$AnimatorListener;)V
    .locals 4

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v1, 0x3f000000    # 0.5f

    .line 467810
    invoke-virtual {p0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 467811
    invoke-virtual {p0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 467812
    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 467813
    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 467814
    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 467815
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 467816
    invoke-virtual {p0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 467817
    return-void
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;II)Z
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 467809
    iget v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/lit8 v1, p1, -0x5

    if-lt v0, v1, :cond_0

    iget v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/lit8 v1, p1, 0x5

    if-gt v0, v1, :cond_0

    iget v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int/lit8 v1, p2, -0x5

    if-lt v0, v1, :cond_0

    iget v0, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int/lit8 v1, p2, 0x5

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
