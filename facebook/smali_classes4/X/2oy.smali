.class public abstract LX/2oy;
.super LX/2oX;
.source ""


# instance fields
.field public a:Landroid/view/ViewGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public g:Landroid/view/ViewGroup;

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2oa",
            "<+",
            "LX/2ol;",
            ">;>;"
        }
    .end annotation
.end field

.field public i:LX/2oj;

.field public j:LX/2pb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/video/player/RichVideoPlayer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public final m:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 467493
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 467494
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 467543
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 467544
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 467536
    invoke-direct {p0, p1, p2, p3}, LX/2oX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 467537
    iput-boolean v0, p0, LX/2oy;->l:Z

    .line 467538
    iput-boolean v0, p0, LX/2oy;->b:Z

    .line 467539
    iput-boolean v0, p0, LX/2oy;->c:Z

    .line 467540
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/2oy;->m:Ljava/util/Queue;

    .line 467541
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/2oy;->h:Ljava/util/List;

    .line 467542
    return-void
.end method

.method public static a(LX/2oy;Landroid/view/ViewGroup;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 467524
    iget-object v0, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467525
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 467526
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 467527
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 467528
    instance-of v1, p0, Lcom/facebook/video/player/plugins/VideoPlugin;

    if-eqz v1, :cond_1

    .line 467529
    iget-object v1, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 467530
    :cond_0
    :goto_1
    iget-object v1, p0, LX/2oy;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 467531
    :cond_1
    iget-object v1, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 467532
    if-ltz p2, :cond_0

    .line 467533
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    .line 467534
    :cond_2
    iput-object p1, p0, LX/2oy;->a:Landroid/view/ViewGroup;

    .line 467535
    return-void
.end method

.method public static g(LX/2oy;)V
    .locals 2

    .prologue
    .line 467514
    iget-object v0, p0, LX/2oy;->a:Landroid/view/ViewGroup;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467515
    :goto_0
    iget-object v0, p0, LX/2oy;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 467516
    iget-object v0, p0, LX/2oy;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 467517
    iget-object v1, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 467518
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 467519
    if-eqz v1, :cond_0

    .line 467520
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 467521
    :cond_0
    iget-object v1, p0, LX/2oy;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 467522
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/2oy;->a:Landroid/view/ViewGroup;

    .line 467523
    return-void
.end method


# virtual methods
.method public a(LX/2pa;)V
    .locals 1

    .prologue
    .line 467511
    invoke-virtual {p0}, LX/2oy;->d()V

    .line 467512
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/2oy;->a(LX/2pa;Z)V

    .line 467513
    return-void
.end method

.method public a(LX/2pa;Z)V
    .locals 0

    .prologue
    .line 467510
    return-void
.end method

.method public a(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V
    .locals 3

    .prologue
    .line 467502
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2oy;->l:Z

    .line 467503
    iput-object p1, p0, LX/2oy;->j:LX/2pb;

    .line 467504
    iput-object p2, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 467505
    iget-boolean v0, p0, LX/2oy;->b:Z

    if-nez v0, :cond_0

    .line 467506
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    const/4 v1, 0x0

    iget-object v2, p0, LX/2oy;->h:Ljava/util/List;

    invoke-static {v0, v1, v2}, LX/2pC;->a(LX/2oj;LX/2oj;Ljava/util/List;)V

    .line 467507
    :cond_0
    invoke-virtual {p0, p3}, LX/2oy;->a(LX/2pa;)V

    .line 467508
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2oy;->b:Z

    .line 467509
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 467495
    iget-boolean v0, p0, LX/2oy;->c:Z

    if-eqz v0, :cond_0

    .line 467496
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This plugin has already been attached to a RichVideoPlayer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 467497
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467498
    iput-object p1, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    .line 467499
    const/4 v0, -0x1

    invoke-static {p0, p0, v0}, LX/2oy;->a(LX/2oy;Landroid/view/ViewGroup;I)V

    .line 467500
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2oy;->c:Z

    .line 467501
    return-void
.end method

.method public b()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 467460
    iget-object v0, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    .line 467461
    invoke-static {p0}, LX/2oy;->g(LX/2oy;)V

    .line 467462
    const/4 v1, 0x0

    iput-object v1, p0, LX/2oy;->g:Landroid/view/ViewGroup;

    .line 467463
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/2oy;->c:Z

    .line 467464
    return-object v0
.end method

.method public b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 467485
    iput-boolean v0, p0, LX/2oy;->l:Z

    .line 467486
    iput-object p1, p0, LX/2oy;->j:LX/2pb;

    .line 467487
    iput-object p2, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 467488
    iget-boolean v2, p0, LX/2oy;->b:Z

    if-nez v2, :cond_0

    .line 467489
    iget-object v2, p0, LX/2oy;->i:LX/2oj;

    const/4 v3, 0x0

    iget-object v4, p0, LX/2oy;->h:Ljava/util/List;

    invoke-static {v2, v3, v4}, LX/2pC;->a(LX/2oj;LX/2oj;Ljava/util/List;)V

    .line 467490
    :cond_0
    iget-boolean v2, p0, LX/2oy;->b:Z

    if-nez v2, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {p0, p3, v0}, LX/2oy;->a(LX/2pa;Z)V

    .line 467491
    iput-boolean v1, p0, LX/2oy;->b:Z

    .line 467492
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 467545
    return-void
.end method

.method public dH_()V
    .locals 0

    .prologue
    .line 467484
    return-void
.end method

.method public dI_()V
    .locals 0

    .prologue
    .line 467483
    return-void
.end method

.method public dJ_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 467480
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    iget-object v1, p0, LX/2oy;->h:Ljava/util/List;

    invoke-static {v2, v0, v1}, LX/2pC;->a(LX/2oj;LX/2oj;Ljava/util/List;)V

    .line 467481
    iput-object v2, p0, LX/2oy;->i:LX/2oj;

    .line 467482
    return-void
.end method

.method public im_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 467474
    invoke-virtual {p0}, LX/2oy;->d()V

    .line 467475
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    iget-object v1, p0, LX/2oy;->h:Ljava/util/List;

    invoke-static {v2, v0, v1}, LX/2pC;->a(LX/2oj;LX/2oj;Ljava/util/List;)V

    .line 467476
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/2oy;->b:Z

    .line 467477
    iput-object v2, p0, LX/2oy;->j:LX/2pb;

    .line 467478
    iput-object v2, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 467479
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 467472
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/2oy;->l:Z

    .line 467473
    return-void
.end method

.method public q()V
    .locals 0

    .prologue
    .line 467471
    return-void
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 467470
    const/4 v0, 0x0

    return v0
.end method

.method public s()V
    .locals 0

    .prologue
    .line 467469
    return-void
.end method

.method public setEventBus(LX/2oj;)V
    .locals 2

    .prologue
    .line 467466
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    iget-object v1, p0, LX/2oy;->h:Ljava/util/List;

    invoke-static {p1, v0, v1}, LX/2pC;->a(LX/2oj;LX/2oj;Ljava/util/List;)V

    .line 467467
    iput-object p1, p0, LX/2oy;->i:LX/2oj;

    .line 467468
    return-void
.end method

.method public t()V
    .locals 0

    .prologue
    .line 467465
    return-void
.end method
