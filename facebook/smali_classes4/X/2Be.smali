.class public final LX/2Be;
.super LX/2B6;
.source ""


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 381408
    invoke-direct {p0}, LX/2B6;-><init>()V

    .line 381409
    iput-object p1, p0, LX/2Be;->a:Ljava/lang/Class;

    .line 381410
    iput-object p2, p0, LX/2Be;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 381411
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)LX/2B6;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/2B6;"
        }
    .end annotation

    .prologue
    .line 381412
    new-instance v0, LX/4rT;

    iget-object v1, p0, LX/2Be;->a:Ljava/lang/Class;

    iget-object v2, p0, LX/2Be;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-direct {v0, v1, v2, p1, p2}, LX/4rT;-><init>(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381413
    iget-object v0, p0, LX/2Be;->a:Ljava/lang/Class;

    if-ne p1, v0, :cond_0

    .line 381414
    iget-object v0, p0, LX/2Be;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 381415
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
