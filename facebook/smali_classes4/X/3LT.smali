.class public LX/3LT;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0fK;

.field public final d:LX/0Uh;

.field public final e:LX/3LR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 550562
    const-class v0, LX/3LT;

    sput-object v0, LX/3LT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0fK;LX/0Uh;)V
    .locals 1
    .param p2    # LX/0fK;
        .annotation runtime Lcom/facebook/divebar/Right;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 550563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550564
    iput-object p1, p0, LX/3LT;->b:Landroid/content/Context;

    .line 550565
    iput-object p2, p0, LX/3LT;->c:LX/0fK;

    .line 550566
    iput-object p3, p0, LX/3LT;->d:LX/0Uh;

    .line 550567
    new-instance v0, LX/3LR;

    invoke-direct {v0}, LX/3LR;-><init>()V

    iput-object v0, p0, LX/3LT;->e:LX/3LR;

    .line 550568
    return-void
.end method

.method public static a(LX/0QB;)LX/3LT;
    .locals 4

    .prologue
    .line 550569
    new-instance v3, LX/3LT;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/10Y;->b(LX/0QB;)LX/0fK;

    move-result-object v1

    check-cast v1, LX/0fK;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-direct {v3, v0, v1, v2}, LX/3LT;-><init>(Landroid/content/Context;LX/0fK;LX/0Uh;)V

    .line 550570
    move-object v0, v3

    .line 550571
    return-object v0
.end method
