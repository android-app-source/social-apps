.class public LX/2Dd;
.super Ljava/io/Writer;
.source ""


# instance fields
.field private final a:Ljava/io/OutputStream;

.field public b:Ljava/nio/charset/CharsetEncoder;

.field public c:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 384487
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, LX/2Dd;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;Ljava/nio/ByteBuffer;)V

    .line 384488
    return-void
.end method

.method private constructor <init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 384480
    invoke-direct {p0, p1}, Ljava/io/Writer;-><init>(Ljava/lang/Object;)V

    .line 384481
    iput-object p1, p0, LX/2Dd;->a:Ljava/io/OutputStream;

    .line 384482
    iput-object p3, p0, LX/2Dd;->c:Ljava/nio/ByteBuffer;

    .line 384483
    invoke-virtual {p2}, Ljava/nio/charset/Charset;->newEncoder()Ljava/nio/charset/CharsetEncoder;

    move-result-object v0

    iput-object v0, p0, LX/2Dd;->b:Ljava/nio/charset/CharsetEncoder;

    .line 384484
    iget-object v0, p0, LX/2Dd;->b:Ljava/nio/charset/CharsetEncoder;

    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v0, v1}, Ljava/nio/charset/CharsetEncoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetEncoder;

    .line 384485
    iget-object v0, p0, LX/2Dd;->b:Ljava/nio/charset/CharsetEncoder;

    sget-object v1, Ljava/nio/charset/CodingErrorAction;->REPLACE:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v0, v1}, Ljava/nio/charset/CharsetEncoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetEncoder;

    .line 384486
    return-void
.end method

.method public static a(LX/2Dd;Z)V
    .locals 5

    .prologue
    .line 384470
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 384471
    :try_start_0
    invoke-direct {p0}, LX/2Dd;->b()V

    .line 384472
    iget-object v0, p0, LX/2Dd;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .line 384473
    if-lez v0, :cond_0

    .line 384474
    iget-object v2, p0, LX/2Dd;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 384475
    iget-object v2, p0, LX/2Dd;->a:Ljava/io/OutputStream;

    iget-object v3, p0, LX/2Dd;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    iget-object v4, p0, LX/2Dd;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    invoke-virtual {v2, v3, v4, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 384476
    iget-object v0, p0, LX/2Dd;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 384477
    :cond_0
    if-eqz p1, :cond_1

    .line 384478
    iget-object v0, p0, LX/2Dd;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 384479
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/nio/CharBuffer;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 384464
    :goto_0
    iget-object v0, p0, LX/2Dd;->b:Ljava/nio/charset/CharsetEncoder;

    iget-object v1, p0, LX/2Dd;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, v1, v2}, Ljava/nio/charset/CharsetEncoder;->encode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;Z)Ljava/nio/charset/CoderResult;

    move-result-object v0

    .line 384465
    invoke-virtual {v0}, Ljava/nio/charset/CoderResult;->isOverflow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384466
    invoke-static {p0, v2}, LX/2Dd;->a(LX/2Dd;Z)V

    goto :goto_0

    .line 384467
    :cond_0
    invoke-virtual {v0}, Ljava/nio/charset/CoderResult;->isError()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 384468
    invoke-virtual {v0}, Ljava/nio/charset/CoderResult;->throwException()V

    .line 384469
    :cond_1
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 384461
    iget-object v0, p0, LX/2Dd;->b:Ljava/nio/charset/CharsetEncoder;

    if-nez v0, :cond_0

    .line 384462
    new-instance v0, Ljava/io/IOException;

    const-string v1, "OutputStreamWriter is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384463
    :cond_0
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 6

    .prologue
    .line 384442
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 384443
    :try_start_0
    iget-object v0, p0, LX/2Dd;->b:Ljava/nio/charset/CharsetEncoder;

    if-eqz v0, :cond_4

    .line 384444
    const/4 v5, 0x0

    .line 384445
    invoke-static {v5}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v0

    .line 384446
    :goto_0
    iget-object v2, p0, LX/2Dd;->b:Ljava/nio/charset/CharsetEncoder;

    iget-object v3, p0, LX/2Dd;->c:Ljava/nio/ByteBuffer;

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Ljava/nio/charset/CharsetEncoder;->encode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;Z)Ljava/nio/charset/CoderResult;

    move-result-object v2

    .line 384447
    invoke-virtual {v2}, Ljava/nio/charset/CoderResult;->isError()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 384448
    invoke-virtual {v2}, Ljava/nio/charset/CoderResult;->throwException()V

    .line 384449
    :cond_0
    :goto_1
    iget-object v0, p0, LX/2Dd;->b:Ljava/nio/charset/CharsetEncoder;

    iget-object v2, p0, LX/2Dd;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/charset/CharsetEncoder;->flush(Ljava/nio/ByteBuffer;)Ljava/nio/charset/CoderResult;

    move-result-object v0

    .line 384450
    :goto_2
    invoke-virtual {v0}, Ljava/nio/charset/CoderResult;->isUnderflow()Z

    move-result v2

    if-nez v2, :cond_3

    .line 384451
    invoke-virtual {v0}, Ljava/nio/charset/CoderResult;->isOverflow()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 384452
    invoke-static {p0, v5}, LX/2Dd;->a(LX/2Dd;Z)V

    goto :goto_1

    .line 384453
    :cond_1
    invoke-virtual {v2}, Ljava/nio/charset/CoderResult;->isOverflow()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 384454
    invoke-static {p0, v5}, LX/2Dd;->a(LX/2Dd;Z)V

    goto :goto_0

    .line 384455
    :cond_2
    invoke-virtual {v0}, Ljava/nio/charset/CoderResult;->throwException()V

    goto :goto_2

    .line 384456
    :cond_3
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/2Dd;->a(LX/2Dd;Z)V

    .line 384457
    iget-object v0, p0, LX/2Dd;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 384458
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Dd;->b:Ljava/nio/charset/CharsetEncoder;

    .line 384459
    const/4 v0, 0x0

    iput-object v0, p0, LX/2Dd;->c:Ljava/nio/ByteBuffer;

    .line 384460
    :cond_4
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 384440
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/2Dd;->a(LX/2Dd;Z)V

    .line 384441
    return-void
.end method

.method public final write(I)V
    .locals 4

    .prologue
    .line 384417
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 384418
    :try_start_0
    invoke-direct {p0}, LX/2Dd;->b()V

    .line 384419
    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v2, 0x0

    int-to-char v3, p1

    aput-char v3, v0, v2

    invoke-static {v0}, Ljava/nio/CharBuffer;->wrap([C)Ljava/nio/CharBuffer;

    move-result-object v0

    .line 384420
    invoke-direct {p0, v0}, LX/2Dd;->a(Ljava/nio/CharBuffer;)V

    .line 384421
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final write(Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 384428
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 384429
    if-gez p3, :cond_0

    .line 384430
    :try_start_0
    invoke-static {p1, p2, p3}, LX/2Df;->a(Ljava/lang/String;II)Ljava/lang/StringIndexOutOfBoundsException;

    move-result-object v0

    throw v0

    .line 384431
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 384432
    :cond_0
    if-nez p1, :cond_1

    .line 384433
    :try_start_1
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "str == null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384434
    :cond_1
    or-int v0, p2, p3

    if-ltz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, p3

    if-le p2, v0, :cond_3

    .line 384435
    :cond_2
    invoke-static {p1, p2, p3}, LX/2Df;->a(Ljava/lang/String;II)Ljava/lang/StringIndexOutOfBoundsException;

    move-result-object v0

    throw v0

    .line 384436
    :cond_3
    invoke-direct {p0}, LX/2Dd;->b()V

    .line 384437
    add-int v0, p3, p2

    invoke-static {p1, p2, v0}, Ljava/nio/CharBuffer;->wrap(Ljava/lang/CharSequence;II)Ljava/nio/CharBuffer;

    move-result-object v0

    .line 384438
    invoke-direct {p0, v0}, LX/2Dd;->a(Ljava/nio/CharBuffer;)V

    .line 384439
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final write([CII)V
    .locals 2

    .prologue
    .line 384422
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 384423
    :try_start_0
    invoke-direct {p0}, LX/2Dd;->b()V

    .line 384424
    array-length v0, p1

    invoke-static {v0, p2, p3}, LX/2Df;->a(III)V

    .line 384425
    invoke-static {p1, p2, p3}, Ljava/nio/CharBuffer;->wrap([CII)Ljava/nio/CharBuffer;

    move-result-object v0

    .line 384426
    invoke-direct {p0, v0}, LX/2Dd;->a(Ljava/nio/CharBuffer;)V

    .line 384427
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
