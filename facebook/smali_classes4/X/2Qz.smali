.class public LX/2Qz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/platform/server/protocol/GetAppNameMethod$Params;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 409330
    const-class v0, LX/2Qz;

    sput-object v0, LX/2Qz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 409331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409332
    return-void
.end method

.method public static a(LX/0QB;)LX/2Qz;
    .locals 1

    .prologue
    .line 409333
    new-instance v0, LX/2Qz;

    invoke-direct {v0}, LX/2Qz;-><init>()V

    .line 409334
    move-object v0, v0

    .line 409335
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 409336
    check-cast p1, Lcom/facebook/platform/server/protocol/GetAppNameMethod$Params;

    .line 409337
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409338
    iget-object v0, p1, Lcom/facebook/platform/server/protocol/GetAppNameMethod$Params;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409339
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 409340
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "getAppName"

    .line 409341
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 409342
    move-object v1, v1

    .line 409343
    const-string v2, "GET"

    .line 409344
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 409345
    move-object v1, v1

    .line 409346
    iget-object v2, p1, Lcom/facebook/platform/server/protocol/GetAppNameMethod$Params;->a:Ljava/lang/String;

    .line 409347
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 409348
    move-object v1, v1

    .line 409349
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 409350
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 409351
    move-object v1, v1

    .line 409352
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 409353
    move-object v0, v1

    .line 409354
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 409355
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
