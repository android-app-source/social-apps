.class public final LX/3IS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;)V
    .locals 0

    .prologue
    .line 546453
    iput-object p1, p0, LX/3IS;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;B)V
    .locals 0

    .prologue
    .line 546454
    invoke-direct {p0, p1}, LX/3IS;-><init>(Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;)V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 546455
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 546456
    const v1, 0x3f666666    # 0.9f

    mul-float/2addr v1, v0

    .line 546457
    const v2, 0x3f333333    # 0.7f

    mul-float/2addr v2, v0

    .line 546458
    iget-object v3, p0, LX/3IS;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v3, v3, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->c:Lcom/facebook/spherical/ui/HeadingBackgroundView;

    invoke-virtual {v3, v2}, Lcom/facebook/spherical/ui/HeadingBackgroundView;->setAlpha(F)V

    .line 546459
    iget-object v2, p0, LX/3IS;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v2, v2, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->d:Lcom/facebook/spherical/ui/HeadingFovView;

    invoke-virtual {v2, v1}, Lcom/facebook/spherical/ui/HeadingFovView;->setAlpha(F)V

    .line 546460
    iget-object v2, p0, LX/3IS;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v2, v2, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->e:Lcom/facebook/spherical/ui/HeadingPoiView;

    invoke-virtual {v2, v1}, Lcom/facebook/spherical/ui/HeadingPoiView;->setAlpha(F)V

    .line 546461
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 546462
    iget-object v0, p0, LX/3IS;->a:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v0, v0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->p:LX/3E2;

    invoke-virtual {v0}, LX/3E2;->start()Landroid/os/CountDownTimer;

    .line 546463
    :cond_0
    return-void
.end method
