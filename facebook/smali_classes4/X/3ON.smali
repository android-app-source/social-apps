.class public final enum LX/3ON;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3ON;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3ON;

.field public static final enum FACEBOOK_TAB:LX/3ON;

.field public static final enum INVITE_BUTTON_PICKER:LX/3ON;

.field public static final enum MESSENGER_TAB:LX/3ON;

.field public static final enum NEUE_PICKER:LX/3ON;

.field public static final enum ONE_LINE:LX/3ON;

.field public static final enum SINGLE_TAP_SEND:LX/3ON;

.field public static final enum SINGLE_TAP_SEND_WITH_UNDO:LX/3ON;

.field public static final enum TWO_LINE:LX/3ON;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 560420
    new-instance v0, LX/3ON;

    const-string v1, "MESSENGER_TAB"

    invoke-direct {v0, v1, v3}, LX/3ON;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3ON;->MESSENGER_TAB:LX/3ON;

    .line 560421
    new-instance v0, LX/3ON;

    const-string v1, "FACEBOOK_TAB"

    invoke-direct {v0, v1, v4}, LX/3ON;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3ON;->FACEBOOK_TAB:LX/3ON;

    .line 560422
    new-instance v0, LX/3ON;

    const-string v1, "NEUE_PICKER"

    invoke-direct {v0, v1, v5}, LX/3ON;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3ON;->NEUE_PICKER:LX/3ON;

    .line 560423
    new-instance v0, LX/3ON;

    const-string v1, "ONE_LINE"

    invoke-direct {v0, v1, v6}, LX/3ON;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3ON;->ONE_LINE:LX/3ON;

    .line 560424
    new-instance v0, LX/3ON;

    const-string v1, "TWO_LINE"

    invoke-direct {v0, v1, v7}, LX/3ON;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3ON;->TWO_LINE:LX/3ON;

    .line 560425
    new-instance v0, LX/3ON;

    const-string v1, "INVITE_BUTTON_PICKER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3ON;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3ON;->INVITE_BUTTON_PICKER:LX/3ON;

    .line 560426
    new-instance v0, LX/3ON;

    const-string v1, "SINGLE_TAP_SEND"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/3ON;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3ON;->SINGLE_TAP_SEND:LX/3ON;

    .line 560427
    new-instance v0, LX/3ON;

    const-string v1, "SINGLE_TAP_SEND_WITH_UNDO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/3ON;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3ON;->SINGLE_TAP_SEND_WITH_UNDO:LX/3ON;

    .line 560428
    const/16 v0, 0x8

    new-array v0, v0, [LX/3ON;

    sget-object v1, LX/3ON;->MESSENGER_TAB:LX/3ON;

    aput-object v1, v0, v3

    sget-object v1, LX/3ON;->FACEBOOK_TAB:LX/3ON;

    aput-object v1, v0, v4

    sget-object v1, LX/3ON;->NEUE_PICKER:LX/3ON;

    aput-object v1, v0, v5

    sget-object v1, LX/3ON;->ONE_LINE:LX/3ON;

    aput-object v1, v0, v6

    sget-object v1, LX/3ON;->TWO_LINE:LX/3ON;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3ON;->INVITE_BUTTON_PICKER:LX/3ON;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3ON;->SINGLE_TAP_SEND:LX/3ON;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3ON;->SINGLE_TAP_SEND_WITH_UNDO:LX/3ON;

    aput-object v2, v0, v1

    sput-object v0, LX/3ON;->$VALUES:[LX/3ON;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560429
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3ON;
    .locals 1

    .prologue
    .line 560419
    const-class v0, LX/3ON;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3ON;

    return-object v0
.end method

.method public static values()[LX/3ON;
    .locals 1

    .prologue
    .line 560418
    sget-object v0, LX/3ON;->$VALUES:[LX/3ON;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3ON;

    return-object v0
.end method
