.class public final LX/2z9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HeR;

.field public final synthetic b:LX/HeQ;


# direct methods
.method public constructor <init>(LX/HeQ;LX/HeR;)V
    .locals 0

    .prologue
    .line 483047
    iput-object p1, p0, LX/2z9;->b:LX/HeQ;

    iput-object p2, p0, LX/2z9;->a:LX/HeR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x7c44a91a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 483048
    iget-object v0, p0, LX/2z9;->b:LX/HeQ;

    iget-object v2, p0, LX/2z9;->a:LX/HeR;

    invoke-virtual {v0, v2}, LX/HeQ;->a(LX/HeR;)V

    .line 483049
    iget-object v0, p0, LX/2z9;->b:LX/HeQ;

    iget-object v0, v0, LX/HeQ;->a:LX/0l1;

    iget-object v2, p0, LX/2z9;->a:LX/HeR;

    const/4 v5, 0x1

    const/4 p1, 0x0

    .line 483050
    iget v4, v2, LX/HeR;->c:I

    move v4, v4

    .line 483051
    packed-switch v4, :pswitch_data_0

    .line 483052
    iget v4, v2, LX/HeR;->c:I

    move v4, v4

    .line 483053
    iget-object v5, v0, LX/0l1;->c:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 483054
    iget-object v5, v0, LX/0l1;->c:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 483055
    if-eqz v5, :cond_0

    .line 483056
    iget-object v6, v0, LX/0l1;->r:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/katana/urimap/IntentHandlerUtil;

    iget-object p1, v0, LX/0l1;->d:Landroid/app/Activity;

    invoke-virtual {v6, p1, v5}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 483057
    :cond_0
    :goto_0
    iget-object v0, p0, LX/2z9;->b:LX/HeQ;

    iget-object v0, v0, LX/HeQ;->e:Landroid/content/Context;

    instance-of v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    if-eqz v0, :cond_1

    .line 483058
    iget-object v0, p0, LX/2z9;->b:LX/HeQ;

    iget-object v0, v0, LX/HeQ;->e:Landroid/content/Context;

    check-cast v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v2, p0, LX/2z9;->a:LX/HeR;

    invoke-virtual {v0, v2}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(LX/HeR;)V

    .line 483059
    :cond_1
    iget-object v0, p0, LX/2z9;->a:LX/HeR;

    .line 483060
    iget v2, v0, LX/HeR;->c:I

    move v0, v2

    .line 483061
    const/16 v2, 0x3ee

    if-eq v0, v2, :cond_2

    .line 483062
    iget-object v0, p0, LX/2z9;->b:LX/HeQ;

    invoke-virtual {v0}, LX/HeQ;->d()V

    .line 483063
    :cond_2
    const v0, -0xa6ce5be

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 483064
    :pswitch_0
    iget-object v4, v0, LX/0l1;->g:LX/0hx;

    sget-object v5, LX/CIp;->h:Ljava/lang/String;

    invoke-virtual {v4, v5, p1}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483065
    new-instance v4, Landroid/content/Intent;

    iget-object v5, v0, LX/0l1;->d:Landroid/app/Activity;

    const-class v6, Lcom/facebook/katana/settings/activity/SettingsActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 483066
    iget-object v5, v0, LX/0l1;->d:Landroid/app/Activity;

    invoke-virtual {v5, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 483067
    :pswitch_1
    iget-object v4, v0, LX/0l1;->g:LX/0hx;

    sget-object v5, LX/CIp;->i:Ljava/lang/String;

    invoke-virtual {v4, v5, p1}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483068
    new-instance v4, Landroid/content/Intent;

    iget-object v5, v0, LX/0l1;->d:Landroid/app/Activity;

    const-class v6, Lcom/facebook/about/AboutActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 483069
    iget-object v5, v0, LX/0l1;->l:Lcom/facebook/content/SecureContextHelper;

    iget-object v6, v0, LX/0l1;->d:Landroid/app/Activity;

    invoke-interface {v5, v4, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 483070
    :pswitch_2
    iget-object v4, v0, LX/0l1;->g:LX/0hx;

    sget-object v6, LX/CIp;->j:Ljava/lang/String;

    invoke-virtual {v4, v6, p1}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483071
    iget-object v4, v0, LX/0l1;->p:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Fjp;

    invoke-virtual {v4, v5}, LX/Fjp;->a(Z)V

    goto :goto_0

    .line 483072
    :pswitch_3
    iget-object v4, v0, LX/0l1;->g:LX/0hx;

    sget-object v5, LX/CIp;->k:Ljava/lang/String;

    invoke-virtual {v4, v5, p1}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483073
    iget-object v4, v0, LX/0l1;->d:Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0l1;->j(LX/0l1;Landroid/app/Activity;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 483074
    iget-object v4, v0, LX/0l1;->d:Landroid/app/Activity;

    .line 483075
    iget-object v5, v0, LX/0l1;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/6G2;

    invoke-virtual {v5, v4}, LX/6G2;->a(Landroid/content/Context;)V

    .line 483076
    goto/16 :goto_0

    .line 483077
    :pswitch_4
    iget-object v4, v0, LX/0l1;->g:LX/0hx;

    sget-object v5, LX/CIp;->l:Ljava/lang/String;

    invoke-virtual {v4, v5, p1}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483078
    new-instance v4, LX/0ju;

    iget-object v5, v0, LX/0l1;->d:Landroid/app/Activity;

    invoke-direct {v4, v5}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 483079
    const-string v5, "How do you want to fail?"

    invoke-virtual {v4, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v5

    const-string v6, "OOM crash"

    new-instance p1, LX/JcA;

    invoke-direct {p1, v0}, LX/JcA;-><init>(LX/0l1;)V

    invoke-virtual {v5, v6, p1}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    const-string v6, "Hard crash"

    new-instance p1, LX/Jc9;

    invoke-direct {p1, v0}, LX/Jc9;-><init>(LX/0l1;)V

    invoke-virtual {v5, v6, p1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    const-string v6, "Soft error"

    new-instance p1, LX/Jc8;

    invoke-direct {p1, v0}, LX/Jc8;-><init>(LX/0l1;)V

    invoke-virtual {v5, v6, p1}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 483080
    invoke-virtual {v4}, LX/0ju;->b()LX/2EJ;

    goto/16 :goto_0

    .line 483081
    :pswitch_5
    iget-object v4, v0, LX/0l1;->g:LX/0hx;

    sget-object v6, LX/CIp;->n:Ljava/lang/String;

    invoke-virtual {v4, v6, p1}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483082
    const/4 v6, 0x0

    .line 483083
    iget-object v4, v0, LX/0l1;->e:LX/0WJ;

    invoke-virtual {v4}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object p1

    .line 483084
    if-eqz p1, :cond_3

    iget-object v4, v0, LX/0l1;->k:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/10M;

    .line 483085
    iget-object v2, p1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p1, v2

    .line 483086
    invoke-virtual {v4, p1}, LX/10M;->b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 483087
    :goto_1
    iget-object v4, v0, LX/0l1;->m:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2lP;

    iget-object v6, v0, LX/0l1;->d:Landroid/app/Activity;

    invoke-virtual {v4, v6, v5}, LX/2lP;->a(Landroid/app/Activity;Z)V

    goto/16 :goto_0

    .line 483088
    :pswitch_6
    iget-object v4, v0, LX/0l1;->g:LX/0hx;

    sget-object v6, LX/CIp;->o:Ljava/lang/String;

    invoke-virtual {v4, v6, p1}, LX/0hx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483089
    iget-object v4, v0, LX/0l1;->a:LX/HeQ;

    invoke-virtual {v4}, LX/HeQ;->d()V

    .line 483090
    iget-object v4, v0, LX/0l1;->a:LX/HeQ;

    iget-object v6, v0, LX/0l1;->d:Landroid/app/Activity;

    invoke-static {v6}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v4, v6, v5, v5}, LX/HeQ;->a(Landroid/view/View;ZZ)V

    goto/16 :goto_0

    .line 483091
    :pswitch_7
    iget-object v4, v0, LX/0l1;->q:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/17X;

    iget-object v5, v0, LX/0l1;->d:Landroid/app/Activity;

    sget-object v6, LX/0ax;->ak:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 483092
    const-string v5, "trigger"

    const-string v6, "context_menu"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 483093
    iget-object v5, v0, LX/0l1;->d:Landroid/app/Activity;

    invoke-virtual {v5, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 483094
    :pswitch_8
    iget-object v4, v0, LX/0l1;->n:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/33u;

    invoke-virtual {v4}, LX/33u;->c()LX/33y;

    move-result-object v4

    invoke-virtual {v4}, LX/33y;->h()V

    goto/16 :goto_0

    :cond_3
    move v5, v6

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_6
        :pswitch_2
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
