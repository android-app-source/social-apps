.class public final LX/2C5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/2AX;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/2AX;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 382029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 382030
    iput-object p1, p0, LX/2C5;->a:LX/0QB;

    .line 382031
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 382032
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/2C5;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 382033
    packed-switch p2, :pswitch_data_0

    .line 382034
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 382035
    :pswitch_0
    new-instance v0, LX/2zm;

    invoke-direct {v0}, LX/2zm;-><init>()V

    .line 382036
    move-object v0, v0

    .line 382037
    move-object v0, v0

    .line 382038
    :goto_0
    return-object v0

    .line 382039
    :pswitch_1
    invoke-static {p1}, LX/2Ht;->a(LX/0QB;)LX/2Ht;

    move-result-object v0

    goto :goto_0

    .line 382040
    :pswitch_2
    invoke-static {p1}, LX/2Hx;->a(LX/0QB;)LX/2Hx;

    move-result-object v0

    goto :goto_0

    .line 382041
    :pswitch_3
    invoke-static {p1}, LX/2Hy;->a(LX/0QB;)LX/2Hy;

    move-result-object v0

    goto :goto_0

    .line 382042
    :pswitch_4
    invoke-static {p1}, LX/2zo;->a(LX/0QB;)LX/2zo;

    move-result-object v0

    goto :goto_0

    .line 382043
    :pswitch_5
    invoke-static {p1}, LX/2I0;->a(LX/0QB;)LX/2I0;

    move-result-object v0

    goto :goto_0

    .line 382044
    :pswitch_6
    invoke-static {p1}, LX/2I1;->getInstance__com_facebook_omnistore_mqtt_OmnistoreMqttTopicsSetProvider__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2I1;

    move-result-object v0

    goto :goto_0

    .line 382045
    :pswitch_7
    new-instance v1, LX/2zv;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v0}, LX/2zv;-><init>(LX/0Uh;)V

    .line 382046
    move-object v0, v1

    .line 382047
    goto :goto_0

    .line 382048
    :pswitch_8
    invoke-static {p1}, LX/2AW;->a(LX/0QB;)LX/2AW;

    move-result-object v0

    goto :goto_0

    .line 382049
    :pswitch_9
    invoke-static {p1}, LX/2IA;->a(LX/0QB;)LX/2IA;

    move-result-object v0

    goto :goto_0

    .line 382050
    :pswitch_a
    invoke-static {p1}, LX/2I3;->a(LX/0QB;)LX/2AX;

    move-result-object v0

    goto :goto_0

    .line 382051
    :pswitch_b
    invoke-static {p1}, LX/2I5;->a(LX/0QB;)LX/2I5;

    move-result-object v0

    goto :goto_0

    .line 382052
    :pswitch_c
    new-instance v0, LX/2zx;

    invoke-direct {v0}, LX/2zx;-><init>()V

    .line 382053
    move-object v0, v0

    .line 382054
    move-object v0, v0

    .line 382055
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 382056
    const/16 v0, 0xd

    return v0
.end method
