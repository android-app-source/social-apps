.class public LX/3Jl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3Jf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/3Jf;


# direct methods
.method public constructor <init>(IILjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LX/3Jf;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 547992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547993
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "group_id"

    invoke-static {v3, v0, v4}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/3Jl;->a:I

    .line 547994
    iput p2, p0, LX/3Jl;->b:I

    .line 547995
    sget-object v0, LX/3Jf;->a:Ljava/util/Comparator;

    invoke-static {p3, v0}, LX/3Jh;->a(Ljava/util/List;Ljava/util/Comparator;)V

    .line 547996
    sget-object v0, LX/3JT;->ANCHOR_POINT:LX/3JT;

    invoke-static {p3, v0}, LX/3Jm;->a(Ljava/util/List;LX/3JT;)LX/3Jf;

    move-result-object v0

    iput-object v0, p0, LX/3Jl;->d:LX/3Jf;

    .line 547997
    invoke-static {p3}, LX/3Jh;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    :goto_1
    const-string v2, "animations"

    invoke-static {v0, v1, v2}, LX/3Je;->a(Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/3Jl;->c:Ljava/util/List;

    .line 547998
    return-void

    :cond_0
    move v0, v2

    .line 547999
    goto :goto_0

    :cond_1
    move v1, v2

    .line 548000
    goto :goto_1
.end method


# virtual methods
.method public final d()LX/9Ul;
    .locals 1

    .prologue
    .line 548001
    iget-object v0, p0, LX/3Jl;->d:LX/3Jf;

    if-nez v0, :cond_0

    .line 548002
    const/4 v0, 0x0

    .line 548003
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3Jl;->d:LX/3Jf;

    .line 548004
    iget-object p0, v0, LX/3Jf;->f:LX/3Jj;

    move-object v0, p0

    .line 548005
    check-cast v0, LX/9Ul;

    goto :goto_0
.end method
