.class public final LX/2jN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/2iz;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2iz;)V
    .locals 1

    .prologue
    .line 452836
    iput-object p1, p0, LX/2jN;->a:LX/2iz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 452837
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/2jN;->b:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x2d9a32df

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 452819
    const-string v1, "extra_request_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 452820
    iget-object v2, p0, LX/2jN;->a:LX/2iz;

    iget-object v2, v2, LX/2iz;->e:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v2, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v2

    .line 452821
    iget-object v3, p0, LX/2jN;->a:LX/2iz;

    invoke-virtual {v3, v1}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v3

    .line 452822
    if-eqz v3, :cond_0

    .line 452823
    invoke-virtual {v3, p2}, LX/2jY;->a(Landroid/content/Intent;)V

    .line 452824
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 452825
    invoke-virtual {v3}, LX/2jY;->t()LX/CfG;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 452826
    invoke-virtual {v3}, LX/2jY;->t()LX/CfG;

    move-result-object p0

    invoke-interface {p0, v1, v2}, LX/CfG;->a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V

    .line 452827
    :goto_0
    const v1, 0x4070032c

    invoke-static {v1, v0}, LX/02F;->e(II)V

    return-void

    .line 452828
    :cond_0
    iget-object p1, p0, LX/2jN;->a:LX/2iz;

    iget-object v3, p0, LX/2jN;->b:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p1, v3}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v3

    .line 452829
    if-eqz v3, :cond_1

    .line 452830
    invoke-virtual {v3, p2}, LX/2jY;->a(Landroid/content/Intent;)V

    .line 452831
    invoke-virtual {v3}, LX/2jY;->t()LX/CfG;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 452832
    invoke-virtual {v3}, LX/2jY;->t()LX/CfG;

    move-result-object v3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v3, p1, v2}, LX/CfG;->a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V

    .line 452833
    :cond_1
    goto :goto_0

    .line 452834
    :cond_2
    iput-object v1, v3, LX/2jY;->m:Ljava/lang/String;

    .line 452835
    goto :goto_0
.end method
