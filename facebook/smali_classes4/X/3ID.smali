.class public LX/3ID;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field private b:F

.field private c:F

.field private d:I

.field public e:Z

.field public f:Z

.field private g:F

.field private h:F

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 545741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 545742
    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, LX/3ID;->l:F

    .line 545743
    const/16 v0, 0x8

    iput v0, p0, LX/3ID;->a:I

    .line 545744
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 545745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 545746
    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, LX/3ID;->l:F

    .line 545747
    iput p1, p0, LX/3ID;->a:I

    .line 545748
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 545738
    iput-boolean v0, p0, LX/3ID;->e:Z

    .line 545739
    iput-boolean v0, p0, LX/3ID;->f:Z

    .line 545740
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 545737
    iget-boolean v0, p0, LX/3ID;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/3ID;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 545694
    iget-boolean v0, p0, LX/3ID;->k:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 545695
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_0

    .line 545696
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object p1

    .line 545697
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 545698
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 545699
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 545700
    :cond_1
    :goto_1
    :pswitch_0
    iget-boolean v0, p0, LX/3ID;->e:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/3ID;->f:Z

    if-eqz v0, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    return v2

    .line 545701
    :pswitch_1
    iput-boolean v1, p0, LX/3ID;->k:Z

    .line 545702
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, LX/3ID;->b:F

    .line 545703
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LX/3ID;->c:F

    .line 545704
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, LX/3ID;->d:I

    .line 545705
    invoke-direct {p0}, LX/3ID;->d()V

    goto :goto_1

    .line 545706
    :pswitch_2
    iget v0, p0, LX/3ID;->d:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 545707
    if-ltz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 545708
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    .line 545709
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    .line 545710
    iget v4, p0, LX/3ID;->b:F

    sub-float v3, v4, v3

    iput v3, p0, LX/3ID;->g:F

    .line 545711
    iget v3, p0, LX/3ID;->c:F

    sub-float v0, v3, v0

    iput v0, p0, LX/3ID;->h:F

    .line 545712
    iget v0, p0, LX/3ID;->g:F

    iget v3, p0, LX/3ID;->h:F

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 545713
    iget-boolean v7, p0, LX/3ID;->i:Z

    if-eqz v7, :cond_7

    .line 545714
    iput-boolean v5, p0, LX/3ID;->f:Z

    .line 545715
    iput-boolean v6, p0, LX/3ID;->e:Z

    .line 545716
    :cond_4
    :goto_2
    goto :goto_1

    .line 545717
    :pswitch_3
    iput-boolean v2, p0, LX/3ID;->k:Z

    .line 545718
    const/4 v0, -0x1

    iput v0, p0, LX/3ID;->d:I

    .line 545719
    invoke-direct {p0}, LX/3ID;->d()V

    goto :goto_1

    .line 545720
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 545721
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 545722
    iget v4, p0, LX/3ID;->d:I

    if-ne v3, v4, :cond_1

    .line 545723
    if-nez v0, :cond_5

    move v0, v1

    .line 545724
    :goto_3
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iput v3, p0, LX/3ID;->b:F

    .line 545725
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iput v3, p0, LX/3ID;->c:F

    .line 545726
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, LX/3ID;->d:I

    goto :goto_1

    :cond_5
    move v0, v2

    .line 545727
    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 545728
    :cond_7
    iget-boolean v7, p0, LX/3ID;->j:Z

    if-eqz v7, :cond_8

    .line 545729
    iput-boolean v6, p0, LX/3ID;->f:Z

    .line 545730
    iput-boolean v5, p0, LX/3ID;->e:Z

    goto :goto_2

    .line 545731
    :cond_8
    mul-float v7, v0, v0

    mul-float v8, v3, v3

    add-float/2addr v7, v8

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v7

    iget v9, p0, LX/3ID;->a:I

    int-to-double v9, v9

    cmpl-double v7, v7, v9

    if-lez v7, :cond_9

    move v5, v6

    .line 545732
    :cond_9
    div-float v7, v3, v0

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->atan(D)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v7

    .line 545733
    if-eqz v5, :cond_4

    .line 545734
    iget v5, p0, LX/3ID;->l:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v5, v9

    float-to-double v9, v5

    cmpg-double v5, v7, v9

    if-gez v5, :cond_a

    .line 545735
    iput-boolean v6, p0, LX/3ID;->e:Z

    goto :goto_2

    .line 545736
    :cond_a
    iput-boolean v6, p0, LX/3ID;->f:Z

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
