.class public final enum LX/3CA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3CA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3CA;

.field public static final enum DISABLED:LX/3CA;

.field public static final enum ENABLED:LX/3CA;


# instance fields
.field private final mShowPresence:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 529914
    new-instance v0, LX/3CA;

    const-string v1, "ENABLED"

    invoke-direct {v0, v1, v2, v3}, LX/3CA;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/3CA;->ENABLED:LX/3CA;

    .line 529915
    new-instance v0, LX/3CA;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v3, v2}, LX/3CA;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/3CA;->DISABLED:LX/3CA;

    .line 529916
    const/4 v0, 0x2

    new-array v0, v0, [LX/3CA;

    sget-object v1, LX/3CA;->ENABLED:LX/3CA;

    aput-object v1, v0, v2

    sget-object v1, LX/3CA;->DISABLED:LX/3CA;

    aput-object v1, v0, v3

    sput-object v0, LX/3CA;->$VALUES:[LX/3CA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 529917
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 529918
    iput-boolean p3, p0, LX/3CA;->mShowPresence:Z

    .line 529919
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3CA;
    .locals 1

    .prologue
    .line 529920
    const-class v0, LX/3CA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3CA;

    return-object v0
.end method

.method public static values()[LX/3CA;
    .locals 1

    .prologue
    .line 529921
    sget-object v0, LX/3CA;->$VALUES:[LX/3CA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3CA;

    return-object v0
.end method


# virtual methods
.method public final shouldShowPresence()Z
    .locals 1

    .prologue
    .line 529922
    iget-boolean v0, p0, LX/3CA;->mShowPresence:Z

    return v0
.end method
