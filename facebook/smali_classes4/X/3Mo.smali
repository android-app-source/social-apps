.class public LX/3Mo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static volatile f:LX/3Mo;


# instance fields
.field private final c:LX/3MK;

.field private final d:LX/2Ud;

.field private final e:LX/3MF;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 555810
    new-array v0, v3, [Ljava/lang/String;

    sget-object v1, LX/3MJ;->b:LX/0U1;

    .line 555811
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 555812
    aput-object v1, v0, v2

    sput-object v0, LX/3Mo;->a:[Ljava/lang/String;

    .line 555813
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/3MM;->a:LX/0U1;

    .line 555814
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 555815
    aput-object v1, v0, v2

    sget-object v1, LX/3MM;->d:LX/0U1;

    .line 555816
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 555817
    aput-object v1, v0, v3

    const/4 v1, 0x2

    sget-object v2, LX/3MM;->e:LX/0U1;

    .line 555818
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 555819
    aput-object v2, v0, v1

    sput-object v0, LX/3Mo;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/3MK;LX/2Ud;LX/3MF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 555833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555834
    iput-object p1, p0, LX/3Mo;->c:LX/3MK;

    .line 555835
    iput-object p2, p0, LX/3Mo;->d:LX/2Ud;

    .line 555836
    iput-object p3, p0, LX/3Mo;->e:LX/3MF;

    .line 555837
    return-void
.end method

.method public static a(LX/0QB;)LX/3Mo;
    .locals 6

    .prologue
    .line 555820
    sget-object v0, LX/3Mo;->f:LX/3Mo;

    if-nez v0, :cond_1

    .line 555821
    const-class v1, LX/3Mo;

    monitor-enter v1

    .line 555822
    :try_start_0
    sget-object v0, LX/3Mo;->f:LX/3Mo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 555823
    if-eqz v2, :cond_0

    .line 555824
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 555825
    new-instance p0, LX/3Mo;

    invoke-static {v0}, LX/3MK;->a(LX/0QB;)LX/3MK;

    move-result-object v3

    check-cast v3, LX/3MK;

    invoke-static {v0}, LX/2Ud;->a(LX/0QB;)LX/2Ud;

    move-result-object v4

    check-cast v4, LX/2Ud;

    invoke-static {v0}, LX/3MF;->a(LX/0QB;)LX/3MF;

    move-result-object v5

    check-cast v5, LX/3MF;

    invoke-direct {p0, v3, v4, v5}, LX/3Mo;-><init>(LX/3MK;LX/2Ud;LX/3MF;)V

    .line 555826
    move-object v0, p0

    .line 555827
    sput-object v0, LX/3Mo;->f:LX/3Mo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 555828
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 555829
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 555830
    :cond_1
    sget-object v0, LX/3Mo;->f:LX/3Mo;

    return-object v0

    .line 555831
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 555832
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/3Mo;J)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 555608
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 555609
    iget-object v1, p0, LX/3Mo;->e:LX/3MF;

    invoke-virtual {v1, p1, p2}, LX/3MF;->a(J)Ljava/lang/String;

    move-result-object v1

    .line 555610
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 555611
    sget-object v2, LX/3MM;->e:LX/0U1;

    .line 555612
    iget-object v4, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v4

    .line 555613
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 555614
    :cond_0
    sget-object v1, LX/3MM;->d:LX/0U1;

    .line 555615
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 555616
    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 555617
    sget-object v1, LX/3MM;->b:LX/0U1;

    .line 555618
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 555619
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 555620
    sget-object v1, LX/3MM;->c:LX/0U1;

    .line 555621
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 555622
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 555623
    invoke-static {p0, p1, p2, v0}, LX/3Mo;->a(LX/3Mo;JLandroid/content/ContentValues;)V

    .line 555624
    return-void
.end method

.method public static a(LX/3Mo;JLandroid/content/ContentValues;)V
    .locals 5

    .prologue
    .line 555791
    sget-object v0, LX/3MM;->a:LX/0U1;

    .line 555792
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 555793
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 555794
    iget-object v1, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, -0x2d56d968

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 555795
    :try_start_0
    iget-object v1, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "threads_table"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, p3, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 555796
    if-nez v0, :cond_1

    .line 555797
    sget-object v0, LX/3MM;->a:LX/0U1;

    .line 555798
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 555799
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 555800
    iget-object v0, p0, LX/3Mo;->e:LX/3MF;

    invoke-virtual {v0, p1, p2}, LX/3MF;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 555801
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 555802
    sget-object v1, LX/3MM;->e:LX/0U1;

    .line 555803
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 555804
    invoke-virtual {p3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 555805
    :cond_0
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "threads_table"

    const/4 v2, 0x0

    const v3, 0x4469fb98

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {v0, v1, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x472d9394

    invoke-static {v0}, LX/03h;->a(I)V

    .line 555806
    :cond_1
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 555807
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x71e72bf

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 555808
    return-void

    .line 555809
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, -0x5b560703

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public static a(LX/3Mo;Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 555777
    sget-object v0, LX/3MJ;->a:LX/0U1;

    .line 555778
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 555779
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 555780
    iget-object v1, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, -0x136ac309

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 555781
    :try_start_0
    iget-object v1, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "address_table"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, p2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 555782
    if-nez v0, :cond_0

    .line 555783
    sget-object v0, LX/3MJ;->a:LX/0U1;

    .line 555784
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 555785
    invoke-virtual {p2, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 555786
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "address_table"

    const/4 v2, 0x0

    const v3, -0x358f1376    # -3947298.5f

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {v0, v1, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x26aa4c19

    invoke-static {v0}, LX/03h;->a(I)V

    .line 555787
    :cond_0
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 555788
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x31416258

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 555789
    return-void

    .line 555790
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, 0x3036d591

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private static a(LX/3Mo;JLjava/util/List;Landroid/database/Cursor;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/database/Cursor;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 555770
    sget-object v1, LX/3MM;->e:LX/0U1;

    invoke-virtual {v1, p4}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 555771
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 555772
    invoke-static {p0, p1, p2}, LX/3Mo;->b(LX/3Mo;J)V

    .line 555773
    :cond_0
    :goto_0
    return v0

    .line 555774
    :cond_1
    invoke-static {p0, v1, p3}, LX/3Mo;->a(LX/3Mo;Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 555775
    invoke-static {p0, p1, p2}, LX/3Mo;->a(LX/3Mo;J)V

    .line 555776
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/3Mo;Ljava/lang/String;Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 555838
    iget-object v0, p0, LX/3Mo;->e:LX/3MF;

    invoke-virtual {v0, p1}, LX/3MF;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 555839
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 555840
    :goto_0
    return v0

    .line 555841
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 555842
    invoke-interface {p2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 555843
    goto :goto_0

    .line 555844
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/3Mo;J)V
    .locals 4

    .prologue
    .line 555762
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 555763
    iget-object v1, p0, LX/3Mo;->e:LX/3MF;

    invoke-virtual {v1, p1, p2}, LX/3MF;->a(J)Ljava/lang/String;

    move-result-object v1

    .line 555764
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 555765
    :goto_0
    return-void

    .line 555766
    :cond_0
    sget-object v2, LX/3MM;->e:LX/0U1;

    .line 555767
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 555768
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 555769
    invoke-static {p0, p1, p2, v0}, LX/3Mo;->a(LX/3Mo;JLandroid/content/ContentValues;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(JLjava/util/List;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 555742
    sget-object v0, LX/3MM;->a:LX/0U1;

    .line 555743
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 555744
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 555745
    :try_start_0
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "threads_table"

    const/4 v2, 0x0

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 555746
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0, p1, p2, p3, v1}, LX/3Mo;->a(LX/3Mo;JLjava/util/List;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 555747
    sget-object v0, LX/3MM;->d:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 555748
    if-eqz v1, :cond_0

    .line 555749
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 555750
    :cond_0
    :goto_0
    return-object v0

    .line 555751
    :cond_1
    if-eqz v1, :cond_2

    .line 555752
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move-object v0, v8

    .line 555753
    goto :goto_0

    .line 555754
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 555755
    :goto_2
    :try_start_2
    const-string v2, "SmsTakeoverThreadDbHandler"

    const-string v3, "Error getting group name"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 555756
    if-eqz v1, :cond_2

    .line 555757
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 555758
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_3
    if-eqz v1, :cond_3

    .line 555759
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 555760
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 555761
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 555722
    sget-object v0, LX/3MJ;->a:LX/0U1;

    .line 555723
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 555724
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 555725
    :try_start_0
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "address_table"

    sget-object v2, LX/3Mo;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 555726
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 555727
    sget-object v0, LX/3MJ;->b:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 555728
    if-eqz v1, :cond_0

    .line 555729
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 555730
    :cond_0
    :goto_0
    return-object v0

    .line 555731
    :cond_1
    if-eqz v1, :cond_2

    .line 555732
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move-object v0, v8

    .line 555733
    goto :goto_0

    .line 555734
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 555735
    :goto_2
    :try_start_2
    const-string v2, "SmsTakeoverThreadDbHandler"

    const-string v3, "Error getting smsc"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 555736
    if-eqz v1, :cond_2

    .line 555737
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 555738
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_3
    if-eqz v1, :cond_3

    .line 555739
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 555740
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 555741
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 555702
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 555703
    const v0, 0x2d9f4f6c

    invoke-static {v7, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 555704
    :try_start_0
    iget-object v0, p0, LX/3Mo;->d:LX/2Ud;

    invoke-virtual {v0}, LX/2Ud;->a()J

    move-result-wide v4

    .line 555705
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 555706
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v6, 0x1

    move-object v1, p0

    .line 555707
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 555708
    sget-object v9, LX/3MM;->b:LX/0U1;

    .line 555709
    iget-object p1, v9, LX/0U1;->d:Ljava/lang/String;

    move-object v9, p1

    .line 555710
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v9, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 555711
    sget-object v9, LX/3MM;->c:LX/0U1;

    .line 555712
    iget-object p1, v9, LX/0U1;->d:Ljava/lang/String;

    move-object v9, p1

    .line 555713
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, v9, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 555714
    invoke-static {v1, v2, v3, v0}, LX/3Mo;->a(LX/3Mo;JLandroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 555715
    goto :goto_0

    .line 555716
    :catch_0
    move-exception v0

    .line 555717
    :try_start_1
    const-string v1, "SmsTakeoverThreadDbHandler"

    const-string v2, "Error mark threads [read] in readonly mode"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 555718
    const v0, -0x73ed925e

    invoke-static {v7, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 555719
    :goto_1
    return-void

    .line 555720
    :cond_0
    :try_start_2
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 555721
    const v0, 0xf0d5f49

    invoke-static {v7, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_1

    :catchall_0
    move-exception v0

    const v1, 0x1e9c1a1c

    invoke-static {v7, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(JJILjava/util/List;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJI",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 555668
    iget-object v0, p0, LX/3Mo;->d:LX/2Ud;

    invoke-virtual {v0, p3, p4}, LX/2Ud;->a(J)J

    move-result-wide v0

    .line 555669
    const/4 v2, 0x1

    if-ne p5, v2, :cond_0

    .line 555670
    const/4 v0, 0x2

    new-array v0, v0, [LX/0ux;

    const/4 v1, 0x0

    sget-object v2, LX/3MM;->a:LX/0U1;

    .line 555671
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 555672
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/3MM;->b:LX/0U1;

    .line 555673
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 555674
    const-string v3, "2"

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    move-object v4, v0

    .line 555675
    :goto_0
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "threads_table"

    const/4 v2, 0x0

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 555676
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 555677
    sget-object v0, LX/3MM;->e:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 555678
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 555679
    invoke-static {p0, p1, p2}, LX/3Mo;->b(LX/3Mo;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 555680
    const/4 v0, 0x1

    if-eq p5, v0, :cond_1

    const/4 v0, 0x1

    .line 555681
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 555682
    :goto_2
    return v0

    .line 555683
    :cond_0
    const/4 v2, 0x3

    new-array v2, v2, [LX/0ux;

    const/4 v3, 0x0

    sget-object v4, LX/3MM;->a:LX/0U1;

    .line 555684
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 555685
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, LX/3MM;->c:LX/0U1;

    .line 555686
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 555687
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/0uu;->f(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x2

    sget-object v1, LX/3MM;->b:LX/0U1;

    .line 555688
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 555689
    const-string v3, "1"

    invoke-static {v1, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v2}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    .line 555690
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 555691
    :cond_2
    :try_start_1
    invoke-static {p0, v0, p6}, LX/3Mo;->a(LX/3Mo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_4

    .line 555692
    const/4 v0, 0x1

    if-eq p5, v0, :cond_3

    const/4 v0, 0x1

    .line 555693
    :goto_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 555694
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 555695
    :cond_4
    :try_start_2
    invoke-static {p0, p1, p2}, LX/3Mo;->a(LX/3Mo;J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 555696
    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 555697
    :goto_4
    const/4 v0, 0x1

    if-ne p5, v0, :cond_6

    const/4 v0, 0x1

    goto :goto_2

    .line 555698
    :catch_0
    move-exception v0

    .line 555699
    :try_start_3
    const-string v2, "SmsTakeoverThreadDbHandler"

    const-string v3, "Error checking thread state in readonly mode"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 555700
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_4

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 555701
    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b(JLjava/util/List;)LX/6fr;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/6fr;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 555633
    sget-object v0, LX/3MM;->a:LX/0U1;

    .line 555634
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 555635
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 555636
    :try_start_0
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "threads_table"

    const/4 v2, 0x0

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 555637
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0, p1, p2, p3, v1}, LX/3Mo;->a(LX/3Mo;JLjava/util/List;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 555638
    sget-object v0, LX/3MM;->f:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->d(Landroid/database/Cursor;)I

    move-result v0

    .line 555639
    sget-object v2, LX/3MM;->g:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->d(Landroid/database/Cursor;)I

    move-result v2

    .line 555640
    sget-object v3, LX/3MM;->h:LX/0U1;

    invoke-virtual {v3, v1}, LX/0U1;->d(Landroid/database/Cursor;)I

    move-result v3

    .line 555641
    sget-object v4, LX/3MM;->i:LX/0U1;

    invoke-virtual {v4, v1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 555642
    if-nez v0, :cond_2

    if-nez v2, :cond_2

    if-nez v3, :cond_2

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v5

    if-eqz v5, :cond_2

    .line 555643
    if-eqz v1, :cond_0

    .line 555644
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v8

    .line 555645
    :cond_1
    :goto_0
    return-object v0

    .line 555646
    :cond_2
    :try_start_2
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadCustomization;->newBuilder()LX/6fr;

    move-result-object v5

    .line 555647
    iput v0, v5, LX/6fr;->b:I

    .line 555648
    move-object v0, v5

    .line 555649
    iput v2, v0, LX/6fr;->c:I

    .line 555650
    move-object v0, v0

    .line 555651
    iput v3, v0, LX/6fr;->a:I

    .line 555652
    move-object v0, v0

    .line 555653
    iput-object v4, v0, LX/6fr;->e:Ljava/lang/String;

    .line 555654
    move-object v0, v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 555655
    if-eqz v1, :cond_1

    .line 555656
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 555657
    :cond_3
    if-eqz v1, :cond_4

    .line 555658
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    :goto_1
    move-object v0, v8

    .line 555659
    goto :goto_0

    .line 555660
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 555661
    :goto_2
    :try_start_3
    const-string v2, "SmsTakeoverThreadDbHandler"

    const-string v3, "Error querying thread customization"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 555662
    if-eqz v1, :cond_4

    .line 555663
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 555664
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_3
    if-eqz v1, :cond_5

    .line 555665
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 555666
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 555667
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 555625
    sget-object v0, LX/3MM;->d:LX/0U1;

    .line 555626
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 555627
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "%"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 555628
    :try_start_0
    iget-object v0, p0, LX/3Mo;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "threads_table"

    sget-object v2, LX/3Mo;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 555629
    :goto_0
    return-object v0

    .line 555630
    :catch_0
    move-exception v0

    .line 555631
    const-string v1, "SmsTakeoverThreadDbHandler"

    const-string v2, "Error searching group name"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v8

    .line 555632
    goto :goto_0
.end method
